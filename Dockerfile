FROM 004941777231.dkr.ecr.ap-southeast-2.amazonaws.com/parent:latest

RUN rm -rf /var/www/html
ADD weeloy.com /var/www/html

RUN rm -rf /var/www/html/.htaccess /var/www/html/tracking/.htaccess && \
cp -r /var/www/html/conf /etc/PHP_LIB/conf && \
cp -r /var/www/html/lib /etc/PHP_LIB/lib && \
cp -r /var/www/html/conf/conf.mysql_prod_sydney.inc.php /etc/PHP_LIB/conf/conf.mysql.inc.php && \
cp -r /var/www/html/conf/conf.init_prod_sydney.inc.php /etc/PHP_LIB/conf/conf.init.inc.php && \
cp -r /var/www/html/.htaccess_prod_sydney /var/www/html/.htaccess && \
cp -r /var/www/html/tracking/.htaccess_prod_sydney /var/www/html/tracking/.htaccess && \
cp -r /var/www/html/robots_disallow.txt /var/www/html/robots.txt && \
cp -r /var/www/html/.server/httpd/vhosts_sydney.conf /etc/apache2/sites-available/000-default.conf && \
cp -r /var/www/html/.server/aws/config_sydney /root/.aws/config && \
cp -r /var/www/html/.server/aws/credentials_sydney /root/.aws/credentials && \
rm -rf /var/www/html/.git /var/www/html/conf /var/www/html/.htaccess_dev /var/www/html/.htaccess_prod /var/www/html/tracking/.htaccess_dev /var/www/html/tracking/.htaccess_prod /etc/httpd/conf.d/vhosts.conf /etc/apache2/sites-available/default-ssl.conf awscli-bundle awscli-bundle.zip && \
chmod -R 777 /var/www/html/tmp

# Install app
#RUN cd /var/www && /usr/bin/composer install
#VOLUME [ "/var/www/vhosts" ]
# Add volume for sessions to allow session persistence
#VOLUME ["/var/www/vhosts", "/sessions", "${MYSQL_DATA_DIR}", "${MYSQL_RUN_DIR}"]
RUN chown -R www-data:www-data /var/www/html /var/log/xdebug && \
chmod -R g+ws /var/log && \
grep -rl 'css/style.css"' /var/www/html | xargs sed -i 's/css\/style.css\"/css\/style.css?'$(date +%s)'\"/g' && \
echo "`hostname -i` www.weeloy.asia" >> /etc/hosts
#RUN aws s3 cp /var/www/html/client/test.php s3://static.weeloy.asia/js/  --region ap-southeast-2 --acl public-read --cache-control 'public, max-age=2592000, s-maxage=2592000'
#RUN echo "`hostname -i` www.weeloy.asia" >> /etc/hosts 	# - repeted above