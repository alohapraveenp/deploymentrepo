 (function(app) {
    app.factory('API', API);
    API.$inject = [
        'PromotionService'
    ]; 
    function API(PromotionService) {
        var service = {
            promotions: PromotionService
        };
        return service;
    }
})(angular.module('ressources.md_marketing.api', ['ressources.md_marketing.promotions']));