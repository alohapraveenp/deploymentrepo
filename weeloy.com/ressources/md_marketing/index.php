<?php
    require_once 'mdconfig.init.inc.php';
//include __DIR__ . '/includes/header.php';


//echo '<br><br><br>';
////var_dump($_SESSION);
//
//        echo '<pre>';
//        print_r($_SESSION);
//        echo  '</pre>';



?>

<!DOCTYPE html>
<html lang="en" ng-app="MdWeeloyApp" ng-controller="RootCtrl">

    <head>
        <meta charset="UTF-8">
        <title ng-bind="title">
            <?php echo $title; ?>
        </title>
        <meta name="description" content="<?php echo $description; ?>" />
        <meta property="og:title" content="<?php echo $FacebookMetaTitle; ?>" />
        <meta property="og:type" content="website" />
        <meta property="og:image" content="<?php echo $header_image ?>" />
        <meta property="og:description" content="<?php echo $FacebookMetaDescription; ?>" />
        <meta property="og:locale" content="en_US" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="owner" content="weeloy.com">
        <meta name="apple-itunes-app" content="app-id=973030193">
        <base href="<?php echo __ROOTDIR__; ?>/">
        <link rel="shortcut icon" href="favicon.ico" title="favoris icone">

        <?php
            // include __DIR__ . '/criticalcss/restaurant_info_page.php';
            //include __DIR__ . '/criticalcss/home_page.php';

            ///$ua = strtolower($_SERVER['HTTP_USER_AGENT']);
//            if(stripos($ua,'android') !== false) { // && stripos($ua,'mobile') !== false) {
//                    echo '<link rel="stylesheet" type="text/css" href="mdclient/assets/css/app.min.css?v='.$app_version.'">';
//            }
        ?>

        <script type="text/javascript">
            var BASE_URL = "<?php echo __BASE_URL__; ?>";
            var BASE_PATH = "<?php echo $path; ?>";
                            
        </script>
    </head>

    <body>

        <header>
            <nav class="navbar navbar-default navbar-fixed-top" style='min-height: 60px;background-color:#fff'>
                <div class="container" style='min-height:60px;'>
                    <div class="navbar-header" style='min-height:60px;'>
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="<?php echo __BASE_URL__; ?>" title="Reserve restaurant with weeloy">
                            <img  src="images/logo.jpg" alt="Weeloy restaurant reservation">
                        </a>
                    </div>

                    <div  ng-include src="'mdclient/app/shared/partial/_menu.tpl.html'"></div>
                </div>
            </nav>
          </header>
        <div class="wrapper">
<!--            <div ng-if="true" ng-include="'mdclient/app/shared/partial/_message_wrapper.tpl.html'"></div>-->
            <div ng-view>
            </div>
        </div>
        <?php if(empty($no_footer)) {echo "<footer ng-include src=\"'mdclient/app/shared/partial/_footer.tpl.html'\"></footer>";}?>
        <script>
            var cb = function () {
                var l = document.createElement('link');
                l.rel = 'stylesheet';
                l.href = 'mdclient/assets/css/app.min.css?v=<?php echo "$app_version";?>';
                var h = document.getElementsByTagName('head')[0];
                h.parentNode.insertBefore(l, h);
            };
            var raf = requestAnimationFrame || mozRequestAnimationFrame ||
                    webkitRequestAnimationFrame || msRequestAnimationFrame;
            if (raf)
                raf(cb);
            else
                window.addEventListener('load', cb);
        </script>

<?php
if (strpos(__BASE_URL__, 'localhost') !== false || strpos(__BASE_URL__, '.dev') !== false) {
    echo '<script src="mdclient/weeloy.js?v='.$app_version.'" defer></script>';
} else {
    echo '<script src="mdclient/weeloy.min.js?v='.$app_version.'" defer></script>';
}
?>



        <!-- Google Tag Manager -->
        <noscript>
        <iframe src="//www.googletagmanager.com/ns.html?id=GTM-TQZ2W7" height="0" width="0" style="display:none;visibility:hidden"></iframe>
        </noscript>
        <script>
                            (function (w, d, s, l, i) {
                                w[l] = w[l] || [];
                                w[l].push({
                                    'gtm.start': new Date().getTime(),
                                    event: 'gtm.js'
                                });
                                var f = d.getElementsByTagName(s)[0],
                                        j = d.createElement(s),
                                        dl = l != 'dataLayer' ? '&l=' + l : '';
                                j.async = true;
                                j.src =
                                        '//www.googletagmanager.com/gtm.js?id=' + i + dl;
                                f.parentNode.insertBefore(j, f);
                            })(window, document, 'script', 'dataLayer', 'GTM-TQZ2W7');
        </script>
        <!-- End Google Tag Manager -->
 </body>

</html>
