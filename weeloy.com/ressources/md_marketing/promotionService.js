 (function(app) {
    app.service('PromotionService',['$http', '$q', function ($http, $q) {
        this.GetPromotions = function() {
            var deferred = $q.defer();
            return $http({method: 'GET', url: '../../api/services.php/event/getDetailedActiveEvents', cache: true })
              .success(function(response) {
                  console.log('PromotionService.GetPromotions(): Successful!');
                  deferred.resolve(response);
              }).error(function(response) {
                  console.log('PromotionService.GetPromotions(): Oops and error');
              });
              return deferred.promise;
          };
     }]);
 })(angular.module('ressources.md_marketing.promotions', []));