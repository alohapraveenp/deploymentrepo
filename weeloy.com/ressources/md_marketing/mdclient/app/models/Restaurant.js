function Restaurant(options) {
    this.GPS = '';
    this.ID = null;
    this.address = '';
    this.address1 = '';
    this.url = '';
    this.tel = '';
    this.trackingUrl = '';
    this.best_offer = {};
    this.best_offer_cpp = {};
    this.book_button = {};
    this.city = '';
    this.country = '';
    this.cuisine = [];
    this.currency = '';
    this.extraflag = '';
    this.hotelname = '';
    this.image = '';
    this.images = [];
    this.internal_path = '';
    this.is_bookable = null;
    this.is_displayed = null;
    this.is_favorite = null;
    this.is_wheelable = null;
    this.likes = 0;
    this.logo = '';
    this.map = '';
    this.mealtype = [];
    this.morder = '';
    this.openhours = '';
    this.pricediner = '';
    this.pricelunch = '';
    this.pricing = '';
    this.rating = '';
    this.region = '';
    this.restaurant = '';
    this.status = '';
    this.title = '';
    this.wheel = '';
    this.wheelImageUrl = '';
    this.wheelvalue = '';
    this.zip = '';
    this.video = '';
    this.services = [];
    this.description = [];
    this.menu = [];
    this.reviews = [];
    this.chef = [];
    this.takeoutrestaurant = 0;
    this.externalLink = 0;

    this.affiliate_program = 0;
    this.multiprod_allotment = 0;

    this.onlineordertnc = '';
    this.onlineordertax = '';
    this.minonlineorder = '';
    this.pictures = [];
    this.paypalflg = 0;
    this.gmapMarker = null;
    this.mediaServer = '//media.weeloy.com';

    BaseModel.call(this, options);
}

Restaurant.prototype = new BaseModel();

Restaurant.prototype.setMediaServer = function(mediaServer) {
    this.mediaServer = mediaServer;
};

Restaurant.prototype.setMenu = function(val) {
    if (Array.isArray(val)) {
        this.menu = val;
    }
    return this;
};

Restaurant.prototype.setChef = function(val) {
    this.chef = val;
    return this;
};

Restaurant.prototype.getImage = function(imageSizePath) {
    imageSizePath = imageSizePath || '/';
    var url = this.mediaServer + '/upload/restaurant/' + encodeURIComponent(this.restaurant) + imageSizePath + encodeURIComponent(this.image);
    return url;
};

Restaurant.prototype.getWheelImage = function(imageSizePath) {
    var url = this.mediaServer + '/upload/wheelvalues' + imageSizePath + 'wheelvalue_' + this.wheelvalue + '.png';
    return url;
};

Restaurant.prototype.getInternalPath = function() {
    var restaurant_details = this.restaurant.split('_');
    var city;
    switch (restaurant_details[1]) {
        case 'SG':
            city = 'singapore';
            break;
        case 'HK':
            city = 'hong-kong';
            break;
        case 'BK':
            city = 'bangkok';
            break;
        case 'PK':
            city = 'phuket';
            break;
        case 'KL':
            city = 'kuala-lumpur';
            break;
        case 'PR':
            city = 'paris';
            break;
        case 'SE':
            city = 'seoul';
            break;
        case 'MU':
            city = 'mumbai';
            break;
        default:
            city = 'singapore';
            break;
    }
    var type = 'restaurant';
    var restaurant_name = this.restaurant.substr(8);
    restaurant_name = restaurant_name.replace(/_/g, '');
    restaurant_name = restaurant_name.replace(/([A-Z])/g, '-$1');
    restaurant_name = restaurant_name.replace(/[-]+/, '-');
    restaurant_name = restaurant_name.toLowerCase();
    if (restaurant_name.charAt(0) == '-') {
        restaurant_name = restaurant_name.substr(1);
    }


    var restaurant_url = type + '/' + city + '/' + restaurant_name;
    return restaurant_url;
};


Restaurant.prototype.getBannerImage = function(imageSizePath) {
    imageSizePath = imageSizePath || '/1440/';
    var banner = this.mediaServer + '/upload/restaurant/' + this.restaurant + imageSizePath + encodeURIComponent(this.images[0]);
    return banner;
};

Restaurant.prototype.bannerStyle = function(imageSizePath) {
    var style = {
        backgroundImage: 'url(\'' + this.getBannerImage(imageSizePath) + '\')',
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
    };
    return style;
};

Restaurant.prototype.bannerStyleBookNow = function(imageSizePath) {
    var style = {
        background: 'linear-gradient(rgba(255, 255, 255, 0.30), rgba(255, 255, 255, 0.2)), url(\'' + this.getBannerImage(imageSizePath) + '\') ',
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
    };
    return style;
};

Restaurant.prototype.bannerStyleCart = function(imageSizePath) {
    var style = {
        backgroundImage: 'url(\'https://media.weeloy.com/upload/restaurant/SG_SG_R_TheFunKitchen/1440/catering3.jpg\')',
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
    };
    return style;
};

Restaurant.prototype.getRestaurantId = function() {
    return this.restaurant;
};

Restaurant.prototype.getTitle = function() {
    return this.title;
};

Restaurant.prototype.getRegion = function() {
    return this.region;
};

Restaurant.prototype.getStatus = function() {
    return this.status;
};

Restaurant.prototype.getAddress = function() {
    return this.address;
};

Restaurant.prototype.getZip = function() {
    return this.zip;
};

Restaurant.prototype.getCity = function() {
    return this.city;
};

Restaurant.prototype.getCountry = function() {
    return this.country;
};

Restaurant.prototype.getPhone = function() {
    return this.tel;
};

Restaurant.prototype.getUrl = function() {
    return this.url;
};

Restaurant.prototype.getWheelValue = function() {
    return this.wheelvalue;
};

Restaurant.prototype.getLogoImage = function() {
    var logo = this.mediaServer + '/upload/restaurant/' + this.restaurant + '/' + encodeURIComponent(this.logo);
    return logo;
};

Restaurant.prototype.getCuisineAsArray = function() {
    return this.cuisine.split('|');
};

Restaurant.prototype.getWheelAsArray = function() {
    var wheel = this.wheel.split('|');
    wheel.splice(0, 1);
    return wheel;
};

Restaurant.prototype.getLatitude = function() {
    var GPS = this.GPS;
    GPS = GPS.split(',');
    return {
        lat: GPS[0],
        lng: GPS[1]
    };
};

Restaurant.prototype.getDescription = function() {
    return this.description;
};

Restaurant.prototype.getCurrency = function() {
    return this.currency;
};

Restaurant.prototype.getChefType = function() {
    return this.chef.chef_type;
};

Restaurant.prototype.getGallery = function(imageSizePath) {
    imageSizePath = imageSizePath || /1440/;
    var images = [];
    var mediaServer = this.mediaServer;
    var restaurant = this.restaurant;
    this.images.forEach(function(img) {
        images.push({
            large: mediaServer + '/upload/restaurant/' + restaurant + imageSizePath + img,
            thumbnail: mediaServer + '/upload/restaurant/' + restaurant + '/140/' + img,
        });
    });
    return images;
};

Restaurant.prototype.getWheelRewardImage = function() {
    var wheel = this.mediaServer + '/upload/restaurant/' + this.restaurant + '/wheel.png?time=55ea4bf6d05e6';
    return wheel;
};

Restaurant.prototype.getWheelCPPRewardImage = function() {
    var wheel = this.mediaServer + '/upload/restaurant/' + this.restaurant + '/corporate_wheel.png?time=55ea4bf6d05e6';
    return wheel;
};

Restaurant.prototype.getMarker = function() {
    return this.gmapMarker;
};

Restaurant.prototype.setMarker = function(marker) {
    this.gmapMarker = marker;
    return this;
};

Restaurant.prototype.getOnlineOrderTnc = function() {
    return this.onlineordertnc;
};

Restaurant.prototype.getMinOnlineOrder = function() {
    return this.minonlineorder;
};

Restaurant.prototype.getOnlineOrderTax = function() {
    return this.onlineordertax;
};

Restaurant.prototype.getRestaurantIdFromUrlParam = function(city, restaurant) {
    var RestaurantID = '';
    switch (city) {
        case 'singapore':
            RestaurantID = 'SG_SG';
            break;
        case 'hong-kong':
            RestaurantID = 'HK_HK';
            break;
        case 'bangkok':
            RestaurantID = 'TH_BK';
            break;
        case 'phuket':
            RestaurantID = 'TH_PK';
            break;
        case 'paris':
            RestaurantID = 'FR_PR';
            break;
        case 'seoul':
            RestaurantID = 'KR_SE';
            break;
        case 'mumbai':
            RestaurantID = 'IN_MU';
            break;
        default:
            RestaurantID = 'SG_SG';
            break;
    }

    RestaurantID = RestaurantID + '_R_';

    function toPascalCase(str) {

        var arr = str.split(/\s|_|-/);
        for (var i = 0, l = arr.length; i < l; i++) {
            arr[i] = arr[i].substr(0, 1).toUpperCase() +
                (arr[i].length > 1 ? arr[i].substr(1).toLowerCase() : "");
        }
        return arr.join("");
    }
    RestaurantID = RestaurantID + toPascalCase(restaurant);
    return RestaurantID;
};

Restaurant.prototype.getBookNowPageUrl = function() {
    var url_section =this.multiprod_allotment > 0 ? "booknow-section" : "booknow";
    var url = this.getInternalPath() + "/"+url_section;
    return url;
};
