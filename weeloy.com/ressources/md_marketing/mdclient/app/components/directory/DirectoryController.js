var DirectoryController = angular.module('DirectoryController', ['NfSearch','DirectoryService']);
DirectoryController.controller('DirectoryCtrl', [
    '$rootScope',
    '$scope',
    '$location',
    'Directory',
    '$sce',
    '$window',
    function($rootScope, $scope, $location, Directory, $sce, $window) {
      
       
  
        $scope.items = [];
        $scope.itemstemp = [];
        $scope.restaurant = [];
        $scope.temp=null;

        $scope.loading = true;  
        var index = 0;
        $scope.restaurantsearch = [];


        $scope.query = {}
        $scope.queryBy = '$'

        Directory.getDirectoryList().then(function(response) {
     
              if(response.status == 1){
                 
                  var restaurantlist = response.data;
                  $scope.restaurantsearch = response.data;

                  for (i = 0; i < restaurantlist.length; i++) {
                        $scope.itemstemp[i] = restaurantlist[i];
                      
                      }
                      $scope.more();  
                      //$scope.restaurant = response.data;
              }
                    
              //console.log("RESPONSE " + JSON.stringify($scope.restaurant)); 
             
         });


        $scope.more = function()
        {
          
          temp = $scope.itemstemp.slice(index, index+20);
              for (x = 0; x < temp.length; x++) {
                    $scope.restaurant.push(temp[x]);
              }
              index=index+20;
        };


        //search for AZ
        $scope.searchaz = function (search) {
          $scope.restaurant =[];
              var convertarray = '';
              var ser = '';
              for (var i = 0, len = $scope.restaurantsearch.length; i < len; i++) {
                  convertarray = $scope.restaurantsearch[i].title;
                  convertarray = convertarray.toLowerCase();
                  ser = search.toLowerCase();
                  if (convertarray.match(ser)) 
                    {
                        $scope.restaurant.push($scope.restaurantsearch[i]);
                    }
              }
        }

        //search for Location
        $scope.searchloc = function (search) {
          $scope.restaurant =[];
              var convertarray = '';
              var ser = '';
              for (var i = 0, len = $scope.restaurantsearch.length; i < len; i++) {
                  convertarray = $scope.restaurantsearch[i].city;
                  convertarray = convertarray.toLowerCase();
                  ser = search.toLowerCase();
                  if (convertarray.match(ser)) 
                    {
                        $scope.restaurant.push($scope.restaurantsearch[i]);
                    }
              }
        }

        //search for Cuisine
        $scope.searchcus = function (search) {
          $scope.restaurant =[];
              var convertarray = '';
              var ser = '';
              for (var i = 0, len = $scope.restaurantsearch.length; i < len; i++) {
                  convertarray = $scope.restaurantsearch[i].cuisine;
                  convertarray = convertarray.toLowerCase();
                  ser = search.toLowerCase();
                  if (convertarray.match(ser)) 
                    {
                        $scope.restaurant.push($scope.restaurantsearch[i]);
                    }
              }
        }

        //search for Price
        $scope.searchprice = function (search) {
          $scope.restaurant =[];
              var convertarray = '';
              var ser = '';
              for (var i = 0, len = $scope.restaurantsearch.length; i < len; i++) {
                  convertarray = $scope.restaurantsearch[i].price;
                  convertarray = convertarray.toLowerCase();
                  ser = search.toLowerCase();
                  if (convertarray.match(ser)) 
                    {
                        $scope.restaurant.push($scope.restaurantsearch[i]);
                    }
              }
        }

        //search for Price
        $scope.searchds = function (search) {
          $scope.restaurant =[];
              var convertarray = '';
              var ser = '';
              for (var i = 0, len = $scope.restaurantsearch.length; i < len; i++) {
                  convertarray = $scope.restaurantsearch[i].dailyspecials;
                  convertarray = convertarray.toLowerCase();
                  ser = search.toLowerCase();
                  if (convertarray.match(ser)) 
                    {
                        $scope.restaurant.push($scope.restaurantsearch[i]);
                    }
              }
        }
    

 
    }]);

// we create a simple directive to modify behavior of <div>
  DirectoryController.directive("whenScrolled", function(){
    return{
      
      restrict: 'A',
      link: function(scope, elem, attrs){
      
        raw = elem[0];

        elem.bind("scroll", function(){
          if(raw.scrollTop+raw.offsetHeight+5 >= raw.scrollHeight){
            scope.loading = true;          
          // we can give any function which loads more elements into the list
            scope.$apply(attrs.whenScrolled);
          }
        });
      }
    }
  });
