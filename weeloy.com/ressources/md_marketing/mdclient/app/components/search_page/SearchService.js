var SearchService = angular.module('SearchService', []);
SearchService.service('Search', ['$http', '$q', function($http, $q) {
    this.getRestaurant = function(status, city, cuisine, tags, area, pricing, free_search, page) {
        if (status === undefined) {
            status = 'both';
        }
        if (city === undefined) {
            city = 'singapore';
        }
        if (city === 'hong-kong') {
            city = 'Hong Kong';
        }
        if (cuisine !== undefined) {
            if (Array.isArray(cuisine)) {
                cuisine = cuisine.join('|');
            }
        } else {
            cuisine = '';
        }
        if (tags !== undefined) {
            if (Array.isArray(tags)) {
                tags = tags.join('|');
            }
        } else {
            tags = '';
        }
        if (area !== undefined) {
            if (Array.isArray(area)) {
                area = area.join('|');
            }
        } else {
            area = '';
        }
        if (free_search === undefined) {
            free_search = '';
        }
        if (page === undefined) {
            page = 1;
        }
        if (pricing === undefined) {
            pricing = '';
        } else {
            switch (pricing) {
                case '$':
                    pricing = 1;
                    break;
                case '$$':
                    pricing = 2;
                    break;
                case '$$$':
                    pricing = 3;
                    break;
                default:
                    pricing = '';
                    break;
            }
        }
        var defferred = $q.defer();
        var params = {
            status: status,
            city: city,
            cuisine: cuisine,
            free_search: free_search,
            pricing: pricing,
            tags: tags,
            area: area
        };
        //$http.get('https://dev.weeloy.asia/api/search/restaurant', {
        $http.get('https://dev.weeloy.com/api/search/restaurant', {
            params: params,
            cache: true,
        }).success(function(response) {
            var restaurants = [];
            if (response.status === 1) {               
                if (response.data.no_result === true) {
                    defferred.reject('No result found');
                } else {
                    response.data.restaurant.forEach(function(value) {
                        restaurants.push(new Restaurant(value));
                    });
                }
            }
            defferred.resolve(restaurants);
        });
        return defferred.promise;
    };
    this.getBestRestaurants = function(city, limit) {
        if (city === undefined) {
            city = 'singapore';
        }
        if (city === 'hong-kong') {
            city = 'Hong Kong';
        }
        if (limit === undefined) {
            limit = 1;
        }
        var defferred = $q.defer();
        var API_URL = 'https://dev.weeloy.asia/api/restaurant/getShowcase/' + city + '/' + limit;
        $http.get(API_URL, {
            cache: true
        }).success(function(response) {
            defferred.resolve(response);
        });
        return defferred.promise;
    };
    this.insertSitemap = function(url, nbresult) {
        $http.post('https://dev.weeloy.asia/api/sitemap/insert', {
            url: url,
            nbresult: nbresult
        }).success(function(response) {});
    };
}]);
