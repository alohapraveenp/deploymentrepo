var DailySpecialController = angular.module('DailySpecialController', ['NfSearch','DailySpecialService', 'ngFileUpload']);

DailySpecialController.controller('DailySpecialCtrl', [
    '$rootScope',
    '$scope',
    '$location',
    'DailySpecial',
    '$sce',
    '$window',
    'Upload',
    '$http',
    function($rootScope, $scope, $location, DailySpecial, $sce, $window,Upload,$http) {
        
        $scope.restaurant = null;
  
        $scope.items = [];
        $scope.itemstemp = [];
        $scope.temp=null;
        $scope.cuisinelist = [];
        $scope.today = new Date();
        //cheat $scope.today.setDate($scope.today.getDate() + 1);
        $scope.today = $scope.today.getFullYear() + "-" +   ("0"+($scope.today.getMonth()+1)).slice(-2) + "-" + ("0" + $scope.today.getDate()).slice(-2);

        $scope.loading = true;  

        $scope.initdsb = function() {
            $scope.restaurantdsb = {
              restaurant: "",
              code: "",
              headline: "",
              description: "",
              status: "dsb",
              selectedOption: {
                cat1: "",
                cat2: "",
                cat3: ""
              },
              images: {
                cover: {}
              }
            };

            $scope.login = {
              email: "",
              password: ""
            };
          }  

        var index = 0;
         DailySpecial.getDailySpecial().then(function(response) {
            $scope.dsblist = null;
              var dsblistcheck = response.status;
              
              if(dsblistcheck == 1)
              {
                var dsblist = response.data;

                if(dsblist.length == 0)
                {
                  alert("There are no Daily Specials for Today");
                }

                for (i = 0; i < dsblist.length; i++) {
                      cc = dsblist[i];
                      $scope.itemstemp[i] = cc;
                    }
                    $scope.more();  
              }
              else
              {
                $scope.loading = false;
                //alert(response.data.errors);
              } 
             
         });

         DailySpecial.getcuisine().then(function(response) {

              var cuisinecheck = response.status;
              if(cuisinecheck == 1)
              {

                $scope.cuisinelist = response.data.cuisine;

              }
              else
              {
                //$scope.loading = false;
                alert(response.data.errors);
              } 
             
         });

         $scope.initdsb();

        $scope.more = function()
        {
          $scope.loading = false;
          temp = $scope.itemstemp.slice(index, index+10);
              for (x = 0; x < temp.length; x++) {
                    cc = temp[x];
                    $scope.items.push(cc);
              }
              index=index+10;

        };


        $scope.saveitemnew = function(Dailyspecialform) {

          DailySpecial.postdailyspecial($scope.restaurantdsb,$scope.login).then(function(response) {
              if (response == 'Done')
              {
                  alert("Daily Specials has been posted");
                  $scope.initdsb();
                  // $scope.restaurantdsb.restaurant = '';
                  // $scope.restaurantdsb.code = '';
                  // $scope.restaurantdsb.headline = '';
                  // $scope.restaurantdsb.description = '';
                  // $scope.restaurantdsb.selectedOption.cat1 = 'Select Categories';
                  // $scope.restaurantdsb.selectedOption.cat2 = 'Select Categories';
                  // $scope.restaurantdsb.selectedOption.cat3 = 'Select Categories';
                  // $scope.login.email = '';
                  // $scope.login.password = '';
              }
              else if (response == 'Email')
              {
                  alert("Wrong Email Address of Password");
              }
          });
        };


        $scope.uploadImagedsb = function(element)  {

          if(element.files.length == 0) {
            alert("Select/Drop one file");
            return false;
          }
          filename = element.files[0].name;
          
          ext = filename.substring(filename.lastIndexOf(".")+1).toLowerCase();
          if(ext != "jpg" && ext != "jpeg" && ext != "png" && ext != "gif") {
            alert("Invalid file type (jpg, jpeg, png, gif)");
            return false;
          }


          filename = element.files[0].name;    
          ext = filename.substring(filename.lastIndexOf(".")+1).toLowerCase();
          if(ext != "jpg" && ext != "jpeg" && ext != "png" && ext != "gif") {
            alert("Invalid file type (jpg, jpeg, png, gif)");
            return false;
          }
          
          var reader = new FileReader();
          $scope.restaurantdsb.images.cover.name = element.files[0].name;
          $scope.restaurantdsb.images.cover.media_type = 'picture';
          reader.addEventListener("load", function () {
            $scope.restaurantdsb.images.cover.content = reader.result;      
          }, false);
          reader.readAsDataURL(element.files[0]);

        };


        $scope.showregistration = function()
        {
          $('#dbsformmodal').modal('hide'); 
          $('#dbsformmodal').on('hidden.bs.modal', function (e) {
            $('#dbsregistrationformmodal').modal('show'); 
          })
         
        }

        // setTimeout(function(){
        //   $('#dbsformmodal').modal('show');
        //  console.log("Delay in PopUp");
        // }, 2000);
         



        //daily special registration part code
        $scope.predicate = '';
        $scope.reverse = false;
        $scope.tabletitleContent = [
                { a:'title', b:'Title', c:'', d:'tower', t:'input', i:0, r:false, type:'text' },
                { a:'firstname', b:'First Name', c:'', d:'pencil', t:'input', i:0, r:true, type:'text' },
                { a:'lastname', b:'Last Name', c:'', d:'pencil', t:'input', i:0, r:true, type:'text' },
                { a:'email', b:'Email', c:'', d:'inbox', t:'input', i:0, r:true, type:'email' },
                { a:'phone', b:'Phone', c:'', d:'phone', t:'input', i:0, r:false, type:'text' },
                { a:'owner', b:'I am the authorized representative of this business', c:'', d:'user', t:'checkbox', i:0 },
                { a:'name', b:'Restaurant Name', c:'', d:'pencil', t:'input', i:0, r:true, type:'text' },
                { a:'address', b:'Street Address', c:'', d:'map-marker', t:'input', i:0, r:false, type:'text' },
                { a:'city', b:'City', c:'', d:'pencil', t:'input', i:0, r:true, type:'text' },
                { a:'country', b:'Country', c:'', d:'pencil', t:'input', i:0, r:true, type:'text' },
                { a:'postalcode', b:'Postal Code', c:'', d:'pencil', t:'input', i:0, r:false, type:'text' },
                { a:'restaurantemail', b:'Restaurant Email', c:'', d:'inbox', t:'input', i:0, r:true, type:'email' },
                { a:'restaurantphone', b:'Restaurant Phone', c:'', d:'phone', t:'input', i:0, r:false, type:'text' },
                { a:'cuisines', b:'Cuisines', c:'', d:'cutlery', t:'input', i:0, r:false, type:'text' },
                { a:'website', b:'Website', c:'', d:'home', t:'input', i:0, r:true, type:'url' },
                { a:'pricepoint', b:'Price Point', c:'', d:'usd', t:'input', i:0, r:false, type:'text' },
                { a:'description', b:'Description', c:'', d:'comment', t:'textarea', i:0 },
                { a:'logo', b:'Logo', c:'', d:'picture', t:'imagebutton' },
                { a:'cover', b:'Cover', c:'', d:'picture', t:'imagebutton' },
                { a:'type', b:'Select Listing Type', c:'', d:'list', t:'dropdown', val: ["Basic - Free", "Enhanced", "Partner"] },
              ];
        $scope.path ="https://media.weeloy.com/upload/restaurant/";
        $scope.restaurant = null;
        $scope.showCaptcha = window.location.toString().indexOf("admin_weeloy") < 0;
        $scope.init = function() {
          $scope.restaurant = {
            title: "",
            firstname: "",
            lastname: "",
            email: "",
            phone: "",
            owner: false,
            name: "",
            address: "",
            city: "",
            country: "",
            postalcode: "",
            restaurantemail: "",
            restaurantphone: "",
            cuisines: "",
            website: "",
            pricepoint: "",
            images: {
              logo: {},
              cover: {}
            },
            type: "Basic - Free",
            status: "dsb"
          };
          $('#logo').files = [];
          $('#cover').files = [];
        }
        $scope.file = null;
        $scope.init();
        $scope.cleaninput = function(ll) {
          if(typeof $scope[ll] === 'string')
            $scope[ll] =  $scope[ll].replace(/\'|\"/g, '’');
        };
        $scope.currentDropElement = null;
        $scope.onDragStart = function(data, dragElement, dropElement) {};
        $scope.onDragEnd = function(data, dragElement, dropElement) {};
        $scope.onDragOver = function(data, dragElement, dropElement) {
          $scope.currentDropElement = dropElement;
        };
        $scope.onDragLeave = function() {
          $scope.currentDropElement = null;
        };
        $scope.onDrop = function(data,dragElement,dropElement,event,label) {
          var lbl =$("#"+label).attr('for');
          console.log(lbl);
          console.log(data);
          if (data && $scope.currentDropElement) {
            if(lbl=="notassigned") {
              $scope.notassigned.push(data);
              $scope.remove($scope.assigned, data);
            } else {
              $scope.assigned.push(data);
              $scope.remove($scope.notassigned, data);
            }
          }
        };
        $scope.saveitem = function(RegistrationForm) {
          
            if (RegistrationForm.$invalid) {
            var top;
            if (RegistrationForm.name.$invalid || RegistrationForm.country.$invalid || RegistrationForm.city.$invalid || RegistrationForm.email.$invalid || RegistrationForm.restaurantemail.$invalid || RegistrationForm.website.$invalid) {
            top = $('input[name="title"]').offset().top;
            $(window).scrollTop(top - 200);
            }
            return false;
            }
           
            DailySpecial.restaurantregistration($scope.restaurant).then(function(response) {


              if (response == 'Done')
              {
                  alert("Daily Specials has been posted");
                  $scope.init();
                  // $scope.restaurant.title = '';
                  // $scope.restaurant.firstname = '';
                  // $scope.restaurant.lastname = '';
                  // $scope.restaurant.email = '';
                  // $scope.restaurant.phone = '';
                  // $scope.restaurant.owner = false;
                  // $scope.restaurant.name = '';
                  // $scope.restaurant.address = '';
                  // $scope.restaurant.city = '';
                  // $scope.restaurant.country = '';
                  // $scope.restaurant.postalcode = '';
                  // $scope.restaurant.restaurantemail = '';
                  // $scope.restaurant.restaurantphone = '';

                  // $scope.restaurant.cuisines = '';
                  // $scope.restaurant.website = '';
                  // $scope.restaurant.pricepoint = '';
              }
          });




          // var u, msg, apiurl, ind;
          // // if($scope.showCaptcha && grecaptcha.getResponse() == "") {
          // //     alert("You can't proceed!");
          // //     return false;
          // // }
          // if (RegistrationForm.$invalid) {
          //   var top;
          //   if (RegistrationForm.name.$invalid || RegistrationForm.country.$invalid || RegistrationForm.city.$invalid || RegistrationForm.email.$invalid || RegistrationForm.restaurantemail.$invalid || RegistrationForm.website.$invalid) {
          //     top = $('input[name="title"]').offset().top;
          //     $(window).scrollTop(top - 200);
          //   }
          //   return false;
          // }

          // var url = window.location.toString();
          // var index = url.indexOf("weeloy.com");
          // if (index >= 0)
          //   url = url.slice(0, index) + "weeloy.com/";
          // else {
          //   index = url.indexOf("weeloy.asia");
          //   if (index >= 0)
          //     url = url.slice(0, index) + "weeloy.asia/";
          // }
          // var params = {httpMethod: "POST", body: {'api': 'dsb', 'operation': 'NewRestaurant', 'data': $scope.restaurant}};
          // var xhr = new XMLHttpRequest();
          // xhr.open("POST", "https://api.weeloy.asia/dsbnewrestaurant", true);
          // xhr.onreadystatechange = function() {
          //     if (xhr.readyState == XMLHttpRequest.DONE)      
          //       console.log("POST successful!");
          // }
          // xhr.setRequestHeader("Content-type", "application/json");
          // xhr.send(JSON.stringify(params));
        };
        $scope.uploadImage = function(element)  {
          
          if(element.files.length == 0) {
            alert("Select/Drop one file");
            return false;
          }
          filename = element.files[0].name;    
          ext = filename.substring(filename.lastIndexOf(".")+1).toLowerCase();
          if(ext != "jpg" && ext != "jpeg" && ext != "png" && ext != "gif") {
            alert("Invalid file type (jpg, jpeg, png, gif)");
            return false;
          }
          var reader = new FileReader();
          $scope.restaurant.images[element.id].name = element.files[0].name;
          $scope.restaurant.images[element.id].media_type = 'picture';
          
          reader.addEventListener("load", function () {
            
            $scope.restaurant.images[element.id].content = reader.result;      
          }, false);
          reader.readAsDataURL(element.files[0]);

        };

 
    }]);

// we create a simple directive to modify behavior of <div>
DailySpecialController.directive("whenScrolled", function(){
  return{
    
    restrict: 'A',
    link: function(scope, elem, attrs){
    
      raw = elem[0];

      elem.bind("scroll", function(){
        if(raw.scrollTop+raw.offsetHeight+5 >= raw.scrollHeight){
          scope.loading = true;          
        // we can give any function which loads more elements into the list
          scope.$apply(attrs.whenScrolled);
        }
      });
    }
  }
});



