var DailySpecialService = angular.module('DailySpecialService', []);
DailySpecialService.service('DailySpecial', ['$http', '$q', function($http, $q) {
    this.getDailySpecial = function() {
        var defferred = $q.defer();
        // local var API_URL = 'http://localhost:8888/weeloy.com/api/md_marketing/getdailyspecial/';
         // var API_URL = 'https://dev.weeloy.asia/api/md_marketing/getdailyspecial/';
        var API_URL = 'http://localhost:8888/weeloy.com/api/dailyboard.php/md_dailyspecial/getdailyspecial';
        //var API_URL = 'https://dev.weeloy.asia/api/dailyboard.php/md_dailyspecial/getdailyspecial';
        $http.post(API_URL, {
            "body":{
              "data": {
                "code": ""  
              }
            }
        }).success(function(response) {
            defferred.resolve(response);
        });
        return defferred.promise;
    };

    this.getcuisine = function() {
        var defferred = $q.defer();
        
        // local var API_URL = 'http://localhost:8888/weeloy.com/api/cuisinelist';
         var API_URL = 'https://dev.weeloy.asia/api/cuisinelist';
        $http.get(API_URL, {
            cache: true
        }).success(function(response) {
            defferred.resolve(response);
        });
        return defferred.promise;
    };

    this.postdailyspecial = function(restaurantdsb,login) {
        var defferred = $q.defer();

        $http({
                  //url: "https://dev.weeloy.asia/api/md_marketing/dsb/login/",
                  url: "http://localhost:8888/weeloy.com/api/md_marketing/dsb/login/",
                  data:$.param(
                          {'login': login }
                        ),
                  method: 'POST',
                  processData: false,
                  contentType: false,
                  headers: {
                  'Content-Type': 'application/x-www-form-urlencoded'
                }
          }).success(function(response) {
              
              logincheck = response.status;
              if (logincheck == 1)
              {
                restaurant = response.data.row;
                restaurantdsb.restaurant = restaurant[0].content;
                restaurantdsb.code = restaurant[1].content;

                var url = window.location.toString();
                var index = url.indexOf("weeloy.com");
                if (index >= 0)
                  url = url.slice(0, index) + "weeloy.com/";
                else {
                  index = url.indexOf("weeloy.asia");
                  if (index >= 0)
                    url = url.slice(0, index) + "weeloy.asia/";
                }
                var params = {httpMethod: "POST", body: {'api': 'dsb', 'operation': 'DSBNewDailyPost', 'data': restaurantdsb}};
                var xhr = new XMLHttpRequest();
                // xhr.open("POST", "https://api.weeloy.asia/dsbnewdailypost", true);
                xhr.open("POST", "https://dev.weeloy.asia/api/dailyboard.php/md_dailyspecial/postdailyspecial", true);
                // xhr.open("POST", "http://localhost:8888/weeloy.com/api/dailyboard.php/md_dailyspecial/postdailyspecial", true);
                xhr.onreadystatechange = function() {
                    if (xhr.readyState == XMLHttpRequest.DONE)
                      console.log("POST successful!");
                      defferred.resolve("Done");
                }
                xhr.setRequestHeader("Content-type", "text/plain");
                xhr.send(JSON.stringify(params));
                }
                else
                {
                    defferred.resolve("Email");
                }
          });

        return defferred.promise;
    };


    this.restaurantregistration = function(restaurant) {
        var defferred = $q.defer();
        
        var u, msg, apiurl, ind;
        var url = window.location.toString();
        var index = url.indexOf("weeloy.com");
        if (index >= 0)
        url = url.slice(0, index) + "weeloy.com/";
        else {
        index = url.indexOf("weeloy.asia");
        if (index >= 0)
        url = url.slice(0, index) + "weeloy.asia/";
        }
        
        var params = {httpMethod: "POST", body: {'api': 'dsb', 'operation': 'NewRestaurant', 'data': restaurant}};
        var xhr = new XMLHttpRequest();
        
        xhr.open("POST", "https://api.weeloy.asia/dsbnewrestaurant", true);
        xhr.onreadystatechange = function() {
        if (xhr.readyState == XMLHttpRequest.DONE)   

        console.log("POST successful!");
        defferred.resolve("Done");
        }
        xhr.setRequestHeader("Content-type", "text/plain");
        xhr.send(JSON.stringify(params));


        return defferred.promise;
    };


}]);
