var NfSearch = angular.module('NfSearch', []);
NfSearch.run(['$rootScope', function($rootScope) {}]);
NfSearch.directive('nfSearch', [
    '$rootScope',
    '$http',
    '$location',
    '$route',
    '$routeParams',
    //'params',
    function($rootScope, $http, $location, $route, $routeParams) {
        return {
            restrict: 'AE',
            scope: {
                url: '@',
                cities: '=',
                placeholder: '=',
                buttonText: '=',
                hideInputOnMobile: '@',
                isMobile: '=',
            },
            templateUrl: 'mdclient/app/shared/partial/_search_form.html',
            link: function(scope, element, attrs) {
                console.log("ROOTPARAMS " + JSON.stringify($routeParams));

                scope.screenWidth = $(window).width();
                $(window).resize(function() {
                    scope.screenWidth = $(window).width();
                    scope.$apply();
                });

                scope.selectItemEvt = false;
                var API_URL;
                if (scope.buttonText === undefined) {
                    scope.buttonText = 'Search';
                }
                if (scope.nfSearch === undefined || scope.nfSearch === '') {
                    API_URL = 'api/search/fulltext';
                } else {
                    API_URL = scope.nfSearch;
                }
                if ($rootScope.SearchParams.query !== undefined && $rootScope.SearchParams.query.data !== '') {
                    scope.free_search = $rootScope.SearchParams.query.data;
                }

                if (scope.cities === undefined) {
                    scope.cities = [{
                        id: 1,
                        name: 'Singapore',
                        data: 'singapore',
                    }, {
                        id: 2,
                        name: 'Bangkok',
                        data: 'bangkok',
                    }, {
                        id: 3,
                        name: 'Phuket',
                        data: 'phuket',
                    }, {
                        id: 4,
                        name: 'Hong Kong',
                        data: 'hong-kong',
                    }];
                }

                if ($rootScope.UserSession !== undefined && $rootScope.UserSession.search_city !== undefined && scope.cities !== undefined) {
                    scope.cities.forEach(function(value, key) {
                        if (value.name.toLowerCase() == $rootScope.UserSession.search_city.toLowerCase()) {
                            scope.city = scope.cities[key];
                        }
                    });
                } else {
                    scope.city = scope.cities[0];
                }
                if ($rootScope.SearchParams.city !== undefined && $rootScope.SearchParams.city.data !== undefined && $rootScope.SearchParams.city !== '') {
                    scope.cities.forEach(function(city) {
                        if (city.data == $rootScope.SearchParams.city.data) {
                            scope.city = city;
                        }
                    });
                }



                var Input = $(element).find('.search-input');

                scope.SearchFormSelectCity = function(city) {
                    $rootScope.UserSession.search_city = city.data;
                    $rootScope.$broadcast('ChangeCity', city);
                    scope.city = city;
                    //params.update('city', city.data);
                    //params.update('query', '');
                    $rootScope.getSearchParams();
                    Input.typeahead('destroy');
                    localStorage.removeItem('__restaurants_bloodhood_cache_key__data');
                    var restaurants = RestaurantResourceInit(city);
                    TypeaheadInit(Input, restaurants, cuisinelist);
                };

                $rootScope.showAutoCorrect = false;
                var cuisinelist = CuisineListInit();
                var restaurants = RestaurantResourceInit(scope.city);

                TypeaheadInit(Input, restaurants, cuisinelist);

                function RestaurantResourceInit(city) {
                    var prefetch_url = API_URL + '?city=' + city.name;
                    var remote_url = API_URL + "?city=" + city.name + "&query=%QUERY*";
                    var restaurants = new Bloodhound({
                        datumTokenizer: function(d) {
                            var test = Bloodhound.tokenizers.whitespace(d.title);
                            $.each(test, function(k, v) {
                                i = 0;
                                while ((i + 1) < v.length) {
                                    test.push(v.substr(i, v.length));
                                    i++;
                                }
                            });
                            return test;
                        },
                        queryTokenizer: Bloodhound.tokenizers.whitespace,
                        prefetch: {
                            url: prefetch_url,
                            thumbprint: 'ver 1.4',
                            cacheKey: 'restaurants_bloodhood_cache_key',
                            transform: function(response) {
                                var restaurant;
                                var data = response.data.restaurants;
                                data.forEach(function(value, key) {
                                    data[key].cuisine = data[key].cuisine.replace("|", " ");
                                    restaurant = new Restaurant(value);
                                    data[key].internal_path = restaurant.getInternalPath();
                                });
                                return data;
                            },
                        },
                        remote: {
                            url: remote_url,
                            wildcard: '%QUERY',
                            transform: function(response) {
                                var current = [];
                                $('.tt-suggestion').each(function() {
                                    current.push($(this).text());
                                });
                                var data = [];
                                response.data.restaurants.forEach(function(value, key) {
                                    if (current.indexOf(value.title) < 0) {
                                        response.data.restaurants[key].cuisine = value.cuisine.replace("|", " ");
                                        restaurant = new Restaurant(value);
                                        response.data.restaurants[key].internal_path = restaurant.getInternalPath();
                                        data.push(response.data.restaurants[key]);
                                    }
                                });
                                return data;
                            },
                        },
                    });
                    return restaurants;
                }

                function CuisineListInit() {
                    var cuisinelist = new Bloodhound({
                        datumTokenizer: function(d) {
                            var test = Bloodhound.tokenizers.whitespace(d.cuisine);
                            $.each(test, function(k, v) {
                                i = 0;
                                while ((i + 1) < v.length) {
                                    test.push(v.substr(i, v.length));
                                    i++;
                                }
                            });
                            return test;
                        },
                        queryTokenizer: Bloodhound.tokenizers.whitespace,
                        prefetch: {
                            url: "api/cuisinelist",
                            transform: function(response) {
                                var data = response.data.cuisine;
                                return data;
                            },
                        }
                    });
                    return cuisinelist;
                }

                function TypeaheadInit(Input, restaurants, cuisinelist) {
                    Input.typeahead({
                        minLength: 1,
                        highlight: true,
                        hint: true,
                    }, {
                        name: 'title',
                        display: 'title',
                        source: restaurants,
                        templates: {
                            header: '<p class="search-box-header"><strong>Restaurant</strong><p>',
                        }
                    }, {
                        name: 'cuisine',
                        display: 'cuisine',
                        source: cuisinelist,
                        templates: {
                            header: '<p class="search-box-header"><strong>Cuisine</strong><p>',
                        }
                    });
                }
                Input.bind('typeahead:render', function(evt, suggestions, flag, name) {
                    if (suggestions === undefined) {
                        var length = $('.twitter-typeahead').find('.tt-suggestion').length;
                        if (length === 0) {
                            if ($('#auto_correct').attr('query') === Input.val() || element.val() === '') {
                                return;
                            }
                            var params = {
                                query: Input.val(),
                                city: scope.city.name,
                            };
                            $('#auto_correct').attr('query', Input.val());
                            $http.get('../api/search/fulltext', {
                                params: params
                            }).success(function(response) {
                                if (response.status == 1) {
                                    $rootScope.showAutoCorrect = true;
                                    $rootScope.suggestions = response.data.restaurants;
                                }
                            });
                        }
                    } else {
                        $rootScope.showAutoCorrect = false;
                    }
                });
                // change ng-model when typeahead:select
                Input.bind('typeahead:select', function(evt, data) {
                    if (data.title !== undefined) {
                        scope.selectItemEvt = true;
                        var url = BASE_URL + '/' + data.internal_path;
                        window.open(url, "_self");
                    } else {
                        // scope.free_search = data.cuisine;
                        scope.selectItemEvt = true;
                        $location.path('search/' + $rootScope.SearchParams.city.data + '/cuisine/' + data.cuisine);
                    }
                });
                scope.SearchInputKeyUp = function(query, city, evt) {
                    if (evt.keyCode == 13 && !scope.selectItemEvt) {
                        go_search(evt.target.value, city);
                    }
                };
                scope.Search = function(query, city) {
                    console.log("IN SEARCH function" + JSON.stringify(city));

                    go_search(query, city);
                };

                function go_search(query, city) {
                    var new_path;
                    if (query !== undefined) {
                        new_path = '/search/' + city.data + '/' + query;
                    } else {
                        new_path = '/search/' + city.data;
                    }
                    //window.locaion.redirect = new_path;
                    console.log("NEW PATH " + new_path);
                    $location.path(new_path);
                }
            },
        };
    }
]);
