var LengthFilter = angular.module('LengthFilter', []);
LengthFilter.filter('length', function() {
    return function(array, length) {
        if (array == undefined) {
            return true;
        } else {
            if (array.length == length) {
                return true;
            } else {
                return false;
            };
        };
    };
});
LengthFilter.filter('lengthMoreThan', function() {
    return function(array, length) {
        if (array == undefined) {
            return false;
        } else {
            if (array.length > length) {
                return true;
            } else {
                return false;
            };
        };
    };
});

LengthFilter.filter('lengthLessThan', function() {
    return function(array, length) {
        if (array == undefined) {
            return false;
        } else {
            if (array.length < length) {
                return true;
            } else {
                return false;
            };
        };
    };
});
