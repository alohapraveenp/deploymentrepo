var TitleAndMetaTag = angular.module('TitleAndMetaTag', []);
TitleAndMetaTag.service('TitleAndMeta', ['$rootScope', function($rootScope) {
    this.setTitle = function(title) {
        setTitle(title);
    };

    function setTitle(title) {
        $rootScope.title = title;
    };
    this.setMetaDescription = function(description) {
        setMetaDescription(description);
    };

    function setMetaDescription(description) {
        $('meta[name="description"]').attr('content', description);
    };
    this.setFaceBookMetaTitle = function(facebookMetaTitle) {
        setFaceBookMetaTitle(facebookMetaTitle);
    };
    function setFaceBookMetaTitle(facebookMetaTitle) {
        $('meta[property="og:title"]').attr('content', facebookMetaTitle);
    };
    this.setFaceBookMetaDescription = function(description) {
        setFaceBookMetaDescription(description);
    };

    function setFaceBookMetaDescription(description) {
        $('meta[property="og:description"]').attr('content', description);
    };
    this.setCity = function(city, page) {
        // Restaurant info page meta data has been set in /client/app/components/restaurant_info_page/RestaurantInfoController.js
        // All Rewards page meta data has been set in /client/app/components/all_rewards_page/AllRewardsController.js
        switch (page) {
            case 'HomeCtrl':
                setHomePageMeta(city);
                break;
            case 'SearchCtrl':
                setSearchPageMeta(city);
                break;
            case 'InfoContactCtrl':
                setContactPageMeta(city);
                break;
            case 'InfoPartnerCtrl':
                setPartnerPageMeta(city);
                break;
            case 'FAQCtrl':
                setFAQPageMeta(city);
                break;
            case 'TermsAndConditionsOfServiceCtrl':
                setTermAndServicePageMeta(city);
                break;
            case 'PrivacyPolicyCtrl':
                setPrivacyPolicyPageMeta(city);
                break;
            case 'HowItWorkCtrl':
                setHowItWorksPageMeta(city);
                break;
            case 'MyBookingCtrl':
                setMyBookingPageMeta(city);
                break;
            case 'MyReviewCtrl':
                setMyReviewPageMeta(city);
                break;
            case 'MyAccountCtrl':
                setMyAccountPageMeta(city);
                break;
            case 'EventCtrl':
                setEventPageMeta(city);
                break;
            case 'CnyCtrl':
                setCnyPageMeta(city);
                break;
            case 'VDayCtrl':
                setMyAccountPageMeta(city);
                break;
            default:
                setHomePageMeta(city);
                break;
        };
    };

    function setHomePageMeta(city) {
        var title = 'Book Best Restaurants in ' + city + ' and Get Rewarded with Weeloy';
        setTitle(title);

        var description = 'Book Best Restaurants in ' + city + ' with Weeloy and discover exclusive deals and promotions. Book your table and spin the wheel at your restaurant in ' + city;
        setMetaDescription(description);

        var FacebookMetaTitle = 'Book Best Restaurants in ' + city + ' and Get Rewarded with Weeloy';
        setFaceBookMetaTitle(FacebookMetaTitle);

        var FacebookMetaDescription = 'Book Best Restaurants in ' + city + ' with Weeloy and discover exclusive deals and promotions.Book your table and spin the wheel at your restaurant in ' + city;
        setFaceBookMetaDescription(FacebookMetaDescription);
    };

    function setSearchPageMeta(city) {
        var title = 'Search find and book your favorite restaurant in ' + city;
        setTitle(title);

        var description = 'Find where to eat in ' + city + ' with fun Discover and book best restaurant in Asia Choose the top restaurant, spin the wheel and get exclusive discount';
        setMetaDescription(description);

        var FacebookMetaTitle = 'Search find and book your favorite restaurant in ' + city;
        setFaceBookMetaTitle(FacebookMetaTitle);

        var FacebookMetaDescription = 'Find where to eat in ' + city + ' with fun Discover and book best restaurant in Asia Choose the top restaurant, spin the wheel and get exclusive discount';
        setFaceBookMetaDescription(FacebookMetaDescription);
    };

    function setContactPageMeta(city) {
        var title = 'Contact Weeloy - Book Best Restaurants in ' + city + ' and Get Rewarded';
        setTitle(title);

        var description = 'Weeloy is Free, Easy, Fun. Book Best Restaurants in ' + city + ' with Weeloy and discover exclusive promotions. Book your table and spin the wheel at your restaurant in ' + city
        setMetaDescription(description);

        var FacebookMetaTitle = 'Contact Weeloy - Book Best Restaurants in ' + city + ' and Get Rewarded';
        setFaceBookMetaTitle(FacebookMetaTitle);

        var FacebookMetaDescription = 'Weeloy is Free, Easy, Fun. Book Best Restaurants in ' + city + ' with Weeloy and discover exclusive promotions. Book your table and spin the wheel at your restaurant in ' + city
        setFaceBookMetaDescription(FacebookMetaDescription);
    };

    function setPartnerPageMeta(city) {
        var title = 'Weeloy Partner – Market Your Restaurant Online';
        setTitle(title);

        var description = 'Weeloy is Free, Easy, Fun. Let Weeloy be your preferred technology partner. Online restaurant booking system for customers to enjoy promotions.';
        setMetaDescription(description);

        var FacebookMetaTitle = 'Weeloy Partner – Market Your Restaurant Online';
        setFaceBookMetaTitle(FacebookMetaTitle);

        var FacebookMetaDescription = 'Weeloy is Free, Easy, Fun. Let Weeloy be your preferred technology partner. Online restaurant booking system for customers to enjoy promotions.';
        setFaceBookMetaDescription(FacebookMetaDescription);
    };

    function setFAQPageMeta(city) {
        var title = 'FAQ Weeloy - How to book and review restaurant online';
        setTitle(title);

        var description = 'Wonder where to eat? Discover new restaurants new places Get special promotions in your city Make reservation, win discount, eat best food, review your experience';
        setMetaDescription(description);

        var FacebookMetaTitle = 'FAQ Weeloy - How to book and review restaurant online';
        setFaceBookMetaTitle(FacebookMetaTitle);

        var FacebookMetaDescription = 'Wonder where to eat? Discover new restaurants new places Get special promotions in your city Make reservation, win discount, eat best food, review your experience';
        setFaceBookMetaDescription(FacebookMetaDescription);
    };

    function setTermAndServicePageMeta(city) {
        var title = 'Terms and conditions of services Weeloy.com | Dine with discount';
        setTitle(title);

        var description = 'General terms, Prohibited and acceptable uses, Service, Responsibility. Which discounts can you win with Weeloy? Try the best food in Asia with special offer';
        setMetaDescription(description);

        var FacebookMetaTitle = 'Terms and conditions of services Weeloy.com | Dine with discount';
        setFaceBookMetaTitle(FacebookMetaTitle);

        var FacebookMetaDescription = 'General terms, Prohibited and acceptable uses, Service, Responsibility. Which discounts can you win with Weeloy? Try the best food in Asia with special offer';
        setFaceBookMetaDescription(FacebookMetaDescription);
    }

    function setPrivacyPolicyPageMeta(city) {
        var title = 'Privacy Policy and terms | Singapore new place | Weeloy';
        setTitle(title);

        var description = 'Data policies, restaurant newsletter, personal account conditions Book a restaurant online and win special discounts, rewards Enjoy your diner and post best review';
        setMetaDescription(description);

        var FacebookMetaTitle = 'Privacy Policy and terms | Singapore new place | Weeloy';
        setFaceBookMetaTitle(FacebookMetaTitle);

        var FacebookMetaDescription = 'Data policies, restaurant newsletter, personal account conditions Book a restaurant online and win special discounts, rewards Enjoy your diner and post best review';
        setFaceBookMetaDescription(FacebookMetaDescription);
    }

    function setHowItWorksPageMeta(city) {
        var title = 'How it works with Weeloy';
        setTitle(title);

        var description = 'Discover the Weeloy concept. Book your restaurant and get rewarded in 3 steps';
        setMetaDescription(description);

        var FacebookMetaTitle = 'How it works with Weeloy';
        setFaceBookMetaTitle(FacebookMetaTitle);

        var FacebookMetaDescription = 'Discover the Weeloy concept. Book your restaurant and get rewarded in 3 steps';
        setFaceBookMetaDescription(FacebookMetaDescription);
    }

    function setMyBookingPageMeta(city) {
        var title = 'Bookings | book restaurant | weeloy.com';
        setTitle(title);

        var description = 'Manage your booking, Spin the wheel, book your favorite restaurant and get rewards Weeloy.com';
        setMetaDescription(description);

        var FacebookMetaTitle = 'Bookings | book restaurant | weeloy.com';
        setFaceBookMetaTitle(FacebookMetaTitle);

        var FacebookMetaDescription = 'Manage your booking, Spin the wheel, book your favorite restaurant and get rewards Weeloy.com';
        setFaceBookMetaDescription(FacebookMetaDescription);
    }

    function setMyReviewPageMeta(city) {
        var title = 'Reviews | manage reviews | weeloy.com';
        setTitle(title);

        var description = 'Manage your reviews, Spin the wheel, book your favorite restaurant and get rewards Weeloy.com';
        setMetaDescription(description);

        var FacebookMetaTitle = 'Reviews | manage reviews | weeloy.com';
        setFaceBookMetaTitle(FacebookMetaTitle);

        var FacebookMetaDescription = 'Manage your reviews, Spin the wheel, book your favorite restaurant and get rewards Weeloy.com';
        setFaceBookMetaDescription(FacebookMetaDescription);
    }

    function setMyAccountPageMeta(city) {
        var title = 'Account | find best restaurant | weeloy.com';
        setTitle(title);

        var description = 'Manage your account, Spin the wheel, book your favorite restaurant and get rewards Weeloy.com';
        setMetaDescription(description);

        var FacebookMetaTitle = 'Account | find best restaurant | weeloy.com';
        setFaceBookMetaTitle(FacebookMetaTitle);

        var FacebookMetaDescription = 'Manage your account, Spin the wheel, book your favorite restaurant and get rewards Weeloy.com';
        setFaceBookMetaDescription(FacebookMetaDescription);
    }

    function setEventPageMeta(city) {
        var title = 'All Restaurant Events - Reserve your table now';
        setTitle(title);

        var description = 'All Restaurant Events with Weeloy. Reserve your table with one of the finest restaurants we have lined up for you and hurry!';
        setMetaDescription(description);

        var FacebookMetaTitle = 'All Restaurant Events - Reserve your table now';
        setFaceBookMetaTitle(FacebookMetaTitle);

        var FacebookMetaDescription = 'All Restaurant Events with Weeloy. Reserve your table with one of the finest restaurants we have lined up for you and hurry!';
        setFaceBookMetaDescription(FacebookMetaDescription);
    }
    function setCnyPageMeta(city) {
        var title = 'Chinese New Year 2016 with Weeloy - Reserve your table now';
        setTitle(title);

        var description = 'Chinese New Year 2016 with Weeloy. Chinese new year restaurant menu. Reserve your table with one of the finest restaurants we have lined up for you and hurry!';
        setMetaDescription(description);

        var FacebookMetaTitle = 'Chinese New Year 2016 with Weeloy - Reserve your table now';
        setFaceBookMetaTitle(FacebookMetaTitle);

        var FacebookMetaDescription = 'Chinese New Year 2016 with Weeloy. Chinese new year restaurant menu. Reserve your table with one of the finest restaurants we have lined up for you and hurry!';
        setFaceBookMetaDescription(FacebookMetaDescription);
    }
    
    function setVdayPageMeta(city) {
        var title = 'Valentine\'s day 2016 with Weeloy - Reserve your table now';
        setTitle(title);

        var description = 'Valentine\'s day 2016 with Weeloy. Valentine\'s day restaurant menu. Reserve your table with one of the finest restaurants we have lined up for you and hurry!';
        setMetaDescription(description);

        var FacebookMetaTitle = 'Valentine\'s day 2016 with Weeloy - Reserve your table now';
        setFaceBookMetaTitle(FacebookMetaTitle);

        var FacebookMetaDescription = 'Valentine\'s day 2016 with Weeloy. Valentine\'s day restaurant menu. Reserve your table with one of the finest restaurants we have lined up for you and hurry!';
        setFaceBookMetaDescription(FacebookMetaDescription);
    }

}]);
