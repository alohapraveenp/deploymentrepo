<?php
  require_once("conf/conf.init.inc.php");
  require_once("lib/wpdo.inc.php");
  require_once("lib/class.event.inc.php");
?>
<!DOCTYPE html>
<html ng-app="app">
<head>
 <title>Restaurant Promotions</title>
</head>
<body>
  <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.9/angular.min.js"></script>
  <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.6.1/angular-route.min.js"></script>
  <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.6.1/angular-animate.min.js"></script>
  <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.6.1/angular-sanitize.min.js"></script>
  <script src="//angular-ui.github.io/bootstrap/ui-bootstrap-tpls-2.5.0.js"></script>
  <link href="//netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
  <script type="text/javascript" src="promotionService.js"></script>
  <script type="text/javascript" src="promotionFactory.js"></script>
  <script type="text/javascript" src="api.js"></script>
  <div ng-controller="PromotionController as $ctrl">
    <!-- <script type="text/ng-template" id="/promotions.html"> -->
      Search: <input type="text" ng-model="search.name">
      <ul>
        <li ng-repeat="r in promotions|filter:search"> 
        <a href="#/{{$index}}" ng-click="$ctrl.open($index)">{{r.name}}</a>
        </li>
      </ul>
    <!-- </script> -->
    <script type="text/ng-template" id="/promotiondetails.html">
        <div class="modal-header">
              <h3 class="modal-title" id="modal-title">{{$ctrl.promotion.restaurant_name}}</h3>
        </div>
        <!-- ID,eventID, name, restaurant, maxpax, title, morder, city, country, start, end, description, picture, is_homepage, pdf_link, display, price, tnc, type, menu -->
        <div class="modal-body" id="modal-body">
          <h2>{{$ctrl.promotion.name}}</h2>
          <img ng-src="{{$ctrl.promotion.picture}}" class="img-responsive"/>
          <br/>
          <p>Contact Us:<br/>
          <input ng-model="$ctrl.promotion.address" ng-readonly="true"/><br/>
          <input ng-model="$ctrl.promotion.tel" ng-readonly="true"/><br/>
          <input ng-model="$ctrl.promotion.email" ng-readonly="true"/><br/>
          <input ng-model="$ctrl.promotion.url" ng-readonly="true"/><br/>
          </p>
        </div>
        <div class="modal-footer">
          <button class="btn btn-primary" type="button" ng-click="$ctrl.ok()">OK</button>
          <button class="btn btn-warning" type="button" ng-click="$ctrl.cancel()">Cancel</button>
        </div>
    </script>
  </div>
  </div>
</body>
</html>