angular.module('app', ['ngRoute', 'ngSanitize', 'ui.bootstrap', 'ressources.md_marketing.api'])
.factory('PromotionFactory', ['API', '$q', function(API, $q) {
	var restaurants;
	return {
		promotions: function() {
			var deferred = $q.defer();
			if (!restaurants) {
				console.log("PromotionFactory.getPromotions()");
				API.promotions.GetPromotions().then(function (response) {
		    		if (response.status == 200) {
		    			console.log("PromotionFactory.getPromotions(): Successfully retrieved "+response.data.data.length+" promotions");
		        		restaurants = response.data.data;
		        		deferred.resolve(restaurants);
		    		}
					else
		        		console.log("PromotionFactory.getPromotions() failed: " + JSON.stringify(response));
				});
			} else
				deferred.resolve(restaurants);
			return deferred.promise;
    	},
    	promotion: function(id) {
    		return angular.isUndefined(restaurants) || restaurants === null? [] :restaurants[id];
    	}
	};
}])
.controller('PromotionController', ['$scope', 'PromotionFactory', '$uibModal', function($scope, PromotionFactory, $uibModal) {
	//console.log("PromotionController");
	var $ctrl = this;
	if (angular.isUndefined($scope.promotions) || $scope.promotions === null)
		PromotionFactory.promotions().then(function(result) { $scope.promotions = result;});
	$ctrl.open = function (index, parentSelector) {
	    $ctrl.promotion = PromotionFactory.promotion(index);
	    //console.log("ModalDemoCtrl.open(): index: "+index+" parentSelector: "+JSON.stringify(parentSelector)+" promotion: "+JSON.stringify($ctrl.promotion));
	    $ctrl.promotion.picture = "https://media.weeloy.com/upload/restaurant/"+$ctrl.promotion.restaurant+"/360/"+$ctrl.promotion.picture;
	    var parentElem = parentSelector ? 
	      angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
	    var modalInstance = $uibModal.open({
	      animation: $ctrl.animationsEnabled,
	      ariaLabelledBy: 'modal-title',
	      ariaDescribedBy: 'modal-body',
	      templateUrl: '/promotiondetails.html',
	      controller: 'ModalInstanceCtrl',
	      controllerAs: '$ctrl',
	      size: 'sm',
	      appendTo: parentElem,
	      resolve: {
		       	promotion: function () {
					return $ctrl.promotion;
	        	}
	      }
	    });

	    modalInstance.result.then(function () {
	      console.log('Modal dismissed at: ' + new Date());
	    });
  	};
}])
// Please note that $uibModalInstance represents a modal window (instance) dependency.
// It is not the same as the $uibModal service used above.
.controller('ModalInstanceCtrl', function ($uibModalInstance, promotion) {
	console.log("ModalInstanceCtrl");
	var $ctrl = this;
	$ctrl.promotion = promotion;
	$ctrl.ok = function () {
		console.log("ModalInstanceCtrl.ok()");
		$uibModalInstance.close();
	};
	$ctrl.cancel = function () {
		console.log("ModalInstanceCtrl.cancel()");
		$uibModalInstance.dismiss('cancel');
	};
})
// Please note that the close and dismiss bindings are from $uibModalInstance.
.component('modalComponent', {
  templateUrl: '/promotiondetails.html',
  bindings: {
    resolve: '<',
    close: '&',
    dismiss: '&'
  },
  controller: function () {
    var $ctrl = this;
    $ctrl.$onInit = function () {
      $ctrl.promotion = $ctrl.resolve.promotion;
    };
    $ctrl.ok = function () {
      //$ctrl.close({$value: $ctrl.selected.item});
      $ctrl.close();
    };
    $ctrl.cancel = function () {
      //$ctrl.dismiss({$value: 'cancel'});
      $ctrl.dismiss();
    };
  }
});