<?php
    require_once 'conf/conf.init.inc.php';
    require_once 'lib/class.media.inc.php';

    $type = filter_input (INPUT_GET, 'type', FILTER_SANITIZE_STRING);
    $restaurant = filter_input (INPUT_GET, 'restaurant_id', FILTER_SANITIZE_STRING);
    $size = filter_input (INPUT_GET, 'size', FILTER_SANITIZE_STRING);
    $img_name = filter_input (INPUT_GET, 'img', FILTER_SANITIZE_STRING);
    $media_redirect = 'https://media.weeloy.com/upload/restaurant/';
    
    $r = filter_input (INPUT_GET, 'r', FILTER_SANITIZE_STRING);
    
    //$imgpath = filter_input (INPUT_GET, 'img-path', FILTER_SANITIZE_STRING);   
    if (preg_match("/.pdf/i", $img_name)){
        $URL = $media_redirect . $restaurant . '/'. $img_name . '?ok' ;
        header('Location: ' . $URL);
        exit;
    	}

    if($r == 't'){
        $media_redirect = 'https://media.weeloy.com.s3.amazonaws.com/upload/restaurant/';
        $FileName = $media_redirect . $restaurant . '/' . $size . '/' . $img_name ;
        header('Location: ' . $URL);
        exit;
    }

    if($type == 'restaurant'){
        if(is_size_allowed($size) ){
            $media = new WY_Media($restaurant);
            if($media->l_file_exists($size.'/'.$img_name)){
                error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": size: ".$size);
                $URL = $media_redirect.$restaurant.'/'.$size.'/'.$img_name ;
            }else{
                if ($media->l_resize($img_name, $img_name, $size)) {
                    error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": size: ".$size);
                    $URL = $media_redirect.$restaurant.'/'.$size.'/'.$img_name ;
                    }else{
                        return error_404();
                    }
            }
        }
    }

//    header('Content-Type: '.mime_content_type($FileName));
//    header("Cache-Control: must-revalidate");
//    $offset = 60 * 0;
//    $ExpStr = "Expires: " . gmdate("D, d M Y H:i:s", time() + 1) . " GMT";
//    header($ExpStr);
//    ob_clean();
//    flush();
//    readfile($FileName);
    
    $URL .= '?r=t';
    
    header('Location: ' . $URL);

	
    function error_404(){
        header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header("HTTP/1.0 404 Not Found");
        echo "file name not found";   
    }
    
    function is_size_allowed($size){
        switch ($size){
            case '100':
            case '140':
            case '180':
            case '270':
            case '300':
            case '325':
            case '450':
            case '360':
            case '500':
            case '600':
            case '700':
            case '1024':
            case '1440':
                return true;
            default :
                return false;
        }
    }
    
    function get_size_model($size){
        switch ($size){
            case '100':
            case '140':
            case '180':
            case '270':
            case '300':
            case '325':
            case '450':
            case '360':
                return 500;
            case '600':
            case '700':
                return 1024;
            default :
                return 1440;
        }
    }
?>