<?php 


define('ENCRYPTION_KEY', 'd0a7e7997baaafcd55f4b5c32fffb87c');

function simple_encrypt($text) {
	return strtr(trim(base64_encode(mcrypt_encrypt(MCRYPT_BLOWFISH, ENCRYPTION_KEY, $text, MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_BLOWFISH, MCRYPT_MODE_ECB), MCRYPT_RAND))), " ="), "+/", "._");
}

function simple_decrypt($text) {
	return trim(mcrypt_decrypt(MCRYPT_BLOWFISH, ENCRYPTION_KEY, base64_decode(strtr($text, "._", "+/") . "="), MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_BLOWFISH, MCRYPT_MODE_ECB), MCRYPT_RAND)));
}

function tokenize($text) { 
 	global $tokenKey;

 	$index = rand(0, 25);
 	$text = $text . $tokenKey[$index]; 	
 	$text = strrev($text);
 	return simple_encrypt($text) . chr(65 + $index); 
 }
 	
function detokenize($text) { 
 	global $tokenKey;
 	
 	$ch = substr($text, strlen($text) - 1, 1);
  	$index = ord($ch) - 65;
	$text = substr($text, 0, strlen($text) - 1);
 	$text = simple_decrypt($text);
 	$text = strrev($text);
	
 	$pat = $tokenKey[$index];
 	if(substr($text, strlen($text) - strlen($pat)) != $pat)
 		return "BAD " . substr($text, 0, strlen($text) - strlen($pat)) . "-" . $pat . "-";
 	return substr($text, 0, strlen($text) - strlen($pat));
 }
 	


	$data = "ImYjvwUOLIYOYO2g_RZcZZp3INiXZSUybURFUpbkdzsU"; //michael.dipalma@pscafe.com
	$data = "XMl9Kxxlri81wLy6oKe.FxWFbJujj19jF95aaJKoweviyeRn8_y5XwD"; //thanyingsingapore@gmail.com
	$data = "V.FC6RbNs48jwgL214xo0zL_t4FObv39x8wHhmKkU8gRcmfmfSelkwB"; //okurabangkok.weeloy@gmail.com
	$data = "DbUI9rBLRRmnMMg9f4bDvDL_t4FObv39p1jOTue9K.0t5mjhaflEmQE";//pings.weeloy@gmail.com
	$data = "gJHt4ePj4JghTTMk5DYmTCLzTux_oJlWqmaG0lI3uE8M"; //tembusu.weeloy@gmail.com
	$data = "oUri6ljo7XQhTTMk5DYmTCLzTux_oJlW521758uVx58L"; //pings facebook
    $emailcluster = detokenize($data);
    echo $emailcluster;

?>