<?php
    require_once("conf/conf.init.inc.php");
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html;" />
        <meta name="author" content="weeloy. https://www.weeloy.com"/>
        <meta name="copyright" content="weeloy. https://www.weeloy.com"/>  
        <meta http-equiv='expires' content='mon, 1 jan 1998 05:00:00 gmt'/>
        <meta http-equiv='pragma' content='no-cache'/>
        <meta http-equiv='pragma' content='cache-control: max-age=0'/>
        <meta http-equiv='last-modified' content='mon, 1 jan 1998 05:00:00 gmt'/>
        <meta http-equiv='cache-control' content='no-cache, must-revalidate'/>
        <meta name='robots' content='noindex, nofollow'/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <meta name="HandheldFriendly" content="true" />
        <title>Profile</title>
        <base href="<?php echo __ROOTDIR__; ?>/" />
        
        <link rel="icon" href="../favicon.ico" type="image/gif" sizes="16x16">
        <link href="client/bower_components/nvd3/nv.d3.min.css" rel="stylesheet" type="text/css" />    
        <link href="client/bower_components/material/angular-material.min.css" rel="stylesheet" type="text/css" />    
        <link href="client/bower_components/material-design-icons/iconfont/material-icons.css" rel="stylesheet" type="text/css" />    
        <link href="client/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="client/bower_components/bootstrap/dist/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
        <link href="client/bower_components/jquery-ui/themes/base/jquery-ui.css" rel="stylesheet" type="text/css">
        <link href="client/bower_components/fontawesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="client/bower_components/material/css/bootstrap-material-datetimepicker.css" rel="stylesheet"  type="text/css" />
        <link href="css/admin-style.css" rel="stylesheet" type="text/css"/>
        <link href="css/carousel.css" rel="stylesheet" type="text/css">
        <link href="css/bullet.css" rel="stylesheet" type="text/css">
        <link href="css/dropdown.css" rel="stylesheet" type="text/css">
        <link href="css/bootstrap-social.css" rel="stylesheet" type="text/css"/>
        <link href="css/login.css" rel="stylesheet" type="text/css">
        <link href="css/modal.css" rel="stylesheet" type="text/css">
        <link href="css/multirange.css" rel="stylesheet" type="text/css">
        <link href='https://fonts.googleapis.com/css?family=Dosis:400,600,700bold|Roboto:400,900,700italic,700,500italic,500,400italic' rel='stylesheet' type='text/css'/>

        <script type='text/javascript' src="client/bower_components/jquery/dist/jquery.min.js"></script>
        <script type='text/javascript' src="client/bower_components/jquery-ui/jquery-ui.js"></script>
        <script type='text/javascript' src="client/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script type='text/javascript' src="client/bower_components/angular/angular.min.js"></script>
        <script type='text/javascript' src="client/bower_components/angular/angular-animate.min.js"></script>
        <script type='text/javascript' src="client/bower_components/material/angular-material.min.js"></script>
        <script type='text/javascript' src="client/bower_components/angular/angular-aria.min.js"></script>
        <script type='text/javascript' src="client/bower_components/angular-sanitize/angular-sanitize.min.js"></script>
        <script type='text/javascript' src="client/bower_components/angular/angular-messages.min.js"></script>
        <script type="text/javascript" src="client/bower_components/moment/min/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="client/bower_components/material/bootstrap-material-datetimepicker.js"></script>
        <script type='text/javascript' src="client/bower_components/ui-bootstrap-tpls-0.14.2.min.js"></script>
        <script type='text/javascript' src="client/bower_components/nvd3/angular-nvd3/angular-nvd3.min.js"></script>
        <script type='text/javascript' src="client/bower_components/angular-material-icons/angular-material-icons.js"></script>
        <script type='text/javascript' src="client/bower_components/material/angular-material-calendar.js"></script>
        <script type="text/javascript" src="js/angular-file-upload.js"></script>
        <script type='text/javascript' src="js/alog.js?7"></script>
        <script type='text/javascript' src="js/date.format.js"></script>

        <script>
        var app = angular.module('ProfileControllerNew', ['ui.bootstrap', 'ngMaterial', 'ngMessages', 'ngMdIcons', 'materialCalendar']);
        </script>
		<script type="text/javascript" src="ressources/test/profile/profile.js"></script> 
    </head>
<body ng-app="ProfileControllerNew" style="background:transparent;">
<?php include_once("profile.php") ?>
</body>
</html>