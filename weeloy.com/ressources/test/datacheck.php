<?

	require_once("conf/conf.init.inc.php");
	require_once("lib/wpdo.inc.php");
	require_once("lib/class.datacheck.inc.php");
	require_once("lib/Browser.inc.php");
	require_once("lib/class.mail.inc.php");
	require_once("lib/class.async.inc.php");
	require_once("lib/class.spool.inc.php");
	
?>

<!-- Angualr Code -->

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="author" content="weeloy. https://www.weeloy.com"/>
        <meta name="copyright" content="weeloy. https://www.weeloy.com"/>  
        <meta http-equiv='expires' content='mon, 1 jan 1998 05:00:00 gmt'/>
        <meta http-equiv='pragma' content='no-cache'/>
        <meta http-equiv='pragma' content='cache-control: max-age=0'/>
        <meta http-equiv='last-modified' content='mon, 1 jan 1998 05:00:00 gmt'/>
        <meta http-equiv='cache-control' content='no-cache, must-revalidate'/>
        <meta name='robots' content='noindex, nofollow'/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <meta name="HandheldFriendly" content="true" />
        <title> Data Check</title>



        <link href="../../client/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../../client/bower_components/bootstrap/dist/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
        <link href="../../client/bower_components/fontawesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

        <link href="../../modules/booking/assets/css/css/bootstrap-select.css" rel="stylesheet" />
        <link href="../../modules/booking/assets/css/css/bootstrap-social.css" rel="stylesheet" />

        <script type='text/javascript' src="../../client/bower_components/angular/angular.min.js"></script>
        <script type="text/javascript" src="../../client/bower_components/jquery/dist/jquery.min.js"></script>

        <script type="text/javascript" src="../../js/jquery-ui.js"></script>
        <script type="text/javascript" src="../../client/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="../../js/bootstrap-select.js"></script>
        <script type='text/javascript' src="../../js/ui-bootstrap-tpls-0.12.1.min.js"></script>
        <script type="text/javascript" src="../../js/ngStorage.min.js"></script>
        <script type="text/javascript" src="../../js/mylocal.js"></script>

        <script> var app = angular.module("myApp", ['ui.bootstrap', 'ngLocale', 'ngStorage']);</script> 
        
        <style>
            /*CSS Styling*/
        </style>

        <body ng-app="myApp">
            <div class="wrapper" ng-controller='DatacheckController' ng-init="moduleName = 'datacheck';" style='margin-top:20px;'>
                <div class="container container-table"  >
                    <div class="row ">
                        <div class="container">
                            <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-2 col-lg-8 col-md-8 col-sm-8 col-xs-12"> 
								
								<label>DB Name*</label>
								<input type="text" class="form-control" id="schemaname" name="schemaname" ng-model="schemaname" required>  

								<label>Table Name</label> 
								<input type="text" class="form-control" id="tablename" name="tablename" ng-model="tablename">

								<label>Column Name</label> 
								<input type="text" class="form-control" id="columnname" name="columnname" ng-model="columnname">      

								<input type="button" ng-click="checkNow()" class="btn btn_default" value="CheckNow" style="border-radius:0px;" >

                                <p> </br>
                                </p>

                                <table border="1px solid black">
                                    <tr>
                                        <th>DB Name 
                                        </th>
                                        <th>Table Name 
                                        </th> 
                                        <th>Primary Key 
                                        </th> 
                                        <th>Key Value
                                        </th> 
                                        <th>JASON Data Column 
                                        </th> 
                                        <th>JASON Value 
                                        </th> 
                                    </tr>
                                    <tr ng-repeat="data in myDataRow" ng-if="isJson(data.JASON_MSG) == false">
                                        <td>{{ data.DB_Name }}</td>
                                        <td>{{ data.Table_Name }}</td>
                                        <td>{{ data.Key_Column }}</td>
                                        <td>{{ data.PK }}</td>
                                        <td>{{ data.Column_Name }}</td>
                                        <td>{{ data.JASON_MSG }}</td>
                                    </tr>
                                </table>

                            </div>

                        </div>

                    </div>
                       
                </div>
            </div>
           
            <script>
                        app.controller('DatacheckController', function ($scope, $http) {
                            
                            $scope.isJson = function (str) {
                                    try {
                                        JSON.parse(str);
                                    } catch (e) {
                                        return false;
                                    }
                                    return true;
                            }

                            $scope.checkNow = function () {

                                $scope.depth = 0; // Single column

                            	if (typeof $scope.columnname == 'undefined') {
        							$scope.columnname = '';
                                    $scope.depth = 2; // Single Table
                                }

                                if (typeof $scope.tablename == 'undefined') {
                                    $scope.tablename = '';
                                    $scope.depth = 1; // Fulll Schema
                                }

                                if (typeof $scope.schemaname == 'undefined' || $scope.schemaname == '') {
                                    alert ("Please enter DB Name");
                                }

                                $http.post("../../api/datacheck",
                                        {
                                            'schemaname':$scope.schemaname,
                                            'tablename':$scope.tablename,
                                            'columnname':$scope.columnname 
                                        }).then(function (response) {
                                        	// console.log("RESPONSE",JSON.stringify(response.data.data));
                                            console.log("Amin");
                                        	console.log("RESPONSE",response);
                                            return false;
                                        	$scope.myData = response.data.data;
                                            if ($scope.depth == 0) {
                                                console.log($scope.myData);
                                                $scope.myDataRow = $scope.myData;
                                            } 

                                            if ($scope.depth == 2) {
                                                console.log($scope.depth);
                                                angular.forEach($scope.myData, function(value, key){
                                                    $scope.myDataRow = value;

                                                }); 
                                            }    

                                            if ($scope.depth == 1) {
                                                // console.log($scope.myData);
                                                console.log($scope.depth);
                                                angular.forEach($scope.myData, function(value, key){
                                                    $scope.myDataRowValue = value;

                                                    angular.forEach($scope.myDataRowValue, function(value, key){
                                                        $scope.myDataRow = value;

                                                    }); 

                                                }); 
                                            } 

                                });


                            };


                        });
                       
            </script>


        </body>
</html>


