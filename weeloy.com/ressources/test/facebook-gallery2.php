<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<html>
    <head>
        <style>
            .facebookImgThumb i {
                border: 2px solid #b0b0b0;
                padding: 0;
                margin: 0;
                vertical-align: middle;
                width: 149px;
                height: 116px;
                display: inline-block;
                background-position: center 25%;
                background-repeat: no-repeat;
            }
            .facebookImgThumb p {
                vertical-align: bottom;
                font-size: 0.85em;
            }
            .facebookImgThumb {
                margin: 5px;
                display: inline-block;
                width: 151px;
            }
            .facebookImgThumb:hover {
                cursor: pointer;
            }
            .loader{
                display:none;
                /*set the div in the bottom right corner*/
                position:fixed;
                bottom:8px;
                right:8px;
                width:31px;
                height:31px;
                /*give it some background and border*/
                background:url(https://dl.dropbox.com/u/1017142/ajax-loader.gif) no-repeat;
            }
        </style>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <!-- Add mousewheel plugin (this is optional) -->
<!--        <script type="text/javascript" src="fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>-->

        <!-- Add fancyBox -->
        <link rel="stylesheet" href="fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
        <script type="text/javascript" src="fancybox/source/jquery.fancybox.pack.js?v=2.1.5"></script>
<!--
         Optionally add helpers - button, thumbnail and/or media 
        <link rel="stylesheet" href="fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
        <script type="text/javascript" src="fancybox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
        <script type="text/javascript" src="fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>

        <link rel="stylesheet" href="fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />
        <script type="text/javascript" src="fancybox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>-->

        
        <script src="historyjs/scripts/bundled/html4+html5/jquery.history.js"></script>
        
        
    </head>
    <body>
        <div id="facebookPhotoGallery"></div>
    </body>
    <script>

        (function ($) {
            var History = window.History;
            jQuery.support.cors = true;
            jQuery.ajaxSetup({
                beforeSend: function () {
                    $('.loader').show();
                },
                complete: function () {
                    $('.loader').hide();
                }
            });
            function processState(state) {
                if ('album' in state.data) {
                    $.fancybox.close();
                    facebookAlbumImages(state.data.album);
                } else if ('image' in state.data) {
                    // get the album id
                    $.getJSON('https://graph.facebook.com/' + state.data.image, function (response) {
                        var re = /a\.([0-9]+)\./g;
                        facebookAlbumImages(re.exec(response.link)[1]);
                    });
                } else {
                    facebookAlbums(1375199076138604);
                }
            }
//            // Bind to StateChange Event
            History.Adapter.bind(window, 'statechange', function () { // Note: We are using statechange instead of popstate
                var state = History.getState(); // Note: We are using History.getState() instead of event.state
                processState(state);
            });
            function facebookAlbumImages(albumID) {
                $.getJSON('https://graph.facebook.com/' + albumID + '/photos?callback=?', function (response) {
                    var data = response.data;
                    if (!('image' in History.getState().data)) {
                        History.pushState({album: albumID}, null, "?album=" + albumID);
                    }
                    $("#facebookPhotoGallery").html("<div id='facebookImages'></div>");
                    $.each(data, function (index, imageData) {
                        var thumb = imageData.images[5];
                        $("#facebookImages").append("<div class='facebookImgThumb " + imageData.id + "'><a href='" + imageData.source + "' title='' class='fancybox' rel='facebookGalleryImages'><i style='background-image: url(" + thumb.source + ")'></i></a></div>");
                    });
                });
            }
            $(document).on("click", ".facebookImgThumb > a.fancybox", function () {
                var imageID = $(this).parent().attr("class").split(" ")[1];
                History.pushState({image: imageID}, null, "?image=" + imageID);
            });
            function facebookAlbums(oID) {
                $.getJSON('https://graph.facebook.com/' + oID + '/albums?callback=?', function (response) {
                    console.log("working?");
                    var data = response.data;
                    $("#facebookPhotoGallery").html("<div id='facebookAlbums'></div>");
                    $.each(data, function (index, val) {
                        if ('cover_photo' in val) {
                            $.getJSON('https://graph.facebook.com/' + val.cover_photo + '?callback=?', function (responsePicture) {
                                // calculate size
                                var image = responsePicture.images[5];
                                $("#facebookAlbums").append("<div class='facebookImgThumb " + val.id + "'><i style='background-image: url(" + image.source + ")'></i><p>" + val.name + "</p></div>");
                            });
                        }
                    });
                }).error(function (jqXHR, textStatus, errorThrown) {
                    alert(errorThrown);
                });
                $(document).on("click", ".facebookImgThumb", function () {
                    $(document).off("click", ".facebookImgThumb");
                    var albumID = $(this).attr("class").split(" ")[1];
                    History.pushState({album: albumID}, null, "?album=" + albumID);
                });
            }
            $(".fancybox").fancybox({
                openEffect: 'none',
                closeEffect: 'none',
                afterClose: function () {
                    console.log("Going back!");
                    History.back();
                }
            });
            processState(History.getState());
        })(jQuery);
    </script>
</html>