ALTER TABLE `event` ADD `tnc` TEXT NOT NULL AFTER `price`, ADD `type` ENUM('public','private') NOT NULL AFTER `tnc`, ADD `menu` VARCHAR(24) NOT NULL AFTER `type`;
