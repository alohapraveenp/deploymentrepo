ALTER TABLE `restaurant`  ADD `laptime` INT NOT NULL  AFTER `facebook_pid`,  ADD `lastorderlunch` VARCHAR(8) NOT NULL  AFTER `laptime`,  ADD `lastorderdiner` VARCHAR(8) NOT NULL  AFTER `lastorderlunch`,  ADD `tnc` MEDIUMTEXT NOT NULL  AFTER `lastorderdiner`,  ADD `bookinfo` VARCHAR(512) NOT NULL  AFTER `tnc`;
ALTER TABLE `restaurant` ADD `dfmaxpers` INT NOT NULL AFTER `dfminpers`;
ALTER TABLE `restaurant` CHANGE `dfminpers` `dfminpers` INT(11) NOT NULL DEFAULT '1';
ALTER TABLE `restaurant` CHANGE `dfmaxpers` `dfmaxpers` INT(11) NOT NULL DEFAULT '10';
ALTER TABLE `restaurant` CHANGE `laptime` `laptime` INT(11) NOT NULL DEFAULT '0';
ALTER TABLE `restaurant` ADD `bookhours` TINYTEXT NOT NULL AFTER `openhours`;