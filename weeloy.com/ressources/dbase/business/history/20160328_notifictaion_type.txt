CREATE TABLE `notification_type` (
  `id` int(20) NOT NULL,
  `type` varchar(20) NOT NULL,
  `action` varchar(50) NOT NULL,
  `label` varchar(256) NOT NULL,
  `status` varchar(25) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

ALTER TABLE `notification_type`
  ADD PRIMARY KEY (`id`);