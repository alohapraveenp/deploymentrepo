ALTER TABLE  `log_users` ADD  `user_token` VARCHAR( 36 ) NULL AFTER  `source`;
ALTER TABLE  `log_users` CHANGE  `object`  `object` VARCHAR( 64 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;
ALTER TABLE  `log_users` CHANGE  `source`  `source` VARCHAR( 128 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;