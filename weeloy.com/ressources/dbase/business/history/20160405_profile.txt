DROP TABLE IF EXISTS `profile`;
DROP TABLE IF EXISTS `profile_sub`;

CREATE TABLE IF NOT EXISTS `profile` (
`ID` int(11) NOT NULL,
  `systemid` varchar(24) NOT NULL,
  `salutation` varchar(16) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(64) NOT NULL,
  `mobile` varchar(32) NOT NULL,
  `extraemail` varchar(64) NOT NULL,
  `extramobile` varchar(32) NOT NULL,
  `company` varchar(32) NOT NULL,
  `birth` date NOT NULL,
  `lastupdate` datetime NOT NULL,
  `createdate` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `profile_sub` (
`ID` int(11) NOT NULL,
  `systemid` varchar(24) NOT NULL,
  `restaurant` varchar(64) NOT NULL,
  `content` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

ALTER TABLE `profile` ADD PRIMARY KEY (`ID`), ADD UNIQUE KEY `systemid` (`systemid`), ADD KEY `mobile` (`mobile`), ADD KEY `email` (`email`);
ALTER TABLE `profile` MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `profile_sub` ADD PRIMARY KEY (`ID`), ADD UNIQUE KEY `profrest` (`systemid`,`restaurant`), ADD KEY `restaurant` (`restaurant`);
ALTER TABLE `profile_sub` MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;