ALTER TABLE `menu` ADD `menuID` VARCHAR(16) NOT NULL AFTER `restaurant`;
ALTER TABLE `menu` ADD `itemID` VARCHAR(16) NOT NULL AFTER `restaurant`;
ALTER TABLE `menu` ADD INDEX(`menuID`);
ALTER TABLE `menu_categorie` ADD `menuID` VARCHAR(16) NOT NULL AFTER `restaurant`;
ALTER TABLE `menu_categorie` ADD `itemindex` INT NOT NULL AFTER `menuID`;
ALTER TABLE `menu_categorie` ADD INDEX(`menuID`);
