CREATE TABLE `restaurant_payment_service` (
  `id` int(11) NOT NULL,
  `restaurant` varchar(64) NOT NULL,
  `payment_type` varchar(32) NOT NULL,
  `payment_mode` varchar(256) NOT NULL,
  `product` varchar(64) NOT NULL,
  `status` varchar(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `restaurant_payment_service`
--



--
-- Indexes for dumped tables
--

--
-- Indexes for table `restaurant_payment_service`
--
ALTER TABLE `restaurant_payment_service`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `restaurant_payment_service`
--
ALTER TABLE `restaurant_payment_service`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;