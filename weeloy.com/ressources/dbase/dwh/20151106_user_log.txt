ALTER TABLE `log_users` CHANGE `other` `other` VARCHAR(25) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;


INSERT INTO `log_users_actions` (`ID`, `id_action`, `name_action`) VALUES
(32, 101, 'Booking_cancel'),
(34, 121, 'Facebook_share'),
(35, 103, 'Booking-modify'),
(37, 104, 'Account-infomodify'),
(38, 105, 'review_post'),
(39, 122, 'Twitter-share'),
(40, 200, 'admin-RestCreation'),
(41, 201, 'admin-RestUpdate'),
(42, 202, 'admin-RestRename'),
(43, 203, 'admin-RestDublicate'),
(44, 204, 'admin-RestReactivate'),
(45, 205, 'admin-RestDeactivate'),
(46, 206, 'admin-RestDelete'),
(47, 210, 'admin-MemberCreation'),
(48, 211, 'admin-MemberUpdate'),
(49, 801, 'Booking-form'),
(50, 802, 'Booking-prebook'),
(51, 820, 'Booking-confirmation'),
(52, 900, 'Rest-tracking'),
(53, 901, 'Tracking'),
(54, 220, 'Admin_Area'),
(55, 221, 'Admin-cuisine'),
(56, 222, 'Admin-genTracking');



