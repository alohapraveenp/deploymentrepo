
--
-- Database: `weeloy88_dwh_dev`
--

-- --------------------------------------------------------

--
-- Table structure for table `sms_status_history`
--

CREATE TABLE IF NOT EXISTS `sms_status_history` (
`ID` int(11) NOT NULL,
  `sent` varchar(256) NOT NULL,
  `reference` varchar(256) NOT NULL,
  `received` varchar(256) NOT NULL,
  `recipient` varchar(256) NOT NULL,
  `statuscode` varchar(256) NOT NULL,
  `errorcode` varchar(256) NOT NULL,
  `errordescription` varchar(256) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `sms_status_history`
--
ALTER TABLE `sms_status_history`
 ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `sms_status_history`
--
ALTER TABLE `sms_status_history`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
