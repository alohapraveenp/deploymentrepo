CREATE TABLE `log_stripe_api` (
  `ID` int(11) NOT NULL,
  `booking_id` varchar(32) NOT NULL,
  `restaurant` varchar(64) NOT NULL,
  `headers` varchar(512) NOT NULL,
  `parameters` varchar(1024) NOT NULL,
  `response` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
ALTER TABLE `log_stripe_api`
  ADD PRIMARY KEY (`ID`);
ALTER TABLE `log_stripe_api`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;


CREATE TABLE `log_stripe_api_error` (
  `ID` int(11) NOT NULL,
  `booking_id` varchar(32) NOT NULL,
  `restaurant` varchar(64) NOT NULL,
  `headers` varchar(512) NOT NULL,
  `parameters` varchar(1024) NOT NULL,
  `response` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
ALTER TABLE `log_stripe_api_error`
  ADD PRIMARY KEY (`ID`);
ALTER TABLE `log_stripe_api_error`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;