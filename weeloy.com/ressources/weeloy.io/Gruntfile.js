module.exports = function(grunt){
	// grunt.registerTask("default", "", function(){
	// grunt.log.write("Hello World");
	// });

	//Configure min project settings
	grunt.initConfig({

		//Basic Settings about our plugins (cssmin and uglify)

		pkg: grunt.file.readJSON('package.json'),

		//Name of plugin (plugin name without the "grunt-contrib-")
		//cssmin
		cssmin:{
			combine: {
				files: {
					'css/main.css' : ['css/bootstrap.min.css','css/glyphicons.css','css/font-awesome.min.css','css/mdb.css','css/style.css']
				}
			}
		},

		// concat: {
		// 	js: {
		// 		src: ['js/lib/angular.min.js','js/lib/angular-animate.min.js',
		// 			  'js/lib/angular-material.min.js','js/lib/angular-aria.min.js'
		// 			  ,'js/lib/angular-sanitize.min.js',2'js/lib/ui-bootstrap-tpls-0.14.2.min.js','js/lib/angular-route.min.js'],
		// 		dest: 'partials/vendor.js'
		// 	}

		// }

		//concat
		concat: {
			js: {
				src: ['js/jquery-3.1.1.min.js','js/tether.min.js'
				,'js/bootstrap.min.js','js/mdb.min.js','partials/index.js'],
				dest: 'partials/main.js'
			}

		},

		//uglify
		uglify:{
			dist: {
				files: {
					'partials/main.min.js' : ['partials/main.js']
				}
			}
		},

		//htmlmin
		htmlmin: {                                     // Task
		    dist: {                                      // Target
		      options: {                                 // Target options
		        removeComments: true,
		        collapseWhitespace: true
		      },
		      files: {                                   // Dictionary of files
		        'partialsmin/home.htm': 'partials/home.htm',     // 'destination': 'source'
		        'partialsmin/footer.htm': 'partials/footer.htm',
		        'partialsmin/navbar.htm': 'partials/navbar.htm',
		        'partialsmin/modals.htm': 'partials/modals.htm',
		        'partialsmin/aboutus.htm': 'partials/aboutus.htm',
		        'partialsmin/contact.htm': 'partials/contact.htm',
		        'partialsmin/newsletter.htm': 'partials/newsletter.htm',
		        'partialsmin/termsandcond.htm': 'partials/termsandcond.htm',
		        'partialsmin/policy.htm': 'partials/policy.htm',
		        'partialsmin/main.htm': 'partials/main.htm'
		      }
		    }
		  }
		

	});

	// Load the plug in
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-htmlmin');

	//Do the task
	grunt.registerTask("default", ['cssmin','concat','uglify','htmlmin']);
};