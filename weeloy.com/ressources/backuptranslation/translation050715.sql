-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: weeloy-business.cweyuhtatlxl.ap-southeast-1.rds.amazonaws.com
-- Generation Time: Jul 05, 2015 at 08:09 PM
-- Server version: 5.6.19-log
-- PHP Version: 5.5.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `weeloy88_business`
--

-- --------------------------------------------------------

--
-- Table structure for table `translation`
--

CREATE TABLE IF NOT EXISTS `translation` (
`id` int(11) NOT NULL,
  `zone` varchar(24) DEFAULT NULL,
  `elem_name` varchar(32) NOT NULL,
  `object_name` varchar(64) NOT NULL,
  `lang` varchar(5) NOT NULL,
  `content` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2582 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `translation`
--

INSERT INTO `translation` (`zone`, `elem_name`, `object_name`, `lang`, `content`) VALUES
( 'global', 'trs_book', '', 'en', 'bookings'),
( 'member_section', 'your_bookings', '', 'en', 'Last bookings'),
( 'member_section', 'your_bookings', '', 'cn', 'Last bookings'),
( 'BOOKING', 'restaurant', '', 'en', 'restaurant'),
( 'BOOKING', 'guests', '', 'en', 'guests'),
( 'BOOKING', 'title', '', 'en', 'title'),
( 'BOOKING', 'firstname', '', 'en', 'firstname'),
( 'BOOKING', 'lastname', '', 'en', 'lastname'),
( 'BOOKING', 'lunch', '', 'en', 'lunch'),
( 'BOOKING', 'dinner', '', 'en', 'dinner'),
( 'BOOKING', 'mr', '', 'en', 'mr.'),
( 'BOOKING', 'mrs', '', 'en', 'mrs.'),
( 'BOOKING', 'ms', '', 'en', 'ms.'),
( 'BOOKING', 'today', '', 'en', 'today'),
( 'BOOKING', 'clear', '', 'en', 'clear'),
( 'BOOKING', 'close', '', 'en', 'close'),
( 'BOOKING', 'name', '', 'en', 'name'),
( 'BOOKING', 'mobile', '', 'en', 'mobile'),
( 'BOOKING', 'special_request', '', 'en', 'special request'),
( 'BOOKING', 'request', '', 'en', 'request'),
( 'BOOKING', 'date', '', 'en', 'date'),
( 'BOOKING', 'time', '', 'en', 'time'),
( 'BOOKING', 'number_guest', '', 'en', 'number of guests'),
( 'BOOKING', 'please_confirm', '', 'en', 'Please confirm your information'),
( 'BOOKING', 'book_now', '', 'en', 'book now'),
( 'BOOKING', 'agree_tac', '', 'en', 'I agree to the terms and conditions'),
( 'BOOKING', 'view_tac', '', 'en', 'view terms and conditions'),
( 'BOOKING', 'approve_tac', '', 'en', 'You need to approve the terms and conditions in order to reserve a table'),
( 'BOOKING', 'book_facebook', '', 'en', 'book with facebook'),
( 'BOOKING', 'book_table', '', 'en', 'book your table'),
( 'BOOKING', 'book_slogan', '', 'en', 'You are not far from your next FUN dining out experience'),
( 'BOOKING', 'no_booking_fee', '', 'en', 'Free Booking'),
( 'BOOKING', 'instant_reservation', '', 'en', 'Instant Reservation'),
( 'BOOKING', 'spin_weel', '', 'en', 'Spin the Wheel <br />at the restaurant'),
( 'BOOKING', 'invalid_email', '', 'en', 'invalid email. Try again'),
( 'BOOKING', 'invalid_firstname', '', 'en', 'invalid firstname. Try again'),
( 'BOOKING', 'invalid_lastname', '', 'en', 'invalid lastname. Try again'),
( 'BOOKING', 'invalid_country', '', 'en', 'invalid country. Try again'),
( 'BOOKING', 'invalid_mobile', '', 'en', 'invalid mobile number. Try again'),
( 'BOOKING', 'timeout', '', 'en', 'time out'),
( 'BOOKING', 'book_summary', '', 'en', 'Booking summary'),
( 'BOOKING', 'restaurant', '', 'fr', 'restaurant'),
( 'BOOKING', 'guests', '', 'fr', 'convive'),
( 'BOOKING', 'title', '', 'fr', 'salutation'),
( 'BOOKING', 'firstname', '', 'fr', 'prénom'),
( 'BOOKING', 'lastname', '', 'fr', 'nom'),
( 'BOOKING', 'lunch', '', 'fr', 'déjeuner'),
( 'BOOKING', 'dinner', '', 'fr', 'diner'),
( 'BOOKING', 'mr', '', 'fr', 'Mr'),
( 'BOOKING', 'mrs', '', 'fr', 'Mme'),
( 'BOOKING', 'ms', '', 'fr', 'Melle'),
( 'BOOKING', 'today', '', 'fr', 'aujourd`hui'),
( 'BOOKING', 'clear', '', 'fr', 'effacer'),
( 'BOOKING', 'close', '', 'fr', 'fermer'),
( 'BOOKING', 'name', '', 'fr', 'nom'),
( 'BOOKING', 'mobile', '', 'fr', 'téléphone'),
( 'BOOKING', 'special_request', '', 'fr', 'requête spéciale'),
( 'BOOKING', 'request', '', 'fr', 'requête'),
( 'BOOKING', 'date', '', 'fr', 'date'),
( 'BOOKING', 'time', '', 'fr', 'horaire'),
( 'BOOKING', 'number_guest', '', 'fr', 'nombre de convives'),
( 'BOOKING', 'please_confirm', '', 'fr', 'Confirmez les informations'),
( 'BOOKING', 'book_now', '', 'fr', 'réserver'),
( 'BOOKING', 'agree_tac', '', 'fr', 'j`accepte les conditions de vente'),
( 'BOOKING', 'view_tac', '', 'fr', 'voir les conditions de vente'),
( 'BOOKING', 'approve_tac', '', 'fr', 'Vous devez approuver les conditions de ventes pour réserver une table'),
( 'BOOKING', 'book_facebook', '', 'fr', 'réserver avec facebook'),
( 'BOOKING', 'book_table', '', 'fr', 'réserver une table'),
( 'BOOKING', 'book_slogan', '', 'fr', 'Vous êtes tres prêt de votre prochaine expérience culinaire inoubliable'),
( 'BOOKING', 'no_booking_fee', '', 'fr', 'Réservation gratuite'),
( 'BOOKING', 'instant_reservation', '', 'fr', 'Réservation Instantanee'),
( 'BOOKING', 'spin_weel', '', 'fr', 'Tourner la roue<br/> au restaurant'),
( 'BOOKING', 'invalid_email', '', 'fr', 'email invalide. Essayez encore'),
( 'BOOKING', 'invalid_firstname', '', 'fr', 'prénom invalide. Essayez encore'),
( 'BOOKING', 'invalid_lastname', '', 'fr', 'nom invalide. Essayez encore'),
( 'BOOKING', 'invalid_country', '', 'fr', 'pays invalide. Essayez encore'),
( 'BOOKING', 'invalid_mobile', '', 'fr', 'téléphone invalide. Essayez encore'),
( 'BOOKING', 'timeout', '', 'fr', 'temps expiré'),
( 'BOOKING', 'book_summary', '', 'fr', 'Résume de réservation'),
( 'BOOKING', 'booking_details', '', 'en', 'Booking Details'),
( 'BOOKING', 'personal_details', '', 'en', 'Personal Details'),
( 'BOOKING', 'booking_details', '', 'fr', 'Informations de réservation'),
( 'BOOKING', 'personal_details', '', 'fr', 'Informations Personnelles'),
( 'BOOKING', 'win_book', '', 'en', 'Win for every booking'),
( 'BOOKING', 'win_book', '', 'fr', 'Gagner à chaque fois'),
( 'BOOKING', 'every_booking', '', 'en', 'for every booking'),
( 'BOOKING', 'every_booking', '', 'fr', 'à chaque réservation'),
( 'BOOKING', 'booking_modif', '', 'en', 'Modify booking'),
( 'BOOKING', 'booking_modif', '', 'fr', 'Modifier la réservation'),
( 'BOOKING', 'booking_confirm', '', 'en', 'Your booking is confirmed at '),
( 'BOOKING', 'booking_confirm', '', 'fr', 'Votre reservation est confirmée a'),
( 'BOOKING', 'booking_confnumb', '', 'en', 'We are happy to confirm your reservation'),
( 'BOOKING', 'booking_confnumb', '', 'fr', 'Nous vous confirmons votre réservation'),
( 'BOOKING', 'booking_send', '', 'en', 'You will receive the details of your reservation by email and SMS'),
( 'BOOKING', 'booking_send', '', 'fr', 'Vous allez recevoir les détails de votre réservation par email et sms'),
( 'BOOKING', 'booking_sign', '', 'en', 'You can sign in using your email to view all your bookings and leave your reviews'),
( 'BOOKING', 'booking_sign', '', 'fr', 'Vous pouvez vous enregister avec votre email pour consulter toutes vos reservations et avis'),
( 'BOOKING', 'booking_confirmed', '', 'en', 'Booking confirmed'),
( 'BOOKING', 'booking_confirmed', '', 'fr', 'Réservation confirmée'),
( 'BOOKING', 'restaurant', '', 'cn', '餐馆'),
( 'BOOKING', 'guests', '', 'cn', '人数'),
( 'BOOKING', 'title', '', 'cn', '称谓'),
( 'BOOKING', 'firstname', '', 'cn', '名'),
( 'BOOKING', 'lastname', '', 'cn', '姓'),
( 'BOOKING', 'lunch', '', 'cn', '午餐'),
( 'BOOKING', 'dinner', '', 'cn', '晚餐'),
( 'BOOKING', 'mr', '', 'cn', '先生'),
( 'BOOKING', 'mrs', '', 'cn', '夫人'),
( 'BOOKING', 'ms', '', 'cn', '女士'),
( 'BOOKING', 'today', '', 'cn', '今天'),
( 'BOOKING', 'clear', '', 'cn', '清除'),
( 'BOOKING', 'close', '', 'cn', '关闭'),
( 'BOOKING', 'name', '', 'cn', '姓名'),
( 'BOOKING', 'mobile', '', 'cn', '手机号码'),
( 'BOOKING', 'special_request', '', 'cn', '特殊要求'),
( 'BOOKING', 'request', '', 'cn', '要求'),
( 'BOOKING', 'date', '', 'cn', '到店日期'),
( 'BOOKING', 'time', '', 'cn', '时间'),
( 'BOOKING', 'number_guest', '', 'cn', '座位人数'),
( 'BOOKING', 'please_confirm', '', 'cn', '请确认您的信息'),
( 'BOOKING', 'book_now', '', 'cn', '立即预订'),
( 'BOOKING', 'agree_tac', '', 'cn', '我同意此条款和条件'),
( 'BOOKING', 'view_tac', '', 'cn', '查看此条款和条件'),
( 'BOOKING', 'approve_tac', '', 'cn', '您需要批准此条款和条件才能预订餐桌'),
( 'BOOKING', 'book_facebook', '', 'cn', '通过Facebook预订'),
( 'BOOKING', 'book_table', '', 'cn', '预定餐桌'),
( 'BOOKING', 'book_slogan', '', 'cn', '您已经距离您的下一个FUN乐趣外出用餐体验更近了'),
( 'BOOKING', 'no_booking_fee', '', 'cn', '无预订费用'),
( 'BOOKING', 'instant_reservation', '', 'cn', '即时预定'),
( 'BOOKING', 'spin_weel', '', 'cn', '在餐厅旋转幸运转盘'),
( 'BOOKING', 'invalid_email', '', 'cn', '邮箱地址输入错误。请再试一次'),
( 'BOOKING', 'invalid_firstname', '', 'cn', '名字输入错误。请再试一次'),
( 'BOOKING', 'invalid_lastname', '', 'cn', '姓氏输入错误。请再试一次'),
( 'BOOKING', 'invalid_country', '', 'cn', '国家输入错误。请再试一次'),
( 'BOOKING', 'invalid_mobile', '', 'cn', '手机号码输入错误。请再试一次'),
( 'BOOKING', 'timeout', '', 'cn', '连接超时'),
( 'BOOKING', 'book_summary', '', 'cn', '预订信息'),
( 'BOOKING', 'booking_details', '', 'cn', '预订详情'),
( 'BOOKING', 'personal_details', '', 'cn', '个人资料'),
( 'BOOKING', 'win_book', '', 'cn', '每一个预订都能为您带来赢奖的机会'),
( 'BOOKING', 'every_booking', '', 'cn', '每一个预订'),
( 'BOOKING', 'booking_modif', '', 'cn', '修改预订'),
( 'BOOKING', 'booking_confirm', '', 'cn', '您的预订已被确认在'),
( 'BOOKING', 'booking_confnumb', '', 'cn', '我们很高兴地确认您的预订'),
( 'BOOKING', 'booking_send', '', 'cn', '通过电子邮件和短信您将收到您的预订的细节'),
( 'BOOKING', 'booking_sign', '', 'cn', '您可以使用您的电子邮件登入查看您的所有预订和留下您的点评'),
( 'BOOKING', 'booking_confirmed', '', 'cn', '预订确认'),
( 'BOOKING', 'email', '', 'en', 'email'),
( 'BOOKING', 'email', '', 'fr', 'email'),
( 'BOOKING', 'email', '', 'cn', '电子邮件'),
( 'BOOKING', 'restaurant', '', 'th', 'ร้านอาหาร'),
( 'BOOKING', 'guests', '', 'th', 'จำนวนลูกค้า'),
( 'BOOKING', 'title', '', 'th', 'คำนำหน้า'),
( 'BOOKING', 'firstname', '', 'th', 'ชื่อจริง'),
( 'BOOKING', 'lastname', '', 'th', 'นามสกุล'),
( 'BOOKING', 'lunch', '', 'th', 'เวลาอาหารมื้อกลางวัน'),
( 'BOOKING', 'dinner', '', 'th', 'เวลาอาหารมื้อค่ำ'),
( 'BOOKING', 'mr', '', 'th', 'นาย'),
( 'BOOKING', 'mrs', '', 'th', 'นาง'),
( 'BOOKING', 'ms', '', 'th', 'นางสาว'),
( 'BOOKING', 'today', '', 'th', 'วันนี้'),
( 'BOOKING', 'clear', '', 'th', 'ชัดเจน'),
( 'BOOKING', 'close', '', 'th', 'ปิดทำการ'),
( 'BOOKING', 'name', '', 'th', 'ชื่อ'),
( 'BOOKING', 'mobile', '', 'th', 'หมายเลขโทรศัพท์มือถือ'),
( 'BOOKING', 'special_request', '', 'th', 'คำขอพิเศษ'),
( 'BOOKING', 'request', '', 'th', 'คำขอ'),
( 'BOOKING', 'date', '', 'th', 'วันที่'),
( 'BOOKING', 'time', '', 'th', 'เวลา'),
( 'BOOKING', 'number_guest', '', 'th', 'จำนวนลูกค้า'),
( 'BOOKING', 'please_confirm', '', 'th', 'โปรดยืนยันข้อมูลการจองของคุณ'),
( 'BOOKING', 'book_now', '', 'th', 'ดำเนินการจอง'),
( 'BOOKING', 'agree_tac', '', 'th', 'คุณลูกค้าเห็นด้วยกับข้อกำหนดและเงื่อนไข'),
( 'BOOKING', 'view_tac', '', 'th', 'ดูข้อกำหนดและเงื่อนไข'),
( 'BOOKING', 'approve_tac', '', 'th', 'คุณต้องทำการยื่นยันข้อตกลงและเงื่อนไขก่อนที่จะสามารถทำการจองโต๊ะอาหาร'),
( 'BOOKING', 'book_facebook', '', 'th', 'ทำการจองผ่าน Facebook'),
( 'BOOKING', 'book_table', '', 'th', 'สำรองโต๊ะของคุณ'),
( 'BOOKING', 'book_slogan', '', 'th', 'มาร่วมสนุกและพบประสบการณ์รูปแบบใหม่เมื่อคุณได้มารับประทานอาหารนอกบ้านกัน'),
( 'BOOKING', 'no_booking_fee', '', 'th', 'ฟรีบุ๊คกิ้ง'),
( 'BOOKING', 'instant_reservation', '', 'th', 'สำรองโต๊ะได้ทันที'),
( 'BOOKING', 'spin_weel', '', 'th', 'หมุนวงล้อที่ร้านอาหาร'),
( 'BOOKING', 'invalid_email', '', 'th', 'อีเมลไม่ถูกต้อง โปรดลองอีกครั้ง'),
( 'BOOKING', 'invalid_firstname', '', 'th', 'ชื่อจริงไม่ถูกต้อง โปรดลองอีกครั้ง'),
( 'BOOKING', 'invalid_lastname', '', 'th', 'นามสกุลไม่ถูกต้อง โปรดลองอีกครั้ง'),
( 'BOOKING', 'invalid_country', '', 'th', 'ประเทศที่เลือกไม่ถูกต้อง โปรดลองอีกครั้ง'),
( 'BOOKING', 'invalid_mobile', '', 'th', 'หมายเลขโทรศัพท์มือถือไม่ถูกต้อง โปรดลองอีกครั้ง'),
( 'BOOKING', 'timeout', '', 'th', 'เวลาหมด'),
( 'BOOKING', 'book_summary', '', 'th', 'รายละเอียดของบุ๊คกิ้ง'),
( 'BOOKING', 'booking_details', '', 'th', 'ข้อมูลบุ๊คกิ้ง'),
( 'BOOKING', 'personal_details', '', 'th', 'ข้อมูลส่วนตัว'),
( 'BOOKING', 'win_book', '', 'th', 'ลุ้นรับรางวัล สำหรับทุกๆ บุ๊คกิ้ง'),
( 'BOOKING', 'every_booking', '', 'th', 'สำหรับทุกๆ บุ๊คกิ้ง'),
( 'BOOKING', 'booking_modif', '', 'th', 'เปลี่ยนแปลบุ๊คกิ้งของคุณ'),
( 'BOOKING', 'booking_confirm', '', 'th', 'บุ๊คกิ้งของคุณถูกยืนยันแล้ว'),
( 'BOOKING', 'booking_confnumb', '', 'th', 'ทางร้านอาหารได้มีการสำรองโต๊ะของคุณเป็นที่เรียบร้อยแล้ว'),
( 'BOOKING', 'booking_send', '', 'th', 'คุณลูกค้าจะได้รับรายละเอียดการสำรองโต๊ะผ่านทางอีเมลและ SMS'),
( 'BOOKING', 'booking_sign', '', 'th', 'คุณสามารถเข้าสู่ระบบได้โดยใช้อีเมลของคุณ เพื่อดูรายละเอียดบุ๊คกิ้งและทำการกรอกรายละเอียดแบบสอบถาม'),
( 'BOOKING', 'booking_confirmed', '', 'th', 'ได้รับการยืนยันการจองของคุณ'),
( 'BOOKING', 'email', '', 'th', 'อีเมล'),
( 'BOOKING', 'button_confirm', '', 'en', 'Confirm'),
( 'BOOKING', 'button_confirm', '', 'fr', 'Confirmer'),
( 'BOOKING', 'button_confirm', '', 'th', 'ดำเนินการจอง'),
( 'BOOKING', 'button_confirm', '', 'cn', '立即预订'),
( 'BOOKING', 'invalid_date', '', 'en', 'invalid date. Try again'),
( 'BOOKING', 'invalid_time', '', 'en', 'invalid time. Try again'),
( 'BOOKING', 'invalid_cover', '', 'en', 'invalid number of persons. Try again'),
( 'BOOKING', 'invalid_notavailability', '', 'en', 'Restaurand is not available at that time. Try again'),
( 'BOOKING', 'invalid_restaurantclose', '', 'en', 'the restaurant is close that day. Try another time/day'),
( 'BOOKING', 'invalid_nomoreseat', '', 'en', 'there is not enough seats for this request. Modify your search'),
( 'BOOKING', 'invalid_toomanyguest', '', 'en', 'too many guests'),
( 'BOOKING', 'invalid_seatleft', '', 'en', 'seats left'),
( 'BOOKING', 'invalid_date', '', 'fr', 'invalide date. Essayer encore'),
( 'BOOKING', 'invalid_cover', '', 'fr', 'nombre de personnes invalide. Essayer encore'),
( 'BOOKING', 'invalid_time', '', 'fr', 'horaire invalide. Essayer encore.'),
( 'BOOKING', 'invalid_notavailability', '', 'fr', 'restaurant n`a pas de place pour cet horaire. Essayer encore.'),
( 'BOOKING', 'invalid_seatleft', '', 'fr', 'places restantes'),
( 'BOOKING', 'invalid_restaurantclose', '', 'fr', 'le restaurant est ferme ce jour-là. Essayer un autre jour/horaire'),
( 'BOOKING', 'invalid_nomoreseat', '', 'fr', 'il n`y a pas assez de place pour cette demande. Modifier votre recherche'),
( 'BOOKING', 'invalid_toomanyguest', '', 'fr', 'trop de convives'),
( 'BOOKING', 'booking_tmc1', '', 'en', 'By confirming, I agree to'),
( 'BOOKING', 'booking_tmc2', '', 'en', 'the terms and conditions'),
( 'BOOKING', 'booking_tmc1', '', 'fr', 'En confirmant, j`accepte'),
( 'BOOKING', 'booking_tmc2', '', 'fr', 'les conditions de ventes'),
( 'BOOKING', 'booking_tmc1', '', 'th', 'โดยการยืนยัน ผม/ดิฉัน เห็นด้วยกับ'),
( 'BOOKING', 'invalid_date', '', 'th', 'วันที่ที่เลือกไม่ถูกต้อง โปรดลองอีกครั้ง'),
( 'BOOKING', 'invalid_cover', '', 'th', 'จำนวนลูกค้าที่เลือกไม่ถูกต้อง โปรดลองอีกครั้ง'),
( 'BOOKING', 'invalid_time', '', 'th', 'เวลาที่เลือกไม่ถูกต้อง โปรดลองอีกครั้ง'),
( 'BOOKING', 'invalid_notavailability', '', 'th', 'ทางร้านอาหารไม่เปิดให้บริการในช่วงเวลาดังกล่าว โปรดเลือกเวลาอื่นอีกครั้ง'),
( 'BOOKING', 'invalid_seatleft', '', 'th', 'จำนวนที่นั่งที่เหลือ'),
( 'BOOKING', 'invalid_restaurantclose', '', 'th', 'ทางร้านอาหารได้ปิดให้บริการในวันนี้ โปรดเลือกเวลาหรือวันอื่นอีกครั้ง'),
( 'BOOKING', 'booking_tmc2', '', 'th', 'ข้อกำหนดและเงื่อนไข'),
( 'BOOKING', 'invalid_nomoreseat', '', 'th', 'ที่นั่งไ่ม่เพียงพอตามที่ได้เลือกไว้ โปรดแก้ไขการค้นหาของคุณ'),
( 'BOOKING', 'invalid_toomanyguest', '', 'th', 'จำนวนลูกค้ามากเกินไป'),
( 'BOOKING', 'book_now', '', 'es', 'prenotar ahora'),
( 'BOOKING', 'book_facebook', '', 'es', 'prenotar con facebook'),
( 'BOOKING', 'book_table', '', 'es', 'prenotar una mesa'),
( 'BOOKING', 'booking_confirmed', '', 'es', 'reserva confirmada'),
( 'BOOKING', 'booking_details', '', 'es', 'Detalles de la Reserva'),
( 'BOOKING', 'book_summary', '', 'es', 'Resumen de la Reserva'),
( 'BOOKING', 'booking_tmc1', '', 'es', 'Al confirmar, yo estoy de acuerdo con'),
( 'BOOKING', 'clear', '', 'es', 'claro'),
( 'BOOKING', 'close', '', 'es', 'cercano'),
( 'BOOKING', 'button_confirm', '', 'es', 'Confirmar'),
( 'BOOKING', 'date', '', 'es', 'apuntamiento'),
( 'BOOKING', 'dinner', '', 'es', 'cena'),
( 'BOOKING', 'email', '', 'es', 'correo electrónico'),
( 'BOOKING', 'firstname', '', 'es', 'primer nombre'),
( 'BOOKING', 'every_booking', '', 'es', 'por cada reserva'),
( 'BOOKING', 'no_booking_fee', '', 'es', 'Reserva Gratuita'),
( 'BOOKING', 'guests', '', 'es', 'huésped'),
( 'BOOKING', 'agree_tac', '', 'es', 'Estoy de acuerdo con los términos y condiciones'),
( 'BOOKING', 'instant_reservation', '', 'es', 'Reserva Instantánea'),
( 'BOOKING', 'invalid_country', '', 'es', 'país inválido. Inténtalo de nuevo'),
( 'BOOKING', 'invalid_date', '', 'es', 'apuntamiento inválido. Inténtalo de nuevo'),
( 'BOOKING', 'invalid_email', '', 'es', 'correo electrónico inválido. Inténtalo de nuevo'),
( 'BOOKING', 'invalid_firstname', '', 'es', 'primer nombre inválido. Inténtalo de nuevo'),
( 'BOOKING', 'invalid_lastname', '', 'es', 'apellido inválido. Inténtalo de nuevo'),
( 'BOOKING', 'invalid_mobile', '', 'es', 'número de móvil inválido. Inténtalo de nuevo'),
( 'BOOKING', 'invalid_cover', '', 'es', 'número de personas inválido. Inténtalo de nuevo'),
( 'BOOKING', 'invalid_time', '', 'es', 'tiempo inválido. Inténtalo de nuevo'),
( 'BOOKING', 'lastname', '', 'es', 'apellido'),
( 'BOOKING', 'lunch', '', 'es', 'comida'),
( 'BOOKING', 'mobile', '', 'es', 'móvil'),
( 'BOOKING', 'booking_modif', '', 'es', 'Modificar la reserva'),
( 'BOOKING', 'mr', '', 'es', 'Sr.'),
( 'BOOKING', 'mrs', '', 'es', 'Sra.'),
( 'BOOKING', 'ms', '', 'es', 'Srta.'),
( 'BOOKING', 'name', '', 'es', 'nombre'),
( 'BOOKING', 'number_guest', '', 'es', 'número de huéspedes'),
( 'BOOKING', 'personal_details', '', 'es', 'Detalles Personales'),
( 'BOOKING', 'please_confirm', '', 'es', 'Por favor confirme su información'),
( 'BOOKING', 'request', '', 'es', 'petición'),
( 'BOOKING', 'invalid_notavailability', '', 'es', 'El restaurante no está disponible en ese momento. Inténtalo de nuevo'),
( 'BOOKING', 'restaurant', '', 'es', 'restaurante'),
( 'BOOKING', 'invalid_seatleft', '', 'es', 'asientos dejados'),
( 'BOOKING', 'special_request', '', 'es', 'instancia especial'),
( 'BOOKING', 'spin_weel', '', 'es', 'Haga girar la rueda <br /> en el restaurante'),
( 'BOOKING', 'invalid_restaurantclose', '', 'es', 'El restaurante está cerrado ese día. Pruebe otra vez / día'),
( 'BOOKING', 'booking_tmc2', '', 'es', 'Términos y Condiciones'),
( 'BOOKING', 'invalid_nomoreseat', '', 'es', 'No hay suficientes puestos para esta petición. Modifique su búsqueda'),
( 'BOOKING', 'time', '', 'es', 'tiempo'),
( 'BOOKING', 'timeout', '', 'es', 'tiempo muerto'),
( 'BOOKING', 'title', '', 'es', 'título'),
( 'BOOKING', 'today', '', 'es', 'hoy'),
( 'BOOKING', 'invalid_toomanyguest', '', 'es', 'Demasiados huèspedes'),
( 'BOOKING', 'view_tac', '', 'es', 'Mire los tèrminos y condiciones'),
( 'BOOKING', 'booking_confnumb', '', 'es', 'Estamos encantados de confirmar su reserva'),
( 'BOOKING', 'win_book', '', 'es', 'Gana por cada reserva'),
( 'BOOKING', 'book_slogan', '', 'es', 'Usted no estás muy lejos de tu próxima experencia FUN para cenar fuera'),
( 'BOOKING', 'booking_sign', '', 'es', 'Puede registrarse utilizando su correo electrónico para ver todas sus reservas y dejar sus comentarios'),
( 'BOOKING', 'approve_tac', '', 'es', 'Usted tiene que aprobar los términos y condiciones para prenotar una mesa'),
( 'BOOKING', 'booking_send', '', 'es', 'Recibirá los detalles de sureserva por correo electrónico y SMS'),
( 'BOOKING', 'booking_confirm', '', 'es', 'Su reserva está confirmada sobre'),
( 'BOOKING', 'book_now', '', 'it', 'prenotare adesso'),
( 'BOOKING', 'book_facebook', '', 'it', 'prenotarsi con facebook'),
( 'BOOKING', 'book_table', '', 'it', 'riservare un tavolo'),
( 'BOOKING', 'booking_confirmed', '', 'it', 'Prenotazione Confermata'),
( 'BOOKING', 'booking_details', '', 'it', 'Dettagli sulla Prenotazione'),
( 'BOOKING', 'book_summary', '', 'it', 'Sommario della Prenotazione'),
( 'BOOKING', 'booking_tmc1', '', 'it', 'Confermandolo, Acconseto a'),
( 'BOOKING', 'clear', '', 'it', 'chiaro'),
( 'BOOKING', 'close', '', 'it', 'vicino'),
( 'BOOKING', 'button_confirm', '', 'it', 'Confermare'),
( 'BOOKING', 'date', '', 'it', 'Appuntamento'),
( 'BOOKING', 'dinner', '', 'it', 'cena'),
( 'BOOKING', 'email', '', 'it', 'indirizzo elettronico'),
( 'BOOKING', 'firstname', '', 'it', 'nome'),
( 'BOOKING', 'every_booking', '', 'it', 'per ogni prenotazione'),
( 'BOOKING', 'no_booking_fee', '', 'it', 'Prenotazione Gratuita'),
( 'BOOKING', 'guests', '', 'it', 'ospiti'),
( 'BOOKING', 'agree_tac', '', 'it', 'Acconsento ai termini e condizioni'),
( 'BOOKING', 'instant_reservation', '', 'it', 'Prenotazione Istantanea'),
( 'BOOKING', 'invalid_country', '', 'it', 'paese invalido. Riprova'),
( 'BOOKING', 'invalid_date', '', 'it', 'appuntamento  invalido. Riprova'),
( 'BOOKING', 'invalid_email', '', 'it', 'email  invalida. Riprova'),
( 'BOOKING', 'invalid_firstname', '', 'it', 'nome  invalido. Riprova'),
( 'BOOKING', 'invalid_lastname', '', 'it', 'cognome  invalido. Riprova'),
( 'BOOKING', 'invalid_mobile', '', 'it', 'numero di cellulare  invalido. Riprova'),
( 'BOOKING', 'invalid_cover', '', 'it', 'numero di persone  invalido. Riprova'),
( 'BOOKING', 'invalid_time', '', 'it', 'tempo  invalido. Riprova'),
( 'BOOKING', 'lastname', '', 'it', 'cognome'),
( 'BOOKING', 'lunch', '', 'it', 'pranzo'),
( 'BOOKING', 'mobile', '', 'it', 'cellulare'),
( 'BOOKING', 'booking_modif', '', 'it', 'Modifica la prenotazione'),
( 'BOOKING', 'mr', '', 'it', 'Sig.'),
( 'BOOKING', 'mrs', '', 'it', 'Sig.ra'),
( 'BOOKING', 'ms', '', 'it', 'Sig.na'),
( 'BOOKING', 'name', '', 'it', 'nome'),
( 'BOOKING', 'number_guest', '', 'it', 'numero degli invitati'),
( 'BOOKING', 'personal_details', '', 'it', 'Dettagli Personali'),
( 'BOOKING', 'please_confirm', '', 'it', 'Prego conferma la tua informazione'),
( 'BOOKING', 'request', '', 'it', 'richiesta'),
( 'BOOKING', 'invalid_notavailability', '', 'it', 'Il ristorante non è disponibile quel giorno. Riprova'),
( 'BOOKING', 'restaurant', '', 'it', 'ristorante'),
( 'BOOKING', 'invalid_seatleft', '', 'it', 'posti rimasti'),
( 'BOOKING', 'special_request', '', 'it', 'richiesta speciale'),
( 'BOOKING', 'spin_weel', '', 'it', 'Gira la ruota <br/> al ristorante'),
( 'BOOKING', 'invalid_restaurantclose', '', 'it', 'il ristorante è chiuso quel giorno. Riprova un`altra volta/giorno'),
( 'BOOKING', 'booking_tmc2', '', 'it', 'Termini e Condizioni'),
( 'BOOKING', 'invalid_nomoreseat', '', 'it', 'Non ci sono abbastanza posti per questa richiesta. Modifica la tua ricerca'),
( 'BOOKING', 'time', '', 'it', 'tempo'),
( 'BOOKING', 'timeout', '', 'it', 'tempo scaduto'),
( 'BOOKING', 'title', '', 'it', 'titolo'),
( 'BOOKING', 'today', '', 'it', 'oggi'),
( 'BOOKING', 'invalid_toomanyguest', '', 'it', 'troppi invitati'),
( 'BOOKING', 'view_tac', '', 'it', 'vedi termini e condizioni'),
( 'BOOKING', 'booking_confnumb', '', 'it', 'Siamo lieti di confermare la vostra prenotazione'),
( 'BOOKING', 'win_book', '', 'it', 'Vinci per ogni prenotazione'),
( 'BOOKING', 'book_slogan', '', 'it', 'Voi non siete lontani dalla vostra prossima esperienza FUN per cenare fuori'),
( 'BOOKING', 'booking_sign', '', 'it', 'Potete registrarvi usando la vostra email per visualizzare le vostre prenotazioni e lasciare i vostri commenti'),
( 'BOOKING', 'approve_tac', '', 'it', 'Avete bisogno di approvare i termini e condizioni per prenotare un tavolo'),
( 'BOOKING', 'booking_send', '', 'it', 'Riceverete i dettagli della vostra prenotazione via email e SMS'),
( 'BOOKING', 'booking_confirm', '', 'it', 'La vostra prenotazione è confermata a'),
( 'BOOKING', 'booking_tmc1', '', 'cn', '通过确认，我同意'),
( 'BOOKING', 'invalid_date', '', 'cn', '日期输入错误。请再试一次'),
( 'BOOKING', 'invalid_cover', '', 'cn', '人数输入错误。请再试一次'),
( 'BOOKING', 'invalid_time', '', 'cn', '时间输入错误。请再试一次'),
( 'BOOKING', 'invalid_notavailability', '', 'cn', '餐厅此时不可预订。请再试一次'),
( 'BOOKING', 'invalid_seatleft', '', 'cn', '剩余座位人数'),
( 'BOOKING', 'invalid_restaurantclose', '', 'cn', '餐厅关闭无法接受预订。请再尝试另一个时间/天'),
( 'BOOKING', 'booking_tmc2', '', 'cn', '条款和条件'),
( 'BOOKING', 'invalid_nomoreseat', '', 'cn', '没有足够的席位接受预订。请尝试修改搜索'),
( 'BOOKING', 'invalid_toomanyguest', '', 'cn', '人数太多');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `translation`
--
ALTER TABLE `translation`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `uniq_trans` (`zone`,`elem_name`,`object_name`,`lang`), ADD KEY `lang` (`lang`), ADD KEY `element` (`elem_name`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `translation`
--
ALTER TABLE `translation`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2582;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
