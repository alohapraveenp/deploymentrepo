<!DOCTYPE html>
<html>
<head>
<style>
html, body, map-canvas {
height: 100%;
}
</style>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
<script>

function initialize() {

<?php

$arg = $_REQUEST["arg"];
if(!empty($arg))
	{
	$argAr = explode(">", $arg);
	$value = "[\n";
	for($i = 0, $sep = ""; $i < count($argAr); $i++, $sep = ", ")
		{
		$lineAr = explode("|", $argAr[$i]);
		$value .= $sep . "[";
		for($k = 0, $sep1 = ""; $k < 4; $k++, $sep1 = ", ")
			{
			if($k == 0) $value .= $sep1 . "'" . $lineAr[$k] . "'";
			else $value .= $sep1 . $lineAr[$k];
			}
		$value .= "]";
		}
	$value .= "]";
	}

printf(" var beaches = %s;\n", $value);

/*
var beaches = [
  ['TheFunKitchen', 1.30, 103.83, 4],
  ['Hilton', 1.32, 103.84, 5],
  ['Shangri La', 1.33, 103.85, 3],
  ['Fullerton', 1.34, 103.86, 2],
  ['Marina Bay', 1.35, 103.87, 1]
];

*/

?>

  var infowindow = new google.maps.InfoWindow();
  var myzoom = (beaches.length > 2) ? 12 : 13;
  var delta = (beaches.length > 2) ? 0 : 0.05;
  var beach = beaches[0];
  var myLatlng = new google.maps.LatLng(beach[1]-delta, beach[2]);

  var mapOptions = {
   center: myLatlng,
   zoom: myzoom
  }
  
  var mymap = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

  for (var i = 0; i < beaches.length; i++) {
    beach = beaches[i];
    myLatLng = new google.maps.LatLng(beach[1], beach[2]);
	mytitle = (beaches.length > 2) ? beach[0] : beach[0]+'('+beach[1]+', ' + beach[2] + ')';
	zindex = beach[3];
	
    var marker = new google.maps.Marker({
        position: myLatLng,
        map: mymap,
        title: mytitle,
        zIndex: zindex
    });

      google.maps.event.addListener(marker, 'mouseover', (function(marker, i) {
        return function() {
          infowindow.setContent(marker.title);
          infowindow.open(mymap, marker);
        }
      })(marker, i));

    google.maps.event.addListener(marker, 'mouseout', function() { infowindow.close(); }); 
  }
}

google.maps.event.addDomListener(window, 'load', initialize);

</script>
</head>
<body>
<div id="map-canvas" style='width:800px;height:800px;'></div>
</body>
</html>

