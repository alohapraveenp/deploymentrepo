var map;
// Load coordinates
var myLatlng = new google.maps.LatLng(longitude, latitude);
// Initialize
function initialize() {
    // Map Options
    var mapOptions = {
        zoom: 14,
        center: myLatlng,
        mapTypeId: google.maps.MapTypeId.TERRAIN,
        scrollwheel: false,
        navigationControl: false,
        mapTypeControl: false,
        scaleControl: false,
        draggable: false,
        zoomControl: false
    };
    // Load Map
    map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
    // Marker
    var marker = new google.maps.Marker({
        position: myLatlng,
        map: map,
    });
}
google.maps.event.addDomListener(window, 'load', initialize);
