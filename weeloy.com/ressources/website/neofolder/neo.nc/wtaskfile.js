module.exports = function(grunt) {
        // Project configuration.
        grunt.initConfig({
        dirs: {
            output: '../ressources/website/neofolder/neo.nc/'
        },
        uglify: {
           options: {
            },            
            my_target: {
        		files: {
                    '<%= dirs.output %>mini/neo.min.js': ['<%= dirs.output %>mini/neohide.js']
                }
            }
        },
        concat: {
            options: {
                separator: ';',
            },
            dist: {
                src: [
                    '<%= dirs.output %>js/lib/angular-animate.min.js',
                    '<%= dirs.output %>js/lib/angular-material.min.js',
                    '<%= dirs.output %>js/lib/angular-aria.min.js',
                    '<%= dirs.output %>js/lib/angular-sanitize.min.js',
                    '<%= dirs.output %>js/lib/ui-bootstrap-tpls-0.14.2.min.js',
                    '<%= dirs.output %>js/lib/angular-route.min.js',
                    '<%= dirs.output %>js/tether.min.js',
                    '<%= dirs.output %>js/jquery-3.1.1.min.js',
                    '<%= dirs.output %>js/bootstrap.min.js',
                    '<%= dirs.output %>js/mdb.min.js',
                    '<%= dirs.output %>partials/data.js',
                    '<%= dirs.output %>partials/index.js'
                     
                ],
                dest: '<%= dirs.output %>mini/neo.min.js',
            },
        }
   });

    // Load the plugin that provides the "uglify" task.
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-obfuscator');
    grunt.loadNpmTasks('grunt-string-replace');
    
    // Default task(s).
    grunt.registerTask('default', [ 'concat']);
    //grunt.registerTask('default', [ 'concat', 'uglify']);
};
