app.config(function($routeProvider) {
    $routeProvider
    .when("/", {
        templateUrl : "partials/main.htm"
    })
    .when("/main", {
        templateUrl : "partials/main.htm"
    })
    .when("/offre", {
        templateUrl : "partials/offre.htm"
    })
    .when("/reference", {
        templateUrl : "partials/reference.htm"
    })
    .when("/equipe", {
        templateUrl : "partials/equipe.htm"
    })
    .when("/recrutement", {
        templateUrl : "partials/recrutement.htm"
    })
    .when("/contact", {
        templateUrl : "partials/contact.htm"
    })
    .otherwise({
    	templateUrl : "partials/main.htm"
    })
;
});

app.controller('myController', ['$scope', '$http', '$filter', '$timeout', '$location', '$anchorScroll', function($scope, $http, $filter, $timeout, $location, $anchorScroll) {

	var index= 0, page = 0;
	$scope.mobileflg = (/Mobi/.test(navigator.userAgent)) || (/iPhone|iPad|iPod|Android/i.test(navigator.userAgent));
	$scope.useragent = navigator.userAgent + ' mobile=' + (($scope.mobileflg) ? 'YES' : 'NO');

	$scope.pagetitle = '';
	$scope.showview = 1;
	$scope.mAr = missionData;
	$scope.mBr = missionBlock;
	$scope.equipe = equipeData;
	$scope.service = serviceData;
	$scope.gallery = galleryData;
	$scope.clients = clientslogo;
	$scope.categories = categories;
	$scope.captcha = false;
	$scope.captchaResponse = "";
	$scope.user = { name: "", email:"", subject:"", comment:"" };
	$scope.request = "";
		
	$scope.webtemplate = [ "main", "offre", "reference", "equipe", "recrutement", "contact" ];
	$scope.pagelabel = ["Accueil", "L’Offre", "Clients", "L’equipe", "Le Recrutement", "Contact"];

	$scope.pageScrollto = function(url) {
		var tt = url.split("/");
		console.log('SCROLL', tt[0], url);
		$location.path("/" + tt[0]);
	        $location.hash(tt[1]);

	        $anchorScroll();
		};
		
	$scope.checking = function(o, l, content) {
		  if(typeof o !== "string" || o.length < l) {
		  	alert("renseignez "+content);
		  	return false;
		  	}
		  return true;
		};

	$scope.emailvalidation = function(email) {
		var mailformat = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		console.log('VALID', email, email.match(mailformat));
		return (!email.match(mailformat)) ? false : true;
		}
		
	$scope.onsubmit = function(what) {
		var captchaResponse, u = $scope.user;
		
		$scope.captchaResponse = "none";
		if($scope.request !== "") {
			alert("votre demande est traitée actuellement ou a déjà été traitée");
			return false;
			}
			
		if($scope.captcha === false && false) {
			$scope.captchaResponse = grecaptcha.getResponse();
			if($scope.captchaResponse === "") {
				alert("Etes-vous un 'robot'!?");
				return false;
				} 
			}

		$scope.captcha = true;
		if($scope.checking(u.name, 3, 'votre nom') !== true) 
			return false;
		if($scope.checking(u.email, 8, 'votre email') !== true)
			return false;
		if($scope.emailvalidation(u.email) !== true) {
			alert("Renseignez un email valid");
			return false;
			}
			
		if(what === 'c') {
			// if($scope.checking(u.subject, 5, 'le suject') !== true)  return false;
			// if($scope.checking(u.comment, 8, 'l’object de votre demande') !== true) return false;
		} else { 
			u.subject = "newsletter"; 
			u.comment = ""; 
			}

		$scope.request = $http({
			method: "post",
			url: "./checkemail.php",
			data: { name: u.name, email: u.email, subject: u.subject, comment: u.comment, captcha: $scope.captchaResponse },
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			});
		
		$scope.request.success(function (status) { 
			if(typeof status === "string" && status.length > 1 && status.indexOf('success') > -1)
				;//alert("Votre demande nous a été envoyée "); 
			else {
				alert("Votre demande n’a pas été envoyée "+status); 
				$scope.request = "";
				}
			});
		alert("Votre demande nous a été envoyée "); 
		return true;
	};
		
	$scope.$on("$routeChangeStart", function (event, next, current) {
		$.getScript("https://www.google.com/recaptcha/api.js?hl=fr");
		$scope.captcha = false;
		$scope.request = "";
		$scope.captchaResponse = "";
		$scope.user = { name: "", email:"", subject:"", comment:"" };
		$scope.pagetitle = "";
		$scope.showview = (next.templateUrl && next.templateUrl.indexOf("main") >= 0) ? 1 : 0;   
		$scope.webtemplate.map(function(ll, index) {
			if(next.templateUrl) {
				var k = next.templateUrl.indexOf(ll);
				 if(k  >= 0) {
					 $scope.pagetitle = $scope.pagelabel[index];
					  console.log('TITLE', $scope.pagelabel[index], k, next.templateUrl);
					 }
				}
			});    			   
		$('#collapseEx').collapse('hide');
		$('#collapseEx').collapse('hide');
		console.log('ROUTE', next.templateUrl);
   	 });

	$scope.isRouteActive = function(route) { 
		if(route === "accueil") {
			var tmp = $location.path().replace(/^.*index.html/, "");
			return (tmp.length < 4 || tmp === "/main");
			}
		return ($location.path().match(route)) ? true : false;
	};
		
	$scope.mAr.map(function(o) { o.index = index++ });
	
	$scope.even = function(x) {
		return ((x.index % 2) !== 0);
		}

}]);