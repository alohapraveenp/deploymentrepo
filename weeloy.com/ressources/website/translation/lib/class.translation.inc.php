<?php

class WY_Translation {
    
    var $msg;
    var $content;
    var $result;
    var $allow_langue  = array('en' , 'fr' , 'cn' , 'hk' , 'th' , 'my' , 'jp' , 'kr' , 'id' , 'es' , 'it' , 'de' , 'ru', 'pt', 'vi');


    function __construct() {
    	}
    	    	
 	function readcontent($zone, $lang) {
 		if(!in_array($lang, $this->allow_langue)) {
 			$this->msg = "Invalid language " . $lang;
 			return $this->result;
 			}
 			
 		$this->content = array();
		$firstpass = pdo_multiple_select("SELECT zone, elem_name, object_name, lang, content from translation WHERE zone = '$zone' and lang = 'en' order by content");
		if(count($firstpass) < 1)
			return $this->result = -1;
		$i = 0; 
    
		foreach($firstpass as $data) {
			$elem_name = $data['elem_name'];
			$object_name = $data['object_name'];
			$row = pdo_single_select("SELECT content from translation WHERE zone = '$zone' and lang = '$lang' and elem_name = '$elem_name' and object_name = '$object_name' limit 1");	
			$content[$i]['translated'] = (count($row) > 0) ? $row['content'] : "";	
			$content[$i]['zone'] = $data['zone'];
			$content[$i]['elem_name'] = $data['elem_name'];
			$content[$i]['object_name'] = $data['object_name'];
			$content[$i]['content'] = $data['content'];
			$content[$i]['langue'] = $lang;
			$i++;
			}
		$this->content = $content;
		return $this->result = 1;
 		} 	

}

?>