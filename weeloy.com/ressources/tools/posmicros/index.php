<html>
<body>
<?php
include('./httpful.phar');
include('lib/wpdo.inc.php');
require('restaurant.php');
$apiKey = "70e48a32d9444c29bb972d39372e45be";
$url = "https://api.omnivore.io/0.1/locations";
function SaveTicketToDB($restaurant, $table, $ticket) {
	try {
		$date = date("Y-m-d");
		$time = date("H:i:sa");
		$strTicket = json_encode($ticket);
		$isOpen = $ticket->open;
        $revenueCentre = $ticket->revenueCentre->name;
		$sql_check = "SELECT ID FROM pos_orders WHERE restaurantID = '$restaurant->id' AND revenue_centre = '$revenueCentre' AND tablenumber = '$table->number' AND ticketnumber = '$ticket->number' LIMIT 1";
		$sql_ins = "INSERT INTO pos_orders (date, time, restaurantID, revenue_centre, tablenumber, ticketnumber, orders, IsOpen) VALUES ('$date', '$time', '$restaurant->id', '$revenueCentre', '$table->number', '$ticket->number', '$strTicket', '$isOpen')";
		$result = pdo_insert_unique($sql_check, $sql_ins);
		if ($result == 0)
			echo "Restaurant: ".$restaurant->id." Revenue Centre: ".$revenueCentre.", table ".$table->number.", ticket ".$ticket->name." new order saved: <br>";
		else {
			echo "Updating table ".$table->number.", ticket ".$ticket->name."...<br>";
			$sql = "UPDATE pos_orders SET orders = :orders, IsOpen = :IsOpen WHERE ID = :ID";
            $parameters = array('orders' => $strTicket, 'IsOpen' => $isOpen, 'ID' => $result);
            pdo_update($sql, $parameters);
		}
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}
function SaveRestaurantOrdersToDB($restaurant) {
	echo "Restaurant ".$restaurant->id." has ".count($restaurant->tables)." tables<br>";
	for ($i = 0; $i < count($restaurant->tables); $i++) {
		echo "Table ".$restaurant->tables[$i]->number." has ".count($restaurant->tables[$i])." has ".count($restaurant->tables[$i]->tickets)." tickets";
		for ($j = 0; $j < count($restaurant->tables[$i]->tickets); $j++)
			SaveTicketToDB($restaurant, $restaurant->tables[$i], $restaurant->tables[$i]->tickets[$j]);
	}
}
function OpenTicketInEachTable($restaurant) {
	global $url, $apiKey;
	$restaurantURL = $url.'/'.$restaurant->id;
	echo "Restaurant ".$restaurant->name." has ".count($restaurant->tables)." tables<br>";
	$revenue_center = 0;
	for ($i = 0; $i < count($restaurant->tables); $i++) {
		// Open tickets
		//$OpenTicket = json_decode(file_get_contents('./ticket.json', FILE_USE_INCLUDE_PATH));
		$OpenTicket = new OpenTicket();
		$OpenTicket->employee = $restaurant->employees[0]->id;
		$OpenTicket->order_type = $restaurant->orderTypes[0]->id;
		$OpenTicket->revenue_center = $restaurant->revenueCentres[$revenue_center]->id;
		$OpenTicket->table = $restaurant->tables[$i]->id;
		$OpenTicket->guest_count = $i + 1;
		$OpenTicket->name = "Ticket".$i;
		$OpenTicket->auto_send = true;
		$revenue_center++;
		$revenue_center %= count($restaurant->revenueCentres);
		//var_dump($restaurantURL.'/tickets');
		//var_dump($OpenTicket);
		//var_dump(json_encode($OpenTicket, JSON_PRETTY_PRINT));
		$response = \Httpful\Request::post($restaurantURL.'/tickets')
		    ->sendsJson()
		    ->addHeader('Api-Key', $apiKey)
		    ->addHeader('Content-Type', 'application/json')
		    ->body(json_encode($OpenTicket, JSON_PRETTY_PRINT))
		    ->send();
		//var_dump($response->body);
		$table = \Httpful\Request::get($restaurantURL."/tables/".$OpenTicket->table)
			   	->addHeader('Api-Key', $apiKey)
				->send();
		if ($table->body->id != $OpenTicket->table)
			die($table->body->id. " : ".$OpenTicket->table);
		//var_dump($table->body);
		//var_dump($restaurant->tables[$i]->tickets);
		foreach ($table->body->_embedded->open_tickets as $t) {
			$ticket = new Ticket($t);
			//var_dump($ticket);
			array_push($restaurant->tables[$i]->tickets, $ticket);
		}
		echo "Restaurant ".$restaurant->name." Table ".$restaurant->tables[$i]->number." has ".count($restaurant->tables[$i]->tickets)." tickets:<br>";
		//var_dump($restaurant->tables[$i]->tickets);
	}
}

function GetAllInformation() {
	global $url, $apiKey;
$locations = \Httpful\Request::get($url)
    ->addHeader('Api-Key', $apiKey)
    ->send();
echo "<b>".count($locations->body->_embedded->locations)." Locations:</b> ";
//var_dump($response->body);
echo '<br>';
echo '<ul>';
$restaurants = array();
$restaurantFile = fopen("restaurants.json", "w") or die("Unable to open file restaurants.json for writing!");
foreach ($locations->body->_embedded->locations as $location)
{
	$restaurantURL = $url.'/'.$location->id;
	//var_dump($location);
	//echo '<br>';
	$restaurant = new Restaurant();
	$restaurant->id = $location->id;
	$restaurant->address = $location->address->street1.", ".$location->address->street2.", ".$location->address->state.", ".$location->address->city.", ".$location->address->zip.", ".$location->address->country;
	$restaurant->name = $location->name;
	$restaurant->phone = $location->phone;
	$restaurant->url = $location->website;
	echo "<li><b>ID:</b> ".$location->id."</li>";
	echo "<li><b>Address:</b> ".$location->address->street1.", ".$location->address->street2.", ".$location->address->state.", ".$location->address->city.", ".$location->address->zip.", ".$location->address->country.", "."</li>";
	echo "<li><b>Name:</b> ".$location->name."</li>";
	echo "<li><b>Phone:</b> ".$location->phone."</li>";
	echo "<li><b>URL:</b> ".$location->website."</li>";
	//A Menu has no intrinsic data, but instead contains other resources that describe the Menu.
	//A Location only has one Menu.
	$menu = \Httpful\Request::get($location->_links->menu->href)
	    ->addHeader('Api-Key', $apiKey)
	    ->send();
	echo "<li><b>Menu</b> ".$menu->body->_links->self->href." :";
	//var_dump($menu->body);
	//echo '<br>';
	$categories = \Httpful\Request::get($menu->body->_links->categories->href)
	   	->addHeader('Api-Key', $apiKey)
		->send();
	//var_dump($categories->body);
	//var_dump($categories->body->_embedded->categories);
	echo "<br>";
	echo "<b>".count($categories->body->_embedded->categories)." Categories:</b> ";
	echo "<ul>";
	foreach ($categories->body->_embedded->categories as $category) {
		echo "<li>ID: ".$category->id."</li>";
		echo "<li>Name: ".$category->name."</li>";
		echo count($category->_embedded->items)." Items: <br>";
		echo "<ul>";
		foreach ($category->_embedded->items as $item) {
			echo "<li>Category: ".$item->_embedded->menu_categories[0]->name."</li>";
			echo "<li>ID: ".$item->id."</li>";
			echo "<li>Name: ".$item->name."</li>";
			echo "<li>Price: ".$item->price."</li>";
			echo "<br>";
		}
		echo "</ul>";
	}
	echo "</ul>";
	// Menu Items
	$items = \Httpful\Request::get($menu->body->_links->items->href)
	   	->addHeader('Api-Key', $apiKey)
		->send();
	//var_dump($items->body);
	//var_dump($items->body->_embedded->menu_items);
	echo "<b>".count($items->body->_embedded->menu_items)." Menu Items:</b> ";
	echo "<ul>";
	foreach ($items->body->_embedded->menu_items as $item) {
		$menuItem = new Menu();
		$menuItem->id = $item->id;
		$menuItem->name = $item->name;
		$menuItem->category = $item->_embedded->menu_categories[0]->name;
		$menuItem->price = $item->price;
		if (!array_key_exists($menuItem->category, $restaurant->menu))
			$restaurant->menu[$menuItem->category] = array();
		array_push($restaurant->menu[$menuItem->category], $menuItem);
		echo "<li>Category: ".$item->_embedded->menu_categories[0]->name."</li>";
		echo "<li>ID: ".$item->id."</li>";
		echo "<li>Name: ".$item->name."</li>";
		echo "<li>Price: ".$item->price."</li>";
		echo "<br>";
	}
	echo "</ul>";
	// Revenue Centers
	$centres = \Httpful\Request::get($location->_links->revenue_centers->href)
	    ->addHeader('Api-Key', $apiKey)
	    ->send();
	//var_dump($tables->body);
	echo "<b>".count($centres->body->_embedded->revenue_centers)." Revenue Centres:</b> ";
	echo "<ul>";
	foreach ($centres->body->_embedded->revenue_centers as $centre) {
		//var_dump($centre);
		$t = new RevenueCentre($centre);
		array_push($restaurant->revenueCentres, $t);
		echo "<li>ID: ".$centre->id."</li>";
		echo "<li>Name: ".$centre->name."</li>";
		echo "<li>Default: ".($centre->default?"Yes":"No")."</li>";
		echo "<li>".count($t->tickets)." open tickets</li>";
		echo "<br>";
	}
	echo "</ul>";	
	// Tables
	$tables = \Httpful\Request::get($location->_links->tables->href)
	    ->addHeader('Api-Key', $apiKey)
	    ->send();
	//var_dump($tables->body);
	echo "<b>".count($tables->body->_embedded->tables)." Tables:</b> ";
	echo "<ul>";
	foreach ($tables->body->_embedded->tables as $table) {
		//var_dump($table);
		$t = new Table($table);
		array_push($restaurant->tables, $t);
		echo "<li>ID: ".$table->id."</li>";
		echo "<li>Name: ".$table->name."</li>";
		echo "<li>Number: ".$table->number."</li>";
		echo "<li>Seats: ".$table->seats."</li>";
		echo "<li>Available: ".($table->available?"Yes":"No")."</li>";
		echo "<li>".count($t->tickets)." open tickets</li>";
		echo "<br>";
	}
	echo "</ul>";
	// Employees
	$employees = \Httpful\Request::get($location->_links->employees->href)
	    ->addHeader('Api-Key', $apiKey)
	    ->send();
	//var_dump($employees->body->_embedded->employees);
	echo "<b>".count($employees->body->_embedded->employees)." Employees:</b> ";
	echo "<ul>";
	foreach ($employees->body->_embedded->employees as $employee) {
		$employeeItem = new Employee();
		$employeeItem->id = $employee->id; 
		$employeeItem->name = $employee->first_name.', '.$employee->last_name;
		$employeeItem->firstName = $employee->first_name;
		$employeeItem->lastName = $employee->last_name;
		$employeeItem->checkName = $employee->check_name;
		$employeeItem->login = $employee->login;
		array_push($restaurant->employees, $employeeItem);
		echo "<li>ID: ".$employee->id."</li>";
		echo "<li>First Name: ".$employee->first_name."</li>";
		echo "<li>Last Name: ".$employee->last_name."</li>";
		echo "<li>Login: ".$employee->login."</li>";
		echo "<br>";
	}
	echo "</ul>";
	// Order Types
	$orderTypes = \Httpful\Request::get($location->_links->order_types->href)
	    ->addHeader('Api-Key', $apiKey)
	    ->send();
	//var_dump($orderTypes->body->_embedded->order_types);
	echo "<b>".count($orderTypes->body->_embedded->order_types)." Order Types:</b><br>";
	//echo "<ul>";
	foreach ($orderTypes->body->_embedded->order_types as $order) {
		$orderType = new OrderType();
		$orderType->id = $order->id;
		$orderType->name = $order->name;
		$orderType->available = $order->available;
		array_push($restaurant->orderTypes, $orderType);
	}
	//echo "</ul>";
	echo "</ul>";		
	$tickets = \Httpful\Request::get($location->_links->tickets->href)
	    ->addHeader('Api-Key', $apiKey)
	    ->send();
	if ($tickets) {
	//var_dump($tickets->body);
	echo "<b>".count($tickets->body->_embedded->tickets)." Tickets:</b> ";
	echo "<ul>";
	foreach ($tickets->body->_embedded->tickets as $t) {
		//var_dump($table);
		$ticket = new Ticket($t);
		array_push($restaurant->tickets, $ticket);
		echo "<li>ID: ".$t->id."</li>";
		echo "<li>Name: ".$t->name."</li>";
		echo "<li>Number: ".$t->ticket_number."</li>";
		echo "<li>Open: ".($t->open?"Yes":"No")."</li>";
		echo "<li>Totals: ";
		echo "<ul>";
		echo "<li>Due: ".$t->totals->due."</li>";
		echo "<li>Other Charges: ".$t->totals->other_charges."</li>";
		echo "<li>Service Charges: ".$t->totals->service_charges."</li>";
		echo "<li>Sub Total: ".$t->totals->sub_total."</li>";
		echo "<li>Tax: ".$t->totals->tax."</li>";
		echo "<li>Total: ".$t->totals->total."</li>";
		echo "</ul>";
		echo "</li>";
		echo "<br>";
	}
	echo "</ul>";
	// Print information of the first table
	$table = \Httpful\Request::get($restaurantURL."/tables/".$restaurant->tables[0]->id)
	    ->addHeader('Api-Key', $apiKey)
	    ->send();
	//var_dump($table->body);
    }//if ($tickets)
    OpenTicketInEachTable($restaurant);
    array_push($restaurants, $restaurant);
}//foreach
fwrite($restaurantFile, json_encode($restaurants, JSON_PRETTY_PRINT));
fclose($restaurantFile);
echo '</ul>';
SaveRestaurantOrdersToDB($restaurant);
}
GetAllInformation();
echo "Done!";
?>
</body>
</html>