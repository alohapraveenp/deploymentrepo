<?php
class Base {
	public $id;
	public $name;
}
class Restaurant extends Base {
	public $address;
	public $phone;
	public $url;
	public $menu = array();
	public $tables = array();
	public $tickets = array();
	public $employees = array();
	public $orderTypes = array();
	public $revenueCentres = array();
}
class Employee extends Base {
	public $firstName;
	public $lastName;
	public $checkName;
	public $login;
}
class Menu extends Base {
	public $category;
	public $price;
}
class RevenueCentre extends Base {
	public $default;
	//public $tables = array();
	public $tickets = array();
	public function __construct($rc) {
		//var_dump($rc);
		$this->id = $rc->id;
		$this->name = $rc->name;
		foreach ($rc->_embedded->open_tickets as $ticket)
			array_push($this->tickets, new Ticket($ticket));						
	}
}
class Table extends Base {
	public $number;
	public $seats;
	public $available;
	public $revenueCentre;
	public $tickets = array();
	public function __construct($t) {
		//var_dump($t);
		$this->id = $t->id;
		$this->name = $t->name;
		$this->number = $t->number;
		$this->seats = $t->seats;
		$this->available = $t->available;
		if ($t->_embedded) {
			$this->revenueCentre = $t->_embedded->revenue_center;
			//var_dump($t->_embedded->open_tickets);
			foreach ($t->_embedded->open_tickets as $table)
				array_push($this->tickets, new Ticket($table));
			//var_dump($this->tickets);
		}
	}
}
class OrderType extends Base {
	public $available;
}
class Ticket extends Base {
	public $number;
	public $revenueCentre;
	public $open;
	public $totalDue;
	public $otherCharges;
	public $serviceCharges;
	public $subTotal;
	public $tax;
	public $total;
	public $closedAt;
	public $openedAt;
	public $autoSend; // When false, items aren’t sent to kitchen until ticket is paid in full.
	public $guestCount;
	public $items = array();
	public function __construct($t) {
		$this->id = $t->id;
		$this->name = $t->name;
		$this->number = $t->ticket_number;
		$this->open = $t->open;
		$this->totalDue = $t->totals->due;
		$this->otherCharges = $t->totals->other_charges;
		$this->serviceChages = $t->totals->service_charges;
		$this->subTotal = $t->totals->sub_total;
		$this->total = $t->totals->total;
		$this->closedAt = $t->closed_at;
		$this->openedAt = $t->opened_at;
		$this->autoSend = $t->auto_send;
		$this->guestCount = $t->guest_count;
		if ($t->_embedded) {
			$this->revenueCentre = $t->_embedded->revenue_center;
			//var_dump($t->_embedded->items);
			foreach ($t->_embedded->items as $i) {
				$item = new TicketItem();
				$item->id = $i->id;
				$item->name = $i->name;
				$item->sent = $i->sent;
				$item->comment = $i->comment;
				$item->unitprice = $i->price_per_unit;
				$item->quantity = $i->quantity;
				array_push($this->items, $item);
			}
		}
		//var_dump($this->items);
	}
}
class TicketModifier extends Base {
	public $comment;
	public $unitprice;
	public $quantity;
}
class TicketItem extends TicketModifier {
	public $sent; // True if item has been sent to the kitchen. False if item has been held.
	public $modifiers = array();
	public $menu;
}
class OpenTicket {
	public $employee;
	public $order_type;
	public $revenue_center;
	public $table;
	public $guest_count;
	public $name;
	public $auto_send;// When false, items aren’t sent to kitchen until ticket is paid in full.
}
class Discount extends Base {
	public $appliesTo;
	public $available;
	public $type;
	public $value;
	public $min;
	public $max;
	public $minTicketTotal;
	public $open;
}
class TicketItemDiscount extends Base {
	public $comment;
	public $value;
}
?>