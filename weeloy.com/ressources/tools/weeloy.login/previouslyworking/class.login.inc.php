<?

/**
 *	Richard Kefs (c) 2014
 *
 * 	Contains all classes definition for login session.
 *
*/

define("__COOKIENAME__", "Weeloy_Member");

class WY_Profile {

  var $email;
  var $password;
  var $retypepassword;
  var $oldpassword;
  var $name;
  var $firstname;
  var $mobile;
  var $country;
  
}

class WY_Login {

   var $db;
   var $ui;
   var $msg;
   var $result;
   var $member;
   var $selection;
   var $signIn;
   
   function init($db) {
		$this->db = $db;
		$this->signIn = array();
	}
   
	private function errormsg($msg, $retvalue) {
		$this->msg = $msg;
		$this->result = $retvalue;
		return $this->result;
	}

	private function test_input($data)
		{
		$data = trim($data);
		$data = stripslashes($data);
		$data = htmlspecialchars($data);
		return $data;
		}

	private function email_validation($email)
	{		
		if(empty($email))
			return -1;
		if(!preg_match("/([\w\-]+\@[\w\-]+\.[\w\-]+)/", $email))
			return -1;
		return 1;
	}

	static protected $mycodeAr = array('Anguille', 'Brochet', 'Carrelet', 'Dragonnet', 'Esturgeon', 'Fletan', 'Grenadier', 'Hareng', 'Inanga', 'Julienne', 'Kokopu', 'Lamproie', 'Maquereau', 'Napilus', 'Ouananiche', 'Piranha', 'Quillback', 'Rascasse', 'Sardine', 'Turbots', 'Uranoscope', 'Vivaneau', 'Warbonnet', 'Xenotoca', 'Yellowtail', 'Zebrafish');

	function myencode($plaintext)
	{
		$keysAr = self::$mycodeAr;
		
		$plaintext = strlen($plaintext) . "|" . $plaintext;
		$cle = rand(0, 25);
		$key = $keysAr[$cle];
	
		// the key should be random binary, use scrypt, bcrypt or PBKDF2 to
		// convert a string into a key is specified using hexadecimal
		//$key = pack('H*', $code);
	
		// show key size use either 16, 24 or 32 byte keys for AES-128, 192 and 256 respectively
		$key_size =  strlen($key);    
		$iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
		$iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
	
		// creates a cipher text compatible with AES (Rijndael block size = 128)
		// to keep the text confidential 
		// only suitable for encoded input that never ends with value 00h
		// (because of default zero padding)
		$ciphertext = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $plaintext, MCRYPT_MODE_CBC, $iv);

		// prepend the IV for it to be available for decryption
		$ciphertext = $iv . $ciphertext;
	
		// encode the resulting cipher text so it can be represented by a string
		$ciphertext = chr(65 + $cle) . base64_encode($ciphertext);
		return $ciphertext;
		}

	function mydecode($ciphertext)
	{
		$keysAr = self::$mycodeAr;
		$cle = ord($ciphertext[0]) - 65;	
		$key = $keysAr[$cle];
		$ciphertext = substr($ciphertext, 1);
			
		// === WARNING ===

		// Resulting cipher text has no integrity or authenticity added
		// and is not protected against padding oracle attacks.
	
		// --- DECRYPTION ---
	
		//$key = pack('H*', $code);
		$key_size =  strlen($key);    
		$iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);

		$ciphertext_dec = base64_decode($ciphertext);
	
		// retrieves the IV, iv_size should be created using mcrypt_get_iv_size()
		$iv_dec = substr($ciphertext_dec, 0, $iv_size);
	
		// retrieves the cipher text (everything except the $iv_size in the front)
		$ciphertext_dec = substr($ciphertext_dec, $iv_size);

		// may remove 00h valued characters from end of plain text
		$plaintext = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, $ciphertext_dec, MCRYPT_MODE_CBC, $iv_dec);

		// get rid of the padding => length . | . plaintexte
		for($start = 0; $start < 8; $start++)
			if($plaintext[$start] < '0' || $plaintext[$start] > '9')
				break;
		if($start >= 8 || $start == 0) return "";
		$len = intval(substr($plaintext, 0, $start));

		return substr($plaintext, $start+1, $len);		
	}
	
	private function getNewpassword($limit) {
		for($str = "", $i = 0; $i < $limit; $i++)
			{
			$n = rand(1, 52) + 64;
			if($n > 90) $n += 6;
			$str .= chr($n);
			}
		return $str;
		}
	
	private function setlogin($email, $chgpass, $application, $extrafield) {

		$db = $this->db;

		$db->query("select firstname from member where email = '$email' limit 1") ;
		$row = $db->fetch_row();
		$firstname = $row[0];
		$this->msg = "Welcome " . ucfirst($firstname);
		$this->result = 2;
		$IPaddr = ip2long($_SERVER['REMOTE_ADDR']);

		$date = new DateTime(gmdate("Y-m-d H:i"));
		$date->add(new DateInterval('PT60M'));
		$token = $this->getNewpassword(16);
		$info = $date->format('H-i');
		$jsonStr = json_encode(array("firstname" => $firstname,  "token" => $token,  "email" => $email,  "info" => $info,  "chgpass" => $chgpass));
		$tokenCookie = $this->myencode($jsonStr);
		$db->query("delete from logonehour where Email = '$email' ");
		$db->query("insert into logonehour (Email, Token, Info, Application, Extrafield) values ('$email', '$token', '$info', '$application', '$extrafield')");
		$db->query("insert into logaccess (Email, Info, Createdate, IPaddr) values ('$email', '$info', NOW(), '$IPaddr')");	

		SetCookie(__COOKIENAME__, "", time()-100000, "/");		// get rid off previous cookie
		SetCookie(__COOKIENAME__, $tokenCookie, time()+3600, "/");

		$this->result = 1;
	}
		
	private function setlogout($email) {
	
		$this->deleteLogins($email, "You have logged out");		
		return $this->result = 1;
	}
	
	
	private function deleteLogins($email, $msg) {
	
		$db = $this->db;

		if(!empty($email))
			$db->query("delete from logonehour where Email = '$email'");
		
		SetCookie(__COOKIENAME__, "", time()-10000, "/");

		$this->msg = $msg;
		$this->result = -1;
	}	

	function GetToken() {
	
		if(empty($_COOKIE[__COOKIENAME__]))
			return array("", "", "", "", "");

		$decoded = $this->mydecode($_COOKIE[__COOKIENAME__]);
		$this->signIn = json_decode($decoded, true);
		if(empty($this->signIn['token']))
			return array("", "", "", "", "");
	
		return $this->signIn;
	}
	
	function logout() {

		$db = $this->db;
				
		$this->signIn = $this->GetToken();

		if(!empty($this->signIn['email']))
			return $this->setlogout($this->signIn['email']);

		return $this->errormsg("Not logged In", -1);
	
	}
	
	function CheckLoginStatus() {
	
		$db = $this->db;	
		
		$db->query("delete from logonehour where DATE_SUB(CURRENT_TIMESTAMP(),INTERVAL 1 HOUR) > TheTS");
			
		$this->signIn = $this->GetToken();
		$token = $this->signIn['token'];	
				
		if(empty($token))
			return $this->errormsg("Please Sign in.", -1);
		$db->query("SELECT logonehour.Token, logonehour.TheTS+0, CURRENT_TIMESTAMP()+0, logonehour.Email FROM logonehour, login WHERE logonehour.Token = '$token' and login.Email = logonehour.Email limit 1");
		if(!($row = $db->fetch_row()))
			return $this->errormsg("Please Sign in.", -2);
	
		$email = $row[3];
		$this->remaining = 3600 - intval(floatval($row[2]) - floatval($row[1]));
		if($this->remaining < 0)
			return $this->deleteLogins($this->signIn['email'], "session expired");

		$statusAr = $this->GetSystemStatus();	// check for system status.
		if($email != "Super_Admin@weeloy.com" && $statusAr['onmaintenance'] + $statusAr['redirection'] > 0)
			return $this->deleteLogins($this->signIn['email'], "On maintenance");
		
		$this->msg = "You are logged In " . ucfirst($this->signIn['firstname']);
		return $this->result = 1;	
	}

	function process_login($type_action, $profile) {
	
		$db = $this->db;
		$type_action = $this->test_input($type_action);

		if(isset($profile->email)) $profile->email = $this->test_input($profile->email);
		if(isset($profile->password)) $profile->password = substr($profile->password, 0, 8);
		if(isset($profile->retypepassword)) $profile->retypepassword = $this->test_input($profile->retypepassword);
		if(isset($profile->newpassword)) $profile->oldpassword = $this->test_input($profile->newpassword);
		if(isset($profile->name)) $profile->name = $this->test_input($profile->name);
		if(isset($profile->firstname)) $profile->firstname = $this->test_input($profile->firstname);
		if(isset($profile->mobile)) $profile->mobile = $this->test_input($profile->mobile);
		if(isset($profile->country)) $profile->country = $this->test_input($profile->country);
		if(isset($profile->application)) $profile->application = $this->test_input($profile->application);
		if(isset($profile->extrafield)) $profile->extrafield = $this->test_input($profile->extrafield);
		
		$this->msg = "";
		$this->result = 0;
		$email = $profile->email;
		$application = $profile->application;
		$extrafield = $profile->extrafield;
		$checkpasswrd = true;
		
		if($application == "twitter")
			{
			$db->query("SELECT Email FROM member where name = '$profile->name' and firstname = '$profile->firstname' limit 1") ;
			if(!($row = $db->fetch_row()))
				return $this->errormsg("Unknown user through twitter", -1);
			$email = $row[0];
			$checkpasswrd = false;
			}

		else if($application == "facebook" || $application == "linkedin")
			$checkpasswrd = false;
					
		$chgpass = $store_password = $tmp_password = "";
		$tmpTimeStamp = 0;

		if(empty($email) || empty($type_action))
			return $this->errormsg("Invalid request", -1);

		if($this->email_validation($email) < 0)
			return $this->errormsg("Invalid email -$email-", -1);
		
		$db->query("update login set Password='', tmpPassword = '0' where tmpTimeStamp < NOW() and tmpPassword = '1'");
		
		$db->query("SELECT Email, Password, tmpPassword FROM login where Email = '$email' limit 1") ;
		if($row = $db->fetch_row())
			{
			$store_password = $row[1];
			$tmp_password = $row[2];
			}

		switch($type_action)
			{
		  	case "Login":
				if(empty($store_password))
					return $this->errormsg("$email email does not exists.<br>Please login with another name or register", -1);

				if($checkpasswrd)
					if($store_password != crypt($profile->password, $store_password))
						return $this->errormsg("This is wrong password or email", -2);
			
				$chgpass = ($tmp_password == "1" && $checkpasswrd)? "pass=2" : "";

				return $this->setlogin($email, $chgpass, $application, $extrafield);

		  	case "Logout":
				return $this->setlogout($email);
			
		  	case "Register":
				if(empty($profile->name) || empty($profile->password))
					return $this->errormsg("Empty value (name, password)", -1);
			
				if(!empty($store_password))
					return $this->errormsg("$email email already exists. Unable to create new member ", -3);
		
				$password = crypt(urlencode($profile->password));

				$db->query("delete from login where Email = '$email' ");
				$db->query("delete from member where email = '$email' ");
				$db->query("insert into member (email, name, firstname, mobile, country) values ('$email', '$profile->name', '$profile->firstname', '$profile->mobile', '$profile->country') ");
				$db->query("insert into login (Email, Password, tmpPassword) values ('$email', '$password', '0')");
			
				return $this->setlogin($email, $chgpass);
			
		  	case "LostPassword":
				if(empty($store_password))
					return $this->errormsg("$email email does not exists.<br>Please login with another name or register", -1);
				
				$tmp_password = $this->getNewpassword(8);
				$password = crypt(urlencode($tmp_password));			
				$db->query("update login set Password = '$password', tmpPassword = '1', tmpTimeStamp = DATE_ADD(NOW(), INTERVAL 7 DAY) where Email = '$email' limit 1");
				$body = sprintf("This temporary password to login in Weeloy System. \n\r Please, change your password afterwards\n\r\n\r Temporary Password: %s\n\r\n\r\n\rBest regards,\n\rWeeloy Support Team", $tmp_password);
				$mailObj = new JM_Mail;
				$mailObj->sendmail($email, "New temporary Password - Weeloy", $body);
				$this->msg = "A new password has been sent ($tmp_password)";
				return $this->result = 1;

		  	case "UpdatePassword":
				return $this->UpdatePassword($profile);
				
			default:			
				return $this->errormsg("Invalid command -$type_action-", -1);
		}
	}
		
	private function UpdatePassword($profile) {

		$db = $this->db;

		$email = $profile->email;
		$OldPassword = urlencode($profile->password);
		$NewPassword = $profile->newpassword;
		$RetypePassword = $profile->retypepassword;

		if(empty($email))
			return $this->errormsg("Email empty. Unable to update login information ", -1);
		
		if($NewPassword != $RetypePassword)
			return $this->errormsg("New and Retype password are different. Try again", -2);
		
		$db->query("SELECT Password FROM login where Email = '$email' limit 1") ;
		if(!($row = $db->fetch_row()))
			return $this->errormsg("User does not exists", -2);

		$store_password = $row[0];
		if(empty($OldPassword) || $store_password != crypt($OldPassword, $store_password))
			return $this->errormsg("This is wrong password or email", -2);

		$NewPassword = crypt(urlencode($NewPassword));
		$db->query("Update login set Password='$NewPassword', tmpPassword='0' where Email = '$email' limit 1");
		$this->msg = "Password has been updated";
		return $this->result = 1;
	}

	function DeleteMember($email) {

		$db = $this->db;
		$db->query("delete from member where email = '$email'") ;
		$db->query("delete from login where Email = '$email'");
		$db->query("delete from logonehour where Email = '$email'");

		return $this->result = 1;
	}
	
	function UpdateMember($value) {

		$db = $this->db;
		$fieldAr = $db->field_names("member");

		$query = $email = "";
		for($i = 0, $sep = ""; $i < count($fieldAr); $i++, $sep = ", ")
			if($fieldAr[$i] != "ID" && array_key_exists($fieldAr[$i], $value)) 
				{
				$query .= $sep . $fieldAr[$i] . " = '" . $value[$fieldAr[$i]] . "'";
				if($fieldAr[$i] == "Email")
					$email = $value["Email"];
				}
				
		if(empty($email))
			return $this->errormsg("Email empty. Unable to update member", -1);

		$db->query("SELECT Email FROM member where email = '$email' limit 1") ;
		if(!($row = $db->fetch_row()))
			return $this->errormsg("Email does not exists. Unable to update User", -1);

		$db->query("Update member set $query  where email = '$email' limit 1");
		return $this->result = 1;
	}
	
	function GetSystemStatus() {
	
		$db = $this->db;		
		$db->query("SELECT Status FROM logtask WHERE Status like 'SYSTEM_STATUS:%' limit 1");
		if(!($row = $db->fetch_row()))
			return array("", "");
	
		$SysAr = explode(":", $row[0]);
		return array("onmaintenance" => intval($SysAr[0]), "redirection" => intval($SysAr[1]));	
		}
	
}

function getCookieEmail($db) {

	$login = new WY_Login;
	$login->init($db);

	$login->signIn = $login->GetToken();
	return $login->signIn['email'];	
}

?>