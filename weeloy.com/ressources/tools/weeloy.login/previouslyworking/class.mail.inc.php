<?

/**
 *	Richard Kefs (c) 2014
 *
 * 	Contains all classes definition for mail session.
 *
*/

//singlemail("administration_ibe@jumanji.com", "the title", "the body", "From: thesend@jumanji.com");


class JM_Mail {

   var $db;
   var $recipient;
   var $subject;
   var $body;
   var $header;
   var $result;
   
	function sendmail($recipient, $subject, $body) {
		$this->send_frommail($recipient, $subject, $body, "From: richard@rkefs.com");
		}
		
	function send_frommail($recipient, $subject, $body, $header) {
		mail($recipient, $subject, $body, $header);
		}
		
	function ltest() {
		$recipient = "richard@kefs.me";
		$subject = "testing mail";
		$body = "this is a test";
		
		$this->sendmail($recipient, $subject, $body);
		}
}

?>