<?

define("__LIBDIR__", $_SERVER['DOCUMENT_ROOT'] . "/../LIB/");

require_once(__LIBDIR__ . "dblib.inc.php");
require_once(__LIBDIR__ . "class.login.inc.php");
require_once(__LIBDIR__ . "class.mail.inc.php");

	$db = new db_mySQL;		// could be user by the included files below
	$db->init();

	$login = new WY_Login;
	$login->init($db);
	
	$testdata = "La prudente neutralité. Dans ce conflit, le Front national ne souhaite pas choisir son camp, tel que le rappelait au Figaro le président de l'Observatoire des radicalités politiques (Orap), Jean-Yves Camus: à la suite des manifestations propalestiniennes interdites qui ont dégénéré, «Marine Le Pen aurait pu dénoncer le communautarisme, selon le chercheur. Elle ne l'a pas fait, ce qui est évidemment le signe d'un malaise au sein du FN sur la question israélo-palestinienne. Ce retrait est évidemment calculé. C'est la conséquence d'une division à l'intérieur du FN qui l'empêche de prendre parti pour l'un ou l'autre camp sans mettre le feu aux poudres».. • Le soutien à Gaza.  Une frange de l'extrême droite penche du côté des Palestiniens. Ainsi, en 1993, pour ses 25 ans, le GUD, syndicat étudiant nationaliste, adoptait pour slogan: «À Paris comme à Gaza, Intifada!» Le rat noir, symbole du mouvement, est pour l'occasion affublé d'un keffieh palestinien et d'un lance-pierre. C'est Frédéric Chatillon, aujourd'hui dans l'entourage de Marine Le Pen, qui est à l'origine de ce choix, par antisionisme et en défense de l'identité nationale (1). Un slogan dont usent toujours les gudars d'aujourd'hui.. À gauche, une affiche du GUD datant de 1993. À droite une image du GUD Lyon, publiée récemment.. Ce soutien à l'indépendance nationale palestinienne se retrouve globalement dans le milieu national-révolutionnaire. Ce dernier rejette ce qu'il considère comme «l'impérialisme américain», sa «thalassocratie» (puissance fondée sur la mer) et ses alliés, dont fait partie Israël. Ainsi, il n'est pas rare d'entendre dans leurs rangs le slogan «Israël assassin, Américains complices!».. • La dénonciation de la présence islamique en France. C'est la position du Bloc identitaire, qui, dans sa logique de rejet du multiculturalisme, et dans la foulée de sa campagne sur les supporteurs algériens pendant la Coupe du monde de football, insiste sur le caractère ethnico-culturel des violences de fin de manifestation. L'historien des extrêmes droites Nicolas Lebourg rappelle: «Le Bloc identitaire est né des cendres d'Unité radicale, qui faisait l'apologie de Ben Laden et des kamikazes palestiniens. Lui se fixe sur une ligne “ni keffieh ni kippa”: il s'agit de dire que cette affaire est moyen-orientale et non française ou européenne. Il y a eu ensuite de lourdes tentations de s'entendre avec l'extrême droite juive. D'autant que d'une part il y a une progression de l'islamophobie dans les milieux juifs, d'autre part Marine Le Pen a su y séduire avec un discours où son État fort se pose en protecteur contre l'islamisme. L'islamophobie est bien plus facile à manier dans l'espace public que l'antisémitisme.». • La dénonciation radicale du communautarisme. Enfin, certains groupes renvoient dos à dos Palestiniens et Israéliens. Ainsi, l'hebdomadaire Rivarol estime dans ses colonnes que «la France est (…) le pays d'Europe qui compte la plus grande communauté juive et la plus importante population arabo-musulmane, le cocktail est forcément explosif. Faire venir des millions d'immigrés du Maghreb et mener parallèlement une politique hystériquement pro-israélienne est une attitude criminelle qui ne peut conduire qu'aux pires catastrophes, aux débordements les plus extrêmes, aux tensions les plus aiguës». C'est à peu près la même position développée par la nébuleuse de l'ancienne Œuvre française, dissoute en 2013 par le gouvernement, qui assimile les soldats israéliens aux djihadistes français partis en Syrie.";
	$testAr = explode(".", $testdata);
	for($i = 0; $i < count($testAr); $i++)
		{
		$vv = $testAr[$i];
		$tt = $login->myencode($vv);
		$uu = $login->mydecode($tt);
		$log = ($uu == $vv) ? 1 : 0;
		printf("%s<br />%s<br />%s <br />%d<br/><hr>", $tt, $vv, $uu, $log);
		}
		
?>
