<?

	require_once("conf/conf.init.inc.php");
	require_once("lib/wpdo.inc.php");
	require_once("lib/class.tool.inc.php");
	require_once("lib/Browser.inc.php");
	require_once("lib/class.mail.inc.php");
	require_once("lib/class.async.inc.php");
	require_once("lib/class.spool.inc.php");
	require_once("lib/wglobals.inc.php");
	
?>

<!-- Angualr Code -->

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="author" content="weeloy. https://www.weeloy.com"/>
        <meta name="copyright" content="weeloy. https://www.weeloy.com"/>  
        <meta http-equiv='expires' content='mon, 1 jan 1998 05:00:00 gmt'/>
        <meta http-equiv='pragma' content='no-cache'/>
        <meta http-equiv='pragma' content='cache-control: max-age=0'/>
        <meta http-equiv='last-modified' content='mon, 1 jan 1998 05:00:00 gmt'/>
        <meta http-equiv='cache-control' content='no-cache, must-revalidate'/>
        <meta name='robots' content='noindex, nofollow'/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <meta name="HandheldFriendly" content="true" />
        <title> Tool Set</title>

        <link href="../../client/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../../client/bower_components/bootstrap/dist/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
        <link href="../../client/bower_components/fontawesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

        <link href="../../modules/booking/assets/css/css/bootstrap-select.css" rel="stylesheet" />
        <link href="../../modules/booking/assets/css/css/bootstrap-social.css" rel="stylesheet" />

        <script type='text/javascript' src="../../client/bower_components/angular/angular.min.js"></script>
        <script type="text/javascript" src="../../client/bower_components/jquery/dist/jquery.min.js"></script>

        <script type="text/javascript" src="../../js/jquery-ui.js"></script>
        <script type="text/javascript" src="../../client/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="../../js/bootstrap-select.js"></script>
        <script type='text/javascript' src="../../js/ui-bootstrap-tpls-0.12.1.min.js"></script>
        <script type="text/javascript" src="../../js/ngStorage.min.js"></script>
        <script type="text/javascript" src="../../js/mylocal.js"></script>

        <script> var app = angular.module("myApp", ['ui.bootstrap', 'ngLocale', 'ngStorage']);</script> 
        
        <style>
            /*CSS Styling*/
        </style>

        <body ng-app="myApp">
            <div class="wrapper" ng-controller='ToolSetController' ng-init="moduleName = 'datacheck';" style='margin-top:20px;'>
                <div class="container container-table"  >
                    <div class="row ">
                        <div class="container">
                            <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-2 col-lg-8 col-md-8 col-sm-8 col-xs-12"> 
						
								<input type="button" ng-click="checkNow()" class="btn btn_default" value="Data Check" style="border-radius:0px;" >
                                <p> </br></p>
                                <table border="1px solid black">
                                    <tr>
                                        <th>Table Name - JASON Data Column
                                        </th> 
                                    </tr>
                                    <tr ng-repeat="record in data">
                                        <td><a href="" ng-click="checkColumn(record)">{{ record }}</a></td>
                                    </tr>
                                </table>   

                                <p> </br></p>
                                <table border="1px solid black">
                                    <tr>
                                        <th>ID
                                        </th>
                                        <th>Column Value
                                        </th>
                                        <th>JSON Value
                                        </th> 
                                    </tr>
                                    <!-- {{columnVal|json}} -->
                                    <tr ng-repeat="value in columnVal" ng-if="value != ''">
                                        <td>{{ value.ID }}</td>
                                        <td>{{ value.COLMN }}</td>
                                        <!-- <td>{{ value.JASON_MSG}}</td> -->
                                        <td><input id="changedata" name="changedata" type='text' ng-model ="value.JASON_MSG"/></td>
                                        <td><input type='button' ng-click="saveData($index, value.ID, value.JASON_MSG)" value="Save"/></td>
                                    </tr>
                                </table>                                 

                            </div>

                        </div>

                    </div>
                       
                </div>
            </div>
           
            <script>
                        app.controller('ToolSetController', function ($scope, $http) {
                            

                            $scope.checkNow = function () {

                            	var API_URL = '../../api/tool/datacheck';
                                $http.post(API_URL).then(function (response) {
                                	$scope.data = response.data.data;

                                });

                            };

                            $scope.checkColumn = function (record) {

                                $scope.columnVal = '';
                                $scope.record = record

                                var API_URL = '../../api/tool/columncheck';

                                $http.post(API_URL,
                                            {
                                                'record': $scope.record
                                            }).then(function (response) {

                                    $scope.columnVal = response.data.data;


                                });

                            };  

                            $scope.saveData = function (index,id,value ){

                                var API_URL = '../../api/tool/updatecolumn';

                                $http.post(API_URL,
                                            {
                                                'record': $scope.record,
                                                'id': id,
                                                'value':value
                                            }).then(function (response) {

                                    alert(JSON.stringify(response.data));

                                });

                            }

                        });
                       
            </script>


        </body>
</html>


