<?php 

echo "turn off protection"; exit;

require_once("conf/conf.init.inc.php");
require_once("lib/wpdo.inc.php");
require_once("conf/conf.session.inc.php");
require_once("lib/wglobals.inc.php");
require_once("lib/class.media.inc.php");
require_once("lib/class.restaurant.inc.php");
require_once("lib/class.booking.inc.php");

set_time_limit(1500);
$starting = microtime(true);
$theRestaurant = "SG_SG_R_DbBistroOysterBar";
$bkg = new WY_Booking();
$nbline = 0;
$keysAr = array("Title", "FirstName", "LastName", "Organisation", "Email", "Telephone", "Mobile", "Res_Date", "Res_Time", "Party_Size", "Date_Res_Made", "Table_ID", "Res_Status", "Operator_Made", "Food_Expenditure", "Bev_Expenditure", "Total_Expenditure", "Room No.", "Notes", "Waiter_Remarks", "Res_Notes", "Reference_No");
$fieldAr = array("salutation", "firstname", "lastname", "company", "email", "mobile", "mobile", "rdate", "rtime", "cover", "cdate", "tablename", "status", "booker", "foodprice", "drinkprice", "foodtotal", "hotelguest", "notestext", "notestext", "specialrequest", "remotebooking");
$fielddatalabel = array("salutation", "firstname", "lastname", "company", "email", "mobile", "mobile", "rdate", "rtime", "cover", "cdate", "tablename", "status", "booker", "generic", "hotelguest", "specialrequest");

$nkeys =  count($keysAr);
$nfield =  count($fieldAr);

if($nfield != $nkeys) {
	echo "unmatched array size. cannot continue";
	exit;
	}

$nbline = 0;
$filedata = file('databooking.txt');
echo count($filedata) . "line<br />";
foreach($filedata as $line) {
	if($nbline++ == 0) {
		$titleAr = explode("\t", $line);
		continue;
		}

//	echo $nline . ", " . $line . "<br />";
	$col = explode("\t", $line);
	$ncol = count($col);
	$generic = $gnfood = $notestext = "";

	for($j = 0; $j < $nfield; $j++)
		$data[$fieldAr[$j]] = "";
		
	for($i = 0; $i < $ncol; $i++) {
		$ckey = $titleAr[$i];	
		$index = array_search($ckey, $keysAr);
		if($index === false) // need 3 === as 0 is false
			continue;
			
		$target = $fieldAr[$index];
		$nn = $col[$i];
		$nn = preg_replace("/\"|\'/", "", $nn);
		$nn = preg_replace("/\\\\/", "", $nn);
		$nn = preg_replace("/’/", "`", $nn);
		$nn = preg_replace("/\s+/", " ", $nn);
		$nn = trim($nn);
		
		if(empty($nn))
			continue;
			
		switch($ckey) {

			case "Email":
				$data[$target] = strtolower($nn);
				break;
				
			case "Mobile":
			case "Telephone":
				$nn = trim(preg_replace("/[^0-9+ ]/", "", $nn));
				if(strlen($nn) == 8)
					$nn = "+65 " . $nn;
				else if(substr($nn, 0, 2) == "65")
					$nn = "+65 " . substr($nn, 2);
				$data[$target] = $nn;
				break;
		
			case "Waiter_Remarks":
			case "Res_Notes":
			case "Res_Notes":
				$data[$target] .= (!empty($data[$target]) ? ", " : "") . $nn;
				break;
				
			case "Res_Date":
			case "Date_Res_Made":
				$data[$target] = date("Y-m-d", strtotime($nn));
				break;
				
			case "Res_Time":
				$pp = (preg_match("/PM/", $nn) && substr($nn, 0, 2) != "12") ? 12 : 0;
				$nn = substr($nn, 0, 5);
				if($pp > 0) {
					$pp = explode(":", $nn);
					$pp[0] = strval(intval($pp[0]) + 12);
					$nn = implode(":", $pp);
					}
				$data[$target] = $nn;
				break;

			case "Res_Status":
				if($nn == "Cancelled") $nn = "cancel";
				else if($nn != "cancel") $nn = "";

				if($nn == "New")
					$nn = "";
				$data[$target] = $nn;
				break;
				
			case "Table_ID":
				if(!empty($nn)) {
					$nn = trim($nn);
					$nn = preg_replace("/[^\d;,]/", "", $nn);
					$nn = preg_replace("/;/", ",", $nn);
					$nn = preg_replace("/,$/", "", $nn);
				}
				$data[$target] = $nn;
				break;

			case "Notes":
			case "Waiter_Remarks":
				$notestext .= ((!empty($notestext)) ? ";" : "") . $nn;
				break;
							
			case "Food_Expenditure":
			case "Bev_Expenditure":
			case "Total_Expenditure":
				$gnfood .= ((!empty($gnfood)) ? "," : "") . "{’". $target . "’:’" . $nn . "’}";
				break;
									
			default:
				$data[$target] = trim($nn);
				break;				
			}
		}

	$notestext = trim($notestext);
	$remotebooking = (!empty($data['remotebooking'])) ? $data['remotebooking']  : "";
	if(empty($data['mobile']))
		$data['mobile'] = "+65 99999999";
	if(empty($data['email']))
		$data['email'] = "no@email.com";
	if(!empty($gnfood))
		$generic = "’orders’:[" . $gnfood . "]";
	if(!empty($notestext))
		$generic .= ((!empty($generic)) ? "," : "") . "’notestext’:’" . $notestext . "’";
	if(!empty($remotebooking))
		$generic .= ((!empty($generic)) ? "," : "") . "’remotebooking’:’" . $remotebooking . "’";

	if(!empty($generic))
		$data['generic'] = $generic = "{". $generic . "}";

	echo "<br /><hr />";
	print_r($data);

	$fielddata = array();
	foreach($fielddatalabel as $label) 
		$fielddata[$label] = $data[$label];
		
	if($bkg->duplicate($theRestaurant, $data['rdate'], $data['email'], $data['mobile'], 1) == 1) {
		printf("<br />duplicate %s %s<br />", $bkg->confirmation, $bkg->cover);
		if($data["tablename"] != "")
			$bkg->setBooking($theRestaurant, $bkg->confirmation, $data["tablename"], "tablename", null);
		continue;
		}
		
	$bkg->import($theRestaurant, $fielddata, $remotebooking);
	}

printf("DONE time = %.2f", (microtime(true) - $starting) );
?>
