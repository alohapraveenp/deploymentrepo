<?php 


echo "turn off protection"; exit;

require_once("lib/wpdo.inc.php");
require_once("conf/conf.init.inc.php");
require_once("conf/conf.s3.inc.php");
require_once("conf/conf.session.inc.php");
require_once("lib/wglobals.inc.php");
require_once("lib/class.media.inc.php");
require_once("lib/class.restaurant.inc.php");
require_once("lib/class.booking.inc.php");
require_once("lib/class.profile.inc.php");
require_once("lib/PhpSpreadsheet/Bootstrap.php");
require_once("lib/imagelib.inc.php");
require_once("lib/class.s3.inc.php");
require_once("lib/class.string.inc.php");
//set_time_limit(900);
$profilePictureURL = "https://static.weeloy.com/images/profiles/";
function RetrieveAndSaveProfileData($inputFile, $theRestaurant, $keysAr, $fieldAr, $fielddatalabel) {
	$starting = microtime(true);
	echo __FILE__." ".__FUNCTION__." ".__LINE__."<br/>";
	$prof = new WY_Profile();
	$nbline = 0;
	$nkeys =  count($keysAr);
	$nfield =  count($fieldAr);
	if($nfield != $nkeys) {
		echo "unmatched array size. cannot continue $nfield != $nkeys";
		exit;
	}
	$nbline = 0;
	$filedata = file($inputFile);
	echo count($filedata) . " line<br />";
	foreach($filedata as $line) {
		if($nbline++ == 0) {
			$titleAr = explode("\t", $line);
			continue;
			}

	//	echo $nline . ", " . $line . "<br />";
		$col = explode("\t", $line);
		$ncol = count($col);
		$content = "";

		for($j = 0; $j < $nfield; $j++)
			$data[$fieldAr[$j]] = "";
			
		for($i = 0; $i < $ncol; $i++) {
			$ckey = $titleAr[$i];	
			$index = array_search($ckey, $keysAr);
			if($index === false) // need 3 === as 0 is false
				continue;
				
			$target = $fieldAr[$index];
			$nn = WY_StringProcessor::FilterValue($col[$i]);
			
			if(empty($nn))
				continue;
				
			switch($ckey) {

				case "Email":
					$data[$target] = strtolower($nn);
					break;
					
				case "Contact Number":
					$data[$target] = WY_StringProcessor::FilterPhoneNumber($nn);
					break;
			
				default:
					$data[$target] = trim($nn);
					break;				
				}
			}
		if(empty($data['mobile']))
			$data['mobile'] = "+65 99999999";
		if(empty($data['email']))
			$data['email'] = "no@email.com";
		if(!empty($data['remark']))
			$content .= ((!empty($content)) ? "," : "") . "’remark’:’" . $data['remark'] . "’";
		if(!empty($data['notes']))
			$content .= ((!empty($content)) ? "," : "") . "’notes’:’" . $data['notes'] . "’";
		if(!empty($data['description']))
			$content .= ((!empty($content)) ? "," : "") . "’description’:’" . $data['description'] . "’";
		if(!empty($content))
			$data['content'] =  "{" . $content . "}";
		//print_r($data['content']);echo "<br/>";
		//error_log("data['content']: ".$data['content']);
		$fielddata = array();
		$fielddata['PROFILEFORMAT0000'] =  "{’remark’:’’, ’notes’:’’, ’description’:’’}";
		$fielddata['internal'] =  "dbbistro-import";
		foreach($fielddatalabel as $label) 
			$fielddata[$label] = $data[$label];
			
		$names = WY_StringProcessor::SplitFullname($data['fullname']);
		if ($names != null && !empty($names)) {
			$fielddata['salutation'] = $names['salutation'];
			$fielddata['firstname'] = $names['firstname'];
			$fielddata['lastname'] = $names['lastname'];
		}
		if ((isset($fielddata['firstname']) && !empty($fielddata['firstname'])) || (isset($fielddata['lastname']) && !empty($fielddata['lastname']))) {
			//echo $fielddata['salutation']." ".$fielddata['firstname']." ".$fielddata['lastname']." ".$fielddata['mobile']." ".$fielddata['email']." ".$fielddata['remark']." ".$fielddata['notes']." ".$fielddata['description']." ".$fielddata['content'];
			print_r($fielddata);
			echo "<br /><br />";
			$prof->import($theRestaurant, $fielddata);
		}
	}
	echo __FILE__." ".__FUNCTION__." ".__LINE__." takes ".printf("%.2f", (microtime(true) - $starting) )."s<br/>";
} // RetrieveAndSaveProfileData
function RetrieveAndSaveProfileImages($target_file, $imageCol, $nameCol, $mobileCol, $emailCol) {
	$starting = microtime(true);
	// if(!isset($_POST['submit'])) {
	// 	error_log("Not POST!");
	// 	return;
	// }
	// $target_file = sys_get_temp_dir() . '/' . basename($_FILES["fileToUpload"]["name"]);
	// // Check if file already exists
	// if (file_exists($target_file))
	// 	unlink($target_file);
	// // Allow certain file formats
	// if(pathinfo($target_file,PATHINFO_EXTENSION) != "xlsx") {
	//     echo "Sorry, only Excel files are allowed.<br/>";
	//     return;
	// }
	// // Check if $uploadOk is set to 0 by an error
	// if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file))
 //        echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded successfully!<br/>";
	// else {
	//     echo "Sorry, there was an error uploading your file!<br/>";
	//     return;
	// }
	$spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($target_file);
	echo $spreadsheet->getSheetCount()." sheets"."<br/>";
	$images = array();
	$prof = new WY_Profile();
	for ($i = 0; $i < $spreadsheet->getSheetCount(); ++$i) {
		$sheet = $spreadsheet->getSheet($i);
		$iterator = $sheet->getDrawingCollection()->getIterator();
		echo "Sheet ".$i.": "."<br/>";
	    while ($iterator->valid()) {
	    	if ($iterator->current() instanceof \PhpOffice\PhpSpreadsheet\Worksheet\Drawing) {
					$fielddata = array();
	    			$imageTL = \PhpOffice\PhpSpreadsheet\Cell::coordinateFromString($iterator->current()->getCoordinates());
	    			//echo "Image coordinate: ".print_r($imageTL)."<br/>";
	    			//$fielddata["name"] = $sheet->getCell(++$imageTL[0].$imageTL[1])->getValue();
	    			//$fielddata["mobile"] = $sheet->getCell(chr(ord($imageTL[0]) + 2).$imageTL[1])->getValue();
	    			//$fielddata["email"] = $sheet->getCell(chr(ord($imageTL[0]) + 7).$imageTL[1])->getValue();
					$fielddata["name"] = $sheet->getCell($nameCol.$imageTL[1])->getValue();
	    			$fielddata["mobile"] = $sheet->getCell($mobileCol.$imageTL[1])->getValue();
	    			$fielddata["email"] = $sheet->getCell($emailCol.$imageTL[1])->getValue();
	    		    $filename = $iterator->current()->getPath();
	                if (substr($filename, 0, 1) == '.')
	                    $filename = substr($filename, 1);
	                if (substr($filename, 0, 1) == '.' && substr($filename, 0, 2) != './')
	                    $filename = substr($filename, 1);
					$fielddata["mobile"] = WY_StringProcessor::FilterPhoneNumber($fielddata["mobile"]);
	                $html = "";
	                $fielddata["name"] = WY_StringProcessor::FilterValue($fielddata["name"]);
					//$fielddata["name"] = preg_replace("/\//", "", $fielddata["name"]);
					$names = WY_StringProcessor::SplitFullname($fielddata['name']);
					if ($names != null && !empty($names)) {
						$fielddata['salutation'] = $names['salutation'];
						$fielddata['firstname'] = trim(preg_replace("/ /", "_", $names['firstname']));
						$fielddata['lastname'] = trim(preg_replace("/ /", "_", $names['lastname']));
					}
					if (empty($fielddata['firstname']) && empty($fielddata['lastname'])) {
						$iterator->next();
						continue;
					}
	                // Convert UTF8 data to PCDATA
	                $filename= htmlspecialchars($filename);
	                //$imageData = base64_encode(file_get_contents($fielddata['filepath']));
	                // Format the image SRC:  data:{mime};base64,{data};
					//$src = 'data: '.mime_content_type($fielddata['filepath']).';base64,'.$imageData;			
					// Save the image into DB media table
					$pathinfo = pathinfo($filename);
					$basename = $fielddata['firstname'].'_'.$fielddata['lastname'].'.'.$pathinfo['extension'];
					$f1 = sys_get_temp_dir() . '/' . $fielddata['firstname'] . '_' . $fielddata['lastname'].'.o.'.$pathinfo['extension'];
					$upload = sys_get_temp_dir() . '/' . $basename;
					//echo "firstname: ".$fielddata['firstname'].", lastname: ".$fielddata["lastname"]."<br/>";
					//echo "f1: ".$f1.", upload: ".$upload."<br/>";
					file_put_contents($f1, file_get_contents($filename));
					if (!filesize($f1)) {
						error_log("Invalid filesize of ".$f1);
						$iterator->next();
						continue;
					}
					if (!resizecompress($f1, $upload, 128) || filesize($upload) > filesize($f1)) {
						echo ("Unoptimized resizecompress of ".$f1." failed!<br/>");
						error_log("Unoptimized resizecompress of ".$f1." failed!");
						if (!resizecompress($f1, $upload, 128, true)) {
							echo ("Optimized resizecompress of ".$f1." failed!<br/>");
							error_log("Optimized resizecompress of ".$f1." failed!");
						}
					}
					if (!file_exists($upload) || !filesize($upload)) {
						error_log("resizecompress of ".$f1." failed. Use the original file content!");
						$upload = $f1;
					}
					// if (filesize($upload) > filesize($f1)) {
					// 	echo ("resizecompress of ".$f1." failed! Use the original file content!<br/>");
					// 	error_log("Bad resizecompress of ".$f1.". Use the original file content!");
					// 	$upload = $f1;
					// }
					$url = '';
					$s3 = new S3(awsAccessKey, awsSecretKey, false, 's3.amazonaws.com', awsRegion, awsS3Version);
	        		$imageData = base64_encode(file_get_contents($upload));
					$src = 'data: '.mime_content_type($upload).';base64,'.$imageData;
					// Upload a file.
					$url = $s3->putObjectFile($upload, "static.weeloy.com", "images/profiles/".$basename, S3::ACL_PUBLIC_READ, array("Cache-Control" => "max-age=11"), "image/jpeg");
					if (isset($url) && !empty($url))
						$prof->updateProfilePicture($fielddata, $basename);
					else
						error_log("Failed to upload ".$basename." to AWS S3!!!");	
					// Echo out a sample image
					$imagesize = getimagesize($upload);
					echo $iterator->current()->getCoordinates().', Firstname: '.$fielddata["firstname"].', Lastname: '.$fielddata['lastname'].", ".$fielddata["mobile"].", ".$fielddata["email"].": ".$upload.', url: '.$url['ObjectURL'].' '.$imagesize[0].'x'.$imagesize[1].' : <img src="' . $src . '"><br/>';
					if (file_exists($f1))
						unlink($f1);
					if (file_exists($upload))
						unlink($upload);
	    	} else if ($iterator->current() instanceof \PhpOffice\PhpSpreadsheet\Worksheet\MemoryDrawing) {
	        	echo "MemoryDrawing @".\PhpOffice\PhpSpreadsheet\Cell::coordinateFromString($iterator->current()->getCoordinates())."<br/>";
	    		$imageTL = \PhpOffice\PhpSpreadsheet\Cell::coordinateFromString($iterator->current()->getCoordinates());
	    	    ob_start(); //  Let's start output buffering.
	            imagepng($iterator->current()->getImageResource()); //  This will normally output the image, but because of ob_start(), it won't.
	            $contents = ob_get_contents(); //  Instead, output above is saved to $contents
	            ob_end_clean(); //  End the output buffer.
	    		$images[$imageTL[1]] = base64_encode($contents);
	    		//echo "Image @".$iterator->current()->getCoordinates()."<br/>";
	    	} 
	        //$this->writeDrawing($objWriter, $iterator->current(), $i);
			$iterator->next();
	        //++$i;
	    }
	}		
	$spreadsheet->disconnectWorksheets();
	unset($spreadsheet);
	echo __FILE__." ".__FUNCTION__." ".__LINE__." takes ".printf("%.2f", (microtime(true) - $starting) )."s<br/>";
}
// RetrieveAndSaveProfileData('datamember.txt', 'SG_SG_R_DbBistroOysterBar', array("Flash Card", "Name", "Job Title / Company", "Contact Number", "Remarks", "Special Notes", "Description", "Email Address", "Mailing Address"), array("flash", "fullname", "company", "mobile", "remark", "notes", "description", "email", "adress"), array("fullname", "company", "mobile", "email", "remark", "notes", "description", "content"));
// RetrieveAndSaveProfileImages('B', 'C', 'E', 'J');

?>
