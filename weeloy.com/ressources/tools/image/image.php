<?php
require_once("lib/imagelib.inc.php");
require_once("lib/class.upload.inc.php");
$upload = new WY_Upload(sys_get_temp_dir());
$filePath = $upload->Upload();
function ProcessImage($filename) {
	$sizes = [128, 140, 180, 256, 270, 300, 325, 360, 450, 500, 512, 600, 700, 1024, 1440];
	$type = mime_content_type($filename);
 	echo "MIME content type: ".$type."<br/>";
	if (isset($filename) && !empty($filename) && file_exists($filename) && filesize($filename)) {
		$imageData = base64_encode(file_get_contents($filename));
		$imagesize = getimagesize($filename);
		$src = 'data: '.mime_content_type($filename).';base64,'.$imageData;
		echo 'Original: '.$imagesize[0].'x'.$imagesize[1].': <img src="' . $src . '"><br/>';
		for($i = 0; $i < count($sizes); $i++) {
			$pathinfo = pathinfo($filename);
			error_log("sys_get_temp_dir: ".sys_get_temp_dir());
			$f1 = sys_get_temp_dir().DIRECTORY_SEPARATOR.$pathinfo['filename']."_".$sizes[$i].".".$pathinfo['extension'];
			if (!resizecompress($filename, $f1, $sizes[$i]) || filesize($f1) > filesize($filename)) {
				echo ("Unoptimized resizecompress of ".$f1." failed!<br/>");
				error_log("Unoptimized resizecompress of ".$f1." failed!");
				if (!resizecompress($filename, $f1, $sizes[$i], true)) {
					echo ("Optimized resizecompress of ".$f1." failed!<br/>");
					error_log("Optimized resizecompress of ".$f1." failed!");
				}
			}
			if (!file_exists($f1) || !filesize($f1)) {
				echo "resizecompress of ".$f1." failed. Use the original file content!<br/>";
				error_log("resizecompress of ".$f1." failed. Use the original file content!");
				continue;
			}
			$imagesize = getimagesize($f1);
			$imageData = base64_encode(file_get_contents($f1));
			$src = 'data: '.mime_content_type($f1).';base64,'.$imageData;
			echo $f1.' '.$imagesize[0].'x'.$imagesize[1].' generated : <img src="' . $src . '"><br/>';
		}
	} else {
		echo "Invalid file: ".$filename."<br/>";
		error_log("Invalid file: ".$filename);
	}
}
ProcessImage($filePath);
?>s