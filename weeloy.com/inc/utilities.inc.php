<?php

require_once("lib/Browser.inc.php");

function print_javascript() {

    global $globalScript, $globalScriptOnload;

    echo "<script type='text/javascript'>" .
    "var currencyMaxObj = " . getmaxCurrency('jsstring') . ";" .
    "function resetcurrencyslider(vv) { var max, i, index = currencyMaxObj.indexOf(vv); if(typeof slideritempricing1 === 'undefined') return; var sliderAr = [slideritempricing1, slideritempricing2]; if(index < 0 || index >= currencyMaxObj.length - 1 ) max = 150; else max = currencyMaxObj[index+1]; for(i = 0; i < sliderAr.length; i++) if(sliderAr[i]) sliderAr[i].slider('option', 'max', max); return false; }\n" .
    "function ProcFormAlert(jForm, NextHtm, msg) { if(confirm(msg)) return ProcForm(jForm, NextHtm); return false; }\n" .
    "function ProcFormAlertDelete(jForm, NextHtm, msg, id) { id = '#' + id; val = $(id).val(); if(confirm(msg + ' \"' + val + '\" ?')) return ProcForm(jForm, NextHtm); return false; }\n" .
    "function ProcForm(jForm, NextHtm) { $('#theState').val(NextHtm); jForm.submit(); return (true); }\n" .
    "function togglecheckbox(id) { fullid = '#'+id; newval = ($(fullid).val() == 1) ? 0 : 1; $(fullid).val(newval); }\n" .
    "function closeAccordion(id) { $('#collapse'+id).collapse('hide'); }\n" .
    "var emptyMenuObj = {'morder':'', 'menu':'<br \/><br \/><br \/><br \/><br \/><br \/><br \/><br \/><br \/><br \/><br \/><br \/><br \/><br \/>'}; \n" .
    "function showmenu(name) { for(menuindex=0;menuindex<AllMenuVar.length && AllMenuVar[menuindex].name != name; menuindex++); if(menuindex >= AllMenuVar.length) mobj = emptyMenuObj; else mobj = AllMenuVar[menuindex]; $('#itemmorder').val(mobj.morder); data = mobj.menu.split('<br \/>'); $('#itemmenustitle').val(data[0]); for(i=1;i<6;i++) { $('#itemmenustitle'+i).val(data[((i-1)*2)+1]); $('#itemmenusdesc'+i).val(data[i*2]); }}\n " .
    "var emptyEventObj = {'morder':'', 'title':'', 'start':'', 'end':'', 'description':'', 'picture':''}; \n" .
    "function showevent(name) { for(eventindex=0;eventindex<AllEventVar.length && AllEventVar[eventindex].name != name; eventindex++); if(eventindex >= AllEventVar.length) mobj = emptyEventObj; else mobj = AllEventVar[eventindex]; $('#itemeventorder').val(mobj.morder); $('#itemeventtitle').val(mobj.title); $('#itemeventcity').val(mobj.city); $('#itemeventcountry').val(mobj.country); $('#itemeventdesc').val(mobj.description); if(mobj.start != '') { val = mobj.start.split('-'); vv = val[2] + '/' + val[1] + '/' + val[0]; } else vv = ''; $('#itemeventstart').val(vv); if(mobj.end != '') { val = mobj.end.split('-'); vv = val[2] + '/' + val[1] + '/' + val[0]; } else vv = ''; $('#itemeventend').val(vv); $('#itemeventpicture').val(mobj.picture); }\n " .
    "function new_window(url) { winp = window.open(url,'weeloy','toolbar=no,width=1050,height=750,menubar=no,scrollbars=yes,resizable=no,alwaysRaised=yes'); winp.moveTo(15,25); winp.resizeTo(1050, 750); winp.focus(); }\n " .
    "function offer_type_switch_controls(vv) { if(vv == 'listing'){  \$('#tmp_itemis_wheelable').prop('checked', false);  \$('#tmp_itemis_wheelable').closest('div').parent().hide(); }else{if(vv == 'wheel'){\$('#tmp_itemis_wheelable').closest('div').parent().show(); }}}\n " .
    "function status_switch_controls(vv) { "
    . "if(vv == 'demo_reference'){\$('#tmp_itemis_displayed').prop('disabled', false);\$('#tmp_itemis_wheelable').prop('disabled', false);\$('#tmp_itemis_bookable').prop('disabled', false); }\n"
    . "if(vv == 'comingsoon'){\$('#tmp_itemis_displayed').prop('disabled', false);\$('#tmp_itemis_wheelable').prop('disabled', false);\$('#tmp_itemis_bookable').prop('disabled', false);}\n "
    . "if(vv == 'active'){\$('#tmp_itemis_displayed').prop('checked', true);\$('#tmp_itemis_displayed').prop('disabled', true);\$('#tmp_itemis_wheelable').prop('disabled', false);\$('#tmp_itemis_bookable').prop('disabled', false);}\n "
    . "if(vv == 'pending'){\$('#tmp_itemis_displayed').prop('disabled', true);\$('#tmp_itemis_wheelable').prop('disabled', false);\$('#tmp_itemis_bookable').prop('disabled', false);}\n "
    . "if(vv == 'deleted'){\$('#tmp_itemis_displayed').prop('checked', false);\$('#tmp_itemis_displayed').prop('disabled', true);\$('#tmp_itemis_wheelable').prop('disabled', true);\$('#tmp_itemis_bookable').prop('disabled', true); }}\n "
    . $globalScript .
    "\n $(document).ready(function() {" . $globalScriptOnload . "}); " .
    "</script>";
}

function print_allinput($resdata, $dataobject, $platform) {

    global $ccardlist, $citylist, $countlist, $regionlist, $foodlist, $foodtype, $restaurantstatuslist, $promotionchoice, $restaurantoffertypelist, $memberstatuslist, $membertypelist, $tracktypelist;
    global $inputAr, $theRestaurant, $navbar, $theRestaurantTitle, $theMember, $theMemberEmail, $accountemail;
    global $globalScript, $globalScriptOnload, $picture_typelist, $PictureTypeSelection;
    global $imgdata, $globalMessage, $globalMessageType, $glbChefTypeList;

    $accordion_index = 0; // need to be zero
    $curcurrency = "SGD";

    if ($inputAr == "")
        return;

    $PictureTypeSelection = $picture_typelist[0];
    if (isset($_COOKIE['picture_type_cookie']))
        if (in_array($_COOKIE['picture_type_cookie'], $picture_typelist))
            $PictureTypeSelection = $_COOKIE['picture_type_cookie'];

    $action = $_SERVER['PHP_SELF'];
    $idForm = "idFormsQuery2";

	$perms = intval($_SESSION['user_backoffice']['member_permission']);
			
 	$priviledge = in_array($accountemail, array("richard@kefs.me", "richard.kefs@weeloy.com", "soraya.kefs@weeloy.com","vs.kala@weeloy.com", "khteh@weeloy.com")); 
 	$priviledge1 = in_array($accountemail, array("richard@kefs.me", "richard.kefs@weeloy.com", "soraya.kefs@weeloy.com", "matthew.fam@weeloy.com",  "khteh@weeloy.com")); 
    $resdata = new WY_restaurant();
    $resdata->getRestaurant($theRestaurant);
    $menudata = new WY_menu($theRestaurant);
    $eventdata = new WY_Event($theRestaurant);
    $mediadata = new WY_Media($theRestaurant);
    $clusterdata = new WY_Cluster($theRestaurant);
    $dataAr = array();

	$logo = $mediadata->getLogo($theRestaurant);

    $foodlist = getCuisine();

    $globalScript .= "function showimage(name, id) { if(name !=\"\") $(id).html('<img src=\"" . $mediadata->getPartialPath() . "'+name+'\" width=\"100\" style=\"margin: 0 0 0 100px;\">'); else $(id).html(''); }";

    if (!empty($globalMessage))
        echo "<div class='alert alert-$globalMessageType' role='alert'>$globalMessage</div>";

    echo "<form enctype='multipart/form-data' name='$idForm' id='$idForm' method='POST' action='$action'>
          <input type='hidden' value='$theRestaurant' id='theRestaurant' name='theRestaurant'>
          <input type='hidden' value='$theMember' id='theMember' name='theMember'>
          <input type='hidden' value='$theRestaurantTitle' id='theRestaurant' name='theRestaurantTitle'>
          <input type='hidden' value='$theMemberEmail' id='theMember' name='theMemberEmail'>
          <input type='hidden' value='$navbar' id='navbar' name='navbar'>";

    $cleartitle = false;

    $is_show = in_array($accountemail, array("richard@kefs.me", "richard.kefs@weeloy.com",  "soraya.kefs@weeloy.com", "philippe.benedetti@weeloy.com", "vs.kala@weeloy.com", "gultiga.lueskul@weeloy.com", "edwin.tan@weeloy.com","hody.leong@weeloy.com", "khteh@weeloy.com","amin.mahetar@weeloy.com","pengyee.lee@weeloy.com"));
    $multProduct = ($resdata->mutipleProdAllote()) ? true : false;
    $blocklist = ($resdata->checkBlockedList()) ? true : false;
    $pabxenable = ($resdata->checkPabx()) ? true : false;
    $ccmgt = ($resdata->checkCallcenterMgt() && ($perms & __MGT_CALL_CENTER__) != 0) ? true : false;
    $googleauth=($resdata->checkauthentication()) ? true : false;
    $smstemplate=($resdata->checksmstemplate()) ? true : false;
    $ccenterflg =  (($perms & __CALL_CENTER__) == 0) ? 0 : 1;  

    for ($i = 0; $i < count($inputAr); $i += 3) {
  
        if ($cleartitle && $inputAr[$i + 2] != "title") { // get rid off current title block, cleartitle is set un title, in the switch
            continue;
        }

        $cleartitle = false;

        $flgmultiple = 0;
        $label = $inputAr[$i];
        $reference = $inputAr[$i + 1];
        $type = $inputAr[$i + 2];
        $cn = strlen($reference);
        if (substr($reference, $cn - 2) == "[]") {
            $reference = substr($reference, 0, $cn - 2);
            if (array_key_exists($reference, $dataAr)) {
                $dataAr[$reference] = $flgmultiple = $dataAr[$reference] + 1;
            } else {
                $dataAr[$reference] = $flgmultiple = 1;
            }
        }

        $value = "";

        if (isset($dataobject[$reference])) {
            $value = $dataobject[$reference];
        }

        // $$reference;
        $arg = "";
        if (preg_match("/\([^\)]+\)/", $type, $match)) {
            $arg = preg_replace("/\(|\)/", "", $match[0]);
            $type = preg_replace("/\([^\)]+\)/", "", $type);
        }
      

       //if($type == "inputmultiple") error_log("TESTING $reference, $label, $type, $value, $arg\n");
        switch ($type) {
            case 'titlein':
            case 'title' :
                if ($label == "Call Center" && ($perms & __CALL_CENTER__) == 0  && ($perms & __RESTRICTED_CALL_CENTER__) == 0)
                    $cleartitle = true;
                else if($label == "Call Center Member" && $ccmgt == false )
                    $cleartitle = true;
                else if ($label == "Promotions" && $_SESSION['restaurant']['promotion'] != true)
                    $cleartitle = true;
                else if ($label == "Restaurant Booking Hours" && $resdata->enableBookhours() == false)
                    $cleartitle = true;
                else if ($label == "Booking Hours By Slot" && $resdata->bookingHourBySlot() == false)
                    $cleartitle = true;
                else if ($label == "Website Information" && $resdata->websitefields() == false)
                     $cleartitle = true;
                else if ($label == "Sponsor Wheel" && $resdata->sponsor() == false)
                     $cleartitle = true;
                else if ($label == "Coporate Wheel" && $resdata->corporateWheel() == false)
                    $cleartitle = true;
                else if(preg_match("/invoice|invoicing/i", $label) && !$priviledge)
                      $cleartitle = true;              	
               else if ($label == "TMS Now" && ($perms & __GROUP_TMS__) == 0)
                    $cleartitle = true;
               else if ($label == "Restaurant Analytics" && ($perms & __GROUP_REPORT__) == 0)
                    $cleartitle = true;
               else if ($label == "Restaurant Availability" && ($perms & __NO_ACCESS_AVAILABILITY__) != 0)
                    $cleartitle = true;
               else if($label == "PAYMENT" && $resdata->checkpaymentinvoice() == false )
                   $cleartitle = true;
               else if($label == "Contact information" && !$is_show)
                    $cleartitle = true;
               else if($label == "Booking cancel policy" && !$is_show)
                    $cleartitle = true;
               else if($label == "Section Configuration" && $multProduct == false )
                    $cleartitle = true;
               else if($label == "Blocklist" && $blocklist == false )
                    $cleartitle = true;
               else if($label == "PABX" && $pabxenable == false )
                    $cleartitle = true;
                else if($label == "Scan code" && ($perms & __2FACTAUTH__) == 0 )
                    $cleartitle = true;
               else if($label == "Sms Template Management" && $smstemplate == false )
                    $cleartitle = true;
               
               
                   
                else print_accordion($label, $type, ++$accordion_index);
                break;

            case 'input':
               if(($label == "Lunch Booking Deposit" || $label == "Diner Booking Deposit") && $resdata->checkbkdeposit() == 0)
                    break;
               if($label == "SMS ID" && $resdata->checksmsheader() == 0)
                    break;
 
               // hack readonly email if white label email on
                if($label == 'Email' && $resdata->emailWhitelabel())
                    $type = 'readonly';
                
                print_input($value, $reference, $label, $type, $arg);
                break;
                
            case 'inputmultiple':
            case 'readonly':
            case 'inputalphanum':
            case 'inputnum':
                print_input($value, $reference, $label, $type, $arg);
                break;

            case 'message':
                print_alert();
                break;

            case 'flagchecked':
            case 'checked':
                print_check($value, $reference, $label, $type, $arg);
                break;

            case 'textarea':
                if($label === "Deposit Cancel Tnc" && $resdata->checkbkdeposit() == 0)
                    break;
                
                print_textarea($value, $reference, $label, $type);
                break;
                
            case 'textareareadonly':
                  print_textarea($value, $reference, $label, $type);
                break;
             

            case 'cutoff-slider' :
            	if($resdata->dailyCutoff()) {
            		if($reference == "itemcutoffdiner")
            			break;
            		if($reference == "itemcutofflunch")
            			$label = "Daily Cutoff";            		
            		}
            case 'slider':
                print_slider($value, $reference, $label, $flgmultiple, $curcurrency);
                break;

            case 'dblslider':
                print_dblslider($value, $reference, $label);
                break;

            case 'pdatepicker':
            case 'datepicker':
                datepicker($reference, $label, $type);
                break;

            // $reference is the name of the variable, get its value
            case 'hiddenvalue':
                printf("<input type='hidden' value='%s' id='%s' name='%s'>", $value, $label, $label);
                break;

            case 'hidden':
                printf("<input type='hidden' value='%s' id='%s' name='%s'>", $reference, $label, $label);
                break;

            case 'flot':
                flot($reference, $label, $type, $theRestaurant);
                break;

            case 'close':
                print_button($accordion_index, $label, $idForm, $type);
                break;

            case 'select':
            case 'button':
            case 'submit':
            case 'delete':
            case 'irreversable' :
                print_button($reference, $label, $idForm, $type);
                break;

            case 'file':
                $label = $label . " (on '$PictureTypeSelection' category)";
                print_input($value, $reference, $label, $type, '');
                break;

                print_input($value, $reference, $label, $type, '');
                break;

            case 'routine':
                call_routine($reference, $theRestaurant, $label, $resdata, $logo, $perms); 
                break;

            case 'listrestaurant':
                print_list($resdata->getSimpleListRestaurant(), $value, $reference, $label, $type);
                break;

            case 'search':
                switch ($reference) {
                    case 'itemclustersearch' :
                        $category = $dataobject[$reference . "_category"];
                        $slave = (!empty($dataobject[$reference . "_slave"])) ? $dataobject[$reference . "_slave"] : "";
                        print_search_cluster($value, $reference, $label, $type, $category, $slave);
                        break;
                }

            case 'list' :
            case 'arlist' :
            case 'clist' :
            case 'elist' :
            case 'alist' :
            case 'mlist' :
            case 'plist' : // select the picture type
            case 'ilist' : // for image -> show selected image
            case 'olist' : //offer type list to hide controls on super admin
            case 'slist' : //restaurant status list to hide controls on super admin
            case 'flist' : //no reset
                switch ($reference) {

                    case 'itemmealslot':
                        print_list(array(1, 2, 3, 4, 5, 6, 7, 8, 9), $value, $reference, $label, $type);
                        break;

                    case 'itemgender':
                        print_list(array("Mr.", "Mrs.", "Ms"), $value, $reference, $label, $type);
                        break;

                    case 'itemclstype':
                    case 'itemclusttype':
                        print_list(array("MASTER", "SLAVE"), $value, $reference, $label, $type);
                        break;

                    case 'itemclustcategory':
                        print_list($clusterdata->getCategory(), $value, $reference, $label, $type);
                        break;

                    case 'itemclsparent':
                    case 'itemclustparent':
                    	$clusterdata->getListMasterCluster();
                    	if($clusterdata->result > 0) {
							print_list($clusterdata->clustname, $value, $reference, $label, $type);
							}
                        break;

                    case 'itemchef_type' :
                        print_list($glbChefTypeList, $value, $reference, $label, $type);
                        break;

                    case 'itemcurrency' :
                        if (!empty($value))
                            $curcurrency = $value;
                        print_list($resdata->getCurrencyList(), $value, $reference, $label, $type);
                        break;

                    case 'itemhotelname' :
                        print_list($resdata->getListHotelRestaurant(), $value, $reference, $label, $type);
                        break;

                    case 'itemeventcity':
                    case 'itemcity':
                        $citylist = array();
                        $cities = $resdata->getActiveCities(true, true);
//                        //print_r($cities);
                        foreach ($cities as $city) {
                            $citylist[] = $city['city'];
                        }
                        print_list($citylist, $value, $reference, $label, $type);
                        break;

                    case 'itemeventcountry':
                    case 'itemcountry':
                        $countlist = array();
                        $countries = $resdata->getCountyList(true, true);
                        foreach ($countries as $country) {
                            $countlist[] = $country['country'];
                        }
                        print_list($countlist, $value, $reference, $label, $type);
                        break;
                    case 'itemregion':
                        create_jsarea($resdata);
                        $cityisoCode = $resdata->getCityIsoCode($resdata->city);
                        
                        
                        $areaes = $resdata->getAreaList($cityisoCode);
        
                        foreach ($areaes as $area) {
   
                            if ($area['weeloy_name'] != ''){
                                $arealist[] = $area['weeloy_name'];
                            }else
                            if ($area['name'] != ''){
                                $arealist[] = $area['name'];
                            }
                        }
                        
                        print_list($arealist, $value, $reference, $label, $type);
                        break;

                    case 'itemcreditcard':
                        print_list($ccardlist, $value, $reference, $label, $type);
                        break;

                    case 'itemcuisine':
                        print_list($foodlist, $value, $reference, $label, $type);
                        break;

                    case 'itemimages' :
                        $imgAr = $mediadata->getRestaurantPictureType($theRestaurant, $PictureTypeSelection);
                        print_list($imgAr, $value, $reference, $label, $type);
                        break;

                    case 'itemlogo' :
                        $imgAr = $mediadata->getRestaurantPictureType($theRestaurant, "logo");
                        print_list($imgAr, $value, $reference, $label, $type);
                        break;

                    case 'itemchef_logo' :
                        $imgAr = $mediadata->getRestaurantPictureType($theRestaurant, "chef");
                        print_list($imgAr, $value, $reference, $label, $type);
                        break;

                    case 'itempicturetype':
                        print_list($picture_typelist, $PictureTypeSelection, $reference, $idForm, $type);
                        break;

                    case 'itemeventpicture':
                    case 'itemeventimages':
                        $imgAr = $mediadata->getEventPictureNames($theRestaurant);
                        print_list($imgAr, $value, $reference, $label, $type);
                        break;

                    case 'itemeventname':
                        $eventAr = $eventdata->readEventName();
                        print_list($eventAr, $value, $reference, $label, $type);
                        $globalScript .= "var AllEventVar = " . json_encode($eventdata->readAllEvents()) . ";";
                        break;

                    case 'itemmenusname':
                        $menuAr = $menudata->readMenusName();
                        print_list($menuAr, $value, $reference, $label, $type);
                        $globalScript .= "var AllMenuVar = " . json_encode($menudata->readAllMenus()) . ";";
                        break;

                    case 'itemmealtype':
                    case 'itemdfmealtype':
                        print_list($foodtype, $value, $reference, $label, $type);
                        break;

                    case 'itemdfminpers':
                        //$prelabel = "Min"; 
                       if(!isset($prelabel)) $prelabel = ""; 
                        $aall = 4;
                        if (!isset($value) || $value < 1 || $value > 4)
                            $value = 1;
                        $aa = array();
                        for ($kk = 1; $kk <= $aall; $kk++)
                            $aa[] = $kk;
                        print_list($aa, $value, $reference, $prelabel . $label, $type);
                        break;

                    case 'itemdfmaxpers':
                        //$prelabel = "Max"; 
                        //if($resdata->minforMax()) { $prelabel = "Max"; $aall = 31; }
                        $aall = 31;
                        if (!isset($value) || $value < 4)
                            $value = 10;
                        $aa = array();
                        for ($kk = 4; $kk < $aall; $kk++)
                            $aa[] = $kk;
                        print_list($aa, $value, $reference, $label, $type);
                        break;

                    case 'itemstatus':
                        print_list($restaurantstatuslist, $value, $reference, $label, $type);
                        break;

                    case 'itempromotionchoice':
                        print_list($promotionchoice, $value, $reference, $label, $type);
                        break;

                    case 'itemoffer_type':
                        print_list($restaurantoffertypelist, $value, $reference, $label, $type);
                        break;
                    case 'itemmemberstatus':
                        print_list($memberstatuslist, $value, $reference, $label, $type);
                        break;
                    case 'itemmember_type':
                        print_list($membertypelist, $value, $reference, $label, $type);
                        break;

                    case 'itemtracktype':
                        print_list($tracktypelist, $value, $reference, $label, $type);
                        break;

                    case 'itemwheelsponsor':
                        $sponsorlist = $mediadata->sponsorlist($theRestaurant);
                        print_list($sponsorlist, $value, $reference, $label, $type);
                        break;

                    case 'itempartlist':
                        $wheeldata = new WY_wheel($theRestaurant, '', '');
                        $extraslice = $wheeldata->extraslices();
                        $listpart = array();
                        $limit = count($extraslice);

                        for ($kk = 0; $kk < $limit; $kk++) {
                            $listpart[] = $extraslice[$kk]['offer'];
                        }

                        print_list($listpart, $value, $reference, $label, $type);
                        break;
                }

                break;
        }
    }

    $path = "../" . $resdata->internal_path . "?time=" . time();
    if ($platform == "backoffice")
        echo "</div><button type='button' class='btn btn-info ButtonBtn' onclick=\"javascript:new_window('$path');\">test $theRestaurantTitle</button><br /></form>";
}

function getdropstring($valAr, $reference, $label, $value, $noreset) {

    $str = "";
    $str .= "<div class='input-group'>";
    $str .= "<div class='input-group-btn' uib-dropdown >";
    $str .= "<button type='button' class='btn btn-info btn-sm' uib-dropdown-toggle >$label <span class='caret'></span></button>";

    $str .= "<ul class='dropdown-menu dropdown_$reference scrollable-menu' uib-dropdown-menu role='menu'>";

    for ($i = 0; $i < count($valAr); $i++) {
        $str .= "<li><a href='#' class='dummy_$reference'>" . $valAr[$i] . "</a></li>";
    }
    //}


    if (!isset($noreset) || $noreset == false) {
        $str .= "<li class='divider'></li>";
        $str .= "<li><a href='#' class='dummy_$reference'>Reset</a></li>";
    }
    $str .= "</ul>";
    $str .= "</div>";
    $str .= "<input type='text' value='$value' class='form-control input-sm' name='$reference' id='$reference' readonly>";
    $str .= "</div>";

    return $str;
}

function create_jsarea($resdata) {
    global $globalScript;
    $city = array('SG', 'BK', 'PK', 'HK', 'FR', 'KR');
    for ($i = 0; $i < count($city); $i++) {
        $areaes = $resdata->getAreaList($city[$i]);
        $arealist = array();

        $sep = "";
        $globalScript .= "\nvar areaList_" . $city[$i] . " = [ '', ";
        foreach ($areaes as $area) {
            if ($area['weeloy_name'] != '') {
                $globalScript .= $sep . " '" . clean_input($area['weeloy_name']) . "'";
                $sep = ",";
            }else{
            if ($area['name'] != '') {
                $globalScript .= $sep . " '" . clean_input($area['name']) . "'";
                $sep = ",";
            }
            }
        }
        $globalScript .= "];\n";

    }
    $globalScript .= "function changeArea(value) {
            var htm='';
            var i='';
            var valAr =areaList_SG;
            
            if(value==='Singapore'){valAr =areaList_SG;} else if(value=='Phuket'){var valAr =areaList_PK;} else if(value=='Bangkok'){ valAr =areaList_BK;}else if(value=='Hong Kong'){ valAr =areaList_HK;}

            for (i = 0; i < valAr.length; i++) {
                htm+='<li><a href=\"#\" class=\"dummy_itemregion\">'+valAr[i]+'</a></li>'
            }
            $('.dropdown_itemregion').html();
            $('.dropdown_itemregion').html(htm);
                if($('ul.dropdown_itemregion').length) $('a.dummy_itemregion').click(function() { var value= $(this).html(); $('#itemregion').val(value);$(this).closest('div').removeClass('open');  return false; });
            
        }";
          

    

    //$areaes =$resdata->getAreaList($city[$i]);
}

function getdropstring_array($valAr, $reference, $label, $value) {

    $str = "";
    $str .= "<div class='input-group'>";
    $str .= "<div class='input-group-btn' uib-dropdown>";
    $str .= "<button type='button' class='btn btn-info btn-sm' uib-dropdown-toggle >$label <span class='caret'></span></button>";

    $str .= "<ul class='dropdown-menu dropdown_$reference' uib-dropdown-menu >";
    for ($i = 0; $i < count($valAr); $i++) {
        $str .= "<li><a href='#' class='dummy_$reference'>" . $valAr[$i][0] . "</a></li>";
    }
    $str .= "<li class='divider'></li>";
    $str .= "<li><a href='#' class='dummy_$reference'>Reset</a></li>";
    $str .= "</ul>";
    $str .= "</div>";
    $str .= "<input type='text' value='$value' class='form-control input-sm' name='$reference' id='$reference' readonly>";
    $str .= "</div>";

    return $str;
}

function print_alert() {

    if (isset($_SESSION['message-error']) && strlen($_SESSION['message-error']) > 5)
        echo "<div class='alert alert-danger'>" . $_SESSION['message-error'] . "</div>";

    if (isset($_SESSION['message']) && strlen($_SESSION['message']) > 5)
        echo "<div class='alert alert-info'>" . $_SESSION['message'] . "</div>";
}

function print_accordion($label, $type, $accordion_index) {


    if ($accordion_index == 1)
        echo "<div class='panel-group' id='accordion'>";
    else
        echo "</div></div></div>";

    if ($label == "end")
        return;

    $collapse = ($type == 'title') ? "collapse" : "collapse in";
    echo "<div class='panel panel-default'>
		<div class='panel-heading'>
		<h4 class='panel-title'>
		<a data-toggle='collapse' data-parent='#accordion' href='#collapse$accordion_index'>
		<span class='badge' style='background-color: #428bca;'>$accordion_index</span> $label</a>
		</h4>
		</div>
		<div id='collapse$accordion_index' class='panel-collapse $collapse'>
		<div class='panel-body'>";
}

function print_input($value, $reference, $label, $type, $arg) {

	$onchange = " ";
    if ($type == 'file') {
        echo "<div class='form-group row'>
			<hr><label for='$reference' class='col-sm-4 control-label'>$label</label>
			<div class='col-sm-6'>
			<input class='btn' type='file' name='file' id='file'>
			</div>
			</div>";
        return;
    	}

    $attribute = ($type == "readonly") ? "readonly" : "";
    $info = "";
    if($type == "readonly") {
        $info = ($label == "Web Site") ? "(contact support to update this field must display the redirect link of the external campaign)" : "";
    	}

    if ($type == "inputmultiple") {
        $reference .= $arg;
    	if(preg_match("/\|/", $value)) {
    		$valueAr = explode("|", $value);
	    	$value = $valueAr[intval($arg) - 1];
	    	}
    	}
    
    if ($type == "inputalphanum") {
    	$onchange = " onchange='this.value=this.value.replace(/[^a-zA-Z0-9]/, \"\"); ";
    	}

    if ($type == "inputnum") {
    	$onchange = " onchange='this.value=this.value.replace(/[^0-9]/g, 0); if(this.value.length > 0) this.value = this.value.substr(0, $arg);' ";
    	}
    		
    echo "<div class='form-group row'>
        <label for='$reference' class='col-sm-2 control-label'>$label</label>
		<div class='col-sm-6'>
		<input type='text' value = '$value' class='form-control input-sm' id='$reference' name='$reference' placeholder='$label' $attribute $onchange > <h6 style='color:#9a9a9a;'>$info</h6> 
		</div>
		</div>";
}

function print_textarea($value, $reference, $label, $type) {
 
    $readonly = ($type == "textareareadonly") ? "readonly" : "";
    echo "<div class='form-group row'>
        <label for='$reference' class='col-sm-2 control-label'>$label</label>
		<div class='col-sm-6'>
        <textarea id='$reference' name='$reference' class='form-control' rows='4' placeholder='$label' $readonly>$value</textarea>
		</div>
		</div>";
}

function print_check($value, $reference, $label, $type, $arg) {

    $value = intval($value);

    if ($type == "flagchecked") {
        $reference .= $arg;
        $value = ($value & (1 << intval($arg) - 1)) ? 1 : 0;
    }

    if ($value == 1) {
        $input = "<input type='checkbox' value = '$value' class='form-control input-sm' id='tmp_$reference' name='tmp_$reference' checked onclick=\"togglecheckbox('$reference');\"><input type='hidden' name='$reference' id='$reference' value='1'>";
    } else {
        $input = "<input type='checkbox' value = '$value' class='form-control input-sm' id='tmp_$reference' name='tmp_$reference' onclick=\"togglecheckbox('$reference');\"><input type='hidden' name='$reference' id='$reference' value='0'>";
    }

    echo "<div class='form-group row'>
        <label for='$reference' class='col-sm-2 control-label'>$label</label>
		<div class='col-sm-6'>
		$input
		</div>
		</div>";
}

function print_button($reference, $label, $idForm, $type) {

    $sublabel = strstr($label, '|');
    if (!empty($sublabel)) {
        $sublabel = substr($sublabel, 1);
        $label = preg_replace("/\|.*/", "", $label);
    }

    $extra = "btn-primary customColor";
    $onclick = "onclick=\"ProcForm($idForm, '$reference');\"";

    if ($type == "submit")
        $extra = "btn-primary ";
    else if ($type == "delete" || $type == "irreversable") {
        $extra = "btn-danger ";
        $msg = "Are you sure that you want to $label ";
        $extr = (!empty($sublabel)) ? $sublabel : "";
        $onclick = "onclick=\"ProcFormAlertDelete($idForm, '$reference', '$msg', '$extr');\"";
    } else if ($type == "close")
        $onclick = "onclick=\"closeAccordion('$reference');\"";

    echo "<div class='form-group row'>
	<label for='' class='col-sm-2 control-label'>
	<input type='button' class='btn $extra' value='$label' $onclick>
	</label>
	<div class='col-sm-10'></div>
	</div>";
}

function print_list($listAr, $value, $reference, $label, $type) {
      
    global $globalScript, $globalScriptOnload; 
       
   
    if ($type == "wlist") {
        $globalScript .= "if($('ul.dropdown_$reference').length) $('a.dummy_$reference').click(function() { value= $(this).html(); if(value == 'Reset') value = ''; vv = ''; vv += value;  $('#$reference').val(vv);  $(this).closest('div').removeClass('open'); return false;});";
        return getdropstring($listAr, $reference, $label, $value, false);
    }

    if (is_array($listAr[0])) {
        echo "<div class='form-group row'>
		<label for='' class='col-sm-2 control-label'>$label</label>
		<div class='col-sm-6'>" .
        getdropstring_array($listAr, $reference, $label, $value, false) .
        "</div></div>";
    } else if ($type == "ilist") {
        echo "<div class='form-group row'>
		<label for='' class='col-sm-2 control-label'>$label</label>
		<div class='col-sm-6'>" .
        getdropstring($listAr, $reference, $label, $value, false) .
        "</div></div>" .
        "<div class='row'><label for='' class='col-sm-2 control-label'></label><div id ='" . $reference . "_image'></div></div><br />";
    } else if ($type == "plist") {
        //while(list($ll, $vv) = each($listAr)) $tmpAr[] = $ll;
        //$listAr = $tmpAr;
        $idForm = $label;
        $onclick = "onclick=\"ProcForm($idForm, '$reference');\"";
        echo "<div class='form-group row'>
		<label for='' class='col-sm-2 control-label'>
		<input type='button' class='btn btn-info btn-sm' value='Select' $onclick>
		</label>
		<div class='col-sm-6'>" .
        getdropstring($listAr, $reference, "Picture Type", $value, false) .
        "</div></div><div class='row'><br /><hr /></div>";
    } else if ($type == "flist") {
        $noreset = true;
        echo "<div class='form-group row'>
		<label for='' class='col-sm-2 control-label'>$label</label>
		<div class='col-sm-6'>" .
        getdropstring($listAr, $reference, $label, $value, true) .
        "</div></div>";
    } else if ($type == "arlist") {
        $noreset = true;
        echo "<div class='form-group row'>
		<label for='' class='col-sm-2 control-label'>$label</label>
		<div class='col-sm-6'>" .
        getdropstring($listAr, $reference, $label, $value, true) .
        "</div></div>";
    } else {
        echo "<div class='form-group row'>
		<label for='' class='col-sm-2 control-label'>$label</label>
		<div class='col-sm-6'>" .
        getdropstring($listAr, $reference, $label, $value, false) .
        "</div></div>";
    }

    if ($type == "clist") {
        $globalScript .= "if($('ul.dropdown_$reference').length) $('a.dummy_$reference').click(function() { value= $(this).html(); if(value == 'Reset') value = ''; vv = ''; vv += value;  $('#$reference').val(vv); $(this).closest('div').removeClass('open'); resetcurrencyslider(vv); return false;}); ";
        $globalScriptOnload .= "\n resetcurrencyslider('$value');";
    } else if ($type == "plist")
        $globalScript .= "if($('ul.dropdown_$reference').length) $('a.dummy_$reference').click(function() { value= $(this).html(); if(value == 'Reset') value = ''; vv = ''; vv += value;  $('#$reference').val(vv); resetcookie('picture_type_cookie', value, 7 * 24 * 60); $(this).closest('div').removeClass('open'); ProcForm($idForm, '$reference'); return false;});";
    else if ($type == "ilist")
        $globalScript .= "if($('ul.dropdown_$reference').length) $('a.dummy_$reference').click(function() { id = '#" . $reference . "_image'; value= $(this).html(); if(value == 'Reset') value = ''; vv = ''; vv += value;  $('#$reference').val(vv); showimage(value, id); $(this).closest('div').removeClass('open'); return false;});";
    else if ($type == "alist")
        $globalScript .= "if($('ul.dropdown_$reference').length) $('a.dummy_$reference').click(function() { vv = $('#$reference').val(); value= $(this).html(); if(vv != '' && vv.indexOf(value) != -1) return; if(vv != '') vv += '|'; vv += value;  if(value == 'Reset') value = vv = ''; $('#$reference').val(vv);  $(this).closest('div').removeClass('open'); return false;});";
    else if ($type == "mlist")
        $globalScript .= "if($('ul.dropdown_$reference').length) $('a.dummy_$reference').click(function() { value= $(this).html(); if(value == 'Reset') value = ''; vv = ''; vv += value;  $('#$reference').val(vv); showmenu(vv);  $(this).closest('div').removeClass('open'); return false;});";
    else if ($type == "elist")
        $globalScript .= "if($('ul.dropdown_$reference').length) $('a.dummy_$reference').click(function() { value= $(this).html(); if(value == 'Reset') value = ''; vv = ''; vv += value;  $('#$reference').val(vv); showevent(vv);  $(this).closest('div').removeClass('open'); return false;});";
    else if ($type == "olist")
        $globalScript .= "if($('ul.dropdown_$reference').length) $('a.dummy_$reference').click(function() { value= $(this).html(); if(value == 'Reset') value = ''; vv = ''; vv += value;  $('#$reference').val(vv); offer_type_switch_controls(vv); $(this).closest('div').removeClass('open'); return false;});";
    else if ($type == "slist")
        $globalScript .= "if($('ul.dropdown_$reference').length) $('a.dummy_$reference').click(function() { value= $(this).html(); if(value == 'Reset') value = ''; vv = ''; vv += value;  $('#$reference').val(vv); status_switch_controls(vv); $(this).closest('div').removeClass('open'); return false;});";
    else if ($type == "arlist") {
        $globalScript .= "if($('ul.dropdown_$reference').length) $('a.dummy_$reference').click(function() { value= $(this).html();changeArea($(this).html()); if(value == 'Reset') value = ''; vv = ''; vv += value;  $('#$reference').val(vv); status_switch_controls(vv); $(this).closest('div').removeClass('open'); return false;});";
        //$globalScript .= "if($('ul.dropdown_$reference').length) $('a.dummy_$reference').click(function() { value= $(this).html(); if(value == 'Reset') value = ''; vv = ''; vv += value;  $('#$reference').val(vv); status_switch_controls(vv); $(this).closest('div').removeClass('open'); return false;});";

       $globalScriptOnload .= "\n changeArea('$value');";
       $globalScriptOnload .= "if($('ul.dropdown_itemregion').length) $('a.dummy_itemregion').click(function() { var value= $(this).html();  $('#itemregion').val(value);  return false;});";

       //$globalScript .= "if($('ul.dropdown_itemregion').length) $('a.dummy_itemregion').click(function() { value= $(this).html();console.log(); if(value == 'Reset') value = ''; vv = ''; vv += value;  $('#itemregion').val(vv); status_switch_controls(vv); $(this).closest('div').removeClass('open'); return false;});";
    } else 
        $globalScript .= "if($('ul.dropdown_$reference').length) $('a.dummy_$reference').click(function() { value= $(this).html(); if(value == 'Reset') value = ''; vv = ''; vv += value;  $('#$reference').val(vv); $(this).closest('div').removeClass('open'); return false;});";
}

function getmaxCurrency($type) {
    $curmax = array('THB' => 6000, 'HKD' => 1000, 'KRW' => 200000, 'INR' => 10000, 'MYR' => 600); 
    if($type == "array")
    	return $curmax;
    else if($type == 'jsstring') {
    	$tt = $sep = "";
    	foreach($curmax as $key => $value) {
    		$tt .= $sep . "'" . $key . "', " . $value;
    		$sep = ", ";
    		}
    	return "[ " . $tt . " ]";
    	}
    else return "";
	}
	
function print_slider($value, $reference, $label, $flgmultiple, $currency) {

    global $globalScriptOnload;

    $curmax = getmaxCurrency('array'); 
    $ext = "";
    $valueslide = 0; // just set a default
    
    if ($flgmultiple > 0) {
        $valAr = explode(",", $value);
    }

    if ($reference == "itempricing" && $flgmultiple == 1) {
        $value = $valAr[$flgmultiple - 1];
        if (empty($value)) {
            $value = "0";
        }
        $divider = 1;
        $valueslide = $divider * $value;
        $min = 0;
        $max = (isset($curmax[$currency])) ? $curmax[$currency] : 150;
        $ext = $flgmultiple;
    } else if ($reference == "itempricing" && $flgmultiple == 2) {
        $value = $valAr[$flgmultiple - 1];
        if (empty($value)) {
            $value = "0";
        }
        $divider = 1;
        $valueslide = $divider * $value;
        $min = 0;
        $max = (isset($curmax[$currency])) ? $curmax[$currency] : 150;
        $ext = $flgmultiple;
    } else if ($reference == "itemlastorderlunch" || $reference == "itemcutofflunch") {
        $value = trim($value);
        if (!empty($value)) {
            $valueslide = floatval(strtr($value, ":", "."));
            $valueslide = floor($valueslide) + (($valueslide - floor($valueslide)) * 6 / 10);
        } else
            $value = "";

        $divider = 4;
        $valueslide = $divider * floatval($valueslide);
        $min = ($reference == "itemcutofflunch") ? 36 : 48;
        $max = 64;
    } else if ($reference == "itemlastorderdiner" || $reference == "itemcutoffdiner") {
        $value = trim($value);
        if (!empty($value)) {
            $valueslide = floatval(strtr($value, ":", "."));
            $valueslide = floor($valueslide) + (($valueslide - floor($valueslide)) * 6 / 10);
        } else
            $value = "";

        $divider = 4;
        $valueslide = $divider * floatval($valueslide);
        $min = ($reference == "itemcutoffdiner") ? 64 : 72;
        $max = 96;
    } else if ($reference == "itemlaptime") {
        if (empty($value))
            $value = "0";

        $divider = 2;
        $valueslide = $divider * floatval($value);
        $min = 0;
        $max = 12;
    } else if ($reference == "itemlagtime") {
        if (empty($value))
            $value = "0";

        $divider = 1;
        $valueslide = $divider * floatval($value);
        $min = 0;
        $max = 2;
    } else {
        if (empty($value)) {
            $value = "7.8";
        }
        $divider = 10;
        $valueslide = $divider * floatval($value);
        $min = 0;
        $max = 100;
    }

    $reference .= $ext;
    $datasliderid = "slider_$reference" . "Slider";

    /*
      <input id='slider_$reference' data-slider-id='$datasliderid' type='text' data-slider-min='" . $min . "' data-slider-max='" . $max . "' data-slider-step='1' data-slider-value='$value' style='width:150px;'/>
      $globalScriptOnload .= "\n $('#slider_$reference').slider({ formater: function(value) { value /= " . $divider . "; $('#$reference').val(value); return 'Value: ' + value; }  }); \n";
     */

    echo "<div class='form-group row'>
        <label for='$reference' class='col-sm-2 control-label'>$label</label>
		<div class='col-sm-2'>
		<input type='text' value ='$value' class='form-control input-sm' id='$reference' name='$reference' readonly style='border:0;width: 60px;'>
		</div>
		<div class='col-sm-3'>
		<div id='slider_$reference'></div>
		</div></div>";

    if ($reference == "itemlastorderdiner" || $reference == "itemlastorderlunch" || $reference == "itemcutoffdiner" || $reference == "itemcutofflunch")
        $globalScriptOnload .= "\n slider" . $reference . " = $('#slider_$reference').slider({  range: 'min', min: " . $min . ", max: " . $max . ", value: " . $valueslide . ", slide: function( event, ui ) { var h = Math.floor(ui.value / " . $divider . "), mn = (ui.value % " . $divider . ") * 15; if(mn == 0) mn = '00';  var newv = h + ':' + mn; if(ui.value === " . $min . ") newv = ''; $( '#$reference' ).val( newv );  } }) ;";
    else if ($reference == "itemlaptime")
        $globalScriptOnload .= "\n slider" . $reference . " = $('#slider_$reference').slider({  range: 'min', min: " . $min . ", max: " . $max . ", value: " . $valueslide . ", slide: function( event, ui ) { var vv = ui.value / " . $divider . "; if(ui.value == 11) vv = 24; if(ui.value == 12) vv = 48; $( '#$reference' ).val( vv );  } }) ;";
    else
        $globalScriptOnload .= "\n slider" . $reference . " = $('#slider_$reference').slider({  range: 'min', min: " . $min . ", max: " . $max . ", value: " . $valueslide . ", slide: function( event, ui ) { $( '#$reference' ).val( ui.value / " . $divider . " );  } }) ;";
}

function datepicker($reference, $label, $type) {

    global $globalScriptOnload;

    echo "<div class='form-group row'>
        <label for='$reference' class='col-sm-2 control-label'>$label</label>
		<div class='form col-sm-6'>
		<div class='input-group'>
		<input id='$reference' name='$reference' type='text' class='date-picker form-control input-sm' ' />
		<label for='$reference' class='input-group-addon btn'><span class='glyphicon glyphicon-calendar'></span></label>
		</div>
		</div>
		</div>";

    if ($type == "datepicker") {
        $globalScriptOnload .= "\n $('#" . $reference . "').datepicker({format: 'dd/mm/yyyy', startDate: '-60d', endDate: '0d', autoclose: true }); \n";
    } else if ($type == "pdatepicker") {
        $globalScriptOnload .= "\n $('#" . $reference . "').datepicker({format: 'dd/mm/yyyy', startDate: '+0d', endDate: '+90d', autoclose: true }); \n";
    }
}

function print_dblslider($value, $reference, $label) {

    global $globalScriptOnload;

    if (empty($value)) {
        $value = "110,150";
    }

    $tmpAr = explode(",", $value);
    if (count($tmpAr) < 2) {
        $value = "120,150";
        $tmpAr[1] = "150";
        $tmpAr[0] = "100";
    }
    $tmpAr[0] = intval($tmpAr[0]);
    $tmpAr[1] = intval($tmpAr[1]);
    if (intval($tmpAr[0]) <= 0 || intval($tempAr[1]) > 350) {
        $value = "130,150";
        $tmpAr[0] = 130;
        $tmpAr[1] = 150;
    }
    $avg = floor(($tmpAr[0] + $tmpAr[1]) / 2);

    $datasliderid = "slider_$reference" . "Slider";
    echo "<div class='form-group row'>
        <label for='$reference' class='col-sm-2 control-label'>$label</label>
		<div class='col-sm-2'>
		<input type='text' value ='$avg' class='form-control input-sm' id='tt_$reference' name='tt_$reference' placeholder='$label' readonly>
		</div>
		<div class='col-sm-4'>
		&nbsp;<b>€ 10</b><input type='text' id='$reference' name='$reference' class='span3' value='' data-slider-min='10' data-slider-max='350' data-slider-step='5' data-slider-value='[$value]'  style='width:150px;'/><b>€350</b>
		</div></div>";

    $globalScriptOnload .= "$('#$reference').slider({}); $('#$reference').on('slide', function(ev){ vv = $('#$reference').val().split(','); tt = Math.floor((parseInt(vv[0]) + parseInt(vv[1]))/2); $('#tt_$reference').val(tt); });";
}

function print_left_navbar($navbarAr) {
    global $theRestaurant, $theMember, $login_status, $theCuisine;
    $action = $_SERVER['PHP_SELF'];
    $current = (isset($_REQUEST['navbar'])) ? $_REQUEST['navbar'] : "";
    $size = count($navbarAr) * 35;
    if($size > 400) $size = 400;
    $height = ($login_status == _LOG_IN_) ? $size . "px" : "0";
    echo "<div class='row' style='margin-top:-25px;'>";
    echo "<div class='collapse navbar-collapse mybarnav' id='bs-example-navbar-collapse-1'>
		<form class='navbar-form' role='search' id='idFormnavbar' name='idFormnavbar' method='POST' action='$action'>
		<input type='hidden' value='HOME' id='navbar' name='navbar'>
		<input type='hidden' value='$theRestaurant' id='theRestaurant' name='theRestaurant'>
                <input type='hidden' value='$theMember' id='theMember' name='theMember'>
                <input type='hidden' value='$theCuisine' id='theCuisine' name='theCuisine'>
                <input type='hidden' value='en' id='emlang' name='emlang'>
		";

    echo "<div style='overflow-y:auto;height:" . $height . ";width:150px;text-align:left;'>";

    if ($login_status == _LOG_IN_) {
        for (; $val = current($navbarAr); next($navbarAr)) {
            if ($current != $val) {
                echo "<a href=\"javascript:setNavBar('$val');\"><h5 style='margin-bottom:1px;margin-left:1px;color:#282E5B'>$val</h5></a>";
            } else {
                echo "<a href=\"javascript:setNavBar('$val');\"><h5 class='fuchsia' style='margin-bottom:1px;margin-left:1px;font-family:dosis;font-size:16px;font-weight:bold;color:fuchsia;'>$val</h5></a>";
            }
        }
    }
    echo "</div></div>";

    $login_type = set_action_login_type();

    if (!isset($_SESSION['user_backoffice']['id'])) {
        $theRestaurant = NULL;
        $theMember = NULL;
        echo '<input type="button" style="color:black;" class="btn btn-default btn-sm" data-toggle="modal" data-target="#loginModal" value="Login" href="../modules/login/login.php?platform=' . $login_type . '">';
    }

    if (isset($_SESSION['user_backoffice']['id'])) {
        echo '<a type="button" style="margin-top:10px;color:black;" class="btn btn-default btn-sm" value="Logout" onclick="removecookie(cookiename);" id="logout_btn">Logout</a>';
    }

    /* echo "<br/><button type='button' class='btn btn-default' onclick=\"setNavBar('LOG');\">Login/Logout</button> */
    echo "<script>function setNavBar(value) { $('#navbar').val(value); idFormnavbar.submit(); } function setViewBar(value,lan) { $('#navbar').val(value); var lang = $('#itemlage').val();$('#emlang').val(lang);idFormnavbar.submit(); } </script>
		</form></div>";

//		<a href=\"javascript:setNavBar('REPORT1');\"><h5>REPORTS TABLE</h5></a>
}

function print_selectlist($resdata, $theRestaurant, $navbarTitle, $restrictedCountry = NULL) {
    // for list restaurant page
    global $navbar;

    $browser = new Browser();

    $action = $_SERVER['PHP_SELF'];

    $accountemail = $_SESSION['user_backoffice']['email'];
    $accounttype = $_SESSION['user_backoffice']['member_type'];
    $orderBy = (preg_match('/backoffice/', $action)) ? "title" : "restaurant";

    $resdata->getListRestaurant($accountemail, $accounttype, $orderBy, $restrictedCountry); // order
    $restaurantAr = $resdata->restaurant;
    $countryAr = $resdata->country;
    $limit = count($restaurantAr);
    $titleAr = $resdata->title;

	if(preg_match("/backoffice/", $_SERVER['PHP_SELF'], $match) && $limit > 14) {
		require_once("inc/print_restaurant.inc.php");
		return;
		}
		
	if(preg_match("/admin_weeloy/", $_SERVER['PHP_SELF'], $match) && $limit > 14) {
		require_once("../backoffice/inc/print_restaurant.inc.php");
		return;
		}
		

    echo "<form enctype='multipart/form-data' name='idFormsQuery' id='idFormsQuery' method='POST' action='$action'>
	  <input type='hidden' value='$navbar' id='navbar' name='navbar'>
      <table width='100%'><tr><td style='font-size:x-large;font-weight: bold;'><a href='$action'> $navbarTitle </a></td><td>
          </td></tr></table><hr>";

    // did not work with modal
    //         <a href='WeeloyUserManual/index.html' id='buttonterms' data-toggle='modal' data-target='#remoteModal' style='color:red;'>[ Help <span class='glyphicon glyphicon-book'></span> &nbsp;]</a>

    if ($limit < 15) {
        echo "<select onchange='idFormsQuery.submit();' id='theRestaurant' name='theRestaurant'>";

        for ($i = 0; $i < $limit; $i++)
            printf("<option value='%s' %s>%s</option>", $restaurantAr[$i], ($theRestaurant == $restaurantAr[$i]) ? "selected" : "", ($titleAr[$i] != "") ? $titleAr[$i] : $restaurantAr[$i]);


        echo "</select>";
    } else {
        $limit = count($resdata->restaurant);
        $countryAr = array();
        for ($i = 0; $i < $limit; $i++) {
            if (empty($resdata->country[$i]))
                $resdata->country[$i] = "unknown";
            if (!in_array($resdata->country[$i], $countryAr))
                $countryAr[] = $resdata->country[$i];

            if (empty($resdata->title[$i]))
                $resdata->title[$i] = $resdata->restaurant[$i];
            $restotitle[$resdata->country[$i]][] = $resdata->title[$i];
            $restoname[$resdata->country[$i]][] = $resdata->restaurant[$i];
            if ($resdata->restaurant[$i] == $theRestaurant)
                $theTitle = $resdata->title[$i];
        }

        sort($countryAr);

        if ($browser->isMobile() == false) {
            echo "
				<div class='input-group'>
				<div class='input-group-btn' uib-dropdown>
					<button type='button' id='itemdftime' class='btn btn-default btn' uib-dropdown-toggle >
						&nbsp;<i class='glyphicon glyphicon-cutlery'></i>&nbsp;
					</button>
					<ul class='dropdown-menu multi-level' uib-dropdown-menu role='menu' aria-labelledby='dropdownMenu'>
				";

            for ($i = 0; $i < count($countryAr); $i++) {
                $country = $countryAr[$i];
                echo "<li class='dropdown-submenu'>
					<a href='javascript:;' >" . $country . "</a>\n
					<ul class='dropdown-menu' uib-dropdown-menu>";
                for ($k = 0; $k < count($restoname[$country]); $k++) {
                    echo "<li><a href=\"javascript:$('.theRestaurant').val('" . $restoname[$country][$k] . "'); $('#restitle').val('" . $restotitle[$country][$k] . "'); idFormsQuery.submit(); \">" . $restotitle[$country][$k] . "</a></li>";
                }
                echo "</ul></li>";
            }

            echo "</ul>
				</div>
				<input type='text' value='$theTitle' class='form-control input' id='restitle' name='restitle' readonly>
				<input type='hidden' value='$theRestaurant' class='theRestaurant' id='theRestaurant' name='theRestaurant'>
			</div>";
        } else { // mobile
            echo "<select onchange='$(\"#restitle\").val(this[this.selectedIndex].text);; idFormsQuery.submit();' id='theRestaurant' name='theRestaurant' class='selectpicker' data-style='btn-primary' data-width='50%' data-live-search='true'>";
            for ($i = 0; $i < count($countryAr); $i++) {
                $country = $countryAr[$i];
                printf("<option %s>%s</option>", "disabled", " ");
                printf("<option %s>%s</option>", "disabled", $country);
                printf("<option %s>%s</option>", "disabled", "-------------------------------");
                for ($k = 0; $k < count($restoname[$country]); $k++) {
                    printf("<option value='%s' %s>%s</option>", $restoname[$country][$k], ($restoname[$country][$k] == $theRestaurant) ? "selected" : "", $restotitle[$country][$k]);
                }
            }
            echo "</select><br /><br /><input type='hidden' id='restitle' name='restitle'>";
        }
    }
    echo "<br />	<button type='submit' class='btn btn-info btn-sm customColor'><span class='glyphicon glyphicon-search'></span> &nbsp;Search</button></form><br /><br />";
}

function print_div_login_modal() {
    echo '
    <div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="myLoginModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">

          <div class="modal-body"></div>

        </div>
      </div>
    </div>';
    //<div class="modal-header"></div>
    //<div class="modal-footer"></div>
}

function print_search_cluster($value, $reference, $label, $type, $category, $slave) { // for list restaurant page
    global $globalScriptOnload;

    $clsdata = new WY_Cluster;
    $clsSubdata = new WY_Cluster;

    $dataAr = $categoryAr = $dataSubAr = array();
    $clsdata->getListMasterCluster(); // order
    if ($clsdata->result > 0) {
        $dataAr = $clsdata->clustname;
        $categoryAr = $clsdata->clustcategory;
        $theValue = $value;
    }

    $clsSubdata->getListSlaveCluster($value, $category); // order
    if ($clsSubdata->result > 0)
        $dataSubAr = $clsSubdata->clustname;

    // you can search in the select if more than 40 entries
    $datasearch = (count($dataSubAr) > 30) ? "data-live-search='true'" : "";

    $idForm = "idFormsQuery2";
    $theCategory = "";
    $reference_slave = $reference . "_slave";
    $reference_category = $reference . "_category";

//	$dataAr = $titleAr = array("Siang", "Pedro", "albert", "ramone", "christian", "eric", "raphael", "david", "jacob", "ryan", "joshua", "ethan", "william", "samuel", "james", "carole", "dominique", "rebecca");
//	$categoryAr = array("male", "male", "male", "male", "male", "male", "male", "male", "male", "male", "male", "male", "male", "male", "male", "female", "female", "female");
//	$dataSubAr = array("arianne", "henry", "carter", "gabriel", "olivier", "benjamin", "evelyn", "amelia", );

    $limit = count($dataAr);
    echo "<h4>" . $label . "</h4><hr />";

    echo "
		<div class='input-group'>
		<div class='input-group-btn' uib-dropdown >
			<button type='button' id='itemdftime' class='btn btn-default btn' uib-dropdown-toggle >
				&nbsp;<i class='glyphicon glyphicon-tower'></i>&nbsp;
			</button>
			<ul class='dropdown-menu multi-level' uib-dropdown-menu role='menu' aria-labelledby='dropdownMenu'>
		";

    $limit = count($dataAr);
    $categoryIndex = "";
    for ($i = 0; $i < $limit; $i++) {
        if ($categoryAr[$i] != $categoryIndex) {
            if ($categoryIndex != "")
                echo "</ul></li>";
            $categoryIndex = $categoryAr[$i];
            echo "<li class='dropdown-submenu'><a href='javascript:;' >" . $categoryIndex . "</a>\n<ul class='dropdown-menu' uib-dropdown-menu >";
        }
        if ($value == $dataAr[$i] && $category == $categoryAr[$i])
            $theCategory = "(" . $categoryAr[$i] . ")";
        echo "<li><a href=\"javascript:$('.Class" . $reference . "').val('" . $dataAr[$i] . "'); $('.Class" . $reference_category . "').val('" . $categoryAr[$i] . "'); $('#itemrestitle').val('" . $dataAr[$i] . "'); $('#" . $reference_slave . "').val(''); $idForm.submit(); \">" . $dataAr[$i] . "</a></li>";
    }

    echo "</ul>
		</div>
		<input type='text' value='$value    $theCategory' class='form-control input' id='itemrestitle' name='itemrestitle' readonly>
		<input type='hidden' value='$value' class='Class" . $reference . "' id='$reference' name='$reference'>
		<input type='hidden' value='$category' class='Class" . $reference_category . "' id='" . $reference_category . "' name='" . $reference_category . "'>
	</div><br />";

    $limit = count($dataSubAr);
    if ($limit > 0) {
        echo "<select onchange='$idForm.submit();' id='" . $reference_slave . "' name='" . $reference_slave . "' class='selectpicker  show-tick' data-style='btn-info' data-width='50%'  $datasearch >"; //data-live-search='true'  
        echo "<option data-content='select a slave CLUSTER'></option>";

        for ($i = 0; $i < $limit; $i++)
            printf("<option value='%s' %s>%s</option>", $dataSubAr[$i], ($slave == $dataSubAr[$i]) ? "selected" : "", $dataSubAr[$i]);

        echo "</select><br />";
    } else
        echo "<input type='hidden' value='' id='$reference_slave' name='$reference_slave'>";

    $globalScriptOnload = " $('.selectpicker').selectpicker(); ";

    echo "<br />	<button type='submit' class='btn btn-info btn-sm customColor'><span class='glyphicon glyphicon-search'></span> &nbsp;Search</button><br /><br />";
}

function print_search_bar_restau($theRestaurant, $restrictedCountry) { // for list restaurant page
    global $navbar;


    if (isset($_SESSION['message'])) {
        echo '<div class="alert alert-success"><a href="javascript:;"  class="close" data-dismiss="alert">'
        . '     &times;</a><strong></strong> ' . $_SESSION['message'] . '</div>';
        unset($_SESSION['message']);
    }

    $action = $_SERVER['PHP_SELF'];
    $resdata = new WY_restaurant();

    $accountemail = $_SESSION['user_backoffice']['email'];
    $accounttype = $_SESSION['user_backoffice']['member_type'];

    switch ($navbar) {
        case 'ADD_RESTAU':
            $title_btn = "Add";
            return;
            break;
        case 'UPD_RESTAU':
            $title_btn = "Select Restaurant";
            break;

        case 'DEL_RESTAU':
            $title_btn = "Remove";
            echo "<h2><a href='$action'> $title_btn Restaurant</a></h2>
		  <form enctype='multipart/form-data' name='idFormsQuery' id='idFormsQuery' method='POST' onsubmit='return validate(this);' action='$action'>
		  <input type='hidden' value='$navbar' id='navbar' name='navbar'>
                  <input type='hidden' value='delrestaurant' id='action' name='action'>
		  <select id='theRestaurant' name='theRestaurant'>";

            $resdata->getListRestaurant($accountemail, $accounttype, 'title', $restrictedCountry);
            $restaurantAr = $resdata->restaurant;
            $limit = count($restaurantAr);
            for ($i = 0; $i < $limit; $i++)
                printf("<option value='%s' %s>%s</option>", $restaurantAr[$i], ($theRestaurant == $restaurantAr[$i]) ? "selected" : "", preg_replace('/_/', ' ', $restaurantAr[$i]));

            echo "</select><br />
                    <button type='submit' class='btn btn-info btn-sm $title_btn'><span class='glyphicon glyphicon-$title_btn'></span> &nbsp;$title_btn</button>
                    </form><br />
                    <script>function validate(form) {
                        var res = confirm('Do you really want to delete the restaurant?');
                        return res;}</script>";
            return;
            break;

        default :
            $title_btn = "Search";
    }

    print_selectlist($resdata, $theRestaurant, $navbarTitle, $restrictedCountry);
}

function print_search_bar_member($theMember) { // for list restaurant page
    if (isset($_SESSION['message'])) {
        echo '<div class="alert alert-success"><a href="javascript:;"  class="close" data-dismiss="alert">'
        . '     &times;</a><strong></strong> ' . $_SESSION['message'] . '</div>';
        unset($_SESSION['message']);
    }

    global $navbar;

    $action = $_SERVER['PHP_SELF'];
    $dbLabel = array('email', 'firstname', 'name', 'member_type');

    $where = '';
    if (substr($navbar, 0, 3) == 'DEL') {
        $where = " WHERE status != 'deleted'";
    }

    $order = "order by member_type, firstname, name, email";
    $field = implode(",", $dbLabel);
    $dbLabel_cn = count($dbLabel);

    $data = pdo_multiple_select("SELECT $field FROM member $where $order");

    if (count($data) <= 0) {
        return $data;
    }

    foreach ($data as $row) {
        for ($i = 0; $i < $dbLabel_cn; $i++) {
            $tt = $dbLabel[$i] . "Ar";
            ${$tt}[] = $row[$dbLabel[$i]];
        }
    }

    switch ($navbar) {

        case 'ADD_MEMBER':
            $title_btn = "Add";
            //include 'inc/members/members.inc.php';
            return;
            break;

        case 'ASSIGN_MEMBER_RESTAURANTS_LIST':
            include 'app/components/members/restaurants_managers.inc.php';
//            include 'inc/members/restaurants_managers.inc.php';
            return;

        case 'ASSIGN_MEMBER_APPMANAGER_RESTAURANTS_LIST':
            include 'app/components/members/restaurant_AppManager.inc.php';
            return;


        case 'UPD_MEMBER':
        case 'DEL_MEMBER':
        default:
            include 'app/components/members/print_members.inc.php';
            return;
            break;
    }
}

function getGeoCity() {
    return $_SESSION['user']['search_city'];
}

function getGeoCountry() {
    return $_SESSION['user']['search_country'];
}

?>