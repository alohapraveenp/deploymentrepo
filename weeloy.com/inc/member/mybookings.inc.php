<?php


require_once("lib/class.member.inc.php");
require_once 'lib/class.booking.inc.php';
require_once("lib/class.media.inc.php");
require_once("lib/class.review.inc.php");
require_once("lib/wpdo.inc.php");

$member = new WY_Member();
$bookings = new WY_Booking();

$included_data['facebook_id'] = WEBSITE_FB_APP_ID;
$included_data['google_id'] = WEBSITE_GOOGLE_APP_ID;
$included_data['google_key'] = WEBSITE_GOOGLE_APP_KEY;
$included_data['fb_link'] = WEBSITE_FB_Link;


$member->getMember($_SESSION['user']['email']);


switch ($_GET['f']) {
    case 'today':
        $period = 'today';
        break;
    case 'past':
        $period = 'past';
        break;
    default:
        $period = 'coming';
        break;
}

$bookings->getUserBookings($member->email, array('include_reviews' => true, 'period' => $period, 'tracking'=>'no_white_label'));
 $media = new WY_Media();
 
$included_data['profile_picture'] = $media->getUserProfilePicture($_SESSION['user']['email']);
 $image_resto = array();
 $review = new WY_Review();

foreach ($bookings->bookings as $book) {
    
    $getreview = $review->getReview($book->confirmation,'','myreview');
    $book->canViewReview = count($getreview)>0 ? true : false ;
    
    $book->reviewGrade =$getreview['reviewgrade'];
    $book->bsBookid = $bookings->base64url_encode($book->bookid);
    $book->bsConfirm = $bookings->base64url_encode($book->confirmation);
    $book->dateInterval = $bookings->getDateInterval($book->rdate);

    if (!isset($image_resto[$book->restaurantinfo->restaurant])) {

        $image_resto[$book->restaurantinfo->restaurant] = $media->getDefaultPicture($book->restaurantinfo->restaurant);
    }
}
//$action = "'" . __ROOTDIR__ . "/modules/member/inviteemail.inc.php'";
//Kala created this  line for invite email actions

$actionOther = "modules/invite_email/invite_others.php";
$actionGmail = "modules/invite_email/invite_gmail.php";
$arrAction = array('invite_other' > $actionOther);
$included_data['bookings'] = $bookings->bookings;
$included_data['image_resto'] = $image_resto;
$included_data['active_filter'] = $_GET['f'];
$included_data['invite_action'] = $actionOther;
$included_data['invite_gmail'] = $actionGmail;
