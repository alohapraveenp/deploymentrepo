modalInstance = null;
token = '';
localcookie = 'weeloy_trad';

Array.prototype.priviledge = function() {
	if(this === null) return -1;

	var i, limit = this.length;
	for(i = 0; i < limit; i++) 
		if(this[i].name.toLowerCase() === 'admin')
			return i;
	return -1;
	}
	
app.constant('URL', 'data/');

app.filter('capitalize', function() {
  return function(str, scope) {
    if (str != null && str != "")
    str = str.toLowerCase();
    return str.substring(0,1).toUpperCase()+str.substring(1);
  }
});


app.service('interfaceModel', function ($http, URL) {

this.translatestart = [ 
			{"content_type" : "image", "title":"../images/admin/bo_slider_928x460.jpg", "id":"img" } 
			];
				
this.translatedata = [
			{"content_type" : "text", "title":"email" },
			{"content_type" : "inputsm", "type":"text", "title" : "Email", "topmargin" : "0", "id":"email" }
			];
					   
this.translatelist = [
			{"content_type" : "templatelist" }
			];

this.translatenew = [
			{"content_type" : "templatenew" }
			];
					   
this.login = [
			{"content_type" : "title", "title":"Login" },
			{"content_type" : "header1", "title":"Login", "title1":"Forgot password?", "title2": "Register", "arg1" : "forgot", "arg2" : "register" },
			{"content_type" : "input", "type":"text", "variable": "email", "title" : "Email", "topmargin" : "0", "glyphicon":"envelope", "id":"email" },
			{"content_type" : "password", "type":"password", "variable": "password", "title" : "Password", "topmargin" : "25px", "glyphicon":"lock", "id":"password" },
			{"content_type" : "showpass", "type":"checkbox", "variable": "showp", "title" : "Show password", "topmargin" : "5px", "glyphicon":"", "id":"showp" },
			{"content_type" : "submit", "type":"submit", "variable": "login", "title" : "Login", "topmargin" : "25px", "glyphicon":"", "id":"login" },
			{"content_type" : "facebookfooter", "type":"submit", "variable": "login", "title" : "Continue with Facebook", "topmargin" : "25px", "glyphicon":"", "id":"facebooklogin" }
			];

this.change = [
			{"content_type" : "title", "title":"Change Password" },
			{"content_type" : "header2", "title":"Change password", "title1":"", "title2": "", "arg1" : "", "arg2" : "" },
			{"content_type" : "input", "type":"text", "variable": "email", "title" : "Email", "topmargin" : "0", "glyphicon":"envelope", "id":"email" },
			{"content_type" : "password", "type":"password", "variable": "password", "title" : "Old Password", "topmargin" : "25px", "glyphicon":"lock", "id":"password" },
			{"content_type" : "password", "type":"password", "variable": "npassword", "title" : "New Password", "topmargin" : "25px", "glyphicon":"lock", "id":"npassword" },
			{"content_type" : "password", "type":"password", "variable": "rpassword", "title" : "Retype password", "topmargin" : "25px", "glyphicon":"lock", "id":"rpassword" },
			{"content_type" : "showpass", "type":"checkbox", "variable": "showp", "title" : "Show password", "topmargin" : "5px", "glyphicon":"", "id":"showp" },
			{"content_type" : "empty", "type":"text", "variable": "", "title" : "", "topmargin" : "10px;", "glyphicon":"", "id":"empty" },
			{"content_type" : "submitfooter", "type":"submit", "variable": "change", "title" : "Change your Password", "topmargin" : "10px", "glyphicon":"", "id":"change" }
			];

this.register = [
			{"content_type" : "title", "title":"Register" },
			{"content_type" : "header1", "title":"Register", "title1":"Login", "arg1" : "login", "title2": "Forgot password?", "arg2" : "forgot" },
			{"content_type" : "input", "type":"text", "variable": "email", "title" : "Email", "topmargin" : "0", "glyphicon":"envelope", "id":"email" },
			{"content_type" : "input", "type":"text", "variable": "firstname", "title" : "First Name", "topmargin" : "25px", "glyphicon":"user", "id":"firstname" },
			{"content_type" : "input", "type":"text", "variable": "lastname", "title" : "Last Name", "topmargin" : "25px", "glyphicon":"user", "id":"lastname" },
			{"content_type" : "input", "type":"text", "variable": "mobile", "title" : "Phone", "topmargin" : "25px", "glyphicon":"earphone", "id":"mobile" },
			{"content_type" : "password", "type":"password", "variable": "password", "title" : "New Password", "topmargin" : "25px", "glyphicon":"lock", "id":"password" },
			{"content_type" : "password", "type":"password", "variable": "rpassword", "title" : "Retype password", "topmargin" : "25px", "glyphicon":"lock", "id":"rpassword" },
			{"content_type" : "showpass", "type":"checkbox", "variable": "showp", "title" : "Show password", "topmargin" : "5px", "glyphicon":"", "id":"showp" },
			//{"content_type" : "empty", "type":"text", "variable": "", "title" : "", "topmargin" : "10px;", "glyphicon":"", "id":"empty" },
			{"content_type" : "submit", "type":"submit", "variable": "change", "title" : "Register", "topmargin" : "10px", "glyphicon":"", "id":"register" },
			{"content_type" : "facebookfooter", "type":"submit", "variable": "login", "title" : "Continue with Facebook", "topmargin" : "25px", "glyphicon":"", "id":"facebooklogin" }
			];

this.forgot = [
			{"content_type" : "title", "title":"Forgot Password" },
			{"content_type" : "header1", "title":"Forgot Password", "title1":"Login", "arg1" : "login", "title2": "Register", "arg2" : "register" },
			{"content_type" : "input", "type":"text", "variable": "email", "title" : "Email", "topmargin" : "0", "glyphicon":"envelope", "id":"email" },
			{"content_type" : "empty", "type":"text", "variable": "", "title" : "", "topmargin" : "10px;", "glyphicon":"", "id":"empty" },
			{"content_type" : "submitfooter", "type":"submit", "variable": "forgot", "title" : "Retrieve", "topmargin" : "10px", "glyphicon":"", "id":"forgot" }
			];

this.templateData = {
		  "modalTitle": "<title>{{content.title}}</title>",
		  "modalHeader1": "<meta content='width=device-width, initial-scale=1' name='viewport'><div class='modal-header modal-perso'><div class='panel-title'><span class='headertitle'>{{content.title}} &nbsp;</span><span class='headersubtitle'><a href='javascript:;' ng-click='$parent.open(content.arg1);' class='mwhite'>{{content.title1}}</a> | <a href='javascript:;' ng-click='$parent.open(content.arg2);' class='mwhite'>{{content.title2}}</a></span></div></div><div class='modal-body' style='background-color:white;'>",
		  "modalHeader2": "<meta content='width=device-width, initial-scale=1' name='viewport'><div class='modal-header modal-perso'><div class='panel-title'><span class='headertitle'>{{content.title}} &nbsp;</span><span class='headersubtitle'><a href='javascript:;' ng-click='$parent.open(content.arg1);' class='mwhite'>{{content.title1}}</a></span></div></div><div class='modal-body' style='background-color:white;'>",
		  "inputTemplate": "<div class='input-group' style='margin: {{content.topmargin}} 10px 0 10px;'><span class='input-group-addon'><i class='glyphicon glyphicon-{{content.glyphicon}}'></i></span><input class='form-control' type='text' placeholder='{{content.title}}' ng-model=$parent.user_data[content.id] id='{{content.id}}' name='{{content.id}}' ng-keypress='$parent.myKeyPress($event);'></div>",
		  "inputsmTemplate": "<input class='form-control' type='text' placeholder='{{content.title}}' ng-model=$parent.langdata[content.id] id='{{content.id}}' name='{{content.id}}'>",
		  "passwordTemplate": "<div class='input-group' style='margin: {{content.topmargin}} 10px 0 10px;'><span class='input-group-addon'><i class='glyphicon glyphicon-{{content.glyphicon}}'></i></span><input class='form-control passtype' type='password' placeholder='{{content.title}}' ng-model=$parent.user_data[content.id] id='{{content.id}}' name='{{content.id}}' ng-keypress='$parent.myKeyPress($event);'></div>",
		  "showpassTemplate": "<div class='input-group input-sm' style='margin: {{content.topmargin}} 10px 0 10px;'><label class='checkbox-inline' style='font-size:11px;'><input type='checkbox' ng-click='$parent.genShowPassword(content.id);' id='{{content.id}}' name='{{content.id}}'>{{content.title}}</label></div>",
		  "emptyTemplate": "<div class='input-group input-sm' style='margin: {{content.topmargin}} 10px 0 10px;'><label class='checkbox-inline'></label></div>",
		  "textTemplate": "<p>{{title}}</p>",
		  "imageTemplate": "<img ng-src='{{ $parent.user_data[content.id] }}'>",
		  "submitTemplate": "<div class='form-inline' style='margin: {{content.topmargin}} 10px; 10px 10px;'><div class='input-group'><a class='btn btn-info' href='javascript:;' ng-click='$parent.genSubmit(content.id,user_data)' id='{{content.id}}' name='{{content.id}}'>{{content.title}}</a></div></div></div>",
		  "submitFooterTemplate": "</div><div class='modal-footer'><div class='form-inline' style='margin-top: {{content.topmargin}} 10px 0 10px;'><div class='input-group'><a class='btn btn-info' href='javascript:;' ng-click='$parent.genSubmit(content.id)' id='{{content.id}}' name='{{content.id}}'>{{content.title}}</a></div></div>",
		  "facebookFooterTemplate": "<div class='modal-footer'><div id='facebook'><a id='loginBtn' class='btn btn-sm btn-social btn-facebook' title='With your Facebook account' href='javascript:;' ng-click='$parent.fblogin();'><i class='fa fa-facebook'></i>{{content.title}}</a><div id='status_fb'></div></div></div>",
		  "langTemplate": "<h3>{{$parent.langdata['topic']}}</h3><table class='table table-striped'><tbody><tr ng-repeat='items in $parent.langdataContent track by $index'><td><label class='control-label' for='items.id'>{{ items.content }}</label><input class='form-control' type='text' ng-model=$parent.$parent.langdataContent[$index].translated id='{{items.id}}' name='{{items.id}}'></td></tr></tbody></table><hr/><a class='btn btn-info' href='javascript:;' ng-click='$parent.saveLangue()'> Save the {{$parent.langdata['language'] | capitalize }} {{$parent.langdata['topic'] | capitalize}}</a>",
		  "newlangTemplate": "<h3>{{$parent.langdata['topic']}}</h3><table class='table table-striped'><tbody><tr ng-repeat='items in $parent.langdataContent track by $index'><td><label class='control-label' for='items.id'>[ {{ items.elem_name }} ] => {{ items.content }}</label></td></tr><tr ng-repeat='newf in $parent.newfield track by $index'><td><label class='control-label'>{{ $parent.$parent.newfield[$index].label }}</label><input class='form-control' type='text' ng-model=$parent.$parent.newfield[$index].value></td></tr></tbody></table><hr/><a class='btn btn-info' href='javascript:;' ng-click='$parent.savenewfield()'> Create new element in reference langue English for section {{$parent.langdata['topic'] | capitalize}}</a>"
		};
       
			
});

app.directive('contentItem', function ($compile) {
    var getTemplate = function (templates, contentType) {
		var titles = { title:'modaltitle', header1:'modalHeader1', header2:'modalHeader2', input:'inputTemplate', inputsm:'inputsmTemplate', text:'textTemplate', password:'passwordTemplate', showpass:'showpassTemplate', image:'imageTemplate', submit:'submitTemplate', submitfooter:'submitFooterTemplate', facebookfooter:'facebookFooterTemplate', templatelist:'langTemplate', templatenew:'newlangTemplate', empty:'emptyTemplate', blank:'blankTemplate'}

		if(contentType in titles)
			return templates[titles[contentType]];
		else alert(contentType + " not in template ");
		};

    var linker = function (scope, element, attrs) {
            element.html(getTemplate(scope.$parent.templateData, scope.content.content_type));
            $compile(element.contents())(scope);
		    };

    return {
        link: linker,
    	scope: {
            content: '=',
            myTemplates: '='
            }            
        
    };
});

app.controller('loginModalController',['$scope', '$rootScope', '$modal', '$modalInstance', '$localStorage', 'interfaceModel', 'FormControl', 'loginService', 'tradService', 'Facebook', function($scope, $rootScope, $modal, $modalInstance, $localStorage, interfaceModel, FormControl, loginService, tradService, Facebook){

	$scope.user_data = {};
	$scope.interfaceAll = { "login":"", "forgot":"", "change":"", "register":""};
	$scope.templateData = interfaceModel.templateData;
	
	$scope.interfaceLogin = $scope.interfaceAll.login = interfaceModel.login;
	$scope.interfaceForgot = $scope.interfaceAll.forgot = interfaceModel.forgot;
	$scope.interfaceChange = $scope.interfaceAll.change = interfaceModel.change;
	$scope.interfaceRegister = $scope.interfaceAll.register = interfaceModel.register;

	if(typeof $scope.curTemplate === "undefined")
		$scope.curTemplate = 'login';

	if(typeof $localStorage.langdata !== "undefined" && $localStorage.langdata != "")
		$scope.user_data.email = $localStorage.user_data_email;
		
	$scope.myKeyPress = function(keyEvent) {
	  if (keyEvent.which === 13)
	  	$scope.genSubmit($scope.curTemplate);
	};

	$scope.myrender = function(dataelements) {
      angular.forEach(dataelements, function(data) {
      	if(data.content_type == "input" || data.content_type == "password" || data.content_type == "textarea") {
      		//console.log(data.id + ' = ' + $('#' + data.id).val());
	        //angular.element(data.id).controller('ngModel').$render();
	        //bug in angular js concerning autocomplete
      		$scope.user_data[data.id] = $('#' + data.id).val();
	        }
      });
    };
    
	$scope.open = function(type) {
	
		size = 'sm';
		$modalInstance.close();

		template = 'loginBackoffice.html';		
		if(type == 'forgot') 
			template = 'forgotBackoffice.html';
		else if(type == 'change') 
			template = 'chgpassBackoffice.html';
		else if(type == 'register') 
			template = 'registerBackoffice.html';

		$scope.curTemplate = type;
		
		modalInstance = $modal.open({
			templateUrl: template,
			controller: 'loginModalController',
			size: size
		 })
	 };
	
	
	$scope.genShowPassword = function(id) {
		
		if ($('#password').attr('type') != 'text') {
			$('.passtype').attr('type', 'text');
			$('.showpass').addClass('show');
		} 
		else {
			$('.passtype').attr('type', 'password');
			$('.showpass').removeClass('show');
		}
		return false;
	}
	$scope.genHidePassword = function() {
		$('.passtype').attr('type', 'password');
		$('.showpass').removeClass('show');
	};

	$scope.genSubmit = function(type) {

		if(typeof $scope.interfaceAll[type] !== "undefined" && $scope.interfaceAll[type] != null)
			$scope.myrender($scope.interfaceAll[type]);
			
		
		switch(type) {
		 case 'login':
			$scope.user_data = FormControl.checkLogin($scope.user_data);
			if($scope.user_data.result > 0) {
				return loginService.login($scope.user_data.email, $scope.user_data.password).then(function(response){
					if(response.status == 0) {
						alert(response.errors);
						return;
						}
					console.log(response.data);
					token = response.data.token;
					user = $scope.$parent.usremail = $scope.user_data.email;
					setCookieOnline(cookiename, response.data.cookie, response.data.duration+5);
					setCookieOnline(localcookie, ":token="+token+":user="+user, response.data.duration+5);			
					$localStorage.user_data_email = $scope.user_data.email;		
					$scope.$parent.usremail = $scope.user_data.email;
					$scope.$parent.updatestate();
					$modalInstance.close();
					});
				}				
		 	break;
		 	
		 case 'change':
			$scope.user_data = FormControl.checkChange($scope.user_data);
			if($scope.user_data.result > 0) {
				return loginService.change($scope.user_data.email, $scope.user_data.password, $scope.user_data.npassword, token).then(function(response){
					console.log(response.data);
					if(response.data == '1')
						alert('Password has been changed');
					$modalInstance.close();
					});
				}				
		 	break;
		 	
		 case 'forgot':
			$scope.user_data = FormControl.checkForgot($scope.user_data);
                    if($scope.user_data.result > 0) {
				return loginService.forgot($scope.user_data.email, 't').then(function(response){
					console.log(response.data);
					if(response.data == '1')
						alert('A new Password has been sent to ' + $scope.user_data.email);
					$modalInstance.close();
					});
				}				
		 	break;
		 	
		 case 'register':
			$scope.user_data = FormControl.checkRegister($scope.user_data);
			if($scope.user_data.result > 0) {
				alert('Register not implemented yet. Check status instead');
				return loginService.checkstatus($scope.user_data.email, token).then(function(response){
					console.log(response.data);
					if(response.data == '1')
						alert('login');
					else alert('logout');
					});
				}				
		 	break;
		 	
		 default:
		 	alert("Invalid command "+type);
		 	return;
		 }
		 
		if($scope.user_data.result < 0) {
			alert($scope.user_data.msg);
			return;
			}
	};
	
	$scope.genHidePassword() ; 

// facebook login
	
	$rootScope.session = {};
    $rootScope.$on("fb_statusChange", function (event, args) {
        $rootScope.$apply();
    });
    $rootScope.$on("fb_get_login_status", function () {
        Facebook.getLoginStatus();
    });
    $rootScope.$on("fb_login_failed", function () {
        console.log("fb_login_failed");
    });
    $rootScope.$on("fb_logout_succeded", function () {
        console.log("fb_logout_succeded");
    });
    $rootScope.$on("fb_logout_failed", function () {
        console.log("fb_logout_failed!");
    });

    $rootScope.$on("fb_connected", function (event, args) {
        /*
         If facebook is connected we can follow two paths:
         The users has either authorized our app or not.

         ---------------------------------------------------------------------------------
         http://developers.facebook.com/docs/reference/javascript/FB.getLoginStatus/

         the user is logged into Facebook and has authenticated your application (connected)
         the user is logged into Facebook but has not authenticated your application (not_authorized)
         the user is not logged into Facebook at this time and so we don't know if they've authenticated
         your application or not (unknown)
         ---------------------------------------------------------------------------------

         If the user is connected to facebook, his facebook_id will be enough to authenticate him in our app,
         the only thing we will have to do is to post his facebook_id to 'php/auth.php' and get his info
         from the database.

         If the user has a status of unknown or not_authorized we will have to do a facebook api call to force him to
         connect and to get some extra data we might need to unthenticated him.
         */

        var params = {};

		$rootScope.session.facebook_id = args.facebook_id;
		$rootScope.session.facebook_token = args.facebook_token;
		FB.api('/me', function(response) {
			if(response.name != "") {
				console.log('get user data');
				console.log(response);
				$rootScope.session.email = response.email;
				$rootScope.session.first_name = response.first_name;
				$rootScope.session.last_name = response.last_name;
				$rootScope.session.name = response.name;
				$rootScope.session.timezone = response.timezone;
				ses = $rootScope.session;
            	params = {'email': ses.email, 'facebookid': ses.facebook_id, 'facebooktoken': ses.facebook_token };
				console.log(ses);
            	loginService.loginfacebook(params).then(function(response){
					console.log(response.data);
            		token = response.data.token;
					user = $scope.$parent.usremail = ses.email;
					setCookieOnline(cookiename, response.data.cookie, response.data.duration+5);
					setCookieOnline(localcookie, ":token="+token+":user="+user, response.data.duration+5);			
					$scope.$parent.updatestate();
					$modalInstance.close();
            		});
				}
			});

        if (args.userNotAuthorized === true) {
            //if the user has not authorized the app, we must write his credentials in our database
            console.log("user is connected to facebook but has not authorized our app");
            
    		}
        else {
            console.log("user is connected to facebook and has authorized our app");
            //the parameter needed in that case is just the users facebook id
            params = {'facebook_id':args.facebook_id};
            //authenticateViaFacebook(params);
        }

    });


    // button functions
    $scope.fbgetLoginStatus = function () {
        Facebook.getLoginStatus();
    };

    $scope.fblogin = function () {
        Facebook.login();
    };

    $scope.fblogout = function () {
        Facebook.logout();
        $rootScope.session = {};
        //make a call to a php page that will erase the session data
        //$http.post("php/logout.php");
    };

    $scope.fbunsubscribe = function () {
        Facebook.unsubscribe();
    }

    $scope.fbgetInfo = function () {
        FB.api('/' + $rootScope.session.facebook_id, function (response) {
            console.log('Good to see you, ' + response.name + '.');
        });
    };
 	  

}]);


app.controller('boHomeController',['$scope', '$rootScope', '$modal','interfaceModel', 'FormControl', 'loginService', 'tradService', 'Facebook', function($scope, $rootScope, $modal, interfaceModel, FormControl, loginService, tradService, Facebook){
	
	$scope.templateData = interfaceModel.templateData ;
	$scope.translatedata = interfaceModel.translatestart;
	$scope.language = $scope.langue = '';
	$scope.notEditingMode = true;
	$scope.adminrights = false;
	$scope.editcreatemode = '';
	$scope.topic = "";
	
	$scope.user_data = { "img" : "../images/admin/bo_slider_928x460.jpg" };
	$scope.langdata = { "topic" : "None", "language": '' };

	$scope.langdataContent = [];
	$scope.logaction = 'login';
	$scope.usremail = "";
	$scope.loggedin	= false;
	$scope.tablecontent = []; // { name: 'backoffice' }, { name: 'front' }, { name: 'booking' }, { name: 'callcenter' } ];
	$scope.newfield = [{ "label":"new category", "value":"" }, { "label":"new content", "value":"" }];

// save translation

	$scope.saveLangue = function() {
		var saving = '', val;
		for (key in $scope.langdataContent) {
			val = $scope.langdataContent[key].translated;
			if(val)
				$scope.langdataContent[key].translated = val.replace(/['\"]/g, '`');
			//else alert('undefined '+ key);
			saving += $scope.langdataContent[key].translated + ', ';
			}
			
		//alert(saving);
		params = { "email": $scope.usremail, "token" : token, "langue" : $scope.langue, "content" : $scope.langdataContent };
		tradService.tradwriteContent(params).then(function(response) {
			console.log('status write ' + response.status);
			if(response.status == "1") alert('Translation has been saved \n' + saving);
			else alert('Translation could not be saved ' + response.msg);
			});
		}

	$scope.savenewfield = function() {
		var saving = '', val;
		for (key in $scope.newfield) {
			val = $scope.newfield[key].value;
			if(typeof val !== 'undefined') {
				$scope.newfield[key].value = val.replace(/['\"]/g, '`');
				saving += $scope.newfield[key].value + ', ';
				}
			}
			
		//alert(saving);
		params = { "email": $scope.usremail, "token" : token, "zone" : $scope.section, "element" : $scope.newfield[0].value, "content": $scope.newfield[1].value };
		tradService.tradnewElement(params).then(function(response) {
			console.log('status write ' + response.status);
			if(response.status == "1") alert('new element has been saved \n' + saving);
			else alert('new element could not be saved ' + response.errors);
			});
		}

	$scope.process = function(section) {

		if(section === 'set edition mode' || section === 'set insertion mode') {
			$scope.translatedata = ($scope.translatedata === interfaceModel.translatelist) ? interfaceModel.translatenew : interfaceModel.translatelist;
			var limit = $scope.tablecontent.length;
			$scope.editinsertmode = (section === 'set edition mode') ? 'set insertion mode' : 'set edition mode';
			return;
			}
		
		$scope.newfield[0].value = $scope.newfield[1].value = "";
		
		$scope.langdata.topic = $scope.topic = $scope.section = section;
		$scope.langdata.language = $scope.language;
		params = { "email": $scope.usremail, "token" : token, "langue" : $scope.langue, "topic" : $scope.topic };
		tradService.tradreadContent(params).then(function(response) {
			if(response.status == 1) {
				$scope.langdataContent = response.data.row.slice(0);
				$scope.translatedata = ($scope.editinsertmode === 'set edition mode') ? interfaceModel.translatenew : interfaceModel.translatelist;
				// console.log($scope.langdataContent);
				}
			else {
				// console.log('TRAD RESPONSE', response);
				$scope.translatedata = interfaceModel.translatestart;
				$scope.notEditingMode = true;
				// alert(response.errors);
				if(response.status === -7) {
					if(confirm("do you want to create a new topic ?"))
						// alert("new translate");
						$scope.translatedata = interfaceModel.translatenew;
					}
				}
			});
		}	

	$scope.allowlang = ['en', 'fr', 'cn', 'hk', 'th', 'my', 'jp', 'kr', 'id', 'es', 'it', 'de', 'pt', 'ru', 'vi'];
	
	$scope.resetLang = function(lg) {
		if($scope.allowlang.indexOf(lg) > -1) {
			$scope.langue = lg;
			$scope.language = $scope.getLangName($scope.langue);
			if($scope.topic) $scope.process($scope.topic);
			}
		}
	
	$scope.getLangName = function(lang) {
		switch(lang) {
			case 'en': return 'english';
			case 'fr': return 'french';
			case 'cn': return 'simplifed Chinese';
			case 'hk': return 'traditional Chinese';
			case 'th': return 'thai';
			case 'my': return 'malay';
			case 'jp': return 'japanese';
			case 'kr': return 'korean';
			case 'id': return 'bahasa';
			case 'es': return 'spanish';
			case 'it': return 'italian';
			case 'de': return 'german';
			case 'pt': return 'portuguese';
			case 'ru': return 'russian';
			case 'vi': return 'vietnamese';
			default: return 'english';
			}
		};

// get table content left menu after sign in

	$scope.updatestate = function() {
		$scope.logaction =  'login';
		$scope.loggedin	= false;
		if(update_session_time() > 0) {
			if(typeof $scope.usremail == "undefined" || $scope.usremail == null || true) {
				$scope.usremail = getcookievalues(localcookie, "user");
				token = getcookievalues(localcookie, "token");
				}
			tradService.tradSections({ email: $scope.usremail, token: token }).then(function(response){
				var index;
				$scope.logaction =  'logout';
				$scope.loggedin	= true;
				$scope.adminrights = false;
				if(response.data == null) {
					alert($scope.usremail + ' is not registered for managing tables. Logout');
					return $scope.logout();
					}
				index = response.data.section.priviledge();
				if(index >= 0) {
					$scope.adminrights = true;
					response.data.section.splice(index, 1);
					}
					
				$scope.tablecontent = response.data.section;
				if($scope.adminrights) $scope.editcreatemode = 'set insertion mode';
				// check for online langue selection
				$scope.onlineselect = (response.data.langue === "mm");
				if(response.data.langue === "mm") response.data.langue = 'en';
				$scope.resetLang(response.data.langue);
				console.log(response.data, $scope.language);
				});
			}
			
		}

	if(update_session_time() > 0)
		$scope.updatestate();
				
	$scope.loginout = function (type) { 
		switch(type) {
			case 'login':
				$scope.login('login');
				break;
			
			case 'preferences':
				$scope.login('preferences');
				break;
			
			default:
			case 'logout':
				$scope.logout();
				break;				
			}
		}
	
	$scope.logout = function() {
		$scope.notEditingMode = true;
		$scope.translatedata = interfaceModel.translatestart;
		removecookie(cookiename);
		$scope.updatestate();
		if($scope.usremail != "")
			return loginService.logout($scope.usremail, token).then(function(response){ console.log('logout'); console.log(response); });
		}

	$scope.login = function (type) {
	
	$scope.curTemplate = 'login'; 
	templateUrl = 'loginBackoffice.html';
	if(type == 'preferences') {
		$scope.curTemplate = 'change';
		templateUrl = 'chgpassBackoffice.html';
		}
				
	size = 'sm';
	if(modalInstance != null)
		modalInstance.close();

	modalInstance = $modal.open({
		templateUrl: templateUrl,
		controller: 'loginModalController',
		size: size,
		scope: $scope
	});
	};
	
}])
