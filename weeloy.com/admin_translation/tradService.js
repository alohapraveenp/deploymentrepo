app.service('tradService',['$http',function($http){

	this.tradSections = function(params){
		return $http.post("../api/services.php/translation/gettranssection/", params)
    			.then(function(response) {return response.data;});
	};

	this.tradreadContent = function(params){
		return $http.post("../api/services.php/translation/gettransreadcontent/", params)
    			.then(function(response) {return response.data;});
	};

	this.tradwriteContent = function(params){
		return $http.post("../api/services.php/translation/gettranswritecontent/", params)
    			.then(function(response) {return response.data;});
	};

	this.tradnewElement = function(params){
		return $http.post("../api/services.php/translation/gettransnewelement/", params)
    			.then(function(response) {return response.data;});
	};


}]);

