<style type="text/css">
.fa {
  -moz-osx-font-smoothing: grayscale;
}

*,
:after,
:before {
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
}

.btn,
img {
  vertical-align: middle;
}

.col-xs-12,
.col-xs-2 {
  float: left;
}

article,
figure,
header,
nav,
section {
  display: block;
}

video {
  display: inline-block;
  vertical-align: baseline;
}

a {
  background-color: transparent;
}

button {
  margin: 0;
  font: inherit;
  color: inherit;
}

button {
  overflow: visible;
}

button::-moz-focus-inner {
  border: 0;
}

.btn,
.navbar-toggle {
  background-image: none;
}

body {
  background-color: #fff;
}

*,
:after,
:before {
  box-sizing: border-box;
}

html {
  font-size: 10px;
}

body {
  font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
  font-size: 14px;
  line-height: 1.42857143;
  color: #333;
}

button {
  line-height: inherit;
}

a {
  color: #337ab7;
  text-decoration: none;
}

h1,
h2 {
  font-family: inherit;
  font-weight: 500;
  line-height: 1.1;
  color: inherit;
}

h1,
h2 {
  margin-top: 20px;
  margin-bottom: 10px;
}

h1 {
  font-size: 36px;
}

h2 {
  font-size: 30px;
}

p {
  margin: 0 0 10px;
}

.text-center {
  text-align: center;
}

@media (min-width:768px) {
  .container {
    width: 750px;
  }
}

.container {
  margin-right: auto;
  margin-left: auto;
}

.container {
  padding-right: 15px;
  padding-left: 15px;
}

@media (min-width:992px) {
  .container {
    width: 970px;
  }
}

@media (min-width:1200px) {
  .container {
    width: 1170px;
  }
}

.col-sm-4,
.col-xs-12,
.col-xs-2 {
  position: relative;
  min-height: 1px;
  padding-right: 15px;
  padding-left: 15px;
}

.col-xs-12 {
  width: 100%;
}

.col-xs-2 {
  width: 16.66666667%;
}

@media (min-width:768px) {
  .col-sm-4 {
    float: left;
  }

  .col-sm-4 {
    width: 33.33333333%;
  }
}

.btn {
  display: inline-block;
  padding: 6px 12px;
  margin-bottom: 0;
  font-size: 14px;
  font-weight: 400;
  line-height: 1.42857143;
  text-align: center;
  white-space: nowrap;
  -ms-touch-action: manipulation;
  touch-action: manipulation;
  border: 1px solid transparent;
  border-radius: 4px;
}

.btn-lg {
  padding: 10px 16px;
  font-size: 18px;
  line-height: 1.3333333;
  border-radius: 6px;
}

.fade {
  opacity: 0;
}

.navbar {
  position: relative;
  min-height: 50px;
  margin-bottom: 20px;
  border: 1px solid transparent;
}

@media (min-width:768px) {
  .navbar {
    border-radius: 4px;
  }

  .navbar-header {
    float: left;
  }
}

.modal {
  overflow: hidden;
}

.container>.navbar-header {
  margin-right: -15px;
  margin-left: -15px;
}

.navbar-fixed-top {
  position: fixed;
  right: 0;
  left: 0;
  z-index: 1030;
}

.navbar-fixed-top {
  top: 0;
  border-width: 0 0 1px;
}

.navbar-brand {
  float: left;
  height: 50px;
  padding: 15px;
  font-size: 18px;
  line-height: 20px;
}

.navbar-brand>img {
  display: block;
}

@media (min-width:768px) {
  .container>.navbar-header {
    margin-right: 0;
    margin-left: 0;
  }

  .navbar-fixed-top {
    border-radius: 0;
  }

  .navbar>.container .navbar-brand {
    margin-left: -15px;
  }
}

.navbar-toggle {
  position: relative;
  float: right;
  padding: 9px 10px;
  margin-top: 8px;
  margin-right: 15px;
  margin-bottom: 8px;
  background-color: transparent;
  border: 1px solid transparent;
  border-radius: 4px;
}

.navbar-toggle .icon-bar {
  display: block;
  width: 22px;
  height: 2px;
  border-radius: 1px;
}

.navbar-toggle .icon-bar+.icon-bar {
  margin-top: 4px;
}

@media (min-width:768px) {
  .navbar-toggle {
    display: none;
  }
}

.navbar-default {
  background-color: #f8f8f8;
  border-color: #e7e7e7;
}

.navbar-default .navbar-brand {
  color: #777;
}

.navbar-default .navbar-toggle {
  border-color: #ddd;
}

.navbar-default .navbar-toggle .icon-bar {
  background-color: #888;
}

.panel {
  margin-bottom: 20px;
  background-color: #fff;
  border: 1px solid transparent;
  border-radius: 4px;
}

.panel-body {
  padding: 15px;
}

.panel-info {
  border-color: #bce8f1;
}

.modal {
  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  z-index: 1050;
  display: none;
  -webkit-overflow-scrolling: touch;
  outline: 0;
}

.modal.fade .modal-dialog {
  -webkit-transform: translate(0,-25%);
  -ms-transform: translate(0,-25%);
  -o-transform: translate(0,-25%);
  transform: translate(0,-25%);
}

.modal-dialog {
  position: relative;
  width: auto;
  margin: 10px;
}

.modal-content {
  position: relative;
  background-color: #fff;
  -webkit-background-clip: padding-box;
  background-clip: padding-box;
  border: 1px solid #999;
  border: 1px solid rgba(0,0,0,.2);
  border-radius: 6px;
  outline: 0;
  -webkit-box-shadow: 0 3px 9px rgba(0,0,0,.5);
  box-shadow: 0 3px 9px rgba(0,0,0,.5);
}

@media (min-width:768px) {
  .modal-dialog {
    width: 600px;
    margin: 30px auto;
  }

  .modal-content {
    -webkit-box-shadow: 0 5px 15px rgba(0,0,0,.5);
    box-shadow: 0 5px 15px rgba(0,0,0,.5);
  }
}

.container:after,
.container:before,
.navbar-header:after,
.navbar-header:before,
.navbar:after,
.navbar:before,
.panel-body:after,
.panel-body:before {
  display: table;
  content: " ";
}

.container:after,
.navbar-header:after,
.navbar:after,
.panel-body:after {
  clear: both;
}

.navbar-default {
  border-radius: 4px;
  background-repeat: repeat-x;
}

@-ms-viewport {
  width: device-width;
}

.navbar-default {
  background-image: -webkit-linear-gradient(top,#fff 0,#f8f8f8 100%);
  background-image: -o-linear-gradient(top,#fff 0,#f8f8f8 100%);
  background-image: -webkit-gradient(linear,left top,left bottom,from(#fff),to(#f8f8f8));
  background-image: linear-gradient(to bottom,#fff 0,#f8f8f8 100%);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffffff', endColorstr='#fff8f8f8', GradientType=0);
  filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);
  -webkit-box-shadow: inset 0 1px 0 rgba(255,255,255,.15),0 1px 5px rgba(0,0,0,.075);
  box-shadow: inset 0 1px 0 rgba(255,255,255,.15),0 1px 5px rgba(0,0,0,.075);
}

.navbar-brand {
  text-shadow: 0 1px 0 rgba(255,255,255,.25);
}

.navbar-fixed-top {
  border-radius: 0;
}

.panel {
  -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
  box-shadow: 0 1px 2px rgba(0,0,0,.05);
}

@font-face {
  font-family: FontAwesome;
  src: url(../fonts/fontawesome-webfont.eot?v=4.4.0);
  src: url(../fonts/fontawesome-webfont.eot?#iefix&v=4.4.0) format('embedded-opentype'),url(../fonts/fontawesome-webfont.woff2?v=4.4.0) format('woff2'),url(../fonts/fontawesome-webfont.woff?v=4.4.0) format('woff'),url(../fonts/fontawesome-webfont.ttf?v=4.4.0) format('truetype'),url(../fonts/fontawesome-webfont.svg?v=4.4.0#fontawesomeregular) format('svg');
  font-weight: 400;
  font-style: normal;
}

.fa {
  display: inline-block;
  font: normal normal normal 14px/1 FontAwesome;
  font-size: inherit;
  text-rendering: auto;
  -webkit-font-smoothing: antialiased;
}

.fa-facebook:before {
  content: "\f09a";
}

.sr-only {
  width: 1px;
  height: 1px;
  margin: -1px;
  overflow: hidden;
  clip: rect(0,0,0,0);
}

.sr-only {
  position: absolute;
  padding: 0;
  border: 0;
}

article,
figure,
header,
nav,
section {
  display: block;
}

img {
  border: 0;
}

video {
  display: inline-block;
}

html {
  font-family: sans-serif;
  -ms-text-size-adjust: 100%;
  -webkit-text-size-adjust: 100%;
}

body,
figure {
  margin: 0;
}

h1 {
  font-size: 2em;
  margin: .67em 0;
}

button {
  font-family: inherit;
  font-size: 100%;
  margin: 0;
}

button {
  line-height: normal;
}

button {
  text-transform: none;
}

button {
  -webkit-appearance: button;
}

button::-moz-focus-inner {
  padding: 0;
}

@font-face {
  font-family: proximanova;
  src: url(../fonts/ProximaNova-Reg_2-webfont.eot);
  src: url(../fonts/ProximaNova-Reg_2-webfont.woff2) format("woff2"),url(../fonts/ProximaNova-Reg_2-webfont.woff) format("woff"),url(../fonts/ProximaNova-Reg_2-webfont.svg) format("svg");
}

@font-face {
  font-family: proximanova-semibold;
  src: url(../fonts/ProximaNova-Sbold_2-webfont.eot);
  src: url(../fonts/ProximaNova-Sbold_2-webfont.woff2) format("woff2"),url(../fonts/ProximaNova-Sbold_2-webfont.woff) format("woff"),url(../fonts/ProximaNova-Sbold_2-webfont.svg) format("svg");
}

@font-face {
  font-family: ProximaNovaLightIt;
  src: url(../fonts/ProximaNovaLightIt.otf);
  src: url(../fonts/ProximaNovaLightIt.otf) format("otf"),url(../fonts/ProximaNovaLightIt.otf) format("otf"),url(../fonts/ProximaNovaLightIt.otf) format("otf");
}

@font-face {
  font-family: ProximaNovaLightIt;
  src: url(../fonts/ProximaNovaLightIt.otf);
}

.wrapper {
  min-height: 600px;
}

body .early_book_restaurant .ctn #best-restaurant .border {
  border: 1px solid #eee;
  margin-bottom: 30px;
  background-color: #fff;
  overflow: auto;
  position: relative;
}

body .early_book_restaurant .ctn #best-restaurant .border .custom-grid {
  width: 100%;
}

body .early_book_restaurant .ctn #best-restaurant .border .custom-grid .img-transparent {
  width: 100%;
  background-size: 100%!important;
}

body .early_book_restaurant .ctn #best-restaurant .border .custom-grid figure {
  position: relative;
}

body .early_book_restaurant .ctn #best-restaurant .border a:link {
  text-decoration: none!important;
}

.icon-weeloy_small {
  background-image: url(https://static2.weeloy.com/images/sprites/header_footer_sprites.png);
}

.icon-weeloy_small {
  background-position: -195px 0;
  width: 116px;
  height: 32px;
}

header .navbar-default {
  border: none;
  border-radius: 0;
  box-shadow: none;
  min-height: 0;
  background-color: #2aa8cf;
  background-image: none;
  margin-bottom: 0;
}

header .navbar-default .navbar-header .navbar-toggle {
  margin-top: 3px!important;
  margin-bottom: 3px!important;
  background: #1fa2e1!important;
  border: 1px solid transparent!important;
}

header .navbar-default .navbar-header .navbar-toggle .icon-bar {
  background-color: #fff;
}

header .navbar-default .navbar-header .navbar-brand {
  padding: 4px 0;
  margin-left: 0!important;
}

header .navbar-default .navbar-header .navbar-brand img {
  max-height: 100%;
  background-image: url(../images/header_footer_sprites.jpg);
  background-position: -195px 0;
  width: 116px;
  height: 32px;
  vertical-align: top;
}

header .navbar-default .navbar-brand {
  line-height: 40px;
  height: 40px;
  padding-top: 0;
}

header #loginModal .modal-dialog {
  width: 400px;
}

header #loginModal .modal-dialog .modal-content {
  border-radius: 2px;
}

header #loginModal .container_modal .mainbox {
  padding: 0;
}

header #loginModal .container_modal .mainbox .panel {
  margin-bottom: 0;
  text-align: center;
  border: 0 solid;
  border-radius: 2px;
}

header .panel {
  border-color: #5285a0!important;
}

header #loginModal .container_modal .mainbox .panel .panel-body .loginbox #facebook #loginBtn {
  border-radius: 2px;
}

header .container_modal {
  margin: 0!important;
  padding: 0!important;
  width: 100%!important;
}

header .or-spacer {
  margin-bottom: 35px;
  margin-top: 43px;
  position: relative;
}

header .or-spacer .mask {
  overflow: hidden;
  height: 20px;
}

header .or-spacer .mask:after {
  content: '';
  display: block;
  margin: -25px auto 0;
  width: 100%;
  height: 25px;
  border-radius: 125px/12px;
  box-shadow: 0 0 4px #000;
}

header .or-spacer span {
  width: 50px;
  height: 50px;
  position: absolute;
  bottom: 100%;
  margin-bottom: -25px;
  left: 50%;
  margin-left: -25px;
  border-radius: 100%;
  box-shadow: 0 2px 4px #999;
  background: #fff;
}

header .or-spacer span i {
  position: absolute;
  top: 4px;
  bottom: 4px;
  left: 4px;
  right: 4px;
  border-radius: 100%;
  border: 1px dashed #aaa;
  text-align: center;
  line-height: 40px;
  font-style: normal;
  color: #999;
}

body {
  overflow-y: scroll;
}

body .header-container {
  width: 100%;
  max-height: 612px;
  position: relative;
  height: 612px;
  padding-top: 40px;
  background: url(https://static2.weeloy.com/videos/banner/pluck/high/pluck.jpg);
  background-size: cover;
}

body .header-container video {
  position: absolute;
  bottom: 0;
  left: 0;
  right: 0;
  width: 100%;
}

body .early_book_restaurant {
  z-index: 1;
  background-color: #efefef;
}

body .early_book_restaurant .ctn .title_home_page {
  color: #2aa8cf;
  font-family: proximanova-semibold;
  font-size: 42px;
  padding-top: 110px;
  margin: 0;
}

body .early_book_restaurant .ctn .sub_title_home_page {
  font-size: 26px;
  font-family: ProximaNovaLightIt;
  margin-top: 0;
  text-align: center;
  color: #424242;
  margin-bottom: 48px;
}

body .early_book_restaurant .ctn #best-restaurant {
  margin-top: 45px;
  margin-bottom: 180px;
  overflow: auto;
  margin: auto;
}

body .early_book_restaurant .ctn #best-restaurant .border .img-absolute {
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: auto;
  overflow: hidden;
}

@media screen and (max-width:700px) {
  body .early_book_restaurant .container .title_home_page {
    margin-top: 80px;
  }
}

@media screen and (max-width:670px) {
  body .early_book_restaurant .container .title_home_page {
    margin-top: 80px;
  }
}

@media screen and (max-width:500px) {
  body .early_book_restaurant .container .title_home_page {
    margin-top: 0;
  }
}

@media screen and (max-width:1020px) {
  body .header-container {
    max-height: 450px;
  }
}

@media screen and (max-width:768px) {
  body .header-container {
    max-height: 350px;
  }

  body .early_book_restaurant .container .title_home_page {
    margin-top: 0!important;
  }
}

.btn-social {
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
}

.btn-social {
  position: relative;
  padding-left: 44px;
  text-align: left;
}

.btn-social>:first-child {
  position: absolute;
  left: 0;
  top: 0;
  bottom: 0;
  width: 32px;
  line-height: 34px;
  font-size: 1.6em;
  text-align: center;
  border-right: 1px solid rgba(0,0,0,.2);
}

.btn-social.btn-lg {
  padding-left: 61px;
}

.btn-social.btn-lg>:first-child {
  line-height: 45px;
  width: 45px;
  font-size: 1.8em;
}

.btn-facebook {
  color: #fff;
  background-color: #3b5998;
  border-color: rgba(0,0,0,.2);
}
</style>