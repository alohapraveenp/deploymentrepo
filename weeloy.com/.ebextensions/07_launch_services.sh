#!/bin/bash
LOGFILE="/var/log/deploy/07_launch_services.log"
PATH=$PATH:/usr/share/filebeat/bin
export PATH
/sbin/chkconfig --add filebeat >> ${LOGFILE} 2>&1
/etc/init.d/filebeat stop >> ${LOGFILE} 2>&1
/etc/init.d/filebeat start >> ${LOGFILE} 2>&1
