#!/bin/bash

cp /usr/share/zoneinfo/Asia/Singapore /etc/localtime

mkdir -p /var/log/deploy/
LOGFILE="/var/log/deploy/03_lib_conf.log"

mv .server/php/php-7.0_prod_bean.ini /etc/php-7.0.ini  >> ${LOGFILE} 2>&1
mkdir -p /etc/PHP_LIB
rm -rf /etc/PHP_LIB/conf
mv conf /etc/PHP_LIB  >> ${LOGFILE} 2>&1
rm -rf /etc/PHP_LIB/lib
mv lib /etc/PHP_LIB  >> ${LOGFILE} 2>&1