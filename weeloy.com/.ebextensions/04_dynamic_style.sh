#!/bin/bash

mkdir -p /var/log/deploy/
LOGFILE="/var/log/deploy/04_dynamic_style.log"

TIMESTAMP=$(date +%s)
grep -rl 'css/style.css"' ./ | xargs sed -i 's/css\/style.css\"/css\/style.css?'$TIMESTAMP'\"/g'  >> ${LOGFILE} 2>&1
# mv css/style.css css/style.css?${TIMESTAMP}  >> ${LOGFILE} 2>&1
