
<div class="container">
    <div class="row">
        <div class="col-md-12 left-sec">

<div>
<div id="Reviewbackoffice" ng-controller="MainControllerReview" ng-init="listingFlag=true; moduleName='review'" >

	<div id='listing' ng-show='listingFlag'>
    <div class="form-group"  style='margin-bottom:25px;'>
        <div class="col-md-4">
			<div class="input-group col-md-3">
				<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
				<input type="text" class="form-control input-sm" ng-model="searchText" ng-change="paginator.setPage(0);" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" style='width:200px;'> 
			</div>
        </div>
 		<div class="col-md-3">
			<span style='font-size:11px;'> Number of selected review: <strong> {{filteredReviews.length}} </strong></span>
		</div>
		<div class="col-md-2">
			<div class="btn-group" uib-dropdown >
			<button type="button" class="btn btn-default btn-xs" uib-dropdown-toggle aria-expanded="false" style='font-size:11px;'>Page Size<span class="caret"></span></button>
			<ul class="dropdown-menu" uib-dropdown-menu role="menu">
			<li ng-repeat="x in paginator.pagerange()" class='glyphiconsize'><a href ng-class="{ 'glyphicon glyphicon-ok glyphiconsize': x === paginator.getRowperPage() }" ng-click="paginator.setRowperPage(x)"> {{x}}</a></li>
			</ul>
			</div> 		
		</div>

		<div class="col-md-3">
		  <a href ng-click='extractSelection();' class="btn btn-success btn-xs" style='color:white;'><span class='glyphicon glyphicon-save'></span> &nbsp;Extract Selection</a>
		</div>
    </div><br/>
    

	<table width='100%' class="table table-condensed table-striped table-hover" style='font-size:12px;'>
		<thead>
		<tr><th ng-repeat="y in tabletitle"><tbtitle var="{{y.a}}" name='tabletitle' module='moduleName'/></th></tr>
		</thead>
		<tr ng-repeat="x in filteredReviews = (reviews | filter:searchText) | sizefilter:this | orderBy:predicate:reverse | slicefilter:paginator.getPage():paginator.getRowperPage():paginator.getItemCount() | limitTo:paginator.getRowperPage()" style='font-family:helvetica;font-size:11px;'>
			<td ng-repeat="y in tabletitle"><a href ng-click="view(x)" ng-style="{ 'text-align' : getAlignment(x[y.a]) }">{{x[y.a] | adatereverse:y.c }}</a></td>
		</tr>
		</tr><tr><td colspan='10'></td></tr>
	</table>
	<div ng-if="filteredReviews.length >= paginator.getRowperPage()" class="col-md-12" align="center" style='font-size:11px' ng-include="'inc/paginator.html'"></div>
</div>
	<div class="col-md-12" ng-hide='listingFlag'>
		<br />
		<a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a>
		<br />
		<table class='table-striped' style="margin: 0 0 150px 30px;font-size:13px;font-family: Roboto">		
			<tr ng-repeat="y in tableReviewContent"><td nowrap><strong>{{ y.b }} </strong></td><td> &nbsp; </td><td>{{ selectedItem[y.a] | adatereverse:y.c }}</td></tr>
			<tr><td nowrap><strong>Restaurant response: </strong></td><td> &nbsp; </td><td><textarea ng-model='selectedItem.response'  ng-change="cleaninput('response')" type='text' rows="4" cols="70"></textarea></td></tr>
			<tr><td>&nbsp;</td><td> &nbsp; </td><td></td></tr>
			<tr><td nowrap><a href class='btn btn-info btn-sm' ng-click="saveresponse()" style='color:white;'s><span class='glyphicon glyphicon-save'></span> &nbsp;Save Response</a></td><td></td></tr>
		</table>
	</div>
	
</div>
</div>


</div>
</div>
</div>
            
<script>


app.controller('MainControllerReview', ['$scope', '$http', 'bookService', 'extractService', function($scope, $http, bookService, extractService) {

	$scope.paginator = new Pagination(25);
  	$scope.selectedItem = null;
	$scope.email = <?php echo "'" . $email . "';"; ?>
	$scope.restaurant= <?php echo "'" . $theRestaurant . "';"; ?>
	$scope.tabletitle = [ {'a':'id', 'b':'index', c:'', q:'down', cc: 'black' }, {'a':'userid', 'b':'guest', c:'', q:'down', cc: 'black' }, {'a':'booking', 'b':'Confirmation', c:'', q:'down', cc: 'black' }, { alter:'datetime', 'a':'date', 'b':'Date', c:'date', q:'up', cc: 'orange' }, {'a':'food', 'b':'Food', c:'', q:'down', cc: 'black' }, {'a':'service', 'b':'Service', c:'', q:'down', cc: 'black' }, {'a':'ambiance', 'b':'Ambience', c:'', q:'down', cc: 'black' }, {'a':'price', 'b':'Price', c:'', q:'down', cc: 'black' }, {'a':'grade', 'b':'Grade', c:'', q:'down', cc: 'black' }, {'a':'is_responded', 'b':'Response', c:'', q:'down', cc: 'black' } ];
	$scope.bckups = $scope.tabletitle.slice(0);
	$scope.tableReviewContent = [ { a:'resto', b:'Restaurant Name' , c:''}, { a:'booking', b:'Booking Confirmation' , c:''}, { a:'guestname', b:'Guest Name' , c:''}, { a:'userid', b:'email' , c:''}, { a:'date', b:'Reservation Date' , c:'date'}, { a:'grade', b:'Grade of Review' , c:''}, { a:'food', b:'Grade of Food' , c:''}, { a:'service', b:'Grade of Service' , c:''}, { a:'ambiance', b:'Grade of Ambience' , c:''}, { a:'price', b:'Grade of Price' , c:''}, { a:'comment', b:'Comment' , c:''} ];
	
	$scope.getAlignment = bkgetalignment;

    $scope.predicate = "datetime";
	$scope.reverse = false;
	
	$scope.initorder = function() {
		$scope.tabletitle = $scope.bckups;
		$scope.predicate = "datetime";
		$scope.reverse = true;
		};
		
	$scope.reorder = function(item, alter) {
		alter = alter || "";
		if (alter !== "")  item = alter;
		$scope.reverse = ($scope.predicate === item) ? !$scope.reverse : false;
		$scope.predicate = item;
	};
	
	bookService.readReview($scope.restaurant).then(function(response) {
		$scope.reviews = response; 
		$scope.paginator.setItemCount($scope.reviews.length);
		$scope.initorder();
		});

	$scope.cleaninput = function(ll) {
		if(typeof $scope.selectedItem[ll] === 'string')
			$scope.selectedItem[ll] =  $scope.selectedItem[ll].replace(/\'|\"/g, '’');
		};
		
  	$scope.view = function(oo) {  
   		$scope.selectedItem = oo;
    	$scope.listingFlag = false;
  	};

  	$scope.backlisting = function() {
    	$scope.listingFlag = true;
  	};

  $scope.extractSelection = function() {
  
	var exportselect = $scope.filteredReviews;
	var i, j, u, data, maxlimit = 1500, limit, titleAr, contentAr, oo;
	var filename, cdate = new Date();

	titleAr = ["restaurant", "confirmation", "bookingdate", "user", "grade", "comments", "reponse"];
	contentAr = ["resto", "booking", "date", "userid", "grade", "comment", "response"];

	data = titleAr.join(",");
	limit = exportselect.length;		
	if (limit > maxlimit) limit = maxlimit;
	for (i = 0; i < limit; i++) {
		data += "\n";			
		u = exportselect[i];
		for (j = 0; j < contentAr.length; j++) 
			data += extractService.filter(u[contentAr[j]]) + ",";			
		}	

	filename = "reviews" + cdate.getDate() + cdate.getMonth() + cdate.getFullYear() + ".csv";
	extractService.save(filename, data); 				
  };


  $scope.saveresponse = function() {
  
  	if($scope.selectedItem == null) {
  		return alert("Wrong Data");
  		}

  	u = $scope.selectedItem;
	bookService.setresponseReview(u.resto, u.booking, u.response, $scope.email).then(function(response) { alert("Response to Review has been saved"); });
	$scope.backlisting();
	};

}]);

</script>
