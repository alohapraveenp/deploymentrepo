</form>
<div class="container">
	<div class="row">
		<div class="col-md-12 left-sec">
			<div id="reporting" ng-controller="AlloteController" ng-init="listingAllotFlag = true">
				<form>
					<div class="col-md-12" id='shortcut' ng-hide='listingAllotFlag'>
						<br/>
						<a href class='btn btn-info btn-sm' ng-click="backlisting()" style='color:white;'><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a>
						<br/>

						<div class='form-group row'>
							<label for='itemeventstart' class='col-sm-2 control-label'>Starting Date (dd-mm-yyyy)</label>
							<div class='form col-sm-6'>
								<div class='input-group'>
									<input type="text" class="form-control input-sm" uib-datepicker-popup="{{ formats[0] }}" ng-model="sday" ng-focus="open($event, 1)" ng-change="checkDate(sday, ntsart);" is-open="start_opened" min-date="minDate" max-date="maxDate" datepicker-options="dateOptions"
										   date-disabled="disabled(date, mode)" ng-required="true" close-text="Close"/>
									<label for='itemeventstart' class='input-group-addon btn'><span class='glyphicon glyphicon-calendar' ng-click="open($event, 1)"></span>
									</label>
								</div>
							</div>
						</div>
						<div class='form-group row'>
							<label for='itemeventend' class='col-sm-2 control-label'>Ending Date (dd-mm-yyyy)</label>
							<div class='form col-sm-6'>
								<div class='input-group'>
									<input type="text" class="form-control input-sm" uib-datepicker-popup="{{ formats[0] }}" ng-model="eday" ng-focus="open($event, 2)" ng-change="checkDate(eday, nend);" is-open="end_opened" min-date="minDate" max-date="maxDate" datepicker-options="dateOptions"
										   date-disabled="disabled(date, mode)" ng-required="true" close-text="Close"/>
									<label for='itemeventend' class='input-group-addon btn'><span class='glyphicon glyphicon-calendar' ng-click="open($event, 2)"></span></label>
								</div>
							</div>
						</div>

						<table style="margin: 0 0 30px 30px;font-size:13px;font-family:Roboto;font-weight:bold;">
							<tr>
								<td ng-repeat="x in fullweekday" width='100'>
									<label class="checkbox" for="{{x.id}}">
										<input type="checkbox" ng-model="weekdayselection[$index].data" ng-true-value="1" ng-false-value="0"/>{{ x.name }}
									</label>
								</td>
							</tr>
						</table>
						<hr/>
						<table style="margin: 0 0 30px 30px;font-size:13px;font-family:Roboto;font-weight:bold;">
							<tr>
								<td ng-repeat="x in mealtype" width='100'>
									<label class="checkbox" for="{{x.id}}">
										<input name='mealtypesel' type="checkbox" ng-model="mealtypeselection[$index].data" ng-true-value="1" ng-false-value="0">{{ x.name }}
									</label>
								</td>
							</tr>
						</table>
						<table class="table" style="margin: 0 0 30px 30px;font-size:13px;font-family:Roboto;font-weight:bold;">
							<tr>
								<td>
									<label class="input" for="Set a value">New Value <input type="input" ng-model="valuemeal" ng-change="changeMealvalue();"/></label>
									&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <span ng-hide="badDate">from selected dates {{ sday | showdate  }} to {{ eday | showdate }}</span>
								</td>
							</tr>
							<tr>
								<td>
									<a href='javascript:;' ng-click='setvalue(0);' class="btn btn-info btn-sm" style='color:yellow;width:300px;'><span class='glyphicon glyphicon-wrench'></span> &nbsp;Set the value - <b style='color:yellow;font-size:16px;'>{{valuemeal}}</b> - </a><br/>
								</td>
							</tr>
						</table>
						<!--
                        <pre ng-bind="weekdayselection | json"></pre>
                        <pre ng-bind="mealtypeselection | json"></pre>
                        -->
					</div>
					<div id='listing' ng-show='listingAllotFlag'>
						<input type="hidden" value="testing" ng-model='content' name="content" id="content"/>
						<div class="btn-group col-md-3"  >
							<div ng-if="yearange.length > 1" uib-dropdown>
								<button type="button" class="btn  btn-primary btn-sm" uib-dropdown-toggle aria-expanded="false" style='color:white;'><span class='glyphicon glyphicon-calendar'></span>&nbsp;&nbsp; select year &nbsp;&nbsp; <span class="caret"></span></button>
								<ul class="dropdown-menu" uib-dropdown-menu role="menu">
									<li ng-repeat="x in yearange"><a href ng-click="setalloteyear(x);">{{x}} <i ng-show='alloteyear === x' class="glyphicon glyphicon-ok text-primary"></i></a></li>
								</ul>
							</div>
						</div>
						<div class="col-md-1">
							<div class="input-group">
								<span><button type="button" class="btn btn-default" ng-click="open($event, 3)"><i class="glyphicon glyphicon-calendar"><span style='font-size:9px;'><br/>week</span></i></button></span>
								<input type="text" class="form-control" uib-datepicker-popup="{{ formats[0] }}" ng-change='setWeeks();' ng-model="aweek" is-open="week_opened" min-date="minDate" max-date="maxDate" datepicker-options="dateOptions" ng-required="true" close-text="Close"
									   style="width:0;opacity:0"/>
							</div>

						</div>
						<div class="col-md-1">&nbsp;</div>
						<div class="col-md-4">
							<div ng-if="multProd.length > 1" uib-dropdown >
								<button type="button" class="btn  btn-warning btn-sm" uib-dropdown-toggle aria-expanded="false" style='color:white;'>
									<span class='glyphicon glyphicon-list'></span>&nbsp;&nbsp; Select Product &nbsp;&nbsp; <span class="caret"></span>
								</button>
								<span style="font-size:11px;font-weight:bold;">&nbsp;  [ {{ bkproduct }} ]</span>
								<ul class="dropdown-menu" uib-dropdown-menu role="menu">
									<li ng-repeat="x in multProd"><a href ng-click="setalloteproduct(x);">{{x}} <i ng-show='bkproduct === x' class="glyphicon glyphicon-ok text-primary"></i></a></li>
								</ul>
							</div>
						</div>
						<div class="col-md-3">
							<a href ng-click='shortcut();' class="btn btn-info btn-sm" style='color:white;width:160px;'><span class='glyphicon glyphicon-wrench'></span> &nbsp;Short Cut</a><br/>
						</div>
						<div class="col-md-12">
							<table width='100%' class="table table-condensed table-striped" style='font-size:12px;margin:15px 0 0 0;'>
								<thead>
								<tr>
									<th>index</th>
									<th>lunch</th>
									<th>dinner</th>
								</tr>
								</thead>
								<tr ng-repeat="x in filteredAllote = (allote| filter:searchText) | slicefilter:paginator.getPage():paginator.getRowperPage():paginator.getItemCount() | limitTo:paginator.getRowperPage()">
									<td>{{x.date}}</td>
									<td><input ng-model='x.lunch' size='4'></td>
									<td><input ng-model='x.dinner' size='4'><span ng-if='twositting === 1' style='margin-left:100px;font-size:11px'> &nbsp;  &nbsp; <input type='button' ng-class="{ 'green': p1[x.index] === 1, 'red': p1[x.index] !== 1 }"
																																										   ng-value="((p1[x.index] === 1) ? 'close' : 'open') + ' P1'" ng-click='mealsitting(1, x.index);'> &nbsp; <input type='button'
																																																																						  ng-class="{ 'green': p2[x.index] === 1, 'red': p2[x.index] !== 1 }"
																																																																						  ng-value="((p2[x.index] === 1) ? 'close' : 'open') + ' P2'"
																																																																						  ng-click='mealsitting(2, x.index);'></span>
									</td>
								</tr>
								<tr>
									<td colspan='3'></td>
								</tr>
							</table>
						</div>
						<div class="col-md-1"><strong>WEEKS</strong></div>
						<div class="col-md-11" align="center" style='font-size:11px' ng-include="'inc/paginator.html'"></div>
						<div class="col-md-8"></div>
						<div class="col-md-4"><a href ng-click='saveresponse();' class="btn btn-success btn-sm" style='color:white;width:200px;'><span class='glyphicon glyphicon-save'></span> &nbsp;Save Allotment</a></div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<script>

	var token = <?php echo "'" . $_SESSION['user_backoffice']['token'] . "';"; ?>
	var currentday = <?php echo "'" . date("Y-m-d") . "'"; ?> ;
	var currentmonth = <?php echo date("m"); ?> ;
	var currentyear = <?php echo date("Y"); ?> ;
	var paginationOffset = <?php echo intval(date("W")) - 1; ?> ; // pagination at the current week
	var dayofyear = <?php echo intval(date("z")); ?> ;
	var resto = <?php echo "'" . $theRestaurant . "';"; ?>
	var multProd = <?php echo "'" . $multProduct . "';"; ?>
	var selectionAr;

	app.controller('AlloteController', ['$scope', '$http', 'bookService', function ($scope, $http, bookService) {

		$scope.usremail = <?php echo "'" . $_SESSION['user_backoffice']['email'] . "';"; ?>
		$scope.aweek = new Date();
		$scope.getAlignment = bkgetalignment;
		$scope.alloteyear = currentyear;
		$scope.start = ($scope.alloteyear === currentyear) ? dayofyear : 0;
		;
		$scope.twositting = <?php printf("%d", $twositting); ?>;
		// show year selection after juin for the following year
		$scope.yearange = [$scope.alloteyear];
		if (currentmonth > 3)		// used to be 5
			$scope.yearange.push($scope.alloteyear + 1);
		$scope.paginator = new Pagination(7);

		$scope.itemsPerPage = 7;
		$scope.restaurant = resto;
		$scope.weekday = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
		$scope.fullweekday = [{"name": "Sunday", "id": 0}, {"name": "Monday", "id": 1}, {"name": "Tuesday", "id": 2}, {"name": "Wednesday", "id": 3}, {"name": "Thursday", "id": 4}, {"name": "Friday", "id": 5}, {"name": "Saturday", "id": 6}];
		$scope.month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
		$scope.mealtype = [{"name": "Lunch", "id": 0}, {"name": "Dinner", "id": 1}];

		$scope.nstart = $scope.nend = $scope.valuemeal = "";
		$scope.mealtypeselection = [{"id": "0", "data": "0"}, {"id": "1", "data": "0"}];
		$scope.weekdayselection = [{"id": "0", "data": "0"}, {"id": "1", "data": "0"}, {"id": "2", "data": "0"}, {"id": "3", "data": "0"}, {"id": "4", "data": "0"}, {"id": "5", "data": "0"}, {"id": "6", "data": "0"}];

		$scope.badDate = true;
		$scope.sday = $scope.eday = new Date();	// format javascript => day-month-year
		$scope.lastweeklunch = [];	// for overlap week, last week
		$scope.lastweekdinner = [];	// for overlap week, last week
		$scope.data2sitting = [];
		$scope.p1 = [];
		$scope.p2 = [];

		$scope.multProd = [];
		if (multProd !== "") {
			$scope.multProd = multProd.split("|");
			//$scope.multProd.splice(0, 0, " ");
		}
		$scope.bkproduct = "";
		if ($scope.multProd.length > 1)
			$scope.bkproduct = $scope.multProd[1];
		else if ($scope.multProd.length > 0)
			$scope.bkproduct = $scope.multProd[0];

		$scope.setalloteproduct = function (val) {
			$scope.bkproduct = val;
			$scope.setalloteyear($scope.alloteyear);
		}

		$scope.getAlloteData = function (restaurant, year, product) {
			bookService.readAlloteYear(restaurant, year, product).then(function (response) {
				var i, start, offset = paginationOffset;
				var end = (($scope.alloteyear % 4 === 0 && $scope.alloteyear % 100 !== 0) || $scope.alloteyear % 400 === 0) ? 365 : 364;
				var today = new Date();
				var lunch = response.data.lunch.split('|');
				var dinner = response.data.dinner.split('|');

				$scope.allote = [];
				start = $scope.start;
				if ($scope.alloteyear > currentyear) {
					$scope.start = start = 0;
					offset = 0;
					today = new Date($scope.alloteyear, 0, 1, 0, 0, 0);
				}

				for (i = start; i <= end; i++) {
					obj = {};
					obj.index = i;
					obj.date = $scope.weekday[today.getDay()] + ', ' + $scope.month[today.getMonth()] + ' ' + today.getDate() + ', ' + today.getFullYear();
					if (i >= 0) {
						obj.lunch = lunch[i];
						obj.dinner = dinner[i];
					}
					else {
						obj.lunch = $scope.lastweeklunch[i - start];
						obj.dinner = $scope.lastweekdinner[i - start];
					}
					$scope.allote.push(obj);
					today.setDate(today.getDate() + 1);
				}

				if ($scope.lastweeklunch.length === 0) // do it only once
					for (i = 365 - 7; i < 365; i++) {
						$scope.lastweeklunch.push(lunch[i]);
						$scope.lastweekdinner.push(dinner[i]);
					}
				$scope.paginator.setItemCount($scope.allote.length);
				$scope.paginator.setPageOffset(offset);
				$scope.paginator.setPage(0);
			});
		};

		$scope.setalloteyear = function (val) {
			var dd, de;
			$scope.alloteyear = val;
			$scope.start = ($scope.alloteyear === currentyear) ? dayofyear : 0;

			if ($scope.alloteyear === currentyear) {
				$scope.minDate = new Date();
				dd = new Date();
				de = new Date();
				de.setTime(de.getTime() + (24 * 3600 * 1000));	// 1 day
			}
			else {
				$scope.minDate = new Date(val, 0, 1, 0, 0, 0);
				dd = new Date(val, 0, 1, 0, 0, 0);
				de = new Date(val, 0, 2, 0, 0, 0);
			}
			$scope.maxDate = new Date(val, 11, 31, 0, 0, 0);
			$scope.nstart = dd.getDateFormat('-');
			$scope.nend = de.getDateFormat('-');
			$scope.sday = dd;
			$scope.eday = de;
			$scope.badDate = false;

			if ($scope.twositting === 1)
				bookService.readtwositting($scope.restaurant, $scope.alloteyear).then(function (response) {
					if (response.status > 0) {
						$scope.data2sitting = response.data.split("|");
						for (var i = 2; i < $scope.data2sitting.length; i += 2) {
							$scope.p1.push(parseInt($scope.data2sitting[i]));
							$scope.p2.push(parseInt($scope.data2sitting[i + 1]));
						}
					}
					else $scope.twositting = 0;
				});

			$scope.getAlloteData($scope.restaurant, $scope.alloteyear, $scope.bkproduct);
		};

		$scope.mealsitting = function (type, index) {
			var start = $scope.start, dd = (type === 1) ? $scope.p1 : $scope.p2;
			dd[index] ^= 1;
			if ($scope.p1[index] === 0 && $scope.p2[index] === 0) {
				//$scope.allote[(index-start)].dinner = "-1";
			}
		};

		// INITIALIZATION call function to populate allotement value
		$scope.setalloteyear($scope.alloteyear);

		$scope.changeMealvalue = function () {
			$scope.valuemeal = $scope.valuemeal.replace(/[^\d-]/g, '');
		};

		$scope.checkDate = function (aa, vv) {
			vv = aa.getDateFormat('-');
			return true;
		};

		$scope.setvalue = function (action) {
			var i, tt, startingIndex, startingIndex, countIndex, dayofweek, weekflag, lunchflg, dinnerflg, value, sum, cday;
			lunchflg = (parseInt($scope.mealtypeselection[0].data) > 0);
			dinnerflg = (parseInt($scope.mealtypeselection[1].data) > 0);
			if (!(lunchflg || dinnerflg)) {
				alert("Please select the mealtype (Lunch/Dinner)");
				return;
			}

			if ($scope.valuemeal === "") {
				alert("Please enter a valid number");
				return;
			}

			if ($scope.sday > $scope.eday) {
				tt = $scope.sday;
				$scope.sday = $scope.eday;
				$scope.eday = tt;
				$scope.nstart = $scope.sday.getDateFormat('-');
				$scope.nend = $scope.eday.getDateFormat('-');
			}

			// check if on week day is checked
			weekflag = 0;
			$scope.weekdayselection.forEach(function (oo, index, arr) {
				oo.data = parseInt(oo.data);
				weekflag += oo.data;
			});
			cday = new Date();
			cday = new Date(cday.getFullYear(), cday.getMonth(), cday.getDate(), 0, 0, 0, 0);
			startingIndex = ($scope.alloteyear === currentyear) ? $scope.sday.toDays(cday) : $scope.sday.toDays(new Date($scope.alloteyear, 0, 1, 0, 0, 0, 0));
			countIndex = startingIndex + $scope.eday.toDays($scope.sday) + 1;
			dayofweek = $scope.sday.getDay();
			value = $scope.valuemeal;

			for (i = startingIndex; i < countIndex; i++) {
				if (weekflag === 0 || $scope.weekdayselection[dayofweek].data === 1) {
					if (lunchflg) $scope.allote[i].lunch = value;
					if (dinnerflg) $scope.allote[i].dinner = value;
				}
				if (++dayofweek > 6) dayofweek = 0;
			}
			alert("Value have been set. Don't forget to save the data when done.");
		};

		$scope.shortcut = function (id) {
			$scope.listingAllotFlag = false;
		};

		$scope.backlisting = function () {
			$scope.listingAllotFlag = true;
		};

		$scope.saveresponse = function () {
			var limit, size, i, value,
				lunchdata = '',
				dinnerdata = '',
				sep = '',
				start = $scope.start;

			if ($scope.twositting === 1) {
				limit = $scope.data2sitting.length / 2;
				for (i = 1; i < limit; i++) {
					$scope.data2sitting[(i * 2)] = $scope.p1[i - 1];
					$scope.data2sitting[(i * 2) + 1] = $scope.p2[i - 1];
					if ($scope.p1[i - 1] === 0 && $scope.p2[i - 1] === 0) {
						//$scope.allote[(i-start)-1].dinner = "-1";
					}
				}
				value = $scope.data2sitting.join("|");
			}

			size = $scope.allote.length;
			limit = $scope.allote.length;
			for (i = 0; i < limit; i++) {
				// overlap year over year, beginning of the week need to be sunday => few negative index for the first week maybe
				if ($scope.allote[i].index < 0) {
					size--;
					continue;
				}
				lunchdata += sep + $scope.allote[i].lunch;
				dinnerdata += sep + $scope.allote[i].dinner;
				sep = '|';
			}

			bookService.saveAlloteYear($scope.restaurant, $scope.alloteyear, size, start, lunchdata, dinnerdata, $scope.bkproduct).then(function (response) {
				if (response.status > 0) {
					var trace = $scope.restaurant+"  "+$scope.usremail;
					bookService.logevent(215,'BO_ALLOTE_SAVE',trace);
					alert("Allotment data has been saved");
					}
				else {
					alert("Unknown Error. Allotment data has NOT been saved. ");
				}
			});

			if ($scope.twositting === 1) {
				bookService.writetwositting($scope.restaurant, value, $scope.alloteyear).then(function (response) {
					if (response.status < 0) alert("Could not saved the dinner sitting open/close");
					else {
						var trace = $scope.restaurant+"  "+$scope.usremail;
						bookService.logevent(215,'BO_ALLOTE2SIT_SAVE',trace);
					}
				});
			}
		};

		$scope.formats = ['dd-MM-yyyy', 'dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'dd.MM.yyyy', 'shortDate'];
		$scope.format = $scope.formats[0];
		$scope.minDate = new Date();
		$scope.maxDate = new Date($scope.alloteyear, 11, 31);
		$scope.dateOptions = {
			formatYear: 'yy',
			startingDay: 1
		};


		// Disable weekend selection
		$scope.disabled = function (date, mode) {
			return false;
			//return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
		};

		$scope.setWeeks = function () {
			var cday = new Date();
			var ww = moment($scope.aweek).week() - moment(cday).week();
			if ($scope.aweek.getDay() == cday.getDay() - 1)
				ww--;
			$scope.paginator.setPage(ww);
		}

		$scope.open = function ($event, $num) {
			$event.preventDefault();
			$event.stopPropagation();

			$scope.dateindex = $num;
			if ($scope.dateindex == 1) $scope.start_opened = true;
			if ($scope.dateindex == 2) $scope.end_opened = true;
			if ($scope.dateindex == 3) $scope.week_opened = true;
		};

	}]);

</script>