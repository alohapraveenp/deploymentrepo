function bkgetalignment(a) {
	return (typeof a === 'number') ? 'center':'left';
}

function long2ip(ip) {

  if (!isFinite(ip))
    return false;

  return [ip >>> 24, ip >>> 16 & 0xFF, ip >>> 8 & 0xFF, ip & 0xFF].join('.');
}

function ip2long(IP) {

  var i = 0;
  IP = IP.match(
    /^([1-9]\d*|0[0-7]*|0x[\da-f]+)(?:\.([1-9]\d*|0[0-7]*|0x[\da-f]+))?(?:\.([1-9]\d*|0[0-7]*|0x[\da-f]+))?(?:\.([1-9]\d*|0[0-7]*|0x[\da-f]+))?$/i
  ); // Verify IP format.
  if (!IP) {
    return false; // Invalid format.
  }
  // Reuse IP variable for component counter.
  IP[0] = 0;
  for (i = 1; i < 5; i += 1) {
    IP[0] += !! ((IP[i] || '')
      .length);
    IP[i] = parseInt(IP[i]) || 0;
  }
  // Continue to use IP for overflow values.
  // PHP does not allow any component to overflow.
  IP.push(256, 256, 256, 256);
  // Recalculate overflow of last component supplied to make up for missing components.
  IP[4 + IP[0]] *= Math.pow(256, 4 - IP[0]);
  if (IP[1] >= IP[5] || IP[2] >= IP[6] || IP[3] >= IP[7] || IP[4] >= IP[8]) {
    return false;
  }
  return IP[1] * (IP[0] === 1 || 16777216) + IP[2] * (IP[0] <= 2 || 65536) + IP[3] * (IP[0] <= 3 || 256) + IP[4] * 1;
}


String.prototype.checkcolorcode = function(ar) {
      var i, l, s = this;
      l = ar.length;
      for(i = 0; i < l; i++)
      	if(s.indexOf(ar[i]) > -1)
      		return true;
      return false;
	};

String.prototype.pad = function(size) {
      var s = this;
      while (s.length < size) {s = "0" + s;}
      return s;
	};
    
function getDiffDay(rdate, hh, mn) {
	if(rdate.length < 10 || rdate.indexOf("-") < 0) 
		return -2;

	var uu = rdate.split('-');
	return parseInt((new Date(parseInt(uu[0]), parseInt(uu[1])-1, parseInt(uu[2]), hh, mn, 0).getTime() - new Date().getTime()) / (24 * 3600 * 1000));
	};

Date.prototype.getDateFormat = function(sep) {
	var d = this.getDate(), m = this.getMonth()+1, y = this.getFullYear();
	if(typeof sep !== 'string' || (sep !== '/' && sep !== '-')) sep = '-';
	return ((d <= 9) ? '0' : '') + d + sep + ((m <= 9) ? '0' : '') + m + sep + y;
	};

Date.prototype.getDateFormatReverse = function(sep) {
	var d = this.getDate(), m = this.getMonth()+1, y = this.getFullYear();
	if(typeof sep !== 'string' || (sep !== '/' && sep !== '-')) sep = '-';
	return y + sep + ((m <= 9) ? '0' : '') + m + sep + ((d <= 9) ? '0' : '') + d;
	};

Date.prototype.today = function() {
	return new Date(this.getFullYear(), this.getMonth(), this.getDate(), 0, 0, 0, 0);
	};

Date.prototype.nextmonth = function() {
	var month = (this.getMonth() < 11) ? this.getMonth()+1 : 0, year = (month > 0) ? this.getFullYear() : this.getFullYear()+1;
	return new Date(year, month, 1, 0, 0, 0, 0);
	};

Date.prototype.prevmonth = function() {
	var month = (this.getMonth() > 0) ? this.getMonth()-1 : 11, year = (month < 11) ? this.getFullYear() : this.getFullYear()-1;
	return new Date(year, month, 1, 0, 0, 0, 0);
	};

Date.prototype.beginmonth = function() {
	return new Date(this.getFullYear(), this.getMonth(), 1, 0, 0, 0, 0);
	};

Date.prototype.endmonth = function() {
	var cyear = this.getFullYear();
	var febsize = ((cyear % 4 === 0 && cyear % 100 !== 0) || cyear % 400 === 0) ? 29 : 28;
	var monthsize = [ 31, febsize, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

	return new Date(this.getFullYear(), this.getMonth(), monthsize[this.getMonth()], 0, 0, 0, 0);
	};
	
Date.prototype.toDays = function(dd) {
	return Math.floor(Math.abs((this.getTime() - dd.getTime())  / (24 * 3600 * 1000)));
	};

String.prototype.mysqljsdate = function() {
	var n, s;
	if(this.length < 10 || this.indexOf('-') === -1) 
		return '2015-01-01';
	s = this.substring(0, 10);
	tt = s.split('-');
	n = parseInt(tt[1]) - 1;
	tt[1] = (n < 10) ? '0'+n : ''+n;
	return tt.join('-');
	};
	
String.prototype.datereverse = function() {

	if(this.length !== 10 || this.indexOf('-') === -1) 
		return this;
	s = this.substring(0, 10);
	tt = s.split('-');
	return tt[2] + "-" + tt[1] + "-" + tt[0];
	};
	
String.prototype.jsdate = function() {
	var n, s, i;
	if(this.length < 10 || this.indexOf('-') === -1) 
		return new Date(2016, 0, 1);
	s = this.substring(0, 10);
	tt = s.split('-');
	for(i = 0; i < tt.length; i++) tt[i] = parseInt(tt[i]);
	tt[1] = tt[1] - 1;
	return new Date(tt[0], tt[1], tt[2], 0, 0, 0, 0);
	};
	
String.prototype.timetoslot = function() {
	var val = this, uu;
	
	if(typeof val === 'undefined') return -2;
	
	if(val !== "" && val.indexOf(":") > 0) {
		uu = val.substring(0, 5).split(":");
		return (parseInt(uu[0]) * 2) + Math.floor(parseInt(uu[1]) / 30);
		}
	return -1;
	};

String.prototype.timeto15slot = function() {
	var val = this, uu;
	
	if(typeof val === 'undefined') return -2;
	
	if(val !== "" && val.indexOf(":") > 0) {
		uu = val.substring(0, 5).split(":");
		return (parseInt(uu[0]) * 4) + Math.floor(parseInt(uu[1]) / 15);
		}
	return -1;
	};

Array.prototype.inObject = function(name, value) {
	for(var i = 0; i < this.length; i++) {
		oo = this[i];
		if(oo[name] && oo[name] === value)
			return i; 
		}
	return -1;
	};

Array.prototype.iconcurrency = function(val, label, field, fieldicon) {
	if(this === null) return;
	if(typeof val !== "string" || val.length < 3 || typeof label !== "string" || label.length < 3 || typeof field !== "string" || field.length < 1 || typeof fieldicon !== "string" || fieldicon.length < 1)
		return;
	
	val = val.toLowerCase();
	var i, limit = this.length, oo;
	for(i = 0; i < limit; i++) {
		oo = this[i];
		if(typeof oo[field] !== "string") continue;
		if(oo[field].toLowerCase() === label) {
			switch(val) {
			  case 'eur':
				oo[fieldicon] = "euro";
				break;
				
			  case 'hkd':
			  case 'sgd':
			  case 'usd':
				oo[fieldicon] = "usd";
				break;

			  case 'krw':
				oo[fieldicon] = "krw";
				break;

			  case 'thb':
				oo[fieldicon] = "thb";
				break;

			  case 'myr':
				oo[fieldicon] = "myr";
				break;			  	
				}
			}
		}
	};

app.filter('drinkfilter', [ function() {
	return function(arr, flg) {
		if (typeof arr === 'undefined' || !(arr instanceof Array) || arr.length < 1) return;
		var tmpAr = [];
		for(var i = 0; i < arr.length; i++) {
			if(arr[i].options === 1 && flg === 3)
				tmpAr.push(arr[i]);
			else if(arr[i].drink !== 1 && arr[i].options !== 1 && flg === 1)
				tmpAr.push(arr[i]);
			else if(arr[i].drink === 1  && flg === 2)
				tmpAr.push(arr[i]);

			}
		return tmpAr;
		}
}]);

app.filter('menufilter', [ function() {
	return function(arr, ID, flg) {
		if (typeof arr === 'undefined' || !(arr instanceof Array) || arr.length < 1) return;
		var oo, ll, tmpAr = [];
		ll = (flg === 1) ? 'menuID' : 'displayID';
		for(var i = 0; i < arr.length; i++) {
			oo = arr[i];
			if(oo[ll] === ID)
				tmpAr.push(arr[i]);
			}
		return tmpAr;
		}
}]);

app.filter('slicefilter', [ function() {
	return function(arr, currentPage, itemsPerPage) {
	if (typeof arr === 'undefined' || !(arr instanceof Array) || arr.length < 1) return;
	return arr.slice(currentPage * itemsPerPage, (currentPage + 1) * itemsPerPage);
	}
}]);


// offset. it starts at the current week
app.filter('slicefilterOffset', [ function() {
	return function(arr, currentPage, itemsPerPage) {
	if (typeof arr === 'undefined' || !(arr instanceof Array) || arr.length < 1) return;
	actualpage = currentPage - paginationOffset;
	return arr.slice(actualpage * itemsPerPage, (actualpage + 1) * itemsPerPage);
	}
}]);

app.filter('timeslotfilter', [ function() {
	return function(arr, time) {
		if (typeof arr === 'undefined' || !(arr instanceof Array) || arr.length < 1) return;
		if(time <= 0) return arr;
		for(var i = 0; i < arr.length; i++)
			if(parseInt(arr[i].time.substring(0,2)) >= time)
				return arr.slice(i, arr.length);
		return [];
		}
}]);

app.filter('fullfilter', [ function() {
	return function(arr, mealtype, waiting, seated, last) {
		if(typeof arr === 'undefined' || !(arr instanceof Array) || arr.length < 1) return;

		if(typeof mealtype === 'undefined' || mealtype === "") mealtype = "";
		if(typeof waiting !== 'number' || waiting < 0 || waiting > 1) waiting = -1;
		if(typeof seated !== 'number' || seated < 0 || seated > 1) seated = -1;
		if(typeof last !== 'number' || last < 0 || last > 1) last = -1;

		var tmpAr = [];
		for(var i = 0; i < arr.length; i++) {
			if(
			((mealtype === "" || arr[i].mealtype === mealtype) &&
			((waiting === 1 && arr[i].state === "waiting" ) || (waiting === 0 && arr[i].state !== "waiting" )) &&
			(seated !== 1 || (arr[i].state === "to come" || arr[i].state === "waiting")) && last !== 1) || arr[i].last === 1
			)
				tmpAr.push(arr[i]);
			}
		return tmpAr;
		}
}]);


app.filter('mealtypefilter', [ function() {
	return function(arr, mealtype) {
		if(typeof arr === 'undefined' || !(arr instanceof Array) || arr.length < 1) return;
		if(typeof mealtype === 'undefined' || mealtype === "") return arr;
		var tmpAr = [];
		for(var i = 0; i < arr.length; i++)
			if(arr[i].mealtype === mealtype)
				tmpAr.push(arr[i]);
		return tmpAr;
		}
}]);

app.filter('waitingfilter', [ function() {
	return function(arr, flg) {
		if(typeof arr === 'undefined' || !(arr instanceof Array) || arr.length < 1) return;
		if(typeof flg !== 'number' || flg < 0 || flg > 1) return arr;
		var tmpAr = [];
		for(var i = 0; i < arr.length; i++)
			if((arr[i].state !== "waiting" && flg === 0) || (arr[i].state === "waiting" && flg !== 0))
				tmpAr.push(arr[i]);
		return tmpAr;
		}
}]);


app.filter('filterBymyDate', [ function() {
	return function(arr, start, end, field) {
		var tmpAr = [], oo;
		if (typeof arr === 'undefined' || !(arr instanceof Array) || arr.length < 1) 
			return tmpAr;
		if(start < 0) return arr;
		oo = arr[0];
		if(typeof oo[field] === 'undefined') {
			console.log('ERROR filterBymyDate undefined field', field);
			return tmpAr;
			}
		for(var i = 0; i < arr.length; i++) {
			oo = arr[i];
			if(oo[field] >= start && oo[field] <= end)
				tmpAr.push(oo);
			}
		return tmpAr;
		}
}]);


app.filter('inversedate', [ function() {
	return function(input) {
		return input.substr(8,2) + "-" + input.substr(5,2) + "-" + input.substr(0,4);
	}
}]);

app.filter('adatereverse', [ function() {
	return function(input, f) {
		if(typeof f!=='string' || typeof input!=='string' || input.length < 10|| f !== 'date') return input;
		return input.substr(8,2) + "-" + input.substr(5,2) + "-" + input.substr(0,4);
	}
}]);

app.filter('apostrophe', [function() {
	return function(s) {
		if(typeof s === "string" && s.length > 1)
			return s.replace(/`/g, "’");
		return s;
	}
}]);

app.filter('notzero', [ function() {
	return function(n) {
		return (typeof n !== 'number' || n > 0) ? n : "";
	}
}]);


app.filter('listsubstr', [ function() {
	return function(input, l) {
		if(typeof l !== 'number') return input;
		return input.substring(0, l);
	}
}]);


app.filter('showdate', [ function() {
	return function(dd) {
		return dd.getDate() + "-" + (dd.getMonth()+1) + "-" + dd.getFullYear();
	}
}]);


app.filter('capitalize', function() {
  return function(str, scope) {
    if (str != null && str != "")
    str = str.toLowerCase();
    return str.substring(0,1).toUpperCase()+str.substring(1);
  }
});

app.directive('buttonRender', function() {
  return {
    restrict: 'A',
    require: 'ngModel',
    link: function(scope, element, attrs, ctrl) {
      ctrl.$render = function() {
        var value = ctrl.$isEmpty(ctrl.$viewValue) ? '' : ctrl.$viewValue;
        if (element.val() !== value) {
          element.val(value);
        }
      };
    }
  };
});

app.directive('showpict', ['$compile', function($compile){
	return {
		restrict: 'AE',
		replace: true,
		link: function(scope, element, attrs){
			if(attrs.pict == '')
				return;
			$('.showpict').html(attrs.pict);
    		}
		};
}]);

app.directive('extitle', ['$compile', function($compile){
	return {
		restrict: 'AE',
		replace: true,
		link: function(scope, element, attrs){
			if(scope.x.a != "divider") element.html('<li style="margin: 0 0 8px 20px;"><a href ng-click="extraselect(x.a)"><span class="custom" id="' + scope.x.a + '" ></span>  {{ x.b }} <br /></a></li>').show();
			else element.html('<li class="divider"></li>').show();
			$compile(element.contents())(scope);
    		}
		};
}]);

app.directive('0extitle', [ function(){
	var linkFunction = function(scope, elem, attrs) { 
		if(scope.x.a == "divider") { scope.x.a = scope.x.b = ""; }
		}
				
	return {
		restrict: 'AE',
		replace: true,
		template: '<a href ng-click="extraselect(x.a)">{{ x.b }}</a>',
		link: linkFunction
		};
}]);

app.directive('autoComplete', ['$timeout',function($timeout) {
    return function(scope, iElement, iAttrs) {
            iElement.autocomplete({
                source: scope[iAttrs.uiItems],
                select: function() {
                       console.log("SDsd"+scope[iAttrs.uiItems]);
                    $timeout(function() {
                      iElement.trigger('input');
                    }, 100);
                }
            });
    };
 }]);

function toggleCarat(item, module, reverse) {
}	

app.directive('tbtitle', [ function(){
	return {
		restrict: 'AE',
		replace: true,
		template: '<a href="javascript:" style="color:{{y.cc}}" class="{{moduleName}}_title {{moduleName}}_{{y.b}}">{{ y.b }} <i class="fa fa-caret-{{y.q}}"></a>',
		link: function(scope, elem, attrs, ctrls) { 
			elem.bind('click', function() {
				var i, ll = attrs["name"], data = scope[ll], cc = elem.css('color'), ncc, cll, cl = scope.moduleName + "_title";
				for(i = 0; i < data.length; i++)  {
					if(data[i].q === "up") { 
						cll = scope.moduleName + '_' + data[i].b;
						$("th ." +cll).html('<a href="javascript:" style="color:rgb(0, 0, 0)" class="'+cl+' '+cll+'">' + data[i].b + ' <i class="fa fa-caret-down"></a>');
						break;
						}
					}
				$("th ." +cl).css('color', 'rgb(0, 0, 0)'); 
				scope.y.q = (cc === "rgb(0, 0, 0)" || cc === "rgb(255, 165, 0)") ? "down" : "up";
				ncc = (scope.y.q == 'up') ? 'rgb(255, 165, 0)' : 'rgb(255, 0, 255)';
				cll = scope.moduleName + '_' + scope.y.b;
				elem.html('<a href="javascript:" style="color:' + ncc + '" class="'+cl+' '+cll+'">' + scope.y.b + ' <i class="fa fa-caret-' + scope.y.q + '"></a>');
				elem.css('color', ncc);	// 'orange' : 'fuchsia'
				scope.reorder(scope.y.a, scope.y.alter);
				scope.$apply();
				}); 
			}
		};
}]);

app.directive('tbtitleold', [ function(){
	return {
		restrict: 'AE',
		replace: true,
		template: '<a href="javascript:" ng-click="reorder(y.a);" style="color:{{y.cc}}">{{ y.b }} <i class="fa fa-caret-{{y.q}}"></a>',
		link: function(scope, elem, attrs) { 
			elem.bind('click', function() {
				var i, ll = attrs["name"], data = scope[ll], cc = elem.css('color');
				for(i = 0; i < data.length; i++) data[i].q = "down";
				scope.y.q = (cc === "rgb(0, 0, 0)" || cc === "rgb(255, 165, 0)") ? "down" : "up";
				$('th a').css('color', 'rgb(0, 0, 0)'); 
				elem.css('color', (scope.y.q === 'up') ? 'rgb(255, 165, 0)' : 'rgb(255, 0, 255)');
				}); 
			}
		};
}]);

app.directive('tbtestTitle', [ function(){
	var linkFunction = function(scope, element, att) {
		scope.ll = att["label"];
		scope.vv = att["val"]; 
		scope.mm = att["val"] + '_' + att["module"];
	};
	return {
		template: function(el, att) {
			return '<a href id="{{vv}}" ng-click="reorder(vv);"><span id="{{mm}}" class="classbgwhite">{{ll}}<i class="fa fa-caret-down"></span></a>';
			},
    	link:linkFunction
		};
}]);

app.directive('tbListing', [ function(){
	return {
		restrict: 'AE',
		replace: true,
		template: '<a href ng-click="view(x)"><div ng-if="z.l ===\"\"">{{ x[z.a]}}</div><div ng-if="z.l !==\"\"" class="truncate" style="width:{{z.l}}px">{{ x[z.a]}}</div></a>'
		};
}]);

app.directive("tbListingbkg", ['$compile', function($compile) { 
	var linkFunction = function( scope, el, at) { 
		if(scope.z.l !== "") {
			var content = el.html(); 		
			el.html('<div class="truncate" style="width:' + scope.z.l + 'px">' + content + '</div').show();
			$compile(el.contents())(scope);
    		}
		}; 
	return { 
		restrict: "AE", 
		link: linkFunction 
	}; 
}]);

// not working on template ...
app.directive("loadingDiv", [ function () {
  return {
    restrict: 'AE',
    replace: true,
    template: '',
    link: function(scope, elem, attrs) {
    	console.log('IN LOADING', scope.loadingTemplate);
		scope.loadingTemplate = 'ok';
    }
  };
}]);


app.service('cleaningData', [ function(){
	this.title = function(data) {
		if(data == '' || !data) return '';		
		return (data != '' && data.length > 0) ? data.replace(/[!@#$%^*=}{\]\[\"\':;><\?/|\\]/g, ' ') : '';
		};
		
	this.input = function(data) {
		if(data == '' || !data) return '';
		return (data != '' && data.length > 0) ? data.replace(/'|"/g, '`') : '';
		};
		
	this.number = function(data) {
		if(data == '' || !data) return '';
		return (data != '' && data.length > 0) ? data.replace(/[^0-9\-\+]/g, '') : '';
		};
		
	this.flag = function(data) {
		if(data == '' || !data) return '';
		return (data != '' && data.length > 0) ? data.replace(/[^0-1]/g, '') : '';
		};
}]);

app.service('extractService', function() {
	
	var self = this;
	var ifrm, ffrm, extrctdata, extrctfile;

	function writeiframe() {
		if(ifrm.contentWindow.foo === undefined)
			return setTimeout(writeiframe, 300);
		document.getElementById("iframeid").contentWindow.chgvalue(extrctfile, extrctdata);
		setTimeout(function() { alert("Extraction started, check download folder");}, 2000);
		}

	function downloadiframe(filename, data) {
	
		extrctfile = filename;
		extrctdata = data;
		
		if(ifrm)
			return writeiframe();
			
		ifrm = document.createElement("iframe");
        ifrm.setAttribute("src", "iframe.html");
        ifrm.setAttribute("id", "iframeid");
        ifrm.style.width = "1px";
        ifrm.style.height = "1px";
        document.body.appendChild(ifrm);
        if(ifrm.contentWindow.foo === undefined)
        	return setTimeout(writeiframe, 300);
    }

	function downloaddata(filename, content) {
        var link;
        filename = filename || 'export.csv';

        if (!content.match(/^data:text\/csv/i) || true) {
            content = 'data:text/csv;charset=utf-8,' + content;
        	}
        data = encodeURI(content);

        link = document.createElement('a');
        link.setAttribute('href', data);
        link.setAttribute('download', filename);
		document.body.appendChild(link);
		link.click();
		document.body.removeChild(link);
		}

	this.fullextract = function(req) {
		if(ffrm)
			ffrm.remove();
			
		ffrm = document.createElement("iframe");
        ffrm.setAttribute("src", "fullextract.php?" + req);
        ffrm.style.width = "1px";
        ffrm.style.height = "1px";
        document.body.appendChild(ffrm);
		setTimeout(function() { alert("Extraction started, check download folder");}, 2000);
		};
				
	this.browserVersion = function() {
		var ua = navigator.userAgent;
		var offset, name = "unknown", version = "";
		var i, p, nameAr = [ "MSIE", "Chrome", "Safari", "Firefox"], offAr = [ 5, 7, 7, 8];
	
		for(i = 0; i < nameAr.length; i++) 
			if ((offset=ua.indexOf(nameAr[i]))!=-1) {
				name = nameAr[i];
				version = ua.substring(offset+offAr[i]);
				if (nameAr[i] === "Safari" && (offset=ua.indexOf("Version"))!=-1) 
					version = ua.substring(offset+8);
				if(version !== "") version = parseInt(version.replace(/[^\d.]/g, ''));
				break;	 
				}
		return { name: name, version: version };
		};
	
	this.filter = function(s) {	
		if(!s) return "";	
		return (typeof s !== "string" || s.length < 1) ? s : s.replace(/[,\n\r\'\"]/g, " ");
		};

	this.save = function(filename, data) {
		var brswr = this.browserVersion();

		if( (brswr.name === "Chrome" && brswr.version > 45) ||
		    (brswr.name === "Firefox" && brswr.version > 40)
			)	
			return downloaddata(filename, data);
		
		else downloadiframe(filename, data);
		};
		
	this.saveObj = function(filename, obj) {
		if(!obj.booking || obj.booking.length < 1)
			return;
			
		var tmpAr = obj.booking, labelAr = Object.keys(tmpAr[0]), data = "";
		
		labelAr.map(function(label) { data += label + ","; });
		tmpAr.map(function(oo) { 
			labelAr.map(function(ll) { data +=  self.filter(oo[ll]) + ","; });
			});
			
		self.save(filename, data);
		};
	
});

app.service('bookService', ['$http','$q', function($http,$q){  

  	var self = this;
	var weeloy_demo_restaurant = ["SG_SG_R_TheFunKitchen", "SG_SG_R_TheOneKitchen", "FR_PR_R_LaTableDeLydia", "KR_SE_R_TheKoreanTable"];

	this.getPlatform = function(val){
		var i, profile = ["Android", "iPhone", "iPad", "Windows", "Apple"];
		
		if(val === "" || typeof val !== 'string') return "";
		if(val.indexOf("name=unknown|version=unknown|platform=Windows") >= 0)
			return "iPhoneApp";
		if(val.indexOf("name=Android") >= 0 && val.indexOf("platform=Android") >= 0)
			return "AndroidApp";
			
		for(i = 0; i < profile.length; i++)
			if(val.indexOf("platform=" + profile[i]) >= 0)
				return profile[i];
		return "";
		};
		
	this.getBrowser = function(val){
		var i, tmp, browser = ["Chrome", "Internet Explorer", "Safari", "Firefox"];
		
		if(val === "" || typeof val !== 'string') return "";
		if(val.indexOf("name=unknown|version=unknown|platform=Windows") >= 0)
			return "iPhoneApp";
		if(val.indexOf("name=Android") >= 0 && val.indexOf("platform=Android") >= 0)
			return "AndroidApp";

		for(i = 0; i < browser.length; i++)
			if(val.indexOf("name=" + browser[i]) >= 0)
				return browser[i];
		return val.replace(/\|.*$/g, '').replace(/name \= /g, '');
		};

	this.sendsms = function(mobile, msg, conf, restaurant, sender) {
		return $http.post("../api/v2/notification/sms/send",
			{
			'mobile': mobile,
			'msg' 	: msg,
			'ref_id': conf,
			'restaurant': restaurant,
			'sender' 	: sender,
			'type': '',
			'token': token
			}).then(function(response) { return response.data;});
		};
				
	this.loginfo = function(category, content) {
		return $http.post("../api/log/info/",
			{
			'category': category,
			'content' 	: content
			}).then(function(response) { return response.data;});
		};

	this.checkDayAvail = function(restaurant, product, time, pers) {
		return $http.post("../api/restaurant/dayavailable/",
			{
			'restaurant': restaurant,
			'product': product,
			'time' 	: time,
			'pers' 	: pers
			}).then(function(response) { return response.data;});
		};

	this.checkAvail = function(restaurant, date, time, pers, booking) {

		date = this.normDate(date, true, '-');
		return $http.post("../api/restaurant/availslotbkg/",
			{
			'restaurant': restaurant,
			'booking': booking,
			'date' 	: date,
			'time' 	: time,
			'pers' 	: pers,
			'token'	: token
			}).then(function(response) { return response.data;});
		};

	this.notifySMS = function(restaurant, booking, mobile, msg, email) {

		return $http.post("../api/sms/notification/",
			{
				'restaurant': restaurant,
				'booking': booking,
				'mobile': mobile,
				'msg' 	: msg,
				'email' : email,
				'token'	: token
            }).then(function(response) { return response.data;});
		};


	this.openPOS = function(restaurant, booking, token) {
		return $http.post("../api/services.php/pos/open/",
			{
				'booking' : booking,
				'restaurant': restaurant,
				'token'	: token
            }).then(function (response) { return response.data; });
		};

	this.statusPOS = function(restaurant, booking, token) {
		return $http.post("../api/services.php/pos/status/",
			{
				'booking' : booking,
				'restaurant': restaurant,
				'token'	: token
            }).then(function (response) { return response.data; });
		};

	this.validateBooking = function(restaurant, booking) {
		return $http.post("../api/services.php/booking/validate/",
			{
				'booking' : booking,
				'restaurant': restaurant,
				'token'	: token
            }).then(function (response) { return response.data; });
		};

	this.summaryBooking = function(restaurant, booking) {
		return $http.post("../api/services.php/booking/summary/",
			{
				'booking' : booking,
				'restaurant': restaurant,
				'token'	: token
            }).then(function (response) { return response.data; });
		};

	this.readRestaurant = function(restaurant) {
		var url = '../api/restaurant/info/' + restaurant;
		return $http.get(url).success(function (response) { return response; });
		};

	this.writeBulletconfig = function(restaurant, data) {
		return $http.post("../api/restaurant/bullet/",
			{
				'data' : data,
				'restaurant': restaurant,
				'token'	: token
            }).then(function (response) { return response.data; });
		};

	this.bookingProfile = function(restaurant, systemid, email) {
		return $http.post("../api/services.php/profile/read/one/booking/",
			{
				'systemid' : systemid,
				'email' : email,
				'restaurant': restaurant,
				'token'	: token
            }).then(function (response) { return response.data; });
		};
				
	this.read1Profile = function(restaurant, email, phone, account) {
		if(typeof account !== "string") account = "";
		return $http.post("../api/services.php/profile/read/one/",
			{
				'phone' : phone,
				'email' : email,
				'account' : account,
				'restaurant': restaurant,
				'token'	: token
            }).then(function (response) { return response.data; });
		};

	this.readProfile = function(restaurant, email) {
		return $http.post("../api/services.php/profile/read/",
			{
				'email' : email,
				'restaurant': restaurant,
				'token'	: token
            }).then(function (response) { return response.data; });
		};

	this.readSubProfile = function(restaurant, email) {
		return $http.post("../api/services.php/profilesub/read/",
			{
				'email' : email,
				'restaurant': restaurant,
				'token'	: token
            }).then(function (response) { return response.data; });
		};

/*
	this.readProfileNew = function(restaurant, email) {
		var str = "https://api.weeloy.asia/profile/GetProfile?email=" + email + "&restaurant=" + restaurant + "&token=" + token;
		return $http.get(str).then(function (response) { return response.data; });
		};

	this.readSubProfileNew = function(restaurant, email) {		
		var str = "https://api.weeloy.asia/profile/GetSubProfile?email=" + email + "&restaurant=" + restaurant + "&token=" + token;
		//var str = '../api/dailyboard.php/md_restaurant/getprofilelist/'+restaurant+'';
		return $http.get(str).then(function (response) { return response.data; });
		};

*/
	this.updateformatsubProfile = function(restaurant, email, oo, nn) {
		return $http.post("../api/services.php/profilesub/updateformat/",
			{
				'email' : email,
				'restaurant': restaurant,
				'old': oo,
				'new': nn,
				'token'	: token
            }).then(function (response) { return response.data; });
		};

	this.updatesubProfile = function(restaurant, email, systemid, subprof) {
		return $http.post("../api/services.php/profilesub/update/",
			{
				'email' : email,
				'restaurant': restaurant,
				'systemid': systemid,
				'subprof': subprof,
				'token'	: token
            }).then(function (response) { return response.data; });
		};

	this.readCodeProfile = function(restaurant, email) {
		return $http.post("../api/services.php/profile/readcodeprof/",
			{
				'email' : email,
				'restaurant': restaurant,
				'token'	: token
            }).then(function (response) { return response.data; });
		};

	this.updateCodeProfile = function(restaurant, email, content) {
		return $http.post("../api/services.php/profile/updatecodeprof/",
			{
				'restaurant': restaurant,
				'content': content,
				'email' : email,
				'token'	: token
            }).then(function (response) { return response.data; });
		};

	this.readCodeBooking = function(restaurant, email) {
		return $http.post("../api/services.php/profile/readcodebkg/",
			{
				'email' : email,
				'restaurant': restaurant,
				'token'	: token
            }).then(function (response) { return response.data; });
		};

	this.updateCodeBooking = function(restaurant, email, content) {
		return $http.post("../api/services.php/profile/updatecodebkg/",
			{
				'restaurant': restaurant,
				'content': content,
				'email' : email,
				'token'	: token
            }).then(function (response) { return response.data; });
		};


	this.readDSB = function(params) {
		return $http.post("../api/dailyboard.php/md_dailyspecial/getdailyspecial",{
				"body":{
					"data": {
						"code": params	
					}
				}               
		}).then(function(response) { return response.data; })
		};

	this.getcuisine = function() {
		//var defferred = $q.defer();
        // local var API_URL = 'http://localhost:8888/weeloy.com/api/cuisinelist';
         var API_URL = 'https://dev.weeloy.asia/api/cuisinelist';
        
        return $http.get(API_URL).then(function(response) {return response.data; })

	};

	this.getrestauranttitle = function(restid) {  
        return $http.post('../api/dailyboard.php/md_dailyspecial/getrestauranttitle',restid).then(function(response) {return response.data; })

	};


	this.createDSB = function(params) {
		var product = "";
		
		return $http.post("../api/dailyboard.php/md_dailyspecial/postdailyspecial",{
				"httpMethod": "POST",
				"body":{
					"data": params
				}               
		}).then(function(response) { return response.data; })
		};

	this.updateDSB = function(params) {
		var product = "";
		
		return $http.post("../api/dailyboard.php/md_dailyspecial/postdailyspecial",{
				"httpMethod": "POST",
				"body":{
					"data": params
				}               
		}).then(function(response) { return response.data; })
		};

	this.deleteDSB = function(restaurant, id, email) {
		var product = "";
		// console.log('DELETE ' + id);
		// var data = [{ resto:'myresoto', name:'mypromotion', dsbID:'anID', pdate:'2017-04-12', dsb:'mycontent'}];
		return $http.post("../api/dailyboard.php/md_dailyspecial/deletedailyspecial", 
			{
				"httpMethod": "DELETE",
				"body":{
					"data":{
						'id': id,
                		'token': token		
					}	
				}

			}).then(function(response) { return response.data; })
		};



	this.invoiceBooking = function(whenday, type, email, restaurant) {
		return $http.post("../api/restaurant/bookinginvoice/",
			{
				'whenday': whenday,
				'type': type,
				'email' : email,
				'restaurant': restaurant,
				'token'	: token
            }).success(function (response) { 
			var i, c, tt, uu, names;

            if(response.status < 0)
            	return response;
			
			names = response.data.bookings;
			for (i = 0; i < names.length; i++) {
				names[i].rdate = names[i].rdate.substr(0,10);
				names[i].rtime = names[i].rtime.substr(0,5);
				names[i].ctime = names[i].cdate.substring(11,16);
				names[i].cdate = names[i].cdate.substring(0,10);
				names[i].vdate = names[i].rdate.jsdate().getTime();
				names[i].vcdate = names[i].cdate.jsdate().getTime();
				names[i].tracking = names[i].tracking.toLowerCase();
				names[i].status = names[i].status.toLowerCase();
				names[i].dinner = (parseInt(names[i].rtime.substr(0,2)) >= 16) ? 1 : 0;
				names[i].lunch = names[i].dinner ^ 1;
				names[i].mealtype = (names[i].lunch === 1) ? "lunch" : "dinner";
				names[i].fullname = names[i].salutation + ' ' + names[i].firstname + ' ' + names[i].lastname;
				names[i].index = i + 1;
				self.setTracking(names[i]);
				}
			return response;
			});
		};
				
	this.readBkgdata = function(year, type, email, restaurant) {
		return $http.post("../api/restaurant/bookingdata/",
			{
				'year': year,
				'type': type,
				'email' : email,
				'restaurant': restaurant,
				'token'	: token
            }).success(function (response) { 
            
            if(response.status < 0)
            	return response;
            
		var i, c, tt, uu, names, oo, bookings = response.data.bookings, validOptions = ['event', 'duration', 'captain', 'cleartime', 'seated', 'repeat', 'notifysmswait', 'notestext', 'notescode', 'validate', 'orders'];
            	var fielAr = response.data.fields.replace(/ /g, '').split(',');
            		names = [];
			for (i = 0; i < bookings.length; i++) {
				oo = {};
				for (j = 0; j < fielAr.length; j++) {
					oo[fielAr[j]] = bookings[i][j];
					}
				names.push(oo);
            	}
            	
			for (i = 0; i < names.length; i++) {
				names[i].rdate = names[i].rdate.substr(0,10);
				names[i].rtime = names[i].rtime.substr(0,5);
				names[i].tracking = names[i].tracking.toLowerCase();
				names[i].status = names[i].status.toLowerCase();
				names[i].dinner = (parseInt(names[i].rtime.substr(0,2)) >= 16) ? 1 : 0;
				names[i].lunch = names[i].dinner ^ 1;
				names[i].mealtype = (names[i].lunch === 1) ? "lunch" : "dinner";
				names[i].navigator = self.getBrowser(names[i].browser);
				names[i].lastvisit = "";
				names[i].index = i + 1;
		 		names[i].test = (names[i].firstname.toLowerCase() === "test" || 
		 	   					names[i].lastname.toLowerCase() === "test" || 
		 	   					names[i].specialrequest.toLowerCase() === "test" || 
		 	   					names[i].specialrequest.toLowerCase().indexOf("test ") >= 0 );
		 	   	names[i].funkitchen = (weeloy_demo_restaurant.indexOf(names[i].restaurant) > -1);
				self.setTracking(names[i]);
				if(names[i].more && typeof names[i].more === "string" && names[i].more !== "") {
					oo = names[i];
					oo.options = self.parseObj(oo.more);
					Object.keys(oo.options).map(function(x) { if(validOptions.indexOf(x) >= 0) oo[x] = oo.options[x]; });                              
					}
				}
			response.data.bookings = names;
			return response;
			});
		};
		
	this.readAnalytics = function(whenday, type, email, restaurant) {
		return $http.post("../api/restaurant/bookingdayreport/",
			{
				'whenday': whenday,
				'type': type,
				'email' : email,
				'restaurant': restaurant,
				'token'	: token
            }).success(function (response) { 
            
            if(response.status < 0)
            	return response;
            	
			var i, j, oo, c, tt, uu, names, data = response.data.bookings; 
			
			if(type.search("o") >= 0) {
				var data = response.data.bookings;
				var bookings = data.bookings;
				var fielAr = data.fields.replace(/ /g, '').split(',');
				names = [];
				for (i = 0; i < bookings.length; i++) {
					oo = {};
					for (j = 0; j < fielAr.length; j++) {
						oo[fielAr[j]] = bookings[i][j];
						}
					names.push(oo);
					}
				}
			else names = response.data.bookings;
			
			names.map(function(oo, i) {
				oo.tracking = oo.tracking.toLowerCase();
				oo.status = oo.status.toLowerCase();
				oo.dinner = (oo.mealtype === '2') ? 1 : 0;
				oo.lunch = oo.dinner ^ 1;
				});
			response.data.bookings = names;
			return response;
			});
		};
		
	this.getbkgcolorcode = function() {
		return ["red", "green", "navy", "purple", "orange"]; //, "darkturquoise"
		};
		
	this.getcolorcode = function(oo, scheme) {
		if(oo.notescode && typeof oo.notescode === "string" && oo.notescode.length > 1) {
			if(scheme && scheme instanceof Array) {
				for(var i = 0; i < scheme.length; i++) {
					if(oo.notescode.checkcolorcode(scheme[i].code))
						return ''+(i+1);
					}
				if(oo.state === "partially seated")
					return '6';
				}
			}
		return '0';
		};

	this.getmealtype = function(oo, dinnertime) {
		return (oo.hh < dinnertime.hh || (oo.hh === dinnertime.hh && oo.mm < dinnertime.mm)) ? "lunch" : "dinner";
		};

	this.cleanstring = function(a) {
		if(typeof a === "string" && a.length > 0)
			return a.replace(/\'|\"|’/g, '`'); 
		return "";
		};

	this.prepareBooking = function(obj, restaurant, index, colorscheme, dinnertime) {
		
		var i, ll, oo = {}, notestext, notescode, sep, dbbistro;
		
		obj.lastname = this.cleanstring(obj.lastname);
		obj.firstname = this.cleanstring(obj.firstname);
		obj.mobile = this.cleanstring(obj.mobile);
		obj.email = this.cleanstring(obj.email);
		obj.nevent = this.cleanstring(obj.nevent);
		obj.comment = this.cleanstring(obj.comment);
		obj.tablename = this.cleanstring(obj.tablename);

		oo.error = 0;
		oo.booking = obj.booking;	
		oo.date = obj.date;		// date object of originaldate
		oo.cdate = obj.cdate;		// date object of originaldate
		oo.last = obj.lastname;
		oo.first = obj.firstname;
		oo.phone = obj.mobile;
		oo.email = obj.email;
		oo.pers = obj.npers;
		oo.time = obj.ntimeslot;
		oo.event = obj.nevent;
		oo.comment = obj.comment;
		oo.state = "to come";
		oo.seated = "";
		oo.type = "walkin";
		oo.tracking = "tms|walkin";
		oo.tablename = obj.tablename;

		if(oo.tablename !== "") {
			oo.state = "seated";
			oo.seated = "1";
			}
			
		notestext = obj.notestext;		
		notescode = sep = "";
		if(obj.notescode instanceof Array) {
			for(i = 0; i < obj.notescode.length; i++)
				if(obj.notescode[i].value) {
					notescode += sep + obj.notescode[i].label;
					sep = ",";
					}
			}

		if(typeof notestext === "string" && notestext !== "") {
			notestext = notestext.replace(/\'|\"/g, '`'); // ’`
			notestext = notestext.replace(/\s+/g, ' '); 
			}
		if(typeof notescode === "string" && notescode !== "") {
			notescode =  notescode.replace(/\'|\"/g, '`'); // ’`
			notescode =  notescode.replace(/\s+/g, ' '); 
			}

		oo.fullnotes = notestext + ((notestext !== "" && notescode !== "") ? "," : "") + notescode; 

		if(!( (typeof oo.date === "string" && oo.date.length > 7) && (typeof oo.time === "string" && oo.time.length > 3) &&
			(typeof oo.pers === "string" && parseInt(oo.pers) > 0) || (typeof oo.pers === "number" && oo.pers > 0) )) {
			oo.msg = 'Value are not set. Cannot modify current booking ' + oo.date + ' ' + oo.time  + ' ' + oo.pers;
			oo.error = 1;
			return oo;
			}

		oo.fullname = obj.firstname + ' ' + obj.lastname;
		oo.slot = oo.time.timetoslot(); 
		oo.slot15 = oo.time.timeto15slot();
		oo.datetime = oo.date.jsdate().getTime();
		oo.cfulldate = oo.cdate + ', ' + oo.ctime;
		oo.createdate = oo.cfulldate;
		oo.ddate = oo.date;
		oo.sdate = oo.date.substr(8, 2) + '/' + oo.date.substr(5, 2);
		oo.ctime = obj.time;
		oo.hh = parseInt(oo.time.substr(0, 2)); 
		oo.mm = parseInt(oo.time.substr(3, 2)); 
		oo.vtime = (oo.hh * 60) + (oo.mm);
		oo.vcdate = oo.cdate.jsdate().getTime();
		oo.vdate = oo.date.jsdate().getTime();
		oo.index = index;
		oo.restaurant = restaurant;
		if(oo.hh < 7) oo.status = 'cancel';
		oo.notestext = notestext;
		oo.notescode = notescode;
		oo.mealtype = this.getmealtype(oo, dinnertime);
		oo.colorcode = this.getcolorcode(oo, colorscheme);
		oo.fullname = oo.fullname.replace("nofirstname", "").replace("nolastname", "").trim();
		oo.captain = oo.cleartime = oo.duration = oo.bkstatus = '';

		Object.keys(oo).map(function(ll) {
			var s = oo[ll];
			if(typeof s === "string" && s !== "") {
				s = s.replace(/\'|\"|’/g, '`'); // ’`
				s = s.replace(/\s+/g, ' '); // ’`
				if(s.length > 0)
					s = s.trim();
				oo[ll] = s;
				}
			});
		
		oo.more = JSON.stringify( { event: oo.event, notestext: notestext, notescode: notescode } );
		return oo;
		};

	this.readDayreport = function(whenday, type, email, restaurant) {
		return $http.post("../api/restaurant/bookingdayreport/",
			{
				'whenday': whenday,
				'type': type,
				'email' : email,
				'restaurant': restaurant,
				'token'	: token
            }).success(function (response) { 
            
            if(response.status < 0)
            	return response;
            	
			var i, j, oo, c, tt, uu, names, data = response.data.bookings; 
			var validOptions = ['event', 'duration', 'captain', 'cleartime', 'seated', 'repeat', 'notifysmswait', 'notestext', 'notescode', 'validate', 'orders'];
			
			if(type.search("o") >= 0) {
				var data = response.data.bookings;
				var bookings = data.bookings;
				var fielAr = data.fields.replace(/ /g, '').split(',');
				names = [];
				for (i = 0; i < bookings.length; i++) {
					oo = {};
					for (j = 0; j < fielAr.length; j++) {
						oo[fielAr[j]] = bookings[i][j];
						}
					names.push(oo);
					}
				}
			else names = response.data.bookings;
			
			console.log('TYPE', type, type.search("o"), names.length);
			names.map(function(oo, i) {
				oo.booking = oo.confirmation;
				oo.rdate = oo.rdate.substr(0,10);
				oo.rtime = oo.rtime.substr(0,5);
				oo.ctime = oo.cdate.substring(11,16);
				oo.cdate = oo.cdate.substring(0,10);
				oo.vdate = oo.rdate.jsdate().getTime();
				oo.vcdate = oo.cdate.jsdate().getTime();
				oo.cfulldate = oo.cdate + ', ' + oo.ctime;
				oo.tracking = oo.tracking.toLowerCase();
				oo.status = oo.status.toLowerCase();
				oo.dinner = oo.diner = (parseInt(oo.rtime.substr(0,2)) >= 16) ? 1 : 0;
				oo.lunch = oo.dinner ^ 1;
				oo.mealtype = (oo.lunch === 1) ? "lunch" : "dinner";
				oo.fullname = oo.salutation + ' ' + oo.firstname + ' ' + oo.lastname;
				oo.iplong = long2ip(oo.ip);
				oo.platform = self.getPlatform(oo.browser);
				oo.navigator = self.getBrowser(oo.browser);
				oo.lastvisit = "";
				oo.index = i + 1;
		 		oo.test = (oo.firstname.toLowerCase() === "test" || 
		 	   					oo.lastname.toLowerCase() === "test" || 
		 	   					oo.specialrequest.toLowerCase() === "test" || 
		 	   					oo.specialrequest.toLowerCase().indexOf("test ") >= 0 );
		 	   	oo.funkitchen = (weeloy_demo_restaurant.indexOf(oo.restaurant) > -1);
				self.setTracking(oo);
				if(oo.more && typeof oo.more === "string" && oo.more !== "") {
					oo.options = self.parseObj(oo.more);
					Object.keys(oo.options).map(function(x) { if(validOptions.indexOf(x) >= 0) oo[x] = oo.options[x]; });                              
					}
				if(!oo.notestext) oo.notestext = "";
				if(!oo.notescode) oo.notescode = "";
				oo.fullnotes = oo.notestext + ((oo.notestext !== "" && oo.notescode !== "") ? "," : "") + oo.notescode; 
				if(oo.canceldate === "0000-00-00 00:00:00" || oo.status !== "cancel") 
					oo.canceldate = "";
				else if(oo.canceldate !== "")
				 	{ 
				 	c = ""; 
				 	tt = oo.canceldate.split(" "); 
				 	uu = tt[1].split(":"); 
				 	uu[0] = (parseInt(uu[0]) + 8); 
				 	if(uu[0] > 24) uu[0] = 24; 
				 	if(uu[0] < 10) c = "0"; 
				   	oo.canceldate = tt[0] + ', ' + c + uu.join(":"); 
				   	}
			 	self.setType(oo);
				});
			response.data.bookings = names;
			return response;
			});
		};
		
	this.readmodifBooking = function(restaurant, mode) {			
		return $http.post('../api/visit/modif/', { 'restaurant': restaurant, 'mode': mode, 'token' : token } ).then(function(response) {
			return response.data.data;
			})
		};
        
//        this.readDepBooking =function(depsoitid){
//            return $http.post('../api/visit/deposit/', { 'depositid': depsoitid} ).then(function(response) {
//			return response.data.data;
//			})
//           
//        };

	this.readBooking = function(restaurant, mode, options) {
		if(!options) options = "";	
		return $http.post('../api/visit/read/', { 'restaurant': restaurant, 'mode': mode, 'options': options, 'token' : token } ).then(function(response) {
			var x, names, i, k, tt, uu, oo, c, validOptions = ['event', 'duration', 'captain', 'cleartime', 'seated', 'repeat', 'notifysmswait', 'notestext', 'notescode', 'validate', 'statepos', 'orders'];
			if(response.data.status < 0)
				return response.data;
				
			names = response.data.data;
			names.map(function(oo, i) {
			  	try {
				oo.restaurant = oo.resto;
				oo.title = oo.restaurant.substring(8, 34);
				oo.platform = self.getPlatform(oo.browser);
				oo.navigator = self.getBrowser(oo.browser);
				oo.index = i + 1;
				oo.time = oo.time.substr(0, 5);
				oo.vtime = (parseInt(oo.time.substr(0, 2)) * 60) + (parseInt(oo.time.substr(3, 2)));
				oo.slot = oo.time.timetoslot();
				oo.slot15 = oo.time.timeto15slot();
				oo.date = oo.date.substring(0,10);
				oo.cfulldate = oo.createdate;
				oo.ctime = oo.createdate.substring(11,16);
				oo.cdate = oo.createdate.substring(0,10);
				oo.vcdate = oo.cdate.jsdate().getTime();
				oo.vdate = oo.date.jsdate().getTime();
				oo.datetime = oo.date.jsdate().getTime();
				oo.ddate = oo.date.jsdate().getDateFormat('-');
				oo.sdate = oo.ddate.substring(0, 5).replace(/-/g, "/");
				oo.mealtype = (parseInt(oo.time.substr(0, 2)) < 16) ? "lunch" : "dinner";
				oo.fullname = oo.first + ' ' + oo.last;
				oo.iplong = long2ip(oo.ip);
				oo.options = {};
				oo.validate = "";
				oo.event = "";
				oo.duration = "";
				oo.cleartime = "";
				oo.repeat = "";
				oo.lastvisit = "";
				oo.depositid = oo.booking_deposit_id;				
				oo.paymethod = (oo.payment_method === "paypal") ? oo.payment_method  : "credit card" ;
                  
				if(oo.more && typeof oo.more === "string" && oo.more !== "") {
					oo.options = self.parseObj(oo.more);   
					Object.keys(oo.options).map(function(x) { if(validOptions.indexOf(x) >= 0) oo[x] = oo.options[x]; });                              
					}
				if(!oo.notestext) oo.notestext = "";
				if(!oo.notescode) oo.notescode = "";
				oo.fullnotes = oo.notestext + ((oo.notestext !== "" && oo.notescode !== "") ? "," : "") + oo.notescode; 

				if(oo.pers !== "" && oo.pers.substr(0, 4) === "more") oo.pers = "99";
				else if(oo.pers === "") oo.pers = "0";
				self.setTracking(oo);
				if(oo.state === "")
					oo.state = (oo.type === "waiting") ? "waiting" : "to come";
				if(oo.state === "waiting" && oo.bkstatus === "") {
					oo.bkstatus = "notify";
					if(typeof oo.notifysmswait === "string" && parseInt(oo.notifysmswait) > 0)
						oo.bkstatus = "notify " + parseInt(oo.notifysmswait);
					}
				if (oo.wheelwin == "" && oo.bkstatus == "cancel" || oo.bkstatus == "noshow" )
                                    
					oo.wheelwin = oo.bkstatus;
				if(oo.canceldate === "0000-00-00 00:00:00" || oo.bkstatus !== "cancel") 
					oo.canceldate = "";
		 		oo.test = (oo.first.toLowerCase() === "test" || 
		 	   					oo.last.toLowerCase() === "test" || 
		 	   					oo.comment.toLowerCase() === "test" || 
		 	   					oo.comment.toLowerCase().indexOf("test ") >= 0 );
		 	   	oo.funkitchen = (weeloy_demo_restaurant.indexOf(oo.restaurant) > -1);
		 	   	self.setType(oo);
				} catch(e) { console.error('READBOOKING', e); }
			  });		
			return response.data.data;
			})
		};
	
	this.getdebugErrorCount = function(email) {
		return $http.post("../api/services.php/debugerror/count/", { 'email' : email, 'token' : token }).success(function (response) { return response.data; });
		};

	this.parseObj = function(str) {
		if(typeof str !== "string" || str === "")
			return {};
		
		str = str.replace(/’/g, "\"");
		if(str !== '')
			str = str.replace(/\s+/g, " ");
		try {
		var oo = JSON.parse(str);
		} catch(err) { console.log(err, str); return {}; }
		return (oo) ? oo : {};
		};
		
	this.setTracking = function(oo) {
		var i, tt, k, trackval = [ "remote", "waiting", "tms", "callcenter", "walkin", "website", "facebook", "partner", "cpp_credit_suisse", "weeloy" ];
		var validtype = ['thirdparty', 'walkin', 'import'];
		
		if(validtype.indexOf(oo.type) >= 0) return;

		tt = oo.tracking.trim();
		if(tt !== '') tt = tt.toLowerCase();
		if(tt.indexOf("|") < 0 && trackval.indexOf(tt) >= 0)
			return oo.type = tt;
		else if(tt.indexOf("|") >= 0) {
			ttAr = tt.split("|");
			for(i = 0; i < trackval.length; i++) 
				if(ttAr.indexOf(trackval[i]) >= 0) {
					return oo.type = trackval[i];
					}
			}
		};
					
	this.setType = function(oo) {
		oo.sudotype = oo.type;
		
		if(oo.type === 'thirdparty')
			oo.sudotype = oo.booker;
		if(oo.tracking.length > 0 && oo.tracking.search("IMASIA") > -1) {
			if(oo.tracking.search("IMASIA|facebook") > -1) oo.sudotype = "imasiafb";
			else if(oo.tracking.search("IMASIAEDM") > -1) oo.sudotype = "imasiaedm";
			else oo.sudotype = "imasia";
			}
		};
			
	this.getAlloteYear = function(restaurant, year) {

		var url = '../api/getallote/' + restaurant + '/' + year;
		return $http.get(url).then(function(response) {
			return response.data;
			});
		};

	this.readAlloteYear = function(restaurant, year, product) {
		if(typeof product !== "string") product = "";
		return $http.post("../api/getallote/",
			{
				'restaurant' : restaurant,
				'product' : product,
				'year' : year
			}).then(function(response) { return response.data; });
		};

	this.readtwositting = function(restaurant, year) {
		return $http.post("../api/allote/read/twositting/",
			{
				'restaurant' : restaurant,
				'year' : year,
				'token'  : token
			}).then(function(response) { return response.data; });
		};

	this.writetwositting = function(restaurant, value, year) {
		return $http.post("../api/allote/write/twositting/",
			{
				'restaurant' : restaurant,
				'value' : value,
				'year' : year,
				'token'  : token
			}).then(function(response) { return response.data; });
		};
				
	this.saveAlloteYear = function(restaurant, year, size, start, ldata, ddata, product) {
		return $http.post("../api/setallote/",
			{
				'restaurant' : restaurant,
				'start' : start,
				'size' : size,
				'year' : year,
				'ldata' : ldata,
				'ddata' : ddata,
				'product' : product,
				'token'  : token
			}).then(function(response) {return response;});
		};
				
	this.getdispoDay = function(restaurant, adate, product) {
		return $http.post("../api/restaurant/dispoday/",
			{
				'restaurant': restaurant,
				'adate' : adate,
				'product' 	: product,
				'token'  : token
			}).then(function(response) {return response.data;})
	};

	this.savechgstateservice = function(restaurant, booking, state, token) {
		return $http.post("../api/bookingservice/state/",
			{
				'restaurant': restaurant,
				'booking' : booking,
				'state' 	: state,
				'token'  : token
			}).then(function(response) {return response.data;})
	};

	this.savechgtableservice = function(restaurant, booking, tablename, captain, token) {
		return $http.post("../api/bookingservice/table/",
			{
				'restaurant': restaurant,
				'booking' : booking,
				'table' 	: tablename,
				'captain' : captain,
				'token'  : token
			}).then(function(response) {return response.data;})
	};


	this.savechgfieldservice = function(restaurant, booking, value, field, token) {
		return $http.post("../api/bookingservice/field/",
			{
				'restaurant': restaurant,
				'booking' : booking,
				'field' 	: field,
				'value' 	: value,
				'token'  : token
			}).then(function(response) {return response.data;})
	};


	this.cancelbackoffice = function(restaurant, booking, email) {
		return $http.post("../api/booking/cancelbackoffice/",
			{
				'restaurant': restaurant,
				'booking' : booking,
				'email' 	: email,
				'token'  : token
			}).then(function(response) {return response.data;})
        };
        
    this.cancelrefundbackoffice = function(deposit,restaurant, booking, email, amount,payment_method) {
		return $http.post("../api/visit/booking/payment/cancel/",
			{
				'restaurant': restaurant,
				'booking' : booking,
				'email'   : email,
				'deposit' :deposit,
				'amount'  :amount,
				'payment_method':payment_method,
				'token'   : token
			}).then(function(response) {return response.data;})
        };
        
        
        this.logbookingevent = function(bookid){
            return $http.post("../api/v2/system/tracking/log/getbooking",
			{
                       'bookid' : bookid,
                       'token'   : token
			}).then(function(response) {return response.data;})
            
        };
            
    this.bookingrefund = function (restaurant, booking, email, deposit,amount,payment_method) {
         
                var API_URL ="../api/v2/payment/refund/"+restaurant;
                 return $http({
                    url: API_URL,
                    method: "POST",
                    data: $.param({
                         'restaurant': restaurant,
                         'booking' : booking,
                         'email'   : email,
                         'deposit' :deposit,
                         'amount'  :amount,
                         'payment_method':payment_method,
                         'token'   : token
                }),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
             }).then(function(response) {return response;})
             
    };         

    this.getCancelPolicy = function (restaurant, payment_method,amount,product) {
            return $http.post("../api/services.php/cancelpolicy/list",
                    {
                        'restaurant': restaurant,
                        'type': payment_method,
                        'amount':amount,
                        'product':product,
                    }).then(function (response) {
                return response.data;
            });
        };
    this.getBkChargeDetails = function (confirmation) {
        var url = "../api/payment/chargedetails/" + confirmation ;

            return $http.get(url).then(function(response) {
                    return response.data;
                    });

    };

        
    // this.bacchanaliacancelpolicy = function(restaurant,date,time,pax){
    //           var API_URL = "../api/v2/restaurant/cancelpolicy/getPolicy";
    //            return $http({
    //                 url: API_URL,
    //                 method: "POST",
    //                 data: $.param({
    //                         'restaurant': restaurant,
    //                         'date' 	: date,
		  //           'time'	: time,
    //                         'pax' : pax
    //             }),
    //             headers: {
    //                 'Content-Type': 'application/x-www-form-urlencoded'
    //             }
    //          }).then(function (response) {
    //             return response.data;
    //         });
    //    };
    //    this.medinicancelpolicy = function(restaurant,payment_method,amount,date,time,pax){
    //           var API_URL = "../api/v2/restaurant/cancelpolicy/read";
    //            return $http({
    //                 url: API_URL,
    //                 method: "POST",
    //                 data: $.param({
    //                                 'restaurant': restaurant,
    //                                 'type': payment_method,
    //                                 'amount': amount,
    //                                 'product' :'',
    //                                 'date' 	: date,
    //                                 'time'	: time,
    //                                 'cover' : pax
    //                     }),
    //             headers: {
    //                 'Content-Type': 'application/x-www-form-urlencoded'
    //             }
    //          }).then(function (response) {
    //             return response.data;
    //         });
    //    };
   
                        
        
        this.bookingnoshow = function(restaurant, booking, email) {
		return $http.post("../api/booking/noshowbackoffice/",
			{
				'restaurant': restaurant,
				'booking' : booking,
				'email' 	: email,
				'token'  : token
			}).then(function(response) {  return response.data;})
        };
        
        this.bookingpaymentnoshow = function(restaurant, booking, email,deposit,amount,payment_method) {
		return $http.post("../api/booking/noshow/payment/",
			{
				'restaurant': restaurant,
				'booking' : booking,
				'email' 	: email,
                                'deposit' :deposit,
                                'amount'  :amount,
                                'payment_method':payment_method,
				'token'  : token
			}).then(function(response) {  return response.data;})
        };
        this.menureminderlink = function(restaurant, booking, email) {
		return $http.post("../api/booking/reminder/menulink/",
			{
				'restaurant': restaurant,
				'booking' : booking,
				'email' 	: email,
				'token'  : token
			}).then(function(response) {  return response.data;})
        };
        
         this.setmenuformstatus = function(restaurant, booking, email,menuform) {
		return $http.post("../api/booking/setmenuformstatus",
			{
				'restaurant': restaurant,
				'booking' : booking,
				'email' 	: email,
                                'menuform' :menuform,
				'token'  : token
			}).then(function(response) {  return response.data;})
        };
        
        this.sendForm = function(restaurant,email,backofficeemail) {
		return $http.post("../api/services.php/evmanagement/form/request/",
			{
				'restaurant': restaurant,
                                'email' 	: email,
                                'backofficeemail' :backofficeemail,
   				'token'  : token
			}).then(function(response) {  return response.data;})
        };
        
        this.resendEmail = function(restaurant,booking,email,recipient) {
		return $http.post("../api/booking/payment/resendemail/",
			{
                            'restaurant': restaurant,
                            'email' 	: email,
                            'booking' :booking,
                            'recipient' :recipient,
                            'token'  : token
			}).then(function(response) {  return response.data;})
        };
        
        this.updateStatus = function(restaurant,booking,email) {
		return $http.post("../api/booking/payment/updatestatus/",
			{
                            'restaurant': restaurant,
                            'email' 	: email,
                            'booking' :booking,
                            'token'  : token
			}).then(function(response) {  return response.data;})
        };
        
        this.resendsms = function(restaurant,mobile,sender,type,booking,email) {
       
                  
            return $http.post("../api/v2/notification/sms/send",
			{
			 'restaurant': restaurant,
                                'mobile':mobile,
                                'msg' :'',
                                'type':type,
                                'sender' 	: sender,
                                'ref_id' :booking,
                                'email' :email,
                                'token'  : token
			}).then(function(response) { return response.data;});
        };

	this.modifbackoffice = function(restaurant, booking, email, cover, time, date, comment, notestext, notescode) {
		if(typeof notestext === "undefined") notestext = "";
		if(typeof notescode === "undefined") notescode = "";
		
		return $http.post("../api/booking/modifbackoffice/",
			{
				'restaurant': restaurant,
				'booking' : booking,
				'email' 	: email,
				'cover' 	: cover,
				'time' 	: time,
				'date' 	: date,
				'comment' 	: comment,
				'notestext' : notestext,
				'notescode' : notescode,
				'token'  : token
			}).then(function(response) { return response.data;})
		};

	this.modifullbooking = function(restaurant, booking, email, obj, dnotify) {
		if(!dnotify) dnotify = false;
		return $http.post("../api/booking/modifullbooking/",
			{
				'restaurant': restaurant,
				'booking' : booking,
				'email'   : email,
				'obj' 	: obj,
				'dnotif' 	: dnotify,
				'token'  : token
			}).then(function(response) { return response.data;})
		};

	this.bookingemail = function(restaurant, email) {
		return $http.post("../api/booking/email/",
			{
				'restaurant': restaurant,
				'email' 	: email,
				'token'		: token
			}).then(function(response) { return response.data;})
		};

	this.bookingmobile = function(restaurant, mobile) {
		return $http.post("../api/booking/mobile/",
			{
				'restaurant': restaurant,
				'mobile' 	: mobile,
				'token'		: token
			}).then(function(response) { return response.data;})
		};
		
	this.createWalking = function(restaurant, obj) {
		return $http.post("../api/booking/createwalking/",
			{
				'restaurant': restaurant,
				'obj' 	: obj,
				'token'  : token
			}).then(function(response) { return response.data;})
		};
	
	this.readBkgohbs = function(restaurant, email) {
		return $http.post("../api/services.php/bkgohbs/read/", 
			{
				'restaurant': restaurant,
				'email' : email,
				'token'  : token
			}).then(function(response) { return response.data.data.data; })
		};
				
	this.updateBkgohbs = function(restaurant, email, ohbs) {
		return $http.post("../api/services.php/bkgohbs/update/",
			{
				'restaurant': restaurant,
				'email' : email,
				'ohbs' : ohbs,
				'token'  : token
			}).then(function(response) { return response.data;})
		};
	
	this.readPromo = function(restaurant, email) {
		return $http.post("../api/services.php/promo/read/", 
			{
				'restaurant': restaurant,
				'email' : email,
				'token'  : token
			}).then(function(response) { return response.data; })
		};
				
	this.createPromo = function(restaurant, email, promos) {
		return $http.post("../api/services.php/promo/create/",
			{
				'restaurant': restaurant,
				'email' : email,
				'promos' : promos,
				'token'  : token
			}).then(function(response) { return response.data;})
		};
	
	this.updatePromo = function(restaurant, email, promos) {
		return $http.post("../api/services.php/promo/update/",
			{
				'restaurant': restaurant,
				'email' : email,
				'promos' : promos,
				'token'  : token
			}).then(function(response) { return response.data;})
		};
	
	this.setupPromo = function(restaurant, email, id, value) {
		return $http.post("../api/services.php/promo/setuppromo/",
			{
				'restaurant': restaurant,
				'email' : email,
				'promoID' : id,
				'value' : value,
				'token'  : token
			}).then(function(response) { return response.data;})
		};
	
	this.deletePromo = function(restaurant, email, id) {
		return $http.post("../api/services.php/promo/delete/",
			{
				'restaurant': restaurant,
				'email' : email,
				'promoID' : id,
				'token'  : token
			}).then(function(response) { return response.data;})
		};
		
	this.setdefaultPromo = function(restaurant, email, id) {
		return $http.post("../api/services.php/promo/setdefault/",
			{
				'restaurant': restaurant,
				'email' : email,
				'promoID' : id,
				'token'  : token
			}).then(function(response) { return response.data;})
		};
		
	this.readEvent = function(restaurant, email) {
		return $http.post("../api/services.php/event/read/", 
			{
				'restaurant': restaurant,
				'email' : email,
				'token'  : token
			}).then(function(response) { return response.data; })
		};
				
	this.createEvent = function(restaurant, email, events) {
		return $http.post("../api/services.php/event/create/",
			{
				'restaurant': restaurant,
				'email' : email,
				'events' : events,
				'token'  : token
			}).then(function(response) { return response.data;})
		};
	
	this.updateEvent = function(restaurant, email, events) {
		return $http.post("../api/services.php/event/update/",
			{
				'restaurant': restaurant,
				'email' : email,
				'events' : events,
				'token'  : token
			}).then(function(response) { return response.data;})
		};
	
	this.deleteEvent = function(restaurant, email, id) {
		return $http.post("../api/services.php/event/delete/",
			{
				'restaurant': restaurant,
				'email' : email,
				'eventID' : id,
				'token'  : token
			}).then(function(response) { return response.data;})
		};
        this.readEventBkDetails = function(confirmation) {
             var url = "../api/services.php/event/bookingDetails/" + confirmation ;
            return $http.get(url).then(function(response) {
                    return response.data;
                    });
        };

	this.readBlock = function(restaurant, product, email) {
		return $http.post("../api/services.php/block/read/", 
			{
				'restaurant': restaurant,
				'product' : product,
				'email' : email,
				'token'  : token
			}).then(function(response) { return response.data; })
		};
				
	this.createBlock = function(restaurant, email, blocks) {
		return $http.post("../api/services.php/block/create/",
			{
				'restaurant': restaurant,
				'email' : email,
				'blocks' : blocks,
				'token'  : token
			}).then(function(response) { return response.data;})
		};
	
	this.updateBlock = function(restaurant, email, blocks) {
		return $http.post("../api/services.php/block/update/",
			{
				'restaurant': restaurant,
				'email' : email,
				'blocks' : blocks,
				'token'  : token
			}).then(function(response) { return response.data;})
		};
	
	this.deleteBlock = function(restaurant, email, id) {
		return $http.post("../api/services.php/block/delete/",
			{
				'restaurant': restaurant,
				'email' : email,
				'blockID' : id,
				'token'  : token
			}).then(function(response) { return response.data;})
		};

	this.readWheelOffers = function(restaurant, email, wheeltype) {
		return $http.post("../api/services.php/wheel/readoffers/", 
			{
				'restaurant': restaurant,
				'type' : wheeltype,
				'email' : email,
				'token'  : token
			}).then(function(response) { return response.data; })
		};
				
	this.readWheel = function(restaurant, email, wheeltype) {
		return $http.post("../api/services.php/wheel/read/", 
			{
				'restaurant': restaurant,
				'type' : wheeltype,
				'email' : email,
				'token'  : token
			}).then(function(response) { return response.data; })
		};
				
	this.createWheel = function(restaurant, email, wheels, wheeltype) {
		return $http.post("../api/services.php/wheel/create/",
			{
				'restaurant': restaurant,
				'wheels' : wheels,
				'type' : wheeltype,
				'email' : email,
				'token'  : token
			}).then(function(response) { return response.data;})
		};
	
	this.updateWheel = function(restaurant, email, wheels, wheeltype) {
		return $http.post("../api/services.php/wheel/update/",
			{
				'restaurant': restaurant,
				'wheels' : wheels,
				'type' : wheeltype,
				'email' : email,
				'token'  : token
			}).then(function(response) { return response.data;})
		};
	
	this.deleteWheel = function(restaurant, email, wheeltype) {
		return $http.post("../api/services.php/wheel/delete/",
			{
				'restaurant': restaurant,
				'type' : wheeltype,
				'email' : email,
				'token'  : token
			}).then(function(response) { return response.data;})
		};
	
	this.readMenu = function(restaurant, email) {
		return $http.post("../api/services.php/menu/read/", 
			{
				'restaurant': restaurant,
				'email' : email,
				'token'  : token
			}).then(function(response) { return response.data; })
		};
				
	this.createMenu = function(restaurant, email, menus) {
		return $http.post("../api/services.php/menu/create/",
			{
				'restaurant': restaurant,
				'email' : email,
				'menus' : menus,
				'token'  : token
			}).then(function(response) { return response.data;})
		};
	
	this.updateMenu = function(restaurant, email, menus) {
		return $http.post("../api/services.php/menu/update/",
			{
				'restaurant': restaurant,
				'email' : email,
				'menus' : menus,
				'token'  : token
			}).then(function(response) { return response.data;})
		};
	
	this.deleteMenu = function(restaurant, email, id) {
		return $http.post("../api/services.php/menu/delete/",
			{
				'restaurant': restaurant,
				'email' : email,
				'menuID' : id,
				'token'  : token
			}).then(function(response) { return response.data;})
		};
	
	this.readMenuPicture = function(restaurant) {
		var url = '../api/restaurant/media/picture/' + restaurant + '/picture';
		return $http.get(url).success(function(response) {
			return response;
                    });
		};
				
	this.readEvOrdering = function(restaurant, email,type) {
		return $http.post("../api/services.php/evordering/read/", 
			{
				'restaurant': restaurant,
				'email' : email,
                                'type'  : type,
				'token'  : token
			}).then(function(response) {
				return response.data;
				})
		};
				
	this.createEvOrdering = function(restaurant, email, orderings) {
		return $http.post("../api/services.php/evordering/create/",
			{
				'restaurant': restaurant,
				'email' : email,
				'orderings' : orderings,
				'token'  : token
			}).then(function(response) { return response.data;})
		};
	
	this.updateEvOrdering = function(restaurant, email, orderings) {
		return $http.post("../api/services.php/evordering/update/",
			{
				'restaurant': restaurant,
				'email' : email,
				'orderings' : orderings,
				'token'  : token
			}).then(function(response) { return response.data;})
		};
	
	this.updateEvOrderingMenu = function(restaurant, email, orderings) {
		return $http.post("../api/services.php/evordering/menus/update/",
			{
				'restaurant': restaurant,
				'email' : email,
				'orderings' : orderings,
				'token'  : token
			}).then(function(response) { return response.data;})
		};	
		
	this.readEvOrderingMenu = function(restaurant, email, id) {
		return $http.post("../api/services.php/evordering/menus/read/", 
			{
				'restaurant': restaurant,
				'email' : email,
				'orderingID' : id,
				'token'  : token
			}).then(function(response) {
				return response.data;
				})
		};
				
	this.deleteEvOrdering = function(restaurant, email, id) {
		return $http.post("../api/services.php/evordering/delete/",
			{
				'restaurant': restaurant,
				'email' : email,
				'orderingID' : id,
				'token'  : token
			}).then(function(response) { return response.data;})
		};
        
       
        this.evDepositRefund = function (restaurant, order_id, email,amount,payment_method) {
         
                var API_URL ="../api/v2/payment/refund/eventbooking/";
                 return $http({
                    url: API_URL,
                    method: "POST",
                    data: $.param({
                         'restaurant': restaurant,
                         'order_id' : order_id,
                         'email'   : email,
                         'amount'  :amount,
                         'payment_method':payment_method,
                         'token'   : token
                }),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
             }).then(function(response) {return response;})
             
           };
           
           
        this.getEventPayment = function(order_id) {
            var API_URL = '../api/v2/payment/depositdetails/' + order_id ;

            return $http.get(API_URL).then(function(response) {
                return response.data;

            });
   
        };
        
        this.getBolatestnews = function() {
            var type = "backoffice";
            var API_URL = '../api/home/getcategories/' + type;
            return $http.get(API_URL).then(function(response) {
                return response.data;
            });
            
        };
    
    
        this.getRestaurantPayment = function(restaurant) {
            var API_URL = '../api/v2/payment/depositdetails/' + restaurant +"/" ;
            return $http.get(API_URL).then(function(response) {
                return response.data;
                
            });
   
        };
        
        this.getRequestFormList = function(restaurant) {
            var API_URL = '../api/v2/event/management/requestform/'+restaurant+"/" ;
            return $http.get(API_URL).then(function(response) {
                return response.data;
            });
            
        };
       
        this.notifysavemenu = function(items) {
            var API_URL = '../api/v2/event/management/menu/notify/';
            return $http.post(API_URL,
                    {
                             items: items
    
                    }).then(function(response) {  return response.data;})
            
        };
        
        this.updateOrderstatus = function(restaurant,event_id,status,email){
               return $http.post("../api/services.php/evordering/updatestatus/", 
                    {
                        'restaurant': restaurant,
                        'event_id': event_id,
                        'status' : status,
                        'email' : email,
                        'token'  : token
                    }).then(function(response) {
                        return response.data.data;
                   })
            
        }

	this.readOrdering = function(restaurant, email) {
		return $http.post("../api/services.php/ordering/read/", 
			{
				'restaurant': restaurant,
				'email' : email,
				'token'  : token
			}).then(function(response) {
				if(response.data.data === null)
					return null;
					
				names = response.data.data.orders;
				items = response.data.data.orders_items;
				for (i = 0; i < names.length; i++) {
					}		
				return response.data.data;
				})
		};
				
	this.createOrdering = function(restaurant, email, orderings) {
		return $http.post("../api/services.php/ordering/create/",
			{
				'restaurant': restaurant,
				'email' : email,
				'orderings' : orderings,
				'token'  : token
			}).then(function(response) { return response.data;})
		};
	
	this.updateOrdering = function(restaurant, email, orderings) {
		return $http.post("../api/services.php/ordering/update/",
			{
				'restaurant': restaurant,
				'email' : email,
				'orderings' : orderings,
				'token'  : token
			}).then(function(response) { return response.data;})
		};
	
	this.deleteOrdering = function(restaurant, email, id) {
		return $http.post("../api/services.php/ordering/delete/",
			{
				'restaurant': restaurant,
				'email' : email,
				'orderingID' : id,
				'token'  : token
			}).then(function(response) { return response.data;})
		};
	
	this.createInvoice = function(restaurant, date, subtotal, license, currency, segments, details, email) {
		return $http.post("../api/services.php/invoice/create/",
			{
				'restaurant': restaurant,
				'date': date,
				'subtotal': subtotal,
				'license': license,				
				'currency': currency,
				'segments' : segments,
				'details' : details,
				'email' : email,
				'token'  : token
			}).then(function(response) { return response.data;})
		};
	
	this.totalInvoice = function(restaurant, date, total, email) {
		return $http.post("../api/services.php/invoice/total/",
			{
				'restaurant': restaurant,
				'date': date,
				'total': total,
				'email' : email,
				'token'  : token
			}).then(function(response) { return response.data;})
		};
	
	this.readinvoice = function(restaurant, date, email) {
		return $http.post("../api/services.php/invoice/read/", 
			{
				'restaurant': restaurant,
				'date' : date,
				'email' : email,
				'token'  : token
			}).then(function(response) { return response.data.data; });
		};

	this.readallinvoice = function(date, email) {
		return $http.post("../api/services.php/invoice/readall/", 
			{
				'date' : date,
				'email' : email,
				'token'  : token
			}).then(function(response) { return response.data.data; });
		};

	this.updateinvoicestatus = function(restaurant, invoicedate, status, email) {
		return $http.post("../api/services.php/invoice/updatestatus/",
			{
				'restaurant': restaurant,
				'invoicedate' : invoicedate,
				'status': status,
				'email' : email,
				'token'  : token
			}).then(function(response) { return response.data;})
		};
	
	this.createinvConf = function(email, invoice) {
		return $http.post("../api/services.php/invoice/createconf/",
			{
				'email' : email,
				'invoice' : invoice,
				'token'  : token
			}).then(function(response) { return response.data;})
		};

	this.updateinvConf = function(email, invoice) {
		return $http.post("../api/services.php/invoice/updateconf/",
			{
				'email' : email,
				'invoice' : invoice,
				'token'  : token
			}).then(function(response) { return response.data;})
		};
	
	this.readallinvConf = function(email) {
		return $http.post("../api/services.php/invoice/readallconf/", 
			{
				'email' : email,
				'token'  : token
			}).then(function(response) { return response.data.data; });
		};
				
	this.readinvConf = function(restaurant, email) {
		return $http.post("../api/services.php/invoice/readconf/", 
			{
				'restaurant': restaurant,
				'email' : email,
				'token'  : token
			}).then(function(response) { return response.data.data; });
		};
				
	this.deleteinvConf = function(restaurant, email) {
		return $http.post("../api/services.php/invoice/deleteconf/", 
			{
				'restaurant': restaurant,
				'email' : email,
				'token'  : token
			}).then(function(response) { return response.data.data; });
		};
				
	this.readallrestonames = function(email) {
		return $http.post("../api/services.php/restaurant/allnames/", 
			{
				'email' : email,
				'token'  : token
			}).then(function(response) { return response.data.data; });
		};
				
	this.readReview = function(restaurant) {

		var url = '../api/getreviews/' + restaurant;           
		return $http.get(url).then(function(response) {
			var i;
			review = response.data.data.review; 
			for(i = 0; i < review.length; i++) {
				review[i].id = i+1;
				review[i].date = review[i].date.substring(0,10);
				review[i].datetime = review[i].date.jsdate().getTime();
				review[i].ddate = review[i].date.jsdate().getDateFormat('-');
				review[i].grade = parseInt(review[i].grade);
				review[i].food = parseInt(review[i].food);
				review[i].service = parseInt(review[i].service);
				review[i].ambiance = parseInt(review[i].ambiance);
				review[i].price = parseInt(review[i].price);
				review[i].is_responded = 'no';				
				if(typeof review[i].response === 'string' && review[i].response.trim() !== ''){
					review[i].is_responded = 'yes';
					}
				}
			return response.data.data.review;
			})
		};

	this.setresponseReview = function(restaurant, confirmation, response, email) {
		return $http.post("../api/services.php/review/response/",
			{
				'restaurant' : restaurant,
				'confirmation' : confirmation,
				'response' : response,
				'email' : email,
				'token'  : token
			}).then(function(response) { return response.data;})
		};
	
	this.validState = function() {
		return [ 'to come', 'seated', 'paying', 'left', 'no show' ];
		};

	this.normDate = function (dd, reverse, sep) {
		if(typeof dd !== "string" && dd instanceof Date === false) 
			dd = new Date();
			
		if(sep !== '-' && sep !== '/') sep = '-';
		if (typeof dd !== "string")
			return (reverse) ? dd.getDateFormatReverse(sep) : dd.getDateFormat(sep);
			
		dd = (sep == '-') ? dd.replace(/\//g, "-") : dd.replace(/-/g, "/");
		aa = dd.split(sep);
		return (reverse) ? aa[2] + sep + aa[1] + sep + aa[0] :  aa[0] + sep + aa[1] + sep + aa[2];
	};

	this.getNotifytype = function () {
		 var url = '../api/notification/getType';
		   var deferred = $q.defer();
		   $http.get(url, {
				cache: true
			}).success(function(response) {
				deferred.resolve(response);
			});
			return deferred.promise;	  
		};

	 this.updateNotifyAction = function (data) {
		 return $http.post("../api/notification/updateactions", 
			{
					'data' : data
			}).then(function(response) { console.log(response); return response.data; });	  
		};

	this.getNotifyActions = function(restaurant) {
		var url = '../api/notification/getnotiifcation/' + restaurant;
		var deferred = $q.defer();
			$http.get(url, {
				 cache: true
			 }).success(function(response) {
				 deferred.resolve(response);
			 });
		return deferred.promise;	  
		};
        
	//audit log for backoffice
	this.logevent = function(action,event,page) {
		var API_URL ="../api/v2/system/tracking/log";

		 return $http({
			 url: API_URL,
			 method: "POST",
			 data: $.param({
				 'action': action,
				 'event' :event,
				 'page' :page
			 }),
			  headers:  {'Content-Type': 'application/x-www-form-urlencoded'}
			 }).then(function(response) {
				 console.log("auditlog="+response.data);
			  return response.data;
		 	});
		}; 
   
	
	this.ModalDataBooking = function() {

		var i, j, timeslotAr=[], persAr=[], hourslotAr=[], minuteslotAr=[];

		for(i = 1; i < 50; i++) 
			persAr.push(i);
		
		for(i = 9; i < 24; i++)
			for(j = 0; j < 60; j += 15) {
				timeslotAr.push((i < 10 ? '0' : '') + i + ':' + (j === 0 ? '0' : '') + j);
				}
			
		for(i = 9; i < 24; i++) 
			hourslotAr.push((i < 10 ? '0' : '') + i);
		
		for(i = 0; i < 60; i += 5) 
			minuteslotAr.push((i < 10 ? '0' : '') + i);
			
		return  {
			name: "", 
			pers: persAr, 
			timeslot: timeslotAr, 
			hourslot: hourslotAr,
			minuteslot: minuteslotAr,
			ntimeslot: 0,
			event: ['Birthday', 'Wedding', 'Reunion'],
			start_opened: false,
			opened: false,
			selecteddate: null,
			originaldate: null,
			dateFormated: "",
			theDate: null,
			today: new Date(),
			minDate: null,
			maxDate: null,
			deposit:null,
			formats: ['dd-MM-yyyy', 'dd/MMMM/yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'dd-MM-yyyy', 'yyyy-MM-dd', 'dd.MM.yyyy', 'shortDate', 'yyyy-MM-dd', ],
			dateOptions: { formatYear: 'yyyy', startingDay: 1 },
			formatsel: "",
			func: null,
			
			setInit: function(ddate, rtime, func, format, minDate, maxDate) {
				
				this.theDate = null;
				this.func = func;
				this.formatsel = this.formats[format];			
				this.ntimeslot = rtime.substring(0, 5);
			
				if(ddate instanceof Date === false) {
					ddate = ddate.jsdate();
					}
				this.originaldate = ddate;
				this.theDate = ddate;
				this.minDate = minDate;
				this.maxDate = maxDate;
				},
		
			setDate: function(ddate) {
				this.theDate = ddate;
				this.originaldate = ddate; // this is for the case that no data selected
				},
			setMaxDate: function(date) {
				this.maxDate = date;
				if (this.minDate > this.maxDate)
					this.minDate = this.maxDate;
			},
			getDate: function(sep, mode) {
				if(this.theDate && this.theDate instanceof Date)
					return (mode !== 'reverse') ? this.theDate.getDateFormat(sep) : this.theDate.getDateFormatReverse(sep);
				return (mode !== 'reverse') ? this.originaldate.getDateFormat(sep) : this.originaldate.getDateFormatReverse(sep);
				},
			getMaxDate: function() {
				return this.maxDate;
			},
			getTheDate: function() {
				if(this.theDate && this.theDate instanceof Date)
					return this.theDate;
				return this.theDate = this.originaldate = this.today ;
				},
				
			getmTime: function(offset) {
				if(this.theDate && this.theDate instanceof Date) 
					return (this.theDate.getTime() + offset);
				console.log('ERROR getmTime');
				return this.today.getTime();
				},
								
			dateopen: function($event) {
				this.start_opened = true;
				this.opened = true;
				$event.preventDefault();
				$event.stopPropagation();
				},
			
			disabled: function(date, mode) {
				return false;
				},
		
			calendar: function() {
				this.theDate = this.originaldate;
				if(this.theDate instanceof Date)
					this.dateFormated = this.theDate.getDateFormat('/');
				},
		
			onchange: function() {
				this.theDate = this.originaldate;
				if(this.theDate instanceof Date)
					this.dateFormated = this.theDate.getDateFormat('/');

				if(this.func !== null)
					this.func();
				}	
			};
		};
        this.wyMakePayment = function (email,restaurant,amount,controller,return_url) {
                var API_URL = "../api/v2/payment/makepayment/"+restaurant +"/" ;
	
                return $http.post(API_URL,
                    {
                        'email' :email,
                        'restaurant': restaurant,
                        'amount' : amount,
                        'controller' :controller,
                        'return_url' :return_url,
                        'token'  : token
                    }).then(function(response) {  return response.data;})
        };
        this.saveccMember = function (email,restaurant,data) {
                var API_URL = "../api/callcenter/member/save/";
	
                return $http.post(API_URL,
                    {
                        'email' :email,
                        'restaurant': restaurant,
                        'item' : data,
                        'token'  : token
                    }).then(function(response) {  return response.data;})
        };
        this.getCallcentermemberList = function(restaurant) {
            var API_URL = '../api/listcallcentermember/'+restaurant ;
            return $http.get(API_URL).then(function(response) {
                return response.data;
            });
            
        };
        this.getCalls = function(restaurant) {
            var API_URL = '../api/getCalls';
            return $http.get(API_URL).then(function(response) {
                return response.data;
            });
            
        };        
        this.deleteccmember = function (backofficeemail,email,restaurant) {
                var API_URL = "../api/callcenter/member/delete/";
	
                return $http.post(API_URL,
                    {
                        'email' :email,
                        'restaurant': restaurant,
                        'bkemail' : backofficeemail,
                        'token'  : token
                    }).then(function(response) {  return response.data;})
        };
        this.getPaymentPolicy = function (restaurant, payment_method, resBookingDeposit,pproduct,bkdate,bktime,bkcover) {
                              
                var API_URL = "../api/v2/restaurant/cancelpolicy/read";
                    var defferred = $q.defer();
                      $http({
                            url: API_URL,
                            method: "POST",
                            data: $.param({
                                    'restaurant':restaurant,
                                    'type': payment_method,
                                    'amount': resBookingDeposit,
                                    'product':pproduct,
                                    'date' : bkdate,
                                    'time' : bktime,
                                    'cover' :bkcover
                        }),
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        }
                     }).success(function(response) {
                        defferred.resolve(response);
                    });
                     return defferred.promise;
            };
            this.submitNewletter = function(name,email){
                var API_URL = "../api/newsletter/addEmail/" + email;
                  return $http.get(API_URL).then(function(response) {
                        return response.data;
                });
            }
        
        
        
        
       
                

}]);

