<div id='booking' ng-controller='BKReportController' ng-init="moduleName='bkreport'; typeBooking=true;bookingTitle='BOOK OF THE DAY'" >
 <div class="container mainbox">    
	<div class="panel panel-info">

<div class="row">
	<div class="col-md-4">
	<div class="input-group">
		<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
		<input type="text" class="form-control input-sm" ng-model="searchText" ng-change="paginator.setPage(0);" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" style='width:150px;'> 
	</div>
	<span style='font-size:10px;'>selected bookings: <strong> {{filteredBooking.length}} </strong></span>
	</div>
		
	<div class="col-md-1">
	<div class="input-group">
		<span><button  type="button" class="btn btn-default" ng-click="mydatastart.dateopen($event)"><i class="glyphicon glyphicon-calendar"></i></button></span><span style='font-size:10px;'>  start</span>
        <input type="text" class="form-control" uib-datepicker-popup="{{mydatastart.formats[0]}}" ng-model="mydatastart.originaldate"  ng-change='mydatastart.onchange();' is-open="mydatastart.opened" min-date="mydatastart.minDate" max-date="mydatastart.maxDate" datepicker-options="mydatastart.dateOptions" ng-required="true" close-text="Close" style="width:0;opacity:0" />
	</div>
	</div>
	
	<div class="col-md-1">
	<div class="input-group">
		<span><button  type="button" class="btn btn-default" ng-click="mydataend.dateopen($event)"><i class="glyphicon glyphicon-calendar"></i></button></span><span style='font-size:10px;'>  end</span>
        <input type="text" class="form-control" uib-datepicker-popup="{{mydataend.formats[0]}}" ng-model="mydataend.originaldate"  ng-change='mydataend.onchange();' is-open="mydataend.opened" min-date="mydataend.minDate" max-date="mydataend.maxDate" datepicker-options="mydataend.dateOptions" ng-required="true" close-text="Close" style="width:0;opacity:0" />
	</div>
	</div>
	<div class="col-sm-3">
		</form>
		<form action="echo.php" id="extractBkOfDay" name="extractBkOfDay" method="POST" target="_blank">
			<input type="hidden" ng-model='contentBkOfDay' name="contentBkOfDay" id="contentBkOfDay">
			<a href ng-click='extractSelection();' class="btn btn-success btn-sm" style='color:white;'><span class='glyphicon glyphicon-save'></span> &nbsp; Extract Selection</a>
		</form>
	</div>
	<div class="col-md-1">
		<div class="btn-group" uib-dropdown >
			<button type="button" class="btn btn-default btn-xs" uib-dropdown-toggle aria-expanded="false" style='font-size:11px;'>Page Size <span class="caret"></span></button>
			<ul class="dropdown-menu" uib-dropdown-menu role="menu">
				<li ng-repeat="x in paginator.pagerange()" class='glyphiconsize'><a href ng-class="{ 'glyphicon glyphicon-ok glyphiconsize': x === paginator.getRowperPage() }" href ng-click="paginator.setRowperPage(x)"> {{x}}</a></li>
			</ul>
		</div> 		
	</div>
	
</div>
<div class="row" ng-if="startBkg !== -1" style="font-size:11px;">
start: {{ mydatastart.originaldate.getDateFormat() }} &nbsp;&nbsp;&nbsp; end: {{ mydataend.originaldate.getDateFormat() }} 
<select-create-booking-date theday="selDayObj"></select-create-booking-date>
<div class="row"> &nbsp; </div>
</div>

<div class="col-md-12" ng-show='listingVisitFlag'>
	
<table width='100%' class="table table-condensed table-striped" style='font-size:11px;'>
	<thead><tr><th ng-repeat="y in tabletitle"><tbtitle var="{{y.a}}" name='tabletitle' module='moduleName'/></th></tr></thead>
	<tbody>
	<tr ng-repeat="x in filteredBooking = (booking | filter:searchText | filterBymyDate:startBkg:endBkg:selDayObj.field) | sizefilter:this | orderBy:predicate:reverse | slicefilter:paginator.getPage():paginator.getRowperPage():paginator.getItemCount() | limitTo:paginator.getRowperPage()">
		<td ng-repeat="z in tabletitle"><tb-listingbkg><a href ng-click="view(x)">{{ x[z.a] | adatereverse:z.c }}</a></tb-listingbkg></td>
	</tr>
	<tr><td colspan='{{tabletitle.length}}'></td></tr>
</table>

<div ng-if="filteredBooking.length >= paginator.getRowperPage()" class="col-md-12" align="center" style='font-size:11px' ng-include="'inc/paginator.html'"></div>

</div>

<div class="col-md-12" ng-hide='listingVisitFlag'>
	<br />
	<a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a>
	<br />
	<table class='table-striped' style="margin: 0 0 150px 30px;font-size:13px;font-family: Roboto">		
		<tr ng-repeat="y in tabletitleContent | filter:showtest"><td nowrap><strong>{{ y.b }} : </strong></td><td> &nbsp; </td><td>{{ selectedItem[y.a] }}</td></tr>
		<tr ng-show="formatDisplay == 'today'"><td nowrap><strong>State: </strong></td><td> </td><td style='color:red;'>{{ x.state}}</td></tr>
		<tr><td>&nbsp;</td><td> &nbsp; </td><td></td></tr>
	</table>
</div>


</div>
</div>
</div>


<script>


<?php
echo "var token = '" . $_SESSION['user_backoffice']['token'] . "';";
echo "var email = '" . $_SESSION['user_backoffice']['email'] . "';";
?>

app.controller('BKReportController', ['$scope', 'bookService', 'extractService', function($scope, bookService, extractService) {

	var forceDate = new Date(new Date().getFullYear()-2, 0, 1);
	$scope.paginator = new Pagination(50);
	$scope.whenday = 32;
	$scope.reporttype = "co";

	$scope.getAlignment = bkgetalignment;

	var newdate = new Date();
	var startdate, endate;
	if(newdate.getMonth() > 6) {
		startdate = new Date(newdate.getFullYear(), 0, 1);
		endate = new Date(newdate.getFullYear()+1, 11, 31);
	} else {
		startdate = new Date(newdate.getFullYear()-1, 0, 1);
		endate = new Date(newdate.getFullYear(), 11, 31);
		}
	$scope.startBkg = -1;
	$scope.clearDates = function() { $scope.startBkg = -1; };
	$scope.endBkg = newdate.getTime();
	$scope.cdate = newdate;
	$scope.mydatastart = new bookService.ModalDataBooking();
	$scope.mydataend = new bookService.ModalDataBooking();
	$scope.mydatastart.setInit(forceDate, "09:00", function() { $scope.startBkg = $scope.mydatastart.getmTime(-86400000); $scope.paginator.setPage(0); }, 3, startdate, endate);
	$scope.mydataend.setInit(forceDate, "09:00", function() { $scope.endBkg = $scope.mydataend.getmTime(0); $scope.paginator.setPage(0); }, 3, startdate, endate);
	$scope.mydataend.setDate(newdate);

	$scope.email = <?php echo "'" . $email . "';"; ?>
	$scope.listingVisitFlag = true;
	$scope.selDayObj = { selectedDay: null, datemodelist: ['booking date', 'create date'], datemode: "", showtitle: true, field: "vcdate", resetfunc: $scope.clearDates }; 
	$scope.selDayObj.datemode = $scope.selDayObj.datemodelist[1];
	$scope.predicate = $scope.oldpredicate = '';
	$scope.reverse = false;
	$scope.filteredBooking = [];
	$scope.tabletitle = [ {a:'platform', b:'platform', c:'', l:'' , q:'down', cc: 'black' }, {a:'confirmation', b:'Confirmation', c:'', l:'' , q:'down', cc: 'black' }, {a:'restaurant', b:'Restaurant', c:'', l:'120' , q:'down', cc: 'black' },  {a:'type', b:'Type', c:'', l:'' , q:'down', cc: 'black' },  {a:'cover', b:'Pax', c:'', l:'' , q:'down', cc: 'black' },  {a:'rtime', b:'Time', c:'', l:'' , q:'down', cc: 'black' }, {a:'fullname', b:'Name', c:'', l:'70' , q:'down', cc: 'black' },  {alter:'vdate', a:'rdate', b:'BookDate', c:'date', l:'' , q:'down', cc: 'black' },  { alter:'vcdate', a:'cdate', b:'CreateDate', c:'date', l:'' , q:'up', cc: 'orange' },  {a:'status', b:'Status', c:'', l:'' , q:'down', cc: 'black' },  {a:'wheelwin', b:'Wins', c:'', l:'40' , q:'down', cc: 'black' } ];
	$scope.bckups = $scope.tabletitle.slice(0);
	$scope.tabletitleContent = [ { a:'restaurant', b:'Restaurant', c: true }, { a:'confirmation', b:'Booking Confirmation', c: true }, { a:'type', b:'Booking Type', c: true }, { a:'status', b:'Status', c: true }, { a:'rdate', b:'Reservation Date', c: true }, { a:'rtime', b:'Reservation Time', c: true }, { a:'cover', b:'Number of Persons', c: true }, { a:'fullname', b:'Name', c: true }, { a:'email', b:'Email', c: true }, { a:'mobile', b:'Mobile', c: true }, { a:'cfulldate', b:'Create Date', c: true }, { a:'canceldate', b:'Cancel date', c: true }, { a:'comment', b:'Request', c: false }, { a:'wheelwin', b:'Win', c: true }, { a:'tracking', b:'Tracking', c: false }, { a:'booker', b:'Staff', c: false }, { a:'company', b:'Company', c: false }, { a:'yes', b:'Hotel Guest', c: false }, { a:'state', b:'State', c: false }, { a:'restCode', b:'restCode', c: true }, { a:'membCode', b:'membCode', c: true }, { a:'specialrequest', b:'Special Request', c: false }, { a:'tablename', b:'Table', c: false }, { a:'browser', b:'Browser', c: false }, { a:'language', b:'Language', c: false }, { a:'review_status', b:'Review', c: false }, { a:'iplong', b:'IP', c: false } ];
	$scope.selectedItem = null; // take the orgnames as names get modified with the shortcuts
		 
	$scope.initorder = function() {
		$scope.tabletitle = $scope.bckups;
		$scope.predicate = $scope.oldpredicate = "vcdate";
		$scope.reverse = true;
	};

	$scope.reorder = function(item, alter) {
		alter = alter || "";
		if (alter !== "")  item = alter;
		$scope.reverse = ($scope.predicate === item) ? !$scope.reverse : false;
		$scope.predicate = item;
	};
				
	$scope.filterdate = function(item) {
		if(!$scope.selDayObj.selectedDay)
			return true;
		var prop = ($scope.selDayObj.datemode === $scope.selDayObj.datemodelist[1]) ? "vcdate" : "vdate";	//"create date"
		var dd = $scope.selectedvDay;
		return (item[prop] && item[prop] === dd);
		};

	$scope.setfilterdate = function() {
		$scope.aSelDay = $scope.mydata.originaldate;
		$scope.selDayObj.selectedDay = $scope.mydata.originaldate.getDateFormatReverse('-');
		$scope.selectedvDay = $scope.selDayObj.selectedDay.jsdate().getTime();
		$scope.paginator.setPage(0);
		};

	bookService.readDayreport($scope.whenday, $scope.reporttype, $scope.email, '').success(function(response) { 	 
		 var i, obj, first, last;
		 $scope.booking = [];

		if(response.status < 0)
			return response;

		 $scope.rdata = response.data.bookings; 
		 for(i = 0; i < $scope.rdata.length; i++) {
		 	obj = $scope.rdata[i];
			if(obj.test || obj.funkitchen)
				continue; 
                                                
			obj.index = i+1; 
			obj.cfulldate = obj.cdate + ', ' + obj.ctime;
			obj.cdtime = obj.cdate.jsdate().getTime() + (parseInt(obj.ctime.substr(0,2)) * 60) + parseInt(obj.ctime.substr(3,2));
			obj.ddate = obj.rdate.jsdate().getDateFormat('-');
			obj.cddate = obj.cdate.jsdate().getDateFormat('-');
			$scope.booking.push(obj);
			}
		$scope.paginator.setItemCount($scope.booking.length);
		$scope.paginator.setPage(0);
		$scope.initorder();
		});

	$scope.showtest = function(y) {
		if(y.c === true || $scope.selectedItem === null) 
			return true;
		return ($scope.selectedItem[y.b] !== '');
	};

	$scope.view = function(oo) {			
		$scope.selectedItem = oo; // take the orgnames as names get modified with the shortcuts
		$scope.listingVisitFlag = false;
		};
		
	$scope.backlisting = function() {
		$scope.listingVisitFlag = true;
		};

	$scope.compareFunc = function(a, b) {
		var log = a.restaurant.localeCompare(b.restaurant);
		if(log !== 0) return log;
		
		return a.cdtime - b.cdtime;
		};

	$scope.extractSelection = function() {

		var exportselect = $scope.filteredBooking;
		var i, j, u, data, maxlimit = 1500, limit, titleAr, contentAr;
		var filename, cdate = new Date();

		titleAr = ["restaurant", "confirmation", "date", "time", "pax", "mealtype", "salutation", "firstname", "lastname", "phone", "request", "state", "status", "email", "restCode", "win", "tablename", "type", "createdate", "booker", "company", "hotelguest", "tracking", "platform"];
		contentAr = ["restaurant", "confirmation", "rdate", "rtime", "cover", "mealtype", "salutation", "firstname", "lastname", "mobile", "specialrequest", "state", "status", "email", "restCode", "wheelwin", "tablename", "type", "cdate", "booker", "company", "hotelguest", "tracking", "platform"];

		data = titleAr.join(",");
		limit = exportselect.length;		
		if (limit > maxlimit) limit = maxlimit;
		for (i = 0; i < limit; i++) {
			data += "\n";			
			u = exportselect[i];
			for (j = 0; j < contentAr.length; j++) {
				if(contentAr[j] !== 'hotelguest')
					data += extractService.filter(u[contentAr[j]]) + ",";
				else data += ((u.hotelguest === "1") ? "yes" : "") + ",";
				}
			}	
	
		filename = "bookingsOfTheday" + cdate.getDate() + cdate.getMonth() + cdate.getFullYear() + ".csv";
		extractService.save(filename, data); 				
		};

}]);

</script>