
<div class="container">
    <div class="row">
        <div class="col-md-12 left-sec">

<div id="Bkgohbsbackoffice" ng-controller="MainControllerBkgohbs" ng-init="listingFlag=true; moduleName='review'" >

	<div class="col-md-8"></div>
	<div class="col-md-2">
		<div class="btn-group" uib-dropdown >
			<button type="button" class="btn btn-success btn-xs" uib-dropdown-toggle style='font-size:13px;'>set day <span class="caret"></span></button>
			<ul class="dropdown-menu" uib-dropdown-menu role="menu">
				<li ng-repeat="x in weekdays | filter:{day:''}" ng-if="x.day!==''" class='glyphiconsize'><a href ng-click="setday(x.day, x.mealtype)"> {{x.day}} {{x.mealtype}}</a><br /></li>
			</ul>
		</div> 		
	</div>
	<div class="col-md-2">
		<div class="btn-group" uib-dropdown >
			<button type="button" class="btn btn-warning btn-xs" uib-dropdown-toggle style='font-size:13px;'>clear day <span class="caret"></span></button>
			<ul class="dropdown-menu" uib-dropdown-menu role="menu">
				<li ng-repeat="x in weekdays | filter:{day:''}" ng-if="x.day!==''" class='glyphiconsize'><a href ng-click="clearday(x.day, x.mealtype)"> {{x.day}} {{x.mealtype}}</a><br /></li>
			</ul>
		</div> 		
	</div>
	<div class="col-md-2"></div>

	<div class="col-md-12">
	<br><br>
	<table width='100%' class="table table-condensed table-striped table-hover" style='font-size:9px;'>
		<tr ng-repeat="y in weekdays track by $index">
			<td style='font-size:11px'><strong>{{y.day}}{{y.sep}}{{y.mealtype}}</strong></td><td ng-repeat="z in y.hours track by $index" nowrap>&nbsp;<span ng-if="z.text!==''"><input type="checkbox" ng-model="data[y.day+'_'+z.ind]" value="1">{{ z.text }} </span></td>
		</tr>
	</table>

	<a href ng-click='updatehobs();' class="btn btn-success btn-sm" style='color:white;width:200px;'><span class='glyphicon glyphicon-save'></span> &nbsp;{{ buttonlabel }} </a><br /><br /><br /><br />
	</div>
	
</div>

</div>
</div>
</div>
            
<script>

var restaurant = <?php echo "'" . $theRestaurant . "';"; ?>
var email = <?php echo "'" . $email . "';"; ?>
var token = <?php echo "'".$_SESSION['user_backoffice']['token']."';"; ?>

app.controller('MainControllerBkgohbs', ['$scope', '$http', 'bookService', function($scope, $http, bookService) {
   var i, lhours, dhours, ehours;
   
   $scope.restaurant = restaurant;
   $scope.email = email;

   lhours = (function() { var i, arr=[]; for(i = 9; i < 16; i++)  { arr.push( {text:i + ':00', ind:(i * 2) } );  arr.push({ text:i + ':30', ind:(i * 2) + 1} ); } arr.push({ text:'', ind:-1 }); arr.push({ text:'', ind:-1 }); return arr; })();	
   dhours = (function() { var i, arr=[]; for(i = 16; i < 24; i++) { arr.push( {text:i + ':00', ind:(i * 2) } );  arr.push({ text:i + ':30', ind:(i * 2) + 1} ); } return arr; })();
   ehours = (function() { var i, arr=[]; for(i = 16; i < 24; i++) { arr.push( {text:'', ind:-1 } ); arr.push({text:'', ind:-1 } ); } return arr; })();	
   $scope.weekdays = [ { day:'Sunday', mealtype:'lunch', hours: lhours, sep: '-' }, { day:'Sunday', mealtype:'dinner', hours: dhours, sep: '-' }, { day:'', mealtype:'', hours: ehours, sep: '' }, { day:'Monday', mealtype:'lunch', hours: lhours, sep: '-' }, { day:'Monday', mealtype:'dinner', hours: dhours, sep: '-' }, { day:'', mealtype:'', hours: ehours, sep: '' }, { day:'Tuesday', mealtype:'lunch', hours: lhours, sep: '-' }, { day:'Tuesday', mealtype:'dinner', hours: dhours, sep: '-' }, { day:'', mealtype:'', hours: ehours, sep: '' }, { day:'Wednesday', mealtype:'lunch', hours: lhours, sep: '-' }, { day:'Wednesday', mealtype:'dinner', hours: dhours, sep: '-' }, { day:'', mealtype:'', hours: ehours, sep: '' }, { day:'Thursday', mealtype:'lunch', hours: lhours, sep: '-' }, { day:'Thursday', mealtype:'dinner', hours: dhours, sep: '-' }, { day:'', mealtype:'', hours: ehours, sep: '' }, { day:'Friday', mealtype:'lunch', hours: lhours, sep: '-' }, { day:'Friday', mealtype:'dinner', hours: dhours, sep: '-' }, { day:'', mealtype:'', hours: ehours, sep: '' }, { day:'Saturday', mealtype:'lunch', hours: lhours, sep: '-' }, { day:'Saturday', mealtype:'dinner', hours: dhours, sep: '-' } ];
   $scope.buttonlabel = "update";
   $scope.data = {};
   $scope.aweek = [];
   
	$scope.getAlignment = bkgetalignment;

   for(i = 0; i < $scope.weekdays.length; i++) {
   		if($scope.weekdays[i].day !== "" && $scope.aweek.indexOf($scope.weekdays[i].day) < 0)
   			$scope.aweek.push($scope.weekdays[i].day)
		}
			   
  $scope.resetdata = function() {
	var i, j, label;

	for(i = 0; i < $scope.aweek.length; i++) {
		for(j = 0; j < lhours.length; j++) 
			if(lhours[j].ind >= 0) {
				label = $scope.aweek[i] + '_' + lhours[j].ind;
				$scope.data[label] = true;
				}
		for(j = 0; j < dhours.length; j++) 
			if(dhours[j].ind >= 0) {
				label = $scope.aweek[i] + '_' + dhours[j].ind;
				$scope.data[label] = true; 
				}  		
		}
  	}
  	
  $scope.updateday = function(day, meal, value) {
  	var j, dd = (meal === "lunch") ? lhours : dhours;
  	
   	for(j = 0; j < dd.length; j++) 
		if(dd[j].ind >= 0) {
			label = day + '_' + dd[j].ind;
			$scope.data[label] = value;
			}
  	}
  	
  $scope.setday = function(day, meal) {
	return $scope.updateday(day, meal, true);
  	}
  	
  $scope.clearday = function(day, meal) {
	return $scope.updateday(day, meal, false);
  	}

  $scope.readhobs = function() {
  	$scope.resetdata();
 	bookService.readBkgohbs($scope.restaurant, $scope.email).then(function(data) { 
 		var i, k, dataAr, tt, s, ttAr;
 		
 		if(typeof data !== 'string' || data.substring(0, 6) !== "BYSLOT")
 			return;

 		dataAr = data.split("|||");
		if(dataAr.length < 8) 
 			return;
 
    	for(i = 0; i < $scope.aweek.length; i++) {
    		ttAr = dataAr[i+1].split('');
    		tt = "";
    		for(k = 0; k < ttAr.length; k++) 
    			tt += parseInt(ttAr[k], 16).toString(2).pad(4);
    		ttAr = tt.split('');	
    		for(j = 18; j < 50; j++) {
    			label = $scope.aweek[i] + '_' + j;
    			$scope.data[label] = (ttAr[j] == "1");
    			}
    			
			}
 	 });
 	}
  	  	
  $scope.updatehobs = function() {
	var i, j, k, tt, ttAr, ohbs;
 
 	ohbs = "BYSLOT";
    for(i = 0; i < $scope.aweek.length; i++) {
    	ttAr = [];
	    for(k = 0; k < 18; k++)
			ttAr[k] = "0";
   		for(j = 0; j < lhours.length; j++) 
   			if(lhours[j].ind >= 0) {
    			label = $scope.aweek[i] + '_' + lhours[j].ind;
   				ttAr[j + 18] = ($scope.data[label]) ? "1" : "0";
   				}
  		for(j = 0; j < dhours.length; j++) 
    		if(dhours[j].ind >= 0) {
    			label = $scope.aweek[i] + '_' + dhours[j].ind;
   				ttAr[j + 32] = ($scope.data[label]) ? "1" : "0";
   				}  		
  		for(j = ttAr.length; j < 52; j++)
  			ttAr[j] = "0";
  		tt = "";
  		for(j = 0; j < ttAr.length; j += 4)
  			tt += parseInt(ttAr[j] + ttAr[j+1] + ttAr[j+2] + ttAr[j+3], 2).toString(16);
  		ohbs +=  "|||" + tt; 
   		}
	bookService.updateBkgohbs($scope.restaurant, $scope.email, ohbs).then(function(response) { if(response.status >= 0) alert("Booking information has been saved"); else alert("Unable to save the Booking information: " + response.errors)});
	};

	$scope.readhobs();
	
}]);

</script>
