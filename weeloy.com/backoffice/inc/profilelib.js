Array.prototype.joinclean = function() { var a = this.join(','); return (a.length > 0) ? a.replace(/`/g, "’") : ""; }

app.service('profileService', ['$http','$q', function($http, $q) {  

  	var self = this;
	var weeloy_demo_restaurant = ["SG_SG_R_TheFunKitchen", "SG_SG_R_TheOneKitchen", "FR_PR_R_LaTableDeLydia", "KR_SE_R_TheKoreanTable"];

	this.gettopfield = function() {
		return [ "salutation", "firstname", "lastname", "email", "mobile", "company", "createdate", "birth", "repeatguest" ];
		};

	this.getsubfield = function(oo) {
		var subfield = Object.keys(oo);
		if((i = subfield.indexOf("codekey0000")) >= 0) subfield.splice(i, 1);
		subfield.push("codekey0000");
		return subfield;
		}
		
	this.filter = function(oo) {
		var val = (oo instanceof Array) ? oo.join("|") : oo;
		return (typeof val === "string" && val.length > 0) ? val.replace(/,/g, "|") : "";
		};
		
	this.codeformat = function(data) {
		if(data && data.codeformat && typeof data.codeformat === "string" && data.codeformat.length > 5) {
			var oo = JSON.parse(data.codeformat.replace(/’/g, "\""));
			return oo.data;
			}
		return [];
		};
		
	this.datacontent = function(data) {
		var arr = [], vv, cc, oo;

		if(!data || !data.content || typeof data.content !== "string" || data.content.length < 6) 
			return [];
		vv = (data.profformat && typeof data.profformat === "string" && data.profformat.length > 5) ? data.profformat : data.content;			

		oo = JSON.parse(data.content.replace(/’/g, "\""));
		pp = JSON.parse(vv.replace(/’/g, "\""));
		Object.keys(pp).map(function(mm) {
			if(mm !== "codekey0000") {
				if(!oo[mm]) oo[mm] = "";
				if(oo[mm] instanceof Array)
					oo[mm] = oo[mm].joinclean();
				arr.push({ a: mm, b: oo[mm] });
				}
			});
		return arr;
		};

	this.codekey = function(data) {
		var arr = [], oo, val;
		if(data && data.content && typeof data.content === "string" && data.content.length > 5) {
			oo = JSON.parse(data.content.replace(/’/g, "\""));
			val = oo["codekey0000"];
			if(val) {
				if(val instanceof Array)
					val = val.joinclean();
				val = val.replace(/\s*,\s*/g, "|");
				arr = val.split('|');
				}
			}
		return arr;
		};
			
	this.printHtml = function(data, restaurant, systemid, token, platform, email, phone){
		var content, codedata, filtercnt = ["salutation", "firstname", "lastname", "bookings", "lastvisit", "systemid", "createdate", "firstvisit"] ;
		content = codedata = "";

		obj = this.profileObj(data, restaurant, systemid, token, platform, email, phone);
		console.log('PROFILE', obj);
		if(obj.others && obj.others.length > 0) {
			obj.others.map(function(oo) {
				content += "<h5>" + oo.label + " : " + oo.value + "</h5>";
				});
			content += "<hr>";
			}
		
		if(obj.content && obj.content.length > 0) {
			obj.content.map(function(oo) {
				content += "<h5>" + oo.label + " : " + oo.value + "</h5>";
				});
			}
		
		if(obj.codedata && obj.codedata !== "") {
			content += "<h5>" + "CODE" + " : " + obj.codedata + "</h5>";
			}

		if(obj.form && obj.form !== "") {
			content += obj.form;
			}

		if(obj.bookings && obj.bookings.length > 0) {
			content += "<br /><hr><h5>Bookings History: </h5>";
			content += "<table><th>date</th><th>time</th><th>pax</th><th>status</th><th>table</th>";
			obj.bookings.map(function(vv) {
				try {
					content += "<tr><td>" + vv.date + "</td><td>" + vv.time + "</td><td>" + vv.pax + "</td><td>" + vv.bkstatus + "&nbsp;</td><td>" + vv.tablename + "&nbsp;</td></tr>";
				} catch(err) {};
				});
			content += "</table>";
			}
		return content;				
		};

	this.profileObj = function(data, restaurant, systemid, token, platform, email, phone){
		var obj = {}, oo, codedata, multipleresto, filtercnt = ["salutation", "firstname", "lastname", "bookings", "lastvisit", "systemid", "createdate", "firstvisit"] ;
		
		codedata = "";
		obj.others = [];
		obj.content = [];
		Object.keys(data).map(function(ll) {
			var uu;
			if(ll === "codeformat") 
				return;
			if(ll === "profformat") 
				return;
			if(ll === "content") {
				uu = JSON.parse(data.content.replace(/’/g, "\""));
				Object.keys(uu).map(function(mm) {
					var val = uu[mm];
					if(mm === "hide") 
						return;
					if(mm === "codekey0000")  {
						codedata = (uu[mm] instanceof Array) ? uu[mm].join(",") : uu[mm]; 
						return;
						}				
					if(val instanceof Array) 
						val = val.joinclean();
					if(typeof val === "string" && val.length > 0)
						val = val.replace(/`/g, "’");
					if(typeof mm === "string" && mm.length > 0)
						mm = mm.replace(/`/g, "’");	
					obj.content.push({ label: mm, value: val});
					});
				if(codedata !== "") 
					obj.codedata = codedata;
					
				if(systemid !== "" && (platform === "tms" || platform === "standalonecc")) {
					obj.form = "";
					param = "'" + token +"', '" + restaurant +"', '" + systemid +"', '" + email +"', '" + phone + "'";
					obj.form += "<form>";
					obj.form += "<br /><input type='button' value='modify' style='background-color:purple;color:white;' onclick=\"javascript=window.opener.triggermodifyprofile(" + param +");\"></form>";
					}
				}
			else {
				if(ll == "bookings" && data.bookings instanceof Array && data.bookings.length > 0) {
					obj.bookings = data.bookings.slice(0);
					multipleresto = 0;
					obj.bookings.map(function(vv) {
						try {
							if(vv.tablename.length > 0) 
								vv.tablename = vv.tablename.replace(/L:/g, "");
							vv.tablename = vv.tablename.substr(0,10);
							vv.date = vv.date.datereverse();
							vv.time = vv.time.substr(0,5);
							vv.bkstatus = vv.bkstatus.substr(0,6);
							vv.restaurant = vv.resto.substr(8);
							if(vv.resto !== restaurant)
								multipleresto = 1;
						} catch(err) {};
						});
					obj.multipleresto = multipleresto;
					}
					
				if(filtercnt.indexOf(ll) >= 0 || data[ll] === "0000-00-00" || data[ll] === "") 
					return;
					
				obj.others.push({ label: ll, value: data[ll]});
				}
			});
			return obj;
		};

	this.updateMasterprofile = function(restaurant, email, x, token) {
		return $http.post("../api/services.php/profile/updateMasterprofile/",
			{
				'token': token,
				'email': email,
				'restaurant': restaurant,
				'content': x
			}).then(function (response) {
				return response.data;
			});
		};
		
	this.updatesubProfile = function(restaurant, email, systemid, code, content, atoken, aService) {
		var ttAr = [];		
		code.map(function(vv) { if(vv.value) ttAr.push(vv.label); });
		content['codekey0000'] = ttAr.join(',');

		token = atoken;
		contentobj = {};
		Object.keys(content).map(function(ll) { if(ll.indexOf("$$hash") < 0) contentobj[ll] = content[ll]; });			
		contentstr = JSON.stringify(contentobj).replace(/\"|'/g, "’");

		return aService.updatesubProfile(restaurant, email, systemid, contentstr).then(function(response) { 
			return response;
			});					
		};
	
	this.getProfiles = function(restaurant, start, end) {
		return $http.post("../api/services.php/profile/getprofiles/",
        {
            'token': token,
            'restaurant': restaurant,
            'start': start,
            'end': end
        }).then(function (response) {
        	return response.data;
        });
	}		
}]);