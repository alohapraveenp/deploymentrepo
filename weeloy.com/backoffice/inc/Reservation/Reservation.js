app.controller('BookingController', ['$scope', '$filter', '$uibModal', '$log', 'bookService', 'extractService', 'ModalService', function($scope, $filter, $uibModal, $log, bookService, extractService, ModalService) {
	$scope.restaurant = $("#restaurant").val();
	$scope.email = $("#email").val();
	$scope.token = $("#token").val();
	$scope.ccextraction = $("#ccextration").val();
	$scope.isTheFunKitchen = $("#isTheFunKitchen").val();
	$scope.imglogo = $("#imglogo").val();
	$scope.multProd = $("#multProd").val();
	$scope.isMitsuba = $("#isMitsuba").val();
	$scope.isShowLog = $("#isShowLog").val();
	$scope.isBacchanalia = $("#isBacchanalia").val();
	$scope.isMultiple = parseInt($("#bkreport").val());
    var todayjs = new Date();
    todayjs = new Date(todayjs.getFullYear(), todayjs.getMonth(), todayjs.getDate(), 0, 0, 0, 0);
    var currentday = $filter('date')(todayjs, "y-M-d");
    var currentmonth = todayjs.getMonth();
    var currentyear = todayjs.getYear();
    var aday = new AdayRoutine(currentday);
	var mydata = new bookService.ModalDataBooking();
	var newdate = new Date();
	var startdate, endate;
	var forceDate = new Date(new Date().getFullYear()-2, 0, 1);
	if(newdate.getMonth() > 6) {
		startdate = new Date(newdate.getFullYear(), 0, 1);
		endate = new Date(newdate.getFullYear()+1, 11, 31);
	} else {
		startdate = new Date(newdate.getFullYear()-1, 0, 1);
		endate = new Date(newdate.getFullYear(), 11, 31);
	}
	$scope.startBkg = -1;
	$scope.endBkg = newdate.getTime();
	$scope.cdate = newdate;
	$scope.mydata = mydata;
	$scope.mydatastart = new bookService.ModalDataBooking();
	$scope.mydatastart.setInit(forceDate, "09:00", function() { $scope.startBkg = this.getmTime(-86400000); $scope.paginator.setPage(0); }, 3, startdate, endate);
	$scope.mydataend = new bookService.ModalDataBooking();
	$scope.mydataend.setInit(forceDate, "09:00", function() { $scope.endBkg = this.getmTime(0); $scope.paginator.setPage(0); }, 3, startdate, endate);
	$scope.mydataend.setDate(newdate);
	$scope.getAlignment = bkgetalignment;
	
    $scope.showModal = function (title, initval, func, template, mydata, prepostcall) {

		$scope.mydata = mydata;
		$scope.mydata.name = initval;
		if(prepostcall !== null) prepostcall(0);
		
        var modalOptions = {
            closeButtonText: 'Cancel',
            actionButtonText: 'OK',
            headerText: title,
            mydata: mydata,
            submit:function(result){ if(prepostcall !== null) prepostcall(1); $modalInstance.close(result); },
            myclose:function(){ if(prepostcall !== null) prepostcall(1); $modalInstance.dismiss('cancel'); }        
        }
        
        ModalService.setTemplate(template);
        ModalService.setSize('');

        var $modalInstance = ModalService.showModal({}, modalOptions);
        $modalInstance.result.then(function (result) {
             func(result);
        });
    }
	//isMultiple = 0;
	// would need to be update for multiple product restaurant
	$scope.product = "";
	$scope.aSelDay = currentday;
	$scope.selDayObj = { selectedDay: null, datemodelist: ['booking date', 'create date'], datemode: "", showtitle: true }; 
	$scope.selDayObj.datemode = $scope.selDayObj.datemodelist[0];
	$scope.paginator = new Pagination(25);
	$scope.restotitle = $scope.restaurant.substring(8,99);
	$scope.typeselection = 1;
	$scope.tmpArr = [];
	$scope.modifnames = [];
	//$scope.isTheFunKitchen = (isTheFunKitchen != 0);
	//$scope.isMitsuba = (parseInt(isMitsuba) === 1);
	$scope.thirdpartyflg = false;
	//$scope.isMultiple = (isMultiple === 1);
	$scope.oneresto = 1;
	$scope.selectionrestotype = "single";
	//$scope.multProd = multProd;	
	$scope.predicate = '';
	$scope.reverse = false;
	$scope.codebooking = null;
    //$scope.isShowLog = (parseInt(isShowLog) === 1);
    //$scope.isBacchanalia = (parseInt(isBacchanalia) === 1);

	$scope.tabletitleStandard = [ {a:'index', b:'n', c:'', l:'25', q:'down', cc: 'black' }, {a:'fullname', b:'Name', c:'', l:'70', q:'down', cc: 'black' }, {a:'phone', b:'Mobile', c:'', l:'85', q:'down', cc: 'black' }, {alter: 'vdate', a:'date', b:'BookDate', c:'date', l:'', q:'up', cc: 'orange' }, {a:'time', b:'Time', c:'', l:'', q:'down', cc: 'black' }, {a:'pers', b:'Pax', c:'', l:'15', q:'down', cc: 'black' }, {a:'booking', b:'Booking', c:'', l:'', q:'down', cc: 'black' }, {alter: 'vcdate', a:'cdate', b:'createDate', c:'date', l:'', q:'down', cc: 'black' }, {a:'bkstatus', b:'Status', c:'', l:'40', q:'down', cc: 'black' },{a:'sudotype', b:'Type', c:'', l:'40', q:'down', cc: 'black' } ];
	$scope.tabletitleToday = [ {a:'time', b:'Time', c:'', l:'', q:'down', cc: 'black' }, {a:'fullname', b:'Name', c:'', l:'70', q:'down', cc: 'black' }, {a:'pers', b:'Pax', c:'', l:'', q:'down', cc: 'black' }, {a:'phone', b:'Mobile', c:'', l:'100', q:'down', cc: 'black' }, {a:'comment', b:'Request', c:'', l:'80', q:'down', cc: 'black' }, {a:'bkstatus', b:'Status', c:'', l:'', q:'down', cc: 'black' }, {a:'sudotype', b:'Type', c:'', l:'', q:'down', cc: 'black' }, {a:'booking', b:'Booking', c:'', l:'', q:'down', cc: 'black' }, {a:'mealtype', b:'Meal Type', c:'', l:'', q:'down', cc: 'black' }, {a:'state', b:'State', c:'', l:'', q:'down', cc: 'black' } ];
	$scope.tabletitleContent = [ { a:'booking', b:'Booking Confirmation', c:'' }, { a:'type', b:'Booking Type', c:'' }, { a:'product', b:'Product', c:'' }, { a:'bkstatus', b:'Status', c:'' }, { a:'canceldate', b:'Cancel date', c:'' }, { a:'date', b:'Reservation Date', c:'date' }, { a:'time', b:'Reservation Time', c:'' }, { a:'pers', b:'Number of Persons', c:'' }, { a:'fullname', b:'Name', c:'' }, { a:'email', b:'Email', c:'' }, { a:'phone', b:'Mobile', c:'' }, { a:'createdate', b:'Create Date', c:'date' }, { a:'comment', b:'Request', c:'' }, { a:'fullnotes', b:'Notes', c:'' }, { a:'wheelwin', b:'Win', c:'' }, {a: 'captain', b: 'Captain', c:''}, {a: 'event', b: 'Special occasion', c:''}, { a:'tracking', b:'Tracking', c:'' }, { a:'booker', b:'Staff', c:'' }, { a:'hotelguest', b:'Hotel Guest', c:'' }, { a:'company', b:'Company', c:'' }, { a:'state', b:'State', c:'' }, { a:'repeat', b:'Repeat Guest', c:'' }, { a:'duration', b:'Duration (mm)', c:'' }, { a:'restCode', b:'restCode', c:'' }, { a:'tablename', b:'Table', c:'' }, { a:'browser', b:'Browser', c:'' }, { a:'language', b:'Language', c:'' }, { a:'optin', b:'optin', c:'' }, { a:'review_status', b:'Review', c:'' }  ];
        //$scope.tabletitleContent = [ { a:'Booking Confirmation', b:'booking', c:'' }, { a:'Booking Type', b:'type', c:'' }, { a:'Status', b:'bkstatus', c:'' }, { a:'Cancel date', b:'canceldate', c:'' }, { a:'Reservation Date', b:'date', c:'date' }, { a:'Reservation Time', b:'time', c:'' }, { a:'Number of Persons', b:'pers', c:'' }, { a:'Name', b:'fullname', c:'' }, { a:'Email', b:'email', c:'' }, { a:'Mobile', b:'phone', c:'' }, { a:'Create Date', b:'createdate', c:'date' }, { a:'Request', b:'comment', c:'' }, { a:'Notes', b:'fullnotes', c:'' }, { a:'Win', b:'wheelwin', c:'' }, {a: 'event', b: 'Special occasion', c:''}, { a:'Tracking', b:'tracking', c:'' }, { a:'Staff', b:'booker', c:'' }, { a:'Hotel Guest', b:'hotelguest', c:'' }, { a:'Company', b:'company', c:'' }, { a:'State', b:'state', c:'' }, { a:'Duration (mm)', b:'duration', c:'' }, { a:'restCode', b:'restCode', c:'' }, { a:'Table', b:'tablename', c:'' }, { a:'Browser', b:'browser', c:'' }, { a:'Language', b:'language', c:'' }, { a:'optin', b:'optin', c:'' }, { a:'Review', b:'review_status', c:'' } ];
	$scope.bkfield = [ 
		{ a:'restaurant', b:'Restaurant', c: false }, 
		{ a:'booking', b:'Confirmation', c: false }, 
		{ a:'type', b:'Type', c: false }, 
		{ a:'bkstatus', b:'Status', c: false }, 
		{ a:'date', b:'BDate*', c: false }, 
		{ a:'time', b:'Time', c: false }, 
		{ a:'pers', b:'Pax', c: false }, 
		{ a:'fullname', b:'FullName', c: false }, 
		{ a:'email', b:'Email', c: false }, 
		{ a:'phone', b:'Mobile', c: false }, 
		{ a:'comment', b:'Request', c: false }, 
		{ a:'tablename', b:'Table', c: false }, 
		{ a:'tracking', b:'Tracking', c: false }, 
		{ a:'createdate', b:'CDate*', c: false }, 
		{ a:'canceldate', b:'ADate*', c: false }, 
		{ a:'wheelwin', b:'Win', c: false }, 
		{ a:'event', b: 'Special occasion', c: false }, 
		{ a:'booker', b:'Staff', c: false }, 
		{ a:'hotelguest', b:'Hotelguest', c: false }, 
		{ a:'company', b:'Company', c: false }, 
		{ a:'state', b:'State', c: false }, 
		{ a:'duration', b:'Duration (mm)', c: false }, 
		{ a:'restCode', b:'restCode', c: false }, 
		{ a:'browser', b:'Browser', c: false }, 
		{ a:'language', b:'Language', c: false }, 
		{ a:'optin', b:'optin', c: false }, 
		{ a:'review_status', b:'Review', c: false } ];
	if ($scope.restaurant.indexOf('SG_SG_R_Pscafe') >= 0)
		$scope.bkfield.splice(18, 1);
	if($scope.multProd !== '') { // 'type' field must be the last one
		$scope.bkfield.splice(4, 0, { a:'product', b:'Product', c:false });
		$scope.tabletitleStandard.splice(8, 0, { a:'product', b:'Product', c:'', l:'45', q:'down', cc: 'black' });
		$scope.tabletitleToday.splice(8, 0, { a:'product', b:'Product', c:'', l:'45', q:'down', cc: 'black' });
	}
		
	if($scope.isMultiple === 1) {
		$scope.tabletitleStandard.splice(1, 0, {a:'title', b:'restaurant', c:'', l:'70', q:'down', cc: 'black' });
		$scope.tabletitleToday.splice(0, 0, {a:'title', b:'restaurant', c:'', l:'70', q:'down', cc: 'black' });
		$scope.tabletitleContent.splice(1, 0, {a:'title', b:'Restaurant', c:'', l:'70', q:'down', cc: 'black' });
        }

	$scope.tabletitle = $scope.tabletitleStandard;
	$scope.bckups = $scope.tabletitleStandard.slice(0);
	$scope.bckupt = $scope.tabletitleToday.slice(0);	

	$scope.searchDictionaryAr = ['today', 'lunch', 'dinner', 'week', 'lastweek', 'month', 'lastmonth', 'future', 'past', 'spin', 'unspin', 'cancel', 'uncancel', 'noshow']
	$scope.extratitle = [ {'a':'today', 'b':'Today' }, {'a':'lunch', 'b':'---> Lunch' }, {'a':'dinner', 'b':'---> Dinner' }, {'a':'week', 'b':'Current Week' }, {'a':'lastweek', 'b':'Last Week' }, {'a':'month', 'b':'Current Month' }, {'a':'lastmonth', 'b':'Last Month' }, {'a':'future', 'b':'Future' }, {'a':'past', 'b':'Past' }, {'a':'divider', 'b':'divider' }, {'a':'website', 'b':'Website' }, {'a':'facebook', 'b':'Facebook' }, {'a':'callcenter', 'b':'Call Center' }, {'a':'walkin', 'b':'Walkin' }, {'a':'divider', 'b':'divider' }, {'a':'spin', 'b':'Used' }, {'a':'unspin', 'b':'Not Used' }, {'a':'cancel', 'b':'Cancelled' }, {'a':'uncancel', 'b':'Not Cancelled' }, {'a':'noshow', 'b':'No Show' }, {'a':'all', 'b':'All' }, {'a':'divider', 'b':'divider' }  ];
	$scope.extradictionary = (function() { 
		var tmp=[]; 
		for(var i = 0; i < $scope.extratitle.length; i++) 
			tmp.push($scope.extratitle[i].a); 
		return tmp; 
	})();
	$scope.extratitlesegment = [{'a':'reserve', 'b':' by reservation date' }, {'a':'creation', 'b':' by creation date' } ];
	$scope.stateAr = bookService.validState();

	$scope.selectedItem = null;
	
	if(localStorage.viewmultresto && localStorage.viewmultresto === "71") {
		$scope.oneresto = 0;
		$scope.selectionrestotype = "multiple";
		}
				
	$scope.initorder = function() {
		$scope.tabletitleStandard = $scope.bckups;
		$scope.tabletitleToday = $scope.bckupt;
		$scope.tabletitle = $scope.tabletitleStandard;
		$scope.predicate = "vdate";
		$scope.reverse = true;
		};
		
	$scope.reorder = function(item, alter) {
		alter = alter || "";
		if (alter !== "")  item = alter;
		$scope.reverse = ($scope.predicate === item) ? !$scope.reverse : false;
		$scope.predicate = item;
	};
				
	$scope.showtest = function(y) {
		if(y.c === true || $scope.selectedItem === null) 
			return true;
		if(typeof $scope.selectedItem[y.a] === 'undefined')
			return false;
		return ($scope.selectedItem[y.a] !== '' && $scope.selectedItem[y.a] !== '0' && $scope.selectedItem[y.a] !== null );
	};

	$scope.validatebkg = function(x) {
		bookService.validateBooking(x.restaurant, x.booking).then(function(response) {
			var content = response.data;
			if(response.status === 1) { 
				x.validate = 1;
				}
			});
		};
		
	$scope.readCodeBooking = function() {
		bookService.readCodeBooking($scope.restaurant, $scope.email).then(function(response) {
			var content = response.data;
			if(response.status === 1 && typeof content === 'string' && content !== '') { 
				try {
					var oo = JSON.parse(content.replace(/’/g, "\""));
					$scope.codebooking = oo.data;
				} catch(e) { console.error("JSON-PROFILE", e.message); }
				}
			});
		};
                

	$scope.resetproperty = function() {
			var i, data = $scope.names;
			for(i = 0; i < data.length; i++) {
				$scope.setproperty(data[i]);
				}
				
		item = localStorage.extraselect;
		if(item && typeof item === "string" && item.length > 0 && $scope.extradictionary.indexOf(item) >= 0)
			$scope.extraselect(item);
        };
                

	
	$scope.setproperty = function(oo, nn) {
		var i, j, k, item, tt, cmonth, lmonthy, originAr = ['c', 'r'];

		cmonth = parseInt(currentmonth);
		lmonthy = (cmonth > 1) ? currentyear + '-' + (((cmonth-1) < 10) ? "0":"") + (cmonth-1) : (parseInt(currentyear) - 1) + '-12';
		oo.selectflg = 0;
		if($scope.thirdpartyflg === false)
			if (typeof oo.tracking === "string" && oo.tracking.length > 0 && oo.tracking.toLowerCase().indexOf("remote") > -1)
 				$scope.thirdpartyflg = true;
 				
		k = 1;
		for(i = 0; i < $scope.searchDictionaryAr.length; i++)
			for(j = 0; j < originAr.length; j++, k <<= 1) {
				item = $scope.searchDictionaryAr[i];
				dd = (originAr[j] === 'c') ? oo.cdate : oo.date;
				tt = (originAr[j] === 'c') ? oo.vcdate : oo.vdate;
				switch(item) {
					case 'today':
						if(dd == currentday) {
							oo.selectflg |= k;
							}
						break;
					case 'lunch':
						if(dd == currentday && oo.mealtype == 'lunch') 
							oo.selectflg |= k;
						break;
					case 'dinner':
						if(dd == currentday && oo.mealtype == 'dinner') 
							oo.selectflg |= k;
						break;
					case 'week':
						if(aday.toWeek(dd) >= 0)
							oo.selectflg |= k;
						break;
					case 'lastweek':
						if(aday.tolWeek(dd) >= 0)
							oo.selectflg |= k;
						break;
					case 'month':
						if(parseInt(dd.substring(5, 7)) == cmonth)
							oo.selectflg |= k;
						break;
					case 'lastmonth':
						if(dd.substring(0, 7) == lmonthy)
							oo.selectflg |= k;
						break;
					case 'future':
						if(tt > todayjs.getTime())
							oo.selectflg |= k;
						break;
					case 'past':
						if(tt < todayjs.getTime())
							oo.selectflg |= k;
						break;
					case 'spin':
						if (oo.wheelwin != "")
							oo.selectflg |= k;
						break;
					case 'unspin':
						if (oo.wheelwin == "" && oo.wheelwin != "cancel")
							oo.selectflg |= k;
						break;
					case 'cancel':
						if(oo.bkstatus === "cancel") 
							oo.selectflg |= k;
						break;
					case 'uncancel':
						if(oo.bkstatus !== "cancel")
							oo.selectflg |= k;
						break;
					case 'noshow':
						if (oo.state == "no show")
							oo.selectflg |= k;
						break;
					}
				}
		};
		
	$scope.readBooking = function(restaurant, email, multiple) {
		
		var reference = restaurant, mode = '';
		
		if(multiple) {
			reference = email;
			mode = 'email';
			}
			
		bookService.readBooking(reference, mode).then(function(response) {
			var i, data, oo, flg = $scope.thirdpartyflg, truncateAr = [];
			var today = new Date();
			var yesterday = new Date(today.setDate(today.getDate() - 7));
			var yesterdayTime = new Date(yesterday.getFullYear(), yesterday.getMonth(), yesterday.getDate(), 0, 0, 0).getTime();
	
			$scope.thirdpartyflg = false;	
			$scope.burnendlinkflg = false;
			$scope.mnufrmsubmited = false;
			$scope.bacchaflg = false;
			$scope.names = data = response;
			for(i = 0; i < data.length; i++) {

				oo = data[i];
				oo.sudotype = oo.type;
				
				if(oo.type === 'thirdparty')
					oo.sudotype = oo.booker;
				if(oo.tracking.length > 0 && oo.tracking.search("IMASIA") > -1) {
					if(oo.tracking.search("IMASIA|facebook") > -1) oo.sudotype = "imasiafb";
					else if(oo.tracking.search("IMASIAEDM") > -1) oo.sudotype = "imasiaedm";
					else oo.sudotype = "imasia";
					}
					
				if(oo.bkstatus === "expired" && oo.vcdate < yesterdayTime) { // change to datetime if you want to filter booking date vs create date
					truncateAr.push(i);
					continue;
					}
				oo.chckflgcus = $scope.checkdate(oo);	// cus -> cancel, update, spin
				oo.chckflgnshow = $scope.checktimediff(oo.date, oo.time, oo.bkstatus, oo.wheelwin, oo.tracking);
				if(oo.chckflgnshow) { oo.refund_amount = oo.amount; }
				oo.chckflgspin = $scope.checkspin(oo);
				oo.ordersdetails = [];
				if(oo.orders && oo.orders instanceof Array) {
					var ototal = 0;
					oo.orders.map(function(x) { ototal += x.amount; oo.ordersdetails.push(x.qty + ' x ' + x.name + ' = ' + parseInt(x.amount/100)) });
					oo.ordersdetails.splice(0, 0, "amount = "+parseInt(ototal/100));
					}
					
				if(oo.product === null) oo.product = '';
				oo.chckflgsubmit = $scope.checkfrmsubmit(oo.date, oo.bkstatus,oo.tracking,oo.product,oo.pers,oo.options.chck_form_status);
				if(oo.product == 'Chef table' || (oo.product.toLowerCase() == 'counter seats' && oo.pers > 5)){
					oo.mnufrmsubmited = false;
					if(oo.options.chck_form_status)  oo.mnufrmsubmited = true;
					oo.burnendlinkflg = true;
					}
                                //oo.chckflgpaystatus = $scope.checkpaystatus(oo.date, oo.bkstatus,oo.tracking);
                                oo.recipient = oo.email;
                                oo.receiver_mobile = oo.phone;
                                
                                
                               
				if(oo.bkstatus === "") {
					if(oo.wheelwin !== "") oo.bkstatus = "spin";
					if(oo.state === "no show") oo.bkstatus = "noshow";
					}
				$scope.setproperty(oo);
				}

			if(flg !== $scope.thirdpartyflg) {
				if(flg === false)
					$scope.extratitle.splice(13, 0, {'a':'remote', 'b':'Thirdparty' });
				else { 
					$scope.extratitle.splice(13, 1);	
					console.log('Resetting thirdpary');
					}
				}

			if(truncateAr.length > 0) 
				truncateAr.reverse().map(function(i) { $scope.names.splice(i, 1); });

					
			$scope.orgnames = $scope.names.slice(0);
			if(typeof localStorage.extraselect === "string" && $scope.extradictionary.indexOf(localStorage.extraselect) >= 0)
				$scope.currentSelection = localStorage.extraselect;
			else $scope.currentSelection = "all";
			$scope.extraselect($scope.currentSelection);
			$scope.paginator.setItemCount($scope.names.length);
			   
			$scope.initorder();
			$scope.readCodeBooking();
		
			bookService.readmodifBooking(reference, mode).then(function(response) { 
				var tmpindex = [];
			
				data = response;
				$scope.modifnames = response; 
				for(i = 0; i < data.length; i++) {
					data[i].rdate = data[i].rdate.jsdate().getDateFormat('-');
					tmpindex.push(data[i].booking);
					}
				for(i = 0; i < $scope.names.length; i++) {
					$scope.names[i].modified = tmpindex.indexOf($scope.names[i].booking);
					if($scope.names[i].modified >= 0) {
						moo = data[$scope.names[i].modified];
						$scope.names[i].modifiedinfo = "booking date: " + moo.rdate + ", booking time: " + moo.rtime + ", cover: " + moo.cover;
						}
					}
				});
			});
     	};       

	if($scope.oneresto === 1) 
		$scope.readBooking($scope.restaurant, $scope.email, false);
	else $scope.readBooking($scope.restaurant, $scope.email, (isMultiple === 1));
	
	$scope.extraselect = function(item) {
		var index, limit, data, mask;

		$scope.selDayObj.selectedDay = null;
		
		switch(item) {				
			case 'all':
				$scope.names = $scope.orgnames.slice(0);
				$scope.currentSelection = item;
				$scope.formatDisplay = "standard";
				$scope.tabletitle = $scope.tabletitleStandard;
				$('.custom').removeClass('glyphicon glyphicon-ok');
				$('#' + item).addClass('glyphicon glyphicon-ok');
				localStorage.extraselect = item;
				return;
			
			case 'oneresto':
				$scope.oneresto ^= 1;
				if($scope.oneresto === 1)
					$scope.readBooking($scope.restaurant, $scope.email, false);
				else $scope.readBooking($scope.restaurant, $scope.email, (isMultiple === 1));
				
				$scope.typeselection = 1;
				$scope.selectionrestotype = ($scope.oneresto === 1) ? "single" : "multiple";
				localStorage.viewmultresto =  ($scope.oneresto === 1) ? "" : "71";
				$('.glyphr').toggleClass('glyphicon glyphicon-ok');
				$('.glyphc').toggleClass('glyphicon glyphicon-ok');
				return;

			case 'reserve' :
				$scope.typeselection = 1;
				$('.glyphr').toggleClass('glyphicon glyphicon-ok');
				$('.glyphc').toggleClass('glyphicon glyphicon-ok');
				item = localStorage.extraselect;
				if(['week', 'lastweek', 'month', 'lastmonth'].indexOf(item) < 0)
					return;
				break;
			
			case 'create' :
				$scope.typeselection = 2;
				$('.glyphr').toggleClass('glyphicon glyphicon-ok');
				$('.glyphc').toggleClass('glyphicon glyphicon-ok');
				item = localStorage.extraselect;
				if(['week', 'lastweek', 'month', 'lastmonth'].indexOf(item) < 0)
					return;
				break;
			
			default:
				break;
				}

			
		$scope.current = $scope.names.slice(0);
		$scope.names = $scope.orgnames.slice(0);
		$scope.paginator.setPage(0);
		$scope.tmpArr = [];

		data = $scope.names;
		limit = data.length;
		localStorage.extraselect = item;
		
		switch(item) {
								
			case 'callcenter':
			case 'facebook':
			case 'website':
			case 'walkin':
			case 'remote':
				$scope.currentSelection = item;
				for (i = 0; i < limit; i++)
					if (typeof data[i].tracking === "string" && data[i].tracking.length > 0 && data[i].tracking.toLowerCase().indexOf(item) > -1)
						$scope.tmpArr.push(data[i]);

				$scope.completeselection(item);
				break;
			
			default:
				index = $scope.searchDictionaryAr.indexOf(item) * 2;
				if(index < 0) {
					alert("invalid selection " + item);
					return;
					}
				$scope.currentSelection = item;
				if($scope.typeselection == 1)
					index++;			
				mask = 1 << index;
				for (i = 0; i < limit; i++)
					if((data[i].selectflg & mask) > 0) {
						$scope.tmpArr.push($scope.names[i]);
					}

				$scope.completeselection(item);
				break;
				}
			};

	$scope.completeselection = function(item) {
		if ($scope.tmpArr.length > 0) {
			$scope.names = $scope.tmpArr.slice(0);
			$('.custom').removeClass('glyphicon glyphicon-ok');
			$('#' + item).addClass('glyphicon glyphicon-ok');
			$scope.currentSelection = item;
			if (item !== "today" && item !== "lunch" && item !== "dinner") {
				$scope.formatDisplay = "standard";
				$scope.tabletitle = $scope.tabletitleStandard;
				}
			else {
				$scope.formatDisplay = "today";
				$scope.tabletitle = $scope.tabletitleToday;
				$scope.reorder("time", "");
				}
			}
		else {
			$scope.names = $scope.current.slice(0);
			alert('Search ' + item + ': no booking selected (stay with previous selection');
			}
		};
					
	$scope.view = function(oo, previous) {
            if(typeof previous !== 'string') previous = "";
            $scope.selectedItem = oo; // take the orgnames as names get modified with the shortcuts
            $scope.reset('contentVisitFlag');
            if($scope.isBacchanalia || oo.restaurant == 'SG_SG_R_TheOneKitchen'|| oo.restaurant == 'SG_SG_R_Pollen'|| oo.restaurant == 'SG_SG_R_Nouri' ||oo.restaurant == 'SG_SG_R_Esquina'){
                $scope.selectedItem.chckflgpaystatus = $scope.checkpaystatus(oo.date, oo.bkstatus,oo.tracking);
            }
  
            if(oo.booking_deposit_id && oo.booking_deposit_id !== ""){
            	
            	
            	if(oo.options && oo.options.choice) {
            		 oo.product = oo.options.choice;
            	}
            		$scope.chargeDetails(oo.booking);
                    $scope.cancelPolicy(oo.restaurant, oo.payment_method,oo.amount,oo.product,oo.date,oo.time,oo.pers);
                    
            }
        
            //get booking log event history
            if($scope.isShowLog){
                $scope.getLogEvent(oo);
            }
            $scope.previous = previous;
        };
	
	$scope.previous = "";	
	$scope.backlisting = function() {
		if($scope.previous === "")
			return $scope.reset('listingVisitFlag');
		
		$scope.reset($scope.previous);
		$scope.previous = "";
		};

	$scope.mitsubaValidation = function() {
		$scope.reset('mitsubaFlag');
		};
		
	$scope.reset = function(tag) {
		$scope.contentVisitFlag = false;
		$scope.listingVisitFlag = false;
		$scope.extractFlag = false;
		$scope.mitsubaFlag = false;
		$scope[tag] = true;
		}
				
	$scope.spin = function(oo) {
		state = oo.wheelwin;
		if (state != '') {
			if (state == 'cancel')
				alert("The reservation has been previously canceled");
			else alert("The Wheel has already been spun! (" + state + ")")
				return false;
			}

		winp = window.open('../wheel/rotation/rotation.php?restaurant=' + oo.restaurant + '&confirmation=' + oo.booking + '&restCode=' + oo.restCode + '&guestname=' + oo.first, 'weeloy', 'toolbar=no,width=1050,height=750,menubar=no,scrollbars=yes,resizable=no,alwaysRaised=yes');
		winp.moveTo(15, 25);
		winp.resizeTo(1050, 825);
		winp.focus();
		return false;
		};
	
	$scope.savechgstatebkg = function(x) {
		return bookService.savechgstateservice(x.restaurant, x.booking, x.state, token).then(function(response){ console.log(response); });
		}
	
	$scope.checkspin = function(oo) {
            
		var tracking = oo.tracking, restCode = oo.restCode, booking = oo.booking;
		var restotest = (['SG_SG_R_TheFunKitchen', 'SG_SG_R_TheOneKitchen', 'FR_PR_R_LaTableDeLydia', 'KR_SE_R_TheKoreanTable'].indexOf($scope.restaurant) >= 0);

		if(typeof tracking !== "string" || tracking === "") 
			if(restotest)
			     return true;

		tracking = tracking.toLowerCase();
		if(typeof restCode !== "string" || restCode.length < 4 || restCode === "0000")
			return false;
		
		if(['remote', 'tms', 'callcenter', 'facebook'].indexOf(tracking)>= 0) 
			return false;
			
		if(restotest)
			return true;
			
		return false;
        };
		
	$scope.checktimediff = function(rdate, rtime, status, wheel, tracking) {
		var diffDays, timeParts;
		if (status === 'cancel' || status === 'noshow' || wheel !== '')
			return false;
 		//if(typeof tracking === "string" && tracking !== "" && tracking.search("remote")  > -1) return false;

		timeParts = rtime.split(':');
		var uu = rdate.split('-');
		var addtwoDays = getDiffDay(rdate, 0, 0);
              
        diffDays= (new Date().getTime()-new Date(parseInt(uu[0]), parseInt(uu[1])-1, parseInt(uu[2]), timeParts[0], timeParts[1]).getTime()) / (24 * 3600 * 1000);
		return (diffDays >= 0 && addtwoDays>-3);
	}
        
        $scope.getLogEvent = function(x) {
            $scope.eventlog = [];
            return bookService.logbookingevent(x.booking).then(function(response){ 
                if(typeof response.data!=='undefined' &&  response.data !== "" ){
                      $scope.eventlog = response.data;
                 }              
                });
        }
              
              
	$scope.checkdate = function(x) {        
               //rdate, status, wheel, tracking
            if (x.bkstatus === 'cancel' || x.bkstatus === 'noshow' || x.wheelwin !== '') return false;
            if(x.restaurant != 'SG_SG_R_Bacchanalia' && x.restaurant != 'SG_SG_R_TheFunKitchen' ) {
                if(x.bkstatus === 'pending_payment' || x.bkstatus === 'expired') return false;
                if(typeof x.tracking === "string" && x.tracking !== "" && x.tracking.search("remote")  > -1) return false;
            }

            return (getDiffDay(x.date, 0, 0) > -1);

        };
            
       $scope.checkfrmsubmit = function(rdate, status,tracking,product,pers,frmsubmit) {
      		if (status === 'cancel' || status === 'noshow' || product === '' || product.toLowerCase() === 'bar seats' || frmsubmit || (product.toLowerCase() === 'counter seats' && pers < 6) || status === 'pending_payment') return false;

		if(typeof tracking === "string" && tracking !== "" && tracking.search("remote")  > -1) return false;
		return (getDiffDay(rdate, 0, 0) >= 0 && getDiffDay(rdate, 0, 0)  < 3);
            };
       $scope.checkpaystatus = function(rdate, status,tracking) {
           
            if($scope.isBacchanalia){
                  if (status === 'cancel' || status === 'noshow' || status === 'spin') return false; 
                    if($scope.selectedItem.modified >=0 ){
                         if (status === 'cancel' || status === 'noshow' || status === 'spin') return false;   
                    }else{
                        if (status === 'cancel' || status === 'noshow' || status === '' || status === 'spin') return false;
                     }
                   
                }else{
                    if (status === 'cancel' || status === 'noshow' || status === '' || status === 'spin') return false;
                }

            return (getDiffDay(rdate, 0, 0) > -1);
        };
        
        //refund flage
        $scope.checkrefunddate = function(rdate, rtime,status,isShow,ispayment,depstatus,payment_method) {
		if (status !== 'cancel' || ispayment !== 'paid'  || isShow === 0 || depstatus === 'REFUNDED' || payment_method === 'reddot' ) return false;
                 timeParts = rtime.split(':');
		 var uu = rdate.split('-');
                 var diffDays= (new Date().getTime()-new Date(parseInt(uu[0]), parseInt(uu[1])-1, parseInt(uu[2]), timeParts[0], timeParts[1]).getTime()) / (1000 * 60 * 60 * 24 * 14);
                 return (diffDays < 1);
        };

	$scope.cancelbooking = function(x) {
		if (confirm("Please confirm the cancellation of reservation " + x.booking + " ?") == false)
			return;

		return bookService.cancelbackoffice(x.restaurant, x.booking, x.email).then(function(response){
			console.log(response);
			if(response.status !== 1) {
				return alert('Reservation ' + x.booking + ' has NOT been cancel. ' + response.errors);
				}
				
			x.wheelwin = x.bkstatus = 'cancel';
			x.chckflgcus = $scope.checkdate(x);	// cus -> cancel, update, spin
			x.chckflgnshow = $scope.checktimediff(x.date, x.time, x.bkstatus, x.wheelwin, x.tracking);
			$scope.resetproperty();
			alert('Reservation ' + x.booking + ' has been cancel ');
			});
        };
        
        
        
		
        $scope.cancelPolicy = function(restaurant, payment_method,amount,product,date,time,pax){

        	return bookService.getPaymentPolicy(restaurant, payment_method,amount,product,date,time,pax).then(function(response) {
                   var data = response.data;
                 
                    if (data && data.priceDetails) {
                        if(data.priceDetails.requiredccdetails){
                       //$scope.policybaccahanalia = data.priceDetails.message;  
                            $scope.paymenttnc = data.priceDetails.message;
                            if(restaurant === "SG_SG_R_Pollen") {
                           	 $scope.paymenttncAr = $scope.paymenttnc.split('-'); 
                            }
                    	}
                	}
                });

            // if(restaurant === "SG_SG_R_TheFunKitchen" || restaurant === "SG_SG_R_Bacchanalia" ){

            //     return bookService.bacchanaliacancelpolicy(restaurant,date,time,pax).then(function(response) {
            //        var data = response.data;
            //        if (typeof data !== 'undefined' ) {
            //            $scope.policybaccahanalia = data.priceDetails.message;  
            //         }
            //     });
                
            // }
            // if(restaurant === "SG_SG_R_TheOneKitchen"){
            //     return bookService.medinicancelpolicy(restaurant,payment_method,amount,date,time,pax).then(function(response) {
            //        var data = response.data;
            //        if (typeof data !== 'undefined' ) {
            //            $scope.medinitnc = data.priceDetails.message;  
            //         }
            //     });
                
            
            // }else{
                
            //     return bookService.getCancelPolicy(restaurant, payment_method, amount,product).then(function(response) {
            //         var data =response.data;
            //         var temArr =[];

            //     if(typeof response.data !== 'undefined' &&  response.data !== "" ){
            //             $scope.policy = response.data.range;
            //             $scope.freerange = response.data.lastRange;		
            //     }

            //     }); 
            // }

        }; 
                
        $scope.chargeDetails = function(confirmation){
        

            return bookService.getBkChargeDetails(confirmation).then(function(response) {
                if(response.data){
                    var res = response.data;
                    $scope.selectedItem.paymethod = (res.payment_method === "paypal") ? res.payment_method  : "credit card" ;
                    $scope.selectedItem.isShowDetails = res.isShowDetails;
                    $scope.selectedItem.payment_method = res.payment_method;
                    $scope.selectedItem.deposit = res.deposit;
                    $scope.selectedItem.refund = res.refund;
                    $scope.selectedItem.refund_amount = res.refund_amount;
                    $scope.selectedItem.depstatus = res.depstatus;
                    $scope.selectedItem.amount = res.amount;
                    $scope.selectedItem.currency = res.currency;
                    
                    //$scope.selectedItem.chckflgpaystatus = $scope.checkpaystatus($scope.selectedItem.date, $scope.selectedItem.bkstatus,$scope.selectedItem.tracking);
                    $scope.selectedItem.chckflgrfnd = $scope.checkrefunddate($scope.selectedItem.date, $scope.selectedItem.time,$scope.selectedItem.bkstatus, $scope.selectedItem.isShowDetails, $scope.selectedItem.deposit,$scope.selectedItem.depstatus,$scope.selectedItem.payment_method);
                        if($scope.selectedItem.chckflgrfnd) { $scope.selectedItem.res_refund_amount = $scope.selectedItem.amount; }
                        if($scope.selectedItem.chckflgnshow ) { $scope.selectedItem.refund_amount = $scope.selectedItem.amount; }


                }

            });

        };
	      
	
                
        $scope.cancelpaymentbooking = function(x) {
        	var message = "by cancelling the reservation " + x.booking + ", you charge "+ x.currency + x.refund_amount + " to the client credit card.";
        	if( x.restaurant =='TH_BK_R_Medinii' || x.restaurant ==='TH_BK_R_BangkokHeightz'){
        			message = "Please confirm the cancellation of reservation " + x.booking + " ?";
        	}else if(x.restaurant =='SG_SG_R_Esquina' || x.restaurant =='SG_SG_R_Pollen' ){
                     message = "by cancelling the reservation " + x.booking + ", you refund "+ x.currency + x.refund_amount + " to the guest";
                }

            if(parseInt(x.amount) >= parseInt(x.refund_amount) || parseInt(x.refund_amount) === 0) { 
            	
                    if (confirm(message) == false)
                            return;	   

                    return bookService.cancelrefundbackoffice(x.booking_deposit_id, x.restaurant, x.booking, x.email, x.refund_amount, x.payment_method).then(function(response){

                            x.wheelwin = x.bkstatus = 'cancel';
                            if(x.payment_method === 'carddetails'){
                                    x.depstatus = 'PAID';
                                    x.amount = x.amount;
                            }else{
                                x.depstatus = 'PAID';
                                x.refund_amount = x.amount;
                            }
                            if(response.status === 1){
                                x.chckflgcus = $scope.checkdate(x);	// cus -> cancel, update, spin
                                x.chckflgnshow = $scope.checktimediff(x.date, x.time, x.bkstatus, x.wheelwin, x.tracking);
                                       alert('Reservation ' + x.booking + ' has been cancel ');
                            }else{
                                console.log(response.data);
                                alert('Payment creation Failed : '+ response.data.message);
                            }
                    });
            }
            else{
                alert("IMPT:Refund amount shall not exceed the initial deposit payment. ");
                return;
            }
        };

        $scope.noshowpaymentbooking = function(x){
		if (confirm("Please confirm the status change of reservation " + x.booking + " ?"+ " you charge SGD "+ x.refund_amount + " to the client credit card.") == false)
			return;
			
		return bookService.bookingpaymentnoshow(x.restaurant, x.booking, x.email, x.booking_deposit_id, x.refund_amount, x.payment_method).then(function(response){
                    if(response.status === 1){
                            x.wheelwin = x.bkstatus = 'noshow';
                            x.chckflgcus = $scope.checkdate(x);	// cus -> cancel, update, spin
                            x.chckflgnshow = $scope.checktimediff(x.date, x.time, x.bkstatus, x.wheelwin, x.tracking);

                            alert('Reservation ' + x.booking + ' has been changed to noshow ');
                    }else{
                        console.log(response.data);
                        alert('Payment creation Failed :'+ response.data.message);
                    }
			});
        };
                
        $scope.paymentrefund = function(x){
            
            if(parseInt(x.amount) >= parseInt(x.res_refund_amount)) {
                if (confirm("The refund amount : SGD "+ x.res_refund_amount ) == false)
					return; 
            return bookService.bookingrefund(x.restaurant, x.booking, x.email, x.booking_deposit_id, x.res_refund_amount, x.payment_method).then(function(response){
     
                    if(response.data.status===1){
                        x.depstatus = 'REFUNDED';
                        x.refund_amount = x.res_refund_amount;
                        x.chckflgrfnd = $scope.checkrefunddate(x.date, x.time,x.bkstatus, x.isShowDetails, x.deposit,x.depstatus,x.payment_method);
                       alert('The transaction has been completed.' );
                    }else{
                     
                        alert('refund Failed :'+ response.data.error);
                    }
				});
                        
            }else{
			alert("IMPT:Refund amount shall not exceed the initial deposit payment. ");
                        return;
            }          
        };
        
        $scope.menuresend = function(x){
            if (confirm("Are you sure You want to resend 'Menus & Preferences' link ") == false)
			return;
            return bookService.menureminderlink(x.restaurant, x.booking, x.email).then(function(response){
                alert("Menu & preferences pdflink has been sent");
                
            });
        }
        
        $scope.updatemnufrmtatus = function(x){
                return bookService.setmenuformstatus(x.restaurant, x.booking, x.email,x.mnufrmsubmited).then(function(response){
                    if(response.status === 1){
                       alert("Status updated");
                       x.chckflgsubmit = $scope.checkfrmsubmit(x.date, x.bkstatus,x.tracking,x.product,x.pers,x.options.chck_form_status);
                     }else{
                          alert(response.errors);
                     }
   
        		});
        }
        
        $scope.resendpaymentEmail = function(x){
            return bookService.resendEmail(x.restaurant, x.booking,$scope.email,x.recipient).then(function(response){
                    
                    if(response.status === 1){
                      alert("Email has been sent");
                     }else{
                          alert(response.errors);
                     }
                });          
        }
        
        $scope.waiveccdetails = function(x){
            
             if (confirm("Are you sure You want to  waive the credit card details") == false)
			return;
            return bookService.updateStatus(x.restaurant,x.booking,$scope.email).then(function(response){
                   
                     x.bkstatus = "";
                     x.chckflgpaystatus = $scope.checkpaystatus(x.date, x.bkstatus,x.tracking);
                     x.chckflgcus = $scope.checkdate(x);	// cus -> cancel, update, spin
                     x.chckflgnshow = $scope.checktimediff(x.date, x.time, x.bkstatus, x.wheelwin, x.tracking);
                     if(response.status === 1){
                      alert("Credit Card Details Waived");
                     }else{
                          alert(response.errors);
                     }
                
                });
          
        }
        
        
        $scope.resendpaymentsms = function(x){
            return bookService.resendsms(x.restaurant, x.receiver_mobile,'weeloy','payment_link',x.booking,x.email).then(function(response){
                    alert("Sms has been sent");
                });            
        }
        
        
        
	
	$scope.noshowbooking = function(x){
		if (confirm("Please confirm the status change of reservation " + x.booking + " ?") == false)
			return;
			
		return bookService.bookingnoshow(x.restaurant, x.booking, x.email).then(function(response){
			x.wheelwin = x.bkstatus = 'noshow';
			x.chckflgcus = $scope.checkdate(x);	// cus -> cancel, update, spin
			x.chckflgnshow = $scope.checktimediff(x.date, x.time, x.bkstatus, x.wheelwin, x.tracking);
			
			$scope.resetproperty();
			alert('Reservation ' + x.booking + ' has been changed to noshow ');
			});
		};

	$scope.filterdate = function(item) {
		if(!$scope.selDayObj.selectedDay)
			return true;
		var prop = ($scope.selDayObj.datemode === $scope.selDayObj.datemodelist[1]) ? "vcdate" : "vdate";	//"create date"
		return (item[prop] && item[prop] === $scope.selectedvDay);
		};

	$scope.setfilterdate = function() {
		var oo = $scope.selDayObj;
		$scope.extraselect("all");
		$scope.aSelDay = $scope.mydata.getTheDate();
		oo.selectedDay = $scope.mydata.getDate('-', 'reverse');
		$scope.selectedvDay = oo.selectedDay.jsdate().getTime();
		$scope.paginator.setPage(0);
		};

	mydata.setInit(forceDate, "09:00", $scope.setfilterdate, 3, new Date(currentyear-1, 6, 1), new Date(currentyear+1, 11, 31));

	$scope.setbooking = function() {
		var objstr, morestr, ll, adate, x, booking, email, sep, mod = {}, oo = {}, dnotify, notestext, notescode;

		x = $scope.selectedItem;
		adate = mydata.getDate('-', '');
		booking = x.booking;
		restaurant = x.restaurant;
		email = x.email;
	
		mod.last = mydata.lastname;
		mod.first = mydata.firstname;
		mod.phone = mydata.mobile;
		mod.pers = parseInt(mydata.npers);
		mod.time = mydata.ntimeslot;
		mod.event = mydata.nevent;
		mod.comment = mydata.comment;		
		mod.date = mydata.getDate('-', 'reverse');		// date object of selecteddate
		notestext = mydata.notestext;	
		dnotify = mydata.dnotify	
		notescode = sep = "";
		for(i = 0; i < mydata.notescode.length; i++)
			if(mydata.notescode[i].value) {
				notescode += sep + mydata.notescode[i].label;
				sep = ",";
				}

		if(typeof notestext === "string" && notestext !== "") {
			notestext = notestext.replace(/\'|\"/g, '`'); // ’`
			notestext = notestext.replace(/\s+/g, ' '); 
			}
		if(typeof notescode === "string" && notescode !== "") {
			notescode =  notescode.replace(/\'|\"/g, '`'); // ’`
			notescode =  notescode.replace(/\s+/g, ' '); 
			}
		
		if(typeof mod.date !== "string" || mod.date.length  < 10 || typeof mod.time !== "string" || mod.time.length < 5 || mod.pers < 0 || mod.pers > 99) {
			console.log('SETBOOKING', mod.date, mod.date.length, mod.time, mod.time.length, mod.pers);
			alert('Value are not set. Cannot change current booking with required modifications');
			return;
			}

		Object.keys(mod).map(function(ll) {
			if(ll  !== 'event' && typeof mod[ll] === "string" && mod[ll] !== "" && mod[ll] !== x[ll] ) {
				mod[ll] = mod[ll].replace(/\'|\"/g, '`'); // ’`
				mod[ll] = mod[ll].replace(/\s+/g, ' '); // ’`
				}
			});
			
		mod.more = JSON.stringify( { event: mod.event, notestext: notestext, notescode: notescode } );
		objstr = JSON.stringify( mod );
		
		// after JSON, for update of selectedItem
		mod.fullname = mod.first + ' ' + mod.last; 
		mod.vdate = mod.date.jsdate().getTime();
		mod.ddate = mod.date.jsdate().getDateFormat('-');
		
		if(x.date !== mod.date) {
			bookService.checkAvail(restaurant, adate, mod.time, mod.pers, booking).then(function(response) {
				if (response.data == 0) { 
					alert(response.errors + '. Unable to modify the reservation'); 
					return; 
					}

				bookService.modifullbooking(restaurant, booking, email, objstr, dnotify).then(function(response){ 
					Object.keys(mod).map(function(ll) { x[ll] = mod[ll]; });
					$scope.postmodif(response.status, response.errors, x, notestext, notescode); 
					});
				});
		} else {
			bookService.modifullbooking(restaurant, booking, email, objstr, dnotify).then(function(response){ 
				Object.keys(mod).map(function(ll) { x[ll] = mod[ll]; });
				$scope.postmodif(response.status, response.errors, x, notestext, notescode, dnotify); 
				});
			}
		};

	$scope.modifymodal = function(x) {
		var oo, val, i, maxDate = new Date();
		maxDate.setTime(maxDate.getTime() + (60 * 24 * 3600 * 1000));	// remainingday  120 days
	
		mydata.setInit(x.date.jsdate(), x.time, null, 3, new Date(), maxDate);

		mydata.lastname = x.last;
		mydata.firstname = x.first;
		mydata.mobile = x.phone;
		mydata.npers = x.pers;
		mydata.name = x.confirmation;
		mydata.comment = x.comment;	
		mydata.nevent = x.event;
		mydata.contact = "";	
		mydata.dnotify = true;	

		$scope.selectedItem = x;

		oo = ($scope.codebooking && $scope.codebooking instanceof Array) ? $scope.codebooking : [];	
		val = (x.notescode && typeof x.notescode === "string" && x.notescode.length > 1) ? x.notescode.split(",") : [];
		mydata.notescode = []; 
		for(i = 0; i < oo.length; i++)
			mydata.notescode.push( { label: oo[i].label, value: (val.indexOf(oo[i].label) > -1) });
	
		mydata.notestext = (x.notestext && typeof x.notestext === "string") ? x.notestext : "";
		mydata.notesflag = 1;
		$scope.showModal('Modify the Reservation', 'Modify Booking', $scope.setbooking, "bkgModifBackoffice.html", mydata, null);
		};
							
	$scope.postmodif = function(status, msg, x, notestext, notescode, dnotify) {
		if(status === 1)  {
			x.modified = 1;
			x.notestext = notestext;
			x.notescode = notescode;
			x.fullnotes = x.notestext + ((x.notestext !== "" && x.notescode !== "") ? "," : "") + x.notescode; 
			$scope.resetproperty();
			alert('Reservation ' + x.booking + ' has been modified' + ((!dnotify) ? ' and the notification has been sent to the guest.' : ' and the guest has NOT been notified'));
			}
		else alert('Reservation ' + x.booking + ' has NOT been modified (' + msg + ').' );
		};
				
	$scope.cancel = function () {
		modalInstance.dismiss(false);
		};
			
	$scope.getTotalPax = function() {
		var i, total;
		if (typeof $scope.filteredPeople === "undefined")
			return "";
					//if($scope.formatDisplay != 'today') return "";
		for (i = total = 0; i < $scope.filteredPeople.length; i++) {
			if ($scope.filteredPeople[i].bkstatus == "" && $scope.filteredPeople[i].pers != "99")
				total += parseInt($scope.filteredPeople[i].pers);
				}
		return total;
		};

	$scope.setallfield = function() {
		var i, data = $scope.bkfield;
		for (i = 0; i < data.length; i++)
			data[i].c = true;		
		};
		
	$scope.clearallfield = function() {
		var i, data = $scope.bkfield;
		for (i = 0; i < data.length; i++)
			data[i].c = false;		
		};
		
	$scope.performextraction = function() {
		var i, req, sep, data = $scope.bkfield;
		
		req = sep = "";
		
		if($scope.startBkg === -1 || $scope.startBkg === -1) {
			alert("Please specify a starting date/ending date");
			return;
			}
			
		for (i = 0; i < data.length; i++) {
			if(data[i].c) {
				req += sep + data[i].a;
				sep = "|";
				}
			}
		req = "restaurant=" + $scope.restaurant + "&email=" + $scope.email + "& token=" + token + "&starting=" + $scope.mydatastart.originaldate.getDateFormatReverse() + "&ending=" + $scope.mydataend.originaldate.getDateFormatReverse() + "&field=" + req;	
		//var win = window.open("fullextract.php?"+ req, "Full Extraction", "toolbar=yes,location=no,directories=yes,menubar=yes,scrollbars=yes,width=500, height=800, left=100, top=25");
		extractService.fullextract(req);
		};
						
	$scope.extractSelection = function() {

		var todayflg = ($scope.currentSelection != 'today');
		var exportselect = $scope.filteredPeople;
		var i, j, u, data, maxlimit = 1500, limit, titleAr, contentAr;
		var filename, cdate = new Date();
		
		if (todayflg) {
			titleAr = ["restaurant", "date", "time", "pax", "product", "mealtype", "salutation", "firstname", "lastname", "request", "fullnotes", "phone", "state", "status", "email", "confirmation", "repeat", "restCode", "win", "type", "create date", "booker", "company", "optin", "tablename", "hotelguest", "tracking", "notestext", "notescode"];
			contentAr = ["restaurant", "date", "time", "pers", "product", "mealtype", "salutation", "first", "last", "comment", "fullnotes", "phone", "state", "bkstatus", "email", "booking", "repeat", "restCode", "wheelwin", "type", "createdate", "booker", "company", "optin", "tablename", "hotelguest", "tracking", "notestext", "notescode"];
		} else {
			titleAr = ["restaurant", "time", "firstname", "lastname", "product", "pax", "phone", "request", "fullnotes", "repeat", "status", "booker", "company", "optin", "tablename", "hotelguest", "restCode", "notestext", "notescode"];
			contentAr = ["restaurant", "time", "first", "last", "product", "pers", "phone", "comment", "fullnotes", "repeat", "bkstatus", "booker", "company", "optin", "tablename", "hotelguest", "restCode", "notestext", "notescode"];
			}
			
		if($scope.multProd === '' && (index = titleAr.indexOf("product")) > -1) {
			titleAr.splice(index, 1); 
			contentAr.splice(index, 1); 
			}
		if($scope.isMultiple === '' && (index = titleAr.indexOf("restaurant")) > -1) {
			titleAr.splice(index, 1); 
			contentAr.splice(index, 1); 
			}
					
		data = titleAr.join(",");
		limit = exportselect.length;		
		if (limit > maxlimit) limit = maxlimit;
		for (i = 0; i < limit; i++) {
			data += "\n";			
			u = exportselect[i];
			for (j = 0; j < contentAr.length; j++) 
				data += extractService.filter(u[contentAr[j]]) + ",";			
			}	
	
		filename = "bookings" + cdate.getDate() + cdate.getMonth() + cdate.getFullYear() + ".csv";
		extractService.save(filename, data); 				
		};
					
	$scope.viewdispo = function() {

		var day = (typeof $scope.aSelDay === "Date") ? $scope.aSelDay : new Date($scope.aSelDay);
		var argday = day.getDateFormatReverse('-');
		
		bookService.getdispoDay($scope.restaurant, argday, $scope.product).then(function(response) {
			if (response.status != 1) return;
			var aa, bb, bgcolor;
			typeavail = (response.data.pax == "1") ? "pax" : "tables";
			availAr = response.data.avail.split(",");
			bookAr = response.data.booked.split(",");
			var win = window.open("", "Availability for " + day.toDateString(), "toolbar=yes,location=no,directories=yes,menubar=yes,scrollbars=yes,width=500, height=800, left=100, top=25");
			win.document.open();
			content = "";
			content += "<html><head><title>Weeloy System Dispo</title>";
			content += "<style>body { margin: 20 20 20 20 } table { font-family:helvetica;font-size:14px; } td { align:center; } .truncate { white-space: nowrap; overflow: hidden; text-overflow: ellipsis; }  tbody tr:nth-child(odd) { background: #eee; }</style></head>";
			content += "<body><center>";
			content += "<p><img src='" + imglogo + "' max-width='80' max-height='80' id='theLogo' name='theLogo'></p><br/>";
			content += "<p style='font-size:16px;padding-right:40px;'>Dispo for: <strong> " + day.toDateString(); + "</strong></p>";
			content += "<p></p>";
			content += "<table width='100%'  border='0'><head><tr style='font-weight:bold;font-size:14px'><td align='center'>time</td><td align='center'>current availability</td><td align='center'>current booked " + typeavail + "</td></tr><tr><td colspan='3'><hr/></td></tr>";
			limit = (availAr.length < 31) ? availAr.length : 30;
			for (i = 0; i < limit; i++) {
				aa = (availAr[i] <= 0) ? " " : availAr[i];
				bb = (bookAr[i] <= 0) ? " " : bookAr[i];
				content += "<tr><td>" + (9 + Math.floor(i / 2)) + ":" + ((i % 2) * 3) + "0" + "</td><td align='center'>" + aa + "</td><td align='center'>" + bb + "</td></tr>";
				}
			content += "<tr><td colspan='3'></td></tr></table></center></body></html>";
			win.document.write(content);
			win.document.close();
			win.focus();
			});
		};
			
	$scope.printing = function() {
		//var data = $("#print_content").html(); 
		var content, i, data = $scope.filteredPeople.slice(0);
		var docprint = window.open("", "Printing Reservation", "toolbar=yes,location=no,directories=yes,menubar=yes,scrollbars=yes,width=800, height=600, left=100, top=25");
		docprint.document.open();
		
		data.sort(function(a, b) { var c = a.vdate - b.vdate; return (c != 0) ? c : (a.vtime - b.vtime); });
		content = "";
		content += "<html><head><title>Weeloy System</title>";
		content += "<style>body { margin: 20 20 20 20 } table { font-family:helvetica;font-size:10px; } td { align:center; } .truncate { white-space: nowrap; overflow: hidden; text-overflow: ellipsis; } </style></head>";
		content += "<body onLoad='self.print()'><center>";
		content += "<p><img src='" + imglogo + "' max-width='80' max-height='80' id='theLogo' name='theLogo'></p><br/>";
		content += "<span style='font-size:12px;padding-right:40px;'>selected bookings: <strong> " + $scope.filteredPeople.length + "</strong></span>";
		content += "<span style='font-size:12px;padding-right:40px;'>current selection: <strong> " + $scope.currentSelection + "</strong></span>";
		content += "<span style='font-size:12px;padding-right:40px;'>total pax: <strong> " + $scope.getTotalPax() + "</strong></span>";
		content += "<br/><br />";
		content += "<table width='100%'><head><tr style='font-weight:bold;font-size:10px'><td>time</td><td>First Name</td><td>Last Name</td><td>Pax</td><td>Phone</td><td>Product</td><td>Request</td><td>Status</td><td>Table</td><td>Booker</td><td>Confirmation</td><td>Date</td><td>Repeat</td><td>Fullnotes</td></tr><tr><td colspan='13'><hr/></td></tr>";
		for (i = 0; i < data.length; i++) {
			content += "<tr><td>" + data[i].time + "</td><td>" + data[i].first.substr(0,10) + "</td><td>" + data[i].last.substr(0,10) + "</td><td>" + data[i].pers + "</td><td><div class='truncate' style='width:80px'>" + data[i].phone.substr(0,17) +
				"</div></td><td>" + data[i].product + "</td><td>" + data[i].comment.substr(0,25) + "</td><td>" + data[i].bkstatus + "</td><td>" + data[i].tablename + "</td><td>" + data[i].booker + "</td><td>" + data[i].booking + "</td><td>" + data[i].date + "</td><td>" + data[i].repeat + "</td><td>" + data[i].fullnotes + "</td></tr>";
			}
		//content += data;          
		content += "</table></center></body></html>";
		docprint.document.write(content);
		docprint.document.close();
		docprint.focus();
		};
}]);
function AdayRoutine(todayphp) {
	var dateAr, date, ctime, cweek, mweek, clweek, dd;
	console.log("todayphp: "+todayphp);
	dateAr = todayphp.split('-');
	date = new Date(dateAr[0], parseInt(dateAr[1]) - 1, dateAr[2], 0, 0, 0);
	ctime = date.getTime();
	dd = date.getDay();
	cweek = ctime - (dd * 24 * 60 * 60 * 1000);
	mweek = cweek + (7 * 24 * 60 * 60 * 1000);
	clweek = cweek - (7 * 24 * 60 * 60 * 1000);
	
	return  {
		dateAr: [],
		date: null,
		now: Date.now(), 
		ctime: ctime,
		cweek: cweek,
		mweek: mweek,
		clweek: clweek,
		tmp: 0,
		
		getDate: function(aDate, h, m, s, sep) {
			this.dateAr = aDate.split(sep);
			return new Date(this.dateAr[0], parseInt(this.dateAr[1]) - 1, this.dateAr[2], h, m, s);
			},
			
		toDays: function(aDate) {
			this.date = this.getDate(aDate, 0, 0, 0, '-');
			return ((this.date.getTime() - this.ctime) / (24 * 60 * 60 * 1000));
			},
	
		toWeek: function(aDate) {
			this.date = this.getDate(aDate, 0, 0, 0, '-');
			this.tmp = this.date.getTime();
			return (this.tmp >= this.cweek && this.tmp < this.mweek) ? 1 : -1;
			},
	
		tolWeek: function(aDate) {
			this.date = this.getDate(aDate, 0, 0, 0, '-');
			this.tmp = this.date.getTime();
			return (this.tmp >= this.clweek && this.tmp < this.cweek) ? 1 : -1;
			},
	
		dayofweek: function(aDate) {
			this.date = this.getDate(aDate, 0, 0, 0, '-');
			return this.date.getDay();
			},
	
		numberDay: function(aDate) {
			this.date = this.getDate(aDate, 23, 59, 59, '/');
			return Math.floor(((this.date.getTime() - this.now) / (24 * 60 * 60 * 1000)));
			}
	};
}