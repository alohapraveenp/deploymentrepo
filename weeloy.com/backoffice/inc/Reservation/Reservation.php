<?php
$isTheFunKitchen = (preg_match("/TheFunKitchen/", $theRestaurant)) ? 1 : 0;
$isMitsuba = ($theRestaurant === "SG_SG_R_MitsubaJapaneseRestaurant") ? 1 : 0;
$isBacchanalia = ($theRestaurant === "SG_SG_R_TheFunKitchen" || $theRestaurant === "SG_SG_R_Bacchanalia" ) ? 1 : 0;
if(empty($bkreport)) $bkreport = 0;

$res = new WY_restaurant;
$res->getRestaurant($theRestaurant);

if ($res->result <= 0) {
    $debug = new WY_debug;
    $debug->writeDebug("ERROR-BACKOFFICE", "REPORT", "Invalid Restaurant :" . $theRestaurant);
    exit;
}
$ccextraction = $res->CCExtractionFormat();
$mediadata = new WY_Media($theRestaurant);
$logo = $mediadata->getLogo($theRestaurant);
$isShowLog = ($res->checkactivitylog() >0) ? 1:0;
?>
<div ng-include="'inc/myModalModif.html'"></div>
<div class="container" ng-controller="BookingController" ng-init="moduleName='booking'; listingVisitFlag = true; contentVisitFlag = false; extractFlag = false; formatDisplay = 'standard';">
	<input type='hidden' id='token' value ="<?php echo $_SESSION['user_backoffice']['token'] ?>" />
	<input type='hidden' id='restaurant' value ="<?php echo $theRestaurant ?>" />
	<input type='hidden' id='email' value ="<?php echo $_SESSION['user_backoffice']['email'] ?>" />
	<input type='hidden' id='ccextraction' value = "<?php echo $ccextraction ?>" />
	<input type='hidden' id='isTheFunKitchen' value = "<?php echo $isTheFunKitchen ?>" />
	<input type='hidden' id='imglogo' value="<?php echo $logo ?>" />
	<input type='hidden' id='multProd' value="<?php echo $multProduct ?>" />
	<input type='hidden' id='isMitsuba' value="<?php echo $isMitsuba ?>" />
	<input type='hidden' id='bkreport' value="<?php echo $bkreport ?>"/>
	<input type='hidden' id='isShowLog' value="<?php echo $isShowLog ?>" />
	<input type='hidden' id='isBacchanalia' value="<?php echo $isBacchanalia ?>" />
	<div ng-show='listingVisitFlag'>
	<div class='row'>
		<div class="col-md-2">
			<div class="form-group" style='margin-bottom:25px;width:150px;'>
				<div class="input-group">
					<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
					<input type="text" class="form-control input-sm" ng-model="searchText" ng-change="paginator.setPage(0);" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" > 
				</div>
			</div>
		</div>
		<div class="col-md-10">

		<ul class="nav navbar-nav">
		<li>
			<div class="input-group">
			<span><button  type="button" class="btn btn-default" ng-click="mydata.dateopen($event)"><i class="glyphicon glyphicon-calendar"></i></button></span>
		    	<input type="text" class="form-control" uib-datepicker-popup="{{mydata.formats[0]}}"  ng-model="mydata.originaldate"  ng-change='mydata.onchange();' is-open="mydata.opened" min-date="mydata.minDate" max-date="mydata.maxDate" datepicker-options="mydata.dateOptions" ng-required="true" close-text="Close" style="width:0;opacity:0" />
			</div>
		</li>

		<li>
			<div class="btn-group" uib-dropdown style="margin:0 5px 0 5px;">
				<button type="button" class="btn btn-primary btn-sm" uib-dropdown-toggle aria-expanded="false">Set Selection &nbsp;<span class="caret"></span></button>
				<ul class="dropdown-menu" uib-dropdown-menu role="menu">
					<li ng-repeat="x in extratitle"><extitle name="{{x.a}}"/></extitle></li>
					<li><a href class='glyphr glyphicon glyphicon-ok' ng-click="extraselect('reserve')"> by reservation date</a></li>
					<li><a href class='glyphc' ng-click="extraselect('create')"> by creation date</a></li>
					<li ng-if="isMultiple" class="divider"></li>
					<li ng-if="isMultiple"><a href ng-class="{ 'glyphicon glyphicon-ok': oneresto === 1 }" ng-click="extraselect('oneresto')"> select {{ restotitle }} </a></li>
				</ul>
			</div> 		
		</li>

		<li><button  type="button" class="btn btn-success btn-sm" ng-click='extractSelection();' style='color:white;margin-left:5px'>Extract Selection  &nbsp;<i class='glyphicon glyphicon-save'></i></button></li>

		<li><button  type="button" class="btn btn-info btn-sm" ng-click="reset('extractFlag')" style="margin-left:5px">Full Extract</button></li>

		<li ng-if="multProd === ''"><button  type="button" class="btn btn-default" ng-click="viewdispo()" style="margin-left:15px"><i class="glyphicon glyphicon-eye-open"></i></button></li>
		
		<li><button  type="button" class="btn btn-default" ng-click="printing()" style="margin-left:5px"><i class="glyphicon glyphicon-print"></i></button></li>

		<li ng-if="isMitsuba"><button  type="button" class="btn btn-default" ng-click="mitsubaValidation()" style="margin-left:5px;color:green"><i class="glyphicon glyphicon-ok"></i></button></li>
		
		<li>
			<div class="btn-group" uib-dropdown>
				<button type="button" class="btn btn-default btn-xs" uib-dropdown-toggle aria-expanded="false" style='font-size:11px;margin-left:15px;'>Page Size <span class="caret"></span></button>
				<ul class="dropdown-menu" uib-dropdown-menu role="menu">
					<li ng-repeat="x in paginator.pagerange()" class='glyphiconsize'><a href ng-class="{ 'glyphicon glyphicon-ok glyphiconsize': x === paginator.getRowperPage() }" ng-click="paginator.setRowperPage(x)"> {{x}}</a></li>
				</ul>
			</div> 		
		</li>
		
		</ul>
		</div>
	</div>     
	<div class='row'>
		<div class="col-md-12">
			<ul class="nav navbar-nav">
			<li class='infobk'>selected bookings: <strong> {{filteredPeople.length}} </strong></li>
			<li class='infobk'>current selection: <strong> {{currentSelection}} </strong></li>
			<li class='infobk'>total pax: <strong> {{getTotalPax();}} </strong></li>
			<li class='infobk'>modified: <strong> * </strong></li>
			<li ng-if="isMultiple" class='infobk'>view : <strong> {{ selectionrestotype }} </strong></li>
			<li class='infobk' ng-if="selDayObj.selectedDay" class='infobk'><select-create-booking-date theday="selDayObj"></select-create-booking-date></li>
			</ul>
		</div>
	</div><br/>

	<!-- try ng-switch but doest not seem to work. it has a bad side effect on fileteredPeople !? -->
	<div>
		<table width='100%' class="table table-condensed table-striped" style='font-family:helvetica;font-size:11px;'>
			<thead><tr style='font-family:helvetica;font-size:11px;'><th ng-repeat="y in tabletitle"><tbtitle var="{{y.a}}" name='tabletitle'  module='moduleName'/></th><th ng-if ="multProd !== '' && formatDisplay != 'today'"></th></tr></thead>
			<!-- today normal -->
			<tr ng-if="formatDisplay != 'today'" ng-repeat="x in filteredPeople = (names| filter:searchText | filter: filterdate ) | sizefilter:this | orderBy:predicate:reverse | slicefilter:paginator.getPage():paginator.getRowperPage():paginator.getItemCount() | limitTo:paginator.getRowperPage()" style='font-family:helvetica;font-size:11px;'>
				<td ng-repeat="z in tabletitle">
					
					<tb-listingbkg>
						<a  href ng-click="view(x)">{{ x[z.a] | adatereverse:z.c}}<span ng-if="z.a === 'booking' && x.modified >= 0">*</span> </a>
					</tb-listingbkg>
				</td>
                <td ng-if ="multProd !== ''"><a href popover="{{x.comment}}" data-popover-trigger="mouseenter" ><span ng-if="x.comment !== '' "><i class ='glyphicon glyphicon-asterisk'></i></span></a></td> 
                <td ng-if ="multProd !== '' && x.chckflgsubmit "><a href popover="Menu form has not submit yet " data-popover-trigger="mouseenter" ><span ><i class ='glyphicon glyphicon-warning-sign' style='color:orange'></i></span></a></td>
                
			</tr>

			<!-- today printing -->
			<tr ng-if="formatDisplay == 'today'" ng-repeat="x in filteredPeople = (names| filter:searchText | filter: filterdate ) | sizefilter:this | orderBy:predicate:reverse | slicefilter:paginator.getPage():paginator.getRowperPage():paginator.getItemCount() | limitTo:paginator.getRowperPage()" style='font-family:helvetica;font-size:11px;'>
				<td ng-repeat="z in tabletitle | filter: { a:'!state'}"><tb-listingbkg><a href ng-click="view(x)">{{ x[z.a]  | adatereverse:z.c}}<span ng-if="z.a === 'booking' && x.modified >= 0">*</span></a></tb-listingbkg></td>
				<td><select ng-model="x.state" ng-options="val for val in stateAr" ng-change="savechgstatebkg(x)"></select></td>
			</tr>
			<tr><td colspan='{{tabletitle.length+4}}'></td></tr>
				</table>
				<div ng-if="filteredPeople.length >= paginator.getRowperPage()" class="col-md-12" align="center" style='font-size:11px' ng-include="'inc/paginator.html'"></div>
        </div>
		</div>

	<div ng-show='contentVisitFlag'>
            
		<div class="col-md-12" >
		<br />
		<a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a>
		<br />
                 <h4 style='text-align:center;'>Booking Details </h4>
		<table class='table-striped' style="margin: 0 0 50px 10px;font-size:14px;font-family: Roboto">		
			<tr ng-repeat="z in tabletitleContent | filter:showtest">
				<td nowrap>{{z.b}}: </td><td width='20px'>&nbsp;</td><td>{{ selectedItem[z.a] | adatereverse:z.c }}</td>
			</tr>
			<tr ng-show="selectedItem.ordersdetails.length > 0"><td nowrap>Orders:</td><td> </td><td><select><option ng-repeat="k in selectedItem.ordersdetails">{{k}}</option></select></td></tr>
			<tr ng-show="formatDisplay == 'today'"><td nowrap>State:</td><td> </td><td style='color:red;'>{{ selectedItem.state}}</td></tr>
			<tr ng-show="isTheFunKitchen"><td nowrap>membCode:</td><td> </td><td>{{ selectedItem.membCode}}</td></tr>
			<tr ng-show="selectedItem.modified >= 0"><td nowrap>Modified Booking: </td><td> </td><td>{{ selectedItem.modifiedinfo }}</td></tr>
			<tr><td>&nbsp;</td><td> &nbsp; </td><td></td></tr>
                      
			<tr ng-show="selectedItem.chckflgcus && selectedItem.booking_deposit_id===''"><td>&nbsp;</td>    
				<td nowrap colspan='2'><strong><a href ng-click="cancelbooking(selectedItem);" class="btn btn-warning btn-xs cancelhover"  style='width:100px'>Cancel booking</a><br /></strong></td>
			</tr>
			<tr ng-show="selectedItem.chckflgnshow && selectedItem.booking_deposit_id === ''"><td>&nbsp;</td>
				<td nowrap colspan='2'><strong><a href ng-click="noshowbooking(selectedItem);" class="btn btn-warning btn-xs cancelhover"  style='width:100px'>Set noshow </a><br /></strong></td>
			</tr>
                  
			<tr ng-show="selectedItem.chckflgcus && (selectedItem.booking_deposit_id === '' || isBacchanalia || selectedItem.restaurant =='SG_SG_R_Esquina' ) && (selectedItem.restaurant !=='SG_SG_R_BurntEnds')"><td>&nbsp;</td>
                            <td nowrap colspan='2'><strong><a href ng-click="modifymodal(selectedItem)" class="btn btn-warning btn-xs cancelhover"  style='width:100px'>Modify booking</a><br /></strong></td>
			</tr>

			<tr><td>&nbsp;</td><td> &nbsp; </td><td></td></tr>
                  
                        <tr ng-show="selectedItem.isShowDetails && selectedItem.payment_method ==='carddetails' "><td nowrap>Cancelled  Charge :</td><td width='20px'> &nbsp;</td><td>{{ selectedItem.currency}} &nbsp;{{ selectedItem.amount}}</td></tr>
                        <tr ng-show="selectedItem.isShowDetails && selectedItem.payment_method!=='carddetails' "><td nowrap>Deposit amount : </td><td width='20px'> &nbsp;</td><td>{{ selectedItem.currency}} &nbsp;{{ selectedItem.amount}}</td></tr>
                        <tr ng-show="selectedItem.isShowDetails"><td nowrap>Payment Status: </td><td width='20px'> &nbsp;</td><td>{{ selectedItem.depstatus}}</td></tr>
                        <tr ng-show="selectedItem.isShowDetails && selectedItem.deposit === 'paid'"><td nowrap>Payment Method: </td><td width='20px'> &nbsp;</td><td>{{ selectedItem.paymethod}}</td></tr>
                        <tr ng-show="selectedItem.isShowDetails && selectedItem.deposit === 'paid'"><td nowrap>Payment Id: </td><td width='20px'> &nbsp;</td><td>{{ selectedItem.booking_deposit_id}}</td></tr>
           			
			<tr ng-show="selectedItem.depstatus ==='REFUNDED'"><td nowrap>Refunded Amount: </td><td width='20px'> &nbsp;</td><td>{{ selectedItem.currency}} &nbsp;{{ selectedItem.refund_amount}}</td></tr>
                        
			<tr><td>&nbsp;</td><td> &nbsp; </td><td></td></tr>
			<tr ng-show='selectedItem.chckflgcus && selectedItem.chckflgspin'><td nowrap><strong><a class='btn btn-primary btn-sm' ng-click="spin(selectedItem)" style='color:white;'>SPIN THE WHEEL</a></strong></td><td> &nbsp; </td><td> &nbsp;</td></tr>
			<tr><td>&nbsp;</td><td> &nbsp; </td><td></td></tr>
		</table>
    
                <div ng-if="selectedItem.chckflgcus && multProd !== '' && selectedItem.burnendlinkflg"  style="margin: 0 0 20px 30px;font-size:13px;font-family: Roboto">
                        <h4 style='text-align:left;'>Menus & Preferences Form </h4>
                        <br /> 
                        <div class="form-group mycheckboxdiv">
                            <label>
                               <input  class="mycheckbox" type="checkbox" ng-model="selectedItem.mnufrmsubmited"  ng-checked ='{{selectedItem.mnufrmsubmited}}' ng-click="updatemnufrmtatus(selectedItem)"  >&nbsp; Received Menus form 
                            </label>
                            <label>
                               
                              &nbsp; <div class="btn-group" ><a class='btn btn-warning btn-sm' ng-click="menuresend(selectedItem);"  style='color:white; margin-left:60px;'> Resend form</a></div>
                            </label>
                        </div>
                </div>
                  
              
                <div ng-show="(selectedItem.chckflgcus || selectedItem.chckflgnshow) && selectedItem.booking_deposit_id !=='' && selectedItem.deposit === 'paid' && selectedItem.bkstatus != 'expired'" style="margin: 0 0 20px 10px;font-size:14px;font-family: Roboto">
                
                        <h5 style='text-align:left; display:table'>Booking Cancel policy</h5>
                        <div style="margin: 0 0 20px 20px;"> 
                            <span ng-if ="selectedItem.restaurant !== 'SG_SG_R_Pollen'">{{paymenttnc}}.</span>
                            <span ng-if ="selectedItem.restaurant === 'SG_SG_R_Pollen'"> 
                               <p ng-repeat="str in paymenttncAr">{{ str }} </p>
                            </span>
                            
                         </div>
                        <!-- <div ng-if='!isBacchanalia' style="margin: 0 0 20px 20px;"  > 
                            <div ng-repeat ="p in policy">
                                <span ng-if="p['percentage']== 100">{{p['duration']}} {{ selectedItem.currency}} {{selectedItem.amount}}.</span>
                                <span ng-if="p['percentage']== 0">{{p['duration']}}</p></span>
                            </div>
                         </div>
                        <div ng-if='isBacchanalia' style="margin: 0 0 20px 20px;"> 
                            <span >{{policybaccahanalia}}.</span>
                         </div>
                          <div ng-if="selectedItem.retaurant === 'SG_SG_R_TheOneKitchen'" style="margin: 0 0 20px 20px;"> 
                            <span >{{medinitnc}}.</span>
                         </div> -->
                        
                        <br /> 
                        <div class="amount" style="margin: 0 0 20px 20px;">
                            <p ng-show="selectedItem.amount > 0 && selectedItem.paymethod !=='paypal' "><strong> Maximum cancellation  charge : {{ selectedItem.currency}} {{selectedItem.amount}} </strong>  </p><br />
                            <span>Amount : &nbsp; {{ selectedItem.currency}}&nbsp;</span> <input type="text" ng-model ="selectedItem.refund_amount" >
                            <div class="btn-group" ng-show="!selectedItem.chckflgnshow " style="padding-left:10px;"><a class='btn btn-danger btn-sm' ng-click="cancelpaymentbooking(selectedItem);" class="cancelhover" style='color:white;'> Cancel</a></div>
                            <div class="btn-group" ng-show="selectedItem.chckflgnshow " style="padding-left:10px;" ><a class='btn btn-danger btn-sm' ng-click="noshowpaymentbooking(selectedItem);" class="cancelhover" style='color:white;'> No Show</a></div>
                        </div>
                </div>
                <div ng-show="selectedItem.chckflgrfnd  && selectedItem.booking_deposit_id !==''" style="margin: 0 0 20px 30px;font-size:14px;font-family: Roboto">
                        <h4 style='text-align:center;'>Refund payment</h4>
                        <br /> 
                        <div class="amount" style='text-align:center;'>
                            <span>Amount : &nbsp; {{ selectedItem.currency}}&nbsp;</span> <input type="text" ng-model ="selectedItem.res_refund_amount" >
                            <div class="btn-group"  style="padding-left:10px;"><a class='btn btn-danger btn-sm' ng-click="paymentrefund(selectedItem);" class="cancelhover" style='color:white;'>Refund</a></div>
                        </div>
                            
                </div>
                <div ng-if="selectedItem.chckflgpaystatus && (selectedItem.bkstatus == 'pending_payment' || selectedItem.bkstatus == 'expired' || selectedItem.modified >= 0 )"  style="margin: 0 0 20px 30px;font-size:14px;font-family: Roboto">
                        <h4 style='text-align:left;'>Payment reminder - Resend Email & Sms </h4>
                        <br /> 
                        <div class="form-group mycheckboxdiv">
                        
                            <label>
                                <span style='margin-bottom:10px;'>Send payment reminder email&nbsp;</span>
                               <input  type="text" ng-model="selectedItem.recipient"  >&nbsp;  <input type='button' class='btn btn-warning' ng-click="resendpaymentEmail(selectedItem);" value ='Send Email'>
                            </label>
                            <div class='sms-sections' ng-if="selectedItem.restaurant !=='SG_SG_R_Pollen'">
                              <label style='margin-top:10px;padding-top:10px;'>
                                <span >Send payment reminder sms:&nbsp;</span>
                               &nbsp;&nbsp;<input type="text" ng-model="selectedItem.receiver_mobile"  >&nbsp;  <input type='button' class='btn btn-warning' ng-click="resendpaymentsms(selectedItem);"  value ='Send Sms'>
                            </label>
                            </div>
                        </div>
                </div>
                    
                <div ng-if="selectedItem.chckflgpaystatus && selectedItem.bkstatus == 'pending_payment' && selectedItem.restaurant !=='SG_SG_R_Pollen' && selectedItem.restaurant !=='SG_SG_R_Esquina'"  style="margin: 0 0 20px 30px;font-size:14px;font-family: Roboto">
                    <h4 style='text-align:left;'>Waive credit card details </h4>
                        <br /> 
                    <div class="form-group">
                        <label ng-if="selectedItem.restaurant =='SG_SG_R_TheOneKitchen'">
                           <input type='button' class='btn btn-warning' ng-click="waiveccdetails(selectedItem);" value ='Waive The Payment Now'>
                        </label>
                           <label >
                           <input type='button' class='btn btn-warning' ng-click="waiveccdetails(selectedItem);" value ='Waive CC Details Now'>
                        </label>
                    </div>
                </div>
                <div class='table-striped2' ng-if="eventlog.length>0"  style="margin: 0 0 20px 10px;font-size:14px;font-family: Roboto">
                    <h5 style='text-align:left; display:table'>Actions History</h5>
                        <div ng-repeat ="log in eventlog" style='margin:5px 0 5px 0; display:table-row;padding:5px;'>
                            
                            <span  style="display:table-cell">{{log['name']}} {{log['description']}} <span ng-if="log['action'] == '706' || log['action'] == '717' || log['action'] == '714'  ">(${{log['other']}}) </span> </span>
                            <span style="display:table-cell; padding-left:20px;"> {{log['log_date']}}  </span>  
  
                        </div><br /> 
                </div>

	</div>
	</div>

	<div ng-show='mitsubaFlag'>
		<div class="col-md-12" ng-if='mitsubaFlag'>
		<br />
		<a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a>
		<br />
		<table class='table-striped' style="margin: 0 0 150px 30px;font-size:13px;font-family: Roboto">		
		<thead><tr style='font-family:helvetica;font-size:11px;'><th ng-repeat="y in tabletitle | filter: { b:'!Wins'}"><tbtitle var="{{y.a}}" name='tabletitle'  module='moduleName'/></th><th>validate</th></tr></thead>
		<tr ng-repeat="x in filteredMistuba = (names| filter:{ validate:'!1'}| filter:{ bkstatus:'!cancel'}) | sizefilter:this | orderBy:predicate:reverse | slicefilter:paginator.getPage():paginator.getRowperPage():paginator.getItemCount() | limitTo:paginator.getRowperPage()" style='font-family:helvetica;font-size:11px;'>
			<td ng-repeat="z in tabletitle | filter: { a:'!wheelwin'}"><tb-listingbkg><a href ng-click="view(x, 'mitsubaFlag')">{{ x[z.a] | adatereverse:z.c}}<span ng-if="z.a === 'booking' && x.modified >= 0">*</span></a></tb-listingbkg></td>
			<td><button type='button' class='btn btn-primary btn-xs' ng-click='validatebkg(x)' style='font-size:10px;'>validate</button></td>
		</tr>
		<tr><td colspan='{{tabletitle.length+4}}'>&nbsp;</td></tr>
		<tr><td colspan='{{tabletitle.length+4}}'><div ng-if="filteredMistuba.length >= paginator.getRowperPage()" class="col-md-12" align="center" style='font-size:11px' ng-include="'inc/paginator.html'"></div></td></tr>
		</table>
		
		</div>
	</div>

	<div ng-show='extractFlag'><H3> Extraction </h3>
		<br />
			<a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a>
		<br />
	  <table width='100%'><tr>
		<td width='75'>
		<div class="input-group">
			<span><button  type="button" class="btn btn-default" ng-click="mydatastart.dateopen($event)"><i class="glyphicon glyphicon-calendar"><span style='font-size:9px;'><br />start</span></i></button></span>
		    <input type="text" class="form-control" uib-datepicker-popup="{{mydatastart.formats[0]}}"  ng-model="mydatastart.originaldate"  ng-change='mydatastart.onchange();' is-open="mydatastart.opened" min-date="mydatastart.minDate" max-date="mydatastart.maxDate" datepicker-options="mydatastart.dateOptions" ng-required="true" close-text="Close" style="width:0;opacity:0" />
		</div>
		</td>

		<td width='75'>
		<div class="input-group">
			<span><button  type="button" class="btn btn-default" ng-click="mydataend.dateopen($event)"><i class="glyphicon glyphicon-calendar"><span style='font-size:9px;'><br />end</span></i></button></span>
		    <input type="text" class="form-control" uib-datepicker-popup="{{mydataend.formats[0]}}"  ng-model="mydataend.originaldate"  ng-change='mydataend.onchange();' is-open="mydataend.opened" min-date="mydataend.minDate" max-date="mydataend.maxDate" datepicker-options="mydataend.dateOptions" ng-required="true" close-text="Close" style="width:0;opacity:0" />
		</div>
		</td>
		<td width='30'>&nbsp;</td>
		<td>
		<label ng-repeat="oo in bkfield" style="font-size:12px;margin-left:10px;"><input type="checkbox" ng-model="oo.c"/> {{oo.b}} </label>
		</td>
		</tr></table>
		<div class="row"><button type="button" class="btn btn-default btn-xs" ng-click="setallfield()" style="font-size:10px;">set all</button> <button type="button" class="btn btn-default btn-xs" ng-click="clearallfield()" style="margin-left:20px;font-size:10px;">clear all</button></div><br />
		<div class="row"><span style="font-size:10px;">*BDate: Reservation Date<br />*ADate: Annulation/Cancel Date<br />*CDate: Create Date of Booking</span></div>
		
		<div class="row" ng-if="startBkg !== -1" style="font-size:11px;margin-top:20px;">
		start: {{ mydatastart.originaldate.getDateFormat() }} &nbsp;&nbsp;&nbsp; end: {{ mydataend.originaldate.getDateFormat() }} 
		</div>
		<br />
			<div align="center"><a href class='btn btn-info btn-sm' ng-click="performextraction()" style='width:200px;color:white;'>Execute Extraction &nbsp;<span class='glyphicon glyphicon-save'></span></a></div>
		<br />
		<br />

	</div>
</div>
<script src="inc/Reservation/Reservation.js?1"></script>