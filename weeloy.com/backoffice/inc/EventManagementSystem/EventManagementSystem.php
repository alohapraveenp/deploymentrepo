<script>
<?php
	$mediadata = new WY_Media($theRestaurant);
	$res = new WY_restaurant;
	$res->getRestaurant($theRestaurant);
	$price = ($res->checkeventbooking() > 0) ? 1 : 0;
    $imgAr = $mediadata->getEventPictureNames($theRestaurant);
    printf("var imgEvent = [");
    for($i = 0, $sep = ""; $i < count($imgAr); $i++, $sep = ", ")
    	printf("%s '%s'", $sep, $imgAr[$i]);
    printf("];");
    printf("var pathimg = '%s';", $mediadata->getFullPath('small'));
?>
</script>
<div class="container">
    <div class="row">
        <div class="col-md-12 left-sec">
<div ng-controller="EventController" ng-init="moduleName='event'; listEventFlag = true; viewEventFlag=false; createEventFlag=false;" >
    <input type='hidden' id='token' value ="<?php echo $_SESSION['user_backoffice']['token'] ?>" />
    <input type='hidden' id='email' value ="<?php echo $_SESSION['user_backoffice']['email'] ?>" />
	<input type='hidden' id='restaurant' value ="<?php echo $theRestaurant ?>" />
	<input type='hidden' id='price'  value ="<?php echo $price ?>" />
	<div class="col-md-12" ng-show='viewBkEventFlag'>
		<br /><a href class='btn btn-info btn-sm customColor' ng-click="backlisting('viewlistBkEventFlag')"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a><br />
		<table class='table-striped' style="margin: 0 0 120px 30px;font-size:13px;font-family: Roboto">		
			<tr ng-repeat="y in tableBktitleContent | filter: {b: '!picture'}"><td nowrap ><strong> {{ y.b }} </strong></td><td width='30'> &nbsp; </td><td>{{ selectedBooking[y.a] | adatereverse:y.c }} </td></tr>
                         <tr><td nowrap ><strong>Tracking </strong></td><td width='30'> &nbsp; </td><td>{{ selectedBooking.tracking }} </td></tr>
                        <tr><td nowrap ><strong>Payemnt Id </strong></td><td width='30'> &nbsp; </td><td>{{ selectedBooking.paykey }} </td></tr>
                        <tr ><td nowrap ><strong>Payment Type</strong></td><td width='30'> &nbsp; </td><td>{{ selectedBooking.payment_method }}  </td></tr>
                        <tr ng-if = "selectedBooking.payment_status === 'REFUNDED' "><td nowrap ><strong>Refunded Amount</strong></td><td width='30'> &nbsp; </td><td>{{ selectedBooking.currency }} {{ selectedBooking.refund_amount }} </td></tr>
                        <tr><td nowrap ><strong>Payment Status </strong></td><td width='30'> &nbsp; </td><td>{{ selectedBooking.payment_status }} </td></tr>

		</table>
                <div ng-show="selectedBooking.amount > 0  && selectedBooking.payment_status =='COMPLETED'" style="margin: 0 0 20px 30px;font-size:14px;font-family: Roboto">
                        <h4 style='text-align:left;'>Refund payment</h4>
                        <br /> 
                        <div class="amount" style='text-align:left;'>
                            <span>Amount : &nbsp; SGD&nbsp;</span> <input type="text" ng-model ="selectedBooking.refund_amount" >
                            <div class="btn-group"  style="padding-left:10px;"><a class='btn btn-danger btn-sm' ng-click="depositRefund(selectedBooking);" class="cancelhover" style='color:white;'>Cancel Event</a></div>
                        </div>
                            
                </div>
	</div>
	<div id='listing' ng-show='viewlistBkEventFlag'>
		<br />
		<a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a>
		<br />
       	<h4 style="text-align:center;text-transform: uppercase;color:navy;">  {{ selectedItem.title }} </h4><br />
		<div class="form-group"  style='margin-bottom:25px;'>
			<div class="col-md-4">
				<div class="input-group col-md-4">
					<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
					<input type="text" class="form-control input-sm" ng-model="searchSubText" ng-change="paginatorsub.setPage(0);" style='width:200px;'> 
				</div>
				<span class='infobk' style='margin-left:20px;'>selected event bookings: <strong> {{filteredBook.length}} </strong></span><br />
			</div>
			<div class="col-md-2"></div>
			<div class="col-md-4">
			</form>
			<form action="echo.php" id="extractForm" name="extractForm" method="POST" target="_blank">
				<input type="hidden" value="testing" ng-model='content' name="content" id="content">
				<a href ng-click='extractSelection();' class="btn btn-success btn-sm" style='color:white;margin-left:5px'>Extract Selection  &nbsp;<span class='glyphicon glyphicon-save'></span></a>
			</form>
			</div>
		</div>
       	<div style=" clear: both;"></div>
		<table width='100%' class="table table-condensed table-striped" style='font-size:12px;'>
			<thead><tr><th ng-repeat="y in tabletitlebook"><tbtitle var="{{y.a}}" name='tabletitle' module='moduleName'/></th></tr></thead>
			<tbody style='font-family:helvetica;font-size:12px;'>
				<tr ng-repeat="e in filteredBook = (selectedItem.bkevent | filter:searchSubText) | sizefilter:this | orderBy:predicate:reverse | slicefilter:paginatorsub.getPage():paginatorsub.getRowperPage():paginatorsub.getItemCount() | limitTo:paginatorsub.getRowperPage()" style='font-family:helvetica;font-size:11px;'> 
					<td ng-repeat="w in tabletitlebook"><a href ng-click="viewevbook(e)" ng-style="{ 'text-align' : getAlignment(e[w.a]) }">{{ e[w.a] | adatereverse:w.c | notzero }}</a></td>
				</tr>
				<tr><td colspan='{{tabletitlebook.length + 5}}'></td></tr>
			</tbody>
		</table>
		<div ng-if="filteredBook.length >= paginatorsub.getRowperPage()" class="col-md-12" align="center" style='font-size:11px' ng-include="'inc/paginatorsub.html'"></div>
	</div>

	<div id='listing' ng-show='listEventFlag'>
		<div class="form-group"  style='margin-bottom:25px;'>
			<div class="col-md-4">
				<div class="input-group col-md-4">
					<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
					<input type="text" class="form-control input-sm" ng-model="searchText" ng-change="paginator.setPage(0);" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" style='width:200px;'> 
				</div>
			</div>
			<div class="col-md-2"></div>
			<div class="col-md-4">
			<a href ng-click='create();' class="btn btn-success btn-sm" style='color:white;width:200px;'><span class='glyphicon glyphicon-certificate'></span> &nbsp;Create a New Event</a>
			</div>
		</div>
       		<div style=" clear: both;"></div>
		<table width='100%' class="table table-condensed table-striped" style='font-size:12px;'>
			<thead>
				<tr>
				<th ng-repeat="y in tabletitle"><tbtitle var="{{y.a}}" name='tabletitle' module='moduleName'/></th>
                <th width='20'>featured</th>
  				<th width='20'>update</th>
				<th width='5'> &nbsp; </th>
               	<th width='20'>delete</th>
				</tr>
			 </thead>
			<tbody style='font-family:helvetica;font-size:12px;'>
				<tr ng-repeat="x in filteredEvent = (names | filter:searchText) | sizefilter:this | orderBy:predicate:reverse | slicefilter:paginator.getPage():paginator.getRowperPage():paginator.getItemCount() | limitTo:paginator.getRowperPage()" style='font-family:helvetica;font-size:11px;'> 
					<td ng-repeat="y in tabletitle"><a href ng-click="view(x, y.a)" ng-style="{ 'text-align' : getAlignment(x[y.a]) }"> {{ x[y.a] | adatereverse:y.c | notzero }}<span ng-if="y.a === 'bkpax' && x.bkevent.length > 0 && x.bkpax === 0">0</span></a></td>
					<td align='center'><span ng-if="x['featured']" class='glyphicon glyphicon-star orange'></span> </td>
					<td align='center'><a href ng-click="update(x)"><span class='glyphicon glyphicon-pencil blue'></a></span></td>
					<td >&nbsp;</td>
					<td align='center'><a href ng-click="delete(x)"><span class='glyphicon glyphicon-trash red'></span></a></td>
				</tr>
				<tr><td colspan='{{tabletitle.length + 6}}'></td></tr>
			</tbody>
		</table>
		<div ng-if="filteredEvent.length >= paginator.getRowperPage()" class="col-md-12" align="center" style='font-size:11px' ng-include="'inc/paginator.html'"></div>
	</div>

	<div class="col-md-12" ng-show='viewEventFlag'>
		<br /><a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a><br />
		<table class='table-striped' style="margin: 0 0 150px 30px;font-size:13px;font-family: Roboto">		
			<tr ng-repeat="y in tabletitleContent | filter: {b: '!picture'}"><td nowrap ><strong> {{ y.b }} </strong></td><td> &nbsp; </td><td>{{ selectedItem[y.a] | adatereverse:y.c }} </td></tr>
			<tr ng-if="selectedItem['picture'] != ''"><td nowrap><strong>Picture: </strong></td><td> &nbsp; </td><td class="showpict"><img ng-src="{{path}}{{selectedItem.picture}}" height='150'/></td></tr>
		</table>
	</div>

	<div class="col-md-12" ng-show='createEventFlag'>
		<br />
		<a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a>
		<br />
		 <div class="col-md-2"></div>
		 <div class="col-md-8">
		   <div class="row" ng-repeat="y in tabletitleContent | filter: {t: '!dontshow' }" style="margin: 0 0 20px 0;font-size:12px;font-familly:Roboto">
                        
			<div class="input-group" ng-if="y.t === 'input'">
				<span class="input-group-addon input11"><i class="fa fa-{{y.d}}"></i>&nbsp; {{y.b}}</span>
				<input type="text" class="form-control input-sm" ng-model="selectedItem[y.a]" ng-change="cleaninput(y.a)" ng-readonly="y.readonly ===1 && action=='update'" >
			</div> 
                <div class="input-group" ng-if="y.t === 'inputprice' && showprice === '1'">
				<span class="input-group-addon input11"><i class="fa fa-{{y.d}}"></i>&nbsp; {{y.b}}</span>
				<input type="text" class="form-control input-sm" ng-model="selectedItem[y.a]" ng-change="cleaninput(y.a)" ng-readonly="y.readonly ===1 && action=='update'" >
			</div> 
			<div class="input-group" ng-if="y.t === 'textarea'">
				<span class="input-group-addon input11"><i class="fa fa-{{y.d}}"></i>&nbsp; {{y.b}}</span>
				<textarea class="form-control input-sm" ng-model="selectedItem[y.a]" ng-change="cleaninput(y.a)" rows="5" ></textarea>
			</div>
			<div class="input-group" ng-if="y.t === 'date'">
				<span class="input-group-btn input11">
				<button type="button" class="btn btn-default btn-sm" ng-click="y.u.dateopen($event)"><i class="fa fa-calendar"></i>&nbsp; {{y.b}}</button>
				</span>
				<input type="text" class="form-control input-sm" uib-datepicker-popup="{{y.u.formats[0]}}" min-date="y.u.minDate" max-date="y.u.maxDate" ng-model="y.u.originaldate"
				is-open="y.u.opened" datepicker-options="y.u.dateOptions" ng-change="y.u.onchange();" close-text="Close" readonly />
			</div>
			   <div class="input-group"  ng-if="y.t==='checkbox' ">
				   <input   type="checkbox" ng-model="selectedItem[y.a]" ng-checked ="{{selectedItem[y.a]}}"> &nbsp; {{y.b}}
					   
				</div>
            <div class="input-group" ng-if="y.t === 'array'">
				<div class='input-group-btn' uib-dropdown >
				<button type='button' class='btn btn-default btn-sm input12' uib-dropdown-toggle >
					<i class="fa fa-{{y.d}}"></i>&nbsp; {{y.b}} <span class='caret'></span></button>
				<ul class='dropdown-menu scrollable-menu' uib-dropdown-menu>
				<li ng-repeat="p in y.val"><a href ng-click="selectedItem[y.a]=p;y.func()">{{ p }}</a></li>
				</ul>
				</div>
				<input type='text' ng-model='selectedItem[y.a]' class='form-control input-sm' readonly >
			</div>
			<div class="input-group" ng-if="y.t === 'picture'">
				<div class='input-group-btn' uib-dropdown >
					<button type='button' class='btn btn-default btn-sm input12' uib-dropdown-toggle >
						&nbsp;<i class="fa fa-{{y.d}}"></i>&nbsp; {{y.b}} <span class='caret'></span>
					</button>
					<ul class='dropdown-menu scrollable-menu' uib-dropdown-menu>
						<li ng-repeat="p in imgEvent"><a href ng-click="selectedItem['picture'] = p;">{{ p }}</a></li>
					</ul>
				</div>
				<input type='text' ng-model="selectedItem['picture']" class='form-control input-sm' readonly >
			</div>
			<div class="input-group" ng-if="y.t === 'pictureshow'">
				<p ng-if="selectedItem.picture != ''"><img ng-src="{{path}}{{selectedItem.picture}}" height='150'/></p>
			</div>

		   </div><br />
		</div>
		<div class="col-md-2"></div>
		<div class="col-md-7"></div>
		<div class="col-md-5">
			<a href ng-click='savenewevent();' class="btn btn-success btn-sm" style="color:white;width:150px"><i class='glyphicon glyphicon-save'></i> &nbsp;{{ buttonlabel }} </a><br />
		</div>
	</div>
</div>
</div>
</div>
</div>
<script src="inc/EventManagementSystem/EventManagementSystem.js"></script>