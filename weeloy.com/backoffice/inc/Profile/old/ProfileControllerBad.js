app.controller('ProfileControllerNew', ['$scope', '$timeout', 'bookService', 'extractService', 'profileService', function($scope, $timeout, bookService, extractService, profileService) {
	$scope.paginatorsub = new Pagination(25);
	$scope.paginator = new Pagination(25);
	$scope.restaurant = $("#restaurant").val();
	$scope.email = $("#email").val();
	$scope.editprofiles = ($("#editprofiles").val() !== "0") ? 1 : 0;
	$scope.predicate = '';
	$scope.reverse = false;
	$scope.profilFormat = null;
	$scope.names = [];
	$scope.subnames = [];
	$scope.fooditems = [];
	$scope.pfbookings = [];
	$scope.filteredProf = [];
	$scope.filteredBkg = [];
	$scope.format = [];
	$scope.formatobj = [];
	$scope.codeprofdata = [];
	$scope.codebkgdata = [];
	$scope.codeprofile = [];
	$scope.systemid = "";
	$scope.showinfotype = "profil";
	$scope.action = "";
	
	var startDate = new Date();
	var endDate = new Date();
	var forceDate = new Date(startDate.getFullYear()-2, 0, 1);
	startDate.setFullYear(startDate.getFullYear() - 1);
	$scope.cdate = endDate;
	$scope.exportFields = [];
	angular.forEach(profileService.gettopfield(), function(value, key, obj) {
		$scope.exportFields.push({  key: value, value: false  });
	});
	$scope.startProfile = -1;
	$scope.endProfile = endDate.getTime();
	$scope.mydataend = new bookService.ModalDataBooking();
	$scope.mydatastart = new bookService.ModalDataBooking();
	$scope.mydataend.setInit(forceDate, "09:00", function() { $scope.endProfile = this.getmTime(0); $scope.paginator.setPage(0); $scope.mydatastart.setMaxDate($scope.mydataend.getTheDate());}, 3, startDate, endDate);
	$scope.mydataend.setDate(endDate);
	$scope.mydatastart.setInit(forceDate, "09:00", function() { $scope.startProfile = this.getmTime(-86400000); $scope.paginator.setPage(0); }, 3, startDate, endDate);
	
	$scope.showprofiletest = (['SG_SG_R_TheFunKitchen', 'SG_SG_R_TheOneKitchen', 'FR_PR_R_LaTableDeLydia', 'KR_SE_R_TheKoreanTable'].indexOf($scope.restaurant) > -1);
	$scope.tabletitle = [ {a:'morder', b:'Order', c:'' , q:'down', cc:'fuchsia', l:'40' }, 
						{a:'salutation', b:'Title', c:'' , q:'down', cc: 'black', l:'80' }, 
						{a:'firstname', b:'Firstname', c:'' , q:'down', cc: 'black', l:'80' }, 
						{a:'lastname', b:'Lastname', c:'' , q:'down', cc: 'black', l:'100' }, 
						{a:'mobile', b:'Phone', c:'' , q:'down', cc: 'black', l:'' }, 
						{a:'email', b:'Email', c:'' , q:'down', cc: 'black', l:'' }, 
						{a:'repeatguest', b:'Repeat', c:'' , q:'down', cc: 'black', l:'50%' } ];
	$scope.tablecontent = [ {a:'salutation', b:'Title', c:'' , q:'down', cc: 'black', l:'15%' }, 
						{a:'firstname', b:'Firstname', c:'' , q:'down', cc: 'black', l:'15%' }, 
						{a:'lastname', b:'Lastname', c:'' , q:'down', cc: 'black', l:'10%' }, 
						{a:'mobile', b:'Phone', c:'' , q:'down', cc: 'black', l:'50%' }, 
						{a:'email', b:'Email', c:'' , q:'down', cc: 'black', l:'50%' }, 
						{a:'birth', b:'BirthDay', c:'date' , q:'down', cc: 'black', l:'50%' },
						{a:'repeatguest', b:'Repeat Guest', c:'' , q:'down', cc: 'black', l:'50%' } ];
	$scope.bckups = $scope.tabletitle.slice(0);
	$scope.tabletitlelength = ($scope.editprofiles === 1) ?$scope.tabletitle+1 : $scope.tabletitle;
	$scope.buttonlabel = "Update Prof";
	$scope.bookingtitle = [ {a:'booking', b:'Confirmation', c:'' , q:'down', cc:'fuchsia', l:'' }, 
						{a:'createdate', b:'CreateDate', c:'date' , q:'down', cc: 'black', l:'' }, 
						{a:'date', b:'BookingDate', c:'date' , q:'down', cc: 'black', l:'' }, 
						{a:'time', b:'time', c:'' , q:'down', cc: 'black', l:'' }, 
						{a:'pax', b:'Pax', c:'' , q:'down', cc: 'black', l:'' }, 
						{a:'tablename', b:'table', c:'' , q:'down', cc: 'black', l:'' }, 
						{a:'revenue', b:'Spending', c:'' , q:'down', cc: 'black', l:'' } ];

	$scope.masterprofileInfo = [ 
							{a: "salutation", b: "Title", c:'', t:'static'}, 
							{a: "firstname", b: "Firstname", c:'', t:'static'}, 
							{a: "lastname", b: "Lastname", c:'', t:'static'}, 
							{a: "mobile", b: "Phone", c:'', t:'input'}, 
							{a: "email", b: "Email", c:'', t:'input'}
							];

	$scope.colornames = bookService.getbkgcolorcode();		// [" ", "red", "green", "navy", "purple", "orange"], , "darkturquoise"
	$scope.colornames.splice(0,0," ");
	$scope.subprofmodel = { allergies: [], likes: [], dislikes: [] };
	$scope.getAlignment = bkgetalignment;
	$scope.food = ["Wine", "chicken wings", "French Fries", "foie gras", "confit on toast", "Sizzling prawns", "cream cheese ", "Herb-stuffed Portabello", "Escargots", "Smoked Salmon Salad", "Smoked Duck Salad", "Caesar Salad", "Portobello Rocket Salad", "Seafood Linguine", "Paccheri Pasta alla Napoletana", "Spaghetti ‘Mancini’ alla Carbonara", "Spaghetti Aglio Olio", "Fettuccine Funghi e Tartufo", "Sirloin Angus Beef Tartare", "The Steakhouse Burger", "Canard Confit", "Foie Gras Mushroom Risotto", "Southern Fried Chicken", "Seafood Bouillabaisse", "Herb-Crusted Salmon", "Joue de Bœuf", "Crêpes Suzette", "Profiteroles", "Crème Brûlée", "Tiramisu" ];
	$scope.Objprof = function() {
		return {
			index: 0, 
			morder: '', 
			systemid: '', 
			salutation: '', 
			firstname: '',
			lastname: '', 
			mobile: '', 
			email: '', 
			birth: '', 
			subprof: {}
			};
	};

	$scope.init = function() {
	};

	$scope.initorder = function() {
		$scope.tabletitle = $scope.bckups;
		$scope.predicate = "morder";
		$scope.reverse = false;
	};

	$scope.reorder = function(item, alter) {
		alter = alter || "";
		if (alter !== "")  item = alter;
		$scope.reverse = ($scope.predicate === item) ? !$scope.reverse : false;
		$scope.predicate = item;
	};
		
	$scope.getorderfood = function() {
		var i, items, k, data = $scope.pfbookings;
		
		for (i = 0; i < data.length; i++) {
			items = data[i].order.split(',');
			for(k = 0; k < items.length; k++) {
				l = items[k].trim();
				if($scope.fooditems.indexOf(l) < 0)
					$scope.fooditems.push(l);
				}
			}
		return ($scope.fooditems.length > 0);
		};
		 
	$scope.showorderfood = function() {

		if($scope.getorderfood() === false)
			return;
			
		$scope.showinfotype = "orderfood";	
		$scope.showeditproftitle = "Show Profile and Preferences";
		$timeout( function() { /* forcing apply */ }, 500);
		};
		
	$scope.showbkhistory = function(x) {
		var data, i, k, l, sep;
		
		if(typeof bookService.bookingProfile === "undefined") { 
			alert('New Function');
			return document.location.reload(true);
			}
		
		if($scope.systemid === x.systemid) {
			$scope.showinfotype = "booking";	
			$scope.showeditproftitle = "Show Profile and Preferences";
			$scope.systemid = x.systemid;
			return;
			}
			
		bookService.bookingProfile($scope.restaurant, x.systemid, $scope.email).then(function(response) { 
			if(parseInt(response.status) !== 1)
				return alert("Could not find any booking for " + x.lastname);
			
			data = response.data;	
			data.map(function(oo, i) {
				oo.createdate = oo.createdate.substring(0, 11);
				oo.pax = parseInt(oo.pax);
				oo.revenue = "";
				oo.order = "";
				if($scope.showprofiletest) {
					oo.revenue = (55 * oo.pax) + Math.floor(Math.random() * 10 * 5);
					limit = 3 * oo.pax;
					for(k = 0, sep = ''; k < limit; k++, sep = ', ') {
						l = Math.floor(Math.random() * $scope.food.length);
						oo.order += sep + $scope.food[l];
						if($scope.fooditems.indexOf($scope.food[l]) < 0)
							$scope.fooditems.push($scope.food[l]);
						}
					}
				});
				
			$scope.pfbookings = response.data.slice(0);
			$scope.paginatorsub.setItemCount($scope.pfbookings.length);
			$scope.showinfotype = "booking";	
			$scope.showeditproftitle = "Show Profile and Preferences";
			$scope.systemid = x.systemid;
			});
		};

	$scope.readSubProfile = function() {
		var i, data, limit;
		$scope.readSubProfile($scope.restaurant, $scope.email).then(function(response) {
			
			var jresponse = response;
			if(jresponse.statusCode !== 1 && jresponse.statusCode !== "1") {
				console.log("bookService.readSubProfileNew error: status: "+jresponse.statusCode);
				return $scope.init();
			}
			data = JSON.parse(jresponse.body);
			$scope.subnames = [];
			data.map(function(oo, i) {
				oo.index = oo.morder = i + 1;
				oo.birthdate = (typeof oo.birth === "string" && oo.birth.length > 10) ? oo.birth.jsdate() : null;
				try {
					//console.log("readSubProfile: oo.content type: "+typeof oo.content+" : "+oo.content);
					oo.subprof = (typeof oo.content === "string" && oo.content !== "") ? JSON.parse(oo.content.replace(/’/g, "\"")) : {};
				}  catch(e) { console.error('READPROFIL', "Exception: "+JSON.stringify(e), "oo: "+JSON.stringify(oo)); }
				$scope.subnames.push(oo);

				});
			$scope.readProfile();	
			});
		}
		
	$scope.readCodeProfile = function() {
		bookService.readCodeProfile($scope.restaurant, $scope.email).then(function(response) {
			var content = response.data;
			if(response.status === 1 && typeof content === 'string' && content !== '') { 
				try {
					var oo = JSON.parse(content.replace(/’/g, "\""));
					$scope.codeprofdata = oo.data;
				} catch(e) { console.error("JSON-PROFILE", e.message); }
				}
			});
		};
		
	$scope.readCodeBooking = function() {
		bookService.readCodeBooking($scope.restaurant, $scope.email).then(function(response) {
			var content = response.data;
			if(response.status === 1 && typeof content === 'string' && content !== '') { 
				try {
					var oo = JSON.parse(content.replace(/’/g, "\""));
					$scope.codebkgdata = oo.data;
				} catch(e) { console.error("JSON-PROFILE", e.message); }
				}	
			});
		};
		
	$scope.readProfile = function() {
		var i, k, data, subdata, v, limit, systemid, oo;

		$scope.readProfile($scope.restaurant, $scope.email).then(function(response) {
			
			if(response.statusCode !== 1 && response.statusCode !== "1")
				return;

			data = JSON.parse(response.body);
			subdata = $scope.subnames;
			$scope.names = [];
			$scope.profilFormat = null;
			data.map(function(oo, i) {
				oo.index = oo.morder = i + 1;
				if(oo.birth === "0000-00-00") oo.birth = "";
				if (oo.picture != null && oo.picture != "")
					oo.picture = "https://static.weeloy.com/images/profiles/"+oo.picture;
				else
					oo.picture = "";
				oo.subprof = {};
				if((k = subdata.inObject('systemid', oo.systemid)) >= 0) {
					oo.subprof = vv = subdata[k].subprof;
					if (!angular.isUndefined(vv) && vv !== null) {
						if(vv.Birthday instanceof Array) 
							vv.Birthday = vv.Birthday[0]; // update old method
						if(typeof vv.Birthday === "string") {
							oo.birth = vv.Birthday; 
							delete vv.Birthday;
							}
					}
					if(oo.birth.length > 0) {
						oo.birth = oo.birth.replace(/[^\d\-]/g, "");
						if(oo.birth.length < 10) oo.birth = "";
						}
					}
				$scope.names.push(oo);
			});
			if($scope.names.length > 0)
				$scope.profilFormat = ((k = subdata.inObject('systemid', 'PROFILEFORMAT0000')) >= 0) ? subdata[k].subprof : $scope.names[0].subprof;
			$scope.readCodeProfile();
			$scope.readCodeBooking();
			$scope.paginator.setItemCount($scope.names.length);
			$scope.initorder();
			});
		};

	$scope.readSubProfile();

	$scope.myFilter = function (item) { return !(item.label === 'CODE'); };
			
	$scope.cleaninput = function(oo) {
		if(typeof oo === 'string')
			return oo.replace(/\'|\"|’/g, '`');
		return oo;
		};
		
	$scope.reset = function(item) {
		$scope.listProfFlag = false;
		$scope.viewProfFlag = false;
		$scope.editProfFlag = false;
		$scope.editFormatFlag = false;
		$scope.editProfCodeFlag = false;
		$scope.editBkgCodeFlag = false;
		$scope.updateProfFlag = false;
		$scope.exportFlag = false;
		$scope[item] = true;
		console.log("reset("+item+"): "+$scope[item]);
	};

	$scope.backlisting = function() {
		$scope.reset('listProfFlag');
	};

	$scope.enabledpicker = function() { 
		if($scope.current && typeof $scope.current.birthdate === "string" && $scope.current.birthdate.length > 5)
			$('#materialdate').bootstrapMaterialDatePicker ({ time: false, clearButton: true, format: 'DD-MM-YYYY', weekStart: 0, currentDate: $scope.current.birthdate }); 
		else $('#materialdate').bootstrapMaterialDatePicker ({ time: false, clearButton: true, format: 'DD-MM-YYYY', weekStart: 0 }); 
		};
	
	$scope.editprof = function() {
		if($scope.showeditproftitle == "Edit Profile and Preferences") {
			$scope.reset('editProfFlag');
			$timeout( $scope.enabledpicker, 500);
			}
		else $scope.view($scope.current);
		};

	$scope.getAprofile = function(x) {
		var val, type, label, oo, format;
		if ($scope.profilFormat !== null) format = Object.keys($scope.profilFormat);
		var aprofileInfo = [ {label: "Picture", value: x.picture, c:'', type:'picture'},
							{label: "Title", value: x.salutation, c:'', type:'static'}, 
							{label: "Firstname", value: x.firstname, c:'', type:'static'}, 
							{label: "Lastname", value: x.lastname, c:'', type:'static'}, 
							{label: "Phone", value: x.mobile, c:'', type:'static'}, 
							{label: "Email", value: x.email, c:'', type:'static'}, 
							{label: "RepeatGuest", value: x.repeatguest, c:'', type:'static'}, 
							{label: "Birthday", value: x.birth, d: x.birthdate, c:'', type:'date'}];
		oo = x.subprof;
		if (format !== null) {
			format.sort();
			format.forEach(function(item, index) { 
				if(!oo[item]) oo[item] = "";
				if(oo[item] instanceof Array)
					oo[item] = oo[item].join(", ");	// update old method
				val = oo[item]; 
				type = 'input'; 
				label = item;
				if(item === 'codekey0000') { 
					type = 'checkbox'; label = 'CODE'; 
				}
				aprofileInfo.push({ label: label, value: val, c:"", type: type });
			} );
		}
		return aprofileInfo;
	};
		
	$scope.updateprofile = function(x) {
		var i, val;
		$scope.current = x;
		$scope.selectedItem = x;
		$scope.action = "update";
		$scope.reset('updateProfFlag');
		};
		
	$scope.view = function(x) {
		var i, val;
		$scope.current = x;
		$scope.action = "view";
		$scope.codeprofile = $scope.codeprofdata.slice(0);
		$scope.aprofileInfo = $scope.getAprofile(x);
		$scope.
		val = "";	
		if(x.subprof && x.subprof['codekey0000'])
			val = x.subprof['codekey0000'];
		for(i = 0; i < $scope.codeprofile.length; i++) {
			if (val != null && val != "")
				$scope.codeprofile[i].value = (val.indexOf($scope.codeprofile[i].label) > -1);
		}
		$scope.showeditproftitle = "Edit Profile and Preferences";
		$scope.showinfotype = "profil";	
		$scope.reset('viewProfFlag');
	};


	$scope.Edit = function() {
		var i;

		if($scope.names.length < 0)
			return;
		
		$scope.prevformat = Object.keys($scope.profilFormat);
		$scope.formatobj = []; // codekey0000
		// don't change keep, same order old <-> new
		for(i = 0; i < $scope.prevformat.length; i++)
			$scope.formatobj.push({value: $scope.prevformat[i]});
		$scope.reset('editFormatFlag');
		};
		
	$scope.addfield = function() {
		$scope.formatobj.push({value: 'newfield' + ($scope.format.length+1) });
		};

	$scope.saveformat = function() {
		var i, k, oo, value;
		
		$scope.format = [];
		for(i = 0; i < $scope.formatobj.length; i++) {
			value = $scope.formatobj[i].value.replace(/\'|\"|’/g, "`");
			$scope.formatobj[i].value = value;
			$scope.format.push(value);
			}
			
		if($scope.format.length === $scope.prevformat.length) {
			for(i = 0; i < $scope.format.length && $scope.format[i] === $scope.prevformat[i]; i++);
			if(i >= $scope.format.length) {
				alert("Nothing to save");
				$scope.backlisting();
				return; // no modification
				}
			}

		for(i = 0; i < $scope.names.length; i++) {
			oo = $scope.names[i].subprof;
			for(k =  $scope.prevformat.length; k <  $scope.format.length; k++) {
				oo[$scope.format[k]] = "";
				}
			for(k =  0; k <  $scope.prevformat.length; k++) 
				if($scope.format[k] !== $scope.prevformat[k]) {
					oo[$scope.format[k]] = oo[$scope.prevformat[k]];
					delete oo[$scope.prevformat[k]];
					}
			}

		$scope.saveformatsubProfile($scope.prevformat, $scope.format);
 		$scope.backlisting();		
	};
		
	$scope.saveformatsubProfile = function(oo, nn) {
		bookService.updateformatsubProfile($scope.restaurant, $scope.email, oo, nn).then(function(response) { alert("Profile Format for restaurant " + $scope.restaurant + " has been updated"); });		
 		$scope.backlisting();		
	};

	$scope.savesubProfile = function(x) {
		var subprof, i, ttAr, val, subprofstr;
		
		x = $scope.current;
		subprof	= x.subprof;

		if((i = $scope.aprofileInfo.inObject('type', 'date')) >= 0)
			$scope.aprofileInfo[i].value = $('#materialdate').val();
			
		$scope.aprofileInfo.map(function(vv) {
			if(['input', 'date'].indexOf(vv.type) >= 0) {
				if(vv.value !== "")
					vv.value = vv.value.replace(/\'|\"|’/g, "`");
				subprof[vv.label] = vv.value;
				}
			});

		profileService.updatesubProfile($scope.restaurant, $scope.email, x.systemid, $scope.codeprofile, subprof, $('#token').val(), bookService).then(function(response) {
			if(response.status > 0) {
				alert("profile " + x.firstname + " has been updated"); 
				if(subprof.Birthday instanceof Array) 
					subprof.Birthday = subprof.Birthday[0]; // update old method
				if(typeof subprof.Birthday === "string") {
					x.birth = subprof.Birthday; 
					delete subprof.Birthday;
					}
			}
			$scope.view(x);	
			});
		};

	$scope.editprofcode = function() {
		$scope.reset('editProfCodeFlag');
		};

	$scope.editbkgcode = function() {
		$scope.codebkgdata.map(function(oo) { oo.value = (typeof oo.color === "string" && oo.color.length > 2) ? oo.color : " "; oo.color = oo.value; }); 
		$scope.reset('editBkgCodeFlag');
		};

	$scope.addcode = function(oo, type) {
		if(type === 'booking')
			oo.push({label: 'newcode' + ($scope.format.length+1), description: '', color: '' });
		else oo.push({label: 'newcode' + ($scope.format.length+1), description: '' });
		};

	$scope.savecodeprof = function() {
		var oo = JSON.stringify({ data: $scope.codeprofdata }).replace(/\'|\"/g, "’");
		bookService.updateCodeProfile($scope.restaurant, $scope.email, oo);
 		$scope.backlisting();		
		};
		
	$scope.savecodebkg = function() {
		var oo = JSON.stringify({ data: $scope.codebkgdata }).replace(/\'|\"/g, "’");
		bookService.updateCodeBooking($scope.restaurant, $scope.email, oo);
 		$scope.backlisting();		
		};
		
	$scope.emptyOrNull = function(item) { 
			var tmp = item.value;
			//return !(tmp === null || typeof tmp === 'undefined' || (typeof tmp === 'string' && tmp.trim().length === 0));
			return !(tmp === null || typeof tmp === 'undefined');
		};
		
	$scope.setallfield = function() {
		var i, data = $scope.exportFields;
		for (i = 0; i < data.length; i++)
			data[i].value = true;		
		};
		
	$scope.clearallfield = function() {
		var i, data = $scope.exportFields;
		for (i = 0; i < data.length; i++)
			data[i].value = false;
	};
	
	$scope.export1 = function(profiles, flg) {
		var maxlimit = 10000; // ajax call might not support more data
		var data, filename;
		var exportselect = $scope.filteredProf;
		var limit = exportselect.length, cdate = new Date();
		var field = profileService.gettopfield();
		var subfield = [];
		if(limit < 1)
			return alert("no data");
		if (limit > maxlimit) limit = maxlimit;
			exportselect = $scope.filteredProf.slice(0, limit);
		data = field.join(",") + ",";
		subfield = profileService.getsubfield(exportselect[0].subprof);
		data += subfield.join(",").replace(/codekey0000/, "CODE") + "\n";
		if(profiles.length === 0 && flg === 0)
			return;
		//console.log("field: "+JSON>stringiy(field));
		//console.log("subfield: "+JSON>stringiy(subfield));
		//console.log("exportselect: "+JSON.stringify(exportselect));
		exportselect.map(function(oo, index) {
			if (flg === 1 || profiles.indexOf(oo['systemid']) !== -1) {
				field.map(function(ll) {
					if (ll === 'repeatguest'){
                                            data += (oo[ll] > 1 ? oo[ll] : 0 )+ ",";
                                            if($scope.restaurant === 'SG_SG_R_Bacchanalia'){
                                                 data += (oo[ll] > 1 ? 'Yes' : 'No' )+ ",";
                                             }
                                         }
					else {data += profileService.filter(oo[ll]) +  ","; }
				});
				subfield.map(function(ll) { data += profileService.filter(oo.subprof[ll]) +  ","; });
				data += "\n";
			} 
		});
		filename = "profile" + cdate.getDate() + cdate.getMonth() + cdate.getFullYear() + ".csv";
		extractService.save(filename, data);
	}
	
	$scope.export = function(dstart,dend) {
		var start, end, ndays, data = [];
		if (dstart instanceof Date === false || dend  instanceof Date === false) {
			alert("Please select starting and ending date (no more than 3 months");
			return;
			}
		start = moment(dstart.getDateFormatReverse("-"));
		end = moment(dend.getDateFormatReverse("-"));
		ndays = end.diff(start, 'days');
		console.log('EXPORT', start, end, start.format('YYYY-MM-DD'), end.format('YYYY-MM-DD'), ndays);
		if(ndays < -1) {
			alert("invalid dates, end is before start");
			return;
			}
		if(ndays > 93) {
			alert("Maximum day range is 3 full months (here is"+ndays+" days). Please select dates less than 3 months apart");
			return;
			}
		console.log("export(): start: "+start.format("YYYY-MM-DD")+" end: "+end.format("YYYY-MM-DD"));
		profileService.getProfiles($scope.restaurant, start.format('YYYY-MM-DD'), end.format('YYYY-MM-DD')).then(function (response) {
			if (response.status == 1) {
				response.data.map(function(oo) { data.push(oo.systemid); });
				console.log('DATA', data);
				$scope.export1(data, 0);
				}
			else console.log("Failed to retrieve calls: " + JSON.stringify(response));
		});
	};
	$scope.printing = function() {
		//var data = $("#print_content").html(); 
		var content, i, data = $scope.filteredProf.slice(0);
		var docprint = window.open("", "Printing Guest Profile", "toolbar=yes,location=no,directories=yes,menubar=yes,scrollbars=yes,width=800, height=600, left=100, top=25");
		docprint.document.open();
		
		//data.sort(function(a, b) { var c = a.vdate - b.vdate; return (c != 0) ? c : (a.vtime - b.vtime); });
		content = "";
		content += "<html><head><title>Weeloy System</title>";
		content += "<style>body { margin: 20 20 20 20 } table { font-family:helvetica;font-size:10px; } td { align:center; } .truncate { white-space: nowrap; overflow: hidden; text-overflow: ellipsis; } </style></head><body onLoad='self.print()'><center>";
		content += "<p><img src='" + $('#imglogo').val() + "' max-width='80' max-height='80' id='theLogo' name='theLogo'></p><br/>";
		content += "<span style='font-size:12px;padding-right:40px;'>selected guest profile: <strong> " + $scope.filteredProf.length + "</strong></span>";
		content += "<br/><br />";
		content += "<table width='100%'><head><tr style='font-weight:bold;font-size:10px'><td>Title</td><td>Firstname</td><td>Lastname</td><td>Phone</td><td>Email</td><td>Birthday</td><td>Repeat Guest</td></tr><tr><td colspan='11'><hr/></td></tr>";
		for (i = 0; i < data.length; i++) {
			content += "<tr><td>" + data[i].salutation + "</td><td>" + data[i].firstname.substr(0,10) + "</td><td>" + data[i].lastname.substr(0,10) + "</td><td><div class='truncate' style='width:80px'>" + data[i].mobile.substr(0,17) + "</div></td><td>" + 
			data[i].email + "</td><td>" + data[i].birth + "</td><td>" + data[i].repeatguest + "</td></tr>";
			}
		content += "</table></center></body></html>";
		docprint.document.write(content);
		docprint.document.close();
		docprint.focus();
		};

}]);