<?php $isTheFunKitchen = (preg_match("/TheFunKitchen/", $theRestaurant)); ?>

<div class="container">
    <div class="row">
        <div class="col-md-12 left-sec">

			<div id="reporting" ng-controller="ControllerService" ng-init="listingFlagPromo = true" >

			<div id='listing'>
			
				<table ng-repeat="x in categories" width='100%' class="table table-condensed table-striped" style='font-size:12px;'>
					<thead>
						<tr>
			   

					<th width = '20%'>{{ x.categorie}}</th>
					<th></th>
					<!--<th>Update</th>-->
					</th>
					</tr>
					</thead>
					<tbody style='font-family:helvetica;font-size:12px;'>
						<tr  ng-repeat="z in restaurantservices" ng-if="z.categorie_id == x.categorie_id" width='100%' class="table table-condensed table-striped" style='font-size:12px;'>
							<td><input type="checkbox" ng-model="z.active" ng-true-value="'1'" ng-false-value="'0'"></td>
					   
						   <td>{{ z.service}}</td>
						</tr>
					</tbody>
				</table>
				<a href class='btn btn-info btn-sm customColor' style='width: 20%;' ng-click="saveservices()"> &nbsp;Save</a>
				<p  style='width: 20%;text-align: center;'>{{saving_msg}}</p>
			</div>
		</div>

<script>


var token = <?php echo "'".$_SESSION['user_backoffice']['token']."';"; ?>


app.controller('ControllerService', function ($scope, $http, $timeout) {
	$scope.restaurant = <?php echo "'".$theRestaurant."';"; ?>
	$scope.saving_msg = '';
	$scope.itemsPerPage = 100;
	$scope.currentPage = 0;
	$scope.startIndex = 0;
	$scope.currentPage = 0;
	$scope.selectionSize = 0;
	$scope.categories =''; 


	//$scope.tabletitle = [{'a': 'index', 'b': 'index'}, {'a': 'name', 'b': 'Promo Name'}, {'a': 'title', 'b': 'Promotion'}, {'a': 'start', 'b': 'Start date'}, {'a': 'end', 'b': 'End date'}];

	$scope.dummy = { categorie: $scope.categories  };


	var url = '../api/restaurant/service/categories';
	$http.get(url).success(function (response) {
		$scope.categories = response.data.categories;
	});

	var url = '../api/restaurant/service/restaurantservices/'+$scope.restaurant;
	$http.get(url).success(function (response) {
		$scope.restaurantservices = response.data.restaurantservices; 
	});

	$scope.saveservices = function () {
		$scope.saving_msg = 'Saving...';

	var active_services = new Array();

		for (i = 0; i < $scope.restaurantservices.length; i++) {
			if($scope.restaurantservices[i].active == '1'){
				active_services.push($scope.restaurantservices[i].ID);
			}
		}  


		$http({
			url: '../api/restaurant/service/save',
			method: "POST",
			data: $.param({
				restaurant: $scope.restaurant,
				active_services: active_services
			}),
			headers:  {'Content-Type': 'application/x-www-form-urlencoded'}
		}).success(function(data, status, headers, config) {
			$scope.saving_msg = 'Saved';
			$timeout(function () { $scope.saving_msg = ''; }, 3000);   
		  }).
		  error(function(data, status, headers, config) {
			// called asynchronously if an error occurs
			// or server returns response with an error status.
		  });
	};

	$scope.view = function (id) {

		$scope.SelectedItem = $scope.names.slice(id - 1, id);
		$scope.listingFlagPromo = false;

	};

	$scope.backlisting = function () {

		$scope.SelectedItem = -1;
		$scope.listingFlagPromo = true;
		$scope.createFlagPromo = false;
	};

	$scope.create = function () {
		$scope.listingFlagPromo = false;
		//$scope.listing1EventFlag = false
		$scope.createFlagPromo = true;
		$scope.buttonlabel = "Save promotion";
		$scope.action = "create";
		//$scope.nevent = $scope.nname = $scope.ncity = $scope.ncountry = $scope.nmorder = $scope.npicture = $scope.nstart = $scope.nend = $scope.ndescription = $scope.readonly = "";
		//$('.showpict').html("");

		return false;
	}

	$scope.open = function ($promo, $num) {
		$promo.preventDefault();
		$promo.stopPropagation();

		$scope.dateindex = $num;
		if ($scope.dateindex == 1)
			$scope.start_opened = true;
		if ($scope.dateindex == 2)
			$scope.end_opened = true;
	};


	$scope.clear = function () {
		if ($scope.dateindex == 1)
			$scope.nstart = null;
		if ($scope.dateindex == 2)
			$scope.nend = null;
	};



	$scope.$watch("filteredPeople.length", function (newvalue, oldvalue) {
		if ($scope.filteredPeople == undefined)
			return;
		if ($scope.selectionSize != $scope.filteredPeople.length) {
			$scope.selectionSize = $scope.filteredPeople.length;
			$scope.currentPage = 0;
			$scope.pageCount = Math.ceil($scope.filteredPeople.length / 100) - 1;
		}
	});

});


</script>
</div>
</div>
</div>
