<div id='booking' ng-controller='InvoicingController' ng-init="moduleName='invoicing'; typeBooking=true;bookingTitle='BOOK OF THE DAY'" >
 <div class="container mainbox">    
	<div class="panel panel-info">

<div class="row">
	<div class="col-sm-3">
	<div class="input-group">
		<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
		<input type="text" class="form-control input-sm" ng-model="searchText" ng-change="paginator.setPage(0);" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" > 
	</div>
	<div style='font-size:11px;'>selected bookings: <strong> {{filteredBooking.length}} </strong></div>
	</div>
	
	<div class="col-sm-1" style='font-size:12px;'>
	<div class="input-group">
		<label class="checkbox-inline"><input type="checkbox" value="17" ng-model="allinvoice">all</label>
	</div>
	</div>

	<div class="col-sm-1">
	<div class="input-group">
		<span><button  type="button" class="btn btn-default" ng-click="mydatastart.dateopen($event)"><i class="glyphicon glyphicon-calendar"></i></button></span><span style='font-size:10px;'>  month</span>
		<input type="text" class="form-control" uib-datepicker-popup="{{mydatastart.formats[0]}}" ng-change='mydatastart.onchange();' ng-model="mydatastart.originaldate" is-open="mydatastart.opened" min-date="mydatastart.minDate" max-date="mydatastart.maxDate" datepicker-options="mydatastart.dateOptions" ng-required="true" close-text="Close" style="width:0;opacity:0" />
	</div>
	</div>	

	<div class="col-sm-2">
		<a href ng-click='regenerateService();' class="btn btn-primary btn-sm" style='color:white;'><span class='glyphicon glyphicon-save'></span> &nbsp; Regenerate</a>
	</div>
	<div class="col-sm-2">
			<a href ng-click='extractSelection();' class="btn btn-success btn-sm" style='color:white;'><span class='glyphicon glyphicon-save'></span> &nbsp; Extract</a>
	</div>
	<div class="col-sm-1">
		<div class="btn-group">
			<button type="button" class="btn btn-default btn-xs" uib-dropdown-toggle aria-expanded="false" style='font-size:11px;'>Page Size <span class="caret"></span></button>
			<ul class="dropdown-menu" uib-dropdown-menu role="menu">
				<li ng-repeat="x in paginator.pagerange()" class='glyphiconsize'><a href ng-class="{ 'glyphicon glyphicon-ok glyphiconsize': x === paginator.getRowperPage() }" href ng-click="paginator.setRowperPage(x)"> {{x}}</a></li>
			</ul>
		</div> 		
	</div>
	<div class="col-sm-1">&nbsp;</div>
	<div class="col-sm-1">
	<div class="input-group">
		<button  type="button" class="btn btn-default" ng-click="help()" style="font-size:11px;font-family: Roboto;font-weight:bold;">HELP</button>
	</div>
	</div>	
</div>

<div class="row" ng-if="startBkg !== -1" style="font-size:11px;">
start: {{ startdate }} &nbsp;&nbsp;&nbsp; end: {{enddate }} 
</div>
<br />
<div class="col-md-12" ng-hide='listingVisitFlag'>
	<br />
	<a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a>
	<br />
	<table class='table-striped' style="margin: 0 0 150px 30px;font-size:13px;font-family: Roboto">		
		<tr ng-repeat="y in tabletitleContent | filter:showtest"><td nowrap><strong>{{ y.a }} : </strong></td><td> &nbsp; </td><td>{{ selectedItem[y.b] | adatereverse:y.c }}</td></tr>
		<tr><td>&nbsp;</td><td> &nbsp; </td><td></td></tr>
	</table>
</div>

<div class="col-md-12" ng-show='listingVisitFlag'>
	
<table width='100%' class="table table-condensed table-striped" style='font-size:11px;'>
	<thead><tr><th ng-repeat="y in tabletitle"><tbtitle dum="{{y.b}}" name='tabletitle' /></th></tr></thead>
	<tr ng-repeat="x in filteredBooking = (booking | filter:searchText | filterBymyDate:startBkg:endBkg:'vdate') | sizefilter:this | orderBy:predicate:reverse | slicefilter:paginator.getPage():paginator.getRowperPage():paginator.getItemCount() | limitTo:paginator.getRowperPage()">
		<td ng-repeat="z in tabletitle"><tb-listingbkg><a href ng-click="view(x)">{{ x[z.a] | adatereverse:z.c }}</a></tb-listingbkg></td>
	</tr>
	<tr><td colspan='{{tabletitle.length}}'></td></tr>
</table>

<div ng-if="filteredBooking.length >= paginator.getRowperPage()" class="col-md-12" align="center" style='font-size:11px' ng-include="'inc/paginator.html'"></div>

</div>
</div>
</div>
</div>

<script type="text/javascript" src="../js/pako.js"></script>

<script>


<?php
echo "var token = '" . $_SESSION['user_backoffice']['token'] . "';";
?>

app.controller('InvoicingController', ['$scope', '$http', 'bookService', 'extractService', function($scope, $http, bookService, extractService) {

	var newdate = new Date();
	var mxstartdate, mxendate;

	if(newdate.getDate() > 15)
		newdate = newdate.nextmonth();
		
	$scope.paginator = new Pagination(50);
	$scope.getAlignment = bkgetalignment;

	$scope.mode = "r";
	$scope.cmonth = newdate.getMonth();
	$scope.pinvdate = newdate.beginmonth().getDateFormatReverse('-');
	$scope.startBkg = -1;
	$scope.endBkg = -1;
	$scope.invoicedate = null;
	$scope.startdate = $scope.enddate = "";
		
	if($scope.cmonth > 6) {
		mxstartdate = new Date(newdate.getFullYear(), 0, 1);
		mxendate = new Date(newdate.getFullYear()+1, 11, 31);
	} else {
		mxstartdate = new Date(newdate.getFullYear()-1, 0, 1);
		mxendate = new Date(newdate.getFullYear(), 11, 31);
		}

	$scope.adjustdate = function() {
		var tdate = $scope.mydatastart.getTheDate().beginmonth();
		
		$scope.mydatastart.setInit(tdate, "09:00", $scope.adjustdate, 3, mxstartdate, mxendate);
		$scope.startBkg = tdate.getTime();
		$scope.endBkg = tdate.endmonth().getTime();	// until the end of that day nb of milliseconds
		
		$scope.startdate = tdate.getDateFormat('-');
		$scope.enddate = tdate.endmonth().getDateFormat('-');
		
		$scope.invoicedate = tdate.nextmonth().beginmonth();
		$scope.readdata($scope.invoicedate.getDateFormatReverse('-'), $scope.mode, null);
		};
				
	$scope.mydatastart = new bookService.ModalDataBooking();
	$scope.mydatastart.setInit(newdate.prevmonth(), "09:00", $scope.adjustdate, 3, mxstartdate, mxendate);
	
	$scope.invoicefield = ['websitebkg', 'websitepax', 'lunch', 'dinner', 'remote', 'event']; // if one of those field price not set to 0, show details
	$scope.fieldData = ['confirmation','type','restaurant','email','mobile','salutation','lastname','firstname','cdate','rdate','rtime','cover','tablename','tracking','invoice'];
	$scope.segmentlabel = ['lunch', 'dinner', 'website', 'facebook', 'walkin', 'payment', 'event', 'waiting', 'remote', 'import', 'callcenter', 'ttccenter', 'subpax', 'notinvoiced'];
	$scope.glbconfig = { lunch:0, dinner:0, website:0, facebook:0, walkin:0, payment:0, event: 0, waiting:0, remote:0, import:0, callcenter:0, ttccenter:0, subpax:0, count:0, license:0 };	
	$scope.mservice = [{ service:'mrs', title:'Reservation System'}, { service:'mcc', title:'CallCenter'}, { service:'mtms', title:'TMS'}, { service:'mmkg', title:'Marketing Pack'} ];
	
	$scope.email = <?php echo "'" . $email . "';"; ?>
	$scope.listingVisitFlag = true;
	$scope.invoice = [];
	$scope.filteredBooking = [];
	$scope.invoiceparam = [];
	$scope.tabletitle = [ {a:'index', b:'index', c:'', l:'', q: 'down', cc: 'black' }, {a:'confirmation', b:'Confirmation', c:'', l:'', q: 'down', cc: 'black' }, {a:'restaurant', b:'Restaurant', c:'', l:'120', q: 'down', cc: 'black' },  {a:'type', b:'Type', c:'', l:'', q: 'down', cc: 'black' },  {a:'cover', b:'Pax', c:'', l:'', q: 'down', cc: 'black' },  {a:'rtime', b:'Time', c:'', l:'', q: 'down', cc: 'black' }, {a:'fullname', b:'Name', c:'', l:'90', q: 'down', cc: 'black' },  {a:'rdate', b:'BookDate', c:'date', l:'', q: 'down', cc: 'black' },  {alter:'vcdate', a:'cdate', b:'CreateDate', c:'date', l:'', q: 'up', cc: 'orange' },  {a:'status', b:'Status', c:'', l:'', q: 'down', cc: 'black' },  {a:'win', b:'Wins', c:'', l:'40', q: 'down', cc: 'black' } ];
	$scope.bckups = $scope.tabletitle.slice(0);
	$scope.tabletitleContent = [ { a:'Restaurant', b:'restaurant', c:'' }, { a:'Booking Confirmation', b:'confirmation', c:'' }, { a:'Booking Type', b:'type', c:'' }, { a:'Status', b:'status', c:'' }, { a:'Reservation Date', b:'rdate', c:'date' }, { a:'Reservation Time', b:'rtime', c:'' }, { a:'Number of Persons', b:'cover', c:'' }, { a:'Name', b:'fullname', c:'' }, { a:'Email', b:'email', c:'' }, { a:'Mobile', b:'mobile', c:'' }, { a:'Create Date', b:'cdate', c:'date' }, { a:'Request', b:'comment', c:'' }, { a:'Win', b:'wheelwin', c:'' }, { a:'Tracking', b:'tracking', c:'' }, { a:'Staff', b:'booker', c:'' }, { a:'Company', b:'company', c:'' }, { a:'Hotel Guest', b:'yes', c:'' }, { a:'State', b:'state', c:'' }, { a:'restCode', b:'restCode', c:'' }, { a:'membCode', b:'membCode', c:'' }, { a:'Table', b:'tablename', c:'' }, { a:'Browser', b:'browser', c:'' }, { a:'Language', b:'language', c:'' }];
	$scope.selectedItem = null;	
	$scope.allinvoice = false;
		
	again = false;
	
	$scope.readConfiguration = function() {
		bookService.readallinvConf($scope.email).then(function(response) { 
			var i, j, dd, label = $scope.segmentlabel;
			$scope.invoiceconf = response.config;  
			if($scope.invoiceconf.length < 1) {
				alert('INITIALISATION, RELOAD');
				return; 
				}
			$scope.invoiceconf.forEach(function(oo) {
				obj = JSON.parse(oo.config.replace(/’/g, "\""));
				label.forEach(function(ll) {
					var a = obj[ll] || 0;
					obj[ll] = (a === '' || isNaN(parseFloat(a)) || parseFloat(a) < 0) ? 0 : parseFloat(a); 
					});
				//obj.payment = (obj.restaurant !== "SG_SG_R_BurntEnds") ? 1 : 0;
				//obj.event = 1;
				$scope.invoiceparam.push(obj);
				});
			});
			
			$scope.invoiceparam.map(function(config) {
				config.licence = 0;
				$scope.mservice.some(function(oo) {
					config[oo.service] = config[oo.service] || 0; 
					config[oo.service] = parseFloat(config[oo.service]);
					if(config[oo.service] > 0) {
						config.licence = 1;
						return true;
						}
					return false;
					});
				config['dontinvoice'] = 1;
				$scope.invoicefield.some(function(ll) { 
					if(config[ll] > 0) { 
						config.dontinvoice = 0;
						return true; 
						} 
					return false;
					}); 
				});
			};
	
	$scope.readConfiguration();
	 
	$scope.initorder = function() {
		$scope.tabletitle = $scope.bckups;
		$scope.predicate = "vcdate";
		$scope.reverse = true;
	};

	$scope.reorder = function(item, alter) {
		alter = alter || "";
		if (alter !== "")  item = alter;
		$scope.reverse = ($scope.predicate === item) ? !$scope.reverse : false;
		$scope.predicate = item;
	};

	$scope.readdata = function(invdate, type, func, field, segmentlabel, nreadmonth) {
					
		bookService.invoiceBooking(invdate, type, $scope.email, '').success(function(response) { 	 
			 var i, obj, first, last;
			 $scope.booking = [];

			if(response.status < 0)
				return response;

			 $scope.rdata = response.data.bookings; 
			 for(i = 0; i < $scope.rdata.length; i++) {
				obj = $scope.rdata[i];
												
				obj.index = i+1; 
				obj.ddate = obj.rdate.jsdate().getDateFormat('-');
				obj.cddate = obj.cdate.jsdate().getDateFormat('-');
				$scope.booking.push(obj);
				}
			$scope.paginator.setItemCount($scope.booking.length);
			$scope.initorder();
			
			if(func !== null) {
				func(invdate, field, segmentlabel);
				alert("Done generating invoicing for month " + invdate + ' reading');
				$scope.pinvdate = invdate;
				}
			});
		};

	$scope.adjustdate();
	
	$scope.showtest = function(y) {
		if(y.c === true || $scope.selectedItem === null) 
			return true;
		return ($scope.selectedItem[y.b] !== '');
	};

	$scope.view = function(oo) {
		$scope.selectedItem = oo;
		$scope.listingVisitFlag = false;
		};
		
	$scope.backlisting = function() {
		$scope.selectedItem = null;
		$scope.listingVisitFlag = true;
		};

	$scope.compareFunction = function(a, b) {
		var log = a.restaurant.localeCompare(b.restaurant);
		if(log !== 0) return log;
		
		return a.vcdate - b.vcdate;
		};
	
	$scope.findrestaurant = function(data, restaurant) {
		for(var i = 0; i < data.length; i++)
			if(data[i].restaurant === restaurant)
				return data[i];
		return null;
		};

	$scope.getlicenserestaurant = function(invoiceparam) {
		var restAr = [];
		invoiceparam.forEach(function(config) {
			if(config.licence === 1)
				restAr.push(config.restaurant);
			});
		return restAr;
		};

	$scope.getnotinvoice = function(invoiceparam) {
		var restAr = [];
		invoiceparam.forEach(function(config) {
			if(config.dontinvoice === 1)
				restAr.push(config.restaurant);
			});
		return restAr;
		};

	$scope.compareFunc = function(a, b) {
		var log = a.restaurant.localeCompare(b.restaurant);
		if(log !== 0) return log;
		
		return b.vdate - a.vdate;
		};

	// take all the stored invoiced, and concatenate then, and include final data (bottom line)
	$scope.cumulate = function(config, start, end, field, segmentlabel, restaurant) {
		var tt, u, vv, tmp, i, k, oo;
		var obj =Object.assign({}, $scope.glbconfig);
		
		tt = '';
		tt += restaurant + '\n';
		tt += field.join(',') + '\n';

		// get the licenses but donot store details
		if(config.dontinvoice === 1) {
			return tt + $scope.finaliseInvoice(obj, config, segmentlabel);
			}
			
		for(i = start; i <= end; i++) {
			oo = $scope.invoice[i];
			u = oo.details.replace(/Summary.*/g, '').replace(/,/g, '.');
			tmp = u.split("|||");
			// start one to get rid of titles
			for(k = 1; k < tmp.length; k++) {
				if(tmp[k].trim() === "")
					continue;
				tt += tmp[k].replace(/\t/g, ',') + '\n';
				}
			u = oo.segments.split(";");
			for(k = 0; k < u.length; k++) {
				tmp = u[k].split(",");
				label = tmp[0];
				cn = parseInt(tmp[1]);
				price = parseFloat(tmp[2]);
				if(label in obj) {
					obj[label] += cn;
					config[label] = price;
					}
				}
			}
		tt += $scope.finaliseInvoice(obj, config, segmentlabel);
		return tt;
		};


	$scope.regenerateService = function() {
		var invdate, all = ($scope.allinvoice) ? " all " : "";
		//var field = ['confirmation','type','restaurant','email','mobile','salutation','lastname','firstname','cdate','rdate','rtime','cover','company','hotelguest','state','tablename','wheelwin','tracking','booker','invoice'];
		var field = $scope.fieldData;
		var config, imonth, iyear, nreadmonth;

		$scope.readConfiguration();
		
		if($scope.invoicedate instanceof Date === false) {
			alert("Please select Dates");
			return;			
			}
			
		invdate = $scope.invoicedate.getDateFormatReverse('-');
		imonth = parseInt(invdate.substr(5, 2)) - 1;
		iyear = parseInt(invdate.substr(0, 4));
		
		if(confirm('Do you want to regenerate/save invoicings, for ' + invdate) === false)
			return;
		if($scope.pinvdate == invdate) {
			$scope.generateInvoice(invdate, field, $scope.segmentlabel); // this will create more invoices
			alert("Done generating invoicing for month " + invdate);
			}
		else $scope.readdata(invdate, $scope.mode, $scope.generateInvoice, field, $scope.segmentlabel, nreadmonth);
		};
		
	$scope.extractSelection = function() {
		var invdate, all = ($scope.allinvoice) ? " all " : "";
		//var field = ['confirmation','type','restaurant','email','mobile','salutation','lastname','firstname','cdate','rdate','rtime','cover','company','hotelguest','state','tablename','wheelwin','tracking','booker','invoice'];
		var field = $scope.fieldData;
		var config, imonth, iyear, nreadmonth;

		$scope.readConfiguration();
		
		if($scope.invoicedate instanceof Date === false) {
			alert("Please select Dates");
			return;			
			}
			
		invdate = $scope.invoicedate.getDateFormatReverse('-');
		imonth = parseInt(invdate.substr(5, 2)) - 1;
		iyear = parseInt(invdate.substr(0, 4));
		
		if(confirm('Do you want to extract ' + all + 'invoicings, for ' + invdate) === false)
			return;
		extractfinalInvoice(invdate);
		
		function extractfinalInvoice(invdate) {
			$scope.invoice = [];
			bookService.readallinvoice(invdate, $scope.email).then(function(response) { 
				var i, oo, start, end, total, currency, exporttext, sep, consoflg, pp, json;
				var filename, cdate = new Date();
				
				$scope.data = response.invoice;  
				for(var i = 0; i < $scope.data.length; i++) {
					oo = $scope.data[i];
					oo.vdate = oo.invoicedate.jsdate().getTime();
					oo.ddate = oo.invoicedate.jsdate().getDateFormat('-');
					oo.total = parseFloat(oo.total);
					oo.subtotal = parseFloat(oo.subtotal);
					oo.license = parseFloat(oo.license);
					$scope.invoice.push(oo);
					}

				$scope.invoice.sort($scope.compareFunc);
			 
				exporttext = sep = "";
				for(i = 0; i < $scope.invoice.length; i++) {
					oo = $scope.invoice[i];
					total = oo.subtotal;
					currency = oo.currency;
					if(oo.invoicedate !== invdate) 
						continue;
						
					consoflg = (oo.status === "conso");
					// consolidate previous invoices that the total/license were still at 0 (not invoiced)
					for(start = end = i; end+1 < $scope.invoice.length; end++) {
						if($scope.invoice[end+1].restaurant !== oo.restaurant || $scope.invoice[end+1].total > 0 || $scope.invoice[end+1].license > 0)
							break;
						if($scope.invoice[end+1].restaurant === 'SG_SG_R_MitsubaJapaneseRestaurant')
							console.log('MIT ', $scope.invoice[end+1]);
						total += $scope.invoice[end+1].subtotal;
						}
					if(oo.license > 0 || consoflg || (total >= 20 && currency === 'SGD') || (total >= 100 && currency === 'HKD') || (total >= 500 && currency === 'BAHT')) {
						if(oo.total !== total) {
							oo.total = total;
							bookService.totalInvoice(oo.restaurant, oo.invoicedate, total, $scope.email).then(function(response) {  });
							}
						}
						
					if($scope.allinvoice || oo.total > 0 || oo.license > 0) {
						config = $scope.findrestaurant($scope.invoiceparam, oo.restaurant);
						exporttext += $scope.cumulate(config, start, end, field, $scope.segmentlabel, oo.restaurant);
						}
					i = end;
					}
			
				filename = "invoicings" + cdate.getDate() + cdate.getMonth() + cdate.getFullYear() + ".csv";
				//extractService.save(filename, exporttext);
				var file = new File([exporttext], filename, {type: "text/csv;charset=utf-8"});
				saveAs(file);
				});
			}		
		};
									
	$scope.finaliseInvoice = function(obj, config, segmentlabel) {
		var tt, ll, tp, ident, totalpax, license;
		
		ident = ",,,,,,,,,,";
				
		tt = '\n\n';
		if(typeof obj.subpax === 'number' && obj.subpax > 0)
			tt += ident + 'total  PAX,,' + obj.subpax + '\n';

		obj.callcenter = obj.ttccenter = obj.walkin = obj.waiting =  obj.import = 0; // dont compute		
		if(obj.callcenter > 0 || obj.ttccenter > 0) tt += ident + 'total booking callcenter,,' + obj.callcenter + ',' + 'total transactions ' + obj.ttccenter + '\n';
		if(obj.walkin > 0) { tp = "booking";   tt += ident + 'total ' + tp + ' walkin,,' + obj.walkin + '\n'; }
		if(obj.waiting > 0) { tp = "booking";   tt += ident + 'total ' + tp + ' waiting,,' + obj.waiting + '\n'; }
		if(obj.import > 0) { tp = "booking";   tt += ident + 'total ' + tp + ' import,,' + obj.import + '\n'; }

		if(obj.remote > 0) { tp = "booking";   tt += ident + 'total ' + tp + ' thirdparty,,' + obj.remote + '\n'; }
		if(obj.event > 0) { tp = "booking";   tt += ident + 'total ' + tp + ' event,,' + obj.event + '\n'; }
		if(obj.payment > 0) { tp = "booking";   tt += ident + 'total ' + tp + ' payment,,' + obj.payment + '\n'; }
		if(obj.facebook > 0) { tp = (config.websitepax > 0) ? "pax" : "booking";  tt += ident + 'total ' + tp + ' facebook,,' + obj.facebook + '\n'; }
		if(obj.website > 0) { tp = (config.websitepax > 0) ? "pax" : "booking";   tt += ident + 'total ' + tp + ' website,,' + obj.website + '\n'; }
		if(obj.lunch > 0) { tp = (config.type === 'P') ? "pax" : "booking";   tt += ident + 'total ' + tp + ' lunch,,' + obj.lunch + '\n'; }
		if(obj.dinner > 0) { tp = (config.type === 'P') ? "pax" : "booking";   tt += ident + 'total ' + tp + ' dinner,,' + obj.dinner + '\n'; }
		tt += ident + 'Pricing,Lunch,' + config.lunch +',Dinner,' + config.dinner +'\n';
		tt += ident + 'Total Booking,,= (0)';
		subtotal = 0;
		segmentlabel.forEach(function(ll) {
			if(['walkin', 'waiting', 'callcenter', 'import', 'notinvoiced'].indexOf(ll) > -1)
				return;
			if(typeof config[ll] === 'undefined' || isNaN(parseFloat(config[ll])) || parseFloat(config[ll]) < 0) 
				config[ll] = 0;
			if(parseFloat(config[ll]) > 0) {
				tt += '+ (' + obj[ll] +' * ' + config[ll] + ')';
				subtotal += parseFloat(obj[ll]) * parseFloat(config[ll]);
				}
			});
			
		tt += ',' + config.currency + ' ' + subtotal.toFixed(2) + '\n';

		license = 0;
		$scope.mservice.forEach(function(oo) {
			config[oo.service] = config[oo.service] || 0; 
			config[oo.service] = parseFloat(config[oo.service]);
			if(config[oo.service] > 0) {
				license += config[oo.service];
				tt += ident + 'Monthly '+ oo.title + ' ,,' + config[oo.service] +'\n';
				}
			});
			
		if(license > 0)
			tt += ident + 'Total Bill,, ' + config.currency + ' ' + (subtotal + license).toFixed(2) + '\n';
		tt += '\n\n\n\n\n';
		return tt;
		};

	$scope.saveInvoice = function(obj, config, segmentlabel, restaurant, date, details, currency) {
		var k, subtotal, tp, segments, license;
		
		details += '\rSummary\r\r';
		details += 'total PAX \t\t' + obj.subpax + '\r';
			
		obj.callcenter = obj.ttccenter = obj.walkin = obj.waiting =  obj.import = 0; // dont compute		
		if(obj.callcenter > 0) details += 'total booking callcenter\t\t' + obj.callcenter + '\t' + 'total transactions ' + obj.ttccenter + '\r';
		if(obj.walkin > 0) { tp = "booking";   details += 'total ' + tp + ' walkin\t\t' + obj.walkin + '\r'; }
		if(obj.waiting > 0) { tp = "booking";   details += 'total ' + tp + ' waiting\t\t' + obj.waiting + '\r'; }
		if(obj.import > 0) { tp = "booking";   details += 'total ' + tp + ' import\t\t' + obj.import + '\r'; }

		if(obj.remote > 0) { tp = "booking";   details += 'total ' + tp + ' thirdparty\t\t' + obj.remote + '\r'; }
		if(obj.event > 0) { tp = "booking";   details += 'total ' + tp + ' event\t\t' + obj.event + '\r'; }
		if(obj.payment > 0) { tp = "booking";   details += 'total ' + tp + ' payment\t\t' + obj.payment + '\r'; }
		if(obj.facebook > 0) { tp = (config.websitepax > 0) ? "pax" : "booking";  details += 'total ' + tp + ' facebook\t\t' + obj.facebook + '\r'; }
		if(obj.website > 0) { tp = (config.websitepax > 0) ? "pax" : "booking";   details += 'total ' + tp + ' website\t\t' + obj.website + '\r'; }
		if(obj.lunch > 0) { tp = (config.type === 'P') ? "pax" : "booking";   details += 'total ' + tp + ' lunch\t\t' + obj.lunch + '\r'; }
		if(obj.dinner > 0) { tp = (config.type === 'P') ? "pax" : "booking";   details += 'total ' + tp + ' dinner\t\t' + obj.dinner + '\r'; }
		details += 'Pricing\tLunch\t' + config.lunch +'\tDinner\t' + config.dinner + '\r';
		details += 'Formula = ';
		subtotal = 0;
		segments = "";
		segmentlabel.forEach(function(ll) {
			if(['walkin', 'waiting', 'callcenter', 'import', 'notinvoiced'].indexOf(ll) > -1)
				return;
			if(typeof config[ll] === 'undefined' || isNaN(parseFloat(config[ll])) || parseFloat(config[ll]) < 0) 
				config[ll] = 0;
			details += '+ (' + obj[ll] +' * ' + config[ll] + ')';
			segments += ll + ',' + obj[ll] + ',' + config[ll] + ';';
			subtotal += parseFloat(obj[ll]) * parseFloat(config[ll]);
			});
			
		subtotal = Math.round(subtotal * 100) / 100;

		segments += "ttccenter" + ',' + obj.ttccenter + ',0';
		details += '\r\rTotal Booking = ' + config.currency + ' ' + subtotal.toFixed(2);

		license = 0;
		$scope.mservice.forEach(function(oo) {
			config[oo.service] = config[oo.service] || 0; 
			config[oo.service] = parseFloat(config[oo.service]);
			if(config[oo.service] > 0) {
				license += config[oo.service];
				details += 'Monthly '+ oo.title + ' \t\t' + config[oo.service] + '\r';
				}
			});
			
		if(license > 0)
			details += '\r\rTotal Bill = ' + config.currency + ' ' + (subtotal + license).toFixed(2);
		
		details = details.replace(/\'|\"/g, "’");				
		details = details.replace(/\r/g, "|||");	
		bookService.createInvoice(restaurant, date, subtotal.toFixed(2), license, currency, segments, details, $scope.email).then(function(response) {  });
		};

	// recalculate and store invoice, with detail.
	$scope.generateInvoice = function(invdate, field, segmentlabel) {

		var i, k, u, p, dontinvoice; // ajax call will not support more data
		var crestaurant, titles, error, pers, restodetails, currency, exportselect, email, restAr;		
		var config, defaultConf = { type: 'P', lunch: 3, dinner: 3, callcenter:0.5, websitebkg: 0, websitepax: 1, notinvoiced: 0 };
		var obj =Object.assign({}, $scope.glbconfig);

		exportselect = $scope.booking.slice(0);

		// add fake resa to force invoicing of licences
		restAr = $scope.getlicenserestaurant($scope.invoiceparam);
		console.log('LICENSE RESTO', restAr);
		restAr.forEach(function(resto) {
			exportselect.push({ restaurant: resto, status: "license" });
			});
			
		exportselect.sort($scope.compareFunction);
		Object.keys(obj).map(function(ll) { obj[ll] = 0; });
		crestaurant = "";
		currency = "";
		restodetails = field.join('\t') + '\r';
		p = 0;	
		exportselect.forEach(function(u) {
			if(crestaurant !== u.restaurant) {
				if(crestaurant !== '') {
					$scope.saveInvoice(obj, config, segmentlabel, crestaurant, invdate, restodetails, currency);
					}
				restodetails = field.join('\t') + '\r';
				crestaurant = u.restaurant;
				config = $scope.findrestaurant($scope.invoiceparam, crestaurant);
				if(config === null) {
					alert('Empty config for restaurant '+crestaurant + '. Enable to continue');
					return;
					}
				currency = config.currency;
				obj = Object.assign({}, $scope.glbconfig);
				Object.keys(obj).map(function(ll) { obj[ll] = 0; });
				if(config === null) { config = defaultConf; error = 1; }
				config.website = config.facebook = (config.websitebkg > 0) ? config.websitebkg : config.websitepax;
				}
				
			u.invoice = "";
			if(config.payment === 0) u.deposit = "";
			
			if(typeof u.type === "string") 
				u.type = u.type.trim();

			if(u.status === 'license') {
				obj.license++;
				return;
				}

			if(u.status === 'cancel' || u.status === 'noshow' || u.state === "no show") { //  || u.status === "expired"
				if(u.deposit !== '') {
					obj.payment++; 
					u.invoice = "payment";
					}
				return;
				}
			
			obj.count++;
							
			u.invoice = "booking";
				
			pers = parseInt(u.cover);
			email = u.email || "";
			email = email.toLowerCase();
			u.email = email;
			// indexOf tracking for the callcenter, as it can be waiting, walkin, callcenter...
			if(u.type === "website") { obj.website = (config.websitepax > 0) ? obj.website+pers : obj.website+1; u.invoice = "website";   obj.subpax += pers; }
			else if(u.type === "event") { obj.event += pers; u.invoice = "event"; }
			else if(u.type === "thirdparty") { obj.remote++; u.invoice = "thirdparty"; }
			else if(u.type === "import") { obj.import++; u.invoice = "import"; }
			else if(u.type === "facebook") { obj.facebook = (config.websitepax > 0) ? obj.facebook+pers : obj.facebook+1; u.invoice = "facebook";   obj.subpax += pers; }
			else if(u.tracking.indexOf("callcenter") > -1 || u.tracking.indexOf("tms") > -1) { if(email.substring(0,3) !== "no@" && email !== "") { obj.callcenter++; } obj.ttccenter++; u.invoice = "callcenter"; }
			else if(u.type === "walkin") { obj.walkin++; u.invoice = "walkin"; }
			else if(u.type === "waiting") { obj.waiting++; u.invoice = "waiting"; }
			else {
				if(typeof u.mealtype !== "string" || u.mealtype === "" || ["lunch", "dinner"].indexOf(u.mealtype) < 0)
					u.mealtype = "dinner";
				u.invoice = u.mealtype;
				obj[u.mealtype] += ((config.type === 'P') ? pers : 1); 
				u.type = 'weeloy'; 
				u.tracking = '';  
				obj.subpax += pers;
				}

			if(u.deposit !== '') {
				obj.payment++; 
				u.invoice += " | payment";
				}
			else if(['walkin', 'waiting', 'callcenter', 'import', 'notinvoiced'].indexOf(u.invoice) > -1)
				return;
				
			for(k = 0, vv=''; k < field.length; k++)
				vv += u[field[k]] + '\t';
			restodetails += vv + '\r';
			});
	
		// don't forget the last invoice
			$scope.saveInvoice(obj, config, segmentlabel, crestaurant, invdate, restodetails, currency);
		};
		
	$scope.help = function() {
		
		var imglogo = "../images/logo_puce.svg";

  		var helpgeneral = [
			"start", "you need first to create all the account configurations",
  			"(Re)generate invoice records", "generate monthly reports by selecting a month (any date of that month), and click on 'regenerate'",
  			"manage invoice records", "the newly created invoice records are managed in the 'View Invoices' sections",
  			"monthly invoice extraction", "the monthly invoice extraction (excel spreadsheets) is generated with the extract button",
  			"extraction description", "Only 'cumulated' invoices of $20 or plus will be generated. Invoices of less than $20, will be culumated in the future months. You can get full extraction by setting the 'all' flag. Also, monthly license fees are added to every invoices."
			];  			
			
		var myhelp = helpgeneral, theme = "General Description";
		var helpwin = window.open("","Help Table Management System","toolbar=yes,location=no,directories=yes,menubar=yes,scrollbars=yes,width=700, height=600, left=100, top=25"); 
		var content = ""; 
		content += "<html><head><title>Weeloy TMS</title>";
	    	content += "<link href='https://fonts.googleapis.com/css?family=Dosis:400,600,700bold|Roboto:400,900,700italic,700,500italic,500,400italic' rel='stylesheet' type='text/css'>";
		content += "<style>body { margin: 20 20 20 20 } table { font-family:Dosis;font-size:13px;border:0px } tr { height:35px; } td { align:center; } .truncate { white-space: nowrap; overflow: hidden; text-overflow: ellipsis; } h3 { font-family:Dosis;} </style></head><body onLoad='self.print()'><center>"; 
		content += "<p><img src='" + imglogo + "' max-width='80' max-height='80' id='theLogo' name='theLogo'></p>";
		content += "<h3>" + theme.toUpperCase() + "</h3><br/>";
		content += "<table width='100%'><head><tr style='font-weight:bold;font-size:12px'><td>Command</td><td>&nbsp; &nbsp;</td><td>Description</td></tr><tr><td colspan='10'><hr/></td></tr>";
		for(i = 0; i < myhelp.length && myhelp[i] != ""; i += 2) {
			content += "<tr><td nowrap>" + myhelp[i] + "</td><td>&nbsp; &nbsp;&nbsp; &nbsp;</td><td>" + myhelp[i+1] + "</td></tr>";
			}        
		content += "</table></center></body></html>"; 
		helpwin.document.write(content); 
		};


}]);

</script>
