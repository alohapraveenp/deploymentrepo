<div class="container">
    <div class="row">
        <div class="col-md-12 left-sec">

<div ng-controller="mginvoiceController" ng-init="moduleName='Mginvoice'; listingMgFlag = true; listing1MgFlag=false; createMgFlag=false;" >

	<div id='listing' ng-show='listingMgFlag'>
		<div class="form-group"  style='margin-bottom:25px;'>
			<div class="col-md-4">
				<div class="input-group col-md-4">
					<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
					<input type="text" class="form-control input-sm" ng-model="searchText" ng-change="paginator.setPage(0);" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" style='width:200px;'> 
				</div>
			</div>
			<div class="col-md-2"></div>
			<div class="col-md-4">
				<a href ng-click='create();' class="btn btn-success btn-sm" style='color:white;width:250px;'><span class='glyphicon glyphicon-certificate'></span> &nbsp;Create an invoice Configuration</a>
			</div>
			<div class="col-md-1">
				<div class="btn-group" uib-dropdown >
					<button type="button" class="btn btn-default btn-xs" uib-dropdown-toggle aria-expanded="false" style='font-size:11px;'>Page Size <span class="caret"></span></button>
					<ul class="dropdown-menu" uib-dropdown-menu role="menu">
						<li ng-repeat="x in paginator.pagerange()" class='glyphiconsize'><a href ng-class="{ 'glyphicon glyphicon-ok glyphiconsize': x === paginator.getRowperPage() }" href ng-click="paginator.setRowperPage(x)"> {{x}}</a></li>
					</ul>
				</div> 		
			</div>
		</div>
        <div style=" clear: both;"></div>

		<table width='100%' class="table table-condensed table-striped" style='font-size:12px;'>
			<thead>
				<tr>
				<th ng-repeat="y in tabletitle | filter:{h:0}"><tbtitle var="{{y.a}}" name='tabletitle'  module='moduleName'/></th>
				<th>update</th><th> &nbsp; </th><th>delete</th>
				</tr>
			 </thead>
			<tbody style='font-family:Roboto;font-size:12px;'>
			<tr ng-repeat="x in filteredMginvoice = (invoiceparam | filter:searchText) | sizefilter:this | orderBy:predicate:reverse | slicefilter:paginator.getPage():paginator.getRowperPage():paginator.getItemCount() | limitTo:paginator.getRowperPage()" style='font-family:helvetica;font-size:11px;'>
				<td ng-repeat="y in tabletitle | filter:{h:0}"><a href ng-click="view(x)">{{ x[y.a] | adatereverse:y.c}}</a></td>
				<td align='center'><a href ng-click="update(x)"><span class='glyphicon glyphicon-pencil blue'></a></span></td>
				<td width='10'>&nbsp;</td>
				<td align='center'><a href ng-click="delete(x)"><span class='glyphicon glyphicon-trash red'></a></span></td>
			</tr><tr><td colspan='{{tabletitle.length+3}}'></td></tr>
			</tbody>
		</table>
		<div ng-if="filteredMginvoice.length >= paginator.getRowperPage()" class="col-md-12" align="center" style='font-size:11px' ng-include="'inc/paginator.html'"></div>
	</div>

	<div class="col-md-12" ng-show='listing1MgFlag'>
		<br /><a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a><br />
		<table class='table-striped' style="margin: 0 0 150px 30px;font-size:13px;font-family: Roboto">		
			<tr ng-repeat="z in tabletitle"><td nowrap><strong>{{ z.b }} :</strong></td><td width='30'> &nbsp; </td><td> {{ selectedItem[z.a] }}</td></tr>
		</table>
	</div>

	<div class="col-md-12" ng-show='createMgFlag'>
		<br /><a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a><br />

		 <div class="col-md-2"></div>
		 <div class="col-md-8">
			   <div ng-repeat="z in tabletitle" class="row" style="padding-bottom:20px;">
				<div class="input-group">
					<div class='input-group-btn' uib-dropdown ng-if="z.v.length > 0">
					<button type='button' class='btn btn-default btn-sm' uib-dropdown-toggle >&nbsp;<i class="glyphicon glyphicon-{{z.d}}"> {{z.b}} </i>&nbsp;<span class='caret'></span></button>
					<ul class='dropdown-menu scrollable-menu' uib-dropdown-menu >
					<li ng-repeat="p in z.v"><a href ng-click="selectedItem[z.a]=p;">{{ p }}</a></li>
					<li class="divider"></li>
					<li><a href ng-click="selectedItem[z.a]='';">Reset</a></li>
					</ul>
					</div>
					<span class="input-group-addon" ng-if="z.v.length === 0"><i class="glyphicon glyphicon-{{z.d}}"></i> {{z.b}}</span>
					<input type="text" class="form-control input-sm" ng-model="selectedItem[z.a]" placeholder="{{z.b}}" ng-readonly="z.a==='restaurant' || z.v.length > 0"> 
				</div>
				</div>
							
			<div class="row"> 
				<p align='right'><a href ng-click='savenew();' class="btn btn-success btn-sm" style='color:white;width:200px;'><span class='glyphicon glyphicon-save'></span> &nbsp;{{ buttonlabel }} </a></p>
			</div>
		</div>
		<div class="col-md-2"></div>
	</div>
</div>

</div>
</div>
</div>

<script>

var token = <?php echo "'".$_SESSION['user_backoffice']['token']."';"; ?>
var selectionAr;

app.controller('mginvoiceController', ['$scope', '$http', 'bookService', function($scope, $http, bookService) {
	
	var forceDate = new Date(new Date().getFullYear()-2, 0, 1);
	$scope.paginator = new Pagination(25);
	$scope.email = <?php echo "'" . $email . "';"; ?>
	$scope.predicate = '';
	$scope.reverse = false;
	$scope.tabletitle = [ {a:'restaurant', b:'Restaurant', c:'', d:'tower', v:[] , q:'down', cc:'', d: 'fuchsia', h:0 }, {a:'type', b:'T', c:'', d:'tags', v:['B', 'P'] , q:'down', cc:'', d: 'black', h:0 }, {a:'lunch', b:'Lun', c:'', d:'cutlery', v:[] , q:'down', cc:'', d: 'black', h:0 }, {a:'dinner', b:'Din', c:'', d:'cutlery', v:[] , q:'down', cc:'', d: 'black', h:0 }, {a:'event', b:'Evt', c:'', d:'birthday-cake', v:[] , q:'down', cc:'', d: 'black', h:0 }, {a:'payment', b:'Pay', c:'', d:'credit-card', v:[] , q:'down', cc:'', d: 'black', h:0 }, {a:'callcenter', b:'CC', c:'', d:'phone-alt', v:[], q:'down', cc:'', d: 'black', h:1 }, {a:'websitepax', b:'WP', c:'', d:'user', v:[] , q:'down', cc:'', d: 'black', h:0 }, {a:'websitebkg', b:'WB', c:'', d:'shopping-cart', v:[] , q:'down', cc:'', d: 'black', h:0 }, {a:'remote', b:'BC', c:'', d:'shopping-cart', v:[] , q:'down', cc:'', d: 'black', h:0 }, {a:'currency', b:'Cur', c:'', d:'usd', v:['SGD', 'BAHT', 'HKD'] , q:'down', cc:'', d: 'black', h:0 }, {a:'mrs', b:'MRS', c:'', d:'ban-circle', v:[] , q:'down', cc:'', d: 'black', h:0 }, {a:'mcc', b:'MCC', c:'', d:'ban-circle', v:[] , q:'down', cc:'', d: 'black', h:0 }, {a:'mtms', b:'MTMS', c:'', d:'ban-circle', v:[] , q:'down', cc:'', d: 'black', h:0 }, {a:'mmkg', b:'MMKG', c:'', d:'ban-circle', v:[] , q:'down', cc:'', d: 'black', h:0 } ];
	$scope.bckups = $scope.tabletitle.slice(0);
	$scope.allrestaurant = [];

	$scope.getAlignment = bkgetalignment;

	$scope.Objaccount = function() {
		return {
			restaurant: '',
			type: 'P', 
			lunch: 3, 
			dinner: 3, 
			callcenter:0, 
			websitebkg: 0, 
			websitepax: 1, 
			remote: 0.3, 
			event: 1, 
			notinvoiced: 0,
			currency: 'SGD',
			mrs: '',
			mcc: '',
			mtms: '',
			mmkg: '',
			
			replicate: function(obj) {
				for (var attr in this)
		        	if(this.hasOwnProperty(attr) && typeof this[attr] !== 'function' && attr !== '$$hashKey') 
        				this[attr] = obj[attr];
				return this;
				},						
			clean: function() {
				for (var attr in this)
		        	if(this.hasOwnProperty(attr) && typeof this[attr] !== 'function' && attr !== '$$hashKey') {
        				if(typeof this[attr] === 'string')
        					this[attr] = this[attr].replace(/[^a-zA-Z_\-.0-9]/g, '');
        				}
				return this;
				}	
			};
		};
		
	$scope.initorder = function() {
		$scope.tabletitle = $scope.bckups;
		$scope.predicate = "restaurant";
		$scope.reverse = false;
	};

	$scope.reorder = function(item, alter) {
		alter = alter || "";
		if (alter !== "")  item = alter;
		$scope.reverse = ($scope.predicate === item) ? !$scope.reverse : false;
		$scope.predicate = item;
	};
				
	bookService.readallinvConf($scope.email).then(function(response) { 
		var i, j, dd, ll = ['lunch', 'dinner', 'callcenter', 'websitebkg', 'websitepax', 'remote', 'notinvoiced'];
		$scope.invoiceconf = response.config;  
		if($scope.invoiceconf.length < 1) {
			//$scope.initInvoice();
			alert('UNABLE TO READ');
			return; 
			}
		$scope.invoiceparam = [];
		for(i = 0; i < $scope.invoiceconf.length; i++) {
			obj = JSON.parse($scope.invoiceconf[i].config.replace(/’/g, "\""));
			for(j = 0; j < ll.length; j++) {
				obj[ll[j]] = (obj[ll[j]] === '' || isNaN(parseFloat(obj[ll[j]]))) ? 0 : parseFloat(obj[ll[j]]); 
				}
			$scope.invoiceparam.push(obj);
			}

		$scope.invoiceparam.sort(function(a, b) { return a.restaurant.localeCompare(b.restaurant); });	
		$scope.paginator.setItemCount($scope.invoiceparam.length);
		$scope.initorder();
		});

	bookService.readallrestonames($scope.email).then(function(response) { 
		for(var i = 0; i < response.names.length; i++)
			$scope.allrestaurant.push(response.names[i].restaurant);
		});
	 
	$scope.reset = function(item) {
		$scope.listingMgFlag = false;
		$scope.listing1MgFlag = false
		$scope.createMgFlag = false;
		$scope[item] = true;	
		};

	$scope.backlisting = function() {
		$scope.reset('listingMgFlag');
		};
		
	$scope.view = function(oo) {
		$scope.selectedItem = oo;
		$scope.reset('listing1MgFlag');
		};
	
	$scope.create = function() {
		$scope.selectedItem = new $scope.Objaccount();
		$scope.tabletitle[0].v = $scope.allrestaurant;
		$scope.reset('createMgFlag');
		$scope.buttonlabel = "Save new account";
		$scope.action = "create";
		return false;
		}
				
	$scope.update = function(oo) {
		$scope.selectedItem = new $scope.Objaccount().replicate(oo);
		$scope.tabletitle[0].v = [];
		$scope.reset('createMgFlag');
		$scope.buttonlabel = "update account";
		$scope.action = "update";
		};

	$scope.findaccount = function(restaurant) {
		for(var i = 0; i < $scope.invoiceparam.length; i++)
			if($scope.invoiceparam[i].restaurant === restaurant)
				return i;
		return -1;
		};
		
	$scope.savenew = function() {
		var oo = $scope.selectedItem;
		oo.clean();
		if($scope.action === "create") { 
			bookService.createinvConf($scope.email, oo )
			.then(function(response) {
                               bookService.logevent(160,'BO_INVOICE_create','BO_INVOICE');
				$scope.invoiceparam.push(oo);
				$scope.invoiceparam.sort(function(a, b) { return a.restaurant.localeCompare(b.restaurant); });	
				alert("Account has been created"); 
				});
			}

		else {
			bookService.updateinvConf($scope.email, $scope.selectedItem )
			.then(function(response) { 
                            bookService.logevent(162,'BO_INVOICE_update','BO_INVOICE');
				var ind = $scope.findaccount($scope.selectedItem.restaurant);
				if(ind >= 0) {
					$scope.invoiceparam.splice(ind, 1);
					$scope.invoiceparam.push($scope.selectedItem);
					$scope.invoiceparam.sort(function(a, b) { return a.restaurant.localeCompare(b.restaurant); });	
					}
				alert("Account has been updated"); 
				});
			}			
 		$scope.backlisting();		
		};
		
	$scope.delete = function(oo) {	
		if(confirm("Are you sure you want to delete " + oo.restaurant) == false)
			return;
		bookService.deleteinvConf(oo.restaurant, $scope.email).then(function() { 
                      bookService.logevent(162,'BO_INVOICE_delete','BO_INVOICE');
			var ind = $scope.findaccount(oo.restaurant);			
			$scope.invoiceparam.splice(ind, 1);
			alert(oo.restaurant + " has been deleted");
			});
		};
}]);

	
</script>

