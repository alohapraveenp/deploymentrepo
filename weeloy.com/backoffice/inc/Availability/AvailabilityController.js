app.controller('AvailabilityController', ['$scope', 'bookService', function($scope, bookService) {
	var forceDate = new Date(new Date().getFullYear()-2, 0, 1);
	var atime = (function() { var i, v, arr=[]; for(i = 9; i < 24; i++) { v = ((i<10)?'0':'')+i+':'; arr.push(v+"00"); arr.push(v+"30"); } return arr; })();
	var aslots = (function() { var i, arr=[]; for(i = 1; i < 10; i++) { arr.push(i); } return arr; })();
	$scope.paginator = new Pagination(25);
	$scope.token = $('#token').val();
	$scope.restaurant = $("#restaurant").val();
	$scope.email = $("#email").val();
	$scope.predicate = '';
	$scope.reverse = false;
	$scope.selectedItem = null;
	$scope.nonefunc = function() { var n = $scope.selectedItem.etime.timetoslot() - $scope.selectedItem.stime.timetoslot(); if(n < 0) n = 0; $scope.selectedItem.slots = n; };
	$scope.mydata_start = new bookService.ModalDataBooking();
	$scope.mydata_start.setInit(new Date(), "09:00", function() { $scope.selectedItem.date = $scope.mydata_start.originaldate.getDateFormatReverse('-'); }, 3, new Date(), new Date(new Date().getFullYear()+1, 11, 31));
	$scope.tabletitle = [ {alter:'datetime', a:'date', b:'Date', c:'date', q:'up', cc: 'orange' }, 
							{a:'stime', b:'Time', c:'', q:'down', cc: 'black'}, {a:'slots', b:'Duration', c:'', q:'down', cc: 'black',  }, 
							{a:'tablepax', b:'Table/Pax', c:'', q:'down', cc: 'black',  }, {a:'description', b:'Description', c:'', q:'down', cc: 'black',  } ];
	$scope.bckups = $scope.tabletitle.slice(0);
	$scope.tabletitleContent = [ { a:'date', b:'Date', c:'date', d:'calendar', t:'date', u:$scope.mydata_start }, 
						{ a:'stime', b:'Starting Time', c:'', d:'time', t:'array', val: atime, func: $scope.nonefunc }, 
						{ a:'etime', b:'Ending Time', c:'', d:'time', t:'array', val: atime, func: $scope.nonefunc }, 
						{ a:'slots', b:'Duration', c:'', d:'flash', t:'input' }, 
						{ a:'tablepax', b:'Table/Pax', c:'', d:'cutlery', t:'input' }, 
						{ a:'description', b:'Description', c:'', d:'list', t:'textarea' } ];
	$scope.getAlignment = bkgetalignment;
 	$scope.type = 'Inventory';
 	$scope.typeChanged = function() {
	}
	
	$scope.Objblock = function() {
		return {
			restaurant: $scope.restaurant, 
			blockID: Math.floor((Math.random() * 10000000) + 1),
			index: 0, 
			date: '',
			ddate:'',
			datetime: 0, 
			stime: '12:00', 
			etime: '12:00', 
			slots: 0, 
			tablepax: 1, 
			description: '', 
			remove: function() { 
				for(var i = 0; i < $scope.names.length; i++) 
					if($scope.names[i].blockID === this.blockID) {
						$scope.names.splice(i, 1);
						break;
						}
				},
			replicate: function(obj) {
				for (var attr in this)
		        	if(this.hasOwnProperty(attr) && typeof this[attr] !== 'function' && attr !== '$$hashKey') 
        				this[attr] = obj[attr];
				return this;
				},		
			clean: function() {
				if(this.date === '') 
					this.date = $scope.mydata_start.getDate('-', 'reverse');
				for (var attr in this)
		        	if(this.hasOwnProperty(attr) && typeof this[attr] !== 'function' && attr !== '$$hashKey' && typeof this[attr] === 'string')
        				this[attr] = this[attr].replace(/\'|\"/g, '’');
				return this;
			},		
			check: function() {
				if(this.slots === 0) { alert('Duration is null'); return -1;}
				if(this.date === '') { alert('Invalid date'); return -1;}
				if($scope.type === 'Inventory' && this.tablepax < 1) { 
					alert('Invalid table or pax. It should be >= 1'); 
					return -1;
				} else if ($scope.type === 'Time')
					this.tablepax = -13;
				return 1;
			}
		};
	};
	$scope.multProd = [];
	if($("#multiproduct").val()	!== "") {
		$scope.multProd = $("#multiproduct").val().split("|");
		//console.log("multiproduct: " + JSON.stringify($scope.multProd));
	}
		
	$scope.bkproduct = "";
	if($scope.multProd.length > 0) 
		$scope.bkproduct = $scope.multProd[0];
	
	$scope.setalloteproduct = function(val) {
		$scope.bkproduct = val;
		$scope.readBlock($scope.restaurant, $scope.bkproduct);
		}
		
	$scope.initorder = function() {
		$scope.tabletitle = $scope.bckups;
		$scope.predicate = "datetime";
		$scope.reverse = true;
	};

	$scope.reorder = function(item, alter) {
		alter = alter || "";
		if (alter !== "")  item = alter;
		$scope.reverse = ($scope.predicate === item) ? !$scope.reverse : false;
		$scope.predicate = item;
	};

	$scope.readBlock = function(restaurant, product) {
		bookService.readBlock(restaurant, product, $scope.email).then(function(response) {
			var i, data;

			if(response.status !== 1 && response.status !== "1")
				return;
		
			$scope.names = [];	 	
			data = response.data.block;
			for (i = 0; i < data.length; i++) {
				data[i].index = i + 1;
				nslots = data[i].stime.timetoslot() + parseInt(data[i].slots);
				data[i].datetime = data[i].date.jsdate().getTime();
				data[i].ddate = data[i].date.jsdate().getDateFormat('-');
				data[i].etime = parseInt(nslots / 2) + ":" + parseInt(nslots % 2) * 3 + '0';
				$scope.names.push(new $scope.Objblock().replicate(data[i]));
				}
			
			$scope.paginator.setItemCount($scope.names.length);
			$scope.initorder();
			});
		};

	$scope.readBlock($scope.restaurant, $scope.bkproduct);
	
	$scope.cleaninput = function(ll) {
		if(typeof $scope.selectedItem[ll] === 'string')
			$scope.selectedItem[ll] =  $scope.selectedItem[ll].replace(/\'|\"/g, '’');
		};
		
	$scope.reset = function(item) {
		$scope.listBlockFlag = false;
		$scope.viewBlockFlag = false
		$scope.createBlockFlag = false;
		$scope[item] = true;	
		};

	$scope.backlisting = function() {
		$scope.reset('listBlockFlag');
		};
		
	$scope.findaccount = function(blockID) {
		for(var i = 0; i < $scope.names.length; i++)
			if($scope.names[i].blockID === blockID)
				return i;
		return -1;
		};
		
	$scope.view = function(oo) {
		$scope.selectedItem = oo;
		$scope.reset('viewBlockFlag');
		};
	
	$scope.create = function() {
		$scope.selectedItem = new $scope.Objblock();
		$scope.mydata_start.setDate(new Date());
		$scope.reset('createBlockFlag');
		$scope.buttonlabel = "Save new block";
		$scope.action = "create";
		return false;
		}
				
	$scope.update = function(oo) {
		$scope.selectedItem = new $scope.Objblock().replicate(oo);
		$scope.mydata_start.originaldate = new Date($scope.selectedItem.date).getDateFormatReverse('-');;
		$scope.reset('createBlockFlag');
		$scope.buttonlabel = "Update new block";
		$scope.action = "update";
		};

	$scope.savenewblock = function() {
		var u, msg, apiurl, ind;
	
		$scope.selectedItem.product = $scope.bkproduct;
		$scope.selectedItem.clean();		
		if($scope.selectedItem.check() < 0)
			return;
		$scope.selectedItem.datetime = $scope.selectedItem.date.jsdate().getTime();
		$scope.selectedItem.ddate = $scope.selectedItem.date.jsdate().getDateFormat('-');
	
		if($scope.action === "create") {
			$scope.selectedItem.index = ($scope.names.length > 0) ? $scope.names[$scope.names.length - 1].index + 1 : 1;
			//console.log("token: " + $scope.token + " Restaurant: " + $scope.restaurant + " email: " + $scope.email + " selectedItem: " + JSON.stringify($scope.selectedItem));
			bookService.createBlock($scope.restaurant, $scope.email, $scope.selectedItem).then(function(response) {
				//console.log("createBlock response: " + JSON.stringify(response));
				$scope.names.push($scope.selectedItem);
				//$scope.$apply();
				alert("Bock period has been created"); 
				});
			}
		else {
			bookService.updateBlock($scope.restaurant, $scope.email, $scope.selectedItem).then(function(response) { 
				ind = $scope.findaccount($scope.selectedItem.blockID);	
				if(ind >= 0) $scope.names.splice(ind, 1);
				$scope.names.push($scope.selectedItem);
				//$scope.$apply();
				alert("Bock period has been updated"); 
				});
			}
 		$scope.backlisting();		
		};
		
	$scope.delete = function(oo) {
		if(confirm("Are you sure you want to delete blocking period " + oo.date) == false)
			return;
			
		bookService.deleteBlock($scope.restaurant, $scope.email, oo.blockID).then(function() { 
			oo.remove();
			alert(oo.date + " block period has been deleted"); 
			});		
		};
}]);