
<div class="container">
    <div class="row">
        <div class="col-xl-12 left-sec">
            <div ng-controller="SmsinvoiceController" ng-init="moduleName = 'sms'; listSMSFlag = true; viewSMSFlag = false; createSMSFlag = false;" >

                <div id='listing' ng-show='listSMSFlag'>
                    <div class="form-group"  style='margin-bottom:25px;'>
                        <div class="col-md-4">
                            <div class="input-group col-md-4">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                                <input type="text" class="form-control input-sm" ng-model="searchText" ng-change="paginator.setPage(0);" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" style='width:200px;'> 
                            </div>
                        </div>

                       <div class="col-md-2">
                            <div class="btn-group" uib-dropdown>
                                <button type="button" class="btn btn-default btn-xs"  uib-dropdown-toggle aria-expanded="false" style='font-size:11px;'>Filter <span class="caret"></span></button>
                                <ul class="dropdown-menu" uib-dropdown-menu>
                                    <li ng-repeat="x in filterlabel" class='glyphiconsize'>
                                        <a href ng-class="{ 'glyphicon glyphicon-ok glyphiconsize': x === currentfilter }" ng-click="getData(x)"> {{x}}</a>
                                    </li>
                                </ul>
                            </div> 
                        </div>
 
                        <div class="col-md-2" style='font-size:10px;'>
                            selected records: <strong> {{filteredSMS.length}} </strong>
                        </div>

                        <div class="col-md-2">
                            <div class="btn-group"  uib-dropdown>
                                <button type="button" class="btn btn-default btn-xs"  uib-dropdown-toggle  aria-expanded="false" style='font-size:11px;'>Page Size <span class="caret"></span></button>
                                <ul class="dropdown-menu" uib-dropdown-menu>
                                    <li ng-repeat="x in paginator.pagerange()"><a href ng-class="{ 'glyphicon glyphicon-ok glyphiconsize': x === paginator.getRowperPage() }"  ng-click="paginator.setRowperPage(x)">  {{x}}</a></li>
                                </ul>
                            </div> 		
                        </div>

                        <div class="col-sm-2">
                            <form action="echo.php" id="extractBkOfDay" name="extractBkOfDay" method="POST" target="_blank">
                                <input type="hidden" ng-model='contentBkOfDay' name="contentBkOfDay" id="contentBkOfDay">
                                <a href ng-click='extractSelection();' class="btn btn-success btn-sm" style='color:white;'><span class='glyphicon glyphicon-save'></span> &nbsp;Extract</a>
                            </form>
                        </div>

                    </div>

                    <div style=" clear: both;"></div>
                    <table width='100%' class="table table-condensed table-striped" style='font-size:12px;'>
                        <thead>
                            <tr>
                                <th ng-repeat="y in tabletitle"><tbtitle var="{{y.a}}" name='tabletitle' module='moduleName'/></th>
                        </tr>
                        </thead>
                        <tbody style='font-family:helvetica;font-size:12px;'>
                            <tr ng-repeat="x in filteredSMS = (names| filter:searchText) | sizefilter:this | orderBy:predicate:reverse | slicefilter:paginator.getPage():paginator.getRowperPage():paginator.getItemCount() | limitTo:paginator.getRowperPage()" style='font-family:helvetica;font-size:11px;'>
                                <td ng-repeat="y in tabletitle">
                                    {{ x[y.a] | adatereverse:y.c }}
                                    </span>
                                </td>
                            </tr><tr><td colspan='{{tabletitle.length + 3}}'></td></tr>
                        </tbody>
                    </table>
                    <div ng-if="filteredSMS.length >= paginator.getRowperPage()" class="col-md-12" align="center" style='font-size:11px' ng-include="'inc/paginator.html'"></div>
                </div>

                <div class="col-md-12" ng-show='viewSMSFlag'>
                    <br /><a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a><br />
                    <table class='table-striped' style="margin: 0 0 150px 30px;font-size:13px;font-family: Roboto">		
                        <tr ng-repeat="y in tabletitle| filter: {b: '!picture'}"><td nowrap ><strong> {{ y.b}} </strong></td><td> &nbsp; </td><td>{{ selectedItem[y.a] | adatereverse:y.c }} </td></tr>

                    </table>
                </div>

            </div>

        </div>
    </div>
</div>
<script>
    var restaurant = <?php echo "'" . $theRestaurant . "';"; ?>
    app.controller('SmsinvoiceController', ['$scope', '$http', '$timeout', 'bookService', function($scope, $http, $timeout, bookService) {
    var todaydate = new Date();
            var aorder = (function() { var i, arr = []; for (i = 0; i < 30; i++) arr.push(i); return arr; })();
            $scope.paginator = new Pagination(100);
            $scope.searchText = "";
            $scope.restaurant = restaurant;
            $scope.email = <?php echo "'" . $email . "';"; ?>
            $scope.predicate = '';

            $scope.reverse = false;
            $scope.nonefunc = function() {};
            $scope.filterlabel = ['day', 'week', 'lastweek', 'month', 'lastmonth', 'year'];
            $scope.isopendate = false;
            $scope.tabletitle = [ {a:'index', b:'ID', c:'', q:'down', cc: 'black' }, {a:'restaurant', b:'Restaurant', c:'', q:'down', cc: 'black' }, {a:'type', b:'Type', c:'', q:'down', cc: 'black' }, {a:'count', b:'TotalCount', c:'', q:'down', cc: 'black' }, { a:'DATE', b:'Date', c:'', q:'down', cc: 'black' }];
  
            //$scope.tabletitleContent = [ { a:'index', b:'ID', c:'', d:'sort', t:'dropdown',  }, { a:'restaurant', b:'Restaurant', c:'', d:'sort' },{ a:'type', b:'Type', c:'', d:'flag', t:'checkarr' }];

            $scope.is_showdate = function(){
            $scope.isopendate = true;
            };
            $scope.initorder = function() {
            $scope.tabletitle = $scope.bckups;
                    $scope.predicate = "vorder";
                    $scope.reverse = true;
            };
            $scope.reorder = function(item, alter) {
            alter = alter || "";
                    if (alter !== "")  item = alter;
                    $scope.reverse = ($scope.predicate === item) ? !$scope.reverse : false;
                    $scope.predicate = item;
            };
            $scope.sortdate = function(x, curr){
            var m_names = ['January', 'February', 'March',
                    'April', 'May', 'June', 'July',
                    'August', 'September', 'October', 'November', 'December'];
                    var d = new Date(), n;
                    if (x === 'month'){
            var m = new Date(curr);
                    n = m_names[m.getMonth()];
            }
            if (x === 'lastmonth'){n = m_names[d.getMonth() - 1]; }
            if (x === 'year'){  var y = new Date(curr); n = y.getFullYear(); }
            if (x === 'lastweek'){
            var firstday = new Date(d.setDate(d.getDate() - d.getDay() - 7));
                    var lastday = new Date(d.setDate(d.getDate() - d.getDay() + 6));
                    var date = new Date(firstday), ldate = new Date(lastday), f, l;
                    var month = date.getMonth() + 1;
                    f = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
                    l = ldate.getFullYear() + '-' + (ldate.getMonth() + 1) + '-' + ldate.getDate();
                    n = f + "/" + l;
            }
            if (x === 'week'){
            var firstday = new Date(d.setDate(d.getDate() - d.getDay()));
                    var lastday = new Date(d.setDate(d.getDate() - d.getDay() + 6));
                    var date = new Date(firstday), ldate = new Date(lastday), f, l;
                    f = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
                    l = ldate.getFullYear() + '-' + (ldate.getMonth() + 1) + '-' + ldate.getDate();
                    n = f + "/" + l;
            }
            return n;
            };
            $scope.getData = function(x){
            	if($scope.filterlabel.indexOf(x) < 0) {
			alert('Invalid filter ' + x);
			return;
			}
		$scope.currentfilter = x;
                var url = "../api/notification/smstracking/"+$scope.restaurant +"/" + x + "/1";
                $http.get(url).then(function(response) {
                        var items = $scope.roles;
                        $scope.selectedItem = [];
                        $scope.names = [];
                        var data = response.data.data.history;
                        data.map(function(oo) {
				//if (!oo.email || oo.email === "")  return;                         	
				if (x !== 'day') 
					oo.DATE = $scope.sortdate(x, oo.DATE);
				oo.index = oo.ID;
				$scope.names.push(oo);
				});
			$scope.paginator.setItemCount($scope.names.length);
                });
            }

	    $scope.currentfilter = 'day';
            $scope.getData($scope.currentfilter);
            $scope.cleaninput = function(ll) {
            if (typeof $scope.selectedItem[ll] === 'string')
                    $scope.selectedItem[ll] = $scope.selectedItem[ll].replace(/\'|\"/g, '’');
            };
            $scope.reset = function(item) {
            $scope.listSMSFlag = false;
                    $scope.viewSMSFlag = false
                    $scope.createSMSFlag = false;
                    $scope[item] = true;
            };
            $scope.backlisting = function() {
            $scope.reset('listSMSFlag');
          };
            $scope.view = function(oo) {
            $scope.selectedItem = oo;
                    $scope.reset('viewSMSFlag');
            };
            $scope.extractSelection = function() {

                var tt, sep, limit, maxlimit = 1000, exportselect = $scope.filteredSMS; // ajax call will not support more data
                        limit = (exportselect.length <= maxlimit) ? exportselect.length : maxlimit;
                        tt = sep = '';
                        for (i = 0; i < limit; i++, sep = ', ') {
                u = exportselect[i];
                        tt += sep + '{ ' +
                        '"restaurant":"' + u.restaurant + '", ' +
                        '"type":"' + u.type + '", ' +
                        '"date":"' + u.DATE + '", ' +
                        '"TotalCount":"' + u.count + '" }';
                }
                tt = '{ "smstracking":[' + tt + '] }';
                        $scope.contentBkOfDay = tt;
                        $('#contentBkOfDay').val(tt);
                        $('#extractBkOfDay').submit();
          };
    }]);</script>

