angular.module("materialExample", ["ngMaterial", "materialCalendar"]);
angular.module("materialExample").config(function ($mdThemingProvider) {
        $mdThemingProvider
            .theme("default")
            .primaryPalette("cyan")
            .accentPalette("light-green");
    });
app.controller('EventOrderingController', ['$scope', '$http', '$mdDialog', 'bookService', 'cleaningData', 'MaterialCalendarData', 'extractService', '$filter', '$q', '$timeout', '$log', function ($scope, $http, $mdDialog, bookService, cleaningData, MaterialCalendarData, extractService, $filter, $q, $timeout, $log) {
        var paxnumbers = (function () {
            var i, arr = [];
            for (i = 0; i <= 300; i++)
                arr.push(i);
            return arr;
        })();
        var aoccasion = ['Corporate dinner', 'Meeting and Seminars', 'Social ie Anniversary / Birthday', 'Solemnisation Ceremony', 'Weddings', 'Others'];
        var astatus = ['pending', 'postponed', 'cancel', 'active', 'close'];
        var atype = ['Private', 'Public'];
        var atitle = ['Mr.', 'Mrs.', 'Ms'];
        var aorder = (function () {
            var i, arr = [];
            for (i = 0; i < 30; i++)
                arr.push(i);
            return arr;
        })();


	var forceDate = new Date(new Date().getFullYear()-2, 0, 1);
        $scope.showhidearchive = "Show Archive";
        $scope.currency = "";
        $scope.paginator = new Pagination(25);
        $scope.restaurant = $("#restaurant").val();
        $scope.restoUrl = $("#theRestoUrl").val();
        $scope.typeselection = 1;
        $scope.email = $("#email").val();
        $scope.names = [];
        $scope.subnames = [];
        $scope.menus = [];
        $scope.submenus = [];
        $scope.pmenus = [];
        $scope.psubmenus = [];
        $scope.eventimages = [];
        $scope.catergoryAr = [];
        $scope.predicate = 'index';
        $scope.reverse = false;
        $scope.notifydetails = false;
        $scope.pendingsave = false;    
        $scope.milestones = {};
        $scope.multipledata = ['media', 'timing', 'additional'];
        $scope.deposit = ['no deposit' ,'25%' ,'50%' ,'100%'];
        $scope.payment_method = [{'creditcard' :false, 'banktransfer' :false, 'manual' :false,'nodeposit' :false}];
        $scope.multiplemodel = {
            media: {name: ''},
            additional: {name: ''},
            timing: {name: '', funct: '', venue: '', garantee: '', expected: ''}
        };

        $scope.menutypeflag = ['private', 'drink', 'extented', 'setlunch', 'options'];
        $scope.milestonetitle = [{a: 'eventrequest', b: 'Event Request', c: 'request_sent'}, {a: 'buildmenu', b: 'Menu Built', c: 'menu_built'}, {a: 'selmenuitem', b: 'Select Menu Items', c: 'menu_selected'}, {
            a: 'saveandpayment',
            b: 'Save and Proceed to payment',
            c: 'pending_payment'
        },{a: 'finalize', b: 'Finalize', c: 'confirmed'}];
        $scope.moretitle = [{a: 'deposit', b: 'Deposit'}, {a: 'billing', b: 'Billing'},{a: 'minimumprice', b: 'Minimum Price'},{a: 'beo', b: 'BEO'}, {a: 'leadtime', b: 'Lead Time'},{a: 'privatenote', b: 'Private Note'},{a: 'additionalnote', b: 'Additional Note'}];
        $scope.tabletitle = [  {alter: 'datetime', 'a': 'rdate', 'b': 'Date', c: 'date', q: 'down', cc: 'black'},
                        { 'a': 'rtime', 'b': 'Time', c: '', q: 'down', cc: 'black'}, 
                        {'a': 'restitle', 'b': 'Restaurant', c: '', q: 'down', cc: 'black'}, 
                        {alter: 'cdatetime', 'a': 'cdate', 'b': 'CreateDate', c: 'date', q: 'up', cc: 'orange'}, 
                        {'a': 'company', 'b': 'Company', c: '', q: 'down', cc: 'black'},
        				{ 'a': 'eventID', 'b': 'Event ID',  c: '', q: 'down', cc: 'black' }, 
                        {'a': 'fullname', 'b': 'guest', c: '', q: 'down', cc: 'black'}, 
                        {'a': 'pax', 'b': 'pax', c: '', q: 'down', cc: 'black'},
                        {'a': 'type', 'b': 'Type', c: '', q: 'down', cc: 'black'}, 
                        {'a': 'status', 'b': 'Status', c: '', q: 'down', cc: 'black'}, 
                        {'a': 'warningmsg', 'b': 'warning', c: '', q: 'down', cc: 'black'}, ];
        $scope.bckups = $scope.tabletitle.slice(0);
        $scope.path = pathimg;
        $scope.current = null;
        $scope.mydata = new bookService.ModalDataBooking();
        $scope.cdate = new Date().today();
        $scope.createdate = new Date().today().getDateFormatReverse('-');
        $scope.editdate = $scope.cdate.getTime() - (2 * 24 * 3600 * 1000);
        $scope.isShowMulti = (multirest > 1) ? true : false;
        
        $scope.cleananinput = function (x) {
            x.clean();
        }
        $scope.mydata.setInit($scope.cdate, "09:00", function () {
            $scope.selectedItem.rdate = $scope.mydata.originaldate.getDateFormatReverse('-');
        }, 3, new Date(), new Date($scope.cdate.getFullYear() + 1, 11, 31));

        $scope.orderViewContent = [{value: 'eventID', label: 'Order number', c: ''}, {value: 'rdate', label: 'event date', c: 'date'}, {value: 'fullname', label: 'Guest', c: ''}, {value: 'email', label: 'Email', c: ''}, {value: 'phone', label: 'Phone', c: ''}, {
            value: 'cdate',
            label: 'Create Date',
            c: 'date'
        }, {value: 'total', label: 'Total Order', c: ''}, {value: 'nborder', label: 'Number of meals', c: ''}, {value: 'quantity', label: 'Total quantity', c: ''}, {value: 'tax', label: 'Tax', c: ''}, {value: 'address', label: 'Address', c: ''}, {value: 'zip', label: 'Zip Code', c: ''}];
        //$scope.orderCreaContent = [ { a:'restaurant', b:'Restaurant', c:false, d:'cutlery', t:'input', r:true }, { a:'eventID', b:'Order ID', c:false, d:'tag', t:'input', r:true }, { a:'status', b:'Status', c:false, d:'check-circle', t:'input', r:true }, { a: $scope.mydata, b:'Delivery Date', c:false, d:'calendar', t:'date', r:false }, { a:'total', b:'Total Price', c:false, d:'usd', t:'input', r:false }, { a:'tax', b:'Tax', c:false, d:'plus', t:'input', r:false }, { a:'delivery_mode', b:'Delivery Type', c:false, d:'truck', t:'array', val: atype, func: $scope.nonefunc, r:false }, { a:'title', b:'Title', c:false, d:'user', t:'array', val: atitle, func: $scope.nonefunc, r:false }, { a:'firstname', b:'First Name', c:false, d:'user', t:'input', r:false }, { a:'lastname', b:'Last Name', c:false, d:'user', t:'input', r:false }, { a:'email', b:'Email', c:false, d:'envelope', t:'input', r:false }, { a:'phone', b:'Phone', c:false, d:'phone', t:'input', r:false }, { a:'address', b:'Address', c:false, d:'home', t:'input', r:false }, { a:'zip', b:'Zip Code', c:false, d:'globe', t:'input', r:false } ];   // { a:'currency', b:'Currency', c:false, d:'usd', t:'array', val: acurrency, func: $scope.nonefunc, r:false }
        //$scope.itemCreaContent = [ { a:'eventID', b:'Order number', c:false, d:'tags', t:'input', r:true, func: $scope.cleananinput }, { a:'itemID', b:'Item number', c:false, d:'list', t:'input', r:true, func: $scope.cleananinput }, { a:'status', b:'Status', c:false, d:'check', t:'input', r:true, func: $scope.cleananinput }, { a:'item_title', b:'Name', c:false, d:'tag', t:'input', r:false, func: $scope.cleananinput }, { a:'unit_price', b:'Price', c:false, d:'usd', t:'input', r:false, func: updateprice }, { a:'quantity', b:'Quantity', c:false, d:'align-justify', t:'array', val: aorder, func: updateprice, r:false } ];
        $scope.orderCreaContent = [{a: 'restaurant', b: 'Restaurant', c: false, d: 'cutlery', t: 'input', r: true}, {a: 'eventID', b: 'Event ID', c: false, d: 'tag', t: 'input', r: true}, {a: 'rdate', b: 'Date', c: false, d: 'calendar', t: 'date', r: false, u: $scope.mydata},{a: 'rtime', b: 'Time', c:'', d: 'calendar', t: 'input'}, {
            a: 'occasion',
            b: 'Occasion',
            c: false,
            d: 'birthday-cake',
            t: 'array',
            val: aoccasion,
            func: $scope.nonefunc,
            r: false
        }, {a: 'type', b: 'Type', c: false, d: 'user', t: 'array', val: atype, func: $scope.nonefunc, r: false}, {a: 'company', b: 'Company/Project', c: false, d: 'bank', t: 'input', r: false}, {
            a: 'title',
            b: 'Title',
            c: false,
            d: 'user',
            t: 'array',
            val: atitle,
            func: $scope.nonefunc,
            r: false
        }, {a: 'firstname', b: 'First Name', c: false, d: 'user', t: 'input', r: false}, {a: 'lastname', b: 'Last Name', c: false, d: 'user', t: 'input', r: false}, {a: 'email', b: 'Email', c: false, d: 'envelope', t: 'input', r: false},{a: 'pax', b: 'Pax', c: false, d: 'cutlery', t: 'input', r: false},{
            a: 'phone',
            b: 'Phone',
            c: false,
            d: 'phone',
            t: 'input',
            r: false
        }, {a: 'address', b: 'Address', c: false, d: 'home', t: 'input', r: false}, {a: 'zip', b: 'Zip Code', c: false, d: 'globe', t: 'input', r: false}, {a: 'status', b: 'Status', c: false, d: 'tag', t: 'array', val: astatus, r: false}];   // { a:'currency', b:'Currency', c:false, d:'usd', t:'array', val: acurrency, func: $scope.nonefunc, r:false }

        //$scope.tabletitleContent = [ { a:'name', b:'Name', c:'', d:'anchor', t:'input', i:0 }, { a:'title', b:'Title', c:'', d:'tag', t:'input', i:0 },{ a:'description', b:'Description', c:'', d:'list', t:'textarea' }, { a:'city', b:'Location', c:'', d:'map-marker', t:'input', i:0 }, { a:'price', b:'Price', c:'', d:'money', t:'inputprice', i:0 }, { a:'maxpax', b:'Max Pax', c:'', d:'male', t:'array', val:paxnumbers, i:0 }, {a:'start', b:'Starting Date', c:'date', d:'calendar', t:'date', u:$scope.mydata_start }, {a:'end', b:'Ending Date', c:'date', d:'calendar', t:'date', u:$scope.mydata_endng }, {a:'display', b:'Display Date', c:'date', d:'calendar', t:'date', u:$scope.mydata_display },{ a:'tnc', b:'Terms/Conditions', c:'', d:'legal', t:'textarea' },{ a:'type', b:'Type', c:'', d:'info', t:'array', val: $scope.atype, func: $scope.nonefunc },{ a:'menu', b:'Menus', c:'', d:'cutlery', t:'array', val: $scope.amenus, func: $scope.nonefunc },{ a:'morder', b:'Position', c:'', d:'sort', t:'array', val: aorder, func: $scope.nonefunc },{ a:'picture', b:'Select an image', c:'', d:'photo', t:'picture' } , { a:'picture', b:'Picture', c:'', d:'photo', t:'pictureshow' } ];

        $scope.itemListFlg = [{label: 'vegi', width: '16'}, {label: 'spicy1', width: '20'}, {label: 'spicy2', width: '20'}, {label: 'spicy3', width: '20'}, {label: 'halal', width: '16'}, {label: 'chef_reco', width: '16'}];

        $scope.glbtax = 0.05;
        $scope.minorder = 0;
        $scope.archivefilter = {status: '!archive'};

        $scope.setmytemplate = function (data) {
            var dd = "";
            data.forEach(function (item, index) {
                dd += "<md-option value='" + item + "'>" + item + "</md-option>"
            });
            $scope.mytemplate = "<div layout='row' style='width=300px'><span>select a category ?</span><md-select ng-model='drink' placeholder='Category'>" + dd + "</md-select></div>";
        };

        $scope.getmytemplate = function () {
            return $scope.mytemplate;
        };
        $scope.setmytemplate(['water', 'Juice', 'Milk', 'Wine', 'Mead']);
        $scope.cleaninput = cleaninput;

        $scope.showAlert = function (ev, text) {
            // Appending dialog to document.body to cover sidenav in docs app
            // Modal dialogs should fully cover application
            // to prevent interaction outside of dialog
            $mdDialog.show(
                $mdDialog.alert()
                //.parent(angular.element(document.querySelector('#evContainer')))
                    .clickOutsideToClose(true)
                    .title('')
                    .textContent(text)
                    .ariaLabel('Alert Dialog Demo')
                    .ok('Got it!')
                    .targetEvent(ev)
            );
        };

        $scope.showConfirm = function (ev, title, text, action) {
            // Appending dialog to document.body to cover sidenav in docs app
            var confirm = $mdDialog.confirm()
                .title(title)
                .textContent(text)
                .ariaLabel('')
                .targetEvent(ev)
                .ok('Please do it!')
                .cancel('No');

            $mdDialog.show(confirm).then(function () {
                if (action !== null)
                    action();
            }, function () {
                $scope.status = '';
            });
        };

        $scope.showPrompt = function (ev, title, text, action, extra) {
            // Appending dialog to document.body to cover sidenav in docs app
            var confirm = $mdDialog.prompt()
                .title(title)
                .textContent(text)
                .placeholder('')
                .ariaLabel('')
                //	  .initialValue('')
                .targetEvent(ev)
                .ok('Okay!')
                .cancel('Cancel');
            $mdDialog.show(confirm).then(function (result) {
                if (action !== null)
                    action(result, extra);
            }, function () {
                $scope.status = '';
            });
        };

        $scope.selectCategory = function (action, v, type) {
            $scope.aCategory = "";

            $mdDialog.show({
                clickOutsideToClose: true,
                scope: $scope,
                require: 'ngModel',
                preserveScope: true,
                template: "<md-card>" +
                "<md-card-title style='width:600px;margin:10px 10px 10px 20px'>" +
                "<md-card-title-text>" +
                "<span class='md-headline'>Select a Category</span>" +
                "</md-card-title-text>" +
                "</md-card-title>" +
                "<md-card-content>" +
                "<div layout='row'>" +
                "<md-input-container style='width:600px;margin:10px 10px 10px 20px'>" +
                "<label>Category</label>" +
                "<md-select ng-model='aCategory'>" +
                "<md-option ng-repeat='m in catergoryAr' ng-value='m'> {{ m }}</md-option>" +
                "</md-select>" +
                "<div class='row' style='margin-top:100px'></div>" +
                "</md-input-container>" +
                "</div>" +
                "</md-card-content>" +
                
                "<md-dialog-actions layout='row'><span flex></span>" +
                "<md-button ng-click='answer(0)'>Cancel</md-button>" +
//                "<md-button  class='md-primary md-raised' ng-click='promptpcategory($event, \'options\');'  >Add categorie</md-button>" +
                "<md-button class='md-primary md-raised' ng-click='answer(1)' style='margin-right:30px'>OK</md-button>" +
    "</md-dialog-actions>" +
                "</md-card>",
                controller: function DialogController($scope, $mdDialog) {
                    $scope.closeDialog = function () {
                        $mdDialog.hide();
                    };

                    $scope.answer = function (value) {
                        if (value === 1){
                            action($scope.aCategory, v);
                            $scope.pendingsave = true;
                        }
                        
                        $mdDialog.hide();
                    };
                }
            });
        };

        $scope.getAlignment = bkgetalignment;

        $scope.togglearchive = function () {
            $scope.archivefilter.status = ($scope.archivefilter.status !== '' && $scope.archivefilter.status === "archive") ? "!archive" : "archive";
            $scope.showhidearchive = ($scope.archivefilter.status === "archive") ? "Hide archive" : "Show archive";
        }

        $scope.menuItemFactory = function (eventid, menuid, itemid, displayid) {
            return {
                restaurant: $scope.restaurant,
                eventID: eventid,
                menuID: menuid,
                itemID: itemid,
                displayID: displayid,
                currency: $scope.currency,
                typeflag: 0,
                item_description: '',
                item_title: '',
                morder: 0,
                price: 0,
                status: 'new',
                type: 'item',
                menu_categorie_id: 0,
                mimage: '',
                spicy1: 0,
                spicy2: 0,
                spicy3: 0,
                vegi: 0,
                chef_reco: 0,
                halal: 0,
                takeout: 0,
                gblindex: function () {
                    for (var i = 0; i < $scope.submenus.length; i++)
                        if ($scope.submenus[i].menuID === this.menuID && $scope.submenus[i].itemID === this.itemID)
                            return i;
                    return -1;
                },
                clean: function () {
                    var data = Object.keys(this), oo = this;
                    data.forEach(function (item, index) {
                        if (typeof oo[item] === 'string')
                            oo[item] = oo[item].replace(/\'|\"/g, '’');
                    });
                    return oo;
                },
                transfer: function () {
                    var index = this.gblindex();
                    if (index < 0)
                        return null;
                    this.replicate($scope.submenus[index]);
                    return this;
                },
                replicate: function (obj) {
                    var data = Object.keys(this), oo = this, currency = this.currency;
                    data.forEach(function (item, index, arr) {
                        if (['displayID', 'itemID', 'menuID', 'eventID'].indexOf(item) < 0)
                            oo[item] = obj[item]
                    });
                    index = ['USD', 'SGD', 'EURO', 'EUR'].indexOf(this.currency);
                    this.cicon = (index >= 0) ? ['usd', 'usd', 'eur', 'eur'][index] : "";
                    return this;
                }
            };
        };

        $scope.menuFactory = function (eventid, menuid, menuitemid, value, morder, status) {
            if (typeof morder === "undefined")
                morder = 1;
            if (typeof value === "undefined")
                value = '';
            if (typeof status === "undefined")
                status = '';
            var currency = $scope.currency;
            var index = ['USD', 'SGD', 'EURO', 'EUR'].indexOf(currency);
            var cicon = (index >= 0) ? ['usd', 'usd', 'eur', 'eur'][index] : "";

            return {
                ID: 0,
                restaurant: $scope.restaurant,
                eventID: eventid,
                menuID: menuid,
                itemID: menuitemid,
                itemindex: 1,
                status: status,
                typeflag: 0,
                private: 0,
                drink: 0,
                morder: parseInt(morder),
                price: 0,
                cicon: cicon,
                currency: $scope.currency,
                type: 'categorie',
                value: value,
                description: '',
                gblindex: function () {
                    for (var i = 0; i < $scope.names.length; i++)
                        if ($scope.names[i].menuID === this.menuID)
                            return i;
                    return -1;
                },
                clean: function () {
                    var data = Object.keys(this), oo = this;
                    data.forEach(function (item, index) {
                        if (typeof oo[item] === 'string')
                            oo[item] = oo[item].replace(/\'|\"/g, '’');
                    });
                    return oo;
                },
                replicate: function (obj) {
                    var data = Object.keys(this), oo = this, currency = this.currency;
                    data.forEach(function (item, index) {
                        if (['itemID', 'menuID', 'eventID'].indexOf(item) < 0)
                            oo[item] = obj[item]
                    });
                    index = ['USD', 'SGD', 'EURO', 'EUR'].indexOf(currency);
                    this.cicon = (index >= 0) ? ['usd', 'usd', 'eur', 'eur'][index] : "";
                    return this;
                }
            };
        };

        $scope.orderingFactory = function (currency) {
            return {
                restaurant: '',
                restitle:'',
                eventID: Math.floor((Math.random() * 10000000) + 1),
                status: '',
                cdate: $scope.createdate,
                rdate: '',
                rtime :'',
                type: '',
                currency: (typeof currency !== "undefined") ? currency : "",
                company: '',
                title: '',
                firstname: '',
                lastname: '',
                email: '',
                phone: '',
                address: '',
                zip: '',
                datetime: 0,
                ddate: '',
                cdatetime: 0,
                cddate: '', // for search 
                updated_at: '',
                description: '',
                setup: '',
                tnc: '',
                tnc_cancel: '',
                billing: '',
                milestones: '',
                payment_method :'',
                more: '',
                occasion: '',
                type: '',
                menu: '',
                maxpax: 0,
                morder: 0,
                bkpax: 0,
                bkevent: [],
                vorder: 0,
                featured: false,
                picture: '',
                price: 0,
                pax: 0,
                total_amount :0,
                clean: function () {
                    var tmpAr = Object.keys(this), attr, oo = this;
                    tmpAr.forEach(function (attr) {
                        if (typeof oo[attr] === 'string')
                            oo[attr] = oo[attr].replace(/\'|\"/g, '’');
                    })
                    return this;
                },
                replicate: function (obj) {
                    var tmpAr = Object.keys(this), attr, oo = this;
                    tmpAr.forEach(function (attr) {
                        oo[attr] = obj[attr];
                    })
                    return this;
                }
            };
        };

        $scope.addpitem = function (category, v) {
            var i, oo;

            for (i = 0; i < $scope.pmenus.length; i++)
                if ($scope.pmenus[i].value === category)
                    break;
            if (i >= $scope.pmenus.length)
                return;

            oo = new $scope.menuItemFactory($scope.selectedItem.eventID, v.menuID, v.itemID, $scope.pmenus[i].menuID).replicate(v);
            oo.morder = ($scope.psubmenus.length < 1) ? 1 : Math.max.apply(Math, $scope.psubmenus.map(function (o) {
                return (o.displayID === v.displayID) ? o.morder + 1 : 1;
            }));
            $scope.psubmenus.push(oo);
        };


        $scope.addallitems = function (ev, u, type) {
            var category = u.value, menuID = u.menuID;
            if ($scope.addpcategory(u.value, type, u) > 0){
                $scope.submenus.map(function (oo, index, arr) {
                    if (oo.menuID === menuID){
                        $scope.addpitem(category, oo);
                    }
                });
                $scope.pendingsave = true;
            }
        };

        $scope.additem = function (ev, v, type) {

//            if ($scope.pmenus.length < 1)
//                return $scope.showAlert(ev, 'No Category to choose from...');

            $scope.catergoryAr = [];
            $scope.pmenus.map(function (oo) {
                if (oo.status === 'deleted')
                    return;
                if (type === 'food' && oo.drink !== 1)
                    $scope.catergoryAr.push(oo.value);
                else if (type === 'drink' && oo.drink === 1)
                    $scope.catergoryAr.push(oo.value);
                else if (type === 'options' && oo.options === 1)
                    $scope.catergoryAr.push(oo.value);
            });
            
            $scope.selectCategory($scope.addpitem, v, type);
            
        };

        $scope.addpcategory = function (category, type, u) {
            var oo, dd, index;

            if (typeof category !== "string" || category.length < 3)
                return -1;

            if ($scope.pmenus.inObject(category, "value") > -1) // duplicates
                return -1;

            dd = new Date().getTime()
            oo = new $scope.menuFactory($scope.selectedItem.eventID, dd, dd);
            oo.value = category;

            index = $scope.menutypeflag.indexOf(type);
            if (index >= 0) {
                oo.typeflag += (1 << index);
                oo[type] = 1;
            }
            if ($scope.pmenus.length < 1)
                oo.morder = 1;
            else
                oo.morder = Math.max.apply(Math, $scope.pmenus.map(function (o) {
                    return o.morder + 1;
                }));
            if (u && u.typeflag) {
                oo.typeflag = u.typeflag;
                oo.price = u.price;
                $scope.menutypeflag.map(function (val, index, arr) {
                    oo[val] = u[val];
                });
            }
            $scope.pmenus.push(oo);
            return 1;
        }

        $scope.delpcategory = function (category) {
            var i;

            for (i = 0; i < $scope.pmenus.length; i++)
                if ($scope.pmenus[i].value === category)
                    break;

            if (i >= $scope.pmenus.length)
                return;

            $scope.pmenus.splice(i, 1);
        }

        $scope.moveUp = function (u) {
            var start = u.morder - 1;

            if ($scope.pmenus.length < 2)
                return;

            if (start > 0) {
                $scope.pmenus.map(function (oo, index, arr) {
                    if (oo.morder >= start)
                        oo.morder++;
                });
                u.morder = start;
            } else {
                start = Math.max.apply(Math, $scope.pmenus.map(function (o) {
                    return o.morder;
                }));
                $scope.pmenus.map(function (oo, index, arr) {
                    if (oo.morder < 2)
                        oo.morder = 1;
                    else
                        oo.morder--;
                });
                u.morder = start;
            }
            $scope.pmenus.sort(function (a, b) {
                return a.morder - b.morder;
            });
        };

        $scope.IsEmail = function (email) {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return regex.test(email);
        }

        $scope.sendform = function (ev) {


            $scope.showPrompt(ev, "Write a valid email", "", function (result) {
                var tt = ($scope.IsEmail(result)) ? "Request form has been sent to " : "invalid email ";
                if ($scope.IsEmail(result)) {
                    bookService.sendForm($scope.restaurant, result, $scope.email).then(function (response) {
                    });
                }
                $scope.showAlert(ev, tt + result);
            });

        };


        $scope.getObjectData = function (oo, field, titleAr) {
      
            if (typeof oo[field] === "string" && oo[field].length > 5) {
                try {
                    oo[field] = JSON.parse(oo[field].replace(/’/g, "\""));
                } catch (e) {
                    console.log("JSON-MILESTONE", e.message);
                }
            } else {
                oo[field] = {};
    
                titleAr.map(function (m) {
                    if(field != 'payment_method'){
                        oo[field][m.a] = 0;
                    }
                });
            }
        };

        $scope.tagaction = function (x, ev, action) {
            if (action === 'rename')
                $scope.showPrompt(ev, "Please rename it", "", function (result) {
                    x.value = result;
                });
            else if (action === 'delete')
                $scope.showConfirm(ev, "Ok to proceed to deletion ?", "", function () {
                    x.status = 'deleted';
                    $scope.savepmenu(ev);
                });
            else if (action === 'moveup')
                $scope.moveUp(x);
        };

        $scope.promptpcategory = function (ev, type) { 
            var oo, category = $scope.showPrompt(ev, "Please enter a category", "", $scope.addpcategory, type);
        };

        $scope.deletepcategory = function (ev) {
            var oo, category = $scope.selectCategory($scope.delpcategory);
        };

        $scope.initorder = function () {
            $scope.tabletitle = $scope.bckups;
            $scope.predicate = "cdate";
            $scope.reverse = true;
        };

        $scope.reorder = function (item, alter) {
            alter = alter || "";
            if (alter !== "")
                item = alter;
            $scope.reverse = ($scope.predicate === item) ? !$scope.reverse : false;
            $scope.predicate = item;
        };
        $scope.export = function() {
            var maxlimit = 10000; // ajax call might not support more data
            var data, filename, cdate = new Date();
            var exportselect = $scope.filteredOrdering;
            var limit = exportselect.length, cdate = new Date();
            var field = [];
            if(limit < 1)
                return alert("no data");
            if (limit > maxlimit) 
                limit = maxlimit;
            exportselect = $scope.filteredOrdering.slice(0, limit);
            angular.forEach($scope.tabletitle, function(value, key, obj) {
                field.push(value['a']);
            });
            data = field.join(",");
            data += "\n";
            exportselect.map(function(oo, index) {
                field.map(function(ll) {
                    data += extractService.filter(oo[ll]) +  ",";
                });
                data += "\n";
            });
            filename = "EventOrdering" + cdate.getDate() + cdate.getMonth() + cdate.getFullYear() + ".csv";
            extractService.save(filename, data);
        };
        bookService.readMenuPicture($scope.restaurant, $scope.email).then(function (response) {
            var i, data = response.data.data;
            $scope.eventimages = [];
            for (i = 0; i < data.length; i++)
                if (data[i].object_type === 'event')
                    $scope.eventimages.push(data[i].name);

        });

        bookService.readRestaurant($scope.restaurant).then(function (response) {
            var data = response.data.data;
            $scope.glbtax = parseFloat(data.tkouttax);
            $scope.minorder = parseFloat(data.tkoutminorder);
            $scope.currency = data.currency;
            if (typeof $scope.currency !== "string" || $scope.currency === "")
                $scope.currency = 'SGD';
            //$scope.orderCreaContent.iconcurrency($scope.currency, "total", "a", "d");
        });

        bookService.readMenu($scope.restaurant, $scope.email).then(function (response) {
            var i, oo;
            if (response === null)
                return;

            $scope.menus = [];
            $scope.submenus = [];
            $scope.data = response.data.menus;
            $scope.subdata = response.data.menus_items;



            for (i = 0; i < $scope.data.length; i++) {
                oo = new $scope.menuFactory(0, $scope.data[i].menuID, 0).replicate($scope.data[i]);
                oo.index = i + 1;
                oo.typeflag = parseInt(oo.typeflag);
                oo.price = parseInt(oo.price);
                if (isNaN(oo.price))
                    oo.price = 0;
                $scope.menutypeflag.map(function (val, index, arr) {
                    oo[val] = (oo.typeflag & (1 << index)) ? 1 : 0;
                });
                oo.morder = parseInt(oo.morder);
                $scope.menus.push(oo);
            }
            if ($scope.menus.length > 1)
                $scope.menus.sort(function (a, b) {
                    return a.morder - b.morder;
                });

            for (i = 0; i < $scope.subdata.length; i++) {
                oo = new $scope.menuItemFactory(0, $scope.subdata[i].menuID, $scope.subdata[i].itemID, 0).replicate($scope.subdata[i]);
                oo.index = i + 1;
                oo.morder = parseInt(oo.morder);
                oo.price = parseInt(oo.price);
                if (isNaN(oo.price))
                    oo.price = 0;
                $scope.submenus.push(oo);
            }
            if ($scope.menus.length > 1)
                $scope.menus.sort(function (a, b) {
                    return a.morder - b.morder;
                });
            if ($scope.submenus.length > 1)
                $scope.submenus.sort(function (a, b) {
                    return (a.menuID === b.menuID) ? (a.morder - b.morder) : (a.menuID - b.menuID);
                });
            $scope.setContentViaService();
        });

        $scope.addmultiple = function (oo, prop) {
            if (prop in $scope.multiplemodel && oo.more[prop] instanceof Array)
                oo.more[prop].push(JSON.parse(JSON.stringify($scope.multiplemodel[prop])));	// duplicate object
            else
                console.log('ERROR', prop);
        };

        $scope.clearmultiple = function (oo, index, prop) {
            if (index < oo.more[prop].length)
                oo.more[prop].splice(index, 1);
        };

        $scope.readEventOrder = function(type){
                bookService.readEvOrdering($scope.restaurant, $scope.email,type).then(function (response) {
                var i, oo, content;
                if (response === null)
                    return;
                $scope.data = response.data;
                    $scope.names =[];

                for (i = 0; i < $scope.data.length; i++) {
                    oo = new $scope.orderingFactory().replicate($scope.data[i]);
                    oo.index = i + 1;
                    if (oo.firstname === "")
                        oo.firstname = 'John';
                    if (oo.lastname === "")
                        oo.lastname = 'Smith';
                    oo.fullname = oo.title + ' ' + oo.firstname + ' ' + oo.lastname;
                    oo.datetime = oo.rdate.jsdate().getTime();
                    oo.ddate = oo.rdate.jsdate().getDateFormat('-');
                    oo.cddate = oo.cdate.jsdate().getDateFormat('-');
                    oo.updatedate = oo.updated_at;
                    oo.chckflgconfirm = $scope.checkwarning(oo.ddate, oo.status);

                    //oo.cdatetime = oo.cdate.jsdate().getTime();
                    $scope.getObjectData(oo, "milestones", $scope.milestonetitle);
                    $scope.getObjectData(oo, "payment_method", $scope.payment_method);

                    $scope.getObjectData(oo, "more", $scope.moretitle);
                    $scope.multipledata.map(function (ll) {
                        if (!oo.more[ll] || oo.more[ll] instanceof Array === false)
                            oo.more[ll] = [];
                        else
                            oo.more[ll].map(function (pp) {
                                pp.name = pp.name.replace(/\|\|\|/g, '\n')
                            });
                        $scope.addmultiple(oo, ll);
                    });
                    oo.minimumprice = (oo.more.minimumprice > 0) ? true : false;
                    oo.warningmsg = $scope.warningmsg(oo.status, oo.updatedate, oo.more.leadtime);
                    $scope.names.push(oo);
                    $scope.insertcalendar(oo.rdate, oo.firstname, oo.lastname);
                }

                $scope.paginator.setItemCount($scope.names.length);
                $scope.initorder();
            });
        };
        $scope.readEventOrder('single');

        $scope.checkwarning = function (edate, status) {
            var uu = edate.split('-');
            var date = uu[2] + "-" + uu[1] + "-" + uu[0]

            if (status === 'confirmed' || status === 'request_send')
                return false;
            return (getDiffDay(date, 0, 0) >= 0 && getDiffDay(date, 0, 0) < 5);
        };

        $scope.showall = function(){
          //$scope.isSwitchFlg = (x ==='multiple') ? (a.morder - b.morder) : (a.menuID - b.menuID);
            $scope.readEventOrder('multiple');
        };

        
        $scope.warningmsg = function (status, udate, leadtime) {
            

            if(status === 'confirmed'){
                return "";
            }
 
            var position = 1;
            $scope.milestonetitle.map(function (mt, index) {
                if (status === mt.c) {
                    position = index + 1;
                }
            });

            if (position === 5)
                return "";
            
            var duration = ((leadtime != undefined) ? parseInt(leadtime) : 0),
                addDate = new Date(udate);
        
            var  diffDays= Math.round(new Date().getTime()- addDate.getTime()) / (24 * 3600 * 1000);
            //console.log("SDASDSADS"+ diffDays  + "duration " +duration);  //.jsdate().getDateFormat('-');
            return ((diffDays >= duration) ? $scope.milestonetitle[position].b : '');

        }

        $scope.buildtask = function (value) {
            $scope.buildmenusflg = false;
            $scope.builddrinkflg = false;
            $scope.buildoptionsflg = false;
            $scope.buildtncsflg = false;
            $scope.milestonesflg = false;
            $scope[value] = true;
            $scope.reset('detailsOrderingFlag');
        };

        $scope.buildmenus = function () {
            $scope.buildtask('buildmenusflg');
        };
        $scope.builddrink = function () {
            $scope.buildtask('builddrinkflg');
        };
        $scope.buildoptions = function () {
            $scope.buildtask('buildoptionsflg');
        };
        $scope.buildtncs = function () {
            $scope.buildtask('buildtncsflg');
        };
        $scope.editmilestones = function () {
            var  position = $scope.selectedItem.milestonePos;
             $scope.milestonetitle.map(function(mt, index) {
                    if(index <= position){
                        $scope.selectedItem.milestones[mt.a] = true;
                    }else{
                        $scope.selectedItem.milestones[mt.a] = false; 
                    }
            });
            $scope.buildtask('milestonesflg');
        };
        $scope.showcalendar = function () {
            $scope.reset('calendarflg');
        };

        $scope.reset = function (item) {
            $scope.calendarflg = false;
            $scope.listingOrderingFlag = false;
            $scope.detailsOrderingFlag = false
            $scope.createOrderingFlag = false;
            $scope[item] = true;
        };

        $scope.switchStatus = function (item, val) {
            if (val === false) {
                var position = 0;
                $scope.milestonetitle.map(function (mt, index) {
                    if (item === mt.c) {
                        position = index - 1;
                    }
                });

                $scope.mstStatus = $scope.milestonetitle[position].c;
            } else {
                $scope.mstStatus = item;
            }
            $scope.selectedItem.status = $scope.mstStatus;
            bookService.updateOrderstatus($scope.restaurant, $scope.selectedItem.eventID, $scope.mstStatus, $scope.email).then(function (response) {
            });
        };
        $scope.evnotify = function(ev){
            //if($scope.notifydetails === true){
          
            $scope.selectedItem.restaurant = $scope.restaurant;
            $scope.selectedItem.notify_type = 'event_management';
            bookService.notifysavemenu($scope.selectedItem).then(function (result) {
                $scope.showAlert(ev, "Notification has been sent");
            });
            //}
     
        };

        $scope.view = function (x) {
            $scope.selectedItem = x;
            $scope.restaurant = x.restaurant;
            var position = 0;
            $scope.milestonetitle.map(function (mt, index) {
                if (x.status === mt.c) {
                    position = index;
                }
            });
            $scope.selectedItem.milestonePos = position;
            
            if(typeof $scope.selectedItem.more.deposit === 'undefined' || $scope.selectedItem.more.deposit === 0 ){
                $scope.selectedItem.more.deposit = '50%';
            }
             if(typeof $scope.selectedItem.more.privatenote === 'undefined' || $scope.selectedItem.more.privatenote === 0 ){
                $scope.selectedItem.more.privatenote = '';
            }

            bookService.readEvOrderingMenu($scope.restaurant, $scope.email, $scope.selectedItem.eventID).then(function (response) {
                $scope.pmenus = [];
                $scope.psubmenus = [];
                if (response === null || response.status !== 1)
                    return;
                response.data.map(function (pp, index, arr) {
                    var oo;
                    if (pp.menuID === pp.menuItemID && pp.notes !== "") {
                        oo = new $scope.menuFactory(pp.eventID, pp.menuID, pp.menuItemID, pp.notes, pp.morder, pp.status);
                        oo.typeflag = parseInt(pp.typeflag);
                        oo.drink = oo.private = 0;
                        $scope.menutypeflag.map(function (val, index, arr) {
                            if (oo.typeflag & (1 << index))
                                oo[val] = 1;
                        });
                        $scope.pmenus.push(oo);
                    } else {
                        oo = new $scope.menuItemFactory(pp.eventID, pp.menuID, pp.menuItemID, pp.displayID).transfer();
                        if (oo === null)
                            return;
                        oo.status = pp.status;
                        oo.typeflag = parseInt(pp.typeflag);
                        oo.drink = oo.private = 0;
                        $scope.menutypeflag.map(function (val, index, arr) {
                            if (oo.typeflag & (1 << index))
                                oo[val] = 1;
                        });
                        $scope.psubmenus.push(oo);
                    }
                });
                if ($scope.pmenus.length > 1)
                    $scope.pmenus.sort(function (a, b) {
                        return a.morder - b.morder;
                    });
                if ($scope.psubmenus.length > 1)
                    $scope.psubmenus.sort(function (a, b) {
                        return (a.displayID === b.displayID) ? (a.morder - b.morder) : (a.displayID - b.displayID);
                    });
            });

            $scope.reset('detailsOrderingFlag');
        };

        $scope.cleandata = function (ll) {
            var oo = $scope.selectedItem;
            oo[ll] = $scope.cleaninput(oo[ll], 'text');
        };

        $scope.backlisting = function () {
            var a = $scope.selectedItem.rdate.split("-");
            console.log("SE  " + $scope.selectedItem.rdate);
            $scope.selectedItem.ddate = $scope.selectedItem.rdate;
            if($scope.selectedItem.rdate  && a[2].length === 4) {
                $scope.selectedItem.rdate = a[2]+ "-"+ a[1] +"-" + a[0];
            }
            $scope.buildmenusflg = false;
            $scope.builddrinkflg = false;
            $scope.buildoptionsflg = false;
            $scope.buildtncsflg = false;
            $scope.milestonesflg = false;
            $scope.reset('listingOrderingFlag');
        };

        $scope.create = function () {
            $scope.selectedItem = new $scope.orderingFactory($scope.currency);
            $scope.buttonlabel = "Save new ordering";
            $scope.action = "create";
            $scope.reset('createOrderingFlag');
            return false;
        };

        $scope.displayupdate = function (ev, oo) {
            $scope.selectedItem = oo;
            $scope.restaurant = oo.restaurant;
            $scope.selectedItem.rdate = oo.ddate; 
            $scope.mydata.originaldate = $scope.selectedItem.rdate;
   
            $scope.buttonlabel = "Update";
            $scope.action = "update";
            $scope.reset('createOrderingFlag');
            return false;
        };

        $scope.savepmenu = function (ev) {
            var dd, status, obj = [], todel = [], uu = $scope.selectedItem;


            uu.setup = $scope.cleaninput(uu.setup, 'text');
            uu.tnc = $scope.cleaninput(uu.tnc, 'text');
            uu.tnc_cancel = $scope.cleaninput(uu.tnc_cancel, 'text');
            uu.billing = $scope.cleaninput(uu.billing, 'text');

            $scope.pmenus.map(function (oo, index, arr) {
                if (oo.status === "new")
                    oo.status = "";
                dd = {restaurant: oo.restaurant, eventID: oo.eventID, menuID: oo.menuID, menuItemID: oo.itemID, displayID: '', morder: oo.morder, notes: oo.value, typeflag: oo.typeflag, status: oo.status};
                if (oo.status === "deleted")
                    todel.push(oo.menuID);
                obj.push(dd);
            });

            $scope.psubmenus.map(function (oo, index, arr) {
                if (oo.status === "new")
                    oo.status = "";
                status = (todel.indexOf(oo.menuID) < 0) ? oo.status : "deleted";
                dd = {restaurant: oo.restaurant, eventID: oo.eventID, menuID: oo.menuID, menuItemID: oo.itemID, displayID: oo.displayID, morder: oo.morder, typeflag: oo.typeflag, notes: "", status: status};
                obj.push(dd);
            });

            bookService.updateEvOrderingMenu($scope.restaurant, $scope.email, {menus: obj, eventID: $scope.selectedItem.eventID})
                .then(function (response) {
                    $scope.pendingsave = false;
                    var id = $scope.selectedItem.eventID
                    if (response.status < 1)
                        $scope.showAlert(ev, "Menus for order " + id + " have NOT been created/updated. " + response.data.msg);                         
//                    
//                    if (response.status > 0)
//                        $scope.showAlert(ev, "Menus for order " + id + " have been created/updated");
//                    else
//                        $scope.showAlert(ev, "Menus for order " + id + " have NOT been created/updated. " + response.data.msg);
                });
        };

        $scope.updatesvr = function (ev, oo) {
            var milestones,payment_method, morestr, obj = {};
            
            $scope.moretitle.map(function (v) {
                var ll = v.a;
                oo.more[ll] = obj[ll] = $scope.cleaninput(oo.more[ll], 'text');
            });
            $scope.multipledata.map(function (ll) {
                var i, vv = oo.more[ll], newAr = [];
                vv.map(function (uu, index, arr) {
                    Object.keys(uu).map(function (mm) {
                        uu[mm] = $scope.cleaninput(uu[mm], 'text');
                    });
                    if (uu.name && typeof uu.name === "string" && uu.name.length > 0) {
                        newAr.push(uu);
                    }
                });
                newAr.map(function (pp) {
                    pp.name = pp.name.replace(/\r|\n/g, '|||')
                });
                obj[ll] = newAr;
            });

            if(obj.deposit === 'no deposit'){
                oo.payment_method = {'nodeposit': true};
            }else{
                oo.payment_method.nodeposit = false;
            }
            milestones = JSON.stringify(oo.milestones).replace(/\'|\"/g, "’");
            payment_method = JSON.stringify(oo.payment_method).replace(/\'|\"/g, "’");
            morestr = JSON.stringify(obj).replace(/\'|\"/g, "’");

            bookService.updateEvOrdering($scope.restaurant, $scope.email, {orders: oo, setup: oo.setup, tnc: oo.tnc, tnc_cancel: oo.tnc_cancel, billing: oo.billing, milestones: milestones, more: morestr,payment_method: payment_method})
                .then(function (response) {
                    $scope.showAlert(ev, "Order " + oo.eventID + " has been updated");
                    oo.serveraction = "";
                });
        };

        $scope.write = function (ev, mode) {
            var oo = $scope.selectedItem;
            oo.fullname = oo.title + ' ' + oo.firstname + ' ' + oo.lastname;
             var a = oo.rdate.split("-");

            if(a[2].length === 4) {
                oo.rdate = a[2]+ "-"+ a[1] +"-" + a[0];
            }
            oo.datetime = oo.rdate.jsdate().getTime();
            oo.ddate = oo.rdate.jsdate().getDateFormat('-');
            oo.ddate =  oo.rdate;
            //oo.cdatetime = oo.cdate.jsdate().getTime('-');
            oo.cddate = oo.cdate.jsdate().getDateFormat('-');
            oo.serveraction = "";
            console.log("SDSDSd" +oo.ddate + "SDSDS" + oo.rdate );

            if (mode === "create") {
                bookService.createEvOrdering($scope.restaurant, $scope.email, {orders: oo})
                    .then(function (response) {
                        if (response.status < 0)
                            return $scope.showAlert(ev, "Order " + oo.eventID + " has NOT been created. " + response.data.msg);
                        $scope.names.push(oo);
                        $scope.showAlert(ev, "Order " + oo.eventID + " has been created");
                    });
                     $scope.backlisting();
            } else {
                if (mode === "cancel") {
                    $scope.showConfirm(ev, "Are you sure you want to cancel " + oo.eventID, "", function () {
                        oo.status = "cancel";
                        oo.serveraction = "do_cancel";
                        $scope.updatesvr(ev, oo);
                    });
                     $scope.backlisting();
                } else
                    $scope.updatesvr(ev, oo);
            }

           
        };

        $scope.archivemaster = function (ev, oo) {
            oo.status = oo.status + "|archive" || "archive";
            bookService.updateEvOrdering($scope.restaurant, $scope.email, {orders: oo})
                .then(function (response) {
                    $scope.showAlert(ev, "Order " + oo.eventID + " has been archived");
                });
        }
        $scope.unarchivemaster = function (ev, oo) {
          
            if(oo.status){
                var result = oo.status.split('|'); 
                oo.status = result[0] || "unarchive";
            }
            bookService.updateEvOrdering($scope.restaurant, $scope.email, {orders: oo})
                .then(function (response) {
                    $scope.showAlert(ev, "Order " + oo.eventID + " has been archived");
                });
        }

        $scope.deleteevent = function (ev, oo) {
            if (confirm("Are you sure you want to delete " + oo.eventID) === false)
                return;

            bookService.deleteEvOrdering($scope.restaurant, $scope.email, oo.eventID).then(function () {
                var i, ind = $scope.names.inObject('eventID', oo.eventID);

                if (ind > -1)
                    $scope.names.splice(ind, 1);
                $scope.showAlert(ev, oo.eventID + " has been deleted");
            });
        };

        $scope.notify = function (ev, x) {
            x.restaurant = $scope.restaurant;
            x.notify_type = 'event_menu_selection_req';
            bookService.notifysavemenu(x).then(function (result) {
                $scope.showAlert(ev, "Notification has been sent");
            });

        };

        $scope.originaldate = new Date();
        $scope.weekStartsOn = 0;
        $scope.dayFormat = "d";
        $scope.tooltips = true;
        $scope.disableFutureDates = false;


        $scope.setDirection = function (direction) {
            $scope.direction = direction;
            $scope.dayFormat = direction === "vertical" ? "EEEE, MMMM d" : "d";
        };

        $scope.dayClick = function (date) {
            $scope.msg = "You clicked " + $filter("date")(date, "MMM d, y h:mm:ss a Z");
        };

        $scope.prevMonth = function (data) {
            $scope.msg = "You clicked (prev) month " + data.month + ", " + data.year;
        };

        $scope.nextMonth = function (data) {
            $scope.msg = "You clicked (next) month " + data.month + ", " + data.year;
        };

        $scope.setContentViaService = function () {
            var today = new Date();
            MaterialCalendarData.setDayContent(today, '<span> Today </span>')
        }

        var eventdates = {}; // {"2015-01-01":[ {"name":"Last Day of Kwanzaa","country":"US","date":"2015-01-01"}, {"name":"New Year's Day","country":"US","date":"2015-01-01"}] };
        $scope.insertcalendar = function (dd, ff, ll) {
            var nn = ff + '-' + ll;
            var oo = {"name": nn, "date": dd};
            var adte = dd.jsdate();
            MaterialCalendarData.setDayContent(adte, nn)
            if (typeof eventdates[dd] === "undefined") {
                eventdates[dd] = [oo];
            } else
                eventdates[dd].push(oo);

        };
        // You would inject any HTML you wanted for
        // that particular date here.
        var numFmt = function (num) {
            num = num.toString();
            if (num.length < 2) {
                num = "0" + num;
            }
            return num;
        };

        var loadContentAsync = true;
        console.info("setDayContent.async", loadContentAsync);
        $scope.setDayContent = function (date) {

            var key = [date.getFullYear(), numFmt(date.getMonth() + 1), numFmt(date.getDate())].join("-");
            var data = (eventdates[key] || [{name: ""}])[0].name;
            if (loadContentAsync) {
                var deferred = $q.defer();
                $timeout(function () {
                    deferred.resolve(data);
                });
                return deferred.promise;
            }

            return data;

        };

        $scope.showproject = function () {
            var url = "/modules/event_management/staff.php?event_id=" + $scope.selectedItem.eventID;
            //var url = "/restaurant/singapore/" + $scope.restoUrl + "/event-management/staff/" + $scope.selectedItem.eventID + "?dspl_f=f&dspl_h=f";
            var eventwin = window.open(url, "event", "toolbar=yes,location=no,directories=yes,menubar=yes,scrollbars=yes,width=700, height=600, left=100, top=25");
        };

        function cleaninput(a, type) {
            switch (type) {
                case 'text':
                    return (typeof a === "string" && a.length > 0) ? a.replace(/\'|\"/g, '’') : "";
                case 'currency':
                    return (typeof a === "string" && a.length > 0) ? a.replace(/[^$\d]/g, '') : "";
                case 'number':
                    return (typeof a === "string" && a.length > 0) ? a.replace(/[^\d]/g, '') : "";
            }
            return "";
        }


    }]).config(function ($mdThemingProvider) {
        $mdThemingProvider.theme('dark-pink').primaryPalette('yellow').backgroundPalette('pink');
        $mdThemingProvider.theme('dark-orange').backgroundPalette('orange').dark();
        $mdThemingProvider.theme('dark-red').primaryPalette('red').dark();
        $mdThemingProvider.theme('dark-blue').backgroundPalette('blue').dark();

        $mdThemingProvider.theme('customTheme')
            .primaryPalette('blue')
            .accentPalette('orange')
            //	  .backgroundPalette('blue')
            .warnPalette('purple');
        $mdThemingProvider.theme('default')
            .primaryPalette('purple', {
                'default': '400', // by default use shade 400 from the pink palette for primary intentions
                'hue-1': '100', // use shade 100 for the <code>md-hue-1</code> class
                'hue-2': '600', // use shade 600 for the <code>md-hue-2</code> class
                'hue-3': 'A100' // use shade A100 for the <code>md-hue-3</code> class
            })
            // If you specify less than all of the keys, it will inherit from the
            // default shades
            .accentPalette('blue', {
                'default': '200' // use shade 200 for default, and keep all other shades the same
            });

        $mdThemingProvider.definePalette('amazingPaletteName', {
            '50': 'ffebee',
            '100': 'ffcdd2',
            '200': 'ef9a9a',
            '300': 'e57373',
            '400': 'ef5350',
            '500': 'f44336',
            '600': 'e53935',
            '700': 'd32f2f',
            '800': 'c62828',
            '900': 'b71c1c',
            'A100': 'ff8a80',
            'A200': 'ff5252',
            'A400': 'ff1744',
            'A700': 'd50000',
            'contrastDefaultColor': 'light', // whether, by default, text (contrast)
            // on this palette should be dark or light
            'contrastDarkColors': ['50', '100', //hues which contrast should be 'dark' by default
                '200', '300', '400', 'A100'],
            'contrastLightColors': undefined    // could also specify this if default was 'dark'
        });
        $mdThemingProvider.theme('testing')
            .primaryPalette('amazingPaletteName');

    }).config(function ($mdIconProvider) {
        $mdIconProvider
            .iconSet('social', '../client/bower_components/material-design-icons/iconsets/social-icons.svg', 24)
            //.defaultIconSet('img/icons/sets/core-icons.svg', 24)
            .fontSet('md', 'material-icons');
    }).config(function ($mdThemingProvider) {
        $mdThemingProvider
            .theme("default")
            .primaryPalette("cyan")
            .accentPalette("light-green");
    });

    app.filter('keyboardShortcut', function ($window) {
        return function (str) {
            if (!str)
                return;
            var keys = str.split('-');
            var isOSX = /Mac OS X/.test($window.navigator.userAgent);
            var seperator = (!isOSX || keys.length > 2) ? '+' : '';
            var abbreviations = {
                M: isOSX ? '⌘' : 'Ctrl',
                A: isOSX ? 'Option' : 'Alt',
                S: 'Shift'
            };
            return keys.map(function (key, index) {
                var last = index == keys.length - 1;
                return last ? key : abbreviations[key];
            }).join(seperator);
        };
    });