<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$login = new WY_Login(LOGIN_BACKOFFICE);
$email = $_SESSION['user_backoffice']['email'];

$authentication = $login->getGooleCredentials();
$qrCodeUrl = '';

$secret = $authentication['secret_key'];

if (count($authentication) > 0) {
    $qrCodeUrl = $authentication['qrcode'];
}
?>

<div class="container">
    <div class="row">
        <div  ng-controller="AuthService" >
            <div id="authentication"  >
               <div class="form-group"  style='margin-bottom:25px;'>
                   <img ng-src='{{qrcode}}'>
                   <div>
                   <a href ng-show ='qrcode' class='btn btn-success btn-sm' ng-click="setauthflg()" style='color:white; width:150px;float:left;margin-right:30px;'>Set Authentication</a>
                    <a href ng-show ='qrcode' class='btn btn-warning btn-sm' ng-click="resetauthflg()" style='color:white; width:150px;float:left;'>Reset Authentication</a>
                   </div>
                   
                  
                </div>

            </div>
        </div>
    </div>
</div>



<script>

    app.controller('AuthService', ['$scope', '$http', 'bookService', function ($scope, $http, bookService) {
        var qrcode ='<?php echo $qrCodeUrl ?>';
        var email = "<?php echo $email ?>";
        $scope.qrcode = qrcode; 
        $scope.setauthflg = function() {
            $http({
                    url: "../api/backoffice/settwofactauth",
                    method: "POST",
                    data: $.param({
                         email: email,
                    }),
			headers:  {'Content-Type': 'application/x-www-form-urlencoded'}
                    }).then(function(response) {
                        if(response.data.status === 1){
                            alert("Successfully Switeched two Fact authentication");
                        }
                    });
        }
        $scope.resetauthflg = function() {
            $http({
                    url: "../api/backoffice/resettwofactauth",
                    method: "POST",
                    data: $.param({
                         email: email,
                    }),
			headers:  {'Content-Type': 'application/x-www-form-urlencoded'}
                    }).then(function(response) {
                        if(response.data.status === 1){
                            alert("Successfully Reset two Fact authentication");
                        }
                    });
        }


   }]);
</script>


