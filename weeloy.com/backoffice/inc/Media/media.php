<div class="container">
    <div class="row">
        <div class="col-md-12 left-sec">

<div ng-controller="ImageController" ng-init="moduleName='image'; listingImageFlag = true; listing1ImageFlag=false; updateImageFlag=false; renameImageFlag=false; $scope.createImageFlag = false;" >
	<input type='hidden' id='token' value ="<?php echo $_SESSION['user_backoffice']['token'] ?>" />
	<input type='hidden' id='restaurant' value ="<?php echo $theRestaurant ?>" />
	<input type='hidden' id='email' value ="<?php echo $_SESSION['user_backoffice']['email'] ?>" />
	<div id='listing' ng-show='listingImageFlag'>
		<div class="form-group"  style='margin-bottom:25px;'>
			<div class="col-md-4">
				<div class="input-group col-md-4">
					<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
					<input type="text" class="form-control input-sm" ng-model="searchText" ng-change="paginator.setPage(0);" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" style='width:200px;'> 
				</div>
			</div>
			<div class="col-md-2"></div>

			<div class="col-md-2">
				<div class="btn-group" uib-dropdown>
				<button type="button" class="btn btn-default btn-xs" uib-dropdown-toggle aria-expanded="false" style='font-size:11px;'>Page Size<span class="caret"></span></button>
				<ul class="dropdown-menu" uib-dropdown-menu role="menu">
				<li ng-repeat="x in paginator.pagerange()" class='glyphiconsize'><a href ng-class="{ 'glyphicon glyphicon-ok glyphiconsize': x === paginator.getRowperPage() }" ng-click="paginator.setRowperPage(x)"> {{x}}</a></li>
				</ul>
				</div> 		
			</div>

			<div class="col-md-4">
			<a href ng-click='create();' class="btn btn-success btn-sm" style='color:white;width:200px;'><span class='glyphicon glyphicon-certificate'></span> &nbsp;Create a New Image</a>
			</div>
		</div><br/>

		<table width='100%' class="table table-condensed table-striped" style='font-family:helvetica;font-size:12px;'>
			<thead>
				<tr>
				<th ng-repeat="y in tabletitle"><tbtitle var="{{y.a}}" name='tabletitle'  module='moduleName'/></th>
				<th>rename</th><th> &nbsp; </th><th>update</th><th> &nbsp; </th><th>delete</th>
				</tr>
			 </thead>
			<tr ng-repeat="x in filteredImage = (names | filter:searchText) | sizefilter:this | orderBy:predicate:reverse | slicefilter:paginator.getPage():paginator.getRowperPage():paginator.getItemCount() | limitTo:paginator.getRowperPage()" ng-if="formatDisplay!='today'" style='font-family:helvetica;font-size:11px;'>
				<td ng-repeat="y in tabletitle"><a href ng-click="view(x)">{{ x[y.a] }}</a></td>
				<td align='center'><a href ng-click="rename(x)"><span class='glyphicon glyphicon-tag purple'></span></a></td>
				<td width='30'>&nbsp;</td>
				<td align='center'><a href ng-click="update(x)"><span class='glyphicon glyphicon-pencil blue'></span></a></td>
				<td width='30'>&nbsp;</td>
				<td align='center'><a href ng-click="delete(x)"><span class='glyphicon glyphicon-trash red'></span></a></td>
			</tr>
			<tr><td colspan='{{ tabletitle.length + 5}}'></td></tr>
		</table>
		<div ng-if="filteredImage.length >= paginator.getRowperPage()" class="col-md-12" align="center" style='font-size:11px' ng-include="'inc/paginator.html'"></div>
	</div>

	<div class="col-md-12" ng-show='listing1ImageFlag'>
		<br />
		<a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a>
		<br />
		<table class='table-striped' style="margin: 0 0 150px 30px;font-size:13px;font-family: Roboto">		
			<tr ng-repeat="y in tableContent" ><td><strong> {{ y.a }}: </strong></td><td> &nbsp; </td><td>{{ selectedItem[y.a] }}</td></tr>
			<tr ng-if="selectedItem.picture != ''"><td colspan='3' align='right'><br/><br/><img ng-src="{{path}}{{selectedItem.name}}" height='200'/></td></tr>
		</table>
	</div>

	<div class="col-md-12" ng-show='renameImageFlag'>
		<br />
		<a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a>
		<br />
		 <div class="col-md-2"></div>
		 <div class="col-md-8">
				<div class="input-group">
					<span class="input-group-addon"><i class="glyphicon glyphicon-tag"></i></span>
					<input type="text" class="form-control input-sm" ng-model="selectedItem.name" placeholder="current name of the media" readonly > 
				</div><br />

				<div class="input-group">
					<span class="input-group-addon"><i class="glyphicon glyphicon-tag"></i></span>
					<input type="text" class="form-control input-sm" ng-model="selectedItem.newname" placeholder="new name of the media"> 
				</div><br />
				<div ng-if="selectedItem.picture != ''" class="row"><img ng-src="{{path}}{{selectedItem.name}}" height='200'/></div><br /><br />							
				<div class="col-md-4">
				<a href ng-click='renamemedia();' class="btn btn-success btn-sm" style='color:white;width:200px;'><span class='glyphicon glyphicon-save'></span> &nbsp;{{ buttonlabel }} </a><br /><br /><br /><br />
				</div>
		</div>
		 <div class="col-md-2"></div>
	</div>

	<div class="col-md-12" ng-show='createImageFlag'>
		<br />
		<a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a>
		<br />
		 <div class="col-md-2"></div>
		 <div class="col-md-8">
			<div class='input-group'>
			<div class='input-group-btn' uib-dropdown >
			<button type='button' class='btn btn-info btn-sm' uib-dropdown-toggle >Category <span class='caret'></span></button>
			<ul class='dropdown-menu' uib-dropdown-menu role="menu" aria-labelledby="single-button">
			<li ng-repeat="p in objectTypelist"><a href ng-click="selectedItem.object_type=p;">{{ p }}</a></li>
			</ul></div><input type='text' class='form-control input-sm' ng-model="selectedItem.object_type" required>
			</div><br />

			<div class='input-group'>
			<div class='input-group-btn' uib-dropdown >
			<button type='button' class='btn btn-info btn-sm' uib-dropdown-toggle >Tag <span class='caret'></span></button>
			<ul class='dropdown-menu' uib-dropdown-menu role="menu" aria-labelledby="single-button">
			<li ng-repeat="p in tags"><a href ng-click="selectedItem.tag=p;">{{ p }}</a></li>
			</ul></div><input type='text' class='form-control input-sm' ng-model="selectedItem.tag" required>
			</div><br />

			<div class="input-group">
				<span class="input-group-addon"><i class="glyphicon glyphicon-file"></i></span>
				<input type="file" name="file_upload" id="file_upload" class="form-control input-sm" ng-model="nfiles" placeholder="ipload files">  
			</div><br />
                        <div id="progressbox" style="display:none;" ><div id="progressbar" ></div><div id="statustxt">0%</div> </div><br/>
			
			<div class="col-md-4">
			<a href  ng-click='uploadFiles();' class="btn btn-success btn-sm" style='color:white;width:200px;'><span class='glyphicon glyphicon-save'></span> &nbsp;{{ buttonlabel }} </a><br /><br /><br /><br />
			</div>
                        

		</div>
		 <div class="col-md-2"></div>
	</div>

	<div class="col-md-12" ng-show='updateImageFlag'>
		<br />
		<a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a>
		<br />
		 <div class="col-md-2"></div>
		 <div class="col-md-8">

				<div class="input-group">
					<span class="input-group-addon"><i class="glyphicon glyphicon-tag"></i></span>
					<input type="text" class="form-control input-sm" ng-model="selectedItem.name" placeholder="name of the media" readonly required> 
				</div><br />

				<div class='input-group'>
				<div class='input-group-btn' uib-dropdown >
				<button type='button' class='btn btn-info btn-sm' uib-dropdown-toggle >Category <span class='caret'></span></button>
				<ul class='dropdown-menu' uib-dropdown-menu role="menu" aria-labelledby="single-button">
				<li ng-repeat="p in objectTypelist"><a href ng-click="selectedItem.object_type=p;">{{p}}</a></li>
				</ul></div><input type='text' class='form-control input-sm' ng-model="selectedItem.object_type" required>
				</div><br />

				<div class='input-group'>
				<div class='input-group-btn' uib-dropdown >
				<button type='button' class='btn btn-info btn-sm' uib-dropdown-toggle >Status <span class='caret'></span></button>
				<ul class='dropdown-menu' uib-dropdown-menu role="menu" aria-labelledby="single-button">
				<li ng-repeat="p in ['active', 'inactive']"><a href ng-click="selectedItem.status=p">{{p}}</a></li>
				</ul></div><input type='text' class='form-control input-sm' ng-model="selectedItem.status" required>
				</div><br />

				<div class="input-group">
					<span class="input-group-addon"><i class="glyphicon glyphicon-sort"></i></span>
					<input type="text" class="form-control input-sm" ng-model="selectedItem.morder" placeholder="order preference"> 
				</div><br />

				<div class="input-group">
					<span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>
					<textarea class="form-control input-sm" ng-model="selectedItem.description" rows="5" placeholder="description"></textarea>
				</div><br />
				<div ng-if="selectedItem.picture != ''" class="row"><img ng-src="{{path}}{{selectedItem.name}}" height='200'/></div><br /><br />
				<div class="col-md-4">
				<a href ng-click='updatemedia();' class="btn btn-success btn-sm" style='color:white;width:200px;'><span class='glyphicon glyphicon-save'></span> &nbsp;{{ buttonlabel }} </a><br /><br /><br /><br />
				</div>
		</div>
		 <div class="col-md-2"></div>
	</div>
</div>
</div>
</div>
</div>
<style>
    #progressbox {
    border: 1px solid #0099CC;
    padding: 1px; 
    position:relative;
    width:400px;
    border-radius: 3px;
    margin: 10px;
    display:none;
    text-align:left;
    }
    #progressbar {
    height:20px;
    border-radius: 3px;
    background-color: #49afcd;
    width:1%;
    }
    #statustxt {
    top:3px;
    left:50%;
    position:absolute;
    display:inline-block;
    color: #000000;
    }
</style>
<script>
<?php
global $super_member_type_allowed;
$mediadata = new WY_Media($theRestaurant);
printf("var pathimg = '%s';", $mediadata->getPartialPath());
$object = "";
$supermember = (isset($_SESSION['user_backoffice']['member_type']) && in_array($_SESSION['user_backoffice']['member_type'], $super_member_type_allowed));
$promotionflg = $_SESSION['restaurant']['promotion'];
for($i = 0, $sep = ""; $i < count($picture_typelist); $i++, $sep = ",")
	if($picture_typelist[$i] != "profile" && 
		($picture_typelist[$i] != "promotion" || $promotionflg) &&
		($picture_typelist[$i] != "sponsor" || $supermember) )
		$object .= $sep . "'" . $picture_typelist[$i] . "'";

printf("\nvar type_object = [%s];", $object);
?>
</script>
<script src="inc/Media/MediaController.js"></script>