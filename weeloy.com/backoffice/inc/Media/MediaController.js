app.controller('ImageController', function($scope, $http) {
	$scope.paginator = new Pagination(25);
	$scope.token = $('#token').val();
	$scope.restaurant = $('#restaurant').val();
	$scope.email = $('#email').val();
	$scope.path = pathimg;
	$scope.typeselection = 1;
	$scope.tmpArr = [];
	$scope.predicate = '';
	$scope.reverse = false;
	$scope.tabletitle = [ {a:'index', b:'index' , q:'down', cc: 'fuchsia' }, 
						{a:'name', b:'Title' , q:'down', cc: 'black' }, 
						{a:'object_type', b:'Type' , q:'down', cc: 'black' }, 
						{a:'status', b:'Status' , q:'down', cc: 'black' }, 
						{a:'tag', b:'Tag' , q:'down', cc: 'black' }, 
						{a:'morder', b:'List Order' , q:'down', cc: 'black' } ];
	$scope.bckups = $scope.tabletitle.slice(0);
	$scope.tableContent = [ {a:'name', b:'Title' }, 
					{a:'object_type', b:'Object Type' }, 
					{a:'status', b:'Status' },
					{a:'tag', b:'Tag' },
					{a:'media_type', b:'Media Type' }, 
					{a:'morder', b:'List Order' }, 
					{a:'description', b:'Description' } ];
	$scope.objectTypelist = type_object.slice(0);
	$scope.tags = [];
	$scope.getAlignment = bkgetalignment;
	$scope.Objmedia = function() {
		return {
			restaurant: $scope.restaurant,
			index: '',
			name: '',
			newname: '',
			path: '',
			object_type: 'restaurant',
			tag: '',
			media_type: '',
			status: '',
			description: '',
			morder: '',
			
			remove: function() { 
				for(var i = 0; i < $scope.names.length; i++) 
					if($scope.names[i].name === this.name) {
						$scope.names.splice(i, 1);
						break;
						}
				},
				
			replicate: function(obj) {
				for (var attr in this)
		        	if(this.hasOwnProperty(attr) && typeof this[attr] !== 'function' && attr !== '$$hashKey') 
        				this[attr] = obj[attr];
				return this;
				},		
								
			clean: function() {
				for (var attr in this)
		        	if(this.hasOwnProperty(attr) && typeof this[attr] !== 'function' && attr !== '$$hashKey') {
        				if(typeof this[attr] === 'string')
        					this[attr] = this[attr].replace(/\'|\"/g, '’');
        				}
				return this;
				}	
			};
		};
	$scope.initorder = function() {
		$scope.tabletitle = $scope.bckups;
		$scope.predicate = "index";
		$scope.reverse = false;
	};
	$scope.reorder = function(item, alter) {
		alter = alter || "";
		if (alter !== "")  item = alter;
		$scope.reverse = ($scope.predicate === item) ? !$scope.reverse : false;
		$scope.predicate = item;
	};
	var url = '../api/restaurant/media/picture/' + $scope.restaurant +"/picture";
	$http.get(url).success(function(response) {	
		var i, data;
		$scope.names = [];
		data = response.data;
		for (var i = 0; i < data.length; i++)
			if(data[i].name.indexOf('wheel') != 0 || ( data[i].object_type == 'sponsor' && $scope.objectTypelist.indexOf('sponsor') >= 0) ) {
				data[i].index = i + 1;
				$scope.names.push(new $scope.Objmedia().replicate(data[i]));
			}
		$scope.paginator.setItemCount($scope.names.length);
		$scope.initorder();
	});
	$http.get('../api/restaurant/gettags/').success(function(response) {	
		var i, data;
		$scope.tags = [];
		data = response.data;
		var uniqueTags = {};
		//console.log("tags: "+JSON.stringify(data));
		for (var i = 0; i < data.length; i++) {
			if (data[i].tags) {	
				var tags = data[i].tags.split(']');
				for (var j = 0; j < tags.length; j++) {
					var str = tags[j].replace(/[\]\[]/gi, ' ');
					str = str.trim();
					if (str && !uniqueTags.hasOwnProperty(str)) {
						$scope.tags.push(str);
						uniqueTags[str] = str;
					}
				}
			}
		}
	});
	$scope.reset = function(item) {
               
		$scope.listingImageFlag = false;
		$scope.listing1ImageFlag = false;
		$scope.updateImageFlag = false
		$scope.renameImageFlag = false
		$scope.createImageFlag = false;
		$scope[item] = true;	
		};

	$scope.backlisting = function() {
            console.log("listingImageFlag");
		$scope.reset('listingImageFlag');
		};
		
	$scope.view = function(oo) {
		$scope.selectedItem = oo;
		$scope.reset('listing1ImageFlag');
		};
	
	$scope.create = function() {	
		$scope.selectedItem = new $scope.Objmedia();
		$scope.reset('createImageFlag');
		$scope.buttonlabel = "Save new media";
		$scope.action = "create";
		dom = $('#file_upload')[0];
		if(dom.files.length > 0) dom.files[0].name = '';
		return false;
		}
				
	$scope.rename = function(oo) {
		$scope.selectedItem = new $scope.Objmedia().replicate(oo);
		$scope.reset('renameImageFlag');
		$scope.buttonlabel = "Rename new media";
		$scope.action = "rename";
		return false;
		}
				
	$scope.update = function(oo) {
		$scope.selectedItem = new $scope.Objmedia().replicate(oo);
		$scope.reset('updateImageFlag');
		$scope.buttonlabel = "Update new media";
		$scope.action = "update";
		};


	$scope.renamemedia = function() {
		var newext, ext, i;
		$scope.selectedItem.newname = $scope.selectedItem.newname.replace(/[!@#$%^*()=}{\]\[\"\':;><\?/|\\]/g, ' ');
		
		if($scope.selectedItem.newname.length < 6) {
			alert("image name require 6 characters or more..." + $scope.selectedItem.newname.length);
			return;
			}
			
		newext = $scope.selectedItem.newname.substring($scope.selectedItem.newname.lastIndexOf(".")+1)
		ext = $scope.selectedItem.name.substring($scope.selectedItem.name.lastIndexOf(".")+1)
		if(newext != ext) {
			alert("Images can only be renamed with the same extention (" + newext + "/" + ext + ")");
			return false;
			}
		for(i = 0; i < $scope.names.length; i++)
			if($scope.names[i].name == $scope.selectedItem.newname) {
				alert("this name " + $scope.selectedItem.newname + " already exists. Please choose another one.");
				return false;
				}

		apiurl = '../api/renamemedia';
		$.ajax({
			url : apiurl,
			type: "POST",
			data: {
				restaurant: $scope.restaurant,
				name: $scope.selectedItem.name,
				newname: $scope.selectedItem.newname,
				type: $scope.selectedItem.media_type,
				category: $scope.selectedItem.object_type,
				token:token
				},
			success: function(data, textStatus, jqXHR) { 
				alert("'" + $scope.selectedItem.newname + "' has been renamed"); 
				},
			error: function (jqXHR, textStatus, errorThrown) { 
				alert("Unknown Error. Image '" + $scope.selectedItem.newname + "' has NOT been renamed." + textStatus); 
				}
			});
			
		for(i = 0; i < $scope.names.length; i++)
			if($scope.names[i].name == $scope.selectedItem.name) {
				$scope.names[i].name = $scope.selectedItem.newname;
				break;
				}
		 $scope.backlisting();
		};
		
	$scope.delete = function(oo) {
		var i, apiurl = '../api/deletemedia', recordname;

		if(confirm("Are you sure you want to delete " + oo.name) == false)
			return;
		
		recordname = oo.name;
		$.ajax({
			url : apiurl,
			type: "POST",
			data: {
				restaurant: $scope.restaurant,
				name: oo.name,
				category: oo.object_type,
				token:token
				},
			success: function(data, textStatus, jqXHR) { alert(recordname + " has been deleted");  },
				error: function (jqXHR, textStatus, errorThrown) { alert("Unknown Error. " + recordname + " Image has NOT been deleted. " + textStatus); }
			});
		
		oo.remove();		
 		$scope.backlisting();		
		};

	$scope.updatemedia = function() {
		var i, recordname;
		console.log("updatemedia()");
		$scope.selectedItem.clean();		
		$scope.selectedItem.morder = $scope.selectedItem.morder.replace(/[^0-9]/g, ' ');
		if($scope.selectedItem.morder == '')
			return alert('Invalid number');

		apiurl = '../api/updatemedia';
		for(i = 0; i < $scope.names.length; i++)
			if($scope.names[i].name === $scope.selectedItem.name) {
				$scope.names.splice(i, 1);
				$scope.names.push($scope.selectedItem);
				break;
				}
				
		recordname = $scope.selectedItem.name;		
		$.ajax({
			url : apiurl,
			type: "POST",
			data: {
				restaurant: $scope.restaurant,
				name: $scope.selectedItem.name,
				category: $scope.selectedItem.object_type,
				type: $scope.selectedItem.media_type,
				description: $scope.selectedItem.description,
				status: $scope.selectedItem.status,
				morder: $scope.selectedItem.morder,
				token:token
				},
			success: function(data, textStatus, jqXHR) { alert("'" + recordname + "' has been updated"); },
				error: function (jqXHR, textStatus, errorThrown) { alert("Unknown Error. Image '" + recordname + "'has NOT been updated." + textStatus); }
			});
		
 		$scope.backlisting();		
		};

	// Grab the files and set them to our variable
	//function prepareUpload(event) { files = event.target.files; }

	// Catch the form submit and upload the files
	$scope.uploadFiles = function()	{
		console.log("uploadFiles()");
		var object_type = $scope.selectedItem.object_type;
		dom = $('#file_upload')[0];
		files = dom.files;  //get the DOM -> files = document.getElementById('file_upload').files;
		
		if(files.length == 0) {
			alert("Select/Drop one file");
			return false;
			}

		filename = files[0].name;
		ext = filename.substring(filename.lastIndexOf(".")+1).toLowerCase();
		if(ext != "jpg" && ext != "jpeg" && ext != "png" && ext != "gif" && ext != "pdf") {
			alert("Invalid file type (jpg, jpeg, png, gif)");
			return false;
			}
		if(object_type == "") {
			alert("Please Choose a category");
			return false;
			}
			
		$scope.selectedItem.name = files[0].name;
		$scope.selectedItem.object_type = object_type;
		if(ext != 'pdf'){
           $scope.selectedItem.media_type = 'picture';
        }else if(ext == 'pdf'){
            $scope.selectedItem.media_type = 'pdf';
        }
        
		$scope.selectedItem.path = '';
		$scope.selectedItem.status = 'inactive';
		$scope.selectedItem.description = '';
		$scope.selectedItem.morder = '99';
		$scope.selectedItem.index = ($scope.names.length > 0) ? $scope.names[$scope.names.length - 1].index + 1 : 1;

		$scope.names.push( $scope.selectedItem );
		

       // START A LOADING SPINNER HERE

        // Create a formdata object and add the files
		var data = new FormData();
		$.each(files, function(key, value)
		{
			data.append(key, value);
		});

		data.append('token', token);
		data.append('restaurant', $scope.restaurant);
		data.append('category', $scope.selectedItem.object_type);
        data.append('media_type', $scope.selectedItem.media_type);
        data.append('tag', $scope.selectedItem.tag);
        $("#progressbox").css('display','block');

        $.ajax({
            url: 'saveimages.php?files',
            type: 'POST',
            data: data,
            cache: false,
            dataType: 'json',
            processData: false, // Don't process the files
            contentType: false,
            xhr: function() {
                    var myXhr = $.ajaxSettings.xhr();
                    if(myXhr.upload){
                        myXhr.upload.addEventListener('progress',progressHandlingFunction, false);
                    }
                     return myXhr;
                },// Set content type to false as jQuery will tell the server its a query string request
            success: function(data, textStatus, jqXHR)
            	{
            	if(typeof data.error === 'undefined')
            		$scope.submitForm(dom, data);
                       
            	else {
            		alert(data.error);
            		$scope.selectedItem.remove();
            		$scope.$apply();
            		}
                   
             },
            error: function(jqXHR, textStatus, errorThrown) {
           		alert(textStatus);		
            }
        });
      
    };
    
    function progressHandlingFunction(e){
            console.log("in progress");
        var max = e.total;
        var current = e.loaded;
         var percent = (current / max) * 100;
        console.log("in progress"+percent);
         if(percent>80)
            {
                  console.log(percent);
                $("#statustxt").css('color','#fff'); //change status text to white after 50%
            }
        $('#progressbar').width(percent + '%') //update progressbar percent complete
        $('#statustxt').html(percent + '%');
    }

    $scope.submitForm = function(event, data) {
		// Create a jQuery object from the form
		$form = $(event.target);
		
		// Serialize the form data
		var formData = $form.serialize();
		
		// You should sterilise the file names
		$.each(data.files, function(key, value)
		{
			formData = formData + '&filenames[]=' + value;
		});

		formData = formData + '&token=' + token + '&restaurant=' + $scope.restaurant + '&category=' + $scope.selectedItem.object_type + '&tag=' + $scope.selectedItem.tag;

		$.ajax({
			url: 'saveimages.php',
            type: 'POST',
            data: formData,
            cache: false,
            dataType: 'json',
            success: function(data, textStatus, jqXHR) {
                
            	if(typeof data.error === 'undefined') {
            		$scope.selectedItem.name = $scope.selectedItem.name.replace(/\'|\"/g, "_");
            		// Success so call function to process the form
            		alert('The file ' + $scope.selectedItem.name + ' has been uploaded');
                        
                       
            		console.log('SUCCESS: ' + data.success);
                        
            		}
            	else { console.log('ERRORS: ' + data.error); /* Handle errors here */ }
            	},
            error: function(jqXHR, textStatus, errorThrown) { console.log('ERRORS: ' + textStatus); /* Handle errors here */ },
            complete: function() {
                 $("#progressbox").css('display','none');
                   window.location.reload();
                 /* STOP LOADING SPINNER  */ }
           
		});
                
	};

		
        
	
	});