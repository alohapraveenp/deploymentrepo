<?php

$baseUrl = __BASE_URL__;
$res = new WY_restaurant;


$currency = $res->getCurrency($theRestaurant);
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<div class="container">
    <div class="row">
        <div class="col-md-12 left-sec">
            <div ng-controller="PaymentController"  ng-init="moduleName='payment'; listPaymentFlag = true;createView = false" >
                <div class="col-md-2"></div>
                
                    <div id='listing' ng-show='listPaymentFlag'>
                        <div class="form-group"  style='margin-bottom:25px;'>
                                <div class="col-md-4">
                                        <div class="input-group col-md-4">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                                                <input type="text" class="form-control input-sm" ng-model="searchText" ng-change="paginator.setPage(0);" style='width:200px;'> 
                                        </div>
                                </div>
                                <div class="col-md-2"></div>
                                <div class="col-md-4">
                                <a href ng-click='create();' class="btn btn-success btn-sm" style='color:white;width:200px;'><span class='glyphicon glyphicon-certificate'></span> &nbsp;Make Payment</a>
                                </div>
                        </div>
                        <div style=" clear: both;"></div>
                        <table width='100%' class="table table-condensed table-striped" style='font-size:12px;'>
                            <thead>
				<tr>
				<th ng-repeat="y in tabletitle"><tbtitle var="{{y.a}}" name='tabletitle' module='moduleName'/></th>
				</tr>
                            </thead>
                            <tbody style='font-family:helvetica;font-size:12px;'>
                                    <tr ng-repeat="x in filteredEvent = (names | filter:searchText) | sizefilter:this | orderBy:predicate:reverse | slicefilter:paginator.getPage():paginator.getRowperPage():paginator.getItemCount() | limitTo:paginator.getRowperPage()" style='font-family:helvetica;font-size:11px;'> 
                                            <td ng-repeat="y in tabletitle"><a href ng-click="view(x)" ng-style="{ 'text-align' : getAlignment(x[y.a]) }"> {{ x[y.a] | adatereverse:y.c | notzero }}</a></td>
                                      </tr>
                                    <tr><td colspan='{{tabletitle.length + 5}}'></td></tr>
                            </tbody>
                                
                        </table>
<!--                        <div ng-if="filteredEvent.length >= paginator.getRowperPage()" class="col-md-12" align="center" style='font-size:11px' ng-include="'inc/paginator.html'"></div>-->
                    </div>
                    <div class="col-md-12" ng-show='viewPaymentFlag'>
                        <br /><a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a><br />
                        <table class='table-striped' style="margin: 0 0 150px 30px;font-size:13px;font-family: Roboto">	

                                <tr ng-repeat="y in tabletitleContent | filter: {b: '!picture'}"><td nowrap ><strong> {{ y.b }} </strong></td><td> &nbsp; </td><td>{{ selectedItem[y.a] | adatereverse:y.c }} </td></tr>

                        </table>
                    </div>
                <div iid='create' ng-show='createView' >
                   
                    <div id='create' class="col-md-12" >
                        <br />
                        <a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a>
                        <br />
                        
                         <div class="col-md-2"> </div>
              
                        <div class="col-md-6">
                             <h4 style='text-align:center;'>Pay your billings</h4>
                            <div class="row"  style="margin: 0 0 20px 0;font-size:12px;font-familly:Roboto">
                                 <div class="input-group"  style="margin-bottom:20px;">
                                     <span class="input-group-addon input11" ng-bind="currency"></span>
                                    <input type="text"  class="form-control input-sm" ng-model="amount" ng-change='checkvalidamount();'  required  >
                                </div> 
                                <div class="payment-method">
                                   
                                   <div class="form-group checkbox ">
                                       <label><h4>Payment method : </h4></label>
<!--                                        <label>
                                            <input type="radio" name="optionsPaymentRadios" id="optionsPaymentRadios1" ng-model="paymentType" value="creditcard" > Credit card
                                        </label>-->
                                        <label  >
                                             <input  type="radio" name="optionsPaymentRadios" id="optionsPaymentRadios2" ng-model="paymentType" value="paypal" ng-checked="paypal = true" > Paypal
                                        </label>
                                    </div>
                                     
                                </div>
                            </div>
                          </div>
                        
                    </div>
                <div class="col-md-6"></div>
<!--		<div class="col-md-7"></div>-->
		<div class="col-md-6">
                        <a href ng-click='makepayment();' class="btn btn-success btn-sm" style="color:white;width:150px"><i class='glyphicon glyphicon-save'></i> &nbsp;PAY </a><br />
		</div>
            </div>
            </div>

        </div>
    </div>
</div>
<script>
 
    app.controller('PaymentController', ['$scope', '$http', 'bookService', function($scope, $http, bookService) {
        $scope.restaurant = <?php echo "'" . $theRestaurant . "';"; ?>
        $scope.email =  <?php echo "'".$_SESSION['user_backoffice']['email']."';";?>
        $scope.baseUrl = <?php echo "'".$baseUrl."';";?>
        $scope.names = [];
        $scope.currency = <?php echo "'" . $currency . "';"; ?>
        var aorder = (function() { var i, arr=[]; for(i = 0; i < 30; i++) arr.push(i); return arr; })();
	
	$scope.paginator = new Pagination(25);
        $scope.reset = function(item) {
            $scope.listPaymentFlag = false;
            $scope.createView = false;
            $scope.viewPaymentFlag = false;
            $scope[item] = true;	
        };
        $scope.backlisting = function(page) {
            $scope.reset('listPaymentFlag');
	};
        $scope.initorder = function() {
            $scope.predicate = "index";
            $scope.reverse = true;
	};
        
        $scope.reorder = function(item, alter) {
		alter = alter || "";
		if (alter !== "")  item = alter;
		$scope.reverse = ($scope.predicate === item) ? !$scope.reverse : false;
		$scope.predicate = item;
	};
        
   
        $scope.create = function(){
            $scope.listPaymentFlag = false;
            $scope.viewPaymentFlag = false;
            $scope.createView = true;
        }
        $scope.tabletitle = [ {a:'index', b:'ID', c:'' , l:'', q:'down', cc: 'fuchsia' },{a:'order_id', b:'Order', c:'' , l:'', q:'down', cc: 'fuchsia' }, {a:'payment_id', b:'Payment', c:'' , l:'25', q:'down', cc: 'black' }, {a:'currency', b:'Currency', c:'' , l:'', q:'down', cc: 'black' },{a:'amount', b:'Amount', c:'' , l:'', q:'down', cc: 'black' }, {a:'status', b:'Status', c:'' , l:'', q:'down', cc: 'black' }];
        $scope.tabletitleContent = [ { a:'order_id', b:'Order ID', c:'' }, { a:'payment_id', b:'Payment ID', c:'' }, { a:'currency', b:'Currency', c:'' }, { a:'amount', b:'Amount', c:'' }, { a:'status', b:'Status', c:'' }, { a:'payment_method', b:'Payment Method', c:'' }, { a:'created_at', b:'Date of Paid', c:'' }];
        
        bookService.getRestaurantPayment($scope.restaurant).then(function(response) {

            if(response.status === 1){
                var data = response.data.payment;
                for (var i = 0; i < data.length; i++) {
                        data[i].index = i + 1;
                        data[i].order_id = data[i].object_id;
                        $scope.names.push(data[i]); 
                } 
                
            }
        });
        $scope.cleanamount = function (obj) {  

            obj = obj.replace(/[^0-9\.]/g, '');

            return obj;
        }
        $scope.checkvalidamount = function() {
            console.log("SDsd"+$scope.amount.length);
            if($scope.amount.length > 0 ){
                $scope.amount = $scope.cleanamount($scope.amount); 
            }
        }
        
        $scope.view = function(oo) {
            $scope.selectedItem = oo;
            $scope.reset('viewPaymentFlag');

        };
        $scope.makepayment = function(){
            $scope.paymentType ='paypal';
            var url = $scope.baseUrl+"/modules/payment/payresponse.php?refid=";
            bookService.wyMakePayment($scope.email,$scope.restaurant,$scope.amount,$scope.paymentType,url).then(function(response) {
 
            if (response.status == '1') {
                var form = document.createElement('form');
                form.action = response.data.payment.paypalUrl;
                form.method = 'POST';
                document.body.appendChild(form);
                console.log(form.action);
                form.submit();

            }
             
             
        });

      };
    }]);
</script>