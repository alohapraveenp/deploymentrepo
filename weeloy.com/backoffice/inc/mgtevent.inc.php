<div class="container">
    <div class="row">
        <div class="col-md-12 left-sec">
<div ng-controller="EventController" ng-init="moduleName='event'; listEventFlag = true; viewEventFlag=false; createEventFlag=false;" >


	<div class="col-md-12" ng-show='viewBkEventFlag'>
		<br /><a href class='btn btn-info btn-sm customColor' ng-click="backlisting('viewlistBkEventFlag')"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a><br />
		<table class='table-striped' style="margin: 0 0 120px 30px;font-size:13px;font-family: Roboto">		
			<tr ng-repeat="y in tableBktitleContent | filter: {b: '!picture'}"><td nowrap ><strong> {{ y.b }} </strong></td><td width='30'> &nbsp; </td><td>{{ selectedBooking[y.a] | adatereverse:y.c }} </td></tr>
                         <tr><td nowrap ><strong>Tracking </strong></td><td width='30'> &nbsp; </td><td>{{ selectedBooking.tracking }} </td></tr>
                        <tr><td nowrap ><strong>Payemnt Id </strong></td><td width='30'> &nbsp; </td><td>{{ selectedBooking.paykey }} </td></tr>
                        <tr ><td nowrap ><strong>Payment Type</strong></td><td width='30'> &nbsp; </td><td>{{ selectedBooking.payment_method }}  </td></tr>
                        <tr ng-if = "selectedBooking.payment_status === 'REFUNDED' "><td nowrap ><strong>Refunded Amount</strong></td><td width='30'> &nbsp; </td><td>{{ selectedBooking.currency }} {{ selectedBooking.refund_amount }} </td></tr>
                        <tr><td nowrap ><strong>Payment Status </strong></td><td width='30'> &nbsp; </td><td>{{ selectedBooking.payment_status }} </td></tr>

		</table>
                <div ng-show="selectedBooking.amount > 0  && selectedBooking.payment_status =='COMPLETED'" style="margin: 0 0 20px 30px;font-size:14px;font-family: Roboto">
                        <h4 style='text-align:left;'>Refund payment</h4>
                        <br /> 
                        <div class="amount" style='text-align:left;'>
                            <span>Amount : &nbsp; SGD&nbsp;</span> <input type="text" ng-model ="selectedBooking.refund_amount" >
                            <div class="btn-group"  style="padding-left:10px;"><a class='btn btn-danger btn-sm' ng-click="depositRefund(selectedBooking);" class="cancelhover" style='color:white;'>Cancel Event</a></div>
                        </div>
                            
                </div>
	</div>

	<div id='listing' ng-show='viewlistBkEventFlag'>
		<br />
		<a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a>
		<br />
       	<h4 style="text-align:center;text-transform: uppercase;color:navy;">  {{ selectedItem.title }} </h4><br />
		<div class="form-group"  style='margin-bottom:25px;'>
			<div class="col-md-4">
				<div class="input-group col-md-4">
					<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
					<input type="text" class="form-control input-sm" ng-model="searchSubText" ng-change="paginatorsub.setPage(0);" style='width:200px;'> 
				</div>
				<span class='infobk' style='margin-left:20px;'>selected event bookings: <strong> {{filteredBook.length}} </strong></span><br />
			</div>
			<div class="col-md-2"></div>
			<div class="col-md-4">
			</form>
			<form action="echo.php" id="extractForm" name="extractForm" method="POST" target="_blank">
				<input type="hidden" value="testing" ng-model='content' name="content" id="content">
				<a href ng-click='extractSelection();' class="btn btn-success btn-sm" style='color:white;margin-left:5px'>Extract Selection  &nbsp;<span class='glyphicon glyphicon-save'></span></a>
			</form>
			</div>
		</div>
       	<div style=" clear: both;"></div>
		<table width='100%' class="table table-condensed table-striped" style='font-size:12px;'>
			<thead><tr><th ng-repeat="y in tabletitlebook"><tbtitle var="{{y.a}}" name='tabletitle' module='moduleName'/></th></tr></thead>
			<tbody style='font-family:helvetica;font-size:12px;'>
				<tr ng-repeat="e in filteredBook = (selectedItem.bkevent | filter:searchSubText) | sizefilter:this | orderBy:predicate:reverse | slicefilter:paginatorsub.getPage():paginatorsub.getRowperPage():paginatorsub.getItemCount() | limitTo:paginatorsub.getRowperPage()" style='font-family:helvetica;font-size:11px;'> 
					<td ng-repeat="w in tabletitlebook"><a href ng-click="viewevbook(e)" ng-style="{ 'text-align' : getAlignment(e[w.a]) }">{{ e[w.a] | adatereverse:w.c | notzero }}</a></td>
				</tr>
				<tr><td colspan='{{tabletitlebook.length + 5}}'></td></tr>
			</tbody>
		</table>
		<div ng-if="filteredBook.length >= paginatorsub.getRowperPage()" class="col-md-12" align="center" style='font-size:11px' ng-include="'inc/paginatorsub.html'"></div>
	</div>

	<div id='listing' ng-show='listEventFlag'>
		<div class="form-group"  style='margin-bottom:25px;'>
			<div class="col-md-4">
				<div class="input-group col-md-4">
					<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
					<input type="text" class="form-control input-sm" ng-model="searchText" ng-change="paginator.setPage(0);" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" style='width:200px;'> 
				</div>
			</div>
			<div class="col-md-2"></div>
			<div class="col-md-4">
			<a href ng-click='create();' class="btn btn-success btn-sm" style='color:white;width:200px;'><span class='glyphicon glyphicon-certificate'></span> &nbsp;Create a New Event</a>
			</div>
		</div>
       		<div style=" clear: both;"></div>
		<table width='100%' class="table table-condensed table-striped" style='font-size:12px;'>
			<thead>
				<tr>
				<th ng-repeat="y in tabletitle"><tbtitle var="{{y.a}}" name='tabletitle' module='moduleName'/></th>
                <th width='20'>featured</th>
  				<th width='20'>update</th>
				<th width='5'> &nbsp; </th>
               	<th width='20'>delete</th>
				</tr>
			 </thead>
			<tbody style='font-family:helvetica;font-size:12px;'>
				<tr ng-repeat="x in filteredEvent = (names | filter:searchText) | sizefilter:this | orderBy:predicate:reverse | slicefilter:paginator.getPage():paginator.getRowperPage():paginator.getItemCount() | limitTo:paginator.getRowperPage()" style='font-family:helvetica;font-size:11px;'> 
					<td ng-repeat="y in tabletitle"><a href ng-click="view(x, y.a)" ng-style="{ 'text-align' : getAlignment(x[y.a]) }"> {{ x[y.a] | adatereverse:y.c | notzero }}<span ng-if="y.a === 'bkpax' && x.bkevent.length > 0 && x.bkpax === 0">0</span></a></td>
					<td align='center'><span ng-if="x['featured']" class='glyphicon glyphicon-star orange'></span> </td>
					<td align='center'><a href ng-click="update(x)"><span class='glyphicon glyphicon-pencil blue'></a></span></td>
					<td >&nbsp;</td>
					<td align='center'><a href ng-click="delete(x)"><span class='glyphicon glyphicon-trash red'></span></a></td>
				</tr>
				<tr><td colspan='{{tabletitle.length + 6}}'></td></tr>
			</tbody>
		</table>
		<div ng-if="filteredEvent.length >= paginator.getRowperPage()" class="col-md-12" align="center" style='font-size:11px' ng-include="'inc/paginator.html'"></div>
	</div>

	<div class="col-md-12" ng-show='viewEventFlag'>
		<br /><a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a><br />
		<table class='table-striped' style="margin: 0 0 150px 30px;font-size:13px;font-family: Roboto">		
			<tr ng-repeat="y in tabletitleContent | filter: {b: '!picture'}"><td nowrap ><strong> {{ y.b }} </strong></td><td> &nbsp; </td><td>{{ selectedItem[y.a] | adatereverse:y.c }} </td></tr>
			<tr ng-if="selectedItem['picture'] != ''"><td nowrap><strong>Picture: </strong></td><td> &nbsp; </td><td class="showpict"><img ng-src="{{path}}{{selectedItem.picture}}" height='150'/></td></tr>
		</table>
	</div>

	<div class="col-md-12" ng-show='createEventFlag'>
		<br />
		<a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a>
		<br />
		 <div class="col-md-2"></div>
		 <div class="col-md-8">
		   <div class="row" ng-repeat="y in tabletitleContent | filter: {t: '!dontshow' }" style="margin: 0 0 20px 0;font-size:12px;font-familly:Roboto">
                        
			<div class="input-group" ng-if="y.t === 'input'">
				<span class="input-group-addon input11"><i class="fa fa-{{y.d}}"></i>&nbsp; {{y.b}}</span>
				<input type="text" class="form-control input-sm" ng-model="selectedItem[y.a]" ng-change="cleaninput(y.a)" ng-readonly="y.readonly ===1 && action=='update'" >
			</div> 
                    <div class="input-group" ng-if="y.t === 'inputprice' && showprice===1 ">
				<span class="input-group-addon input11"><i class="fa fa-{{y.d}}"></i>&nbsp; {{y.b}}</span>
				<input type="text" class="form-control input-sm" ng-model="selectedItem[y.a]" ng-change="cleaninput(y.a)" ng-readonly="y.readonly ===1 && action=='update'" >
			</div> 
			<div class="input-group" ng-if="y.t === 'textarea'">
				<span class="input-group-addon input11"><i class="fa fa-{{y.d}}"></i>&nbsp; {{y.b}}</span>
				<textarea class="form-control input-sm" ng-model="selectedItem[y.a]" ng-change="cleaninput(y.a)" rows="5" ></textarea>
			</div>
			<div class="input-group" ng-if="y.t === 'date'">
				<span class="input-group-btn input11">
				<button type="button" class="btn btn-default btn-sm" ng-click="y.u.dateopen($event)"><i class="fa fa-calendar"></i>&nbsp; {{y.b}}</button>
				</span>
				<input type="text" class="form-control input-sm" uib-datepicker-popup="{{y.u.formats[0]}}" min-date="y.u.minDate" max-date="y.u.maxDate" ng-model="y.u.originaldate"
				is-open="y.u.opened" datepicker-options="y.u.dateOptions" ng-change="y.u.onchange();" close-text="Close" readonly />
			</div>
			   <div class="input-group"  ng-if="y.t==='checkbox' ">
				   <input   type="checkbox" ng-model="selectedItem[y.a]" ng-checked ="{{selectedItem[y.a]}}"> &nbsp; {{y.b}}
					   
				</div>
            <div class="input-group" ng-if="y.t === 'array'">
				<div class='input-group-btn' uib-dropdown >
				<button type='button' class='btn btn-default btn-sm input12' uib-dropdown-toggle >
					<i class="fa fa-{{y.d}}"></i>&nbsp; {{y.b}} <span class='caret'></span></button>
				<ul class='dropdown-menu scrollable-menu' uib-dropdown-menu>
				<li ng-repeat="p in y.val"><a href ng-click="selectedItem[y.a]=p;y.func()">{{ p }}</a></li>
				</ul>
				</div>
				<input type='text' ng-model='selectedItem[y.a]' class='form-control input-sm' readonly >
			</div>
                       
			<div class="input-group" ng-if="y.t === 'picture'">
				<div class='input-group-btn' uib-dropdown >
					<button type='button' class='btn btn-default btn-sm input12' uib-dropdown-toggle >
						&nbsp;<i class="fa fa-{{y.d}}"></i>&nbsp; {{y.b}} <span class='caret'></span>
					</button>
					<ul class='dropdown-menu scrollable-menu' uib-dropdown-menu>
						<li ng-repeat="p in imgEvent"><a href ng-click="selectedItem['picture'] = p;">{{ p }}</a></li>
					</ul>
				</div>
				<input type='text' ng-model="selectedItem['picture']" class='form-control input-sm' readonly >
			</div>
			<div class="input-group" ng-if="y.t === 'pictureshow'">
				<p ng-if="selectedItem.picture != ''"><img ng-src="{{path}}{{selectedItem.picture}}" height='150'/></p>
			</div>

		   </div><br />
		</div>
		<div class="col-md-2"></div>
		<div class="col-md-7"></div>
		<div class="col-md-5">
			<a href ng-click='savenewevent();' class="btn btn-success btn-sm" style="color:white;width:150px"><i class='glyphicon glyphicon-save'></i> &nbsp;{{ buttonlabel }} </a><br />
		</div>
		</div>
</div>

</div>
</div>
</div>

<script>

<?php
$mediadata = new WY_Media($theRestaurant);
$res = new WY_restaurant;
$res->getRestaurant($theRestaurant);
$isPrice = ($res->checkeventbooking() > 0) ? 1 : 0;
 
 
    $imgAr = $mediadata->getEventPictureNames($theRestaurant);
    printf("var imgEvent = [");
    for($i = 0, $sep = ""; $i < count($imgAr); $i++, $sep = ", "){
    	printf("%s '%s'", $sep, $imgAr[$i]);
    }
    printf("];");

    printf("var pathimg = '%s';", $mediadata->getFullPath('small'));
?>

var token = <?php echo "'".$_SESSION['user_backoffice']['token']."';"; ?>
var email =  <?php echo "'".$_SESSION['user_backoffice']['email']."';";?>


app.controller('EventController', ['$scope', 'bookService', '$http', function($scope, bookService, $http) {
       
	var forceDate = new Date(new Date().getFullYear()-2, 0, 1);
	var todaydate = new Date();
	var aorder = (function() { var i, arr=[]; for(i = 0; i < 30; i++) arr.push(i); return arr; })();
	var paxnumbers = (function() { var i, arr=[]; for(i = 0; i <= 300; i++) arr.push(i); return arr; })();
	
	$scope.paginator = new Pagination(25);
	$scope.paginatorsub = new Pagination(25); 
	$scope.evbooking = [];
	$scope.names = [];
	$scope.atype = ['public', 'private'];
	$scope.amenus = [];
	$scope.documentdpf = ['cadeau'];
	$scope.path = pathimg;
	$scope.restaurant = <?php echo "'".$theRestaurant."';"; ?>
	$scope.email = <?php echo "'" . $email . "';"; ?>
    $scope.showprice=<?php echo $isPrice ?>;
	$scope.predicate = '';
	$scope.reverse = false;
	$scope.nonefunc = function() {};
	$scope.mydata_start = new bookService.ModalDataBooking();
	$scope.mydata_endng = new bookService.ModalDataBooking();
    $scope.mydata_display = new bookService.ModalDataBooking();
	$scope.mydata_start.setInit(todaydate, "09:00", function() { $scope.selectedItem.start = $scope.mydata_start.originaldate.getDateFormatReverse('-'); }, 3, todaydate, new Date(todaydate.getFullYear()+1, 11, 31));
	$scope.mydata_endng.setInit(todaydate, "09:00", function() { $scope.selectedItem.end = $scope.mydata_endng.originaldate.getDateFormatReverse('-'); }, 3, todaydate, new Date(todaydate.getFullYear()+1, 11, 31));
    $scope.mydata_display.setInit(todaydate, "09:00", function() { $scope.selectedItem.display = $scope.mydata_display.originaldate.getDateFormatReverse('-'); }, 3, todaydate, new Date(todaydate.getFullYear()+1, 11, 31));

	$scope.tableBktitleContent = [{ a: 'eventID', b: 'eventID' }, { a: 'orderID', b: 'orderID' }, { a: 'pax', b: 'Pax' }, { a: 'amount', b: 'Amount' }, { a: 'currency', b: 'Currency' }, { a: 'firstname', b: 'Firstname' }, { a: 'lastname', b: 'Lastname' }, { a: 'email', b: 'Email' }, { a: 'phone', b: 'Phone' }, { a: 'status', b: 'Status' }, { a: 'cdate', b: 'Create Date' }];
	$scope.tabletitlebook = [ {a:'orderID', b:'orderID', c:'' , l:'', q:'down', cc: 'black' }, {a:'pax', b:'Pax', c:'' , l:'', q:'down', cc: 'black' }, {a:'amount', b:'Amount', c:'' , l:'', q:'down', cc: 'black' }, {a:'fullname', b:'Name', c:'' , l:'', q:'down', cc: 'black' }, {a:'email', b:'Email', c:'' , l:'', q:'down', cc: 'black' }, {a:'phone', b:'Phone', c:'' , l:'', q:'down', cc: 'black' }, {a:'status', b:'Status', c:'' , l:'', q:'down', cc: 'black' }];
	$scope.tabletitle = [ {a:'vorder', b:'Order', c:'' , l:'', q:'down', cc: 'fuchsia', h:'left' }, {a:'name', b:'Title', c:'' , l:'25', q:'down', cc: 'black', h:'left' }, {a:'city', b:'Location', c:'' , l:'', q:'down', cc: 'black', h:'left' }, {a:'maxpax', b:'MaxPax', c:'' , l:'', q:'down', cc: 'black', h:'left' }, {a:'price', b:'Price', c:'' , l:'', q:'down', cc: 'black', h:'left' }, {alter:'startv', a:'start', b:'Start', c:'date' , q:'down', cc: 'black', h:'left' }, {alter:'endv',  a:'end', b:'End', c:'date' , q:'down', cc: 'black', h:'left' }, {alter:'displayv',  a:'display', b:'Display', c:'date' , q:'down', cc: 'black', h:'left' } ];
	if($scope.showprice === 1 || true) {
		$scope.tabletitle.push({a:'bkpax', b:'BkPax', c:'' , l:'', q:'down', cc: 'black' });
		}
	$scope.bckups = $scope.tabletitle.slice(0);

	//$scope.tabletitleContent = [ { a:'name', b:'Name', c:'', d:'anchor', t:'input', i:0 }, { a:'title', b:'Title', c:'', d:'tag', t:'input', i:0 },{ a:'description', b:'Description', c:'', d:'list', t:'textarea' }, { a:'city', b:'Location', c:'', d:'map-marker', t:'input', i:0 }, { a:'price', b:'Price', c:'', d:'money', t:'inputprice', i:0 }, { a:'maxpax', b:'Max Pax', c:'', d:'male', t:'array', val:paxnumbers, i:0 }, {a:'start', b:'Starting Date', c:'date', d:'calendar', t:'date', u:$scope.mydata_start }, {a:'end', b:'Ending Date', c:'date', d:'calendar', t:'date', u:$scope.mydata_endng }, {a:'display', b:'Display Date', c:'date', d:'calendar', t:'date', u:$scope.mydata_display },{ a:'tnc', b:'Terms/Conditions', c:'', d:'legal', t:'textarea' },{ a:'type', b:'Type', c:'', d:'info', t:'array', val: $scope.atype, func: $scope.nonefunc },{ a:'menu', b:'Menus', c:'', d:'cutlery', t:'array', val: $scope.amenus, func: $scope.nonefunc },{ a:'morder', b:'Position', c:'', d:'sort', t:'array', val: aorder, func: $scope.nonefunc },{ a:'picture', b:'Select an image', c:'', d:'photo', t:'picture' } , { a:'picture', b:'Picture', c:'', d:'photo', t:'pictureshow' } ];
    $scope.tabletitleContent = [ { a:'name', b:'Name', c:'', d:'anchor', t:'input', i:0, readonly :1 }, { a:'title', b:'Title', c:'', d:'tag', t:'input', i:0,readonly :1 },{ a:'description', b:'Description', c:'', d:'list', t:'textarea' }, { a:'city', b:'Location', c:'', d:'map-marker', t:'input', i:0 }, { a:'price', b:'Price', c:'', d:'money', t:'inputprice', i:0 }, { a:'maxpax', b:'Max Pax', c:'', d:'male', t:'array', val:paxnumbers, i:0 }, {a:'start', b:'Starting Date', c:'date', d:'calendar', t:'date', u:$scope.mydata_start }, {a:'end', b:'Ending Date', c:'date', d:'calendar', t:'date', u:$scope.mydata_endng }, {a:'display', b:'Display Date', c:'date', d:'calendar', t:'date', u:$scope.mydata_display },{ a:'tnc', b:'Terms/Conditions', c:'', d:'legal', t:'textarea' },{ a:'type', b:'Type', c:'', d:'info', t:'array', val: $scope.atype, func: $scope.nonefunc },{ a:'morder', b:'Position', c:'', d:'sort', t:'array', val: aorder, func: $scope.nonefunc }, { a:'pdf_link', b:'document-link', c:'', d:'male', t:'array', val:$scope.documentdpf, i:0 }, { a:'picture', b:'Select an image', c:'', d:'photo', t:'picture' } , { a:'picture', b:'Picture', c:'', d:'photo', t:'pictureshow' } ];

	$scope.currency = 'SGD';

	$scope.getAlignment = function(a) {
		return (typeof a === 'number') ? 'center':'left';
		}
	
	$scope.imgEvent = imgEvent.slice(0);
		
   	if(typeof email === 'string' && email !== '' && email.search('@weeloy.com') > 0) {
		$scope.tabletitleContent.splice(-2, 0, { a:'featured', b:'Featured On Home Page', c:'', d:'sort', t:'checkbox',i:0 });
		}

	$scope.Objevent = function() {
		return {
			restaurant: $scope.restaurant, 
			eventID: 0,
			index: 0, 
			name: '',
			title: '', 
			city: '', 
			country: '', 
			start: '', 
			end: '', 
			startv: '', 
			endv: '',
			displayv: '',
			display:'',
			sddate:'',
			eddate:'', 
            dpdate:'',
            pdf_link: '',
			description: '',
			tnc: '',
			type: $scope.atype[0],
			menu: '',
			maxpax: 0, 
			morder: 0,
			bkpax: 0,
			bkevent: [],
                        object_id :'',
			vorder: 0,
           	featured :false,
			picture: '',
			price: '',
			
			remove: function() { 
				for(var i = 0; i < $scope.names.length; i++) 
					if($scope.names[i].name === this.name) {
						$scope.names.splice(i, 1);
						break;
						}
				},
				
			replicate: function(obj) {
				for (var attr in this)
		        	if(this.hasOwnProperty(attr) && typeof this[attr] !== 'function' && attr !== '$$hashKey') 
        				this[attr] = obj[attr];
				return this;
				},
										
			clean: function() {
				if(this.start === '') this.start = $scope.mydata_start.getDate('-', 'reverse');
				if(this.end === '') this.end = $scope.mydata_endng.getDate('-', 'reverse');
                		if(this.display === '') this.display = $scope.mydata_display.getDate('-', 'reverse');

				for (var attr in this)
		        	if(this.hasOwnProperty(attr) && typeof this[attr] !== 'function' && attr !== '$$hashKey') {
        				if(typeof this[attr] === 'string') {
        					this[attr] = this[attr].replace(/\'|\"/g, '’');
        					}
        				}
				return this;
				},
					
			check: function() {
				if(this.name === '' || this.name.length < 6) { alert('Invalid title name, empty or too short(5)'); return -1;}
				if(this.title === '' || this.title.length < 3) { alert('Invalid title name, empty or too short(2)'); return -1;}
				return 1;
				}
			};
	};
		
	$scope.initorder = function() {
		$scope.tabletitle = $scope.bckups;
		$scope.predicate = "vorder";
		$scope.reverse = true;
	};

	$scope.reorder = function(item, alter) {
		alter = alter || "";
		if (alter !== "")  item = alter;
		$scope.reverse = ($scope.predicate === item) ? !$scope.reverse : false;
		$scope.predicate = item;
	};


	bookService.readEvent($scope.restaurant, $scope.email).then(function(response) {
		var i, j, data, bkdata, bkflag;
		
		if(response.status !== 1 && response.status !== "1")
			return;
			 	
		$scope.names = [];
		data = response.data.event;
		bkdata = response.data.evbooking;
		bkflag = response.data.evbookable;
		$scope.currency = response.data.currency;
		if(typeof $scope.currency === "string" && $scope.currency !== "")
			$scope.tabletitleContent.iconcurrency($scope.currency, "price", "a", "d");

		$scope.evbooking = bkdata.slice(0);
		
		data.map(function(oo) {
			oo.index = i + 1;
			oo.vorder = parseInt(oo.morder);
			if(typeof oo.maxpax !== 'string') 
				oo.maxpax = "0";
			oo.maxpax = parseInt(oo.maxpax);
			oo.startv = oo.start.jsdate().getTime();
			oo.endv = oo.end.jsdate().getTime();
			if(oo.display==='0000-00-00'){
				oo.display = todaydate.getDateFormatReverse('-');
				oo.dpdate = todaydate.getDateFormat('-');
				oo.displayv = todaydate.getTime();		   
			}else{
				oo.displayv = oo.display.jsdate().getTime();
				oo.dpdate = oo.display.jsdate().getDateFormat('-');			  
				}
			oo.sddate = oo.start.jsdate().getDateFormat('-');
			oo.eddate = oo.end.jsdate().getDateFormat('-');
			oo.bkpax = 0;
			oo.bkevent = [];
			oo.featured = (oo.is_homepage==='1') ? true : false;
			if(bkflag === 1) {
				bkdata.map(function(vv) {
					if(vv.eventID === oo.eventID) {
						if(vv.status !== 'cancel')
							oo.bkpax += parseInt(vv.pax);
						vv.fullname = vv.firstname + ' ' + vv.lastname;
						oo.bkevent.push(vv);
						}
					});
				}
			$scope.names.push(new $scope.Objevent().replicate(oo));                    
			});
		$scope.getPDF();
		$scope.paginator.setItemCount($scope.names.length);
		$scope.initorder();
		});

	bookService.readMenu($scope.restaurant, $scope.email).then(function(response) {
		var i, data, oo;

		$scope.amenus = [];		
		if(response.status !== 1 && response.status !== "1")
			return;
		
		data = response.data.menus;
		for (i = 0; i < data.length; i++) {
			$scope.amenus.push(data[i].value + '-' + data[i].menuID);
			}
		oo = $scope.tabletitleContent.find(function(oo, index, arr) { return (oo.a === "menu"); });
		if(oo && oo.val) oo.val = $scope.amenus;
		});
        $scope.readEvBooking = function(confirmation){

        	return bookService.readEventBkDetails(confirmation).then(function(response) {
                    $scope.selectedBooking.tracking ='';
                    if(response.data){
                        var res = response.data;
                        $scope.selectedBooking.tracking = res;
                    }
                });
            };

	$scope.cleaninput = function(ll) {
		if(typeof $scope.selectedItem[ll] === 'string' && $scope.selectedItem[ll] !== '')
			$scope.selectedItem[ll] =  $scope.selectedItem[ll].replace(/\'|\"/g, '’');
		};
		
	$scope.reset = function(item) {
		$scope.listEventFlag = false;
		$scope.viewEventFlag = false
		$scope.createEventFlag = false;
		$scope.viewlistBkEventFlag = false;
		$scope.viewBkEventFlag = false;
		if(item === 'viewlistBkEventFlag') {
			$scope.paginatorsub.setItemCount($scope.selectedItem.bkevent.length);
			$scope.paginatorsub.setPage(0);
			}
		$scope[item] = true;	
		};

	$scope.backlisting = function(page) {
		if(page === undefined)
			$scope.reset('listEventFlag');
		else $scope.reset(page);
		};
		
	$scope.findaccount = function(name) {
		for(var i = 0; i < $scope.names.length; i++)
			if($scope.names[i].name === name)
				return i;
		return -1;
		};
	
	$scope.viewevbook = function(oo) {
            console.log(JSON.stringify(oo));
        
		$scope.selectedBooking = oo;
                $scope.selectedBooking.refund_amount = oo.amount;
                $scope.selectedBooking.payment_status = oo.status;
                $scope.getPaymentDetails(oo);
		$scope.reset('viewBkEventFlag');
                if(oo.object_id !== '')
                    $scope.readEvBooking(oo.object_id);
        };
			
	$scope.view = function(oo, item) {
		$scope.selectedItem = oo;
		if(item !== 'bkpax')
			$scope.reset('viewEventFlag');
		else $scope.reset('viewlistBkEventFlag');
		};
	
	$scope.create = function() {
		$scope.selectedItem = new $scope.Objevent();
		$scope.mydata_start.setDate(todaydate);
		$scope.mydata_endng.setDate(todaydate);
                $scope.mydata_display.setDate(todaydate);
		$scope.reset('createEventFlag');
		$scope.buttonlabel = "Save new event";
		$scope.action = "create";
		return false;
		}
				
	$scope.update = function(oo) {
     
		$scope.selectedItem = new $scope.Objevent().replicate(oo);
		$scope.mydata_start.originaldate = new Date($scope.selectedItem.start);
		$scope.mydata_endng.originaldate = new Date($scope.selectedItem.end);
                $scope.mydata_display.originaldate = new Date($scope.selectedItem.display);
		$scope.reset('createEventFlag');
		$scope.buttonlabel = "Update new event";
		$scope.action = "update";
		};

	$scope.savenewevent = function() {
		var u, msg, apiurl, ind, oo = $scope.selectedItem;
		
		oo.clean();		
		if(oo.check() < 0)
			return;
		            
		oo.startv = oo.start.jsdate().getTime();
		oo.endv = oo.end.jsdate().getTime();
        oo.displayv = oo.end.jsdate().getTime();
		oo.vorder = parseInt(oo.morder);
		if($scope.atype.indexOf(oo.type) < 0)
			oo.type = $scope.atype[0];
		if(typeof oo.morder !== "number" || oo.morder === 0 || oo.morder === "")
			oo.morder = oo.vorder = Math.max.apply(Math,$scope.names.map(function(o){return o.morder;})) + 1;

			
		if($scope.action === "create"){
			if($scope.findaccount(oo.name) >= 0) {
				alert("name " + oo.name + " already exists. Please choose another name !");
				return;
				}				
           	 bookService.logevent(170,'BO_EVENT_creation','BO_EVENT');
		 bookService.createEvent($scope.restaurant, $scope.email, oo ).then(function(response) { 
			if(response.status > 0) {
				oo.index = ($scope.names.length > 0) ? $scope.names[$scope.names.length - 1].index + 1 : 1;
				oo.eventID = response.status;
				alert("Event has been created with ID "+oo.eventID); 
				$scope.names.push(oo);
				}
			});
           	 }
		else {
			bookService.logevent(171,'BO_EVENT_update','BO_EVENT');
			bookService.updateEvent($scope.restaurant, $scope.email, oo ).then(function(response) { 
				if(response.status > 0) {
					ind = $scope.findaccount(oo.name);	
					if(ind >= 0) $scope.names.splice(ind, 1);
					$scope.names.push(oo);
					alert("Event has been updated");
					}
				});
         		 }
		
 		$scope.backlisting();		
		};
		
	$scope.delete = function(oo) {
		if(confirm("Are you sure you want to delete " + oo.name) == false)
			return;
			
		bookService.deleteEvent($scope.restaurant, $scope.email, oo.name).then(function() { 
                    bookService.logevent(171,'BO_EVENT_delete','BO_EVENT');
			oo.remove();
			alert(oo.name + " has been deleted"); 
			});
		
		};
		
	$scope.extractSelection = function() {

		var maxlimit = 1500, tt, sep, sep1, i, j, k; // ajax call might not support more data
		var model = $scope.tabletitlebook, exportselect = $scope.filteredBook;
		var limit = exportselect.length;

		if (limit > maxlimit) limit = maxlimit;
		tt = '';
		for (i = 0, sep = ''; i < limit; i++, sep = ', ') {
			u = exportselect[i];
			tt += sep + '{ ';
			for(j = 0, sep1 = ''; j < model.length; j++, sep1 = ', ') {
				k = model[j].a;
				tt += sep1 + '"' + k + '":"' + u[k] + '"';
				}
			tt += ' }';
			}
		tt = '{ "booking":[' + tt + '] }';
		$scope.content = tt;
		$('#content').val(tt);
		$('#extractForm').submit();
		};
                
	$scope.getPDF = function() {
		var url = '../api/restaurant/media/picture/' + $scope.restaurant + "/pdf";
		$http.get(url).success(function(response) {	
			$scope.documentdpf = [];
			response.data.map(function(oo) { $scope.documentdpf.push(oo.name); });
			var index = $scope.tabletitleContent.inObject("a", "pdf_link");
			if(index >= 0) 
				$scope.tabletitleContent[index].val = $scope.documentdpf;
			});
		};
	                
             //event booking payment details
        $scope.getPaymentDetails = function(oo){
            
            bookService.getEventPayment(oo.paykey).then(function(response) { 
                if(response.status === 1 && response.data.payment){
                    if(response.data.payment.status === 'REFUNDED'){
                        $scope.selectedBooking.refund_amount = response.data.payment.refund_amount;
                    }
                    $scope.selectedBooking.payment_status = response.data.payment.status;
                    $scope.selectedBooking.payment_method = response.data.payment.payment_method;
                   
                }

            });
            
        };
             
        $scope.depositRefund = function(oo) {
            if(parseInt(oo.amount) >= parseInt(oo.refund_amount)) { 
                    if (confirm("by cancelling the Event " + oo.orderID + ", you need refund to guest SGD "+ oo.refund_amount ) == false)
                            return;
                bookService.evDepositRefund($scope.restaurant,  oo.orderID,$scope.email,oo.refund_amount,'stripe').then(function(response) { 
                    if(response.data.status === 1){
                        $scope.selectedBooking.payment_status = 'REFUNDED';
                        $scope.selectedBooking.refund_amount = oo.refund_amount;
                       alert('The transaction has been completed.' );
                    }else{
                        alert('refund Failed :'+ response.data.error);
                    }
                        
                });
            }else{
                alert("IMPT:Refund amount shall not exceed the initial deposit payment. ");
                return;
            }
        };
	
			

}]);

	
</script>

