<div id='booking' ng-controller='BKAnalyticsController' ng-init="moduleName='analytics'; typeBooking=true;bookingTitle='BOOK OF THE DAY'" >
 <div class="container mainbox">    

<div class="row">

<div class="col-sm-3">
<div class="input-group">
	<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
	<input type="text" class="form-control input-sm" ng-model="searchText" ng-change="paginator.setPage(0);" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" style='width:150px;'> 
</div>
<span style='font-size:10px;'>selected days: <strong> {{filteredBooking.length}} </strong></span>
<span ng-if="debugcount >= 0" style='font-size:10px;margin-left:25px;'><strong> ed: {{debugcount}} </strong> </span>
</div>

<div class="col-sm-9">
<ul class="nav navbar-nav">

<li style="margin-left:10px;">
	<button  type="button" class="btn btn-warning btn-sm" ng-click='toggleFormat();' style='color:black;'>select {{ cformat }} date &nbsp;<span class='glyphicon glyphicon-eye'></button>
</li>


<li style="margin-left:10px;">
<div class="input-group">
	<span><button  type="button" class="btn btn-default" ng-click="mydatastart.dateopen($event)"><i class="glyphicon glyphicon-calendar"><span style='font-size:9px;'><br />month</span></i></button></span>
    <input type="text" class="form-control" uib-datepicker-popup="{{ mydatastart.formats[0] }}" ng-model="mydatastart.originaldate"  ng-change='mydatastart.onchange();' is-open="mydatastart.opened" min-date="mydatastart.minDate" max-date="mydatastart.maxDate" datepicker-options="mydatastart.dateOptions" ng-required="true" close-text="Close" style="width:0;opacity:0" />
</div>
</li>


<li style="margin-left:20px;">
	<button type="button" class="btn btn-primary btn-sm" ng-click='readAnalyticBooking(-1, -1);' style='color:white;' >Last 31 days &nbsp;<span class='glyphicon glyphicon-eye'></button>
</li>

<li style="margin-left:20px;">
	<button  type="button" class="btn btn-success btn-sm" ng-click='extractSelection();' style='color:white;'>Extract Selection &nbsp;<span class='glyphicon glyphicon-save'></span></button>
</li>

<li style="margin-left:20px;">
	<div class="btn-group" uib-dropdown >
		<button type="button" class="btn btn-default btn-xs" uib-dropdown-toggle aria-expanded="false" style='font-size:11px;'>Page Size <span class="caret"></span></button>
		<ul class="dropdown-menu" uib-dropdown-menu role="menu">
			<li ng-repeat="x in paginator.pagerange()" class='glyphiconsize'><a href ng-class="{ 'glyphicon glyphicon-ok glyphiconsize': x === paginator.getRowperPage() }" href ng-click="paginator.setRowperPage(x)"> {{x}}</a></li>
		</ul>
	</div> 		
</li>
</ul>
</div>
</div>

<div class="row" style="font-size:11px;margin-left:20px;">
<br />
<span ng-if="startBkg !== -1">current month: <strong> {{ curmonthname }}  </strong></span>
<span style="margin-left:20px;">type: <strong>{{ format }} date </strong></span>
</div>
<br />
<!-- give more information for a specific day -->
<div class="col-md-12" ng-hide='listingVisitFlag'>
	<a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a>
	<br />
	<table class='table-striped' ng-repeat="x in SelectedItem" style="margin: 0 0 150px 30px;font-size:13px;font-family: Roboto">		
		<tr><td>&nbsp;</td><td> &nbsp; </td><td></td></tr>
	</table>
</div>

<div class="col-md-12" ng-show='listingVisitFlag'>
	
<table width='100%' class="table table-condensed table-striped" style='font-size:11px;'>
	<thead><tr><th ng-repeat="y in tabletitle"><tbtitle var="{{y.a}}" name='tabletitle' module='moduleName'/></th></tr></thead>
	<tbody>
	<tr ng-repeat="x in filteredBooking = (booking | filter:searchText) | sizefilter:this | orderBy:predicate:reverse | slicefilter:paginator.getPage():paginator.getRowperPage():paginator.getItemCount() | limitTo:paginator.getRowperPage()">
		<td ng-repeat="y in tabletitle">{{ x[y.a] | adatereverse:y.c}}</td>
	</tr>
	<tr><td colspan='{{tabletitle.length}}'></td></tr>
</table>

<div ng-if="filteredBooking.length >= paginator.getRowperPage()" class="col-md-12" align="center" style='font-size:11px' ng-include="'inc/paginator.html'"></div>

</div>
</div>
</div>


<script>


<?php
echo "var token = '" . $_SESSION['user_backoffice']['token'] . "';";
echo "var email = '" . $_SESSION['user_backoffice']['email'] . "';";
?>

app.controller('BKAnalyticsController', ['$scope', '$http', 'bookService', 'extractService', function($scope, $http, bookService, extractService) {

	$scope.paginator = new Pagination(50);
	$scope.booking = [];
	$scope.prebook = [];
	$scope.getAlignment = bkgetalignment;
	$scope.reporttype = "co";
	$scope.restaurant = <? printf("'%s';", isset($theRestaurant) ? $theRestaurant : ''); ?>
	$scope.whenday = 32;
	var forceDate = new Date(new Date().getFullYear()-2, 0, 1);
	
	month = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
	
	var newdate = new Date();
	var startdate, endate;
	if(newdate.getMonth() > 6) {
		startdate = new Date(newdate.getFullYear(), 0, 1);
		endate = new Date(newdate.getFullYear()+1, 11, 31);
	} else {
		startdate = new Date(newdate.getFullYear()-1, 0, 1);
		endate = new Date(newdate.getFullYear(), 11, 31);
		}
	$scope.startBkg = -1;
	$scope.endBkg = newdate.getTime();
	$scope.cdate = newdate;
	$scope.month = $scope.year = -1;

	$scope.onchangestart = function() {
		var tt, dd;
		dd = new Date($scope.mydatastart.originaldate.getFullYear(), $scope.mydatastart.originaldate.getMonth(), 1); 
		if(dd instanceof Date) {
			$scope.startBkg = dd.getTime(); 
			tt = dd.getMonth();
			if(tt >= 0 && tt < 12) {
				$scope.curmonthname = month[tt] + " " + dd.getFullYear();
				$scope.readAnalyticBooking(tt, dd.getFullYear());
				}
			} 
		};
		
	$scope.mydatastart = new bookService.ModalDataBooking();
	$scope.mydatastart.setInit(forceDate, "09:00", $scope.onchangestart, 3, startdate, endate);

	$scope.debugcount = -1;
	$scope.email = <?php echo "'" . $email . "';"; ?>
	$scope.listingVisitFlag = true;
	$scope.predicate = '';
	$scope.format = "create";
	$scope.cformat = "booking";
	$scope.reverse = false;
	$scope.tabletitle = [ {a:'index', b:'index', c:'', q:'down', cc: 'black' }, { alter:'datetime', a:'thedate', b:'CreateDate', c:'date', q:'up', cc: 'orange' }, {a:'booking', b:'Trans', c:'', q:'down', cc: 'black' }, {a:'valid', b:'Net', c:'', q:'down', cc: 'black' }, {a:'weeloy', b:'Weeloy', c:'', q:'down', cc: 'black' }, {a:'cctms', b:'CC/Tms', c:'', q:'down', cc: 'black' },  {a:'website', b:'Website', c:'', q:'down', cc: 'black' },  {a:'facebook', b:'Facebook', c:'', q:'down', cc: 'black' },  {a:'walkin', b:'Walkin', c:'', q:'down', cc: 'black' },  {a:'waiting', b:'Waiting', c:'', q:'down', cc: 'black' },  {a:'remote', b:'Thirdparty', c:'', q:'down', cc: 'black' },  {a:'win', b:'Wins', c:'', q:'down', cc: 'black' }, {a:'cancel', b:'Cancel', c:'', q:'down', cc: 'black' },  {a:'noshow', b:'No Show', c:'', q:'down', cc: 'black' },  {a:'pax', b:'Pax', c:'', q:'down', cc: 'black' },  {a:'lunch', b:'L', c:'', q:'down', cc: 'black' },  {a:'dinner', b:'D', c:'', q:'down', cc: 'black' } ];
	$scope.bckups = $scope.tabletitle.slice(0);
	
	 
	 $scope.compare = function(a, b) {
	 	return (b.datetime - a.datetime);
	 	}

	 $scope.analyticsObj = function(index, oo) {
	 	return {
			index: index,
			thedate: oo.thedate,
			datetime: oo.datetime,
			lunch: 0,
			dinner: 0,
			pax: 0,
			booking: 0,
			facebook: 0,
			website: 0,
			weeloy: 0,
			tms: 0,
			callcenter: 0,
			cctms: 0,
			walkin: 0,
			waiting: 0,
			remote: 0,
			valid: 0,
			win: 0,
			cancel: 0,
			noshow: 0,
			expired:0
			};	 			
	 	}
	 		
	$scope.initorder = function() {
		$scope.tabletitle = $scope.bckups.slice(0);
		$scope.predicate = "datetime";
		$scope.reverse = true;
	};

	$scope.reorder = function(item, alter) {
		alter = alter || "";
		if (alter !== "")  item = alter;
		$scope.reverse = ($scope.predicate == item) ? !$scope.reverse : false;
		$scope.predicate = item;
	};

	$scope.changetitlefield = function(field, val) {
		var index = -1;
		$scope.tabletitle.some(function(oo, ind) { if(oo.a === field) { index = ind; return true; } return false; });
		if(index >= 0)
			$scope.tabletitle[index].b = val;
		};
		
	$scope.addtitlefield = function(field, oo) {
		var index = -1;
		$scope.tabletitle.some(function(oo, ind) { if(oo.a === field) { index = ind; return true; } return false; });
		if(index >= 0)
			$scope.tabletitle.splice(index+1, 0, oo);
		};
			
	$scope.removetitlefield = function(fieldAr) {
		fieldAr.map(function(field) {
			var index = -1;
			$scope.tabletitle.some(function(oo, ind) { if(oo.a === field) { index = ind; return true; } return false; });
			if(index >= 0)
				$scope.tabletitle.splice(index, 1);
			});
		};
			
	$scope.hidetitlefieldifempty = function(ar, fieldAr) {
		fieldAr.map(function(field) { 
			if(ar.length < 1 || ar.indexOf(field) < 0)
				return;
			$scope.removetitlefield([field]);
			});
		};
		
	$scope.readAnalyticBooking = function(mm, yy) {
		var labelAr;
		var xyformat = ($scope.format === "create") ? "x" : "y";
		var format = ($scope.format === "create") ? "c" : "r";
		
		$scope.month = mm;
		$scope.year = yy;
		
		if(mm < 0 || yy < 0)
			$scope.reporttype = format + "oa";
		else $scope.reporttype = xyformat + "oa" + "|" + mm + "|" + yy;
		
		bookService.readAnalytics($scope.whenday, $scope.reporttype, $scope.email, $scope.restaurant).success(function(response) { 	 
			 var obj, x, i, datetime, index, k = 0, validbkg, product, brules = { Cou:19, Che:8, Bar: 6, PDR: 14}, burntends = ($scope.restaurant === "SG_SG_R_BurntEnds");
			 $scope.booking = [];
			 $scope.prebook = [];
			 $scope.productlist = [];

           		 if(response.status < 0)
            			return;

			 $scope.rdata = response.data.bookings; 
			 for(i = 0; i < $scope.rdata.length; i++) {
				obj = $scope.rdata[i];
				obj.index = i+1; 
				obj.pax = parseInt(obj.pax);
				if($scope.format === "create") {
					obj.thedate = obj.cdate;
					obj.datetime =  obj.cdate.jsdate().getTime();
					}
				else {
					obj.thedate = obj.rdate;
					obj.datetime =  obj.rdate.jsdate().getTime();
					}
				$scope.prebook.push(obj);
				}

			 if($scope.prebook.length < 1) 
				return;

			 $scope.prebook.sort($scope.compare);
		 	 x = $scope.prebook[0];
			 index = 1;
			 obj = new $scope.analyticsObj(index++, x);
			 datetime = obj.datetime;
			 $scope.prebook.map(function(x) {
				if(datetime !== x.datetime) {
					obj.cctms = obj.tms + obj.callcenter;
					$scope.booking.push(obj);
					obj = new $scope.analyticsObj(index++, x);
					}
				datetime = x.datetime;	
				obj.booking++;
				validbkg = (x.status !== "cancel" && x.status !== "noshow" && x.state !== "no show" && x.status !== "expired");
				   
				if(x.status === "cancel") obj.cancel++;
				else if(x.status === "noshow" || x.state.indexOf("no show") >= 0) obj.noshow++;
				else if(x.status === "expired") obj.expired++;
				else if(x.tracking.indexOf("remote") >= 0) obj.remote++;
				else if(x.tracking.indexOf("website") >= 0) obj.website++;
				else if(x.tracking.indexOf("facebook") >= 0) obj.facebook++;
				else if(x.tracking.indexOf("walkin") >= 0) obj.walkin++;
				else if(x.tracking.indexOf("waiting") >= 0) obj.waiting++;
				else if(x.tracking.indexOf("tms") >= 0) obj.tms++;
				else if(x.tracking.indexOf("callcenter") >= 0) obj.callcenter++;
				else { obj.weeloy++; }
				if(validbkg) {
					obj.lunch += x.lunch;
					obj.dinner += x.dinner;
					obj.pax += x.pax;
					}

				if(x.wheelwin !== '') obj.win++;
				if(burntends && x.product !== '' && validbkg) {
					product = x.product.replace(/\s+.*/g, "");
					if(typeof obj[product] !== "string") {
						if($scope.productlist.indexOf(product) < 0)
							$scope.productlist.push(product);
						obj[product+"_L"] = obj[product+"_D"] = 0;
						}
					if(x.lunch === 1) obj[product+"_L"] += x.pax;
					else obj[product+"_D"] += x.pax;
					obj[product] = obj[product+"_L"] + " | " + obj[product+"_D"];
					if($scope.format === "booking") {
						Object.keys(brules).some(function(ll) { 
							if(product.substr(0, 3) === ll) { 
								if(obj[product+"_L"] > brules[ll] || obj[product+"_D"] > brules[ll]) 
									obj[product] += " | ***"; 
								return true; 
								} 
							return false; });
						}
					}

				if(validbkg && x.tracking.indexOf("callcenter") < 0)
					obj.valid++;
				});

			obj.pax += x.pax;
			obj.cctms = obj.tms + obj.callcenter;
			$scope.booking.push(obj);

			labelAr = ["walkin", "remote", "callcenter", "remote", "waiting", "weeloy", "tms", "win", "cctms", "website", "facebook", "noshow"];
			$scope.booking.some(function(oo) {
				if(labelAr.length > 0)
					labelAr = labelAr.filter(function(ll) { return (typeof oo[ll] !== "number" || oo[ll] === 0); });
				return (labelAr.length < 1);
				})
			console.log('FILTER', labelAr);	

			$scope.paginator.setItemCount($scope.booking.length);
			$scope.paginator.setPage(0);
			$scope.initorder();
			console.log('INIT', $scope.tabletitle.slice(0));

			$scope.hidetitlefieldifempty(labelAr, ['win', 'callcenter']);
				
			if($scope.restaurant === "SG_SG_R_BurntEnds") {
				$scope.hidetitlefieldifempty(labelAr, ['cctms', 'walkin', 'waiting', 'facebook', 'remote', 'weeloy', 'noshow']);
				$scope.removetitlefield(['booking', 'valid']);
				console.log('PRODUCT', $scope.productlist);
				$scope.productlist.sort();
				$scope.booking.forEach(function(oo) { $scope.productlist.forEach(function(l) { if(typeof oo[l] !== "string") oo[l] = '-'; }); });
				$scope.productlist.forEach(function(ll) {
					$scope.addtitlefield('website', {a: ll, b:ll, c:'', q:'down', cc: 'black' });	//Object.assign
					});
				console.log($scope.tabletitle);
				}
			});
		
		};
	
	
	$scope.readAnalyticBooking(-1, -1);
	
	 $scope.toggleFormat = function() {
	 	if($scope.format === "create") {
	 		$scope.format = "booking";
		 	$scope.cformat = "create";
		 	$scope.changetitlefield("thedate", "BookDate");
		 	}
		 else {
		 	$scope.format = "create";
		 	$scope.cformat = "booking";
		 	$scope.changetitlefield("thedate", "CreateDate");
		 	}
		$scope.readAnalyticBooking($scope.month,  $scope.year); 
	 	};
	 		 

	bookService.getdebugErrorCount($scope.email).success(function(response) { $scope.debugcount = parseInt(response.data); console.log('DEBUG', $scope.debugcount); });
			
	$scope.backlisting = function() {
		$scope.listingVisitFlag = true;
		};
		
	$scope.extractSelection = function() {		
		var i, j, u, data, maxlimit = 500, limit, titleAr, contentAr;
		var filename, cdate = new Date();
		var exportselect = $scope.filteredBooking;

		titleAr = ["index", "date", "booking", "net", "pax", "lunch", "dinner", "weeloy", "cc/tms", "website", "facebook", "walkin", "waiting", "thirdpary", "win", "cancel", "noshow"];
		contentAr = ["index", "thedate", "booking", "valid", "pax", "lunch", "dinner", "weeloy", "cctms", "website", "facebook", "walkin", "waiting", "remote", "win", "cancel", "noshow"];

		data = "";
		data += titleAr.join(",") + ",";
		data += 'mode: ' + $scope.format + " date" + ",,";
		if($scope.month >= 0)
			data += $scope.year + "," + month[$scope.month] + ",";
		limit = exportselect.length;		
		if (limit > maxlimit) limit = maxlimit;
		for (i = 0; i < limit; i++) {
			data += "\n";			
			u = exportselect[i];
			for (j = 0; j < contentAr.length; j++) 
				data += extractService.filter(u[contentAr[j]]) + ",";			
			}	
	
		filename = "analytics" + cdate.getDate() + cdate.getMonth() + cdate.getFullYear() + ".csv";
		extractService.save(filename, data); 				
		};
		

}]);

</script>
