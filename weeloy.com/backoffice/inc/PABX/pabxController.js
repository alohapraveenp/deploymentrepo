app.controller('PABXController',  ['$scope', 'CallInfo', function ($scope, CallInfo) {
	$scope.paginator = new Pagination(25);
	$scope.token = $('#token').val();
	$scope.tabletitle = [
		{a:'index', b:'n', c:'', l:'25', q:'down', cc: 'black' },
		{a:'Restaurant', b:'Restaurant', c:'', q:'down', cc: 'black' },
		{a:'Number', b:'Number', c:'', q:'down', cc: 'black' },
		{a:'Caller', b:'Caller', c:'', q:'down', cc: 'black' },
		{a:'CallCentre', b:'Call Centre', c:'', q:'down', cc: 'black' }, 
		{a:'Status', b:'Status', c:'', q:'down', cc: 'black' }, 
		{a:'Start', b:'Start', c:'',  q:'down', cc: 'fuchsia' },
		{a:'End', b:'End', c:'', q:'down', cc: 'black' },
	];
	$scope.predicate = '';
	$scope.reverse = false;
	$scope.readCall = function() {
		CallInfo.getCalls().then(function (response) {
		  if (response.status == 1) {
			console.log("SuccessFully retrieved " + response.data.length + " calls");
			$scope.records = [];
			var data = response.data;
			var calls = {};
			var i = 0;
			angular.forEach(data, function(value, key, obj) {
				if (value.convID != null && value.convID.length > 0) {
					if (!calls.hasOwnProperty(value.convID)) {
					  calls[value.convID] = new Object();
					  calls[value.convID].index = i++;
					  calls[value.convID].Restaurant = value.restaurant;
					  calls[value.convID].Number = value.virtual;
					  calls[value.convID].Caller = value.fromline;
					  calls[value.convID].CallCentre = value.fixline;
					  calls[value.convID].Start = value.cdate;
					} else {
					  calls[value.convID].End = value.cdate;
					}
					calls[value.convID].Status = value.type;
					if (calls[value.convID].Status === "report")
					  calls[value.convID].Status = "Answered";
				  }
			});
			for (var key in calls) {
				//console.log(calls[key]);
				$scope.records.push(calls[key]);
			}
			console.log($scope.records.length + " calls: ");// +$scope.records);
			$scope.predicate = "Start";
			$scope.reverse = true;
		  } else
			console.log("Failed to retrieve calls: " + JSON.stringify(response));
		});
	}
	$scope.readCall();
	$scope.reorder = function (item, alter) {
		alter = alter || "";
		if (alter !== "")
			item = alter;
		$scope.reverse = ($scope.predicate === item) ? !$scope.reverse : false;
		$scope.predicate = item;
	};
}]);