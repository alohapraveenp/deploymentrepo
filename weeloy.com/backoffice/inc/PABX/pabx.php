<?php
$baseUrl = __BASE_URL__;
$res = new WY_restaurant;
$res->getRestaurant($theRestaurant);
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div ng-include="'inc/myModalModif.html'"></div>
<div class="container" ng-controller="PABXController" ng-init="moduleName='pabx'; formatDisplay = 'standard';">
    <input type='hidden' id='restaurant' value ="<?php echo $theRestaurant ?>" />
    <input type='hidden' id='token' value ="<?php echo $_SESSION['user_backoffice']['token'] ?>" />
    <input type='hidden' id='email' value ="<?php echo $_SESSION['user_backoffice']['email'] ?>" />
    <div class='row'>
        <div class="col-md-2">
            <div class="form-group" style='margin-bottom:25px; width:200px;'>
                <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                    <input type="text" class="form-control input-sm" ng-model="searchText" ng-change="paginator.setPage(0);" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"/>  
                </div>
            </div>
        </div>
        <div class="col-md-10">
            </form>
            <ul class="nav navbar-nav">
            <li>
                <div class="btn-group" style='font-size:11px;margin-left:100px;' uib-dropdown>
                    <button type="button" class="btn btn-default btn-xs" uib-dropdown-toggle aria-expanded="false">Page Size <span class="caret"></span></button>
                    <ul class="dropdown-menu" uib-dropdown-menu role="menu">
                        <li ng-repeat="x in paginator.pagerange()" class='glyphiconsize'><a href ng-class="{ 'glyphicon glyphicon-ok glyphiconsize': x === paginator.getRowperPage() }" ng-click="paginator.setRowperPage(x)"> {{x}}</a></li>
                    </ul>
                </div>      
            </li>
            </ul>
        </div>
    </div>  
    <div class="row">
        <div class="col-md-12 left-sec">
            <table width='100%' class="table table-condensed table-striped" style='font-size:12px;'>
                <thead>
            		<tr>
                        <th ng-repeat="y in tabletitle">
                            <tbtitle var="{{y.a}}" name='tabletitle' module='moduleName'></tbtitle>
                        </th>
            		</tr>
        		</thead>
        		<tbody style='font-family:helvetica;font-size:12px;'>
                    <tr ng-repeat="x in frecords = (records | filter:searchText) | sizefilter:this | orderBy:predicate:reverse | slicefilter:paginator.getPage():paginator.getRowperPage():paginator.getItemCount() | limitTo:paginator.getRowperPage()" style='font-family:helvetica;font-size:12px;' ng-click="view(x)">
                        <td ng-repeat="y in tabletitle" ng-class-odd="'odd'" ng-class-even="'even'">
                            {{ x[y.a] | adatereverse:y.c }}
                        </td>
                    </tr>
                    <tr>
                        <td colspan='{{tabletitle.length + 3}}'></td>
                    </tr>
                </tbody>
            </table>
            <div ng-if="frecords.length >= paginator.getRowperPage()" class="col-md-12" align="center" style='font-size:11px' ng-include="'inc/paginator.html'"></div>
        </div> 
    </div>
</div>
<script>
var token = <?php echo "'".$_SESSION['user_backoffice']['token']."';"; ?>
</script>
<script src="inc/PABX/pabxController.js?1"></script>
<script src="inc/PABX/pabxService.js"></script>