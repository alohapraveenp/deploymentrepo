app.controller('ContactInformationCtrl', ['$scope', 'ContactInfo', function ($scope, ContactInfo) {
        
        var RestaurantID =$scope.restaurant= $("#restaurant").val();
        var email = $("#email").val();
        $scope.predicate = '';
	$scope.reverse = false;
        $scope.smsnotify = $("#smsnotify").val();
        $scope.smsheader =  $("#smsheader").val(); 
        //console.log($scope.smsnotify);
        $scope.Objcontact = function () {
            console.log("scope . Objcontact called!");
            return {
                ID: '',
                restaurant: $scope.restaurant,
                job_title:'',
                firstname: '',
                lastname: '',
                email: '',
                mobile: '',
                notify_email: '',
                notify_sms: '',
                sms_header: ''
            };
        };

        //table definition
        $scope.tabletitle = [
            {a: 'job_title', b: 'Jobtitle', c: '', q: 'down', cc: 'fuchsia'},
            {a: 'firstname', b: 'Firstname', c: '', q: 'down', cc: 'fuchsia'},
            {a: 'lastname', b: 'Lastname', c: '', q: 'down', cc: 'black'},
            {a: 'email', b: 'Email', c: '', q: 'down', cc: 'black'},
            {a: 'mobile', b: 'Mobile', c: '', q: 'down', cc: 'black'},
           
      //            {a: 'notify_email', b: 'Notify Email', c: '', q: 'down', cc: 'black'},
            //{a: 'notify_sms', b: 'Notify Sms', c: '', q: 'down', cc: 'black'}
        ];
        if($scope.smsnotify == '1' ){
            $scope.tabletitle.push({a: 'notify_sms', b: 'Notify Sms', c: '', q: 'down', cc: 'black'});
        }
        if($scope.smsheader == '1' ){
             $scope.tabletitle.push({a: 'sms_header', b: 'Sms Header', c: '', q: 'down', cc: 'black'});
        }
        $scope.bckups = $scope.tabletitle.slice(0);
        //field list for update form
        $scope.tabletitleContent = [
            
            {a: 'job_title', b: 'Jobtitle', c: '', d: '', t: 'input'},
            {a: 'firstname', b: 'Firstname', c: '', d: '', t: 'input'},
            {a: 'lastname', b: 'Lastname', c: '', d: '', t: 'input'},
            {a: 'email', b: 'Email', c: '', d: '', t: 'input'},
            {a: 'mobile', b: 'Mobile', c: '', d: '', t: 'input'},
            {a: 'notify_email', b: 'Notify Email', c: '', d: '', t: 'checkbox1'},
          
            //{a: 'notify_sms', b: 'Notify Sms', c: '', d: '', t: 'checkbox'}
        ];
        if($scope.smsnotify == '1'){
            $scope.tabletitleContent.push({a: 'notify_sms', b: 'Notify Sms', c: '', d: '', t: 'checkbox'});
        }
        if($scope.smsheader == '1'){
            $scope.tabletitleContent.push({a: 'sms_header', b: 'Sms Header', c: '', d: '', t: 'smscheck'});
        }

        //get contact list
        ContactInfo.getContactInfo(RestaurantID, email)
                .then(function (response) {
                    if (response.status == 1) {
                        $scope.contacts = response.data;
                    }
                    $scope.initorder();
                });
        
        $scope.savecontact = function () {
            console.log(this.selectedItem);
    
            ContactInfo.saveContact(RestaurantID, email, this.selectedItem)
                    .then(function (response) {
                        if (response.status == 1) {
                            if(!$scope.selectedItem.ID){
                                $scope.selectedItem.ID = response.data;
                                $scope.contacts.push($scope.selectedItem);
                            }
                             $scope.switchview('list',$scope.selectedItem);
                        }
                    });
        };
        
        $scope.deletecontact = function (object) {
            if (confirm("Are you sure you want to delete " + object.email) == false) {
                return;
            }
            ContactInfo.deleteContact(RestaurantID, email, object).then(function (response) {
                if (response.status == 1) {
                    var index = $scope.contacts.indexOf(object);
                    $scope.contacts.splice(index, 1);    
                }
                else{
                    alert('contact ' + object.email + ' has NOT been deleted. ' + response.errors);
                }
            });
        };

        $scope.initorder = function () {
            $scope.tabletitle = $scope.bckups;
            $scope.predicate = "firstname";
            $scope.reverse = false;
        };
        $scope.reorder = function (item, alter) {
            alter = alter || "";
            if (alter !== "")
                item = alter;
            $scope.reverse = ($scope.predicate === item) ? !$scope.reverse : false;
            $scope.predicate = item;
        };

        $scope.switchview = function (view, object) {
              if (view === 'edit') {
                $scope.selectedItem = new $scope.Objcontact();
                $scope.editModeDisplay = true;
            } else {
                if (view === 'update') {
      
                    $scope.selectedItem = object;
                    $scope.editModeDisplay = true;
                }else {
                    $scope.editModeDisplay = false;
                }
            }
        };
        


    }]);

