app.service('ContactInfo', ['$http', '$q', function ($http, $q) {
        this.getContactInfo = function (restaurant, email) {
            return $http.post("../api/services.php/contact/list/",
                    {
                        'restaurant': restaurant,
                        'email': email,
                        'token': token
                    }).then(function (response) {
                return response.data;
            });
        };
        
        this.saveContact = function (restaurant, email, contact) {

            return $http.post("../api/services.php/contact/save/",
                    {
                        'restaurant': restaurant,
                        'email': email,
                        'token': token,
                        'contact':contact,
                    }).then(function (response) {
                
                return response.data;
            });
        };
        
        this.deleteContact = function (restaurant, email, contact) {
            return $http.post("../api/services.php/contact/delete/",
                    {
                        'restaurant': restaurant,
                        'email': email,
                        'token': token,
                        'contact':contact
                    }).then(function (response) {
                return response.data;
            });
        };
    }]);
