/*

var currentday = <?php echo "'" . date("Y-m-d") . "'"; ?>;
var currentmonth = <?php echo date("m"); ?>;
var currentyear = <?php echo "'" . date("Y") . "'"; ?>;
var currentweek = <?php echo intval(date("W")); ?>;

var aday = {
	init: function(aDate) {
		dateAr = aDate.split('-');
		this.date = new Date(dateAr[0], parseInt(dateAr[1]) - 1, dateAr[2], 0, 0, 0);
		this.tmp = new Date();
		},
	toDays: function(a2Date) {
		dateAr = a2Date.split('-');
		date2 = new Date(dateAr[0], parseInt(dateAr[1]) - 1, dateAr[2], 0, 0, 0);
		datediff = date2.getTime() - this.date.getTime();
		return (datediff / (24 * 60 * 60 * 1000));
		},
	toWeek: function(dd) {
		this.date = new Date(this.tmp.getFullYear(),0,1);
		return Math.ceil((this.toDays(dd) + 4) / 7);
		}
	};

aday.init(currentday);
var selectionAr;

var app = angular.module("myApp", []);

app.filter('slice', function() {
  return function(arr, currentPage, itemsPerPage) {
  	if(arr == undefined) return; 
    return arr.slice(currentPage*itemsPerPage, (currentPage+1)*itemsPerPage);
  }
});

app.directive('tbtitle',function(){
  return{
    restrict: 'AE',
    replace: true,
    template: '<a href="javascript:;" id="{{y.a}}" ng-click="reorder(y.a);"><span id="{{y.a}}_span">{{ y.b }}<i class="fa fa-caret-down"></span></a>',
    link: function(scope,elem,attrs){
      elem.bind('click',function(){
       	item = elem.attr('id');
      	content = elem.text();
        //elem.css('color', scope.titlecolor);
      });
    }
  }
});

app.controller('MainController', function($scope, $http) {

	$scope.itemsPerPage = 100;
  	$scope.currentPage = 0;
  	$scope.startIndex = 0;
  	$scope.currentPage = 0;
  	$scope.selectionSize = 0;
	$scope.restaurant= <?php echo "'" . $theRestaurant . "';"; ?>
	$scope.typeselection = 1;
  	$scope.tmpArr = [];
	$scope.predicate = 'date';
	$scope.oldpredicate = 'date';
	$scope.reverse = 'false';
	
	$scope.tabletitle = [ {'a':'index', 'b':'index' }, {'a':'first', 'b':'First Name' }, {'a':'last', 'b':'Last Name' },  {'a':'phone', 'b':'Mobile' },  {'a':'date', 'b':'Date' },  {'a':'time', 'b':'Time' },  {'a':'pers', 'b':'Cover' },  {'a':'booking', 'b':'Visit' },  {'a':'wheelwin', 'b':'Wins' } ];

	$scope.extratitle = [ {'a':'today', 'b':'Today' }, {'a':'week', 'b':'Current Week' },  {'a':'month', 'b':'Current Month' },  {'a':'future', 'b':'Future' }, {'a':'divider', 'b':'divider' }, 
						  {'a':'spin', 'b':'Used' }, {'a':'unspin', 'b':'Not Used' },  {'a':'cancel', 'b':'Cancelled' },  {'a':'uncancel', 'b':'Not Cancelled' }, {'a':'all', 'b':'All' }, {'a':'divider', 'b':'divider' }, 
						  {'a':'reserve', 'b':' by reservation date' }, {'a':'creation', 'b':' by creation date' } ];
	
	var url = '../api/visit/' + $scope.restaurant;
            
        $http.get(url).success(function(response) { 
    	$scope.names = response.data.visit; 
    	$scope.pageCount = Math.ceil($scope.names.length/100) - 1; 
    	$scope.selectionSize = $scope.names.length;
    	for(i = 0; i < $scope.names.length; i++) {
    		$scope.names[i].index = i+1; 
    		$scope.names[i].time = $scope.names[i].time.substr(0,5);
    		if($scope.names[i].wheelwin == "" && $scope.names[i].bkstatus == "cancel")
    			$scope.names[i].wheelwin = "cancel";
    		}
    	$scope.orgnames = $scope.names.slice(0);
		$scope.predicate = $scope.oldpredicate = "date";
		$scope.reverse = true;
		$scope.setContent("date");
    	});

  $scope.reorder = function(item) {
 	$scope.predicate = item;
	if($scope.oldpredicate == item)
		$scope.reverse = !$scope.reverse;
	else $scope.reverse = (item != 'date') ? false : true;
	$scope.oldpredicate = item;
	$scope.setContent(item);
	};	

  $scope.setContent = function(item) {
	titlecolor = ($scope.reverse) ? 'orange' : 'fuchsia';
    content = $('#'+item+"_span").text();
	$('th a span').css('color', 'black');
	$('#'+item+"_span").css('color', titlecolor);
	if($scope.reverse) $('#'+item+"_span").html(content + "<i class='fa fa-caret-up'>");
	else $('#'+item+"_span").html(content + "<i class='fa fa-caret-down'>");
	};	


  $scope.extraselect = function(item) {
  
  	$scope.current = $scope.names.slice(0);
	$scope.names = $scope.orgnames.slice(0);			
  	limit = $scope.names.length;
  	
  	$scope.tmpArr = [];
  	
  	switch(item) {
  	
  		case 'today': 
			for(i = limit - 1; i >= 0; i--) {
				dd  = ($scope.typeselection == 1) ? $scope.names[i].date : $scope.names[i].createdate;
				if(dd == currentday)
					$scope.tmpArr.push($scope.names[i]);
				}
				
			$scope.finish('Today');
			break;

  		case 'week': 
			for(i = 0; i < limit; i++) {
				dd  = ($scope.typeselection == 1) ? $scope.names[i].date : $scope.names[i].createdate;
				if(aday.toWeek(dd) == currentweek)
					$scope.tmpArr.push($scope.names[i]);
				}
			
			$scope.finish('current week');
			break;

  		case 'month': 
			for(i = 0; i < limit; i++) {
				dd  = ($scope.typeselection == 1) ? $scope.names[i].date : $scope.names[i].createdate;
				if(parseInt(dd.substring(5, 7)) == currentmonth)
					$scope.tmpArr.push($scope.names[i]);
				}
			
			$scope.finish('current month');
			break;

  		case 'future': 
			for(i = 0; i < limit; i++) {
				dd  = ($scope.typeselection == 1) ? $scope.names[i].date : $scope.names[i].createdate;
				if(today.toDays(dd) >= 0)
					$scope.tmpArr.push($scope.names[i]);
				}
			
			$scope.finish('coming bookings');
			break;

  		case 'spin': 
			for(i = 0; i < limit; i++) {
				if($scope.names[i].wheelwin != "")
					$scope.tmpArr.push($scope.names[i]);
				}
			
			$scope.finish('Spon');
			break;

  		case 'unspin': 
			for(i = 0; i < limit; i++) {
				if($scope.names[i].wheelwin == "" && $scope.names[i].wheelwin != "cancel")
					$scope.tmpArr.push($scope.names[i]);
				}
			
			$scope.finish('Unspin');
			break;

  		case 'cancel': 
			for(i = 0; i < limit; i++) {
				if($scope.names[i].bkstatus == "cancel")
					$scope.tmpArr.push($scope.names[i]);
				}
			
			$scope.finish('cancel');
			break;

  		case 'uncancel': 
			for(i = 0; i < limit; i++) {
				if($scope.names[i].bkstatus != "cancel")
					$scope.tmpArr.push($scope.names[i]);
				}
			
			$scope.finish('Uncancel');
			break;

  		case 'all': 
			$scope.names = $scope.orgnames.slice(0);			
			break;

		case 'reserve' :
			$scope.typeselection = 1;
			$('.glyphr').toggleClass('glyphicon glyphicon-ok');
			$('.glyphc').toggleClass('glyphicon glyphicon-ok');
			break;
			
		case 'create' :
			$scope.typeselection = 2;
			$('.glyphr').toggleClass('glyphicon glyphicon-ok');
			$('.glyphc').toggleClass('glyphicon glyphicon-ok');
			break;
						
		default:
			break;
		}
 

  };

  $scope.finish = function(mess) {

	if($scope.tmpArr.length > 0) {
		$scope.names = $scope.tmpArr.slice(0);
		zz = $scope.tmpArr.length?'s':'';
		alert('Search ' + mess + ': found ' + $scope.tmpArr.length + ' booking' + zz);
		}
	else {
		$scope.names = $scope.current.slice(0);
		alert('Search ' + mess + ': no booking selected');
		}

  }
  
  $scope.view = function(id) {
  
    $scope.SelectedItem = $scope.names.slice(id - 1, id);
    $scope.listingFlag = false;

  };

  $scope.backlisting = function() {
  
  	$scope.SelectedItem = -1;
    $scope.listingFlag = true;
  };

  $scope.spin = function(id) { 
  	state = $scope.names[id-1].wheelwin;
  	if(state != '') {
  		if(state == 'cancel')
  			alert("The reservation has been previously canceled");
  		else alert("The Wheel has already been spun! ("+ state + ")")
  		return false;
  		}

  	winp = window.open('../wheel/rotation/rotation.php?restaurant='+ $scope.restaurant + '&confirmation=' + $scope.names[id - 1].booking + '&restCode='+ $scope.names[id - 1].restCode + '&guestname='+ $scope.names[id - 1].first, 'weeloy','toolbar=no,width=1050,height=750,menubar=no,scrollbars=yes,resizable=no,alwaysRaised=yes'); 
  	winp.moveTo(15,25); 
  	winp.resizeTo(1050, 825); 
  	winp.focus();
  	return false;
  	}
  
  $scope.range = function() {
    var ret = [];
    for (i=0; i <= $scope.pageCount; i++) {
      ret.push(i);
    }
    return ret;
  };

  $scope.nextPage = function() {
    if ($scope.currentPage < $scope.pageCount) {
      $scope.currentPage++;
    }
  };

  $scope.numPage = function() {
    	return $scope.pageCount;
  };

  $scope.prevPage = function() {
    if ($scope.currentPage > 0) {
      $scope.currentPage--;
    }
  };

  $scope.setPage = function(n) {
    $scope.currentPage = n;
  };

  $scope.prevPageDisabled = function() {
    return $scope.currentPage === 0 ? "disabled" : "";
  };

  $scope.nextPageDisabled = function() {
    return $scope.currentPage === $scope.pageCount ? "disabled" : "";
  };
  

  $scope.$watch("filteredPeople.length", function(newvalue, oldvalue) { 
  	if($scope.filteredPeople == undefined) return; 
  	if($scope.selectionSize != $scope.filteredPeople.length) { 
  		$scope.selectionSize = $scope.filteredPeople.length;
  		$scope.currentPage = 0; 
  		$scope.pageCount = Math.ceil($scope.filteredPeople.length/100) - 1;  
  		} 
  });


  $scope.extractSelection = function() {
  
  	tt = sep = "";
  	limit = $scope.filteredPeople.length;
  	for(i = 0; i < limit; i++, sep = ', ') {
  		u = $scope.filteredPeople[i];
  		tt += sep + '{ "salutation":"' + u.salutation + '", "firstname":"' + u.first + '", "lastname":"' + u.last + '", "email":"'  + u.email + '", "phone":"' + u.phone + '", "confirmation":"' + u.booking + '", "restCode":"' + u.restCode + '", "status":"' + u.bkstatus + '", "win":"' + u.wheelwin + '", "date":"' + u.date + '", "arrival time":"' + u.time + '", "create date":"' + u.createdate + '"}';
   		}
   	tt = '{ "booking":[' + tt + '] }';
   	$scope.content = tt;
   	$('#content').val(tt);
   	$('#extractForm').submit();
  };

});

  function extractingSelection($scope) {
  
  	if($scope.filteredPeople == undefined) return []; 

  	selectionAr = [];
  	limit = $scope.filteredPeople.length;
  	for(i = 0; i < limit; i++) {
  		u = $scope.filteredPeople[i];
  		selectionAr[i] = [u.first, u.last, u.phone, u.booking, u.date, u.time];
  		}
  	alert(selectionAr[0]);
   	return selectionAr;
  };
*/