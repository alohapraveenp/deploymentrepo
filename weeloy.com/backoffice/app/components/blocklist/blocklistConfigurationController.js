app.controller('blockListConfigurationController', ['$scope', 'API','bookService', function ($scope, API,bookService) {
        $scope.paginator = new Pagination(25);
        var RestaurantID =$scope.restaurant= $("#restaurant").val();
        var email = $("#email").val();
         $scope.predicate = '';
	 $scope.reverse = false;
         $scope.createModeDisplay = false;
        
        
        //table definition
        $scope.tabletitle = [
            
            {a: 'email', b: 'Email', c: '', q: 'down', cc: 'black'},
            {a: 'mobile', b: 'Mobile', c: '', q: 'down', cc: 'black'},
            {a: 'firstname', b: 'Firstname', c: '',q: 'down', cc: 'black'},
            {a: 'lastname', b: 'Lastname', c: '', q: 'down', cc: 'black'},
            {a: 'status', b: 'Status', c: '',q: 'down', cc: 'black'},
           
        ];
     
        //field list for update form
        $scope.tabletitleContent = [
            {a: 'email', b: 'Email', c: '', d: '', t: 'input'},
            {a: 'mobile', b: 'Mobile', c: '', d: '', t: 'inputmobile'},
            {a: 'firstname', b: 'Firstname', c: '', d: '', t: 'input'},
            {a: 'lastname', b: 'Lastname', c: '', d: '', t: 'input'},
            {a: 'reason', b: 'Reason', c: '', d: '', t: 'textarea'},
            {a: 'status', b: 'Status', c: '', d: '', t: 'inputread'}
          
            //{a: 'notify_sms', b: 'Notify Sms', c: '', d: '', t: 'checkbox'}
        ];
        
        $scope.Objblocklist = function () {
            return {
                ID: '',
                restaurant: $scope.restaurant,
                email: '',
                firstname :'',
                lastname :'',
                reason:'',
                mobile: '',
                status: '',
                changestatus: '',
                
                replicate: function(obj) {
                    for (var attr in this)
                    if(this.hasOwnProperty(attr) && typeof this[attr] !== 'function' && attr !== '$$hashKey') 
                            this[attr] = obj[attr];
                    return this;
                    },
            };
        };
        $scope.bckups =$scope.tabletitle.slice(0);
        
        $scope.initorder = function () {
            $scope.tabletitle = $scope.bckups;
            $scope.predicate = "email";
            $scope.reverse = false;
        };
        
        $scope.reorder = function (item, alter) {
            alter = alter || "";
            if (alter !== "")
                item = alter;
            $scope.reverse = ($scope.predicate === item) ? !$scope.reverse : false;
            $scope.predicate = item;
        };
        
        
        $scope.getBlockList = function(){
            API.blocklistconfiguration.getBlockList(RestaurantID,email).then(function (response) {

                var i, data;

               if (response.data.status == 1) {
                    $scope.blockedlist = data = response.data.data;

                    for(i = 0; i < data.length; i++) {
                      data[i].changestatus = (data[i].status &&  data[i].status === 'blocked') ? 'Allow' :'Block';
                    }
                         console.log("in"+$scope.blockedlist);
               }
                $scope.initorder();

            }); 
        }
        $scope.getBlockList();
        $scope.savecontact = function () {
            $scope.mode= 'list';
         this.selectedItem.status ='blocked';
         
            $scope.saveBlocklist(RestaurantID,email,this.selectedItem.email,this.selectedItem.mobile,this.selectedItem.status,this.selectedItem.firstname,this.selectedItem.lastname,this.selectedItem.reason);
            $scope.switchview('list','');
        };
         $scope.cleantext = function (obj) {
            obj = obj.replace(/[!@#$%^&*()=}{\]\[\"\':;><\?/|\\]/g, '');
        
            return obj;
        }
        
        $scope.switchstatus = function(mode,x){
          
           
            var backofficeemail = email;
            $scope.mode= mode;
            var curstatus = (x.changestatus &&  x.changestatus === 'Block') ? 'blocked' :'allowed';
            $scope.saveBlocklist(RestaurantID,backofficeemail,x.email,x.mobile,curstatus,x.firstname,x.lastname,x.reason);
            x.status = curstatus;
            x.changestatus = (x.changestatus &&  x.changestatus === 'Block') ? 'Allow' :'Block';
        };
        $scope.saveBlocklist = function(RestaurantID,backofficeemail,email,mobile,status,firstname,lastname,reason){
            API.blocklistconfiguration.savelist(RestaurantID,backofficeemail,email,mobile,status,firstname,lastname,reason).then(function (response) {
                if(response.data.status == 1){
                    if($scope.mode === 'list'){
                        $scope.selectedItem.changestatus = (status &&  status === 'blocked') ? 'Allow' :'Block';
                        $scope.blockedlist.push($scope.selectedItem);
                   }
                    
                }
    
                
            });
        }
        $scope.switchview = function (view, object) {
            
              if (view === 'create') {
                $scope.selectedItem = new $scope.Objblocklist();
                $scope.selectedItem.status ='blocked';
                $scope.createModeDisplay = true;
                } else {
                
                    $scope.createModeDisplay = false;
                }
                     console.log("in"+$scope.blockedlist);
                
        };
        
        $scope.importlist = function (upfiles) {
       
                var file = document.getElementById('file_upload').files;

                var files =file[0];
                var filename = files.name;
		var ext = filename.substring(filename.lastIndexOf(".")+1).toLowerCase();
		if(ext != "csv" && ext != "xls" ) {
                alert("Invalid file type (jpg, jpeg, png, gif)");
                return false;
	        }
                // Create a formdata object and add the files
		var data = new FormData();
                data.append('files', files); 
		data.append('restaurant', RestaurantID);
                $.ajax({
                    url: 'importblocklist.php?files',
                    type: 'POST',
                    data: data,
                    cache: false,
                    dataType: 'json',
                    processData: false, // Don't process the files
                    contentType: false,
                    success: function(data, textStatus, jqXHR)
                        {
                        if( data.status === 1){
//                           var list = data.data ;
//                            for(var i = 0; i < list.length; i++) {
//                                list[i].changestatus = (list[i].status &&  list[i].status === 'blocked') ? 'Allow' :'Block';
//                                var value =list[i];
//                                $scope.blockedlist.push(new $scope.Objblocklist().replicate(value));
//                            }
                            $scope.getBlockList();
                             
                         }else {
                            alert('There was an error uploading your file');
                             $scope.switchview('list',''); 
                            }
                             
                     },
                    error: function(jqXHR, textStatus, errorThrown) {
                           alert(textStatus);		
                    }
                    
                });
   
        }
//        
       
        
    }]);
 app.directive('customOnChange', function() {
            return {
                restrict: 'A',
                link: function (scope, element, attrs) {
                  var onChangeFunc = scope.$eval(attrs.customOnChange);
                  element.bind('change', function(event){
                            var files = event.target.files;
                            onChangeFunc(files);
                  });

                  element.bind('click', function(){
                    element.val('');
                  });
                }
            };
    });