app.controller('smsController', ['$scope', 'API','bookService', function ($scope, API,bookService) {
        $scope.paginator = new Pagination(25);
        var RestaurantID =$scope.restaurant= $("#restaurant").val();
        var email = $("#email").val();
         $scope.predicate = '';
	 $scope.reverse = false;
         $scope.createModeDisplay = false;
        
         $scope.nonefunc = function () {
         };
     
         var status = ['active', 'inactive'];
         
         
         //table definition
        $scope.tabletitle = [
            {a: 'title', b: 'Title', c: '', q: 'down', cc: 'black'},
            {a: 'message', b: 'Message', c: '', q: 'down', cc: 'black'},
            {a: 'status', b: 'Status', c: '', q: 'down', cc: 'black'}
         
        ];
        
        $scope.tabletitleContent = [
            {a: 'title', b: 'title', c: '', d: '', t: 'input'},
            {a: 'message', b: 'message', c: '', d: '', t: 'input'},
            {a: 'status', b: 'status', c: '', d: 'sort', t: 'dropdown', val: status, func: $scope.nonefunc}
          
            //{a: 'notify_sms', b: 'Notify Sms', c: '', d: '', t: 'checkbox'}
        ];
        $scope.Objtemplate = function () {
            return {
                id: '',
                restaurant: $scope.restaurant,
                title :'',
                message: '',
                status :'',
                action:'',
                
                
                replicate: function(obj) {
                    for (var attr in this)
                    if(this.hasOwnProperty(attr) && typeof this[attr] !== 'function' && attr !== '$$hashKey') 
                            this[attr] = obj[attr];
                    return this;
                    }
               
            };
        };
        
        $scope.bckups =$scope.tabletitle.slice(0);
        
        $scope.initorder = function () {
            $scope.tabletitle = $scope.bckups;
            $scope.predicate = "title";
            $scope.reverse = false;
        };
        
        $scope.reorder = function (item, alter) {
            alter = alter || "";
            if (alter !== "")
                item = alter;
            $scope.reverse = ($scope.predicate == item) ? !$scope.reverse : false;
            $scope.predicate = item;
        };
        
         $scope.getTempalteList = function(){
            API.templateconfiguration.getTemplateList(RestaurantID).then(function (response) {
                console.log(response.status);
                if(response.status == '1'){
                 $scope.templatelist = response.data;

              }
              $scope.initorder();
                

            }); 
        };
        $scope.getTempalteList();
        
         $scope.savetemplatelist = function(){
//             console.log("selcted"+JSON.stringify(this.selectedItem));return;
             API.templateconfiguration.savelist(RestaurantID,this.selectedItem.message,this.selectedItem.title,this.selectedItem.status,this.selectedItem.action,this.selectedItem.id).then(function (response) {
                 console.log(response.data);
                 $scope.selectedItem.id = response.data;
                  $scope.templatelist.push($scope.selectedItem);
                  $scope.switchview('list','');
            });
         };
        $scope.switchview = function (view, object) {
            
              if (view == 'create') {
                $scope.selectedItem = new $scope.Objtemplate();
                $scope.selectedItem.action ='create';
                $scope.createModeDisplay = true;
                } else {
                
                    $scope.createModeDisplay = false;
                }
                    
                
        };
         $scope.update = function (x) {
             $scope.selectedItem = x;
             $scope.selectedItem.action ='update';
             $scope.createModeDisplay = true;
         };
         $scope.remove =function(x){

            for(var i = 0; i < $scope.templatelist.length; i++) 
                if($scope.templatelist[i].title === x.title) {
                        $scope.templatelist.splice(i, 1);
                        break;
                }
            
         }
         $scope.delete =function(x){
             if(confirm("Are you sure you want to delete " + x.title) === false)
			return;
             API.templateconfiguration.removeTemplateList(RestaurantID,x.id).then(function (response) {
                if(response.status == '1'){
                    $scope.remove(x);
                    alert(x.title + " has been deleted"); 
                }
 
              });
             
             
         };
        
        
        
//        
       
        
    }]);
