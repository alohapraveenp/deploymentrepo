(function(app) {
app.service('BlocklistConfigurationService', ['$http', '$q', function ($http, $q) {
       
       this.getBlockList = function (restaurant,email) {
        var defferred = $q.defer();
            return $http.post("../api/services.php/blockedlist/get/",
                    {
                        'restaurant': restaurant,
                        'email'       :email,
                        'token'    :token
                     }).success(function(response) {
                defferred.resolve(response);
            });
             return defferred.promise;
        };
        this.savelist = function (restaurant,backofficeemail,email,mobile,status,firstname,lastname,reason) {
            var defferred = $q.defer();
                return $http.post("../api/services.php/blockedlist/save/",
                        {
                            'restaurant': restaurant,
                            'backofficeemail':backofficeemail,
                            'email' :email,
                            'mobile' :mobile,
                            'status' :status,
                            'firstname' :firstname,
                            'lastname' :lastname,
                            'reason' :reason,
                            'token'    :token
                         }).success(function(response) {
                    defferred.resolve(response);
                });
                 return defferred.promise;
        };
       
       
 
    }]);
})(angular.module('app.api.blocklist.blocklistconfiguration', []));


