(function(app) {
app.service('TemplateConfigurationService', ['$http', '$q', function ($http, $q) {
       
       this.getTemplateList = function (restaurant) {
        var defferred = $q.defer();
                var API_URL ="../api/v2/notification/sms/template/list";
                 $http({
                    url: API_URL,
                    method: "POST",
                    data: $.param({
                         'restaurant': restaurant,
                         'token'   : token
                }),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
             }).success(function(response) {
                defferred.resolve(response);
            });
             return defferred.promise;
          };
          
          
        this.savelist = function (restaurant,message,title,status,action,id) {
            var defferred = $q.defer();
            
            var API_URL ="../api/v2/notification/sms/template/save";
                 $http({
                    url: API_URL,
                    method: "POST",
                    data: $.param({
                         'restaurant': restaurant,
                         'message': message,
                         'title': title,
                         'status': status,
                         'action':action,
                         'id':id,
                         'token'   : token
                }),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
             }).success(function(response) {
                defferred.resolve(response);
            });
             return defferred.promise;
        };
        this.removeTemplateList = function (restaurant,id) {
            var defferred = $q.defer();
            var API_URL ="../api/v2/notification/sms/template/delete";
                 $http({
                    url: API_URL,
                    method: "POST",
                    data: $.param({
                         'restaurant': restaurant,
                         'id':id,
                         'token'   : token
                }),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
             }).success(function(response) {
                defferred.resolve(response);
            });
             return defferred.promise;
        };
        
        
        
  
       
       
 
    }]);
})(angular.module('app.api.templatemanagement.templateconfiguration', []));


