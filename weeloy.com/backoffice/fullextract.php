<?php

require_once("conf/conf.init.inc.php");
require_once("lib/wpdo.inc.php");
require_once("lib/wglobals.inc.php");
require_once("conf/conf.session.inc.php");
require_once("lib/class.coding.inc.php");
require_once("lib/class.login.inc.php");
require_once("lib/class.booking.inc.php");
require_once("lib/class.review.inc.php");
require_once("lib/class.restaurant.inc.php");
require_once("lib/class.debug.inc.php");

	$validArg = array('restaurant', 'email', 'token', 'starting', 'ending', 'field');

	while(list($label, $value) = each($_REQUEST))
		if(in_array($label, $validArg)) {
			$$label = clean_input($value);
			}

	//echo $restaurant . "<br />" . $email . "<br />" . $token . "<br />" . $starting . "<br />" . $ending . "<br />" . $field;

	$field = preg_replace("/,/", ";", $field);
	$fieldAr = explode("|", $field);
	$fieldtitle = implode(",", $fieldAr) . "\r\n";
	
    $login = new WY_Login(set_valid_login_type('backoffice'));
    if ($login->checktoken($token) === false) {
    	echo "invalid token. Enable to complete extraction. Please login";
    	exit;
    	}
	$mobj = new WY_Booking();

	$datestr = date('dmy');
	header("Content-type: application/vnd.ms-excel");
	header("Content-disposition: attachment; filename=\"weeloy_data_$datestr.csv\"");

	echo $fieldtitle;
	for($i = 0; $i < 10; $i++) {
		$data = $mobj->getBookingField($restaurant, $fieldAr, $starting, $ending, $i);
		if(count($data) == 0)
			break;

		foreach($data as $row) {
			$fieldcontent = implode("|||", $row);
			$fieldcontent = preg_replace("/,/", ";", $fieldcontent);
			$fieldcontent = preg_replace("/\r|\n/", ";", $fieldcontent);
			$fieldcontent = preg_replace("/0000-00-00 00:00:00/", "", $fieldcontent);
			$fieldcontent = preg_replace("/\|\|\|/", ",", $fieldcontent) . "\r\n";
			echo $fieldcontent;
			}
		}

?>