<div ng-controller="FooterController" ng-cloak ng-init="moduleName='footer';"  >               
<div class="row" style='background: #f6f6f6 !important;' >
                    <div class="col-md-5" style='background: #f6f6f6 !important;'>
                        <div class="white-bg NoMargin"  style="min-height:100%;">
                            <h3>About Us</h3>
                            <div class="custombox1">
                                <p>Weeloy is a technology & marketing solutions specialist serving the hospitality industry. Our product suite is designed specifically for restaurants to streamline workflows, create operational efficiencies, eliminate administrative redundancies and allow staff and management to focus on providing an optimal dining experience for their guests.</p>
<!--                                <p>Weeloy is a Dynamic Distribution platform offering to FOOD lovers a new & FUN way to enjoy a world of privileges at participating RESTAURANTS.</p>-->
                                <div class="aboutus">

                                    <ul style="margin-bottom: 0px;margin-left:25px;">
                                        <li><span><i class="fa fa-home"></i>83 Amoy Street, Singapore 069902</span></li>
                                        <li><span><i class="fa fa-phone"></i>SG: +65 92478778</span></li>
                                         <li><span><i class="fa fa-phone"></i>TH: +66 818262109</span></li>
                                        <li><span><i class="fa fa-envelope"></i> hotline@weeloy.com</span></li>
                                        <li><a href ='http://www.weeloy.io' target="_blank"><span><i class="fa fa-external-link"></i> www.weeloy.io</span></a></li>
                                      
                                    </ul>
                                </div>
                                <br/>
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4" style="height:100%;">
                        <div class="white-bg NoMargin">
                            <div data-jmodtip="&lt;strong&gt;Edit module&lt;/strong&gt;&lt;br /&gt;Newsletter&lt;br /&gt;Position: footer-b" data-jmodediturl="" class="rt-block box1 nomarginright jmoddiv">
                                <div class="module-surround">
                                    <div class="module-title">
                                        <h3 class="title">Newsletter</h3>
                                    </div>
                                    <div class="acymailing_introtext" style='margin-bottom:30px;'>Receive weekly news, updates, alerts, tips & best practices in your inbox.</div>
                                    <br>
                                    <form style="margin-bottom:30px;">
                                        <input type="text" value="" name="user[name]" class="form-control" placeholder="Name" id="user_name_formAcymailing21311" ng-model='username'>
                                        <br>
                                        <input type="text" value="" name="user[email]" class="form-control" placeholder="E-mail" ng-model='email' id="user_email_formAcymailing21311">
                                        <br>
                                        <input type="button" ng-click="SubmitNewletter();" name="Submit" value="Subscribe" class="button subbutton btn btn-primary customColor">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 white-bg NoMargin" >
                      <?php
                          if($_SESSION['user']['member_type'] != 'visitor'){
                        
                    echo('<div class="">
                            <h3>Need help?</h3>
                            <div class="SocialIcons"><h4 style="margin-bottom:20px;">User manual</h4> 
                                <a target="_blank" style="margin-right:5px" href="javascript:new_window(\'WeeloyUserManual/Current/index.php\')" title="">Best Practices</a> 
                            </div>
                    </div>');
                        }
                    ?>
                    <div  style='margin-top:60px;'>
                            <h3>Follow us</h3>
                            <div class="SocialIcons" style='margin-top:20px;margin-bottom:30px;'> 
                                <a target="_blank" style="margin-right:5px; padding:0px 5px;" href="https://www.facebook.com/weeloy.sg" title="Facebook"><span class="fa fa-facebook"></span></a> 
                                <a target="_blank" style="margin-right:5px; padding:0px 5px;" href="https://twitter.com/weeloyasia" title="Twitter"><span class="fa fa-twitter"></span></a> 
                                <a target="_blank" style="margin-right:5px; padding:0px 5px;"  href="https://www.linkedin.com/company/weeloy-pte-ltd" title="Linkedin"><span class="fa fa-linkedin"></span></a>
                            </div>
                        </div>
                        <p></p>
                        <br><br>
                    </div>
                <div class="copyright" style="text-align:center"><a href="https://www.weeloy.com/"><i class='fa fa-copyright'></i>Copyright 2017 Weeloy Pte Ltd.</a></div>
                </div>
</div>
<script>

var token = <?php echo "'".$_SESSION['user_backoffice']['token']."';"; ?>
var email =  <?php echo "'".$_SESSION['user_backoffice']['email']."';";?> 

app.controller('FooterController', ['$scope', '$http', 'bookService', function($scope, $http, bookService) {
        $scope.SubmitNewletter = function() {
            var msg = "";
            if ($scope.email === '' || typeof $scope.email === 'undefined') {
                msg = 'All fields are required!';
                alert(msg);
                return false;
            }
             bookService.submitNewletter($scope.username,$scope.email).then(function(response) {
                 if(response.status === 1){
                      var msg = response.data.message + $scope.email;
                      alert(msg);
                      $scope.email ='';
                      $scope.username ='';
                 }
                 

             });
          
        };
        
        
}]);
</script>




