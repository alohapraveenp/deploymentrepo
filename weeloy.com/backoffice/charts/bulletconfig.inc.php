<div ng-controller="Bmultirange">
<md-content class="md-padding" layout-xs="column" layout="row">
<div flex-xs flex-gt-xs="100" layout="column">
<md-card>

<div style='text-align:center'><h3> Configuration for Bullet Chart</h3></div><br />

Enter your target booking, and the maximum bookings for the year<br /> 

<md-card-actions layout="row" layout-align="start center">
<md-input-container>
<label>maximum</label>     
<input type="text" ng-model="maximum" ng-change="updateval()" ng-model-options="{ updateOn: 'blur' }" required>
</md-input-container>

<md-input-container>
<label>target</label>     
<input type="text" ng-model="target" ng-change='updateval()' ng-model-options="{ updateOn: 'blur' }" required>
</md-input-container>
</md-card-actions>

<div  ng-show="false" style="margin-bottom: 10px;">
  <div ng-repeat="range in rangeArray">
  <input style="display:block;" type="number" ng-model="range.value">
  </div>
</div>


<div ng-show="false" >
  <input type="text" ng-model="entry" placeholder="add entry"><button type="button" ng-click="add()">Add</button>
  <input type="number" ng-model="view" placeholder="View index">
</div>

<div ng-show="false" style="margin-bottom: 10px;">
  <div ng-repeat="range in rangeArray">
  <input style="display:block;" type="number" ng-model="range.value">
  </div>
</div>

<vds-multirange ng-model="rangeArray" style="margin-bottom: 10px;" views="views" view="view"></vds-multirange>

<br />
<md-card-actions layout="column" layout-align="start">
<section layout="row" layout-sm="column" layout-align="center center" layout-wrap>
    <md-button ng-click='saveconfig();' class="md-primary md-raised md-hue-1"><span class='glyphicon glyphicon-save'></span> &nbsp;{{ buttonlabel }}</md-button>
</section>
</md-card-actions>

<br /><br /><br />
</md-card>
</div>
</md-content></div>

<script>

var token = <?php echo "'".$_SESSION['user_backoffice']['token']."';"; ?>
var email =  <?php echo "'".$_SESSION['user_backoffice']['email']."';";?>

app.controller('Bmultirange', function ($scope, bookService, vdsMultirangeViews) {
	
	$scope.restaurant = <?php echo "'".$theRestaurant."';"; ?>
	$scope.target = 500;
	$scope.maximum = 1000;
	$scope.buttonlabel = "save configuration";
	
    // populate some data
    $scope.rangeArray = [
      { name: 'POOR', value: 0.2 },
      { name: 'GOOD', value: 0.4  },
      { name: 'VERY GOOD', value: 0.6 },
      { name: 'target', value: 0.5 },
      { name: 'maximum', value: 1 }
    ];

    $scope.views = vdsMultirangeViews.DEFAULT; // TIME;

	$scope.readconfig = function() {

		bookService.readRestaurant($scope.restaurant).then(function(response) {
			var i, tmpAr, data, oo = {};
		

			if(response.statusText !== "OK" && response.data && response.data.status === 1)
				return;
		
			data = response.data.data;
			if(!data.restogeneric || typeof data.restogeneric !== "string" || data.restogeneric.length < 5)
				return;
				
			try {
			oo = JSON.parse(data.restogeneric.replace(/’/g, "\""));
			} catch(err) { console.log(err, data.restogeneric); return; }
			
			if(!oo.bullet || typeof oo.bullet !== "string" || oo.bullet.length < 5)
				return;
			
			tmpAr = oo.bullet.split('|');
			for(i = 0; i < $scope.rangeArray.length; i++)
				$scope.rangeArray[i].value = parseFloat(tmpAr[i]);
			$scope.target = parseInt(tmpAr[i++]);
			$scope.maximum = parseInt(tmpAr[i++]);				
			});
		};

	$scope.updateval = function() {
		var index = $scope.rangeArray.inObject('name', 'target');

		$scope.target = parseInt($scope.target);
		$scope.maximum = parseInt($scope.maximum);
		
		if(index >= 0 && $scope.maximum > 0 && $scope.target > 0) 
			$scope.rangeArray[index].value = parseFloat(($scope.target / $scope.maximum).toFixed(2));		
		};
		
	$scope.saveconfig = function() {
		var data, index, tmpAr = [];

		$scope.target = parseInt($scope.target);
		$scope.maximum = parseInt($scope.maximum);
		
		if($scope.maximum < 100)
			$scope.maximum = 100;
			
		if($scope.target > $scope.maximum) 
			$scope.target = $scope.maximum;
			
		if((index = $scope.rangeArray.inObject('name', 'target')) < 0)
			$scope.rangeArray[index].value = Math.floor($scope.target / $scope.maximum)

		if((index = $scope.rangeArray.inObject('name', 'maximum')) < 0)
			$scope.rangeArray[index].value = 1;

		for(i = 0; i < $scope.rangeArray.length; i++)
			tmpAr.push($scope.rangeArray[i].value);
		tmpAr.push($scope.target);
		tmpAr.push($scope.maximum);
		
		data = tmpAr.join('|');	
		bookService.writeBulletconfig($scope.restaurant, data).then(function(response) { alert("Config has been saved/updated"); setNavBar('REPORT'); });
		};

	$scope.readconfig();
	
	$scope.watchchg = function () {
		var index;

		$scope.maximum = parseInt($scope.maximum);

		if((index = $scope.rangeArray.inObject('name', 'maximum')) >= 0) 
			$scope.rangeArray[index].value = 1;

		if((index = $scope.rangeArray.inObject('name', 'target')) >= 0 && $scope.maximum > 0) 
			$scope.target = Math.floor($scope.maximum * $scope.rangeArray[index].value);
		};
		
	$scope.$watch('rangeArray', $scope.watchchg, true);
		
    $scope.add = function (name, val) {
      $scope.rangeArray.push({ name: name, value: val });
    };
});
  
</script>