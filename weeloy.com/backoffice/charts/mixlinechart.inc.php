<?php

	$year = 2016;

	$sdate = $year . "-01-01";
	$edate = $year . "-12-31";
	
	$stats = new WY_stats($theRestaurant, $sdate, $edate);
	$stats->linechart();

	$logs = new WY_log("website");
	$logs->readLog($theRestaurant, $sdate, $edate);
	
	$vmonth = $cbkg = $rbkg = $rcvr = $sep = $data1 = $data2 = $data3 = "";
	$k = 1;
	for($i = 0; $i < count($stats->monthr) && $i < 4; $i++, $sep = ", ") {
		$vmonth .= $sep . "'" . $stats->monthr[$i] . "'";
		$cbkg .= $sep . $stats->monthcbkg[$i];
		$rbkg .= $sep . $stats->monthrbkg[$i];
		$rcvr .= $sep . $stats->monthrcvr[$i];
		if($i >= 11) $k = 2;
		$data1 .= $sep . "[gd(cyear, $i+1, $k)," . $stats->monthrcvr[$i] . "]";
		$data2 .= $sep . "[gd(cyear, $i+1, $k)," . $logs->count[$i] . "]";
		$data3 .= $sep . "[gd(cyear, $i+1, $k)," . $stats->monthrbkg[$i] . "]";
		}
    
	echo "<script>";
	echo "var cyear = 2015;\n";
	echo "var vmonth = [" . $vmonth . "];\n";
	echo "var cbkg = [" . $cbkg . "];\n";
	echo "var rbkg = [" . $rbkg . "];\n";
	echo "var data1 = [" . $data1 . "];\n";
	echo "var data2 = [" . $data2 . "];\n";
	echo "var data3 = [" . $data3 . "];\n";

?>

	var dataset = [
		{
			label: "Bookings",
			data: data3,         
			color: "purple",
			bars: {
				show: true, 
				align: "center",
				barWidth:  800 * 60 * 60 * 600,
				lineWidth:1
			}
		}, {
			label: "Visits",
			data: data2,
			yaxis: 2,
			color: "#0062FF",
			points: { symbol: "triangle", fillColor: "#0062FF", show: true },
			lines: {show:true}
		}, {
			label: "Covers",
			data: data1,
			yaxis: 3,
			color: "#FF0000",
			points: { symbol: "circle", fillColor: "#FF0000", show: true },
			lines: { show: true }
		}
	];

	
	var options = {
		xaxis: {
			mode: "time",
			tickSize: [1, "month"],        
			tickLength: 0,
			axisLabel: "Date",
			axisLabelUseCanvas: true,
			axisLabelFontSizePixels: 12,
			axisLabelFontFamily: 'Verdana, Arial',
			axisLabelPadding: 10,
			color: "black"
		},
		yaxes: [{
				position: "left",
	            min: 10,
	            max: 100,
				color: "black",
				axisLabel: "Bookings",
				axisLabelUseCanvas: true,
				axisLabelFontSizePixels: 12,
				axisLabelFontFamily: 'Verdana, Arial',
				axisLabelPadding: 1            
			}, {
				position: "right",
				clolor: "black",
				axisLabel: "Visits",
				axisLabelUseCanvas: true,
				axisLabelFontSizePixels: 12,
				axisLabelFontFamily: 'Verdana, Arial',
				axisLabelPadding: 3            
			},{
				position: "right",
				color: "black",
				axisLabel: "Covers",
				axisLabelUseCanvas: true,
				axisLabelFontSizePixels: 12,
				axisLabelFontFamily: 'Verdana, Arial',
				axisLabelPadding: 3            
			}
		],
		legend: {
			noColumns: 1,
			labelBoxBorderColor: "#000000",
			position: "nw"        
		},
		grid: {
			hoverable: true,
			borderWidth: 3,        
			backgroundColor: { colors: ["#ffffff", "#EDF5FF"] }
		}
	};

	$(document).ready(function () {
		$.plot($("#flot-placeholder_mlc"), dataset, options);
		$("#flot-placeholder_mlc").UseTooltip();

	});




function gd(year, month, day) {
    return new Date(year, month - 1, day).getTime();
}

var previousPoint = null, previousLabel = null;

$.fn.UseTooltip = function () {
    $(this).bind("plothover", function (event, pos, item) {
        if (item) {
            if ((previousLabel != item.series.label) || (previousPoint != item.dataIndex)) {
                previousPoint = item.dataIndex;
                previousLabel = item.series.label;
                $("#tooltip").remove();
                //console.log(item.datapoint);
                var x = item.datapoint[0];
                var y = item.datapoint[1];

                var color = item.series.color;
                var date = "Jan " + new Date(x).getDate();

                //console.log(item);
                var unit = "";

                if (item.series.label == "Bookings") {
                    unit = "";
                } else if (item.series.label == "Visits") {
                    unit = "";
                } else if (item.series.label == "Covers") {
                    unit = "";
                }

                showTooltip(item.pageX, item.pageY, color,
                            "<strong>" + item.series.label + "</strong><br>" + date +
                            " : <strong>" + y + "</strong> " + unit + "");
            }
        } else {
            $("#tooltip").remove();
            previousPoint = null;
        }
    });
};

function showTooltip(x, y, color, contents) {
    $('<div id="tooltip">' + contents + '</div>').css({
        position: 'absolute',
        display: 'none',
        top: y - 40,
        left: x - 120,
        border: '2px solid ' + color,
        padding: '3px',
        'font-size': '9px',
        'border-radius': '5px',
        'background-color': '#fff',
        'font-family': 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
        opacity: 0.9
    }).appendTo("body").fadeIn(200);
}

</script>

    <div style="width:650px;height:300px;text-align:center;margin:10px">       
        <div id="flot-placeholder_mlc" style="width:100%;height:100%;"></div>       
    </div>
