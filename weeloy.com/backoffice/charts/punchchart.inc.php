<div id='mydisplay' class="plot-container" ng-controller="tmpChart">
<div id='punch_chart'></div>
</div>

<script>
app.controller('tmpChart', ['$scope', 'tmsAnalytic', function($scope, tmsAnalytic) {
	tmsAnalytic.drawchart(null, 'backoffice');
}]);
</script>
