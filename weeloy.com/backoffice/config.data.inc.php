<?php

// add item in Label
// add field in Label
// ADD ITEM IN RESTAURANT CLASS 
// add item in list in utilities

	$itemLabel_dsb = array('theRestaurant', 'navbar', 'saveSegment', 'itemmember', 'itemrestaurant', 'itemtitle', 'itemhotelname', 'theState');
	$dbLabel_dsb = array('restaurant', 'title', 'hotelname');
	$input_dsb = array(
	'Daily Special Board', '1', 'title', 
	'theMember', 'theMember', 'hiddenvalue',
	'itemmember', 'theMember', 'hiddenvalue',
	'theState', '', 'hidden',
	'DSB', 'dsbmanagement', 'routine',
	'Close', '1', 'close',
	'end', '0', 'title', 
	);

	$itemLabel_callcenter = array('theRestaurant', 'navbar', 'saveSegment', 'itemmember', 'itemrestaurant', 'itemtitle', 'itemhotelname', 'theState');
	$dbLabel_callcenter = array('restaurant', 'title', 'hotelname');
	$input_callcenter = array(
	'Call Center', '1', 'title', 
	'theMember', 'theMember', 'hiddenvalue',
	'itemmember', 'theMember', 'hiddenvalue',
	'theState', '', 'hidden',
	'CallCenter', 'callcenter', 'routine',
	'Close', '1', 'close',
	'Call Center Member', '15', 'title', 
	'CallCenter Member', 'callcentermember', 'routine',
	'Close', '15', 'close',
	'end', '0', 'title', 
	);


	$itemLabel_bkreport = array('theRestaurant', 'navbar', 'saveSegment', 'itemmember', 'itemrestaurant', 'itemtitle', 'itemhotelname', 'theState');
	$dbLabel_bkreport = array('restaurant', 'title', 'hotelname');
	$input_bkreport = array(
	'Booking of the Day', '1', 'title', 
	'theMember', 'theMember', 'hiddenvalue',
	'itemmember', 'theMember', 'hiddenvalue',
	'theState', '', 'hidden',
	'Visites', 'bkreport', 'routine',
	'Close', '1', 'close',
	'Booking Analytics', '2', 'title', 
	'Booking Analytics', 'bkanalytics', 'routine',
	'Close', '2', 'close',
	'end', '0', 'title', 
	);


	$itemLabel_invoicing = array('theRestaurant', 'navbar', 'saveSegment', 'itemmember', 'itemrestaurant', 'itemtitle', 'itemhotelname', 'theState');
	$dbLabel_invoicing = array('restaurant', 'title', 'hotelname');
	$input_invoicing = array(
	'Booking for Invoicing', '1', 'title', 
	'theMember', 'theMember', 'hiddenvalue',
	'itemmember', 'theMember', 'hiddenvalue',
	'theState', '', 'hidden',
	'Invoicing', 'invoicing', 'routine',
	'Close', '1', 'close',
	'View Invoices', '2', 'title', 
	'Invoices', 'viewinvoice', 'routine',
	'Close', '2', 'close',
	'Account Invoice', '3', 'title', 
	'Account', 'mgtinvoice', 'routine',
	'Close', '3', 'close',
	'Sms for Invoicing', '4', 'title',
	'smsinvoice', 'mgtsmsinvoice', 'routine',
	'Close', '4', 'close',
            
       'end', '0', 'title', 
	);

	$itemLabel_profile = array('theRestaurant', 'navbar', 'saveSegment', 'itemmember', 'itemrestaurant', 'itemtitle', 'itemhotelname', 'theState');
	$dbLabel_profile = array('restaurant', 'title', 'hotelname');
	$input_profile = array(
	'Guest Profile', '1', 'title', 
	'theMember', 'theMember', 'hiddenvalue',
	'itemmember', 'theMember', 'hiddenvalue',
	'theState', '', 'hidden',
	'Profile', 'consumerprofile', 'routine',
	'Close', '1', 'close',
	'end', '0', 'title', 
	);

	$itemLabel_reservation = array('theRestaurant', 'navbar', 'saveSegment', 'itemmember', 'itemrestaurant', 'itemtitle', 'itemhotelname', 'theState');
	$dbLabel_reservation = array('restaurant', 'title', 'hotelname');
	$input_reservation = array(
	'Reservations', '1', 'title', 
	'theMember', 'theMember', 'hiddenvalue',
	'itemmember', 'theMember', 'hiddenvalue',
	'theState', '', 'hidden',
	'Visites', 'reportbooking', 'routine',
	'Close', '1', 'close',
	'Restaurant Analytics', '2', 'title', 
	'Analytics Individual', 'bkanalyticsresto', 'routine',
	'Close', '2', 'close',
	'PABX', '4', 'title', 
	'PABX info', 'pabx', 'routine',
	'Close', '4', 'close',
	'Call Center', '5', 'title', 
	'CallCenter', 'callcenter', 'routine',
	'Close', '5', 'close',
	'Call Center Member', '15', 'title', 
	'CallCenter Member', 'callcentermember', 'routine',
	'Close', '15', 'close',
	'Book Now', '6', 'title', 
	'Book Now', 'booksystem', 'routine',
	'Close', '6', 'close',
	'TMS Now', '7', 'title', 
	'TMS Settings', 'tmssytem', 'routine',
	'TMS Lunch Report', 'tmsdayreportlunch', 'routine',	
	'TMS Dinner Report', 'tmsdayreportdinner', 'routine',	
	'Close', '7', 'close',
        'PAYMENT', '8', 'title', 
	'PAYMENT', 'payment', 'routine',
        'Close', '8', 'close',
        'Blocklist', '9', 'title', 
        'blockist', 'blocklist', 'routine',
	'Close', '9', 'close',
        'Sms Template Management', '9', 'title', 
        'smstemplate', 'smstemplate', 'routine',
	'Close', '9', 'close',
	'end', '0', 'title', 
	);

	$itemLabel_takeout = array('theRestaurant', 'navbar', 'saveSegment', 'itemmember', 'itemrestaurant', 'itemtitle', 'itemhotelname', 'theState', 'itemtkoutpayment', 'itemtkoutpickup', 'itemtkoutdeliver', 'itemtkouttax', 'itemtkoutminorder', 'itemtkout_tnc', 'itemtnc', 'itemspecific_tnc');
	$dbLabel_takeout = array('restaurant', 'title', 'hotelname', 'tkoutpayment', 'tkoutpickup', 'tkoutdeliver', 'tkouttax', 'tkoutminorder', 'tkout_tnc', 'tnc', 'specific_tnc');
	$input_takeout = array(
	'Configuration', '1', 'title',
	'theMember', 'theMember', 'hiddenvalue',
	'itemmember', 'theMember', 'hiddenvalue',
	'theState', '', 'hidden',
	'Online Payment', 'itemtkoutpayment', 'checked', 
	'Pickup', 'itemtkoutpickup', 'checked', 
	'Delivery', 'itemtkoutdeliver', 'checked', 
	'GST', 'itemtkouttax', 'input', 
	'Min per delivery', 'itemtkoutminorder', 'input', 
	'Terms and conditions', 'itemtkout_tnc', 'textarea',
	'Save changes', '1takeout', 'submit',
	'Close', '1', 'close',
	"Event Terms And Conditions", "2", "title",
		'Terms and conditions', 'itemtnc', 'textarea',
		'Specific Terms and conditions', 'itemspecific_tnc', 'textarea',
		'Save', 'event_tnc', 'submit',
	'Close', '2', 'close',
	'Orders', '3', 'title', 
	'Orders', 'takeoutorder', 'routine',
	'Close', '3', 'close',
	'Event Management', '4', 'title', 
	'Event Management', 'eventordering', 'routine',
        'Event Order Send Details', '4', 'title', 
	'Event Order Send Details', 'eventsendform', 'routine',
	'end', '0', 'title', 
	);
//	'Radial', 'radial', 'flot',
//	'Wins', '4', 'title', 
//	'Wins', 'piechartD3', 'flot',
//	'Close', '4', 'close',
//	'Review', '4', 'title', 
//	'Close', '4', 'close',

	$itemLabel_report = array('theRestaurant', 'navbar', 'saveSegment', 'itemmember', 'itemrestaurant', 'itemtitle', 'itemhotelname', 'theState');
	$dbLabel_report = array('restaurant', 'title', 'hotelname');
	$input_report = array(
	'Report Summary', '1', 'title', 
	'theMember', 'theMember', 'hiddenvalue',
	'itemmember', 'theMember', 'hiddenvalue',
	'theState', '', 'hidden',
	'Bullet', 'bullet', 'flot',
	'Close', '1', 'close',
//don't change the order. the first file 'linechart' have all the include and preparation for cumul and pie
	'Booking Channels', '2', 'title',
	'Booking Channels', 'linechart', 'flot',
	'Close', '2', 'close',
	'Booking Cumulated', '3', 'title',
	'Booking Cumulated', 'cumulchart', 'flot',
	'Close', '3', 'close',
	'Booking Origins', '4', 'title',
	'Origins', 'piechart', 'flot',
	'Close', '4', 'close',
	'Tms', '5', 'title', 
	'Occupency', 'punchchart', 'flot',
	'Close', '5', 'close',
	'Chart Configuration', '6', 'title',
	'Bullet Configuration', 'bulletconf', 'flot',
	'Close', '2', 'close',
	'end', '0', 'title', 
	);


	$itemLabel_wheel = array('theRestaurant', 'navbar', 'saveSegment', 'itemmember', 'itemrestaurant', 'itemwheelvalue1', 'itemwheelvalue3', 'itemwheelvalue3', 'itemwheelvalue4', 'itemwheelvalue5', 'itemwheelvalue6', 'itemwheelvalue7', 'itemwheelvalue8', 'itemwheelvalue9', 'itemwheelvalue10', 'itemwheelvalue11', 'itemwheelvalue12', 'itemwheelvalue13', 'itemwheelvalue14', 'itemwheelvalue15', 'itemwheelvalue16', 'itemwheelvalue17', 'itemwheelvalue18', 'itemwheelvalue19', 'itemwheelvalue20', 'itemwheelvalue21', 'itemwheelvalue22', 'itemwheelvalue23', 'itemwheelvalue24', 'itemwheelsponsor', 'theState');
	$dbLabel_wheel = array('restaurant', 'title', 'hotelname', 'wheel', 'wheelsponsor');
	$input_wheel = array(
	'Wheel Complete Description', '1', 'title', 
	'theMember', 'theMember', 'hiddenvalue',
	'itemmember', 'theMember', 'hiddenvalue',
	'theState', '', 'hidden',
	'Wheel Description', 'itemwheeldescription', 'routine', 
	'Close', '1', 'close',
	'Wheel Standard Description', '2', 'title', 
	'Wheel Description', 'itemwheelstddescription', 'routine', 
	'Close', '2', 'close',
	'Current Wheel Image', '3', 'title', 
	'Wheel Part', 'itemwheelimage', 'routine', 
	'Close', '3', 'close',
	'Current Wheel Legend', '4', 'title',
	'Wheel Part Legend', 'itemwheelchoice', 'routine', 
	'Close', '4', 'close',
	'Build Your Wheel Configuration', '5', 'title', 
	'Wheel Part', 'itemwheelvalue', 'routine', 
	'Save changes', 'savewheel', 'submit',
	'Set a standard wheel', 'savestandardwheel', 'irreversable',	// this is a button with an alert 	
	'Sponsor Wheel', '6', 'title', 
	'Select Sponsor Wheel', 'itemwheelsponsor', 'list',
	'Wheel View', 'itemsponsorimage', 'routine',
	'Save changes', 'savesponsor', 'submit',
	'Close', '7', 'close',
	'Extra Wheel', 'extrawheel', 'title', 
    	'Extra Wheel', 'extrawheel', 'routine',
	'Close', '7', 'close',

	'end', '0', 'title', 
	);

	$itemLabel_event = array('theRestaurant', 'navbar', 'saveSegment', 'itemmember', 'itemrestaurant', 'itemlogo', 'itemnewimgname', 'itemimages', 'itemevent', 'itemeventname', 'itemeventcity', 'itemeventcountry', 'itemeventstart', 'itemeventend', 'itemeventpicture', 'itemeventorder', 'itemeventtitle', 'itemeventdesc', 'itemcreateevent', 'theState', 'itempromotionchoice', 'itempromotionstart', 'itempromotionend', 'itempromotionoffer');
	$dbLabel_event = array('restaurant', 'title', 'hotelname', );
	$input_event = array(
	'Menus', '1', 'title',
	'theMember', 'theMember', 'hiddenvalue',
	'itemmember', 'theMember', 'hiddenvalue',
	'theState', '', 'hidden',
	'Menu', 'menuadmin', 'routine',
	'Close', '1', 'close',

	'Reviews', '2', 'title', 
	'Reviews', 'reportreview', 'routine',
	'Close', '2', 'close',
	
	'Events', '3', 'title',
	'Event', 'eventadmin', 'routine',
	'Close', '3', 'close',
	
	'Promotions', '4', 'title', 
        'promotion', 'promotion', 'routine',
	'Close', '4', 'close',

            
            
	
//            
            
            
            
	'end', '0', 'title'
	);

	$array_promotion = array(
	'Promotions', '4', 'title', 
    'promotion', 'promotion', 'routine',
	/*'Create Promotion', '5', 'title', 
	'Select Promotion', 'itempromotionchoice', 'list',
	'Starting Date (dd/mm/yy)', 'itempromotionstart', 'pdatepicker', 
	'Ending Date (dd/mm/yy)', 'itempromotionend', 'pdatepicker', 
	'Promotion Name', 'itempromotionname', 'input', 
	'Description', 'itempromotiondesc', 'textarea', 
	'Create Promotion', 'createpromotion', 'submit',
	'Close', '5', 'close',*/
	'end', '0', 'title'
	);
	
    $itemLabel_promotion = array('theRestaurant', 'navbar', 'theState', 'itempromotionchoice', 'itempromotionstart', 'itempromotionend', 'itempromotionoffer');
	$dbLabel_promotion = array('restaurant', 'title' );
	$input_promotion = array(
    'Active Promotion', '1', 'title', 
    'theState', '', 'hidden',
    'promotion', 'promotion', 'routine', 
	'Create Promotion', '2', 'title', 
	'Select Promotion', 'itempromotionchoice', 'list',
	'Starting Date (dd/mm/yy)', 'itempromotionstart', 'pdatepicker', 
	'Ending Date (dd/mm/yy)', 'itempromotionend', 'pdatepicker', 
	'Promotion Name', 'itempromotionname', 'input', 
	'Description', 'itempromotiondesc', 'textarea', 
	'Create Promotion', 'createpromotion', 'submit',
	'end', '0', 'title', 
	);
        
	$itemLabel_media = array('theRestaurant', 'navbar', 'saveSegment', 'itemmember', 'itemrestaurant', 'itemlogo', 'itemnewimgname', 'itemimages', 'itemmenus', 'itemmenusname', 'itemmorder', 'itemmenustitle', 'itemmenustitle1', 'itemmenusdesc1', 'itemmenustitle2', 'itemmenusdesc2', 'itemmenustitle3', 'itemmenusdesc3', 'itemmenustitle4', 'itemmenusdesc4', 'itemmenustitle5', 'itemmenusdesc5', 'itemcreatemenu', 'itemreviewgrade', 'itemreviewdtvisite', 'itemreviewguest', 'itemreviewdesc', 'theState');
	$dbLabel_media = array('restaurant', 'title', 'hotelname', );
	$input_media = array(
	'Pictures', '1', 'title', 
	'theMember', 'theMember', 'hiddenvalue',
	'itemmember', 'theMember', 'hiddenvalue',
	'theState', '', 'hidden',
	'Stage', 'Load', 'hidden',
            
        
//	'Select', 'itempicturetype', 'plist', 
//	'Pictures', 'itemimages', 'ilist', 
//	'Stage', 'Load', 'hidden',
//	'New Name', 'itemnewimgname', 'input',
//	'Rename', 'renameimage', 'button',
//	'Delete Selected Image', 'deleteimage', 'delete',

//	'Upload Picture', '2', 'title',
//	'Image File', '', 'file',
//	'Upload Image', 'uploadimage', 'button',

//	'Images', '3', 'title',
	'Images', 'itemmgtimage', 'routine',
	'Close', '1', 'close',
            
        'Upload Documents', '2', 'title', 
	'theMember', 'theMember', 'hiddenvalue',
	'itemmember', 'theMember', 'hiddenvalue',
	'theState', '', 'hidden',
	'Stage', 'Load', 'hidden',
        'Documents', 'itemmgtdoc', 'routine',
        'Close', '2', 'close',
	
	'end', '0', 'title'
	);
        

	$itemLabel_availability = array('theRestaurant', 'theState', 'navbar', 'itemrestaurant', 'itemopenhours', 'itembookhours', 'itembookhourslot');
	$dbLabel_availability = array('restaurant', 'title', 'hotelname', 'openhours', 'bookhours');
	
	$input_availability = array(
	'Restaurant Opening Hours', '1', 'title', 
	'theMember', 'theMember', 'hiddenvalue',
	'itemmember', 'theMember', 'hiddenvalue',
	'theState', '', 'hidden',
	'Openinghours', 'itemopenhours', 'routine',
	'Save changes', '1openhours', 'submit',
	'Close', '1', 'close',
	'Restaurant Booking Hours', '2', 'title', 
	'Bookinghours', 'itembookhours', 'routine',
	'Save changes', '1bookhours', 'submit',
	'Close', '2', 'close',
	'Booking Hours By Slot', '3', 'title', 
	'BookinghoursBySlot', 'itembookhourslot', 'routine',
	'Close', '3', 'close',
	'Restaurant Availability', '4', 'title', 
	'Allotment', 'allotment', 'routine',
	'Close', '4', 'close',
	'Blocking Period', '5', 'title', 
	'Block Period', 'blocking', 'routine',
	'Close', '5', 'close',
	'end', '0', 'title', 
	);
	
	$itemLabel_configuration = array('theRestaurant', 'theState', 'navbar', 'saveSegment', 'itemmember', 'itemrestaurant', 'itemtitle', 'itemhotelname', 'itemaddress', 'itemaddress1', 'itemcity', 'itemzip', 'itemcountry', 'itemtel', 'itememail', 'itemsmsid', 'itemmap', 'itemdescription', 'itemstate', 'itemfax', 'itemlogo', 'itemurl', 
					    'itemaward', 'itemcreditcard', 'itemwheel', 'itemregion', 'itemmealtype', 'itempricing', 'itemcurrency', 'itemopenhours', 'itemimages', 'itemstars', 'itemcuisine', 
					    'itemchef', 'itemchef_award', 'itemchef_origin', 'itemchef_description', 'itemchef_believe', 'itemchef_logo', 'itemchef_type', 
						'itemmenus', 'itemrating', 'itemmealslot', 'itemdfmealtype', 'itemwheelRescode', 'itemdfminpers', 'itemdfmaxpers', 'itemGPS', 'itembo_name', 'itembo_tel', 'itembo_email', 'itemmgr_name', 'itemmgr_tel', 'itemmgr_email', 'itemPOS', 'itemlaptime', 'itemlagtime', 'itemlastorderlunch', 'itemlastorderdiner', 'itemcutofflunch', 'itemcutoffdiner', 'itemrestaurant_tnc', 'itembookinfo', 'itemdeposit_cancel_tnc','itembookcritical', 'itemwebrestodesc', 'itemwebchefdesc');
	$dbLabel_configuration = array('restaurant', 'title', 'hotelname', 'address', 'address1', 'city', 'zip', 'country', 'tel', 'email', 'smsid', 'map', 'description', 'state', 'fax', 'logo', 'url', 'award', 'creditcard', 'wheel', 'region', 'mealtype', 'pricing', 'currency', 'openhours', 'images', 'chef', 'chef_award', 'chef_origin', 'chef_description', 'chef_believe', 'chef_logo', 'chef_type', 'stars', 'cuisine', 'menus', 'rating', 'mealslot', 'dfmealtype', 'wheelRescode', 'dfminpers', 'dfmaxpers', 'GPS', 'bo_name', 'bo_tel', 'bo_email', 'mgr_name', 'mgr_tel', 'mgr_email', 'POS', 'laptime', 'lagtime', 'lastorderlunch', 'lastorderdiner', 'cutofflunch', 'cutoffdiner', 'restaurant_tnc', 'bookinfo', 'booking_deposit_lunch', 'booking_deposit_dinner','deposit_cancel_tnc', 'bookcritical', 'webrestodesc', 'webchefdesc');

	$input_configuration = array(
	'Restaurant Information', '1', 'title', 
	'theMember', 'theMember', 'hiddenvalue',
	'itemmember', 'theMember', 'hiddenvalue',
	'theState', '', 'hidden',
	'Restaurant Title', 'itemtitle', 'input',
	'Hotel', 'itemhotelname', 'list', 
	'Address', 'itemaddress', 'input', 
	'Address1', 'itemaddress1', 'input', 
	'City', 'itemcity', 'arlist', 
	'Zip', 'itemzip', 'input', 
	'State', 'itemstate', 'input', 
	'Country', 'itemcountry', 'list', 
	'Area', 'itemregion', 'list', 
	'Tel', 'itemtel', 'input', 
	'Fax/CC', 'itemfax', 'input', 
	'Email', 'itememail', 'input', 
	'SMS ID', 'itemsmsid', 'input', 
	'Map', 'itemmap', 'input', 
	'Item GPS', 'itemGPS', 'input',
        'Point Of Sale System', 'itemPOS', 'input', 
	'Save changes', '1savehotel', 'submit',
	'Restaurant Description', '2', 'title', 
	'Logo Image', 'itemlogo', 'list', 
	'Web Site', 'itemurl', 'readonly', 
	'Desc', 'itemdescription', 'textareareadonly', 
	'Cuisine', 'itemcuisine', 'alist', 
	'Mealtype', 'itemmealtype', 'alist', 
	'Chef', 'itemchef', 'input',
	'Chef Award', 'itemchef_award', 'textarea', 
	'Chef Origin', 'itemchef_origin', 'input',
	'Chef Description', 'itemchef_description', 'textarea',
	'Chef Believes', 'itemchef_believe', 'input',
	'Chef Logo', 'itemchef_logo', 'list',
//	'Stars', 'itemstars', 'input', 
//	'Rating', 'itemrating', 'slider', 
	'Awards', 'itemaward', 'input', 
	'Chef Info', 'itemchef_type', 'list', 
	'Save changes', '2savehotel', 'submit',
        
        'Services', 'A', 'title',
        'services', 'services', 'routine',
        
    'Pricing Information', '3', 'title', 
//	'mealslot (0/1)', 'itemmealslot', 'checked', 
//	'Dines Free Mealtype', 'itemdfmealtype', 'alist', 
//	'Credit Cards', 'itemcreditcard', 'alist',
	'Currency', 'itemcurrency', 'clist', 
	'Dinner Spend/Cover', 'itempricing[]', 'slider', 
	'Lunch Spend/Cover', 'itempricing[]', 'slider',
	'Save changes', '3savehotel', 'submit',
        
    'Booking Information', '4', 'title', 
	'Wheel Restaurant Code', 'itemwheelRescode', 'inputnum(4)', 
	'Min number of Cover(s)', 'itemdfminpers', 'flist',
	'Max number of Cover(s)', 'itemdfmaxpers', 'flist',
	'Laptime-leadtime(h)', 'itemlaptime', 'slider',
	'Lunch Last Order', 'itemlastorderlunch', 'slider',
	'Dinner Last Order', 'itemlastorderdiner', 'slider',
	'Lunch Cutoff', 'itemcutofflunch', 'cutoff-slider',
	'Dinner Cutoff', 'itemcutoffdiner', 'cutoff-slider',
	'Lunch Booking Deposit', 'itembooking_deposit_lunch', 'input',
    'Diner Booking Deposit', 'itembooking_deposit_dinner', 'input',
    'Deposit Cancel Tnc', 'itemdeposit_cancel_tnc', 'textarea',
	'Terms and conditions', 'itemrestaurant_tnc', 'textarea',
	'Specific Information', 'itembookinfo', 'textarea',
	'Critical Information', 'itembookcritical', 'textarea',
	'Save changes', '4savehotel', 'submit',
          
/*
	'Contact Information', '5', 'title', 
	'BackOffice Manager Name', 'itembo_name', 'input', 
	'BackOffice Manager Phone', 'itembo_tel', 'input', 
	'BackOffice Manager Email', 'itembo_email', 'input', 
	'Main Manager Name', 'itemmgr_name', 'input', 
	'Main Manager Phone', 'itemmgr_tel', 'input', 
	'Main Manager Email', 'itemmgr_email', 'input', 
	'Point Of Sale System', 'itemPOS', 'input', 
	'Save changes', '5savehotel', 'submit',
*/
            
	'Website Information', '6', 'title', 
	'Website Resto Description', 'itemwebrestodesc', 'textarea', 
	'Website Chef Description', 'itemwebchefdesc', 'textarea',
	'Save changes', '6savehotel', 'submit',
        
        'Contact information', '7', 'title', 
	'contact information', 'contactinformation', 'routine',
//            
        

	'end', '0', 'title', 
	);
        $itemLabel_notification = array('theRestaurant', 'navbar', 'saveSegment', 'itemmember', 'itemrestaurant', 'itemlogo');
	$input_notification = array(
	'List', '1', 'title', 
	'theMember', 'theMember', 'hiddenvalue',
	'itemmember', 'theMember', 'hiddenvalue',
	'theState', '', 'hidden',
	'Stage', 'Load', 'hidden',
//	'Select', 'itempicturetype', 'plist', 
//	'Pictures', 'itemimages', 'ilist', 
//	'Stage', 'Load', 'hidden',
//	'New Name', 'itemnewimgname', 'input',
//	'Rename', 'renameimage', 'button',
//	'Delete Selected Image', 'deleteimage', 'delete',

//	'Upload Picture', '2', 'title',
//	'Image File', '', 'file',
//	'Upload Image', 'uploadimage', 'button',

//	'Images', '3', 'title',
	'Notiifcation', 'notification', 'routine',
//	'Close', '3', 'close',
	
	'end', '0', 'title'
	);
        $itemLabel_authentication = array('theRestaurant', 'navbar', 'saveSegment', 'itemmember', 'itemrestaurant', 'itemlogo');
	$dbLabel_bkreport = array('restaurant', 'title', 'hotelname');
	$input_authentication = array(
	'Scan code', '1', 'title', 
	'theMember', 'theMember', 'hiddenvalue',
	'itemmember', 'theMember', 'hiddenvalue',
	'theState', '', 'hidden',

        'Authentication', 'authentication', 'routine',
            
//	'Close', '2', 'close',
	'end', '0', 'title', 
	);

        $input_bkreport = array(
	'Booking of the Day', '1', 'title', 
	'theMember', 'theMember', 'hiddenvalue',
	'itemmember', 'theMember', 'hiddenvalue',
	'theState', '', 'hidden',
	'Visites', 'bkreport', 'routine',
	'Close', '1', 'close',
	'Booking Analytics', '2', 'title', 
	'Booking Analytics Consolidated', 'bkanalytics', 'routine',
	'Close', '2', 'close',
	'end', '0', 'title', 
	);

?>