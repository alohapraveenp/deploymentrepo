<?php

	// don't move those line as cookie header need to send prior to anything
	require_once("conf/conf.init.inc.php");
	require_once("lib/wpdo.inc.php");
	require_once("lib/gblcookie.inc.php");
	require_once("lib/class.debug.inc.php");
	require_once("lib/class.login.inc.php");
	require_once("lib/class.coding.inc.php");
	require_once("conf/conf.session.inc.php");
	require_once("lib/class.media.inc.php");

	require_once("lib/class.images.inc.php");	
	require_once("../inc/utilities.inc.php");
	require_once("config.data.inc.php");
	require_once("lib/class.analytics.inc.php");
	require_once("lib/class.restaurant.inc.php");
	require_once("lib/class.member.inc.php");
	require_once("lib/class.cluster.inc.php");
	require_once("lib/class.stats.inc.php");

	require_once("lib/wglobals.inc.php");
	require_once("lib/class.login.inc.php");
	require_once("lib/class.images.inc.php");
	require_once("lib/class.wheel.inc.php");
	require_once("lib/class.menu.inc.php");
	require_once("lib/class.review.inc.php");
	require_once("lib/class.event.inc.php");
	require_once("lib/class.promotion.inc.php");
	require_once("lib/class.stats.inc.php");
	require_once("lib/class.analytics.inc.php");
	
	$logger = new WY_log("backoffice");
		
	$inputAr = $itemLabel = $dbLabel = array();
	$navbar = (isset($_REQUEST['navbar'])) ? $_REQUEST['navbar'] : "";       
	if(empty($navbar))
		$navbar = 'HOME';
        
	$globalMessage = "";
	$globalMessageType = "info";
	$globalScript = "";
	$globalScriptOnload = "";
        	        	
    //$_SESSION['login_type'] = $login_type = LOGIN_BACKOFFICE;
    
    $login_type = set_valid_login_type("backoffice");
    $login = new WY_Login($login_type);
    $login->CheckLoginStatus();

    // to be used for backoffice ajax
    	
 	$navbarAr = array();  
   	$_SESSION['user_backoffice']['id'] = $_SESSION['user_backoffice']['token'] = $_SESSION['user_backoffice']['email'] = $_SESSION['user_backoffice']['member_permission'] = $_SESSION['user_backoffice']['member_type'] = "";

	$login_status = _LOG_IN_;
   	if($login->result < 0) {
	 	$login_status = _LOG_OUT_;
 		$navbar = "HOME";            
	 	unset($_SESSION['user_backoffice']['member_type']);
        	unset($_SESSION['user_backoffice']['id']);
 		$globalScript .= "var logstatus='out';";
	 } else {            
		$_SESSION['user_backoffice']['id'] = $login->signIn['user_id'];
		$_SESSION['user_backoffice']['token'] = $login->signIn['token'];	
		$_SESSION['user_backoffice']['email'] = $login->signIn['email'];
		$_SESSION['user_backoffice']['member_permission'] = intval($login->signIn['member_permission']);
		$_SESSION['user_backoffice']['member_type'] = $login->signIn['member_type'];
		$_SESSION['user']['member_type'] = $_SESSION['user_backoffice']['member_type']; // if navigating from front-end
		if($login->extrafield != "backoffice")
			$login->process_login_remote_setapp("backoffice", $login->email, $login->token);
 		$globalScript .= "var logstatus='in';";
		}
	
    $accountemail = $_SESSION['user_backoffice']['email'];
    $accounttype = $_SESSION['user_backoffice']['member_type'];
                
//echo $_SESSION['member_permission'] . " " . ($_SESSION['member_permission'] & __RESTRICTED_RESERVATION__); exit;
	$perms = 0;
	if($login_status == _LOG_IN_) {
		$perms = intval($_SESSION['user_backoffice']['member_permission']);

	 	if($perms & __RESTRICTED_AVAILABILITY__)
    			$navbarAr[] = "AVAILABILITY"; 

	 	if($perms & __RESTRICTED_RESERVATION__) {
	    		$navbarAr[] = "RESERVATION"; 
	    		$navbarAr[] = "PROFILE"; 
	    		}

	 	if($perms & __RESTRICTED_CALL_CENTER__)
	    		$navbarAr = array("CALL_CENTER"); 

	 	if($perms & __RESTRICTED_DSB__)
	    		$navbarAr = array("MEDIA", "DSB"); 

    	if(count($navbarAr) < 1)
    		$navbarAr = array("CONFIGURATION", "AVAILABILITY", "MEDIA", "WHEEL", "CONTENT", "PROFILE", "RESERVATION", "REPORT");

		$resdata = new WY_restaurant();
		$nb_resto = $resdata->getNbListRestaurant($accountemail, $accounttype);
		if($nb_resto < 1) {
			$navbar = "HOME";              
			$navbarAr = array();            
			}
				
		if(isset($_REQUEST['theRestaurant'])) {
			if($resdata->checkRestaurant($_REQUEST['theRestaurant']) == false) {
				$_REQUEST['theRestaurant'] = $theRestaurant = "";
				}
			}

        	$theRestaurant = $_REQUEST['theRestaurant'];        
		if(empty($theRestaurant)) {
			$resdata->getDefaultRestaurant($accountemail, $accounttype);
			$theRestaurant = $resdata->restaurant;
			$_REQUEST['theRestaurant'] = $theRestaurant;
			}						
		mng_RestoCookie('theRestaurant');
                
		if(!isset($theRestaurant))
			header('Location: ../index.php');

		$dataobject = $resdata->getRestaurant($theRestaurant);
		if(count($dataobject) <= 0) {
			WY_debug::recordDebug("ERROR-BACKOFFICE", "BACKOFFICE", "Invalid dataobject" . " \n " . "theRestaurant = $theRestaurant");
			exit;
			}

		if(isset($resdata->is_bookable) && !$resdata->is_wheelable && count($navbarAr) > 2) {		// $resdata->extraflag & WITHPROMOTION
			if(($key = array_search('WHEEL', $navbarAr)) !== false)
				array_splice($navbarAr, $key, 1);
			}								  

	 	if(($perms & __DSB__) && $resdata->checkDailyspecialboard() !== 0)
	    		$navbarAr[] = "DSB"; 

	 	if(($perms & __GROUP_REPORT__) && $nb_resto > 1)
	    		$navbarAr[] = "BKREPORT"; 

		if($resdata->takeOutRestaurant() && count($navbarAr) > 2)
			array_splice( $navbarAr, 5, 0, "ORDERING" );   

		if(!in_array('BKREPORT', $navbarAr))
			if(in_array($login->signIn['email'], array("richard@kefs.me", "richard.kefs@weeloy.com", "soraya.kefs@weeloy.com", "gultiga.lueskul@weeloy.com", "matthew.fam@weeloy.com")))
				$navbarAr[count($navbarAr) + 1] = 'BKREPORT';

		if(!in_array('INVOICING', $navbarAr))
			if(in_array($login->signIn['email'], array("richard@kefs.me", "richard.kefs@weeloy.com", "soraya.kefs@weeloy.com", "vs.kala@weeloy.com")))
				$navbarAr[count($navbarAr) + 1] = 'INVOICING';
                        
		if($resdata->checkRestrictedDSB() !== 0)
	    		$navbarAr = array("MEDIA", "DSB"); 
						
                        
        if($perms & __2FACTAUTH__){		// $resdata->checkauthentication()
            $navbarAr[count($navbarAr) + 1] = 'AUTHENTICATION';
            }
                        
//                 if(!in_array('NOTIFICATION', $navbarAr))
//			if(in_array($login->signIn['email'], array("richard@kefs.me", "richard.kefs@weeloy.com", "vs.kala@weeloy.com", "philippe.benedetti@weeloy.com","soraya.kefs@weeloy.com")))
//				$navbarAr[count($navbarAr) + 1] = 'NOTIFICATION';       
  
		$logger->LogEvent($_SESSION['user_backoffice']['id'], 50, $resdata->ID, $theRestaurant,$navbar, date("Y-m-d H:i:s"));

		}
	
	/////  check user right to display the page ////
	/////  ONLY SUPER_WEELOY and ADMIN  ////
	// if you, as a member, do have the right permission, you will be redirected on the home website
	if(!empty($accounttype) && !in_array($accounttype, $backoffice_member_type_allowed)){
		header('Location: ../index.php');
		}
	
	$globalScript .= "var mapou='" . strrev($accounttype) . $nb_resto . "," . $login->result . "," . $perms . "';";

	switch($navbar) {
	
		case 'HOME':
		case 'LOG':
		case 'PREFERENCES':
			break;
			
		case 'CONFIGURATION':
			$inputAr = $input_configuration;
			$itemLabel = $itemLabel_configuration;
			$dbLabel = $dbLabel_configuration;
			$navbarTitle = "Restaurant Configuration";
			break;

		case 'AVAILABILITY':
			$inputAr = $input_availability;
			$itemLabel = $itemLabel_availability;
			$dbLabel = $dbLabel_availability;
			$navbarTitle = "Restaurant Availability";
			break;

		case 'CONTENT':
			$inputAr = $input_event;
			$itemLabel = $itemLabel_event;
			$dbLabel = $dbLabel_event;
			$navbarTitle = "Restaurant Event";
			break;

		case 'MEDIA':
			$inputAr = $input_media;
			$itemLabel = $itemLabel_media;
			$dbLabel = $dbLabel_media;
			$navbarTitle = "Restaurant Media";
			break;

		case 'WHEEL':
			$inputAr = $input_wheel;
			$itemLabel = $itemLabel_wheel;
			$dbLabel = $dbLabel_wheel;
			$navbarTitle = "Restaurant Wheel";
			break;

		case 'PROFILE':
			$inputAr = $input_profile;
			$itemLabel = $itemLabel_profile;
			$dbLabel = $dbLabel_profile;
			$navbarTitle = "Restaurant Profiles";
			break;	
	
		case 'RESERVATION':
			$inputAr = $input_reservation;
			$itemLabel = $itemLabel_reservation;
			$dbLabel = $dbLabel_reservation;
			$navbarTitle = "Restaurant Reservation";
			break;	
	
		case 'ORDERING':
			$inputAr = $input_takeout;
			$itemLabel = $itemLabel_takeout;
			$dbLabel = $dbLabel_takeout;
			$navbarTitle = "Catering Takeout";
			break;	
	
		case 'DSB':
			$inputAr = $input_dsb;
			$itemLabel = $itemLabel_dsb;
			$dbLabel = $dbLabel_dsb;
			$navbarTitle = "Daily Special Board";
			break;	
	
		case 'REPORT':
			$inputAr = $input_report;
			$itemLabel = $itemLabel_report;
			$dbLabel = $dbLabel_report;
			$navbarTitle = "Restaurant Report";
			break;	
	
		case 'BKREPORT':
			$inputAr = $input_bkreport;
			$itemLabel = $itemLabel_bkreport;
			$dbLabel = $dbLabel_bkreport;
			$navbarTitle = "BOOKING Report";
			break;	
	
		case 'INVOICING':
			$inputAr = $input_invoicing;
			$itemLabel = $itemLabel_invoicing;
			$dbLabel = $dbLabel_invoicing;
			$navbarTitle = "Invoicing";
			break;	

		case 'CALL_CENTER':
			$inputAr = $input_callcenter;
			$itemLabel = $itemLabel_callcenter;
			$dbLabel = $dbLabel_callcenter;
			$navbarTitle = "callcenter";
			break;	

        	case 'AUTHENTICATION':
            		$inputAr = $input_authentication;
			$itemLabel = $itemLabel_authentication;
            		$dbLabel = $dbLabel_authentication;
			$navbarTitle = "AUTHENTICATION";
                        
			break;	
                  
                     
                     break;
		default:
			$navbar = "LOG";
			break;	
		}
	
	// with $validArg and include, the variable come to live
	$theState = "";	
	$validArg = $itemLabel;
	if(isset($validArg) && count($validArg) > 0 && is_array($validArg))
		while(list($index, $label) = each($validArg)) {
			if(isset($_REQUEST[$label]))
				$$label = clean_input($_REQUEST[$label]);
			}

	$_SESSION['restaurant']['promotion'] = (isset($resdata->is_bookable) && !$resdata->is_wheelable && $resdata->extraflag & WITHPROMOTION);		

	if($login->signIn['email'] == "richard@kefs.me") $_SESSION['restaurant']['promotion'] = true;
	
	if(count($inputAr) > 0 && !empty($theRestaurant) && $login_status == _LOG_IN_)
		{	
		process_data($resdata, $inputAr, $navbar, $dbLabel, $theState, $theRestaurant, $logger);

		$dataobject = $resdata->getRestaurant($theRestaurant); //do it again, as data were changed in process_data
		$eventTNC = $resdata->getEventTermsAndConditions($theRestaurant);
        $imgdata = new WY_Images($theRestaurant);					
		$itemrestaurant = $theRestaurant;	
		$dbLabel_cn = count($dbLabel);
	
		for($i = 0; $i < $dbLabel_cn; $i++)
			${"item" . $dbLabel[$i]} = "";

		for($i = 0; $i < $dbLabel_cn; $i++) {
			$dataobject["item" . $dbLabel[$i]] = "";
			if(isset($dataobject[$dbLabel[$i]]))
				$dataobject["item" . $dbLabel[$i]] = $dataobject[$dbLabel[$i]];
			else if(isset($eventTNC[$dbLabel[$i]]))
				$dataobject["item" . $dbLabel[$i]] = $eventTNC[$dbLabel[$i]];
		}
		//error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": dataobject: ".print_r($dataobject, true));
		$theRestaurantTitle = $dataobject['title']; 
		}
	       

function process_data($resdata, $inputAr, $navbar, $dbLabel, $theState, $theRestaurant, $logger) {

	global $globalMessage, $globalMessageType, $picture_typelist;
	
	if($inputAr == "")
		return;

	if($navbar == "CONFIGURATION") {
		if(substr($theState, 1) == "savehotel") {
            $logger->LogEvent($_SESSION['user_backoffice']['id'], 150, substr($theState, 0,1),$theRestaurant,$navbar, date("Y-m-d H:i:s"));
			saveHotel($inputAr, substr($theState, 0, 1), $theRestaurant);
			}
		return;
		}
	
	
	if($navbar == "AVAILABILITY") {
		if(substr($theState, 1) == "openhours") {
            $logger->LogEvent($_SESSION['user_backoffice']['id'], 150, substr($theState, 0,1),$theRestaurant,$navbar, date("Y-m-d H:i:s"));
			$resdata->saveOpeninghours($_REQUEST['itemopenhours'], $theRestaurant);
		}	
		if(substr($theState, 1) == "bookhours") {
            $logger->LogEvent($_SESSION['user_backoffice']['id'], 150, substr($theState, 0,1),$theRestaurant,$navbar, date("Y-m-d H:i:s"));
			$resdata->saveBookinghours($_REQUEST['itembookhours'], $theRestaurant);
		}	
	}

	if($navbar == "ORDERING") {
		if(substr($theState, 1) == "takeout") {
            $logger->LogEvent($_SESSION['user_backoffice']['id'], 150, substr($theState, 0,1),$theRestaurant, $navbar, date("Y-m-d H:i:s"));
			$resdata->saveTakeout($_REQUEST['itemtkoutpayment'], $_REQUEST['itemtkoutpickup'], $_REQUEST['itemtkoutdeliver'], $_REQUEST['itemtkouttax'], $_REQUEST['itemtkoutminorder'], $_REQUEST['itemtkout_tnc'], $theRestaurant);
		} else if ($theState == "event_tnc") {
			$logger->LogEvent($_SESSION['user_backoffice']['id'], 150, substr($theState, 0,1),$theRestaurant, $navbar, date("Y-m-d H:i:s"));
			$resdata->saveEventTermsAndConditions($theRestaurant, $_REQUEST['itemtnc'], $_REQUEST['itemspecific_tnc']);
		}
	}

	reset($picture_typelist);
	$PictureTypeSelection = $picture_typelist[0];
	if(isset($_COOKIE['picture_type_cookie']))
		if(in_array($_COOKIE['picture_type_cookie'], $picture_typelist))
			$PictureTypeSelection = $_COOKIE['picture_type_cookie'];
			
	if($navbar == "MEDIA") {
		
        $itemimages = $_REQUEST['itemimages'];
		$itemnewimgname = $_REQUEST['itemnewimgname'];

		$itemmenu = $_REQUEST['itemmenu'];
		$itemcreatemenu = $_REQUEST['itemcreatemenu'];
		$itemmenusname = $_REQUEST['itemmenusname'];

		$itemreviewdesc = $_REQUEST['itemreviewdesc'];

		//$imgdata = new WY_Images($theRestaurant);
                //$imgdata->object_type = 'photo';
                
        $mediadata = new WY_Media($theRestaurant);
                
        $menudata = new WY_Menu($theRestaurant);
		$reviewdata = new WY_Review($theRestaurant);

		
		if($theState == "deleteimage" && isset($itemimages)){
            $log = $mediadata->deleteImg($theRestaurant, $itemimages, $PictureTypeSelection);
			if($log < 0) $globalMessageType = "warning";
			$globalMessage = $mediadata->msg;
            }

		else if($theState == "renameimage" && !empty($itemimages) && !empty($itemnewimgname)) {
			$log = $mediadata->renameImg($theRestaurant, $itemimages, $itemnewimgname, $PictureTypeSelection);
			if($log < 0) $globalMessageType = "warning";
			$globalMessage = $mediadata->msg;
			}

		else if($theState == "uploadimage") {
                        
			$log = $mediadata->uploadImage($theRestaurant, $PictureTypeSelection);
			if($log < 0) $globalMessageType = "warning";
			$globalMessage = $mediadata->msg;
			}
			
		return;
		}		

	if($navbar == "CONTENT") {
		$itemevent = $_REQUEST['itemevent'];
		$itemeventname = $_REQUEST['itemeventname'];
		$itemeventimages = $_REQUEST['itemeventimages'];
		$itemnewimgname = $_REQUEST['itemnewimgname'];

	
                
        $itempromotionname = filter_input(INPUT_POST, 'itempromotionname', FILTER_SANITIZE_SPECIAL_CHARS);
        $itempromotionoffer = filter_input(INPUT_POST, 'itempromotionchoice', FILTER_SANITIZE_SPECIAL_CHARS);

		$mediadata = new WY_Media($theRestaurant);
		$eventdata = new WY_Event($theRestaurant);

		if($theState == "createevent" && isset($itemevent)) {
            $logger->LogEvent($_SESSION['user_backoffice']['id'], 153, 2, $theRestaurant,$navbar, date("Y-m-d H:i:s"));
			list($log, $msg) = $eventdata->insert($itemevent);
			if($log < 0) $globalMessageType = "warning";
			$globalMessage = $msg;
			}
			
		else if($theState == "deleteevent" && isset($itemeventname)) {
			list($log, $msg) = $eventdata->delete($itemeventname);
			if($log < 0) $globalMessageType = "warning";
			$globalMessage = $msg;
			}
			
		else if($theState == "saveevent" && isset($itemeventname)) {
            $logger->LogEvent($_SESSION['user_backoffice']['id'], 153, 3, $theRestaurant,$navbar, date("Y-m-d H:i:s"));
			list($log, $msg) = $eventdata->save($itemeventname);
			if($log < 0) $globalMessageType = "warning";
			$globalMessage = $msg;
			}
			
		else if($theState == "createmenu" && isset($itemmenu)) {
			list($log, $msg) = $menudata->insert($itemmenu);
			if($log < 0) $globalMessageType = "warning";
			$globalMessage = $msg;
			}
			
		else if($theState == "deletemenu" && isset($itemmenusname)) {
			list($log, $msg) = $menudata->delete($itemmenusname);
			if($log < 0) $globalMessageType = "warning";
			$globalMessage = $msg;
			}

		else if($theState == "savemenu" && isset($itemmenusname)) {
            $logger->LogEvent($_SESSION['user_backoffice']['id'], 151, substr($theState, 0,1), $theRestaurant,$navbar, date("Y-m-d H:i:s"));
			list($log, $msg) = $menudata->save($itemmenusname);
			if($log < 0) $globalMessageType = "warning";
			$globalMessage = $msg;
			}
			
		else if($theState == "createreview" && isset($itemreviewdesc)) {
			list($log, $msg) = $reviewdata->insert();
			if($log < 0) $globalMessageType = "warning";
			$globalMessage = $msg;
			}

		else if($theState == "createpromotion" && isset($itempromotionname) && $itempromotionname != '') {

			$promotiondata = new WY_Promotion($theRestaurant);
			$logger->LogEvent($_SESSION['user_backoffice']['id'], 159, 2, $theRestaurant,$navbar, date("Y-m-d H:i:s"));
			$itempromotiondetails = array();

			foreach($GLOBALS['promotionchoice'] as $promo){   
				if($promo[0] == $itempromotionoffer){
					$itempromotiondetails = $promo;
					}
				}
			$promotiondata->value = $itempromotiondetails[1];
			$promotiondata->tnc = $itempromotiondetails[2];
			list($log, $msg) = $promotiondata->insert($itempromotionname);
			if($log < 0) $globalMessageType = "warning";
			$globalMessage = $msg;
                    
            }

		return;
		}


//	if($navbar == "PROMOTION") {}

	if($navbar == "WHEEL") {
		$itemwheelvalue1 = $_REQUEST['itemwheelvalue1'];

		$wheeldata = new WY_wheel($theRestaurant, '', '');
		
		if($theState == "savewheel" && isset($itemwheelvalue1)) {
            $logger->LogEvent($_SESSION['user_backoffice']['id'], 152, 1, $theRestaurant,$navbar, date("Y-m-d H:i:s"));
            $arg = ($resdata->sponsor() && $resdata->wheelsponsor != "") ? "sponsor" : "";
			list($log, $msg) = $wheeldata->save_wheel($arg);
			if($log < 0) $globalMessage = $msg;
			}
			
		else if($theState == "savestandardwheel") {
            $logger->LogEvent($_SESSION['user_backoffice']['id'], 152, 2, $theRestaurant,$navbar, date("Y-m-d H:i:s"));
			list($log, $msg) = $wheeldata->save_stdwheel();
			if($log < 0) $globalMessage = $msg;
			}
			
		else if($theState == "savesponsor") {
            $logger->LogEvent($_SESSION['user_backoffice']['id'], 152, 3, $theRestaurant,$navbar, date("Y-m-d H:i:s"));
			list($log, $msg) = $wheeldata->savesponsor($_REQUEST['itemwheelsponsor']);
			if($log < 0) $globalMessage = $msg;
			}
			

		return;
		}		

}


function print_usefull() {
	global $navbar;
	
	if($navbar != "REPORT" || true)
		include("usefullinks.inc.php");
}

// Xsavehotel -> curTitle = X. If error, check of duplicate 'X'
function saveHotel($inputAr, $curTitle, $theRestaurant) {
	// save only the segment
	$debug = array();
	$dataAr = array();
	$dataCn = array();
	$valueSaveAr = array();
	$inputAr_cn = count($inputAr);
	for($start = 0; $start < $inputAr_cn; $start += 3)
		if($inputAr[$start+2] == "title" && $inputAr[$start+1] == $curTitle)
			break;
	
	for($i = $start+3, $sep = ""; $i < $inputAr_cn && $inputAr[$i+2] != "title"; $i += 3) {
		$val = $inputAr[$i+1];
		$debug[] = $inputAr[$i+1]; $debug[$val] = $_REQUEST[$val];
		if(substr($val, 0, 4) != "item")
			continue;
		if($val == "itemfax" || $val == "itemtel")
			$_REQUEST[$val] = preg_replace("/[^\d+]/", "", $_REQUEST[$val]);			
		$cn = strlen($val);
		if(substr($val, $cn - 2) == "[]") {
			$val = substr($val, 0, $cn - 2);
			if(!array_key_exists($val, $dataAr)) {
				$dataCn[$val] = 1;
				$dataAr[$val] = $_REQUEST[$val . $dataCn[$val]];
				}
			else $dataAr[$val] = $dataAr[$val] . "," . $_REQUEST[$val . ++$dataCn[$val]];
			continue;
			}
		$valueSaveAr[$val] = $_REQUEST[$val];
		}
	
	reset($dataAr);
	while(list($label, $val) = each($dataAr))
		$valueSaveAr[$label] = $val;
	
	if(count($valueSaveAr) > 0) {	
		$resdata = new WY_restaurant();
		$resdata->partialSave($valueSaveAr, $theRestaurant);
	} else {
		//error_log("SAVEHOTEL :" . $curTitle . print_r($debug, true));
		}
}

function contactinformation($theRestaurant, $email) {
	require_once("inc/configuration/contact_information/contact_information.php");
}


function allotment($theRestaurant, $email, $twositting, $multProduct) {
	require_once("inc/allote.inc.php");
}

function bkgHourBySlot($theRestaurant, $email) { 
	require_once("inc/mgtbkgohbs.inc.php");
}

function blocking($theRestaurant, $email, $multProduct) {
	require_once("inc/Availability/Availability.php");
}

function promotion($theRestaurant, $email) {
	$whl = new WY_wheel($theRestaurant, '', '');
	$promoAr = $whl->extraslices();
		
	foreach($promoAr as $name)
		$promotions_list[] = $name['offer'];
	
	require_once("inc/promotion.inc.php");
}

function blocklist($theRestaurant, $email) {
	require_once("app/components/blocklist/blocklist.inc.php");
}

function pabx($theRestaurant, $email) {
	require_once("inc/PABX/pabx.php");
}

function callcentermember($theRestaurant, $email) {
	require_once("inc/mgtccmember.inc.php");
}

function service($theRestaurant) {
	require_once("inc/restaurant_service.inc.php");
}

function dsbmanagement($theRestaurant, $email) {
	require_once("inc/dsb.inc.php");
}

function reportbooking($theRestaurant, $email, $logo, $perms, $multProduct) {
	$bkreport = (($perms & __GROUP_REPORT__) > 0) ? 1 : 0;
//	require_once("inc/Reservation/Reservation.php");
	require_once("inc/reportvisit.inc.php");
}

function reportreview($theRestaurant, $email) {
	require_once("inc/review.inc.php");
}

function consumerprofile($theRestaurant, $email, $logo, $editperms) {
	require_once("inc/Profile/profile.inc.php");
}

function menuadmin($theRestaurant, $takeoutflg, $label, $email) {
 	require_once("inc/mgtmenu.inc.php");
}

function eventadmin($theRestaurant, $email) {
	require_once("inc/EventManagementSystem/EventManagementSystem.php");
}

function imageadmin($theRestaurant, $email) {
	global $picture_typelist;
	//require_once("inc/mgtimage.inc.php");
	require_once("inc/Media/media.php");
}
function mgtidocument($theRestaurant, $email) {
	global $picture_typelist;	

	require_once("inc/mgtidocument.inc.php");
}


function bkreport($email) {
	require_once("inc/bkreport.inc.php");
}

function bkanalytics($email) {
	require_once("inc/bkanalytics.inc.php");
}

function bkanalyticsresto($theRestaurant, $email) {
	require_once("inc/bkanalytics.inc.php");
}

function invoicing($email) {
	require_once("inc/invoicing.inc.php");
}

function mgtinvoice($email) {
	require_once("inc/mgtinvoice.inc.php");
}
function mgtsmsinvoice($theRestaurant, $email) {
    require_once("inc/mgtsmsinvoice.inc.php");
}

function viewinvoice($email) {
	require_once("inc/viewinvoice.inc.php");
}

function takeoutorder($theRestaurant, $email) {
	require_once("inc/mgtordering.inc.php");
}

function eventordering($theRestaurant, $email) {
	$theRestoUrl = substr(strtolower(preg_replace("/([A-Z])/", "-$1", substr($theRestaurant, 8))), 1);
    require_once("inc/EventOrderingSystem/EventOrderingSystem.inc.php");
}
function eventsendform($theRestaurant, $email) {

    require_once("inc/mgteventrequest.inc.php");
}


function payment($theRestaurant, $email){
    require_once("inc/mgtpayment.inc.php");
}

function authentication($theRestaurant, $email) {

   require_once("inc/twofactauthentication.inc.php");
}

function smstemplate($theRestaurant, $email) {
   require_once("app/components/templatemanagement/sms.inc.php");
}

function tmsdayreportlunch($theRestaurant, $title, $email, $token)  {
	printf("<br /><form><a class='btn btn-info ButtonBtn' style='width:300px;' href='../admin_tms/tmsreport/index.html?restaurant=$theRestaurant&title=$title&email=$email&token=$token&meal=lunch' target='_blank'>Print TMS Lunch Report</a></form><br /><br />");
}

function tmsdayreportdinner($theRestaurant, $title, $email, $token)  {
	printf("<br /><form><a class='btn btn-info ButtonBtn' style='width:300px;' href='../admin_tms/tmsreport/index.html?restaurant=$theRestaurant&title=$title&email=$email&token=$token&meal=dinner' target='_blank'>Print TMS Dinner Report</a></form><br /><br />");
}
	
function extrawheel($theRestaurant, $email, $resdata) {

	$extratype = array();
	if($resdata->corporateWheel())
		$extratype[] = "corporate";
	if($resdata->offpeakWheel())
		$extratype[] = "offpeak";
	if(count($extratype) < 1) return;
		
	require_once("inc/mgtextrawheel.inc.php");
}

function tmssytem($theRestaurant, $email, $resdata) {
	
	$data = tokenize("$resdata->restaurant|$email|". time());
	printf("<form><a class='btn btn-info ButtonBtn' style='width:300px;' href='../backoffice/settms.php?arg=$data' target='_blank'>SET TMS to $resdata->restaurant</a></form><br /><br />");
}


function callcenter($email, $perms, $restaurant) {
	$data = tokenize($email);
	$value = $limited = "";
	$label = "Type fixed line";
	$perms = intval($_SESSION['user_backoffice']['member_permission']);
	$account = $email;
	
	if(($perms & __RESTRICTED_CALL_CENTER__) && $restaurant == "SG_SG_R_DbBistroOysterBar")
		$account = "dbbistrooysterbarrestricted";
		
	$cls = new WY_Cluster;
	$cls->read("SLAVE", $account, "CALLCENTER", "", "", "");  // should only return 1 answer
	if ($cls->result > 0 && !empty($cls->clustcontent[0])) {
		$content = $cls->clustcontent[0];
		if(preg_match("/.*phone=(\+?\d+)/", $content, $results))
			$value = $results[1];
		if(preg_match("/limited/", $content, $results) || ($perms & __RESTRICTED_CALL_CENTER__))
			$limited = "|limited";
		}

	printf("<form><a class='btn btn-info ButtonBtn' style='width:300px;' href='../modules/callcenter/callcenter.php?bktracking=CALLCENTER%s&data=$data' target='_blank'>CALL CENTER</a></form><br /><br />", $limited);
	if($perms & __PABX__ ) {
		$updatedate = <<<CCENTER
			<form>
			<div class='form-group row'>
			<label for='fixline' class='col-sm-2 control-label'>$label</label>
			<div class='col-sm-6'>
			<input type='text' value = '$value' class='form-control input-sm' id='fixline' name='fixline' placeholder='$label' maxlength = '16' >
			</div>
			</div>
			<br /><br /><a class='btn btn-success' style='width:120px;color:white;' href='javascript:updatefixline();'>submit phone</a></form><br /><br />
  			<script>function updatefixline() { \$.ajax({ url: '../api/services.php/pabx/fixline/', dataType: 'json', type: 'POST', data: { phone:  \$('#fixline').val(), token:token }, success: function (data) { alert('done'); } }); }</script>
CCENTER;
		echo $updatedate;
		}
}

function booksystem($email) {
	$data = tokenize($email);
	printf("<form><a class='btn btn-info ButtonBtn' style='width:300px;' href='#' onclick=\"window.open('../modules/booking/book_form.php?&bktracking=WEBSITE&data=$data', '_blank', 'toolbar=no,menubar=no,scrollbars=yes,resizable=yes,width=650,height=850');\">BOOK NOW</a></form><br /><br />");
}

function flot($reference, $label, $type, $theRestaurant) {
	
	$titleflot = $label; // this is use to communicate to the flot, what to display
	
	if($reference == "bullet") {
		require_once("charts/jplot.js");
		require_once("charts/bullet.inc.php");
		return;
		}
		
	if($reference == "bulletconf") {
		require_once("charts/bulletconfig.inc.php");
		return;
		}
		
	if($reference == "linechart") {
		require_once("charts/nvd3linechart.inc.php");
		echo "<hr /><br />";
		return;
		}

	if($reference == "cumulchart") {
		require_once("charts/nvd3cumulchart.inc.php");
		echo "<hr /><br />";
		return;
		}

	if($reference == "piechart") {
		require_once("charts/jplot.js");
		include("charts/piechart.inc.php");
		echo "<hr /><br />";
		return;
		}

	if($theRestaurant != "SG_SG_R_TheFunKitchen")
		return;

	if($reference == "punchchart") {
		require_once("charts/jplot.js");
		include("charts/punchchart.inc.php");
		echo "<hr /><br />";
		return;
		}
					
/*
	if($reference == "mixlinechart") {
		require_once("charts/jplot.js");
		require_once("charts/mixlinechart.inc.php");
		echo "<hr /><br />";
		return;
		}
*/		
	if($reference == "piechartD3") {
		require_once("charts/jplot.js");
		include("charts/piechartD3.inc.php");
		echo "<hr /><br />";
		return;
		}
		
/*
	if($reference == "radial") {
		echo "<iframe src='flots/radial/radial.html' width='700' height='200' frameBorder='0' scrolling='no' seamless></iframe>";
		echo "<hr /><br />";
		return;
		}
*/

}

$OpenBookHoursFunctionLoaded = false;

function call_routine($reference, $theRestaurant, $label, $resdata, $logo, $perms) {

	global $dataobject, $globalScript, $stdwheeloffers, $OpenBookHoursFunctionLoaded;

	$token = $_SESSION['user_backoffice']['token'];
	$perms = intval($_SESSION['user_backoffice']['member_permission']);
	
	//if(preg_match("/wheel/", $reference))
	$wheeldata = new WY_wheel($theRestaurant, '', '');
	$wheelimage = new WY_Images($theRestaurant);
	
	$wheeloffers = &$wheeldata->wheelconfoffer;	
	$wheelvalues = &$wheeldata->wheelconfvalue;
   	$email = $_SESSION['user_backoffice']['email'];
     
	switch($reference)
		{

		case 'itembookhourslot':
			bkgHourBySlot($theRestaurant, $email);
			break;			
                					
		case 'itembookhours':
		case 'itemopenhours':
			$ohvalue = $dataobject[$reference]; //22|||30|||22|||30|||22|||30|||22|||30|||22|||30|||22|||30|||22|||30|||36|||46|||36|||46|||36|||46|||36|||46|||36|||46|||36|||46|||36|||46;
			$weekday = array("", "Sunday", "Monday", "Tuesday", "Wednesday &nbsp;&nbsp;&nbsp;", "Thursday", "Friday", "Saturday");

			$template = "<table class='table' class='myfont' width='400px;'>";
			$template .= "<th>Day</th><th>Lunch</th><th>Dinner</th>";
			for($kk = 1; $kk < 8; $kk++) {
				$aa = "<input type='text' id='variable".  $reference . $kk . "' readonly class='myrange'><div id='slider-range". $reference . $kk ."' style='width:80px; height:10px'></div>";
				$bb = "<input type='text' id='variable".  $reference . ($kk+7) . "' readonly class='myrange'><div id='slider-range".  $reference . ($kk+7) ."' style='width:80px;'></div>";
				$template .= "<tr><td>" . $weekday[$kk] . "</td><td>$aa</td><td>$bb</td></tr>";
				}		
			$template .= "</table>";
	
			printf("<input type='hidden' value='%s' id='%s' name='%s'>", $ohvalue, $reference, $reference);
			printf("<div>%s</div>", $template);

			if($OpenBookHoursFunctionLoaded == false) {		// include them only once
				$globalScript .= "function cleanRange(a, min) { a = parseInt(a); if(isNaN(a)) a = min; if(a < min) a = min; return a; } ";
				$globalScript .= "function setUpRange(id, a, b) { start = Math.floor(a * 0.5) + ':' + (a % 2) * 3 + '0'; end = Math.floor(b * 0.5); if(end >= 24) end = '0' + (end - 24); end = end + ':' + (b % 2) * 3 + '0';  return start + ' - ' + end; } ";
				$globalScript .= "function setUp(ref) { Ar = $('#'+ref).val().split('|||'); for(i = 1; i < 15; i++) { k = (i-1) * 2; if(i < 8) { /* lunch */ mm = 18; nn = mm + 14;} else { /* dinner */ mm = 32; nn = mm + 22; } a = cleanRange(Ar[k], mm); b = cleanRange(Ar[k+1], mm);  $( '#variable'+ref+i ).val( setUpRange(i, a, b) ); $( '#slider-range'+ref+i ).slider({ range: true, step: 1, min: mm, max: nn, values: [ a, b ], background: 'blue', slide: function( event, ui ) { updateid = $('#'+this.id).slider('option', 'var'); value = setUpRange(updateid, ui.values[ 0 ], ui.values[ 1 ]); $( '#variable'+ref+updateid ).val(value); saveOH(updateid, ui.values[ 0 ], ui.values[ 1 ], ref); } });  $( '#slider-range'+ref+i ).slider('option', 'var', i); $( '#slider-range'+ref+i ).css({'height':'7px'});$('.ui-slider-range.ui-widget-header.ui-corner-all').css({'height':'7px!important'});$('.ui-slider-range').css({'height':'7px !important'});$('.ui-slider-handle.ui-state-default.ui-corner-all').css({'height':'0.8em', 'width':'0.8em'}); }} ";
				$globalScript .= "function saveOH(id, a, b, ref) { k = (id-1) * 2; Ar = $('#'+ref).val().split('|||'); Ar[k] = a; Ar[k+1] = b; $('#'+ref).val(Ar.join('|||')); } ";
				$OpenBookHoursFunctionLoaded = true;
				}
			
			// tempval = '22|||32|||22|||32|||22|||32|||22|||32|||22|||32|||22|||32|||22|||32|||32|||46|||32|||46|||32|||46|||32|||46|||32|||46|||32|||46|||32|||46'; 
			$globalScript .= "setUp('$reference');\n";
			break;
						
		case "itemwheeldescription":

			printf("<table class='table table-striped'>");
			printf("<thead><tr><th>Parts</th><th>Offer  <i class='icon-gift icon-2x' style='color:green;'></i></th><th>Value  <i class='icon-signal icon-2x' style='color:green;'></i></th><th>Terms and conditions  <i class='icon-info-sign icon-2x' style='color:green;'></i></th></tr></thead>");
			$limit = count($wheeloffers) / 3;
			for($i = 0; $i < $limit; $i++)
				{
				$index = $i * 3;
				$offer = $wheeloffers[$index];
				$offerval = $wheeloffers[$index + 1];
				$offerdesc = $wheeloffers[$index + 2];
				$class = ($i % 2 == 0)? "style='background-color:white;'":"class='success'";
				printf("<tr $class><td width='10%%' class='gray'><span style='font-size: 18px; color: black; text-shadow: 5px 5px 10px black;'>%d</span></td>", $i + 1);
				printf("<td width='20%%' nowrap>%s</td><td width='15%%' nowrap>%s</td><td>%s</td>", $offer, $offerval, $offerdesc);
				printf("</tr>");
				}
			printf("</table>");	
			break;
			
		case "itemwheelstddescription":

			printf("<table class='table table-striped'>");
			printf("<thead><tr><th>Parts</th><th>Offer  <i class='icon-gift icon-2x' style='color:green;'></i></th><th>Value  <i class='icon-signal icon-2x' style='color:green;'></i></th><th>Terms and conditions  <i class='icon-info-sign icon-2x' style='color:green;'></i></th></tr></thead>");
			$limit = count($stdwheeloffers);
			for($i = 0; $i < $limit; $i++)
				{
				$index = $stdwheeloffers[$i] * 3;
				$offer = $wheeloffers[$index];
				$offerval = $wheeloffers[$index + 1];
				$offerdesc = $wheeloffers[$index + 2];
				$class = ($i % 2 == 0)? "style='background-color:white;'":"class='success'";
				printf("<tr $class><td width='10%%'><span style='font-size: 18px; color: black; text-shadow: 5px 5px 10px black;'>%d</span></td>", $i + 1);
				printf("<td width='20%%' nowrap>%s</td><td width='15%%' nowrap>%s</td><td>%s</td>", $offer, $offerval, $offerdesc);
				printf("</tr>");
				}
			printf("</table>");	
			break;
			
		case "itemwheelchoice":
		
			$parts = $wheeldata->summary();
			if(count($parts) < 1)
				break;
				
			printf("<table class='table table-striped'>");
			for($k = 1; list($lab, $val) = each($parts); $k++) {
				printf("<tr><td width='10%%'><span style='font-size: 18px; color: black; text-shadow: 5px 5px 10px black;'>%d</span></td>", $k);
				printf("<td nowrap>(%d parts) %s</td></tr>", $val, $lab);
				}
			printf("<tr><td width='10%%' colspan = '2'><span style='font-size: 18px; color: black; text-shadow: 5px 5px 10px black;'>%s &nbsp;&nbsp; %d</span>", "Total Value", $wheeldata->wheelvalue);
			printf("</table>");	
			break;
			
		case "itemwheelvalue":

			$wheel_short_offers = array();
			reset($wheelvalues);
			while(list($label, $value) = each($wheelvalues))
				$wheel_short_offers[] = $label;

			$wheeldata->wheel = $wheeldata->readWheel();
			if(empty($wheeldata->wheel))
				break;
			
			$log = (count($wheeldata->wheelpart) > 1 && $wheeldata->valid);
			$limit = 24;
			printf("<table class='table'>");
			for($i = 0; $i < $limit; $i++)
				{
				$value = ($log) ? $wheeldata->wheelpart[$i] : "";
				printf("<tr><td width='10%%'><span style='font-size: 18px; color: black; text-shadow: 5px 5px 10px black;'>%d</span></td>", $i+1);
				printf("<td nowrap>"); 
				$str = print_list($wheel_short_offers, "", $reference . ($i+1), "Select", "wlist");	
				echo "<div class='col-sm-6'>" . print_list($wheel_short_offers, $value, $reference . ($i+1), "Select", "wlist") . "</div>";	
				printf("</td></tr>");
				}
			printf("<tr class='default' height='1'><td colspan=2' height='1'></td></tr></table>");	
			break;
			
		case "itemwheelimage":
		
			$wheeldata->wheel = $wheeldata->readWheel();
			if(empty($wheeldata->wheel))
				break;
			
			if($wheeldata->valid)
				 printf("<img src='%s' id='wheel_image' name='wheel_image' />", $wheelimage->showPicture($theRestaurant, "wheel.png?tt=" . time()));
			else printf("<img src='%s' id='wheel_image' name='wheel_image' />", $wheelimage->rootdir . "wheel/wheel.png");
			break;

		case "itemevoucherhistory" :
			printf("<tr><td width='200'>Purchase Date</td><td width='200'>Value Pack Name</td><td width='200'>Number of E-vouchers</td></tr>");	
			printf("<tr><td>Date1</td><td>ValuePackName1</td><td>Nbevouchers1</td></tr>");	
			break;

		case "itemvisithistory" :		
			printf("<tr><td width='120'>Visit Date</td><td width='120'>Restaurant Name</td><td width='120'>City</td><td width='120'>Weeloy Wheel</td><td width='120'>E-Voucher</td></tr>");
			printf("<tr><td>Date1</td><td>Restaurant1</td><td>City1</td><td>Promotion1</td><td>Redeem1</td></tr>");
			printf("<tr><td colspan='4'>Number of E-Vouchers Left: value1</td></tr>");
			break;

		case "contactinformation" :
			contactinformation($theRestaurant, $email);
			break;

		case "reportbooking" :
			$multProduct = ($resdata->mutipleProdAllote()) ? $resdata->dfproduct : "";
			reportbooking($theRestaurant, $email, $logo, $perms, $multProduct);
			break;
			
		case "allotment" :
			$twoSitting = ($resdata->twoSitting()) ? 1:0;
			$multProduct = ($resdata->mutipleProdAllote()) ? $resdata->dfproduct : "";
			allotment($theRestaurant, $email, $twoSitting, $multProduct);
			break;
			
		case "blocking" :
			$multProduct = ($resdata->mutipleProdAllote()) ? $resdata->dfproduct : "";
			blocking($theRestaurant, $email, $multProduct);
			break;
			
		case "reportreview" :
			reportreview($theRestaurant, $email);
			break;			

		case "consumerprofile" :
			consumerprofile($theRestaurant, $email, $logo, ($perms & __EDITPROFILES__));
			break;			

		case "services" :
			service($theRestaurant);
			break;
					
		case "promotion" :
			promotion($theRestaurant, $email);
			break;

		case "blocklist" :
           		blocklist($theRestaurant, $email);
			break;
			
		case "callcenter" :
			callcenter($email, $perms, $theRestaurant);
			break;

		case "callcentermember" :
           		callcentermember($theRestaurant, $email);
			break;
					
		case "pabx" :
           		pabx($theRestaurant, $email);
			break;
			
		case "authentication" :
			authentication($theRestaurant, $email);
			break;
		
		case "smstemplate" :     
			smstemplate($theRestaurant, $email);
			break;
                                      
		case "tmsdayreportlunch":
			$title = preg_replace("/\s+/", "_", $resdata->title);
			if($resdata->tmsApplication())
				tmsdayreportlunch($theRestaurant, $title, $email, $token);
			break;
			
		case "tmsdayreportdinner":
			$title = preg_replace("/\s+/", "_", $resdata->title);
			if($resdata->tmsApplication())
				tmsdayreportdinner($theRestaurant, $title, $email, $token);
			break;
			
		case "tmssytem" :
			tmssytem($theRestaurant, $email, $resdata);
			break;

		case "menuadmin" :
			$takeoutflg = $resdata->takeOutRestaurant();
			menuadmin($theRestaurant, $takeoutflg, $label, $email);
			break;
                    
		case "dsbmanagement" :
			dsbmanagement($theRestaurant, $email);
			break;
                    
		case "eventadmin" :
			eventadmin($theRestaurant, $email);
			break;
			
		case "payment" :
			payment($theRestaurant, $email);
			break;
			 			
		case "itemmgtimage":
			imageadmin($theRestaurant, $email);
			break;

                case "itemmgtdoc":                
			mgtidocument($theRestaurant, $email);
			break;
			
		case "booksystem" :
			booksystem($email);
			break;

		case "bkreport" :
			bkreport($email);
			break;

		case "invoicing" :
			invoicing($email);
			break;

		case "mgtinvoice" :
			mgtinvoice($email);
			break;
			
                case "mgtsmsinvoice" :
			mgtsmsinvoice($theRestaurant, $email);
			break;

		case "viewinvoice" :
			viewinvoice($email);
			break;

		case "bkanalytics" :
			bkanalytics($email);
			break;

		case "bkanalyticsresto" :
			bkanalyticsresto($theRestaurant, $email);
			break;

		case "takeoutorder" :
			takeoutorder($theRestaurant, $email);
			break;

		case "eventordering" :
			eventordering($theRestaurant, $email);
			break;
                case "eventsendform" :
			eventsendform($theRestaurant, $email);
			break;

		case "extrawheel" :
			extrawheel($theRestaurant, $email, $resdata);
			break;

			                  
		default: 
			return;
		}
}


function print_body() {
	
	global $navbar, $navbarTitle, $theRestaurant, $inputAr, $dataobject;

	if($navbar == "LOG") {
		//echo "<div src='../modules/login/login.php' width='400' height='800' frameBorder='0' scrolling='no' seamless></div>";
		return;
		}
		
	if($navbar == "HOME") {
            if(isset($_SESSION['info_message'])){
                echo '<div class="alert alert-'.$_SESSION['info_message']['type'].'"><a href="#" class="close" data-dismiss="alert">&times;</a><strong></strong> '.$_SESSION['info_message']['message'].'</div>';
                unset($_SESSION['info_message']);
            }
		
//        require_once("inc/carousel.inc.php");
		require_once("inc/latestnews.inc.php");
		return;
		}
		
	if($inputAr == "")
		return;
		

	$resdata = new WY_restaurant();

	print_selectlist($resdata, $theRestaurant, $navbarTitle);
	print_allinput($resdata, $dataobject, "backoffice");
}
?>
