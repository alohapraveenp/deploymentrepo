<div class="container">
    <div class="row">
        <div class="col-md-12 left-sec">

<div id="Menubackoffice" ng-controller="MenuController" ng-init="moduleName='menu'; listingMenuFlag = true; listing1MenuFlag=false; createMenuFlag=false;" >

	<div id='listing' ng-show='listingMenuFlag'>
		<div class="form-group"  style='margin-bottom:25px;'>
			<div class="col-md-4">
				<div class="input-group col-md-4">
					<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
					<input type="text" class="form-control input-sm" ng-model="searchText" ng-change="paginator.setPage(0);" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" style='width:200px;'> 
				</div>
			</div>
			<div class="col-md-2"></div>
			<div class="col-md-4">
			<a href='javascript:;' ng-click='create();' class="btn btn-success btn-sm" style='color:white;width:200px;'><span class='glyphicon glyphicon-certificate'></span> &nbsp;Create a New Menu</a>
			</div>
		</div><br/>

		<table width='100%' class="table table-condensed table-striped" style='font-size:12px;'>
			<thead>
				<tr>
				<th ng-repeat="y in tableMenutitle"><tbtitle name="{{y.b}}"/></th>
				<th>update</th><th> &nbsp; </th><th>delete</th>
				</tr>
			 </thead>
			<tbody style='font-family:helvetica;font-size:12px;'>
				<tr ng-repeat="x in filteredMenu = (names | filter:searchText) | sizefilter:this | orderBy:predicate:reverse | slicefilter:paginator.getPage():paginator.getRowperPage():paginator.getItemCount() | limitTo:paginator.getRowperPage()" style='font-family:helvetica;font-size:11px;'>
					<td>{{ x.index}}</td>
					<td><a href="javascript:;" ng-click="view(x.index)" style='color:red;'>{{ x.categorie.value}}</a></td>
					<td>{{ x.categorie.morder}}</td>
					<td><a href="javascript:;" ng-click="update(x.index)" style='color:blue;'><span class='glyphicon glyphicon-pencil'></a></span></td>
					<td width='30'>&nbsp;</td>
					<td nowrap><a href="javascript:;" ng-click="delete(x.index)" style='color:red;'><span class='glyphicon glyphicon-trash'></a></span></td>
				</tr>
	            <tr><td colspan='6'></td></tr>
			</tbody>
		</table>
		<div class="col-md-12" align="center" style='font-size:11px' ng-include="'inc/paginator.html'"></div>
	</div>

	<div class="col-md-12" ng-show='listing1MenuFlag'>
		<br />
		<a href="javascript:;" class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a>
		<br />
		<table class='table-striped' ng-repeat="x in SelectedItem" style="margin: 0 0 10px 30px;font-size:13px;font-family: Roboto">		

			<tr><td nowrap><strong>Menu Title: </strong></td><td> &nbsp; </td><td class="strong" >{{ x.categorie.value}}</td></tr>
			<tr><td class="strong" nowrap>Description: </td><td> &nbsp; </td><td>{{ x.categorie.description}}</td></tr>
			<tr><td class="strong" nowrap>Preference Order: </td><td> &nbsp; </td><td>{{ x.categorie.morder}}</td></tr>
			<tr><td class="strong" nowrap>Menu Items: </td><td> &nbsp; </td><td>  &nbsp;  </td></tr>
            <tr ng-repeat="y in x.items track by $index" ng-if="y.item_title != '' && y.type == 'item'" ><td colspan='3'>
				<table>
					<tr><td width='90' align='right'>
					<span ng-show="y.vegi == '1'"><img src='../images/restaurant_icons/vegi.png' height='16' width='16''></span>
					<span ng-show="y.spicy1 == '1'"><img src='../images/restaurant_icons/spicy1.png' height='20' width='20''></span>
					<span ng-show="y.spicy2 == '1'"><img src='../images/restaurant_icons/spicy2.png' height='20' width='20''></span>
					<span ng-show="y.spicy3 == '1'"><img src='../images/restaurant_icons/spicy3.png' height='20' width='20''></span>
					<span ng-show="y.halal == '1'"><img src='../images/restaurant_icons/halal.png' height='16' width='16''></span>
					<span ng-show="y.chef_reco == '1'"><img src='../images/restaurant_icons/chef_reco.png' height='16' width='16''></span>
					<span ng-show="y.takeout == '1' && takeoutflg === true"><img src='../images/restaurant_icons/takeout.png' height='20' width='20''></span>
					</td><td width='30'></td><td class="strong" >{{ y.item_title }}</td></tr>
					<tr><td></td><td></td><td>{{ y.item_description }}</td></tr>
				</table>
                </td></tr>
		</table>
	</div>

	<div class="col-md-12" ng-show='createMenuFlag'>
		<br />
		<a href="javascript:;" class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a>
		<br />
		 <div class="col-md-2"></div>
		 <div class="col-md-8">
			   <div class="row">
				<div class="row">
				<div class="input-group">
					<span class="input-group-addon"><i class="glyphicon glyphicon-tower"></i></span>
					<input type="hidden" class="form-control input-sm" ng-model="nmenu"> 
					<input type="text" class="form-control input-sm" ng-model="ntitle" placeholder="title of the menu"> 
				</div></div><br />
                                
				<div class="row">
				<div class="input-group">
					<span class="input-group-addon"><i class="glyphicon glyphicon-tag"></i></span>
					<input type="text" class="form-control input-sm" ng-model="ndescription" placeholder="description of the menu" > 
				</div></div><br />

				<div class="row">
				<div class="input-group">
					<span class="input-group-addon"><i class="glyphicon glyphicon-sort"></i></span>
					<input type="text" class="form-control input-sm" ng-model="nmorder" placeholder="order preference"> 
				</div></div><br />

				<h3><center>Menu Items Description</center></h3>
				<div ng-repeat="x in itemListIndex" class="row">
				<div class="input-group">
					<span class="input-group-addon"><i class="glyphicon glyphicon-tasks"></i></span>
                    <input type="hidden" class="form-control input-sm" ng-model="menuid[x]" placeholder="title of the menu"> 
					<input type="text" class="form-control input-sm" ng-model="menutitle[x]" maxlength='120' placeholder="Title item {{ x }}"> 
				</div>
				<div class="input-group">
					<span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>
					<input type="text" class="form-control input-sm" ng-model="menudescription[x]" maxlength='200' placeholder="Menu item {{ x }}"> 
				</div>
                    <div class="input-group">
					<span class="input-group-addon"><i class="glyphicon glyphicon-usd"></i></span>
					<input type="text" class="form-control input-sm" ng-model="menuprice[x]" maxlength='20' placeholder="Price"> 
				</div>
				<div class="input-group">
					<table width='100%' style='padding: 18px;margin: 10px;font-size: 12;'>
						<tr><td><span> Spicy 1 </span></td><td><input type="checkbox" ng-model="menuspicy1[x]" ng-true-value="'1'" ng-false-value="'0'"></td>
						<td rowspan='7'>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</td><td rowspan='7' valign='top'>
								<div class='input-group'>
								<div class='input-group-btn' dropdown>
									<button type='button' class='btn btn-default dropdown-toggle btn-sm' data-toggle='dropdown'>
										&nbsp;<i class="glyphicon glyphicon-image"></i>&nbsp; Select an image <span class='caret'></span>
									</button>
									<ul class='dropdown-menu scrollable-menu'>
										<li ng-repeat="p in imageobject"><a href="javascript:;" ng-click="setImage(x,p.name);">{{ p.name }}</a></li>
									</ul></div>
								<input type='text' ng-model='mimage[x]' class='form-control  input-sm' readonly >
								</div>
								<br />
								<p ng-show="showImage(x);"><img ng-src="{{path}}{{mimage[x]}}" height='70'/></p>
								</td></tr>
						<tr><td><span> Spicy 2 </span></td><td><input type="checkbox" ng-model="menuspicy2[x]" ng-true-value="'1'" ng-false-value="'0'"></td></tr>
						<tr><td><span> Spicy 3 </span></td><td><input type="checkbox" ng-model="menuspicy3[x]" ng-true-value="'1'" ng-false-value="'0'"></td></tr>
						<tr><td><span> Halal </span></td><td><input type="checkbox" ng-model="menuhalal[x]" ng-true-value="'1'" ng-false-value="'0'"></td></tr>
						<tr><td><span> Chef Reco </span></td><td><input type="checkbox" ng-model="menuchef_reco[x]" ng-true-value="'1'" ng-false-value="'0'"></td></tr>
						<tr><td><span> Vegetarian </span></td><td><input type="checkbox" ng-model="menuvegi[x]" ng-true-value="'1'" ng-false-value="'0'"></td> </tr>
						<tr ng-if='takeoutflg'><td><span> Take Out </span></td><td><input type="checkbox" ng-model="takeout[x]" ng-true-value="'1'" ng-false-value="'0'"></td> </tr>
					</table>
				</div>
                               
				<br />
				</div>
				 <a href='javascript:;' ng-click='onemore();' class="btn btn-info btn-sm" style='color:white;'><span class='glyphicon glyphicon-plus'></span> &nbsp;one more </a><br /><br /><br /><br />
				<hr />

			</div>
		</div>
		 <div class="col-md-2"></div>
		<div class="col-md-8"></div>
		<div class="col-md-4">
		<a href='javascript:;' ng-click='savenewmenu();' class="btn btn-success btn-sm" style='color:white;width:200px;'><span class='glyphicon glyphicon-save'></span> &nbsp;{{ buttonlabel }} </a><br /><br /><br /><br />
		</div>

	</div>
</div>

</div>
</div>
</div>

<script>

var token = <?php echo "'".$_SESSION['user_backoffice']['token']."';"; ?>

<?php
    $mediadata = new WY_Media($theRestaurant);
    printf("var pathimg = '%s';", $mediadata->getFullPath('small'));
?>


app.controller('MenuController', function($scope, $http) {
	
	$scope.paginator = new Pagination(25);
	$scope.restaurant = <?php echo "'".$theRestaurant."';"; ?>
	$scope.typeselection = 1;
	$scope.takeoutflg = <?php echo ($takeoutflg != 0) ? 'true' : 'false'; ?>;
	
	$scope.tmpArr = [];
        
	$scope.menuid = [];
	$scope.menutitle = [];
	$scope.menudescription = [];
	$scope.menuprice = [];
	$scope.menuspicy1 = [];
	$scope.menuspicy2 = [];
	$scope.menuspicy3 = [];
	$scope.menuhalal = [];
	$scope.menuvegi = [];
	$scope.menuchef_reco = [];
	$scope.takeout = [];
	$scope.mimage = [];
        
	$scope.predicate = $scope.oldpredicate = 'index';
	$scope.reverse = 'false';
	$scope.tableMenutitle = [ {'a':'index', 'b':'index' }, {'a':'name', 'b':'Menu Title' }, {'a':'morder', 'b':'Preference Order' } ];

	$scope.itemListIndex = [ 0 ];
	$scope.glbItemIndex = -1000;
	$scope.imageopened = false;
	$scope.imageobject = [];
	$scope.path = pathimg;
	
	var url = '../api/menulist/' + $scope.restaurant;       
	$http.get(url).success(function(response) {
	
		$scope.names = response.data.menu;  
		for (i = 0; i < $scope.names.length; i++) {
			$scope.names[i].index = i + 1;
			}
		$scope.paginator.setItemCount($scope.names.length);
		$scope.orgnames = $scope.names.slice(0);
		$scope.predicate = $scope.oldpredicate = "index";
		$scope.reverse = false;
		$scope.setContent("index");
		});

	url = '../api/restaurant/media/picture/' + $scope.restaurant;
	$http.get(url).success(function(response) {
	
		var obj = response.data;
		$scope.imageobject = [];
		for (i = 0; i < obj.length; i++) {
			if(obj[i].object_type === 'menu')
				$scope.imageobject.push(obj[i]);
			}
		});

	$scope.setImage = function(x, name) {
		$scope.mimage[x] = name;
		};

		
	$scope.showImage = function(x) {
		return (typeof $scope.mimage[x] === 'string' && $scope.mimage[x].length > 0);
		};

		
	$scope.onemore = function() {
		$scope.menuid.push(String($scope.glbItemIndex--));
		$scope.menutype.push('item');
		$scope.menutitle.push('');
		$scope.menudescription.push('');
		$scope.menuprice.push('');
		$scope.menuvegi.push('');
		$scope.menuspicy1.push('');
		$scope.menuspicy2.push('');
		$scope.menuspicy3.push('');
		$scope.menuhalal.push('');
		$scope.menuchef_reco.push('');
		$scope.takeout.push('');
		$scope.mimage.push('');

		$scope.itemListIndex.push($scope.itemListIndex.length);
		}
		
	$scope.reorder = function(item) {
		predval = item;
		if(predval == 'name') predval = "categorie.value";
		else if(predval == 'morder') predval = "categorie.morder";
		$scope.predicate = predval;
		if ($scope.oldpredicate == predval)
			$scope.reverse = !$scope.reverse;
		else $scope.reverse = false;
		$scope.oldpredicate = predval;
		$scope.setContent(item);
		};
	
	$scope.setContent = function(item) {
		toggleCarat(item, $scope.moduleName, $scope.reverse);
		};

	$scope.backlisting = function() {

		$scope.SelectedItem = [];
		$scope.listingMenuFlag = true;
		$scope.listing1MenuFlag = false
		$scope.createMenuFlag = false;
		};
		
	$scope.view = function(id) {

		id = id-1;
		
		$scope.SelectedItem = $scope.names.slice(id, id+1);
		$scope.listingMenuFlag = false;
		$scope.listing1MenuFlag = true;
		$scope.createMenuFlag = false;
				
		};

	$scope.reset = function() {

		$scope.listingMenuFlag = false;
		$scope.listing1MenuFlag = false
		$scope.createMenuFlag = true;
		
		$scope.menuid = [];
		$scope.menutype = [];
		$scope.menutitle = [];
		$scope.menudescription = [];
		$scope.menuprice = [];
		$scope.menuvegi =[];
		$scope.menuspicy1 = [];
		$scope.menuspicy2 =[];
		$scope.menuspicy3 = []; 
		$scope.menuhalal = []; 
		$scope.menuchef_reco = [];  
		$scope.takeout = [];  
		$scope.mimage = [];
		$scope.items = [];
		$scope.nmenu = $scope.ntitle = $scope.nmorder = $scope.ndescription = $scope.readonly = "";

		$scope.itemListIndex = [ 0 ];
		}
			
	$scope.create = function() {

		$scope.buttonlabel = "Save new menu";
		$scope.action = "create";

		$scope.reset();
		
		$scope.menuid.push(String($scope.glbItemIndex--));
		$scope.menutype.push('item');

		return false;
		}
				
	$scope.update = function(id) {

		id = id-1;
		$scope.Selectedid = id;
 
 		if($scope.names[id].categorie.ID <= 0) {
			alert("reload the page");
			return;
			}
			
               
		$scope.buttonlabel = "Update new menu";
		$scope.action = "update";
		
		$scope.reset();
		
		$scope.SelectedItem = $scope.names.slice(id, id+1);
		$scope.nmenu = $scope.names[id].categorie.ID;
		$scope.ntitle = $scope.names[id].categorie.value;
		$scope.ndescription = $scope.names[id].categorie.description;
		$scope.nmorder = $scope.names[id].categorie.morder;
		$scope.readonly = "readonly";
		
		limit = $scope.names[id].items.length;                
		for(i = 0; i < limit; i++) {
			obj = $scope.names[id].items[i];
			$scope.menuid.push(obj.ID);
			$scope.menutype.push('item');
			$scope.menutitle.push(obj.item_title);
			$scope.menudescription.push(obj.item_description);
			$scope.menuprice.push(obj.price);
			$scope.menuvegi.push(obj.vegi);
			$scope.menuspicy1.push(obj.spicy1);
			$scope.menuspicy2.push(obj.spicy2);
			$scope.menuspicy3.push(obj.spicy3);
			$scope.menuhalal.push(obj.halal);
			$scope.menuchef_reco.push(obj.chef_reco);
			$scope.takeout.push(obj.takeout);
			$scope.mimage.push(obj.mimage);
			}
		
		limit = $scope.menuid.length;
		for(i = 1; i < limit; i++)
			$scope.itemListIndex.push(i);
	};

	$scope.savenewmenu = function() {

		$scope.ntitle = $scope.cleantitle($scope.ntitle);
		if($scope.ndescription != '') {
             $scope.ndescription = $scope.cleantitle($scope.ndescription);
             }
         $scope.nmorder = $scope.cleannumber($scope.nmorder);
		
		if($scope.ntitle == '' || $scope.ntitle.length < 2)
			return alert('Invalid categorie title');
		if($scope.nmorder == '')
			return alert('Invalid number');

	// you don't want to keep blank title, this is one way to delete. But you still need to send the information to the server
	// use 2 different array, one to be sent, one to update local names
		$scope.data = [];
		$scope.lcdata = [];	

		for(i = 0; i < 20; i++) {
            if($scope.menutitle[i] !== undefined && $scope.menudescription[i] !== undefined) {
            	$scope.menuid[i] = $scope.cleannumber($scope.menuid[i]);
            	$scope.menutitle[i] = $scope.cleantitle($scope.menutitle[i]);
            	$scope.menudescription[i] = $scope.cleantitle($scope.menudescription[i]);
            	$scope.menuprice[i] = $scope.cleaninput($scope.menuprice[i]);
            	$scope.menuspicy1[i] = $scope.cleanflag($scope.menuspicy1[i]);
            	$scope.menuspicy2[i] = $scope.cleanflag($scope.menuspicy2[i]);
            	$scope.menuspicy3[i] = $scope.cleanflag($scope.menuspicy3[i]);
            	$scope.menuhalal[i] = $scope.cleanflag($scope.menuhalal[i]);
            	$scope.menuvegi[i] = $scope.cleanflag($scope.menuvegi[i]);
            	$scope.menuchef_reco[i] = $scope.cleanflag($scope.menuchef_reco[i]);
            	$scope.takeout[i] = $scope.cleanflag($scope.takeout[i]);
            	$scope.mimage[i] = $scope.cleantitle($scope.mimage[i]);
                $scope.data.push({ID:$scope.menuid[i], item_title:$scope.menutitle[i], type:'item', item_description:$scope.menudescription[i], price:$scope.menuprice[i], spicy1:$scope.menuspicy1[i], spicy2:$scope.menuspicy2[i], spicy3:$scope.menuspicy3[i], halal:$scope.menuhalal[i], vegi:$scope.menuvegi[i], chef_reco:$scope.menuchef_reco[i], takeout:$scope.takeout[i], mimage: $scope.mimage[i], morder:i});
                if($scope.menutitle[i] != '')
                	$scope.lcdata.push({ID:$scope.menuid[i], item_title:$scope.menutitle[i], type:'item', item_description:$scope.menudescription[i], price:$scope.menuprice[i], spicy1:$scope.menuspicy1[i], spicy2:$scope.menuspicy2[i], spicy3:$scope.menuspicy3[i], halal:$scope.menuhalal[i], vegi:$scope.menuvegi[i], chef_reco:$scope.menuchef_reco[i], takeout:$scope.takeout[i],  mimage: $scope.mimage[i], morder:i});
                }
            }
            

		if($scope.action == "update") {
			apiurl = '../api/menu/update';
			msg = "updated";
			id = selectnew = $scope.Selectedid;
			$scope.names[id].categorie.value = $scope.cleaninput($scope.ntitle);
            $scope.names[id].categorie.description = $scope.cleaninput($scope.ndescription);
			$scope.names[id].categorie.morder = $scope.cleannumber($scope.nmorder);
			$scope.names[id].nitems = $scope.lcdata.length;
			$scope.names[id].items = $scope.lcdata.slice(0);	
			
			//alert($scope.names[id].items[0].item_title);
			}
			
		else if($scope.action == "create") { 
			apiurl = '../api/menu/create';
			msg = "created";
			if($scope.names.length <= 0) $scope.names = [];
			newindex = ($scope.names.length > 0) ? $scope.names[$scope.names.length - 1].index + 1 : 1;
			$scope.ntitle = $scope.cleaninput($scope.ntitle);
			$scope.ndescription = $scope.cleaninput($scope.ndescription);
			$scope.nmorder = $scope.cleannumber($scope.nmorder);
			$scope.names.push({index: newindex, categorie:{ID: -1, value:$scope.ntitle, description:$scope.ndescription, morder:$scope.nmorder }, items:{}});
			selectnew = $scope.Selectedid = $scope.names.length - 1;
        	$scope.names[selectnew].nitems = $scope.lcdata.length;
            $scope.names[selectnew].items = $scope.lcdata.slice(0);
			}

		//alert($scope.names[$scope.Selectedid].items.length + ' ' + $scope.names[$scope.Selectedid].items[$scope.names[$scope.Selectedid].items.length - 1].item_title);
		
		recordname = $scope.ntitle;
		console.log($scope.data);
		$.ajax({
			url : apiurl,
			type: "POST",
			data: {
				//name: $scope.nmenu,
				restaurant: $scope.restaurant,
				id: $scope.nmenu,
				title: $scope.ntitle,
				description: $scope.ndescription,
				morder: $scope.nmorder,
                items: $scope.data,
				token:token
				},
			success: function(data, textStatus, jqXHR) {
						if(data.data != "" && data.data) {
							dataAr = data.data.split('|');
							cursor = 0;
							if(msg == "created") 
								$scope.names[selectnew].categorie.ID = dataAr[cursor++];
							
							for(; cursor < dataAr.length; cursor++) {
								pattern = dataAr[cursor].split('=');
								for(j = 0; $scope.names[selectnew].items.length; j++) {
									if($scope.names[selectnew].items[j].ID == pattern[0]) {
										$scope.names[selectnew].items[j].ID = pattern[1];
										break;	
										}
									}
								}
							}
						console.log($scope.names[selectnew]);
						alert("'" + recordname + "' has been " + msg);
						},
				error: function (jqXHR, textStatus, errorThrown) { alert("Unknown Error. Menu '" + recordname + "' has NOT been " + msg + "." + textStatus); }
			});
		
		$scope.orgnames = $scope.names.slice(0);
 		$scope.backlisting();		
		};
		
	$scope.delete = function(id) {
	
		id = id-1;
		if(confirm("Are you sure you want to delete " + $scope.names[id].categorie.value) == false)
			return;
		
		deletename = $scope.names[id].categorie.value;
		var apiurl = '../api/menu/delete';
		
		$.ajax({
			url : apiurl,
			type: "POST",
			data: {
				restaurant: $scope.restaurant,
				id: $scope.names[id].categorie.ID,
				token:token
				},
			success: function(data, textStatus, jqXHR) { alert(deletename + " has been deleted"); },
				error: function (jqXHR, textStatus, errorThrown) { alert("Unknown Error. " + deletename + " Menu has NOT been deleted. " + textStatus); }
			});
		
		$scope.names = [];
		for(i = k = 0; i < $scope.orgnames.length; i++)
			if(i != id) {
				$scope.orgnames[i].index = ++k;
				$scope.names.push($scope.orgnames[i]);
				}
		$scope.orgnames = $scope.names.slice(0);
		};
	
	$scope.cleantitle = function(data) {
		if(data == '' || !data) return '';		
		return (data != '' && data.length > 0) ? data.replace(/[!@#$%^*=}{\]\[\"\':;><\?/|\\]/g, ' ') : '';
		};
		
	$scope.cleaninput = function(data) {
		if(data == '' || !data) return '';
		return (data != '' && data.length > 0) ? data.replace(/'|"/g, '`') : '';
		};
		
	$scope.cleannumber = function(data) {
		if(data == '' || !data) return '';
		return (data != '' && data.length > 0) ? data.replace(/[^0-9\-\+]/g, '') : '';
		};
		
	$scope.cleanflag = function(data) {
		if(data == '' || !data) return '';
		return (data != '' && data.length > 0) ? data.replace(/[^0-1]/g, '') : '';
		};
		
	});

</script>

