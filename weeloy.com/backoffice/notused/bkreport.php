<?php
require_once("lib/Browser.inc.php");
$browser = new Browser();

require_once("conf/conf.init.inc.php");
require_once("lib/wpdo.inc.php");
require_once("conf/conf.session.inc.php");
require_once("lib/wglobals.inc.php");
require_once("lib/class.images.inc.php");
require_once("lib/class.restaurant.inc.php");
require_once("lib/class.member.inc.php");
require_once("lib/class.booking.inc.php");

?>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="refresh" content="300">
<meta http-equiv='expires' content='mon, 1 jan 1998 05:00:00 gmt'>
<meta http-equiv='pragma' content='no-cache'>
<meta http-equiv='pragma' content='cache-control: max-age=0'>
<meta http-equiv='last-modified' content='mon, 1 jan 1998 05:00:00 gmt'>
<meta http-equiv='cache-control' content='no-cache, must-revalidate'>
<meta name='robots' content='noindex, nofollow'>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="HandheldFriendly" content="true" />
<title>Booking of the day</title>

<base href="<?php echo __ROOTDIR__; ?>/">

<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href='https://fonts.googleapis.com/css?family=Montserrat|Unkempt|Roboto:400,900,700italic,700,500italic,500,400italic' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/bootstrap-formhelpers.js"></script>
<script type='text/javascript' src="js/angular.min.js"></script>

<style>
.truncate { white-space: nowrap; overflow: hidden; text-overflow: ellipsis; }
th { font-size:12px; }
tr { font-size:11px; }
</style>
</head>

<body ng-app="myApp">

	<div id='booking' ng-controller='MainController' ng-init="typeBooking=true;bookingTitle='BOOK OF THE DAY'" >
	 <div class="container mainbox">    
		<div class="panel panel-info">
			<div class="panel-heading">
				<div class="panel-title" > {{ bookingTitle }} </div>
			</div>     

	<div class="col-md-4">
		<div class="input-group col-md-4" style='margin: 20px 0 0 0;'>
			<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
			<input type="text" class="form-control input-sm" ng-model="searchText" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" style='width:200px;'> 
		</div>
		<p style='font-size:12px;margin: 0 0 50px 0;'>selected bookings: <strong> {{filteredPeople.length}} </strong></p>
	</div>
		
	<table width='100%' class="table table-condensed table-striped">
		<thead><tr><th ng-repeat="y in tabletitle"><tbtitle name="{{y.b}}"/></th></tr></thead>
		<tbody>
		<tr ng-repeat="x in filteredPeople = (booking | filter:searchText) | sizefilter:this | orderBy:predicate:reverse | slicefilter:paginator.getPage():paginator.getRowperPage():paginator.getItemCount() | limitTo:paginator.getRowperPage()">
			<td>{{ x.index }}</td>
			<td>{{ x.confirmation }}</td>
			<td><div class='truncate' style='width:140px'>{{ x.restaurant }}</div></td>
			<td>{{ x.status }}</td>
			<td>{{ x.type }}</td>
			<td>{{ x.cover }}</td>
			<td>{{ x.rtime }}</td>
			<td><div class='truncate' style='width:80px'>{{ x.fullname }}</div></td>
			<td>{{ x.rdate }}</td>
			<td>{{ x.cdate }}</td>
			<td><div class='truncate' style='width:80px'>{{ x.wheelwin }}</div></td>
			<td>{{ x.membCode }}</td>
		</tr>
	</table

</div>
</div>
</div>
<div class="col-md-12" align="center" style='font-size:11px' ng-include="'backoffice/inc/paginator.html'"></div>


<script>


<?
if(isset($_REQUEST['days'])) echo "var whenday = " . intval($_REQUEST['days']) . ";";
else echo "var whenday = 0;";
if(isset($_REQUEST['type'])) echo "var type = '" . $_REQUEST['type'] . "';";
else echo "var type = 'c';";

?>

var app = angular.module("myApp", []);

</script>
<script type="text/javascript" src="backoffice/inc/backofficelib.js"></script>
<script type="text/javascript" src="backoffice/inc/paginator.js"></script>

<script>

app.controller('MainController', function($scope, $http) {

	 $scope.paginator = new Pagination(30);
	 $scope.tabletitle = [ {'a':'index', 'b':'index' }, {'a':'confirmation', 'b':'Confirmation' }, {'a':'restaurant', 'b':'Restaurant' },  {'a':'status', 'b':'Status' },  {'a':'type', 'b':'Type' },  {'a':'cover', 'b':'Pers' },  {'a':'rtime', 'b':'Time' }, {'a':'fullname', 'b':'Name' },  {'a':'rdate', 'b':'Booking Date' },  {'a':'cdate', 'b':'Create Date' },  {'a':'win', 'b':'Wins' } ,  {'a':'membCode', 'b':'MCode' } ];
	 whenday = 30;
	 $http.get("api/restaurant/bookingdayreport/" + whenday + "/" + type).success(function(response) { 	 
		 var trackval = [ "callcenter", "website", "facebook" ], obj ;
		 $scope.booking = [];
		 $scope.rdata = response.data.bookings; 
		 for(i = 0; i < $scope.rdata.length; i++) {
		 	obj = $scope.rdata[i];
		 	if(document.location.hostname != 'localhost')
				if(obj.restaurant === "SG_SG_R_TheFunKitchen"|| obj.restaurant === "SG_SG_R_TheOneKitchen")
					continue; 

			var first = obj.firstname.toLowerCase();
			var last = obj.lastname.toLowerCase();
			if(first === "test" || last === "test")
				continue;
                                                
			obj.index = i+1; 
			obj.cdate = obj.cdate.substr(0,10);
			obj.rtime = obj.rtime.substr(0,5);
			obj.fullname = first + ' ' + last;
			if(obj.type == "booking") { 
				tt = obj.tracking.trim();
				if(tt !== '') tt = tt.toLowerCase();
				if(trackval.indexOf(tt) >= 0)
					obj.type = tt;
				}
			$scope.booking.push(obj);
			}
		$scope.paginator.setItemCount($scope.booking.length);
		$scope.predicate = $scope.oldpredicate = "rdate";
		$scope.reverse = false;
		$scope.setContent("rdate");
		});

	$scope.reorder = function(item) {
		$scope.predicate = item;
		if($scope.oldpredicate == item)
			$scope.reverse = !$scope.reverse;
		else $scope.reverse = (item != 'date') ? false : true;
		$scope.oldpredicate = item;
		$scope.setContent(item);
		};	

	$scope.setContent = function(item) {
		toggleCarat(item, $scope.moduleName, $scope.reverse);
		};
		

});

	$(document).ready(function() {  });

</script>
</body>
</html>

