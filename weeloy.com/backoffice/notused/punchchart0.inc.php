<script>
$(function () {
    // Grab the data
		var data = [0, 0, 0, 0, 23, 0, 0, 0, 0, 40, 0, 15, 10, 0, 17, 0, 14, 12, 20, 16, 0, 0, 5, 28, 35, 11, 0, 2, 35, 0, 22, 0, 0, 4, 3, 0, 29, 5, 34, 0, 0, 26, 0, 0, 0, 0, 29, 8, 0, 31, 0, 19, 35, 0, 0, 0, 0, 1, 20, 0, 0, 21, 7, 5, 17, 23, 9, 0, 6, 0, 38, 27, 12, 0, 0, 0, 37, 23, 17, 0, 32, 0, 0, 7, 0, 0, 27, 0, 0, 18, 0, 1, 0, 39, 3, 0, 0, 0, 0, 0, 17, 30, 0, 2, 27, 22, 0, 0, 0, 0, 40, 38, 8, 0, 0, 8, 0, 9, 0, 38, 0, 0, 18, 15, 38, 0, 0, 0, 24, 0, 30, 0, 10, 0, 0, 0, 20, 30, 0, 0, 34, 0, 0, 30, 0, 11, 30, 0, 0, 20, 3, 0, 0, 0, 26, 3, 28, 23, 0, 0, 0, 21, 0, 0, 0, 20, 0, 0 ],
			axisx = ["12am", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12pm", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11"],
			axisy = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"];
 
     // Draw
    var width = 800,
        height = 450,
        leftgutter = 30,
        bottomgutter = 20,
        r = Raphael("punch_chart", width, height),
        X = 30,
        Y = 25,
        max = Math.round(X / 2) - 1;
    // r.rect(0, 0, width, height, 5).attr({fill: "#000", stroke: "none"});
    for (var i = 0, ii = axisx.length; i < ii; i++) {
        r.text(leftgutter + X * (i + .5), 180, axisx[i]);
    }
    for (var i = 0, ii = axisy.length; i < ii; i++) {
        r.text(10, Y * (i + .5), axisy[i]);
    }
    
    var o = 0;
    for (var i = 0, ii = axisy.length; i < ii; i++) {
        for (var j = 0, jj = axisx.length; j < jj; j++) {
			X = 30;
			Y = 50;
            var R = data[o] && Math.min(Math.round(Math.sqrt(data[o] / Math.PI) * 4), max);
            if (R) {
                (function (dx, dy, R, value, yy) {
                     var color = "hsb(" + [(1 - R / max) * .5, 1, .75] + ")";
                    var dt = r.circle(dx, dy, R).attr({stroke: "none", fill: color});
                    if (R < 6) {
                        var bg = r.circle(dx, dy, 6).attr({stroke: "none", fill: "#000", opacity: .4}).hide();
                    }
                    var dot = r.circle(dx, dy, max).attr({stroke: "none", fill: "#000", opacity: 0});
                    var lbl = r.text(dx, yy - 11, value).hide();
                    dot[0].onmouseover = function () {
                        if (bg) {
                            bg.show();
                        } else {
                            var clr = Raphael.rgb2hsb(color);
                            clr.b = .5;
                            dt.attr("fill", Raphael.hsb2rgb(clr).hex);
                        }
                        lbl.show();
                    };
                    dot[0].onmouseout = function () {
                        if (bg) {
                            bg.hide();
                        } else {
                            dt.attr("fill", color);
                        }
                        lbl.hide();
                    };
                })(leftgutter + X * (j + .5), Y * (i + .5), R, data[o], Y * (i + 1)/2);
            }
            o++;
        }
    }
});
</script>

<div class="plot-container">
<div id='punch_chart' ></div> <!-- style='background-color:black;' -->
</div>
