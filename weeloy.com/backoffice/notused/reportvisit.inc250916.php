<?php
$isTheFunKitchen = (preg_match("/TheFunKitchen/", $theRestaurant)) ? 1 : 0;
$isMitsuba = ($theRestaurant === "SG_SG_R_MitsubaJapaneseRestaurant") ? 1 : 0;
if(empty($bkreport)) $bkreport = 0;

$res = new WY_restaurant;
$res->getRestaurant($theRestaurant);
if ($res->result <= 0) {
    $debug = new WY_debug;
    $debug->writeDebug("ERROR-BACKOFFICE", "REPORT", "Invalid Restaurant :" . $theRestaurant);
    exit;
}
$ccextraction = $res->CCExtractionFormat();
$mediadata = new WY_Media($theRestaurant);
$logo = $mediadata->getLogo($theRestaurant);
$isShowLog = ($res->checkactivitylog() >0) ? 1:0;
?>


<div ng-include="'inc/myModalModif.html'"></div>

<div class="container" ng-controller="BookingController" ng-init="moduleName='booking'; listingVisitFlag = true; contentVisitFlag = false; extractFlag = false; formatDisplay = 'standard';">

	<div ng-show='listingVisitFlag'>
	<div class='row'>
		<div class="col-md-2">
			<div class="form-group" style='margin-bottom:25px;width:150px;'>
				<div class="input-group">
					<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
					<input type="text" class="form-control input-sm" ng-model="searchText" ng-change="paginator.setPage(0);" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" > 
				</div>
			</div>
		</div>
		<div class="col-md-10">
		</form>

		<ul class="nav navbar-nav">
		<li>
			<div class="input-group">
				<span><button  type="button" class="btn btn-default" ng-click="mydata.dateopen($event)"><i class="glyphicon glyphicon-calendar"></i></button></span>
				<input type="text" class="form-control" datepicker-popup="{{mydata.formatsel}}" ng-change='mydata.onchange();' ng-model="mydata.selecteddate" is-open="mydata.start_opened" 
				min-date="mydata.minDate" max-date="mydata.maxDate" datepicker-options="mydata.dateOptions" date-disabled="mydata.disabled(date, mode)" ng-required="true" close-text="Close" style="width:0;opacity:0" />
			</div>
		</li>

		<li>
			<div class="btn-group" style="margin:0 5px 0 5px;">
				<button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Set Selection &nbsp;<span class="caret"></span></button>
				<ul class="dropdown-menu" role="menu">
					<li ng-repeat="x in extratitle"><extitle name="{{x.a}}"/></extitle></li>
					<li><a href class='glyphr glyphicon glyphicon-ok' ng-click="extraselect('reserve')"> by reservation date</a></li>
					<li><a href class='glyphc' ng-click="extraselect('create')"> by creation date</a></li>
					<li ng-if="isMultiple" class="divider"></li>
					<li ng-if="isMultiple"><a href class='glyphc' ng-click="extraselect('oneresto')"> select {{ restotitle }} </a></li>
				</ul>
			</div> 		
		</li>

		<li>
			<form action="echo.php" id="extractForm" name="extractForm" method="POST" target="_blank">
				<input type="hidden" value="testing" ng-model='content' name="content" id="content">
				<a href ng-click='extractSelection();' class="btn btn-success btn-sm" style='color:white;margin-left:5px'>Extract Selection  &nbsp;<span class='glyphicon glyphicon-save'></span></a>
			</form>
		</li>

		<li><button  type="button" class="btn btn-info btn-sm" ng-click="reset('extractFlag')" style="margin-left:5px">Full Extract</button></li>

		<li ng-if="multProd === ''"><button  type="button" class="btn btn-default" ng-click="viewdispo()" style="margin-left:15px"><i class="glyphicon glyphicon-eye-open"></i></button></li>
		
		<li><button  type="button" class="btn btn-default" ng-click="printing()" style="margin-left:5px"><i class="glyphicon glyphicon-print"></i></button></li>

		<li ng-if="isMitsuba"><button  type="button" class="btn btn-default" ng-click="mitsubaValidation()" style="margin-left:5px;color:green"><i class="glyphicon glyphicon-ok"></i></button></li>
		
		<li>
			<div class="btn-group">
				<button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style='font-size:11px;margin-left:15px;'>Page Size <span class="caret"></span></button>
				<ul class="dropdown-menu" role="menu">
					<li ng-repeat="x in paginator.pagerange()" class='glyphiconsize'><a href ng-class="{ 'glyphicon glyphicon-ok glyphiconsize': x === paginator.getRowperPage() }" ng-click="paginator.setRowperPage(x)"> {{x}}</a></li>
				</ul>
			</div> 		
		</li>
		
		</ul>
		</div>
	</div>     
	<div class='row'>
		<div class="col-md-10">
			<span class='infobk' style='margin-left:20px;'>selected bookings: <strong> {{filteredPeople.length}} </strong></span>
			<span class='infobk'>current selection: <strong> {{currentSelection}} </strong></span>
			<span class='infobk'>total pax: <strong> {{getTotalPax();}} </strong></span>
			<span class='infobk'>modified: <strong> * </strong></span>
		</div>
	</div><br/>

	<!-- try ng-switch but doest not seem to work. it has a bad side effect on fileteredPeople !? -->
	<div>
		<table width='100%' class="table table-condensed table-striped" style='font-family:helvetica;font-size:11px;'>
			<thead><tr style='font-family:helvetica;font-size:11px;'><th ng-repeat="y in tabletitle"><tbtitle var="{{y.a}}" name='tabletitle'  module='moduleName'/></th><th ng-if ="multProd !== '' && formatDisplay != 'today'"></th></tr></thead>
			<!-- today normal -->
			<tr ng-if="formatDisplay != 'today'" ng-repeat="x in filteredPeople = (names| filter:searchText) | sizefilter:this | orderBy:predicate:reverse | slicefilter:paginator.getPage():paginator.getRowperPage():paginator.getItemCount() | limitTo:paginator.getRowperPage()" style='font-family:helvetica;font-size:11px;'>
				<td ng-repeat="z in tabletitle">
					
					<tb-listingbkg>
						<a  href ng-click="view(x)">{{ x[z.a] | adatereverse:z.c}}<span ng-if="z.a === 'booking' && x.modified >= 0">*</span> </a>
					</tb-listingbkg>
				</td>
                <td ng-if ="multProd !== ''"><a href popover="{{x.comment}}" data-popover-trigger="mouseenter" ><span ng-if="x.comment !== '' "><i class ='glyphicon glyphicon-asterisk'></i></span></a></td> 
                <td ng-if ="multProd !== '' && x.chckflgsubmit "><a href popover="Menu form has not submit yet " data-popover-trigger="mouseenter" ><span ><i class ='glyphicon glyphicon-warning-sign' style='color:orange'></i></span></a></td>
                
			</tr>

			<!-- today printing -->
			<tr ng-if="formatDisplay == 'today'" ng-repeat="x in filteredPeople = (names| filter:searchText) | sizefilter:this | orderBy:predicate:reverse | slicefilter:paginator.getPage():paginator.getRowperPage():paginator.getItemCount() | limitTo:paginator.getRowperPage()" style='font-family:helvetica;font-size:11px;'>
				<td ng-repeat="z in tabletitle | filter: { a:'!state'}"><tb-listingbkg><a href ng-click="view(x)">{{ x[z.a]  | adatereverse:z.c}}<span ng-if="z.a === 'booking' && x.modified >= 0">*</span></a></tb-listingbkg></td>
				<td><select ng-model="x.state" ng-options="val for val in stateAr" ng-change="savechgstatebkg(x)"></select></td>
			</tr>
			<tr><td colspan='{{tabletitle.length+4}}'></td></tr>
				</table>
				<div ng-if="filteredPeople.length >= paginator.getRowperPage()" class="col-md-12" align="center" style='font-size:11px' ng-include="'inc/paginator.html'"></div>
        </div>
		</div>

	<div ng-show='contentVisitFlag'>
            
		<div class="col-md-12" >
		<br />
		<a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a>
		<br />
                 <h4 style='text-align:center;'>Booking Details </h4>
		<table class='table-striped' style="margin: 0 0 50px 10px;font-size:14px;font-family: Roboto">		
			<tr ng-repeat="z in tabletitleContent | filter:showtest">
				<td nowrap>{{z.b}}: </td><td width='20px'>&nbsp;</td><td>{{ selectedItem[z.a] | adatereverse:z.c }}</td>
			</tr>
			<tr ng-show="formatDisplay == 'today'"><td nowrap>State:</td><td> </td><td style='color:red;'>{{ selectedItem.state}}</td></tr>
			<tr ng-show="isTheFunKitchen"><td nowrap>membCode:</td><td> </td><td>{{ selectedItem.membCode}}</td></tr>
			<tr ng-show="selectedItem.modified >= 0"><td nowrap>Modified Booking: </td><td> </td><td>{{ selectedItem.modifiedinfo }}</td></tr>
			<tr><td>&nbsp;</td><td> &nbsp; </td><td></td></tr>
                      
			<tr ng-show="selectedItem.chckflgcus && selectedItem.booking_deposit_id===''"><td>&nbsp;</td>    
				<td nowrap colspan='2'><strong><a href ng-click="cancelbooking(selectedItem);" class="btn btn-warning btn-xs cancelhover"  style='width:100px'>Cancel booking</a><br /></strong></td>
			</tr>
			<tr ng-show="selectedItem.chckflgnshow && selectedItem.booking_deposit_id === ''"><td>&nbsp;</td>
				<td nowrap colspan='2'><strong><a href ng-click="noshowbooking(selectedItem);" class="btn btn-warning btn-xs cancelhover"  style='width:100px'>Set noshow </a><br /></strong></td>
			</tr>
			<tr ng-show="selectedItem.chckflgcus && selectedItem.booking_deposit_id === ''"><td>&nbsp;</td>
				<td nowrap colspan='2'><strong><a href ng-click="modifymodal(selectedItem)" class="btn btn-warning btn-xs cancelhover"  style='width:100px'>Modify booking</a><br /></strong></td>
			</tr>

			<tr><td>&nbsp;</td><td> &nbsp; </td><td></td></tr>
                        <tr ng-show="selectedItem.isShowDetails && selectedItem.payment_method==='carddetails' "><td nowrap>Cancelled  Charge :</td><td width='20px'> &nbsp;</td><td>SGD &nbsp;{{ selectedItem.amount}}</td></tr>
                        <tr ng-show="selectedItem.isShowDetails && selectedItem.payment_method!=='carddetails' "><td nowrap>Deposit amount : </td><td width='20px'> &nbsp;</td><td>SGD &nbsp;{{ selectedItem.amount}}</td></tr>
                        <tr ng-show="selectedItem.isShowDetails"><td nowrap>Payment Status: </td><td width='20px'> &nbsp;</td><td>{{ selectedItem.depstatus}}</td></tr>
                        <tr ng-show="selectedItem.isShowDetails && selectedItem.deposit === 'paid'"><td nowrap>Payment Method: </td><td width='20px'> &nbsp;</td><td>{{ selectedItem.paymethod}}</td></tr>
                        <tr ng-show="selectedItem.isShowDetails && selectedItem.deposit === 'paid'"><td nowrap>Payment Id: </td><td width='20px'> &nbsp;</td><td>{{ selectedItem.booking_deposit_id}}</td></tr>
           			
			<tr ng-show="selectedItem.depstatus ==='REFUNDED'"><td nowrap>Refunded Amount: </td><td width='20px'> &nbsp;</td><td>SGD &nbsp;{{ selectedItem.refund_amount}}</td></tr>
                        
			<tr><td>&nbsp;</td><td> &nbsp; </td><td></td></tr>
			<tr ng-show='selectedItem.chckflgcus && selectedItem.chckflgspin'><td nowrap><strong><a class='btn btn-primary btn-sm' ng-click="spin(selectedItem)" style='color:white;'>SPIN THE WHEEL</a></strong></td><td> &nbsp; </td><td> &nbsp;</td></tr>
			<tr><td>&nbsp;</td><td> &nbsp; </td><td></td></tr>
		</table>
    
                <div ng-if="selectedItem.chckflgcus && multProd !== '' && selectedItem.burnendlinkflg"  style="margin: 0 0 20px 30px;font-size:13px;font-family: Roboto">
                        <h4 style='text-align:left;'>Menus & Preferences Form </h4>
                        <br /> 
                        <div class="form-group mycheckboxdiv">
                        
                            <label>
                              
                               <input  class="mycheckbox" type="checkbox" ng-model="selectedItem.mnufrmsubmited"  ng-checked ='{{selectedItem.mnufrmsubmited}}' ng-click="updatemnufrmtatus(selectedItem)"  >&nbsp; Received Menus form 
                            </label>
                            <label>
                               
                              &nbsp; <div class="btn-group" ><a class='btn btn-warning btn-sm' ng-click="menuresend(selectedItem);"  style='color:white; margin-left:60px;'> Resend form</a></div>
                            </label>
                        </div>

                    
                </div>
                  
              
                <div ng-show="(selectedItem.chckflgcus || selectedItem.chckflgnshow) && selectedItem.booking_deposit_id !=='' && selectedItem.deposit === 'paid' && selectedItem.bkstatus != 'expired'" style="margin: 0 0 20px 10px;font-size:14px;font-family: Roboto">
                        <h5 style='text-align:left; display:table'>Booking Cancel policy</h5>
                        <div ng-if='selectedItem.resto !="SG_SG_R_TheFunKitchen"  || selectedItem.resto !="SG_SG_R_Bacchanalia" ' style="margin: 0 0 20px 20px;"  > 
                            <div ng-repeat ="p in policy">
                                <span ng-if="p['percentage']==100">{{p['duration']}} SGD {{selectedItem.amount}}.</span>
                                <span ng-if="p['percentage']==0">{{p['duration']}}</p></span>
                            </div>
                         </div>
                        <div ng-if='selectedItem.resto =="SG_SG_R_TheFunKitchen"  || selectedItem.resto =="SG_SG_R_Bacchanalia" ' style="margin: 0 0 20px 20px;"> 
                            <span >{{policybaccahanalia}}</span>
                         </div>
                        <br /> 
                        <div class="amount" style="margin: 0 0 20px 20px;">
                            <p ng-show="selectedItem.amount > 0"><strong> Maximum cancellation  charge : SGD {{selectedItem.amount}} </strong>  </p><br />
                            <span>Amount : &nbsp; SGD&nbsp;</span> <input type="text" ng-model ="selectedItem.refund_amount" >
                            <div class="btn-group" ng-show="!selectedItem.chckflgnshow " style="padding-left:10px;"><a class='btn btn-danger btn-sm' ng-click="cancelpaymentbooking(selectedItem);" class="cancelhover" style='color:white;'> Cancel</a></div>
                            <div class="btn-group" ng-show="selectedItem.chckflgnshow " style="padding-left:10px;" ><a class='btn btn-danger btn-sm' ng-click="noshowpaymentbooking(selectedItem);" class="cancelhover" style='color:white;'> No Show</a></div>
                        </div>
                            
                </div>
                <div ng-show="selectedItem.chckflgrfnd  && selectedItem.booking_deposit_id !==''" style="margin: 0 0 20px 30px;font-size:14px;font-family: Roboto">
                        <h4 style='text-align:center;'>Refund payment</h4>
                        <br /> 
                        <div class="amount" style='text-align:center;'>
                            <span>Amount : &nbsp; SGD&nbsp;</span> <input type="text" ng-model ="selectedItem.res_refund_amount" >
                            <div class="btn-group"  style="padding-left:10px;"><a class='btn btn-danger btn-sm' ng-click="paymentrefund(selectedItem);" class="cancelhover" style='color:white;'>Refund</a></div>
                        </div>
                            
                </div>
                <div ng-if="selectedItem.chckflgpaystatus && selectedItem.bkstatus == 'pending_payment'"  style="margin: 0 0 20px 30px;font-size:14px;font-family: Roboto">
                        <h4 style='text-align:left;'>Payment reminder - Resend Email & Sms </h4>
                       
                        <br /> 
                        <div class="form-group mycheckboxdiv">
                        
                            <label>
                                <span style='margin-bottom:10px;'>Send payment reminder email&nbsp;</span>
                               <input  type="text" ng-model="selectedItem.email"  >&nbsp;  <input type='button' class='btn btn-warning' ng-click="resendpaymentEmail(selectedItem);" value ='Send Email'>
                            </label>
                            <div class='sms-sections'>
                              <label style='margin-top:10px;padding-top:10px;'>
                                <span >Send payment reminder sms:&nbsp;</span>
                               &nbsp;&nbsp;<input type="text" ng-model="selectedItem.phone"  >&nbsp;  <input type='button' class='btn btn-warning' ng-click="resendpaymentsms(selectedItem);"  value ='Send Sms'>
                            </label>
                            </div>
                         
                        </div>

                </div>
                    
                <div ng-if="selectedItem.chckflgpaystatus && selectedItem.bkstatus == 'pending_payment'"  style="margin: 0 0 20px 30px;font-size:14px;font-family: Roboto">
                    <h4 style='text-align:left;'>Waive credit card details </h4>
                       
                        <br /> 
                    <div class="form-group">
                        <label>
                           <input type='button' class='btn btn-warning' ng-click="updatestatus(selectedItem);" value ='Waive CC Details Now'>
                        </label>
                    </div>
                </div>
                <div class='table-striped2' ng-if="eventlog.length>0"  style="margin: 0 0 20px 10px;font-size:14px;font-family: Roboto">
                    <h5 style='text-align:left; display:table'>Actions History</h5>
                        <div ng-repeat ="log in eventlog" style='margin:5px 0 5px 0; display:table-row;padding:5px;'>
                            
                            <span  style="display:table-cell">{{log['name']}} {{log['description']}} <span ng-if="log['action'] == '706' || log['action'] == '717' || log['action'] == '714'  ">(${{log['other']}}) </span> </span>
                            <span style="display:table-cell; padding-left:20px;"> {{log['log_date']}}  </span>  
  
                        </div><br /> 
                </div>

	</div>
	</div>

	<div ng-show='mitsubaFlag'>
		<div class="col-md-12" ng-if='mitsubaFlag'>
		<br />
		<a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a>
		<br />
		<table class='table-striped' style="margin: 0 0 150px 30px;font-size:13px;font-family: Roboto">		
		<thead><tr style='font-family:helvetica;font-size:11px;'><th ng-repeat="y in tabletitle | filter: { b:'!Wins'}"><tbtitle var="{{y.a}}" name='tabletitle'  module='moduleName'/></th><th>validate</th></tr></thead>
		<tr ng-repeat="x in filteredMistuba = (names| filter:{ validate:'!1'}| filter:{ bkstatus:'!cancel'}) | sizefilter:this | orderBy:predicate:reverse | slicefilter:paginator.getPage():paginator.getRowperPage():paginator.getItemCount() | limitTo:paginator.getRowperPage()" style='font-family:helvetica;font-size:11px;'>
			<td ng-repeat="z in tabletitle | filter: { a:'!wheelwin'}"><tb-listingbkg><a href ng-click="view(x, 'mitsubaFlag')">{{ x[z.a] | adatereverse:z.c}}<span ng-if="z.a === 'booking' && x.modified >= 0">*</span></a></tb-listingbkg></td>
			<td><button type='button' class='btn btn-primary btn-xs' ng-click='validatebkg(x)' style='font-size:10px;'>validate</button></td>
		</tr>
		<tr><td colspan='{{tabletitle.length+4}}'>&nbsp;</td></tr>
		<tr><td colspan='{{tabletitle.length+4}}'><div ng-if="filteredMistuba.length >= paginator.getRowperPage()" class="col-md-12" align="center" style='font-size:11px' ng-include="'inc/paginator.html'"></div></td></tr>
		</table>
		
		</div>
	</div>

	<div ng-show='extractFlag'><H3> Extraction </h3>
		<br />
			<a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a>
		<br />
	  <table width='100%'><tr>
		<td width='75'>
		<div class="input-group">
			<span><button  type="button" class="btn btn-default" ng-click="mydatastart.dateopen($event)"><i class="glyphicon glyphicon-calendar"><span style='font-size:9px;'><br />start</span></i></button></span>
			<input type="text" class="form-control" datepicker-popup="dd/mm/yyyy" ng-change='mydatastart.onchange();' ng-model="mydatastart.selecteddate" is-open="mydatastart.opened" min-date="mydatastart.minDate" max-date="mydatastart.maxDate" datepicker-options="mydatastart.dateOptions" ng-required="true" close-text="Close" style="width:0;opacity:0" />
		</div>
		</td>

		<td width='75'>
		<div class="input-group">
			<span><button  type="button" class="btn btn-default" ng-click="mydataend.dateopen($event)"><i class="glyphicon glyphicon-calendar"><span style='font-size:9px;'><br />end</span></i></button></span>
			<input type="text" class="form-control" datepicker-popup="dd/mm/yyyy" ng-change='mydataend.onchange();' ng-model="mydataend.selecteddate" is-open="mydataend.opened" min-date="mydataend.minDate" max-date="mydataend.maxDate" datepicker-options="mydataend.dateOptions" ng-required="true" close-text="Close" style="width:0;opacity:0" />
		</div>
		</td>
		<td width='30'>&nbsp;</td>
		<td>
		<label ng-repeat="oo in bkfield" style="font-size:12px;margin-left:10px;"><input type="checkbox" ng-model="oo.c"/> {{oo.b}} </label>
		</td>
		</tr></table>
		<div class="row"><button type="button" class="btn btn-default btn-xs" ng-click="setallfield()" style="font-size:10px;">set all</button> <button type="button" class="btn btn-default btn-xs" ng-click="clearallfield()" style="margin-left:20px;font-size:10px;">clear all</button></div><br />
		<div class="row"><span style="font-size:10px;">*BDate: Reservation Date<br />*ADate: Annulation/Cancel Date<br />*CDate: Create Date of Booking</span></div>
		
		<div class="row" ng-if="startBkg !== -1" style="font-size:11px;margin-top:20px;">
		start: {{ mydatastart.selecteddate.getDateFormat() }} &nbsp;&nbsp;&nbsp; end: {{ mydataend.selecteddate.getDateFormat() }} 
		</div>
		<br />
			<div align="center"><a href class='btn btn-info btn-sm' ng-click="performextraction()" style='width:200px;color:white;'>Execute Extraction &nbsp;<span class='glyphicon glyphicon-save'></span></a></div>
		<br />
		<br />

	</div>
</div>

<script>

var token = <?php echo "'" . $_SESSION['user_backoffice']['token'] . "';"; ?>
var email = <?php echo "'" . $_SESSION['user_backoffice']['email'] . "';"; ?>
var restaurant = <?php echo "'" . $theRestaurant . "';"; ?>
var currentday = <?php echo "'" . date("Y-m-d") . "';"; ?>
var currentmonth = <?php echo date("m"); ?>;
var currentyear = <?php echo date("Y"); ?>;
var currentjour = <?php echo date("d"); ?>;
var currentweek = <?php echo date("W"); ?>;
var ccextraction = <?php echo "'" . $ccextraction . "';"; ?>
var isTheFunKitchen = <?php echo $isTheFunKitchen . ";"; ?>
var imglogo = <?php echo "'" . $logo . "';"; ?>
var multProd = <?php echo "'".$multProduct."';"; ?>
var isMitsuba = <?php echo "'" . $isMitsuba . "';"; ?>
var isMultiple = <?php echo "parseInt('" . $bkreport . "');"; ?>
var multProd = <?php echo "'".$multProduct."';"; ?>
var isShowLog = <?php echo "'".$isShowLog."';"; ?> 

app.controller('BookingController', ['$scope', 'bookService', '$modal', '$log', 'ModalService', function($scope, bookService, $modal, $log, ModalService) {

    var todayjs = new Date(); todayjs = new Date(todayjs.getFullYear(), todayjs.getMonth(), todayjs.getDate(), 0, 0, 0, 0);

    var aday = new AdayRoutine(currentday);
	var mydata = new bookService.ModalDataBooking();
	var newdate = new Date();
	var startdate, endate;

	if(newdate.getMonth() > 6) {
		startdate = new Date(newdate.getFullYear(), 0, 1);
		endate = new Date(newdate.getFullYear()+1, 11, 31);
	} else {
		startdate = new Date(newdate.getFullYear()-1, 0, 1);
		endate = new Date(newdate.getFullYear(), 11, 31);
		}

	$scope.startBkg = -1;
	$scope.endBkg = newdate.getTime();
	$scope.cdate = newdate;
	$scope.mydata = mydata;

	$scope.mydatastart = new bookService.ModalDataBooking();
	$scope.mydatastart.setInit($scope.cdate, "09:00", function() { $scope.startBkg = $scope.mydatastart.selecteddate.getTime(); $scope.paginator.setPage(0); }, 3, startdate, endate);
	$scope.mydataend = new bookService.ModalDataBooking();
	$scope.mydataend.setInit($scope.cdate, "09:00", function() { $scope.endBkg = $scope.mydataend.selecteddate.getTime() + ((3600 * 24 * 1000) - 1); $scope.paginator.setPage(0); }, 3, startdate, endate);
	$scope.mydataend.setDate(newdate);

	$scope.getAlignment = bkgetalignment;
	
    $scope.showModal = function (title, initval, func, template, mydata, prepostcall) {

		$scope.mydata = mydata;
		$scope.mydata.name = initval;
		if(prepostcall !== null) prepostcall(0);
		
        var modalOptions = {
            closeButtonText: 'Cancel',
            actionButtonText: 'OK',
            headerText: title,
            mydata: mydata,
            submit:function(result){ if(prepostcall !== null) prepostcall(1); $modalInstance.close(result); },
            myclose:function(){ if(prepostcall !== null) prepostcall(1); $modalInstance.dismiss('cancel'); }        

        }
        
        ModalService.setTemplate(template);

        var $modalInstance = ModalService.showModal({}, modalOptions);
        $modalInstance.result.then(function (result) {
             func(result);
        });
    }

	//isMultiple = 0;
	
	
	// would need to be update for multiple product restaurant
	$scope.product = "";
	
	$scope.aSelDay = currentday;
	$scope.paginator = new Pagination(25);
	$scope.restaurant = restaurant;
	$scope.restotitle = restaurant.substring(8,99);
	$scope.email = email;
	$scope.typeselection = 1;
	$scope.tmpArr = [];
	$scope.modifnames = [];
	$scope.isTheFunKitchen = (isTheFunKitchen != 0);
	$scope.isMitsuba = (parseInt(isMitsuba) === 1);
	$scope.thirdpartyflg = false;
	$scope.isMultiple = (isMultiple === 1);
	$scope.oneresto = 0;
	$scope.multProd = multProd;	
	$scope.predicate = '';
	$scope.reverse = false;
	$scope.codebooking = null;
        $scope.isShowLog = (parseInt(isShowLog) === 1);
      

	$scope.tabletitleStandard = [ {a:'index', b:'n', c:'', l:'25', q:'down', cc: 'black' }, {a:'fullname', b:'Name', c:'', l:'70', q:'down', cc: 'black' }, {a:'phone', b:'Mobile', c:'', l:'85', q:'down', cc: 'black' }, {alter: 'vdate', a:'date', b:'BookDate', c:'date', l:'', q:'up', cc: 'orange' }, {a:'time', b:'Time', c:'', l:'', q:'down', cc: 'black' }, {a:'pers', b:'Pax', c:'', l:'15', q:'down', cc: 'black' }, {a:'booking', b:'Booking', c:'', l:'', q:'down', cc: 'black' }, {alter: 'vcdate', a:'cdate', b:'createDate', c:'date', l:'', q:'down', cc: 'black' }, {a:'bkstatus', b:'Status', c:'', l:'40', q:'down', cc: 'black' },{a:'sudotype', b:'Type', c:'', l:'40', q:'down', cc: 'black' } ];
	$scope.tabletitleToday = [ {a:'time', b:'Time', c:'', l:'', q:'down', cc: 'black' }, {a:'fullname', b:'Name', c:'', l:'70', q:'down', cc: 'black' }, {a:'pers', b:'Pax', c:'', l:'', q:'down', cc: 'black' }, {a:'phone', b:'Mobile', c:'', l:'100', q:'down', cc: 'black' }, {a:'comment', b:'Request', c:'', l:'80', q:'down', cc: 'black' }, {a:'bkstatus', b:'Status', c:'', l:'', q:'down', cc: 'black' }, {a:'sudotype', b:'Type', c:'', l:'', q:'down', cc: 'black' }, {a:'booking', b:'Booking', c:'', l:'', q:'down', cc: 'black' }, {a:'mealtype', b:'Meal Type', c:'', l:'', q:'down', cc: 'black' }, {a:'state', b:'State', c:'', l:'', q:'down', cc: 'black' } ];
	$scope.tabletitleContent = [ { a:'booking', b:'Booking Confirmation', c:'' }, { a:'type', b:'Booking Type', c:'' }, { a:'product', b:'Product', c:'' }, { a:'bkstatus', b:'Status', c:'' }, { a:'canceldate', b:'Cancel date', c:'' }, { a:'date', b:'Reservation Date', c:'date' }, { a:'time', b:'Reservation Time', c:'' }, { a:'pers', b:'Number of Persons', c:'' }, { a:'fullname', b:'Name', c:'' }, { a:'email', b:'Email', c:'' }, { a:'phone', b:'Mobile', c:'' }, { a:'createdate', b:'Create Date', c:'date' }, { a:'comment', b:'Request', c:'' }, { a:'fullnotes', b:'Notes', c:'' }, { a:'wheelwin', b:'Win', c:'' }, {a: 'event', b: 'Special occasion', c:''}, { a:'tracking', b:'Tracking', c:'' }, { a:'booker', b:'Staff', c:'' }, { a:'hotelguest', b:'Hotel Guest', c:'' }, { a:'company', b:'Company', c:'' }, { a:'state', b:'State', c:'' }, { a:'repeat', b:'Repeat Guest', c:'' }, { a:'duration', b:'Duration (mm)', c:'' }, { a:'restCode', b:'restCode', c:'' }, { a:'tablename', b:'Table', c:'' }, { a:'browser', b:'Browser', c:'' }, { a:'language', b:'Language', c:'' }, { a:'optin', b:'optin', c:'' }, { a:'review_status', b:'Review', c:'' }  ];
        //$scope.tabletitleContent = [ { a:'Booking Confirmation', b:'booking', c:'' }, { a:'Booking Type', b:'type', c:'' }, { a:'Status', b:'bkstatus', c:'' }, { a:'Cancel date', b:'canceldate', c:'' }, { a:'Reservation Date', b:'date', c:'date' }, { a:'Reservation Time', b:'time', c:'' }, { a:'Number of Persons', b:'pers', c:'' }, { a:'Name', b:'fullname', c:'' }, { a:'Email', b:'email', c:'' }, { a:'Mobile', b:'phone', c:'' }, { a:'Create Date', b:'createdate', c:'date' }, { a:'Request', b:'comment', c:'' }, { a:'Notes', b:'fullnotes', c:'' }, { a:'Win', b:'wheelwin', c:'' }, {a: 'event', b: 'Special occasion', c:''}, { a:'Tracking', b:'tracking', c:'' }, { a:'Staff', b:'booker', c:'' }, { a:'Hotel Guest', b:'hotelguest', c:'' }, { a:'Company', b:'company', c:'' }, { a:'State', b:'state', c:'' }, { a:'Duration (mm)', b:'duration', c:'' }, { a:'restCode', b:'restCode', c:'' }, { a:'Table', b:'tablename', c:'' }, { a:'Browser', b:'browser', c:'' }, { a:'Language', b:'language', c:'' }, { a:'optin', b:'optin', c:'' }, { a:'Review', b:'review_status', c:'' } ];
	$scope.bkfield = [ { a:'restaurant', b:'Restaurant', c: false }, { a:'booking', b:'Confirmation', c: false }, { a:'type', b:'Type', c: false }, { a:'bkstatus', b:'Status', c: false }, { a:'date', b:'BDate*', c: false }, { a:'time', b:'Time', c: false }, { a:'pers', b:'Pax', c: false }, { a:'fullname', b:'FullName', c: false }, { a:'email', b:'Email', c: false }, { a:'phone', b:'Mobile', c: false }, { a:'comment', b:'Request', c: false }, { a:'tablename', b:'Table', c: false }, { a:'tracking', b:'Tracking', c: false }, { a:'createdate', b:'CDate*', c: false }, { a:'canceldate', b:'ADate*', c: false }, { a:'wheelwin', b:'Win', c: false }, {a: 'event', b: 'Special occasion', c: false }, { a:'booker', b:'Staff', c: false }, { a:'hotelguest', b:'Hotelguest', c: false }, { a:'company', b:'Company', c: false }, { a:'state', b:'State', c: false }, { a:'duration', b:'Duration (mm)', c: false }, { a:'restCode', b:'restCode', c: false }, { a:'browser', b:'Browser', c: false }, { a:'language', b:'Language', c: false }, { a:'optin', b:'optin', c: false }, { a:'review_status', b:'Review', c: false } ];

	if($scope.multProd !== '') { // 'type' field must be the last one
		$scope.bkfield.splice(4, 0, { a:'product', b:'Product', c:false });
		$scope.tabletitleStandard.splice(8, 0, { a:'product', b:'Product', c:'', l:'45', q:'down', cc: 'black' });
		$scope.tabletitleToday.splice(8, 0, { a:'product', b:'Product', c:'', l:'45', q:'down', cc: 'black' });
		}
		
	if(isMultiple === 1) {
		$scope.tabletitleStandard.splice(1, 0, {a:'title', b:'restaurant', c:'', l:'70', q:'down', cc: 'black' });
		$scope.tabletitleToday.splice(0, 0, {a:'title', b:'restaurant', c:'', l:'70', q:'down', cc: 'black' });
		$scope.tabletitleContent.splice(1, 0, {a:'title', b:'Restaurant', c:'', l:'70', q:'down', cc: 'black' });
        }

	$scope.tabletitle = $scope.tabletitleStandard;
	$scope.bckups = $scope.tabletitleStandard.slice(0);
	$scope.bckupt = $scope.tabletitleToday.slice(0);	

	$scope.searchDictionaryAr = ['today', 'lunch', 'dinner', 'week', 'lastweek', 'month', 'lastmonth', 'future', 'past', 'spin', 'unspin', 'cancel', 'uncancel', 'noshow']
	$scope.extratitle = [ {'a':'today', 'b':'Today' }, {'a':'lunch', 'b':'---> Lunch' }, {'a':'dinner', 'b':'---> Dinner' }, {'a':'week', 'b':'Current Week' }, {'a':'lastweek', 'b':'Last Week' }, {'a':'month', 'b':'Current Month' }, {'a':'lastmonth', 'b':'Last Month' }, {'a':'future', 'b':'Future' }, {'a':'past', 'b':'Past' }, {'a':'divider', 'b':'divider' }, {'a':'website', 'b':'Website' }, {'a':'facebook', 'b':'Facebook' }, {'a':'callcenter', 'b':'Call Center' }, {'a':'walkin', 'b':'Walkin' }, {'a':'divider', 'b':'divider' }, {'a':'spin', 'b':'Used' }, {'a':'unspin', 'b':'Not Used' }, {'a':'cancel', 'b':'Cancelled' }, {'a':'uncancel', 'b':'Not Cancelled' }, {'a':'noshow', 'b':'No Show' }, {'a':'all', 'b':'All' }, {'a':'divider', 'b':'divider' }  ];
	$scope.extradictionary = (function() { var tmp=[]; for(var i = 0; i < $scope.extratitle.length; i++) tmp.push($scope.extratitle[i].a); return tmp; })();
	$scope.extratitlesegment = [{'a':'reserve', 'b':' by reservation date' }, {'a':'creation', 'b':' by creation date' } ];
	$scope.stateAr = bookService.validState();

    $scope.selectedItem = null;
	
	$scope.initorder = function() {
		$scope.tabletitleStandard = $scope.bckups;
		$scope.tabletitleToday = $scope.bckupt;
		$scope.tabletitle = $scope.tabletitleStandard;
		$scope.predicate = "vdate";
		$scope.reverse = true;
		};
		
	$scope.reorder = function(item, alter) {
		alter = alter || "";
		if (alter !== "")  item = alter;
		$scope.reverse = ($scope.predicate === item) ? !$scope.reverse : false;
		$scope.predicate = item;
	};
				
	$scope.showtest = function(y) {
		if(y.c === true || $scope.selectedItem === null) 
			return true;
		if(typeof $scope.selectedItem[y.a] === 'undefined')
			return false;
		return ($scope.selectedItem[y.a] !== '' && $scope.selectedItem[y.a] !== '0' && $scope.selectedItem[y.a] !== null );
	};

	$scope.checkdate = function(rdate, status, wheel) {
		if (status === 'cancel' || status === 'noshow' || wheel !== '' || status === 'pending_payment')
			return false;
		
		return (getDiffDay(rdate, 0, 0) > -1);
		};

	$scope.validatebkg = function(x) {
		bookService.validateBooking(x.restaurant, x.booking).then(function(response) {
			var content = response.data;
			if(response.status === 1) { 
				x.validate = 1;
				}
			});
		};
		
	$scope.readCodeBooking = function() {
		bookService.readCodeBooking($scope.restaurant, $scope.email).then(function(response) {
			var content = response.data;
			if(response.status === 1 && typeof content === 'string' && content !== '') { 
				try {
					var oo = JSON.parse(content.replace(/’/g, "\""));
					$scope.codebooking = oo.data;
				} catch(e) { console.error("JSON-PROFILE", e.message); }
				}
			});
		};
                

	$scope.resetproperty = function() {
			var i, data = $scope.names;
			for(i = 0; i < data.length; i++) {
				$scope.setproperty(data[i]);
				}
				
		item = localStorage.extraselect;
		if(item && typeof item === "string" && item.length > 0 && $scope.extradictionary.indexOf(item) >= 0)
			$scope.extraselect(item);
        };
                

	
	$scope.setproperty = function(oo, nn) {
		var i, j, k, item, tt, cmonth, lmonthy, originAr = ['c', 'r'];

		cmonth = parseInt(currentmonth);
		lmonthy = (cmonth > 1) ? currentyear + '-' + (((cmonth-1) < 10) ? "0":"") + (cmonth-1) : (parseInt(currentyear) - 1) + '-12';
		oo.selectflg = 0;
		if($scope.thirdpartyflg === false)
			if (typeof oo.tracking === "string" && oo.tracking.length > 0 && oo.tracking.toLowerCase().indexOf("remote") > -1)
 				$scope.thirdpartyflg = true;
 				
		k = 1;
		for(i = 0; i < $scope.searchDictionaryAr.length; i++)
			for(j = 0; j < originAr.length; j++, k <<= 1) {
				item = $scope.searchDictionaryAr[i];
				dd = (originAr[j] === 'c') ? oo.cdate : oo.date;
				tt = (originAr[j] === 'c') ? oo.vcdate : oo.vdate;
				switch(item) {
					case 'today':
						if(dd == currentday) {
							oo.selectflg |= k;
							}
						break;
					case 'lunch':
						if(dd == currentday && oo.mealtype == 'lunch') 
							oo.selectflg |= k;
						break;
					case 'dinner':
						if(dd == currentday && oo.mealtype == 'dinner') 
							oo.selectflg |= k;
						break;
					case 'week':
						if(aday.toWeek(dd) >= 0)
							oo.selectflg |= k;
						break;
					case 'lastweek':
						if(aday.tolWeek(dd) >= 0)
							oo.selectflg |= k;
						break;
					case 'month':
						if(parseInt(dd.substring(5, 7)) == cmonth)
							oo.selectflg |= k;
						break;
					case 'lastmonth':
						if(dd.substring(0, 7) == lmonthy)
							oo.selectflg |= k;
						break;
					case 'future':
						if(tt > todayjs.getTime())
							oo.selectflg |= k;
						break;
					case 'past':
						if(tt < todayjs.getTime())
							oo.selectflg |= k;
						break;
					case 'spin':
						if (oo.wheelwin != "")
							oo.selectflg |= k;
						break;
					case 'unspin':
						if (oo.wheelwin == "" && oo.wheelwin != "cancel")
							oo.selectflg |= k;
						break;
					case 'cancel':
						if(oo.bkstatus === "cancel") 
							oo.selectflg |= k;
						break;
					case 'uncancel':
						if(oo.bkstatus !== "cancel")
							oo.selectflg |= k;
						break;
					case 'noshow':
						if (oo.state == "no show")
							oo.selectflg |= k;
						break;
					}
				}
		};
		
	$scope.readBooking = function(restaurant, email, multiple) {
		
		var reference = restaurant, mode = '';
		
		if(multiple) {
			reference = email;
			mode = 'email';
			}
			
		bookService.readBooking(reference, mode).then(function(response) {
			var i, data, oo, flg = $scope.thirdpartyflg, truncateAr = [];
			var today = new Date();
			var yesterday = new Date(today.setDate(today.getDate() - 1));
			var yesterdayTime = new Date(yesterday.getFullYear(), yesterday.getMonth(), yesterday.getDate(), 0, 0, 0).getTime();
	
			$scope.thirdpartyflg = false;	
			$scope.burnendlinkflg = false;
			$scope.mnufrmsubmited =false;
			$scope.names = data = response;
			for(i = 0; i < data.length; i++) {

				oo = data[i];
				oo.sudotype = oo.type;
				
				if(oo.type === 'thirdparty')
					oo.sudotype = oo.booker;
				if(oo.tracking.length > 0 && oo.tracking.search("IMASIA") > -1) {
					if(oo.tracking.search("IMASIA|facebook") > -1) oo.sudotype = "imasiafb";
					else if(oo.tracking.search("IMASIAEDM") > -1) oo.sudotype = "imasiaedm";
					else oo.sudotype = "imasia";
					}
					
				if(oo.bkstatus === "expired" && oo.vcdate < yesterdayTime) { // change to datetime if you want to filter booking date vs create date
					truncateAr.push(i);
					continue;
					}
				oo.chckflgcus = $scope.checkdate(oo.date, oo.bkstatus, oo.wheelwin, oo.tracking);	// cus -> cancel, update, spin
				oo.chckflgnshow = $scope.checktimediff(oo.date, oo.time, oo.bkstatus, oo.wheelwin, oo.tracking);
                                if(oo.chckflgnshow) { oo.refund_amount =oo.amount; }
				oo.chckflgspin = $scope.checkspin(oo.tracking);
				oo.chckflgrfnd = $scope.checkrefunddate(oo.date, oo.time,oo.bkstatus, oo.isShowDetails, oo.deposit,oo.depstatus);
				if(oo.chckflgrfnd) { oo.res_refund_amount =oo.amount; }

                          
				if(oo.product === null) oo.product = '';
				oo.chckflgsubmit = $scope.checkfrmsubmit(oo.date, oo.bkstatus,oo.tracking,oo.product,oo.pers,oo.options.chck_form_status);
				if(oo.product == 'Chef table' || (oo.product.toLowerCase() == 'counter seats' && oo.pers > 5)){
					oo.mnufrmsubmited = false;
					if(oo.options.chck_form_status)  oo.mnufrmsubmited = true;
					oo.burnendlinkflg = true;
					}
                                oo.chckflgpaystatus = $scope.checkpaystatus(oo.date, oo.bkstatus,oo.tracking);
                               
				if(oo.bkstatus === "") {
					if(oo.wheelwin !== "") oo.bkstatus = "spin";
					if(oo.state === "no show") oo.bkstatus = "noshow";
					}
				$scope.setproperty(oo);
				}

			if(flg !== $scope.thirdpartyflg) {
				if(flg === false)
					$scope.extratitle.splice(13, 0, {'a':'remote', 'b':'Thirdparty' });
				else { 
					$scope.extratitle.splice(13, 1);	
					console.log('Resetting thirdpary');
					}
				}

			if(truncateAr.length > 0) 
				truncateAr.reverse().map(function(i) { $scope.names.splice(i, 1); });

					
			$scope.orgnames = $scope.names.slice(0);
			if(typeof localStorage.extraselect === "string" && $scope.extradictionary.indexOf(localStorage.extraselect) >= 0)
				$scope.currentSelection = localStorage.extraselect;
			else $scope.currentSelection = "all";
			$scope.extraselect($scope.currentSelection);
			$scope.paginator.setItemCount($scope.names.length);
			   
			$scope.initorder();
			$scope.readCodeBooking();
		
			bookService.readmodifBooking(reference, mode).then(function(response) { 
				var tmpindex = [];
			
				data = response;
				$scope.modifnames = response; 
				for(i = 0; i < data.length; i++) {
					data[i].rdate = data[i].rdate.jsdate().getDateFormat('-');
					tmpindex.push(data[i].booking);
					}
				for(i = 0; i < $scope.names.length; i++) {
					$scope.names[i].modified = tmpindex.indexOf($scope.names[i].booking);
					if($scope.names[i].modified >= 0) {
						moo = data[$scope.names[i].modified];
						$scope.names[i].modifiedinfo = "booking date: " + moo.rdate + ", booking time: " + moo.rtime + ", cover: " + moo.cover;
						}
					}
				});
			});
     	};       

	$scope.readBooking($scope.restaurant, $scope.email, (isMultiple === 1)); 
	
	$scope.extraselect = function(item) {
		var index, limit, data, mask;
		
		switch(item) {				
			case 'all':
				$scope.names = $scope.orgnames.slice(0);
				$scope.currentSelection = item;
				$scope.formatDisplay = "standard";
				$scope.tabletitle = $scope.tabletitleStandard;
				$('.custom').removeClass('glyphicon glyphicon-ok');
				$('#' + item).addClass('glyphicon glyphicon-ok');
				localStorage.extraselect = item;
				return;
			
			case 'oneresto':
				if($scope.oneresto === 0)
					$scope.readBooking($scope.restaurant, $scope.email, false);
				else $scope.readBooking($scope.restaurant, $scope.email, (isMultiple === 1));
				
				$scope.oneresto ^= 1;
				$scope.typeselection = 1;
				$('.glyphr').toggleClass('glyphicon glyphicon-ok');
				$('.glyphc').toggleClass('glyphicon glyphicon-ok');
				return;

			case 'reserve' :
				$scope.typeselection = 1;
				$('.glyphr').toggleClass('glyphicon glyphicon-ok');
				$('.glyphc').toggleClass('glyphicon glyphicon-ok');
				item = localStorage.extraselect;
				if(['week', 'lastweek', 'month', 'lastmonth'].indexOf(item) < 0)
					return;
				break;
			
			case 'create' :
				$scope.typeselection = 2;
				$('.glyphr').toggleClass('glyphicon glyphicon-ok');
				$('.glyphc').toggleClass('glyphicon glyphicon-ok');
				item = localStorage.extraselect;
				if(['week', 'lastweek', 'month', 'lastmonth'].indexOf(item) < 0)
					return;
				break;
			
			default:
				break;
				}

			
		$scope.current = $scope.names.slice(0);
		$scope.names = $scope.orgnames.slice(0);
		$scope.paginator.setPage(0);
		$scope.tmpArr = [];

		data = $scope.names;
		limit = data.length;
		localStorage.extraselect = item;
		
		switch(item) {
								
			case 'callcenter':
			case 'facebook':
			case 'website':
			case 'walkin':
			case 'remote':
				$scope.currentSelection = item;
				for (i = 0; i < limit; i++)
					if (typeof data[i].tracking === "string" && data[i].tracking.length > 0 && data[i].tracking.toLowerCase().indexOf(item) > -1)
						$scope.tmpArr.push(data[i]);

				$scope.completeselection(item);
				break;
			
			default:
				index = $scope.searchDictionaryAr.indexOf(item) * 2;
				if(index < 0) {
					alert("invalid selection " + item);
					return;
					}
				$scope.currentSelection = item;
				if($scope.typeselection == 1)
					index++;			
				mask = 1 << index;
				for (i = 0; i < limit; i++)
					if((data[i].selectflg & mask) > 0) {
						$scope.tmpArr.push($scope.names[i]);
					}

				$scope.completeselection(item);
				break;
				}
			};

	$scope.completeselection = function(item) {
		if ($scope.tmpArr.length > 0) {
			$scope.names = $scope.tmpArr.slice(0);
			$('.custom').removeClass('glyphicon glyphicon-ok');
			$('#' + item).addClass('glyphicon glyphicon-ok');
			$scope.currentSelection = item;
			if (item !== "today" && item !== "lunch" && item !== "dinner") {
				$scope.formatDisplay = "standard";
				$scope.tabletitle = $scope.tabletitleStandard;
				}
			else {
				$scope.formatDisplay = "today";
				$scope.tabletitle = $scope.tabletitleToday;
				$scope.reorder("time", "");
				}
			}
		else {
			$scope.names = $scope.current.slice(0);
			alert('Search ' + item + ': no booking selected (stay with previous selection');
			}
		};
					
	$scope.view = function(oo, previous) {
		if(typeof previous !== 'string') previous = "";
		$scope.selectedItem = oo; // take the orgnames as names get modified with the shortcuts
                
		//$scope.listingVisitFlag = false;
		$scope.reset('contentVisitFlag');
                //get Deposit cancel policy
    
        if(oo.booking_deposit_id && oo.booking_deposit_id !== ""){
	        $scope.cancelPolicy(oo.restaurant, oo['payment_method'],oo['amount'],oo['product'],oo['date'],oo['time'],oo['pers']);
            }
            //get booking log event history
            if($scope.isShowLog){
                $scope.getLogEvent(oo);
            }
            $scope.previous = previous;
		};
	
	$scope.previous = "";	
	$scope.backlisting = function() {
		if($scope.previous === "")
			return $scope.reset('listingVisitFlag');
		
		$scope.reset($scope.previous);
		$scope.previous = "";
		};

	$scope.mitsubaValidation = function() {
		$scope.reset('mitsubaFlag');
		};
		
	$scope.reset = function(tag) {
		$scope.contentVisitFlag = false
		$scope.listingVisitFlag = false;
		$scope.extractFlag = false;
		$scope.mitsubaFlag = false
		$scope[tag] = true;
		}
				
	$scope.spin = function(oo) {
		state = oo.wheelwin;
		if (state != '') {
			if (state == 'cancel')
				alert("The reservation has been previously canceled");
			else alert("The Wheel has already been spun! (" + state + ")")
				return false;
			}

		winp = window.open('../wheel/rotation/rotation.php?restaurant=' + oo.restaurant + '&confirmation=' + oo.booking + '&restCode=' + oo.restCode + '&guestname=' + oo.first, 'weeloy', 'toolbar=no,width=1050,height=750,menubar=no,scrollbars=yes,resizable=no,alwaysRaised=yes');
		winp.moveTo(15, 25);
		winp.resizeTo(1050, 825);
		winp.focus();
		return false;
		};
	
	$scope.savechgstatebkg = function(x) {
		return bookService.savechgstateservice(x.restaurant, x.booking, x.state, token).then(function(response){ console.log(response); });
		}
	
	$scope.checkspin = function(tracking, restCode) {
		if(typeof tracking !== "string" || tracking === "") return true;
		tracking = tracking.toLowerCase();
		if(typeof resCode !== "string" || resCode.length < 4 || resCode === "0000")
			return false;
			
		return (['remote', 'tms', 'callcenter', 'facebook'].indexOf(tracking)  < 0) ? true : false;
		};
		
	$scope.checktimediff = function(rdate, rtime, status, wheel, tracking) {
		var diffDays, timeParts;
		if (status === 'cancel' || status === 'noshow' || wheel !== '')
			return false;
 		//if(typeof tracking === "string" && tracking !== "" && tracking.search("remote")  > -1) return false;

		timeParts = rtime.split(':');
		var uu = rdate.split('-');
		var addtwoDays = getDiffDay(rdate, 0, 0);
              
        diffDays= (new Date().getTime()-new Date(parseInt(uu[0]), parseInt(uu[1])-1, parseInt(uu[2]), timeParts[0], timeParts[1]).getTime()) / (24 * 3600 * 1000);
		return (diffDays >= 0 && addtwoDays>-3);
	}
        
        
        $scope.getLogEvent = function(x) {
        	// RICHARD -> 
        	// it does not get the actions relative to that booking (much more), 
        	// should be limited to xx actions (no more than 30), 
        	// must be activated per restaurant
        	// should be actionnable on a click 
        	// as is, it blocks the backoffice
        	
        	//return ; 
                
            
            return bookService.logbookingevent(x.booking).then(function(response){ 
                if(typeof response.data!=='undefined' &&  response.data!=="" ){
                      console.log(JSON.stringify(response));
                      $scope.eventlog = response.data;
                	}              
                });
        }
                       



	$scope.checkdate = function(rdate, status, wheel, tracking) {
		if (status === 'cancel' || status === 'noshow' || wheel !== '' || status === 'pending_payment') return false;
		if(typeof tracking === "string" && tracking !== "" && tracking.search("remote")  > -1) return false;

		return (getDiffDay(rdate, 0, 0) > -1);
            };
            
       $scope.checkfrmsubmit = function(rdate, status,tracking,product,pers,frmsubmit) {
      		if (status === 'cancel' || status === 'noshow' || product === '' || product.toLowerCase() === 'bar seats' || frmsubmit || (product.toLowerCase() === 'counter seats' && pers < 6) || status === 'pending_payment') return false;

		if(typeof tracking === "string" && tracking !== "" && tracking.search("remote")  > -1) return false;
		return (getDiffDay(rdate, 0, 0) >= 0 && getDiffDay(rdate, 0, 0)  < 3);
            };
       $scope.checkpaystatus = function(rdate, status,tracking) {
      		if (status === 'cancel' || status === 'noshow' || status === '' || status === 'spin') return false;
		if(typeof tracking === "string" && tracking !== "" && tracking.search("remote")  > -1) return false;
		return (getDiffDay(rdate, 0, 0) > -1);
            };
        
        //refund flage
        $scope.checkrefunddate = function(rdate, rtime,status,isShow,ispayment,depstatus) {
		if (status !== 'cancel' || ispayment !== 'paid'  || isShow === 0 || depstatus === 'REFUNDED' ) return false;
                 timeParts = rtime.split(':');
		 var uu = rdate.split('-');
                 var diffDays= (new Date().getTime()-new Date(parseInt(uu[0]), parseInt(uu[1])-1, parseInt(uu[2]), timeParts[0], timeParts[1]).getTime()) / (1000 * 60 * 60 * 24 * 14);
   
                 return (diffDays < 1);
		
        };

	$scope.cancelbooking = function(x) {
		if (confirm("Please confirm the cancellation of reservation " + x.booking + " ?") == false)
			return;

		return bookService.cancelbackoffice(x.restaurant, x.booking, x.email).then(function(response){
			x.wheelwin = x.bkstatus = 'cancel';
			x.chckflgcus = $scope.checkdate(x.date, x.bkstatus, x.wheelwin, x.tracking);	// cus -> cancel, update, spin
			x.chckflgnshow = $scope.checktimediff(x.date, x.time, x.bkstatus, x.wheelwin, x.tracking);
			$scope.resetproperty();
			alert('Reservation ' + x.booking + ' has been cancel ');
			});
		};
		
		$scope.cancelPolicy = function(restaurant, payment_method,amount,product,date,time,pax){
           
                    if(restaurant ==="SG_SG_R_TheFunKitchen" ||restaurant ==="SG_SG_R_Bacchanalia" ){
                        return bookService.bacchanaliacancelpolicy(restaurant,date,time,pax).then(function(response) {
                           
                                var data = response.data;
                               
                            if (typeof data !== 'undefined' ) {
                               $scope.policybaccahanalia = data.priceDetails.message;  
                            }
                        });
                 
                        
                    }else{
			return bookService.getCancelPolicy(restaurant, payment_method, amount,product).then(function(response) {
			var data =response.data;
			var temArr =[];
		
			if(typeof response.data!=='undefined' &&  response.data!=="" ){
			 	$scope.policy = response.data.range;
			  	$scope.freerange = response.data.lastRange;		
				}

                    }); 
                }
           
	 };  
	      
	
                
	$scope.cancelpaymentbooking = function(x) {
	
		if(parseInt(x.amount) >= parseInt(x.refund_amount)) { 
			if (confirm("by cancelling the reservation " + x.booking + ", you charge SGD "+ x.refund_amount + " to the client credit card.") == false)
				return;	   

			return bookService.cancelrefundbackoffice(x.booking_deposit_id, x.restaurant, x.booking, x.email, x.refund_amount, x.payment_method).then(function(response){

				x.wheelwin = x.bkstatus = 'cancel';
				if(x.payment_method === 'carddetails'){
					x.depstatus = 'PAID';
					x.amount = x.amount;
				}else{
					x.depstatus = 'REFUNDED';
					x.refund_amount = x.amount;
				}
                                if(response.status===1){
                                    x.chckflgcus = $scope.checkdate(x.date, x.bkstatus, x.wheelwin, x.tracking);	// cus -> cancel, update, spin
                                    x.chckflgnshow = $scope.checktimediff(x.date, x.time, x.bkstatus, x.wheelwin, x.tracking);
                                           alert('Reservation ' + x.booking + ' has been cancel ');
                                }else{
                                    console.log(response.data);
                                    alert('Payment creation Failed : '+ response.data.message);
                                }
				});
			}
                        else{
			alert("IMPT:Refund amount shall not exceed the initial deposit payment. ");
		    return;
			}
		};

        $scope.noshowpaymentbooking = function(x){
		if (confirm("Please confirm the status change of reservation " + x.booking + " ?"+ " you charge SGD "+ x.refund_amount + " to the client credit card.") == false)
			return;
			
		return bookService.bookingpaymentnoshow(x.restaurant, x.booking, x.email, x.booking_deposit_id, x.refund_amount, x.payment_method).then(function(response){
                    if(response.status===1){
                            x.wheelwin = x.bkstatus = 'noshow';
                            x.chckflgcus = $scope.checkdate(x.date, x.bkstatus, x.wheelwin, x.tracking);	// cus -> cancel, update, spin
                            x.chckflgnshow = $scope.checktimediff(x.date, x.time, x.bkstatus, x.wheelwin, x.tracking);

                            alert('Reservation ' + x.booking + ' has been changed to noshow ');
                    }else{
                        console.log(response.data);
                        alert('Payment creation Failed :'+ response.data.message);
                    }
			});
		};
                
        $scope.paymentrefund = function(x){
            if(parseInt(x.amount) >= parseInt(x.res_refund_amount)) {
                if (confirm("The refund amount : SGD "+ x.res_refund_amount ) == false)
			return; 
            return bookService.bookingrefund(x.restaurant, x.booking, x.email, x.booking_deposit_id, x.res_refund_amount, x.payment_method).then(function(response){
     
                    if(response.data.status===1){
                        x.depstatus = 'REFUNDED';
                        x.refund_amount = x.res_refund_amount;
                        x.chckflgrfnd = $scope.checkrefunddate(x.date, x.time,x.bkstatus, x.isShowDetails, x.deposit,x.depstatus);
                       alert('The transaction has been completed.' );
                    }else{
                     
                        alert('refund Failed :'+ response.data.error);
                    }
			});
                        
                }else{
			alert("IMPT:Refund amount shall not exceed the initial deposit payment. ");
                        return;
                    }
            
        };
        
        $scope.menuresend = function(x){
            if (confirm("Are you sure You want to resend 'Menus & Preferences' link ") == false)
			return;
            return bookService.menureminderlink(x.restaurant, x.booking, x.email).then(function(response){
                alert("Menu & preferences pdflink has been sent");
                
            });
        }
        $scope.updatemnufrmtatus = function(x){
                return bookService.setmenuformstatus(x.restaurant, x.booking, x.email,x.mnufrmsubmited).then(function(response){
                    alert("Status updated");
                     x.chckflgsubmit = $scope.checkfrmsubmit(x.date, x.bkstatus,x.tracking,x.product,x.pers,x.options.chck_form_status);
               
        });


        }
        $scope.resendpaymentEmail = function(x){
            return bookService.resendEmail(x.restaurant, x.booking,$scope.email).then(function(response){
                    alert("Email has been sent");
                });
          
        }
        $scope.updatestatus = function(x){
             if (confirm("Are you sure You want to  waive the credit card details") == false)
			return;
            return bookService.updateStatus(x.restaurant,x.booking,$scope.email).then(function(response){
                   
                     x.bkstatus = "";
                     x.chckflgpaystatus = $scope.checkpaystatus(x.date, x.bkstatus,x.tracking);
                     x.chckflgcus = $scope.checkdate(x.date, x.bkstatus, x.wheelwin, x.tracking);	// cus -> cancel, update, spin
                     x.chckflgnshow = $scope.checktimediff(x.date, x.time, x.bkstatus, x.wheelwin, x.tracking);
                      alert("Credit Card Details Waived");
                
                });
          
        }
        
        
        $scope.resendpaymentsms = function(x){
           
            return bookService.resendsms(x.restaurant, x.phone,'weeloy','payment_link',x.booking,x.email).then(function(response){
                    alert("Sms has been sent");
                });
                    
            
        }
        
        
	
	$scope.noshowbooking = function(x){
		if (confirm("Please confirm the status change of reservation " + x.booking + " ?") == false)
			return;
			
		return bookService.bookingnoshow(x.restaurant, x.booking, x.email).then(function(response){
			x.wheelwin = x.bkstatus = 'noshow';
			x.chckflgcus = $scope.checkdate(x.date, x.bkstatus, x.wheelwin, x.tracking);	// cus -> cancel, update, spin
			x.chckflgnshow = $scope.checktimediff(x.date, x.time, x.bkstatus, x.wheelwin, x.tracking);
			
			$scope.resetproperty();
			alert('Reservation ' + x.booking + ' has been changed to noshow ');
			});
		};

	$scope.filterdate = function() {
		$scope.aSelDay = $scope.mydata.selecteddate;
		$scope.searchText = $scope.mydata.selecteddate.getDateFormat('-');
		$scope.paginator.setPage(0);
		}

	mydata.setInit($scope.cdate, "09:00", $scope.filterdate, 4, new Date(currentyear-1, 6, 1), new Date(currentyear+1, 11, 31));

	$scope.setbooking = function() {
		var objstr, morestr, field, adate, x, booking, email, sep, mod = {}, oo = {}, tmp, notestext, notescode;

		x = $scope.selectedItem;
		adate = mydata.getDate('-', '');
		booking = x.booking;
		restaurant = x.restaurant;
		email = x.email;
	
		mod.last = mydata.lastname;
		mod.first = mydata.firstname;
		mod.phone = mydata.mobile;
		mod.pers = parseInt(mydata.npers);
		mod.time = mydata.ntimeslot;
		mod.event = mydata.nevent;
		mod.comment = mydata.comment;		
		mod.date = mydata.getDate('-', 'reverse');		// date object of selecteddate
		notestext = mydata.notestext;		
		notescode = sep = "";
		for(i = 0; i < mydata.notescode.length; i++)
			if(mydata.notescode[i].value) {
				notescode += sep + mydata.notescode[i].label;
				sep = ",";
				}

		notestext = (typeof notestext === "string" && notestext !== "") ? notestext.replace(/'|"/g, '`') : ""; // ’`
		notescode = (typeof notescode === "string" && notescode !== "") ? notescode.replace(/'|"/g, '`') : ""; // ’`
		
		if(typeof mod.date !== "string" || mod.date.length  < 10 || typeof mod.time !== "string" || mod.time.length < 5 || mod.pers < 0 || mod.pers > 99) {
			console.log('SETBOOKING', mod.date, mod.date.length, mod.time, mod.time.length, mod.pers);
			alert('Value are not set. Cannot change current booking with required modifications');
			return;
			}

        for(field in mod) {
		    if(mod.hasOwnProperty(field) && typeof mod[field] !== 'function' && field !== '$$hashKey' && field !== 'event') {
		    	tmp = mod[field];
		    	if(tmp !== x[field]) 
		    		oo[field] = (typeof tmp === "string" && tmp !== "") ? tmp.replace(/'|"/g, '`') : tmp; // ’`
		    	}
          	}

		mod.more = JSON.stringify( { event: mod.event, notestext: notestext, notescode: notescode } );
		objstr = JSON.stringify( mod );
		
		// after JSON, for update of selectedItem
		mod.fullname = mod.first + ' ' + mod.last; 
		mod.vdate = mod.date.jsdate().getTime();
		mod.ddate = mod.date.jsdate().getDateFormat('-');
		
		if(x.date !== mod.date) {
			bookService.checkAvail(restaurant, adate, mod.time, mod.pers, booking).then(function(response) {
				if (response.data == 0) { 
					alert(response.errors + '. Unable to modify the reservation'); 
					return; 
					}

				bookService.modifullbooking(restaurant, booking, email, objstr).then(function(response){ 
					for(var field in mod) {
						if(mod.hasOwnProperty(field) && typeof mod[field] !== 'function' && field !== '$$hashKey') 
							x[field] = mod[field];
						}
					$scope.postmodif(response.status, response.errors, x, notestext, notescode); 
					});
				});
		} else {
			bookService.modifullbooking(restaurant, booking, email, objstr).then(function(response){ 
				for(var field in mod) {
					if(mod.hasOwnProperty(field) && typeof mod[field] !== 'function' && field !== '$$hashKey') 
						x[field] = mod[field];
					}
				$scope.postmodif(response.status, response.errors, x, notestext, notescode); 
				});
			}
		};

	$scope.modifymodal = function(x) {
		var oo, val, i, maxDate = new Date();
		maxDate.setTime(maxDate.getTime() + (60 * 24 * 3600 * 1000));	// remainingday  120 days
	
		mydata.setInit(x.date.jsdate(), x.time, null, 3, new Date(), maxDate);

		mydata.lastname = x.last;
		mydata.firstname = x.first;
		mydata.mobile = x.phone;
		mydata.npers = x.pers;
		mydata.name = x.confirmation;
		mydata.comment = x.comment;	
		mydata.nevent = x.event;
		mydata.contact = "";	
		$scope.selectedItem = x;

		oo = ($scope.codebooking && $scope.codebooking instanceof Array) ? $scope.codebooking : [];	
		val = (x.notescode && typeof x.notescode === "string" && x.notescode.length > 1) ? x.notescode.split(",") : [];
		mydata.notescode = []; 
		for(i = 0; i < oo.length; i++)
			mydata.notescode.push( { label: oo[i].label, value: (val.indexOf(oo[i].label) > -1) });
	
		mydata.notestext = (x.notestext && typeof x.notestext === "string") ? x.notestext : "";
		mydata.notesflag = 1;
		$scope.showModal('Modify the Reservation', 'Modify Booking', $scope.setbooking, "bkgModifBackoffice.html", mydata, null);	//"bookingModif.html"	
		};
							
	$scope.postmodif = function(status, msg, x, notestext, notescode) {
		if(status === 1)  {
			x.modified = 1;
			x.notestext = notestext;
			x.notescode = notescode;
			x.fullnotes = x.notestext + ((x.notestext !== "" && x.notescode !== "") ? "," : "") + x.notescode; 
			$scope.resetproperty();
			alert('Reservation ' + x.booking + ' has been modified.'); 
			}
		else alert('Reservation ' + x.booking + ' has NOT been modified (' + msg + ').' );
		};
				
	$scope.cancel = function () {
		modalInstance.dismiss(false);
		};
			
	$scope.getTotalPax = function() {
		var i, total;
		if (typeof $scope.filteredPeople === "undefined")
			return "";
					//if($scope.formatDisplay != 'today') return "";
		for (i = total = 0; i < $scope.filteredPeople.length; i++) {
			if ($scope.filteredPeople[i].bkstatus == "" && $scope.filteredPeople[i].pers != "99")
				total += parseInt($scope.filteredPeople[i].pers);
				}
		return total;
		};

	$scope.setallfield = function() {
		var i, data = $scope.bkfield;
		for (i = 0; i < data.length; i++)
			data[i].c = true;		
		};
		
	$scope.clearallfield = function() {
		var i, data = $scope.bkfield;
		for (i = 0; i < data.length; i++)
			data[i].c = false;		
		};
		
	$scope.performextraction = function() {
		var i, req, sep, data = $scope.bkfield;
		
		req = sep = "";
		
		if($scope.startBkg === -1 || $scope.startBkg === -1) {
			alert("Please specify a starting date/ending date");
			return;
			}
			
		for (i = 0; i < data.length; i++) {
			if(data[i].c) {
				req += sep + data[i].a;
				sep = "|";
				}
			}
		req = "restaurant=" + $scope.restaurant + "&email=" + $scope.email + "& token=" + token + "&starting=" + $scope.mydatastart.selecteddate.getDateFormatReverse() + "&ending=" + $scope.mydataend.selecteddate.getDateFormatReverse() + "&field=" + req;	
		var win = window.open("fullextract.php?"+ req, "Full Extraction", "toolbar=yes,location=no,directories=yes,menubar=yes,scrollbars=yes,width=500, height=800, left=100, top=25");
		};
						
	$scope.extractSelection = function() {

		var maxlimit = 1500; // ajax call might not support more data
		var todayflg = ($scope.currentSelection != 'today');
		var exportselect = $scope.filteredPeople;
		var data, sep, limit = exportselect.length;
		var i, j, tt, titleAr, contentAr, oo;
		var filename, cdate = new Date();
		var brswr = browserVersion();
		
		if (todayflg) {
			titleAr = ["restaurant", "date", "time", "pax", "product", "mealtype", "salutation", "firstname", "lastname", "request", "fullnotes", "phone", "state", "status", "email", "confirmation", "repeat", "restCode", "win", "type", "create date", "booker", "company", "optin", "tablename", "hotelguest", "tracking"];
			contentAr = ["restaurant", "date", "time", "pers", "product", "mealtype", "salutation", "first", "last", "comment", "fullnotes", "phone", "state", "bkstatus", "email", "booking", "repeat", "restCode", "wheelwin", "type", "createdate", "booker", "company", "optin", "tablename", "hotelguest", "tracking"];
		} else {
			titleAr = ["restaurant", "time", "firstname", "lastname", "product", "pax", "phone", "request", "fullnotes", "repeat", "status", "booker", "company", "optin", "tablename", "hotelguest", "restCode"];
			contentAr = ["restaurant", "time", "first", "last", "product", "pers", "phone", "comment", "fullnotes", "repeat", "bkstatus", "booker", "company", "optin", "tablename", "hotelguest", "restCode"];
			}
			
		if($scope.multProd === '' && (index = titleAr.indexOf("product")) > -1) {
			titleAr.splice(index, 1); 
			contentAr.splice(index, 1); 
			}
		if($scope.isMultiple === '' && (index = titleAr.indexOf("restaurant")) > -1) {
			titleAr.splice(index, 1); 
			contentAr.splice(index, 1); 
			}
					
		if (limit > maxlimit) limit = maxlimit;

		if(
		(brswr.name === "Chrome" && brswr.version > 45) ||
		(brswr.name === "FireFox" && brswr.version > 40)
		)	 {	
			data = titleAr.join(",");			
			for (i = 0; i < limit; i++) {
				data += "\n";			
				u = exportselect[i];
				for (j = 0; j < contentAr.length; j++) 
					data += $scope.extractfilter(u[contentAr[j]], 2) + ",";			
				}	
		
			filename = "extraction" + cdate.getDate() + cdate.getMonth() + cdate.getFullYear() + ".csv";   
			return downloaddata(filename, data);
		} else {
			data = [];
			for (i = 0; i < limit; i++) {
				u = exportselect[i];
				oo = {};
				contentAr.map(function(label, j, arr) { oo[titleAr[j]] = $scope.extractfilter(u[label], 1); } );
				data.push(oo);
				}
			console.log(data);
			tt = JSON.stringify({ booking: data });			
			$scope.content = tt;
			$('#content').val(tt);
			$('#extractForm').submit();
			}
				
		};
	
	$scope.extractfilter = function(s, flg) {
		if(typeof s !== "string" || s.length < 1)
			return s;
		
		var patt = (flg === 1) ? /[,\n\r\'\"]/g : /[\n\r\'\"]/g;
		return s.replace(patt, " ");
		};
				
	$scope.viewdispo = function() {

		var day = (typeof $scope.aSelDay === "Date") ? $scope.aSelDay : new Date($scope.aSelDay);
		var argday = day.getDateFormatReverse('-');
		
		bookService.getdispoDay($scope.restaurant, argday, $scope.product).then(function(response) {
			if (response.status != 1) return;
			var aa, bb, bgcolor;
			typeavail = (response.data.pax == "1") ? "pax" : "tables";
			availAr = response.data.avail.split(",");
			bookAr = response.data.booked.split(",");
			var win = window.open("", "Availability for " + day.toDateString(), "toolbar=yes,location=no,directories=yes,menubar=yes,scrollbars=yes,width=500, height=800, left=100, top=25");
			win.document.open();
			content = "";
			content += "<html><head><title>Weeloy System Dispo</title>";
			content += "<style>body { margin: 20 20 20 20 } table { font-family:helvetica;font-size:14px; } td { align:center; } .truncate { white-space: nowrap; overflow: hidden; text-overflow: ellipsis; }  tbody tr:nth-child(odd) { background: #eee; }</style></head>";
			content += "<body><center>";
			content += "<p><img src='" + imglogo + "' max-width='80' max-height='80' id='theLogo' name='theLogo'></p><br/>";
			content += "<p style='font-size:16px;padding-right:40px;'>Dispo for: <strong> " + day.toDateString(); + "</strong></p>";
			content += "<p></p>";
			content += "<table width='100%'  border='0'><head><tr style='font-weight:bold;font-size:14px'><td align='center'>time</td><td align='center'>current availability</td><td align='center'>current booked " + typeavail + "</td></tr><tr><td colspan='3'><hr/></td></tr>";
			limit = (availAr.length < 31) ? availAr.length : 30;
			for (i = 0; i < limit; i++) {
				aa = (availAr[i] <= 0) ? " " : availAr[i];
				bb = (bookAr[i] <= 0) ? " " : bookAr[i];
				content += "<tr><td>" + (9 + Math.floor(i / 2)) + ":" + ((i % 2) * 3) + "0" + "</td><td align='center'>" + aa + "</td><td align='center'>" + bb + "</td></tr>";
				}
			content += "<tr><td colspan='3'></td></tr></table></center></body></html>";
			win.document.write(content);
			win.document.close();
			win.focus();
			});
		};
			
	$scope.printing = function() {
		//var data = $("#print_content").html(); 
		var content, i, data = $scope.filteredPeople.slice(0);
		var docprint = window.open("", "Printing Reservation", "toolbar=yes,location=no,directories=yes,menubar=yes,scrollbars=yes,width=800, height=600, left=100, top=25");
		docprint.document.open();
		
		data.sort(function(a, b) { var c = a.vdate - b.vdate; return (c != 0) ? c : (a.vtime - b.vtime); });
		content = "";
		content += "<html><head><title>Weeloy System</title>";
		content += "<style>body { margin: 20 20 20 20 } table { font-family:helvetica;font-size:10px; } td { align:center; } .truncate { white-space: nowrap; overflow: hidden; text-overflow: ellipsis; } </style></head>";
		content += "<body onLoad='self.print()'><center>";
		content += "<p><img src='" + imglogo + "' max-width='80' max-height='80' id='theLogo' name='theLogo'></p><br/>";
		content += "<span style='font-size:12px;padding-right:40px;'>selected bookings: <strong> " + $scope.filteredPeople.length + "</strong></span>";
		content += "<span style='font-size:12px;padding-right:40px;'>current selection: <strong> " + $scope.currentSelection + "</strong></span>";
		content += "<span style='font-size:12px;padding-right:40px;'>total pax: <strong> " + $scope.getTotalPax() + "</strong></span>";
		content += "<br/><br />";
		content += "<table width='100%'><head><tr style='font-weight:bold;font-size:10px'><td>time</td><td>First Name</td><td>Last Name</td><td>Pax</td><td>Phone</td><td>Product</td><td>Request</td><td>Status</td><td>Table</td><td>Booker</td><td>Confirmation</td><td>Date</td><td>Repeat</td><td>Fullnotes</td></tr><tr><td colspan='13'><hr/></td></tr>";
		for (i = 0; i < data.length; i++) {
			content += "<tr><td>" + data[i].time + "</td><td>" + data[i].first.substr(0,10) + "</td><td>" + data[i].last.substr(0,10) + "</td><td>" + data[i].pers + "</td><td><div class='truncate' style='width:80px'>" + data[i].phone.substr(0,17) +
				"</div></td><td>" + data[i].product + "</td><td>" + data[i].comment.substr(0,25) + "</td><td>" + data[i].bkstatus + "</td><td>" + data[i].tablename + "</td><td>" + data[i].booker + "</td><td>" + data[i].booking + "</td><td>" + data[i].date + "</td><td>" + data[i].repeat + "</td><td>" + data[i].fullnotes + "</td></tr>";
			}
		//content += data;          
		content += "</table></center></body></html>";
		docprint.document.write(content);
		docprint.document.close();
		docprint.focus();
		};
}]);

function downloaddata(filename, content) {
        var link;
        filename = filename || 'export.csv';

        if (!content.match(/^data:text\/csv/i) || true) {
            content = 'data:text/csv;charset=utf-8,' + content;
        }
        data = encodeURI(content);

        link = document.createElement('a');
        link.setAttribute('href', data);
        link.setAttribute('download', filename);
	document.body.appendChild(link);
	link.click();
	document.body.removeChild(link);
}

function AdayRoutine(todayphp) {
	var dateAr, date, ctime, cweek, mweek, clweek, dd;
	
	dateAr = todayphp.split('-');
	date = new Date(dateAr[0], parseInt(dateAr[1]) - 1, dateAr[2], 0, 0, 0);
	ctime = date.getTime();
	dd = date.getDay();
	cweek = ctime - (dd * 24 * 60 * 60 * 1000);
	mweek = cweek + (7 * 24 * 60 * 60 * 1000);
	clweek = cweek - (7 * 24 * 60 * 60 * 1000);
	
	return  {
		dateAr: [],
		date: null,
		now: Date.now(), 
		ctime: ctime,
		cweek: cweek,
		mweek: mweek,
		clweek: clweek,
		tmp: 0,
		
		getDate: function(aDate, h, m, s, sep) {
			this.dateAr = aDate.split(sep);
			return new Date(this.dateAr[0], parseInt(this.dateAr[1]) - 1, this.dateAr[2], h, m, s);
			},
			
		toDays: function(aDate) {
			this.date = this.getDate(aDate, 0, 0, 0, '-');
			return ((this.date.getTime() - this.ctime) / (24 * 60 * 60 * 1000));
			},
	
		toWeek: function(aDate) {
			this.date = this.getDate(aDate, 0, 0, 0, '-');
			this.tmp = this.date.getTime();
			return (this.tmp >= this.cweek && this.tmp < this.mweek) ? 1 : -1;
			},
	
		tolWeek: function(aDate) {
			this.date = this.getDate(aDate, 0, 0, 0, '-');
			this.tmp = this.date.getTime();
			return (this.tmp >= this.clweek && this.tmp < this.cweek) ? 1 : -1;
			},
	
		dayofweek: function(aDate) {
			this.date = this.getDate(aDate, 0, 0, 0, '-');
			return this.date.getDay();
			},
	
		numberDay: function(aDate) {
			this.date = this.getDate(aDate, 23, 59, 59, '/');
			return Math.floor(((this.date.getTime() - this.now) / (24 * 60 * 60 * 1000)));
			}
	};
}

</script>
