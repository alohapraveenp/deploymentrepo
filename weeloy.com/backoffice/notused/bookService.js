app.service('bookService',['$http',function($http){

	this.checkAvail = function(date, time, pers, booking) {

		if(typeof date !== "string") 
			date = this.getStringDate(date, 2);	
		date = date.replace(/\//g, "-");
		return $http.post("../api/restaurant/availslotbkg/",
			{
				'restaurant': restaurant,
				'booking': booking,
				'date' 	: date,
				'time' 	: time,
				'pers' 	: pers,
				'token'	: token
			}).then(function(response) { return response.data;})
		};

	this.readBooking = function(restaurant) {
		return $http.post('../api/visit/read/', { 'restaurant': restaurant, 'mode': '', 'token' : token } ).then(function(response) {
			names = response.data.data;
			for (i = 0; i < names.length; i++) {
				names[i].index = i + 1;
				names[i].time = names[i].time.substr(0, 5);
				names[i].vdate = (new Date(names[i].date)).getTime();	
				names[i].ddate = names[i].date.substr(8, 2) + '-' + names[i].date.substr(5, 2) + '-' + names[i].date.substr(0, 4);
				names[i].mealtype = (parseInt(names[i].time.substr(0, 2)) < 16) ? "lunch" : "dinner";
				if(names[i].state == "") names[i].state = "to come";
				if (names[i].wheelwin == "" && names[i].bkstatus == "cancel")
					names[i].wheelwin = "cancel";
					}
				}
			return response.data.data;
			}
		)};
				
	this.getStringDate = function(dd, type) {	
		aa = new Date(dd);
		yy = aa.getFullYear();
		mm = parseInt(aa.getMonth())+1;
		dd = aa.getDate()
		if(mm < 10) mm = "0" + mm;
		if(dd < 10) dd = "0" + dd;
		if(type == 1) return yy + "-" + mm + "-" + dd;
		else if(type == 2) return dd + "-" + mm + "-" + yy;
		else return aa.getTime();
		};		


}]);

