
app.controller('ControllerPromo', ['$scope', 'bookService', function ($scope, bookService) {
	var forceDate = new Date(new Date().getFullYear()-2, 0, 1);
        var todaydate = new Date();
        $scope.paginator = new Pagination(25);
        $scope.startIndex = 0;
        $scope.restaurant = restaurant;
        $scope.email = email;
        $scope.typeselection = 1;
        $scope.tmpArr = [];
        $scope.predicate = 'value';
        $scope.reverse = false;
        $scope.Offers = Offers.slice(0);
        $scope.PartnerPrograms = [' ','cpp_credit_suisse'];
        $scope.mydata_start = new bookService.ModalDataBooking();
        $scope.mydata_endng = new bookService.ModalDataBooking();
        $scope.startpromo = $scope.endpromo = "";
        $scope.mydata_start.setInit(todaydate, "09:00", function () { $scope.selectedItem.start = $scope.mydata_start.originaldate.getDateFormatReverse('-'); }, 3, todaydate, new Date(todaydate.getFullYear() + 1, 11, 31));
        $scope.mydata_endng.setInit(todaydate, "09:00", function () { $scope.selectedItem.end = $scope.mydata_endng.originaldate.getDateFormatReverse('-');}, 3, todaydate, new Date(todaydate.getFullYear() + 1, 11, 31));
        $scope.tabletitle = [{a: 'value', b: 'Order', c: '', q: 'down', cc: 'fuchsia'}, {a: 'name', b: 'Promo Name', c: '', q: 'down', cc: 'black'}, {a: 'offer', b: 'Promotion', c: '', q: 'down', cc: 'black'}, {alter: 'startv', a: 'start', b: 'StartDate', c: 'date', q: 'down', cc: 'black'}, {alter: 'endv', a: 'end', b: 'EndDate', c: 'date', q: 'down', cc: 'black'}, {a: 'cpp', b: 'Partner Program', c: '', q: 'down', cc: 'black'}];
        $scope.bckups = $scope.tabletitle.slice(0);
        $scope.tabletitleContent = [{a: 'value', b: 'Order', c: '', d: 'sort', t: 'input'}, {a: 'name', b: 'Promo Name', c: '', d: 'tag', t: 'input'}, {a: 'description', b: 'Description', c: '', d: 'list', t: 'textarea'}, {a: 'tnc', b: 'Terms and Conditions', c: '', d: 'list', t: 'textarea'}, {a: 'offer', b: 'Promotion', c: '', d: 'tower', t: 'array', u: $scope.Offers}, {a: 'start', b: 'Start date', c: 'date', d: 'tower', t: 'date', u: $scope.mydata_start }, {a: 'end', b: 'End date', c: 'date', d: 'tower', t: 'date', u: $scope.mydata_endng }, {a: 'cpp', b: 'Partner Program', c: '', d: 'tower', t: 'array', u: $scope.PartnerPrograms}];
        $scope.Objpromo = function () {
            var i, m = 0, isdefault, index;
            for (i = 0; i < $scope.promos.length; i++)
                if ($scope.promos[i].value > m)
                    m = $scope.promos[i].value;
            m += 1;
            isdefault = ($scope.promos.length == 0) ? 1 : 0;
            index = ($scope.promos.length > 0) ? $scope.promos[$scope.promos.length - 1].index + 1 : 1;
            return {
                restaurant: $scope.restaurant,
                ID: 0,
                index: index,
                name: '',
                value: m,
                offer: '',
                description: '',
                tnc: '',
                start: '',
                end: '',
                startv: '',
                endv: '',
                sddate: '',
                eddate: '',
                is_default: isdefault,
                cpp: '',
                remove: function () {
                    for (var i = 0; i < $scope.promos.length; i++)
                        if ($scope.promos[i].name === this.name) {
                            $scope.promos.splice(i, 1);
                            break;
                        }
                },
                replicate: function (obj) {
                    for (var attr in this) {
                        if (this.hasOwnProperty(attr) && typeof this[attr] !== 'function' && attr !== '$$hashKey' && typeof obj[attr] !== 'undefined' && typeof obj[attr] !== null)
                            this[attr] = obj[attr];
                    }
                    return this;
                },
                Retdata: function () {
                    var tmp = {};
                    for (var attr in this)
                        if (this.hasOwnProperty(attr) && typeof this[attr] !== 'function' && attr !== '$$hashKey') {
                            tmp[attr] = this[attr];
                        }
                    return tmp;
                },
                clean: function () {
                    if (this.start === '')
                        this.start = $scope.mydata_start.getDate('-', 'reverse');
                    if (this.end === '')
                        this.end = $scope.mydata_endng.getDate('-', 'reverse');
                    for (var attr in this)
                        if (this.hasOwnProperty(attr) && typeof this[attr] !== 'function' && attr !== '$$hashKey') {
                            if (typeof this[attr] === 'string') {
                                this[attr] = this[attr].replace(/\'|\"/g, '’');
                            }
                        }
                    return this;
                },
                check: function (action) {
                    if (this.name === '' || this.name.length < 6) {
                        alert('Invalid name, empty or too short(5)');
                        return -1;
                    }
                    if (this.description === '' || this.description.length < 3) {
                        alert('Invalid description, empty or too short(2)');
                        return -1;
                    }
                    if (this.offer === '' || this.offer.length < 5) {
                        alert('Invalid offer, empty ?');
                        return -1;
                    }
                    if (action == 'create')
                        for (var i = 0; i < $scope.promos.length; i++)
                            if ($scope.promos[i].name === this.name) {
                                alert("name is not unique");
                                return -1
                            }
                    return 1;
                }
            };
        };
        $scope.initorder = function () {
            $scope.tabletitle = $scope.bckups;
            $scope.predicate = "value";
            $scope.reverse = false;
        };
        $scope.reorder = function (item, alter) {
            alter = alter || "";
            if (alter !== "")
                item = alter;
            $scope.reverse = ($scope.predicate === item) ? !$scope.reverse : false;
            $scope.predicate = item;
        };
        bookService.readPromo($scope.restaurant, $scope.email).then(function (response) {
            var i, data;
            if (response.status !== 1 && response.status !== "1")
                return;
            $scope.promos = [];
            data = response.data.promo;
            for (i = 0; i < data.length; i++) {
                data[i].index = 1 + i;
                data[i].startv = data[i].start.jsdate().getTime();
                data[i].endv = data[i].end.jsdate().getTime();
                data[i].value = parseInt(data[i].value);
                data[i].sddate = data[i].start.jsdate().getDateFormat('-');
                data[i].eddate = data[i].end.jsdate().getDateFormat('-');
                $scope.promos.push(new $scope.Objpromo().replicate(data[i]));
            }
            $scope.initorder();
        });
        $scope.cleaninput = function (ll) {
            if (typeof $scope.selectedItem[ll] === 'string')
                $scope.selectedItem[ll] = $scope.selectedItem[ll].replace(/\'|\"/g, '’');
        };
        $scope.view = function (oo) {
            $scope.selectedItem = oo;
            $scope.reset('viewFlagPromo');
        };
        $scope.reset = function flag(field) {
            $scope.listingFlagPromo = false;
            $scope.createFlagPromo = false;
            $scope.viewFlagPromo = false;
            $scope[field] = true;
        };
        $scope.backlisting = function () {
            $scope.reset('listingFlagPromo');
        };
        $scope.create = function () {
            $scope.selectedItem = new $scope.Objpromo();
            $scope.mydata_start.setDate(todaydate);
            $scope.mydata_endng.setDate(todaydate);
            $scope.reset('createFlagPromo');
            $scope.buttonlabel = "Save promotion";
            $scope.action = "create";
            return false;
        };
        
        $scope.update = function (oo) {
            $scope.selectedItem = new $scope.Objpromo().replicate(oo);
			$scope.mydata_start.originaldate = new Date($scope.selectedItem.start);
			$scope.mydata_endng.originaldate = new Date($scope.selectedItem.end);
			console.log('UPDATE', $scope.mydata_endng, $scope.tabletitleContent);
            $scope.reset('createFlagPromo');
            $scope.buttonlabel = "Update promotion";
            $scope.action = "update";
            return false;
        };
        $scope.setdefault = function (oo) {

            bookService.setdefaultPromo($scope.restaurant, $scope.email, oo.name).then(function (response) {
                if (response.status > 0) {
                    for (var i = 0; i < $scope.promos.length; i++)
                        $scope.promos[i].is_default = '0';
                    oo.is_default = '1';
                }
                else
                    alert('promotion ' + oo.name + ' has NOT been set as default. ' + response.errors);
            });
        };
        $scope.setuppromo = function (oo) {
            var i, value = oo.value;
            if (value <= 1)
                return;
            bookService.setupPromo($scope.restaurant, $scope.email, oo.name, oo.value).then(function (response) {
                for (var i = 0; i < $scope.promos.length; i++)
                    if ($scope.promos[i].value === (value - 1)) {
                        $scope.promos[i].value = value;
                    }
                oo.value = value - 1;
                $scope.promos.sort(function (a, b) {
                    return a.value - b.value;
                });
            });
        }


        $scope.delete = function (oo) {
            if (confirm("Are you sure you want to delete " + oo.name) == false)
                return;
            bookService.deletePromo($scope.restaurant, $scope.email, oo.name).then(function (response) {
                if (response.status > 0) {
                    alert('promotion ' + oo.name + ' has been deleted');
                    oo.remove();
                }
                else
                    alert('promotion ' + oo.name + ' has NOT been deleted. ' + response.errors);
            });
        };
        $scope.savepromotion = function () {
            var apiurl, oo = $scope.selectedItem, msg = ($scope.action == "create") ? "updated" : "created";
 			
			oo = new $scope.Objpromo().replicate($scope.selectedItem);
            oo.clean();
            if (oo.check($scope.action) < 0) {
                return;
            }
            oo.startv = oo.start.jsdate().getTime();
            oo.endv = oo.end.jsdate().getTime();
            oo.sddate = oo.start.jsdate().getDateFormat('-');
            oo.eddate = oo.end.jsdate().getDateFormat('-');
            if ($scope.action == "update") {
                bookService.updatePromo($scope.restaurant, $scope.email, oo).then(function (response) {
                    if (response.status > 0) {
                        alert('promotion ' + oo.name + ' has been updated');
                        oo.remove();
                        $scope.promos.push(oo);
                    }
                    else
                        alert('promotion ' + oo.name + ' has NOT been updated. ' + response.errors);
                });
            }
            else if ($scope.action == "create") {
                bookService.createPromo($scope.restaurant, $scope.email, oo).then(function (response) {
                    if (response.status > 0) {
                        $scope.promos.push(oo);
                        alert('promotion ' + oo.name + ' has been created');
                    }
                    else
                        alert('promotion ' + oo.name + ' has NOT been created. ' + response.errors);
                });
            }

            $scope.backlisting();
        };
    }]);