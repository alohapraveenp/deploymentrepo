<?php

require_once("config.inc.php");

$isTheFunKitchen = (preg_match("/TheFunKitchen/", $theRestaurant));

?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
<meta name="author" content="weeloy. https://www.weeloy.com">
<meta name="copyright" content="2014 weeloy. All rights reserved. https://www.weeloy.com">  
<title>Weeloy - backoffice</title>
<link rel="icon" href="/favicon.ico" type="image/gif" sizes="16x16">
<link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="../css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
<link href="../css/jquery-ui.css" rel="stylesheet" type="text/css">
<link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="../css/admin-style.css" rel="stylesheet" type="text/css"/>
<link href="../css/carousel.css" rel="stylesheet" type="text/css">
<link href="../css/bullet.css" rel="stylesheet" type="text/css">
<link href="../css/dropdown.css" rel="stylesheet" type="text/css">
<link href="../css/bootstrap-social.css" rel="stylesheet" type="text/css"/>
<link href="../css/login.css" rel="stylesheet" type="text/css">
<link href="../css/modal.css" rel="stylesheet" type="text/css">
<link href='https://fonts.googleapis.com/css?family=Dosis:400,600,700bold|Roboto:400,900,700italic,700,500italic,500,400italic' rel='stylesheet' type='text/css'/>

<script type="text/javascript" src="../js/jquery.min.js"></script>
<script type="text/javascript" src="../js/jquery-ui.js"></script>
<script type="text/javascript" src="../js/bootstrap.min.js"></script>
<script type='text/javascript' src="../js/angular.min.js"></script>
<script type="text/javascript" src="../js/angular-file-upload.js"></script>
<script type='text/javascript' src="../js/ui-bootstrap-tpls-0.12.1.min.js"></script>
<script type='text/javascript' src="../js/alog.js?0"></script>

<style> 
#pieD3 { font-family: Roboto, Helvetica, Arial, sans-serif; width: 850px; height: 500px; position: relative; } 
svg { width: 100%; height: 100%; } path.slice{ stroke-width:2px; } 
polyline{ opacity: .3; stroke: black; stroke-width: 2px; fill: none; } 
.myinput { font-family: helvetica;  font-size: 10px; }
.input11 { font-size: 11px; }
.input13 { font-size: 13px; }
.input14 { font-size: 14px; }
.mysave { width:200px; color:#ffffff; }
.popover { min-width: 350px ! important; }
.myrange { border:0; color:#f6931f; font-size:12px; font-weight:bold; }
.ui-slider-range { border-color: #ef2929; background-color: #2AABD2;}
.red { color:red; }
.green { color:green; }
.purple { color:purple; }
.orange { color:orange; }
.fuchsia  { color:fuchsia; }
.black { color:black; }
.cancelhover { color:orange; }
.cancelhover:hover { color:red; text-decoration:underline; }
.infobk { font-size:12px; padding-right:40px; }
.truncate { white-space: nowrap; overflow: hidden; text-overflow: ellipsis; }
.glyphiconsize  { font-size:12px; }

.plot-container {
	box-sizing: border-box;
	width: 850px;
	height: 500px;
	padding: 20px 15px 15px 15px;
	margin: 15px auto 30px auto;
	border: 1px solid #ddd;
	background: #fff;
	background: linear-gradient(#f6f6f6 0, #fff 50px);
	background: -o-linear-gradient(#f6f6f6 0, #fff 50px);
	background: -ms-linear-gradient(#f6f6f6 0, #fff 50px);
	background: -moz-linear-gradient(#f6f6f6 0, #fff 50px);
	background: -webkit-linear-gradient(#f6f6f6 0, #fff 50px);
	box-shadow: 0 3px 10px rgba(0,0,0,0.15);
	-o-box-shadow: 0 3px 10px rgba(0,0,0,0.1);
	-ms-box-shadow: 0 3px 10px rgba(0,0,0,0.1);
	-moz-box-shadow: 0 3px 10px rgba(0,0,0,0.1);
	-webkit-box-shadow: 0 3px 10px rgba(0,0,0,0.1);
}

.plot-placeholder {
	width: 100%;
	height: 100%;
	font-size: 14px;
	line-height: 1.2em;
}


#punch_chart {
	color: #333;
	left: 50%;
	margin: -150px 0 0 -400px;
	position: absolute;
	top: 50%;
	width: 300px;
	width: 800px;
}


.scrollable-menu { 
	height: auto; 
	max-height: 200px; 
	overflow-x: hidden; 
	}


</style>
<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<script src="js/selectivizr-min.js"></script>
<script src="js/modernizr.js"></script>
<link rel="stylesheet" type="text/css" href="css/ie.css" media="screen" />
<![endif]-->
</head>

<body ng-app="backoffice">

<script>
var token = <?php echo "'".$_SESSION['user_backoffice']['token']."';"; ?>
var app = angular.module("backoffice", ['ui.bootstrap', 'angularFileUpload']);
</script>

<script type="text/javascript" src="inc/libService.js"></script>
<script type='text/javascript' src="inc/backofficelib.js?11"></script>
<script type='text/javascript' src="inc/paginator.js"></script>

<div class='modal fade' id='remoteModal' tabindex='1' role='dialog' aria-labelledby='remoteModalLabel' aria-hidden='true'>  
<div class='modal-dialog'><div class='modal-content'></div></div></div> 


<div class="ProfileBox">
  <div class="container">
    <div class="row">
    <div class="col-md-12 ProfileName">
		 <a href="index.php"><img src='../images/logo_w.png' width='120'><hr />  </a>        
		 <nav class="navbar navbar-default white-bg" role="navigation" style='border: 1px; padding: 1px; margin: 0 0 0 0;'>
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>
			<?php print_left_navbar($navbarAr); ?>
		 </div>
		</nav>
		<hr />
		<p id="sessiontime" class = 'small'></p><hr/>
		</div>
	</div>
</div>
</div>


<div class="move">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="white-bg">

<?php print_body(); ?>    

                        </div>
                    </div>
                </div>


<?php print_usefull(); ?>

            </div>
        </div>

<script> 
<?php printf("var cookiename = '%s';", getCookiename('backoffice')); ?>
</script>

<?php print_javascript(); ?>

<?php print_div_login_modal(); ?>   
    </body>
</html>