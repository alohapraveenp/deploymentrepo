<?php $isTheFunKitchen = (preg_match("/TheFunKitchen/", $theRestaurant)); ?>

<div class="container">
    <div class="row">
        <div class="col-md-12 left-sec">

            <div>
                <div id="reporting" ng-controller="VisiteController" ng-init="listingVisitFlag = true;formatDisplay='standard';" >

                    <div id='listing' ng-show='listingVisitFlag'>
                        <div class="form-group"  style='margin-bottom:25px;'>
                            <div class="col-md-4">
                                <div class="input-group col-md-4">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                                    <input type="text" class="form-control input-sm" ng-model="searchText" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" style='width:200px;'> 
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Customized Selection<span class="caret"></span></button>
                                    <ul class="dropdown-menu" role="menu">
                                        <li ng-repeat="x in extratitle"><extitle name="{{x.a}}"/></extitle></li>
                                        <li><a href="javascript:;" class='glyphr glyphicon glyphicon-ok' ng-click="extraselect('reserve')"> by reservation date</a></li>
                                        <li><a href="javascript:;" class='glyphc' ng-click="extraselect('create')"> by creation date</a></li>
                                    </ul>
                                </div>		
                            </div>

                            <div class="col-md-4">
                                </form>

                                <form action="echo.php" id="extractForm" name="extractForm" method="POST" target="_blank">
                                    <input type="hidden" value="testing" ng-model='content' name="content" id="content">
                                    <a href="javascript:;" ng-click='extractSelection();' class="btn btn-success btn-sm" style='color:white;'><span class='glyphicon glyphicon-save'></span> &nbsp;Extract Selection</a>
                                </form>
                            </div>
                        </div><br/>

                                <span style='font-size:12px;padding-right:40px;'>selected bookings: <strong> {{filteredPeople.length}} </strong></span>
                                <span style='font-size:12px;padding-right:40px;'>current selection: <strong> {{currentSelection}} </strong></span>
                                <br /><br />

<!-- try ng-switch but doest not seem to work. it has a bad side effect on fileteredPeople !? -->

                        <table width='100%' class="table table-condensed table-striped" style='font-family:helvetica;font-size:12px;'>
                            <thead><tr><th ng-repeat="y in tabletitle"><tbtitle name="{{y.b}}"/></th></tr></thead>
							<tr ng-repeat="x in filteredPeople = (names| filter:searchText) | orderBy:predicate:reverse | sliceVisit:currentPage:itemsPerPage:selectionSize | limitTo:itemsPerPage" ng-if="formatDisplay!='today'">
								<td>{{ x.index}}</td>
								<td>{{ x.first}}</td>
								<td>{{ x.last}}</td>
								<td>{{ x.phone}}</td>
								<td>{{ x.date}}</td>
								<td>{{ x.time}}</td>
								<td>{{ x.pers}}</td>
								<td><a href="javascript:;" ng-click="view(x.index)" style='color:red;'>{{ x.booking}}</a></td>
								<td>{{ x.wheelwin}}</td>
							</tr>

							<tr ng-repeat="x in filteredPeople = (names| filter:searchText) | orderBy:predicate:reverse | sliceVisit:currentPage:itemsPerPage:selectionSize | limitTo:itemsPerPage" ng-if="formatDisplay=='today'">
								<td>{{ x.first}}</td>
								<td>{{ x.last}}</td>
								<td>{{ x.phone}}</td>
								<td>{{ x.date}}</td>
								<td>{{ x.time}}</td>
								<td>{{ x.pers}}</td>
								<td><a href="javascript:;" ng-click="view(x.index)" style='color:blue;'>{{ x.booking}}</a></td>
								<td>{{ x.mealtype}}</td>
								<td><select ng-model="x.state" ng-options="val for val in stateAr" ng-change="savechgstatebkg(x.booking, x.state)"></select></td>
							</tr>

							<tr><td colspan='11' align='center' style='background-color:white;'>

									<div>
										<ul class="pagination">
											<li ng-class="prevPageDisabled()">
												<a href="javascript:;" ng-click="prevPage()">« Prev</a>
											</li>
											<li ng-repeat="n in range()" ng-class="{active: n == currentPage}" ng-click="setPage(n)">
												<a href="javascript:;">{{n + 1}}</a>
											</li>
											<li ng-class="nextPageDisabled()">
												<a href="javascript:;" ng-click="nextPage()">Next »</a>
											</li>
										</ul>
									</div>

								</td></tr>
                        </table>
                    </div>

                    <div class="col-md-12" id='aReview' ng-hide='listingVisitFlag'>
                        <br />
                        <a href="javascript:;" class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a>
                        <br />
                        <table class='table-striped' ng-repeat="x in SelectedItem" style="margin: 0 0 150px 30px;font-size:13px;font-family: Roboto">		
                            <tr><td nowrap><strong>Booking Confirmation: </strong></td><td> &nbsp; </td><td>{{ x.booking}}</td></tr>
                            <tr><td nowrap><strong>Status: </strong></td><td> &nbsp; </td><td>{{ x.bkstatus}}</td></tr>
                            <tr><td nowrap><strong>Reservation Date: </strong></td><td> &nbsp; </td><td>{{ x.date}}</td></tr>
                            <tr><td nowrap><strong>Reservation Time: </strong></td><td> &nbsp; </td><td>{{ x.time.substr(0, 5)}}</td></tr>
                            <tr><td nowrap><strong>Number of Persons: </strong></td><td> </td><td>{{ x.pers}}</td></tr>
                            <tr><td nowrap><strong>First Name: </strong></td><td> &nbsp; </td><td>{{ x.first}}</td></tr>
                            <tr><td nowrap><strong>Last Name: </strong></td><td> &nbsp; </td><td>{{ x.last}}</td></tr>
                            <tr><td nowrap><strong>Email: </strong></td><td> &nbsp; </td><td>{{ x.email}}</td></tr>
                            <tr><td nowrap><strong>Mobile: </strong></td><td> </td><td>{{ x.phone}}</td></tr>
                            <tr><td nowrap><strong>Create Date: </strong></td><td> </td><td>{{ x.createdate}}</td></tr>
                            <tr ng-show="x.comment !=''"><td nowrap><strong>Request: </strong></td><td> </td><td>{{ x.comment}}</td></tr>
                            <tr><td nowrap><strong>Win: </strong></td><td> </td><td>{{ x.wheelwin}}</td></tr>
                            <tr><td nowrap><strong>ResCode: </strong></td><td> </td><td>{{ x.restCode}}</td></tr>
                            <tr ng-show="x.tracking !=''"><td nowrap><strong>Tracking: </strong></td><td> </td><td>{{ x.tracking}}</td></tr>
                            <tr ng-show="x.booker !=''"><td nowrap><strong>Staff: </strong></td><td> </td><td>{{ x.booker}}</td></tr>
                            <tr ng-show="formatDisplay =='today'"><td nowrap><strong>State: </strong></td><td> </td><td style='color:red;'>{{ x.state}}</td></tr>
                            <?php if($isTheFunKitchen) echo "<tr><td nowrap><strong>membCode: </strong></td><td> </td><td>{{ x.membCode }}</td></tr>"; ?>
                            <tr ng-show='checkdate(x.date, x.bkstatus, x.wheelwin)'><td>&nbsp;</td><td nowrap colspan='2'><strong><a href='../modules/manage_booking/visitcancel.php?confirmation={{x.booking}}&bkemail={{x.email}}' target='_blank' class="cancelhover">Cancel booking</a></strong></td></tr>
                            <tr><td>&nbsp;</td><td> &nbsp; </td><td></td></tr>
                            <tr ng-show='checkdate(x.date, x.bkstatus, x.wheelwin)'><td>&nbsp;</td><td nowrap colspan='2'><strong><a href='javascript:;' ng-click="cancel(x.index)" class="cancelhover">Modify booking</a></strong></td></tr>
                            <tr><td>&nbsp;</td><td> &nbsp; </td><td></td></tr>
                            <tr><td nowrap><strong><a class='btn btn-primary btn-sm' ng-click="spin(x.index)" style='color:white;'>SPIN THE WHEEL</a></strong></td><td> &nbsp; </td><td> &nbsp;</td></tr>
                            <tr><td>&nbsp;</td><td> &nbsp; </td><td></td></tr>
                        </table>
                    </div>

                </div>
            </div>

<script>

var token = <?php echo "'".$_SESSION['user_backoffice']['token']."';"; ?>
var restaurant = <?php echo "'".$theRestaurant."';"; ?>
var currentday = <?php echo "'".date("Y-m-d")."';"; ?>
var currentmonth = <?php echo date("m") . ";"; ?>
var currentyear = <?php echo "'".date("Y")."';"; ?>
var currentjour = <?php echo "'".date("d")."';"; ?>
var currentweek = <?php echo intval(date("W")) . ";"; ?>

aday.init(currentday);

var selectionAr;

backoffice.filter('sliceVisit', function() {
	return function(arr, currentPage, itemsPerPage) {
	if (arr == undefined) return;
	return arr.slice(currentPage * itemsPerPage, (currentPage + 1) * itemsPerPage);
}
});

backoffice.directive('extitle',function($compile){
	return {
		restrict: 'AE',
		replace: true,
		link: function(scope, element, attrs){
			if(scope.x.a != "divider") element.html('<li style="margin: 0 0 8px 20px;"><a href="javascript:;" ng-click="extraselect(x.a)"><span class="custom" id="' + scope.x.a + '" ></span>  {{ x.b }} <br /></a></li>').show();
			else element.html('<li class="divider"></li>').show();
			$compile(element.contents())(scope);
    		}
		}
});


backoffice.directive('0extitle',function(){
	var linkFunction = function(scope, elem, attrs) { 
		if(scope.x.a == "divider") { scope.x.a = scope.x.b = ""; }
		}		
	return {
		restrict: 'AE',
		replace: true,
		template: '<a href="javascript:;" ng-click="extraselect(x.a)">{{ x.b }}</a>',
		link: linkFunction
		}
});

backoffice.directive('tbtitle', function(){
	return {
		restrict: 'AE',
		replace: true,
		template: '<a href="javascript:;" id="{{y.a}}" ng-click="reorder(y.a);"><span id="{{y.a}}_span">{{ y.b }}<i class="fa fa-caret-down"></span></a>',
		link: function(scope, elem, attrs) { }
		}
});


backoffice.service('HttpService',['$http',function($http){
this.savechgstateservice = function(resstaurant, booking, state, token){

	return $http.post("../api/bookingservice/state/",
        {
			'restaurant': restaurant,
        	'booking' : booking,
            'state' 	: state,
            'token'  : token
        }).then(function(response) {return response.data;})
	};
}]);


backoffice.controller('VisiteController', function($scope, $http, HttpService) {

	$scope.itemsPerPage = 100;
	$scope.currentPage = 0;
	$scope.startIndex = 0;
	$scope.currentPage = 0;
	$scope.selectionSize = 0;
	$scope.restaurant = <?php echo "'".$theRestaurant."';"; ?>
	$scope.typeselection = 1;
	$scope.tmpArr = [];
	$scope.predicate = 'date';
	$scope.oldpredicate = 'date';
	$scope.reverse = 'false';
	$scope.tabletitleStandard = [ {'a':'index', 'b':'index' }, {'a':'first', 'b':'First Name' }, {'a':'last', 'b':'Last Name' }, {'a':'phone', 'b':'Mobile' }, {'a':'date', 'b':'Date' }, {'a':'time', 'b':'Time' }, {'a':'pers', 'b':'Cover' }, {'a':'booking', 'b':'Visit' }, {'a':'wheelwin', 'b':'Wins' } ];
	$scope.tabletitleToday = [ {'a':'first', 'b':'First Name' }, {'a':'last', 'b':'Last Name' }, {'a':'phone', 'b':'Mobile' }, {'a':'date', 'b':'Date' }, {'a':'time', 'b':'Time' }, {'a':'pers', 'b':'Cover' }, {'a':'booking', 'b':'Visit' }, {'a':'mealtype', 'b':'Meal Type' }, {'a':'state', 'b':'State' } ];
	$scope.tabletitle = $scope.tabletitleStandard;

	$scope.extratitle = [ {'a':'today', 'b':'Today' }, {'a':'lunch', 'b':'---> Lunch' }, {'a':'dinner', 'b':'---> Dinner' }, {'a':'week', 'b':'Current Week' }, {'a':'month', 'b':'Current Month' }, {'a':'future', 'b':'Future' }, {'a':'divider', 'b':'divider' }, {'a':'spin', 'b':'Used' }, {'a':'unspin', 'b':'Not Used' }, {'a':'cancel', 'b':'Cancelled' }, {'a':'uncancel', 'b':'Not Cancelled' }, {'a':'callcenter', 'b':'Call Center' }, {'a':'all', 'b':'All' }, {'a':'divider', 'b':'divider' }  ];
	$scope.extratitlesegment = [{'a':'reserve', 'b':' by reservation date' }, {'a':'creation', 'b':' by creation date' } ];

	$scope.stateAr=[ 'to come', 'seated', 'paying', 'left', 'no show' ]; 
	
//	$scope.extratitleTime = [ {'a':'today', 'b':'Today' }, {'a':'week', 'b':'Current Week' }, {'a':'month', 'b':'Current Month' }, {'a':'future', 'b':'Future' }, {'a':'divider', 'b':'divider' }];
//	$scope.extratitleUsage = [ {'a':'spin', 'b':'Used' }, {'a':'unspin', 'b':'Not Used' }, {'a':'cancel', 'b':'Cancelled' }, {'a':'uncancel', 'b':'Not Cancelled' }, {'a':'all', 'b':'All' } ];

	var url = '../api/visit/' + $scope.restaurant;
	$http.get(url).success(function(response) {
	$scope.names = response.data.visit;
	$scope.pageCount = Math.ceil($scope.names.length / 100) - 1;
	$scope.currentSelection = "all";
	$scope.selectionSize = $scope.names.length;
	for (i = 0; i < $scope.names.length; i++) {
		$scope.names[i].index = i + 1;
		$scope.names[i].time = $scope.names[i].time.substr(0, 5);
		$scope.names[i].mealtype = (parseInt($scope.names[i].time.substr(0, 2)) < 16) ? "lunch" : "dinner";
		if($scope.names[i].state == "") $scope.names[i].state = "to come";
		if ($scope.names[i].wheelwin == "" && $scope.names[i].bkstatus == "cancel")
			$scope.names[i].wheelwin = "cancel";
		}
	$scope.orgnames = $scope.names.slice(0);
	$scope.predicate = $scope.oldpredicate = "date";
	$scope.reverse = true;
	$scope.setContent("date");
	$scope.extraselect('all');
	});


	$scope.reorder = function(item) {
		$scope.predicate = item;
		if ($scope.oldpredicate == item)
			$scope.reverse = !$scope.reverse;
		else $scope.reverse = (item != 'date') ? false : true;
		$scope.oldpredicate = item;
		$scope.setContent(item);
		};
	
	$scope.setContent = function(item) {
		titlecolor = ($scope.reverse) ? 'orange' : 'fuchsia';
		content = $('#' + item + "_span").text();
		$('th a span').css('color', 'black');
		$('#' + item + "_span").css('color', titlecolor);
		if ($scope.reverse) $('#' + item + "_span").html(content + "<i class='fa fa-caret-up'>");
		else $('#' + item + "_span").html(content + "<i class='fa fa-caret-down'>");
		};
		
	$scope.extraselect = function(item) {

		$scope.current = $scope.names.slice(0);
		$scope.names = $scope.orgnames.slice(0);
		limit = $scope.names.length;
		$scope.tmpArr = [];
		switch (item) {

	case 'today':
	case 'lunch':
	case 'dinner':
		for (i = limit - 1; i >= 0; i--) {
			dd = ($scope.typeselection == 1) ? $scope.names[i].date : $scope.names[i].createdate;
			if(dd == currentday && (item == 'today' || $scope.names[i].mealtype == item))
					$scope.tmpArr.push($scope.names[i]); 
			}

		$('.custom').removeClass('glyphicon glyphicon-ok');
		$('#' + item).addClass('glyphicon glyphicon-ok');

		$scope.finish('Today', item);
		break;
			
	case 'week':
		for (i = 0; i < limit; i++) {
			dd = ($scope.typeselection == 1) ? $scope.names[i].date : $scope.names[i].createdate;
			if (aday.toWeek(dd) == currentweek)
			$scope.tmpArr.push($scope.names[i]);
		}

		$scope.finish('current week', item);
		break;
		
	case 'month':
		for (i = 0; i < limit; i++) {
			dd = ($scope.typeselection == 1) ? $scope.names[i].date : $scope.names[i].createdate;
			if (parseInt(dd.substring(5, 7)) == currentmonth)
			$scope.tmpArr.push($scope.names[i]);
		}

		$scope.finish('current month', item);
		break;
		
	case 'future':
		for (i = 0; i < limit; i++) {
			dd = ($scope.typeselection == 1) ? $scope.names[i].date : $scope.names[i].createdate;
			if (aday.toDays(dd) >= 0)
			$scope.tmpArr.push($scope.names[i]);
			}

		$scope.finish('coming bookings', item);
		break;
		
	case 'spin':
		for (i = 0; i < limit; i++) {
			if ($scope.names[i].wheelwin != "")
			$scope.tmpArr.push($scope.names[i]);
			}

		$scope.finish('Spon', item);
		break;
		
	case 'unspin':
		for (i = 0; i < limit; i++) {
			if ($scope.names[i].wheelwin == "" && $scope.names[i].wheelwin != "cancel")
			$scope.tmpArr.push($scope.names[i]);
			}

		$scope.finish('Unspin', item);
		break;
		
	case 'cancel':
		for (i = 0; i < limit; i++) {
			if ($scope.names[i].bkstatus == "cancel")
				$scope.tmpArr.push($scope.names[i]);
		}

		$scope.finish('cancel', item);
		break;
		
	case 'uncancel':
		for (i = 0; i < limit; i++) {
			if ($scope.names[i].bkstatus != "cancel")
				$scope.tmpArr.push($scope.names[i]);
			}

		$scope.finish('Uncancel', item);
		break;
		
	case 'callcenter':
		for (i = 0; i < limit; i++) {
			if ($scope.names[i].tracking == "CALLCENTER")
				$scope.tmpArr.push($scope.names[i]);
			}

		$scope.finish('Call Center', item);
		break;
		
	case 'all':
		$scope.names = $scope.orgnames.slice(0);
		$scope.currentSelection = item;
		$('.custom').removeClass('glyphicon glyphicon-ok');
		$('#' + item).addClass('glyphicon glyphicon-ok');
		break;

	case 'reserve' :
		$scope.typeselection = 1;
		$('.glyphr').toggleClass('glyphicon glyphicon-ok');
		$('.glyphc').toggleClass('glyphicon glyphicon-ok');
		break;
		
	case 'create' :
		$scope.typeselection = 2;
		$('.glyphr').toggleClass('glyphicon glyphicon-ok');
		$('.glyphc').toggleClass('glyphicon glyphicon-ok');
		break;
		
	default:
		break;
	}


	};
	
	$scope.finish = function(mess, item) {

		if ($scope.tmpArr.length > 0) {
			$scope.names = $scope.tmpArr.slice(0);
			zz = $scope.tmpArr.length?'s':'';
			//alert('Search ' + mess + ': found ' + $scope.tmpArr.length + ' booking' + zz);
			$('.custom').removeClass('glyphicon glyphicon-ok');
			$('#' + item).addClass('glyphicon glyphicon-ok');
			$scope.currentSelection = item;
			}
		else {
			$scope.names = $scope.current.slice(0);
			alert('Search ' + mess + ': no booking selected (stay with previous selection');
			}
		
		if(item != "today" && item != "lunch" && item != "dinner") {
			$scope.formatDisplay = "standard";
			$scope.tabletitle = $scope.tabletitleStandard;
			}
		else {
			$scope.formatDisplay = "today";
			$scope.tabletitle = $scope.tabletitleToday;
			}
		

		};

	$scope.view = function(id) {

		$scope.SelectedItem = $scope.orgnames.slice(id - 1, id);		// take the orgnames as names get modified with the shortcuts
		$scope.listingVisitFlag = false;
		};
	
	$scope.backlisting = function() {

		$scope.SelectedItem = [];
		$scope.listingVisitFlag = true;
		};
		
	$scope.spin = function(id) {
		state = $scope.names[id - 1].wheelwin;
		if (state != '') {
			if (state == 'cancel')
				alert("The reservation has been previously canceled");
			else alert("The Wheel has already been spun! (" + state + ")")
			return false;
		}

		winp = window.open('../wheel/rotation/rotation.php?restaurant=' + $scope.restaurant + '&confirmation=' + $scope.names[id - 1].booking + '&restCode=' + $scope.names[id - 1].restCode + '&guestname=' + $scope.names[id - 1].first, 'weeloy', 'toolbar=no,width=1050,height=750,menubar=no,scrollbars=yes,resizable=no,alwaysRaised=yes');
		winp.moveTo(15, 25);
		winp.resizeTo(1050, 825);
		winp.focus();
		return false;
		};

	$scope.cancel = function(id) {
		winp = window.open('../modules/manage_booking/visitcancel.php?confirmation=' + $scope.names[id - 1].booking + '&bkemail=' + $scope.names[id - 1].email, 'weeloy - cancel', 'toolbar=no,width=1050,height=750,menubar=no,scrollbars=yes,resizable=no,alwaysRaised=yes');
		winp.moveTo(15, 25);
		winp.resizeTo(1050, 825);
		winp.focus();
		return false;	
		}
	
	$scope.savechgstatebkg = function(booking, state) {
	
		return HttpService.savechgstateservice($scope.restaurant, booking, state, token).then(function(response){ console.log(response); });
		

		apiurl = "../api/booking/state/";
		$.ajax({
			url : apiurl,
			type: "POST",
			data: {
				restaurant: $scope.restaurant,
				confirmation: booking,
				state: state,
				token:token
				},
			success: function(data, textStatus, jqXHR) { },
			error: function (jqXHR, textStatus, errorThrown) {}
			});
		}
		
	$scope.checkdate = function(rdate, status, wheel) {
		if(status == 'cancel')
			return false;
		
		if(wheel != '')
			return false;
			
		var month = currentmonth;
		var day = currentjour;
		var year = currentyear;
		dd = rdate.split('-');
		year1 = parseInt(dd[0]); month1 = parseInt(dd[1]); day1 = parseInt(dd[2]);
		//alert(year + "-"  + month + "-"  + day + "-"  + '\n' + year1 + "-"  + month1 + "-"  + day1 + "-" );
			
		return (year1 > year || (year1 == year && (month1 > month || (month1 == month && day1 >= day))));
		} 

	$scope.range = function() {
		var ret = [];
		for (i = 0; i <= $scope.pageCount; i++) {
			ret.push(i);
			}
		return ret;
		};
		
	$scope.nextPage = function() {
		if ($scope.currentPage < $scope.pageCount) {
			$scope.currentPage++;
			}
		};
		
	$scope.numPage = function() {
		return $scope.pageCount;
		};
		
	$scope.prevPage = function() {
		if ($scope.currentPage > 0) {
		$scope.currentPage--;
		}
		};
		
	$scope.setPage = function(n) {
		$scope.currentPage = n;
		};
		
	$scope.prevPageDisabled = function() {
		return $scope.currentPage === 0 ? "disabled" : "";
		};
		
	$scope.nextPageDisabled = function() {
		return $scope.currentPage === $scope.pageCount ? "disabled" : "";
		};
		
	$scope.$watch("filteredPeople.length", function(newvalue, oldvalue) {
		if ($scope.filteredPeople == undefined) 
			return;
		if ($scope.selectionSize != $scope.filteredPeople.length) {
			$scope.selectionSize = $scope.filteredPeople.length;
			$scope.currentPage = 0;
			$scope.pageCount = Math.ceil($scope.filteredPeople.length / 100) - 1;
			}
		});
		
	$scope.extractSelection = function() {

		tt = sep = "";
		limit = $scope.filteredPeople.length;
		for (i = 0; i < limit; i++, sep = ', ') {
			u = $scope.filteredPeople[i];
			tt += sep + '{ "salutation":"' + u.salutation + '", "firstname":"' + u.first + '", "lastname":"' + u.last + '", "pers":"' + u.pers + '", "date":"' + u.date + '", "mealtype":"' + u.mealtype + '", "arrival time":"' + u.time + '", "state":"' + u.state + '", "request":"' + u.comment + '", "email":"' + u.email + '", "phone":"' + u.phone + '", "confirmation":"' + u.booking + '", "restCode":"' + u.restCode + '", "status":"' + u.bkstatus + '", "win":"' + u.wheelwin + '", "create date":"' + u.createdate + '"}';
			}
		tt = '{ "booking":[' + tt + '] }';
		$scope.content = tt;
		$('#content').val(tt);
		$('#extractForm').submit();
		};
	});

function extractingSelection($scope) {

	if ($scope.filteredPeople == undefined) return [];
	selectionAr = [];
	limit = $scope.filteredPeople.length;
	for (i = 0; i < limit; i++) {
		u = $scope.filteredPeople[i];
		selectionAr[i] = [u.first, u.last, u.phone, u.booking, u.date, u.time];
		}
	alert(selectionAr[0]);
	return selectionAr;
	}
</script>

        </div>
    </div>
</div>
