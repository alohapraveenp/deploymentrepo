<?php

require_once("config.inc.php");

$isTheFunKitchen = (preg_match("/TheFunKitchen/", $theRestaurant));

$adminversion = "1.30";
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
<meta name="author" content="weeloy. https://www.weeloy.com">
<meta name="copyright" content="2014 weeloy. All rights reserved. https://www.weeloy.com">  
<title>Weeloy - backoffice</title>
<link rel="icon" href="../favicon.ico" type="image/gif" sizes="16x16">
<link href="../client/bower_components/nvd3/nv.d3.min.css" rel="stylesheet" type="text/css" />    
<link href="../client/bower_components/material/angular-material.min.css" rel="stylesheet" type="text/css" />    
<link href="../client/bower_components/material-design-icons/iconfont/material-icons.css" rel="stylesheet" type="text/css" />    
<link href="../client/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="../client/bower_components/bootstrap/dist/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
<link href="../client/bower_components/jquery-ui/themes/base/jquery-ui.css" rel="stylesheet" type="text/css">
<link href="../client/bower_components/fontawesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="../client/bower_components/material/css/bootstrap-material-datetimepicker.css" rel="stylesheet"  type="text/css" />
<link href="../css/admin-style.css" rel="stylesheet" type="text/css"/>
<link href="../css/carousel.css" rel="stylesheet" type="text/css">
<link href="../css/bullet.css" rel="stylesheet" type="text/css">
<link href="../css/dropdown.css" rel="stylesheet" type="text/css">
<link href="../css/bootstrap-social.css" rel="stylesheet" type="text/css"/>
<link href="../css/login.css" rel="stylesheet" type="text/css">
<link href="../css/modal.css" rel="stylesheet" type="text/css">
<link href="../css/multirange.css" rel="stylesheet" type="text/css">
<link href='https://fonts.googleapis.com/css?family=Dosis:400,600,700bold|Roboto:400,900,700italic,700,500italic,500,400italic' rel='stylesheet' type='text/css'/>

<script type='text/javascript' src="../client/bower_components/jquery/dist/jquery.min.js"></script>
<script type='text/javascript' src="../client/bower_components/jquery-ui/jquery-ui.js"></script>
<script type='text/javascript' src="../client/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script type='text/javascript' src="../client/bower_components/angular/angular.min.js"></script>
<script type='text/javascript' src="../client/bower_components/angular/angular-animate.min.js"></script>
<script type='text/javascript' src="../client/bower_components/material/angular-material.min.js"></script>
<script type='text/javascript' src="../client/bower_components/angular/angular-aria.min.js"></script>
<script type='text/javascript' src="../client/bower_components/angular-sanitize/angular-sanitize.min.js"></script>
<script type='text/javascript' src="../client/bower_components/angular/angular-messages.min.js"></script>
<script type="text/javascript" src="../client/bower_components/moment/min/moment-with-locales.min.js"></script>
<script type="text/javascript" src="../client/bower_components/material/bootstrap-material-datetimepicker.js"></script>
<script type='text/javascript' src="../client/bower_components/ui-bootstrap-tpls-0.14.2.min.js"></script>
<script type='text/javascript' src="../client/bower_components/nvd3/angular-nvd3/angular-nvd3.min.js"></script>
<script type='text/javascript' src="../client/bower_components/angular-material-icons/angular-material-icons.js"></script>
<script type='text/javascript' src="../client/bower_components/material/angular-material-calendar.js"></script>
<script type="text/javascript" src="../js/angular-file-upload.js"></script>
<script type='text/javascript' src="../js/alog.js?7"></script>
<script type='text/javascript' src="../js/date.format.js"></script>


<script type="text/javascript" src="app/api/blocklist/BlocklistConfigurationService.js"></script>
<script type="text/javascript" src="app/api/templatemanagement/TemplateConfigurationService.js"></script>
<script type='text/javascript' src="app/api/api.js"></script>




<style> 
#pieD3 { font-family: Roboto, Helvetica, Arial, sans-serif; width: 850px; height: 500px; position: relative; } 
//svg { width: 100%; height: 100%; } path.slice{ stroke-width:2px; } 
polyline{ opacity: .3; stroke: black; stroke-width: 2px; fill: none; } 
.myinput { font-family: helvetica;  font-size: 10px; }
.inputsmall { font-size:12px; height:22px }
.input11 { font-size: 11px; }
.input13 { font-size: 13px; }
.input14 { font-size: 14px; }
.mysave { width:200px; color:#ffffff; }
.popover { min-width: 350px ! important; }
.myrange { border:0; color:#f6931f; font-size:12px; font-weight:bold; }
.ui-slider-range { border-color: #ef2929; background-color: #2AABD2;}
.white { color:white; }
.blue { color:blue; }
.red { color:red; }
.green { color:green; }
.purple { color:purple; }
.orange { color:orange; }
.fuchsia  { color:fuchsia; }
.black { color:black; }
.cancelhover { color:orange; }
.cancelhover:hover { color:red; text-decoration:underline; }
.infobk { font-size:12px; padding-right:20px; }
.truncate { white-space: nowrap; overflow: hidden; text-overflow: ellipsis; }
.glyphiconsize  { font-size:12px; }

.plot-container {
	box-sizing: border-box;
	width: 850px;
	height: 500px;
	padding: 20px 15px 15px 15px;
	margin: 15px auto 30px auto;
	border: 1px solid #ddd;
	background: #fff;
	background: linear-gradient(#f6f6f6 0, #fff 50px);
	background: -o-linear-gradient(#f6f6f6 0, #fff 50px);
	background: -ms-linear-gradient(#f6f6f6 0, #fff 50px);
	background: -moz-linear-gradient(#f6f6f6 0, #fff 50px);
	background: -webkit-linear-gradient(#f6f6f6 0, #fff 50px);
	box-shadow: 0 3px 10px rgba(0,0,0,0.15);
	-o-box-shadow: 0 3px 10px rgba(0,0,0,0.1);
	-ms-box-shadow: 0 3px 10px rgba(0,0,0,0.1);
	-moz-box-shadow: 0 3px 10px rgba(0,0,0,0.1);
	-webkit-box-shadow: 0 3px 10px rgba(0,0,0,0.1);
}

.plot-placeholder {
	width: 100%;
	height: 100%;
	font-size: 14px;
	line-height: 1.2em;
}


#punch_chart {
	color: #333;
	left: 50%;
	margin: -150px 0 0 -400px;
	position: absolute;
	top: 50%;
	width: 300px;
	width: 800px;
}


.scrollable-menu { 
	height: auto; 
	max-height: 200px; 
	overflow-x: hidden; 
	}

.scrollable-menu::-webkit-scrollbar {
  -webkit-appearance: none;
  width: 10px; }

.scrollable-menu::-webkit-scrollbar-thumb {
  border-radius: 2px;
  background-color: black;
  -webkit-box-shadow: 0 0 1px rgba(255, 255, 255, 0.75); }


.dashed { stroke-dasharray: 5,5; }

.md-button.md-small {
	  text-transform: none;
	  font-size: 12px;
      line-height: 32px;
      min-height: 32px;
}

.md-button.md-red {background: red;color:white; }
.md-button.md-red:hover {background-color: orange;color:black; }

/*
.icon-thb { background-image: url("../images/cur_thb_icon&16.png"); }
.icon-myr { background-image: url("../images/cur_myr_icon&16.png"); }

.icon-thb, .icon-myr {
    background-position: center center;
    height: 16px;
    position: relative;
    top: -1px;
    width: 16px;
}
*/


    .table-striped2 > div:nth-of-type(odd) {
        background-color: #f9f9f9;
    }
    .table-striped2 > div>span {
        padding:5px 10px;
    }
</style>
<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<script src="js/selectivizr-min.js"></script>
<script src="js/modernizr.js"></script>
<link rel="stylesheet" type="text/css" href="css/ie.css" media="screen" />
<![endif]-->
<script>

var app = angular.module("backoffice", ['ui.bootstrap', 'angularFileUpload', 'nvd3', 'vds.multirange', 'ngMaterial', 'ngMessages', 'ngMdIcons','app.api', 'materialCalendar']);
</script>
</head>

<body ng-app="backoffice">

<script type='text/javascript' src="inc/multirange.js"></script>
<script>
var token = <?php echo "'".$_SESSION['user_backoffice']['token']."';"; ?>

</script>

<?php

if(!isset($adminversion)) $adminversion = strval(time());

printf("<script type='text/javascript' src='inc/libService.js?%s'></script>", $adminversion);
printf("<script type='text/javascript' src='inc/backofficelib.js?%s'></script>", $adminversion);
printf("<script type='text/javascript' src='inc/profilelib.js?%s'></script>", $adminversion);
?>

<script type='text/javascript' src="inc/paginator.js"></script>
<script type='text/javascript' src="FileSaver.min.js"></script>

<div class='modal fade' id='remoteModal' tabindex='1' role='dialog' aria-labelledby='remoteModalLabel' aria-hidden='true'>  
<div class='modal-dialog'><div class='modal-content'></div></div></div> 

 <div class="container ProfileBox">
    <div class="col-md-12 ProfileName">
		 <a href="javascript:location.reload(true);"><img src='../images/logo_puce.svg' width='80'><hr />  </a>        
		 <nav class="navbar navbar-default white-bg" role="navigation" style='border: 1px; padding: 1px; margin: 0 0 0 0;'>
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>
			<?php print_left_navbar($navbarAr); ?>
		 </div>
		</nav>
		<hr />
		<p id="sessiontime" class = 'small'></p><hr/>
		</div>
	
</div>


<div class="move">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="white-bg">

<?php print_body(); ?>    

                        </div>
                    </div>
                </div>


<?php print_usefull(); ?>

            </div>
        </div>

<script> 
<?php printf("var cookiename = '%s';", getCookiename('backoffice')); ?>
</script>

<?php print_javascript(); ?>

<?php print_div_login_modal(); ?>   
    </body>
</html>