jQuery(document).ready(function(){

});

(function() {
new Headroom(document.querySelector("#header"), {
tolerance: 10,
offset : 140,
classes: {
initial: "animated",
pinned: "slideDown",
unpinned: "slideUp"
}
}).init();
}());



      var map;
	  // Load coordinates
	  var myLatlng = new google.maps.LatLng(1.295585,103.858965);
	  // Initialize
      function initialize() {
        // Map Options
		var mapOptions = {
          zoom: 18,
          center: myLatlng,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        };
		// Load Map
        map = new google.maps.Map(document.getElementById('map_canvas'),mapOptions);
		// Marker
		var marker = new google.maps.Marker({
			position: myLatlng,
			map: map,
		});
		
		// Weather Layer
        var weatherLayer = new google.maps.weather.WeatherLayer({
          temperatureUnits: google.maps.weather.TemperatureUnit.CELCIUS
        });
        weatherLayer.setMap(map);
		// Cloud Layer
        var cloudLayer = new google.maps.weather.CloudLayer();
        cloudLayer.setMap(map);
	}
      google.maps.event.addDomListener(window, 'load', initialize);
