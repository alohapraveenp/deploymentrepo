(function(app) {
    app.controller('SectionBookingCtrl', SectionBookingCtrl);
    SectionBookingCtrl.$inject = ['$rootScope', '$scope', '$window', '$q', '$http'];

    function SectionBookingCtrl($rootScope, $scope, $window, $q, $http) {
        /**
         * Initial Section
         */
        $scope.sections = [{
            id: 1,
            name: 'Chef table'
        }, {
            id: 2,
            name: 'Counter seats'
        }, {
            id: 3,
            name: 'Bar seats'
        }];
        $scope.section = $scope.sections[0];
        $scope.sectionID = $scope.sections[0].id;

        /**
         * Initial Pax
         */
        $scope.paxs = [];
        for (i = 1; i <= 10; i++) {
            $scope.paxs.push(i);
        }
        $scope.pax = $scope.paxs[0];

        /**
         * Initial Type
         */
        $scope.types = [{
            id: 1,
            name: 'lunch',
            times: [
                '10:00',
                '10:15',
                '10:30',
                '11:00'
            ]
        }, {
            id: 2,
            name: 'dinner',
            times: [
                '16:00',
                '16:15',
                '17:00'
            ]
        }];
        $scope.type = $scope.types[0];
        $scope.time = $scope.type.times[0];

        $scope.selectedDate = moment().format('YYYY-MM-DD');
        $scope.checkDateAfterChangePax = angular.copy($scope.selectedDate);

        var params = {
            restaurant: $window.bkrestaurant,
            product: $scope.section.name,
            time: $scope.time,
            pers: $scope.pax
        };

        function getDayavailable(params) {
            var API_URL = '/api/restaurant/dayavailable/';
            var defferred = $q.defer();
            $http.post(API_URL, params, {
                cache: false,
            }).success(function(response) {
                generateAvaiableDatePicker(response);
            }).error(function(err) {
                console.log(err);
            });
            // API.restaurant.dayavailable(params)
            //     .then(function(result) {
            //         generateAvaiableDatePicker(result);
            //     })
            //     .catch(function(error) {
            //         console.log(error);
            //     });

        }
        getDayavailable(params);

        function generateAvaiableDatePicker(dayavailable) {
            var enabledDates = [];
            if (dayavailable.data !== undefined) {
                var daylength = dayavailable.data.length;
                var timeNow = 0;
                var day;
                for (var i = 0; i < daylength; i++) {
                    if (dayavailable.data.charAt(i) === '1') {
                        day = moment().add(timeNow, 'days').format('YYYY-MM-DD');
                        enabledDates.push(day);
                    }
                    timeNow++;
                }
            }
            $scope.enabledDates = _.map(enabledDates, function(date) {
                return moment(date);
            });
        }

        $scope.changeSectionRadio = function(section) {
            $scope.sectionID = section.id;
            params.product = section.name;
            getDayavailable(params);
        };

        $scope.changePax = function(pax) {
            $scope.pax = pax;
            params.pers = pax;
            getDayavailable(params);
        };

        $rootScope.$on('selectedDate', function(event, selectedDate) {
            $scope.selectedDate = selectedDate;
        });

        $scope.changeType = function(type) {
            $scope.type = type;
            $scope.time = type.times[0];
        };

        $scope.changeTime = function(time) {
            $scope.time = time;
            params.time = time;
            getDayavailable(params);
        };

        $scope.bookNow = function() {
            if ($scope.sectionID === undefined | $scope.sectionID === null) {
                Notification.show('warning', 'Please select section');
                return false;
            }
            if ($scope.selectedDate === undefined | $scope.selectedDate === null) {
                Notification.show('warning', 'Please select date');
                return false;
            }
            if ($scope.pax === undefined | $scope.pax === null) {
                Notification.show('warning', 'Please select pax');
                return false;
            }
            if ($scope.type === undefined | $scope.type === null) {
                Notification.show('warning', 'Please select type');
                return false;
            }
            if ($scope.time === undefined | $scope.time === null) {
                Notification.show('warning', 'Please select time');
                return false;
            }
        };
    }

    app.directive('datePicker', ['$rootScope', function($rootScope) {
        return {
            strict: 'AE',
            scope: {
                enabledDates: '=',
                selectedDate: '=',
            },
            link: function(scope, elem, attrs) {
                var options = {
                    locale: 'en',
                    format: 'DD MMM, YYYY',
                    useCurrent: true,
                    showTodayButton: false,
                    inline: true,
                    keepOpen: true,
                    focusOnShow: false
                };
                $(elem).datetimepicker(options);
                scope.$watch('enabledDates', function(enabledDates) {
                    if ($(elem).data('DateTimePicker') !== undefined) {
                        $(elem).data('DateTimePicker').enabledDates(enabledDates);
                        $(elem).on("dp.change", function(e) {
                            var DateSelected = moment(e.date._d).format('YYYY-MM-DD');
                            $rootScope.$broadcast('selectedDate', DateSelected);
                        });
                    }
                });
            }
        };
    }]);
    app.directive('materialSelect', function() {
        return {
            restrict: 'A',
            scope: {
                ngModel: '=',
            },
            link: function(scope, element, attrs) {
                $(element).material_select();
                scope.$watch('ngModel', function(ngModel) {
                    if (ngModel !== undefined) {
                        $(element).material_select();
                    }
                });
            }
        }
    });
})(angular.module('app', []));
