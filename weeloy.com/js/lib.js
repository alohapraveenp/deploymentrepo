function randstr(length, special){
	$randstr = "";
	$specialchars = new Array('&', '#', '$', '%', '@');
	//alert(special);
	for($i = 0; $i < length; $i++){	
		$randnum = Math.floor(Math.random()*61);
		if($randnum < 10){		
			$randstr = $randstr + String.fromCharCode($randnum+48);			
		}else if($randnum < 36){		
			$randstr = $randstr + String.fromCharCode($randnum+55);			
		}else if(special > 0){
			var tmp = Math.floor(Math.random() * $specialchars.length);
			$randstr = $randstr + $specialchars[tmp];
		}else{		
			$randstr = $randstr + String.fromCharCode($randnum+61);			
		}
	}
	
	if(special > 0){
		return $randstr;
	}else{
		return $randstr.toLowerCase();	
	}
};

//Finds the position of the element
function findelpos(ele){
	var curleft = 0;
	var curtop = 0;
	if(ele.offsetParent){
		while(1){
			curleft += ele.offsetLeft;
			curtop += ele.offsetTop;
			if(!ele.offsetParent){
				break;
			}
			ele = ele.offsetParent;
		}
	}else if(ele.x){
		curleft += ele.x;
		curtop += ele.y;
	}
	return [curleft,curtop];
};

function getAttributeByName(node, attribute){
	if(typeof NamedNodeMap != "undefined"){
		if(node.attributes.getNamedItem(attribute)){
			return node.attributes.getNamedItem(attribute).value;
		}
	}else{
		return node.getAttribute(attribute);
	}
};




function randomstring(L){
    var str= "";
    var randomchar=function(){
    	var n= Math.floor(Math.random()*62);
    	if(n<10) return n; //1-10
    	if(n<36) return String.fromCharCode(n+55); //A-Z
    	return String.fromCharCode(n+61); //a-z
    }
    while(str.length< L) str+= randomchar();
    return str;
}


///////////////////////////////
////// Password strength meter
///////////////////////////////
function passwordStrength(password1) {
	
	var shortPass = 1, badPass = 2, goodPass = 3, strongPass = 4, mismatch = 5, symbolSize = 0, natLog, score = 0;
	var pass_strength = Array();
	//password < 4
	if ( password1.length < 4 ){
		score = 9;
		pass_strength = [shortPass, parseInt(score)];
		return pass_strength;
	}

	if ( password1.match(/[0-9]/) )
		symbolSize +=10;
	if ( password1.match(/[a-z]/) )
		symbolSize +=26;
	if ( password1.match(/[A-Z]/) )
		symbolSize +=26;
	if ( password1.match(/[^a-zA-Z0-9]/) )
		symbolSize +=31;

	natLog = Math.log( Math.pow(symbolSize, password1.length) );
	score = natLog / Math.LN2;

	if (score < 40 ){
		pass_strength = [badPass, parseInt(score)];
		return pass_strength;
	}
		
	if (score < 56 ){
		pass_strength = [goodPass, parseInt(score)];
		return pass_strength;
	}
	
	pass_strength = [strongPass, parseInt(score)];
	return pass_strength;
}

function check_pass_strength() {
	
	var pass1 = $("#admin_pass").val();
	var strength = Array();
	
	//alert(pass1);
	$("#pass-strength-result").removeClass("short bad good strong");
	
	if (!pass1) {
		display_pass_strength("strength_indicator");
		return;
	}
	
	try{
			
		strength = passwordStrength(pass1);
		
		if(strength[1] > 100) strength[1] = 100;
		
		$("#pass-strength-hidden").val(strength[1]);
		
		switch ( strength[0] ) {
			case 2:
				score  = "bad";// For Bad password 
				display_pass_strength(score, strength[1]);
				break;
			case 3:
				score = "good"; // For Good password 
				display_pass_strength(score, strength[1]);
				break;
			case 4:
				score = "strong";// For Strong password 
				display_pass_strength(score, strength[1]);
				break;
			default:
				score = "short";// For Bad password 
				display_pass_strength(score, strength[1]);
		}
		
	}catch(e){
	}
}


var goto_top_type = -1;
var goto_top_itv = 0;
var last_scrolltop = 0;

function goto_top_timer() {
	var y = goto_top_type == 1 ? document.documentElement.scrollTop : document.body.scrollTop;
	//alert(y+' - '+last_scrolltop);
	
	if(y == last_scrolltop){	
		var moveby = 10; // set this to control scroll seed. minimum is fast
		
		y -= Math.ceil(y * moveby / 100);
		if (y < 0)
		y = 0;
		
		if (goto_top_type == 1)
		document.documentElement.scrollTop = y;
		else
		document.body.scrollTop = y;
		
		last_scrolltop = y;
	}else{
		y = 0;	
	}
	
	if (y == 0) {
	clearInterval(goto_top_itv);
	goto_top_itv = 0;
	}
}

function goto_top() {
	if (goto_top_itv == 0) {
		if (document.documentElement && document.documentElement.scrollTop)
		goto_top_type = 1;
		else if (document.body && document.body.scrollTop)
		goto_top_type = 2;
		else
		goto_top_type = 0;
		
		if (goto_top_type > 0){
			last_scrolltop = goto_top_type == 1 ? document.documentElement.scrollTop : document.body.scrollTop;
			goto_top_itv = setInterval('goto_top_timer()', 25);
		}
	}
}

//Trims a string
function trim(str){
	return str.replace(/^[\s]+|[\s]+$/, "");
};

//Give a random integer
function AEFrand(min, max){
	return Math.floor(Math.random() * (max - min + 1) + min);
};

//Checks the entire range of checkboxes
function check(field, checker){
	if(checker.checked == true){
		for(i = 0; i < field.length; i++){
			field[i].checked = true;
		}
	}else{
		for(i = 0; i < field.length; i++){  
			field[i].checked = false;
		}
	}
};
//The page width
function getwidth(){
	return document.body.clientWidth;
};
//The page height
function getheight(){
	return document.body.clientHeight;
};

//Get the scrolled height
function scrolledy(){
	//Netscape compliant
	if(typeof(window.pageYOffset) == 'number'){
		return window.pageYOffset;
	//DOM compliant
	}else if(document.body && document.body.scrollTop){
		return document.body.scrollTop;
	//IE6 standards compliant mode
	}else if(document.documentElement && typeof(document.documentElement.scrollTop)!='undefined'){
		return document.documentElement.scrollTop;
	}else{
		return 0;	
	}
}

