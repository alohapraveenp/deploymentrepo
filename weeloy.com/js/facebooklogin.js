submitflg = false;
function getUserData() {
	FB.api('/me', function(response) {
		$('#status_fb').val('Hello ' + response.name);
		if(response.name != "") {
			console.log(response);
			$('#fbuserid').val(response.id);
			$('#fbemail').val(response.email);
			$('#fbname').val(response.name);
			$('#fbfirstname').val(response.first_name);
			$('#fblastname').val(response.last_name);
			$('#fbtimezone').val(response.timezone);
			$('#application').val('facebook');
			}
		if($('#fbuserid').val() != '' && submitflg) {
			updateinput();
			$('#login_form').submit(); 
			}
	});
}
 
window.fbAsyncInit = function() {
	//SDK loaded, initialize it
	FB.init({
		appId      : '1590587811210971',
		xfbml      : true,
		version    : 'v2.2'
	});
 
	//check user session and refresh it
	FB.getLoginStatus(function(response) {
		if (response.status === 'connected') {
			//user is authorized
			//document.getElementById('loginBtn').style.display = 'none';
			if(typeof response.authResponse !== 'undefined')
				$('#fbtoken').val(response.authResponse.accessToken);

			submitflg = false;
			getUserData();
		} else {
			//user is not authorized
		}
	});
};
 
//load the JavaScript SDK
(function(d, s, id){
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) {return;}
	js = d.createElement(s); js.id = id;
	js.src = "//connect.facebook.net/en_US/sdk.js";
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
 
//add event listener to login button
document.getElementById('loginBtn').addEventListener('click', function() {
	//do the login
	FB.login(function(response) {
		if (response.authResponse) {
			//user just authorized your app
			//document.getElementById('loginBtn').style.display = 'none';
			if(typeof response.authResponse !== 'undefined')
				$('#fbtoken').val(response.authResponse.accessToken);

			if($('#fbtoken').val().length > 10 && $('#type_action').val() == "Login")
				submitflg = true;
			getUserData();
			if($('#fbtoken').val().length > 10 && $('#type_action').val() == "Register")
				updateinput();
		}
	}, {scope: 'email,public_profile', return_scopes: true});
}, false);

