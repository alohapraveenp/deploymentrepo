/* dayroutine 1.1 (c) 2015 by richard kefs. All right reserved */

function AdayRoutine(todayphp) {
	var dateAr, date, ctime, cweek, mweek, clweek, dd;
	
	dateAr = todayphp.split('-');
	date = new Date(dateAr[0], parseInt(dateAr[1]) - 1, dateAr[2], 0, 0, 0);
	ctime = date.getTime();
	dd = date.getDay();
	cweek = ctime - (dd * 24 * 60 * 60 * 1000);
	mweek = cweek + (7 * 24 * 60 * 60 * 1000);
	clweek = cweek - (7 * 24 * 60 * 60 * 1000);
	
	return  {
		dateAr: [],
		date: null,
		now: Date.now(), 
		ctime: ctime,
		cweek: cweek,
		mweek: mweek,
		clweek: clweek,
		tmp: 0,
		
		getDate: function(aDate, h, m, s, sep) {
			this.dateAr = aDate.split(sep);
			return new Date(this.dateAr[0], parseInt(this.dateAr[1]) - 1, this.dateAr[2], h, m, s);
			},
			
		toDays: function(aDate) {
			this.date = this.getDate(aDate, 0, 0, 0, '-');
			return ((this.date.getTime() - this.ctime) / (24 * 60 * 60 * 1000));
			},
	
		toWeek: function(aDate) {
			this.date = this.getDate(aDate, 0, 0, 0, '-');
			this.tmp = this.date.getTime();
			return (this.tmp >= this.cweek && this.tmp < this.mweek) ? 1 : -1;
			},
	
		tolWeek: function(aDate) {
			this.date = this.getDate(aDate, 0, 0, 0, '-');
			this.tmp = this.date.getTime();
			return (this.tmp >= this.clweek && this.tmp < this.cweek) ? 1 : -1;
			},
	
		dayofweek: function(aDate) {
			this.date = this.getDate(aDate, 0, 0, 0, '-');
			return this.date.getDay();
			},
	
		numberDay: function(aDate) {
			this.date = this.getDate(aDate, 23, 59, 59, '/');
			return Math.floor(((this.date.getTime() - this.now) / (24 * 60 * 60 * 1000)));
			}
	};
}
