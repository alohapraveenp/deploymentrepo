var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;

app.service('FormControl',[function(){
	
	var minpasslen = 7;
	var minphonelen = 8;
	var minnamelen = 4;
	this.checkLogin = function(data) {
	
		data.result = -1;
		data.msg = "";
		data = this.checkData(data, 'email', 'email');
		if(data.result < 0) {
			data.msg = "Please enter a valid email " + data.email;
			return data;
			}
		data = this.checkData(data, 'password', 'password');
		if(data.result < 0)
			return data;

		data.result = 1;
		return data;
 		}

	this.checkChange = function(data) {
	
		data.result = -1;
		data.msg = "";
		data = this.checkLogin(data);
		if(data.result < 0) {
			return data;
			}
		
	// check npassword and rpassword	
		data = this.checkData(data, 'npassword', 'password');
		if(data.result < 0) {
			if(data.msg == "")
				data.msg = "Please enter a valid Password (at least " + minpasslen + " chars)";
			return data;
			}
	// check npassword and rpassword	
		data = this.checkData(data, 'rpassword', 'password');
		if(data.result < 0) {
			if(data.msg == "")
				data.msg = "Please enter a valid Password (at least " + minpasslen + " chars)";
			return data;
			}

		if(data.npassword !== data.rpassword) {
			data.msg = "New password is different from the retype password";
			return data;
			}
			
		data.result = 1;
		return data;
 		}

	this.checkRegister = function(data) {
	
		data.result = -1;
		data.msg = "";
		data = this.checkLogin(data);
		if(data.result < 0) {
			return data;
			}
		
	// check npassword and rpassword	
		data = this.checkData(data, 'rpassword', 'password');
		if(data.result < 0) {
			if(data.msg == "")
				data.msg = "Please enter a valid Password (at least " + minpasslen + " chars)";
			return data;
			}
			
		if(data.password !== data.rpassword) {
			data.msg = "New password is different from the retype password";
			return data;
			}
		
		data = this.checkData(data, 'lastname', 'text');
		if(data.result < 0) {
			if(data.msg == "")
				data.msg = "Please enter a valid name";
			return data;
			}
			
		data = this.checkData(data, 'firstname', 'text');
		if(data.result < 0) {
			if(data.msg == "")
				data.msg = "Please enter a valid firstname";
			return data;
			}
			
		data = this.checkData(data, 'mobile', 'mobile');
		if(data.result < 0) {
			if(data.msg == "")
				data.msg = "Please enter a valid phone number";
			return data;
			}
			
		data.result = 1;
		return data;
 		}

	this.checkForgot = function(data) {

		data.result = -1;
		data.msg = "";
		data = this.checkData(data, 'email', 'email');
		if(data.result < 0) {
			data.msg = "Please enter a valid email";
			return data;
			}
		data.result = 1;
		return data;		
		}

	this.checkData = function(data, passlabel, type) {
		data.result = -1;
		
		if(typeof data[passlabel] === "undefined" || data[passlabel] === null || data[passlabel] === "") {
			data[passlabel] = "";
			data.msg = passlabel + " is empty" ;
			return data;
			}
			
		switch(type) {
			case 'email':
				data[passlabel] = data[passlabel].replace(/[!#$%^&*()=}{\]\[:;><\?/|\\]/g, '');
				if(data[passlabel].length > 0)
					data[passlabel] = data[passlabel].replace(/[\'\"]/g, '');

				if(data[passlabel].length <= 0)
					return;
			
				if(regex.test(data[passlabel]) == false) 
					return data;
				break;
				
			case 'password':
				data[passlabel] = data[passlabel].replace(/[()\'\"]/g, '');
				data[passlabel] = data[passlabel].trim();
				if(data[passlabel].length < minpasslen) {
					data.msg = "password is at least " + minpasslen + " chars" ;
					return data;
					}
				break;
		
			case 'text':
				data[passlabel] = data[passlabel].replace(/[!@#$%^&*()=}{\]\[\"\':;><\?/|\\]/g, '');;
				data[passlabel] = data[passlabel].trim();
				if(data[passlabel].length < minnamelen) {
					data.msg = passlabel + " is at least " + minnamelen + " chars" ;
					return data;
					}
				break;
		
			case 'mobile':
				data[passlabel] = data[passlabel].replace(/[^0-9 \+]/g, '');
				data[passlabel] = data[passlabel].trim();
				if(data[passlabel].length < minphonelen) {
					data.msg = passlabel + " is at least " + minphonelen + " digits" ;
					return data;
					}
				break;
		
			default:
				data.msg = passlabel + ": internal error" ;
				return data;
			}
		data.result = 1;
		return data;
		}
}])