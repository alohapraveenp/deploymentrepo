app.service('tradService',['$http',function($http){

	this.translate = function(langue, topic, zang) {
		return $http.post("api/services.php/translation/gettrans/", { 'language': langue, 'topic' : topic }).then(function (response) {
			trans = response.data.data.row;
			for (ele in zang) {
				if (typeof zang[ele].lb == 'undefined' || zang[ele].lb == "")
					continue;
				str = zang[ele].lb.toLowerCase();
				for (i = 0; i < trans.length; i++) {
					if (typeof trans[i].content == 'undefined' || trans[i].content == "")
						continue;
					if (trans[i].content.toLowerCase() == str) {
						cc = trans[i].translated;
						if (zang[ele].f == 'c')
							zang[ele].vl = cc.capitalize();
						else if (zang[ele].f == 'u')
							zang[ele].vl = cc.toUpperCase();
						else zang[ele].vl = cc;
						break
					}
				}
			}
		});
	};

}]);
