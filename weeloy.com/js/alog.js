//duration in minutes

function resetcookie(name, value, duration){
	removecookie(name);
	setcookie(name, value, duration);
}

function setcookie(name, value, duration){
	var d = new Date();
	value = escape(value);
	if(duration) {
		d.setTime(d.getTime() + (duration * 60000));
		value += "; expires=" + d.toUTCString();
		}
	document.cookie = name + "=" + value;
}

//Gets the cookie value
function getcookie(name){
	value = document.cookie.match('(?:^|;)\\s*'+name+'=([^;]*)');
	return value ? unescape(value[1]) : "";
}

//Removes the cookies
function removecookie(name){
  	document.cookie = name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
  	document.cookie = name + '=; Max-Age=0; path=/';
}

function getfieldvalue(pat) {

	if(pat == "")
		return "";

	field = getcookie(cookiename);
	if(field == "")
		return "";
	uu = field.substr(field.indexOf(pat)+pat.length);
	vv = uu.split(":");
	return vv[0];
}


function getloginname() {

	return name = getfieldvalue(":firstname=");
}

function getcookievalues(name, pat) {
	field = getcookie(name);
	if(field == "")
		return "";
	
	vv = field.split(":");
	for(i = 0; i < vv.length; i++) {
		if(vv[i].length < 1) continue;
		uu = vv[i].split("=");
		if(uu[0] == pat)
			return uu[1];
		}
	return "";
}

	
function update_session_time() {
	var today = new Date(), uu, vv, lhh, lmm, hh, mm, ss;

	uu = getfieldvalue(":expire=");
	if(uu == "") {
		return -20000;
		}
		
	vv = uu.split("-");
	lhh = parseInt(vv[0]);
	lmm = parseInt(vv[1]);
	ldd = parseInt(vv[2]);
	
	hh = today.getUTCHours();
	mm = today.getUTCMinutes();
	ss = today.getUTCSeconds();
	dd = today.getUTCDate();
	
	if(ldd !== dd) lhh += 24;
	sessiontime = ((((lhh * 60) + lmm) - ((hh * 60) + mm)) * 60) - ss;
	//console.log('SESSION', vv, ldd, dd, lhh + ":" + lmm, hh + ":" + mm + ":" + ss, sessiontime);
	return sessiontime;
}

var previous_counter = 100;
var loginTitleTormat = 'regular';

var windowBOlogin = "resetting";
var validLogintype = ['weeloy_loginbackoffice', 'weeloy_logintranslation', 'weeloy_loginadmin', 'weeloy_logintms'];

function count_down() {

	sessiontime = update_session_time();

	if(sessiontime <= 0) {
		if(loginTitleTormat == 'regular') {
			tt = "<h6><span class='glyphicon glyphicon-user' aria-hidden='true'>&nbsp;</span><strong> Please Login in</strong></h6>";
			tt += "<span class='glyphicon glyphicon-time' aria-hidden='true'></span> session time is <strong>over</strong></div>";
			} else {
			tt = "<br /><span class='glyphicon glyphicon-time' aria-hidden='true'></span> session time is <strong>over</strong></div>";			
			}
		$("#sessiontime").html(tt);	
		}

	if(typeof logstatus === 'string' && logstatus === "out") {
		return;
		}

	if(sessiontime <= 0 && sessiontime > -50000)
		if(previous_counter < 10 && windowBOlogin === "in") {
			windowBOlogin = "resetting";
			removecookie(cookiename);
			myStopFunction();
			window.location.reload(true);
			return;
			}

	if(sessiontime > 100 && validLogintype.indexOf(cookiename) >= 0) {
		windowBOlogin = "in";
		}

	if(sessiontime > 0) {
		hh = Math.floor(sessiontime / 3600);
		mm = Math.floor(sessiontime / 60) - (hh * 60);
		ss = sessiontime % 60;
		
		//define color
		color = (hh == 0 && mm < 5 && ss % 2 === 0) ? " style = 'color:red;' " : "";

		hh = (hh > 0) ? hh + 'h:' : '';
		mm = (mm < 10) ? '0' + mm : mm;
		ss = (ss < 10) ? '0' + ss : ss;
	
		if(loginTitleTormat == 'regular') {
			tt = "<h6><span class='glyphicon glyphicon-user' aria-hidden='true'>&nbsp;</span>  Welcome  " + "<strong>" + getloginname() + "</strong></h6>";
			tt += "<div " + color + " ><span class='glyphicon glyphicon-time' aria-hidden='true'></span> " + " session time <br />" + hh + mm + ":" + ss + "</div>";
			}
		else {
			tt = "<span>Welcome  <strong>" + getloginname() + "</strong></span>";
			tt += "<div " + color + " ><span class='glyphicon glyphicon-time' aria-hidden='true'></span> " + " session time <br />" + hh + mm + ":" + ss + "</div>";
			}
		$("#sessiontime").html(tt);
		}
	
	current_time_out = setTimeout(count_down, 1000);
	previous_counter = sessiontime;
	return sessiontime;
}

function myStopFunction() {
	if(typeof current_time_out !== "undefined")
	    clearTimeout(current_time_out);
}

function reStartFunction() {
	myStopFunction() ;
	count_down();
}

function setCookieOnline(cookiename, data, duration) {
	resetcookie(cookiename, data, duration); // ":firstname=" + firstname + ':expire=' + expire
	reStartFunction();
	}
	

$(document).ready(function() { sessiontime = count_down(); }); 

