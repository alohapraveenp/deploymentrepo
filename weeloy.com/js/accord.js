//<![CDATA[
//pre click
jQuery('.ol-typo-pre-title').each(function() {
jQuery(this).next().wrap('<div class="codediv" />');
var prewidth = jQuery(this).width();
if (prewidth < 500 ) { jQuery(this).css('background-position', '97% center'); }
}).click(function(){
var pretitle = jQuery(this), thiscode = jQuery(this).next('.codediv');
if (thiscode.is(':visible') && pretitle.hasClass('expanded')) {	thiscode.slideUp(500,'easeInBack', function() {	pretitle.removeClass('expanded'); pretitle.animate({marginBottom: '0.6em'}, 300); });
} else { pretitle.animate({marginBottom: 0}, 300, function() { pretitle.addClass('expanded');  thiscode.slideDown(700,'easeOutBack'); }); }
jQuery('.ol-typo-pre-title').each(function(){
if (jQuery(this).hasClass('expanded') && jQuery(this).next().is(':visible')) { jQuery(this).next().slideUp(700,'easeInBack', function() { jQuery(this).prev().animate({marginBottom: '0.6em'}, 300); jQuery(this).prev().removeClass('expanded'); }); } 
});
});
//]]>