// Richard Kefs, Weeloy, (c) copyright, All Rights Reserved

function IsEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}



function CleanEmail(obj) { obj.value = obj.value.replace(/[!#$%^&*()=}{\]\[:;><\?/|\\]/g, ''); }
function CleanTel(obj) { obj.value = obj.value.replace(/[^0-9 \+]/g, ''); }
function CleanPass(obj) { obj.value = obj.value.replace(/[^0-9A-Za-z]/g, ''); }
function CleanText(obj) { obj.value = obj.value.replace(/[!@#$%^&*()=}{\]\[\"\':;><\?/|\\]/g, ''); }
function invalid(str) { alert(str); return false; }
function checkSubmit() {

	action = $('#type_action').val();
    $('#login_form input[type=text], input[type=password]').each( function() { 
    	this.value = this.value.trim();
		});
		
	email = $('#email').val();
	if(action == "Login") {
		if(email == "" || !IsEmail(email))
			return invalid('Invalid email, try again');
		if($('#password').val().length < 6)
			return invalid('Invalid password, try again');
		}

	else if(action == "LostPassword") {
		if(email == "" || !IsEmail(email))
			{
			return invalid('Invalid email, try again');
			}
		}
			
	else if(action == "Register") {
		if($('#r_email').val() == "" || !IsEmail($('#r_email').val()))
			return invalid('Invalid email, try again');
		if($('#r_lastname').val() == "")
			return invalid('Invalid name, try again');
		if($('#r_firstname').val() == "")
			return invalid('Invalid firstname, try again');
		if($('#r_mobile').val() == "")
			return invalid('Invalid tel, try again');
		if($('#r_country').val() == "")
			return invalid('Invalid country, try again');
		if($('#r_password').val() == "")
			return invalid('Invalid password, try again');
		if($('#r_rpassword').val() == "" || $('#r_password').val() != $('#r_retypepassword').val())
			return invalid('password and retype password are different, try again');
		}
	return true;
}
