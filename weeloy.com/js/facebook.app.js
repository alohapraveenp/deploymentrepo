

function CleanEmail(obj) { obj.value = obj.value.replace(/[!#$%^&*()=}{\]\[:;><\?/|\\]/g, ''); }
function CleanTel(obj) { obj.value = obj.value.replace(/[^0-9 \+]/g, ''); }
function CleanPass(obj) { obj.value = obj.value.replace(/[^0-9A-Za-z]/g, ''); }
function CleanText(obj) { obj.value = obj.value.replace(/[!@#$%^&*()=}{\]\[\"\':;><\?/|\\]/g, ' '); }
function invalid(id, str) { alert(str); $(id).focus(); }
function checkSubmit() {

	$('#book_form input[type=text], input[type=password]').each(function() {
		this.value = this.value.trim();
		});
	if ($('#tmp_bkdate').val() == "")
		return invalid('#tmp_date', 'Invalid date, try again');
	if ($('#bktime').val() == "")
		return invalid('#bktime', 'Invalid time, try again');
	if ($('#bktime').val().substr(0,6) == "closed")
		return invalid('#bktime', 'The restaurant is ' + $('#bktime').val() +', try another time/day');
	if ($('#bkemail').val() == "" || !IsEmail($('#bkemail').val()))
		return invalid('#bkemail', 'Invalid email, try again');
	if ($('#bkfirst').val() == "")
		return invalid('#bkfirst', 'Invalid firstname, try again');
	if ($('#bklast').val() == "")
		return invalid('#bklast', 'Invalid name, try again');
	if ($('#bkmobile').val().length < 5)
		return invalid('#bkmobile', 'Invalid mobile number, try again');
	if ($('#bkchtc').is(':checked') == false)
		return invalid('#bkchtc', 'You need to approve the terms and conditions in order to reserve a table');
	$('#bkphone').val($('#bkmobile').val());

	if (fullfeature) {
		$('#book_form').attr('action', 'prebook.php'); book_form.submit();
	} else {
		var current_date = new Date(); 
		var current_year = current_date.getFullYear();
		var current_month = current_date.getMonth();
		var lastDayOfMonth =  daysInMonth(current_month,current_year);

		alert('Reservation will be open on '+months[current_month]+' '+lastDayOfMonth+', '+current_year);
	}
}

function daysInMonth(iMonth, iYear) {
    return 32 - new Date(iYear, iMonth, 32).getDate();
}