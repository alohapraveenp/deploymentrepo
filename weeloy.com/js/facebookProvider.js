var app = angular.module('FacebookProvider', []);

app.factory('Facebook', ['$rootScope', function ($rootScope) {
    return {
        getLoginStatus:function () {
            FB.getLoginStatus(function (response) {
                $rootScope.$broadcast("fb_statusChange", {'status':response.status});
            }, true);
        },
        login:function () {
            FB.getLoginStatus(function (response) {
                switch (response.status) {
                    case 'connected':
                    	console.log('connected');
                    	console.log(response);
                        $rootScope.$broadcast('fb_connected', {facebook_id:response.authResponse.userID, facebook_token:response.authResponse.accessToken});
                        break;
                    case 'not_authorized':
                    case 'unknown':
                        FB.login(function (response) {
							console.log(response.status);
							console.log(response);
                            if (response.authResponse) {
                                $rootScope.$broadcast('fb_connected', {
                                    facebook_id:response.authResponse.userID,
                                    facebook_token:response.authResponse.accessToken,
                                    userNotAuthorized:true
                                });
                            } else {
                                $rootScope.$broadcast('fb_login_failed');
                            }
                        }, {scope: 'email,public_profile', return_scopes: true});
                        break;
                    default:
                        FB.login(function (response) {
                            if (response.authResponse) {
								console.log('default');
								console.log(response);
                               $rootScope.$broadcast('fb_connected', {facebook_id:response.authResponse.userID, facebook_token:response.authResponse.accessToken});
                                $rootScope.$broadcast('fb_get_login_status');
                            } else {
                                $rootScope.$broadcast('fb_login_failed');
                            }
                        });
                        break;
                }
            }, true);
        },
        logout:function () {
            FB.logout(function (response) {
                if (response) {
                    $rootScope.$broadcast('fb_logout_succeded');
                } else {
                    $rootScope.$broadcast('fb_logout_failed');
                }
            });
        },
        unsubscribe:function () {
            FB.api("/me/permissions", "DELETE", function (response) {
                $rootScope.$broadcast('fb_get_login_status');
            });
        }
    };
}]);

