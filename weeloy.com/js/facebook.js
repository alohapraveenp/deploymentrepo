var gStatus = 'not connected';

// This is called with the results from from FB.getLoginStatus().
function statusChangeCallback(response) {
console.log('statusChangeCallback');
console.log(response);
// The response object is returned with a status field that lets the
// app know the current login status of the person.
// Full docs on the response object can be found in the documentation
// for FB.getLoginStatus().
if (response.status === 'connected') {
  // Logged into your app and Facebook.
  gStatus = 'connected';
  getInfoAPI(response.authResponse.accessToken);
} else if (response.status === 'not_authorized') {
  // The person is logged into Facebook, but not your app.
  document.getElementById('status').innerHTML = 'Please log ' +
	'into this app.';
} else {
  // The person is not logged into Facebook, so we're not sure if
  // they are logged into this app or not.
  document.getElementById('status').innerHTML = 'Please log ' +
	'into Facebook.';
}
}

// This function is called when someone finishes with the Login
// Button.  See the onlogin handler attached to it in the sample
// code below.
function checkLoginState() {
FB.getLoginStatus(function(response) {
  statusChangeCallback(response);
});
}

window.fbAsyncInit = function() {
FB.init({
appId      : '1590587811210971',
status     : false,
cookie     : true,  // enable cookies to allow the server to access 
					// the session
xfbml      : true,  // parse social plugins on this page
version    : 'v2.0' // use version 2.0
});

// Now that we've initialized the JavaScript SDK, we call 
// FB.getLoginStatus().  This function gets the state of the
// person visiting this page and can return one of three states to
// the callback you provide.  They can be:
//
// 1. Logged into your app ('connected')
// 2. Logged into Facebook, but not your app ('not_authorized')
// 3. Not logged into Facebook and can't tell if they are logged into
//    your app or not.
//
// These three cases are handled in the callback function.

FB.getLoginStatus(function(response) {
reset();
statusChangeCallback(response);
});

};

// Load the SDK asynchronously
(function(d, s, id) {
var js, fjs = d.getElementsByTagName(s)[0];
if (d.getElementById(id)) return;
js = d.createElement(s); js.id = id;
js.src = '//connect.facebook.net/en_US/sdk.js';
fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

// Here we run a very simple test of the Graph API after login is
// successful.  See statusChangeCallback() for when this call is made.
function getInfoAPI(token) {
$('#token').val(token);
//console.log(token + '\nWelcome!  Fetching your information.... ');
FB.api('/me', function(response) {
	$('#userid').val(response.id);
	$('#email').val(response.email);
	$('#userfirstname').val(response.first_name);
	$('#userlastname').val(response.last_name);
			$('#username').val(response.name);
	$('#timezone').val(response.timezone);
	$('#application').val('facebook');
  console.log('userid = ' + $('#userid').val());
  console.log('gStatus = '+gStatus);
  console.log(JSON.stringify(response));
  //console.log('Successful login for: ' + response.name);
  //document.getElementById('status').innerHTML = 'Thanks for logging in, ' + response.name + '!';
  if(gStatus = 'connected' && response.verified == true && $('#userid').val() != '') 
	$('#idForm').submit();
});
}

function reset() {
	 FB.api(
	"/me/permissions",
	"DELETE",
	function (response) {
	  if (response && !response.error) {
			/* handle the result */
			}
	  }
	);
}

