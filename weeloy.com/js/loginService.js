app.service('loginService',['$http',function($http){

	this.login = function(email, passw, platform, application) {
		if(typeof platform !== 'string' || platform === '') platform = 'translation';
		if(typeof application !== 'string') application = '';
		return $http.post("../api/services.php/rmloginc/module/login/", { 'email': email, 'passw' : passw, 'platform' : platform, 'application' : application })
				.then(function(response) { return response.data; })
	};

	this.change = function(email, passw, npassw, token){
		return $http.post("../api/services.php/rmloginc/module/change/",{'email':email, 'password':passw, 'npassword':npassw, 'token': token})
        		.then(function(response) {return response.data;});
	};

	this.setapp = function(app, email, token){
		return $http.post("../api/services.php/rmloginc/module/loginsetapp/",{'application':app, 'email':email, 'token': token})
        		.then(function(response) {return response.data;});
	};

	this.forgot = function(email, redirect_to){
		return $http.post("../api/services.php/rmloginc/module/forgot/",{'email':email, 'redirect_to':redirect_to })
        		.then(function(response) {return response.data;});
	};

	this.logout = function(email, token){
		return $http.post("../api/services.php/rmloginc/module/logout/",{'email': email, 'token': token })
        		.then(function(response) {return response.data;})
	};

	this.register = function(name, email, passw){
		return $http.post("../api/services.php/rmloginc/module/register/",{'name':name, 'email':email, 'password':passw})
    			.then(function(response) {return response.data;});
	};

	this.checkstatus = function(email, token){
		return $http.post("../api/services.php/rmloginc/module/checkstatus/",{ 'email':email, 'token':token})
    			.then(function(response) {return response.data;});
	};

	this.loginfacebook = function(params){
		if(typeof params.platform !== 'string') params.platform = 'translation';
		return $http.post("../api/services.php/rmloginc/module/loginfacebook/", params)
    			.then(function(response) {return response.data;});
	};

}]);

app.factory('DataService', [ '$http', 'URL', function ($http, URL) {
    var getData = function () {
        return $http.get(URL + 'content.json');
    };

    return {
        getData: getData
    };
}]);

