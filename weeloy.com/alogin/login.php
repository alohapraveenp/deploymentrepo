<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
<title>Weeloy - translation</title>
<link rel="icon" href="/favicon.ico" type="image/gif" sizes="16x16">
<link href="../css/login.css" rel="stylesheet" type="text/css">
<link href="../css/modal.css" rel="stylesheet" type="text/css">


<link href="../client/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="../client/bower_components/bootstrap/dist/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
<link href="../client/bower_components/jquery-ui/themes/base/jquery-ui.css" rel="stylesheet" type="text/css">
<link href="../client/bower_components/fontawesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="../css/bootstrap-social.css" rel="stylesheet" type="text/css"/>
<link href="../css/login-modal.css" rel="stylesheet" type="text/css"/>

<link href='https://fonts.googleapis.com/css?family=Dosis:400,600,700bold|Roboto:400,900,700italic,700,500italic,500,400italic' rel='stylesheet' type='text/css'>

<script type='text/javascript' src="../client/bower_components/jquery/dist/jquery.min.js"></script>
<script type='text/javascript' src="../client/bower_components/jquery-ui/jquery-ui.js"></script>
<script type='text/javascript' src="../client/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script type='text/javascript' src="../client/bower_components/angular/angular.min.js"></script>
<script type='text/javascript' src="../client/bower_components/angular/angular-animate.min.js"></script>
<script type="text/javascript" src="../js/ui-bootstrap-tpls-0.14.2.min.js"></script>
<script type="text/javascript" src="../js/facebookProvider.js"></script>
<script type="text/javascript" src="../js/facebookRun.js"></script>
<script type="text/javascript" src="../js/loginService.js"></script>
<script type="text/javascript" src="../js/ngStorage.min.js"></script>
<script type="text/javascript" src="../js/formControl.js"></script>
<script type="text/javascript" src="../backoffice/inc/libService.js"></script>
<script type="text/javascript" src="../backoffice/inc/backofficelib.js"></script>
<script type="text/javascript" src="../js/alog.js"></script>

<style> 
.headertitle { font-size:14px; font-weight:bold; color:white; font-family:helvetica;}
.headersubtitle { font-size:10px; color:white; font-weight:bold; font-family:helvetica; padding-left:10px; }
.mwhite { color:white; }
.leftm { padding-bottom:20px; color:black; }
	
</style>

<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<script src="js/selectivizr-min.js"></script>
<script src="js/modernizr.js"></script>
<link rel="stylesheet" type="text/css" href="css/ie.css" media="screen" />
<![endif]-->
</head>
<script>

var cookiename = 'translation';
var showme = true;

</script>

<script>var app = angular.module('loginpage', ['ui.bootstrap', 'FacebookProvider', 'ngStorage']); </script>

<body ng-app="loginpage" ng-controller="loginController" ng-init="moduleName='login';">

<div >
<div ng-show='showinfo'>
<a href ng-click="login()"><span class="glyphicon glyphicon-log-in"></span> login </a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<a href ng-click="logout()"><span class="glyphicon glyphicon-log-out"></span> logout </a><br/><br/>
<span id="sessiontime" class="small"></span>
</div>
    
<div id="fb-root"></div>
</div>


<script type='text/javascript' src="loginController.js"></script>
<script type="text/ng-template" id="loginBackoffice.html">
	<content-item ng-repeat="item in interfaceLogin" content="item" myTemplates="templateData"></content-item>
</script>

<script type="text/ng-template" id="forgotBackoffice.html">
	<content-item ng-repeat="item in interfaceForgot" content="item" myTemplates="templateData"></content-item>
</script>

<script type="text/ng-template" id="chgpassBackoffice.html">
	<content-item ng-repeat="item in interfaceChange" content="item" myTemplates="templateData"></content-item>
</script>

<script type="text/ng-template" id="registerBackoffice.html"> 
	<content-item ng-repeat="item in interfaceRegister" content="item" myTemplates="templateData"></content-item>
</script>

</body>
</html>
