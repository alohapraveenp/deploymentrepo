/*
 *  ┌─────────────────────────────────────────────┐ 
 *  │ TMS 1.0 - JavaScript Vector Library             │ 
 *  ├─────────────────────────────────────────────┤ 
 *  │ Copyright © 2015 Weeloy. All rights reserved.   │ 
 *  ├─────────────────────────────────────────────┤ 
 *  │ Company: https://www.weeloy.com				  │ 
 *  ├─────────────────────────────────────────────┤ 
 *  │ Author Richard Kefs							  │ 
 *  └─────────────────────────────────────────────┘ 
 *
*/

token = '';
localcookie = 'weeloy_trad';

app.constant('URL', ['data/']);

app.service('interfaceModel', ['$http', 'URL', function ($http, URL) {

	this.translateblank = [ 
				{"content_type" : "blank", "title":"" } 
				];

	this.login = [
				{"content_type" : "title", "title":"Login" },
				{"content_type" : "header1", "title":"Login", "title1":"Forgot pass?", "arg1" : "forgot", "title2": "Change pass", "arg2" : "change" },
				{"content_type" : "input", "type":"text", "variable": "email", "title" : "Email", "topmargin" : "0", "glyphicon":"envelope", "id":"email" },
				{"content_type" : "password", "type":"password", "variable": "password", "title" : "Password", "topmargin" : "5px", "glyphicon":"lock", "id":"password" },
				{"content_type" : "showpass", "type":"checkbox", "variable": "showp", "title" : "Show password", "topmargin" : "5px", "glyphicon":"", "id":"showp" },
				{"content_type" : "submit", "type":"submit", "variable": "login", "title" : "Login", "topmargin" : "5px", "glyphicon":"", "id":"login" },
				{"content_type" : "facebookfooter", "type":"submit", "variable": "login", "title" : "Continue with Facebook", "topmargin" : "5px", "glyphicon":"", "id":"facebooklogin" }
				];

	this.change = [
				{"content_type" : "title", "title":"Change Password" },
				{"content_type" : "header1", "title":"Change password", "title1":"Login", "arg1" : "login", "title2":"Forgot pass?", "arg2" : "forgot",  },
				{"content_type" : "input", "type":"text", "variable": "email", "title" : "Email", "topmargin" : "0", "glyphicon":"envelope", "id":"email" },
				{"content_type" : "password", "type":"password", "variable": "password", "title" : "Password", "topmargin" : "5px", "glyphicon":"lock", "id":"password" },
				{"content_type" : "password", "type":"password", "variable": "npassword", "title" : "New Password", "topmargin" : "5px", "glyphicon":"lock", "id":"npassword" },
				{"content_type" : "password", "type":"password", "variable": "rpassword", "title" : "Retype password", "topmargin" : "5px", "glyphicon":"lock", "id":"rpassword" },
				{"content_type" : "showpass", "type":"checkbox", "variable": "showp", "title" : "Show password", "topmargin" : "5px", "glyphicon":"", "id":"showp" },
				{"content_type" : "submitfooter", "type":"submit", "variable": "change", "title" : "Change your Password", "topmargin" : "5px", "glyphicon":"", "id":"change" }
				];

	this.forgot = [
				{"content_type" : "title", "title":"Forgot Password" },
				{"content_type" : "header1", "title":"Forgot Password", "title1":"Login", "arg1" : "login", "title2": "Change password", "arg2" : "change" },
				{"content_type" : "input", "type":"text", "variable": "email", "title" : "Email", "topmargin" : "0", "glyphicon":"envelope", "id":"email" },
				{"content_type" : "submitfooter", "type":"submit", "variable": "forgot", "title" : "Retrieve", "topmargin" : "10px", "glyphicon":"", "id":"forgot" }
				];

	this.register = [
				{"content_type" : "title", "title":"Register" },
				{"content_type" : "header1", "title":"Register", "title1":"Login", "arg1" : "login", "title2": "Forgot pass?", "arg2" : "forgot", "title3": "Change pass", "arg3" : "change" },
				{"content_type" : "input", "type":"text", "variable": "email", "title" : "Email", "topmargin" : "0", "glyphicon":"envelope", "id":"email" },
				{"content_type" : "input", "type":"text", "variable": "firstname", "title" : "First Name", "topmargin" : "5px", "glyphicon":"user", "id":"firstname" },
				{"content_type" : "input", "type":"text", "variable": "lastname", "title" : "Last Name", "topmargin" : "5px", "glyphicon":"user", "id":"lastname" },
				{"content_type" : "input", "type":"text", "variable": "mobile", "title" : "Phone", "topmargin" : "5px", "glyphicon":"earphone", "id":"mobile" },
				{"content_type" : "password", "type":"password", "variable": "password", "title" : "New Password", "topmargin" : "5px", "glyphicon":"lock", "id":"password" },
				{"content_type" : "password", "type":"password", "variable": "rpassword", "title" : "Retype password", "topmargin" : "5px", "glyphicon":"lock", "id":"rpassword" },
				{"content_type" : "showpass", "type":"checkbox", "variable": "showp", "title" : "Show password", "topmargin" : "0", "glyphicon":"", "id":"showp" },
				{"content_type" : "submit", "type":"submit", "variable": "change", "title" : "Register", "topmargin" : "10px", "glyphicon":"", "id":"register" },
				{"content_type" : "facebookfooter", "type":"submit", "variable": "login", "title" : "Continue with Facebook", "topmargin" : "5px", "glyphicon":"", "id":"facebooklogin" }
				];

	this.templateData = {
			  "blankTemplate": "",
			  "modalTitle": "<title>{{content.title}}</title>",
			  "modalHeader1": "<span class='headersubtitle'><a href ng-click='$parent.open(content.arg1);'>{{content.title1}}</a> | <a href ng-click='$parent.open(content.arg2);'>{{content.title2}}</a></span><br /><br />",
			  "modalHeader2": "<span class='headersubtitle'><a href ng-click='$parent.open(content.arg1);'>{{content.title1}}</a></span><br /><br />",
			  "inputTemplate": "<div class='input-group input-sm' style='margin: {{content.topmargin}} 10px 0 10px;'><span class='input-group-addon'><i class='glyphicon glyphicon-{{content.glyphicon}}'></i></span><input class='form-control' type='text' placeholder='{{content.title}}' ng-model=$parent.user_data[content.id] id='{{content.id}}' name='{{content.id}}' ng-keypress='$parent.myKeyPress($event);'></div>",
			  "inputsmTemplate": "<input class='form-control' type='text' placeholder='{{content.title}}' ng-model=$parent.langdata[content.id] id='{{content.id}}' name='{{content.id}}'>",
			  "passwordTemplate": "<div class='input-group input-sm' style='margin: {{content.topmargin}} 10px 0 10px;'><span class='input-group-addon'><i class='glyphicon glyphicon-{{content.glyphicon}}'></i></span><input class='form-control passtype' type='password' placeholder='{{content.title}}' ng-model=$parent.user_data[content.id] id='{{content.id}}' name='{{content.id}}' ng-keypress='$parent.myKeyPress($event);'></div>",
			  "showpassTemplate": "<div class='input-group input-sm' style='margin: {{content.topmargin}} 10px 0 10px;'><label class='checkbox-inline' style='font-size:11px;'><input type='checkbox' ng-click='$parent.genShowPassword(content.id);' id='{{content.id}}' name='{{content.id}}'>{{content.title}}</label></div>",
			  "emptyTemplate": "<div class='input-group input-sm' style='margin: {{content.topmargin}} 10px 0 10px;'><label class='checkbox-inline'></label></div>",
			  "textTemplate": "<p>{{title}}</p>",
			  "imageTemplate": "<img ng-src='{{ $parent.user_data[content.id] }}'>",
			  "submitTemplate": "<div class='form-inline' style='margin: {{content.topmargin}} 10px; 10px 10px;'><div class='input-group'><a class='btn btn-info' href ng-click='$parent.genSubmit(content.id,user_data)' id='{{content.id}}' name='{{content.id}}'>{{content.title}}</a></div></div></div>",
			  "submitFooterTemplate": "<div class='modal-footer'><div class='form-inline' style='margin-top: {{content.topmargin}} 10px 0 10px;'><div class='input-group'><a class='btn btn-info' href ng-click='$parent.genSubmit(content.id)' id='{{content.id}}' name='{{content.id}}'>{{content.title}}</a></div></div>",
			  "facebookFooterTemplate": "<div class='modal-footer'><div id='facebook'><a id='loginBtn' class='btn btn-sm btn-social btn-facebook' title='With your Facebook account' href ng-click='$parent.fblogin();'><i class='fa fa-facebook'></i>{{content.title}}</a><div id='status_fb'></div></div></div>"
			};       			
}]);

app.directive('contentItem', ['$compile' ,function ($compile) {
    var getTemplate = function (templates, contentType) {
        var template = '';
		var titles = {title:'modaltitle', header1:'modalHeader1', header2:'modalHeader2', input:'inputTemplate', inputsm:'inputsmTemplate', text:'textTemplate', password:'passwordTemplate', showpass:'showpassTemplate', image:'imageTemplate', submit:'submitTemplate', submitfooter:'submitFooterTemplate', facebookfooter:'facebookFooterTemplate', empty:'emptyTemplate', blank:'blankTemplate' };

		if(contentType in titles)
			 template = templates[titles[contentType]];

        return template;
    };

    var linker = function (scope, element, attrs) {
            element.html(getTemplate(scope.$parent.templateData, scope.content.content_type));
            $compile(element.contents())(scope);
		    };

    return {
        link: linker,
    	scope: {
            content: '=',
            myTemplates: '='
            }            
        
    };
}]);

modalInstance = null;
// '$modalInstance',
//$modalInstance, 
app.controller('loginController',['$scope', '$rootScope', '$localStorage', '$location', '$modal', '$modalInstance', 'interfaceModel', 'loginService', 'Facebook', 'FormControl', 'ModalService', 'bookService', function($scope, $rootScope, $localStorage, $location, $modal, $modalInstance, interfaceModel, loginService, Facebook, FormControl, ModalService, bookService){

	$scope.logaction = 'unknown';
	$scope.showinfo = showme;
	$scope.user_data = {};
	$scope.interfaceAll = { "login":"", "forgot":"", "change":"", "register":""};
	$scope.templateData = interfaceModel.templateData;
	
	$scope.interfaceLogin = $scope.interfaceAll.login = interfaceModel.login;
	$scope.interfaceForgot = $scope.interfaceAll.forgot = interfaceModel.forgot;
	$scope.interfaceChange = $scope.interfaceAll.change = interfaceModel.change;
	$scope.interfaceRegister = $scope.interfaceAll.register = interfaceModel.register;
	$scope.theTemplate = interfaceModel.login;
	
	$scope.updatestate = function() {
		if(update_session_time() > 0) {
			if(typeof $scope.usremail == "undefined" || $scope.usremail == null || true) {
				$scope.usremail = getcookievalues(localcookie, "user");
				token = getcookievalues(localcookie, "token");
				$scope.logaction =  'logout';
				$scope.loggedin	= true;
				}
			}
		else {
			$scope.logaction =  'login';
			$scope.loggedin	= false;
			}			
		};

	$scope.updatestate();
	
	if(typeof $localStorage.weeloyuser === "String")
		$scope.user_data.email = $localStorage.weeloyuser;
		
	$scope.myKeyPress = function(keyEvent) {
	  if (keyEvent.which === 13)
	  	$scope.genSubmit($scope.theTemplate);
	};

	$scope.myrender = function(dataelements) {
      angular.forEach(dataelements, function(data) {
      	if(data.content_type == "input" || data.content_type == "password" || data.content_type == "textarea") {
      		//console.log(data.id + ' = ' + $('#' + data.id).val());
	        //angular.element(data.id).controller('ngModel').$render();
	        //bug in angular js concerning autocomplete
      		$scope.user_data[data.id] = $('#' + data.id).val();
	        }
      });
    };
    
	
	$scope.genShowPassword = function(id) {
		
		if ($('#password').attr('type') != 'text') {
			$('.passtype').attr('type', 'text');
			$('.showpass').addClass('show');
		} 
		else {
			$('.passtype').attr('type', 'password');
			$('.showpass').removeClass('show');
		}
		return false;
	}
	$scope.genHidePassword = function() {
		$('.passtype').attr('type', 'password');
		$('.showpass').removeClass('show');
	};

	$scope.genSubmit = function(type) {

		if(typeof $scope.interfaceAll[type] !== "undefined" && $scope.interfaceAll[type] != null)
			$scope.myrender($scope.interfaceAll[type]);		
		
		switch(type) {
		 case 'login':
			$scope.user_data = FormControl.checkLogin($scope.user_data);
			if($scope.user_data.result > 0) {
				return loginService.login($scope.user_data.email, $scope.user_data.password).then(function(response){
					if(response.status == 0) {
						alert(response.errors);
						return;
						}
					token = response.data.token;
					user = $scope.usremail = $localStorage.weeloyuser = $scope.user_data.email;
					setCookieOnline(cookiename, response.data.cookie, response.data.duration+5);
					setCookieOnline(localcookie, ":token="+token+":user="+user, response.data.duration+5);			
					$scope.logaction =  'logout';
					$scope.loggedin	= true;
					$scope.updatestate();
					});
				}				
		 	break;
		 	
		 case 'change':
			$scope.user_data = FormControl.checkChange($scope.user_data);
			if($scope.user_data.result > 0) {
				return loginService.change($scope.user_data.email, $scope.user_data.password, $scope.user_data.npassword, token).then(function(response){
					if(response.data == '1')
						alert('Password has been changed');
					else alert(response.errors);
					});
				}				
		 	break;
		 	
		 case 'forgot':
			$scope.user_data = FormControl.checkForgot($scope.user_data);
			if($scope.user_data.result > 0) {
				return loginService.forgot($scope.user_data.email).then(function(response){
					if(response.data == '1')
						alert('A new Password has been sent to ' + $scope.user_data.email);
					});
				}				
		 	break;
		 	
		 case 'register':
			$scope.user_data = FormControl.checkRegister($scope.user_data);
			if($scope.user_data.result > 0) {
				alert('Register not implemented yet. Check status instead');
				return loginService.checkstatus($scope.user_data.email, token).then(function(response){
					if(response.data == '1')
						alert('login');
					else alert('logout');
					});
				}				
		 	break;
		 	
		 default:
		 	alert("Invalid command "+type);
		 	return;
		 }
		 
		if($scope.user_data.result < 0) {
			alert($scope.user_data.msg);
			return;
			}
	};
	
	$scope.genHidePassword() ; 
	$scope.open = function(type) {
		switch(type) {
			default:
			case 'login':
				$scope.theTemplate = interfaceModel.login;
				break;
				
			case 'forgot':
				$scope.theTemplate = interfaceModel.forgot;
				break;

			case 'change':
				$scope.theTemplate = interfaceModel.change;
				break;

			case 'register':
				$scope.theTemplate = interfaceModel.register;
				break;
			}
	 };	
	
	$scope.login = function() {
		$scope.theTemplate = interfaceModel.login;
		};

	$scope.logout = function() {
		removecookie(cookiename);
		$scope.updatestate();
		if($scope.usremail != "")
			return loginService.logout($scope.usremail, token).then(function(response){ console.log('logout'); console.log(response); });
		document.location.reload();
		}
							
	if(update_session_time() > 0)
		$scope.updatestate();

	$scope.login = function (type) {
	
		$scope.curTemplate = 'login'; 
		templateUrl = 'loginBackoffice.html';
		if(type == 'preferences') {
			$scope.curTemplate = 'change';
			templateUrl = 'chgpassBackoffice.html';
			}
				
		size = 'sm';
		if(modalInstance != null)
			modalInstance.close();

		modalInstance = $modal.open({
			templateUrl: templateUrl,
			controller: 'loginController',
			size: size,
			scope: $scope
			});
	};


	$scope.login();	
	
// facebook login
	
	$rootScope.session = {};
    $rootScope.$on("fb_statusChange", function (event, args) {
        $rootScope.$apply();
    });
    $rootScope.$on("fb_get_login_status", function () {
        Facebook.getLoginStatus();
    });
    $rootScope.$on("fb_login_failed", function () {
        console.log("fb_login_failed");
    });
    $rootScope.$on("fb_logout_succeded", function () {
        console.log("fb_logout_succeded");
    });
    $rootScope.$on("fb_logout_failed", function () {
        console.log("fb_logout_failed!");
    });

    $rootScope.$on("fb_connected", function (event, args) {
        /*
         If facebook is connected we can follow two paths:
         The users has either authorized our app or not.

         ---------------------------------------------------------------------------------
         http://developers.facebook.com/docs/reference/javascript/FB.getLoginStatus/

         the user is logged into Facebook and has authenticated your application (connected)
         the user is logged into Facebook but has not authenticated your application (not_authorized)
         the user is not logged into Facebook at this time and so we don't know if they've authenticated
         your application or not (unknown)
         ---------------------------------------------------------------------------------

         If the user is connected to facebook, his facebook_id will be enough to authenticate him in our app,
         the only thing we will have to do is to post his facebook_id to 'php/auth.php' and get his info
         from the database.

         If the user has a status of unknown or not_authorized we will have to do a facebook api call to force him to
         connect and to get some extra data we might need to unthenticated him.
         */

        var params = {};

		$rootScope.session.facebook_id = args.facebook_id;
		$rootScope.session.facebook_token = args.facebook_token;
		FB.api('/me', function(response) {
			if(response.name != "") {
				$rootScope.session.email = response.email;
				$rootScope.session.first_name = response.first_name;
				$rootScope.session.last_name = response.last_name;
				$rootScope.session.name = response.name;
				$rootScope.session.timezone = response.timezone;
				ses = $rootScope.session;
            	params = {'email': ses.email, 'facebookid': ses.facebook_id, 'facebooktoken': ses.facebook_token };
            	loginService.loginfacebook(params).then(function(response){
            		token = response.data.token;
					user = $scope.usremail = ses.email;
					setCookieOnline(cookiename, response.data.cookie, response.data.duration+5);
					setCookieOnline(localcookie, ":token="+token+":user="+user, response.data.duration+5);			
					$scope.updatestate();
            		});
				}
			});

        if (args.userNotAuthorized === true) {
            //if the user has not authorized the app, we must write his credentials in our database
            console.log("user is connected to facebook but has not authorized our app");
            
    		}
        else {
            console.log("user is connected to facebook and has authorized our app");
            //the parameter needed in that case is just the users facebook id
            params = {'facebook_id':args.facebook_id};
            //authenticateViaFacebook(params);
        }

    });


    // button functions
    $scope.fbgetLoginStatus = function () {
        Facebook.getLoginStatus();
    };

    $scope.fblogin = function () {
        Facebook.login();
    };

    $scope.fblogout = function () {
        Facebook.logout();
        $rootScope.session = {};
        //make a call to a php page that will erase the session data
        //$http.post("php/logout.php");
    };

    $scope.fbunsubscribe = function () {
        Facebook.unsubscribe();
    }

    $scope.fbgetInfo = function () {
        FB.api('/' + $rootScope.session.facebook_id, function (response) {
            console.log('Good to see you, ' + response.name + '.');
        });
    };
 	  

}]);
// <iframe src="../modules/callcenter/callcenter.php?bktracking=CALLCENTER&data=HNN_sAkvG2LRHm1sK1FHWsDz0Pw.X7LqF" width="550" height="700" frameBorder="0"></iframe>

app.directive('contentIframe',['$compile',function($compile){
	return {
		restrict: 'AE',
		replace: true,
		link: function(scope, element, attrs){
			element.html(scope.modalOptions.mydata.urldata).show();
			$compile(element.contents())(scope);
    		}
		};
}]);

