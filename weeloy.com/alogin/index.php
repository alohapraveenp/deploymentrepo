<?php
require_once("lib/Browser.inc.php");
$browser = new Browser();

// don't move those line as cookie header need to send prior to anything
require_once("conf/conf.init.inc.php");
require_once("lib/wpdo.inc.php");
require_once("lib/gblcookie.inc.php");
require_once("lib/class.login.inc.php");
require_once("lib/class.coding.inc.php");
require_once("conf/conf.session.inc.php");
require_once("lib/class.media.inc.php");
	
require_once("lib/class.images.inc.php");	
require_once("../inc/utilities.inc.php");
require_once("lib/class.analytics.inc.php");
require_once("lib/class.restaurant.inc.php");
require_once("lib/class.member.inc.php");
require_once("lib/class.cluster.inc.php");

?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="author" content="weeloy. https://www.weeloy.com">
<meta name="copyright" content="weeloy. https://www.weeloy.com">  
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
<title>Weeloy - Login System</title>
<link rel="icon" href="/favicon.ico" type="image/gif" sizes="16x16">
<link href="../css/bootstrap33.min.css" rel="stylesheet" type="text/css" />
<link href="../css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
<link href="../css/jquery-ui.css" rel="stylesheet" type="text/css">
<link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="../css/admin-style.css" rel="stylesheet" type="text/css"/>
<link href="../css/bootstrap-social.css" rel="stylesheet" type="text/css"/>

<link href='https://fonts.googleapis.com/css?family=Dosis:400,600,700bold|Roboto:400,900,700italic,700,500italic,500,400italic' rel='stylesheet' type='text/css'>

<script type="text/javascript" src="../js/jquery.min.js"></script>
<script type="text/javascript" src="../js/angular.min.js"></script>
<script type="text/javascript" src="../js/jquery-ui.js"></script>
<script type="text/javascript" src="../js/bootstrap.min.js"></script>
<script type="text/javascript" src="../js/ui-bootstrap-tpls-0.12.1.min.js"></script>
<script type="text/javascript" src="../js/facebookProvider.js"></script>
<script type="text/javascript" src="../js/facebookRun.js"></script>
<script type="text/javascript" src="../js/loginService.js"></script>
<script type="text/javascript" src="../js/ngStorage.min.js"></script>
<script type="text/javascript" src="../js/formControl.js"></script>
<script type="text/javascript" src="../backoffice/inc/libService.js"></script>
<script type="text/javascript" src="../backoffice/inc/backofficelib.js"></script>
<script type="text/javascript" src="../js/alog.js"></script>
<script type="text/javascript" src="loginController.js"></script>

<style>
body { 
	padding: 0 0 0 0;
	background-color:white;
	font-size:10px;
	font-family:Roboto;
	 }

.headertitle {
	font-size:12px;
	font-family:Roboto;
	}
	
.headersubtitle	 {
	padding: 0 0 0 20px;
	font-size:12px;
	font-family:Roboto;
	}

.ui-dialog {
    width  : 300px;
    height : 650px;
}
	
</style>

<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<script src="js/selectivizr-min.js"></script>
<script src="js/modernizr.js"></script>
<link rel="stylesheet" type="text/css" href="css/ie.css" media="screen" />
<![endif]-->
</head>


<script> var app = angular.module('loginsession',['ui.bootstrap', 'FacebookProvider', 'ngStorage']); </script>

<body bgcolor='white'>

<div ng-app="loginsession" ng-controller="loginController" ng-init="moduleName='login';">
<div ng-show='showinfo'>
<a href ng-click="logout()"><span class="glyphicon glyphicon-log-out"></span> logout </a><br/><br/>
<a href ng-click="login()"><span class="glyphicon glyphicon-log-in"></span> login </a><br /><br />
<span id="sessiontime" class="small"></span>
</div>
    
<div id="fb-root"></div>
</div>
<script>

<?php 
echo "var cookiename = '" . getCookiename('translation') . "';\n"; /* connected to the alog.js script file */ 
printf("var tablette = %s;", ($browser->isTablet()) ? 'true': 'false');
printf("var showme = %s;", ($_REQUEST['showme'] == "1") ? 'true': 'false');
?>

$(document).ready(function() { }); 

</script>

</body>
</html>
