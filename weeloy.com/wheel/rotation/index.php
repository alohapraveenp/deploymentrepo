<html>
    <head>
        <title>Rotation</title>
        <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
        <script src="http://ricostacruz.com/jquery.transit/jquery.transit.min.js" type="text/javascript"></script>
        <script src="//code.jquery.com/mobile/1.4.2/jquery.mobile-1.4.2.min.js"></script>
        <link rel="stylesheet" href="bootstrap3_2/css/bootstrap.min.css">
        <link rel="stylesheet" href="bootstrap3_2/css/bootstrap-theme.min.css">
        <link rel="stylesheet" type="text/css" href="common.css">
    </head>
    <body>
        <div class="start form-group">
            <input type="text" class="form-control" id="costumer-name" name="costumer-name" placeholder="Type you name"></input>
            <button type="button" class="btn btn-default" id="button-submit-name">submit</button>  

        </div>
        <div class="wheel-wrapper">
            <img class="wheel" src="wheel.png">
            <img class="marker" src="pointer.png">
            <div class="swipe-box">
            </div>
            <div id="win-notification"></div>
            <img class="gif star-left1" src="star_3.gif" style="visibility:hidden"/>
            <img class="gif star-left2" src="star_2.gif" style="visibility:hidden"/>
            <img class="gif star-right" src="star_3.gif" style="visibility:hidden"/>
            <img class="gif star-right" src="star_2.gif" style="visibility:hidden"/>
            <img class="gif fireworks-right" src="fireworks_3.gif" style="visibility:hidden"/>
            <img class="gif fireworks-right" src="fireworks_1.gif" style="visibility:hidden"/>
            
            <audio id="wheel-sound" preload="auto">
                <source type="audio/mp3" src="wheel_sound.mp3"></source>
            </audio> 
            <audio id='audio1'  preload='auto'>
                <source type='audio/mp3' src='end_wheel.mp3'></source>
            </audio>                
        </div>
            
            <script src="bootstrap3_2/js/bootstrap.min.js"></script>
            <script src="constants.js"></script>
            <script src="controller.js"></script>
        </div>
    </body>
</html>