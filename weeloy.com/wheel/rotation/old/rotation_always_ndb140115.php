<?php

class WY_Images { function l_file_exists($filename) { if($filename == "wheelscript.js") return true; return false; } function showPicture($dummy, $imgname) { return $imgname; } }
class WY_restaurant {  var $logo; var $wheel; function __construct() { $this->logo = "logo.png"; $this->wheel = "stdwheel.png"; } function getRestaurant($dummy) { } }
$_SESSION['user']['member_type'] = "";

function showWheel($content, $style) {

	echo "<html><head><title>Rotation</title>"
		. "<meta name='copyrightright' content='all right reserved (c) WEELOY PTE, 2014'>"
		. "<meta name='viewport' content='user-scalable=1.0,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0'>"
		. "<meta name='apple-mobile-web-app-capable' content='yes'>"
		. "<meta name='format-detection' content='telephone=no'>"
		. "<link rel='stylesheet' href='bootstrap3_2/css/bootstrap.min.css'>"
		. "<link rel='stylesheet' href='bootstrap3_2/css/bootstrap-theme.min.css'>"
		. "<link rel='stylesheet' type='text/css' href='common.css'>"
		. "<style>" . $style . "</style>"
		. "</head><body>"
		. "<div class='wheel-wrapper'>"
		. $content
		. "</div></body></html>";
	exit;

}

$theRestaurant = $_REQUEST['restaurant'];
if(empty($theRestaurant)) $theRestaurant = "SG_SG_R_TheFunKitchen";

$fullfeature = ($_SESSION['user']['member_type'] == 'weeloy_agent' || $_SESSION['user']['member_type'] == 'weeloy_admin' || true);
$logipad = (preg_match("/iPad/", $_SERVER['HTTP_USER_AGENT']));
$imgsize = ($logipad) ? "height='550' width='550'" : "";


$imgdata = new WY_Images($theRestaurant);
$resdata = new WY_restaurant();

$resdata->getRestaurant($theRestaurant);

$logoName = "logo.png";
if($imgdata->l_file_exists($resdata->logo))
	$logoName = $imgdata->showPicture($theRestaurant, $resdata->logo);
$logoimg = "<img class='logo' src='" . $logoName . "'>";

if(empty($resdata->wheel)) {
	showWheel($logoimg . "<h3>The wheel has not been configured by the restaurant.</h3>", "body { margin: 100px 100px 100px 100px; }");		// never return;
	}

if($imgdata->l_file_exists("wheelscript.js") == false) {
	showWheel($logoimg . "<h3>The wheel has not been configured by the restaurant</h3>", "body { margin: 100px 100px 100px 100px; }");
	exit;
	}

$pict = "stdwheel.png";
if($imgdata->l_file_exists("wheel.png"))
	$pict = $imgdata->showPicture($theRestaurant, "wheel.png");	
$imgwheel = "<img class='wheel' src='" . $pict . "'" . $imgsize . ">";
		
$info = parse_url($_SERVER['HTTP_REFERER']);
//if($info['host'] !== $_SERVER['HTTP_HOST']) exit;

if($fullfeature == false) {
	showWheel($imgwheel . $logoimg, "");
	exit;
}
	
$confirmation = $resCode = $membCode = $guestname = "";
if(isset($_REQUEST['confirmation']))
	$confirmation = $_REQUEST['confirmation'];
if(isset($_REQUEST['restCode']))
	$restCode = $_REQUEST['restCode'];
if(isset($_REQUEST['membCode']))
	$membCode = $_REQUEST['membCode'];
if(isset($_REQUEST['guestname']))
	$guestname = $_REQUEST['guestname'];
	
$html = "<html><head><title>Rotation</title>"
		. "<meta name='copyrightright' content='all right reserved (c) WEELOY PTE, 2014'>"
		. "<meta name='viewport' content='user-scalable=1.0,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0'>"
		. "<meta name='apple-mobile-web-app-capable' content='yes'>"
		. "<meta name='format-detection' content='telephone=no'>"
		. "<link rel='stylesheet' href='bootstrap3_2/css/bootstrap.min.css'>"
		. "<link rel='stylesheet' href='bootstrap3_2/css/bootstrap-theme.min.css'>"
		. "<link rel='stylesheet' type='text/css' href='common.css'>"
		. "</head><body>"
		. "<div class='start form-group'>"
                . ""
		. "<input type='text' value = '$guestname' class='form-control' id='guest-name' name='guest-name' placeholder='Type your name'></input>"
		. "<input type='text' value = '$confirmation' class='form-control' id='confirmation-code' name='confirmation-code' placeholder='Type your confirmation number'></input>"
		. "<input type='text' value = '$restCode' class='form-control' id='restaurant-code' name='restaurant-code' placeholder='Type your Restaurant Code'></input>"
		. "<input type='text' value = '$membCode' class='form-control' id='guest-code' name='guest-code' placeholder='Type your Member Code'></input>"
		. "<button type='button' class='btn btn-default' id='button-submit-name'>submit</button>"
		. "</div>"
		. "<div class='wheel-wrapper' style='visibility:hidden'>"
		. $imgwheel . $logoimg
		. "<img class='marker' src='pointer.png' border='0'>"
		. "<div class='swipe-box'></div>"
		. "<div id='win-notification'></div>"
		. "<img class='gif star-left1' src='star_3.gif' border='0' style='visibility:hidden'/>"
		. "<img class='gif star-left2' src='star_2.gif' border='0' style='visibility:hidden'/>"
		. "<img class='gif star-right' src='star_3.gif' border='0' style='visibility:hidden'/>"
		. "<img class='gif star-right' src='star_2.gif' border='0' style='visibility:hidden'/>"
		. "<img class='gif fireworks-right' src='fireworks_3.gif' border='0' style='visibility:hidden'/>"
		. "<img class='gif fireworks-right' src='fireworks_1.gif' border='0' style='visibility:hidden'/>"
		. "<audio id='sound-track' preload='auto'><source type='audio/mp3' src='wheel_soundtrack.mp3'></source>\n"
		. "</audio></div>"
		. "<script src='a_js/jquery-latest.min.js' type='text/javascript'></script>"
		. "<script src='bootstrap3_2/js/bootstrap.min.js' type='text/javascript'></script>"
		. "<script src='a_js/jquery.mobile-1.4.2.min.js' type='text/javascript'></script>"
		. "<script src='a_js/copyrights.js' type='text/javascript'></script>"
		. "<script src='a_js/jquery.transit.min.js' type='text/javascript'></script>"
		. "<script src='a_js/constants.js' type='text/javascript'></script>"
		. "<script src='a_js/controller.js' type='text/javascript'></script>"
		. "<script src='a_js/ajax.js' type='text/javascript'></script>"
		;
		printf("%s", $html);


printf("<script src='%s'></script>", $imgdata->showPicture($theRestaurant, "wheelscript.js"));

if($logipad)
	{
	printf("<script language='javascript' type='text/javascript'>");
	printf("$(document).ready(function() {\n");
	printf("$('.marker').css({top:40, left:280});");
	printf("$('.swipe-box').css({ background: bgWebKit }).css({ background: bgMoz }); ");
	printf("}); ");
	printf("</script>");
	}
?>

</body>
</html>