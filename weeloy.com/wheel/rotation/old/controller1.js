$(document).ready(init);

function init(){
    var wheel = new WHEELOFFORTUNE();
    wheel.init();
}

function WHEELOFFORTUNE(){

    this.cache = {};            

    this.init = function () {
        console.log('controller init...');
        this.configForMobile();
        
        //animations and audios
        this.cache.gifs = document.getElementsByClassName('gif');
        this.cache.audios = document.getElementsByClassName('audio');
        
        //spin parameters
        this.cache.deg = 0;
        this.cache.duration = 0;

        //dom elements
        this.cache.wheel = $('.wheel');
        this.cache.wheelMarker = $('.marker');
        this.cache.wheelSpinBtn = $('.wheel');
        this.cache.box = $('.swipe-box');

        this.initEvents();



        //setup prize events
        //this.prizeEvents();
    };

    this.initEvents = function() {
        console.log('events init...')
        var _this = this;

        var startX, startY;
        this.cache.box.on('mouseenter',function(e){
            startX = e.pageX;
            startY = e.pageY;
        });

        var endX,endY;
        this.cache.box.on('mouseleave',function(e){
            if(!_this.cache.box.hasClass('disabled'))
            {
                endX = e.pageX;
                endY = e.pageY;
                var distance = _this.computeDistance(startX,startY,endX,endY)
                var spinParams = _this.computeSpinParam(distance);
                if (!_this.cache.wheelSpinBtn.hasClass('disabled') && spinParams.deg > 0) {
                    _this.resetSpin();
                    _this.spin(spinParams);
                }
            }
        });

        this.cache.box.on('click',function(e){
            if(!_this.cache.box.hasClass('disabled'))
            {
                if (!_this.cache.wheelSpinBtn.hasClass('disabled')) {
                    _this.resetSpin();
                    _this.spin({deg:13000,duration:10000});
                }
            }
        });
        
        this.cache.box.on('swipe',function(e){
            console.log('triggered swipe event ...')
            if(!_this.cache.box.hasClass('disabled'))
            {
                
                var distance = e.distX;
                var spinParams = _this.computeSpinParam(distance);
                if (!_this.cache.wheelSpinBtn.hasClass('disabled') && spinParams.deg > 0) {
                    _this.resetSpin();
                    _this.spin(spinParams);
                }
            }
        });

        $('.swipe-box,#spin-notification').attr('unselectable','on')
            .css({'-moz-user-select':'-moz-none',
                '-moz-user-select':'none',
                '-o-user-select':'none',
                '-khtml-user-select':'none', 
                '-webkit-user-select':'none',
                '-ms-user-select':'none',
                'user-select':'none'
            }).bind('selectstart', function(){ return false; });  

     };


    this.computeSpinParam = function(distance)
    {
        console.log('computing params');
        console.log('distance: ' + distance);
        if(distance < MIN_DISTANCE_TH){
            $('#spin-notification').text('Come on swipe!'); 
        	return {deg : 0, duration : 0};
        }
        else{ 
             var deg = ROT_DEG4 + Math.round(Math.random() * 360);
            var duration = _DURATION_;
            $('#spin-notification').text('SUPER FASTTTTT!');
            return {deg : deg, duration : duration};
            //console.log('error in computing spin params');
        }
        return {deg : deg, duration : duration};

    };

    this.spin = function (spinParam) {
        console.log('spinning wheel');

        var _this = this;
        // reset wheel

        //disable spin button while in progress
        this.cache.wheelSpinBtn.addClass('disabled');
        this.cache.box.addClass('disabled');

        var deg = spinParam.deg;
        var duration =  spinParam.duration;

        //transition queuing
        //ff bug with easeOutBack
        this.cache.wheel.transition({
            rotate: '0deg'
        }, 0)
            .transition({
            rotate: deg + 'deg'
        }, duration, 'easeOutExpo');
        document.getElementById('wheel-sound').play();
        //move marker
        _this.cache.wheelMarker.transition({
            rotate:  MARKER_ROT_DEG + 'deg'
        }, 0, 'snap');

        //a bit before the wheel finishies
        setTimeout(function(){
            $('#spin-notification').text('oooOOO!');
        },duration - duration/4);

        //just before wheel finish
        setTimeout(function () {
            //reset marker
            _this.cache.wheelMarker.transition({
                rotate: '0deg'
            }, MARKER_DOWN_TIME, 'easeOutExpo');
        }, duration - WHEEL_ENDING_TIME);

        //start animation just before finish because of delay
        setTimeout(function () {
            _this.startAnimation();
        },duration-500);
        //wheel finish
        setTimeout(function () {

        //compute segment
        var spins = Math.ceil(deg/360);
        var segment = (NUMBER_OF_SEGMENTS - Math.round((deg/15) % NUMBER_OF_SEGMENTS))%NUMBER_OF_SEGMENTS ;
        // logs
        console.log('degree: ' + deg);
        console.log('spins: ' + spins);
        console.log('segment:' + segment);

        //reset wheel
        $('#win-notification').text('You won ' + wheelmsg[segment] + '!');
        _this.allowSpin();
        //re-enable wheel spin
            setTimeout(function(){
                _this.stopAnimation();
            }, 10000);
        }, duration);

    };

    this.computeDistance = function(x1,y1,x2,y2){
        return Math.sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2));

    };

    this.allowSpin = function () {
        console.log('reseting...');
        this.cache.wheelSpinBtn.removeClass('disabled');
        this.cache.box.removeClass('disabled');
        this.cache.deg = 0;
        this.cache.duration = 0;
    };
    
    this.resetSpin = function(){
        $('#win-notification').text('');
        this.cache.wheel.transition({
            rotate: '0deg'
        }, 300,'snap');
    };
    
    this.startAnimation = function(){
        console.log('start animation ...');
        for(var i = 0; i < this.cache.gifs.length; i++){
                this.cache.gifs[i].style.visibility = 'visible';
        }
		document.getElementById('audio1').play();
		document.getElementById('audio2').play();
    };
    
    this.stopAnimation = function(){
        console.log('stop animation ...');
        for(var i = 0; i < this.cache.gifs.length; i++){
                this.cache.gifs[i].style.visibility = 'hidden';
        }
    };
    
    $.event.special.swipe.handleSwipe = function(start,stop){
            console.log('handling swipe...');
            if ( stop.time - start.time < $.event.special.swipe.durationThreshold &&
                Math.abs( start.coords[ 0 ] - stop.coords[ 0 ] ) < $.event.special.swipe.verticalDistanceThreshold &&
                Math.abs( start.coords[ 1 ] - stop.coords[ 1 ] ) > $.event.special.swipe.horizontalDistanceThreshold ) 
            {
                start.origin.trigger( "swipe" );
            }
    };
    
    this.configForMobile = function(){
        $( document ).on( "mobileinit", function() { //because those idiots add an auto loading message..
            $.mobile.loader.prototype.options.disabled = true;
        });
        document.ontouchmove = function(event){ //disable page move
            event.preventDefault();
        }
    };
};


