/*** 
 * CONSTANTS FILE 
 * ***/

/*** MISC ***/
var NUMBER_OF_SEGMENTS = 24;
var WHEEL_ENDING_TIME = 500; //ms
var MARKER_DOWN_TIME = 300; //ms - time the marker takes to fall back
var MARKER_ROT_DEG = -50;

/***SPIN PARAMS (rotation in degree - duration in ms - 1 2 3 4 5 are speed ***/
    //thresholds
var MIN_DISTANCE_TH = 60; //px
var DISTANCE_TH1 = 130;
var DISTANCE_TH2 = 170;
var DISTANCE_TH3 = 220;
var DISTANCE_TH4 = 350;
var DISTANCE_TH5 = 500;

    //angle of rotation
var ROT_DEG1 = 1000;
var ROT_DEG2 = 2000;
var ROT_DEG3 = 4000;
var ROT_DEG4 = 10000;
var ROT_DEG5 = 15000;

    //duration of rotation
var DURATION1 = 10000;
var DURATION2 = 13000;
var DURATION3 = 17000;
var DURATION4 = 23000;
var DURATION5 = 30000;

var _DURATION_ = 15000;

//animation
var DING_SRC = 'ding.wav';
var CROWD_SRC = 'crowd_cheering.wav'


