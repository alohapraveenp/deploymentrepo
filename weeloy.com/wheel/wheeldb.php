<?php

header('Content-Type: image/png');
require_once("conf/conf.init.inc.php");
require_once("lib/wpdo.inc.php");
require_once("lib/wglobals.inc.php");
require_once("lib/class.restaurant.inc.php");
require_once("lib/class.member.inc.php");
require_once("lib/class.images.inc.php");
require_once("lib/class.media.inc.php");
require_once("lib/class.wheel.inc.php");

function clean($data) { return (!empty($data)) ? preg_replace("/\W/", "", $data) : "";  }

$sponsorflg = false;
$image_reference = 'wheelnolgd.png';
$saveWheel = clean($_REQUEST['savewheel']);
$theRestaurant = clean($_REQUEST['restaurant']);
$type = clean($_REQUEST['type']);
$type_name = ($type == "") ? "" : $type . "_";

if(empty($theRestaurant)) {
	$photoImage = imagecreatetruecolor(702, 702);
	imagesavealpha($photoImage, true);
	$trans_color = imagecolorallocatealpha($photoImage, 0, 0, 0, 127);
	imagefill($photoImage, 0, 0, $trans_color);
	$wheel = imagecreatefrompng('stdwheel.png');
	imagecopy($photoImage, $wheel, 0, 0, 0, 0, 700, 700);
	imagepng($photoImage);
	imagedestroy($photoImage);
	exit;
}

if($type == "") {
	$resdata = new WY_restaurant();
	$resdata->getRestaurant($theRestaurant);
	$wheel = $resdata->wheel;
	if(empty($wheel))
		exit;

	$wheel = $resdata->wheel;
	if($resdata->sponsor() && $resdata->wheelsponsor != "") {
		$mediadata = new WY_Media($theRestaurant);
		$image_reference = $mediadata->getPartialPath() . $resdata->wheelsponsor;
		if($resdata->wheelsponsor != 'wheelnolgdUEFA.png')
			$sponsorflg = true;
		}
} else {
	$wh = new WY_wheel($theRestaurant, '', $type);
	$wh->readWheel();
	$wheel = $wh->wheel;
	if(empty($wheel))
		exit;
error_log('WHEEL ' . $type . " " . $wheel);
}
	
$stringAr = explode("|", strtoupper($wheel));
	
	
function myrand() {
	$rr = array("10", "10", "10", "10", "10", "25", "25", "25", "25", "35", "35", "10", "10", "10", "10", "10", "25", "25", "25", "25", "35", "35", "10", "10", "10", "10", "10", "25", "25", "25", "25", "35", "35", "100");
	$k = rand(0, count($rr)-1);
	return $rr[$k];
}

function writemystring($png, $deg, $deltaX, $deltaY, $color, $font_path, $font_size, $string, $vertical) {
	
	$newstr = "";
	$space = "  ";
        
	$data = array(
				"/^[0-9]+%[^a-zA-Z]+/", "", 
				"/^[$][0-9]+[^a-zA-Z]+/", "", 
				"/^Free[^a-zA-Z]+/i", "",
                                "/^Comp[^a-zA-Z]+/i", "",
				"/^[0-9] \+ [0-9][^a-zA-Z]+/", "",
				"/^1.for.1[^a-zA-Z]+/i", '1-1', 
				);
						   
    for($i = 0; $i < count($data); $i += 2) { 
    	$fontz = 10;   
		if(preg_match($data[$i], $string, $match)) {
			$string = preg_replace($data[$i], "", $string);
			$theMatch = ($data[$i + 1] == "") ? $match[0] : $data[$i + 1];
			$newstr = $theMatch;
			if(strlen($theMatch) > 4) $fontz = 7;
			if($vertical == false) $fontz += 3;
			imagettftext($png, $fontz, $deg, $deltaX, $deltaY, $color, $font_path, $newstr);
			
			}
		}
		
	if($vertical) {
		$newstr = "\n\n";
		$limit = strlen($string);
		for($i = 0; $i < $limit; $i++)
			$newstr .= $space . $string[$i] . " \n";
		}
	else {
		$newstr = "\n";
		$limit = strlen($string);
		for($i = 0; $i < $limit; $i++)
			$newstr .= ($string[$i] == " ") ? "\n" : $string[$i];
		$font_size += 2;
		} 	
	imagettftext($png, $font_size, $deg, $deltaX, $deltaY, $color, $font_path, $newstr);
	
}

putenv('GDFONTPATH=' . realpath('.'));
//$font_path = 'fonts/Sancreek-Regular';

//$font_path = 'fonts/cantebriggia';
//$font_path = 'fonts/WHITRABT';
//$font_path = 'fonts/zai_CourierPolski1941';
//$font_path = 'fonts/Masquerade';
//$font_path = 'fonts/Dearest';
//$font_path = 'fonts/vtcbelialsbladeshadow';
//$font_path = 'fonts/VastShadow-Regular';
$font_path = 'fonts/GoblinOne';
//$font_path = 'fonts/RobotoCondensed-Bold';

$font_size = 7;

$photoImage = imagecreatetruecolor(702, 702);
imagesavealpha($photoImage, true);
$trans_color = imagecolorallocatealpha($photoImage, 0, 0, 0, 127);
imagefill($photoImage, 0, 0, $trans_color);
$wheel = imagecreatefrompng($image_reference);
imagecopy($photoImage, $wheel, 0, 0, 0, 0, 702, 702);
$black = imagecolorallocate($photoImage, 0, 0, 0);


$CoordXY = array(338, 20, 425, 28, 507, 60, 574, 110, 630, 175, 667, 256, 678, 340,
672, 425, 642, 507, 592, 575, 525, 630, 445, 667, 362, 681, 278, 672, 197, 641, 128, 592,
74, 526, 35, 444, 22, 365, 32, 275, 63, 197, 110, 130, 171, 71, 252, 40, 0, 0);

$Ray = 330;
for($i = 0; $i < 24; $i++)
	{
	$deg = ($i * 15);
	$deltaX = 350 + floor((sin(deg2rad($deg - 2))) * $Ray);
	$deltaY = 20 + floor((1 - cos(deg2rad($deg - 2))) * $Ray);
	if($sponsorflg == false || ($i != 0 && $i != 1 && $i != 23))
		writemystring($photoImage, -$deg, $deltaX, $deltaY, $black, $font_path, $font_size, $stringAr[$i+1], true);
	else if($sponsorflg == true && $i == 0) {
		$deltaY = 170 + floor((1 - cos(deg2rad($deg - 2))) * 710);
		$deltaX -= 20;
		writemystring($photoImage, -$deg, $deltaX, $deltaY, $black, $font_path, $font_size, $stringAr[$i+1], false);
		}
		
	}
	
imagepng($photoImage);	// show to the browser. Very usefull for debugging

if($saveWheel == "w76849361") 
	{
	
	$image = new WY_Images($theRestaurant);
	$tmp_filename = $image->tmpdir . $theRestaurant . "_"  . $type_name . "wheel.png";
	$filename = $type_name . "wheel.png";

	if($image->l_file_exists($filename)) {
		$image->l_unlink($filename, 'wheel');
		}
	
	imagepng($photoImage, $tmp_filename);
	$image->l_move_file($tmp_filename, $filename, 'image/png', 'wheel');
	unlink($tmp_filename);

	$tmp_filename = $image->tmpdir . $theRestaurant . "_"  . $type_name . "wheel350.png";
	$filename = $type_name . "wheel350.png";
	if($image->l_file_exists($filename)){
	$image->l_unlink($filename, 'wheel');}

	$newwidth = $newheight = 350;
	$smphotoImage = imagecreatetruecolor($newwidth, $newheight);
	imagesavealpha($smphotoImage, true);
	$trans_color = imagecolorallocatealpha($smphotoImage, 0, 0, 0, 127);
	imagefill($smphotoImage, 0, 0, $trans_color);
	imagecopyresized($smphotoImage, $photoImage, 0, 0, 0, 0, $newwidth, $newheight, 702, 702);
	imagepng($smphotoImage, $tmp_filename);
	imagedestroy($smphotoImage);

	$image->l_move_file($tmp_filename, $filename, 'image/png', 'wheel');
	unlink($tmp_filename);

    }

// don't move as we need it to create the small version 350
imagedestroy($photoImage);	// release server ressources


?>