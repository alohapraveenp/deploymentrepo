<?php

for($i = 10; $i < 257; $i++) {
	$wheelvalue = $i;
	$imgSize = 150;

	header('Content-Type: image/png');

	$photoImage = imagecreatetruecolor($imgSize, $imgSize);
	imagesavealpha($photoImage, true);
	$trans_color = imagecolorallocatealpha($photoImage, 0, 0, 0, 127);
	imagefill($photoImage, 0, 0, $trans_color);
	$wheel = imagecreatefrompng('wheelrating.png');
	imagecopy($photoImage, $wheel, 0, 0, 0, 0, $imgSize, $imgSize);
	$black = imagecolorallocate($photoImage, 0, 0, 0);

	putenv('GDFONTPATH=' . realpath('.'));
	$font_path = 'fonts/Sancreek-Regular';

	//$font_path = 'fonts/cantebriggia';
	//$font_path = 'fonts/WHITRABT';
	$font_path = 'fonts/zai_CourierPolski1941';
	//$font_path = 'fonts/Masquerade';
	//$font_path = 'fonts/Dearest';
	//$font_path = 'fonts/vtcbelialsbladeshadow';
	//$font_path = 'fonts/VastShadow-Regular';
	//$font_path = 'fonts/GoblinOne';

	$font_size = 38;
	$deltaX = 45;
	$deltaY = 90;
	if($wheelvalue > 99)
		{
		$font_size = 28;
		$deltaX = 40;
		}
	imagettftext($photoImage, $font_size, 0, $deltaX, $deltaY, $black, $font_path, $wheelvalue);
	imagepng($photoImage);

	$filename = "./wheelfolder/wheelvalue_" . $i . ".png";

	imagepng($photoImage, $filename);
	imagedestroy($photoImage);
}

?>