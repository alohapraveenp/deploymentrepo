<?php
	// header('Access-Control-Allow-Origin: *');
	// header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
	// header('Access-Control-Allow-Headers: origin, x-requested-with, content-type');
	require_once("conf/conf.init.inc.php");
	require_once("lib/wpdo.inc.php");
	require_once("lib/class.tool.inc.php");
	require_once("lib/Browser.inc.php");
	require_once("lib/class.mail.inc.php");
	require_once("lib/class.async.inc.php");
	require_once("lib/class.spool.inc.php");
	require_once("lib/wglobals.inc.php");
	require_once("lib/class.dailyspecials.inc.php");
	// require_once("lib/class.restaurant.inc.php");

	$app->post('/getdailyspecial', function() use ($app) {
		return getDailySpecial($app);
   	});

	$app->post('/postdailyspecial', function() use ($app) {
		return postDailySpecial($app);
   	});

	$app->post('/deletedailyspecial', function() use ($app) {
		return deleteDailySpecial($app);
   	});

   	$app->post('/getrestauranttitle', function() use ($app) {
		return getrestauranttitle($app);
   	});

   	$app->post('/getallrestaurant', function() use ($app) {
		return getallrestaurant($app);
   	});

   	$app->post('/getallrestaurantdetails', function() use ($app) {
		return getallrestaurantdetails($app);
   	});

   	function getallrestaurantdetails($app) {

	    $rest = new WY_dailyspecial();
	    //error_log($app);
	    $params = "";

		$params = $app->request()->getBody();
	    
	    $result = $rest->getallrestaurantdetails($params);
	    
	    echo format_api(1, $result, 1, '');

	}

   	function getallrestaurant($app) {

	    $rest = new WY_dailyspecial();
	    //error_log($app);
	    $params = "";

		$params = "";
	    
	    $result = $rest->getallrestaurant();
	    
	    echo format_api(1, $result, 1, '');

	}

   	function getrestauranttitle($app) {

	    $rest = new WY_dailyspecial();
	    //error_log($app);
	    $params = "";

		$params = $app->request()->getBody();
	    
	    $result = $rest->getrestTitle($params);
	    
	    echo format_api(1, $result, 1, '');

	}

    function getDailySpecial($app) {
		$data = '';
		$status = $sizedata = 0;
		$msg = "";
		$params = $app->request()->getBody();
		$header = array('Content-Type:text/plain'); 
		
		// $apiUrl = 'https://dev.weeloy.asia/api/md_marketing/getdailyspecial';
		$apiUrl = 'https://api.weeloy.asia/dsbnewdailypost';
            
        $result  = remote_call($apiUrl,'POST',$params,$header);
	    $data = $result;
	    $status = $data['statusCode'];
	    try
	    {
	    	$sizedata = $data['sizedata'];
	    }
	    catch (Exception $e)
	    {
	    	$sizedata = '0';
	    }
	    $msg = $data['message'];
		echo format_api($status, json_decode($data['body']), $sizedata , $msg);
		
	}

    function postDailySpecial($app) {
		$data = '';
		$status = $sizedata = 0;
		$msg = "";
		$params = "";

		$params = $app->request()->getBody();

    	$header = array('Content-Type:text/plain'); 
		
		$apiUrl = 'https://api.weeloy.asia/dsbnewdailypost';
            
       	$result  = remote_call($apiUrl,'POST',$params,$header);

		$data = $result;
	    $status = $data['statusCode'];
	    $sizedata = $data['sizedata'];
	    $msg = $data['message'];
	    
		echo format_api($status, $data['body'], $sizedata, $msg);
		
	}

    function deleteDailySpecial($app) {
		$data = '';
		$status = $sizedata = 0;
		$msg = "";
		$params = "";

		$params = $app->request()->getBody();
    	$header = array('Content-Type:text/plain'); 
		
		$apiUrl = 'https://api.weeloy.asia/dsbnewdailypost';
            
       	$result  = remote_call($apiUrl,'POST',$params,$header);

		$data = $result;
	    $status = $data['statusCode'];
	    $sizedata = $data['sizedata'];
	    $msg = $data['message'];
	    
		echo format_api($status, $data['body'], $sizedata, $msg);
		
	}
	
?>