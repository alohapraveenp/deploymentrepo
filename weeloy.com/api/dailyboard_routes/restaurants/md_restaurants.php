<?php
// header('Access-Control-Allow-Origin: *');
// header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
// header('Access-Control-Allow-Headers: origin, x-requested-with, content-type');

	require_once("conf/conf.init.inc.php");
	require_once("lib/wpdo.inc.php");
	require_once("lib/class.tool.inc.php");
	require_once("lib/Browser.inc.php");
	require_once("lib/class.mail.inc.php");
	require_once("lib/class.async.inc.php");
	require_once("lib/class.spool.inc.php");
	require_once("lib/wglobals.inc.php");
	require_once("lib/class.dailyspecials.inc.php");

   	$app->post('/newdsbresto', function() use ($app) {
		return newDSBResto($app);
   	});

   	$app->post('/getdsbrestlist', function() use ($app) {
		return getDSBRestList($app);

   	});

   	$app->post('/deletedsbresto', function() use ($app) {
		return deleteDSBResto($app);
   	});

   	$app->post('/getrestauranttitle', function() use ($app) {
		return newDSBResto($app);
   	});

   	$app->get('/getprofilelist/:restaurant', 'getProfileList');

   	

   	$app->get('/verifyemail/:code', 'updateemailverify');
   	//$app->get('/sendemail/:code/:email', 'sendemail');
   	$app->post('/sendemail', function() use ($app) {
		return sendemail($app);
   	});

   	function updateemailverify($code)
   	{
   		$data = '';
		$status = $sizedata = 0;
		$msg = "";
		$params ='{"httpMethod": "POST","body":{"data":{"statusupdate":"1","code":"'.$code.'"}}}';

		$header = array('Content-Type:text/plain'); 
		
		$apiUrl = 'https://api.weeloy.asia/dsbnewrestaurant';
            
        $result  = remote_call($apiUrl,'POST',$params,$header);
	    $data = $result;
	    $status = $data['statusCode'];
	   	$sizedata = $data['sizedata'];
	    $msg = $data['message'];
	    
	    if($data)
	    {
	    	echo "Email Verification successful!!!";	
	    }
	    
		//echo format_api($status, json_decode($data['body']), $sizedata, $msg);
   	}

   	function getDSBRestList($app) {
		$data = '';
		$status = $sizedata = 0;
		$msg = "";
		$params = "";

		$header = array('Content-Type:text/plain'); 
		
		$apiUrl = 'https://api.weeloy.asia/dsbnewrestaurant';
            
        $result  = remote_call($apiUrl,'POST',$params,$header);
	    $data = $result;
	    $status = $data['statusCode'];
	   	$sizedata = $data['sizedata'];
	    $msg = $data['message'];
	    
		echo format_api($status, json_decode($data['body']), $sizedata, $msg);
		
	}
	function getProfileList($restaurant) {
		
		$data = '';
		$status = $sizedata = 0;
		$msg = "";
		$params = "";

		$header = array('Content-Type:text/plain'); 
		$apiUrl = 'https://api.weeloy.asia/profile/GetSubProfile?restaurant='.$restaurant.'';
        $result  = remote_callget($apiUrl,$params,$header);
        
	    error_log("Im here");
	    $data = $result;
	    //error_log($data);
	    $status = $data['statusCode'];
	   	$sizedata = $data['sizedata'];
	    $msg = $data['message'];
	    
		echo format_api($status, json_decode($data['body']), $sizedata, $msg);
		
	}

    function newDSBResto($app) {
		$data = '';
		$status = $sizedata = 0;
		$msg = "";
		$params = "";

		$params = $app->request()->getBody();

		$paramsx = json_decode($app->request()->getBody(),true);
		$email = $paramsx['body']['data']['email'];
		$domain = substr(strrchr($email, "@"), 1);

		$checkemail = validate_email($domain);
		
		//if valid dns return 1 otherwise invalid
		if($checkemail == 1)
		{

			$header = array('Content-Type:text/plain'); 
		
			$apiUrl = 'https://api.weeloy.asia/dsbnewrestaurant';
	            
	       	$result  = remote_call($apiUrl,'POST',$params,$header);

			$data = $result;
		    $status = $data['statusCode'];
		   	$sizedata = $data['sizedata'];
		    $msg = $data['message'];

			echo format_api($status,json_decode($data['body']), $sizedata, $msg);
		}
		else
		{
			echo format_api(0,null, null, null);
		}
    	
	}

    function deleteDSBResto($app) {
		$data = '';
		$status = $sizedata = 0;
		$msg = "";
		$params = "";

		$params = $app->request()->getBody();

    	$header = array('Content-Type:text/plain'); 
		
		$apiUrl = 'https://api.weeloy.asia/dsbnewrestaurant';
            
       	$result  = remote_call($apiUrl,'POST',$params,$header);

		$data = $result;
	    $status = $data['statusCode'];
	    $sizedata = $data['sizedata'];
	    $msg = $data['message'];	   
	    
		echo format_api($status, json_decode($data['body']), $sizedata, $msg);
		
	}

	function sendemail($app)
	{
		$paramsx = json_decode($app->request()->getBody(),true);
		$code = $paramsx['code'];
		$restoemail = $paramsx['email'];
		$protocol = 'https://';
        if (strpos($_SERVER['HTTP_HOST'], 'localhost') !== false) {
            $protocol = 'http://';
            $url = $protocol . $_SERVER['HTTP_HOST'] . "/weeloy.com/api/dailyboard.php/md_restaurant/verifyemail/";
        }
        else
        {
        	$url = $protocol . $_SERVER['HTTP_HOST'] . "/api/dailyboard.php/md_restaurant/verifyemail/";	
        }

		$body = "";
		$body .= "<center><h1>Thank you for Registering for Daily Special Board<h1></center> <br/><br/> <center><a href ='".$url."".$code."'>Click here to verify Email</a></center>";
		$subject = "Email Verification";
		$emailsender = new JM_Mail;
  		$emailsender->sendmail($restoemail,$subject,$body,null);

		//check dns
		// $domain = substr(strrchr($restoemail, "@"), 1);
		// $result = dns_get_record($domain);
		// print_r($result);
		//get the ip
		// $ip = gethostbyname($domain);
		// print_r($ip);

		

	}

	//validate email address domain
	function validate_email($domain)
	{
		if(!checkdnsrr($domain,'MX'))
		{
			return 0;
		}
		else
		{
			return 1;
		}
	}
?>