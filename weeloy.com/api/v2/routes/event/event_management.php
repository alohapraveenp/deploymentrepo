<?php

require_once 'lib/class.event_management.inc.php';
require_once("conf/conf.session.inc.php");


//// EVENT MANAGEMENT
$app->get('/{event_id}/', function ($request, $response, $args) {
   return getEventManagement($request, $response, $args);
});

$app->post('/updateprice/', function ($request, $response, $args) {
   return updateEventPrice($request, $response, $args);
});
$app->post('/paymentdetails/', function ($request, $response, $args) {
   return saveEventPayment($request, $response, $args);
});

$app->post('/create/', function ($request, $response, $args) {
   return createEventManagement($request, $response, $args);
});

$app->get('/menu/{event_id}/', function ($request, $response, $args) {
   return getEventMenu($request, $response, $args);
});

$app->post('/menu/{event_id}/save/', function ($request, $response, $args) {
   return saveMenuSelection($request, $response, $args);
});
$app->post('/menu/notify/', function ($request, $response, $args) {
   return notifySaveMenuSelection($request, $response, $args);
});

$app->get('/modifydetails/{event_id}/', function ($request, $response, $args) {
   return getEventModifyDetails($request, $response, $args);
});
$app->get('/requestform/{restaurant}/', function ($request, $response, $args) {
   return getEventRequestList($request, $response, $args);
});





function getEventManagement($request, $response, $args) {
    try {
        $event_management = new WY_EventManagement();
        $resultat = $event_management->getEventProjet($args);
    

        if ($resultat) {
            $status = 1;
            $restaurant_id = $resultat['restaurant'];
            $res = new WY_restaurant;
            $res->getRestaurant($restaurant_id);
            $res->getEventTermsAndConditions($restaurant_id);
            $resultat['booked_by'] = (isset($res->mgr_name)) ? $res->mgr_name : "Restaurant Manager";
            $resultat['globaltnc'] = $res->tnc;
            $resultat['globalspltnc'] = $res->specific_tnc;
            $data = array('event' => $resultat);
            $count = count(1);
            $error = NULL;
            $http_status = 200;
            return format_response($response, $status, $data, $count, $error, $http_status);
            } else {
            $status = 0;
            $data = NULL;
            $count = 0;
            $error = array('message'=>'We couldn’t get the event details');
            $http_status = 404;
            return format_response($response, $status, $data, $count, $error, $http_status);
        }
    } catch (Exception $e) {
        echo format_response(0, $e->getMessage(), 0, null);
    }
}


function createEventManagement($request, $response) {
    $json = $request->getBody();
    $args = json_decode($json, true); 
    try {
        $restaurant_id = $args['restaurant'];
        
        $event_management = new WY_EventManagement($restaurant_id);
        $minmaxcover = $event_management->createEventProjet($args);

        if ($minmaxcover) {
            $status = 1;
            $data = array('minmaxpax' => $minmaxcover);
            $count = count(1);
            $error = NULL;
            $http_status = 200;
            return format_response($response, $status, $data, $count, $error, $http_status);
            } else {
            $status = 0;
            $data = NULL;
            $count = 0;
            $error = array('message'=>'We couldn’t get service categories');
            $http_status = 404;
            return format_response($response, $status, $data, $count, $error, $http_status);
        }
    } catch (Exception $e) {
        echo format_response(0, $e->getMessage(), 0, null);
    }

}

function getEventMenu($request, $response, $args) {
    try {
        $event_management = new WY_EventManagement();
        $resultat = $event_management->getEventMenu($args);

        if ($resultat) {
            $status = 1;
            $data = array('menu' => $resultat);
            $count = count(1);
            $error = NULL;
            $http_status = 200;
            return format_response($response, $status, $data, $count, $error, $http_status);
            } else {
            $status = 0;
            $data = NULL;
            $count = 0;
            $error = array('message'=>'We couldn’t get the event details');
            $http_status = 404;
            return format_response($response, $status, $data, $count, $error, $http_status);
        }
    } catch (Exception $e) {
        echo format_api_message(0, $e->getMessage(), 0, null);
    }
}

function saveMenuSelection($request, $response, $args) {
    $json = $request->getBody();
    $args_json = json_decode($json, true); 
    
    $params['event_id'] = $args['event_id'];
    $params['items'] = $args_json['items'];
    $params['status'] = $args_json['status'];
    
    try {
        $event_management = new WY_EventManagement();
        $resultat = $event_management->saveMenuSelection($params);

        if ($resultat) {
            $status = 1;
            $data = array('resultat' => 'success');
            $count = count(1);
            $error = NULL;
            $http_status = 200;
            return format_response($response, $status, $data, $count, $error, $http_status);
            } else {
            $status = 0;
            $data = NULL;
            $count = 0;
            $error = array('message'=>'We couldn’t get save menus');
            $http_status = 404;
            return format_response($response, $status, $data, $count, $error, $http_status);
        }
    } catch (Exception $e) {
        echo format_api_message(0, $e->getMessage(), 0, null);
    }
}

function notifySaveMenuSelection($request, $response, $args){
    $json = $request->getBody();
    $args_json = json_decode($json, true); 
    $params =$args_json['items'];
    
    try {
        $restaurant_id = $params['restaurant'];
        $event_management = new WY_EventManagement($restaurant_id);
        $minmaxcover = $event_management->notifyEventMangement($params,$params['notify_type']);
        $status = 1;
        $data = array('resultat' => 'success');
        $count = count(1);
        $error = NULL;
        $http_status = 200;
        return format_response($response, $status, $data, $count, $error, $http_status);
    } catch (Exception $e) {
        echo format_api_message(0, $e->getMessage(), 0, null);
    }
}
function updateEventPrice($request, $response, $args){
    $json = $request->getBody();
    $params = json_decode($json, true); 
   
    try {
        $restaurant_id = $params['restaurant'];
        $event_management = new WY_EventManagement($restaurant_id);
        $total_amount = $event_management->updateprice($params);
        $status = 1;
        $data = array('resultat' => 'success');
        $count = count(1);
        $error = NULL;
        $http_status = 200;
        return format_response($response, $status, $data, $count, $error, $http_status);
    } catch (Exception $e) {
        echo format_api_message(0, $e->getMessage(), 0, null);
    }
}
function saveEventPayment($request, $response, $args){
    $json = $request->getBody();
    $params = json_decode($json, true); 
    try {
        $restaurant_id = $params['restaurant'];
        $event_management = new WY_EventManagement($restaurant_id);
        $total_amount = $event_management->saveEvPaymentDetails($params);
    } catch (Exception $e) {
        echo format_api_message(0, $e->getMessage(), 0, null);
    }
}

function getEventModifyDetails($request, $response, $args){
    try {
        $event_management = new WY_EventManagement();
        $resultat = $event_management->getEventModifyDetails($args['event_id']);

        if ($resultat) {
            $status = 1;
            $data = array('details' => $resultat);
            $count = count(1);
            $error = NULL;
            $http_status = 200;
            return format_response($response, $status, $data, $count, $error, $http_status);
            } else {
            $status = 0;
            $data = NULL;
            $count = 0;
            $error = array('message'=>'We couldn’t get the event details');
            $http_status = 404;
            return format_response($response, $status, $data, $count, $error, $http_status);
        }
    } catch (Exception $e) {
        echo format_api_message(0, $e->getMessage(), 0, null);
    }

}
function getEventRequestList($request, $response, $args){
    try {
        $event_management = new WY_EventManagement();
        $resultat = $event_management->getRequestformList($args['restaurant']);

        if (true) {
            $status = 1;
            $data = array('details' => $resultat);
            $count = count($resultat);
            $error = NULL;
            $http_status = 200;
            return format_response($response, $status, $data, $count, $error, $http_status);
        } else { // this generate an error
            $status = 0;
            $data = NULL;
            $count = 0;
            $error = array('message'=>'We couldn’t get the event request details');
            $http_status = 404;
            return format_response($response, $status, $data, $count, $error, $http_status);
        }
    } catch (Exception $e) {
        echo format_api_message(0, $e->getMessage(), 0, null);
    }

}


