<?php

//// AWS email verification services

require_once 'conf/conf.sqs.inc.php';

$app->get('/verifyemail/status/{email}', function ($request, $response, $args) {
   return verifyEmailStatus($request, $response, $args);
});

$app->get('/verifyemail/{email}', function ($request, $response, $args) {
   return verifyEmail($request, $response, $args);
});


function verifyEmailStatus($request, $response, $args) {
    $email = $args['email'];

    if (preg_match('/,/', $args['email'])) {
          $emails = explode(",",$args['email']);
          $email =  $emails[0];
    }

    //$email = $args['email'];
   
    $restaurant = new WY_restaurant();
    $status_response = $restaurant->getAWSEmailVerificationStatus($email);
     
    if($status_response !== 'verified'){
        $ses = new SES();

        $res = $ses->getIdentityVerificationStatus($email);
        $status_response = false;
      
        if($res == 'Success'){
            $status_response = 'verified';
        }
        if($res == 'Pending'){
            $status_response = 'pending';
        } 
        if($res == 'Failed'){
            $status_response = 'failed';
        }  
        if($res === false){
            $status_response = 'unknown';
        }

        $restaurant->updateAWSEmailVerification($email, $status_response);
    }
    if ($status_response) {
        $status = 1;
        $data = array('status' => $status_response);
        $count = count(1);
        $error = NULL;
        $http_status = 200;
        return format_response($response, $status, $data, $count, $error, $http_status);
    } else {
        $status = 0;
        $data = NULL;
        $count = 0;
        $error = array('message'=>'We couldn’t get service categories');
        $http_status = 404;
        return format_response($response, $status, $data, $count, $error, $http_status);
    }
}


function verifyEmail($request, $response, $args) {
    $email = $args['email'];
    //$email = $args['email'];

    if (preg_match('/,/', $args['email'])) {
          $emails = explode(",",$args['email']);
          $email =  $emails[0];
    }
    $ses = new SES();
    
    $status_response = $ses->verifyEmailAddress($email);
    return verifyEmailStatus($request, $response, $args);
}