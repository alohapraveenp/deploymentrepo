<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once 'lib/class.restaurant.inc.php';
require_once 'lib/class.restaurant_cancel_policy.inc.php';


$app->get('/{restaurant}', function ($request, $response, $args) {
   return getBacchanaliapolicy($request, $response, $args );
});

$app->post('/getPolicy', function ($request, $response, $args) {
   return getBanCancelPolicy($request, $response, $request->getParsedBody());
});
$app->post('/read', function ($request, $response, $args) {
   return getPaymentCancelPolicy($request, $response, $request->getParsedBody());
});

function getBacchanaliapolicy($request, $response, $args) {
    $restaurant = $args['restaurant'];
    
    $policy = new WY_Restaurant_policy();
    $policyDetails = $policy->bacchanalia_clpolicy($restaurant);

    if (count($policyDetails)>0) {
        $status = 1;        
        $data = array('policyDetails' => $policyDetails);
        $count = count($policyDetails);
        $error = NULL;
        $http_status = 200;
        return format_response($response, $status, $data, $count, $error, $http_status);
                
    } else {
        $status = 0;        
        $data = NULL;
        $count = 0;
        $error = array('message'=>'We couldn’t get cancelpolicy');
        $http_status = 200;
        return format_response($response, $status, $data, $count, $error, $http_status);
    }
    
}

function getBanCancelPolicy($request, $response, $args){
 
   $restaurant = $args['restaurant'];
    $pax = $args['pax'];
    $date = $args['date'];
    $time = $args['time'];
    
    
     $price = new WY_Restaurant_policy();
    
    $priceDetails = $price->getBacchaDeadline($restaurant,$date,$time,$pax);
    
    if (count($priceDetails)>0) {
        $status = 1;        
        $data = array('priceDetails' => $priceDetails);
        $count = count($priceDetails);
        $error = NULL;
        $http_status = 200;
        return format_response($response, $status, $data, $count, $error, $http_status);
                
    } else {
        $status = 0;        
        $data = NULL;
        $count = 0;
        $error = array('message'=>'We couldn’t get price');
        $http_status = 200;
        return format_response($response, $status, $data, $count, $error, $http_status);
    }
    
}

function getPaymentCancelPolicy($request, $response, $args){
        
    $restaurant = $args['restaurant'];
    $type =  (isset($args['type'])) ? $args['type'] : "";  
    $amount =  (isset($args['amount'])) ? $args['amount'] : "0";  
    $product =  (isset($args['product'])) ? $args['product'] : "";  
    $date = (isset($args['date'])) ? $args['date'] : " "; 
    $pax = (isset($args['cover'])) ? $args['cover'] : "";  
    $time = (isset($args['time'])) ? $args['time'] : "";  
    
    try {
        $price = new WY_Restaurant_policy();
    
        $priceDetails = $price->getPaymentPolicy($restaurant,$amount,$date,$time,$pax,$product);
    
            if (count($priceDetails)>0) {
                $status = 1;        
                $data = array('priceDetails' => $priceDetails);
                $count = count($priceDetails);
                $error = NULL;
                $http_status = 200;
                return format_response($response, $status, $data, $count, $error, $http_status);

            } else {
                $status = 0;        
                $data = NULL;
                $count = 0;
                $error = array('message'=>'We couldn’t get price');
                $http_status = 200;
                return format_response($response, $status, $data, $count, $error, $http_status);
            }
        } catch (PDOException $e) { api_error($e, "getPaymentCancelPolicy"); }
   
    
}