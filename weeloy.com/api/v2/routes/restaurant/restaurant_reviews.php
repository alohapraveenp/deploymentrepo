<?php

require_once("lib/class.review.inc.php");
require_once("lib/class.media.inc.php");

$app->get('/{query}/{page}', 'getReviews');

$app->get('/{query}/list/summary', 'getReviewsList');


$app->get('/{restaurant}/list/{page}', function ($request, $response, $args) {    
   return getReviewsList($request, $response, $args);
});


function getReviewsList($request, $response, $args) {
    
    $restaurant = $args['restaurant'];
    $page = $args['page'];
    
    $media = new WY_Media();
    $item_per_page = 4;
    $limit = " LIMIT 0,3";
    if (!empty($page)) {
        $page = $page - 1;
        $item = $page * $item_per_page;
        $limit = " LIMIT $item,$item_per_page";
    }
    try {
        $reviews = new WY_Review($restaurant);
        $count_reviews = $reviews->getReviewsCount();
        $reviews->getReviewsList($limit);

        $count = 0;

        //unactive user picture
        foreach ($reviews->data as $review) {
            $count = $count + 1;
            //$review['user_picture'] = $media->getUserProfilePicture($review['email']);
            $review['user_picture'] = NULL;
            unset($review['email']);
            $review_res[] = $review;
        }
        if ($count > 0) {
            $status = 1;
            $data = array(
                "restaurant" => $reviews->restaurant,
                "count" => $count_reviews['count'],
                "score" => $count_reviews['score'],
                "score_desc" => $count_reviews['review_desc'],
                "reviews" => $review_res);

            $count = count($reviews->data);
            $error = NULL;
            $http_status = 200;
            return format_response($response, $status, $data, $count, $error, $http_status);
        } else {
                $status = 0;
                $data = array();
                $count = 0;
                $error = NULL;
                $http_status = 204;
                return format_response($response, $status, $data, $count, $error, $http_status);
        }
    } catch (PDOException $e) {
        api_error($e, "getReviews");
    }
}

