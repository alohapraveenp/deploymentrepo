<?php
require_once 'lib/class.service.inc.php';

//// RESTAURANT SERVICES
$app->get('/categories', function ($request, $response, $args) {
   return getServiceCategories($request, $response, $args );
});

$app->get('/services', function ($request, $response, $args) {
   return getServices($request, $response, $args );
});

$app->get('/{restaurant}[/{option}]', function ($request, $response, $args) {
   return getRestaurantServices($request, $response, $args);
});

//$app->get('/restaurantservices/{restaurant}/{option}', function ($request, $response, $args) {
//   return getRestaurantServices($request, $response, $args);
//});

$app->post('/save', function ($request, $response, $args) {
    return saveRestaurantServices($request, $response, $request->getParsedBody());
});


//// RESTAURANT SERVICE

function getServiceCategories($request, $response, $args) {
    
    $service = new WY_Service();
    $categories = $service->getCategories();

    if ($categories) {
        $status = 1;
        $data = array('categories' => $categories);
        $count = count($categories);
        $error = NULL;
        $http_status = 200;
        return format_response($response, $status, $data, $count, $error, $http_status);

        } else {
        $status = 0;
        $data = NULL;
        $count = 0;
        $error = array('message'=>'We couldn’t get service categories');
        $http_status = 404;
        return format_response($response, $status, $data, $count, $error, $http_status);
    }
}

function getServices($request, $response, $args) {
    $service = new WY_Service();
    $services = $service->getServices();

    if ($services) {
        $status = 1;
        $data = array('services' => $services);
        $count = count($services);
        $error = NULL;
        $http_status = 200;
        return format_response($response, $status, $data, $count, $error, $http_status);
    } else {
        $status = 0;
        $data = NULL;
        $count = 0;
        $error = array('message'=>'We couldn’t get services');
        $http_status = 404;
        return format_response($response, $status, $data, $count, $error, $http_status);
    }
}

function getRestaurantServices($request, $response, $args) {
    
    $restaurant = $args['restaurant'];
    $option = $args['option'];
    
    
    $service = new WY_Service();
    
    $restaurantservices = $service->getRestaurantServices($restaurant, $option);

    if ($restaurantservices) {
        $status = 1;        
        $data = array('restaurantservices' => $restaurantservices);
        $count = count($restaurantservices);
        $error = NULL;
        $http_status = 200;
        return format_response($response, $status, $data, $count, $error, $http_status);
                
    } else {
        $status = 0;        
        $data = NULL;
        $count = 0;
        $error = array('message'=>'We couldn’t get services');
        $http_status = 500;
        return format_response($response, $status, $data, $count, $error, $http_status);
    }
}

function saveRestaurantServices($request, $response, $args) {

    $restaurant = $args['restaurant'];
    $active_services = $args['active_services'];

    $service = new WY_Service();
    $resultat = $service->updateServices($restaurant, $active_services);

    if ($resultat) {
        $status = 1;        
        $data = true;
        $count = 1;
        $error = NULL;
        $http_status = 200;
        return format_response($response, $status, $data, $count, $error, $http_status);
        
        
    } else {
        $status = 0;
        $data = true;
        $count = 1;
        $error = array('message'=>'An error occured, please try again later');
        $http_status = 200;
        return format_response($response, $status, $data, $count, $error, $http_status);
    }
}