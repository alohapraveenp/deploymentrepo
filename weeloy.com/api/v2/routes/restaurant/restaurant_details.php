<?php
require_once 'lib/class.restaurant.inc.php';

//// RESTAURANT DETAILS
$app->get('/{restaurant}/min-max-pax', function ($request, $response, $args) {
   return getRestaurantMinMaxPax($request, $response, $args);
});


function getRestaurantMinMaxPax($request, $response, $args) {
    $restaurant_id = $args['restaurant'];
    $restaurant = new WY_restaurant();
    $minmaxcover = $restaurant->getRestaurantMinMaxPax($restaurant_id);

    if ($minmaxcover) {
        $status = 1;
        $data = array('minmaxpax' => $minmaxcover);
        $count = count(1);
        $error = NULL;
        $http_status = 200;
        return format_response($response, $status, $data, $count, $error, $http_status);
        } else {
        $status = 0;
        $data = NULL;
        $count = 0;
        $error = array('message'=>'We couldn’t get service categories');
        $http_status = 404;
        return format_response($response, $status, $data, $count, $error, $http_status);
    }
}