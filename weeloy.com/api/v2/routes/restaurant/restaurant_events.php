<?php

require_once("lib/class.event.inc.php");

//// RESTAURANT EVENTS
$app->get('/{restaurant}', function ($request, $response, $args) {
   return getEventsRestaurant($request, $response, $args);
});
   

$app->get('/active/{restaurant}', function ($request, $response, $args) {
    return getActiveEventsRestaurant($request, $response, $args);
});
        

//// RESTAURANT EVENTS

function getEventsRestaurant($request, $response, $args) {

    $restaurant = clean_text($args['restaurant']);
    $eobj = new WY_Event($restaurant);
    try {
        $obj = $eobj->getEvents();
        $event = $obj["event"];
        $status = 1;        
        $data = array("event" => $event);
        $count = count($event);
        $error = NULL;
        $http_status = 200;
        
        return format_response($response, $status, $data, $count, $error, $http_status);
        
    } catch (PDOException $e) {
        
        api_error($e, "getEventsRestaurant");
        $status = 0;        
        $data = NULL;
        $count = 0;
        $error = array('message'=>'We couldn’t get events');
        $http_status = 500;
        
        return format_response($response, $status, $data, $count, $error, $http_status);
    }
}




function getActiveEventsRestaurant($request, $response, $args) {

    $restaurant = clean_text($args['restaurant']);

    $eobj = new WY_Event($restaurant);
    try {
        $event = $eobj->getActiveEvents();
        $status = 1;        
        $data = array("event" => $event);
        $count = count($event);
        $error = NULL;
        $http_status = 200;
        return format_response($response, $status, $data, $count, $error, $http_status);
    } catch (PDOException $e) {
        api_error($e, "getActiveEventsRestaurant");
        $status = 0;        
        $data = NULL;
        $count = 0;
        $error = array('message'=>'We couldn’t get events');
        $http_status = 500;
        return format_response($response, $status, $data, $count, $error, $http_status);
    }
}