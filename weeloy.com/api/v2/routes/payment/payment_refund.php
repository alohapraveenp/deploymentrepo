<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once 'lib/class.restaurant.inc.php';
require_once 'lib/class.payment_stripe.inc.php';
require_once 'lib/class.evorder.inc.php';


$app->post('/{restaurant}', function ($request, $response, $args) {

    return depsoitRefund($request, $response, $request->getParsedBody());
});
$app->post('/eventbooking/', function ($request, $response, $args) {

    return eventDepsoitRefund($request, $response, $request->getParsedBody());
});


function depsoitRefund($request, $response, $args){

    $payment = new WY_Payment_Stripe();
    $payment_id = $args['deposit'];
    $restaurant = $args['restaurant'];
    $confirmation = $args['booking'];
    $email = $args['email'];
    $token ='';
    $amount = $args['amount'];
    $paymentDetails = $payment->stripeDepositRefund($payment_id, $restaurant, $confirmation, $email, $token, $amount);
    $status = 1;        
    $count = count($paymentDetails);
    $error = $paymentDetails['message'];
    $http_status = 200;
    return format_response($response, $status, $paymentDetails, $count, $error, $http_status);
    
}

function eventDepsoitRefund($request, $response, $args){

    $payment = new WY_Payment_Stripe();
    $event = new WY_Event();
    $evorder = new WY_EvOrder();

    $restaurant = $args['restaurant'];
    $order_id = $args['order_id'];

    $ev = $event->getEventTicket($order_id);
    
    $payment_id ='';
    
    if(count($ev) > 0){
        $payment_id = $ev['paykey'];
    }
    
    $email = $args['email'];
    $token ='';
    $amount = $args['amount'];
    $payment_method = $ev['payment_method'];
    $is_update = false;

    if(intval($amount) > 0 ){
        if($payment_method === 'creditcard'){
            $paymentDetails = $payment->stripeDepositRefund($payment_id, $restaurant, $order_id, $email, $token, $amount,'event_booking');
        }
//        else{
//            $paymentDetails = $pay->depositrefund($deposit, $restaurant, $confirmation, $email, $token,$amount, $type);
//        }
  
        if($paymentDetails['status'] == 'succeeded' || $paymentDetails['status'] == 'REFUNDED' ){
           $is_update = true;
        }
    }else{
         $is_update = true;
    }
    if($is_update){
        $evorder->updateEventOrder($payment_id,'cancel',$ev->object_id);
    }
    
//    if(trim($payment_method) === "stripe"){
//                  
//                $resData = $stripe->stripeDepositRefund($deposit, $restaurant, $confirmation, $email, $token, $amount);
//                $status =$resData['status'];
//
//            }
//            else if(trim($payment_method) === "carddetails"){
//
//                 $resData = $stripe->createStripePayment($deposit, $restaurant, $confirmation, $email,$amount, $type);
//                 $status =$resData['status'];
//
//            }else{ $status = $pay->depositrefund($deposit, $restaurant, $confirmation, $email, $token,$amount, $type);
//            $resData['status'] =$status;
//            $resData['message'] ='success';
//            }

    $status = 1;        
    $count = count($paymentDetails);
    $error = $paymentDetails['message'];
    $http_status = 200;
    return format_response($response, $status, $paymentDetails, $count, $error, $http_status);
    
}
