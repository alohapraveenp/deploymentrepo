<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once 'lib/class.restaurant.inc.php';
require_once 'lib/class.payment.inc.php';


$app->get('/{payment_id}',function ($request, $response, $args) {
  return getEventPaymentDetails($request, $response, $args);
});
$app->get('/{restaurant}/',function ($request, $response, $args) {
  return getRestaurantPayment($request, $response, $args);
});



function getEventPaymentDetails($request, $response, $args) {
    try {
        
        $payment = new WY_Payment();
        $resultat = $payment->getPaymentDetails($args['payment_id']);
        
       
        if ($resultat) {
            $status = 1;
            $data = array('payment' => $resultat);
            $count = count($data);
            $error = NULL;
            $http_status = 200;
            return format_api_message($status, $data, $count, $error);
            } else {
            $status = 0;
            $data = NULL;
            $count = 0;
            $error = array('message'=>'We couldn’t get the payment details');
            $http_status = 404;
            return format_api_message($status, $data, $count, $error);
        }
    } catch (Exception $e) {
        echo format_response(0, $e->getMessage(), 0, null);
    }
}

function getRestaurantPayment($request, $response, $args) {
    try {
        
        $payment = new WY_Payment();
        $resultat = $payment->getRestaurantPayment($args['restaurant']);
        
       
        if ($resultat) {
            $status = 1;
            $data = array('payment' => $resultat);
            $count = count($data);
            $error = NULL;
            $http_status = 200;
            return format_api_message($status, $data, $count, $error);
            } else {
            $status = 0;
            $data = NULL;
            $count = 0;
            $error = array('message'=>'We couldn’t get the payment details');
            $http_status = 404;
            return format_api_message($status, $data, $count, $error);
        }
    } catch (Exception $e) {
        echo format_response(0, $e->getMessage(), 0, null);
    }
    
}