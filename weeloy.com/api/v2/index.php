<?php

/**
 * Step 1: Require the Slim Framework using Composer's autoloader
 *
 * If you are not using Composer, you need to load Slim Framework with your own
 * PSR-4 autoloader.
 */
header("Access-Control-Allow-Origin: *");

require_once("conf/conf.init.inc.php");
require_once("lib/wpdo.inc.php");
require_once("lib/wglobals.inc.php");
require_once("lib/class.coding.inc.php");
require_once("api.utilities.inc.php");

require 'vendor/autoload.php';




/**
 * Step 2: Instantiate a Slim application
 *
 * This example instantiates a Slim application using
 * its default settings. However, you will usually configure
 * your Slim application now by passing an associative array
 * of setting names and values into the application constructor.
 */


// Register service provider with the container
$container = new \Slim\Container;
$container['cache'] = function () {
    return new \Slim\HttpCache\CacheProvider();
};

// Add middleware to the application
$app = new \Slim\App($container);
$app->add(new \Slim\HttpCache\Cache('public', 86400));


//$app = new Slim\App();

/**
 * Step 3: Define the Slim application routes
 *
 * Here we define several Slim application routes that respond
 * to appropriate HTTP request methods. In this example, the second
 * argument for `Slim::get`, `Slim::post`, `Slim::put`, `Slim::patch`, and `Slim::delete`
 * is an anonymous function.
 */

// API group  
$app->group('/restaurant', function () use ($app) {
    $app->group('/details', function () use ($app) {
        require 'routes/restaurant/restaurant_details.php';
    });
    $app->group('/events', function () use ($app) {
        require 'routes/restaurant/restaurant_events.php';
    });
    
    $app->group('/services', function () use ($app) {
        require 'routes/restaurant/restaurant_services.php';
    });
    $app->group('/reviews', function () use ($app) {
        require 'routes/restaurant/restaurant_reviews.php';
    });
    $app->group('/groups', function () use ($app) {
        require 'routes/restaurant/restaurant_groups.php';
    });
     $app->group('/sections', function () use ($app) {
        require 'routes/restaurant/restaurant_sections.php';
    });
     $app->group('/cancelpolicy', function () use ($app) {
        require 'routes/restaurant/restaurant_cancelpolicy.php';
    });
});

$app->group('/booking', function () use ($app) {
//    $app->group('/details', function () use ($app) {
//        require 'routes/booking/booking_details.php';
//    });
    $app->group('/consumer', function () use ($app) {
        require 'routes/booking/booking_consumer.php';
    });
//    $app->group('/restaurant', function () use ($app) {
//        require 'routes/booking/booking_restaurant.php';
//    });
});

$app->group('/system', function () use ($app) {
    $app->group('/tracking', function () use ($app) {
        require 'routes/system/system_tracking.php';
    });
    $app->group('/secure', function () use ($app) {
        require 'routes/system/system_secure.php';
    });
    $app->group('/version', function () use ($app) {
        require 'routes/system/system_app_version.php';
    });
});

$app->group('/notification', function () use ($app) {
    $app->group('/sms', function () use ($app) {
        require 'routes/notification/notification_sms.php';
    });
});

$app->group('/admin', function () use ($app) {
    $app->group('/restaurant', function () use ($app) {
        require 'routes/admin/restaurant.php';
    });
});

$app->group('/external_website', function () use ($app) {
    $app->group('/info', function () use ($app) {
        require 'routes/external_website/restaurant.php';
    });
});



$app->group('/payment', function () use ($app) {
    $app->group('/paypal', function () use ($app) {
        require 'routes/payment/paypal.php';
    });
    $app->group('/refund', function () use ($app) {
        require 'routes/payment/payment_refund.php';
    });
    $app->group('/depositdetails', function () use ($app) {
        require 'routes/payment/payment_depositdetails.php';
    });
    $app->group('/makepayment', function () use ($app) {
        require 'routes/payment/payment_makepayment.php';
    });
   $app->group('/method', function () use ($app) {
    require 'routes/payment/payment_method.php';
    });
});




$app->group('/event', function () use ($app) {
    $app->group('/management', function () use ($app) {
        require 'routes/event/event_management.php';
    });
});

/**
 * Step 4: Run the Slim application
 *
 * This method should be called last. This executes the Slim application
 * and returns the HTTP response to the HTTP client.
 */
$app->run();
