<?php
	// header('Access-Control-Allow-Origin: *');
	// header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
	// header('Access-Control-Allow-Headers: origin, x-requested-with, content-type');
	require_once("conf/conf.init.inc.php");
	require_once("lib/wpdo.inc.php");
	require_once("lib/class.tool.inc.php");
	require_once("lib/Browser.inc.php");
	require_once("lib/class.mail.inc.php");
	require_once("lib/class.async.inc.php");
	require_once("lib/class.spool.inc.php");
	require_once("lib/wglobals.inc.php");
	require_once("lib/class.dailyspecials.inc.php");
	// require_once("lib/class.restaurant.inc.php");

   	$app->post('/authenticate', function() use ($app) {
		return authenticate($app);
   	});

   	$app->post('/exportwdates', function() use ($app) {
		return exportwdates($app);
   	});

   	function exportwdates($app) {
		$data = '';
		$status = $sizedata = 0;
		$msg = "";
		$params = "";

		$params = $app->request()->getBody();

    	$header = array('Content-Type:text/plain'); 
		
		$apiUrl = 'https://api.weeloy.asia/exportprofile';
            
       	$result  = remote_call($apiUrl,'POST',$params,$header);

		$data = $result;
	    $status = $data['statusCode'];
	    $sizedata = $data['sizedata'];
	    $msg = $data['message'];
	    
		echo format_api($status, json_decode($data['body']), $sizedata, $msg);
		
	}

    function authenticate($app) {
		$data = '';
		$status = $sizedata = 0;
		$msg = "";
		$params = "";

		$params = $app->request()->getBody();

    	$header = array('Content-Type:text/plain'); 
		
		$apiUrl = 'https://api.weeloy.asia/authenticate';
            
       	$result  = remote_call($apiUrl,'POST',$params,$header);

		$data = $result;
	    $status = $data['statusCode'];
	    $sizedata = $data['sizedata'];
	    $msg = $data['message'];
	    
		echo format_api($status, json_decode($data['body']), $sizedata, $msg);
		
	}

	
?>