<?php

require_once "conf/conf.init.inc.php";
require_once "lib/wpdo.inc.php";
require_once "lib/wglobals.inc.php";
require_once "conf/conf.session.inc.php";
require_once "lib/class.coding.inc.php";
require_once "lib/class.login.inc.php";
require_once "lib/class.restaurant.inc.php";
require_once "lib/class.member.inc.php";
require_once "lib/class.images.inc.php";
require_once "lib/class.media.inc.php";
require_once "lib/imagelib.inc.php";
require_once "lib/class.mail.inc.php";
require_once "lib/class.sms.inc.php";
require_once "lib/class.pushnotif.inc.php";
require_once "lib/class.debug.inc.php";


require_once "lib/Browser.inc.php";

require_once "lib/class.spool.inc.php";
require_once "lib/class.async.inc.php";

define("__MEDIA__SERVER_NAME__", 'https://media.weeloy.com/upload/restaurant/');

require 'Slim/Slim.php';

\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();

//$app->response()->header('Content-Type', 'application/json;charset=utf-8');
$app->contentType('application/json;charset=utf-8');


// API for Project-MD - start

$app->group('/md_restaurant', function () use ($app) {
    require "dailyboard_routes/restaurants/md_restaurants.php";  
 
});

$app->group('/md_dailyspecial', function () use ($app) {
    require_once 'dailyboard_routes/dailyspecial/md_dailyspecial.php';  

});

function api_error($e, $funcname) {

    $trace = preg_replace("/\'|\"/", "`", $e->getTraceAsString());
    $traceAr = $e->getTrace();
    $file = preg_replace("/^.*\//", "", $traceAr[0]['file']);
    $msg = $e->getMessage() . " - " . $file . " - " . $traceAr[0]['line'];
	WY_debug::recordDebug("ERROR-API", "API-" . strtoupper($funcname), $msg . " \n " . $trace);
    echo format_api(0, 0, 0, $funcname . "->" . $msg);
}

function format_api($status, $data, $count, $errors, $header_status = 200) {
        
	set_header_status($header_status);
	
	$resultat['status'] = $status;
	$resultat['data'] = $data;
	$resultat['count'] = $count;
	$resultat['errors'] = $errors;
	return json_encode($resultat);
}

function is_email_valid($email) {
    return !(filter_var($email, FILTER_VALIDATE_EMAIL) === false);
}

function remote_call($apiUrl,$mode,$params ='',$header = array()) {

       	$ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, $apiUrl );
	    curl_setopt($ch, CURLOPT_VERBOSE, 1);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
	    curl_setopt($ch, CURLOPT_FRESH_CONNECT, TRUE);
	    curl_setopt($ch, CURLOPT_POST, true );
	    if ($params != '') {
	    	curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
	    }

        $response_data = curl_exec($ch);
		curl_close( $ch );
	    
	    $response_data = json_decode($response_data, TRUE);

	    return $response_data;

}

function remote_callget($apiUrl,$params ='',$header = array()) {

       	$ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, $apiUrl );
	    curl_setopt($ch, CURLOPT_VERBOSE, 1);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
	    curl_setopt($ch, CURLOPT_FRESH_CONNECT, TRUE);

        $response_data = curl_exec($ch);
		curl_close( $ch );
	    
	    $response_data = json_decode($response_data, TRUE);

	    return $response_data;

}
    
function set_header_status($code) {
	$status = array(
		100 => 'Continue',
		101 => 'Switching Protocols',
		200 => 'OK',
		201 => 'Created',
		202 => 'Accepted',
		203 => 'Non-Authoritative Information',
		204 => 'No Content',
		205 => 'Reset Content',
		206 => 'Partial Content',
		300 => 'Multiple Choices',
		301 => 'Moved Permanently',
		302 => 'Found',
		303 => 'See Other',
		304 => 'Not Modified',
		305 => 'Use Proxy',
		306 => '(Unused)',
		307 => 'Temporary Redirect',
		400 => 'Bad Request',
		401 => 'Unauthorized',
		402 => 'Payment Required',
		403 => 'Forbidden',
		404 => 'Not Found',
		405 => 'Method Not Allowed',
		406 => 'Not Acceptable',
		407 => 'Proxy Authentication Required',
		408 => 'Request Timeout',
		409 => 'Conflict',
		410 => 'Gone',
		411 => 'Length Required',
		412 => 'Precondition Failed',
		413 => 'Request Entity Too Large',
		414 => 'Request-URI Too Long',
		415 => 'Unsupported Media Type',
		416 => 'Requested Range Not Satisfiable',
		417 => 'Expectation Failed',
		500 => 'Internal Server Error',
		501 => 'Not Implemented',
		502 => 'Bad Gateway',
		503 => 'Service Unavailable',
		504 => 'Gateway Timeout',
		505 => 'HTTP Version Not Supported');
        
        header("HTTP/1.1 $code " . $status[$code]);
        return true;
    }
    
    $app->run();
// API for Project-MD - finish

?>
