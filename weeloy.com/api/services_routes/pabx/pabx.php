<?php

$app->post('/list/', function () use ($app) {
	$data = json_decode($app->request()->getBody(), true);
	getCalls($data['token']);
	});

$app->post('/read/', function () use ($app) {
	$data = json_decode($app->request()->getBody(), true);
	readPabx($data['to'], $data['token']);
	});

$app->post('/fixline/',function () use ($app) {
	$phone = $app->request->post("phone");
	$token = $app->request->post("token");
    return updatefixline($phone, $token);
});

$app->post('/getaccount/',function () use ($app) {
	$data = json_decode($app->request()->getBody(), true);
    return getAccountInfo($data['token']);
});
function getAccountInfo($token) {
	$login_type = 'backoffice';
	$mode = 'api-ajax';
	try {
		$login = new WY_Login(set_valid_login_type($login_type));
		if ($token != "callcenter" && $login->checktoken($token) == false) {	// temporary hack as callcenter has no login !!
			error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": bad token: ".$token);
			echo format_api(-1, '', 0, 'bad token ' . $token);
			return;
		}
		$pabx = new WY_Pabx;
		$data = $pabx->getAccountLine($login->email);
		if ($pabx->result > 0) 
			echo format_api(1, $data, 1, "");
		else {
			error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": nothing configured for email: ".$login->email);
			echo format_api(-1, "error", 0, "Nothing configured for email ".$login->email);
		}
	} catch (PDOException $e) { api_error($e, "getAccountLine"); }
}
function getCalls($token) {
	try {
		$login_type = 'backoffice';
		$login = new WY_Login(set_valid_login_type($login_type));
		if ($token != "callcenter" && $login->checktoken($token) == false) {	// temporary hack as callcenter has no login !!
			error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": bad token: ".$token);
			echo format_api(-1, '', 0, 'bad token ' . $token);
			return;
		}
		$pabx = new WY_Pabx();
		$data = $pabx->getCalls($login->email);
		error_log(count($data)." calls: ");//.print_r($data, true));
		$msg = $pabx->msg;
		$status = $pabx->result;
		if ($pabx->result > 0) 
			$zdata = count($data);
		error_log("msg: ".$msg.", status: ".$status);
		echo format_api($data ? 1 : 0, $data, count($data), $msg);
	} catch (PDOException $e) { api_error($e, "getCalls"); }
}

function readPabx($fixline, $token) {
	$data = null;
	$status = $zdata = 0;
	$msg = "";
	$login_type = 'translation';
	$mode = 'api-ajax';	
	try {
		$pabx = new WY_Pabx();
		$data = $pabx->readPabx($fixline);
		$msg = $pabx->msg;
		$status = $pabx->result;
		if ($pabx->result > 0) 
			$zdata = count($data);
		echo format_api($status, $data, $zdata, $msg);
	} catch (PDOException $e) { api_error($e, "readPabx"); }
}

function updatefixline($fixline, $token) {
	$login_type = 'backoffice';
	$mode = 'api-ajax';
	try {
		$login = new WY_Login(set_valid_login_type($login_type));
		if ($token != "callcenter" && $login->checktoken($token) == false) {	// temporary hack as callcenter has no login !!
			echo format_api(-1, '', 0, 'bad token');
			return;
			}
		$result = WY_Pabx::updateUserfixline($fixline, $login->email);
		if ($result > 0) 
			echo format_api(1, "done", 1, "updated " . $fixline);
		else echo format_api(-1, "error", 0, "Not updated " . $fixline);
	} catch (PDOException $e) { api_error($e, "updatefixline"); }
}
?>