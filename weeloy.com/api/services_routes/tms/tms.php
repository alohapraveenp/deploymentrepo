<?

$app->post('/gettmssection/', function () use ($app) {
	$data = json_decode($app->request()->getBody(), true);
	gettmsSection($data['email'], $data['token']);
	});

$app->post('/gettmscalendar/', function () use ($app) {
	$data = json_decode($app->request()->getBody(), true);
	gettmsCalendar($data['restaurant'], $data['token']);
	});

$app->post('/settmscalendar/', function () use ($app) {
	$data = json_decode($app->request()->getBody(), true);
	settmsCalendar($data['restaurant'], $data['token'], $data['obj']);
	});

$app->post('/savetmslayout/', function () use ($app) {
	$data = json_decode($app->request()->getBody(), true);
	savetmslayout($data['email'], $data['token'], $data['obj']);
	});


$app->post('/deletetmslayout/', function () use ($app) {
    $data = json_decode($app->request()->getBody(), true);
    deletetmslayout($data['email'], $data['token'], $data['obj']);
});


$app->post('/renametmslayout/', function () use ($app) {
    $data = json_decode($app->request()->getBody(), true);
    renametmslayout($data['email'], $data['token'], $data['obj']);
});


$app->post('/savetmstablesetting/', function () use ($app) {
	$data = json_decode($app->request()->getBody(),true);
	savetmsTableSetting($data['email'], $data['token'], $data['obj']);
});

$app->post('/gettmsblock/', function () use ($app) {
	$data = json_decode($app->request()->getBody(), true);
	gettmsBlock($data['restaurant'], $data['token']);
	});

$app->post('/settmsblock/', function () use ($app) {
	$data = json_decode($app->request()->getBody(), true);
	settmsBlock($data['restaurant'], $data['token'], $data['obj']);
	});

$app->post('/syncninit/', function () use ($app) {
	$data = json_decode($app->request()->getBody(), true);
	syncninit($data['restaurant'], $data['user'], $data['token']);
	});


function gettmsSection($email, $token) {
        
	$data = null;
	$status = $zdata = 0;
	$msg = "";
	$login_type = 'translation';
	$mode = 'api-ajax';
	
	$log = new WY_Login(set_valid_login_type($login_type));
	$log->check_login_remote($email, $token, $mode);
	$msg = $log->msg;
	if ($log->result > 0) {
		$tms = new WY_Tms;
		$profile = $tms->readprofile($email);
		$msg = $tms->msg;
		$status = $tms->result;
		if ($tms->result > 0) {
			$profile['callcenter'] = tokenize($email);
			$data = array();
			foreach($profile as $label => $value) {
				$data[$label] = $value;
				}
			$data[$label] = $value;
			$zdata = count($profile);
		}
	}
	echo format_api($status, $data, $zdata, $msg);
}
    

function gettmsCalendar($restaurant, $token) {
	$data = null;
	$status = $zdata = 0;
	$msg = "";
	$login_type = 'translation';
	$mode = 'api-ajax';
        
	$login = new WY_Login(set_valid_login_type($login_type));
	if ($token != "callcenter" && $login->checktoken($token) == false) {	// temporary hack as callcenter has no login !!
		echo format_api(-1, '', 0, 'bad token');
		return;
		}
	$tms = new WY_Tms;
	$data = $tms->getCalendar($restaurant);
	$status = $tms->result;
	$msg = $tms->msg;
    echo format_api($status, $data, count($data), $msg);
}

function settmsCalendar($restaurant, $token,  $objstr) {
	$data = null;
	$status = $zdata = 0;
	$msg = "";
	$login_type = 'translation';
	$mode = 'api-ajax';
        
	$login = new WY_Login(set_valid_login_type($login_type));
	if ($token != "callcenter" && $login->checktoken($token) == false) {	// temporary hack as callcenter has no login !!
		echo format_api(-1, '', 0, 'bad token');
		return;
		}
	$tms = new WY_Tms;
	$tms->setCalendar($restaurant,  $objstr);
	$status = $tms->result;
	$msg = $tms->msg;
    echo format_api($status, "", 0, $msg);
}

function gettmsBlock($restaurant, $token) {
	$data = null;
	$status = $zdata = 0;
	$msg = "";
	$login_type = 'translation';
	$mode = 'api-ajax';
        
	$login = new WY_Login(set_valid_login_type($login_type));
	if ($token != "callcenter" && $login->checktoken($token) == false) {	// temporary hack as callcenter has no login !!
		echo format_api(-1, '', 0, 'bad token');
		return;
		}
	$tms = new WY_Tms;
	$data = $tms->getBlock($restaurant);
	$status = $tms->result;
	$msg = $tms->msg;
    echo format_api($status, $data, count($data), $msg);
}

function settmsBlock($restaurant, $token,  $objstr) {
	$data = null;
	$status = $zdata = 0;
	$msg = "";
	$login_type = 'translation';
	$mode = 'api-ajax';
        
	$login = new WY_Login(set_valid_login_type($login_type));
	if ($token != "callcenter" && $login->checktoken($token) == false) {	// temporary hack as callcenter has no login !!
		echo format_api(-1, '', 0, 'bad token');
		return;
		}
	$tms = new WY_Tms;
	$tms->setBlock($restaurant,  $objstr);
	$status = $tms->result;
	$msg = $tms->msg;
    echo format_api($status, "", 0, $msg);
}

function savetmsTableSetting($email, $token,  $objstr) {
	$data = null;
	$status = $zdata = 0;
	$msg = "";
	$login_type = 'translation';
	$mode = 'api-ajax';
        
    $log = new WY_Login(set_valid_login_type($login_type));
    $log->check_login_remote($email, $token, $mode);
    $msg = $log->msg;
	if($log->result > 0) {
		$mobj = new WY_Booking();
                $mobj->savetableSetting($objstr);
		$status = $mobj->result;
		$msg = $mobj->msg;
		}
    echo format_api($status, $data="ok", $zdata=1, $msg);
}

function deletetmslayout($email, $token,  $objstr) {
	$data = null;
	$status = $zdata = 0;
	$msg = "";
	$login_type = 'translation';
	$mode = 'api-ajax';
		
    $log = new WY_Login(set_valid_login_type($login_type));
    $log->check_login_remote($email, $token, $mode);
    $msg = $log->msg;
	if ($log->result > 0) {
        $tms = new WY_Tms;
        $tms->deleteLayout($objstr);
 		$status = $tms->result;
	    $msg = $tms->msg;
	   	}
    echo format_api($status, $data = "ok", $zdata = 1, $msg);
}


function renametmslayout($email, $token,  $objstr) {
	$data = null;
	$status = $zdata = 0;
	$msg = "";
	$login_type = 'translation';
	$mode = 'api-ajax';

    $log = new WY_Login(set_valid_login_type($login_type));
    $log->check_login_remote($email, $token, $mode);
    $msg = $log->msg;
    if ($log->result > 0) {
        $tms = new WY_Tms;
        $tms->renameLayout($objstr);
 		$status = $tms->result;
	    $msg = $tms->msg;
    	}
    echo format_api($status, $data = "ok", $zdata = 1, $msg);
}


function savetmslayout($email, $token,  $objstr) {
	$data = null;
	$status = $zdata = 0;
	$msg = "";
	$login_type = 'translation';
	$mode = 'api-ajax';
		
    $log = new WY_Login(set_valid_login_type($login_type));
    $log->check_login_remote($email, $token, $mode);
    $msg = $log->msg;
    if ($log->result > 0) {
        $tms = new WY_Tms;
        $tms->saveLayout($objstr);
 		$status = $tms->result;
	    $msg = $tms->msg;
        }
    echo format_api($status, $data = "ok", $zdata = 1, $msg);
    }
    
function syncninit($restaurant, $email, $token) {
	$data = null;
	$status = $zdata = 0;
	$msg = "";
	$login_type = 'translation';
	$mode = 'api-ajax';
		
    $log = new WY_Login(set_valid_login_type($login_type));
    $log->check_login_remote($email, $token, $mode);
    $msg = $log->msg;
    if ($log->result > 0) {
		$out = array();
		exec("ps -e | grep php", $out);
		$ps = implode(" ", $out);
		$state = (preg_match("/server.php/", $ps)) ? "YES" : "NO";
		error_log('SYNC_INI ' . $state . ' ' . exec("pwd"));
		if($state === "NO")
			exec('php ../modules/sockets/server.php > /dev/null 2>&1 &');
	    echo format_api(1, $state, 1, $msg);
        }
    else echo format_api(-1, "", 0, $msg);
    }
    


?>