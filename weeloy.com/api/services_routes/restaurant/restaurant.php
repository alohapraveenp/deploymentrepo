<?

$app->post('/allnames/', function () use ($app) {
  $data = json_decode($app->request()->getBody(),true);
  getRestaurantNames($data['email'], $data['token']);
});

function getRestaurantNames($email, $token) {
	$data = null;
	$status = $zdata = 0;
	$login_type = 'backoffice';
	$mode = $msg = "";
		
    try {
	    $log = new WY_Login(set_valid_login_type($login_type));
		$log->check_login_remote($email, $token, $mode);
		$msg = $log->msg;
		error_log('LOGIN readInvCon '. $log->result . " $email, $token");
		if ($log->result > 0) {
			$resto = new WY_Restaurant();
			$obj = $resto->readAllNames();
			$status = $resto->result;
			$msg = $resto->msg;
			if ($resto->result > 0) {
				$data = array('names' => $obj);
				$zdata = count($obj);
				}
			}
	} catch (PDOException $e) { api_error($e, "readRestoAllNames"); }
	echo format_api($status, $data, $zdata, $msg);
}
?>