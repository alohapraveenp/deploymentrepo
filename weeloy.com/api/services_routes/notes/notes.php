<?php

$app->post('/read/', function () use ($app) {
  $data = json_decode($app->request()->getBody(),true); 
  readNotes($data['channel'], $data['app'], $data['mode'], $data['token']);
});

$app->post('/write/', function () use ($app) {
  $data = json_decode($app->request()->getBody(),true);  
  writeNotes($data['channel'], $data['app'], $data['mode'], $data['content'], $data['token']);
});

function readNotes($restaurant, $app, $mode, $token) {
	$data = "";
	$status = -1;
	$zdata = 0;
	$login_type = 'backoffice';
	$msg = "";
    try {
		$log = new WY_Login(set_valid_login_type($login_type));
		$log->checktoken($token);
		$msg = $log->msg;
		if ($log->result) {
			$notes = new WY_Notes($restaurant, $app, $mode);
			$content = $notes->readNotes();
			$status = $notes->result;
			$msg = $notes->msg;
			if ($notes->result > 0) {
				$data = array('restaurant' => $restaurant, 'content' => $content);
				$zdata = count($data);
				}
			}
	} catch (PDOException $e) { api_error($e, "readNotes"); }
	echo format_api($status, $data, $zdata, $msg);
}

function writeNotes($restaurant, $app, $mode, $content, $token) {
	$data = null;
	$status = -1;
	$zdata = 0;
	$login_type = 'backoffice';
	$msg = "";
		
    try {
		$log = new WY_Login(set_valid_login_type($login_type));
		$log->checktoken($token);
		$msg = $log->msg;
		if ($log->result > 0) {
			$notes = new WY_Notes($restaurant, $app, $mode);
			$notes->writeNotes($content);
			$status = $notes->result;
			$msg = $notes->msg;
			$zdata = 1;
			}
	} catch (PDOException $e) { api_error($e, "writeNotes"); }
    echo format_api($status, "", $zdata, $msg);
}


?>