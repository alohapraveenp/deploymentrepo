<?

$app->post('/list/', function () use ($app) {
    $data = json_decode($app->request()->getBody(),true);
    getCancelPolicy($data);
});

$app->post('/save/', function () use ($app) {
  $data = json_decode($app->request()->getBody(),true);
  saveCancelPolicy( $data);
});
$app->post('/update/', function () use ($app) {
  $data = json_decode($app->request()->getBody(),true);
  updateCancelPolicy( $data);
});
$app->post('/delete/', function () use ($app) {
  $data = json_decode($app->request()->getBody(),true);
  deleteCancelPolicy( $data);
});


function saveCancelPolicy($params){
    $result = null;
    $mode = $msg = "";
   
    try {
		$res = new WY_restaurant();
	   
		$result = $res->saveCancelPolicy($params['data']);
		$status = 1;
        } catch (PDOException $e) { api_error($e, "saveCancelPolicy"); }
    echo format_api($status, $result, $zdata = 1, $msg);
}
function updateCancelPolicy($params){
    $result = null;
    $mode = $msg = "";
   
    try {
        $res = new WY_restaurant();
        $result = $res->updateCancelPolicy($params['data']);
        $status = 1;
        } catch (PDOException $e) { api_error($e, "updateCancelPolicy"); }
    echo format_api($status, $result, $zdata = 1, $msg);
}
function deleteCancelPolicy($params){
    $result = null;
    $mode = $msg = "";
   
    try {
        $res = new WY_restaurant();
        $result = $res->deleteCancelPolicy($params['data']);
        $status = 1;
        } catch (PDOException $e) { api_error($e, "deleteCancelPolicy"); }
    echo format_api($status, $result, $zdata = 1, $msg);
}

function getCancelPolicy($data){

    $restaurant = $data['restaurant'];
    $type =  (isset($data['type'])) ? $data['type'] : "";  
    $amount =  (isset($data['amount'])) ? $data['amount'] : "0";  
    $product =  (isset($data['product'])) ? $data['product'] : "";  
    $pax =  (isset($data['pax'])) ? $data['pax'] : "";  
    $date =  (isset($data['date'])) ? $data['date'] : "";  
    $time =  (isset($data['time'])) ? $data['time'] : ""; 

           
    $data = null;
    //$login_type = $platform = 'backoffice';
    $mode = $msg = "";
    //$log = new WY_Login(set_valid_login_type($login_type));
    try {
		$res = new WY_restaurant();  
		$data = $res->getCancelPolicyAll($restaurant,$type,$amount,$product,$date,$time);
		$status = 1;
		$msg = '';
    } catch (PDOException $e) { api_error($e, "getCancelPolicy"); }
    echo format_api($status, $data, $zdata = 1, $msg);
}



?>