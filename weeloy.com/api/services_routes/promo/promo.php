<?

$app->post('/read/', function () use ($app) {
  $data = json_decode($app->request()->getBody(),true);
  readPromo($data['restaurant'], $data['email'], $data['token']);
});

$app->post('/create/', function () use ($app) {
  $data = json_decode($app->request()->getBody(),true);
  createPromo($data['restaurant'], $data['email'], $data['token'], $data['promos']);
});

$app->post('/update/', function () use ($app) {
  $data = json_decode($app->request()->getBody(),true);
  updatePromo($data['restaurant'], $data['email'], $data['token'], $data['promos']);
});

$app->post('/delete/', function () use ($app) {
  $data = json_decode($app->request()->getBody(),true);
  deletePromo($data['restaurant'], $data['email'], $data['token'], $data['promoID']);
});

$app->post('/setdefault/', function () use ($app) {
  $data = json_decode($app->request()->getBody(),true);
  setdefaultPromo($data['restaurant'], $data['email'], $data['token'], $data['promoID']);
});

$app->post('/setuppromo/', function () use ($app) {
  $data = json_decode($app->request()->getBody(),true);
  setupPromo($data['restaurant'], $data['email'], $data['token'], $data['promoID'], $data['value']);
});


function readPromo($restaurant, $email, $token) {
	$data = null;
	$status = $zdata = 0;
	$login_type = 'backoffice';
	$mode = $msg = "";
		
    try {
	    $log = new WY_Login(set_valid_login_type($login_type));
		$log->check_login_remote($email, $token, $mode);
		$msg = $log->msg;
		if ($log->result > 0) {
			$promo = new WY_Promotion($restaurant);
			$obj = $promo->getPromotionsList();
			$msg = $promo->msg;
			$status = $promo->result;
			if ($promo->result > 0) {
				$data = array('restaurant' => $restaurant, 'promo' => $obj);
				$zdata = count($obj);
				}
			}
	} catch (PDOException $e) { api_error($e, "readPromo"); }
	echo format_api($status, $data, $zdata, $msg);
}

function createPromo($restaurant, $email, $token, $obj) {
	$data = null;
	$status = $zdata = 0;
	$login_type = 'backoffice';
	$mode = $msg = "";

    $name = $obj["name"];
    $offer = $obj["offer"];
    $start = $obj["start"];
    $end = $obj["end"];
    $description = $obj["description"];
    $tnc = $obj["tnc"];
    $cpp = $obj["cpp"];
		
    try {
	    $log = new WY_Login(set_valid_login_type($login_type));
		$log->check_login_remote($email, $token, $mode);
		$msg = $log->msg;
		if ($log->result > 0) {
			$promo = new WY_Promotion($restaurant);
			$promo->fullinsert($name, $offer, $description, $start, $end, $tnc, $cpp);
			$status = $promo->result;
			$msg = $promo->msg;
			}
	} catch (PDOException $e) { api_error($e, "createPromo"); }
    echo format_api($status, $data = "ok", $zdata = 1, $msg);
}

function updatePromo($restaurant, $email, $token, $obj) {
	$data = null;
	$status = $zdata = 0;
	$login_type = 'backoffice';
	$mode = $msg = "";
		
    $name = $obj["name"];
    $offer = $obj["offer"];
    $start = $obj["start"];
    $end = $obj["end"];
    $description = $obj["description"];
    $tnc = $obj["tnc"];
    $cpp = $obj["cpp"];

    try {
	    $log = new WY_Login(set_valid_login_type($login_type));
		$log->check_login_remote($email, $token, $mode);
		$msg = $log->msg;
		if ($log->result > 0) {
			$promo = new WY_Promotion($restaurant);
			$promo->fullupdate($name, $offer, $description, $start, $end, $tnc, $cpp);
			$status = $promo->result;
			$msg = $promo->msg;
			}
	} catch (PDOException $e) { api_error($e, "updatePromo"); }
    echo format_api($status, $data = "ok", $zdata = 1, $msg);
}

function deletePromo($restaurant, $email, $token, $promoID) {
	$data = null;
	$status = $zdata = 0;
	$login_type = 'backoffice';
	$mode = $msg = "";
		
    try {
	    $log = new WY_Login(set_valid_login_type($login_type));
		$log->check_login_remote($email, $token, $mode);
		$msg = $log->msg;
		if ($log->result > 0) {
			$promo = new WY_Promotion($restaurant);
			$promo->delete($promoID);
			$status = $promo->result;
			$msg = $promo->msg;
			}
	} catch (PDOException $e) { api_error($e, "deletePromo"); }
    echo format_api($status, $data = "ok", $zdata = 1, $msg);
}

function setdefaultPromo($restaurant, $email, $token, $promoID) {
	$data = null;
	$status = $zdata = 0;
	$login_type = 'backoffice';
	$mode = $msg = "";
		
    try {
	    $log = new WY_Login(set_valid_login_type($login_type));
		$log->check_login_remote($email, $token, $mode);
		$msg = $log->msg;
		if ($log->result > 0) {
			$promo = new WY_Promotion($restaurant);
			$promo->setDefault($promoID);
			$status = $promo->result;
			$msg = $promo->msg;
			}
	} catch (PDOException $e) { api_error($e, "setDefaultPromo"); }
    echo format_api($status, $data = "ok", $zdata = 1, $msg);
}

function setupPromo($restaurant, $email, $token, $promoID, $value) {
	$data = null;
	$status = $zdata = 0;
	$login_type = 'backoffice';
	$mode = $msg = "";
		
    try {
	    $log = new WY_Login(set_valid_login_type($login_type));
		$log->check_login_remote($email, $token, $mode);
		$msg = $log->msg;
		if ($log->result > 0) {
			$promo = new WY_Promotion($restaurant);
			$promo->setupPromo($promoID, $value);
			$status = $promo->result;
			$msg = $promo->msg;
			}
	} catch (PDOException $e) { api_error($e, "deletePromo"); }
    echo format_api($status, $data = "ok", $zdata = 1, $msg);
}



?>