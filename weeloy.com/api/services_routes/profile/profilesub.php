<?

$app->post('/read/', function () use ($app) {
	$data = json_decode($app->request()->getBody(), true);
	getSubProfile($data['restaurant'], $data['email'], $data['token']);
	});

$app->post('/updateformat/', function () use ($app) {
	$data = json_decode($app->request()->getBody(), true);
	updateFormatSubProfile($data['restaurant'], $data['old'], $data['new'], $data['email'], $data['token']);
	});

$app->post('/update/', function () use ($app) {
	$data = json_decode($app->request()->getBody(), true);
	updateSubProfile($data['restaurant'], $data['systemid'], $data['subprof'], $data['email'], $data['token']);
	});

function getSubProfile($restaurant, $email, $token) {
        
	$data = null;
	$status = $zdata = 0;
	$msg = "";
	$login_type = 'translation';
	$mode = 'api-ajax';
	
	$log = new WY_Login(set_valid_login_type($login_type));
	$log->check_login_remote($email, $token, $mode);
	$msg = $log->msg;
	if ($log->result > 0) {
		$prof = new WY_Profile;
		$data = $prof->readSubProf($restaurant);
		$msg = $prof->msg;
		$status = $prof->result;
		if ($prof->result > 0) {
			$zdata = count($data);
			}
		}
	echo format_api($status, $data, $zdata, $msg);
}

function updateFormatSubProfile($restaurant, $oo, $nn, $email, $token) {
        
	$data = null;
	$status = 0;
	$msg = "";
	$login_type = 'translation';
	$mode = 'api-ajax';
	
	$log = new WY_Login(set_valid_login_type($login_type));
	$log->check_login_remote($email, $token, $mode);
	$msg = $log->msg;
	if ($log->result > 0) {
		$prof = new WY_Profile;
		$prof->updateFormatdSubProf($restaurant, $oo, $nn);
		$msg = $prof->msg;
		$status = $prof->result;
		}
	echo format_api($status, "", 1, $msg);
}

function updateSubProfile($restaurant, $systemid, $subprof, $email, $token) {
        
	$data = null;
	$status = 0;
	$msg = "";
	$login_type = 'translation';
	$mode = 'api-ajax';
	
	$log = new WY_Login(set_valid_login_type($login_type));
	$log->check_login_remote($email, $token, $mode);
	$msg = $log->msg;
	if ($log->result > 0) {
		$prof = new WY_Profile;
		$prof->updateSubProf($restaurant, $systemid, $subprof);
		$msg = $prof->msg;
		$status = $prof->result;
		}
	echo format_api($status, "", "", $msg);
}
    



?>