<?

$app->post('/gettranssection/', function () use ($app) {
	$data = json_decode($app->request()->getBody(), true);
	gettradSection($data['email'], $data['token']);
	});

$app->get('/gettrans/:lang/:topic', 'gettrans');
$app->post('/gettrans/', function () use ($app) {
	$data = json_decode($app->request()->getBody(), true);
	gettrans($data['language'], $data['topic']);
	});


$app->post('/gettransreadcontent/', function () use ($app) {
	$data = json_decode($app->request()->getBody(), true);
	getttradreadContent($data['email'], $data['topic'], $data['langue'], $data['token']);
	});

$app->post('/gettranswritecontent/', function () use ($app) {
	$data = json_decode($app->request()->getBody(), true);
	gettradwriteContent($data['email'], $data['content'], $data['langue'], $data['token']);
	});

$app->post('/gettransnewelement/', function () use ($app) {
	$data = json_decode($app->request()->getBody(), true);
	gettradnewelement($data['email'], $data['element'], $data['content'], $data['zone'], $data['token']);
	});


function gettradSection($email, $token) {
        
	$data = null;
	$status = $sizedata = 0;
	$msg = "";
	$login_type = 'translation';
	$mode = 'api-ajax';
	
	$log = new WY_Login(set_valid_login_type($login_type));
	$log->check_login_remote($email, $token, $mode);
	$msg = $log->msg;
	if ($log->result > 0) {
		$trad = new WY_Translation;
		list($langue, $title) = $trad->readprofile($email);
		$status = $trad->result;
		$msg = $trad->msg;
		if ($trad->result > 0) {
			$data = array('section' => $title, 'langue' => $langue);
			$sizedata = count($title);
		}
	}
	echo format_api($status, $data, $sizedata, $msg);
}
    
function gettrans($langue, $topic) {
	
	$data = null;
	$status = $sizedata = 0;
	$msg = "";
	$trad = new WY_Translation;
	$trad->readcontent($topic, $langue);
	$status = $trad->result;
	$msg = $trad->msg;
	if ($trad->result > 0) {
		$content = $trad->content;
		$data = array('row' => $content);
		$sizedata = count($content);
	}
	echo format_api($status, $data, $sizedata, $msg);
}
    
function getttradreadContent($email, $topic, $langue, $token) {
        
	$data = null;
	$status = $sizedata = 0;
	$mode = 'api';
	$msg = "";
	$login_type = 'translation';
	$mode = 'api-ajax';
	
	$log = new WY_Login(set_valid_login_type($login_type));
	$log->check_login_remote($email, $token, $mode);
	$msg = $log->msg;
	if ($log->result > 0) {
		$trad = new WY_Translation;
		$trad->readcontent($topic, $langue);
		$status = $trad->result;
		$msg = $trad->msg;
		if ($trad->result > 0) {
			$content = $trad->content;
			$data = array('row' => $content);
			$sizedata = count($content);
		}
	}
	echo format_api($status, $data, $sizedata, $msg);
}
    
function gettradwriteContent($email, $content, $langue, $token) {
	
	$data = null;
	$status = $sizedata = 0;
	$msg = "";
	$login_type = 'translation';
	$mode = 'api-ajax';
	
	$log = new WY_Login(set_valid_login_type($login_type));
	$log->check_login_remote($email, $token, $mode);
	$msg = $log->msg;
	if ($log->result > 0) {
		$trad = new WY_Translation;
		$trad->writecontent($content, $langue);
		$msg = $trad->msg;
		$status = $trad->result;
		if ($trad->result > 0) {
			$data = "ok";
			$sizedata = 1;
		}
	}
	echo format_api($status, $data, $sizedata, $msg);
}
    
function gettradnewelement($email, $element, $content, $zone, $token) {
	
	$data = null;
	$status = -1;
	$sizedata = 0;
	$msg = "";
	$login_type = 'translation';
	$mode = 'api-ajax';
	
	$log = new WY_Login(set_valid_login_type($login_type));
	$log->check_login_remote($email, $token, $mode);
	$msg = $log->msg;
	if ($log->result > 0) {
		$trad = new WY_Translation;
		$trad->newelement($element, $content, $zone);
		$status = $trad->result;
		$msg = $trad->msg;
		if ($trad->result > 0) {
			$data = "ok";
			$sizedata = 1;
		}
	}
	echo format_api($status, $data, $sizedata, $msg);
}
    
