<?

$app->post('/read/', function () use ($app) {
  $data = json_decode($app->request()->getBody(),true);
  readBlock($data['restaurant'], $data['product'], $data['email'], $data['token']);
});

$app->post('/create/', function () use ($app) {
  $data = json_decode($app->request()->getBody(),true);
  createBlock($data['restaurant'], $data['email'], $data['token'], $data['blocks']);
});

$app->post('/update/', function () use ($app) {
  $data = json_decode($app->request()->getBody(),true);
  updateBlock($data['restaurant'], $data['email'], $data['token'], $data['blocks']);
});

$app->post('/delete/', function () use ($app) {
  $data = json_decode($app->request()->getBody(),true);
  deleteBlock($data['restaurant'], $data['email'], $data['token'], $data['blockID']);
});

function readBlock($restaurant, $product, $email, $token) {
	$data = null;
	$status = $zdata = 0;
	$login_type = 'backoffice';
	$mode = $msg = "";
		
    try {
	    $log = new WY_Login(set_valid_login_type($login_type));
		$log->check_login_remote($email, $token, $mode);
		$msg = $log->msg;
		if ($log->result > 0) {
			$block = new WY_Block($restaurant, $product);
			$obj = $block->getBlocks();
			$msg = $block->msg;
			$status = $block->result;
			if ($block->result > 0) {
				$data = array('restaurant' => $restaurant, 'block' => $obj);
				$zdata = count($obj);
				}
			}
	} catch (PDOException $e) { api_error($e, "readBlock"); }
	echo format_api($status, $data, $zdata, $msg);
}

function createBlock($restaurant, $email, $token,  $obj) {
	$data = null;
	$status = $zdata = 0;
	$login_type = 'backoffice';
	$mode = $msg = "";

    $blockID = $obj["blockID"];
    $date = $obj["date"];
    $stime = $obj["stime"];
    $tablepax = $obj["tablepax"];
    $description = $obj["description"];
    $slots = $obj["slots"];
	$product = (isset($obj["product"])) ? $obj["product"] : "";
		
    try {
	    $log = new WY_Login(set_valid_login_type($login_type));
		$log->check_login_remote($email, $token, $mode);
		$msg = $log->msg;
		if ($log->result > 0) {
			$block = new WY_Block($restaurant, $product);
			$block->insertBlock($blockID, $date, $stime, $tablepax, $description, $slots);
			$status = $block->result;
			$msg = $block->msg;
			}
	} catch (PDOException $e) { api_error($e, "createBlock"); }
    echo format_api($status, $data = "ok", $zdata = 1, $msg);
}

function updateBlock($restaurant, $email, $token,  $obj) {
	$data = null;
	$status = $zdata = 0;
	$login_type = 'backoffice';
	$mode = $msg = "";
		
    $blockID = $obj["blockID"];
    $date = $obj["date"];
    $tablepax = $obj["tablepax"];
    $stime = $obj["stime"];
    $description = $obj["description"];
    $slots = $obj["slots"];

    try {
	    $log = new WY_Login(set_valid_login_type($login_type));
		$log->check_login_remote($email, $token, $mode);
		$msg = $log->msg;
		if ($log->result > 0) {
			$block = new WY_Block($restaurant, "");
			$block->updateBlock($blockID, $date, $stime, $tablepax, $description, $slots);
			$status = $block->result;
			$msg = $block->msg;
			}
	} catch (PDOException $e) { api_error($e, "updateBlock"); }
    echo format_api($status, $data = "ok", $zdata = 1, $msg);
}

function deleteBlock($restaurant, $email, $token,  $blockID) {
	$data = null;
	$status = $zdata = 0;
	$login_type = 'backoffice';
	$mode = $msg = "";
		
    try {
	    $log = new WY_Login(set_valid_login_type($login_type));
		$log->check_login_remote($email, $token, $mode);
		$msg = $log->msg;
		if ($log->result > 0) {
			$block = new WY_Block($restaurant, "");
			$block->delete($blockID);
			$status = $block->result;
			$msg = $block->msg;
			}
	} catch (PDOException $e) { api_error($e, "deleteBlock"); }
    echo format_api($status, $data = "ok", $zdata = 1, $msg);
}




?>