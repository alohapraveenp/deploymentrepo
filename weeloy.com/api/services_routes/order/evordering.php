<?

$app->post('/read/', function () use ($app) {
  $data = json_decode($app->request()->getBody(),true);
  readEvOrdering($data['restaurant'], $data['email'],$data['type'], $data['token']);
});

$app->post('/create/', function () use ($app) {
  $data = json_decode($app->request()->getBody(),true);
  createEvOrdering($data['restaurant'], $data['email'], $data['token'], $data['orderings']);
});

$app->post('/update/', function () use ($app) {
  $data = json_decode($app->request()->getBody(),true);
  updateEvOrdering($data['restaurant'], $data['email'], $data['token'], $data['orderings']);
});

$app->post('/menus/update/', function () use ($app) {
  $data = json_decode($app->request()->getBody(),true);
  updateEvOrderingMenu($data['restaurant'], $data['email'], $data['token'], $data['orderings']);
});

$app->post('/menus/read/', function () use ($app) {
  $data = json_decode($app->request()->getBody(),true);
  readEvOrderingMenu($data['restaurant'], $data['email'], $data['token'], $data['orderingID']);
});

$app->post('/delete/', function () use ($app) {
  $data = json_decode($app->request()->getBody(),true);
  deleteEvOrdering($data['restaurant'], $data['email'], $data['token'], $data['orderingID']);
});
$app->post('/updatestatus/', function () use ($app) {
  $data = json_decode($app->request()->getBody(),true);
  updateEvOrderingStatus($data['restaurant'],$data['event_id'], $data['email'],$data['token'],$data['status']);
});


function readEvOrdering($restaurant,$email,$type,$token) {
	$data = null;
	$status = $zdata = 0;
	$login_type = 'backoffice';
	$mode = $msg = "";
	 
    try {
	    $log = new WY_Login(set_valid_login_type($login_type));    
		$log->check_login_remote($email, $token, $mode);
		$msg = $log->msg; 
		if ($log->result > 0) {
                    $ordering = new WY_EvOrder($restaurant);
			$data = $ordering->getOrders($email,$type);
			if ($ordering->result > 0) {
				$zdata = count($data);
				$status = $ordering->result;
				}
			}
	} catch (PDOException $e) { api_error($e, "readOrdering"); }
	echo format_api($status, $data, $zdata, $msg);
}

function createEvOrdering($restaurant, $email, $token,  $obj) {
	$data = null;
	$status = $zdata = 0;
	$login_type = 'backoffice';
	$mode = $msg = "";
		
    try {
	    $log = new WY_Login(set_valid_login_type($login_type));
		$log->check_login_remote($email, $token, $mode);
		$msg = $log->msg;
		if ($log->result > 0) {
			$ordering = new WY_EvOrder($restaurant);
			$ordering->createOrder($obj);
			$status = $ordering->result;
			$msg = $ordering->msg;
			}
	} catch (PDOException $e) { api_error($e, "createOrdering"); }
    echo format_api($status, $data = "ok", $zdata = 1, $msg);
}

function updateEvOrdering($restaurant, $email, $token,  $obj) {
	$data = null;
	$status = $zdata = 0;
	$login_type = 'backoffice';
	$mode = $msg = "";
		
    try {
	    $log = new WY_Login(set_valid_login_type($login_type));
		$log->check_login_remote($email, $token, $mode);
		$msg = $log->msg;
		if ($log->result > 0) {
			$ordering = new WY_EvOrder($restaurant);
			$ordering->updateOrders($obj);
			$status = $ordering->result;
			$msg = $ordering->msg;
			}
	} catch (PDOException $e) { api_error($e, "updateOrdering"); }
    echo format_api($status, $data = "ok", $zdata = 1, $msg);
}

function updateEvOrderingMenu($restaurant, $email, $token,  $obj) {
	$data = null;
	$status = $zdata = 0;
	$login_type = 'backoffice';
	$mode = $msg = "";
		
    try {
	    $log = new WY_Login(set_valid_login_type($login_type));
		$log->check_login_remote($email, $token, $mode);
		$msg = $log->msg;
		if ($log->result > 0) {
			$ordering = new WY_EvOrder($restaurant);
			$ordering->updateOrdersMenus($obj);
			$status = $ordering->result;
			$msg = $ordering->msg;
			}
	} catch (PDOException $e) { api_error($e, "updateEvOrderingMenu"); }
    echo format_api($status, $data = "ok", $zdata = 1, $msg);
}

function readEvOrderingMenu($restaurant, $email, $token,  $orderID) {
	$data = null;
	$status = $zdata = 0;
	$login_type = 'backoffice';
	$mode = $msg = "";
		
    try {
	    $log = new WY_Login(set_valid_login_type($login_type));
		$log->check_login_remote($email, $token, $mode);
		$msg = $log->msg;
		if ($log->result > 0) {
			$ordering = new WY_EvOrder($restaurant);
			$data = $ordering->readOrdersMenus($orderID);
			$status = $ordering->result;
			$msg = $ordering->msg;
			if ($ordering->result > 0)
				$zdata = count($data);
			}
	} catch (PDOException $e) { api_error($e, "readEvOrderingMenu"); }
    echo format_api($status, $data, $zdata, $msg);
}


function deleteEvOrdering($restaurant, $email, $token,  $orderID) {
	$data = null;
	$status = $zdata = 0;
	$login_type = 'backoffice';
	$mode = $msg = "";
		
    try {
	    $log = new WY_Login(set_valid_login_type($login_type));
		$log->check_login_remote($email, $token, $mode);
		$msg = $log->msg;
		if ($log->result > 0) {
			$ordering = new WY_EvOrder($restaurant);
			$ordering->deleteOrders($orderID);
			$status = $ordering->result;
			$msg = $ordering->msg;
			}
	} catch (PDOException $e) { api_error($e, "deleteOrdering"); }
    echo format_api($status, $data = "ok", $zdata = 1, $msg);
}

 function updateEvOrderingStatus($restaurant,$orderID,$email,$token,$updatestatus){
        
        $data = null;
	$status = $zdata = 0;
	$login_type = 'backoffice';
	$mode = $msg = "";

    try {
	    $log = new WY_Login(set_valid_login_type($login_type));
		$log->check_login_remote($email, $token, $mode);
		$msg = $log->msg;
		//if ($log->result > 0) {
			$ordering = new WY_EvOrder($restaurant);
			$ordering->updateOrderStatus($orderID,$updatestatus);
			$status = $ordering->result;
			$msg = $ordering->msg;
			//}
	} catch (PDOException $e) { api_error($e, "deleteOrdering"); }
    echo format_api($status, $data = "ok", $zdata = 1, $msg);
 
 }


?>