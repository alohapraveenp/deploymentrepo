<?

$app->post('/read/', function () use ($app) {
  $data = json_decode($app->request()->getBody(),true);
  readOrdering($data['restaurant'], $data['email'], $data['token']);
});

$app->post('/create/', function () use ($app) {
  $data = json_decode($app->request()->getBody(),true);
  createOrdering($data['restaurant'], $data['email'], $data['token'], $data['orderings']);
});

$app->post('/update/', function () use ($app) {
  $data = json_decode($app->request()->getBody(),true);
  updateOrdering($data['restaurant'], $data['email'], $data['token'], $data['orderings']);
});

$app->post('/delete/', function () use ($app) {
  $data = json_decode($app->request()->getBody(),true);
  deleteOrdering($data['restaurant'], $data['email'], $data['token'], $data['orderingID']);
});

function readOrdering($restaurant, $email, $token) {
	$data = null;
	$status = $zdata = 0;
	$login_type = 'backoffice';
	$mode = $msg = "";
		
    $log = new WY_Login(set_valid_login_type($login_type));
    
    try {
		$log->check_login_remote($email, $token, $mode);
		$msg = $log->msg;
		error_log('LOGIN readOrdering '. $log->result . " $restaurant, $email, $token");
                
                
		if ($log->result > 0) {
			$ordering = new WY_OrderOnline($restaurant);
			$orders = $ordering->getOrders();
			if ($ordering->result > 0) {
				$data = array('restaurant' => $orders["restaurant"], 'orders' => $orders["orders"], 'orders_items' => $orders["orders_items"]);
				$zdata = count($orders);
				$status = $ordering->result;
				}
			}
	} catch (PDOException $e) { api_error($e, "readOrdering"); }
	echo format_api($status, $data, $zdata, $msg);
}

function createOrdering($restaurant, $email, $token,  $obj) {
	$data = null;
	$status = $zdata = 0;
	$login_type = 'backoffice';
	$mode = $msg = "";
		
    try {
	    $log = new WY_Login(set_valid_login_type($login_type));
		$log->check_login_remote($email, $token, $mode);
		$msg = $log->msg;
		if ($log->result > 0) {
			$ordering = new WY_OrderOnline($restaurant);
			$ordering->createOrder($obj);
			$status = $ordering->result;
			$msg = $ordering->msg;
			}
	} catch (PDOException $e) { api_error($e, "createOrdering"); }
    echo format_api($status, $data = "ok", $zdata = 1, $msg);
}

function updateOrdering($restaurant, $email, $token,  $obj) {
	$data = null;
	$status = $zdata = 0;
	$login_type = 'backoffice';
	$mode = $msg = "";
		
    try {
	    $log = new WY_Login(set_valid_login_type($login_type));
		$log->check_login_remote($email, $token, $mode);
		$msg = $log->msg;
		if ($log->result > 0) {
			$ordering = new WY_OrderOnline($restaurant);
			$ordering->updateOrders($obj);
			$status = $ordering->result;
			$msg = $ordering->msg;
			}
	} catch (PDOException $e) { api_error($e, "updateOrdering"); }
    echo format_api($status, $data = "ok", $zdata = 1, $msg);
}

function deleteOrdering($restaurant, $email, $token,  $orderID) {
	$data = null;
	$status = $zdata = 0;
	$login_type = 'backoffice';
	$mode = $msg = "";
		
    try {
	    $log = new WY_Login(set_valid_login_type($login_type));
		$log->check_login_remote($email, $token, $mode);
		$msg = $log->msg;
		if ($log->result > 0) {
			$ordering = new WY_OrderOnline($restaurant);
			$ordering->deleteOrders($orderID);
			$status = $ordering->result;
			$msg = $ordering->msg;
			}
	} catch (PDOException $e) { api_error($e, "deleteOrdering"); }
    echo format_api($status, $data = "ok", $zdata = 1, $msg);
}



?>