<?php

// ROUTE TO HERE   /member/profile


require_once("lib/class.member.inc.php");
require_once("lib/class.login.inc.php");
require_once("lib/class.restaurant.inc.php");

$app->get('/:id', function ($id) {
    try {
        $sql = "SELECT * FROM member_madison WHERE email LIKE '{$id}' OR netsuite_id LIKE  '{$id}' ";
        $member = pdo_single_select($sql);
        if (count($member) < 1) {
            throw new Exception("member not found", 1);
        } else {
            echo format_api(1, $member, count(1), NULL);
        }
    } catch (Exception $e) {
        $errors['error'] = true;
        $errors['code'] = 'MEMBER_NOT_FOUND';
        $errors['message'] = $e->getMessage();
        echo format_api(0, $errors, 0, NULL);
    }
});


$app->post('/create', function () use ($app) {
    $data = NULL;

    /// INPUT VALIDATION ////
    $required_parameters = array('salutation', 'firstname', 'lastname', 'mobilephone', 'email');
    $required_parameters_type = array('string', 'string', 'string', 'string', 'string', 'string');

    $optional_parameters = array('netsuite_id',
        'entityid',
        'nric_id',
        'companyname',
        'membership_status',
        'phone',
        'date_of_birth',
        'date_of_anniversary',
        'date_of_last_visit',
        'last_holidays',
        'allergies',
        'likes',
        'dislikes',
        'dont_talk',
        'locker_no');

    $tmp_parameters = array_merge($required_parameters, $optional_parameters);

    //load all parameters
    $content_type = $app->request->getContentType();
    if (!empty($content_type) && $content_type == 'application/json') {
        foreach ($tmp_parameters as $tmp_parameter) {
            $json = $app->request->getBody();
            $data = json_decode($json, true);
            $params[] = $params_dico[$tmp_parameter] = array_key_exists($tmp_parameter, $data) ? $data[$tmp_parameter] : NULL;
        }
    } else {
        foreach ($tmp_parameters as $tmp_parameter) {
            $params[] = $app->request->post($tmp_parameter);
            $params_dico[$tmp_parameter] = $app->request->post($tmp_parameter);
        }
    }


    // check validity 
    $result_check_required_params = check_required_params($params_dico, $required_parameters);
    if ($result_check_required_params !== true) {

        echo format_api(0, NULL, 0, $result_check_required_params, 422, $app);
        $app->stop();
    }

    // check type 
    $result_check_params_type = check_params_type($params, $required_parameters, $required_parameters_type);

    if ($result_check_params_type !== true) {
        echo format_api(0, NULL, 0, $result_check_params_type, 422, $app);
        $app->stop();
    }
    //\ INPUT VALIDATION \\\\\


    try {
        $login = new WY_Login(set_valid_login_type('member'));



        $arg = new WY_Arg;
        $arg->email = $params_dico['email'];
        $arg->name = $params_dico['lastname'];
        $arg->firstname = $params_dico['firstname'];

        $arg->password = $arg->rpassword = 'tmp_password';
        $arg->rpassword = $params_dico['firstname'];

        $arg->mobile = $params_dico['mobilephone'];
        $arg->phone = $params_dico['phone'];
        $arg->application = '';
        $arg->platform = 'member';
        $arg->creation_from_madison = true;



        $arg->netsuite_id = $params_dico['netsuite_id'];
        $arg->entityid = $params_dico['entityid'];
        $arg->salutation = $params_dico['salutation'];

        // optional params
        $arg->nric_id = $params_dico['nric_id'];
        $arg->companyname = $params_dico['companyname'];
        $arg->membership_status = $params_dico['membership_status'];
        $arg->date_of_birth = $params_dico['date_of_birth'];
        $arg->date_of_anniversary = $params_dico['date_of_anniversary'];
        $arg->date_of_last_visit = $params_dico['date_of_last_visit'];
        $arg->last_holidays = $params_dico['last_holidays'];
        $arg->allergies = $params_dico['allergies'];
        $arg->likes = $params_dico['likes'];
        $arg->dislikes = $params_dico['dislikes'];
        $arg->dont_talk = $params_dico['dont_talk'];
        $arg->locker_no = $params_dico['locker_no'];



        $is_weeloy_created = true;
        $is_madison_created = true;
        $is_madison_already_created = true;
        //check if not exist in weeloy member table

        $member = new WY_Member();
        $weeloy_id = $member->getMemberId($arg->email);


        // if the member is not in weeloy member data base - we create
        if (count($weeloy_id) < 1) {

            $res = $login->register_remote($arg);
            if ($res > 0) {
                $member = new WY_Member();
                $weeloy_id = $member->getMemberId($arg->email);
                $is_weeloy_created = true;

                //////////to be verified
            } else {

                $is_weeloy_created = false;

                if ($res == -3) {
                    $errors['error'] = true;
                    $errors['code'] = 'DUPLICATE_ENTRY';
                    $errors['message'] = 'email or mobile already exist.';
                } else {
                    $errors['error'] = true;
                    $errors['code'] = 'UNKNOWN_ERROR';
                    $errors['message'] = 'an error occured.';
                }
                echo format_api(0, NULL, 0, $errors, 422, $app);
            }
            //////////to be verified
        }


        //check if not exist in weeloy member table
        $sql = " SELECT ID FROM member_madison WHERE email = '$arg->email' ";
        $madison_id = pdo_single_select($sql);
        // if the member is not in weeloy member data base - we create
        if (count($madison_id) < 1) {

            $is_madison_already_created = false;

            $dbh = getConnection();
            $sql_insert = "INSERT INTO member_madison (ID, netsuite_id, entityid, nric_id, email, mobilephone, phone, salutation, firstname, lastname, companyname, membership_status, date_of_birth, date_of_anniversary, date_of_last_visit, last_holidays, allergies, likes, dislikes, dont_talk, locker_no) "
                    . "VALUES (NULL, :netsuite_id, :entityid, :nric_id, :email, :mobile, :phone, :salutation, :firstname, :name, :companyname, :membership_status, :date_of_birth, :date_of_anniversary, :date_of_last_visit, :last_holidays, :allergies, :likes, :dislikes, :dont_talk, :locker_no);";

            $parameters = array(
                ':netsuite_id' => $arg->netsuite_id,
                ':entityid' => $arg->entityid,
                ':nric_id' => $arg->nric_id,
                ':email' => $arg->email,
                ':mobile' => $arg->mobile,
                ':phone' => $arg->phone,
                ':salutation' => $arg->salutation,
                ':firstname' => $arg->firstname,
                ':name' => $arg->name,
                ':companyname' => $arg->companyname,
                ':membership_status' => $arg->membership_status,
                ':date_of_birth' => $arg->date_of_birth,
                ':date_of_anniversary' => $arg->date_of_anniversary,
                ':date_of_last_visit' => $arg->date_of_last_visit,
                ':last_holidays' => $arg->last_holidays,
                ':allergies' => $arg->allergies,
                ':likes' => $arg->likes,
                ':dislikes' => $arg->dislikes,
                ':dont_talk' => $arg->dont_talk,
                ':locker_no' => $arg->locker_no
            );

            $res = pdo_insert($sql_insert, $parameters);


            if ($res > 0) {
                $member = new WY_Member();
                $weeloy_id = $member->getMemberId($arg->email);
                //            /return rmlogin($email, $password);
            } else {

                $is_madison_created = false;

                if ($res == -3) {
                    $errors['error'] = true;
                    $errors['code'] = 'DUPLICATE_ENTRY';
                    $errors['message'] = 'email or mobile already exist';
                } else {
                    $errors['error'] = true;
                    $errors['code'] = 'UNKNOWN_ERROR';
                    $errors['message'] = 'An error occured';
                }
                echo format_api(0, NULL, 0, $errors, 422, $app);
            }
        }

        if ($is_weeloy_created && $is_madison_created) {
            if ($is_madison_already_created) {
                $data['message'] = 'profile already created';
            } else {
                $data['message'] = 'profile successfully created';
            }
            $data['profile_id'] = $weeloy_id['ID'];
            echo format_api(1, $data, 1, NULL, 200);
        }
    } catch (PDOException $e) {
        api_error($e, "addMember");
    }
});


$app->post('/update', function () use ($app) {
    $data = NULL;

    /// INPUT VALIDATION ////
    $required_parameters = array('email', 'mobilephone');
    $required_parameters_type = array('string', 'string');

    $optional_parameters = array('salutation',
        'firstname',
        'lastname',
        'netsuite_id',
        'entityid',
        'nric_id',
        'companyname',
        'membership_status',
        'phone',
        'date_of_birth',
        'date_of_anniversary',
        'date_of_last_visit',
        'last_holidays',
        'allergies',
        'likes',
        'dislikes',
        'dont_talk',
        'locker_no');

    $tmp_parameters = array_merge($required_parameters, $optional_parameters);

    //load all parameters
    $content_type = $app->request->getContentType();
    if (!empty($content_type) && $content_type == 'application/json') {
        foreach ($tmp_parameters as $tmp_parameter) {
            $json = $app->request->getBody();
            $data = json_decode($json, true);
            $params[] = $params_dico[$tmp_parameter] = array_key_exists($tmp_parameter, $data) ? $data[$tmp_parameter] : NULL;
        }
    } else {
        foreach ($tmp_parameters as $tmp_parameter) {
            $params[] = $app->request->post($tmp_parameter);
            $params_dico[$tmp_parameter] = $app->request->post($tmp_parameter);
        }
    }


    // check validity 
    $result_check_required_params = check_required_params($params_dico, $required_parameters);
    if ($result_check_required_params !== true) {
        echo format_api(0, NULL, 0, $result_check_required_params, 422, $app);
        $app->stop();
    }

    // check type 
    $result_check_params_type = check_params_type($params, $required_parameters, $required_parameters_type);

    if ($result_check_params_type !== true) {
        echo format_api(0, NULL, 0, $result_check_params_type, 422, $app);
        $app->stop();
    }
    //\ INPUT VALIDATION \\\\\


    try {
        $login = new WY_Login(set_valid_login_type('member'));

        $arg = new WY_Arg;
        $arg->email = $params_dico['email'];
        $arg->name = $params_dico['lastname'];
        $arg->lastname = $params_dico['lastname'];
        $arg->firstname = $params_dico['firstname'];

        $arg->password = $arg->rpassword = 'tmp_password';
        $arg->rpassword = $params_dico['firstname'];

        $arg->mobile = $params_dico['mobilephone'];
        $arg->phone = $params_dico['phone'];
        $arg->application = '';
        $arg->platform = 'member';
        $arg->creation_from_madison = true;

        $arg->netsuite_id = $params_dico['netsuite_id'];
        $arg->entityid = $params_dico['entityid'];
        $arg->salutation = $params_dico['salutation'];

        // optional params
        $arg->nric_id = $params_dico['nric_id'];
        $arg->companyname = $params_dico['companyname'];
        $arg->membership_status = $params_dico['membership_status'];
        $arg->date_of_birth = $params_dico['date_of_birth'];
        $arg->date_of_anniversary = $params_dico['date_of_anniversary'];
        $arg->date_of_last_visit = $params_dico['date_of_last_visit'];
        $arg->last_holidays = $params_dico['last_holidays'];
        $arg->allergies = $params_dico['allergies'];
        $arg->likes = $params_dico['likes'];
        $arg->dislikes = $params_dico['dislikes'];
        $arg->dont_talk = $params_dico['dont_talk'];
        $arg->locker_no = $params_dico['locker_no'];



        $member_not_found = false;

        //check if not exist in weeloy member table

        $member = new WY_Member();
        $weeloy_id = $member->getMemberId($arg->email);
        // if the member is not in weeloy member data base - we create
        if (count($weeloy_id) < 1) {
            $member_not_found = true;
        }


        //check if not exist in weeloy member table
        $sql = "SELECT ID FROM member_madison WHERE email LIKE '$arg->email' ";
        $madison_id = pdo_single_select($sql);
        // if the member is not in weeloy member data base - we create
        if (count($madison_id) < 1) {
            $member_not_found = true;
        }


        if ($member_not_found) {
            if (!empty($params_dico['netsuite_id'])) {
                $sql = " SELECT email FROM member_madison WHERE netsuite_id = '$params_dico[netsuite_id]' ";
                $madison_id = pdo_single_select($sql);
                // if the member is not in weeloy member data base - we create
                if (count($madison_id) < 1) {
                    $errors['error'] = true;
                    $errors['code'] = 'UNABLE TO UPDATE';
                    $errors['message'] = 'email not found in db.';
                    echo format_api(0, NULL, 0, $errors, 422, $app);
                    return true;
                } else {
                    $sql = "UPDATE member SET "
                            . "email = :email "
                            . "WHERE email = :email_old; ";
                    $parameters = array('email' => $arg->email, 'email_old' => $madison_id['email']);
                    pdo_update($sql, $parameters);

                    $sql = "UPDATE member_madison SET "
                            . "email = :email "
                            . "WHERE email = :email_old; ";
                    $parameters = array('email' => $arg->email, 'email_old' => $madison_id['email']);
                    pdo_update($sql, $parameters);


                    $sql = "SELECT ID FROM member_madison WHERE email LIKE '$arg->email' ";
                    $madison_id = pdo_single_select($sql);

                    // if the member is not in weeloy member data base - we create
                    if (count($madison_id) < 1) {
                        $member_not_found = true;
                    }
                }
            }
        }

        // MEMBER FOUND SO NOW, we can update values
        $sql = "UPDATE member SET "
                . "name = :lastname, "
                . "firstname = :firstname, "
                . "mobile = :mobile "
                . "WHERE email = :email; ";
        $parameters = array(':lastname' => $arg->lastname, ':firstname' => $arg->firstname, ':mobile' => $arg->mobile, ':email' => $arg->email);
        pdo_update($sql, $parameters);

        $sql = "UPDATE member_madison SET "
                . "lastname = :lastname, "
                . "firstname = :firstname, "
                . "mobilephone = :mobilephone, "
                . "phone = :phone, "
                . "netsuite_id = :netsuite_id, "
                . "entityid = :entityid, "
                . "nric_id = :nric_id, "
                . "salutation = :salutation, "
                . "companyname = :companyname, "
                . "membership_status = :membership_status, "
                . "date_of_birth = :date_of_birth, "
                . "date_of_anniversary = :date_of_anniversary, "
                . "date_of_last_visit = :date_of_last_visit, "
                . "last_holidays = :last_holidays, "
                . "allergies = :allergies, "
                . "likes = :likes, "
                . "dislikes = :dislikes, "
                . "dont_talk = :dont_talk, "
                . "locker_no = :locker_no "
                . "WHERE email = :email; ";

        $parameters = array(
            ':firstname' => $arg->firstname,
            ':lastname' => $arg->name,
            ':mobilephone' => $arg->mobile,
            ':phone' => $arg->phone,
            ':netsuite_id' => $arg->netsuite_id,
            ':entityid' => $arg->entityid,
            ':nric_id' => $arg->nric_id,
            ':salutation' => $arg->salutation,
            ':companyname' => $arg->companyname,
            ':membership_status' => $arg->membership_status,
            ':date_of_birth' => $arg->date_of_birth,
            ':date_of_anniversary' => $arg->date_of_anniversary,
            ':date_of_last_visit' => $arg->date_of_last_visit,
            ':last_holidays' => $arg->last_holidays,
            ':allergies' => $arg->allergies,
            ':likes' => $arg->likes,
            ':dislikes' => $arg->dislikes,
            ':dont_talk' => $arg->dont_talk,
            ':locker_no' => $arg->locker_no,
            ':email' => $arg->email
        );
        pdo_update($sql, $parameters);


        $data_result['message'] = 'profile successfully updated';
        echo format_api(1, $data_result, 1, NULL, 200);
    } catch (PDOException $e) {
        api_error($e, "addMember");
    }
});
//grapList

//$app->get('/grabzlist', function ($request, $response, $args) {
//        $res = new WY_restaurant('');
//            $result =$res->getgrabzListRestaurant();
//            $errors = null;
//            echo format_api(1, $result, count($result), $errors);   
//    
//});
