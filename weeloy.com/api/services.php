<?php
    
require_once "conf/conf.init.inc.php";
require_once "lib/wpdo.inc.php";
require_once "lib/wglobals.inc.php";
require_once "conf/conf.session.inc.php";
require_once "lib/class.coding.inc.php";
require_once "lib/class.login.inc.php";
require_once "lib/class.booking.inc.php";
require_once "lib/class.review.inc.php";
require_once "lib/class.restaurant.inc.php";
require_once "lib/class.member.inc.php";
require_once "lib/class.allote.inc.php";
require_once "lib/class.menu.inc.php";
require_once "lib/class.event.inc.php";
require_once "lib/class.notes.inc.php";
require_once "lib/class.wheel.inc.php";
require_once "lib/class.service.inc.php";
require_once "lib/class.images.inc.php";
require_once "lib/class.media.inc.php";
require_once "lib/imagelib.inc.php";
require_once "lib/class.mail.inc.php";
require_once "lib/class.sms.inc.php";
require_once "lib/class.pushnotif.inc.php";
require_once "lib/class.debug.inc.php";
require_once "lib/class.qrcode.inc.php";
require_once "lib/class.cluster.inc.php";
require_once "lib/class.translation.inc.php";
require_once("lib/class.tms.inc.php");
require_once("lib/class.order_online.inc.php");
require_once("lib/class.evorder.inc.php");
require_once("lib/class.invoice.inc.php");
require_once("lib/class.promotion.inc.php");
require_once("lib/class.profile.inc.php");
require_once("lib/class.blocklist.inc.php");
require_once("lib/class.event_management.inc.php");
require_once("lib/class.mail_crawler.inc.php");
require_once("lib/class.pabx.inc.php");
require_once("lib/class.payment.inc.php");
require_once("lib/class.payment_new.inc.php");
require_once "lib/Browser.inc.php";

require_once "lib/class.spool.inc.php";
require_once "lib/class.async.inc.php";

define("__MEDIA__SERVER_NAME__", 'https://media.weeloy.com/upload/restaurant/');

require 'Slim/Slim.php';

\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();

//$app->response()->header('Content-Type', 'application/json;charset=utf-8');
$app->contentType('application/json;charset=utf-8');


$app->group('/notes', function () use ($app) {
    require 'services_routes/notes/notes.php';
});
$app->group('/crawler', function () use ($app) {
    require 'services_routes/crawler/crawler.php';
});
$app->group('/cluster', function () use ($app) {
    require 'services_routes/cluster/cluster.php';
});
$app->group('/git', function () use ($app) {
    require 'services_routes/git/git.php';
});
$app->group('/translation', function () use ($app) {
    require 'services_routes/translation/translation.php';
});

$app->group('/booking', function () use ($app) {
    require 'services_routes/booking/booking.php';
});

$app->group('/cancelpolicy', function () use ($app) {
    require 'services_routes/policies/cancelpolicy.php';
});

$app->group('/pabx', function () use ($app) {
    require 'services_routes/pabx/pabx.php';
});

$app->group('/pos', function () use ($app) {
    require 'services_routes/pos/pos.php';
});

$app->group('/tms', function () use ($app) {
    require 'services_routes/tms/tms.php';
});

$app->group('/profile', function () use ($app) {
    require 'services_routes/profile/profile.php';
});

$app->group('/profilesub', function () use ($app) {
    require 'services_routes/profile/profilesub.php';
});

$app->group('/menu', function () use ($app) {
    require 'services_routes/menu/menu.php';
});

$app->group('/invoice', function () use ($app) {
    require 'services_routes/invoice/invoice.php';
});

$app->group('/evordering', function () use ($app) {
    require 'services_routes/order/evordering.php';
});

$app->group('/event', function () use ($app) {
    require 'services_routes/event/event.php';
});

$app->group('/ordering', function () use ($app) {
    require 'services_routes/order/ordering.php';
});

$app->group('/wheel', function () use ($app) {
    require 'services_routes/wheel/wheel.php';
});

$app->group('/promo', function () use ($app) {
    require 'services_routes/promo/promo.php';
});

$app->group('/block', function () use ($app) {
    require 'services_routes/alloteblock/block.php';
});

$app->group('/contact', function () use ($app) {
    require 'services_routes/contact/contact.php';
});

$app->group('/blockedlist', function () use ($app) {
    require 'services_routes/blacklist/blockedlist.php';
});

$app->group('/bkgohbs', function () use ($app) {
    require 'services_routes/openhour/bkgohbs.php';
});

$app->group('/rmloginc', function () use ($app) {
    require 'services_routes/rmloginc/rmloginc.php';
});

$app->group('/restaurant', function () use ($app) {
    require 'services_routes/restaurant/restaurant.php';
});

$app->group('/review', function () use ($app) {
    require 'services_routes/review/review.php';
});

$app->group('/evmanagement', function () use ($app) {
    require 'services_routes/evrequest/evrequest.php';
});

$app->group('/debugerror', function () use ($app) {
    require 'services_routes/debugerror/debugerror.php';
});
$app->group('/payment', function () use ($app) {
    require 'services_routes/payment/payment.php';
});

$app->run();

function api_error($e, $funcname) {

    $trace = preg_replace("/\'|\"/", "`", $e->getTraceAsString());
    $traceAr = $e->getTrace();
    $file = preg_replace("/^.*\//", "", $traceAr[0]['file']);
    $msg = $e->getMessage() . " - " . $file . " - " . $traceAr[0]['line'];
	WY_debug::recordDebug("ERROR-API", "API-" . strtoupper($funcname), $msg . " \n " . $trace);
    echo format_api(0, 0, 0, $funcname . "->" . $msg);
}

function api_authentificate() {
        
	$response = array();
	
	if (!isset($_SERVER['PHP_AUTH_USER']) || !isset($_SERVER['PHP_AUTH_PW'])) {
		$response["error"] = true;
		$response["message"] = "Credentials are misssing";
		set_header_status(401);
		echo format_api(1, '', 0, $response);
		exit; //$app->stop();
	}
	
	$email = $_SERVER['PHP_AUTH_USER'];
	$pass = $_SERVER['PHP_AUTH_PW']; //pass ou token
	
	$log = new WY_Login(set_valid_login_type('backoffice'));
	if ($log->check_login_remote($email, $pass, 'api') != '') {
		return true;
	} else if ($log->process_login_remote($email, $pass, 'api') != '') {
		return true;
	} else {
		$response["error"] = true;
		$response["message"] = "wrong login and password";
		set_header_status(401);
		echo format_api(1, '', 0, $response);
		exit;
	}
	
	return false;
}
    
    /* add platform */
/*
 $status: 0/-1 : fail 1: success
 */
function format_api($status, $data, $count, $errors, $header_status = 200) {
        
	set_header_status($header_status);
	
	$resultat['status'] = $status;
	$resultat['data'] = $data;
	$resultat['count'] = $count;
	$resultat['errors'] = $errors;
	return json_encode($resultat);
}
    
    //// later on  ////
    
function authenticate(\Slim\Route $route) {
	// Getting request headers
	$headers = apache_request_headers();
	$response = array();
	$app = Slim::getInstance();
	
	// Verifying Authorization Header
	if (isset($headers['Authorization'])) {
		$db = new DbHandler();
		
		// get the api key
		$api_key = $headers['Authorization'];
		// validating api key
		if (!$db->isValidApiKey($api_key)) {
			// api key is not present in users table
			$response["error"] = true;
			$response["message"] = "Access Denied. Invalid Api key";
			echoRespnse(401, $response);
			$app->stop();
		} else {
			global $user_id;
			// get user primary key id
			$user_id = $db->getUserId($api_key);
		}
	} else {
		// api key is missing in header
		$response["error"] = true;
		$response["message"] = "Api key is misssing";
		echoRespnse(400, $response);
		$app->stop();
	}
	
}
    
function is_email_valid($email) {
    return !(filter_var($email, FILTER_VALIDATE_EMAIL) === false);
}
    
function set_header_status($code) {
	$status = array(
		100 => 'Continue',
		101 => 'Switching Protocols',
		200 => 'OK',
		201 => 'Created',
		202 => 'Accepted',
		203 => 'Non-Authoritative Information',
		204 => 'No Content',
		205 => 'Reset Content',
		206 => 'Partial Content',
		300 => 'Multiple Choices',
		301 => 'Moved Permanently',
		302 => 'Found',
		303 => 'See Other',
		304 => 'Not Modified',
		305 => 'Use Proxy',
		306 => '(Unused)',
		307 => 'Temporary Redirect',
		400 => 'Bad Request',
		401 => 'Unauthorized',
		402 => 'Payment Required',
		403 => 'Forbidden',
		404 => 'Not Found',
		405 => 'Method Not Allowed',
		406 => 'Not Acceptable',
		407 => 'Proxy Authentication Required',
		408 => 'Request Timeout',
		409 => 'Conflict',
		410 => 'Gone',
		411 => 'Length Required',
		412 => 'Precondition Failed',
		413 => 'Request Entity Too Large',
		414 => 'Request-URI Too Long',
		415 => 'Unsupported Media Type',
		416 => 'Requested Range Not Satisfiable',
		417 => 'Expectation Failed',
		500 => 'Internal Server Error',
		501 => 'Not Implemented',
		502 => 'Bad Gateway',
		503 => 'Service Unavailable',
		504 => 'Gateway Timeout',
		505 => 'HTTP Version Not Supported');
        
        header("HTTP/1.1 $code " . $status[$code]);
        return true;
    }
?>