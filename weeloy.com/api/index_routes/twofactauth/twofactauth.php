<?

$app->post('/settwofactauth', function () use ($app) {
    return setAuthFlg($app);
});
$app->post('/resettwofactauth', function () use ($app) {
    return resetAuthFlg($app);
});

function setAuthFlg($app) {
    $email = $app->request()->post('email');
    $log = new WY_Login(set_valid_login_type('backoffice'));
    $data = $log->set_google_auth($email);
    echo format_api(1, $data, 0, 0);
}

function resetAuthFlg($app) {
    $email = $app->request()->post('email');
    $log = new WY_Login(set_valid_login_type('backoffice'));
    $data = $log->reset_google_auth($email);
    echo format_api(1, $data, 0, 0);
}



?>