<?
$app->post('/categories', function () use ($app) {
    $data = json_decode($app->request()->getBody(),true);
    return homecategories($data);
});
$app->post('/event', function () use ($app) {
    return CreateOrUpdateEvent($_REQUEST['data'], $_FILES['file']);
});
$app->post('/footercategories', function () use ($app) {
    $data = json_decode($app->request()->getBody(),true);
    return homefootercategories($data);
});
$app->post('/updatefootercategories', function () use ($app) {
    $data = json_decode($app->request()->getBody(),true);
    return updatefooterCategories($data['categories']);
});
$app->post('/updatecategories', function () use ($app) {
    $data = json_decode($app->request()->getBody(),true);
    return updateCategories($data['categories']);
});
$app->post('/deletecategories', function () use ($app) {
    $data = json_decode($app->request()->getBody(),true);
    return deleteCategories($data['categories']);
});
$app->get('/getcategories/:query', 'getCategories');
$app->get('/getEvents/:query', 'getEvents');
$app->get('/getEventAll',function() use ($app){  
    return getEventall();
});
function CreateOrUpdateEvent($obj, $file) {
    if (isset($obj) && isset($obj['type']) && preg_match("/^event_/", $obj['type'])) {
        if (preg_match('/^[a-z]+_[a-z]+$/i', $obj['tag']))
            $obj['tag'] = str_replace('_', '-', $obj['tag']);
        $res = new WY_restaurant();
        echo $res->CreateOrUpdateEvent($obj, $file) ? format_api(1, "Success!", 1, NULL) : format_api(0, "Invalid input parameters!", 0, NULL);
    } else
        error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": Invalid input parameter: ".print_r($obj, true));
}
function homecategories($obj) {
        $name =$obj["title"];
        $data =$obj["tag"];
        $type =$obj['type'];
        $description =$obj["description"];
        $image1 =$obj["image1"];
        $image2 =$obj["image2"];
        $image3 =$obj["image3"];
        $rest1 =$obj["restaurant1"];
        $rest2 =$obj["restaurant2"];
        $rest3 =$obj["restaurant3"];
        $status =$obj["status"];
        $is_tag = $obj["is_tag"];
        $morder =$obj['morder'];
        $city = $obj["city"];
        $is_mobile = $obj["is_mobile"];
        $dylink =$obj["link"];
        $taglink ="search/singapore/tags/".$data;
        $link = ($is_tag===true)? $taglink:$dylink;
        $images = array(
            array('restaurant'=>trim($rest1),'image'=>$image1),
            array('restaurant'=>trim($rest2), 'image'=>$image2),
            array('restaurant'=>trim($rest3), 'image'=>$image3)
        );
        $images =serialize($images);
        $res = new WY_restaurant();
        $result = $res->homecategories($name,$data,$link,$description,$images,$city,$is_mobile,$status,$morder,$type);
        $errors=null;
      echo format_api(1, 1, $result, $errors);
}

function homefootercategories($obj) {
        $name =$obj["title"];
        $data =$obj["tag"];
        $type =$obj['type'];
        $description =$obj["description"];
        // check staring contains an underscore and is two words
        if (preg_match('/^[a-z]+_[a-z]+$/i', $data))
            $data= str_replace('_', '-', $data);
        error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": type: ".$type);
        $image1 =$obj["image1"];
        $rest1 =$obj["restaurant1"];
        $status =$obj["status"];
        $is_tag = $obj["is_tag"];
        $morder =$obj['morder'];
        $city = $obj["city"];
        $is_mobile = $obj["is_mobile"];
        $dylink =$obj["link"];
        $taglink ="search/".$city."/tags/".$data;
        $link = ($is_tag===true)? $taglink:$dylink;
        $images = array(
            array('restaurant'=>trim($rest1),'image'=>$image1)
        );
        $images =serialize($images);
        $res = new WY_restaurant();
        $result = $res->homecategories($name,$data,$link,$description,$images,$city,$is_mobile,$status,$morder,$type);
        $errors=null;
        echo format_api(1, 1, $result, $errors);
    }

     function updateCategories($obj){
     
        $name =$obj["title"];
        $id= $obj["index"];
        $data =$obj["tag"];
        $description =$obj["description"];
        $image1 =$obj["image1"];
        $image2 =$obj["image2"];
        $image3 =$obj["image3"];
        $rest1 =$obj["restaurant1"];
        $rest2 =$obj["restaurant2"];
        $rest3 =$obj["restaurant3"];
        $status =$obj["status"];
        $is_tag = $obj["is_tag"];
        $morder =$obj['morder'];
        $city = $obj["city"];
        $is_mobile = $obj["is_mobile"];
        $type = (isset($obj["type"]))? $obj["type"]:'';
        $dylink =$obj["link"];
        $taglink ="search/singapore/tags/".$data;

        $link = ($is_tag===true)? $taglink:$dylink;
         $data = ($is_tag===true)? $data:"";
         
            $images = array(
                array('restaurant'=>trim($rest1),'image'=>$image1),
                array('restaurant'=>trim($rest2), 'image'=>$image2),
                array('restaurant'=>trim($rest3), 'image'=>$image3)
            );
         
        $images =serialize($images);
        $res = new WY_restaurant();
        $result = $res->updatecategories($name,$data,$link,$description,$images,$id,$city,$is_mobile,$status,$morder,$type);
        $errors=null;
      echo format_api(1, 1, $result, $errors);
  
    }
    
 function updatefooterCategories($obj){
	$name =$obj["title"];
	$data =$obj["tag"];
	$type =$obj['type'];
	$description =$obj["description"];
      // check staring contains an underscore and is two words
    if (preg_match('/^[a-z]+_[a-z]+$/i', $data))
        $data= str_replace('_', '-', $data);
    error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": type: ".$type);
    $id= $obj["index"];
    $image1 =$obj["image1"];
    $rest1 =$obj["restaurant1"];
    $status =$obj["status"];
    $is_tag = $obj["is_tag"];
    $morder =$obj['morder'];
    $city = $obj["city"];
    $is_mobile = $obj["is_mobile"];
    $dylink =$obj["link"];
    $taglink ="search/".$city."/tags/".$data;
    //$taglink ="search/singapore/tags/".$data;
    $link = ($is_tag===true)? $taglink:$dylink;
    $data = ($is_tag===true)? $data:"";
    $images = array(
        array('restaurant'=>trim($rest1),'image'=>$image1)
    );
    $images =serialize($images);
	$res = new WY_restaurant();
	$result = $res->updatecategories($name,$data,$link,$description,$images,$id,$city,$is_mobile,$status,$morder,$type);
	$errors=null;
    echo format_api(1, 1, $result, $errors);
}

function deleteCategories($obj){
	$id= $obj["index"];
	$res = new WY_restaurant();
	$result = $res->deletecategories($id);
	$errors=null;
  echo format_api(1, 1, $result, $errors);
}

function getCategories($type) {
	$res = new WY_restaurant();
	$result = $res->getCategories($type);
    error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": type: ".$type." ".count($result)." records");
	$data = array("categories" => $result);
	echo format_api(1, $data, count($result), null);
}
function getEvents($restaurant) {
    error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": restaurant: ".$restaurant);
    $res = new WY_restaurant();
    try {
        $event = $res->getActiveEvents($restaurant);
        $data = array("event" => $event);
        echo format_api(1, $data, count($event), "");
    } catch (Exception $e) {
        api_error($e, "Exception: ".$e->getMessage());
    }
}
function getEventall() {
  $eobj =  new WY_restaurant();
    try {
        $event = $eobj->getHomeActiveEvents();
        $data = array("event" => $event);
        echo format_api(1, $data, count($event), "");
    } catch (PDOException $e) {
        api_error($e, "getHomeActiveEvents");
    }  
    
}
?>