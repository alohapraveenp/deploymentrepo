<?

$app->get('/menu/:app/:lang/:city', function($app, $lang, $city) {
    try {
       
        $menu = array();
        $action = array(
            'city'=>$city,
            'freetext'=>NULL,
            'cuisine'=>NULL,
            'pricing'=>NULL,
            'tags'=>'bubble',
            'status'=>'active',
            'page'=>'1'
            );
        $cat1 = array(
            'title'=>'Bubbles',
            'type'=>'search',
            'action'=> $action,
            'icon_url'=> '',
            'order'=>'1'
            );
        //$menu[] = $cat1;
        $action = array(
            'city'=>$city,
            'freetext'=>NULL,
            'cuisine'=>NULL,
            'pricing'=>NULL,
            'tags'=>'international',
            'status'=>'active',
            'page'=>'1'
            );
        $cat2 = array(
            'title'=>'International Selection',
            'type'=>'search',
            'action'=> $action,
            'icon_url'=> '',
            'order'=>'1'
            );
        $menu[] = $cat2;        
        $action = array(
            
            'url'=>'https://www.weeloy.com/partner?dspl_h=f&dspl_f=f'
            );
        $cat3 = array(
            'title'=>'Partner Page',
            'type'=>'web_view',
            'action'=> $action,
            'icon_url'=> '',
            'order'=>'1'
            );
        //$menu[] = $cat3;        
        
        
        if(is_test_env()== false){
           $menu = array(); 
        }
        
        // patch
        
        echo format_api(1, $menu, count($menu), NULL);
   } catch (Exception $e) {
        echo format_api(0, $e->getMessage(), 1, NULL);
   }   
});

///// dynamic category for Mobilr  /////
/////       KALA     /////

$app->get('/category', function() {
    try {
        $type ="home";
        $tempArr=array();
        $item_arr = array();
        $res = new WY_restaurant();
        $url ='https://media4.weeloy.com/upload/restaurant/';
        $result = $res->getCategoriesMobile($type);
            if(count($result)>0){
               foreach ($result as $d) {
                   if($d['title'] == 'category')
                        array_push($tempArr,array(
                            'category'=> $d['description'],
                            'title'=> ucfirst($d['description']),
                            'category_id'=> $d['morder'],
                            'items' =>array()
                        ));
 
                }
                foreach ($tempArr as $t) {
                    foreach ($result as $d) {
 
                        $obj1 = array('title'=>$d['title'],'image'=>$url.$d['title']."/".$d['images'][0]['image'],'params'=>array($d['description'] =>$d['title']));
                            if(strtolower($t['category']) == 'city'){
                                if(strtolower($d['description']) == 'city' && $d['title'] != 'category')
                                    $t['items'][] = $obj1;
                            }elseif(strtolower($t['category']) == 'extras' && $d['title'] != 'category'){
                                if(strtolower($d['description']) == 'extras')
                                    $t['items'][] = $obj1; 
                            }else{
                                 if(strtolower($d['description']) == 'cuisine' && $d['title'] != 'category')
                                    $t['items'][] = $obj1; 
                            }
                       
                    }
                        $item_arr[] = $t;
                }

                echo format_api(1, $item_arr, count($item_arr), NULL);

            }else{
               echo format_api(1, $item_arr, count($item_arr), NULL); 
            }
        } catch (Exception $e) {
            echo format_api(0, $e->getMessage(), 1, NULL);
    } 
        
});

///// dynamic menu  /////

?>