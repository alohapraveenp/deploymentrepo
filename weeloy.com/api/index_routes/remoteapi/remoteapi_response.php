<?php

    $app->post('/touche/', function () use ($app) {

        $credentital_W = '6LdmpA4REHKaA_8MlE9KA2wY_smquvjhw_Tj';    // weeloy
        $credentital_T = '6LdmpA4CFDGKCqaA_8MlE9KA2wY_smquvqOw_Tj'; // touche

        $result = "";
        $errors = "";

        // $request = $app->request->post();
        $request = json_decode($app->request()->getBody(), true);
        $request = filter_var_array($request, FILTER_SANITIZE_STRING);

        if (isset($request['credential']) && $request['credential'] != '') {
            $credential = $request['credential'];
        }

        if($credentital_T !== $credential) {
            $errors = "wrong credentials " . $credential ;
            echo format_api(-1, $result, count($result), $errors); 
        } else {
            if (isset($request['cmd']) && $request['cmd'] != '') {
                $cmd = $request['cmd'];
            }   

            if ($cmd == 'sendbooking') {
                return remote_sendbooking($app);
            } else if ($cmd == 'getprofile') {
                return remote_getprofile($app);    
            } else if ($cmd == 'getlastbooking') {
                // error_log("getlastbooking");
                return remote_getlastbooking($app);    
            } 
        }
        
    });

    // $app->post('/touche/sendbooking/', function () use ($app) {
    //     return remote_sendbooking($app);
    // });

    // $app->get('/touche/getprofile/', function () use ($app) {
    //     return remote_getprofile($app);
    // });

    function remote_sendbooking($app) {
        // $credentital_W = '6LdmpA4REHKaA_8MlE9KA2wY_smquvjhw_Tj';	// weeloy
        // $credentital_T = '6LdmpA4CFDGKCqaA_8MlE9KA2wY_smquvqOw_Tj'; // touche

        // $credential = $app->request->post("credential");
        // $cmd = $app->request->post("cmd");
        // $arg = $app->request->post("arg");
        
        // $result = "";
        // $errors = "";
        // if($credentital_T !== $credential) {
        // 	   $errors = "wrong credentials " . $credential ;
    	   // echo format_api(-1, $result, count($result), $errors); 
        // 	}
        // else {
        // 	   $result = "OK";
    	   // echo format_api(1, $result, count($result), $errors); 
        // 	}

        echo format_api(1, 'sendbooking', 1, '');
    }

    function remote_getprofile($app) {
        
        // $request = $app->request->post();
        $request = json_decode($app->request()->getBody(), true);

        if (isset($request['email']) && $request['email'] != '') {
            $email = $request['email'];
        }
        
        if (isset($request['mobile']) && $request['mobile'] != '') {
            $mobile = $request['mobile'];
        }

        if (isset($request['restaurant']) && $request['restaurant'] != '') {
            $restaurant = $request['restaurant'];
        }

        $result = "";
        $errors = "";

        $returnResult = array();

        $profile = new WY_Profile();

        $account = "";

        $result = $profile->read1Prof($restaurant, $email, $mobile, $account);

        $returnResult['name'] = $result['firstname'] . ' ' . $result['lastname'];
        $returnResult['email'] = $result['email'];
        $returnResult['mobile'] = $result['mobile'];

        // array_push($returnResult, $result['firstname'], $result['lastname'], $result['email'], $result['mobile']);

        echo format_api(1, $returnResult, count($returnResult), $errors); 
        
    }

    function remote_getlastbooking($app) {
        
        // $request = $app->request->post();
        $request = json_decode($app->request()->getBody(), true);

        if (isset($request['email']) && $request['email'] != '') {
            $email = $request['email'];
        }
        
        if (isset($request['mobile']) && $request['mobile'] != '') {
            $mobile = $request['mobile'];
        }

        if (isset($request['restaurant']) && $request['restaurant'] != '') {
            $restaurant = $request['restaurant'];
        }

        $options = array();

        $options['last'] = 1;
        $options['period'] = 'today';

        $result = "";
        $errors = "";

        // $returnResult = array();

        $booking = new WY_Booking();

        $result = $booking->getUserBookings($email, $options);

        $lastBooking = $booking->bookings;

        if (count($lastBooking) == 0) {
            $errors = "You don't have any booking today.";
            echo format_api(1, $lastBooking, count($lastBooking), $errors); 
        } else {
            $lastBooking = array();
        // var_dump($booking->bookings);
            $lastBooking['bookingid'] = $booking->bookings[0]->confirmation;
            $lastBooking['bookingstatus'] = $booking->bookings[0]->state;
            $lastBooking['name'] = $booking->bookings[0]->salutation . ' ' . $booking->bookings[0]->firstname . ' ' . $booking->bookings[0]->lastname ;
            $lastBooking['email'] = $booking->bookings[0]->email;
            $lastBooking['bookingdate'] = $booking->bookings[0]->rdate;
            $lastBooking['bookingtime'] = $booking->bookings[0]->rtime;
            $lastBooking['bookingpax'] = $booking->bookings[0]->cover;
            $lastBooking['restaurant'] = $booking->bookings[0]->restaurantinfo->title;
            $lastBooking['bookingamt'] = $booking->bookings[0]->amount;

            echo format_api(1, $lastBooking, count($lastBooking), $errors); 
        }        
    }
?>
