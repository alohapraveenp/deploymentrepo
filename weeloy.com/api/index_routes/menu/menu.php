<?

$app->post('/create', function () use ($app) {
    return createMenu($app);
});

$app->post('/update', function () use ($app) {
    return updateMenuTMP($app);
});

$app->post('/delete', function () use ($app) {
    return deleteMenuTMP($app);
});

function createMenu($app) {

    $restaurant = $app->request->post("restaurant");
    //$name = $app->request->post("name");
    $title = $app->request->post("title");
    $morder = $app->request->post("morder");
    $description = $app->request->post("description");
    $items = $app->request->post("items");

    $token = $app->request->post("token");
    $login = new WY_Login(set_valid_login_type('backoffice'));
    if ($login->checktoken($token)) {
        $mobj = new WY_Menu($restaurant);
        list($val, $newid, $msg) = $mobj->createMenu($title, $morder, $description, $items);

        if ($val > 0)
            echo format_api(1, $newid, 0, $msg);
        else
            echo format_api(-1, '', 0, $msg);
    } else
        echo format_api(-1, '', 0, 'wrong token');
}

function updateMenuTMP($app) {

    $restaurant = $app->request->post("restaurant");

    $id = $app->request->post("id");
    $title = $app->request->post("title");
    $description = $app->request->post("description");
    $morder = $app->request->post("morder");
    $items = $app->request->post("items");

    $token = $app->request->post("token");
    $login = new WY_Login(set_valid_login_type('backoffice'));
    if ($login->checktoken($token)) {
        $mobj = new WY_Menu($restaurant);
        list($val, $newid, $msg) = $mobj->updateMenu($id, $title, $description, $morder, $items);

        if ($val > 0)
            echo format_api(1, $newid, 0, $msg);
        else
            echo format_api(-1, '', 0, $msg);
    } else
        echo format_api(-1, '', 0, 'wrong token');
}

function deleteMenuTMP($app) {

    $restaurant = $app->request->post("restaurant");
    $id = $app->request->post("id");
    $token = $app->request->post("token");

    $login = new WY_Login(set_valid_login_type('backoffice'));
    if ($login->checktoken($token)) {
        $mobj = new WY_Menu($restaurant);
        list($val, $msg) = $mobj->deleteMenu($id);
        if ($val > 0)
            echo format_api(1, '', 0, '');
        else
            echo format_api(-1, '', 0, $msg);
    } else
        echo format_api(-1, '', 0, 'wrong token');
}


?>