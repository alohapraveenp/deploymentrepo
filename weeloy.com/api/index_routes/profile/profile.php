<?

function updateProfile($label, $value) {
    $email = $_SERVER['PHP_AUTH_USER'];
    $validLabel = array('name', 'firstname', 'mobile', 'country', 'member', 'salutation','gender','address', 'address1', 'zip', 'region', 'state', 'fax');

    if (!in_array($label, $validLabel)) {
        echo format_api(0, NULL, 0, "Invalid Label $label");
        return;
    }
    if($label =='mobile' && substr($value, 0, 1) != '+'){
        $value ='+'.$value;
        $value=trim($value);
    }
    $sql = "UPDATE member SET $label=:value WHERE email=:email limit 1";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("email", $email);
        $stmt->bindParam("value", $value);
        $obj = $stmt->execute();
        $db = null;
        if ($obj) {
            if ($label == 'mobile') {
                $mobile = explode(' ', $value);
                $_SESSION['user']['prefix'] = $mobile[0];
                $_SESSION['user']['mobile'] = $mobile[1];
            } else {
                if ($label == 'name') {
                  $_SESSION['user']['lastname'] = $value;
                } else {
                  $_SESSION['user'][$label] = $value;
                }
            }
            echo format_api(1, "success", 1, $label);
        }
    } catch (PDOException $e) {
        echo format_api(0, NULL, 0, "Update $label failed");
    }
}


?>