<?

function getBookingWithPendingReviews($count) {
    $member = new WY_Member($_SERVER['PHP_AUTH_USER']);
    $pending_review = $member->getBooking(NULL, NULL, 'ASC', 'no_white_label', 1);
    if ($pending_review) {
        echo format_api(1, $pending_review, 1, NULL);
    } else {
        echo format_api(1, NULL, 1, NULL);
    }
}

function getReviews($query, $page = '') {

    if ($page == 'summary') {
        return getReviewsList($query);
    }

	$page = (!empty($page)) ? intval($page) : -1;
    $item_per_page = 2;
	$limit = '';
	if ($page > 0) {
        $item = $page * $item_per_page;
        $limit = " LIMIT $item,$item_per_page";
    }


    $mapping = array("restaurant" => "resto", "confirmation" => "booking", "reviewdesc" => "description", "reviewdtvisite" => "date", "reviewdtcreate" => "cdate", "reviewguest" => "guestname", "user_id" => "userid", "reviewgrade" => "grade", "foodgrade" => "food", "servicegrade" => "service", "ambiancegrade" => "ambiance", "pricegrade" => "price", "comment" => "comment", "response_to" => "response");

    $sql = "SELECT restaurant, confirmation, reviewdesc, reviewdtvisite, reviewdtcreate, reviewguest, user_id, reviewgrade,foodgrade,servicegrade, ambiancegrade, pricegrade, comment, response_to FROM review WHERE restaurant=:query $limit";

    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("query", $query);
        $stmt->execute();
        $review = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;

        $data = json_encode($review);
        while (list($label, $val) = each($mapping))
            $data = preg_replace("/$label/", "$val", $data);

        $data_dec = json_decode($data);
        $errors = null;

        $data = array("review" => $data_dec);
        echo format_api(1, $data, count($data), $errors);
    } catch (PDOException $e) {
        api_error($e, "getReviews");
    }
}

function getReviewsList($query, $page = '') {
    $media = new WY_Media();
    $item_per_page = 4;
	$page = (!empty($page)) ? intval($page) : -1;
        $limit = "";
    if ($page > 0) {
 
        $page = $page - 1;
        $item = $page * $item_per_page;
        $limit = " LIMIT $item,$item_per_page";
    }
    try {
        $reviews = new WY_Review($query);
        $count_reviews = $reviews->getReviewsCount();
        $reviews->getReviewsList($limit);

        $count = 0;

        foreach ($reviews->data as $review) {
            $count = $count + 1;
            $review['user_picture'] = $media->getUserProfilePicture($review['email']);
            unset($review['email']);
            $review_res[] = $review;
        }

        //  WAITING FOR ACTIVATION 
        //  $review_res = array();

        if ($count > 0) {
            $data = array(
                "restaurant" => $reviews->restaurant,
                "count" => $count_reviews['count'],
                "score" => $count_reviews['score'],
                "score_desc" => $count_reviews['review_desc'],
                "reviews" => $review_res);

            echo format_api(1, $data, count($reviews->data), NULL);
        } else {
            echo format_api(1, array(), 0, NULL);
        }
    } catch (PDOException $e) {
        api_error($e, "getReviews");
    }
}

function setReviewResponse($app) {

    $restaurant = $app->request->post("restaurant");
    $confirmation = $app->request->post("confirmation");
    $response = $app->request->post("response");
    $token = $app->request->post("token");

    $login = new WY_Login(set_valid_login_type('backoffice'));
    if ($login->checktoken($token)) {
        $review = new WY_Review($restaurant);
        $review->saveResponse($confirmation, $response);
    }

    echo format_api(1, '', 0, '');
}

function postReview($app) {

    require_once("lib/class.review.inc.php");
    
    $content_type = $app->request->getContentType();
    


    if(!empty($content_type) && strpos($content_type,'application/json') !== false){
        
        $params = json_decode($app->request()->getBody(), true);
    }else{
        $params = $app->request->post();
    }
    
        if(!isset($params['comment'])){
            $params['comment'] = '';
        }
  
    try {
        $review = new WY_Review();
        $userHeader = $app->request->getUserAgent(); //"WeeloyPro/2 CFNetwork/758.2.8 Darwin/15.0.0";
        //$userheader= explode( '/', $userHeader );
        $food_rate = (isset($params['food_rate'])) ? $params['food_rate'] :'';
        $ambiance_rate = (isset($params['ambiance_rate'])) ? $params['ambiance_rate'] :'';
        $service_rate = (isset($params['service_rate'])) ? $params['service_rate'] :'';
        $price_rate = (isset($params['price_rate'])) ? $params['price_rate'] :'';
        $grade = (isset($params['grade'])) ? $params['grade'] :0;
        
        if ($review->postReview($params['confirmation_id'], $params['user_id'], $params['restaurant_id'], $grade,$food_rate, $ambiance_rate, $service_rate, $price_rate, $params['comment'], '', $userHeader)) {
            $errors = null;
            $data = array("review" => 'saved');
            $data2 = json_encode($app->request()->headers()->all(),true);
            //$review->testreqHeader($data2);
            echo format_api(1, $data, count($data), $errors);
        } else {
            echo '{"error":{"text":dtc}}';
        }
        //echo '{"review": ' . json_encode($review) . '}';
    } catch (PDOException $e) {
        api_error($e, "postReview");
    }
}
function postReviewGrade($app) {

    require_once("lib/class.review.inc.php");

    $content_type = $app->request->getContentType();
    


    if(!empty($content_type) && strpos($content_type,'application/json') !== false){
        
        $params = json_decode($app->request()->getBody(), true);
    }else{
        $params = $app->request->post();
    }
    
        if(!isset($params['comment'])){
            $params['comment'] = '';
        }
   
  
    try {
        $review = new WY_Review();
        $userHeader = $app->request->getUserAgent(); //"WeeloyPro/2 CFNetwork/758.2.8 Darwin/15.0.0";
        $type ='create';
        //$userheader= explode( '/', $userHeader );
        if(isset($params['type']) && !empty($params['type'])){$type = $params['type'];}
        
        
        $id = $review->postReviewGrade($params['confirmation_id'], $params['user_id'], $params['restaurant_id'], $params['grade'], $params['comment'], $type,'', $userHeader);
        if (!empty($id)) {
            $errors = null;
            $data = array("review" => 'saved','id'=>$id);
            $data2 = json_encode($app->request()->headers()->all(),true);

            echo format_api(1, $data, count($data), $errors);
        } else {
            echo '{"error":{"text":dtc}}';
        }
        //echo '{"review": ' . json_encode($review) . '}';
    } catch (PDOException $e) {
        api_error($e, "postReview");
    }
}

function getUserPostedReviews() {
error_log("SDSds".$_SERVER['PHP_AUTH_USER']);
    $email = '';
    if (!empty($_SERVER['PHP_AUTH_USER'])) {
        $email = $_SERVER['PHP_AUTH_USER'];
    }
    
    $options = array('include_reviews' => true);
    $reviews = new WY_Review();
    $reviews->getUserReviews($email, $options);


    foreach ($reviews->reviews as $review){
            $images = new WY_Media($review->restaurant);
            $review->image = $images->getDefaultPicture($review->restaurant);
    } 
    
    $errors = null;
    $data = array("reviews" => $reviews->reviews);
    echo format_api(1, $data, count($reviews->reviews), $errors);
    
}

function getWidgetReview ($query) {

    // $sql = "SELECT * FROM  restaurant WHERE id = $query limit 1 ";
    // $row = pdo_single_select($sql); 

    return getReviewsList($query);

}
?>