<?

function getMenu($restaurant) {

    $restaurant = clean_text($restaurant);
    $mobj = new WY_Menu($restaurant);
    $menu = "";
    try {
        $categories = $mobj->getCategorieList('public');

        //categories
        $categorie = array();
        foreach ($categories as $categorie) {
            $menus = $mobj->getCategorieItems($categorie['ID']);
            $menu[] = array('categorie' => $categorie, 'items' => $menus);
        }

        $data = array("menu" => $menu);
        $cn = (is_array($menu)) ? count($menu) : 0;
        echo format_api(1, $data, $cn, "");
    } catch (PDOException $e) {
        api_error($e, "getMenu");
    }
}

function getCuisineList($status = '') {
     $res = new WY_restaurant();
    $cuisine = $res->getcuisinilist($status);
        $errors = null;
        $data = array("cuisine" => $cuisine);
        echo format_api(1, $data, count($cuisine), $errors);
    }


function addCuisine($app){
    $cuisine = $app->request->post("name");
    $query = "SELECT  cuisine FROM cuisine WHERE cuisine ='$cuisine'  LIMIT 1 ";
    $sql = "INSERT INTO cuisine (cuisine) VALUES ('$cuisine')";
     $res = new WY_restaurant();
    $result = $res->updateCusion($cuisine,'add');
    $errors = null;
    if($result===1){
                 echo format_api(1, $cuisine, 1, $errors);
            }else{
                $errors="This Cuisine  has already been created.";
                 echo format_api(1, $cuisine, 1, $errors);
            }    
}

function removecuisine($app){
    $cuisine = $app->request->post("name");
    $cuisineId ='1';
    $result = $res->updateCusion($cuisine,'add');
                    $errors = null;
    if($result===1){
     echo format_api(1, $cuisine, 1, $errors);
            }else{
                $errors="This Cuisine not found in table";
                 echo format_api(1, $cuisine, 1, $errors);
            }  
    }


function updatecuisine($app){
    $cuisineID = $app->request->post("id");
    $cuisine = $app->request->post("name");
    $method = $app->request->post("mode");
    $status = $app->request->post("status");
    $cuisine = ucfirst($cuisine);
    $query = "SELECT  cuisine FROM cuisine WHERE cuisine ='$cuisine'  LIMIT 1 ";
    $msg =$cuisine;
    try {
        $db = getConnection();
            $stmt = $db->prepare($query);
            $stmt->execute(); 
            $cuisineCount = $stmt->fetchAll(PDO::FETCH_OBJ);
             if($method=='update'){
                  $msg =$cuisine;
                  $q =  "UPDATE cuisine set cuisine ='$cuisine' where ID ='$cuisineID' "; 
             }else{
                 if(count($cuisineCount)>0){
                        $status = $method =='pending' ? 'pending' : 'active' ;
                        $msg =$status;
                      $q =  "UPDATE cuisine set status ='$status' where cuisine ='$cuisine' "; 
                 }else{
                     $errors="Cuisine not found in database";
                     echo format_api(1, $cuisine, 1, $errors);
                 }
                 
             }
             $stmt = $db->prepare($q);
             $stmt->execute(); 
             $errors = null;
             echo format_api(1, $msg, 1, $errors);
            
  
    } catch(PDOException $e) {
         api_error($e, "app tracking addCuisine");
    }
    
}

?>