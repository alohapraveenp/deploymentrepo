<?

$app->get('/:query', 'getRestaurantInfo');
function getRestaurantInfo($restaurant) {
    $res = new WY_restaurant;
    try {
            $details = $res->getRestaurantDetails($restaurant);
            if(isset($details)) {
                $rew = new WY_Review($details['restaurant']);
                $review = $rew->getpartialUserReview(3);
                // menu
                $menu = array();
                $mobj = new WY_Menu($details['restaurant']);
                $categories = $mobj->getCategorieList('public');
                //categories
                $categorie = array();
                foreach ($categories as $categorie) {
                    $menus = $mobj->getCategorieItems($categorie['ID']);
                    $menu[] = array('categorie' => $categorie, 'items' => $menus);
                }
//                error_log("Restaurant details: ".print_r($details, true));
                $data = array(
                    "restaurantinfo" => $details,
                    'menu' => $menu,
                    'review' => $review,
                    'chef' => $details['chef']
                );
                echo format_api(1, $data, count($data), NULL);
            }
            else echo format_api(-1, "", 0, NULL);
        } catch (PDOException $e) { api_error($e, "getRestaurantInfo"); }
    }
?>
