<?php
require_once("lib/class.restaurant.inc.php");
$app->get('/:query', 'getRestaurant');
$app->post('/:query', 'getRestaurant');

$app->get('/city/:query', 'getCityRestaurants');
$app->post('/city/:query', 'getCityRestaurants');

$app->get('/favourites/', 'getFavouriteRestaurants');
$app->post('/favourites/', 'getFavouriteRestaurants');

$app->get('', 'getAllRestaurants');
$app->post('', 'getAllRestaurants');

$app->get('/getpicture/:query', 'getAllPictures');

$app->get('/openhours/:query', 'getRestaurantOpenHour');

$app->get('/info/:query', 'RestaurantInfo');
$app->get('/offers/:restaurant/:nb_offers', 'getBestOffers');

$app->get('/map/:query', 'getMap');
$app->get('/pictures/:restaurant/:page', 'getRestaurantPictures');

$app->post('/bullet/', function() use ($app) {
    $data = json_decode($app->request()->getBody(), true);
    return bulletConfig($data['restaurant'], $data['data'], $data['token']);
});

$app->get('/section/:query','getRestaurantSection');

$app->get('/events/:query', 'getEventsRestaurant');
$app->post('/events/:query', 'getEventsRestaurant');

$app->get('/events/active/:query', 'getActiveEventsRestaurant');

$app->get('/events/active/public/:query', 'getActivePublicEventsRestaurant');


$app->get('/gettagrestaurant/:query', 'getlistAssignedtagRestaurant');
$app->get('/gettags/', 'getTags');
$app->get('/service/categories', 'getServiceCategories');
$app->get('/service/services', 'getServices');
$app->get('/service/restaurantservices/:restaurant', 'getRestaurantServices');
$app->get('/service/restaurantservices/:restaurant/:option', 'getRestaurantServices');

$app->post('/service/save', function () use ($app) {
    return saveRestaurantServices($app);
});

$app->get('/media/picture/:query/:type', 'getMediaPictRestaurant');
$app->post('/media/picture/:query/:type', 'getMediaPictRestaurant');
$app->post('/dsb/register', function () use ($app) {
    $restaurant = new WY_restaurant();
    echo $restaurant->registerDSBRestaurant($_REQUEST['restaurant'], $_FILES['file']) ? format_api(1, "Success!", 1, NULL) : format_api(0, "Invalid input parameters!", 0, NULL);
});
// we do not use the date
$app->get('/allote/:restaurant',
	function ($restaurant) {
		$pax = 1;
		$product = "";
		return allotmentData($restaurant, $pax, $product); 
});

$app->get('/allote/:restaurant/:device_date', 
	function ($restaurant, $device_date) { 
		$pax = 1;
		$product = "";
		return allotmentData($restaurant, $pax, $product); 
});

$app->post('/allote/', function () use ($app) {
    $data = json_decode($app->request()->getBody(), true);
    if(!isset($data['pax'])) $data['pax'] = 1;
    if(!isset($data['product'])) $data['product'] = "";
    if(!isset($data['platform'])) $data['platform'] = "";
    allotmentData($data['restaurant'], $data['pax'], $data['product'], $data['platform']);
});

$app->post('/allotepax/', function () use ($app) {
    $data = json_decode($app->request()->getBody(), true);
    if(!isset($data['product'])) $data['product'] = "";
    if(!isset($data['platform'])) $data['platform'] = "";
    allotmentData($data['restaurant'], $data['pax'], $data['product'], $data['platform']);
});


$app->post('/availslot/', function () use ($app) {
    $data = json_decode($app->request()->getBody(), true);
    if(!isset($data['product'])) $data['product'] = "";
    if(!isset($data['platform'])) $data['platform'] = "";
    allotment1slotData($data['restaurant'], $data['date'], $data['time'], $data['pers'], '', $data['product'], $data['token'],  $data['platform']);
});

// same as above with the difference that this is a modification of an existing booking => 
// you would need to 'virtually' release the booking availability first, especially if modification is for same day and 'almost' same time

$app->post('/dayavailable/', function () use ($app) {
    $data = json_decode($app->request()->getBody(), true);
    // add this condition for mobile... 
	if(count($data) < 1) {
        $data = $app->request->post();
    	}    
    dayAvailability($data['restaurant'], $data['product'], $data['time'], $data['pers']);
});

$app->post('/availslotbkg/', function () use ($app) {
    $data = json_decode($app->request()->getBody(), true);
    if(!isset($data['product'])) $data['product'] = "";
    if(!isset($data['platform'])) $data['platform'] = "";
    allotment1slotData($data['restaurant'], $data['date'], $data['time'], $data['pers'], $data['booking'], $data['product'], $data['token'],  $data['platform']);
});

$app->get('/dispoday/:restaurant/:adate',
	function ($restaurant, $adate) { 
		$product = "";
		return getdayDispo($restaurant, $adate, $product); 
});

$app->post('/dispoday/', function () use ($app) {
    $data = json_decode($app->request()->getBody(), true);
    if(!isset($data['product'])) $data['product'] = "";
    getdayDispo($data['restaurant'], $data['adate'], $data['product']);
});

$app->post('/bookingdata/', function () use ($app) {
    $data = json_decode($app->request()->getBody(), true);
    getBookingData($data['year'], $data['type'], $data['email'], $data['restaurant'], $data['token']);
});

$app->post('/bookingdayreport/', function () use ($app) {
    $data = json_decode($app->request()->getBody(), true);
    if(!isset($data['restaurant'])) $data['restaurant'] = "";
    getBookingDayReport($data['whenday'], $data['type'], $data['email'], $data['restaurant'], $data['token']);
});

$app->post('/bookinginvoice/', function () use ($app) {
    $data = json_decode($app->request()->getBody(), true);
    if(!isset($data['restaurant'])) $data['restaurant'] = "";
    getbookinginvoice($data['whenday'], $data['type'], $data['email'], $data['restaurant'], $data['token']);
});

$app->get('/manager/:query', 'getRestaurantByManager');
$app->post('/manager/:query', 'getRestaurantByManager');


$app->get('/location/', 'getCityList');
$app->post('/location/', 'getCityList');
$app->get('/location/area', function() use ($app) {
    return getAreaList($app);
   });
$app->post('/location/addarea', function() use ($app) {
    return addGeoArea($app);
   });
 $app->post('/location/updatearea', function() use ($app) {
    return  updateArea($app);
   });

$app->get('/search/:query', 'findByCity');
$app->post('/search/:query', 'findByCity');

$app->get('/food/:query', 'findByCuisine');
$app->post('/food/:query', 'findByCuisine');

$app->get('/picture/:query', 'getPictRestaurant');
$app->post('/picture/:query', 'getPictRestaurant');

$app->get('/getPickupHours/:restaurant', function ($restaurant) {
    try {
        $res = new WY_restaurant();
        $res_pickuphours = $res->getPickupHours($restaurant);
        echo format_api(1, $res_pickuphours, 1, NULL);
    } catch (PDOException $e) { api_error($e, "showCaseRestaurants"); }
});

$app->get('/getShowcase/:current_city/:count', function ($current_city, $count) {
    try {
        $res = new WY_restaurant();
        $res_showcase = $res->getShowcaseRestaurant($current_city, $count);
        echo format_api(1, $res_showcase, 1, NULL);
    } catch (PDOException $e) { api_error($e, "showCaseRestaurants"); }
});

$app->get('/getFoodSelfieContestShowcase/:current_city/:count', function ($current_city, $count) {
    try {
        $res = new WY_restaurant();
        $res_showcase = $res->getSelfieContestShowcaseRestaurant($current_city, $count);
        echo format_api(1, $res_showcase, 1, NULL);
    } catch (PDOException $e) {
        api_error($e, "showCaseRestaurants");
    }
});

$app->get('/catering/menu/:restaurant', function($restaurant) {
    try {
        // menu catering only
        $mobj = new WY_Menu($restaurant);
        
        $categories = $mobj->getCategorieList();
        //categories
        $categorie = array();
        foreach ($categories as $categorie) {
            $menus = $mobj->getCategorieItems($categorie['ID'], 'catering_only');
            if(count($menus)>0){
                $menu[] = array('categorie' => $categorie, 'items' => $menus);
            }
        }
        if(count($menu) < 0) {
            throw new Exception("item not found", 1);
        } else {
            echo format_api(1, $menu, count($menu), NULL);
        }
   } catch (Exception $e) {
        echo format_api(0, $e->getMessage(), 1, NULL);
   }   
});

$app->get('/catering/gallery/:restaurant_id/:limit', function($restaurant_id, $limit) {
    try {
        $sql = "SELECT * FROM media WHERE object_type = 'menu' and restaurant = '{$restaurant_id}' and status = 'active' LIMIT $limit";
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $items = $stmt->fetchAll(PDO::FETCH_OBJ);
        if(count($items) < 0) {
            throw new Exception("item not found", 1);
        } else {
            echo format_api(1, $items, count($items), NULL);
        }
   } catch (Exception $e) {
        echo format_api(0, $e->getMessage(), 1, NULL);
   }   
});
function getCityRestaurants($city) {
    if (isset($city) && !empty($city))
    try {
        $city = clean_text($city);
        $res = new WY_restaurant;
	    $data = $res->getCityRestaurants($city, isset($_SERVER['PHP_AUTH_USER']) ? $_SERVER['PHP_AUTH_USER'] : "");
    	if($res->result > 0)
	   		echo format_api(1, $data, count($data), NULL);
	   	else echo format_api(-1, "", 0, NULL);
    } catch (PDOException $e) { api_error($e, "getCityRestaurants"); }
    catch (Exception $e) {
        api_error($e, "getCityRestaurants");
   }      
}
function getFavouriteRestaurants() {
    try {
        $res = new WY_restaurant;
        $data = $res->getFavouriteRestaurants();
        if($res->result > 0)
            echo format_api(1, $data, count($data), NULL);
        else echo format_api(-1, "", 0, NULL);
    } catch (PDOException $e) { api_error($e, "getFavouriteRestaurants"); }
    catch (Exception $e) {
        api_error($e, "getFavouriteRestaurants");
   }     
}
function getRestaurant($query) {
    //error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": query: ".$query);
    $sql = "SELECT restaurant, title, address, zip, country, city, region, cuisine, rating, email, tel, stars, GPS, likes, images, pricing, creditcard, mealtype, stars, mealslot, '' as dinesfree, chef, openhours, description, wheelvalue "
            . "FROM restaurant "
            . "WHERE restaurant=:query AND is_displayed = 1 limit 1";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("query", $query);
        $stmt->execute();
        $restaurant = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($restaurant as $key => $resto) {
            //if(!empty($resto['logo'])){
            $restaurant[$key]['media_domain'] = __MEDIA__SERVER_NAME__;
            $logo = "SELECT name as logo FROM media WHERE restaurant=:query and object_type = 'Logo'";

            $stmt_logo = $db->prepare($logo);

            $stmt_logo->bindParam("query", $query);
            $stmt_logo->execute();

            $data_logo = $stmt_logo->fetch(PDO::FETCH_ASSOC);
            $restaurant[$key]['logo'] = $data_logo['logo'];
            //}
        }
        $db = null;
        $errors = null;
        $data = array("restaurant" => $restaurant);
        echo format_api(1, $data, count($restaurant), $errors);
    } catch (PDOException $e) {
        api_error($e, "getRestaurant");
    }
}

function getAllRestaurants() {
    $sql = "select restaurant, title, city, cuisine, rating, logo, stars, GPS, openhours, images, wheelvalue, likes "
            . "FROM restaurant "
            . "WHERE restaurant.status IN ('active') AND is_displayed = 1 ORDER BY wheelvalue DESC, restaurant ASC LIMIT 50";
    try {
        $db = getConnection();
        $stmt = $db->query($sql);
        $restaurant = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $db = null;
        foreach ($restaurant as $key => $resto) {
            $restaurant[$key]['media_domain'] = __MEDIA__SERVER_NAME__;
            if (!empty($resto['images'])) {
                $tmp = explode("|", $resto['images']);
                $restaurant[$key]['images'] = '';
                for ($i = 0; $i < count($tmp); $i++) {
                    if (strtolower(substr($tmp[$i], 0, 5)) != 'logo.' && strtolower(substr($tmp[$i], 0, 5)) != 'chef.' && strtolower(substr($tmp[$i], 0, 5)) != 'wheel') {
                        $restaurant[$key]['images'] = $tmp[$i];
                        break;
                    }
                }
            }
        }

        $errors = null;
        $data = array("restaurant" => $restaurant);

        echo format_api(1, $data, count($restaurant), $errors);
    } catch (PDOException $e) {
        api_error($e, "getAllRestaurants");
    }
}

function getAllPictures($restaurant){
    $restaurant =trim($restaurant);
    $media = new WY_Media($restaurant);
    $img_array = $media->getRestaurantPictures($restaurant,'categories') ;
    foreach ($img_array as $img) {
            $images[] = $img->getName();
        }
    
     $errors = null;
     $data = array("pictures" => $images);
    echo format_api(1, $data, count($images), $errors);
}

function getRestaurantOpenHour($query) {
    $res = new WY_restaurant;
    $res->getRestaurant($query);
	if($res->result < 0) {
		echo format_api(-1, '', 0, $res->msg);
		return;
		}
    $hours = $res->openhours;
    $hours = $res->getOpeningHoursListing($query, true);

    if ($hours) {
        set_header_status(200);
        echo format_api(1, $hours, 1, NULL);
    } else {
        set_header_status(200);
        $error = 'An error occured, please try again later';
        echo format_api(1, NULL, 1, $error);
    }
}

function RestaurantInfo($restaurant) {

    $restaurant = clean_text($restaurant);
    $res = new WY_restaurant;
    try {
	    $data = $res->getRestaurant($restaurant);
    	if($res->result > 0)
	   		echo format_api(1, $data, count($data), NULL);
	   	else echo format_api(-1, "", 0, NULL);
    } catch (PDOException $e) {
        api_error($e, "RestaurantInfo");
    }
    
}

function getBestOffers($restaurant, $nb_offers){
    $res = new WY_restaurant();
    try {
            $data = $res->getBestOffer($restaurant, NULL, NULL, $nb_offers);

            $errors = NULL;
            echo format_api(1, $data, count($restaurant), $errors);
    } catch (PDOException $e) {
        api_error($e, "getMap");
    }
}

function getMap($query) {
    $sql = "SELECT restaurant, title, city, cuisine, rating, logo, stars, GPS FROM restaurant WHERE city=:query";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("query", $query);
        $stmt->execute();
        $restaurant = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;

        $errors = null;
        $data = array("restaurant" => $restaurant);
        echo format_api(1, $data, count($restaurant), $errors);

        //echo '{"restaurant": ' . json_encode($restaurant) . '}';
    } catch (PDOException $e) {
        api_error($e, "getMap");
    }
}

function getRestaurantPictures($restaurant, $page = '1'){

    try {
        $media = new WY_Media();
        $data = $media->getRestaurantRandomPictures($restaurant, $page);
        echo format_api(1, $data, count($data), "");
    } catch (PDOException $e) {
        api_error($e, "getActiveEventsRestaurant");
    }
    
}

function getRestaurantSection($query){
        $section = new WY_Restaurant_Section();
        $errors=null;
        $data = $section->getRestaurantSectionConfig($query);
        
        echo format_api(1, $data, count($data), $errors);
    
}

function getEventsRestaurant($restaurant) {

    $restaurant = clean_text($restaurant);
    $eobj = new WY_Event($restaurant);
    try {
        $obj = $eobj->getEvents();
        $event = $obj["event"];
        $data = array("event" => $event);
        echo format_api(1, $data, count($event), "");
    } catch (PDOException $e) {
        api_error($e, "getEventsRestaurant");
    }
}

function getActiveEventsRestaurant($restaurant) {
    $restaurant = clean_text($restaurant);
    $eobj = new WY_Event($restaurant);
        $evbooking = array();
    try {
        $event = $eobj->getActiveEvents();
        $evbookable = (WY_restaurant::s_checkeventbooking($restaurant) != 0) ? 1 : 0;
       if($evbookable){
           $evbooking = $eobj->getEventBooking();
       }
        $data = array("event" => $event,"evtbookinglist" => $evbooking);
        echo format_api(1, $data, count($event), "");
    } catch (PDOException $e) {
        api_error($e, "getActiveEventsRestaurant");
    }
}

function getActivePublicEventsRestaurant($restaurant) {
    $restaurant = clean_text($restaurant);
    $eobj = new WY_Event($restaurant);
        $evbooking = array();
    try {
        $event = $eobj->getActiveEvents('public');
        $evbookable = (WY_restaurant::s_checkeventbooking($restaurant) != 0) ? 1 : 0;
       if($evbookable){
           $evbooking = $eobj->getEventBooking();
       }
        $data = array("event" => $event,"evtbookinglist" => $evbooking);
        echo format_api(1, $data, count($event), "");
    } catch (PDOException $e) {
        api_error($e, "getActiveEventsRestaurant");
    }
}

function getlistAssignedtagRestaurant($tag){
	$res = new WY_restaurant();
	$assigned = $res->getListAssignedTagRestaurant($tag);
	$not_assigned = $res->getListNotAssignedTagRestaurant($tag);
	$data = array("assigned" => $assigned,'notassigned'=>$not_assigned);
	$errors=null;
	echo format_api(1, $data, count($data), $errors);
}

function getTags() {
    $res = new WY_restaurant();
    $data = $res->getTags();
    echo format_api(1, $data, count($data), null);
}
function getServiceCategories() {
    require_once 'lib/class.service.inc.php';

    $service = new WY_Service();
    $categories = $service->getCategories();

    if ($categories) {
        set_header_status(200);
        echo format_api(1, array('categories' => $categories), 1, NULL);
    } else {
        set_header_status(200);
        $error = 'We couldn’t get service categories';
        echo format_api(1, NULL, 1, $error);
    }
}

function getServices() {
    require_once 'lib/class.service.inc.php';

    $service = new WY_Service();
    $services = $service->getServices();

    if ($services) {
        set_header_status(200);
        echo format_api(1, array('services' => $services), 1, NULL);
    } else {
        set_header_status(200);
        $error = 'We couldn’t get service categories';
        echo format_api(1, NULL, 1, $error);
    }
}

function getRestaurantServices($restaurant,  $option ='') {
    require_once 'lib/class.service.inc.php';
    $service = new WY_Service();
    
    $restaurantservices = $service->getRestaurantServices($restaurant, $option);

    if ($restaurantservices) {
        set_header_status(200);
        echo format_api(1, array('restaurantservices' => $restaurantservices), 1, NULL);
    } else {
        set_header_status(200);
        $error = 'We couldn’t get service categories';
        echo format_api(1, NULL, 1, $error);
    }
}

function saveRestaurantServices($app) {
    require_once 'lib/class.service.inc.php';

    $restaurant = $app->request->post("restaurant");
    $active_services = $app->request->post("active_services");

    $service = new WY_Service();
    $resultat = $service->updateServices($restaurant, $active_services);

    if ($resultat) {
        set_header_status(200);
        echo format_api(1, 'success', 1, NULL);
    } else {
        set_header_status(200);
        $error = 'An error occured, please try again later';
        echo format_api(1, NULL, 1, $error);
    }
}

function getMediaPictRestaurant($restaurant,$type) {
    $restaurant = clean_text($restaurant);
    $media = new WY_Media($restaurant);
    try {
        $data = $media->getRestaurantMedia($restaurant, $type);
        $errors = null;
        echo format_api(1, $data, count($data), $errors);
    } catch (PDOException $e) {
        api_error($e, "getMediaPictRestaurant");
    }
}

function allotmentData($restaurant, $pax, $product, $platform = "") {

    try {
        $restaurant = clean_text($restaurant);
        $res = new WY_restaurant;
        $res->getRestaurant($restaurant);
        if($res->result < 0) {
	    	echo format_api(-1, '', 0, $res->msg);
	    	return;
       		}
        $data = $res->getAlloteOpenHours($restaurant, $pax, $product, $platform);
        if(count($data) > 0){
        	echo stripslashes(format_api(1, $data, 1, "ok"));
        }
        else echo stripslashes(format_api(0, null, 0, $res->msg));
    } catch (Exception $e) {
        api_error($e, "allotmentData");
    }
}

function allotment1slotData($restaurant, $rdate, $rtime, $pax, $conf, $product, $token, $platform="") {

	$ttAr = explode(":", $rtime);
	if(count($ttAr) < 2 || intval($ttAr[0]) < 0 || intval($ttAr[0]) > 24 || intval($ttAr[1]) < 0 || intval($ttAr[1]) > 60) {
		echo format_api(-1, "0", -1, "invalid time format");
		return;
		}
		
    try {
        $restaurant = clean_text($restaurant);
        $res = new WY_restaurant;
        $res->getRestaurant($restaurant);
        if($res->result < 0) {
	    	echo format_api(-1, '', 0, $res->msg);
	    	return;
       		}
        $ret = $res->CheckAvailability($restaurant, $rdate, $rtime, $pax, $conf, $product, $platform);
        echo ($res->result > 0) ? format_api(1, "1", 1, "available") : format_api(1, "0", $ret, "not available");
    } catch (Exception $e) {
        api_error($e, "allotment1slotData");
    }
}

function dayAvailability($restaurant, $product, $rtime, $pax) {
        // hack for Burnt Ends
        // if($rtime == '12:30' || $rtime == '13:00'){ $rtime = '12:00'; }
        // if($rtime == '18:30' || $rtime == '20:00' || $rtime == '20:30'){ $rtime = '18:00'; }      
  
	$ttAr = explode(":", $rtime);

	if(count($ttAr) < 2 || intval($ttAr[0]) < 0 || intval($ttAr[0]) > 24 || intval($ttAr[1]) < 0 || intval($ttAr[1]) > 60) {
		echo format_api(-1, "", -1, "invalid time format");
		return;
		}
    try {
        $restaurant = clean_text($restaurant);
        $res = new WY_restaurant;
        $res->getRestaurant($restaurant);
        if($res->result < 0) {
	    	echo format_api(-1, '', 0, $res->msg);
	    	return;
       		}
        $data = $res->CheckDayAvailability($restaurant, $product, $rtime, $pax);
        if($res->result > 0)
        	echo format_api(1, $data, count($data), $res->msg);
		else echo format_api(-1, "", 0, $res->msg);
    } catch (Exception $e) {
        api_error($e, "dayavailable");
    }
}

function getdayDispo($restaurant, $aDate, $product) {

    try {
        $restaurant = clean_text($restaurant);
        $res = new WY_restaurant;
        $res->getRestaurant($restaurant);
        if($res->result < 0) {
	    	echo format_api(-1, '', 0, $res->msg);
	    	return;
       		}
        $data = $res->reportAvailability($restaurant, $aDate, $product);
        echo stripslashes(format_api(1, $data, 1, "ok"));
    } catch (Exception $e) {
        api_error($e, "getdayDispo");
    }
}

function getBookingData($year, $type, $email, $restaurant, $token) {

    $login = new WY_Login(set_valid_login_type('backoffice'));
    if ($login->checktoken($token) == false || $login->email != $email) {
		echo format_api(-1, null, 0, "invalid token");
		return;
		}

	$bkg = new WY_Booking;
	$errors	= "";
	
    try {
    	$book = $bkg->getBookingdata($year, $type, $email, $restaurant, $login->membertype);
    	if($bkg->result < 0) {
    		echo format_api(-1, null, 0, $bkg->msg);
			return;
			}
        $data = array("restaurant" => $restaurant, "fields" => $book["fields"], "bookings" => $book["bookings"], "title" => $book["title"]);
        echo format_api(1, $data, count($book["bookings"]), $errors);
    } catch (PDOException $e) {
        api_error($e, "getBookingData");
    }
}

function getBookingDayReport($when, $type, $email, $restaurant, $token) {

    $login = new WY_Login(set_valid_login_type('backoffice'));
    if ($login->checktoken($token) == false || $login->email != $email) {
		echo format_api(-1, null, 0, "invalid token");
		return;
		}

	$bkg = new WY_Booking;
	$errors	= "";
	
    try {
    	$bookings = $bkg->getBookingDayReport($when, $type, $email, $restaurant, $login->membertype);
    	if($bkg->result < 0) {
    		echo format_api(-1, null, 0, $bkg->msg);
			return;
			}
        $data = array("bookings" => $bookings);
        echo format_api(1, $data, count($bookings), $errors);
    } catch (PDOException $e) {
        api_error($e, "getBookingDayReport");
    }
}

function getbookinginvoice($when, $type, $email, $restaurant, $token) {

    $login = new WY_Login(set_valid_login_type('backoffice'));
    if ($login->checktoken($token) == false || $login->email != $email) {
		echo format_api(-1, null, 0, "invalid token");
		return;
		}

	$bkg = new WY_Booking;
	$errors	= "";
	
    try {
    	$bookings = $bkg->getbookinginvoice($when, $type, $email, $restaurant, $login->membertype);
    	if($bkg->result < 0) {
    		echo format_api(-1, "FAIL", 0, $bkg->msg);
			return;
			}
        $data = array("bookings" => $bookings);
        echo format_api(1, $data, count($bookings), $errors);
    } catch (PDOException $e) {
        api_error($e, "getbookinginvoice");
    }
}


function getRestaurantByManager($manager) {

    $res = new WY_restaurant();
    $data = $res->getRestaurantByManager($manager);
    $errors = null;
    echo format_api(1, $data, count($data), $errors);
}

function getCityList() {

    try {
        $res = new WY_restaurant();
        $city = $res->getActiveCities();
        $errors = null;
        $data = array("city" => $city);
        echo format_api(1, $data, count($city), $errors);

        //echo json_encode($city);
    } catch (PDOException $e) {
        api_error($e, "getCityList");
    }
}

function getAreaList($app) {
    
    $citycode =$app->request->get('citycode');
    $countrycode =$app->request->get('countrycode');
    $res = new WY_restaurant();
    $area = $res->getAreaList($citycode);
        $errors = null;
        $data = array("area" => $area);
        echo format_api(1, $data, count($area), $errors);

    
    }
	
function addGeoArea($app){
    $area = $app->request->post("area");
    $country = $app->request->post("country");
     $weeloyName = $app->request->post("wname");
    $city = $app->request->post("city");
    $lat = $app->request->post("lat");
    $lng = $app->request->post("lng");
        $res = new WY_restaurant();
        $result = $res->addGeoArea($country,$city,$area,$lat,$lng,$weeloyName);
        $errors = null;
        if($result===1){
                 echo format_api(1, $area, 1, $errors);
                
            }else{
                $errors="This Area  has already been created.";
                 echo format_api(1, $area, 1, $errors);
            }   
}

function updateArea($app){
     
    $area = $app->request->post("area");
    $country = $app->request->post("country");
    $city = $app->request->post("city");
    $updateId = $app->request->post("updateId");
    $method = $app->request->post("method");
    $weeloyName = $app->request->post("wname");
    $lat = $app->request->post("lat");
    $lng = $app->request->post("lng");
    $res = new WY_restaurant();
        $result = $res->updateArea($country,$city,$area,$lat,$lng,$weeloyName,$updateId);
        $msg =$area;
    	$errors = null;
        if($result===1){
                	echo format_api(1, $msg, 1, $errors);
                }else{
                     $errors="Area not found in database";
                     echo format_api(1, $msg, 1, $errors);
                }
            
    }
    
function findByCity($query) {


    $query = preg_replace("/_/", " ", $query);  // for Kuala Lumpur
    $sql = "SELECT restaurant, title, city, cuisine, rating, logo, stars, GPS, openhours FROM restaurant WHERE city=:query ORDER BY restaurant";
    $binlog = true;
    if ($query == "ALL") {
        $sql = "SELECT restaurant, title, city, cuisine, rating, logo, stars, GPS, openhours FROM restaurant where logo != '' ORDER BY city, restaurant";
        $binlog = false;
    }
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        if ($binlog)
            $stmt->bindParam("query", $query);
        $stmt->execute();
        $restaurant = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;

        $errors = null;
        $data = array("restaurant" => $restaurant);
        echo format_api(1, $data, count($restaurant), $errors);
        //echo '{"restaurant": ' . json_encode($restaurant) . '}';
    	} catch (PDOException $e) {
       	 api_error($e, "findByCity");
    }
}

function findByCuisine($query) {

    $request = "";
    $queryAr = explode("|", $query);
    $limit = count($queryAr);
    if ($limit < 1) {
        echo '{"error":{"text":food is empty}}';
        return;
    }

    for ($i = 0, $sep = ""; $i < $limit; $i++, $sep = " or ")
        $request .= $sep . "cuisine like '%" . $queryAr[$i] . "%'";

    $sql = "SELECT restaurant, title, city, cuisine, rating, logo, stars, GPS, openhours FROM restaurant WHERE $request ORDER BY restaurant";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $restaurant = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;

        $errors = null;
        $data = array("restaurant" => $restaurant);
        echo format_api(1, $data, count($restaurant), $errors);
        //echo '{"restaurant": ' . json_encode($restaurant) . '}';
    } catch (PDOException $e) {
        api_error($e, "findByCuisine");
    }
}

function getPictRestaurant($query) {

    $query = preg_replace("/_/", " ", $query);  // for Kuala Lumpur
    $sql = "SELECT images FROM restaurant WHERE restaurant=:query ORDER BY images";
    $binlog = true;
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        if ($binlog)
            $stmt->bindParam("query", $query);
        $stmt->execute();
        $pictures = $stmt->fetchAll(PDO::FETCH_OBJ);
        if (count($pictures) < 1) {
            $errors = null;
            $data = array("pictures" => '');
            echo format_api(1, $data, 0, $errors);
            //echo '{"pictures": }';
            return;
        }
        $db = null;
        $val = $pictures[0]->images;
        $valAr = explode("|", $val);
        for ($i = 0; $i < count($valAr); $i++) {
            if ($i > 0)
                $pictures[$i] = new stdClass();
            $pictures[$i]->images = $valAr[$i];
        }

        $errors = null;
        $data = array("pictures" => $pictures);
        echo format_api(1, $data, count($pictures), $errors);

        //echo '{"pictures": ' . json_encode($restaurant) . '}';
    } catch (PDOException $e) {
        api_error($e, "getPictRestaurant");
    }
}

function bulletConfig($restaurant, $data, $token) {
    $login = new WY_Login(set_valid_login_type('backoffice'));
    if ($login->checktoken($token)) {
        $resto = new WY_restaurant($restaurant);
        $resto->updateGeneric($restaurant, "bullet", $data);
        if($resto->result > 0) echo format_api(1, '', 0, $resto->msg);
        else echo format_api(-1, '', 0, $resto->msg);
    } else echo format_api(-1, '', 0, 'wrong token');
}
?>