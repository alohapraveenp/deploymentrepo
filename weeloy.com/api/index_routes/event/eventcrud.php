<?

function setEvent($app) {

    $restaurant = $app->request->post("restaurant");
    $name = $app->request->post("name");
    $title = $app->request->post("title");
    $city = $app->request->post("city");
    $country = $app->request->post("country");
    $start = $app->request->post("start");
    $end = $app->request->post("end");
    $description = $app->request->post("description");
    $picture = $app->request->post("picture");
    $morder = $app->request->post("morder");
    $price = $app->request->post("price");
    $maxpax = $app->request->post("maxpax");
    $is_homepage = $app->request->post("is_homepage");
    $display = $app->request->post("display");
    $token = $app->request->post("token");

    $login = new WY_Login(set_valid_login_type('backoffice'));
    if ($login->checktoken($token)) {
        $mobj = new WY_Event($restaurant);
        list($val, $msg) = $mobj->insertEvent($name, $title, $morder, $city, $country, $start, $end, $description, $picture, $is_homepage, $display, $price, $maxpax);
        if ($val > 0)
            echo format_api(1, '', 0, 'OK');
        else
            echo format_api(-1, '', 0, $msg);
    } else
        echo format_api(-1, '', 0, 'wrong token');
}

function deleteEvent($app) {

    $restaurant = $app->request->post("restaurant");
    $name = $app->request->post("name");
    $token = $app->request->post("token");

    $login = new WY_Login(set_valid_login_type('backoffice'));
    if ($login->checktoken($token)) {
        $mobj = new WY_Event($restaurant);
        list($val, $msg) = $mobj->delete($name);
        if ($val > 0)
            echo format_api(1, '', 0, '');
        else
            echo format_api(-1, '', 0, $msg);
    } else
        echo format_api(-1, '', 0, 'wrong token');
}

function updateEvent($app) {

    $restaurant = $app->request->post("restaurant");
    $name = $app->request->post("name");
    $title = $app->request->post("title");
    $city = $app->request->post("city");
    $country = $app->request->post("country");
    $start = $app->request->post("start");
    $end = $app->request->post("end");
    $description = $app->request->post("description");
    $picture = $app->request->post("picture");
    $morder = $app->request->post("morder");
    $price = $app->request->post("price");
    $maxpax = $app->request->post("maxpax");
    $is_homepage = $app->request->post("is_homepage");
    $display = $app->request->post("display");
    $token = $app->request->post("token");

    $login = new WY_Login(set_valid_login_type('backoffice'));
    if ($login->checktoken($token)) {
        $mobj = new WY_Event($restaurant);
        list($val, $msg) = $mobj->updateEvent($name, $title, $morder, $city, $country, $start, $end, $description, $picture, $is_homepage, $display, $price, $maxpax);
        if ($val > 0)
            echo format_api(1, '', 0, 'OK');
        else
            echo format_api(-1, '', 0, $msg);
    } else
        echo format_api(-1, '', 0, 'wrong token');
}

function eventOrder($order_id){

//    $sql = "SELECT event_booking.id, event_booking.event_id, event_booking.orderID,event_booking.number_people, event_booking.paykey,event_booking.amount, event_booking.curency, event_booking.time, event_booking.first_name, event_booking.last_name, event_booking.email, event_booking.phone, event_booking.company,event_booking.status, event_booking.special_request, event_booking.created_at, event_booking.updated_at,payment.payment_method FROM event_booking,payment WHERE event_booking.id = '{$order_id}' and event_booking.paykey =payment.payment_id limit 1";
//    $db = getConnection();
//    $stmt = $db->prepare($sql);
//    $stmt->execute();
//    $order = $stmt->fetch(PDO::FETCH_ASSOC);

    
    $event = new WY_Event();
    $notify = new WY_Notification;
    $order =  $event->getEventTicket($order_id); 
    
  
    if(isset($order)) {
           $payment = new WY_Payment();
        if(isset($order['payment_method']) && ($order['payment_method']=='paypal' && $order['status'] == 'pending')){
            $paypal = new WY_Paypal();
            $transStatus = $paypal->getPaymentDetails($order['paykey']);
  
            if(isset($transStatus) && $transStatus =='COMPLETED'){
                $payment->updatepaymentDetails($transStatus,$order['event_id'],$order['amount'],$order['paykey']);
                $payment->updateEventPaymentId($order['event_id'],$order['orderID'],$transStatus,$order['paykey'],'');
                $order['status'] = $transStatus;
                $event->order['status'] = $transStatus;
                $event->tracking ='WEBSITE';
                $event->white_label = true;
                $notify->notifyEvent($event, 'event_booking');
            }
        }
        echo format_api(1, $order, 1, NULL);
    } else {
        echo format_api(0, "can't find order", 1, NULL);
    }
    
}

function getOrderID($order_id) {
    try {
        $order = WY_OrderOnline::getOrder($order_id);
         if(count($order) == 0) {
            throw new Exception("order not found", 1);
        } else {
            //notifyOrder($order_id);
            echo format_api(1, $order, 1, NULL);
        }
    } catch (Exception $e) {
        echo format_api(0, 'order not found', 1, NULL);
    }
}

function getOrderDetails($order_id) {
    try {
        $items = WY_OrderOnline::getOrderDetails($order_id);
        if(count($items) < 0) {
            throw new Exception("item not found", 1);
        } else {
            echo format_api(1, $items, count($items), NULL);
        }
   } catch (Exception $e) {
        echo format_api(0, $e->getMessage(), 1, NULL);
   }   
}

function getOrderList($app) {
    try {
        $user = $_SESSION['user']['email'];
        
        //$user='philippe.benedetti@weeloy.com';
        
        $data = $app->request()->get();
        if(isset($data['page'])) {
            $page = $data['page'];
        } else {
            $page = 1;
        }
        if(!preg_match('/[0-9]+/', $page)) {
            throw new Exception("input invalid", 1);
        }
        if($page < 1) {
            throw new Exception("input invalid", 1);
        }
        $take = 20;
        $skip = ($page - 1) * $take;
        
        $orders = WY_OrderOnline::getOrderList($user, $take, $skip);
        
        $data = [
            'orders' => $orders['orders'],
            'total_orders' => $orders['count'][0],
        ];
        if(count($orders) == 0) {
            throw new Exception("order not found", 1);
        } else {
            echo format_api(1, $data, count($orders), NULL);
        }
    } catch (Exception $e) {
        echo format_api(0, $e->getMessage(), 1, NULL);
    }
}

///   No SQL in the api index file
function notifyOrder($order_id){
       $order = new WY_OrderOnline();
       $items = $order->notifyonlineorder($order_id);
       $notify = new WY_Notification;
       
       
       error_log(print_r($items[0],true));
       error_log(print_r($items,true));
       
       
       $result= $notify->notifyCatering($items[0],$items);
}
///   No SQL in the api index file

?>