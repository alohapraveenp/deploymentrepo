<?

// Full text search
/**
 * Return Restaurant Data
 *
 * @return mixed
 */
$app->get('/restaurant', function () use ($app) {
    return find($app);
});

/**
 * return aggregate of restaurants
 *
 * @return mixed
 */
 
$app->get('/fulltext', function () use ($app) {
    $request = $app->request->get();
    $request = filter_var_array($request, FILTER_SANITIZE_STRING);
    if (isset($request['query']) && $request['query'] != '') {
        $query = str_replace("*", "", $request['query']);
        $query = preg_replace("/\@.*$/", "", $query);
        $explode_array = explode(" ", $query);
        $soundex = [soundex($query)];
        foreach ($explode_array as $word) {
            $soundex[] = soundex($word);
        }
        $soundex = implode(" ", $soundex);
        $city =  (!empty($request['city'])) ? $request['city'] : $city;
        //error_log(__FILE__." ".__FUNCTION__." ".__LINE__." query: ".$query);
        $sql = "SELECT id, title, restaurant, city, cuisine, ROUND(MATCH(title, cuisine, city, soundex_index) AGAINST('{$query}' IN BOOLEAN MODE), 2) as score FROM restaurant WHERE city = '{$city}' AND (MATCH(title, cuisine) AGAINST('{$query}' IN BOOLEAN MODE) OR MATCH(soundex_index) AGAINST('{$soundex}' IN BOOLEAN MODE)) AND is_displayed = 1 AND (status = 'active') ORDER BY score DESC LIMIT 5 OFFSET 0";
    } else if (isset($request['city']) && $request['city'] != '') {
        //error_log(__FILE__." ".__FUNCTION__." ".__LINE__);
    	$city = $request['city'];
        $sql = "SELECT id, title, restaurant, city, cuisine FROM restaurant WHERE city = '{$city}' AND is_displayed = 1 AND (status = 'active') LIMIT 200 OFFSET 0";
    } else
        $sql = "SELECT id, title, restaurant, city, cuisine FROM restaurant WHERE is_displayed = 1 AND (status = 'active') LIMIT 200 OFFSET 0";
    try {
        //error_log(__FILE__." ".__FUNCTION__." ".__LINE__." sql: ".$sql);
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $restaurants = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        $errors = null;
        $data = array("restaurants" => $restaurants);
        echo format_api(1, $data, count($restaurants), $errors);
    } catch (PDOException $e) {
        error_log(__FILE__." ".__FUNCTION__." ".__LINE__." sql: ".$sql." PDOException: ".$e);
        WY_debug::recordDebug("ERROR", "SEARCH-CONTENT", print_r($request, true));
        api_error($e, "search-fulltext");
    }
});

function find($app) {
    $request = $app->request()->get();
    $platform = "";
    //error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": request: ".print_r($request, true));
    $res = new WY_restaurant();
    $errors = null;
	if(preg_match('/dev\.weeloy/' , $_SERVER['HTTP_HOST'])) {
		$headers = apache_request_headers();
		if (array_key_exists("platform", $headers) && preg_match('/ios|android/', $headers["platform"]))
        	WY_debug::recordDebug("DEBUG", "SEARCH-HEADER", print_r($headers, true));		
		}
		
    try {
        $data = $res->find($request, $platform);
        $restaurant = $data['restaurant'];
        if ($res->result < 0) {
            echo '{"error":{"text":food is empty}}';
            return;
        }

        //$data = array("restaurant" => $restaurant);
        echo format_api(1, $data, count($restaurant), $errors);
    } catch (PDOException $e) {
        api_error($e, "find");
    }
}


?>