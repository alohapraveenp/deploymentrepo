<?

/////PASSBOOK
$app->get('/:confirmation', function ($confirmation) {
    
	$path = '';
 
	require_once 'lib/class.passbook.inc.php';
	$passbook = new WY_Passbook();
	$booking = new WY_Booking();
	$booking->getBooking($confirmation);	
	$passbook->serialNumber = $booking->confirmation;
	
	$passbook->restaurant_title = $booking->restaurantinfo->title;
	
	$passbook->booking_date = 'test';
	$passbook->booking_time = 'test';
	$passbook->cover = $booking->cover;
	$passbook->address = $booking->restaurantinfo->address;
	$passbook->city = $booking->restaurantinfo->city;
	$passbook->zip = $booking->restaurantinfo->zip;
	$passbook->member_code = $booking->membCode;
	$passbook->qrcode_string = '{"membCode":"' . $booking->membCode . '"}';
	if (!empty($booking->restaurantinfo->GPS)) {
		$location = explode(',', $booking->restaurantinfo->GPS);
		$passbook->longitude = $location[0];
		$passbook->latitude = $location[1];

		//  var_dump($passbook->latitude);
		//  var_dump($passbook->longitude);
	}
	$passbook->map = $booking->restaurantinfo->map;
	//var_dump($passbook->map);        

	$passbook->generatePassbook();
	$path =  $passbook->createPassbook(false);
	if($path == 0){
		echo format_api(1, NULL, 0, NULL); 
	}
	echo format_api(1, $path, 1, NULL); 
});
  
$app->get('/:confirmation/delete', function ($confirmation) {
            echo format_api(1, true, 1, NULL); 
});

?>