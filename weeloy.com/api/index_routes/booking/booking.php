<?

$app->post('/email/', function () use ($app) {
    $data = json_decode($app->request()->getBody(), true);
    getBookingEmail($data['restaurant'], $data['email'], $data['token']);
});

$app->post('/mobile/', function () use ($app) {
    $data = json_decode($app->request()->getBody(), true);
    getBookingPhone($data['restaurant'], $data['mobile'], $data['token']);
});

$app->get('/deposit/details/:confirmation', function ($confirmation) use ($app) {
    getBookingDepositDetails($confirmation);
});

$app->post('/cancelbackoffice/', function () use ($app) {
    $data = json_decode($app->request()->getBody(), true);
    canceltheBooking($data['restaurant'], $data['booking'], $data['email'], $data['token']);
});

$app->post('/noshowbackoffice/', function () use ($app) {
    $data = json_decode($app->request()->getBody(), true);
    noshowtheBooking($data['restaurant'], $data['booking'], $data['email'], $data['token']);
});
$app->post('/noshow/payment/', function () use ($app) {
    $data = json_decode($app->request()->getBody(), true);
    noshowpaymentBooking($data);
});
$app->post('/reminder/menulink/', function () use ($app) {
    $data = json_decode($app->request()->getBody(), true);
      sendmenupdflink($data);
});

$app->post('/setmenuformstatus/', function () use ($app) {
    $data = json_decode($app->request()->getBody(), true);
      setformstatus($data);
});

$app->post('/payment/resendemail/', function () use ($app) {
    $data = json_decode($app->request()->getBody(), true);
     pendingPaymentemail($data);
});
$app->post('/payment/updatestatus/', function () use ($app) {
    $data = json_decode($app->request()->getBody(), true);
     updateStatus($data);
});

$app->post('/modifbackoffice/', function () use ($app) {
    $data = json_decode($app->request()->getBody(), true);
    if(!isset($data['comment'])) $data['comment'] = "-";
    modiftheBooking($data['restaurant'], $data['booking'], $data['email'], $data['cover'], $data['time'], $data['date'], $data['comment'], $data['token']);
});

$app->post('/modifullbooking/', function () use ($app) {
    $data = json_decode($app->request()->getBody(), true);
    if(!isset($data['dnotif'])) $data['dnotif'] = false;
    modifullBooking($data['restaurant'], $data['booking'], $data['email'], $data['obj'], $data['token'], $data['dnotif']);
});

$app->post('/createwalking/', function () use ($app) {
    $data = json_decode($app->request()->getBody(), true);
    createwalking($data['restaurant'], $data['obj'], $data['token']);
});


$app->post('/pendingpayment/cancel/', function () use ($app) {
    $data = json_decode($app->request()->getBody(), true);
    cancelpendingpaymentbooking($data);
});

$app->post('/state/', function () use ($app) {
    return setBookingState($app);
});

$app->post('/setReviewStatus', 'api_authentificate', function () use ($app) {
    $params = $app->request()->post();
    
    $confirmation_id = $params['confirmation_id'];
    $status = $params['status'];

    if ($status != 'viewed') {
        $error["error"] = true;
        $error["message"] = "bad parameters";
        echo format_api(1, NULL, 1, $error);
        return;
    }

    $bookings = new WY_Booking();
    $update_booking_status = $bookings->setReviewStatus($_SERVER['PHP_AUTH_USER'], $confirmation_id, $status);
    if ($update_booking_status) {
        echo format_api(1, $update_booking_status, 1, NULL);
    } else {
        $error["error"] = true;
        $error["message"] = "bad parameters";
        echo format_api(1, NULL, 1, $error);
    }
});


// WALKABLE
$app->post('/walkable/create', function () use ($app) {
    return addWalkableBooking($app);
});

$app->post('/update', function () use ($app) {
   return updateBooking($app);
});
$app->post('/update/payment_booking', function () use ($app) {
   return updatePaymentBooking($app);
});



function getBookingEmail($restaurant, $email, $token) {

    $restaurant = clean_input($restaurant);

    $res = new WY_Booking();
    if(preg_match("/^SG_SG_R_MadisonRooms/", $restaurant)){
        $data = $res->getMadisonMemberEmail('SG_SG_R_MadisonRooms', $email);  
    } else {
        $data = $res->getBookingEmail($restaurant, $email);
    }
        

    if (count($data) < 1) {
        echo format_api(0, "FAIL", 0, "Confirmation does not exist or wrong restaurant or email");
        return;
    }

    $errors = null;
    echo format_api(1, $data, count($data), $errors);
}

function getBookingPhone($restaurant, $phone, $token) {

    $res = new WY_Booking();

    if(preg_match("/^SG_SG_R_MadisonRooms/", $restaurant)){
        $data = $res->getMadisonBookingPhone('SG_SG_R_MadisonRooms', $phone);
    } else {
        $data = $res->getBookingPhone($restaurant, $phone);
    }
    
    
    if (count($data) < 1) {
        echo format_api(0, "FAIL", 0, "Confirmation does not exist or wrong restaurant or phone number");
        return;
    }

    $errors = null;
    echo format_api(1, $data, count($data), $errors);
}

function getBookingDepositDetails($confirmation) {

    $booking = new WY_Booking();
    $data = $booking->getBookingDepositDetails($confirmation);
    if (count($data) < 1) {
        echo format_api(0, "FAIL", 0, "Confirmation does not exist or wrong restaurant or phone number");
        return;
    }

    $errors = null;
    echo format_api(1, $data, 1, $errors);
}

function canceltheBooking($restaurant, $booking, $email, $token) {

    $login = new WY_Login(set_valid_login_type('backoffice'));
    if ($login->checktoken($token)) {
    	$type = (!empty($login->extrafield)) ? $login->extrafield : "backoffice";
        return cancelVisit($restaurant, $booking, $email, "", $type, $login->email);
        }
    else echo format_api(-1, '', 0, 'wrong token');
}

function noshowtheBooking($restaurant, $booking, $email, $token){
      
       $login = new WY_Login(set_valid_login_type('backoffice'));
        if ($login->checktoken($token)){
            
            $restaurant = clean_text($restaurant);
                $res = new WY_Booking();

                if ($res->getBooking($booking) == false) {
                    echo format_api(0, "FAIL", 0, "Confirmation does not exist or wrong restaurant or email");
                    return;
                }

                if ($res->restaurant != $restaurant || $res->email != $email) {
                    echo format_api(0, "FAIL", 0, "Confirmation does exist for a different restaurant or email ");
                    return;
                }

                if ($res->status == 'cancel') {
                    echo format_api(0, "FAIL", 0, "Confirmation has already been canceled ");
                    return;
                }


                if ($res->noshowBooking($booking) < 0) {
                    echo format_api(0, "FAIL", 0, $res->msg);
                    return;
                }

                echo format_api(1, "OK", 0, "booking status =>noshow");

        }else{
                echo format_api(-1, '', 0, 'wrong token');
             }
  }
   
function noshowpaymentBooking($data){

       $login = new WY_Login(set_valid_login_type('backoffice'));
        if ($login->checktoken($data['token'])){
            
             $restaurant = clean_text($data['restaurant']);
                $res = new WY_Booking();
                  $stripe = new WY_Payment_Stripe();
                
                noshowValidation($res,$data['booking'],$restaurant,$data['email']);

                $resData = $stripe->createStripePayment($data['deposit'],$restaurant, $data['booking'], $data['email'],$data['amount']);
                $status =$resData['status'];
                
                if($status ='succeeded'){
                    if ($res->noshowBooking($data['booking']) < 0) {
                        echo format_api(0, "FAIL", 0, $res->msg);
                        return;
                    }
                  echo format_api(1, "OK", 0, "booking status =>noshow");
                    
                }else{
                     echo format_api(1, $resData, 0, "booking Payment Creation=>noshow");
                }
 

        }else{
                echo format_api(-1, '', 0, 'wrong token');
             }
  }

function noshowValidation($booking,$confirmation,$restaurant,$email){
	if ($booking->getBooking($confirmation) == false) {
			echo format_api(0, "FAIL", 0, "Confirmation does not exist or wrong restaurant or email");
			return;
	}

	if ($booking->restaurant != $restaurant || $booking->email != $email) {
		echo format_api(0, "FAIL", 0, "Confirmation does exist for a different restaurant or email ");
		return;
	}

	if ($booking->status == 'cancel') {
		echo format_api(0, "FAIL", 0, "Confirmation has already been canceled ");
		return;
	}
}
  
function sendmenupdflink($data){
        $bkg = new WY_Booking();
        $logger = new WY_log("backoffice");
        $login = new WY_Login(set_valid_login_type('backoffice'));
        if ($login->checktoken($data['token'])){

            $restaurant = clean_text($data['restaurant']);
            $bkg = new WY_Booking();
            $notification = new WY_Notification();

        if ($bkg->getBooking($data['booking']) == false) {
            echo format_api(0, "FAIL", 0, "Confirmation does not exist or wrong restaurant or email");
            return;
        }

        if ($bkg->status == 'cancel') {
            echo format_api(0, "FAIL", 0, "Confirmation has already been canceled ");
            return;
        }
        $notification->notify($bkg,'reminder_pdflink');
        
        $loguserid = (isset($_SESSION['user_backoffice']['id'])) ? $_SESSION['user_backoffice']['id'] : "unknown";
        
        $logger->LogEvent($loguserid, 716, $data['booking'], 'backoffice', '', date("Y-m-d H:i:s"));
        
        echo format_api(1, "OK", 0, "Menu Pdf Link has been sent");

        }else{
                echo format_api(-1, '', 0, 'wrong token');
             }
    }
    
function setformstatus($data){
        $bkg = new WY_Booking();
        
        $logger = new WY_log("backoffice");
        $login = new WY_Login(set_valid_login_type('backoffice'));
        if ($login->checktoken($data['token'])){

            $restaurant = clean_text($data['restaurant']);
            $bkg = new WY_Booking();
            

        if ($bkg->getBooking($data['booking']) == false) {
            echo format_api(0, "FAIL", 0, "Confirmation does not exist or wrong restaurant or email");
            return;
        }

        if ($bkg->status == 'cancel') {
            echo format_api(0, "FAIL", 0, "Confirmation has already been canceled ");
            return;
        }
        $result = $bkg->addtoGeneric('{"more":"{’chck_form_status’: ’' . $data['menuform'] . '’}"}');
        
        $loguserid = (isset($_SESSION['user_backoffice']['id'])) ? $_SESSION['user_backoffice']['id'] : "unknown";
        
        $logger->LogEvent($loguserid, 715, $data['booking'], 'backoffice', '', date("Y-m-d H:i:s"));
        
        echo format_api(1, "OK", 0, "menu form submitted");

        }else{
                echo format_api(-1, '', 0, 'wrong token');
             }
    }
    
function pendingPaymentemail($data){
        $bkg = new WY_Booking();
        $res = new WY_restaurant();
        $login = new WY_Login(set_valid_login_type('backoffice'));
        if ($login->checktoken($data['token'])){

            $restaurant = clean_text($data['restaurant']);
            $bkg = new WY_Booking();
            $notification = new WY_Notification();

        if ($bkg->getBooking($data['booking']) == false) {
            echo format_api(0, "FAIL", 0, "Confirmation does not exist or wrong restaurant or email");
            return;
        }

        if ($bkg->status == 'cancel') {
            echo format_api(0, "FAIL", 0, "Confirmation has already been cancelled ");
            return;
        }
         //pollen allow to re-send the email without checking on the availability.
        if($restaurant == 'SG_SG_R_Pollen'){
             $res->result = 1;
        } else{
             $ret = $res->CheckAvailability($restaurant, $bkg->rdate, $bkg->rtime, $bkg->cover, $bkg->confirmation, $bkg->product);
        }
        if($res->result > 0) {
            if(isset($data['recipient'])){
               $bkg->email = $data['recipient'];
            }

            $bkg->resendFlg = true; 
            
     
            $logger = new WY_log("backoffice");
            $loguserid = (isset($_SESSION['user_backoffice']['id'])) ? $_SESSION['user_backoffice']['id'] : "unknown";

            $logger->LogEvent($loguserid, 709, $bkg->confirmation,'backoffice', '', date("Y-m-d H:i:s"));
            if($bkg->status == 'expired' || $bkg->status == '' ){
                    $bkg->updateBookingStatus('', $bkg->confirmation, 'pending_payment');
            }
            $bkg->status = 'pending_payment';
            $notification->notify($bkg,'booking_pending');
            echo format_api(1, "OK", 0, "email  has been sent");
         
        }else{
            echo  format_api(-1, "0", $ret, "No more availability. Restaurant =  " . $restaurant . ", date = " . $bkg->rdate . ", time = " . $bkg->rtime . ", covers = " . $bkg->cover);
        }

        }else{
           echo format_api(-1, '', 0, 'wrong token');
           }
    }
   
function updateStatus($data){
        $bkg = new WY_Booking();
        $res = new WY_restaurant();
        $login = new WY_Login(set_valid_login_type('backoffice'));
        $isnotifyflg = 11;
        if ($login->checktoken($data['token'])){

            $restaurant = clean_text($data['restaurant']);
            $bkg = new WY_Booking();
            $notification = new WY_Notification();

        if ($bkg->getBooking($data['booking']) == false) {
            echo format_api(0, "FAIL", 0, "Confirmation does not exist or wrong restaurant or email");
            return;
        }

        if ($bkg->status == 'cancel') {
            echo format_api(0, "FAIL", 0, "Confirmation has already been canceled ");
            return;
        }
      
          $ret = $res->CheckAvailability($restaurant, $bkg->rdate, $bkg->rtime, $bkg->cover, $bkg->confirmation, $bkg->product);
          if($restaurant == 'SG_SG_R_Pollen' || $restaurant == 'SG_SG_R_Esquina'){
            $isnotifyflg = 12;
            $res->result = 1;
          }
            if($res->result > 0) {
                $bkg->updateBookingStatus('', $data['booking'], '');
                $bkg->notifyflg = 'waived';
                $bkg->addtoGeneric('{"more":"{’ccwaived’:’1’}"}');
                if($isnotifyflg == 11){
                    $notification->notify($bkg,'booking');
                }
         
                $logger = new WY_log("backoffice");
                $loguserid = (isset($_SESSION['user_backoffice']['id'])) ? $_SESSION['user_backoffice']['id'] : "unknown";
                $logger->LogEvent($loguserid, 718, $bkg->confirmation,'backoffice', '', date("Y-m-d H:i:s"));
             echo format_api(1, "OK", 0, "email  has been sent");
            }else{
               echo  format_api(-1, "0", $ret, "No more availability. Restaurant =  " . $restaurant . ", date = " . $bkg->rdate . ", time = " . $bkg->rtime . ", covers = " . $bkg->cover);  
            }
        }else{
                echo format_api(-1, '', 0, 'wrong token');
             }
       
   }
    
function modiftheBooking($restaurant, $booking, $email, $cover, $time, $date, $comment, $token) {

    $login = new WY_Login(set_valid_login_type('backoffice'));
    if ($login->checktoken($token) == false) {
        echo format_api(-1, '', 0, 'wrong token');
        return;
    }

    $res = new WY_Booking();
    if ($res->modifBooking($booking, $email, $cover, $time, $date, $comment) < 0) {
        echo format_api(0, "FAIL", 0, $res->msg);
        return;
    }

    echo format_api(1, "OK", 0, "booking has been modified");
}


function createwalking($restaurant, $obj, $token) {

    $login = new WY_Login(set_valid_login_type('backoffice'));
    if ($login->checktoken($token) == false) {
        echo format_api(-1, '', 0, 'wrong token');
        return;
    }

    $bkg = new WY_Booking();
    $data = $bkg->createwalking($restaurant, $obj);
    if($bkg->result < 0) {
        echo format_api(0, "FAIL", 0, $bkg->msg);
        return;
    }
    echo format_api(1, array("booking" => $data), 0, "walking has been created");
}

  
function modifullBooking($restaurant, $booking, $email, $obj, $token, $dnotif) {

    $login = new WY_Login(set_valid_login_type('backoffice'));
    if ($login->checktoken($token) == false) {
        echo format_api(-1, '', 0, 'wrong token');
        return;
    }

    $res = new WY_Booking();
    $notif = ($login->platform == "backoffice" && $dnotif == false);

    if ($res->modifullBooking($booking, $obj, $notif, $login->email) < 0) {
        echo format_api(0, "FAIL", 0, $res->msg);
        return;
    }
    echo format_api(1, "OK", 0, "booking has been modified");
}

  
function cancelpendingpaymentbooking($data){
    $res = new WY_Booking();
   
    $confirmation =$data['confirmation'];
    $restaurant =$data['restaurant'];
    
    
        if ($res->getBooking($confirmation) == false) {
            echo format_api(0, "FAIL", 0, "Confirmation does not exist or wrong restaurant or email");
            return;
        }

        if ($res->restaurant != $restaurant ) {
            echo format_api(0, "FAIL", 0, "Confirmation does exist for a different restaurant");
            return;
        }

        if ($res->status == 'cancel') {
            echo format_api(0, "FAIL", 0, "Confirmation has already been canceled ");
            return;
        }
        if ($res->cancelPayPendingBooking($restaurant,$confirmation) < 0) {
                echo format_api(0, "FAIL", 0, $res->msg);
                return;
        }else{
           echo format_api(1, "OK", 0, "Confirmation has been canceled");
        }
    
}

function setBookingState($app) {

    $restaurant = $app->request->post("restaurant");
    $confirmation = $app->request->post("confirmation");
    $state = $app->request->post("state");
    $token = $app->request->post("token");

    $login = new WY_Login(set_valid_login_type('backoffice'));
    if ($login->checktoken($token)) {
        $mobj = new WY_Booking();
        $mobj->setBooking($restaurant, $confirmation, $state, 'state', null);

        if ($mobj->result > 0)
            echo format_api(1, '', 0, $mobj->msg);
        else
            echo format_api(-1, '', 0, $mobj->msg);
    } else
        echo format_api(-1, '', 0, 'wrong token');
}

function addWalkableBooking($app) {

    //define var

    $request = $app->request()->post();
    $restaurant = $request['restaurant'];

    $firstname = $request['firstname'];
    $lastname = $request['lastname'];
    $email = $request['email'];
    $mobile = $request['mobile'];
    $rdate = $request['date'];
    $rtime = $request['time'];

	$salutation = "";
	$cover = 1;
	$country = "";
	$language = "";
	$specialrequest = "";
	$type = "walkin";
	$tracking = "walkin";
	$booker = "";
	$company = "";
	$hotelguest = 0;
	$state = "to come";
	$optin = "";
	$booking_deposit_id = "";
	$status = "";
	$extra = "";
	$product = "";

    try {
        if (is_restaurant_valid($restaurant) && is_email_valid($email) && is_mobile_valid($mobile)) {
            
            $booking = new WY_Booking();

            //$rdate = date('Y-m-d');
            //$rtime = date("h:i:s");
            $booking->createBooking(
                    $restaurant, $rdate, $rtime, $email, $mobile, $cover, $salutation, $firstname, $lastname, $country, $language, $specialrequest, $type, $tracking, $booker, $company, $hotelguest, $state, $optin, $booking_deposit_id, $status, $extra, $product);


            //$booking->notifyBookingWalkable();
            if ($booking->result < 0) {
                echo format_api(1, NULL, 0, 'An error occured, please try again later');
                return;
            }

            return requestspin($booking->confirmation, $booking->restCode, $booking->membCode);

            //echo format_api(1, $booking, count($booking), $errors);
        } else {
            $errors["error"] = true;
            $errors["message"] = "wrong information";
            echo format_api(1, NULL, 0, $errors);
        }
    } catch (PDOException $e) {
        api_error($e, "addWalkableBooking");
    }
}


function updateBooking($app){
    $payment = new WY_Payment();
    $respolicy = new WY_Restaurant_policy;
    $content_type = $app->request->getContentType();
    $isPendingStatus = 11;
    
    if(!empty($content_type) && strpos($content_type,'application/json') !== false){
        $request = json_decode($app->request()->getBody(), true);
    }else{
        $request = $app->request->post();
    }
  
    $confirmation = $request["confirmation"];
    $cover = $request["npers"];
    $rdate = $request["selecteddate"];
    $rtime = $request["ntimeslot"];
    $specialrequest = $request["specialrequest"];
    $data = $request;

    $booking = new WY_Booking();
    $booking->UpdateBooking($confirmation,$cover, $rtime, $rdate,$specialrequest,$data);
    if(WY_restaurant::s_checkbkdeposit($booking->restaurant) > 0){
        $updateDetails = $payment->updatePaymentId('',$confirmation);
          
    }
    if ($booking->result > 0){
        $notification = new WY_Notification();
        if(WY_restaurant::s_checkmodifypayment($booking->restaurant) > 0 && $booking->restaurant == 'SG_SG_R_Pollen'){
            $requiredccDetails = $respolicy->isRequiredccDetails($booking->restaurant,$rtime,$cover,$rdate);
            if($requiredccDetails){
                $booking->status = "pending_payment";
                 $booking->updateBookingStatus('', $confirmation, 'pending_payment');
                 $isPendingStatus = 12;
            
            };
        } 
        $booking->getBooking($confirmation);
        if($isPendingStatus == 12){
          $notification->notify($booking,'booking_pending');
        }else{
            $notification->notify($booking, 'update');
        }
      
 
         echo format_api(1, 1, 0, 'OK'); 
     }else{
         echo format_api(-1, -1, 0, $booking->msg);  
     }
    
    
    
//    $bkg = new WY_Booking();
//    $bkg->UpdateBooking($confirmation,$cover, $rtime, $rdate,$specialrequest,$data);
//    
//     if ($bkg->result > 0) echo format_api(1, 1, 0, 'OK'); 
     
}


function updatePaymentBooking($app){
    $data = $app->request->post();
    $missing_params  = array();
    $booking = new WY_Booking();
    $respolicy = new WY_Restaurant_policy;
    $stripe = new WY_Payment_Stripe();
    $payment = new WY_Payment();
 
    $confirmation =(isset($data['confirmation'])) ? $data['confirmation'] :'' ;
    $token = (isset($data['token'])) ? $data['token'] :'';
  
    $current_charge = (isset($data['current_charge'])) ? $data['current_charge'] :'0';

    if(empty($confirmation)){
       $missing_params[] = 'confirmation';
    }
    if(empty($token)){
   
        $missing_params[] = 'token';
    }

//    if($current_charge === "0" || $current_charge === 0 || $current_charge = null){
//        $missing_params[] = 'amount';
//    }
    if(count($missing_params)>0){
        $missing_params =json_encode($missing_params);
        echo format_api(0, $missing_params, 0, 'missing params');  
        exit;
    }

    $requiredccDetails = $respolicy->isRequiredccDetails($data['restaurant'],$data['ntimeslot'],$data['npers'],$data['selecteddate']);
  
    if($requiredccDetails){
        $result = $stripe->updateBookingPaymentDetails($data['restaurant'], $confirmation, $data['email'], $current_charge, $token) ;
    }
    if(isset($result) && $result['status'] == 'COMPLETED'){
         $booking->UpdateBooking($confirmation,$data['npers'], $data['ntimeslot'], $data['selecteddate'],$data['specialrequest'],$data);
        if ($booking->result > 0){
           $booking->getBooking($confirmation);
           $notification = new WY_Notification();
           $notification->notify($booking, 'update');
            echo format_api(1, 1, 0, 'OK'); 
        }else{
            echo format_api(-1, -1, 0, $booking->msg);  
        }
    }else{
        echo format_api(-1, -1, 0, 'ccDetails updation failed'); 
    }
  
}


?>