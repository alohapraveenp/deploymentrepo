<?

$app->post('/state/', function () use ($app) {
    $data = json_decode($app->request()->getBody(), true);
    setBookingService($data['restaurant'], $data['booking'], $data['state'], $data['token']);
});

$app->post('/table/', function () use ($app) {
    $data = json_decode($app->request()->getBody(), true);
    if(!isset($data["captain"])) $data["captain"] = null;
    setBookingTable($data['restaurant'], $data['booking'], $data['table'], $data['captain'], $data['token']);
});

$app->post('/field/', function () use ($app) {
    $data = json_decode($app->request()->getBody(), true);
    setBookingField($data['restaurant'], $data['booking'], $data['field'], $data['value'], $data['token']);
});

function setBookingField($restaurant, $confirmation, $field, $value, $token) {

    $login = new WY_Login(set_valid_login_type('backoffice'));
    if ($login->checktoken($token)) {
        $mobj = new WY_Booking();
        $mobj->setBooking($restaurant, $confirmation, $value, $field, null);

        if ($mobj->result > 0)
            echo format_api(1, '', 0, $mobj->msg);
        else
            echo format_api(-1, '', 0, $mobj->msg);
    } else
        echo format_api(-1, '', 0, 'wrong token');
}

function setBookingService($restaurant, $confirmation, $state, $token) {

    $login = new WY_Login(set_valid_login_type('backoffice'));
    if ($login->checktoken($token)) {
        $mobj = new WY_Booking();
        $mobj->setBooking($restaurant, $confirmation, $state, 'state', null);

        if ($mobj->result > 0)
            echo format_api(1, '', 0, $mobj->msg);
        else
            echo format_api(-1, '', 0, $mobj->msg);
    } else
        echo format_api(-1, '', 0, 'wrong token');
}

function setBookingTable($restaurant, $booking, $table, $captain, $token) {

    $login = new WY_Login(set_valid_login_type('backoffice'));
    if ($login->checktoken($token)) {
        $mobj = new WY_Booking();
        $captainflg = ($captain != null && count($booking) == count($captain));
        for ($i = 0; $i < count($booking); $i++) {
            $cap = $captainflg ? $captain[$i] : null;
            $mobj->setBooking($restaurant, $booking[$i], $table[$i], 'tablename', $cap);
        }
        if ($mobj->result > 0)
            echo format_api(1, '', 0, $mobj->msg);
        else
            echo format_api(-1, '', 0, $mobj->msg);
    } else
        echo format_api(-1, '', 0, 'wrong token');
}


?>