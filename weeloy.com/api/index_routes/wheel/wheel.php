<?

function getCurrentWheelVersion($restaurant, $booking_id = NULL) {
    $wheel_type = '';
    $wheel_image = 'wheel.png';
    if(!empty($booking_id)){
        
        $booking = new WY_Booking();
        $booking->getBooking($booking_id);
        if(preg_match("/cpp_credit_suisse/i", $booking->tracking) == true && false){ 
            $wheel_type = 'corporate';
            $wheel_image = 'corporate_wheel.png';
        }
    }
    $wheel = new WY_wheel($restaurant, '', $wheel_type);
    $data = array('wheelversion' => $wheel->getWheelVersion(), 'wheel_image' => $wheel_image);

    echo format_api(1, $data, 1, NULL);
}

function requestSpin($confirmation, $rcode, $mcode) {

    $bkg = new WY_Booking();
    $ret = $bkg->verifyBookingWin($confirmation, $rcode, $mcode);

    if ($ret == "ok") {
        $token = base64_encode($confirmation . "|" . $rcode . "|" . $mcode);
        echo format_api(1, $token, 1, "ok");
    } else
        echo format_api(0, NULL, 0, $ret);
}

function storeSpin($spin, $token) {
    $spin = urldecode($spin);

    $tokenAr = explode("|", base64_decode($token));
    $confirmation = $tokenAr[0];
    $rcode = $tokenAr[1];
    $mcode = $tokenAr[2];

    $bkg = new WY_Booking();
    $ret = $bkg->submitBookingWin($confirmation, $rcode, $mcode, $spin);
    error_log("API res " . $confirmation . '  ' . $ret);
    echo ($ret == "ok") ? format_api(1, NULL, 1, "ok") : format_api(0, NULL, 0, $ret);
}

function getSpinResult($confirmation) {

    $bkg = new WY_Booking();
    $ret = $bkg->getSpinResult($confirmation);
    $data = array('wheelsegment' => $bkg->wheelsegment, 'wheelwin' => $bkg->wheelwin, 'wheeldesc' => $bkg->wheeldesc, 'wheelwinangle' => $bkg->wheelwinangle, 'spinsource' => $bkg->spinsource);
    echo ($ret == "ok") ? format_api(1, $data, 1, NULL) : format_api(0, NULL, 0, $ret);
}

function storeSpinComplete($segment, $spin, $desc, $source, $token) {

    $spin = urldecode($spin);

    $tokenAr = explode("|", base64_decode($token));
    $confirmation = $tokenAr[0];
    $rcode = $tokenAr[1];
    $mcode = $tokenAr[2];

    if (!isset($desc) || $desc == '') {
        $desc = 'empty';
    }

    $bkg = new WY_Booking();
    $ret = $bkg->submitBookingCompleteWin($confirmation, $rcode, $mcode, $segment, $spin, $desc, $source);
    error_log("API res " . $confirmation . '  ' . $ret);
    echo ($ret == "ok") ? format_api(1, NULL, 1, "ok") : format_api(0, NULL, 0, $ret);
}

function storeSpinComplete_HOTFIX($segment, $spin, $source, $token) {

    $spin = urldecode($spin);

    $tokenAr = explode("|", base64_decode($token));
    $confirmation = $tokenAr[0];
    $rcode = $tokenAr[1];
    $mcode = $tokenAr[2];

    if (!isset($desc) || $desc == '') {
        $desc = 'empty';
    }

    $bkg = new WY_Booking();
    $ret = $bkg->submitBookingCompleteWin($confirmation, $rcode, $mcode, $segment, $spin, $desc, $source);
    error_log("API res " . $confirmation . '  ' . $ret);
    echo ($ret == "ok") ? format_api(1, NULL, 1, "ok") : format_api(0, NULL, 0, $ret);
}

function storeSpinCompleteAngle($wheelversion, $segment, $angle, $spin, $desc, $source, $token) {

    $spin = urldecode($spin);
    if (strpos($desc, '%20') !== false) {
        $desc = urldecode($desc);
    }

    $tokenAr = explode("|", base64_decode($token));
    $confirmation = $tokenAr[0];
    $rcode = $tokenAr[1];
    $mcode = $tokenAr[2];

    
    $bkg = new WY_Booking();
    $ret = $bkg->submitBookingCompleteWin($confirmation, $rcode, $mcode, $segment, $spin, $desc, $source, $wheelversion, $angle);
    error_log("API res " . $confirmation . '  ' . $ret);
    echo ($ret == "ok") ? format_api(1, 1, 1, "ok") : format_api(0, NULL, 0, $ret);
}

function storeSpinCompleteAngle2($wheelversion, $segment, $angle, $spin, $desc, $source, $token) {

    //$spin = urldecode($spin);
    //if (strpos($desc,'%20') !== false) {$desc = urldecode($desc);}   

    $tokenAr = explode("|", base64_decode($token));
    $confirmation = $tokenAr[0];
    $rcode = $tokenAr[1];
    $mcode = $tokenAr[2];

    $bkg = new WY_Booking();
    $ret = $bkg->submitBookingCompleteWin($confirmation, $rcode, $mcode, $segment, $spin, $desc, $source, $wheelversion, $angle);
    error_log("API res " . $confirmation . '  ' . $ret);
    echo ($ret == "ok") ? format_api(1, 1, 1, "ok") : format_api(0, NULL, 0, $ret);
}

function getWheel($restaurant) {


    $restaurant = clean_text($restaurant);
    $wheel = new WY_wheel($restaurant, 'api', '');
    try {
	    $data = $wheel->getWheel();
    	if($wheel->result > 0)
			echo format_api(1, $data, count($data["wheel"]), "");
	   	else echo format_api(-1, "", 0, $wheel->msg);

    } catch (PDOException $e) {
        api_error($e, "getWheel");
    }
}

function getWheelDescription($restaurant, $option = NULL, $version = NULL) {
    
    $type = '';
    if(!empty($option) and $option != 'unique_only'){
        $sql = "SELECT type FROM wheel WHERE wheelversion = '$option'";
        $res = pdo_single_select($sql);
        if(isset($res['type']))
        	$type = $res['type'];
    }
    
    
    $restaurant = clean_text($restaurant);
    $wheel = new WY_wheel($restaurant, 'api', $type);
    $wheel->readWheel();
    if($option == 'unique_only'){ $wheel->wheelpart = array_unique($wheel->wheelpart); }
    $data = array("wheel" => $wheel->wheelpart,"wheelvalue" => $wheel->wheelvalue, "description" => $wheel->wheeldesc, "wheelversion" => $wheel->wheelversion);
    echo format_api(1, $data, count( $wheel->wheel), NULL);
}

function getCPPWheelDescription($restaurant, $option = NULL) {

    $restaurant = clean_text($restaurant);
    $wheel = new WY_wheel($restaurant, 'api', 'corporate');
    $wheel->readWheel();
    if($option == 'unique_only'){ $wheel->wheelpart = array_unique($wheel->wheelpart); }
    $data = array("wheel" => $wheel->wheelpart,"wheelvalue" => $wheel->wheelvalue, "description" => $wheel->wheeldesc, "wheelversion" => $wheel->wheelversion);
    echo format_api(1, $data, count( $wheel->wheel), NULL);
}

?>