<?

function renameMedia($app) {

    $restaurant = $app->request->post("restaurant");
    $name = $app->request->post("name");
    $newname = $app->request->post("newname");
    $category = $app->request->post("category");
    $token = $app->request->post("token");

    $login = new WY_Login(set_valid_login_type('backoffice'));
    if ($login->checktoken($token)) {
        $media = new WY_Media($restaurant);
        $log = $media->renameMedia($restaurant, $name, $newname, 'picture', $category);
        echo format_api($log, '', 0, $media->msg);
    } else
        echo format_api(-1, '', 0, 'wrong token');
}

function deleteMedia($app) {

    $restaurant = $app->request->post("restaurant");
    $name = $app->request->post("name");
    $category = $app->request->post("category");
    $token = $app->request->post("token");

    $login = new WY_Login(set_valid_login_type('backoffice'));
    if ($login->checktoken($token)) {
        $media = new WY_Media($restaurant);
        $log = $media->deleteMedia($restaurant, $name, $category);
        echo format_api($log, '', 0, $media->msg);
    } else
        echo format_api(-1, '', 0, 'wrong token');
}

function updateMedia($app) {

    $restaurant = $app->request->post("restaurant");
    $name = $app->request->post("name");
    $category = $app->request->post("category");
    $type = $app->request->post("type");
    $description = $app->request->post("description");
    $status = $app->request->post("status");
    $morder = $app->request->post("morder");
    $token = $app->request->post("token");

    $login = new WY_Login(set_valid_login_type('backoffice'));
    if ($login->checktoken($token)) {
        $media = new WY_Media($restaurant);
        $log = $media->updateMedia($restaurant, $name, $category, $type, $description, $status, $morder);
        echo format_api($log, '', 0, $media->msg);
    } else
        echo format_api(-1, '', 0, 'wrong token');
}


function getlogo() {

    $sql = "select restaurant, logo FROM restaurant ORDER BY restaurant";
    try {
        $db = getConnection();
        $stmt = $db->query($sql);
        $restaurant = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $db = null;
        foreach ($restaurant as $resto) {
            if (!empty($resto['logo'])) {
                $errors = null;
                $data = array("logo" => __MEDIA__SERVER_NAME__ . $resto['restaurant'] . "/" . $resto['logo']);
                echo format_api(1, $data, 1, $errors);
            }
        }
    } catch (PDOException $e) {
        api_error($e, "getlogo");
    }
}

function updatevideolink($app){

  $params = json_decode($app->request()->getBody(), true);
   $restaurant = $params["restaurant"];
   $link = $params['data']['link'];
   $method = $params["method"];
   $restaurant = trim($restaurant);
   $query = "SELECT path FROM  media  WHERE restaurant ='$restaurant' and media_type='video'  LIMIT 1 ";
    try {
            $db = getConnection();
            $stmt = $db->query($query);
            $mediaCount = $stmt->fetchAll(PDO::FETCH_OBJ);
            if($method =='create'){
                if(count($mediaCount)== 0){
                 $sql = "INSERT INTO media(name,object_type,restaurant,media_type,path,status) VALUES ('video','restaurant','$restaurant','video','$link','active')";
                    $errors = null;
                    error_log("SDSds".$sql);
                    $stmt = $db->prepare($sql);
                      error_log("IN 1");
                    $stmt->execute();    
                    error_log("IN 2");
                     echo format_api(1, $link, 1, $errors);

                }else{
                    $errors="A video for this restaurant has already been added.";
                     echo format_api(1, $link, 1, $errors);
                }
                
            }else if($method=='update'){
                if(count($mediaCount)>0){
                    $errors=null;
                    $sql= "UPDATE media set path ='$link' WHERE restaurant='$restaurant' and media_type='video'  LIMIT 1"; 
                    $stmt = $db->prepare($sql);
                     $stmt->execute(); 
                     echo format_api(1, $link, 1, $errors);

                }else{
                    $errors="Please add the video link!!.";
                     echo format_api(1, $link, 1, $errors);
                }
            }
            
  
    } catch(PDOException $e) {
         api_error($e, "app tracking addVideoLink");
    }
}
function getVideoList($app){
   $params = json_decode($app->request()->getBody(), true);
   $restaurant = $params["restaurant"];
   $media = new WY_Media($restaurant);
   $data = $media->getVideoRestaurantDishes($restaurant);
   echo format_api('1', $data, 0, $media->msg);
}

function uploadFoodSelfie($app) {

    $_POST["Stage"] = "Load";
    $category = 'foodselfie';
    
    $email = trim($app->request->post('email'));
    $display_name = trim($app->request->post('name'));
    //$confirmation_id = trim($app->request->post('confirmation'));
    

    ////// CHECKS
    
     //phil remove test -> OPEN FOR EASIER UPLOAD
    $db = getConnection();
        
    $confirmation_id =  '';
    $restaurant = '';
    $title = '';
    $bkusername = '';
    $wheelwin = '';
    
    $extra_field['confirmation'] = $confirmation_id;
    $extra_field['display_name'] = $display_name;
    $extra_field['restaurant'] = $restaurant;
    $extra_field['title'] = $title;
    $extra_field['username'] = $bkusername;
    $extra_field['wheelwin'] = $wheelwin;
    //$extra_field['review'] = $review->getReview($confirmation_id, '', 'myreview');

    $media = new WY_Media($email, NULL, 'foodselfie');

    //CHECK FOLDER EXIST
    $media->l_folder_exist();

    if (!isset($_FILES['file'])) {
        echo "No files uploaded!!v";
        return;
    }

    $files = $_FILES['file'];

    $result[] = -1;
    $result[] = 'error on upload';


    if ($files['error'] === 0) {
        $name = uniqid('img-' . date('Ymd') . '-');
        
        $result = $media->uploadImage($email, $category, 'selfie-contest-2015', $extra_field);
    }

    if ($result[0] < 0) {
        echo format_api(0, NULL, 0, "$result[1]");
        return;
    }
    $responsData =[];
    $sql = "SELECT * FROM _2015_photo_contest WHERE status !='decline' ORDER by id DESC LIMIT 0,48";
    try {
        $stmt = $db->query($sql);
        $responsData = $stmt->fetchAll(PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
        api_error($e, "uploadfoodselfie");
    }
    echo format_api(1, 1, 1, $responsData);
}

function getlogourl($restaurant) {

    $restaurant = clean_text($restaurant);
    $sql = "select restaurant, logo FROM restaurant WHERE restaurant = '$restaurant' ORDER BY restaurant";
    try {
        $db = getConnection();
        $stmt = $db->query($sql);
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $db = null;
        foreach ($data as $resto) {
            if (!empty($resto['logo'])) {
                $errors = null;
                $data = array("logo" => __MEDIA__SERVER_NAME__ . $resto['restaurant'] . "/" . $resto['logo']);
                echo format_api(1, $data, 1, $errors);
            }
        }
    } catch (PDOException $e) {
        api_error($e, "getlogourl");
    }
}

?>