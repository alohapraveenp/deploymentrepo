<?

$app->get('/list/:query', function ($query) {
    return getlist($query);
});
$app->post('/setdefault', function () use ($app) {
    return setdefault($app);
});
$app->post('/delete', function () use ($app) {
    return delete($app);
});
$app->post('/create', function () use ($app) {
    return create($app);
});
$app->post('/setuppromo', function () use ($app) {
    return uppromo($app);
});


function getlist($query) {
        $promo = new WY_Promotion($query);
        $promodata = $promo->getPromotionsList();
        $data = array("promo" => $promodata);
	echo format_api(1, $data, count($data), NULL, 200);
}


function setdefault($app) {
    $restaurant = $app->request->post("restaurant");
    $name = $app->request->post("name");
    $token = $app->request->post("token");

 	$login = new WY_Login(LOGIN_BACKOFFICE);
 	if($login->checktoken($token)) {
		$mobj = new WY_Promotion($restaurant);
 		list($val, $msg) = $mobj->setDefault($name);
		if($val > 0)
			echo format_api(1, 1, 0, '', 200);
		else echo format_api(-1, NULL, 0, $msg, 200);
 		}
 	else echo format_api(-1, NULL, 0, 'wrong token', 401);	
}

function uppromo($app) {
    $restaurant = $app->request->post("restaurant");
    $name = $app->request->post("name");
    $token = $app->request->post("token");
    $orderval = $app->request->post("orderid");
 	$login = new WY_Login(LOGIN_BACKOFFICE);
       
 	if($login->checktoken($token)) {
		$mobj = new WY_Promotion($restaurant);
     
            list($val, $msg) = $mobj->setupPromo($name,$orderval);
		if($val > 0)
            echo format_api(1, 0, '', 200);
		else echo format_api(-1, NULL, 0, $msg, 200);
 		}
 	else echo format_api(-1, NULL, 0, 'wrong token', 401);	
}

function create($app) {
    $restaurant = $app->request->post("restaurant");
    $name = $app->request->post("name");
    $token = $app->request->post("token");

 	$login = new WY_Login(LOGIN_BACKOFFICE);
 	if($login->checktoken($token)) {
		$mobj = new WY_Promotion($restaurant);
 		list($val, $msg) = $mobj->insert($name);
		if($val > 0)
			echo format_api(1, 1, 0, '', 200);
		else echo format_api(-1, NULL, 0, $msg, 200);
 		}
 	else echo format_api(-1, NULL, 0, 'wrong token', 401);	
}

function delete($app) {
    $restaurant = $app->request->post("restaurant");
    $name = $app->request->post("name");
    $token = $app->request->post("token");

 	$login = new WY_Login(LOGIN_BACKOFFICE);
 	if($login->checktoken($token)) {
		$mobj = new WY_Promotion($restaurant);
 		list($val, $msg) = $mobj->delete($name);
		if($val > 0)
			echo format_api(1, 1, 0, '', 200);
		else echo format_api(-1, NULL, 0, $msg, 200);
 		}
 	else echo format_api(-1, NULL, 0, 'wrong token', 401);	
}

?>