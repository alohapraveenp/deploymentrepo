<?

function createMarketingCampaign($data){

    $cd =$data['data'];
  
    $campaign_id =$cd['campaign'];
    $restaurant_id =$cd['restaurant'];
    $type =$cd['type'];
    $group =$cd['group'];
    $redirect_to =$cd['link'];
    $name =$cd['name'];
     $sub_id="";
 

//    $campaign_id =$app->request->post("campaign_id");
//    $restaurant_id =$app->request->post("restaurant_id");
//    $type=$app->request->post("type");
//    $redirect_to=$app->request->post("redirect_to");
//    $sub_id = $app->request->post("sub_id");
    $creation_date = date('Y-m-d H:i:s');
    $refId= date('Ymd');
    if($type==='external'){
        $campaign_id ='0022';
    }
    $campaignToken =uniqid();
      $campaign = new WY_MarketingCampaign();
    $generateToken = $type.'|'.$restaurant_id.'|'.$refId.'|'.$campaign_id.$sub_id.'|'.$campaignToken; 
    
    if(substr( $redirect_to, 0, 4 ) != "http" ){
         $redirect_to ="http://".$redirect_to;
    }
    $redirect_to = $redirect_to.'?utm_source='.$type.'&utm_medium='.$type.$campaign_id.'&utm_campaign='.$type.$campaign_id.$sub_id;
  
    $campaign_token = $campaign->base64url_encode($generateToken);
 
    $longUrl = REDIRECTION.$generateToken;
    $shortUrl = $campaign->bitly_shorten($longUrl);

    $campaign->create($campaign_id, $sub_id,$restaurant_id, $type, $redirect_to, $creation_date, $campaignToken,$group,$name,$shortUrl);
    $errors = null;
    $trackingLink = $shortUrl;
    echo format_api(1, $trackingLink, 0, $errors);
}
function getMarketingTypes($type){
    $campaign = new WY_MarketingCampaign();
    $data = $campaign ->getTypes($type); 
    
    echo format_api(1, $data, 0, 0);
    
}
function getCampaignList(){
     $res = new WY_restaurant();
    $campaign = new WY_MarketingCampaign();
    $data = $campaign ->getCampaignList(); 
    $resAr= $res->getSimpleListRestaurant();
    $result =array('data'=>$data,'restuarant'=>$resAr);
    
    echo format_api(1, $result, 0, 0);
}

?>