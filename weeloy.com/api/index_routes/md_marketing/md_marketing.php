<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
header('Access-Control-Allow-Headers: origin, x-requested-with, content-type');

	require_once("conf/conf.init.inc.php");
	require_once("lib/wpdo.inc.php");
	require_once("lib/class.tool.inc.php");
	require_once("lib/Browser.inc.php");
	require_once("lib/class.mail.inc.php");
	require_once("lib/class.async.inc.php");
	require_once("lib/class.spool.inc.php");
	require_once("lib/wglobals.inc.php");
	require_once("lib/class.dailyspecials.inc.php");
	// require_once("lib/class.restaurant.inc.php");

	$app->post('/register', function() use ($app) {

		return register($app);

   	});
        $app->post('/dsb/register', function () use ($app) {
              return dsbResgister($app);
        });

	//post daily special board
   	$app->post('/dsb/postdaily', function () use ($app) {
	    
	    $restaurant = new WY_dailyspecial();

	    echo $restaurant->PostDailySpecial($_REQUEST['restaurant'],$_FILES['file']) ? format_api(1, "Success!", 1, NULL) : format_api(0, "Invalid input parameters!", 0, NULL);
	});

   	$app->post('/dsb/login/', function () use ($app) {
		getcredentials($app);
	});

	$app->get('/dsb/login2/:email/:password', 'getcredentialsget');

	$app->post('/getdailyspecial', function() use ($app) {

	   	getdailyspecial($app);
   	});

	$app->post('/restaurant/', function() use ($app) {

		return restaurant($app);

   	});

   	$app->get('/getMagazine/',function () use ($app) {
    	return getMagazine($app);
	});

	$app->post('/getMagazine/saveimg/', function() use ($app) {
		return saveimg($app);
   	});

   	$app->post('/getMagazine/savecont/', function() use ($app) {
		return savecont($app);
   	});
   	

	$app->post('/confirmOTP', function() use ($app) {

		return confirmOTP($app);

   	});


	//Function to generate one-time password   
  	function generateOTP($length = 4, $chars = 'abcdefghijklmnopqrstuvwxyz') {  
     	$chars_length = (strlen($chars) - 1);  
     	$string = $chars{rand(0, $chars_length)};  
     	for ($i = 1; $i < $length; $i = strlen($string)) {  
        	$r = $chars{rand(0, $chars_length)};  
        	if ($r != $string{$i - 1}) $string .= $r;  
     	}  
     	return $string;
    }


    function getdailyspecial($app) 
    {
		$data = null;
		$status = $sizedata = 0;
		$msg = "";

		$dailyspecials = new WY_dailyspecial();
		$dailyspecials->GetDailySpecialBoard();

		$status = $dailyspecials->result;

		if ($dailyspecials->result > 0) {
			$content = $dailyspecials->content;
			$data = array('row' => $content);
			$sizedata = count($content);
			echo format_api($status, $data, $sizedata, $msg);
		}
		else
		{
			echo format_api(0, null, 0, $dailyspecials->msg);
		}
	}

	function getMagazine($app){
	    $errors=NULL;
	    $res = new WY_dailyspecial();
	    $articles= $res->getMagazine();
	     
	    echo format_api(1, $articles, count($articles), NULL);

	}

   	
	function register($app) {

		$restaurant = json_decode($app->request()->getBody(),true);

		$rest = new WY_restaurant();

		$resOTP = generateOTP();

		$result = $rest->updateGeneric($restaurant['restaurant'], 'md_otp', $resOTP);

		$sms = new JM_Sms();

		$smsMsg = "Please use One-Time Password (OTP): " . $resOTP . " to register restaurant " . $restaurant['title'] . " on Weeloy.com";

		$res = $sms->sendMessage($restaurant['mobile'], $smsMsg, '', '', '', '', 'textmagic');

		echo format_api(1, $restaurant['restaurant'], 1, ''); 

	}

	function restaurant($app) {

	    $rest = new WY_dailyspecial();

	    $result = $rest->md_restaurant();

	    echo format_api(1, $result, count($result), '');

	}

	function confirmOTP($app) {
		$restOTP = json_decode($app->request()->getBody(),true);

		$rest = new WY_restaurant();

		$checkOTP = $rest->md_confirmOTP($restOTP['restaurant'], $restOTP['otp']);

		if ( $checkOTP === 1 ) {
			$error = 'OTP verification successful';
			$result = $rest->updateGeneric($restOTP['restaurant'], 'md_verified', 'Y');
		} else {
			$error = 'OTP verification failed';
		}
		echo format_api(1, $checkOTP, 1, $error); 

	}

	function getcredentials($app) {
		$data = null;
		$status = $sizedata = 0;
		$msg = "";

		$loginx = new WY_dailyspecial();
		$loginx->getLogindetails($_REQUEST['login']);

		$status = $loginx->result;

		if ($loginx->result > 0) {
			$content = $loginx->content;
			$data = array('row' => $content);
			$sizedata = count($content);
			echo format_api($status, $data, $sizedata, $msg);
		}
		else
		{
			echo format_api(0, null, 0, $loginx->msg);
		}
	}

	function saveimg($app) {

		$data = null;
		$status = $sizedata = 0;
		$msg = "";
		$result = new WY_dailyspecial();

		$params =  json_decode($app->request()->getBody(),true);

		$result->insertimg($params);

		$status = '';
		$data = "";
		$sizedata = 1;
		echo format_api($status, $data, $sizedata, $msg);

	}

	function savecont($app) {

		$data = null;
		$status = $sizedata = 0;
		$msg = "";
		$result = new WY_dailyspecial();

		$params =  json_decode($app->request()->getBody(),true);

		$result->insertcont($params);

		$status = '';
		$data = "";
		$sizedata = 1;
		echo format_api($status, $data, $sizedata, $msg);

	}

	function getcredentialsget($email, $password) {

		$data = null;
		$status = $sizedata = 0;
		$msg = "";
		$loginx = new WY_dailyspecial();

		$loginx->getLogindetailsget($email,$password);

		$status = $loginx->result;

		if ($loginx->result > 0) {
			$content = $loginx->content;
			$data = array('row' => $content);
			$sizedata = count($content);
			echo format_api($status, $data, $sizedata, $msg);
		}
		else
		{
			echo format_api(0, null, 0, $loginx->msg);
		}
	}
       function dsbResgister($app){
            $params = json_decode($app->request()->getBody(),true);
            
            error_log("DSB PARAMS " .print_r($params,true));
            $dsbspl = new WY_dailyspecial();
            $files ='';
            if(isset($_FILES['file'])){
                $files = $_FILES['file'];
            }
            $dsbspl->registerDSBRestaurant($params['restaurant'],$files);
            if($dsbspl->result == 1){
               echo format_api(1, "Success!", 1, NULL);
            }else{
                echo format_api(0, "Invalid input parameters!", 0, NULL);
            }
           
            
        }

?>