<?

$app->post('/info/', function () use ($app) {
    $data = json_decode($app->request()->getBody(), true);
    return loginfo($data['category'], $data['content']);
});

$app->post('/insert/', function () use ($app) {
    $data = json_decode($app->request()->getBody(), true);
    return loginsert($data['content'], $data['token']);
});

$app->post('/read/', function () use ($app) {
    $data = json_decode($app->request()->getBody(), true);
    return logread($data['content'], $data['token']);
});

$app->post('/admin/', function () use ($app) {
    $data = json_decode($app->request()->getBody(), true);
    return logadmin($data['content'], $data['token']);
});

function loginfo($category, $content) {

    $category = clean_text($category);
    $content = clean_text($content);

    $category = substr($content, 0, 12);
    $content = substr($content, 0, 80);
    try {
	    WY_debug::recordDebug("API-LOG", $category, $content);
    } catch (PDOException $e) { api_error($e, "loginfo"); }
}

function loginsert($obj, $token) {

    try {
		$login = new WY_Login(set_valid_login_type('backoffice'));
		if ($login->checktoken($token) == false) {
			echo format_api(-1, '', 0, 'wrong token');
			return;
		}

		$log = new WY_log();
		$data = $log->insert($obj);
		if($log->result < 0)
			echo format_api(0, "FAIL", 0, $log->msg);
		else echo format_api(1, $data, count($data), $log->msg);
    } catch (PDOException $e) { api_error($e, "loginfo"); }
}

function logread($obj, $token) {

    try {
		$login = new WY_Login(set_valid_login_type('backoffice'));
		if ($login->checktoken($token) == false) {
			echo format_api(-1, '', 0, 'wrong token');
			return;
		}

		$log = new WY_log();
		$data = $log->read($obj);
		if($log->result < 0)
			echo format_api(0, "FAIL", 0, $log->msg);
		else echo format_api(1, $data, count($data), $log->msg);
    } catch (PDOException $e) { api_error($e, "loginfo"); }
}

function logadmin($obj, $token) {

    try {
		$login = new WY_Login(set_valid_login_type('backoffice'));
		if ($login->checktoken($token) == false) {
			echo format_api(-1, '', 0, 'wrong token');
			return;
		}

		$log = new WY_log();
		$data = $log->admin($obj);
		if($log->result < 0)
			echo format_api(0, "FAIL", 0, $log->msg);
		else echo format_api(1, $data, count($data), $log->msg);
    } catch (PDOException $e) { api_error($e, "loginfo"); }
}

?>