<?php

$app->get('/profilepicture', 'api_authentificate', function () {
    return getUserProfilePicture();
});

$app->post('/profilepicture/upload', 'api_authentificate', function () use ($app) {
    return addUserProfilePicture($app);
});
$app->post('/profile/update', function () use ($app) {
    return updateMemberProfile($app);
});
$app->post('/updatemembercode', function () use ($app) {
    return updateMemberCode($app);
});
$app->get('/profile/getdetails', 'api_authentificate', function () {
   return getMemberProfile();
});

$app->post('/acl', function () use ($app) {
    $data = json_decode($app->request()->getBody(),true); 
   return createUseracl($data);
});
$app->get('/getuseracl', function ()  {
   return getUseracl();
});
$app->post('/updateacl', function () use ($app) {
   $data = json_decode($app->request()->getBody(),true); 
   return updateUseracl($data);
});

$app->post('/deleteacl', function () use ($app) {
   $data = json_decode($app->request()->getBody(),true); 
   return deleteUseracl($data);
});
$app->post('/mobile/profile/update', function () use ($app) {
    return updateProfileMobile($app);
});

//user favorite

$app->get('/:restaurant/favorite/:is_favorite', 'api_authentificate', function ($restaurant, $is_favorite) use ($app) {
    return setFavorite($app, $restaurant, $is_favorite);
});

$app->get('/booking', 'api_authentificate', function () use ($app) {
    return getBookingsUser();
});

// don't change the order
$app->get('/booking/period/:period', 'api_authentificate', function ($period) {
    return getBookingsUserPeriod($period);
});

$app->get('/booking/:date1/:date2', 'api_authentificate', function ($date1, $date2) use ($app) {
    return getBookingsUser($date1, $date2);
});

$app->get('/booking/:query/:filter/:date1/:date2', function ($query, $filter, $date1, $date2) use ($app) {
    return getBookingsUserFilter($query, $filter, $date1, $date2);
});

$app->get('/booking/:query/:filter/:date1/:date2', 'api_authentificate', function ($query, $filter, $date1, $date2) use ($app) {
    return getBookingsUserFilter($query, $filter, $date1, $date2);
});

$app->post('/addbooking/add', function () use ($app) {
  return addBooking($app);
});

$app->get('/cancelbooking/cancel/:confirmation', 'api_authentificate', function ($confirmation) {
    return cancelUserBooking($confirmation);
});

$app->post('/deregister', 'api_authentificate', function () use ($app) {
    return removeUser($app);
});

$app->post('/member/list', function () use ($app) {
    return listmember();
});



function getUserProfilePicture() {

    $user = $_SERVER['PHP_AUTH_USER'];
    $media = new WY_Media($user, '', 'user');

    $path = $media->getUserProfilePicture($user);
    if (empty($path)) {
        echo format_api(0, NULL, 0, 'Image not found');
        return;
    }
    echo format_api(1, $path, 0, NULL);
}

function addUserProfilePicture($app) {

    $_POST["Stage"] = "Load";
    $category = 'profile';
    $user = $_SERVER['PHP_AUTH_USER'];
    $member = new WY_Member();

    $id = $member->getIdByEmail($user);
    $media = new WY_Media($user, $id, 'user');

    //CHECK FOLDER EXIST
    $media->l_folder_exist();


    if (!isset($_FILES['file'])) {
        echo "No files uploaded!!v";
        return;
    }

    $files = $_FILES['file'];
    $profile_picture =null;

    if ($files['error'] === 0) {
        $name = uniqid('img-' . date('Ymd') . '-');
        $res = $media->uploadImage($user, $category, 'user');
        //kala addded this line for getmember profile
        if($res[0]>0) {
            $proPic = $media->getProfilePic('profile',$user);

            if(isset($proPic['path']) && $proPic['name']){
               $profile_picture ='https://media.weeloy.com'.$proPic['path'].$proPic['name'];
               error_log(__FILE__." ".__FUNCTION__." ".__LINE__." ".$profile_picture);
    }
        }
    }

    if ($res[0] < 0) {
        echo format_api(0, NULL, 0, "$res[1]");
        return;
    }
    echo format_api(1, $profile_picture, 1, NULL);
}

function updateMemberProfile($app) {
 
 
    $_POST["Stage"] = "Load";
    $category = 'profile';
    $user = $_SERVER['PHP_AUTH_USER'];
    $member = new WY_Member();
    $data = $app->request->post();
    $email = $app->request->post('email');
    $prevuserData = $member->getMember($email);
    $member->email = $email;
    $member->firstname = $app->request->post('firstname');
    $member->name = $app->request->post('lastname');
    $member->mobile = $app->request->post('mobile');
    $member->salutation = $app->request->post('salutation');
    $member->gender =(isset($data['gender'])) ? $data['gender'] :$prevuserData['gender'];
    if(isset($data['membercode'])) {
        $member->membercode = $app->request->post('membercode');
        $newMemCode =(!empty($app->request->post('membercode'))) ? $app->request->post('membercode') :'';
    }else{
        $newMemCode = $prevuserData['membercode'];
    }
    if(isset($data['country_code'])){
         $member->mobile = $data['country_code'].$app->request->post('mobile');
    }
    
    
 
    //need to check if membercode  new or old one
    
     //if new one -need to update booking table and send sms to user
    
    $member->updateMember();
       
    
     $id = $member->getIdByEmail($user);
     $media = new WY_Media($user, $id, 'user');
     $userData = $member->getMember($email);
     
     
     if($prevuserData['membercode']!== $newMemCode){
         $member->updateuserMembercode($email,$userData['membercode']);
     }
     $proPic = $media->getProfilePic('profile',$user);
     $userData['profilePic'] = 'https://media.weeloy.com'.$proPic['path'].$proPic['name'];
            
    if(isset($_FILES['file'])){
        $files = $_FILES['file'];
        if ($files['error'] === 0) {
            $name = uniqid('img-' . date('Ymd') . '-'); 
            $res = $media->uploadImage($user, $category, 'user');
            $proPic = $media->getProfilePic('profile',$user);
            $userData['profilePic'] = 'https://media.weeloy.com'.$proPic['path'].$proPic['name'];
        }
    }
    if (@$res[0] < 0) {
        echo format_api(0, NULL, 0, "$res[1]");
        return;
    }
    
    
    $user_info=$userData;
    $mobile_array['prefix'] = '';
    $mobile_array['mobile'] = $user_info['mobile'];

    $tmp = explode(' ', $user_info['mobile']);
    if (count($tmp) == 2 && substr($tmp[0], 0, 1) == '+') {
        $mobile_array['prefix'] = trim($tmp[0]);
        $mobile_array['mobile'] = trim($tmp[1]);
    } else {
        if (substr($user_info['mobile'], 0, 1) == '+') {

            if (substr($user_info['mobile'], 1, 1) == '1') {
                $mobile_array['prefix'] = trim(substr($user_info['mobile'], 0, 4));
                $mobile_array['mobile'] = trim(substr($user_info['mobile'], 4));
            } else {
                $mobile_array['prefix'] = trim(substr($user_info['mobile'], 0, 3));
                $mobile_array['mobile'] = trim(substr($user_info['mobile'], 3));
            }
        }
    }     
    
    
    
    $user_session = $_SESSION['user'];
    $user_session['firstname'] = $member->firstname;
     $user_session['salutation'] = $member->salutation;
    $user_session['lastname'] = $member->name;
    $user_session['gender'] = $member->gender;
    $user_session['prefix'] = $mobile_array['prefix'];
    $user_session['mobile'] = $mobile_array['mobile'];
   
    $_SESSION['user'] = $user_session;
    
      $res = array(
        'firstname' => $member->firstname,
        'lastname' => $member->name,
        'gender' => $member->gender, 
        'mobile' => $member->mobile);
    $_SESSION['email'] = [
          "email" => $member->email,
          "data" => $res,
      ];
    //$_SESSION['user'] = $userData['gender'];
    echo format_api(1, $userData, 1, NULL);
}

function updateProfileMobile($app){
    $missing_params  = array();
    $content_type = $app->request->getContentType();
     
    if(!empty($content_type) && strpos($content_type,'application/json') !== false){
        $params = json_decode($app->request()->getBody(), true);
    }else{
        $params = $app->request->post();
    }

    //$user = $_SERVER['PHP_AUTH_USER'];
    $member = new WY_Member();
    if(empty($params['firstname'])){
       $missing_params[] = 'firstname';
    }
    if(empty($params['email'])){
       $missing_params[] = 'email';
    }
    if(empty($params['lastname'])){
   
        $missing_params[] = 'lastname';
    }
    if(empty($params['mobile'])){
        $missing_params[] = 'mobile';
    }
    if(empty($params['salutation'])){
        $missing_params[] = 'salutation';
    }
    if(count($missing_params)>0){
        $missing_params = json_encode($missing_params);
        echo format_api(0, $missing_params, 0, 'missing params');  
        exit;
    }

    $email = $params['email'];
    $prevuserData = $member->getMember($email);
    $member->email = $email;
    $member->firstname = $params['firstname'];
    $member->name = $params['lastname'];
    $member->mobile = $params['mobile'];
    $member->salutation = $params['salutation'];
    
    if(isset($params['country_code'])){
         $member->mobile = $params['country_code'].$params['mobile'];
    }
    $member->updateMember();
    echo format_api(1, $member, 1, NULL);
}

function updateMemberCode($app){
    $_POST["Stage"] = "Load";
    $category = 'profile';
    //$user = $_SERVER['PHP_AUTH_USER'];
    $member = new WY_Member();
    $email = $app->request->post('email');
    $newMemCode =$app->request->post('membercode');
    $prevuserData = $member->getMember($email);
    $member->email = $email;
    $member->membercode = $newMemCode;
    $member->updateMember();
    $userData = $member->getMember($email);

    if($prevuserData['membercode']!==$newMemCode) {
         $member->updateuserMembercode($email,$newMemCode);
     	}
     echo format_api(1, $userData, 1, NULL);
}

function getMemberProfile(){
    $user = $_SERVER['PHP_AUTH_USER'];
    $member = new WY_Member(); 
    $user = $_SESSION['user']['email'];
    $userData = $member->getMember($user);
    $id = $member->getIdByEmail($user);

    $media = new WY_Media($user, $id, 'user');
    $proPic = $media->getProfilePic('profile',$user);
     $userData['profilePic'] =null;
    if(isset($proPic['path']) && $proPic['name']) {
    	$userData['profilePic'] ='https://media.weeloy.com'.$proPic['path'].$proPic['name'];
    	}
    echo format_api(1, $userData, 1, NULL);
}

function listmember() {
	$member = new WY_Member();
    	$data = $member->getAllMember();
	echo ($member->result > 0) ? format_api(1, $data, count($data), NULL) : format_api(-1, NULL, 0, $member->msg);
}
	
function createUseracl($data){
    $email =$data['email'];
    $role =$data['role'];
    $acl = $data['acl'];

    $member = new WY_Member();
    $result = $member->createUserAcl($email,$role,$acl);
    if ($result < 0) {
        echo format_api(-1, NULL, 1, $member->msg);
    } else {
        echo format_api(1, 1, 1, NULL);
    }
    
}

function getUseracl(){
    $member = new WY_Member();
    $result = $member->getUserAcl();
    $errors=null;
	echo format_api(0, $result, count($result), $errors);  
}

function updateUseracl($data){
   
    $email =$data['email'];
    $role =$data['role'];
    $acl = $data['acl'];
    $id =$data['id'];
    $member = new WY_Member();
    $result = $member->updateUserAcl($id,$email,$role,$acl);
    if ($result < 0) {
        echo format_api(-1, NULL, 1, $member->msg);
    } else {
        echo format_api(1, 1, 1, NULL);
    }
}

function deleteUseracl($data){
   
    $email =$data['email'];
    $member = new WY_Member();
    $result = $member->deleteUserAcl($email);
    if ($result < 0) {
        echo format_api(-1, NULL, 1, $member->msg);
    } else {
        echo format_api(1, 1, 1, NULL);
    }
}

function setFavorite($app, $restaurant, $is_favorite) {
    $user = $_SERVER['PHP_AUTH_USER'];
    $member = new WY_Member($user);
    $result = $member->setFavoriteRestaurant($restaurant, $is_favorite);
    if ($result < 0) {
        echo format_api(0, NULL, 1, $result);
    } else {
        echo format_api(1, 1, 1, NULL);
    }
}

function getBookingsUser($date1 = NULL, $date2 = NULL) {
    $email = $_SERVER['PHP_AUTH_USER'];

    
    //fix chris ios issue
    $date_30 = date('Y-m-d', strtotime("+31 days"));
    if($date2 < $date_30){
        $date2 = date('Y-m-d', strtotime("+365 days"));
    }
    
    try {
        $member = new WY_Member($email);
        $bookingsAr = $member->getBooking($date1, $date2, 'DESC');
        $errors = null;
        $data = array("bookings" => $bookingsAr);
        echo format_api(1, $data, count($bookingsAr), $errors);
    } catch (PDOException $e) {
        api_error($e, "getBookingsUser");
    }
}

function getBookingsUserPeriod($period) {
    $email = $_SERVER['PHP_AUTH_USER'];

    try {
        $params = array('include_reviews' => true, 'period' => $period,'tracking'=>'no_white_label');
        $bookings = new WY_Booking($email);
        $bookings->getUserBookings($email, $params);
        $errors = null;
        $data = array("bookings" => $bookings->bookings);
        echo format_api(1, $data, count($bookings->bookings), $errors);
    } catch (PDOException $e) {
        api_error($e, "getBookingsUser");
    }
}

function getBookingsUserFilter($user, $filter, $date1, $date2) {
    $member = new WY_Member($user);
    $errors = null;
    $bookingsAr = array();
    
    
    // hot fix bug on date  2016
    if(substr($date1, 0, 8) == '2016-12-'){   
        $date1 = str_replace('2016-12-', '2015-12-',$date1);
    }
    // hot fix bug on date
    
    
    try {

        $bookingsAr = $member->getBookingsUserFilter($user, $filter, $date1, $date2, 'not_cancled');
        $data = array("bookings" => $bookingsAr);
        echo format_api(1, $data, count($data), $errors);
    } catch (PDOException $e) {
        api_error($e, "getBookingsUserFilter");
    }
}

function addBooking($app) {
    try {
        if (isset($_REQUEST)) {
            
            $restaurant = filter_input(INPUT_POST, 'restaurant', FILTER_SANITIZE_STRING);
            $rdate = filter_input(INPUT_POST, 'rdate', FILTER_SANITIZE_STRING);
            $rtime = filter_input(INPUT_POST, 'rtime', FILTER_SANITIZE_STRING);
            $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_STRING);
            $mobile = filter_input(INPUT_POST, 'mobile', FILTER_SANITIZE_STRING);
            $cover = filter_input(INPUT_POST, 'cover', FILTER_SANITIZE_STRING);
            $salutation = filter_input(INPUT_POST, 'salutation', FILTER_SANITIZE_STRING);
            $firstname = filter_input(INPUT_POST, 'firstname', FILTER_SANITIZE_STRING);
            $lastname = filter_input(INPUT_POST, 'lastname', FILTER_SANITIZE_STRING);
            $country = filter_input(INPUT_POST, 'country', FILTER_SANITIZE_STRING);
            $language = filter_input(INPUT_POST, 'language', FILTER_SANITIZE_STRING);
            $specialrequest = filter_input(INPUT_POST, 'specialrequest', FILTER_SANITIZE_STRING);
            $status = filter_input(INPUT_POST, 'status', FILTER_SANITIZE_STRING);
            
            
            // header information on app
            $app_platform = $app->request->headers->get('Platform');
            $app_name = $app->request->headers->get('App');
            $app_version = $app->request->headers->get('Version');
            $app_device = $app->request->headers->get('Device');
            $app_sdk = $app->request->headers->get('Sdk');
            $app_user_agent = $app->request->headers->get('User_agent');

            
            $booking = new WY_Booking();
            $notification = new WY_Notification();
            $logger = new WY_log("mobile");
            $type = "booking";
            $tracking = "";
            $booker = "";
            $company = "";
            $hotelguest = 0;
            $state = "to come";
            $optin = "";
            $booking_deposit_id = "";
            $extra = "";
            $product = "";
       
            
            if(isset($app_name) && isset($app_version)){
                $userag = $app_platform . ' ' . $app_name. ' ' . $app_version . ' '  . $app_sdk . ' '  . $app_device . ' '  . $app_user_agent;
            }else{
                $userag = clean_text($app->request->getUserAgent());
            }

            if(isset($app_name) && isset($app_version)  && (is_valid_version($app_platform, $app_name, $app_version))){
                  WY_debug::recordDebug("DEBUG", "BKG-USER-OK", $userag);
                 // ios Weeloy 2.2.22 9.3.4 iPhone 6 Weeloy/11 CFNetwork/758.5.3 Darwin/15.6.0
                 // android Weeloy 2.2.7.5 4.4.2 MID MID okhttp/2.4.0 
            }else{
                if($restaurant == "SG_SG_R_BlackTongueBistroBar" && preg_match("/^Weeloy/",  $userag))
                    if(WY_restaurant::check_iswheelable($restaurant) == false) {
                            WY_debug::recordDebug("DEBUG", "BKG-USER", $userag);
                            echo format_api(1, NULL, 0, "no promo");
                            return;
                            }
            }
            $booking->createBooking($restaurant, $rdate, $rtime, $email, $mobile, $cover, $salutation, $firstname, $lastname, $country, $language, $specialrequest, $type, $tracking, $booker, $company, $hotelguest, $state, $optin, $booking_deposit_id, $status, $extra, $product, $userag);
            if ($booking->result < 0) {
                echo format_api(1, NULL, 0, $booking->msg);
                return;
            }
            $bkconfirmation = $booking->confirmation;
            $booking->getBooking($bkconfirmation);

            $path_tmp = explode(':', get_include_path());
            $qrcode_filename = $path_tmp[count($path_tmp) - 1] . 'tmp/' . $bkconfirmation . '.png';
            $qrcode = new QRcode();
            $qrcode->png('{"membCode":"' . $booking->membCode . '"}', $qrcode_filename);
            //file_put_contents($qrcode_filename,$qrcode->image(8));
            if($restaurant == "SG_SG_R_BurntEnds" && $booking->status == 'pending_payment'){
                $notification->notify($booking, 'booking_pending');
            }else{
                $notification->notify($booking, 'booking');
            }
            $logger->LogEvent('', 701, $bkconfirmation, '', '', date("Y-m-d H:i:s"));
            //    $booking->notifyBooking();
            $errors = null;
            echo format_api(1, $booking, count($booking), $errors);
        }
    } catch (PDOException $e) {
        api_error($e, "addBooking");
    }
}

function cancelUserBooking($confirmation) {

    $user = $_SERVER['PHP_AUTH_USER'];
    try {
        $booking = new WY_Booking();

        $booking->getBooking($confirmation);

        if ($booking->email == $user) {
            $booking->cancelBooking($confirmation, "", "", "");
            
            $notification = new WY_Notification();
            $notification->notify($booking,'cancel');
            
            $errors = null;
            if ($booking->status == 'cancel') {
                echo format_api(1, $booking, 1, NULL);
            } else {
                echo format_api(1, NULL, 1, $errors);
            }
        }
    } catch (PDOException $e) {
        api_error($e, "cancelUserBooking");
    }
}

function removeUser($app) {
    $user = $_SERVER['PHP_AUTH_USER'];
    try {
        $sql = "DELETE FROM member WHERE email = '$user'";

        //pdo_exec($sql);
        $sql = "DELETE FROM login WHERE Email = '$user'";
        //pdo_exec($sql);
        echo format_api(1, 1, 1, NULL);
    } catch (Exception $ex) {
        echo format_api(1, NULL, 1, array('error' => 'error'));
    }
}

function is_valid_version($app_platform, $app_name, $app_version){
    $app_version_exploded = explode('.',$app_version);
    
    if($app_platform == 'ios' && $app_name == 'Weeloy' ){
        if($app_version_exploded[0] < 2){
           return false; 
        }else{
            if($app_version_exploded[1] < 2){
                return false;
            }else{
                if($app_version_exploded[0] < 23){
                    return false;
                }
            }
        }
        return true;
    }
    
    if($app_platform == 'android' && $app_name == 'Weeloy' ){
        
        if($app_version_exploded[0] < 2){
           return false; 
        }else{
            if($app_version_exploded[1] < 2){
                return false;
            }else{
                if($app_version_exploded[0] < 7){
                    return false;
                }else{
                    if($app_version_exploded[0] < 5){
                        return false;
                    }
                }
            }
        }
        return true;
    }
}


?>