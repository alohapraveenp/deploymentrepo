<?

$app->post('/', function () use ($app) {
    $data = json_decode($app->request()->getBody(), true);
    if(!isset($data['product'])) $data['product'] = "";
    setAlloteResponse($data['restaurant'], $data['year'], $data['size'], $data['start'], $data['ldata'], $data['ddata'], $data['product'], $data['token']);
});


function setAlloteResponse($restaurant, $year, $size, $start, $lunchdata, $dinnerdata, $product, $token) {

	$result = false; 
	$msg = "";
	
    try {
        $login = new WY_Login(set_valid_login_type('backoffice'));
        $result = $login->checktoken($token);
        $msg = $login->msg;
        if ($result) {
			$res = new WY_restaurant;
			$res->getRestaurant($restaurant);
			if($res->result < 0) {
				echo format_api(-1, '', 0, $res->msg);
				return;
				}
            $res->saveAlloteRestaurant($restaurant, $year, $size, $start, $lunchdata, $dinnerdata, $product);
            $result = ($res->result > 0);
            $msg = $res->msg;
			}
		if($result)
	        echo format_api(1, '', 0, '');
	    else echo format_api(-1, '', 0, $msg);
	    
    } catch (Exception $e) {
        api_error($e, "setAlloteResponse");
    }
}


?>