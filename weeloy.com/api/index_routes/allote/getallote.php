<?
$app->get('/:restaurant/:year', 
	function ($restaurant, $year) { 
		$product = "";
		return getAlloteReponse($restaurant, $year, $product); 
});

$app->post('/', function () use ($app) {
    $data = json_decode($app->request()->getBody(), true);
    if(!isset($data['product'])) $data['product'] = "";
    getAlloteReponse($data['restaurant'], $data['year'], $data['product']);
});


function getAlloteReponse($restaurant, $year, $product) {

    try {
        $restaurant = clean_text($restaurant);
        $res = new WY_restaurant;
        $res->getRestaurant($restaurant);
        if($res->result < 0) {
	    	echo format_api(-1, '', 0, $res->msg);
	    	return;
       		}
        $data = $res->getAlloteRestaurant($restaurant, $year, $product);
        $errors = null;
        echo format_api(1, $data, count($data), $errors);
    } catch (Exception $e) {
        api_error($e, "getAlloteReponse");
    }
}


?>