<?

$app->get('/:query/:page', 'getSpoolHistory' );

$app->post('/email',  function () use ($app) {
    $data = json_decode($app->request()->getBody(),true); 
    return getS3EmailTemp($data);
});

function getSpoolHistory($query,$page){
         $spool = new WY_Spool;
        $result = $spool->getspoolhistory($query,$page);
        $data = array("history" => $result);
        $errors=null;
        echo format_api(1, $data, count($result), $errors); 
}
function getS3EmailTemp($data){
    $spool = new WY_Spool;
    $result = $spool->getS3EmailTemplate($data); 
    $errors=null;
    echo format_api(1, $result, count($result), $errors); 
    exit;
}


?>