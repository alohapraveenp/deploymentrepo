<?php
header("Access-Control-Allow-Origin: *");

define('RESTAURANT_TEMPLATE_TABLE', 'restaurant_template_infomation');

require_once("conf/conf.init.inc.php");
require_once("lib/wpdo.inc.php");
require_once("lib/wglobals.inc.php");
require_once("conf/conf.session.inc.php");
require_once("lib/class.coding.inc.php");
require_once("lib/class.login.inc.php");
require_once("lib/class.booking.inc.php");
require_once("lib/class.review.inc.php");
require_once("lib/class.restaurant.inc.php");
require_once("lib/class.member.inc.php");
require_once("lib/class.allote.inc.php");
require_once("lib/class.menu.inc.php");
require_once("lib/class.event.inc.php");
require_once("lib/class.wheel.inc.php");
require_once("lib/class.service.inc.php");
require_once("lib/class.images.inc.php");
require_once("lib/class.media.inc.php");
require_once("lib/imagelib.inc.php");
require_once("lib/class.mail.inc.php");
require_once("lib/class.sms.inc.php");
require_once("lib/class.pushnotif.inc.php");
require_once("lib/class.debug.inc.php");
require_once("lib/class.qrcode.inc.php");
require_once("lib/class.cluster.inc.php");
require_once("lib/class.translation.inc.php");
require_once("lib/class.report.inc.php");
require_once("lib/class.order_online.inc.php");
require_once("lib/class.paypal.inc.php");
require_once("lib/class.invoice.inc.php");
//kala added this class
require_once("lib/class.invitefriends.inc.php");
require_once("lib/class.session.inc.php");

require_once("lib/Browser.inc.php");

require_once("lib/class.notification.inc.php");

require_once("lib/class.spool.inc.php");
require_once("lib/class.async.inc.php");
require_once ('lib/class.marketing_campaign.inc.php');
require_once ('lib/class.analytics.inc.php');
require_once ("lib/class.partner.inc.php");
require_once("lib/class.category.inc.php");
require_once("lib/class.payment.inc.php");
require_once('lib/class.payment_stripe.inc.php');
require_once("lib/class.restaurant_section.inc.php");
require_once("lib/class.payment_new.inc.php");

require_once("lib/class.sns.inc.php");

//Class for data check
require_once("lib/class.tool.inc.php");

////testing purpose only
//require_once("lib/class.notification.inc_old.php");

define("__MEDIA__SERVER_NAME__", 'https://media.weeloy.com/upload/restaurant/');
require 'Slim/Slim.php';
\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();

//$app->response()->header('Content-Type', 'application/json;charset=utf-8');
$app->contentType('application/json;charset=utf-8');

$app->group('/booking', function () use ($app) {
    require_once 'index_routes/booking/booking.php';
});

$app->group('/visit', function () use ($app) {
    require_once 'index_routes/booking/getbooking.php';
    require_once 'index_routes/visit/visit.php';
});

$app->group('/home', function () use ($app) {
    require 'index_routes/home/home.php';
});
$app->group('/mailchimp', function () use ($app) {
    require 'index_routes/mailchimp/mailchimp.php';
});
$app->group('/notification', function () use ($app) {
    require 'index_routes/notification/notification.php';
});

$app->group('/promotion', function () use ($app) {
    require 'index_routes/promotion/promotion.php';
});

$app->group('/restaurant', function () use ($app) {
    require 'index_routes/restaurant/restaurant.php';
});

$app->group('/restaurantfullinfo', function () use ($app) {
    require 'index_routes/restaurant/fullinfo.php';
});

$app->group('/user', function () use ($app) {
    require 'index_routes/user/user.php';
});

$app->group('/menu', function () use ($app) {
    require 'index_routes/menu/menu.php';
});

$app->group('/getallote', function () use ($app) {
    require 'index_routes/allote/getallote.php';
});

$app->group('/setallote', function () use ($app) {
    require 'index_routes/allote/setallote.php';
});

$app->group('/allote', function () use ($app) {
    require 'index_routes/allote/allote.php';
});

$app->group('/bookingservice', function () use ($app) {
    require 'index_routes/booking/bookingservice.php';
});

$app->group('/payment', function () use ($app) {
    require 'index_routes/payment/payment.php';
});

$app->group('/template', function () use ($app) {
    require 'index_routes/template/template.php';
});

$app->group('/mobile', function () use ($app) {
    require 'index_routes/mobile/mobile.php';
});

$app->group('/cart', function () use ($app) {
    require 'index_routes/cart/cart.php';
});

$app->group('/event', function () use ($app) {
    require 'index_routes/event/event.php';
});

$app->group('/log', function () use ($app) {
    require 'index_routes/logs/logs.php';
});

$app->group('/search', function () use ($app) {
    require 'index_routes/search/search.php';
});

$app->group('/passbook', function () use ($app) {
    require 'index_routes/passbook/passbook.php';
});

//settwofactauth
$app->group('/backoffice', function () use ($app) {
    require 'index_routes/twofactauth/twofactauth.php';
});

$app->group('/getspoolhistory', function () use ($app) {
    require 'index_routes/spoolhistory/spoolhistory.php';
});

// API for payment - start
$app->group('/remoteapi', function () use ($app) {
    require_once 'index_routes/remoteapi/remoteapi_response.php';  
});
// API for payment - finish

$app->get('/area/listrestaurant', function() use ($app) {
    require_once 'index_routes/restaurant/content.php';
    return getAreaRestaurant($app);
});
  
$app->post('/savecategory', function () use ($app) {
    require_once 'index_routes/restaurant/content.php';
    $data = json_decode($app->request()->getBody(),true);
    return saverestcategory($data);
});

$app->post('/deletecategory', function () use ($app) {
     require_once 'index_routes/restaurant/content.php';
     $data = json_decode($app->request()->getBody(),true);
     return deleterestcategory($data);
});

$app->get('/getrestmanagement', function ()  {
    require_once 'index_routes/restaurant/content.php';
    return getrestManagelevel();
});

$app->post('/sitemap/insert', function () use ($app) {
    require_once 'index_routes/restaurant/content.php';
    $data = json_decode($app->request()->getBody(), true);
    insertSitemap($data['url'], $data['nbresult']);
});

$app->get('/add_index', function () {
    require_once 'index_routes/restaurant/content.php';
    addIndex();
});

$app->get('/getTopRestaurant',function () use ($app) {
    require_once 'index_routes/restaurant/content.php';
    return getResTop($app);
   
});
$app->get('/getBlogArticles',function () use ($app) {
    require_once 'index_routes/restaurant/content.php';
    return getBlogTopArticles($app);
   
});

$app->post('/addPNDevice', function () use ($app) {
    require_once 'index_routes/PNDevice/PNDevice.php';
    return addPNDevice($app);
});

$app->post('/addPNDevice_TEST', function () use ($app) {	
    require_once 'index_routes/PNDevice/PNDevice.php';
    return addPNDevice_test($app);
});

$app->post('/deletePNDevice', function () use ($app) {
    require_once 'index_routes/PNDevice/PNDevice.php';
    return deletePNDevice($app);
});

$app->post('/updaterestauranttags', function () use ($app) {
    require_once 'index_routes/tag/tag.php';
    $data = json_decode($app->request()->getBody(),true);
    return updaterestaurantTag($data);
});
$app->post('/removetags', function () use ($app) {
    require_once 'index_routes/tag/tag.php';
    $data = json_decode($app->request()->getBody(),true);
    return removerestaurantTag($data);
});
$app->post('/tag/create', function () use ($app) {
    require_once 'index_routes/tag/tag.php';
    $data = json_decode($app->request()->getBody(),true);
    return createTag($data);
});

$app->post('/pushnotification', function () use ($app) {
    require_once 'index_routes/notification/getpush.php';
    $data = json_decode($app->request()->getBody(),true);
    return pushNotify($data);
});

$app->get('/getpushnotification',function() use ($app){
    require_once 'index_routes/notification/getpush.php';  
    return getNotificationall();
});

$app->post('/sms/notification/', function () use ($app) {
    require_once 'index_routes/notification/getpush.php';
    $data = json_decode($app->request()->getBody(), true);
    sendgenericsms($data['restaurant'], $data['booking'], $data['mobile'], $data['msg'], $data['email'], $data['token']);
});

$app->post('/setevent', function () use ($app) {
    require_once 'index_routes/event/eventcrud.php';  
    return setEvent($app);
});

$app->post('/deleteevent', function () use ($app) {
    require_once 'index_routes/event/eventcrud.php';  
    return deleteEvent($app);
});

$app->post('/updateevent', function () use ($app) {
    require_once 'index_routes/event/eventcrud.php';  
    return updateEvent($app);
});

$app->get('/event-booking/order/:order_id', function ($order_id) {
    require_once 'index_routes/event/eventcrud.php';  
    return eventOrder($order_id);
});

$app->get('/order/:order_id', function($order_id) {
    require_once 'index_routes/event/eventcrud.php';  
    return getOrderID($order_id);
});

$app->get('/orders', function() use ($app) {
    require_once 'index_routes/event/eventcrud.php';  
	return getOrderList($app);
});

$app->get('/order/:order_id/items', function($order_id) {
    require_once 'index_routes/event/eventcrud.php';  
    return getOrderDetails($order_id);
});

$app->get('/data/extract', function () use ($app) {
    require_once 'index_routes/data/data.php';  
    return extractData($app);
});

$app->get('/data/getlabel',function () use ($app) {    
    require_once 'index_routes/data/data.php';  
    return getLabelList($app);
});

$app->post('/createlabel',function () use ($app) {   
    require_once 'index_routes/data/data.php';  
    return createtData($app);
});

$app->get('/getCurrentWheelVersion/:restaurant', function ($restaurant) use ($app) {
    require_once 'index_routes/wheel/wheel.php';  
    return getCurrentWheelVersion($restaurant);
});

$app->get('/getCurrentWheelVersion/:restaurant/:confirmation', function ($restaurant, $confirmation) use ($app) {
    require_once 'index_routes/wheel/wheel.php';  
    return getCurrentWheelVersion($restaurant, $confirmation);
});


$app->get('/requestspin/:conf/:rcode/:mcode', function ($conf, $rcode, $mcode) use ($app) {
    require_once 'index_routes/wheel/wheel.php';  
    return requestSpin($conf, $rcode, $mcode);
});

$app->post('/requestspin/:conf/:rcode/:mcode', function ($conf, $rcode, $mcode) use ($app) {
    require_once 'index_routes/wheel/wheel.php';  
    return requestSpin($conf, $rcode, $mcode);
});


$app->get('/savespin/:win/:token', function ($win, $token) use ($app) {
    require_once 'index_routes/wheel/wheel.php';  
    return storeSpin($win, $token);
});

$app->get('/spinresult/:confirmation', function ($confirmation) use ($app) {
    require_once 'index_routes/wheel/wheel.php';  
    return getSpinResult($confirmation);
});

$app->get('/savespin/:segment/:win/:desc/:source/:token', function ($segment, $win, $desc, $source, $token) use ($app) {
    require_once 'index_routes/wheel/wheel.php';  
    return storeSpinComplete($segment, $win, $desc, $source, $token);
});

$app->get('/savespin/:segment/:win//:source/:token', function ($segment, $win, $source, $token) use ($app) {
    require_once 'index_routes/wheel/wheel.php';  
    return storeSpinComplete_HOTFIX($segment, $win, $desc, $source, $token);
});

$app->get('/savespin/:wheelversion/:segment/:angle/:win/:desc/:source/:token', function ($wheelversion, $segment, $angle, $win, $desc, $source, $token) use ($app) {
    require_once 'index_routes/wheel/wheel.php';  
    return storeSpinCompleteAngle($wheelversion, $segment, $angle, $win, $desc, $source, $token);
});

$app->get('/savespin2/:wheelversion/:segment/:angle/:win/:desc/:source/:token', function ($wheelversion, $segment, $angle, $win, $desc, $source, $token) use ($app) {
    require_once 'index_routes/wheel/wheel.php';  
    return storeSpinCompleteAngle2($wheelversion, $segment, $angle, $win, $desc, $source, $token);
});

$app->get('/wheel/:query', function ($query) use ($app) {
    require_once 'index_routes/wheel/wheel.php';  
    return getWheel($query);
});

$app->post('/wheel/:query', function ($query) use ($app) {
    require_once 'index_routes/wheel/wheel.php';  
    return getWheel($query);
});

$app->get('/wheeldescription/:query/version/:version', function ($query, $version) use ($app) {
    require_once 'index_routes/wheel/wheel.php';  
    return getWheelDescription($query, $version);
});

$app->get('/wheeldescription/:query', function ($query) use ($app) {
    require_once 'index_routes/wheel/wheel.php';  
    return getWheelDescription($query);
});

$app->get('/wheeldescription/:query/:option', function ($query, $option) use ($app) {
    require_once 'index_routes/wheel/wheel.php';  
    return getWheelDescription($query, $option);
});

$app->get('/cppwheeldescription/:query/:option', function ($query, $option) use ($app) {
    require_once 'index_routes/wheel/wheel.php';  
    return getCPPWheelDescription($query, $option);
});

$app->get('/bkconfirmation/:confirmation',  function ($confirmation) {
    require_once 'index_routes/booking/getbooking.php';
    return getBooking($confirmation);
});


$app->get('/logo', function () {
    require_once 'index_routes/media/media.php';  
    return getlogo();
});

$app->post('/logo', function () {
    require_once 'index_routes/media/media.php';  
    return getlogo();
});

$app->post('/update/videolink', function () use ($app) {
	require_once 'index_routes/media/media.php';  
       
    return updatevideolink($app);
});
$app->post('/videolink/getlist', function () use ($app) {
    require_once 'index_routes/media/media.php';  
    return getVideoList($app);
});


$app->post('/visitMember/', function () use ($app) {
    require_once 'index_routes/member/member.php';  
    $data = json_decode($app->request()->getBody(), true);
    return getVisitMember($data['email'], $data['token']);
});

$app->post('/madison/member/read/', function () use ($app) {
    require_once 'index_routes/member/member.php';  
	$data = json_decode($app->request()->getBody(), true);
	return madisonMemberRead($data['restaurant'], $data['token']);
	});

$app->get('/addmember/:email/:mobile/:password/:lastname/:firstname/:platform',  function ($email, $mobile, $password, $lastname, $firstname, $platform) {
    require_once 'index_routes/member/member.php';
    return addMember($email, $mobile, $password, $lastname, $firstname, $platform);
});

$app->post('/addmember/', function () use ($app) {
   $data = json_decode($app->request()->getBody(), true);
   require_once 'index_routes/member/member.php';
   return addMember($data["email"], $data["mobile"], $data["password"], $data["lastname"], $data["firstname"], $data["platform"], $data["member_type"], $data["gender"], $data["salutation"], $data["country"]);     
});

$app->get('/listmember', function ()  {
   require_once 'index_routes/member/member.php';
   return listMemberRes("dontcheck");     
});

$app->post('/listmember/', function () use ($app) {
   $data = json_decode($app->request()->getBody(), true);
   require_once 'index_routes/member/member.php';
   return listMemberRes($data["token"]);     
});

$app->post('/deletemember/', function () use ($app) {
   $data = json_decode($app->request()->getBody(), true);
   require_once 'index_routes/member/member.php';
   return deleteMember($data["email"], $data["token"]);     
});

$app->post('/readmemberresto/', function () use ($app) {
   $data = json_decode($app->request()->getBody(), true);
   require_once 'index_routes/member/member.php';
   return readMemberResto($data["email"], $data["token"]);     
});


$app->post('/updatememberpermission/', function () use ($app) {
   $data = json_decode($app->request()->getBody(), true);
   require_once 'index_routes/member/member.php';
   return updateMemberPermission($data["email"], $data["content"], $data["token"]);     
});

$app->post('/readpermissionvalue/', function () use ($app) {
   $data = json_decode($app->request()->getBody(), true);
   require_once 'index_routes/member/member.php';
   return readPermissionValue($data["token"]);     
});


$app->post('/callcenter/member/save/', function () use ($app) {
    require_once 'index_routes/member/member.php';  
	$data = json_decode($app->request()->getBody(), true);
	return callcentermember($data['email'],$data['restaurant'], $data['item'],$data['token']);
});

$app->get('/listcallcentermember/:restaurant', function ($restaurant)  {
   require_once 'index_routes/member/member.php';
   return getCallcenterMember($restaurant); 
    
});
$app->post('/callcenter/member/delete/', function () use ($app) {
    require_once 'index_routes/member/member.php';  
	$data = json_decode($app->request()->getBody(), true);
	return deleteccMember($data);
});


$app->get('/walkin/:query/:filter/:date1/:date2', 'api_authentificate', function ($query, $filter, $date1, $date2) use ($app) {
    require_once 'index_routes/booking/getbooking.php';
    return getWalkinsUserFilter($query, $filter, $date1, $date2);
});

$app->get('/confirmation/:conf/:email',  function ($conf, $email) {
    require_once 'index_routes/booking/getbooking.php';
    return getConfirmation($conf, $email);
});

$app->post('/confirmation/:conf/:email',  function ($conf, $email) {
    require_once 'index_routes/booking/getbooking.php';
    return getConfirmation($conf, $email);
});


$app->post('/rmloginc/module/loginfacebook/', function () use ($app) {
    require_once 'index_routes/login/login.php';  

    $data = json_decode($app->request()->getBody(), true);
    $content_type = $app->response->headers->get('Content-Type');
    if(!empty($content_type) && strpos($content_type,'application/json') !== false){
        $data = json_decode($app->request()->getBody(), true);
    }else{
        $data = $app->request->post();
    } 

    return rmloginfacebookc($data['email'], $data['facebookid'], $data['facebooktoken'], 'web', '1.0');
});


$app->get('/rmlogin/:email/:pass', function ($email, $pass) use ($app) {
    require_once 'index_routes/login/login.php';  
    return rmlogin($email, $pass);
});

$app->post('/rmlogin/:email/:pass', function ($email, $pass) use ($app) {
    require_once 'index_routes/login/login.php';  
    return rmlogin($email, $pass);
});

$app->get('/rmlogin/:email/:pass/:name/:version', function ($email, $pass, $name, $version) use ($app) {
    require_once 'index_routes/login/login.php';  
    return rmlogin($email, $pass, $name, $version);
});

$app->post('/rmlogin/:email/:pass/:name/:version', function ($email, $pass, $name, $version) use ($app) {
    require_once 'index_routes/login/login.php';  
    return rmlogin($email, $pass, $name, $version);
});

$app->get('/rmloginc/:email/:pass/:name/:version', function ($email, $pass, $name, $version) use ($app) {
    require_once 'index_routes/login/login.php';  
    return rmloginc($email, $pass, $name, $version);
});

$app->post('/rmloginc_facebook/:email/:facebookid/:facebooktoken/:name/:version', function ($email, $facebookid, $facebooktoken, $name, $version) use ($app) {
    require_once 'index_routes/login/login.php';  
    return rmloginc_facebook($email, $facebookid, $facebooktoken, $name, $version);
});

$app->post('/rmloginc_facebook', function () use ($app) {
    require_once 'index_routes/login/login.php';  
    return rmloginc_facebook($app);
});

$app->get('/facebookuser/:email/:facebookid/:facebooktoken', function ($email, $facebookid, $facebooktoken) use ($app) {
    require_once 'index_routes/login/login.php';  
    return GetFacebookUser($email, $facebookid, $facebooktoken);
});

$app->post('/facebookuser/:email/:facebookid/:facebooktoken', function ($email, $facebookid, $facebooktoken) use ($app) {
    require_once 'index_routes/login/login.php';  
    return GetFacebookUser($email, $facebookid, $facebooktoken);
});

$app->get('/rmlogout/:email', function ($email) use ($app) {
    require_once 'index_routes/login/login.php';  
    return rmlogout($email);
});

$app->post('/rmlogout/:email', function ($email) use ($app) {
    require_once 'index_routes/login/login.php';  
    return rmlogout($email);
});

$app->get('/rmlogout/:email/:platform', function ($email, $platform) use ($app) {
    require_once 'index_routes/login/login.php';  
    return rmlogout($email, $platform);
});

$app->get('/rmreadlogin/:email/:token', function ($email, $token) use ($app) {
    require_once 'index_routes/login/login.php';  
    return rmreadlogin($email, $token);
});
$app->post('/rmreadlogin/:email/:token', function ($email, $token) use ($app) {
    require_once 'index_routes/login/login.php';  
    return rmreadlogin($email, $token);
});

$app->post('/login/user/:email', function ($email) use ($app) {
    require_once 'index_routes/login/login.php';  
    $data =  $app->request->post();
    return getLoginMemberDetails($data['email'],$data['password']);
});

$app->post('/login/resetauthlink/:email', function ($email) use ($app) {
    require_once 'index_routes/login/login.php';  
    return sendAuthResetLink($email);
});
$app->post('/login/resetgooleauth/:email', function ($email) use ($app) {
    require_once 'index_routes/login/login.php';  
    return resetGooleAuth($email);
});

$app->post('/updatePassword/:email/:oldpwd/:newpwd', function ($email, $oldpwd, $newpwd) use ($app) {
    require_once 'index_routes/login/login.php';  
    return updatePassword($email, $oldpwd, $newpwd);
});

$app->get('/forgotPassword/:email', function ($email) use ($app) {
    require_once 'index_routes/login/login.php';  
    return forgotPassword($email);
});

$app->post('/forgotPassword/reset', function() use ($app) {
    require_once 'index_routes/login/login.php';  
    $data = json_decode($app->request()->getBody(), true);
    return ResetPassword($data['email'], $data['token'], $data['password']);
});

// USER PICTURE
$app->post('/renamemedia', function () use ($app) {
    require_once 'index_routes/media/media.php';  
    return renameMedia($app);
});

$app->post('/deletemedia', function () use ($app) {
    require_once 'index_routes/media/media.php';  
    return deleteMedia($app);
});

$app->post('/updatemedia', function () use ($app) {
    require_once 'index_routes/media/media.php';  
    return updateMedia($app);
});

$app->post('/uploadphoto/selfie-contest', function () use ($app) {
    require_once 'index_routes/media/media.php';  
    return uploadFoodSelfie($app);
});

$app->get('/logourl/:restaurant', function($restaurant) use ($app) {
    require_once 'index_routes/media/media.php';  
    return getlogourl($restaurant);
});

$app->get('/reviews/getBookingWithPendingReviews/:count', 'api_authentificate', function ($count) use ($app) {
    require_once 'index_routes/review/review.php';  
	return getBookingWithPendingReviews($count);
});

$app->get('/getreviews/:query', function ($query) use ($app) {
    require_once 'index_routes/review/review.php';  
	return getReviews($query);
});

$app->get('/getreviews/:query/:page', function ($query, $page) use ($app) {
    require_once 'index_routes/review/review.php';  
	return getReviews($query, $page);
});

$app->get('/getreviews/:query/list/summary', function ($query) use ($app) {
    require_once 'index_routes/review/review.php';  
	return getReviewsList($query);
});

$app->get('/getreviews/:query/list/:page', function ($query, $page) use ($app) {
    require_once 'index_routes/review/review.php';  
	return getReviewsList($query, $page);
});
//Review Widget
$app->get('/getwidgetreviews/:query', function ($query) use ($app) {
    require_once 'index_routes/review/review.php';  
    return getWidgetReview($query);
});
// End Review Widget

$app->post('/reviewresponse', function () use ($app) {
    require_once 'index_routes/review/review.php';  
    return setReviewResponse($app);
});

$app->post('/review', function () use ($app) {   
    require_once 'index_routes/review/review.php';  
    return postReview($app);
});
$app->post('/review/grade', function () use ($app) {
    require_once 'index_routes/review/review.php';  
    return postReviewGrade($app);
});

$app->get('/getReviewList', 'api_authentificate', function () use ($app) {
    require_once 'index_routes/review/review.php';  
    return getUserPostedReviews();
});

$app->get('/menulist/:query', function ($query) use ($app) {
    require_once 'index_routes/food/food.php';  
	return getMenu($query);
});

$app->post('/menulist/:query', function ($query) use ($app) {
    require_once 'index_routes/food/food.php';  
	return getMenu($query);
});

$app->get('/cuisinelist', function () use ($app) {
   require_once 'index_routes/food/food.php';  
   return getCuisineList();
});

$app->get('/cuisinelist/:status', function ($status) use ($app) {
    require_once 'index_routes/food/food.php';  
	return getCuisineList($status);
});

$app->post('/addcuisine', function () use ($app) {
    require_once 'index_routes/food/food.php';  
    return addCuisine($app);
});

$app->post('/removecuisine', function () use ($app) {
    require_once 'index_routes/food/food.php';  
    return removeCuisine($app);
});

$app->post('/updatecuisine', function () use ($app) {
    require_once 'index_routes/food/food.php';  
    return updateCuisine($app);
});


/* inviteEmail */
$app->post('/invitemail/other', function() use ($app) {
    require_once 'index_routes/member/member.php';  
    $data = $app->request->post();
    return inviteOther($data['emails'], $data['bookid']);
});

$app->post('/send-contact-email', function () use ($app) {
    require_once 'index_routes/member/member.php';  
    return SendContactEmail($app);
});

$app->post('/send-contact-email2', function () use ($app) {
    require_once 'index_routes/member/member.php';  
    return SendContactEmail2($app);
});

$app->post('/send-website-contact-email', function () use ($app) {
    require_once 'index_routes/member/member.php';  
    return SendWebsiteContactEmail($app);
});

$app->get('/newsletter/addEmail/:email', function ($email) {
    require_once 'index_routes/member/member.php';  
    return NewsletterAddEmail($email);
});

$app->get('/newsletter/externalRestaurant/addEmail/:restaurant_id/:email', function ($restaurant_id, $email) {
    require_once 'index_routes/member/member.php';  
    return NewsletterExternalRestaurantAddEmail($restaurant_id, $email);
});

$app->post('/tracking/add', function () use ($app) {
    require_once 'index_routes/marketing/marketing.php';  
    $data = json_decode($app->request()->getBody(),true);
    return createMarketingCampaign($data);
});

$app->get('/marketing/type/:query', function ($query) {
    require_once 'index_routes/marketing/marketing.php';  
    return getMarketingTypes($query);
});

$app->get('/marketing/campaignlist', function () use ($app) {
     require_once 'index_routes/marketing/marketing.php';  
     return getCampaignList();
});


$app->get('/wheelvalue/alert', function () {
    $data = array('message' => 'The Higher the Score of the wheel, The Better the Rewards at the restaurant.');
    echo format_api(1, $data, 1, NULL);
});

$app->get('/updateProfile/:label/:value', 'api_authentificate', function ($label, $value) use ($app) {
    require_once 'index_routes/profile/profile.php';  
    return updateProfile($label, $value);
});

// problem
$app->post('/updateProfile/:email/:label/:value', function ($email, $label, $value) use ($app) {
    require_once 'index_routes/profile/profile.php';  
    $_SERVER['PHP_AUTH_USER']  =  $email ;
    return updateProfile($label, $value);
});

//API for datacheck
$app->group('/tool', function () use ($app) {
    require_once 'index_routes/tool/tool.php'; 
});
// API For DATA Check finish

// API for Project-MD - start
$app->group('/md_marketing', function () use ($app) {
    require_once 'index_routes/md_marketing/md_marketing.php';  
});
// API for Project-MD - finish

$app->get('/timestamp', 'getUpdateDate');

$app->post('/auditlog', function() use ($app) {
	return logEvent($app);
}); 
  
$app->get('/error', 'getErrorExample');

$app->get('/testencrypt', 'testEncryptExample');
$app->get('/testdecrypt', 'testDecryptExample');

$app->get('/report/reworld/widget/:page', 'getReworldReport');

$app->get('/readmlogintest', 'api_authentificate');
              
$app->get('/headerrequest', 'getheaderInfo');
 
$app->post('/payinvoice', function() use ($app) {
    try {
        $data1 = json_decode($app->request()->getBody(), true);
        $invoice = new WY_Invoice();
        $data = $invoice->invoicepayment($data1);
        echo format_api(200, $data, 1, NULL);
    } catch (Exception $e) {
        echo format_api(0, $e->getMessage(), 1, NULL);
    }
});

function is_test_env(){
        
        if (strpos($_SERVER['HTTP_HOST'], 'api') !== false) 
        	return false;        
        if (strpos($_SERVER['HTTP_HOST'], 'www') !== false) 
        	return false;        
        if (strpos($_SERVER['HTTP_HOST'], 'prod') !== false) 
        	return false;
        if (strpos($_SERVER['HTTP_HOST'], 'codersvn') !== false) 
        	return false;        
        return true;
    }


$app->run();

function getheaderInfo(){
     $sql = "select * FROM testrequest_header  ORDER BY id";
    try {
        $db = getConnection();
        $stmt = $db->query($sql);
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $db = null;
        $errors = null;
        echo format_api(1, $data, 1, $errors);
    } catch (PDOException $e) {
        api_error($e, "getlogourl");
    }

}


function api_error($e, $funcname) {

    $debug = new WY_debug;
    $trace = preg_replace("/\'\"/", "`", $e->getTraceAsString());
    $traceAr = $e->getTrace();
    $file = preg_replace("/^.*\//", "", $traceAr[0]['file']);
    $msg = $e->getMessage() . " - " . $file . " - " . $traceAr[0]['line'];
    $debug->writeDebug("ERROR-API", "API-" . strtoupper($funcname), $msg . " \n " . $trace);
    echo format_api(0, 0, 0, $funcname . "->" . $msg);
}

function getUpdateDate() {
    $errors = null;
    $data = array("timestamp" => "2014-09-01-23:15");
    echo format_api(1, $data, 1, $errors);

    //echo "{\"timestamp\":\"2014-09-01-23:15\"}";
}

function api_authentificate() {
    $response = array();
    if (!isset($_SERVER['PHP_AUTH_USER']) || !isset($_SERVER['PHP_AUTH_PW'])) {
        $response["error"] = true;
        $response["message"] = "Credentials are misssing";
        set_header_status(401);
        echo format_api(1, '', 0, $response);
        exit; //$app->stop();
    }

    $email = $_SERVER['PHP_AUTH_USER'];
    $pass = $_SERVER['PHP_AUTH_PW']; //pass ou token

    $log = new WY_Login(set_valid_login_type('backoffice'));
    if ($log->check_login_remote($email, $pass, 'api') != '') {
        return true;
    } else if ($log->process_login_remote($email, $pass, 'api') != '') {
        return true;
    } else {
        $response["error"] = true;
        $response["message"] = "wrong login and password";
        set_header_status(401);
        echo format_api(1, '', 0, $response);
        exit;
    }
    return false;
}

function logEvent($app){
    $action = $app->request->post('action');
    $event = $app->request->post('event');
    $page = $app->request->post('page');
    $logger = new WY_log("website");
    $userAgent = $app->request->getUserAgent();
    $member = new WY_Member();
    $user_id="";
    if(isset($_SESSION['user']['email'])){
        $user_id = $member->getIdByEmail($_SESSION['user']['email']);
    }
    //tracking logs
    if(isset($user_id)){
       $target =1;  // 1=>member,2=>guest,3=>invite

    }else{$target =2;}
   
        $logger->LogEvent($user_id, $action, 1, $page, $event, date("Y-m-d H:i:s"));
        echo format_api(1, $userAgent, 1, NULL);    
}

//// REPORTS ////

function getReworldReport($page) {
    $report = new WY_Report();
    $res = $report->getReworldReport($page);
    set_header_status(200);
    echo format_api(1, $res, 1, NULL);
}

////error format must be 
//$errors = array('type'=>'','message'=>'');
function getErrorExample() {
    echo format_api(0, null, 0, array('type' => 'error_login', 'message' => 'Login failed please try again later'));
}

function testDecryptExample() {
    $query = $_SERVER['REDIRECT_QUERY_STRING'];

    $coding = new WY_Coding;
    var_dump($coding->mydecode($query));
    //echo format_api(0, null, 0, array('type'=>'error_login','message'=>$_RESQUEST));
}

function testEncryptExample() {
    $query = $_SERVER['REDIRECT_QUERY_STRING'];

    $coding = new WY_Coding;
    var_dump($coding->myencode($query));
    //echo format_api(0, null, 0, array('type'=>'error_login','message'=>$_RESQUEST));
}

function format_api($status, $data, $count, $errors, $header_status = 200) {
    set_header_status($header_status);

    $resultat['status'] = $status;
    $resultat['data'] = $data;
    $resultat['count'] = $count;
    $resultat['errors'] = $errors;
  
    return json_encode($resultat);
}

//// later on  ////

function authenticate(\Slim\Route $route) {
    // Getting request headers
    $headers = apache_request_headers();
    $response = array();
    $app = Slim::getInstance();

    // Verifying Authorization Header
    if (isset($headers['Authorization'])) {
        $db = new DbHandler();

        // get the api key
        $api_key = $headers['Authorization'];
        // validating api key
        if (!$db->isValidApiKey($api_key)) {
            // api key is not present in users table
            $response["error"] = true;
            $response["message"] = "Access Denied. Invalid Api key";
            echoRespnse(401, $response);
            $app->stop();
        } else {
            global $user_id;
            // get user primary key id
            $user_id = $db->getUserId($api_key);
        }
    } else {
        // api key is missing in header
        $response["error"] = true;
        $response["message"] = "Api key is misssing";
        echoRespnse(400, $response);
        $app->stop();
    }
}

///// DEV and TEST ONLY
function is_restaurant_valid($restaurant) {
    return !empty($restaurant);
}

function is_mobile_valid($mobile) {
    return preg_match('/^(NA|[0-9+-]+)$/', str_replace(' ', '', $mobile));
}

function is_email_valid($email) {
    return !(filter_var($email, FILTER_VALIDATE_EMAIL) === false);
}

function set_header_status($code) {
    $status = array(
        100 => 'Continue',
        101 => 'Switching Protocols',
        200 => 'OK',
        201 => 'Created',
        202 => 'Accepted',
        203 => 'Non-Authoritative Information',
        204 => 'No Content',
        205 => 'Reset Content',
        206 => 'Partial Content',
        300 => 'Multiple Choices',
        301 => 'Moved Permanently',
        302 => 'Found',
        303 => 'See Other',
        304 => 'Not Modified',
        305 => 'Use Proxy',
        306 => '(Unused)',
        307 => 'Temporary Redirect',
        400 => 'Bad Request',
        401 => 'Unauthorized',
        402 => 'Payment Required',
        403 => 'Forbidden',
        404 => 'Not Found',
        405 => 'Method Not Allowed',
        406 => 'Not Acceptable',
        407 => 'Proxy Authentication Required',
        408 => 'Request Timeout',
        409 => 'Conflict',
        410 => 'Gone',
        411 => 'Length Required',
        412 => 'Precondition Failed',
        413 => 'Request Entity Too Large',
        414 => 'Request-URI Too Long',
        415 => 'Unsupported Media Type',
        416 => 'Requested Range Not Satisfiable',
        417 => 'Expectation Failed',
        500 => 'Internal Server Error',
        501 => 'Not Implemented',
        502 => 'Bad Gateway',
        503 => 'Service Unavailable',
        504 => 'Gateway Timeout',
        505 => 'HTTP Version Not Supported');

    header("HTTP/1.1 $code " . $status[$code]);
    return true;
}

?>
