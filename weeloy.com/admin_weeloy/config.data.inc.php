<?php

/// RESTAURANT
$itemLabel_crd_restaurant = array('theRestaurant', 'theState', 'navbar', 'saveSegment', 'itemmember', 'itemrestaurant', 'itemtitle', 'itemhotelname', 'itemaddress', 'itemaddress1', 'itemcity','itemcity_iso_code', 'itemzip', 'itemcountry','itemcountry_iso_code', 'itemtel', 'itememail', 'itemmap', 'itemdescription', 'itemstate', 'itemfax', 'itemlogo', 'itemurl',
    'itemaward', 'itemcreditcard', 'itemwheel', 'itemregion', 'itemmealtype', 'itempricing', 'itemopenhours', 'itemimages', 'itemchef', 'itemchef_award', 'itemchef_origin', 'itemchef_description', 'itemchef_believe', 'itemchef_logo', 'itemstars', 'itemcuisine',
    'itemmenus', 'itemrating', 'itemmealslot', 'itemdfmealtype', 'itemdfminpers', 'itemGPS', 'itembo_name', 'itembo_tel', 'itembo_email', 'itemmgr_name', 'itemmgr_tel', 'itemmgr_email', 'itemPOS', 'itemoldrestaurant', 'itemnewrestaurant', 'itemreactiverestaurant', 'itemdeactiverestaurant', 'itemdeleterestaurant');

$dbLabel_crd_restaurant = array('restaurant', 'title', 'hotelname');
$input_crd_restaurant = array(
    'Add Restaurant', '1', 'title',
    'theMember', 'theMember', 'hiddenvalue',
    'itemmember', 'theMember', 'hiddenvalue',
    'theState', '1addrestau', 'hidden',
    'Restaurant ID', 'itemrestaurant', 'hiddenvalue',
    'Restaurant Title', 'itemtitle', 'input',
    'City', 'itemcity', 'list',
    'itemcity_iso_code', 'itemcity_iso_code', 'hiddenvalue',
    'Country', 'itemcountry', 'list',
    'itemcountry_iso_code', 'itemcountry_iso_code', 'hiddenvalue',
    'Add restaurant', '1addrestaurant', 'submit',
    'Rename Restaurant', '2', 'title',
    'Old restaurant', 'itemoldrestaurant', 'listrestaurant',
    'New Name ', 'itemnewrestaurant', 'input',
    'Rename restaurant', '1renamerestaurant', 'submit',
    'Duplicate Restaurant', '3', 'title',
    'Old restaurant', 'itemoldrestaurantduplicate', 'listrestaurant',
    'New Name ', 'itemnewrestaurantduplicate', 'input',
    'Duplicate restaurant', '1duplicaterestaurant', 'submit',
    'Reactivate Restaurant', '4', 'title',
    'Restaurant', 'itemreactiverestaurant', 'listrestaurant',
    'Reactivate restaurant', '1reactiverestaurant', 'submit',
    'Deactivate Restaurant', '5', 'title',
    'Restaurant', 'itemdeactiverestaurant', 'listrestaurant',
    'Deactivate restaurant', '1deactiverestaurant', 'submit',
    'Delete Restaurant', '6', 'title',
    'Restaurant', 'itemdeleterestaurant', 'listrestaurant',
    'Delete restaurant', '1deleterestaurant', 'submit',
    'end', '0', 'title',
);




$itemLabel_del_restaurant = array('theRestaurant', 'theState', 'navbar', 'saveSegment', 'itemmember', 'itemrestaurant', 'itemtitle', 'itemhotelname', 'itemaddress', 'itemaddress1', 'itemcity', 'itemzip', 'itemcountry', 'itemtel', 'itememail', 'itemmap', 'itemdescription', 'itemstate', 'itemfax', 'itemlogo', 'itemurl', 'itememail','itemupdateemail', 'itemaws_email_verification',
    'itemaward', 'itemcreditcard', 'itemwheel', 'itemdeleteRestaurant', 'itemregion', 'itemmealtype', 'itempricing', 'itemopenhours', 'itemimages', 'itemchef', 'itemchef_award', 'itemchef_origin', 'itemchef_description', 'itemchef_believe', 'itemchef_logo', 'itemstars', 'itemcuisine',
    'itemmenus', 'itemrating', 'itemmealslot', 'itemdfmealtype', 'itemdfminpers', 'itemGPS', 'itembo_name', 'itembo_tel', 'itembo_email', 'itemmgr_name', 'itemmgr_tel', 'itemmgr_email', 'itemPOS');
$dbLabel_del_restaurant = array('restaurant', 'title', 'hotelname');
$input_del_restaurant = array('theState', 'deleteRestaurant', 'hidden');

$itemLabel_reserved_restaurant = array('theRestaurant', 'theState', 'navbar','itemrestaurant', 'itemtitle', 'itemreservedflag');
$dbLabel_reserved_restaurant = array('restaurant', 'title', 'reservedflag');
$input_reserved_restaurant = array(
    'Restaurant Information', '1', 'title', // titlein if need to open first
    'theState', '', 'hidden',
    'Credit Card Information Only', 'itemreservedflag', 'flagchecked(1)',// get Credit card information Only
    'Booking Deposit - activate deposit for bookings', 'itemreservedflag', 'flagchecked(2)',  // booking deposit
    'Paypal - Add paypal as payment method', 'itemreservedflag', 'flagchecked(3)',// default credit card
    'POS TESTING', 'itemreservedflag', 'flagchecked(4)',// TEMPORARY for testing
    'TMS', 'itemreservedflag', 'flagchecked(5)',// Tms reports
    'Call Center Member', 'itemreservedflag', 'flagchecked(6)', // sms header from  mobile number 
    '15 mn Booking', 'itemreservedflag', 'flagchecked(7)', // sms header from  mobile number 
    'TMS sync', 'itemreservedflag', 'flagchecked(8)', // tms sync
    '1 Month Booking', 'itemreservedflag', 'flagchecked(9)', // 1 month booking
    'Daily Special Board', 'itemreservedflag', 'flagchecked(10)', // Daily Special Board for existing customer
    'Restricted DSB', 'itemreservedflag', 'flagchecked(11)', // Daily Special Board for "B2C" customer
    'One Year Callcenter Booking Window', 'itemreservedflag', 'flagchecked(12)', // 1 year ahead
    'Callcenter Payment', 'itemreservedflag', 'flagchecked(13)', // callcenter payment booking
    'Modify Payment Booking', 'itemreservedflag', 'flagchecked(14)', // if guest allow modify the booking
    'Save', 'savespecialkeys', 'submit',
    'end', '0', 'title',
);


$itemLabel_upd_restaurant = array('theRestaurant', 'theState', 'navbar', 'saveSegment', 'itemmember', 'itemrestaurant', 'itemtitle', 'itemhotelname', 'itemaddress', 'itemaddress1', 'itemcity', 'itemzip', 'itemcountry', 'itemtel', 'itememail', 'itemmap', 'itemdescription', 'itemtags', 'itemstate', 'itemfax', 'itemlogo', 'itemurl', 'itememail','itemupdateemail','itemaws_email_verification',
    'itemaward', 'itemcreditcard', 'itemwheel', 'itemregion', 'itemmealtype', 'itempricing', 'itemopenhours', 'itemimages', 'itemchef', 'itemchef_award', 'itemchef_origin', 'itemchef_description', 'itemchef_believe', 'itemchef_logo', 'itemstars', 'itemcuisine',
    'itemmenus', 'itemrating', 'itemmealslot', 'itembookwindow', 'itemdfmealtype', 'itemdfminpers', 'itemGPS', 'itemstatus', 'itemdfproduct', 'itembo_name', 'itembo_tel', 'itembo_email', 'itemmgr_name', 'itemmgr_tel', 'itemmgr_email', 'itemPOS', 'itemis_displayed', 'itemis_wheelable', 'itemis_bookable', 'itemextraflag', 
    'itempartlist', 'itempartoffer', 'itempartdesc', 'itempartvalue', 'itempartorder', 'itemtrackdate', 'itemtracktype', 'itemtracknumer', 'itemtrackdesc', 'itemtrackvalue', 'itembkgcustomcolor','itembooking_deposit_lunch', 'itembooking_deposit_dinner','itemfacebook_pid', 'itemprinter'
);

$dbLabel_upd_restaurant = array('restaurant', 'title', 'hotelname', 'address', 'address1', 'city', 'zip', 'country', 'tel', 'email', 'map', 'offer_type', 'status', 'dfproduct', 'is_displayed', 'is_wheelable', 'is_bookable', 'extraflag', 'itemrestogeneric', 'description', 'tags','state', 'fax', 'logo', 'url', 'award', 'creditcard', 'wheel', 'region', 'mealtype', 'pricing', 'openhours', 'images', 'chef', 'chef_award', 'chef_origin', 'chef_description', 'chef_believe', 'chef_logo', 'stars', 'cuisine', 'menus', 'rating', 'mealslot', 'restogeneric', 'bookwindow', 'dfmealtype', 'dfminpers', 'GPS', 'bo_name', 'bo_tel', 'bo_email', 'mgr_name', 'mgr_tel', 'mgr_email', 'POS', 'bkgcustomcolor');
$input_upd_restaurant = array(
    'Restaurant Information', '1', 'title', // titlein if need to open first
    'theMember', 'theMember', 'hiddenvalue',
    'itemmember', 'theMember', 'hiddenvalue',
    'theState', '', 'hidden',
    'Restaurant Title', 'itemtitle', 'input',
    'Web Site', 'itemurl', 'input',
    'itememail', 'itememail', 'hiddenvalue', 
    'Email address', 'itemupdateemail', 'routine', 
    'AWS tmp', 'itemaws_email_verification', 'hiddenvalue', 
    'Desc', 'itemdescription', 'textarea',
    'Offer type', 'itemoffer_type', 'olist',
    'Tags', 'itemtags', 'textarea',
    'Number Slot', 'itemmealslot', 'list',
    'Generic', 'itemrestogeneric', 'textarea',// get Credit card information Only
    'Booking Window Website', 'itembookwindow','inputmultiple(1)',
    'Booking Window callcenter', 'itembookwindow','inputmultiple(2)',
    'Dinner starting Hour (24h)', 'itembookwindow','inputmultiple(3)',
    'Status', 'itemstatus', 'slist',
    'Multiple Allotement', 'itemdfproduct', 'input',
    'WL Background Color', 'itembkgcustomcolor', 'inputmultiple(1)',
    'WL Legacy-notused', 'itembkgcustomcolor', 'inputmultiple(2)',
    'WL Font Color', 'itembkgcustomcolor', 'inputmultiple(3)',
    'WL Button Color', 'itembkgcustomcolor', 'inputmultiple(4)',
    'Lunch Booking Deposit', 'itembooking_deposit_lunch', 'input',
    'Diner Booking Deposit', 'itembooking_deposit_dinner', 'input',
    'Facebook Page Id', 'itemfacebook_pid', 'input',
    'Displayed', 'itemis_displayed', 'checked',
    'Wheelable', 'itemis_wheelable', 'checked',
    'Bookable', 'itemis_bookable', 'checked',
    'Not Walkable', 'itemextraflag', 'flagchecked(1)', // flagchecked => 1,2,3,4,5 ... for value 1, 2, 4, 8, 16 ....
    'Sponsor Wheel', 'itemextraflag', 'flagchecked(2)',
    '24hour/7', 'itemextraflag', 'flagchecked(3)',
    'Booking per Pax', 'itemextraflag', 'flagchecked(4)',			// not per table
    'Call Center Extraction', 'itemextraflag', 'flagchecked(5)',	// includes booker
    '5 slot meal (need to set also for variable length)', 'itemextraflag', 'flagchecked(6)',	// 150 mn meal instead of 90 mn
    'with Promotion', 'itemextraflag', 'flagchecked(7)',	// promotion or not for listing restaurant
    'enable booking hours', 'itemextraflag', 'flagchecked(8)',		// use the minpax field for maxpax for booking engine
    
    'Take Out', 'itemextraflag', 'flagchecked(10)', //if set send sms to restaurant manager
    'Restaurant External website Link', 'itemextraflag', 'flagchecked(11)',
    'Corporate wheel', 'itemextraflag', 'flagchecked(12)',
    'Off-Peak wheel', 'itemextraflag', 'flagchecked(13)',
    'Promo Code Booking Website', 'itemextraflag', 'flagchecked(14)',
    'Opt-in Website', 'itemextraflag', 'flagchecked(15)',
    'Booking Hour by Slot', 'itemextraflag', 'flagchecked(16)',	// for Far East
    'Booking with Children', 'itemextraflag', 'flagchecked(17)',	// for Far East
    'Duplicate Booking Alert', 'itemextraflag', 'flagchecked(18)',

    'Grabz', 'itemextraflag', 'flagchecked(19)',  // grabz flag
    
    'Event Booking', 'itemextraflag', 'flagchecked(21)',  // event booking 
    'Custom Booking Window', 'itemextraflag', 'flagchecked(22)',  // advanced
    'WEBSITE Fields', 'itemextraflag', 'flagchecked(23)',  // website cms to avoid duplicate content
    '2-Setting', 'itemextraflag', 'flagchecked(24)', // 2-seeting
    
    'Daily Cutoff', 'itemextraflag', 'flagchecked(25)', //Daily Cutoff
    
    'SMS notification to restaurant', 'itemextraflag', 'flagchecked(9)', //if set send sms to restaurant manager
    'Email WhiteLabel -  send email to guest from restaurant email address', 'itemextraflag', 'flagchecked(26)', //Email White Label
    'TMS ', 'itemextraflag', 'flagchecked(36)', // Backoffice Booking life cycle activity log integration 
    'Tms Captain', 'itemextraflag', 'flagchecked(27)', // Tms enable captain feature

    'Booking Deposit - activate deposit for bookings', 'itemextraflag', 'flagchecked(20)',  // booking deposit
    'Paypal - Add paypal as payment method', 'itemextraflag', 'flagchecked(28)',// default credit card
    'Credit Card Information Only', 'itemextraflag', 'flagchecked(30)',// get Credit card information Only
    'Adyen - Add adyen as payment method', 'itemextraflag', 'flagchecked(40)',
    'Reddot - Add reddot as payment method', 'itemextraflag', 'flagchecked(41)',
    //'Booking Refund - allow restaurant to refund the guest on cancel', 'itemextraflag', 'flagchecked(29)',// booking refund
    'POS TESTING', 'itemextraflag', 'flagchecked(29)',// TEMPORARY for testing
    'Multiple Product Allotement', 'itemextraflag', 'flagchecked(31)', // BurnEnds
    
    'Blocked Guest List', 'itemextraflag', 'flagchecked(32)', // BurnEnds block list
    'Two Fact Authentication ', 'itemextraflag', 'flagchecked(33)', // google 2fa authentication
    'Sms Template ', 'itemextraflag', 'flagchecked(34)', // Backoffice sms integration 
    'Booking Activity Log ', 'itemextraflag', 'flagchecked(35)', // Backoffice Booking life cycle activity log integration 
    'Send sms from restaurant manager ', 'itemextraflag', 'flagchecked(37)', // sms header from  mobile number 
    'PABX', 'itemextraflag', 'flagchecked(38)', // sms header from  mobile number 
    'Payment Invoice ', 'itemextraflag', 'flagchecked(39)', // sms header from  mobile number 
   // reddot - Add adyen as payment method flag 41
    'TMS V2', 'itemextraflag', 'flagchecked(42)', // tms v2
    
    'Save changes', '1saverestaurant', 'submit',
    'Wheel Part Customization', '2', 'title',
    'Wheel Parts', 'itempartlist', 'list',
    'Select', 'customwheel', 'select',
    'Delete', 'deletepart', 'delete',
    'Offer', 'itempartoffer', 'input',
    'Description', 'itempartdesc', 'input',
    'Value', 'itempartvalue', 'input',
    'Order Display', 'itempartorder', 'input',
    'Save', 'savepart', 'submit',
    'Booking cancel policy', '3', 'title', 
    'Booking cancel policy', 'cancelpolicy', 'routine',
    'Section Configuration', '3', 'title', 
    'Section Information', 'sectionconfig', 'routine',
    'Payment method', '5', 'title', 
    'Payment method', 'mgtpayment', 'routine',
    'Payment  management', '5', 'title', 
    'Payment management', 'mgtconfigpayment', 'routine',

    'Printer IPAD', '4', 'title', 
    'Printer IP', 'itemprinter', 'input',
    'Save', 'saveprinter', 'submit',

    'end', '0', 'title',
);

$itemLabel_tracking = array('theRestaurant', 'itemrestaurant', 'theState', 'navbar', 'saveSegment', 'itemtrackdate', 'itemtracktype', 'itemtracknumer', 'itemtrackdesc', 'itemtrackvalue', 'itemtrackcampaign','itemtrackredirectto');
$dbLabel_tracking = array('restaurant');
$input_tracking = array(
    'Tracking Campaign', '1', 'titlein',
    'theState', '', 'hidden',
    'Type', 'itemtracktype', 'list',
    'Restaurant','itemrestaurant','listrestaurant',
    'Campaign Id', 'itemtrackcampaign', 'input',  
    'Creation Date (dd/mm/yyyy)', 'itemtrackdate', 'pdatepicker',
    'Description', 'itemtrackdesc', 'textarea',
    'Redirect to', 'itemtrackredirectto', 'input',
    'Generate Tracking Code', 'itemtrackgeneate', 'routine',
    
    'Save', '1savetracking', 'submit',
    'end', '0', 'title',
);


/// RESTAURANT
/// MEMBER

$itemLabel_add_member = array('theMember', 'theState', 'navbar', 'saveSegment', 'itemmember', 'itemfirstname', 'itemrestaurant', 'itemaddress', 'itemaddress1', 'itemcity', 'itemzip', 'itemcountry', 'itemtel', 'itememail', 'itemmap', 'itemdescription', 'itemstate', 'itemfax',
    'itemcreditcard', 'itemregion', 'itemmealtype', 'itemstars', 'itemcuisine', 'itemrating', 'itemmemberstatus');
$dbLabel_add_member = array('name', 'firstname', 'email', 'mobile');
$input_add_member = array(
    'Member Information', '1', 'titlein',
    'theState', '', 'hidden',
    'Type', 'itemmember_type', 'list',
    'Gender', 'itemgender', 'list',
    'Name', 'itemname', 'input',
    'First Name', 'itemfirstname', 'input',
    'Mobile', 'itemmobile', 'input',
    'Email', 'itememail', 'input',
    'Country', 'itemcountry', 'list',
    'Add member', '1addmember', 'submit',
    'end', '0', 'title',
);


$itemLabel_del_member = array('theMember', 'theState', 'navbar', 'saveSegment', 'itemmember', 'itemaddress', 'itemaddress1', 'itemcity', 'itemzip', 'itemcountry', 'itemtel', 'itememail', 'itemmap', 'itemdescription', 'itemstate', 'itemfax',
    'itemcreditcard', 'itemregion', 'itemmealtype', 'itemstars', 'itemcuisine', 'itemrating', 'itemmemberstatus');
$dbLabel_del_member = array('name', 'firstname', 'email', 'mobile');
$input_del_member = array(
    'Member Delete', '1', 'titlein',
    'theState', '', 'hidden',
    'Type', 'itemmember_type', 'readonly',
    'Gender', 'itemgender', 'readonly',
    'Name', 'itemname', 'readonly',
    'First Name', 'itemfirstname', 'readonly',
    'Mobile', 'itemmobile', 'readonly',
    'Email', 'itememail', 'readonly',
    'Country', 'itemcountry', 'readonly',
    'Delete|itememail', 'delmember', 'delete',
    'end', '0', 'title',
);



$itemLabel_upd_member = array('theMember', 'theState', 'navbar', 'saveSegment', 'itemmember', 'itemaddress', 'itemaddress1', 'itemcity', 'itemzip', 'itemcountry', 'itemtel', 'itememail', 'itemmap', 'itemdescription', 'itemstate', 'itemfax',
    'itemcreditcard', 'itemregion', 'itemmealtype', 'itemstars', 'itemcuisine', 'itemrating', 'itemmemberstatus');
$dbLabel_upd_member = array('member_type', 'name', 'firstname', 'email', 'mobile', 'country', 'gender');
$input_upd_member = array(
    'Member Update', '1', 'titlein',
    'theState', '1savemember', 'hidden',
    'Type', 'itemmember_type', 'list',
    'Gender', 'itemgender', 'list',
    'Name', 'itemname', 'input',
    'First Name', 'itemfirstname', 'input',
    'Mobile', 'itemmobile', 'input',
    'Email', 'itememail', 'readonly',
    'Country', 'itemcountry', 'list',
    'Update member', '1savemember', 'submit',
    'end', '0', 'title',
);

// MEMBER

$itemLabel_upd_cluster = array('theCluster', 'theState', 'navbar', 'saveSegment', 'itemclustersearch', 'itemclustname', 'itemclusttype', 'itemclustcategory', 'itemclustparent', 'itemclustcontent', 
	'itemclsname', 'itemclstype', 'itemclscategory', 'itemclsparent', 'itemclscontent');
$dbLabel_upd_cluster = array('name', 'type', 'category', 'parent', 'content');
$input_upd_cluster = array(
    'Message', '', 'message',
    'Cluster Master', 'itemclustersearch', 'search',
    'Cluster Create', '1', 'title',
    'theState', '', 'hidden',
    'Name', 'itemclsname', 'input',
    'Type', 'itemclstype', 'list',
    'Category', 'itemclscategory', 'input',
    'Parent', 'itemclsparent', 'list',
    'Content', 'itemclscontent', 'textarea',
    'Create Cluster', 'savecluster', 'submit',
    'Master Cluster Delete', '2', 'title',
    'Delete|itemclustersearch', 'deletemastercluster', 'delete',
    'Slave Cluster Delete', '', 'title',
    'Delete|itemclustersearch_slave', 'deleteslavecluster', 'delete',
    'Cluster Update', '3', 'title',
    'Name', 'itemclustname', 'input',
    'Type', 'itemclusttype', 'readonly',
    'Category', 'itemclustcategory', 'list',
    'Parent', 'itemclustparent', 'list',
    'Content', 'itemclustcontent', 'textarea',
    'Update Cluster', 'updatecluster', 'submit',
    'Seach sub-cluster', '4', 'title', 
    'Search email', 'clustersubemail', 'routine',
    'end', '0', 'title',
);
    $itemLabel_notification = array('theRestaurant', 'navbar', 'saveSegment', 'itemmember', 'itemrestaurant', 'itemlogo');
	$input_notification = array(
	'List', '1', 'title', 
	'theMember', 'theMember', 'hiddenvalue',
	'itemmember', 'theMember', 'hiddenvalue',
	'theState', '', 'hidden',
	'Stage', 'Load', 'hidden',
	'Notiifcation Management', 'notification_management', 'routine',
	
	'end', '0', 'title'
	);
$itemLabel_video = array('theRestaurant', 'navbar', 'saveSegment', 'itemmember', 'itemrestaurant', 'itemlogo');
$input_video_restaurant = array(
    'Video Management', '1', 'title', // titlein if need to open first
    'theState', '', 'hidden',
    'video Management', 'video_management', 'routine',
    'end', '0', 'title',
);

// MEMBER
?>