<?php

require_once("conf/conf.init.inc.php");
require_once("lib/wpdo.inc.php");
require_once("conf/conf.session.inc.php");
require_once("lib/gblcookie.inc.php");
require_once("lib/class.login.inc.php");
require_once("lib/class.coding.inc.php");
require_once("lib/class.media.inc.php");
require_once("lib/class.restaurant.inc.php");
require_once("lib/class.member.inc.php");
require_once("lib/class.cluster.inc.php");

require_once("../inc/utilities.inc.php");
require_once("lib/class.analytics.inc.php");
require_once("lib/wglobals.inc.php");

require_once("lib/class.login.inc.php");
require_once("lib/class.images.inc.php");
require_once("lib/class.wheel.inc.php");
require_once("lib/class.menu.inc.php");
require_once("lib/class.review.inc.php");
require_once("lib/class.event.inc.php");
require_once("lib/class.restaurant.inc.php");
require_once("lib/class.member.inc.php");
require_once("lib/class.stats.inc.php");
require_once("lib/class.analytics.inc.php");
require_once("lib/class.debug.inc.php");
require_once("lib/class.translation.inc.php");
require_once("lib/class.service.inc.php");
require_once("lib/class.mail.inc.php");
require_once("lib/class.booking.inc.php");

$vst = new WY_log("admin_weeloy");

mng_RestoCookie('theRestaurant');
mng_MemberCookie('theMember');
mng_CuisineCookie('theCuisine');
mng_emailmnCookie('emailMangement');
$inputAr = $itemLabel = $dbLabel = array();
$globalMessage = "";
$globalMessageType = "info";
$globalScript = "";
$globalScriptOnload = "";

$_SESSION['message'] = $_SESSION['message-error'] = NULL;

$navbar = (isset($_REQUEST['navbar'])) ? $_REQUEST['navbar'] : "";
//$navbarAr = array("RESTAURANT", "MEMBER", "CLUSTER", "CUISINE", "AREA");

$navbarAr = array();

$login_type = set_valid_login_type("admin");
$login = new WY_Login($login_type);
$login->CheckLoginStatus();

// to be used for backoffice ajax

$login_status = _LOG_IN_;
if ($login->result < 0) {
    $login_status = _LOG_OUT_;
    $navbar = "HOME";
    $navbarAr = array();
    $globalScript .= "var logstatus='out';";
    unset($_SESSION['user_backoffice']['member_type']);
    unset($_SESSION['user_backoffice']['id']);
}
else {
	if($login->extrafield != "admin")
		$login->process_login_remote_setapp("admin", $login->email, $login->token);
	$globalScript .= "var logstatus='in';";
	}

$_SESSION['user_backoffice']['token'] = $login->signIn['token'];
$_SESSION['user_backoffice']['email'] = $login->signIn['email'];
$_SESSION['user_backoffice']['id'] = $login->signIn['user_id'];
$_SESSION['user_backoffice']['member_permission'] = intval($login->signIn['member_permission']);
$_SESSION['user_backoffice']['member_type'] = $login->signIn['member_type'];
$_SESSION['user']['member_type'] = $_SESSION['user_backoffice']['member_type']; // if navigating from front-end

$accountemail = $_SESSION['user_backoffice']['email'];
$accounttype = $_SESSION['user_backoffice']['member_type'];
$authorized = in_array($accountemail, array("richard@kefs.me", "richard.kefs@weeloy.com", "soraya.kefs@weeloy.com","vs.kala@weeloy.com", "khteh@weeloy.com","amin.mahetar@weeloy.com","christian.aromin@weeloy.com"));
$support_team = in_array($accountemail, array("gultiga.lueskul@weeloy.com", "hody.leong@weeloy.com"));

if ($authorized) {
    $navbarAr[count($navbarAr) + 1] = 'UPD_RESERVEDFLAG';
}

/////  check user right to display the page ////
/////  ONLY SUPER_WEELOY and ADMIN  ////

$memdata = new WY_Member();
$memdata->getMember($accountemail);
$userRole = $memdata->getUserRole($accountemail);

 if(!empty($userRole) && isset($userRole) && isset($accounttype)) {
    if ($authorized && $userRole==="")
	 	$userRole ='super';
	 if($userRole ==="" || $userRole ===null) {
		$navbar = "HOME";
		$navbarAr = array();
		$globalScript .= "var logstatus='out';";
		$login->logout(null);
		$login_status = _LOG_OUT_;
		$_SESSION['message'] = $_SESSION['message-error'] = "YOU DO NOT HAVE THE RIGHT PERMISSIONS";
		echo "YOU DO NOT HAVE THE RIGHT PERMISSIONS";
		exit;
		}
 }

//if (isset($accounttype) && !in_array($accounttype, $super_member_type_allowed)) {
//    $navbar = "HOME";
//    $navbarAr = array();
//    $globalScript .= "var logstatus='out';";
//    $login->logout(null);
//    $login_status = _LOG_OUT_;
//    $_SESSION['message'] = $_SESSION['message-error'] = "YOU DO NOT HAVE THE RIGHT PERMISSIONS";
//    echo "YOU DO NOT HAVE THE RIGHT PERMISSIONS";
//    exit;
//}

$restrictedCountry = "";

//user acl permission
$permission = $memdata->getUserPermission($accountemail);
$accessLevel = explode('||',$permission);

for($i=0;$i<count($accessLevel);$i++){
    if(!in_array($accessLevel[$i],$navbarAr))
        $navbarAr[count($navbarAr) + 1] = $accessLevel[$i];
   }

if($support_team ||  $authorized){
    $navbarAr[count($navbarAr) + 1] = 'VIDEOLINK';
    $navbarAr[] = 'DSB Restaurant';
    //$navbarAr[count($navbarAr) + 1] = 'SMSTRACKING';
}
    

if ($authorized) {
    $navbarAr[] = 'CRAWLER';;
    $navbarAr[] = 'PAYMENT';
    $navbarAr[] = 'TOOLS';
    $navbarAr[] = 'MailChimp';
    $navbarAr[] = 'USER ACL';
} else {
   
    if(isset($userRole) && !empty($userRole) && $userRole == "marketing_manager")
        $navbarAr[] = 'MailChimp';
}

if ($memdata->is_restrictedCountry($accountemail))
    $restrictedCountry = $memdata->country;

if (!empty($restrictedCountry)) {
    $navbarAr = array("RESTAURANT");
}

$_SESSION['message'] = $_SESSION['message-error'] = NULL;

if (empty($theRestaurant))
    $theRestaurant = "";


require_once("config.data.inc.php");

if (empty($navbar))
    $navbar = 'HOME';

switch ($navbar) {

    case 'HOME':
    case 'LOG':
        break;

    case 'RESTAURANT':
        $inputAr = $input_restaurant;
        $itemLabel = $itemLabel_restaurant;
        $dbLabel = $dbLabel_restaurant;
        break;

    case 'CRD_RESTAU':
        $inputAr = $input_crd_restaurant;
        $itemLabel = $itemLabel_crd_restaurant;
        $dbLabel = $dbLabel_crd_restaurant;
        break;

    case 'UPD_RESTAU':
        $inputAr = $input_upd_restaurant;
        $itemLabel = $itemLabel_upd_restaurant;
        $dbLabel = $dbLabel_upd_restaurant;
        break;

    case 'UPD_RESERVED_FLAG':
    case 'UPD_RESERVEDFLAG':
        $inputAr = $input_reserved_restaurant;
        $itemLabel = $itemLabel_reserved_restaurant;
        $dbLabel = $dbLabel_reserved_restaurant;
       break;

    case 'TRACKING_RESTAU':
        $inputAr = $input_tracking;
        $itemLabel = $itemLabel_tracking;
        $dbLabel = $dbLabel_tracking;
        break;

    case 'MEMBER':
        $inputAr = $input_member;
        $itemLabel = $itemLabel_member;
        $dbLabel = $dbLabel_member;
        break;

    case 'ADD_MEMBER':
        $inputAr = $input_add_member;
        $itemLabel = $itemLabel_add_member;
        $dbLabel = $dbLabel_add_member;
        break;

    case 'UPD_MEMBER':
        $inputAr = $input_upd_member;
        $itemLabel = $itemLabel_upd_member;
        $dbLabel = $dbLabel_upd_member;
        break;

    case 'DEL_MEMBER':
        $inputAr = $input_del_member;
        $itemLabel = $itemLabel_del_member;
        $dbLabel = $dbLabel_del_member;
        break;

    case 'DEFINE_RESTAURANT_LIST':
        $inputAr = $input_del_member;
        $itemLabel = $itemLabel_del_member;
        $dbLabel = $dbLabel_del_member;
        break;

    case 'CRUD_CLUSTER':
        $inputAr = $input_upd_cluster;
        $itemLabel = $itemLabel_upd_cluster;
        $dbLabel = $dbLabel_upd_cluster;
        break;

    case 'NOTIFICATION_MANAGEMENT':
        $inputAr = $input_notification;
        $itemLabel = $itemLabel_notification;
        $dbLabel = $dbLabel_notification;
        break;
    case 'VIDEOLINK':
        $inputAr = $input_video_restaurant;
        $itemLabel = $itemLabel_video ;
        $dbLabel = $dbLabel_video;
         break;
    
    case 'UPD_CUISINE':
    case 'RMD_CUISINE':
    case 'ADD_CUISINE':
    case 'CUISINE':
    case 'AREA':
    case 'ADD_AREA':
    case 'UPD_AREA':
    case 'DEL_AREA':
    case 'CAMPAIGN':
    case 'ADD_TRACKING':
    case 'LIST_TRACKING':

    case 'ADD_VIDEOLINK':
    case 'UPDATE_VIDEOLINK':
    case 'UPD_VIDEOLINK':
    case 'EMAIL MANAGEMENT':
    case 'ED_BKCONFIRMATION':
    case 'ED_REGISTER':
    case 'ED_BOOKING':
    case 'ED_BKREQUEST':
    case 'ED_BKCONFIRLIST':
    case 'ED_RBKCONFIRMATION':
    case 'ED_BKREMINDER':
    case 'ED_BKREMINDERREQUEST':
    case 'ED_BKREMINDERLISTING':
    case 'ED_RBKREQUEST':
    case 'ED_WHELLWIN':
    case 'ED_RBKCONFIRLIST':
    case 'ED_BKWHELLWIN':
    case 'ED_RKWHELLWIN':
    case 'ED_CALLCENTER':
    case 'ED_BKCALLCENTER':
    case 'ED_RKCALLCENTER':
    case 'ED_REVIEW':
    case 'ED_NOSHOW':
    case 'ED_BKREVIEW':
    case 'ED_BKREVIEWREMINDER':
    case 'ED_BKUPDATE':
    case 'ED_RKUPDATE':
    case 'ED_RKCAllUPDATE':
    case 'ED_BKCAllUPDATE':
    case 'ED_RKREVIEWREMINDER':
    case 'ED_RKREVIEW':
    case 'ED_WEREGISTER':
    case 'ED_CRMEMBER':
    case 'ED_CRRESTRAURANT':
    case 'ED_CRREMINDER':
    case 'ED_CRRKREMINDER':
    case 'ED_BKCANCEL':
    case 'ED_CATERING':
    case 'ED_RKCANCEL':
    case 'ED_BKNOSHOW':
    case 'ED_BKWLNOSHOW':
    case 'ED_BKSMS':
    case 'ED_WEREGISTER':  
    case 'PUSH_NOTIFICATION':
    case 'NOTIFICATION':
    case 'DATAEXTRACT':
    case 'CATEGORIES':
    case 'ADD_CATEGORIES':
    case 'FOOTER_CATEGORIES':
    case 'EVENT_CATEGORIES':
    case 'GET_DATA':
    case 'CREATE_DATA':
    case 'USER ACL':
    case 'USER_ROLE':
    case 'CRAWLER':
    case 'CRAWLER_CRWLACCOUNT':
    case 'SMS_RTRACKING':
    case 'MEDIA_CATEGORIES':
    case 'TAG MANAGEMENT':
    case 'TAG_MANAGEMENT':
    case 'NOTIFICATION_ACTIONCREATION':
    case 'FEATURED_RESTAURANT':
    case 'ED_S3TRACKINGEMAIL' :
    case 'CLUSTER':
    case 'ASSIGN_MEMBER_RESTAURANTS_LIST':
    case 'ASSIGN_MEMBER_APPMANAGER_RESTAURANTS_LIST':
    case 'MEMBERLIST':
    case 'PAYMENT':
    case 'TOOLS':
    case 'PAYMENT_CREDENTIALS':
    case 'TOOLS_DATACHECK':
    case 'DSB Restaurant':
    case 'DSB_MEMBERLIST':
    case 'MailChimp':
    case 'MailChimp_Register':
        break;
    default:
        error_log("ADMIN unknown case" . $navbar);
        $navbar = 'HOME';
        break;
}
// with $validArg and include, the variable come to live	
$validArg = $itemLabel;
if(isset($validArg) && count($validArg) > 0 && is_array($validArg))
	while(list($index, $label) = each($validArg)) {
		if(isset($_REQUEST[$label]))
			$$label = clean_input($_REQUEST[$label]);
		}

$resdata = new WY_restaurant();
if (!empty($theRestaurant))
    if ($resdata->checkRestaurant($theRestaurant) == false) {
        mng_ClearRestoCookie('theRestaurant');
        $theRestaurant = "";
    }

$section = explode('_', $navbar);
$extradataobject = array();

$tmp = new WY_restaurant();
if($navbar == 'UPD_RESERVED_FLAG' && !empty($_POST['theState']) && $_POST['theState'] == "savespecialkeys") {
	$theRestaurantTitle = $theRestaurant;
	for( $i  = 1, $reservedflag = 0; $i < 50 && isset($_POST['itemreservedflag' . $i]); $i++) {
		if($_POST['itemreservedflag' . $i] == 1)
			$reservedflag += (1 << ($i - 1));
		}
	 $tmp->savereservedflag($theRestaurant, $reservedflag);
    	  }

if($navbar == 'UPD_RESERVED_FLAG' || $navbar == 'UPD_RESERVEDFLAG') {
        $data = $tmp->getRestaurant($theRestaurant);
        $dataobject = getDataObject($data);
 	}
if($navbar == 'UPD_VIDEOLINK') {
        $data = $tmp->getRestaurant($theRestaurant);
        $dataobject = getDataObject($data);
}

if (isset($section[1]) && $section[1] == 'RESTAU') {

    $theRestaurantTitle = $theRestaurant;
    if (count($inputAr) > 0 && $navbar != 'CRD_RESTAU') {
        // process data => save hotel, upload image, create menus...

        if (!empty($theRestaurant)) {
            process_data($inputAr, $navbar, $dbLabel, $theState, $theRestaurant, $theMember);
        }

        if (empty($theRestaurant)) {
            $resdata->getDefaultRestaurant($accountemail, $accounttype);
            $theRestaurant = $resdata->restaurant;
        }

        $imgdata = new WY_Images($theRestaurant);

        $itemrestaurant = $theRestaurant;
        $tmp = new WY_restaurant();
        $data = $tmp->getRestaurant($theRestaurant);
        $data["printer"] = $tmp->readGeneric("IPADPrinter");
        $dataobject = getDataObject($data);
        $dataobject['theRestaurant'] = $theRestaurant;

        $theRestaurantTitle = $dataobject['title'];
    } else if (count($inputAr) > 0 && $navbar == 'CRD_RESTAU' && !empty($theState))
        process_data($inputAr, $navbar, $dbLabel, $theState, $theRestaurant, "");

    else if (count($inputAr) > 0 && $navbar == 'TRACKING_RESTAU' && !empty($theState))
        process_data($inputAr, $navbar, $dbLabel, $theState, $theRestaurant, "");
}


if (isset($section[1]) && $section[1] == 'MEMBER') {
    if (count($inputAr) > 0 && $navbar != 'ADD_MEMBER') {
        if (!empty($theMember)) {
            process_data($inputAr, $navbar, $dbLabel, $theState, $theRestaurant, $theMember);
        }
        //$memdata = new WY_member();
        if (empty($theMember)) {
            //$memedata->getDefaultMember();
            $theMember = 'philippe.benedetti@weeloy.com';
        }

        $itemmember = $theMember;
        $tmp = new WY_member;
        $data = $tmp->getMember($theMember);
        $dataobject = getDataObject($data);
        $dataobject['theMember'] = $theMember;

        // $dataobject is a global variable
        $theMemberEmail = $dataobject['email'];
    } else if (count($inputAr) > 0 && $navbar == 'ADD_MEMBER' && !empty($theState))
        process_data($inputAr, $navbar, $dbLabel, $theState, $theRestaurant, $theMember);
}

if (isset($section[1]) && $section[1] == 'CLUSTER') {

    // first set theCluster to a child of a Class/Master if set

    $tmp = new WY_Cluster;
    $data = array();

    if ((!empty($itemclustname) || !empty($_POST['itemclsname'])) && count($inputAr) > 0 && !empty($theState))
        process_data($inputAr, $navbar, $dbLabel, $theState, $itemclustname, $itemclustname);

    if (!empty($_POST['itemclustersearch_slave']) && !empty($_POST['itemclustersearch']) && !empty($_POST['itemclustersearch_category']))
        $data = $tmp->getCluster("SLAVE", $_POST['itemclustersearch_slave'], $_POST['itemclustersearch_category'], $_POST['itemclustersearch']);

    if (count($data) < 1)
        if (!empty($_POST['itemclustersearch']) && !empty($_POST['itemclustersearch_category']))
            $data = $tmp->getCluster("MASTER", $_POST['itemclustersearch'], $_POST['itemclustersearch_category'], '');

    if (count($data) > 0) {
        $itemclustname = $theCluster = $data['clustname'];
        $_POST['itemclustersearch_category'] = $data['clustcategory'];  // can have the same name and content, but different category
        $dataobject = getDataObject($data);
        $dataobject['theCluster'] = $theCluster;
    }
}

foreach ($extradataobject as $key => $value)
    $dataobject[$key] = $value;

// end of global code


function getDataObject($data) {

    foreach ($_POST as $key => $value)
        if (substr($key, 0, 4) == "item")
            $dataobject[$key] = $value;

    foreach ($data as $key => $value)
        if (!is_numeric($key))
            $dataobject["item" . $key] = $dataobject[$key] = $value;

    return $dataobject;
}

function process_data(&$inputAr, $navbar, $dbLabel, $theState, $theRestaurant, $theMember) {
    global $globalMessage, $globalMessageType, $extradataobject;


    if ($inputAr == "")
        return;

    $resdata = new WY_restaurant();

    if ($navbar == "CRD_RESTAU") {

        if ($theState == "1addrestaurant") {
            if (!$_REQUEST['itemtitle'] || !$_REQUEST['itemcity'] || !$_REQUEST['itemcountry']) {
                $_SESSION['info_message']['type'] = 'danger';
                $_SESSION['info_message']['message'] = 'Please fill all fields';
                return;
            }
            createRestaurant($inputAr, substr($theState, 0, 1), $theRestaurant, $dbLabel);
            $_SESSION['message'] = 'Restaurant inserted';
        } else if ($theState == "1renamerestaurant") {
            $itemoldrestaurant = $_REQUEST['itemoldrestaurant'];
            $itemnewrestaurant = $_REQUEST['itemnewrestaurant'];
            $status = $resdata->rename($itemoldrestaurant, $itemnewrestaurant);
            if ($status > 0){
                $action=202;
                $auditlog= logEvent($_SESSION['user_backoffice']['id'],$action,$theState,$itemnewrestaurant);
                $_SESSION['message'] = 'Restaurant ' . $itemoldrestaurant . "renamed to " . $itemnewrestaurant;
            }
            else{
                $_SESSION['message'] = 'Restaurant ' . $itemoldrestaurant . "renamed to " . $itemnewrestaurant . "has FAILED ($status)";
            }
        }

        else if ($theState == "1duplicaterestaurant") {
            $itemoldrestaurant = $_REQUEST['itemoldrestaurantduplicate'];
            $itemnewrestaurant = $_REQUEST['itemnewrestaurantduplicate'];
            $status = $resdata->duplicate($itemoldrestaurant, $itemnewrestaurant);
            if ($status > 0){
                 $action=203;
                 $auditlog= logEvent($_SESSION['user_backoffice']['id'],$action,$theState,$itemnewrestaurant);
                
                $_SESSION['message'] = 'Restaurant ' . $itemnewrestaurant . ' has been duplicated';
            }
            else{
                $_SESSION['message'] = 'Restaurant ' . $itemnewrestaurant . ' has NOT been duplicated: ' . $resdata->msg;
            }
        }

        else if ($theState == "1reactiverestaurant") {
            $itemreactiverestaurant = $_REQUEST['itemreactiverestaurant'];
            $resdata->reactivate($itemreactiverestaurant);
            if ($status > 0){
                 $action=204;
                 $auditlog= logEvent($_SESSION['user_backoffice']['id'],$action,$theState,$itemreactiverestaurant);
                $_SESSION['message'] = 'Restaurant ' . $itemreactiverestaurant . ' has been reactivated';
            }
            else{
                $_SESSION['message'] = 'Restaurant ' . $itemreactiverestaurant . ' has NOT been reactivated: ' . $resdata->msg;
            }
        }

        else if ($theState == "1deactiverestaurant") {
            $itemdeactiverestaurant = $_REQUEST['itemdeactiverestaurant'];
            $status = $resdata->deactivate($itemdeactiverestaurant);
            if ($status > 0){
                  $action=205;
                  $auditlog= logEvent($_SESSION['user_backoffice']['id'],$action,$theState,$itemdeactiverestaurant);
                $_SESSION['message'] = 'Restaurant ' . $itemdeactiverestaurant . ' has been deactivated';
            }
            else{
                $_SESSION['message'] = 'Restaurant ' . $itemdeactiverestaurant . ' has NOT been deactivated: ' . $resdata->msg;
             }
        }

        else if ($theState == "1deleterestaurant") {
            $itemdeleterestaurant = $_REQUEST['itemdeleterestaurant'];
            $status = $resdata->delete($itemdeleterestaurant);
            if ($status > 0){
                    $action=206;
                  $auditlog= logEvent($_SESSION['user_backoffice']['id'],$action,$theState,$itemdeleterestaurant);
                $_SESSION['message'] = 'Restaurant ' . $itemdeleterestaurant . ' has been deleted';
            }
            else{
                $_SESSION['message'] = 'Restaurant ' . $itemdeleterestaurant . ' has NOT been deleted: ' . $resdata->msg;
            }
        }

        return;
    }

    if ($navbar == "UPD_RESTAU") {

    //print_r($inputAr);
    
        if (substr($theState, 1) == "saverestaurant") {
            updateRestaurant($inputAr, substr($theState, 0, 1), $theRestaurant, $dbLabel);
            $_SESSION['message'] = 'Changes saved';
        } else if ($theState == "savepart") {
            $labelvalue = array('itempartoffer', 'itempartdesc', 'itempartvalue', 'itempartorder');
            foreach ($labelvalue as $value)
                $$value = $_REQUEST[$value];

            if (!empty($itempartoffer)) {
                $wheeldata = new WY_wheel($theRestaurant, '', '');
                $wheeldata->savepart($itempartoffer, $itempartdesc, $itempartvalue, $itempartorder);
                $_SESSION['message'] = 'Changes saved ' . $theRestaurant;
            }
        }
        else if($theState == "saveprinter") {
			$resto = new WY_restaurant($theRestaurant);
			$resto->updateGeneric($theRestaurant, "IPADPrinter", $_REQUEST['itemprinter']);        	
        	}
        if ($theState == "customwheel") {
            $itempartlist = $_REQUEST['itempartlist'];

            if (!empty($itempartlist)) {
                $wheeldata = new WY_wheel($theRestaurant, '', '');
                $data = $wheeldata->extractpart($itempartlist);
                if (count($data) > 0) {
                    $extradataobject["itempartoffer"] = $data["offer"];
                    $extradataobject["itempartlist"] = $data["offer"];
                    $extradataobject["itempartdesc"] = $data["description"];
                    $extradataobject["itempartvalue"] = $data["value"];
                    $extradataobject["itempartorder"] = $data["morder"];
                    for ($i = 0; $i < count($inputAr); $i++)
                        if ($inputAr[$i] == "Wheel Part Customization") {
                            $inputAr[$i + 2] = "titlein";
                            break;
                        }
                }
            }
        }

        if ($theState == "deletepart") {
            $itempartlist = $_REQUEST['itempartlist'];
            // error_log("WHEEL DELETE : $itempartlist");

            if (!empty($itempartlist)) {
                $wheeldata = new WY_wheel($theRestaurant, '', '');
                $wheeldata->deletepart($itempartlist);
                $_SESSION['message'] = 'Changes saved';
            }
        }

        return;
    }


    if ($navbar == "TRACKING_RESTAU") {
        if (substr($theState, 1) == "savetracking") {
            $labelvalue = array('itemtracktype', 'itemrestaurant', 'itemtrackcampaign', 'itemtrackdate', 'itemtrackdesc', 'itemtrackredirectto', 'itemtrackgeneate');
            foreach ($labelvalue as $value)
                $$value = $_REQUEST[$value];

            if (!empty($itemtrackvalue) && strlen($itemtrackvalue) > 10) {
                $res = createCampaign($itemtracktype, $itemrestaurant, $itemtrackcampaign, $itemtrackdate,  $itemtrackredirectto, $itemtrackgeneate);

                $_SESSION['info_message']['type'] = '';
                $_SESSION['info_message']['message'] = 'Tracking created';
            }
        }
        return;
    }

    if ($navbar == "DEL_MEMBER" && $_REQUEST['theMember'] != "") {
        if ($theState == "delmember") {
            $memdata = new WY_Member;
            $memdata->deleteMember($_REQUEST['itememail']);
            if($memdata->result > 0) {
            	$info = array('itemmember_type', 'itemgender', 'itemfirstname', 'itemname', 'itemmobile', 'itemcountry', 'itememail');
            	foreach($info as $label) $_POST[$label] = ""; 
	            $_SESSION['message'] = 'Member deleted';
	            }
	        else $_SESSION['message'] = 'Member NOT deleted, as email is empty';
        }
        return;
    }

    if ($navbar == "ADD_MEMBER") {
        if (substr($theState, 1) == "addmember") {
            $memdata = new WY_Member;
            $info = array('itemmember_type', 'itemgender', 'itemfirstname', 'itemname', 'itemmobile', 'itemcountry', 'itememail');
            foreach($info as $label) {
            	$_POST[$label] = trim($_POST[$label]); 
            	if(empty($_POST[$label])) {
            		$ll = preg_replace("/^item/", "", $label);
					$_SESSION['info_message']['type'] = 'danger';
					$_SESSION['info_message']['message'] = "Unable to create, as $ll is empty";
					return;
            		}        
            	}
            $res = $memdata->partialCreateMember($_POST['itemmember_type'], $_POST['itemgender'], $_POST['itemfirstname'], $_POST['itemname'], $_POST['itemmobile'], $_POST['itemcountry'], $_POST['itememail']);
            if ($res > 0) {
                  $action=210;
                  $auditlog= logEvent($_SESSION['user_backoffice']['id'],$action,$navbar,$_POST['itememail']);
                $_SESSION['info_message']['type'] = 'info';
                $_SESSION['info_message']['message'] = 'Member inserted';
            } else {
                $_SESSION['info_message']['type'] = 'danger';
                $_SESSION['info_message']['message'] = 'Unable to create, please update member or use different email address';
                if (isset($res['error'])) {
                    $_SESSION['info_message']['message'] = 'Unable to create, ' . $_POST['itememail'];
                }
            }
        }
        return;
    }
    if ($navbar == "UPD_MEMBER") {
            $info = array('itemmember_type', 'itemgender', 'itemfirstname', 'itemname', 'itemmobile', 'itemcountry', 'itememail');
            foreach($info as $label) {
            	$_POST[$label] = trim($_POST[$label]); 
            	if(empty($_POST[$label])) {
            		$ll = preg_replace("/^item/", "", $label);
					$_SESSION['info_message']['type'] = 'danger';
					$_SESSION['info_message']['message'] = "Unable to create, as $ll is empty";
					return;
            		}        
            	}
        if (substr($theState, 1) == "savemember") {
            $memdata = new WY_Member;
            $memdata->partialUpdateMember($_POST['itemmember_type'], $_POST['itemgender'], $_POST['itemfirstname'], $_POST['itemname'], $_POST['itemmobile'], $_POST['itemcountry'], $_POST['itememail']);
             $action=211;
             $auditlog= logEvent($_SESSION['user_backoffice']['id'],$action,$navbar,$_POST['itememail']);
            $_SESSION['message'] = 'Changes saved';
        }
        return;
    }

    if ($navbar == "CRUD_CLUSTER") {
        $itemclustersearch = (isset($_REQUEST['itemclustersearch'])) ? $_REQUEST['itemclustersearch'] : "";
        $itemclustersearch_slave = (isset($_REQUEST['itemclustersearch_slave'])) ? $_REQUEST['itemclustersearch_slave'] : "";
        $itemclustname = (isset($_REQUEST['itemclustname'])) ? $_REQUEST['itemclustname'] : "";
        $itemclustactegory = (isset($_REQUEST['itemclustcategory'])) ? $_REQUEST['itemclustcategory'] : "";
        $itemclusttype = (isset($_REQUEST['itemclusttype'])) ? $_REQUEST['itemclusttype'] : "";
        $clus = new WY_Cluster;

        if ($theState == "savecluster") {
            if (!empty($_REQUEST['itemclsname']) && isset($_REQUEST['itemclstype']) && !empty($_REQUEST['itemclscategory']) && isset($_REQUEST['itemclscontent'])) {
                $itemclsname = $_REQUEST['itemclsname'];
                $status = $clus->create($_REQUEST['itemclstype'], $_REQUEST['itemclsname'], $_REQUEST['itemclscategory'], $_REQUEST['itemclsparent'], $_REQUEST['itemclscontent']);
                if ($status > 0)
                    $_SESSION['message'] = 'Cluster ' . $itemclsname . ' has been created';
                else
                    $_SESSION['message-error'] = 'Cluster ' . $itemclsname . ' has NOT been created' . $clus->msg;
            } else
                $_SESSION['message-error'] = 'Cluster ' . $itemclsname . ' has NOT been created (undefined values)' .
                        $_REQUEST['itemclstype'] . '-' . $_REQUEST['itemclsname'] . '-' . $_REQUEST['itemclscontent'];
        }
        if ($theState == "updatecluster") {
            if (!empty($itemclustname) && isset($itemclusttype) && !empty($itemclustactegory) && isset($_REQUEST['itemclustcontent'])) {
                $status = $clus->update($itemclusttype, $itemclustname, $itemclustactegory, $_REQUEST['itemclustparent'], $_REQUEST['itemclustcontent']);
                if ($status > 0)
                    $_SESSION['message'] = 'Cluster ' . $itemclustname . ' has been updated';
                else
                    $_SESSION['message-error'] = 'Cluster ' . $itemclustname . ' has NOT been updated' . $clus->msg . "(" . $clus->result . ")";
            } else
                $_SESSION['message-error'] = 'Cluster ' . $itemclustname . ' has NOT been updated (undefined values)' .
                        $itemclusttype . '-' . $itemclustname . '-' . $_REQUEST['itemclustcontent'];
        }
        else if ($theState == "deletemastercluster") {
            if (!empty($itemclustersearch) && !empty($itemclustactegory)) {
                $status = $clus->delete("MASTER", $itemclustname, $itemclustactegory);
                if ($status > 0)
                    $_SESSION['message'] = 'Master Cluster ' . $itemclustersearch . ' has been deleted';
            } else
                $_SESSION['message'] = 'Master Cluster ' . $itemclustersearch . ', ' . $itemclustactegory . ' has NOT been deleted (invalid args)';
        }
        else if ($theState == "deleteslavecluster") {
            if (!empty($itemclustersearch_slave) && !empty($itemclustactegory)) {
                $status = $clus->delete("SLAVE", $itemclustname, $itemclustactegory);
                $_SESSION['message'] = 'Slave Cluster ' . $itemclustersearch_slave . ', ' . $itemclustactegory . ' has been deleted (testing)';
            } else
                $_SESSION['message'] = 'Slave Cluster ' . $itemclustersearch_slave . ', ' . $itemclustactegory . ' has NOT been deleted (invalid args)';
        }
    }
}

function createRestaurant($inputAr, $curTitle, $theRestaurant, $dbLabel) {
    if (!empty($_REQUEST['itemtitle'])) {

        $name = preg_replace("/['\"]/", " ", $_REQUEST['itemtitle']);
        $name_tmp = explode(' ', $name);
        $name = '';
        foreach ($name_tmp as $n) {
            $name .= ucfirst(strtolower($n)) . ' ';
        }
        $name = trim($name);
        $name = preg_replace("/[ ]/", "", $name);
        $name = preg_replace("/[^a-zA-Z0-9_-`]/", "", $name);

        if (empty($name))
            return array('error' => 'empty name');

        $Sql = "select ID from restaurant WHERE 'restaurant' = '$theRestaurant' limit 1";
        $res = pdo_single_select($Sql);
        if (count($res) > 0)
            return updateRestaurant($inputAr, $curTitle, $theRestaurant, $dbLabel);

            $action=200;
        $auditlog= logEvent($_SESSION['user_backoffice']['id'],$action,'CreateResturant',$theRestaurant);
        $resdata = new WY_restaurant();
        $resdata->getRestaurant($theRestaurant);

        $id = $resdata->getCountryIsoCode($_REQUEST['itemcountry']) . '_' . $resdata->getCityIsoCode($_REQUEST['itemcity']) . '_R_' . $name;

        $_REQUEST['itemrestaurant'] = $id;
        $_REQUEST['itemtitle'] = preg_replace("/['\"]/", "`", $_REQUEST['itemtitle']);
        $Sql = "insert into restaurant (restaurant) values ('$id')";
        $status = pdo_exec($Sql);
    
        //auditlog

        if (substr($status, 0, 5) == "error")
            return array('error' => $status);

        mkdir("upload/restaurant/$name", 0777);


        return updateRestaurant($inputAr, $curTitle, $id, $dbLabel);
    }
}

function createMember($inputAr, $curTitle, $theMember, $dbLabel) {
    if (!empty($_REQUEST['itememail'])) {

        $email = preg_replace("/['\"]/", "`", $_REQUEST['itememail']);
        $email = preg_replace("/[ ]/", "_", $email);
        //$email = preg_replace("/[^a-zA-Z0-9_-`]/", "", $email);

        $mobile = clean_tel($_REQUEST['itemmobile']);
        if (empty($email))
            return array('error' => 'empty email');
        if (empty($mobile))
            return array('error' => 'empty mobile');
        $theMember = $email;
        $Sql = "select ID from member WHERE email = '$theMember' or mobile = '$mobile' limit 1";
        $res = pdo_single_select($Sql);
        if (count($res) > 0)
            return array('error' => 'duplicate member, email or mobile');

        $Sql = "insert into login (Email, Password) values ('$email', '0d95748161d335d18410d58e5811443fa9ba7d1c806e688a050cb8e1794b3a77')";
        $status = pdo_exec($Sql);

        if (substr($status, 0, 5) == "error")
            return array('error' => $status);

        $Sql = "insert into member (email) values ('$email')";
        $status = pdo_exec($Sql);
   
        if (substr($status, 0, 5) == "error")
            return array('error' => $status);

        return updateMember($inputAr, $curTitle, $theMember, $dbLabel);
    }
}

function updateRestaurant($inputAr, $curTitle, $theRestaurant, $dbLabel) {

	foreach($_REQUEST as $kk => $vv) { $_REQUEST[$kk] = clean_input($vv); }
		
    $resdata = new WY_restaurant();
    require_once 'lib/class.marketing_campaign.inc.php';
    $campaign = new WY_MarketingCampaign();

    $resdata->getRestaurant($theRestaurant);
    // var_dump($inputAr]);exit;

    if (!empty($_REQUEST['itemcity'])) {
        $_REQUEST['itemcity_iso_code'] = $resdata->getCityIsoCode($_REQUEST['itemcity']);
    }

    if (!empty($_REQUEST['itemcountry'])) {
        $_REQUEST['itemcountry_iso_code'] = $resdata->getCountryIsoCode($_REQUEST['itemcountry']);
    }


    // HACK INJECTION ANGULAR JS form
    if (!empty($_REQUEST['itemupdateemail'])) {
        if($_REQUEST['itememail'] != $_REQUEST['itemupdateemail']){
            $_REQUEST['itemaws_email_verification'] = '';
        }
        
        $_REQUEST['itememail'] = $_REQUEST['itemupdateemail'];    
        unset($_REQUEST['itemupdateemail']);
        unset($_REQUEST['updateemail']);
    }
    
    
    // save only the segment	
    $inputAr_cn = count($inputAr);
    for ($start = 0; $start < $inputAr_cn; $start += 3)
        if (($inputAr[$start + 2] == "title" || $inputAr[$start + 2] == "titlein") && $inputAr[$start + 1] == $curTitle)
            break;

    $Sql = "Update restaurant set ";
    for ($i = $start + 3, $sep = ""; $i < $inputAr_cn && $inputAr[$i + 2] != "title"; $i += 3) {
        $val = $inputAr[$i + 1];
        if (substr($val, 0, 4) != "item")
            continue;
        
        
        //error_log("ITEM RATING " . $val .  " " . $_REQUEST[$val]);
        $field = substr($val, 4);
        
        
        // HACK INJECTION ANGULAR JS form
        if (substr($field, 0, 11) == "updateemail")
            continue;
        
        $newval = (isset($_REQUEST[$val])) ? $_REQUEST[$val] : "";
        $type = $inputAr[$i + 2];
    
        $arg = "";
        if (preg_match("/\([^\)]+\)/", $type, $match)) {
            $arg = preg_replace("/\(|\)/", "", $match[0]);
            $type = preg_replace("/\([^\)]+\)/", "", $type);
        }
        //marketing  campaign external link generation
            if($field==='url'){
                 $campaignToken =uniqid();
                if($resdata->ischangedUrl($theRestaurant,$newval) ){
                   $campaign_id ='0022';
                   $creation_date = date('Y-m-d H:i:s');
                   $refId= date('Ymd');
                   $mode ='external';
                   $generateToken = $mode.'|'.$theRestaurant.'|'.$refId.'|'.$campaign_id.'|'.$campaignToken; ; 
//                   if(substr( $redirect_to, 0, 4 ) != "http" ){
//                        $redirect_to ="http://".$newval;
//                   }
                   $redirect_to = $newval.'?utm_source='.$mode.'&utm_medium='.$mode.$campaign_id.'&utm_campaign='.$mode.$campaign_id;
                   $campaign_token = $campaign->base64url_encode($generateToken);
                   $longUrl = "https://tracking.weeloy.com/$generateToken";
                   $shortUrl = $campaign->bitly_shorten($longUrl);
                   $campaign->create($campaign_id, '',$theRestaurant, $mode, $redirect_to, $creation_date, $campaignToken,'ext1','EXTLINK',$shortUrl);
                }  
            }
            
       //ended marketing campaign external link generation //
        // setting different bits of the same flag. Compute every bit, before turning to an sql cmd
        

        if ($type == "flagchecked") {          
            $inputval = intval($_REQUEST[$val . $arg]);       
            $bit = 1 << (intval($arg) - 1);
            $tmpval = (isset($flagcheckAr[$field])) ? $flagcheckAr[$field] : intval($resdata->data[$field]);       
            $newval = $tmpval ^ ((-$inputval ^ $tmpval) & $bit);   // number ^= (-x ^ number) & (1 << n);
            $flagcheckAr[$field] = $newval;
            continue;
        	}
	
        if ($type == "inputmultiple") {          
            $inputval = $_REQUEST[$val . $arg];       
            $tmpval = (isset($inputmultipleAr[$field])) ? $inputmultipleAr[$field] : $resdata->data[$field];
            $tmpvalAr = explode("|", $tmpval);
            $tmpvalAr[intval($arg) - 1] = $inputval; 
            $newval = implode("|", $tmpvalAr);
            $inputmultipleAr[$field] = $newval;
               error_log("UPDATING " . $field . " => " . $newval . "(" . $inputval . ")");
            continue;
        	}
        	  

                
                
        $Sql .= $sep . $field . "='" . $newval . "'";
        $sep = ", ";
    }

    reset($flagcheckAr);
    reset($inputmultipleAr);
    
    
    $action=201;
    logEvent($_SESSION['user_backoffice']['id'],$action,'updateRestaurantinfo',$theRestaurant);
    
    while (list($field, $newval) = each($flagcheckAr))
        $Sql .= $sep . $field . "='" . $newval . "'";
    while (list($field, $newval) = each($inputmultipleAr))
        $Sql .= $sep . $field . "='" . $newval . "'";

    $Sql .= " where restaurant = '$theRestaurant' limit 1";

    if (empty($sep)) {
        return true;
    }

    return pdo_exec($Sql);
}

function logEvent($user,$action,$page,$event){
     $logger = new WY_log("admin");
     
     return $logger->LogEvent($user, $action, 1, $page, $event, date("Y-m-d H:i:s"));
    
}
function cancelpolicy($theRestaurant, $email) {
    include "app/components/restaurant/cancel_policy/cancel_policy.php";
}
function sectionconfig($theRestaurant, $email) {
    include "app/components/restaurant/section_config/section_config.php";
}
function mgtpayment($theRestaurant, $email) {

    include "app/components/restaurant/payment/paymentconfig.inc.php";

}
function mgtvideo($theRestaurant, $email) {
    $resdata = new WY_restaurant();
    print_search_bar_restau($theRestaurant, $restrictedCountry);
    include 'app/components/video/upload_video_link.inc.php';

}

function clustersubemail($email) {
        require_once('app/components/cluster/cluster.inc.php');
}

function mgtconfigpayment($theRestaurant, $email) {
        include 'app/components/payment/payment.inc.php';

}

function createCampaign($itemtracktype, $itemrestaurant, $itemtrackcampaign, $itemtrackdate, $itemtrackdesc, $itemtrackredirectto, $itemtrackgeneate) {

    require_once 'lib/class.marketing_campaign.inc.php';
    $campaign = new WY_MarketingCampaign();
    return $campaign->createCampaign($itemtracktype, $itemrestaurant, $itemtrackcampaign, $itemtrackdate, $itemtrackredirectto, $itemtrackgeneate,'ext1','website external link creation','');
}

function updateMember($inputAr, $curTitle, $theMember, $dbLabel) {

    // save only the segment	
    $inputAr_cn = count($inputAr);

    for ($start = 0; $start < $inputAr_cn; $start += 3)
        if (($inputAr[$start + 2] == "title" || $inputAr[$start + 2] == "titlein") && $inputAr[$start + 1] == $curTitle)
            break;
    $Sql = "Update member set ";
    for ($i = $start + 3, $sep = ""; $i < $inputAr_cn && $inputAr[$i + 2] != "title"; $i += 3) {
        $val = $inputAr[$i + 1];
        if (substr($val, 0, 4) != "item")
            continue;
        $fieldname = substr($val, 4);
        if ($fieldname == 'mobile' && $_REQUEST[$val] == '')
            continue;
        if ($fieldname == 'email') {
            continue;
        } else if ($fieldname == 'membertype') {
            $fieldname = 'member_type';
        }
        //error_log("ITEM RATING " . $val .  " " . $_REQUEST[$val]);
        $Sql .= $sep . $fieldname . "='" . $_REQUEST[$val] . "'";
        $sep = ", ";
    }

    $Sql .= " where email = '$theMember' limit 1";
    if (empty($sep)) {
        return true;
    }

    $res = pdo_exec($Sql);

    if (substr($res, 0, 5) == "error")
        return array('error' => $res);
    return true;
}


//no need
function call_routine($reference, $theRestaurant, $label) {

    global $globalScript;
    ///////////////    OLD   ////////////////
    //if(preg_match("/wheel/", $reference))
    $wheeldata = new WY_wheel($theRestaurant, '', '');
    $wheeloffers = &$wheeldata->wheelconfoffer;


    switch ($reference) {
        case "itemwheeldescription":

            printf("<table class='table table-striped'>");
            printf("<thead><tr><th>Parts</th><th>Offer  <i class='icon-gift icon-2x' style='color:green;'></i></th><th>Value  <i class='icon-signal icon-2x' style='color:green;'></i></th><th>Terms and conditions  <i class='icon-info-sign icon-2x' style='color:green;'></i></th></tr></thead>");
            $limit = count($wheeloffers);
            for ($i = 0, $k = 1; $i < $limit; $i += 3, $k++) {
                $class = ($k % 2 == 1) ? "style='background-color:white;'" : "class='success'";
                printf("<tr $class><td width='10%%' class='gray'><span style='font-size: 18px; color: black; text-shadow: 5px 5px 10px black;'>%d</span></td>", $k);
                printf("<td width='20%%' nowrap>%s</td><td width='15%%' nowrap>%s</td><td>%s</td>", $wheeloffers[$i], $wheeloffers[$i + 1], $wheeloffers[$i + 2]);
                printf("</tr>");
            }
            printf("</table>");
            break;
            
        case "notfication_management" :
               notfication_management($theRestaurant, $email);
	break;

        case "itemupdateemail" :
               include 'app/components/restaurant/update_restaurant/_update_restaurant.php';
	break;
        case "cancelpolicy" :
                cancelpolicy($theRestaurant, $email);
        break;
        case "mgtpayment" :
               mgtpayment($theRestaurant, $email);
        break;
        case "mgtconfigpayment" :
               mgtconfigpayment($theRestaurant, $email);
        break;
        case "sectionconfig" :
                sectionconfig($theRestaurant, $email);
        break;
        case "video_management" :
                mgtvideo($theRestaurant, $email);
        break;   
	case "clustersubemail":
		clustersubemail($email);
	break;	 
        case "itemtrackgeneate":
            $extra = "btn-primary customColor";
            $onclick = "onclick=\"generate_tracking_code();\"";

            echo "<div class='form-group row'>
			<label for='' class='col-sm-2 control-label'>
			<input type='button' class='btn $extra' value='$label' $onclick>
			</label>
			<div class='col-sm-10'></div>
			</div>";

            $globalScript .= "function generate_tracking_code() { valAr = [ 'itemtrackdate', 'itemtracktype', 'itemtracknumer' ]; hotelname = '" . $theRestaurant . "'; tt = ''; for(i = 0, sep =''; i < valAr.length; i++, sep='|') tt += sep + $('#' + valAr[i]).val();  alert('tt = '+tt + '=> '+ window.btoa(tt) + '=> '+ Base64.encode(tt)); tt = Base64.encode(tt); $('#itemtrackvalue').val(tt); }";

        default:
            return;
    }
///////////////    OLD   ////////////////
}
function notfication_management($theRestaurant, $email){

    
}

function get_hotellist() {

    $action = $_SERVER['PHP_SELF'];
    $dbLabel = array('hotelname');

    $field = implode(",", $dbLabel);
    $dbLabel_cn = count($dbLabel);

    $data = pdo_multiple_select("SELECT $field FROM restaurant where hotelname != '' ORDER by hotelname");
    if (count($data) <= 0)
        return $data;

    foreach ($data as $row) {
        for ($i = 0; $i < $dbLabel_cn; $i++) {
            $tt = $dbLabel[$i] . "Ar";
            ${$tt}[] = $row[$dbLabel[$i]];
        }
    }
    return $hotelnameAr;
}

function print_body() {
    global $navbar, $theRestaurant, $theMember, $inputAr, $dataobject;
    $accountemail = $_SESSION['user_backoffice']['email'];
    $restrictedCountry = "";
    $memdata = new WY_Member();
    $memdata->getMember($accountemail);
    if ($memdata->is_restrictedCountry($accountemail))
        $restrictedCountry = $memdata->country;
    $allowCreateDelete = $memdata->is_allowCreateResto($accountemail);
    if (isset($_SESSION['info_message'])) {
        echo '<div class="alert alert-' . $_SESSION['info_message']['type'] . '"><a href="#" class="close" data-dismiss="alert">&times;</a><strong></strong> ' . $_SESSION['info_message']['message'] . '</div>';
        unset($_SESSION['info_message']);
    }

    if ($navbar == "HOME") {
        printf('Weeloy administration center');
        return;
    }

    if ($navbar == "UPD_RESERVEDFLAG") {
        echo "<a class='mybarnav' href=\"javascript:setNavBar('UPD_RESERVED_FLAG');\"><h5>Update Restaurant Reserved Flag</h5></a>";
        return;
	}
	
    if ($navbar == "RESTAURANT") {
        if ($allowCreateDelete)
            echo "<a class='mybarnav' href=\"javascript:setNavBar('CRD_RESTAU');\"><h5>CRUD</h5></a>";
        echo "<a class='mybarnav' href=\"javascript:setNavBar('UPD_RESTAU');\"><h5>Update</h5></a>";
        if ($allowCreateDelete)
            echo "<a class='mybarnav' href=\"javascript:setNavBar('TRACKING_RESTAU');\"><h5>Tracking Campaign</h5></a>";
        echo "<a class='mybarnav' href=\"javascript:setNavBar('UPD_VIDEOLINK');\"><h5>VIDEOLINK</h5></a>";
        echo "<a class='mybarnav' href=\"javascript:setNavBar('FEATURED_RESTAURANT');\"><h5>Manage Categories</h5></a>";
        return;
    }

    if ($navbar == "MEMBER") {
        //echo "<a class='mybarnav' href=\"javascript:setNavBar('ADD_MEMBER');\"><h5>ADD</h5></a>";
        //echo "<a class='mybarnav' href=\"javascript:setNavBar('UPD_MEMBER');\"><h5>UPDATE</h5></a>";
        //echo "<a class='mybarnav' href=\"javascript:setNavBar('DEL_MEMBER');\"><h5>DELETE</h5></a>";
        //echo "<br><a class='mybarnav' href=\"javascript:setNavBar('ASSIGN_MEMBER_RESTAURANTS_LIST');\"><h5>BackOffice access</h5></a>";
        echo "<a class='mybarnav' href=\"javascript:setNavBar('MEMBERLIST');\"><h5>MEMBERL LIST</h5></a>";
        echo "<br><a class='mybarnav' href=\"javascript:setNavBar('ASSIGN_MEMBER_APPMANAGER_RESTAURANTS_LIST');\"><h5>Mobile app access</h5></a>";

        return;
    }

    if ($navbar == "CLUSTER") {
        echo "<a class='mybarnav' href=\"javascript:setNavBar('CRUD_CLUSTER');\"><h5>C-R-U-D</h5></a>";

        return;
    }
    if ($navbar == "CUISINE") {
        echo "<a class='mybarnav' href=\"javascript:setNavBar('ADD_CUISINE');\"><h5>ADD</h5></a>";
        echo "<a class='mybarnav' href=\"javascript:setNavBar('UPD_CUISINE');\"><h5>UPDATE</h5></a>";
         echo "<a class='mybarnav' href=\"javascript:setNavBar('RMD_CUISINE');\"><h5>DELETE</h5></a>";

        return;
    }
    if ($navbar == "AREA") {
         echo "<a class='mybarnav' href=\"javascript:setNavBar('ADD_AREA');\"><h5>ADD</a>";
        echo "<a class='mybarnav' href=\"javascript:setNavBar('UPD_AREA');\"><h5>UPDATE</a>";
         echo "<a class='mybarnav' href=\"javascript:setNavBar('DEL_AREA');\"><h5>DELETE</a>";

        return;
    }
     if ($navbar == "CAMPAIGN") {
         echo "<a class='mybarnav' href=\"javascript:setNavBar('ADD_TRACKING');\"><h5>ADD</a>";
         echo "<a class='mybarnav' href=\"javascript:setNavBar('LIST_TRACKING');\"><h5>LIST CAMPAIGN</a>";
         return;
    }

    if ($navbar == "EMAIL MANAGEMENT") {
        
        echo "<a class='mybarnav' href=\"javascript:setNavBar('ED_REGISTER');\"><h5>REGISTER</h5></a>";
        echo "<a class='mybarnav' href=\"javascript:setNavBar('ED_BOOKING');\"><h5>BOOKING</h5></a>";
        echo "<a class='mybarnav' href=\"javascript:setNavBar('ED_CALLCENTER');\"><h5>WHITE LABEL</h5></a>";
        echo "<a class='mybarnav' href=\"javascript:setNavBar('ED_WHELLWIN');\"><h5>WHEELWIN</h5></a>";
        echo "<a class='mybarnav' href=\"javascript:setNavBar('ED_CATERING');\"><h5>CATERING</h5></a>";
        echo "<a class='mybarnav' href=\"javascript:setNavBar('ED_REVIEW');\"><h5>REVIEW</h5></a>";
        echo "<a class='mybarnav' href=\"javascript:setNavBar('ED_NOSHOW');\"><h5>NOSHOW</h5></a>";
        echo "<a class='mybarnav' href=\"javascript:setNavBar('ED_S3TRACKINGEMAIL');\"><h5>SPOOL HISTORY</h5></a>";
        return;
    }
    if ($navbar == "NOTIFICATION") {
         echo "<a class='mybarnav' href=\"javascript:setNavBar('PUSH_NOTIFICATION');\"><h5>PUSH NOTIFICATION</a>";
         echo "<a class='mybarnav' href=\"javascript:setNavBar('SMS_RTRACKING');\"><h5>NOTIFICATION TRACKING LIST</a>"; 

	if(in_array($accountemail, array("richard@kefs.me", "richard.kefs@weeloy.com", "vs.kala@weeloy.com","soraya.kefs@weeloy.com","gultiga.lueskul@weeloy.com","hody.leong@weeloy.com")))
    //if ($authenticated && $support_team)			   

         echo "<a class='mybarnav' href=\"javascript:setNavBar('NOTIFICATION_MANAGEMENT');\"><h5>NOTIFICATION MANAGEMENT</a>"; 
         echo "<a class='mybarnav' href=\"javascript:setNavBar('NOTIFICATION_ACTIONCREATION');\"><h5>NOTIFICATION DEFAULT CONFIGURATION</a>"; 
        
    }
    if($navbar == "DATAEXTRACT"){
        echo "<a class='mybarnav' href=\"javascript:setNavBar('GET_DATA');\"><h5>GET DATA</a>"; 
//         echo "<a class='mybarnav' href=\"javascript:setNavBar('CREATE_DATA');\"><h5>CREATE LABEL</a>"; 
    }
    if($navbar =="CATEGORIES") {
        echo "<a class='mybarnav' href=\"javascript:setNavBar('ADD_CATEGORIES');\"><h5>Home</h5></a>"; 
        echo "<a class='mybarnav' href=\"javascript:setNavBar('FOOTER_CATEGORIES');\"><h5>Footer</h5></a>";
        echo "<a class='mybarnav' href=\"javascript:setNavBar('EVENT_CATEGORIES');\"><h5>Event</h5></a>";
    }
    if($navbar == "USER ACL")
         echo "<a class='mybarnav' href=\"javascript:setNavBar('USER_ROLE');\"><h5>MANAGE USER ACL</a>"; 
    if($navbar == "CRAWLER")
         echo "<a class='mybarnav' href=\"javascript:setNavBar('CRAWLER_CRWLACCOUNT');\"><h5>MANAGE CRAWLER ACCOUNT</a>"; 
    if($navbar == "TAG MANAGEMENT")
         echo "<a class='mybarnav' href=\"javascript:setNavBar('TAG_MANAGEMENT');\"><h5>MANAGE TAGS </a>"; 
    if($navbar == "PAYMENT")
         echo "<a class='mybarnav' href=\"javascript:setNavBar('PAYMENT_CREDENTIALS');\"><h5>MANAGE RES PAYMENT CREDENTIALS</a>"; 
    if($navbar == "TOOLS")
         echo "<a class='mybarnav' href=\"javascript:setNavBar('TOOLS_DATACHECK');\"><h5>DATA CHECK</a>"; 
     if ($navbar == "DSB Restaurant")
        echo "<a class='mybarnav' href=\"javascript:setNavBar('DSB_MEMBERLIST');\"><h5>DSB Restaurant List</a>";
     if ($navbar == "MailChimp")
        echo "<a class='mybarnav' href=\"javascript:setNavBar('MailChimp_Register');\"><h5>Home</a>";    
    $section = explode('_', $navbar);
    if ($section[1] == 'RESTAU')
        print_search_bar_restau($theRestaurant, $restrictedCountry);
    if ($navbar == 'UPD_RESERVED_FLAG')
        print_search_bar_restau($theRestaurant, $restrictedCountry);
//    if ($section[1] == 'VIDEOLINK') {
//        echo "SDASDS".$theRestaurant;
//       print_search_bar_restau($theRestaurant, $restrictedCountry);
//        include 'app/components/video/upload_video_link.inc.php';
//    }

    if ($section[1] == 'MEMBER') {
        print_search_bar_member($theMember);
    }
    if ($section[1] == 'CUISINE') {
         $action=221;
         $auditlog= logEvent($_SESSION['user_backoffice']['id'],$action,$navbar,'');
        include 'app/components/cuisine/cuisine_update.inc.php';
    }
    if ($section[1] == 'AREA') {
         $action= 220;
         $auditlog= logEvent($_SESSION['user_backoffice']['id'],$action,$navbar,'');
        include 'app/components/area/area_update.inc.php';
    }
    if ($section[1] == 'TRACKING') {
        $action=222;
        $auditlog= logEvent($_SESSION['user_backoffice']['id'],$action,$navbar,'');
        include 'app/components/tracking/tracking_update.inc.php';  
    }
  
    if ($section[0] == 'ED') {
        include 'app/components/email/email_management.inc.php';
    }
    if ($section[1] == 'NOTIFICATION') {
        include 'app/components/notification/pushnotification.inc.php';
    }
    if ($section[1] == 'DATA') {
        include 'app/components/data/extraction.inc.php';
    }
    if ($section[1] == 'CATEGORIES') {
        switch ($navbar) {
            case 'ADD_CATEGORIES':
                include 'app/components/categories/categories.inc.php';
            break;
            case 'FOOTER_CATEGORIES':
                include 'app/components/categories/footercategories.inc.php';
            break;
            case 'EVENT_CATEGORIES':
                include 'app/components/categories/events.php';
            break;
            default:
                error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": Invalid navigation: ".$navbar);
            break;
        }       
    }
    if ($section[1] == 'ROLE')
        include 'app/components/manageacl/useracl.inc.php';
    if ($section[1] == 'CRWLACCOUNT')
        include 'app/components/crawler/crawler.inc.php';
    if ($section[1] == 'RTRACKING')
        include 'app/components/mailspools/smstracking.inc.php';
    if ($section[1] == 'ACTIONCREATION')
        include 'app/components/notification/configuration/_notification_configuration.php';
    if ($section[0] == 'TAG')
        include 'app/components/tagmanagement/managetag.inc.php';
    if ($section[0] == 'FEATURED')
        include 'app/components/restaurant/restmanagement.inc.php';
    if ($section[1] == 'S3TRACKINGEMAIL')
        include 'app/components/email/email_management_s3.inc.php';
    if ($section[0] == 'DSB' && $section[1] == 'MEMBERLIST') // 0: DSB 1: REGISTER
        include 'app/components/DailySpecialBoard/dsbrestolist.inc.php';
        // include 'app/components/members/memberlist.inc.php';
    if ($section[0] == 'MailChimp' && $section[1] == 'Register') // 0: MailChimp 1: Register
        include 'app/components/MailChimp/MailChimpRegister.inc.php';
    if ($navbar == 'MEMBERLIST')
        include 'app/components/members/memberlist.inc.php';
    $resdata = new WY_restaurant();
    if($navbar == 'NOTIFICATION_MANAGEMENT') { 
    	 print_selectlist($resdata, $theRestaurant, $navbarTitle);
         include "app/components/notification/configuration/mgtnotification.inc.php";
    }
    if($navbar== 'NOTIFICATION_ACTIONCREATION')
         include "app/components/notification/configuration_administration/_notification_configuration_administration.php";
    if ($section[1] == 'DATACHECK')
        include 'app/components/tools/datacheck.inc.php';
    $resdata = new WY_restaurant();
    print_allinput($resdata, $dataobject, "admin");
}
?>