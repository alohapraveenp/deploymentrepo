(function(app) {
app.service('NotificationConfigurationService', ['$http', '$q', function ($http, $q) {
        this.getNotifyActions = function(restaurant) {
            var url = '../api/notification/getnotiifcation/' + restaurant;
//              return $http.get(url).success(function (response) { 
//              
//             });
                var deferred = $q.defer();
                    $http.get(url, {
                         cache: true
                     }).success(function(response) {
                         deferred.resolve(response);
                     });
                return deferred.promise;
          
        }
        this.updateNotifyAction = function (data) {
            return $http.post("../api/notification/updateactions",
                    {
                        'data': data
                    }).then(function (response) {
           
                return response.data;
            });

        };
         this.getNotifytype = function () {
                var url = '../api/notification/getType';
                var deferred = $q.defer();
                $http.get(url, {
                    cache: true
                }).success(function (response) {
                    deferred.resolve(response);
                });
                return deferred.promise;

        };
        this.saveNotify =function(data){

            return $http.post("../api/notification/create",
                {
                    'parent_group': data.parent_group_action,
                    'group': data.group_action,
                    'notification_type': data.notification_type,
                    'status': 'active',
                    'white_label':data.white_label,
                    'email': data.email,
                    'sms': data.sms,
                    'sms_premium': data.sms_premium,
                    'push_notification': data.push_notification,
                    'mode': 'create'
                }).then(function (response) {
                     return response.data;
                    
                });
        };
 
    }]);
})(angular.module('app.api.notification.notifyconfiguration', []));


