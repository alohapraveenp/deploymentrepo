(function(app) {
    app.service('VerifyEmailService', ['$q', '$http', function($q, $http) {
        this.getStatus = function(email) {
            var defferred = $q.defer();
            $http.get('../api/v2/admin/restaurant/verifyemail/status/'+email).success(function(response) {
                defferred.resolve(response);
            });
            return defferred.promise;
        };
        
        this.verifyEmailAddress = function(email) {
            var defferred = $q.defer();
            $http.get('../api/v2/admin/restaurant/verifyemail/'+email).success(function(response) {
                defferred.resolve(response);
            });
            return defferred.promise;
        };
        
        

    }]);

})(angular.module('app.api.restaurant.verifyemail', []));
