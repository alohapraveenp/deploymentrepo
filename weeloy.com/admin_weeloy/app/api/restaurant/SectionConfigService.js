(function(app) {
app.service('SectionConfigService', ['$http', '$q', function ($http, $q) {
        this.getRestaurantSection = function (restaurant,product) {
            var defferred = $q.defer();
            var API_URL = "../api/v2/restaurant/sections/product/"+restaurant+"/"+product;
            return $http.get(API_URL).success(function(response) {
                defferred.resolve(response);
            });
             return defferred.promise;
        };
        
        this.saveSectionConfig = function (section) {
                var defferred = $q.defer();
                var API_URL ="../api/v2/restaurant/sections/save";
                 $http({
                    url: API_URL,
                    method: "POST",
                    data: $.param({
                         restaurant:section.restaurant,
                          description :section.description,
                          product :section.product,
                          min_pax :section.min_pax,
                          max_pax : section.max_pax,
                          additional_info : section.additional_info,
                          require_config :section.require_confirmation,
                          require_text :section.require_confirmation_text,
                          lunch :section.lunch,
                          dinner :section.dinner
                }),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
             }).success(function(response) {
                defferred.resolve(response);
            });
             return defferred.promise;
        };
        
        
        
    }]);
})(angular.module('app.api.restaurant.sectionconfig', []));


