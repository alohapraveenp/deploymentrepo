(function(app) {
app.service('CancelPolicyService', ['$http', '$q', function ($http, $q) {
        this.getCancelPolicy = function (restaurant,product) {
        var defferred = $q.defer();
            return $http.post("../api/services.php/cancelpolicy/list",
                    {
                        'restaurant': restaurant,
                        'type'       :'admin',
                        'product'    :product
                     }).success(function(response) {
                defferred.resolve(response);
            });
             return defferred.promise;
        };
        
        this.savePolicy = function (restaurant,data,product) {
                var defferred = $q.defer();
       
          $http.post("../api/services.php/cancelpolicy/save/",
                    {
                        'restaurant': restaurant,
                        'data':data,
                        'product':product
                         
                    }).success(function(response) {
                defferred.resolve(response);
            });
             return defferred.promise;
        };
        
        
        
    }]);
})(angular.module('app.api.restaurant.cancelpolicy', []));


