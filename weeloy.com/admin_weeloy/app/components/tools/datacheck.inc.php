<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>
<div class="container">
    <div class="row">
        <div class="col-md-12 left-sec">`
            <div ng-controller='ToolSetController' ng-init="moduleName = 'datacheck';" style='margin-top:20px;'>
                <div class="container container-table"  >
                    <div class="row ">
                        <div class="container">
                            <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-2 col-lg-8 col-md-8 col-sm-8 col-xs-12"> 
                                <input type="button" ng-click="checkNow()" class="btn btn_default" value="Data Check" style="border-radius:0px;" >
                                <p> </br></p>
                                <table border="1px solid black">
                                    <tr>
                                        <th>Table Name - JASON Data Column
                                        </th> 
                                    </tr>
                                    <tr ng-repeat="record in data">
                                        <td><a href="" ng-click="checkColumn(record)">{{ record }}</a></td>
                                    </tr>
                                </table>   

                                <p> </br></p>
                                <table border="1px solid black">
                                    <tr>
                                        <th>ID
                                        </th>
                                        <th>Column Value
                                        </th>
                                        <th>JSON Value
                                        </th> 
                                    </tr>
                                    <!-- {{columnVal|json}} -->
                                    <tr ng-repeat="value in columnVal" ng-if="value != ''">
                                        <td>{{ value.ID }}</td>
                                        <td>{{ value.COLMN }}</td>
                                        <!-- <td>{{ value.JASON_MSG}}</td> -->
                                        <td><input id="changedata" name="changedata" type='text' ng-model ="value.JASON_MSG"/></td>
                                        <td><input type='button' ng-click="saveData($index, value.ID, value.JASON_MSG)" value="Save"/></td>
                                    </tr>
                                </table>                                 
                            </div>
                        </div>
                    </div>                       
                </div>
            </div>
        </div>
    </div>
</div>
           
<script>
    app.controller('ToolSetController', ['$scope', 'adminServiceApi', '$http', function($scope, adminServiceApi, $http){

        $scope.checkNow = function () {
            // alert("in clcik");
            adminServiceApi.getDataTables().then(function (response) {
                // console.log(JSON.stringify(response));
                if (response.data.status > 0) {
                    $scope.data = response.data.data;
                    // alert("SuccessFully Saved Your Changes.");
                }
            });
        };

        $scope.checkColumn = function (record) {
            // alert("in clcik");
            $scope.record = record;
            $scope.columnVal = '';
            adminServiceApi.getColumnValue(record).then(function (response) {
                // console.log(JSON.stringify(response));
                if (response.data.status > 0) {
                    $scope.columnVal = response.data.data;
                }
            });
        };    

        $scope.saveData = function (index,id,value) { 

            adminServiceApi.postUpdColumn($scope.record, id, value).then(function (response) {
                if (response.data.status > 0 ) {
                    alert(JSON.stringify(response.data));
                }
            });
        };

    }]);
</script>