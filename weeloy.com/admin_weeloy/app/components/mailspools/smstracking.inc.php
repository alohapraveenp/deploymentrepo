<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>
<div class="container">
    <div class="row">
        <div class="col-xl-12 left-sec">
<div ng-controller="AclController" ng-init="moduleName='acl'; listEventFlag = true; viewEventFlag=false; createEventFlag=false;" >
	<div id='listing' ng-show='listEventFlag'>
		<div class="form-group"  style='margin-bottom:25px;'>
                    <div class="col-md-4">
                            <div class="input-group col-md-4">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                                    <input type="text" class="form-control input-sm" ng-model="searchText" ng-change="paginator.setPage(0);" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" style='width:200px;'> 
                            </div>
                    </div>
<!--                    <div class="col-md-2" style='font-size:10px;'>
                        selected records: <strong> {{filteredEvent.length}} </strong>
                    </div>-->
                     <div class="col-md-2">
                        <div class="btn-group">
			<button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style='font-size:11px;'>Filter <span class="caret"></span></button>
                            <ul class="dropdown-menu" role="menu">
                                <li ng-repeat="x in filter" class='glyphiconsize'>
                                    <a href ng-if="x!=='date'" ng-class="glyphicon glyphicon-ok" href ng-click="getData(x)"> {{x}}</a>
                                 </li>
                            </ul>
                        </div> 
                    </div>
                     <div class="col-md-2" style='font-size:10px;'>
                        selected records: <strong> {{filteredEvent.length}} </strong>
                    </div>
                    <div class="col-md-2">
                        <div class="btn-group">
                                <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style='font-size:11px;'>Page Size <span class="caret"></span></button>
                                <ul class="dropdown-menu" role="menu">
                                        <li ng-repeat="x in paginator.pagerange()"><a href ng-click="paginator.setRowperPage(x)">{{x}}</a></li>
                                </ul>
                        </div> 		
                    </div>

                      <div class="col-sm-2">
			<form action="echo.php" id="extractBkOfDay" name="extractBkOfDay" method="POST" target="_blank">
                            <input type="hidden" ng-model='contentBkOfDay' name="contentBkOfDay" id="contentBkOfDay">
                            <a href ng-click='extractSelection();' class="btn btn-success btn-sm" style='color:white;'><span class='glyphicon glyphicon-save'></span> &nbsp; Extract Selection</a>
                        </form>
                    </div>
                    
                </div>
            
            <div style=" clear: both;"></div>
		<table width='100%' class="table table-condensed table-striped" style='font-size:12px;'>
			<thead>
				<tr>
				<th ng-repeat="y in tabletitle"><tbtitle var="{{y.a}}" name='tabletitle' module='moduleName'/></th>
				
<!--                                <th>delete</th><th> &nbsp; </th>-->
                             
				</tr>
			 </thead>
			<tbody style='font-family:helvetica;font-size:12px;'>
				<tr ng-repeat="x in filteredEvent = (names | filter:searchText) | sizefilter:this | orderBy:predicate:reverse | slicefilter:paginator.getPage():paginator.getRowperPage():paginator.getItemCount() | limitTo:paginator.getRowperPage()" style='font-family:helvetica;font-size:11px;'>
                                    <td ng-repeat="y in tabletitle">
                                    {{ x[y.a] | adatereverse:y.c }}
                                        </span>
                                    </td>
				
<!--                                       <td width='30'>&nbsp;</td>
					<td nowrap><a href ng-click="delete(x)" style='color:red;'><span class='glyphicon glyphicon-trash'></a></span></td>-->
					
				</tr><tr><td colspan='{{tabletitle.length + 3}}'></td></tr>
			</tbody>
		</table>
		<div ng-if="filteredEvent.length >= paginator.getRowperPage()" class="col-md-12" align="center" style='font-size:11px' ng-include="'app/components/paginator.html'"></div>
	</div>

	<div class="col-md-12" ng-show='viewEventFlag'>
		<br /><a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a><br />
		<table class='table-striped' style="margin: 0 0 150px 30px;font-size:13px;font-family: Roboto">		
			<tr ng-repeat="y in tabletitle | filter: {b: '!picture'}"><td nowrap ><strong> {{ y.b }} </strong></td><td> &nbsp; </td><td>{{ selectedItem[y.a] | adatereverse:y.c }} </td></tr>
			
		</table>
	</div>

</div>

</div>
</div>
</div>
<script>
app.controller('AclController', ['$scope','$http','$timeout','bookService', function($scope,$http,$timeout,bookService) {
	var todaydate = new Date();
	var aorder = (function() { var i, arr=[]; for(i = 0; i < 30; i++) arr.push(i); return arr; })();
	
	$scope.paginator = new Pagination(100);

	$scope.restaurant = <?php echo "'".$theRestaurant."';"; ?>
	$scope.email = <?php echo "'" . $email . "';"; ?>
	$scope.predicate = '';
	$scope.reverse = false;
	$scope.nonefunc = function() {};
        $scope.filter=['day','week','lastweek','month','lastmonth','year'];

          $scope.isopendate =false;
      
	$scope.tabletitle = [ {a:'index', b:'ID', c:'' , q:'down', cc: 'black' }, {a:'restaurant', b:'Restaurant', c:'' , q:'down', cc: 'black' }, {a:'type', b:'Type', c:'' , q:'down', cc: 'black' },{a:'count', b:'TotalCount', c:'' , q:'down', cc: 'black' }, { a:'DATE', b:'Date', c:'' , q:'down', cc: 'black' }];

	
        var rolesarr=['Management','Finance','Marketing Manager','Support Manager','Sales Manager','IT MANAGER','IT Team'];
   
	//$scope.tabletitleContent = [ { a:'index', b:'ID', c:'', d:'sort', t:'dropdown',  }, { a:'restaurant', b:'Restaurant', c:'', d:'sort' },{ a:'type', b:'Type', c:'', d:'flag', t:'checkarr' }];
       
   
	$scope.Objevent = function() {
		return {
			
			index: 0, 
			restaurant: "", 
                        type:'',
                        status:'',
                        DATE:"",
                        data:"",
  
			remove: function() { 
				for(var i = 0; i < $scope.names.length; i++) 
					if($scope.names[i].name === this.name) {
						$scope.names.splice(i, 1);
						break;
						}
				},
				
			replicate: function(obj) {
				for (var attr in this)
		        	if(this.hasOwnProperty(attr) && typeof this[attr] !== 'function' && attr !== '$$hashKey') 
        				this[attr] = obj[attr];
				return this;
				},
										
			clean: function() {
				

				for (var attr in this)
		        	if(this.hasOwnProperty(attr) && typeof this[attr] !== 'function' && attr !== '$$hashKey') {
        				if(typeof this[attr] === 'string') {
        					this[attr] = this[attr].replace(/\'|\"/g, '’');
        					}
        				}
				return this;
				},
					
			check: function() {
				if(this.name === '' || this.name.length < 6) { alert('Invalid title name, empty or too short(5)'); return -1;}
				if(this.title === '' || this.title.length < 3) { alert('Invalid title name, empty or too short(2)'); return -1;}
				return 1;
				}
			};
	};
        $scope.is_showdate = function(){
            $scope.isopendate =true;
            
            };
      
	$scope.initorder = function() {
		$scope.tabletitle = $scope.bckups;
		$scope.predicate = "vorder";
		$scope.reverse = true;
	};

	$scope.reorder = function(item, alter) {
		alter = alter || "";
		if (alter !== "")  item = alter;
		$scope.reverse = ($scope.predicate === item) ? !$scope.reverse : false;
		$scope.predicate = item;
	};
        
        $scope.sortdate =function(x,curr){
             var m_names = ['January', 'February', 'March', 
               'April', 'May', 'June', 'July', 
               'August', 'September', 'October', 'November', 'December'];
                 var d = new Date(),n;
             if(x==='month'){ 
                 var m=new Date(curr); 
                n = m_names[m.getMonth()];
            }
             if(x==='lastmonth'){n = m_names[d.getMonth()-1]; }
             if(x==='year'){  var y=new Date(curr);  n = y.getFullYear();}
             if(x==='lastweek'){
                var firstday = new Date(d.setDate(d.getDate() - d.getDay() - 7));
                var lastday = new Date(d.setDate(d.getDate() - d.getDay() + 6));
                var date = new Date(firstday),ldate =new Date(lastday),f,l;
                var month = date.getMonth() + 1;
                f = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
                l = ldate.getFullYear() + '-' + (ldate.getMonth() + 1) + '-' + ldate.getDate();
                n= f+"/"+l;

                }
             if(x==='week'){ 
                var firstday = new Date(d.setDate(d.getDate() - d.getDay()));
                var lastday = new Date(d.setDate(d.getDate() - d.getDay()+6));
                var date = new Date(firstday),ldate =new Date(lastday),f,l;
                  f = date.getFullYear() + '-' + (date.getMonth()+1) + '-' + date.getDate();
                  l = ldate.getFullYear() + '-' + (ldate.getMonth()+1) + '-' + ldate.getDate();
                  n= f+"/"+l;
            }
             return n;
        };
        $scope.getData = function(x){
            var url = "../api/getspoolhistory/" + x + "/1";
            $http.get(url).then(function(response) {
                    var items =$scope.roles;
                $scope.selectedItem=[];
                $scope.names = [];
                    var data = response.data.data.history;
                    
                    for (i = 0; i < data.length; i++) {
                        if(data[i].restaurant==='SG_SG_R_TheOneKitchen'){
                           console.log(data[i].count);
                           console.log(data[i].type);
                    }
                            if(data[i].email!==""){
                                data[i].index = data[i].ID;
                            if(x!=='day'){
                                 var n =$scope.sortdate(x,data[i].DATE);
                                    data[i].DATE =n;}
                                   var value = data[i];
                              $scope.names.push(new $scope.Objevent().replicate(value)); 
                        }
                    }
                    console.log($scope.names.length);
                  $scope.names = data;
            });
        }
    
         $scope.getData('day');
        

	$scope.cleaninput = function(ll) {
		if(typeof $scope.selectedItem[ll] === 'string')
			$scope.selectedItem[ll] =  $scope.selectedItem[ll].replace(/\'|\"/g, '’');
		};
		
	$scope.reset = function(item) {
		$scope.listEventFlag = false;
		$scope.viewEventFlag = false
		$scope.createEventFlag = false;
		$scope[item] = true;	
		};

	$scope.backlisting = function() {
		$scope.reset('listEventFlag');
		};
		

   		
	$scope.view = function(oo) {
		$scope.selectedItem = oo;
		$scope.reset('viewEventFlag');
		};
                
           $scope.extractSelection = function() {

		var tt, sep, limit, maxlimit = 1000, exportselect = $scope.filteredEvent; // ajax call will not support more data
		limit = (exportselect.length <= maxlimit) ? exportselect.length : maxlimit;
		tt = sep = '';
		for (i = 0; i < limit; i++, sep = ', ') {
			u = exportselect[i];
			tt += sep + '{ ' +
			'"restaurant":"' + u.restaurant + '", ' +
			'"type":"' + u.type + '", ' +
                        '"date":"' + u.DATE + '", ' +
			'"TotalCount":"' + u.count + '" }';
		}
		tt = '{ "member":[' + tt + '] }';
		$scope.contentBkOfDay = tt;
		$('#contentBkOfDay').val(tt);
		$('#extractBkOfDay').submit();
		};

		
	
}]);

	
</script>

<script>
function listindex(em){
        var id =$(em).attr('for');
         $("#"+id).val(em.text);
         var tag =$(em).attr('data');
         getImages(em.text,$(em).attr('for'),tag);
        
        
 }
 function listimages(img){
     var id= $(img).attr('for');
       $("#"+id).val(img.text);
     
 }

 function getImages(restaurant,current,tag){
 
     $.ajax({
            url: '../api/restaurant/getpicture/'+restaurant,
            dataType: "json",
            type: "GET",
            success: function (data) {
                console.log(JSON.stringify(data));
 
                var dataArr = data.data.pictures, results = '';
//                console.log(JSON.stringify(data.data.cuisine));
                var currentvalue = "";
                for (var i = 0; i < dataArr.length; i++) {
                      results += "<li  for='"+ current + "' ><a  onclick='listimages(this);' class='dummy_itemrestaurant' for='"+tag+"' >"+dataArr[i]+"</a></li>"
               }
      
               $("ul[data='" + current +"']").html(results);
              

            },
            error: function () {

            },
            complete: function () {

            }
        });
     
 }
 function close_msg() {
        setTimeout(function () {
            $('#msg-alert').fadeOut('slow');
            $(".alert").css('display', 'none');
        }, 10000); // <-- time in milliseconds  
    }
 
 $(".add-btn").bind('click', function () {
       if($("#restaurant1_id").val()==='' || $("#restaurant2_id").val()==='' || $("#restaurant3_id").val()==='' || $("#imag1").val()==='' || $("#imag2").val()===''
            || $("#imag3").val()==='' || $("#title").val()==='' || $("#tag").val()===''|| $("#link").val()==='' || $("#description").val()===''){     
                    alert("Please completed all Fields!!.");
             }
       var url = '../api/home/categories';
        var msg = "You have successFully Added Category:";

            $.ajax({
                url: url,
                dataType: "json",
                type: "POST",
                data: {
                    title: $("#title").val(),
                    tag: $("#tag").val(),
                    link: $("#link").val(),
                    description: $("#description").val(),
                    image1:$("#imag1").val(),
                    image2:$("#imag2").val(),
                    image3:$("#imag3").val(),
                    rest1:$("#restaurant1_id").val(),
                    rest2:$("#restaurant2_id").val(),
                    rest3:$("#restaurant3_id").val()
                },
                success: function (data) {
                    if(data.errors==null){
                     $("#msg-alert").css('display', 'block');
                                $("#msg-alert").addClass('alert-success');
                                $("#msg-alert").text(msg);
                                close_msg();
                    }else{
                       $("#msg-alert").css('display', 'block');
                       $("#msg-alert").addClass('alert-danger');
                       $("#msg-alert").text("OOPS!!.Error in  data.");
                        close_msg(); 
                    }
                  
                }
            });

       
   });
</script>


