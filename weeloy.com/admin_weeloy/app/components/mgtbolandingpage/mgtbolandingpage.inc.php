<div class="container">
    <div class="row">
        <div class="col-md-12 left-sec">
<div ng-controller="CategoryController" ng-init="moduleName='cateory'; listCategoryFlag = true; viewCategoryFlag=false; createCategoryFlag=false;" >
	<div id='listing' ng-show='listCategoryFlag'>
		<div class="form-group"  style='margin-bottom:25px;'>
			<div class="col-md-4">
				<div class="input-group col-md-4">
					<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
					<input type="text" class="form-control input-sm" ng-model="searchText" ng-change="paginator.setPage(0);" style='width:200px;' autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" > 
				</div>
			</div>
			<div class="col-md-2"></div>
			<div class="col-md-4">
			<a href ng-click='create();' class="btn btn-success btn-sm" style='color:white;width:200px;'><span class='glyphicon glyphicon-certificate'></span> &nbsp;Create a New Category</a>
			</div>
		</div>
        <div style=" clear: both;"></div>
		<table width='100%' class="table table-condensed table-striped" style='font-size:12px;'>
			<thead>
				<tr>
				<th ng-repeat="y in tabletitle"><tbtitle var="{{y.a}}" name='tabletitle' module='moduleName'/></th>
				<th>update</th><th> &nbsp; </th>
<!--                                <th>delete</th><th> &nbsp; </th>-->
                             
				</tr>
			 </thead>
			<tbody style='font-family:helvetica;font-size:12px;'>
				<tr ng-repeat="x in filteredEvent = (names | filter:searchText) | sizefilter:this | orderBy:predicate:reverse | slicefilter:paginator.getPage():paginator.getRowperPage():paginator.getItemCount() | limitTo:paginator.getRowperPage()" style='font-family:helvetica;font-size:11px;'>
                                    
                                    <td ng-repeat="y in tabletitle">
                                        <a href ng-click="view(x)"> {{ x[y.a] | adatereverse:y.c }}</a>
                                    </td>
					<td><a href ng-click="update(x)" style='color:blue;'><span class='glyphicon glyphicon-pencil'></a></span></td>
<!--                                       <td width='30'>&nbsp;</td>
					<td nowrap><a href ng-click="delete(x)" style='color:red;'><span class='glyphicon glyphicon-trash'></a></span></td>-->
					
				</tr><tr><td colspan='{{tabletitle.length + 3}}'></td></tr>
			</tbody>
		</table>
		<div ng-if="filteredEvent.length >= paginator.getRowperPage()" class="col-md-12" align="center" style='font-size:11px' ng-include="'app/components/paginator.html'"></div>
	</div>

	<div class="col-md-12" ng-show='viewCategoryFlag'>
		<br /><a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a><br />
		<table class='table-striped' style="margin: 0 0 150px 30px;font-size:13px;font-family: Roboto">		
			<tr ng-repeat="y in tabletitleContent | filter: {b: '!picture'}"><td nowrap ><strong> {{ y.b }} </strong></td><td> &nbsp; </td><td>{{ selectedItem[y.a] | adatereverse:y.c }} </td></tr>
			<tr ng-if="selectedItem['picture'] != ''"><td nowrap><strong>Picture: </strong></td><td> &nbsp; </td><td class="showpict"><img ng-src="{{path}}{{selectedItem.picture}}" height='150'/></td></tr>
		</table>
	</div>

	<div class="col-md-12" ng-show='createCategoryFlag'>
		<br />
		<a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a>
		<br />
		 <div class="col-md-2"></div>
		 <div class="col-md-8">
		   <div class="row" ng-repeat="y in tabletitleContent | filter: {t: '!dontshow' }"  style="margin: 0 0 10px 0;font-size:12px;font-familly:Roboto">
          
                            <div class="input-group"  ng-if="y.t === 'input'">
                                    <span class="input-group-addon input11"><i class="glyphicon glyphicon-{{y.d}} input14"></i>&nbsp;{{y.b}}</span>
                                    <input type="text" class="form-control input-sm" ng-model="selectedItem[y.a]" ng-change="cleaninput(y.a)"  >
                            </div>
                                <div class="input-group" ng-if="y.t === 'dropdown'">
                                        <div class='input-group-btn' dropdown >
                                        <button type='button' class='btn btn-default dropdown-toggle btn-sm input11' data-toggle='dropdown'>
                                                <i class="glyphicon glyphicon-{{y.d}} input13"></i>&nbsp; {{y.b}}<span class='caret'></span></button>
                                        <ul class='dropdown-menu scrollable-menu'style='height: auto;max-height:120px; overflow-x: hidden;' >
                                        <li ng-repeat="p in y.val"><a href ng-click="selectedItem[y.a]=p;y.func()">{{ p }}</a></li>
                                        </ul>
                                        </div>
                                        <input type='text' ng-model='selectedItem[y.a]' class='form-control input-sm' readonly >
                                </div>
                                <div class="input-group" ng-if="y.t === 'array'">
                                        <div class='input-group-btn' dropdown >
                                        <button type='button' class='btn btn-default dropdown-toggle btn-sm input11' data-toggle='dropdown'>
                                                <i class="glyphicon glyphicon-{{y.d}} input13"></i>&nbsp; {{y.b}}<span class='caret'></span></button>
                                        <ul class='dropdown-menu scrollable-menu'style='height: auto;max-height:120px; overflow-x: hidden;' >
                                        <li ng-repeat="p in y.val"><a href ng-click="selectedItem[y.a]=p;y.func()">{{ p }}</a></li>
                                        </ul>
                                        </div>
                                        <input type='text' ng-model='selectedItem[y.a]' class='form-control input-sm' readonly >
                                </div>
                              <div class="input-group"  ng-if="y.t==='checkbox' && is_email==='weeloy.com' ">
                                <input   type="checkbox" ng-model="selectedItem[y.a]" ng-checked ="{{selectedItem[y.a]}}"> &nbsp; {{y.b}}
                               </div> 
                                <div class="input-group"  ng-if="y.t === 'inputtag' && selectedItem['is_tag']===true ">
                                    <span class="input-group-addon input11"><i class="glyphicon glyphicon-{{y.d}} input14"></i>&nbsp;{{y.b}}</span>
                                    <input type="text" class="form-control input-sm" ng-model="selectedItem[y.a]" ng-change="cleaninput(y.a)"  >
                                </div>
                                <div class="input-group"  ng-if="y.t === 'inputlink' && selectedItem['is_tag']===false ">
                                    <span class="input-group-addon input11"><i class="glyphicon glyphicon-{{y.d}} input14"></i>&nbsp;{{y.b}}</span>
                                    <input type="text" class="form-control input-sm" ng-model="selectedItem[y.a]" ng-change="cleaninput(y.a)"  >
                                </div>
                                <div class="input-group"  ng-if="y.t === 'inputtype'  && selectedItem['is_mobile']===true">
                                        <span class="input-group-addon input11"><i class="glyphicon glyphicon-{{y.d}} input14"></i>&nbsp;{{y.b}}</span>
                                        <input type="text" class="form-control input-sm" ng-model="selectedItem[y.a]" ng-change="cleaninput(y.a)"  >
                                </div>
                                <div class="input-group" ng-if="y.t === 'imagebutton'">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-file"></i></span>
                                    <input type="file" name="file_upload" id="file_upload" class="form-control input-sm" onchange="angular.element(this).scope().uploadFiles(this)" ng-model="nfiles" multiple placeholder="ipload files">  
                                </div>
                                    

                                <div class="input-group" ng-if="y.t === 'pictureshow1'">
                                    <p ng-if="selectedItem.image1 != ''"><img ng-src="{{path}}{{selectedItem.restaurant1}}/{{selectedItem.image1}}" height='150'/></p>
                                    <p ng-if="selectedItem.image2 != ''"><img ng-src="{{path}}{{selectedItem.restaurant2}}/{{selectedItem.image2}}" height='150'/></p>
                                    <p ng-if="selectedItem.image2 != ''"><img ng-src="{{path}}{{selectedItem.restaurant3}}/{{selectedItem.image3}}" height='150'/></p>
                                </div>
		   </div><br />
                  
                      <div id="progressbox" style="display:none;" ><div id="progressbar" ></div><div id="statustxt">0%</div> </div>
                   
		</div>
		<div class="col-md-2"></div>
		<div class="col-md-7"></div>
		<div class="col-md-5">
			<a href ng-click='savenewevent();' class="btn btn-success btn-sm" style="color:white;width:150px"><i class='glyphicon glyphicon-save'></i> &nbsp;{{ buttonlabel }} </a><br />
		</div>
		</div>
</div>

</div>
</div>
</div>
<style>
    #progressbox {
    border: 1px solid #0099CC;
    padding: 1px; 
    position:relative;
    width:400px;
    border-radius: 3px;
    margin: 10px;
    display:none;
    text-align:left;
    }
    #progressbar {
    height:20px;
    border-radius: 3px;
    background-color: #5bc0de;
    width:1%;
    }
    #statustxt {
    top:3px;
    left:50%;
    position:absolute;
    display:inline-block;
    color: #000000;
    }
</style>

<script>

<?php
    $mediadata = new WY_Media();
    $resdata = new WY_restaurant();
    //$imgAr = $mediadata->getEventPictureNames($theRestaurant);
        $resAr= $resdata->getSimpleListRestaurant();


    printf("var pathimg = '%s';", $mediadata->getFullPath('small'));


      printf("var resArr = [");
            for($i = 0, $sep = ""; $i < count($resAr); $i++, $sep = ", "){
                printf("%s '%s'", $sep, $resAr[$i]);
            }
    printf("];");
            
    
?>

var token = <?php echo "'".$_SESSION['user_backoffice']['token']."';"; ?>
var email =  <?php echo "'".$_SESSION['user_backoffice']['email']."';";?>

app.controller('CategoryController', ['$scope','$http','$timeout', function($scope,$http,$timeout) {
       
	var todaydate = new Date();
	var aorder = (function() { var i, arr=[]; for(i = 0; i < 30; i++) arr.push(i); return arr; })();
	
	$scope.paginator = new Pagination(100);
	$scope.path = pathimg;
	$scope.restaurant = <?php echo "'".$theRestaurant."';"; ?>
	$scope.email = <?php echo "'" . $email . "';"; ?>
	$scope.predicate = '';
	$scope.reverse = false;
	$scope.nonefunc = function() {};
        $scope.resArr = resArr;
       var status=['active','inactive'];
	$scope.tabletitle = [ {a:'index', b:'ID', c:'' , q:'down', cc: 'black' }, {a:'title', b:'Title', c:'' , q:'down', cc: 'black' }, {a:'tag', b:'Tag', c:'' , q:'down', cc: 'black' }, {a:'description', b:'Description', c:'' , q:'down', cc: 'black' }, { a:'link', b:'Link', c:'' , q:'down', cc: 'black' }, , {a:'status', b:'Status', c:'' , q:'down', cc: 'black' }, {a:'is_mobile', b:'Mobile', c:'' , q:'down', cc: 'black' },{a:'morder', b:'Order', c:'' , q:'down', cc: 'black' },{a:'type', b:'Type', c:'' , q:'down', cc: 'black' } ];
	$scope.bckups = $scope.tabletitle.slice(0);
	$scope.tabletitleContent = [ { a:'title', b:'Title', c:'', d:'tower', t:'input', i:0 }, { a:'description', b:'Description', c:'', d:'flag', t:'input', i:0 },{ a:'city', b:'City', c:'', d:'flag', t:'input', i:0 },{ a:'status', b:'Status', c:'', d:'sort', t:'dropdown', val:status,func: $scope.nonefunc},{ a:'morder', b:'Preference Order', c:'', d:'sort', t:'array', val: aorder, func: $scope.nonefunc },{ a:'is_mobile', b:'Is_mobile', c:'', d:'flag', t:'checkbox', i:0},{ a:'is_tag', b:'Is_tag', c:'', d:'flag', t:'checkbox', i:0},{ a:'type', b:'Type', c:'', d:'flag', t:'inputtype', i:0 },{ a:'tag', b:'Tag', c:'', d:'tag', t:'inputtag', i:0 },{ a:'link', b:'Link', c:'', d:'tag', t:'inputlink', i:0 },{ a:'picture', b:'Picture', c:'', d:'picture', t:'pictureshow1' },{a:'image', b:'SelectOnImage', c:'', d:'picture', t:'imagebutton' }  ];
	//$scope.imgEvent = imgEvent.slice(0);
          
        var mailArr = email.split('@');
        $scope.is_email =mailArr[1];
        //$scope.is_email='gmail.com';
        $scope.path ="https://media.weeloy.com/upload/restaurant/";
	$scope.Objevent = function() {
		return {
			restaurant: $scope.restaurant, 
			index: 0, 
			title: '', 
			tag: '', 
                        link: '', 
                        type:'',
                        status:'',
                        is_mobile:'',
			description: '', 
			restaurant1: '', 
			image1: '', 
                        restaurant2: '', 
			image2: '', 
                        restaurant3: '', 
			image3: '', 
                        morder:'',
                        city:'',
                        is_tag:'',

			remove: function() { 
				for(var i = 0; i < $scope.names.length; i++) 
					if($scope.names[i].name === this.name) {
						$scope.names.splice(i, 1);
						break;
						}
				},
				
			replicate: function(obj) {
				for (var attr in this)
		        	if(this.hasOwnProperty(attr) && typeof this[attr] !== 'function' && attr !== '$$hashKey') 
        				this[attr] = obj[attr];
				return this;
				},
										
			clean: function() {
				for (var attr in this)
		        	if(this.hasOwnProperty(attr) && typeof this[attr] !== 'function' && attr !== '$$hashKey') {
        				if(typeof this[attr] === 'string') {
        					this[attr] = this[attr].replace(/\'|\"/g, '’');
        					}
        				}
				return this;
				},
					
			check: function() {
				if(this.name === '' || this.name.length < 6) { alert('Invalid title name, empty or too short(5)'); return -1;}
				if(this.title === '' || this.title.length < 3) { alert('Invalid title name, empty or too short(2)'); return -1;}
				return 1;
				}
			};
	};
		

        var type='admin';
        var url ="../api/home/getcategories/"+type;
        $http.get(url).then(function(response) {
            $scope.selectedItem=[];
            $scope.names = [];	 	
		data = response.data.data.categories;
                for (var i = 0; i< data.length; i++) {
                    if(data[i].type!=='footer'){
                        data[i].index = data[i].id;
                          data[i].is_mobile = (data[i].is_mobile==='1') ? true : false;
                        var img= data[i].images;
                        var value = data[i];
                       for(var k=0;k<img.length;k++){
                           value.restaurant1= img[0].restaurant.trim();
                           value.image1= img[0].image;
                           if(img.length>1){
                               value.restaurant2=img[1].restaurant.trim();
                                value.image2= img[1].image;
                           }
                           if(img.length>2){
                               value.restaurant3=img[2].restaurant.trim();
                                value.image3= img[2].image;
                           }

                       }

                         $scope.names.push(new $scope.Objevent().replicate(value)); 
                    }
                }

        });


	$scope.cleaninput = function(ll) {
		if(typeof $scope.selectedItem[ll] === 'string')
			$scope.selectedItem[ll] =  $scope.selectedItem[ll].replace(/\'|\"/g, '’');
		};
		
	$scope.reset = function(item) {
		$scope.listCategoryFlag = false;
		$scope.viewCategoryFlag = false
		$scope.createCategoryFlag = false;
		$scope[item] = true;	
		};

	$scope.backlisting = function() {
		$scope.reset('listCategoryFlag');
		};
		
	$scope.findaccount = function(name) {
         var data =$scope.names;
		for(var i = 0; i < data.length; i++){
                    if(data[i].index.trim()===name.trim()){
                           return i;
                    }
                }
                return -1;
                 

		};
   		
	$scope.view = function(oo) {
		$scope.selectedItem = oo;
		$scope.reset('viewCategoryFlag');
		};
	
	$scope.create = function() {
            
		$scope.selectedItem = new $scope.Objevent();
                $scope.selectedItem.is_tag = true;
		$scope.reset('createCategoryFlag');
		$scope.buttonlabel = "Save new categories";
		$scope.action = "create";
		return false;
		}
                
				
	$scope.update = function(oo) {

                if(oo['tag']==="" ||  oo['tag']==='undefiend' ){
                     oo['is_tag']= false;
                 }else{oo['is_tag']=true;}
                  $scope.selectedItem = new $scope.Objevent().replicate(oo);
                    $scope.reset('createCategoryFlag');
                    $scope.buttonlabel = "Update categories";
                    $scope.action = "update";
		};

	$scope.savenewevent = function() {
		var u, msg, apiurl, ind;
		
		$scope.selectedItem.clean();
                if($scope.selectedItem.is_mobile === false){
                   $scope.selectedItem.type=""; 
                }

		if($scope.action == "update") {
                    ind = $scope.findaccount($scope.selectedItem.index);
                  
			if(ind >= 0) $scope.names.splice(ind, 1);
			$scope.names.push($scope.selectedItem);
			}

		else if($scope.action == "create") { 
               
                    
			$scope.names.push($scope.selectedItem);
                      
			}
		
		if($scope.action === "create"){
            
                          $http.post("../api/home/categories",
			{
                           'title' : $scope.selectedItem.title,
                           'tag'  : $scope.selectedItem.tag,
                           'imgtype':"home",
                           'type':$scope.selectedItem.type,
                           'description'  : $scope.selectedItem.description,
                           'restaurant1'  : $scope.selectedItem.restaurant1,
                           'restaurant2'  : $scope.selectedItem.restaurant2,
                           'restaurant3'  : $scope.selectedItem.restaurant3, 
                           'image1'  : $scope.selectedItem.image1,
                           'image2'  : $scope.selectedItem.image2,
                           'image3'  : $scope.selectedItem.image3,
                           'status':$scope.selectedItem.status,
                           'link':$scope.selectedItem.link,
                           'is_mobile':$scope.selectedItem.is_mobile,
                           'is_tag':$scope.selectedItem.is_tag,
                           'morder':$scope.selectedItem.morder,
                           'city':$scope.selectedItem.city
        		}).then(function(response) { alert("Categories has been created");});
                    }

		else{
                   
                     $http.post("../api/home/updatecategories",
			{ 'categories':$scope.selectedItem}).then(function(response) {alert("Categories has been created");});
                  }
		
 		$scope.backlisting();		
		};
                
                
                $scope.uploadFiles = function()	{
                    var object_type = "home";
                    dom = $('#file_upload')[0];
                    files = dom.files;  //get the file
                    if(files.length == 0) {
                            alert("Select/Drop one file");
                            return false;
                            }
                 
                for (var i = 0; i < files.length; i++) {
                        var f = files[i];
                        filename = f.name;
                      ext = filename.substring(filename.lastIndexOf(".")+1).toLowerCase();
                        if(ext != "jpg" && ext != "jpeg" && ext != "png" && ext != "gif") {
                                alert("Invalid file type (jpg, jpeg, png, gif)");
                                return false;
                                }
                        if(object_type == "") {
                                alert("Please Choose a category");
                                return false;
                                }
                       if($scope.selectedItem.title===''){
                            alert("Please complete title and link/tag!.");
                            return false;
                        }
                    $scope.selectedItem.name = files[i].name;
                    var restType = $scope.selectedItem.tag;
                    if($scope.selectedItem.is_tag===false){
                       restType =$scope.selectedItem.title.replace(/ /g,"_"); 
                     
                    }
                
                     $scope.selectedItem.restaurant1 =restType;
                                $scope.selectedItem.restaurant2 =restType; 
                                $scope.selectedItem.restaurant3 =restType; 
                               if(files[0].name && typeof files[0].name!='undfined'){
                                   $scope.selectedItem.image1 =(files[0].name!='undfined') ? files[0].name : "";
                               }
                               if(files.length>1){
                                    if(files[1].name && typeof files[1].name!='undfined'){
                                        $scope.selectedItem.image2 =(files[1].name!='undfined') ? files[1].name : "";
                                    }
                                }
                                if(files.length>2){
                                    if(files[2].name && typeof files[2].name!='undfined'){
                                        $scope.selectedItem.image3 =(files[2].name!='undfined') ? files[2].name : "";
                                    }
                                }
                    var data = new FormData();
                    data.append('file', f);
                    data.append('token', token);
                    data.append('restaurant',restType );
                    data.append('category', 'category');
                         $("#progressbox").css('display','block');
                    $.ajax({
                        url: 'saveimages.php?files[i]',
                        type: 'POST',
                        data: data,
                        cache: false,
                        dataType: 'json',
                        xhr: function() {
                            var myXhr = $.ajaxSettings.xhr();
                            if(myXhr.upload){
                                myXhr.upload.addEventListener('progress',progressHandlingFunction, false);
                            }
                            return myXhr;
                        },
                        cache:false,
                           processData: false, // Don't process the files
                        contentType: false, // Set content type to false as jQuery will tell the server its a query string request
                        success: function(data, textStatus, jqXHR)
                            {
                                 $("#progressbox").css('display','none');
                            if(typeof data.error === 'undefined'){
                                
                                $scope.path ="https://media.weeloy.com/upload/"+object_type+"/"+restType+"/"+files[i].name;
                                 }
                                    //$scope.submitForm(dom, data);
                            else {
                                    alert(data.error);
                                    $scope.selectedItem.remove();
                                    $scope.$apply();
                                    }
                         },
                        error: function(jqXHR, textStatus, errorThrown) {
                                    alert(textStatus);		
                        }
                    });
                }
 
		
		$scope.selectedItem.object_type = object_type;
		$scope.selectedItem.media_type = 'picture';
		$scope.selectedItem.path = '';
		$scope.selectedItem.status = 'active';
	
		

              
                
            };
            function progressHandlingFunction(e){
                    console.log("even="+JSON.stringify(e));
                    var max = e.total;
                    var current = e.loaded;
                   var Percentage = (current * 100)/max;
                   var percent = (current / max) * 100;
                  
                     if(percent>80)
                        {
                              console.log(percent);
                            $("#statustxt").css('color','#fff'); //change status text to white after 50%
                        }
                    $('#progressbar').width(percent + '%') //update progressbar percent complete
                    $('#statustxt').html(percent + '%');
            }
		
	$scope.delete = function(oo) {
  
		if(confirm("Are you sure you want to delete " + oo.title) == false)
			return;
	$http.post("../api/home/deletecategories",
			{ 'categories':oo}).then(function() {oo.remove();
			alert(oo.name + " has been deleted");});

		
		};
}]);

	
</script>







