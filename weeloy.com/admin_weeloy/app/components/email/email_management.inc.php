<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 



    mng_emailmnCookie('emailMangement');
$accountemail = $_SESSION['user']['email'];

function languageopt($title,$label,$tag) {
    $lanList = getLangList();
    echo " <div id='accordion' class='panel-group'>
            <div class='panel panel-default'>
                <div class='panel-heading'>
                  <h4 class='panel-title'>
                    <a data-toggle='collapse' href='#$label'> $title </a>
                  </h4>
                </div>
                <div id='$label' class='panel-collapse collapse '>
                <div class='panel-body' >
                    <div class='form-group row'>
                    <div class='col-sm-6'>
                    <div class='input-group'>
                <div class='input-group-btn'><button data-toggle='dropdown' class='btn btn-info btn-sm dropdown-toggle' type='button'>Language
                    <span class='caret'></span></button>
                    <ul role='menu' class='dropdown-menu dropdown_itemlanguage scrollable-menu' style='height: auto;max-height: 350px; overflow-x: hidden;'>";
                        for ($i = 0; $i < count($lanList); $i++) {
                            $index = $i + 1;
                            $var = $lanList[$i]['a'];
                            $op = $lanList[$i]['b'];
                            printf("<li>
                            <a id='rest_$index' for='sdasd' class='dummy_itemrestaurant' value='$op' onclick='listindex(this);' >$var </a>
                            </li>");
                            }
                   echo  "</ul>
                </div>
                <input type='text' readonly='' for='' id='itemlanguage' name='itemlanguage' class='form-control input-sm itemlanguage' > 
                
            </div>
             
           </div> 
           <div class='col-md-2'>
                 <a  href=\"javascript:setViewBar('$tag');\" class='mybarnav btn btn-info btn-sm' style='color:white;width:100px'>View</a>
            </div>
            </div>
            </div>
            </div>
            </div>
            </div>";
       
                 
}
function getData($type,$mode='member'){
    
    $bookings = new WY_Booking($email);
    $bookings->getLatestBookings($type,$mode);
    $review = new WY_Review();
        if($type==='review'){
            $review->getLatestview();
            $bookings->getBooking($review->confirmation);
            return array('booking'=>$bookings, 'review'=>$review);  
           
        }
     $dataobject =$bookings;
        return $dataobject;
}

function getLangList() {
    return $lang = array(
        array('a' => 'English', 'b' => 'en'),
        array('a' => '華文', 'b' => 'cn'),
        array('a' => '文言', 'b' => 'hk'),
        array('a' => 'ภาษาไทย', 'b' => 'th'),
        array('a' => '日本語', 'b' => 'jp'),
        array('a' => '한국어', 'b' => 'kr'),
        array('a' => 'Tiếng Việt', 'b' => 'vn'),
        array('a' => 'Русский', 'b' => 'ru'),
        array('a' => 'Français', 'b' => 'fr'),
        array('a' => 'Español', 'b' => 'es'),
        array('a' => 'Italiano', 'b' => 'it'),
        array('a' => 'Deutsch', 'b' => 'de'),
        array('a' => 'Portuguese', 'b' => 'pt')
    );
}
function getBkTemplate($label){
  
     echo "<form enctype='multipart/form-data' name='idFormsQuery3' id='idFormsQuery3' method='POST' action='bktemplate' >
            <input type='hidden' value='$label' id='navbar' name='navbar'>
            <input type='hidden' value='' id='action_type' name='action_type'>
            <input type='hidden' value='' id='theMember' name='theMember'>
            <input type='hidden' value='83' id='initstate' name='initstate'>
             <input type='hidden' name='language' id ='itemlage' value='en' />
            </form></br><br />";
  
        $lang = $_REQUEST['emlang'];

        $content['type'] = 'member';
        $trans = new WY_Translation;
        $mailer = new JM_Mail;
        $content = $trans->getEmailContent('EMAIL', $lang);
    //var_dump($content);
    if ($label == 'ED_BKCONFIRLIST') {

        $dataobject = getData('member','listing');
        $dataobject->labelContent = $content;
        $body = $mailer->getTemplate('confirmation/listing/member/weeloy/confirmation', $dataobject);
    }
        
    else if($label == 'ED_BKCONFIRMATION'){
         $dataobject = getData('member');
         $dataobject->labelContent = $content;
         $body = $mailer->getTemplate('confirmation/booking/member/weeloy/confirmation', $dataobject);
      
        
         //include('../templates/email/booking/notifybooking_member.html.twig');
    }
    else if ($label == 'ED_BKREQUEST') {
         $dataobject = getData('member','request');
                 $dataobject->labelContent = $content;
          $body = $mailer->getTemplate('confirmation/request/member/weeloy/confirmation', $dataobject);

    }
    else if ($label == 'ED_RBKCONFIRMATION') {
         $dataobject = getData('restaurant');
         $dataobject->labelContent = $content;
         $body = $mailer->getTemplate('confirmation/booking/restaurant/weeloy/confirmation', $dataobject);

    }
    else if ($label == 'ED_RBKREQUEST') {
          $dataobject = getData('restaurant','request');
          $dataobject->labelContent = $content;
          $body = $mailer->getTemplate('confirmation/request/restaurant/weeloy/confirmation', $dataobject);

    }

    else if ($label == 'ED_RBKCONFIRLIST') {

        $dataobject = getData('restaurant','listing');
        $dataobject->labelContent = $content;
        $body = $mailer->getTemplate('confirmation/listing/restaurant/weeloy/confirmation', $dataobject);
    }
    else if ($label == 'ED_BKUPDATE') {
        $dataobject = getData('member');
        $dataobject->labelContent = $content;
        $body = $mailer->getTemplate('update/member/weeloy/update', $dataobject);
    }
    else if ($label == 'ED_RKUPDATE') {
        $dataobject = getData('restaurant');
        $dataobject->labelContent = $content;
        $body = $mailer->getTemplate('update/restaurant/weeloy/update', $dataobject);
    }
    else if ($label == 'ED_BKREMINDER') {
        $dataobject = getData('member');
        $dataobject->labelContent = $content;
        $body = $mailer->getTemplate('booking/notifybooking_reminder', $dataobject);
    }
     else if ($label == 'ED_BKREMINDERLISTING') {

        $dataobject = getData('member','listing');
        $dataobject->labelContent = $content;
        $body = $mailer->getTemplate('booking/notifylisting_reminder', $dataobject);
    }
     else if ($label == 'ED_BKREMINDERREQUEST') {

        $dataobject = getData('member','request');
        $dataobject->labelContent = $content;
        $body = $mailer->getTemplate('booking/notifyrequest_reminder', $dataobject);
    }
    

    echo "<a class='mybarnav' href=\"javascript:setViewBar('ED_BOOKING');\">Back </a>";
     print($body);

    
}
function getWinTemplate($navbar){

        $lang = $_REQUEST['emlang'];
        $trans = new WY_Translation;
        $content = $trans->getEmailContent('EMAIL', $lang);
            $mailer = new JM_Mail;
        if($navbar == 'ED_BKWHELLWIN'){
            $dataobject = getData('member');
            $dataobject->labelContent = $content;
            $body = $mailer->getTemplate('wheel_win/member/weeloy/win', $dataobject);
        }
        if($navbar == 'ED_RKWHELLWIN'){
            $dataobject = getData('restaurant');
            $dataobject->labelContent = $content;
            $body = $mailer->getTemplate('wheel_win/restaurant/weeloy/win', $dataobject);

        }
        echo "<a class='mybarnav' href=\"javascript:setViewBar('ED_WHELLWIN');\">Back </a>";
         print($body);
        
        
}
function getNoshowTemplate($navbar){

        $lang = $_REQUEST['emlang'];
        $trans = new WY_Translation;
        $content = $trans->getEmailContent('EMAIL', $lang);
            $mailer = new JM_Mail;
        if($navbar == 'ED_BKNOSHOW'){
            $dataobject = getData('member');
            $dataobject->labelContent = $content;
            $body = $mailer->getTemplate('noshow/member/weeloy/noshow', $dataobject);
        }
        if($navbar == 'ED_BKWLNOSHOW'){
            $dataobject = getData('member');
            $dataobject->labelContent = $content;
            $body = $mailer->getTemplate('noshow/member/white_label/noshow', $dataobject);
        }
        echo "<a class='mybarnav' href=\"javascript:setViewBar('ED_NOSHOW');\">Back </a>";
         print($body);
        
        
}
function getCallTemplale($navbar){
  
        $lang = $_REQUEST['emlang'];

        $mailer = new JM_Mail;
        $trans = new WY_Translation;
        $content = $trans->getEmailContent('EMAIL', $lang);
        if($navbar == 'ED_BKCALLCENTER'){
             $dataobject = getData('member');
          $dataobject->labelContent = $content;
          $body = $mailer->getTemplate('confirmation/booking/member/white_label/confirmation', $dataobject);

        }
        if($navbar == 'ED_RKCALLCENTER'){
            $dataobject = getData('restaurant');
            $dataobject->labelContent = $content;
            $body = $mailer->getTemplate('confirmation/booking/restaurant/white_label/confirmation', $dataobject);
        }
         if($navbar == 'ED_BKCAllUPDATE'){
            $dataobject = getData('member');
            $dataobject->labelContent = $content;
            $body = $mailer->getTemplate('update/member/white_label/update', $dataobject);
        }
         if($navbar == 'ED_RKCAllUPDATE'){
            $dataobject = getData('restaurant');
            $dataobject->labelContent = $content;
            $body = $mailer->getTemplate('update/restaurant/white_label/update', $dataobject);
        }
         echo "<a class='mybarnav' href=\"javascript:setViewBar('ED_CALLCENTER');\">Back </a>";
         print($body);
}
function getSmsTemplate($navbar){
  
 $lang = $_REQUEST['emlang'];
        $trans = new WY_Translation;
        $content = $trans->getEmailContent('EMAIL', $lang);
        $mailer = new JM_Mail;
        $content['type'] = 'sms-res';
        $dataobject = getData('restauarant');
        $dataobject->labelContent = $content;
        $body = $mailer->getTemplate('sms_response/restaurant/white_label/reply', $dataobject);
             echo "<a class='mybarnav' href=\"javascript:setViewBar('ED_BOOKING');\">Back </a>";
              print($body);
         
}
function getRegisterTemplate($navbar){
  
     $lang = $_REQUEST['emlang'];


        $trans = new WY_Translation;
        $content = $trans->getEmailContent('EMAIL', $lang);
        $mailer = new JM_Mail;
        
         $dataobject = getData('member');
            $dataobject->labelContent = $content;
            $body = $mailer->getTemplate('register/register_member', $dataobject);
                 echo "<a class='mybarnav' href=\"javascript:setViewBar('EMAIL MANAGEMENT');\">Back </a>";
                  print($body);

}

function getCategringTemplate($navbar){
        $lang = $_REQUEST['emlang'];
        
        $trans = new WY_Translation;
        $content = $trans->getEmailContent('EMAIL', $lang);
         $mailer = new JM_Mail;
        if($navbar == 'ED_CRMEMBER'){
            $dataobject = getData('member');
            $dataobject->labelContent = $content;
            $body = $mailer->getTemplate('catering/notify_member', $dataobject);
      
        }
        if($navbar == 'ED_CRRESTRAURANT'){
            $dataobject = getData('restauarant');
            $dataobject->labelContent = $content;
            $body = $mailer->getTemplate('catering/notify_restaurant_catering', $dataobject);
        }
        echo "<a class='mybarnav' href=\"javascript:setViewBar('ED_CATERING');\">Back </a>";
        print($body);
        
     
    
}
function getCancelTemplate($navbar){

        $lang = $_REQUEST['emlang'];

        $trans = new WY_Translation;
          $mailer = new JM_Mail;
        $content = $trans->getEmailContent('EMAIL', $lang);
        if($navbar == 'ED_BKCANCEL'){
              $dataobject = getData('member');
            $dataobject->labelContent = $content;
            $body = $mailer->getTemplate('cancel/member/weeloy/cancel', $dataobject);
  
        }
        if($navbar == 'ED_RKCANCEL'){
               $dataobject = getData('restuarant');
            $dataobject->labelContent = $content;
            $body = $mailer->getTemplate('cancel/restaurant/weeloy/cancel', $dataobject);
        }

        echo "<a class='mybarnav' href=\"javascript:setViewBar('ED_BOOKING');\">Back </a>";
         print($body);
            
         
}
function getRevTempalte($navbar){

        $lang = $_REQUEST['emlang'];


        $trans = new WY_Translation;
        $content = $trans->getEmailContent('EMAIL', $lang);
        $mailer = new JM_Mail;
        if($navbar == 'ED_BKREVIEW'){
            $dataobject = getData('review');
            $dataobject['labelContent'] = $content;
            $body = $mailer->getTemplate('review/notifyreview_member', $dataobject);
           
        }
        if($navbar == 'ED_RKREVIEW'){
            $dataobject = getData('review');
            $dataobject->labelContent = $content;
            $body = $mailer->getTemplate('review/restaurant/weeloy/review', $dataobject);
        }
        if($navbar == 'ED_BKREVIEWREMINDER'){
        
            $dataobject = getData('member');
            $dataobject->labelContent = $content;
            
            $body = $mailer->getTemplate('reminder/member/weeloy/review', $dataobject);
        }
        if($navbar == 'ED_RKREVIEWREMINDER'){
            $dataobject = getData('restaurant');
            $dataobject->labelContent = $content;
            $body = $mailer->getTemplate('review/notifyrequest_reviewreminder', $dataobject);
        }

       echo "<a class='mybarnav' href=\"javascript:setViewBar('ED_REVIEW');\">Back </a>";
       print($body);
        
}




if ($navbar == 'ED_REGISTER') {
     echo "<form enctype='multipart/form-data' name='idFormsQuery3' id='idFormsQuery3' method='POST' action='$action' >
                    
            <input type='hidden' value='$navbar' id='navbar' name='navbar'>
            <input type='hidden' value='' id='action_type' name='action_type'>
            <input type='hidden' value='$theMember' id='theMember' name='theMember'>
            <input type='hidden' value='83' id='initstate' name='initstate'>
             <input type='hidden' name='language' id ='itemlage' value='en' />
            </form></br><br />";
             printf(languageopt('Register','mb-register','ED_REGISTER'));
      
}


    if ($navbar == 'ED_BOOKING') {

        echo "<form enctype='multipart/form-data' name='idFormsQuery3' id='idFormsQuery3' method='POST' action='$action' >

                    <input type='hidden' value='$navbar' id='navbar' name='navbar'>
                    <input type='hidden' value='' id='action_type' name='action_type'>
                    <input type='hidden' value='$theMember' id='theMember' name='theMember'>
                    <input type='hidden' value='83' id='initstate' name='initstate'>
                     <input type='hidden' name='language' id ='itemlage' value='en' />
                    </form></br><br />";
                echo "<a class='mybarnav' href=\"javascript:setViewBar('EMAIL MANAGEMENT');\">Back to listing </a>";
                
                        printf(languageopt('Booking Confirmation','bk-confirm','ED_BKCONFIRMATION'));
                        printf(languageopt('Booking Listing','bk-listing','ED_BKCONFIRLIST'));
                        printf(languageopt('Booking Request','bk-lrequest','ED_BKREQUEST'));
                        //restaurant view
                        printf(languageopt('Restaurant View  Booking Confirmation','bk-rconfirm','ED_RBKCONFIRMATION'));
                        printf(languageopt('Restaurant View  Booking Listing','bk-rlisting','ED_RBKCONFIRLIST'));
                        printf(languageopt('Restaurant View  Booking Request','bk-rrequest','ED_RBKREQUEST'));
                        printf(languageopt('booking-update-member','bk-update','ED_BKUPDATE'));
                        printf(languageopt('booking-update-restaurant','bk-rupdate','ED_RKUPDATE'));
                        printf(languageopt('booking-reminder','bk-reminder','ED_BKREMINDER'));
                        printf(languageopt('booking-listing-reminder','bk-reminderlisting','ED_BKREMINDERLISTING'));
                        printf(languageopt('booking-request-reminder','bk-reminderrequest','ED_BKREMINDERREQUEST'));
                       
                        printf(languageopt('Cancel-member','bk-callcenter','ED_BKCANCEL'));
                        printf(languageopt('Cancel-restaurant','bk-rcallcenter','ED_RKCANCEL'));
                        printf(languageopt('Sms-restaurant','bk-sms','ED_BKSMS'));


    }
    
    if($navbar == 'ED_NOSHOW'){
         echo "<form enctype='multipart/form-data' name='idFormsQuery3' id='idFormsQuery3' method='POST' action='$action' >

                    <input type='hidden' value='$navbar' id='navbar' name='navbar'>
                    <input type='hidden' value='' id='action_type' name='action_type'>
                    <input type='hidden' value='$theMember' id='theMember' name='theMember'>
                    <input type='hidden' value='83' id='initstate' name='initstate'>
                     <input type='hidden' name='language' id ='itemlage' value='en' />
                    </form></br><br />";
        echo "<a class='mybarnav' href=\"javascript:setViewBar('EMAIL MANAGEMENT');\">Back to listing </a>";
                        printf(languageopt('noshow-member','bk-noshow','ED_BKNOSHOW'));
                        printf(languageopt('noshow_whitelabel','bk-whitelabelnoshow','ED_BKWLNOSHOW'));
     
    }
    if ($navbar == 'ED_WHELLWIN') {
        echo "<form enctype='multipart/form-data' name='idFormsQuery3' id='idFormsQuery3' method='POST' action='$action' >

                    <input type='hidden' value='$navbar' id='navbar' name='navbar'>
                    <input type='hidden' value='' id='action_type' name='action_type'>
                    <input type='hidden' value='$theMember' id='theMember' name='theMember'>
                    <input type='hidden' value='83' id='initstate' name='initstate'>
                     <input type='hidden' name='language' id ='itemlage' value='en' />
                    </form></br><br />";
        echo "<a class='mybarnav' href=\"javascript:setViewBar('EMAIL MANAGEMENT');\">Back to listing </a>";
                        printf(languageopt('Wheelwin-member','bk-wheelwin','ED_BKWHELLWIN'));
                        printf(languageopt('Wheelwin-restaurant','bk-rwheelwin','ED_RKWHELLWIN'));

    }
    if ($navbar == 'ED_CALLCENTER') {
        echo "<form enctype='multipart/form-data' name='idFormsQuery3' id='idFormsQuery3' method='POST' action='$action' >

                    <input type='hidden' value='$navbar' id='navbar' name='navbar'>
                    <input type='hidden' value='' id='action_type' name='action_type'>
                    <input type='hidden' value='$theMember' id='theMember' name='theMember'>
                    <input type='hidden' value='83' id='initstate' name='initstate'>
                     <input type='hidden' name='language' id ='itemlage' value='en' />
                    </form></br><br />";
        echo "<a class='mybarnav' href=\"javascript:setViewBar('EMAIL MANAGEMENT');\">Back to listing </a>";
                        printf(languageopt('Callcenter-member','bk-callcenter','ED_BKCALLCENTER'));
                        printf(languageopt('Callcenter-restaurant','bk-rcallcenter','ED_RKCALLCENTER'));
                        printf(languageopt('Callcenter-update-member','bk-updatecallcenter','ED_BKCAllUPDATE'));
                        printf(languageopt('Callcenter-update-restaurant','bk-rupdatecallcenter','ED_RKCAllUPDATE'));

    }
    if ($navbar == 'ED_CATERING') {
        echo "<form enctype='multipart/form-data' name='idFormsQuery3' id='idFormsQuery3' method='POST' action='$action' >

                    <input type='hidden' value='$navbar' id='navbar' name='navbar'>
                    <input type='hidden' value='' id='action_type' name='action_type'>
                    <input type='hidden' value='$theMember' id='theMember' name='theMember'>
                    <input type='hidden' value='83' id='initstate' name='initstate'>
                     <input type='hidden' name='language' id ='itemlage' value='en' />
                    </form></br><br />";
        echo "<a class='mybarnav' href=\"javascript:setViewBar('EMAIL MANAGEMENT');\">Back to listing </a>";
                        printf(languageopt('Catering-member','bk-callcenter','ED_CRMEMBER'));
                        printf(languageopt('Catering-restaurant','bk-rcallcenter','ED_CRRESTRAURANT'));
//                        printf(languageopt('Catering-reminder','bk-rcallcenter','ED_CRREMINDER'));
//                        printf(languageopt('Catering-rkreminder','bk-rcallcenter','ED_CRRKREMINDER'));

    }
    if ($navbar == 'ED_REVIEW') {
        echo "<form enctype='multipart/form-data' name='idFormsQuery3' id='idFormsQuery3' method='POST' action='$action' >

                    <input type='hidden' value='$navbar' id='navbar' name='navbar'>
                    <input type='hidden' value='' id='action_type' name='action_type'>
                    <input type='hidden' value='$theMember' id='theMember' name='theMember'>
                    <input type='hidden' value='83' id='initstate' name='initstate'>
                     <input type='hidden' name='language' id ='itemlage' value='en' />
                    </form></br><br />";
        echo "<a class='mybarnav' href=\"javascript:setViewBar('EMAIL MANAGEMENT');\">Back to listing </a>";
                        printf(languageopt('Review-member','bk-reviewpost','ED_BKREVIEW'));
                        printf(languageopt('Review-restaurant','bk-rreviewpost','ED_RKREVIEW'));
                         printf(languageopt('Review-reminder_member_view','bk-reviewreminder','ED_BKREVIEWREMINDER'));
          

    }
    if($navbar == 'ED_BKCONFIRMATION' || $navbar == 'ED_BKREMINDER' || $navbar == 'ED_BKREMINDERLISTING' ||$navbar == 'ED_BKREMINDERREQUEST' || $navbar == "ED_RKUPDATE" ||$navbar == "ED_BKUPDATE" ){
             $lbl =$navbar;
         getBkTemplate($lbl);
    }
    if($navbar == 'ED_BKCONFIRLIST'){
        getBkTemplate($navbar);
    }
    if($navbar == 'ED_BKREQUEST'){
       getBkTemplate($navbar); 
    }
    if($navbar == 'ED_RBKCONFIRMATION'){
       getBkTemplate($navbar);  
    }
    if ( $navbar == 'ED_RBKCONFIRLIST'){
        getBkTemplate($navbar);  
    }
    if ( $navbar == 'ED_RBKREQUEST'){
        getBkTemplate($navbar);  
    }
    if($navbar == 'ED_BKSMS'){
      
         getSmsTemplate($navbar);
    }

    if ($navbar == 'ED_REGISTER'){
         getRegisterTemplate($navbar);  
//         echo "<a class='mybarnav' href=\"javascript:setViewBar('EMAIL MANAGEMENT');\">Back </a>";
//        include('inc/emailtemplate/header/member_header.php');
//        include('inc/emailtemplate/register/welcome.php');
//        include('inc/emailtemplate/footer/member_footer.php');
    }
    if ($navbar == 'ED_BKWHELLWIN' ){
        getWinTemplate($navbar);
    }
    if($navbar == 'ED_RKWHELLWIN' ){
        getWinTemplate($navbar);
    }
    if ($navbar == 'ED_BKCALLCENTER' || $navbar == 'ED_BKCAllUPDATE' || $navbar == 'ED_RKCAllUPDATE'){
        getCallTemplale($navbar);
    }
    if($navbar == 'ED_RKCALLCENTER'){
        getCallTemplale($navbar);
    }
    if ($navbar == 'ED_BKCANCEL') {
        getCancelTemplate($navbar);
    }
    if($navbar == 'ED_RKCANCEL'){
         getCancelTemplate($navbar);
    }
    if ($navbar == 'ED_BKREVIEW' ||  $navbar == 'ED_BKREVIEWREMINDER' || $navbar == 'ED_RKREVIEWREMINDER' ||  $navbar == 'ED_RKREVIEW') {
        getRevTempalte($navbar);
    }
//    
//    if($navbar == 'ED_RKREVIEW' ){
//         getRevTempalte($navbar);
//    }
//     
    if($navbar == 'ED_CRMEMBER' ||  $navbar == 'ED_CRRESTRAURANT'){
        getCategringTemplate($navbar);
    }
    if($navbar == 'ED_BKNOSHOW' ||  $navbar == 'ED_BKWLNOSHOW'){
        getNoshowTemplate($navbar);
    }
    
    

?>

<script>
    <?php printf("var cookiename = '%s';", getCookiename('admin')); ?>
    $(function () {
      

        $(".email-temp").css('display', 'none');
        $(".panel-body").css('display', 'block');
//        $(".reg-welcome").bind('click', function () {
//            var url = '../api/translation/content';
//            $(".email-temp").css('display', 'block');
//            $(".panel-body").css('display', 'none');
//            $.ajax({
//                url: url,
//                dataType: "json",
//                type: "get",
//                data: {
//                    element: 'EMAIL',
//                    language: $("#itemlanguage").attr('for')
//                },
//                success: function (data) {
//                    if (data.errors === null) {
//                        console.log(JSON.stringify(data.data));
//                        console.log(data.data.footer_content);
//                    }
//                    $("#ft-header1").text(data.data.footer_content);
//                    $("#ft-header2").text(data.data.app_message);
//
//                }
//
//            });
//
//        });

    });
    function listindex(em) {
        $("#itemlanguage").attr('for', '');
        $(".itemlanguage").val(em.text);
        $(".itemlanguage").attr('for', $(em).attr('value'));
        $("#itemlage").val($(em).attr('value'));

    }
//    function setViewBar(value){
//        $('#navbar').val(value); 
//        alert($('#navbar').val());
////        var idFormnavbar.submit(); 
//        
//    }

</script>
