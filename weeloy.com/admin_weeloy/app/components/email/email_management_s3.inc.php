<div class="container">
    <div class="row">
        <div class="col-xl-12 left-sec">
<div ng-controller="S3emailController" ng-init="moduleName='S3email'; " >
   
	<div id='listing' ng-show='lists3Flag===true'>
		<div class="form-group"  style='margin-bottom:25px;'>
                    <div class="col-md-4">
                            <div class="input-group col-md-4">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                                    <input type="text" class="form-control input-sm" ng-model="searchText" ng-change="paginator.setPage(0);" style='width:200px;' autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" > 
                            </div>
                    </div>
                    <div class="col-md-2" style='font-size:10px;'>
                        selected records: <strong> {{filteredEvent.length}} </strong>
                    </div>
                    <div class="col-md-2">
                        <div class="btn-group">
                                <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style='font-size:11px;'>Page Size <span class="caret"></span></button>
                                <ul class="dropdown-menu" role="menu">
                                        <li ng-repeat="x in paginator.pagerange()"><a href ng-click="paginator.setRowperPage(x)">{{x}}</a></li>
                                </ul>
                        </div> 		
                    </div>

                    
                </div>
            
            <div style=" clear: both;"></div>
		<table width='100%' class="table table-condensed table-striped" style='font-size:12px;'>
			<thead>
				<tr>
				<th ng-repeat="y in tabletitle"><tbtitle var="{{y.a}}" name='tabletitle' module='moduleName'/></th>
                                <th>VIEW</th><th> &nbsp; </th>
				</tr>
			 </thead>
               
			<tbody style='font-family:helvetica;font-size:12px;'>
                   
				<tr ng-repeat="x in filteredEvent = (names | filter:searchText) | emsizefilter:this | orderBy:predicate:reverse | slicefilter:paginator.getPage():paginator.getRowperPage():paginator.getItemCount() | limitTo:paginator.getRowperPage()" style='font-family:helvetica;font-size:11px;'>
                                    <td ng-repeat="y in tabletitle">
                                    {{ x[y.a] | adatereverse:y.c }}
                                        </span>
                                    </td>
				
                                       <td width='30'>&nbsp;</td>
					<td nowrap><a href ng-click="view(x)" style='color:green;'><span class='glyphicon glyphicon-list'></a></span></td>
					
				</tr><tr><td colspan='{{tabletitle.length + 3}}'></td></tr>
			</tbody>
		</table>
  
            <div ng-if="totalCount >= paginator.getRowperPage()" class="col-md-12" align="center" style='font-size:11px' >
                <ul class="pagination">
                    <li ng-class="paginator.isFirstPage()"><a><span class="glyphicon glyphicon-fast-backward" ng-click="paginator.firstPage()"></span></a></li>
                    <li ng-class="paginator.prevPageDisabled()">
                            <a href="javascript:;" ng-click="paginator.prevPage()"><span class="glyphicon glyphicon-step-backward" ng-click="paginator.firstPage()"></span> Prev</a>
                    </li>

                    <li ng-repeat="n in paginator.range()" ng-class="{active: n == paginator.getPage()}" >

                            <a href="javascript:;" ng-click="loadMore(n+1,n);">{{n + 1 + paginator.getPageOffset() }}</a>
                    </li>
                    <li ng-class="paginator.nextPageDisabled()">
                            <a href="javascript:;" ng-click="nextPage();">Next <span class="glyphicon glyphicon-step-forward" ng-click="paginator.firstPage()"></span></a>
                    </li>
                    <li ng-class="paginator.isLastPage()"><a><span class="glyphicon glyphicon-fast-forward" ng-click="lastPage();"></span></a></li>
                </ul>
            </div>
            </div>
	</div>

	<div class="col-md-12" ng-show='views3Flag===true'>
		<br /><a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a><br />
                <form enctype='multipart/form-data' name='idFormsQuery3' id='idFormsQuery3' method='POST' action='bktemplate' >
                    <input type='hidden' value='label' id='navbar' name='navbar'>
                    <input type='hidden' value='' id='action_type' name='action_type'>
                    <input type='hidden' value='' id='theMember' name='theMember'>
                    <input type='hidden' value='83' id='initstate' name='initstate'>
                     <input type='hidden' name='language' id ='itemlage' value='en' />
                    </form></br><br />
                    <span style="font-size:16px;">To: </span><span class="email"></span><br />
                            <span style="font-size:16px;">Reply-To: </span><span class="reply-email"></span><br />
                     <span style="font-size:16px;">Title: </span><span class="title"></span><br />
                       <span style="font-size:16px;">Subject: </span><span class="subject"></span>
                    <div class="email-container">
                        
                    </div>
                    
                    
	</div>

</div>

</div>
</div>
</div>

<script>
    app.controller('S3emailController', ['$scope','$http','$timeout','bookService','Paginator', function($scope,$http,$timeout,bookService,Paginator) {
       $scope.paginator = new Pagination(500);

	$scope.restaurant = <?php echo "'".$theRestaurant."';"; ?>
	$scope.email = <?php echo "'" . $email . "';"; ?>
	$scope.predicate = '';
	$scope.reverse = false;
	$scope.nonefunc = function() {};
          $scope.isopendate =false;
          $scope.lists3Flag = true; 
            $scope.views3Flag = false; 
      
	$scope.tabletitle = [ {a:'index', b:'ID', c:'' , q:'down', cc: 'black' }, {a:'restaurant', b:'Restaurant', c:'' , q:'down', cc: 'black' }, {a:'recipient', b:'Recipient', c:'' , q:'down', cc: 'black' },{a:'booking_id', b:'BookingId', c:'' , q:'down', cc: 'black' }, { a:'DATE', b:'Date', c:'' , q:'down', cc: 'black' }];
     
        $scope.Objevent = function() {
		return {
			
			index: 0, 
			restaurant: "", 
                        recipient:"",
                        booking_id:"",
                        status:'',
                        DATE:"",
                        data:"",
                        totalCount:'',
  
			remove: function() { 
				for(var i = 0; i < $scope.names.length; i++) 
					if($scope.names[i].name === this.name) {
						$scope.names.splice(i, 1);
						break;
						}
				},
				
			replicate: function(obj) {
				for (var attr in this)
		        	if(this.hasOwnProperty(attr) && typeof this[attr] !== 'function' && attr !== '$$hashKey') 
        				this[attr] = obj[attr];
				return this;
				},
										
			clean: function() {
				

				for (var attr in this)
		        	if(this.hasOwnProperty(attr) && typeof this[attr] !== 'function' && attr !== '$$hashKey') {
        				if(typeof this[attr] === 'string') {
        					this[attr] = this[attr].replace(/\'|\"/g, '’');
        					}
        				}
				return this;
				},
					
			check: function() {
				if(this.name === '' || this.name.length < 6) { alert('Invalid title name, empty or too short(5)'); return -1;}
				if(this.title === '' || this.title.length < 3) { alert('Invalid title name, empty or too short(2)'); return -1;}
				return 1;
				}
			};
	};
        
          $scope.reset = function(item) {
          
		$scope.listTagFlag = false;
		$scope.viewTagFlag = false

		$scope[item] = true;	

            };

	   $scope.backlisting = function() {
              $scope.lists3Flag = true; 
                $scope.views3Flag = false; 
		console.log($scope.lists3Flag);
                console.log($scope.views3Flag);

	    };
                
                 
           $scope.getData =function(n){ 
             
                var url ="../api/getspoolhistory/s3/"+n;
//                     Paginator.setPage(n-1);
                 $http.get(url).then(function(response) {
                         var items =$scope.roles;
                     $scope.selectedItem=[];
                     $scope.names = [];
                         var data = response.data.data.history.data;
                         $scope.totalCount =response.data.data.history.totalCount;
                         for (i = 0; i < data.length; i++) {
                               data[i].index = data[i].ID;
                                //data[i].totalCount=$scope.totalCount;
                                   var value = data[i];
                                   $scope.names.push(new $scope.Objevent().replicate(value)); 
                             }
                      
                             $scope.names['page'] =n-1;
                             $scope.names['totalCount']=$scope.totalCount;
                         //$scope.paginator.setItemCount($scope.totalCount);  
                         
                         
//                         Paginator.setItemCount($scope.totalCount)
//                          $scope.pagination =  Paginator.range();
//                     
//                          $scope.getPage =Paginator.getPage();
//                          $scope.getRowperPage =Paginator.getRowperPage();
//                          $scope.getItemCount =Paginator.getItemCount();
                         
                              //
                 });
           };
           
       $scope.loadMore =function(n,p){
           $scope.page =n;
            $scope.getData(n);
  
        }
        $scope.nextPage =function(){
               var n = $scope.page;
            $scope.getData(n+1);  
        }
        $scope.lastPage =function(){
               var n = $scope.paginator.pageCount();
            $scope.getData(n-1);  
        }
            $scope.getData(1);
            
            
           $scope.view = function(oo) {
      
                $scope.removedtagrest=[];
		$scope.selectedItem = oo;
                $scope.lists3Flag = false; 
                $scope.views3Flag = true; 
                $scope.getEmail(oo);
//                 $scope.restaurants(oo['tag']);
            };
            
           $scope.getEmail =function(oo){
                var d=new Date(oo.DATE); 
                 var month =d.getMonth()-1,year=d.getFullYear(),day=d.getDay();
                 
               var obj ={
                        'dataid':oo.data,
                        'month':month,
                        'year' :year,
                        'day' :day,
                        'date':oo.DATE
                    }
                    console.log("obj="+JSON.stringify(obj));
                var url ="../api/getspoolhistory/email";
                $http.post(url,{'item':obj}).then(function(response) {
                    $scope.reset('viewEventFlag');
                   var str =response.data.data.htm;
                   if(response && typeof response.data.data!='undefined'){
                               var txt= unescape(str);
                                txt = txt.replace(/[\u2019]/g,"\'");
                          
                          var stxt =txt.split('||||');
                      
                          $(".email").html(stxt[0]);
                          $(".title").html(stxt[1]);

                       $(".email-container").html(stxt[2]);
                                $(".reply-email").html(response.data.data.emheader.replyto);
                                $(".subject").html(JSON.stringify(response.data.data.emheader.from));
                               //console.log(JSON.parse(stxt[3]));
                      
                    }
          
                     

                });
                	
            }
        

    }]);
</script>