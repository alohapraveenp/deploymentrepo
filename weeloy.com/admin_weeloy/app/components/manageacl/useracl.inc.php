
<div class="container">
    <div class="row">
        <div class="col-xl-12 left-sec">
<div ng-controller="AclController" ng-init="moduleName='acl'; listAclFlag = true; viewAclFlag=false; createAclFlag=false;" >
	<div id='listing' ng-show='listAclFlag'>
		<div class="form-group"  style='margin-bottom:25px;'>
			<div class="col-md-4">
				<div class="input-group col-md-4">
					<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
					<input type="text" class="form-control input-sm" ng-model="searchText" ng-change="paginator.setPage(0);" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" style='width:200px;'> 
				</div>
			</div>
			<div class="col-md-2"></div>
			<div class="col-md-4">
			<a href ng-click='create();' class="btn btn-success btn-sm" style='color:white;width:200px;'><span class='glyphicon glyphicon-certificate'></span> &nbsp;Assign User Access Level</a>
			</div>
		</div>
            <div style=" clear: both;"></div>
		<table width='100%' class="table table-condensed table-striped" style='font-size:12px;'>
			<thead>
				<tr>
				<th ng-repeat="y in tabletitle"><tbtitle var="{{y.a}}" name='tabletitle' module='moduleName'/></th>
				<th>update</th><th> &nbsp; </th>
				<th>delete</th><th> &nbsp; </th>
                             
				</tr>
			 </thead>
			<tbody style='font-family:helvetica;font-size:12px;'>
				<tr ng-repeat="x in filteredAcl = (names | filter:searchText) | sizefilter:this | orderBy:predicate:reverse | slicefilter:paginator.getPage():paginator.getRowperPage():paginator.getItemCount() | limitTo:paginator.getRowperPage()" style='font-family:helvetica;font-size:11px;'>
					<td ng-repeat="y in tabletitle">
						<span  ng-if="x[y.a]===true"><span style="color:green;" class='glyphicon glyphicon-ok'></span> </span>
						<span  ng-if="x[y.a]===false"><span style="color:red;" class='glyphicon glyphicon-remove'></span></span>
						<span ng-if="x[y.a]!==false && x[y.a]!==true ">
						 {{ x[y.a] | adatereverse:y.c }}
						</span>
					</td>
					<td><a href ng-click="update(x)" style='color:blue;'><span class='glyphicon glyphicon-pencil'></span></a></td>
                                      <td width='30'>&nbsp;</td>
					<td nowrap><a href ng-click="delete(x)" style='color:red;'><span class='glyphicon glyphicon-trash'></span></a></td>
					
				</tr><tr><td colspan='{{tabletitle.length + 3}}'></td></tr>
			</tbody>
		</table>
		<div ng-if="filteredAcl.length >= paginator.getRowperPage()" class="col-md-12" align="center" style='font-size:11px' ng-include="'app/components/paginator.html'"></div>
	</div>

	<div class="col-md-12" ng-show='viewAclFlag'>
		<br /><a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a><br />
		<table class='table-striped' style="margin: 0 0 150px 30px;font-size:13px;font-family: Roboto">		
			<tr ng-repeat="y in tabletitleContent | filter: {b: '!picture'}"><td nowrap ><strong> {{ y.b }} </strong></td><td> &nbsp; </td><td>{{ selectedItem[y.a] | adatereverse:y.c }} </td></tr>
			<tr ng-if="selectedItem['picture'] != ''"><td nowrap><strong>Picture: </strong></td><td> &nbsp; </td><td class="showpict"><img ng-src="{{path}}{{selectedItem.picture}}" height='150'/></td></tr>
		</table>
	</div>

	<div class="col-md-12" ng-show='createAclFlag'>
		<br />
		<a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a>
		<br />
		 <div class="col-md-2"></div>
		 <div class="col-md-8">
		   <div class="row" ng-repeat="y in tabletitleContent | filter: {t: '!dontshow' }"  style="margin: 0 0 20px 0;font-size:12px;font-familly:Roboto">

				<div class="input-group"  ng-if="y.t === 'input'">
						<span class="input-group-addon input11"><i class="glyphicon glyphicon-{{y.d}} input14"></i>&nbsp;{{y.b}}</span>
						<input type="text" class="form-control input-sm" ng-model="selectedItem[y.a]" ng-change="cleaninput(y.a)"  ng-readonly="action==='update' && y.a==='email' ">
				</div>
					<div class="input-group" ng-if="y.t === 'array'">
					<div class='input-group-btn' uib-dropdown >
					<button type='button' class='btn btn-default btn-sm input11' uib-dropdown-toggle >
							<i class="glyphicon glyphicon-{{y.d}} input13"></i>&nbsp; {{y.b}}<span class='caret'></span></button>
					<ul class='dropdown-menu scrollable-menu' uib-dropdown-menu style='height: auto;max-height:200px; overflow-x: hidden;' >
					<li ng-repeat="p in y.val"><a href ng-click="selectedItem[y.a]=p;y.func()">{{ p }}</a></li>
					</ul>
					</div>
					<input type='text' ng-model='selectedItem[y.a]' class='form-control input-sm' readonly >
				</div>
				<div class="input-group" ng-if="y.t === 'checkarr'">
					<h4>select permission level</h4>
					  <div ng-repeat="p in proptable">
							<input  type="checkbox" ng-model="selectedItem[p.a]" ng-true-value="true" ng-false-value="false" ng-checked ="selectedItem[p.a] " > &nbsp; {{p.b}}
					  </div>
				</div>
                              
		   </div><br />
		</div>
		<div class="col-md-2"></div>
		<div class="col-md-7"></div>
		<div class="col-md-5">
			<a href ng-click='savenewlevel(action);' class="btn btn-success btn-sm" style="color:white;width:150px"><i class='glyphicon glyphicon-save'></i> &nbsp;{{ buttonlabel }} </a><br />
		</div>
		</div>
</div>

</div>
</div>
</div>

<script>

var token = <?php echo "'".$_SESSION['user_backoffice']['token']."';"; ?>
var email =  <?php echo "'".$_SESSION['user_backoffice']['email']."';";?>

app.controller('AclController', ['$scope','$http','$timeout','adminServiceApi', function($scope,$http,$timeout,adminServiceApi) {
	var todaydate = new Date();
	var aorder = (function() { var i, arr=[]; for(i = 0; i < 30; i++) arr.push(i); return arr; })();
	
	$scope.paginator = new Pagination(25);
	$scope.restaurant = <?php echo "'".$theRestaurant."';"; ?>
	$scope.email = <?php echo "'" . $email . "';"; ?>
	$scope.predicate = '';
	$scope.reverse = false;
	$scope.nonefunc = function() {};
 
    var rolesarr = ['management', 'finance', 'marketing_manager', 'support_manager', 'sales_manager', 'it_manager', 'it_team'];
	$scope.roles = ["RESTAURANT", "MEMBER", "CLUSTER", "CUISINE", "AREA", "CAMPAIGN","EMAIL MANAGEMENT","DATAEXTRACT",'CATEGORIES','NOTIFICATION','TAG MANAGEMENT'];
	var navbar= [ { RESTAURANT:false, name:"Restaurant" }, { MEMBER:false, name:"Member" }, { CLUSTER:false, name:"Cluster" }, { CUISINE:false, name:"Cuisine" }, { AREA:false, name:"Area" }, { CAMPAIGN:false, name:"Campaign" }, { EMAILMANAGEMENT:false, name:"Email management" }, { DATAEXTRACT:false, name:"Dataextract" }, { CATEGORIES:false, name:"Categories" }, { NOTIFICATION:false, name:'Notification' }, { TAGMANAGEMENT:false, name:'Tag management' } ];

	$scope.tabletitle = [ {a:'index', b:'ID', c:'' , q:'down', cc: 'black' }, {a:'email', b:'Email', c:'' , q:'down', cc: 'black' } ];
	$scope.tabletitleContent = [{ a:'email', b:'email', c:'', d:'envelope', t:'input', i:0 }, { a:'role', b:'Role', c:'', d:'sort', t:'array',val:rolesarr },{ a:'title', b:'Title', c:'', d:'flag', t:'checkarr' }];
	$scope.proptable = [];
	
	$scope.propfilter = function(ll) {	
		return ll.replace(/ .*$/, '').toLowerCase() + '_prop'; 
		};

	// add the properties dynamically to table		
	$scope.roles.map(function(ll) {
		var prop = $scope.propfilter(ll);
		var title = ll.toLowerCase().replace(/ MANAGEMENT/, "Mgt").replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1);});
		$scope.proptable.push({ a: prop, b: title, c: '', q: 'down', cc: 'black' });
		$scope.tabletitle.push({ a: prop, b: title, c: '', q: 'down', cc: 'black' });
		});
       
   $scope.ObjectAcl = function() {
		return {
		email: '',
		id: '0',
		permission: 0,
		role: '',
		access_level: ''
			};
		};
       		
	$scope.initorder = function() {
		$scope.tabletitle = $scope.bckups;
		$scope.predicate = "vorder";
		$scope.reverse = true;
	};

	$scope.reorder = function(item, alter) {
		alter = alter || "";
		if (alter !== "")  item = alter;
		$scope.reverse = ($scope.predicate === item) ? !$scope.reverse : false;
		$scope.predicate = item;
	};
    
	adminServiceApi.readusracl().then(function(response) {              
		var items =$scope.roles;
		$scope.selectedItem= null;
		$scope.names = [];	
             	
		var data = response.data.data;
		console.log('DATA',   data);       
		data.map(function(oo, index, arr) {
			if(typeof oo.email !== "string" || oo.email.length < 1 || oo.email.trim().length < 1)
				return;
			oo.email = oo.email.trim();
			oo.index = oo.id;
			oo.permission = 0;
			if(typeof oo.access_level === "string" && oo.access_level.length > 3) {
				var userLevel = "||" + oo.access_level + "||";
				$scope.roles.map(function(ll, index) { 		
					var prop = $scope.propfilter(ll);
					oo[prop] = false;
					if(oo.email === "matthew.fam@weeloy.com") {
						}
					if(userLevel.indexOf('|'+ ll + '|') > 0) { 
						oo.permission += (1 << index); 
						oo[prop] = true; 
						}
					});
				}
			$scope.names.push(oo);                         
			});   
		console.log('DATA',   $scope.names);       
        });
 
	$scope.cleaninput = function(ll) {
		if(typeof $scope.selectedItem[ll] === 'string')
			$scope.selectedItem[ll] =  $scope.selectedItem[ll].replace(/\'|\"/g, '’');
		};
		
	$scope.reset = function(item) {
		$scope.listAclFlag = false;
		$scope.viewAclFlag = false
		$scope.createAclFlag = false;
		$scope[item] = true;	
		};

	$scope.backlisting = function() {
		$scope.reset('listAclFlag');
		};
		
	$scope.removeAclData = function(email) {
         	var i, data = $scope.names;
         	if(typeof email !== "string" || email.length < 3)
         		return -1;
         		
		for(i = 0; i < data.length; i++)
                    if(data[i].email === email)
                           return data.splice(i, 1);
		};
   		
	$scope.view = function(oo) {
		$scope.selectedItem = oo;
		$scope.reset('viewAclFlag');
		};
	
	$scope.create = function() {
		$scope.selectedItem = new $scope.ObjectAcl();
		$scope.reset('createAclFlag');
		$scope.buttonlabel = "Save new user roles";
		$scope.action = "create";
		return false;
		}

	$scope.update = function(oo) {
		$scope.selectedItem = oo;
		$scope.reset('createAclFlag');
		$scope.buttonlabel = "Update categories";
		$scope.action = "update";
		};

	$scope.savenewlevel = function(action) {
		var u, msg, oo = $scope.selectedItem, valAr = [];
      
 		if(typeof oo.email !== "string" || validateEmail(email) === false)
			return alert("Please choose a valid email");

		oo.permission = 0;
		$scope.roles.map(function(ll, index) { 		
			var prop = $scope.propfilter(ll);
			if(oo[prop] === true) {
				oo.permission += (1 << index); 
				oo[prop] = true;
				valAr.push(ll); 
				}
			});
		oo.access_level = (valAr.length > 0) ? valAr.join("||") : "";

		if(action === "create") {
                       if(oo.access_level === "" )
                           return alert( " Please choose user permission pages !");
 
 			adminServiceApi.createusracl(oo.email, oo.role, oo.access_level).then(function(response) {
				if(response.status === 1) {
					$scope.names.push($scope.selectedItem);
					alert("user acl  has been created");
					}
				else alert("user acl has NOT been created: " + response.errors);
				});
				//window.location.reload();
                    }
		else if(action == "update") {
			adminServiceApi.updateusracl(oo.email, oo.index, oo.role, oo.access_level).then(function(response) {
				if(response.status === 1) alert("user acl  has been updated");
				else alert("user acl has NOT been updated: " + reponse.errors);
				});
			}
		$scope.backlisting();	
		};
		
	$scope.delete = function(oo) {	
		var email = oo.email;		
		adminServiceApi.deleteusracl(email).then(function(response) {
			if(response.status === 1) {
				$scope.removeAclData(email);
				alert(email + " acl  has been deleted");
				}
			else alert(email + " acl has NOT been deleted: " + reponse.errors);
			});

		$scope.backlisting();	
			
		};

	function validateEmail(email) {
		if(typeof email !== "string" || email.length < 4)
			return false;
			
    		var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    		return re.test(email);
		}			
}]);

	
</script>

