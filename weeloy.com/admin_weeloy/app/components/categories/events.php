<div class="container">
    <div class="row">
        <div class="col-md-12 left-sec">
<div ng-controller="EventController" ng-init="moduleName='event'; listTagFlag = true; viewTagFlag=false; createTagFlag=false;" >
    <input type='hidden' id='token' value ="<?php echo $_SESSION['user_backoffice']['token'] ?>" />
    <input type='hidden' id='email' value ="<?php echo $_SESSION['user_backoffice']['email'] ?>" />
    <input type='hidden' id='restaurant' value ="<?php echo $theRestaurant ?>" />
	<div id='listing' ng-show='listTagFlag'>
		<div class="form-group"  style='margin-bottom:25px;'>
			<div class="col-md-4">
				<div class="input-group col-md-4">
					<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
					<input type="text" class="form-control input-sm" ng-model="searchText" ng-change="paginator.setPage(0);" style='width:200px;' autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" > 
				</div>
			</div>
			<div class="col-md-2"></div>
			<div class="col-md-4">
			<a href ng-click='create();' class="btn btn-success btn-sm" style='color:white;width:200px;'><span class='glyphicon glyphicon-certificate'></span> &nbsp;Create a New Event</a>
			</div>
		</div>
        <div style=" clear: both;"></div>
		<table width='100%' class="table table-condensed table-striped" style='font-size:12px;'>
			<thead>
				<tr>
				<th ng-repeat="y in tabletitle">
                    <tbtitle var="{{y.a}}" name='tabletitle' module='moduleName'/></th>
				<th>update</th><th> &nbsp; </th>
<!--                                <th>delete</th><th> &nbsp; </th>-->
				</tr>
			 </thead>
			<tbody style='font-family:helvetica;font-size:12px;'>
				<tr ng-repeat="x in filteredEvent = (names | filter:searchText) | sizefilter:this | orderBy:predicate:reverse | slicefilter:paginator.getPage():paginator.getRowperPage():paginator.getItemCount() | limitTo:paginator.getRowperPage()" style='font-family:helvetica;font-size:11px;'>
                    <td ng-repeat="y in tabletitle">
                        <a href ng-click="view(x)"> {{ x[y.a] | adatereverse:y.c }}</a>
                    </td>
					<td><a href ng-click="update(x)" style='color:blue;'><span class='glyphicon glyphicon-pencil'></a></span></td>
<!--                                       <td width='30'>&nbsp;</td>
					<td nowrap><a href ng-click="delete(x)" style='color:red;'><span class='glyphicon glyphicon-trash'></a></span></td>-->
					
				</tr><tr><td colspan='{{tabletitle.length + 3}}'></td></tr>
			</tbody>
		</table>
		<div ng-if="filteredEvent.length >= paginator.getRowperPage()" class="col-md-12" align="center" style='font-size:11px' ng-include="'app/components/paginator.html'"></div>
	</div>
	<div class="col-md-12" ng-show='viewTagFlag'>
            <br /><a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a><br />
                <div class="col-md-4">
                <ul id="restaurant-list" for="notassigned" class="list-group scrollable-menu"  style='height: auto;max-height: 350px; overflow-x: hidden;' >
                    <li class="list-group-item active">List of Not Assigned Restaurant</li>
                    <li  class="list-group-item"
                        ad-drag="true"
                        ad-drag-data="restaurant"
                        ad-drag-begin="onDragStart($data, $dragElement, $event);"
                        ad-drag-end="onDragEnd($data, $dragElement, $lastDropElement, $event);"
                        ad-drop="true"
                        ad-drop-over="onDragOver($data, $dragElement, $dropElement, $event);"
                         ad-drop-leave="onDragLeave($data, $dragElement, $dropElement, $event)"
                         ad-drop-end="onDrop($data, $dragElement, $dropElement, $event,'restaurant-list')"
                        ng-repeat="restaurant in notassigned">
                      <span>
                        {{ restaurant }}
                      </span>
                    </li>
                </ul>
                </div>
                <div class="col-md-1">
                     <div style='float:left;margin-top:100px; font-size: 25px;'>
                        <a class='glyphicon glyphicon-arrow-right' id='add_restau' ></a>
                        <a class='glyphicon glyphicon-arrow-left' id='del_restau' style='display:none'></a>
                    </div>
                </div>
               
                <div class="col-md-4">
                   
                <ul id="assignedrestaurant" class="list-group" for="assigned"
                    ad-drag="true"
                        ad-drag-data="rest"
                        ad-drag-begin="onDragStart($data, $dragElement, $event);"
                        ad-drag-end="onDragEnd($data, $dragElement, $lastDropElement, $event);"
                    ad-drop="true"
                        ad-drop-over="onDragOver($data, $dragElement, $dropElement, $event);"
                         ad-drop-leave="onDragLeave($data, $dragElement, $dropElement, $event)"
                         ad-drop-end="onDrop($data, $dragElement, $dropElement, $event,'assignedrestaurant')"
                          >
                    <li class="list-group-item ">List of Assigned Restaurant</li>
                    <li class="list-group-item"
                         ad-drag="true"
                          ad-drag-data="rest"
                        ad-drag-begin="onDragStart($data, $dragElement, $event);"
                        ad-drag-end="onDragEnd($data, $dragElement, $lastDropElement, $event);"
                        ng-repeat="rest in assigned">
                      <span>
                       
                        {{ rest }}
                      </span>
                    </li>
                    <li class="list-group-item" ng-hide="assigned.length">
                        <span> Drop here</span>
                    </li>
                </ul>
                    <a href ng-click='savetaginrest(currentTag);' class="btn btn-success btn-sm" style="color:white;width:150px"><i class='glyphicon glyphicon-save'></i> &nbsp;save</a><br />
                </div>
	</div>
    <div class="col-md-12" ng-show='restaurantFlag'>
		<br /><a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a><br />
	</div>
	<div class="col-md-12" ng-show='createTagFlag'>
		<br />
		<a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a>
		<br />
		 <div class="col-md-2"></div>
		 <div class="col-md-8">
            <div class="input-group">
                <div class='input-group-btn' dropdown ng-show="action != 'update'">
                    <button type="button" class='btn btn-default dropdown-toggle btn-sm input11' data-toggle="dropdown">Type<span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li ng-repeat="x in types"><a href ng-click="setType(x)">{{x}}</a></li>
                        </ul>
                </div>
                <input type='text' ng-model='selectedItem["type"]' class='form-control input-sm' readonly >
            </div>
            <br/>
		   <div class="row" ng-repeat="y in tabletitleContent | filter: {t: '!dontshow' }"  style="margin: 0 0 20px 0;font-size:12px;font-familly:Roboto">
                 <div class="input-group"  ng-if="y.t === 'input'">
                    <span class="input-group-addon input11"><i class="glyphicon glyphicon-{{y.d}} input14"></i>&nbsp;{{y.b}}</span>
                    <input type="text" class="form-control input-sm" ng-model="selectedItem[y.a]" ng-change="cleaninput(y.a)"  />
                </div>
                <div class="input-group" ng-if="y.t === 'dropdown'">
                    <div class='input-group-btn' dropdown >
                        <button type='button' class='btn btn-default dropdown-toggle btn-sm input11' data-toggle='dropdown'>
                            <i class="glyphicon glyphicon-{{y.d}} input13"></i>&nbsp; {{y.b}}<span class='caret'></span>
                        </button>
                        <ul class='dropdown-menu scrollable-menu'style='height: auto;max-height:120px; overflow-x: hidden;' >
                            <li ng-repeat="p in y.val"><a href ng-click="y.func(p)">{{ p }}</a></li>
                        </ul>
                    </div>
                    <input type='text' ng-model='selectedItem[y.a]' class='form-control input-sm' readonly >
                </div>
                <div class="input-group" ng-if="y.t === 'array'">
                    <div class='input-group-btn' dropdown >
                        <button type='button' class='btn btn-default dropdown-toggle btn-sm input11' data-toggle='dropdown'>
                            <i class="glyphicon glyphicon-{{y.d}} input13"></i>&nbsp; {{y.b}}<span class='caret'></span>
                        </button>
                        <ul class='dropdown-menu scrollable-menu'style='height: auto;max-height:120px; overflow-x: hidden;' >
                            <li ng-repeat="p in y.val"><a href ng-click="selectedItem[y.a]=p;y.func()">{{ p }}</a></li>
                        </ul>
                    </div>
                    <input type='text' ng-model='selectedItem[y.a]' class='form-control input-sm' readonly >
                </div>
                <div class="input-group"  ng-if="y.t==='checkbox' && is_email==='weeloy.com' ">
                    <input   type="checkbox" ng-model="selectedItem[y.a]" ng-checked ="{{selectedItem[y.a]}}"> &nbsp; {{y.b}}
                </div> 
                <div class="input-group"  ng-if="y.t === 'inputtag' && selectedItem['is_tag']===true ">
                    <span class="input-group-addon input11"><i class="glyphicon glyphicon-{{y.d}} input14"></i>&nbsp;{{y.b}}</span>
                    <input type="text" class="form-control input-sm" ng-model="selectedItem[y.a]" ng-change="cleaninput(y.a)"  >
                </div>
                <div class="input-group"  ng-if="y.t === 'inputlink' && selectedItem['is_tag']===false ">
                    <span class="input-group-addon input11"><i class="glyphicon glyphicon-{{y.d}} input14"></i>&nbsp;{{y.b}}</span>
                    <input type="text" class="form-control input-sm" ng-model="selectedItem[y.a]" ng-change="cleaninput(y.a)"  >
                </div>    
                <div class="input-group" ng-if="y.t === 'imagebutton'">
				    <span class="input-group-addon"><i class="glyphicon glyphicon-file"></i></span>
				    <input type="file" name="file_upload" id="file_upload" class="form-control input-sm" onchange="angular.element(this).scope().uploadImage(this)" ng-model="nfiles" placeholder="Upload Files" accept="image/*">  
                </div><br />
                <div class="input-group" ng-if="y.t === 'pictureshow3'  && selectedItem['is_tag']===true">
                    <p ng-if="selectedItem.image3 != ''"><img ng-src="{{path}}{{selectedItem.restaurant3}}/{{selectedItem.image3}}" height='150'/></p>
                </div>
		   </div><br />
		</div>
		<div class="col-md-2"></div>
		<div class="col-md-7"></div>
		<div class="col-md-5">
			<a href ng-click='saveitem();' class="btn btn-success btn-sm" style="color:white;width:150px"><i class='glyphicon glyphicon-save'></i> &nbsp;{{ buttonlabel }} </a><br />
		</div>
        </div>
</div>
</div>
</div>
</div>
<script>
<?php
    $mediadata = new WY_Media();
    $resdata = new WY_restaurant();
    $imgAr = $mediadata->getEventPictureNames($theRestaurant);
    $resAr= $resdata->getSimpleListRestaurant();    
    printf("var imgEvent = [");
    for($i = 0, $sep = ""; $i < count($imgAr); $i++, $sep = ", ")
    	printf("%s '%s'", $sep, $imgAr[$i]);
    printf("];");
    printf("var pathimg = '%s';", $mediadata->getFullPath('small'));
    printf("var resArr = [");
            for($i = 0, $sep = ""; $i < count($resAr); $i++, $sep = ", "){
                printf("%s '%s'", $sep, $resAr[$i]);
            }
    printf("];");
?>
</script>
<script src="http://www.openjs.com/scripts/events/keyboard_shortcuts/shortcut.js"></script>
<script src="app/components/categories/EventController.js"></script>