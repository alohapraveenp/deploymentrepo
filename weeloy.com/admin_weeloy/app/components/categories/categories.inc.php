<div class="container">
    <div class="row">
        <div class="col-md-12 left-sec">
<div ng-controller="CategoryController" ng-init="moduleName='cateory'; listCategoryFlag = true; viewCategoryFlag=false; createCategoryFlag=false;" >
    <input type='hidden' id='token' value ="<?php echo $_SESSION['user_backoffice']['token'] ?>" />
    <input type='hidden' id='email' value ="<?php echo $_SESSION['user_backoffice']['email'] ?>" />
    <input type='hidden' id='restaurant' value ="<?php echo $theRestaurant ?>" />
	<div id='listing' ng-show='listCategoryFlag'>
		<div class="form-group"  style='margin-bottom:25px;'>
			<div class="col-md-4">
				<div class="input-group col-md-4">
					<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
					<input type="text" class="form-control input-sm" ng-model="searchText" ng-change="paginator.setPage(0);" style='width:200px;' autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" > 
				</div>
			</div>
			<div class="col-md-2"></div>
                        <div class="col-md-2">
                        <div class="btn-group">
                                <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style='font-size:11px;'>Page Size <span class="caret"></span></button>
                                <ul class="dropdown-menu" role="menu">
                                        <li ng-repeat="x in paginator.pagerange()"><a href ng-click="paginator.setRowperPage(x)">{{x}}</a></li>
                                </ul>
                        </div> 		
                    </div>
			<div class="col-md-4">
			<a href ng-click='create();' class="btn btn-success btn-sm" style='color:white;width:200px;'><span class='glyphicon glyphicon-certificate'></span> &nbsp;Create a New Category</a>
			</div>
		</div>
        <div style=" clear: both;"></div>
		<table width='100%' class="table table-condensed table-striped" style='font-size:12px;'>
			<thead>
				<tr>
				<th ng-repeat="y in tabletitle"><tbtitle var="{{y.a}}" name='tabletitle' module='moduleName'/></th>
				<th>update</th><th> &nbsp; </th>
<!--                                <th>delete</th><th> &nbsp; </th>-->
                             
				</tr>
			 </thead>
			<tbody style='font-family:helvetica;font-size:12px;'>
				<tr ng-repeat="x in filteredEvent = (names | filter:searchText) | sizefilter:this | orderBy:predicate:reverse | slicefilter:paginator.getPage():paginator.getRowperPage():paginator.getItemCount() | limitTo:paginator.getRowperPage()" style='font-family:helvetica;font-size:11px;'>
                                    
                                    <td ng-repeat="y in tabletitle">
                                        <a href ng-click="view(x)"> {{ x[y.a] | adatereverse:y.c }}</a>
                                    </td>
					<td><a href ng-click="update(x)" style='color:blue;'><span class='glyphicon glyphicon-pencil'></a></span></td>
<!--                                       <td width='30'>&nbsp;</td>
					<td nowrap><a href ng-click="delete(x)" style='color:red;'><span class='glyphicon glyphicon-trash'></a></span></td>-->
					
				</tr><tr><td colspan='{{tabletitle.length + 3}}'></td></tr>
			</tbody>
                       
		</table>
		<div ng-if="filteredEvent.length >= paginator.getRowperPage()" class="col-md-12" align="center" style='font-size:11px' ng-include="'app/components/paginator.html'"></div>
	</div>

	<div class="col-md-12" ng-show='viewCategoryFlag'>
		<br /><a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a><br />
		<table class='table-striped' style="margin: 0 0 150px 30px;font-size:13px;font-family: Roboto">		
			<tr ng-repeat="y in tabletitleContent | filter: {b: '!picture'}"><td nowrap ><strong> {{ y.b }} </strong></td><td> &nbsp; </td><td>{{ selectedItem[y.a] | adatereverse:y.c }} </td></tr>
			<tr ng-if="selectedItem['picture'] != ''"><td nowrap><strong>Picture: </strong></td><td> &nbsp; </td><td class="showpict"><img ng-src="{{path}}{{selectedItem.picture}}" height='150'/></td></tr>
		</table>
	</div>

	<div class="col-md-12" ng-show='createCategoryFlag'>
		<br />
		<a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a>
		<br />
		 <div class="col-md-2"></div>
		 <div class="col-md-8">
		   <div class="row" ng-repeat="y in tabletitleContent | filter: {t: '!dontshow' }"  style="margin: 0 0 10px 0;font-size:12px;font-familly:Roboto">
          
                            <div class="input-group"  ng-if="y.t === 'input'">
                                    <span class="input-group-addon input11"><i class="glyphicon glyphicon-{{y.d}} input14"></i>&nbsp;{{y.b}}</span>
                                    <input type="text" class="form-control input-sm" ng-model="selectedItem[y.a]" ng-change="cleaninput(y.a)"  >
                            </div>
                                <div class="input-group" ng-if="y.t === 'dropdown'">
                                        <div class='input-group-btn' dropdown >
                                        <button type='button' class='btn btn-default dropdown-toggle btn-sm input11' data-toggle='dropdown'>
                                                <i class="glyphicon glyphicon-{{y.d}} input13"></i>&nbsp; {{y.b}}<span class='caret'></span></button>
                                        <ul class='dropdown-menu scrollable-menu'style='height: auto;max-height:120px; overflow-x: hidden;' >
                                        <li ng-repeat="p in y.val"><a href ng-click="selectedItem[y.a]=p;y.func()">{{ p }}</a></li>
                                        </ul>
                                        </div>
                                        <input type='text' ng-model='selectedItem[y.a]' class='form-control input-sm' readonly >
                                </div>
                                <div class="input-group" ng-if="y.t === 'array'">
                                        <div class='input-group-btn' dropdown >
                                        <button type='button' class='btn btn-default dropdown-toggle btn-sm input11' data-toggle='dropdown'>
                                                <i class="glyphicon glyphicon-{{y.d}} input13"></i>&nbsp; {{y.b}}<span class='caret'></span></button>
                                        <ul class='dropdown-menu scrollable-menu'style='height: auto;max-height:120px; overflow-x: hidden;' >
                                        <li ng-repeat="p in y.val"><a href ng-click="selectedItem[y.a]=p;y.func()">{{ p }}</a></li>
                                        </ul>
                                        </div>
                                        <input type='text' ng-model='selectedItem[y.a]' class='form-control input-sm' readonly >
                                </div>
                              <div class="input-group"  ng-if="y.t==='checkbox' && is_email==='weeloy.com' ">
                                <input   type="checkbox" ng-model="selectedItem[y.a]" ng-checked ="{{selectedItem[y.a]}}"> &nbsp; {{y.b}}
                               </div> 
                                <div class="input-group"  ng-if="y.t === 'inputtag' && selectedItem['is_tag']===true ">
                                    <span class="input-group-addon input11"><i class="glyphicon glyphicon-{{y.d}} input14"></i>&nbsp;{{y.b}}</span>
                                    <input type="text" class="form-control input-sm" ng-model="selectedItem[y.a]" ng-change="cleaninput(y.a)"  >
                                </div>
                                <div class="input-group"  ng-if="y.t === 'inputlink' && selectedItem['is_tag']===false ">
                                    <span class="input-group-addon input11"><i class="glyphicon glyphicon-{{y.d}} input14"></i>&nbsp;{{y.b}}</span>
                                    <input type="text" class="form-control input-sm" ng-model="selectedItem[y.a]" ng-change="cleaninput(y.a)"  >
                                </div>
<!--                                <div class="input-group"  ng-if="y.t === 'inputtype'  && selectedItem['is_mobile']===true">
                                        <span class="input-group-addon input11"><i class="glyphicon glyphicon-{{y.d}} input14"></i>&nbsp;{{y.b}}</span>
                                        <input type="text" class="form-control input-sm" ng-model="selectedItem[y.a]" ng-change="cleaninput(y.a)"  >
                                </div>-->
                                <div class="input-group" ng-if="y.t === 'arraytype'">
                                        <div class='input-group-btn' dropdown >
                                        <button type='button' class='btn btn-default dropdown-toggle btn-sm input11' data-toggle='dropdown'>
                                                <i class="glyphicon glyphicon-{{y.d}} input13"></i>&nbsp; {{y.b}}<span class='caret'></span></button>
                                        <ul class='dropdown-menu scrollable-menu'style='height: auto;max-height:120px; overflow-x: hidden;' >
                                        <li ng-repeat="p in y.val"><a href ng-click="selectedItem[y.a]=p;y.func()">{{ p }}</a></li>
                                        </ul>
                                        </div>
                                        <input type='text' ng-model='selectedItem[y.a]' class='form-control input-sm' readonly >
                                </div>
                                <div class="input-group" ng-if="y.t === 'imagebutton'">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-file"></i></span>
                                    <input type="file" name="file_upload" id="file_upload" class="form-control input-sm" onchange="angular.element(this).scope().uploadFiles(this)" ng-model="nfiles" multiple placeholder="ipload files">  
                                </div>
                                    

                                <div class="input-group" ng-if="y.t === 'pictureshow1'">
                                    <p ng-if="selectedItem.image1 != ''"><img ng-src="{{path}}{{selectedItem.restaurant1}}/{{selectedItem.image1}}" height='150'/></p>
                                    <p ng-if="selectedItem.image2 != ''"><img ng-src="{{path}}{{selectedItem.restaurant2}}/{{selectedItem.image2}}" height='150'/></p>
                                    <p ng-if="selectedItem.image2 != ''"><img ng-src="{{path}}{{selectedItem.restaurant3}}/{{selectedItem.image3}}" height='150'/></p>
                                </div>
		   </div><br />
                  
                      <div id="progressbox" style="display:none;" ><div id="progressbar" ></div><div id="statustxt">0%</div> </div>
                   
		</div>
		<div class="col-md-2"></div>
		<div class="col-md-7"></div>
		<div class="col-md-5">
			<a href ng-click='savenewevent();' class="btn btn-success btn-sm" style="color:white;width:150px"><i class='glyphicon glyphicon-save'></i> &nbsp;{{ buttonlabel }} </a><br />
		</div>
		</div>
</div>

</div>
</div>
</div>
<style>
    #progressbox {
    border: 1px solid #0099CC;
    padding: 1px; 
    position:relative;
    width:400px;
    border-radius: 3px;
    margin: 10px;
    display:none;
    text-align:left;
    }
    #progressbar {
    height:20px;
    border-radius: 3px;
    background-color: #5bc0de;
    width:1%;
    }
    #statustxt {
    top:3px;
    left:50%;
    position:absolute;
    display:inline-block;
    color: #000000;
    }
</style>
<script>
<?php
    $mediadata = new WY_Media();
    $resdata = new WY_restaurant();
    //$imgAr = $mediadata->getEventPictureNames($theRestaurant);
    $resAr= $resdata->getSimpleListRestaurant();
    printf("var pathimg = '%s';", $mediadata->getFullPath('small'));
    printf("var resArr = [");
    for($i = 0, $sep = ""; $i < count($resAr); $i++, $sep = ", ")
        printf("%s '%s'", $sep, $resAr[$i]);
    printf("];");
?>
</script>
<script src="app/components/categories/CategoryController.js"></script>