app.controller('EventController', ['$scope','$http','$timeout', 'Upload', function($scope,$http,$timeout, Upload) {
	$scope.paginator = new Pagination(100);
  $scope.token = $("#token").val();
  $scope.email = $("#email").val();
	$scope.restaurant = $("#restaurant").val();
  console.log("restaurant: "+$scope.restaurant);
	$scope.predicate = '';
	$scope.reverse = false;
  $scope.names = [];
	$scope.nonefunc = function() {};
  var status=['active','inactive'];
	$scope.tabletitle = [ 
        {a:'id', b:'ID', c:'' , q:'down', cc: 'black' }, 
        {a:'title', b:'Title', c:'' , q:'down', cc: 'black' },
        {a:'tag', b:'Subtitle', c:'' , q:'down', cc: 'black' }, 
        {a:'description', b:'Description', c:'' , q:'down', cc: 'black' }, 
        {a:'images', b:'Banner', c:'' , q:'down', cc: 'black' }
        ];
	$scope.bckups = $scope.tabletitle.slice(0);
  $scope.setType = function(type) {
    // "LNY", "Valentines", "Easter", "Mother's Day", "Father's Day", "Year End"
    switch (type) {
      case 'LNY':
        $scope.selectedItem.type = "event_lny";
      break;
      case 'Valentines':
        $scope.selectedItem.type = "event_valentines";
      break;
      case 'Easter':
        $scope.selectedItem.type = "event_easter";
      break;
      case "Mother's Day":
        $scope.selectedItem.type = "event_mother";
      break;
      case "Father's Day":
        $scope.selectedItem.type = "event_father";
      break;
      case "Year End":
        $scope.selectedItem.type = "event_yearend";
      break;
      case "VIP Customer Page":
        $scope.selectedItem.type = "event_vip_page";
      break;
      case "Co Branding":
        $scope.selectedItem.type = "event_co_branding";
      break;
      default:
        console.error("setType(): Invalid type: "+type);
      break;
    }
    //console.log("setType(): "+$scope.selectedItem.type);
  };
  $scope.tabletitleContent = [
          { a:'title', b:'Title', c:'', d:'tower', t:'input', i:0 },
          { a:'tag', b:'Subtitle', c:'', d:'subtitles', t:'input', i:0 },
          { a:'description', b:'Description', c:'', d:'comment', t:'input', i:0 },
          { a:'image', b:'SelectOnImage', c:'', d:'picture', t:'imagebutton' },
          { a:'images', b:'Banner', c:'', d:'picture', t:'pictureshow2' },
          { a:'type', b:'Type', c:'' , q:'down', cc: 'black', t:'dropdown', val: ['LNY', 'Valentines', 'Easter', "Mother's Day", "Father's Day", "Year End","VIP Customer Page","Co Branding"], func: $scope.setType},
        ];  
  // $title, $tag, $link, $description, $images, $city, $status, $type
	$scope.imgEvent = imgEvent.slice(0);
  var mailArr = $scope.email.split('@');
  $scope.is_email = mailArr[1];
  //$scope.is_email='gmail.com';
  $scope.path ="https://media.weeloy.com/upload/restaurant/";
	$scope.Objevent = function() {
		return {
			restaurant: $scope.restaurant, 
			id: 0, 
			title: '', 
      type:'',
			tag: '', 
			description: '', 
			images: {},
			remove: function() { 
				for(var i = 0; i < $scope.names.length; i++) 
					if($scope.names[i].name === this.name) {
						$scope.names.splice(i, 1);
						break;
						}
				},
			replicate: function(obj) {
				for (var attr in this)
		        	if(this.hasOwnProperty(attr) && typeof this[attr] !== 'function' && attr !== '$$hashKey') 
        				this[attr] = obj[attr];
				return this;
      },
			clean: function() {
				for (var attr in this)
		        	if(this.hasOwnProperty(attr) && typeof this[attr] !== 'function' && attr !== '$$hashKey') {
        				if(typeof this[attr] === 'string') {
        					this[attr] = this[attr].replace(/\'|\"/g, '’');
        					}
        				}
				return this;
      }
    };
  };
  var type='event_';
  var url ="../api/home/getcategories/"+type;
  $http.get(url).then(function(response) {
    if(parseInt(response.data.status) !== 1)
        return alert("../api/home/getcategories error: "+JSON.stringify(response));
    //console.log("../api/home/getcategories returns "+response.data.data.categories.length+" records: "+JSON.stringify(response.data.data.categories));
    data = response.data.data.categories;
    $scope.names = [];
    for (i = 0; i < data.length; i++) {
      console.log(i+" type: "+data[i].type);
      if(data[i].type.match("^event_"))
        $scope.names.push(data[i]); 
    }
    console.log($scope.names.length+"/"+data.length+" events");
  });
	$scope.cleaninput = function(ll) {
		if(typeof $scope.selectedItem[ll] === 'string')
			$scope.selectedItem[ll] =  $scope.selectedItem[ll].replace(/\'|\"/g, '’');
  };
	$scope.reset = function(item) {
		$scope.listTagFlag = false;
		$scope.viewTagFlag = false;
		$scope.createTagFlag = false;
    $scope.restEventFlag = false;
		$scope[item] = true;	
  };
	$scope.backlisting = function() {
		$scope.reset('listTagFlag');
	};
	$scope.findaccount = function(id) {
    var index = -1;
    angular.forEach($scope.names, function(value, key, obj) {
      if (value.id === id) {
        index = key;
        return;
      }
    });
    return index;
  };
  $scope.restaurants = function(tag) {
    var url ="../api/restaurant/gettagrestaurant/"+tag;
    $http.get(url).then(function(response) {
      $scope.selectedItem=[];
      $scope.assigned=[]; 
		  data = response.data;
      if (response.data.data.assigned) 
        $scope.assigned = response.data.data.assigned;
      $scope.notassigned = response.data.data.notassigned;
      //$scope.imagesArr= response.data.data.pictures;
    });
    // /restaurant/gettagrestaurant/     
  };
  $scope.currentDropElement = null;
  $scope.remove = function(l, o) {
    var index = l.indexOf(o);
    if (index > -1)
      l.splice(index, 1);
  };
  $scope.deletetag =function(o) {
    var l=$scope.notassigned,t=$scope.assigned;
    var index = l.indexOf(o),iDx =t.indexOf(o);
    if (index > -1)
      l.splice(index, 1);
    if (iDx > -1)
      t.splice(iDx, 1);
  };
  $scope.onDragStart = function(data, dragElement, dropElement) {};
  $scope.onDragEnd = function(data, dragElement, dropElement) {};
  $scope.onDragOver = function(data, dragElement, dropElement) {
    $scope.currentDropElement = dropElement;
  };
  $scope.onDragLeave = function() {
    $scope.currentDropElement = null;
  };
  $scope.onDrop = function(data,dragElement,dropElement,event,label) {
    var lbl =$("#"+label).attr('for');
    console.log(lbl);
    console.log(data);
    if (data && $scope.currentDropElement) {
      if(lbl=="notassigned") {
        $scope.notassigned.push(data);
        $scope.remove($scope.assigned, data);
      } else {
        $scope.assigned.push(data);
        $scope.remove($scope.notassigned, data);
      }
    }
  };
	$scope.view = function(oo) {
    $scope.removedtagrest=[];
		$scope.selectedItem = oo;
		$scope.reset('viewTagFlag');
    console.log("view. selectedItem: "+JSON.stringify($scope.selectedItem));
  };
	$scope.create = function() {
		$scope.selectedItem = new $scope.Objevent();
    $scope.selectedItem.is_tag = false;
		$scope.reset('createTagFlag');
		$scope.buttonlabel = "Save new categories";
		$scope.action = "create";
    console.log("create. selectedItem: "+JSON.stringify($scope.selectedItem));        
		return false;
  }
   $scope.update = function(oo) {
        oo['is_tag']= false;
        $scope.selectedItem = new $scope.Objevent().replicate(oo);
        $scope.reset('createTagFlag');
        $scope.buttonlabel = "Update categories";
        $scope.action = "update";

    };
  $scope.savetaginrest =function(tag){
    $http.post("../api/updaterestauranttags",
			{ 'tag':tag,'restaurant':$scope.assigned}).then(function(response) {
          console.log(JSON.stringify(response));
      });
      $scope.backlisting();	  
  }
    $scope.saveitem = function() {
        var u, msg, apiurl, ind;
    if($scope.selectedItem.tag ==="" || $scope.selectedItem.title ==="" ) {
      alert("Please Complete All Required fields!");
      return;
    }

		$scope.selectedItem.clean();	

    // $title, $tag, $link, $description, $images, $city, $status, $type
    $scope.selectedItem.city = "";
    $scope.selectedItem.status = "active";
    $scope.selectedItem.link = "";
		
    var files = [];
    if (!angular.isUndefined($scope.selectedItem.images.toUpload) && $scope.selectedItem.images.toUpload !== null)
      files.push($scope.selectedItem.images.toUpload);
    console.log(files.length+" files");
    Upload.upload({
          url: "../api/home/event",
          data: {file: files, 'data': $scope.selectedItem}
        }).then(function(response) {
            if (typeof response !== "undefined" && response.status > 0) {
                console.log('New event successfully created!');
//                if(response.data !== null && response.data.data !== null)
//                  console.log("response.data.data: " + JSON.stringify(response.data.data));
                $scope.selectedItem.clean();
                if($scope.action == "update") {
                  ind = $scope.findaccount($scope.selectedItem.id);
                  if(ind >= 0) 
                    $scope.names.splice(ind, 1);
                }
                $item = new $scope.Objevent().replicate($scope.selectedItem);
                $item.images = $scope.selectedItem.images.name;
                $scope.names.push($item);
            } else
              // $scope.selectedItem.remove();
              // $scope.$apply();
              console.error("Failed to new event!");
              //Notification.show('error', 'Oops!! Sorry Unexpected Error');
            $scope.backlisting();
        });    
  };
  $scope.uploadImage = function(element) {
    //console.log("uploadImage(): id: "+element.id);
    if(element.files.length == 0) {
      alert("Select/Drop one file");
      return false;
    }
    filename = element.files[0].name;
    ext = filename.substring(filename.lastIndexOf(".")+1).toLowerCase();
    if(ext != "jpg" && ext != "jpeg" && ext != "png" && ext != "gif") {
      alert("Invalid file type (jpg, jpeg, png, gif)");
      return false;
    }
    $scope.selectedItem.restaurant = $scope.selectedItem.type;
    $scope.selectedItem.category = 'category';
    console.log("uploadImage(): selectedItem: "+JSON.stringify($scope.selectedItem)+" element.files: "+JSON.stringify(element.files));
    $scope.selectedItem.images = {};
    $scope.selectedItem.images.name = element.files[0].name;
    $scope.selectedItem.images.media_type = 'picture';
    $scope.selectedItem.images.toUpload = element.files[0];
    console.log("uploadImage(): $scope.selectedItem.images: "+JSON.stringify($scope.selectedItem.images));    
  }
//            $scope.removeaginrest = function(oo) {
//		if(confirm("Are you sure you want to delete banner in restaurant ") == false)
//			return;
//                $http.post("../api/removetags",
//			{ 'restaurant':$scope.removedtagrest}).then(function() {
//                            $scope.removedtagrest=[];
//			alert( "tags has been deleted");});
//
//		};   
}]);