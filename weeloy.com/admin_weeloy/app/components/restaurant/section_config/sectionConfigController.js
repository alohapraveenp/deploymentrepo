app.controller('sectionConfigController',  ['$scope', 'API', function ($scope, API) {
        var RestaurantID =$scope.restaurant= $("#restaurant").val();
        var email = $("#email").val(), 
           multProd = $("#multiproduct").val();
           console.log("multiproduct="+multProd);
        $scope.singleproduct =true;
        $scope.multProd = [];
    
            if(multProd	!== "") {
                   console.log("in multip");
                    $scope.multProd = multProd.split("|");
                    //$scope.multProd.splice(0, 0, " ");
                    }
            $scope.bkproduct = "";
            console.log("multiproduct="+$scope.multProd.length);
            if($scope.multProd.length > 1) 
                    $scope.bkproduct = $scope.multProd[1];
            else if($scope.multProd.length > 0) 
                    $scope.bkproduct = $scope.multProd[0];
   
        
         
        
        //api/v2/restaurant/sections/product/SG_SG_R_TheOneKitchen/Chef table
        
        $scope.getSection =function(){
             API.sectionconfig.getRestaurantSection(RestaurantID,$scope.bkproduct)
                .then(function (response) {
                 if (response.data.status == 1) {
                     var section =response.data.data.productdetails;
                    $scope.description = section.description;
                    $scope.bkproduct = section.product;
                    $scope.min_pax = section.min_pax;
                    $scope.max_pax = section.max_pax;
                    $scope.require_config =section.require_confirmation;
                    $scope.require_text =section.require_confirmation_text;
                    $scope.additional_info = section.additional_info
                   
                    var times =JSON.parse(section.open_time);
                    $scope.lunch =times.lunch;
                    $scope.dinner =times.dinner;
                 }else{
                     $scope.description = '';
                    $scope.min_pax ='';
                    $scope.max_pax = '';
                    $scope.require_config ='';
                    $scope.require_text ='';
                    $scope.lunch ='';
                    $scope.dinner ='';
                 }
                });
        }
         
        
        $scope.getSection();
        $scope.setproduct = function(val) {
            $scope.bkproduct = val;
            $scope.getSection();
         
        }
        $scope.showrange = function(){
            if($scope.is_range2==='1'){
               $scope.multiproduct =true;
               $scope.singleproduct =false;

            }else{
                $scope.multiproduct =false;
               $scope.singleproduct =true;
            }
        }
        $scope.showmessage =function(){
             if($scope.is_range2==='1'){
                 
             }
        }

      $scope.saveSection = function () {
         
          if($scope.require_config !==1){
              $scope.require_config =0;
              $scope.require_text ='null';
            
          }
        
            var tmpArr ={'restaurant':RestaurantID,'product':$scope.bkproduct,'description':$scope.description,'min_pax':$scope.min_pax,'max_pax':$scope.max_pax,'lunch':$scope.lunch,'dinner':$scope.dinner,'require_conf':$scope.require_config,'require_text':$scope.require_text, 'additional_info': $scope.additional_info };    
       
      
            API.sectionconfig.saveSectionConfig(tmpArr)
                .then(function (response) {
                    console.log("response"+response);
                    if (response.status == 1) {
                       alert("SuccessFully Saved Your Changes.");
                    }
                });
        };
        
        
}]);

