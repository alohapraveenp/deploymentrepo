<div class="container">
    <div class="row">
        <div class="col-xl-12 left-sec">
<div ng-controller="FeaturedController" ng-init="moduleName='featured'; listEventFlag = true; viewEventFlag=false; createEventFlag=false;" >
	<div id='listing' ng-show='listEventFlag'>
		<div class="form-group"  style='margin-bottom:25px;'>
			<div class="col-md-4">
				<div class="input-group col-md-4">
					<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
					<input type="text" class="form-control input-sm" ng-model="searchText" ng-change="paginator.setPage(0);" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" style='width:200px;'> 
				</div>
			</div>
			<div class="col-md-2" style='font-size:12px;'>
                            selected Items: <strong> {{names.length}} </strong>
                        </div>
                        <div class="col-md-2">
                            <div class="btn-group">
                                <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style='font-size:11px;'>Page Size <span class="caret"></span></button>
                                <ul class="dropdown-menu" role="menu">
                                    <li ng-repeat="x in paginator.pagerange()"><a href ng-click="paginator.setRowperPage(x)">{{x}}</a></li>
                                </ul>
                            </div> 		
                        </div>
			<div class="col-md-4">
			<a href ng-click='create();' class="btn btn-success btn-sm" style='color:white;width:200px;'><span class='glyphicon glyphicon-certificate'></span> &nbsp;Assign featured restaurant</a>
			</div>
		</div>
            <div style=" clear: both;"></div>
		<table width='100%' class="table table-condensed table-striped" style='font-size:12px;'>
			<thead>
				<tr>
                                 
				<th ng-repeat="y in tabletitle"><tbtitle var="{{y.a}}" name='tabletitle' module='moduleName'/>{{y.a}}</th>
				<th>update</th><th> &nbsp; </th>
                                <th>delete</th><th> &nbsp; </th>
                             
				</tr>
			 </thead>
			<tbody style='font-family:helvetica;font-size:12px;'>
				<tr ng-repeat="x in filteredEvent = (names | filter:searchText) | sizefilter:this | orderBy:predicate:reverse | slicefilter:paginator.getPage():paginator.getRowperPage():paginator.getItemCount() | limitTo:paginator.getRowperPage()" style='font-family:helvetica;font-size:11px;'>
                                    <td ng-repeat="y in tabletitle">
                                       
                                        <span  ng-if="x[y.a]===true"><span style="color:green;" class='glyphicon glyphicon-ok'></span> </span>
                                        <span  ng-if="x[y.a]===false"><span style="color:red;" class='glyphicon glyphicon-remove'></span></span>
                                        <span ng-if="x[y.a]!==false && x[y.a]!==true ">
                                         {{ x[y.a] | adatereverse:y.c }}
                                        </span>
                                    </td>
					<td><a href ng-click="update(x)" style='color:blue;'><span class='glyphicon glyphicon-pencil'></a></span></td>
                                       <td width='30'>&nbsp;</td>
					<td nowrap><a href ng-click="delete(x)" style='color:red;'><span class='glyphicon glyphicon-trash'></a></span></td>
					
				</tr><tr><td colspan='{{tabletitle.length + 3}}'></td></tr>
			</tbody>
		</table>
		<div ng-if="filteredEvent.length >= paginator.getRowperPage()" class="col-md-12" align="center" style='font-size:11px' ng-include="'app/components/paginator.html'"></div>
	</div>

	

	<div class="col-md-12" ng-show='createEventFlag'>
		<br />
		<a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a>
		<br />
		 <div class="col-md-2"></div>
		 <div class="col-md-8">
		   <div class="row" ng-repeat="y in tabletitleContent | filter: {t: '!dontshow' }"  style="margin: 0 0 20px 0;font-size:12px;font-familly:Roboto">

                            <div class="input-group"  ng-if="y.t === 'input'">
                                    <span class="input-group-addon input11"><i class="glyphicon glyphicon-{{y.d}} input14"></i>&nbsp;{{y.b}}</span>
                                    <input type="text" class="form-control input-sm" ng-model="selectedItem[y.a]" ng-change="cleaninput(y.a)"  >
                            </div>
                                <div class="input-group" ng-if="y.t === 'dropdown'">
                                        <div class='input-group-btn' dropdown >
                                        <button type='button' class='btn btn-default dropdown-toggle btn-sm input11' data-toggle='dropdown'>
                                                <i class="glyphicon glyphicon-{{y.d}} input13"></i>&nbsp; {{y.b}}<span class='caret'></span></button>
                                        <ul class='dropdown-menu scrollable-menu' >
                                        <li ng-repeat="p in y.val"><a href ng-click="selectedItem[y.a]=p;y.func()">{{ p }}</a></li>
                                        </ul>
                                        </div>
                                        <input type='text' ng-model='selectedItem[y.a]' class='form-control input-sm' readonly >
                                </div>
                                <div class="input-group" ng-if="y.t === 'array'">
                                        <div class='input-group-btn' dropdown >
                                        <button type='button' class='btn btn-default dropdown-toggle btn-sm input11' data-toggle='dropdown'>
                                                <i class="glyphicon glyphicon-{{y.d}} input13"></i>&nbsp; {{y.b}}<span class='caret'></span></button>
                                        <ul class='dropdown-menu scrollable-menu' >
                                        <li ng-repeat="p in y.val"><a href ng-click="selectedItem[y.a]=p;y.func()">{{ p }}</a></li>
                                        </ul>
                                        </div>
                                        <input type='text' ng-model='selectedItem[y.a]' class='form-control input-sm' readonly >
                                </div>
                                   <div class="input-group" ng-if="y.t === 'checkarr'">
                                        <h4>select permission level</h4>
                                          <div ng-repeat="per in user.finalValue">
                                                <input  type="checkbox" ng-model="user.roles[per.name.toUpperCase()]" ng-true-value="'1'" ng-false-value="'0'" ng-checked ='per.{{per.name.toUpperCase()}}' > &nbsp; {{per.name}}

                                          </div>

                                
                                    </div>
                              
		   </div><br />
		</div>
		<div class="col-md-2"></div>
		<div class="col-md-7"></div>
		<div class="col-md-5">
			<a href ng-click='savenewlevel();' class="btn btn-success btn-sm" style="color:white;width:150px"><i class='glyphicon glyphicon-save'></i> &nbsp;{{ buttonlabel }} </a><br />
		</div>
		</div>
</div>

</div>
</div>
</div>

<script>
    <?php
    $mediadata = new WY_Media();
    $resdata = new WY_restaurant();
    //$imgAr = $mediadata->getEventPictureNames($theRestaurant);
        $resAr= $resdata->getSimpleListRestaurant();

    printf("var pathimg = '%s';", $mediadata->getFullPath('small'));
      printf("var resArr = [");
            for($i = 0, $sep = ""; $i < count($resAr); $i++, $sep = ", "){
                printf("%s '%s'", $sep, $resAr[$i]);
            }
    printf("];");  
?>

var token = <?php echo "'".$_SESSION['user_backoffice']['token']."';"; ?>
var email =  <?php echo "'".$_SESSION['user_backoffice']['email']."';";?>

app.controller('FeaturedController', ['$scope','$http','$timeout','bookService', function($scope,$http,$timeout,bookService) {
	var todaydate = new Date();
	var aorder = (function() { var i, arr=[]; for(i = 0; i < 30; i++) arr.push(i); return arr; })();
	
	$scope.paginator = new Pagination(25);
	$scope.path = pathimg;
	$scope.restaurant = <?php echo "'".$theRestaurant."';"; ?>
	$scope.email = <?php echo "'" . $email . "';"; ?>
	$scope.predicate = '';
	$scope.reverse = false;
	$scope.nonefunc = function() {};
    $scope.resArr = resArr;
         
    var status=['active','inactive'];
        
    $scope.levels = ["HOME", "MOBILE", "BLOG"];
	$scope.tabletitle = [ {a:'index', b:'ID', c:'' , q:'down', cc: 'black' }, {a:'restaurant', b:'Restaurant', c:'' , q:'down', cc: 'black' }, {a:'category', b:'Category', c:'' , q:'down', cc: 'black' }, { a:'home', b:'Home', c:'' , q:'down', cc: 'black' }, , {a:'blog', b:'Blog', c:'' , q:'down', cc: 'black' }, {a:'mobile', b:'Mobile', c:'' , q:'down', cc: 'black' }];
//        var rolesarr=['management','finance','marketing_manager','support_manager','sales_manager','it_manager','it_team'];
   
	$scope.tabletitleContent = [{a:'restaurant', b:'Restaurant', c:'', d:'cutlery', t:'dropdown', val:$scope.resArr,func: $scope.nonefunc },{ a:'category', b:'Type', c:'', d:'tower', t:'input', i:0 },{ a:'morder', b:'Preference Order', c:'', d:'sort', t:'array', val: aorder, func: $scope.nonefunc },{ a:'status', b:'Status', c:'', d:'sort', t:'dropdown', val:status,func: $scope.nonefunc},{ a:'title', b:'Title', c:'', d:'flag', t:'checkarr' }];
        $scope.user = {
			finalValue :[],
			roles :{},
			value: [],        
        };
     
	$scope.Objevent = function() {
		return {
			email: '', 
			index: 0, 
			restaurant: '', 
                        category:'',
                        home:false,
                        mobile:false,
                        blog:false,
                        status:'',
                        morder:'',
                        type:'',
                        
			remove: function() { 
				for(var i = 0; i < $scope.names.length; i++) 
					if($scope.names[i].name === this.name) {
						$scope.names.splice(i, 1);
						break;
						}
				},
				
			replicate: function(obj) {
				for (var attr in this)
		        	if(this.hasOwnProperty(attr) && typeof this[attr] !== 'function' && attr !== '$$hashKey') 
        				this[attr] = obj[attr];
				return this;
				},
										
			clean: function() {
				

				for (var attr in this)
		        	if(this.hasOwnProperty(attr) && typeof this[attr] !== 'function' && attr !== '$$hashKey') {
        				if(typeof this[attr] === 'string') {
        					this[attr] = this[attr].replace(/\'|\"/g, '’');
        					}
        				}
				return this;
				},
					
			check: function() {
				if(this.name === '' || this.name.length < 6) { alert('Invalid title name, empty or too short(5)'); return -1;}
				if(this.title === '' || this.title.length < 3) { alert('Invalid title name, empty or too short(2)'); return -1;}
				return 1;
				}
			};
	};
        
	var url ="../api/getrestmanagement";
	$http.get(url).then(function(response) {
		var items =$scope.levels;
		$scope.selectedItem=[];
		$scope.names = [];	 	
		var data = response.data.data;
			for (i = 0; i < data.length; i++) {
				data[i].index = data[i].id;
				data[i].type =data[i].category;
				var permission=data[i].is_display;
				var displayLevel = permission.split('||');  
				data[i].userlevel =displayLevel;
				angular.forEach(items, function(value, key){
					 if(displayLevel.indexOf(value)>-1){
						  var k=value.toLowerCase() ;
						data[i][k] = true;
						 
					}else{
						 var k=value.toLowerCase() ;
							   data[i][k] = false;    
					}

				});
	 
				var value = data[i];
			  $scope.names.push(new $scope.Objevent().replicate(value)); 
			}
		
		  $scope.names = data;
		   console.log(JSON.stringify($scope.names));

		 //$scope.imagesArr= response.data.data.pictures;
	});
 
         //$scope.tabletitle = $scope.roles;
      var navbar= [{"HOME":false,name:"home"},{"MOBILE":false,name:"mobile"},{"BLOG":false,name:"blog"}];
         $scope.user = {
             finalValue :navbar,
             roles :{},
             value: [],          
        };

	$scope.initorder = function() {
		$scope.tabletitle = $scope.bckups;
		$scope.predicate = "vorder";
		$scope.reverse = true;
	};

	$scope.reorder = function(item, alter) {
		alter = alter || "";
		if (alter !== "")  item = alter;
		$scope.reverse = ($scope.predicate === item) ? !$scope.reverse : false;
		$scope.predicate = item;
	};       

	$scope.cleaninput = function(ll) {
		if(typeof $scope.selectedItem[ll] === 'string')
			$scope.selectedItem[ll] =  $scope.selectedItem[ll].replace(/\'|\"/g, '’');
		};
		
	$scope.reset = function(item) {
		$scope.listEventFlag = false;
		$scope.viewEventFlag = false
		$scope.createEventFlag = false;
		$scope[item] = true;	
		};

	$scope.backlisting = function() {
		$scope.reset('listEventFlag');
		};
		
	$scope.findaccount = function(name) {
        var data = $scope.names;
		for(var i = 0; i < data.length; i++){
            if(data[i].index === name){
                return i;
                }
            }
        return -1;
		};
   		
	$scope.view = function(oo) {
		$scope.selectedItem = oo;
		$scope.reset('viewEventFlag');
		};
	
	$scope.create = function() {

		$scope.selectedItem = new $scope.Objevent();
		$scope.reset('createEventFlag');
		$scope.buttonlabel = "Save new user roles";
		$scope.action = "create";
		return false;
		}


	$scope.update = function(oo) {
//            $scope.user.roles=[];
		$scope.user.value =[];
		var obj={};
		var userLevel = oo.userlevel;

		if(!userLevel)
			return alert("undefined restaurant features");
		
		for(var i=0;i<$scope.user.finalValue.length;i++){
			values=$scope.user.finalValue[i];
			for (var key in values) {
				if (values.hasOwnProperty(key)) {
					if(key !=='name'){
					  if(userLevel.indexOf(key)>-1) {
						 var propkey = ''+ key +'';
						 obj[propkey] ='1';
						  $scope.user.finalValue[i][key] =true;
						   var propkey = '"' + key + '"'
						}

					}

				}
			}
		}

		$scope.user.roles = obj;
		$scope.selectedItem = new $scope.Objevent().replicate(oo);
		$scope.user.findValue =oo;
		$scope.reset('createEventFlag');
		$scope.buttonlabel = "Update categories";
		$scope.action = "update";
		
		};

	$scope.savenewlevel = function() {
		var u, msg, apiurl, ind;
 
 		if(typeof $scope.selectedItem.restaurant !== "string" || $scope.selectedItem.restaurant.length < 6)
 			return alert("invalid restaurant name");
 			
        $scope.user.value=[];
		
		var values = $scope.user.roles;

		angular.forEach(values, function(value, key) {
			if(value ==='1') {
				$scope.user.value.push(key);
				}
			});

		$scope.selectedItem.home= ($scope.user.roles['HOME'] === '1') ? true : false;
		$scope.selectedItem.mobile= ($scope.user.roles['MOBILE'] === '1') ? true : false;
		$scope.selectedItem.blog= ($scope.user.roles['BLOG'] === '1') ? true : false;

		if($scope.action == "update") {
            ind = $scope.findaccount($scope.selectedItem.index);
			if(ind >= 0) $scope.names.splice(ind, 1);
				$scope.names.push($scope.selectedItem);                     
			}

		else if($scope.action == "create") { 
			if($scope.user.value.length ===0 ) {
				return alert( " Please choose restaurant features !");
				}
			$scope.names.push($scope.selectedItem);
            }
                 
		if($scope.action === "create"){

            $http.post("../api/savecategory",
			{
			'restaurant' : $scope.selectedItem.restaurant,
			'status'  : $scope.selectedItem.status,
			'category':$scope.selectedItem.category,
			'morder'  : $scope.selectedItem.morder,
			'level':$scope.user.value,
			'type':'create'
			}).then(function(response) { alert("restaurant category has been created");});
                    }
			//bookService.createEvent($scope.restaurant, $scope.email, $scope.selectedItem ).then(function(response) { alert("Event has been created"); });
		else {
            $http.post("../api/savecategory",
			{ 
		   'id'  : $scope.selectedItem.index,
			'restaurant' : $scope.selectedItem.restaurant,
			'status'  : $scope.selectedItem.status,
			'category':$scope.selectedItem.category,
			'morder'  : $scope.selectedItem.morder,
			'level':$scope.user.value,
			'type':'update'
            }).then(function(response) {console.log(JSON.stringify(response));alert("restaurant category  has been updated");});
                    //bookService.updateEvent($scope.restaurant, $scope.email, $scope.selectedItem ).then(function(response) { alert("Event has been updated"); });
        }
		
 		$scope.backlisting();	
                
		};
		
    $scope.delete = function(oo) {
		if(confirm("Are you sure you want to delete " + oo.restaurant) == false)
			return;
		var resto = oo.restaurant;
        $http.post("../api/deletecategory",
			{               
			'id'  : oo.index,
			'restaurant' : oo.restaurant,
			'status'  : oo.status,
			'category':oo.category                           
            }).then(function(response) { 
            	var ind = $scope.findaccount(oo.index);
				if(ind >= 0) 
					$scope.names.splice(ind, 1); 
				alert(resto + " has been deleted"); 
				});
		};
		
	
}]);

	
</script>

