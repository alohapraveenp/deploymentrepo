app.controller('updateRestaurantController', ['$scope', 'API', function ($scope, API) {
        $scope.email = $('#itememail').val();

        API.verifyemail.getStatus($scope.email)
                .then(function (response) {
                    if (response.status == 1) {
                        $scope.emailstatus = response.data.status;
                    }
                });


        $scope.VerifyEmailAddress = function () {
            var status =  $scope.emailstatus;
            $scope.emailstatus = 'checking';
            
            API.verifyemail.verifyEmailAddress($scope.email)
                    .then(function (response) {
                        if (response.status == 1) {
                            if(response.data.status === 'pending' && status === 'pending'){
                                $scope.emailstatus = 'pending_resent';
                            }else{
                                $scope.emailstatus = response.data.status;
                            }
                        }
                    });
            if($scope.emailstatus === 'checking'){
                $scope.emailstatus = 'pending';
            }
        };
    }]);