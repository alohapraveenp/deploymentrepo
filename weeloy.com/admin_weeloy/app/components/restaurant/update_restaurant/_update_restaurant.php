<div ng-controller="updateRestaurantController" >
    <div class="form-group row">
        <label for="restaurant_email" class="col-sm-2 control-label">Email</label>
        <div class="col-sm-6">
            <input type="text" value="{{email}}" class="form-control input-sm" id="itemupdateemail" name="itemupdateemail" placeholder="Email"> <h6 style="color:#9a9a9a;"></h6> 
        </div>
        <div class="col-sm-2"></div>
        
        <div class="col-sm-10" ng-show="!emailstatus">
            <label>loading...</label>
        </div>
        
        <div class="col-sm-10" ng-show="emailstatus == 'unknown'">
            <label>To use this email address in the From field of White label emails, this address must be verified.</label><a  style="cursor:pointer;" ng-click="VerifyEmailAddress()"> Verify this address</a>
        </div>
        
        <div class="col-sm-10" ng-show="emailstatus == 'pending'">
            <label>An email has been sent to {{email}}. Please check your inbox (or spam folder) and follow the instructions.</label><a style="cursor:pointer;" ng-click="VerifyEmailAddress()"> Re-send</a>
        </div>
        
        <div class="col-sm-10" ng-show="emailstatus == 'pending_resent'">
            <label>RESENT - An email has been sent to {{email}}. Please check your inbox (or spam folder) and follow the instructions.</label>
        </div>
        
        <div class="col-sm-10" ng-show="emailstatus == 'verified'">
        <label >This email has been verified</label>
        </div>
        
        <div class="col-sm-10" ng-show="emailstatus == 'checking'">
            <label  style="cursor: wait;">Sending ...</label>
        </div>
    </div>
</div>

<script type="text/javascript" src="app/components/restaurant/update_restaurant/updateRestaurantController.js"></script>