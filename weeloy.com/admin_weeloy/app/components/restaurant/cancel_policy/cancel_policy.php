<?php
  $res = new WY_restaurant;
  $res->getRestaurant($theRestaurant);
  $multProduct = ($res->mutipleProdAllote()) ? $res->dfproduct : "";

?>
<div class="container">
    <div class="row">
        <div class="col-md-12 left-sec">
<div ng-controller="CancelController" ng-init="moduleName='cancel'; listEventFlag = true;  createEventFlag=false;" >


    <div id='listing' ng-show='listEventFlag'>
        <div class="form-group"  style='margin-bottom:25px;'>
            <div class="col-md-4">
                <div class="input-group col-md-4">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                    <input type="text" class="form-control input-sm" ng-model="searchText" ng-change="paginator.setPage(0);" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" style='width:200px;'> 
                </div>
            </div>
            <div class="col-md-2"></div>
            <div class="col-md-4">
            <a href ng-click='create();' class="btn btn-success btn-sm" style='color:white;width:200px;'><span class='glyphicon glyphicon-certificate'></span> &nbsp;Create a New Policy</a>
            </div>
        </div>
    
            <div style=" clear: both;"></div>
                <table width='100%' class="table table-condensed table-striped" style='font-size:12px;'>
                    <thead>
                        <tr>
                        <th ng-repeat="y in tabletitle"><tbtitle var="{{y.a}}" name='tabletitle' module='moduleName'/></th>
                        <th>update</th><th> &nbsp; </th>
                        <th>delete</th><th> &nbsp; </th>
                        </tr>
                    </thead>
                    <tbody style='font-family:helvetica;font-size:12px;'>
                            <tr ng-repeat="x in filteredEvent = (names | filter:searchText) | sizefilter:this | orderBy:predicate:reverse | slicefilter:paginator.getPage():paginator.getRowperPage():paginator.getItemCount() | limitTo:paginator.getRowperPage()" style='font-family:helvetica;font-size:11px;'> 
                                    <td ng-repeat="y in tabletitle"><a href ng-click="view(x)" ng-style="{ 'text-align' : getAlignment(x[y.a]) }"> {{ x[y.a] | adatereverse:y.c | notzero }}</a></td>
                                    <td><a href ng-click="update(x)" style='color:blue;'><span class='glyphicon glyphicon-pencil'></span></a></td>
                              <td width='30'>&nbsp;</td>
                              <td><a href ng-click="delete(x)" style='color:red;'><span class='glyphicon glyphicon-trash'></span></a></td>
                              </tr>
                            <tr><td colspan='{{tabletitle.length + 5}}'></td></tr>

                    </tbody>

                </table>
<!--        <div ng-if="filteredEvent.length >= paginator.getRowperPage()" class="col-md-12" align="center" style='font-size:11px' ng-include="'inc/paginator.html'"></div>-->
    </div>

    

    <div class="col-md-12" ng-show='createEventFlag'>
        <br />
        <a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a>
        <br />
         <div class="col-md-2"></div>
         <div class="col-md-8">
           <div class="row" ng-repeat="y in tabletitleContent | filter: {t: '!dontshow' }" style="margin: 0 0 20px 0;font-size:12px;font-familly:Roboto">

            <div class="input-group" ng-if="y.t === 'input'">
                <span class="input-group-addon input11"><i class="fa fa-{{y.d}}"></i>&nbsp; {{y.b}}</span>
                <input type="text" class="form-control input-sm" ng-model="selectedItem[y.a]" ng-change="cleaninput(y.a)" ng-readonly="y.a === 'event'" >
            </div> 
                    <div class="input-group" ng-if="y.t === 'inputprice' && showprice===1 ">
                <span class="input-group-addon input11"><i class="fa fa-{{y.d}}"></i>&nbsp; {{y.b}}</span>
                <input type="text" class="form-control input-sm" ng-model="selectedItem[y.a]" ng-change="cleaninput(y.a)" ng-readonly="y.a === 'event'" >
            </div> 
            <div class="input-group" ng-if="y.t === 'textarea'">
                <span class="input-group-addon input11"><i class="fa fa-{{y.d}}"></i>&nbsp; {{y.b}}</span>
                <textarea class="form-control input-sm" ng-model="selectedItem[y.a]" ng-change="cleaninput(y.a)" rows="5" ></textarea>
            </div>

            <div class="input-group" ng-if="y.t === 'date'">
                <span class="input-group-btn input11">
                <button type="button" class="btn btn-default btn-sm" ng-click="y.u.dateopen($event)"><i class="fa fa-calendar"></i>&nbsp; {{y.b}}</button>
                </span>
                <input type="text" class="form-control input-sm" uib-datepicker-popup="{{y.u.formats[0]}}" min-date="y.u.minDate" max-date="y.u.maxDate" ng-model="y.u.originaldate"
                is-open="y.u.opened" datepicker-options="y.u.dateOptions" ng-change="y.u.onchange();" close-text="Close" readonly />
            </div>
               <div class="input-group"  ng-if="y.t==='checkbox' ">
                   <input   type="checkbox" ng-model="selectedItem[y.a]" ng-checked ="{{selectedItem[y.a]}}"> &nbsp; {{y.b}}
                       
                </div>
            <div class="input-group" ng-if="y.t === 'array'">
                <div class='input-group-btn' uib-dropdown >
                <button type='button' class='btn btn-default btn-sm input12' uib-dropdown-toggle >
                    <i class="fa fa-{{y.d}}"></i>&nbsp; {{y.b}} <span class='caret'></span></button>
                <ul class='dropdown-menu scrollable-menu' uib-dropdown-menu>
                <li ng-repeat="p in y.val"><a href ng-click="selectedItem[y.a]=p;y.func()">{{ p }}</a></li>
                </ul>
                </div>
                <input type='text' ng-model='selectedItem[y.a]' class='form-control input-sm' readonly >
            </div>
                       
            <div class="input-group" ng-if="y.t === 'picture'">
                <div class='input-group-btn' uib-dropdown >
                    <button type='button' class='btn btn-default btn-sm input12' uib-dropdown-toggle >
                        &nbsp;<i class="fa fa-{{y.d}}"></i>&nbsp; {{y.b}} <span class='caret'></span>
                    </button>
                    <ul class='dropdown-menu scrollable-menu' uib-dropdown-menu>
                        <li ng-repeat="p in imgEvent"><a href ng-click="selectedItem['picture'] = p;">{{ p }}</a></li>
                    </ul>
                </div>
                <input type='text' ng-model="selectedItem['picture']" class='form-control input-sm' readonly >
            </div>
            <div class="input-group" ng-if="y.t === 'pictureshow'">
                <p ng-if="selectedItem.picture != ''"><img ng-src="{{path}}{{selectedItem.picture}}" height='150'/></p>
            </div>

           </div><br />
        </div>
        <div class="col-md-2"></div>
        <div class="col-md-7"></div>
        <div class="col-md-5">
            <a href ng-click='savePolicy();' class="btn btn-success btn-sm" style="color:white;width:150px"><i class='glyphicon glyphicon-save'></i> &nbsp;{{ buttonlabel }} </a><br />
        </div>
        </div>
</div>

</div>
</div>
</div>

<script>

var token = <?php echo "'".$_SESSION['user_backoffice']['token']."';"; ?>
var email =  <?php echo "'".$_SESSION['user_backoffice']['email']."';";?>


app.controller('CancelController', ['$scope', 'adminServiceApi', '$http', function($scope, adminServiceApi, $http) {
       
    var todaydate = new Date();
     
  $scope.paginator = new Pagination(25);
  $scope.paginatorsub = new Pagination(25); 
    $scope.evbooking = [];
    $scope.names = [];
    
    $scope.restaurant = <?php echo "'".$theRestaurant."';"; ?>
    $scope.email = <?php echo "'" . $email . "';"; ?>
   
    $scope.predicate = '';
    $scope.reverse = false;
    $scope.bkproduct ='';
    
    $scope.nonefunc = function() {};
    $scope.mydata_start = new adminServiceApi.ModalDataBooking();
    $scope.mydata_endng = new adminServiceApi.ModalDataBooking();
    $scope.mydata_start.setInit(todaydate, "09:00", function() { $scope.selectedItem.start = $scope.mydata_start.originaldate.getDateFormatReverse('-'); }, 3, todaydate, new Date(todaydate.getFullYear()+1, 11, 31));
    $scope.mydata_endng.setInit(todaydate, "09:00", function() { $scope.selectedItem.end = $scope.mydata_endng.originaldate.getDateFormatReverse('-'); }, 3, todaydate, new Date(todaydate.getFullYear()+1, 11, 31));

    $scope.tabletitle = [ {a:'duration', b:'Duration', c:'' , l:'', q:'down', cc: 'fuchsia' }, {a:'percentage', b:'Percentage', c:'' , l:'25', q:'down', cc: 'black' }, {a:'belowperct', b:'Below duration', c:'' , l:'', q:'down', cc: 'black' },{a:'pax', b:'Pax', c:'' , q:'down', cc: 'black' }, {a:'lunch', b:'Lunch Charge', c:'' , l:'', q:'down', cc: 'black' }, {a:'dinner', b:'Dinner Charge', c:'' , l:'', q:'down', cc: 'black' },{a:'product', b:'Product', c:'' , l:'', q:'down', cc: 'black' },{ a:'start', b:'Start', c:'date' , q:'down', cc: 'black' }, {alter:'endv',  a:'end', b:'End', c:'date' , q:'down', cc: 'black' } ];    
    $scope.tabletitleContent = [{ a:'lunch', b:'Lunch Charge', c:'', d:'money', t:'input', i:0 }, 
                                         { a:'dinner', b:'Dinner Charge', c:'', d:'money', t:'input', i:0 },
                                         { a:'pax', b:'Pax', c:'', d:'tag', t:'input' }, 
                                         { a:'duration', b:'Duration', c:'', d:'map-marker', t:'input', i:0 },
                                         { a:'percentage', b:'Percentage', c:'', d:'clock-o', t:'input', i:0 },
                                         { a:'belowperct', b:'Below ', c:'', d:'clock-o', t:'input'}, 
                                         {alter:'startv',a:'start', b:'Starting Date', c:'date', d:'calendar', t:'date', u: $scope.mydata_start },
                                         {alter:'endv',a:'end', b:'Ending Date', c:'date', d:'calendar', t:'date', u:$scope.mydata_endng },
                                         {a:'product', b:'Product', c:'', d:'tag', t:'input',i:0 }];

 $scope.Objpolicy = function() {
     return {
         restaurant: $scope.restaurant, 
         id :'',
         index: 0, 
         lunch: '',
         dinner: '', 
         pax: '', 
         duration: '', 
         percentage :'',
         start: '', 
         end: '', 
         startv:'',
         endv :'',
         sddate:'',
         eddate:'',
         belowperct: '', 
         product :'',
         
         
         remove: function() { 
             for(var i = 0; i < $scope.names.length; i++) 
                 if($scope.names[i].name === this.name) {
                     $scope.names.splice(i, 1);
                     break;
                     }
             },
             
         replicate: function(obj) {
             for (var attr in this)
                 if(this.hasOwnProperty(attr) && typeof this[attr] !== 'function' && attr !== '$$hashKey') 
                     this[attr] = obj[attr];
             return this;
             },
                                     
         clean: function() {
             if(this.start === '') this.start = $scope.mydata_start.getDate('-', 'reverse');
             if(this.end === '') this.end = $scope.mydata_endng.getDate('-', 'reverse');
                     if(this.display === '') this.display = $scope.mydata_display.getDate('-', 'reverse');

             for (var attr in this)
                 if(this.hasOwnProperty(attr) && typeof this[attr] !== 'function' && attr !== '$$hashKey') {
                     if(typeof this[attr] === 'string') {
                         this[attr] = this[attr].replace(/\'|\"/g, '’');
                         }
                     }
             return this;
             },
                 
         check: function() {
             if(this.name === '' || this.name.length < 6) { alert('Invalid title name, empty or too short(5)'); return -1;}
             if(this.title === '' || this.title.length < 3) { alert('Invalid title name, empty or too short(2)'); return -1;}
             return 1;
             }
         };
 };
 
 adminServiceApi.getCancelPolicy($scope.restaurant,$scope.bkproduct)
       .then(function (response) {
           console.log("RESPONSE" + response.data +"STATUS " +response.status);

    if(response.data.status > 0) {
        console.log("HI");
           var data = response.data.data;
           console.log("SDSDs" +JSON.stringify(response.data));
        for (i = 0; i < data.length; i++) {
                data[i].index = i + 1;
                data[i].lunch = data[i].lunch_charge;
                data[i].dinner = data[i].dinner_charge;
                $scope.names.push(new $scope.Objpolicy().replicate(data[i]));
        }
    } 
    console.log("NAMES" +$scope.names);
                    
 });
        
        
    $scope.initorder = function() {
        $scope.predicate = "vorder";
        $scope.reverse = true;
    };

    $scope.reorder = function(item, alter) {
        alter = alter || "";
        if (alter !== "")  item = alter;
        $scope.reverse = ($scope.predicate === item) ? !$scope.reverse : false;
        $scope.predicate = item;
    };
    $scope.create = function() {

            $scope.selectedItem = new $scope.Objpolicy();
//            $scope.mydata_start.setDate(todaydate);
//            $scope.mydata_endng.setDate(todaydate);
            $scope.reset('createEventFlag');
            $scope.buttonlabel = "Save new event";
            $scope.action = "create";
            return false;
    };
    $scope.update = function(oo) {
        $scope.selectedItem = new $scope.Objpolicy().replicate(oo);
        $scope.mydata_start.originaldate = new Date($scope.selectedItem.start);
        $scope.mydata_endng.originaldate = new Date($scope.selectedItem.end);
        $scope.reset('createEventFlag');
        $scope.buttonlabel = "Update new policy";
        $scope.action = "update";
    };
    $scope.findaccount = function(id) {
        for(var i = 0; i < $scope.names.length; i++)
                if($scope.names[i].id === id)
                        return i;
        return -1;
    };
    $scope.savePolicy = function () {
        var oo = $scope.selectedItem;
        if($scope.action === "create"){
            $scope.selectedItem.start = $scope.mydata_start.originaldate.getDateFormatReverse('-');
            $scope.selectedItem.end = $scope.mydata_endng.originaldate.getDateFormatReverse('-');
        
            adminServiceApi.savePolicy($scope.restaurant,$scope.selectedItem)
                .then(function (response) {
                    console.log(response);
                    if (response.status > 0) {
                        $scope.names.push(oo);
                       alert("SuccessFully Saved Your Changes.");
                    }
            });
        }else{
            adminServiceApi.updatePolicy($scope.restaurant,oo).then(function(response) {
                console.log(response.status);
                if(response.status > 0) {
                    ind = $scope.findaccount(oo.id);	
                        if(ind >= 0) $scope.names.splice(ind, 1);
                        $scope.names.push(oo);
                        alert("Cancel policy has been updated");
                }
                });
            }
        $scope.backlisting();
    };
    $scope.delete = function(oo){
 
        adminServiceApi.deletePolicy($scope.restaurant,oo)
                .then(function (response) {
                    console.log(response);
                    if (response.status > 0) {
                       ind = $scope.findaccount(oo.id);	
                        if(ind >= 0) $scope.names.splice(ind, 1);
                        alert("Cancel policy has been Deleted");
                    }
       });
     
    };
        
        
     

    $scope.cleaninput = function(ll) {
        if(typeof $scope.selectedItem[ll] === 'string' && $scope.selectedItem[ll] !== '')
            $scope.selectedItem[ll] =  $scope.selectedItem[ll].replace(/\'|\"/g, '’');
        };
        
    $scope.reset = function(item) {
        $scope.listEventFlag = false;
        $scope.viewEventFlag = false
        $scope.createEventFlag = false;
        $scope.viewlistBkEventFlag = false;
        $scope.viewBkEventFlag = false;
        $scope[item] = true;    
        };

    $scope.backlisting = function(page) {
        if(page === undefined)
            $scope.reset('listEventFlag');
        else $scope.reset(page);
        };
 
}]);

    
</script>



