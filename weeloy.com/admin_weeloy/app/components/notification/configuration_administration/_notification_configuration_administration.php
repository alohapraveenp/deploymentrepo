<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="container">
    <div class="row">
        <div class="col-md-12 left-sec">
            <div ng-controller="NotificationConfigurationAdController" ng-init="moduleName = 'notifycreate'; "  vieweventflag = false; createeventflag = false;" >
                <div ng-show='listactionflag'>
                    <div class="form-group"  style='margin-bottom:25px;'>
                        <div class="col-md-4">
                            <div class="input-group col-md-4">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                                <input type="text" class="form-control input-sm" ng-model="searchText" ng-change="paginator.setPage(0);" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" style='width:200px;'> 
                            </div>
                        </div>
                        <div class="col-md-2" style='font-size:12px;'>
                            selected Items: <strong> {{names.length}} </strong>
                        </div>
                        <div class="col-md-2">
                            <div class="btn-group">
                                <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style='font-size:11px;'>Page Size <span class="caret"></span></button>
                                <ul class="dropdown-menu" role="menu">
                                    <li ng-repeat="x in paginator.pagerange()"><a href ng-click="paginator.setRowperPage(x)">{{x}}</a></li>
                                </ul>
                            </div> 		
                        </div>
                        <div class="col-md-2">
                            <a href ng-click='create();' class="btn btn-success btn-sm" style='color:white;width:200px;'><span class='glyphicon glyphicon-certificate'></span> &nbsp;Create a New Notifiy Action</a>
                        </div>
                    </div>
                    <div style=" clear: both;"></div>
                    <table width='100%' class="table table-condensed table-striped" style='font-size:12px;'>
                                <tr>
                                <th ng-repeat="y in tabletitleContent"><tbtitle var="{{y.a}}" name='tabletitle' module='moduleName'/></th>
<!--                                <th ng-repeat="y in tabletitle">{{y}}</th>-->
                                <th>update</th>
				</tr>
                        
                        
                        <tbody style='font-family:helvetica;font-size:12px;'>
				<tr ng-repeat="x in filteredEvent = (names | filter:searchText) | sizefilter:this | orderBy:predicate:reverse | slicefilter:paginator.getPage():paginator.getRowperPage():paginator.getItemCount() | limitTo:paginator.getRowperPage()" style='font-family:helvetica;font-size:11px;'>
                                    {{x|json}}
                                    <td ng-repeat="y in tabletitleContent">
                                        <span  ng-if="x[y.a]===true"><span style="color:green;" class='glyphicon glyphicon-ok'></span> </span>
                                        <span  ng-if="x[y.a]===false"><span style="color:red;" class='glyphicon glyphicon-remove'></span></span>
                                      
                                        <span ng-if="x[y.a]!==true && x[y.a]!==false ">
                                         {{ x[y.a] | adatereverse:y.c }}
                                        </span>
                                    </td>
					<td><a href ng-click="update(x)" style='color:blue;'><span class='glyphicon glyphicon-pencil'></a></span></td>
<!--                                       <td width='30'>&nbsp;</td>
					<td nowrap><a href ng-click="delete(x)" style='color:red;'><span class='glyphicon glyphicon-trash'></a></span></td>-->
					   
				</tr><tr><td colspan='{{tabletitle.length + 3}}'></td></tr>
                         
			</tbody>
                    </table>
                    <div ng-if="filteredEvent.length >= paginator.getRowperPage()" class="col-md-12" align="center" style='font-size:11px' ng-include="'app/components/paginator.html'"></div>
                </div>
                <div class="col-md-12" ng-show='notifycreate' >
                    <br />
                    <a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a>
                    <br /> <br />
                    <div class="col-md-8" style="padding-bottom: :0px;" >
                        <div class="row" ng-repeat="y in tabletitleContent| filter: {t: '!dontshow' }"  style="margin: 0 0 20px 0;font-size:12px;font-familly:Roboto">
                            <div class="input-group"  ng-if="y.t === 'inputtype'">
                                <span class="input-group-addon input11"><i class="glyphicon glyphicon-{{y.d}} input14"></i>&nbsp;{{y.b}}</span>
                                <input type="text" class="form-control input-sm" auto-complete ui-items="names" on-type="updatetype" ng-model="selectedItem[y.a]" ng-change="cleaninput(y.a)" maxlength="120" >
                            </div>

                            <div class="input-group"  ng-if="(y.t === 'inputtitle')">
                                <span class="input-group-addon input11"><i class="glyphicon glyphicon-{{y.d}} input14"></i>&nbsp;{{y.b}}</span>
                                <input type="text" class="form-control input-sm" ng-model="selectedItem[y.a]" ng-change="cleaninput(y.a)" maxlength="120" >
                            </div>
                            <div class="input-group" ng-if="y.t === 'dropdown'">
                                <div class='input-group-btn' dropdown >
                                    <button type='button' class='btn btn-default dropdown-toggle btn-sm input11' data-toggle='dropdown'>
                                            <i class="glyphicon glyphicon-{{y.d}} input13"></i>&nbsp; {{y.b}}<span class='caret'></span></button>
                                    <ul class='dropdown-menu scrollable-menu'style='height: auto;max-height:120px; overflow-x: hidden;' >
                                    <li ng-repeat="p in y.val"><a href ng-model='selectedItem[y.a]' ng-click="notifystatus( p) ">{{p}}</a></li>
                                    </ul>
                                </div>
                             
                                <input type='text' ng-model='selectedItem[y.a]' class='form-control input-sm' readonly >
                            </div>
                               <div class="input-group" ng-if="y.t === 'labelcheckarr'">
                                     <input  type="checkbox" ng-model="selectedItem[y.a]"  ng-true-value="'1'" ng-false-value="'0'" ng-checked ='selectedItem[y.a]' > &nbsp; {{y.a}}
                                    </div>
                   
                             <div class="input-group" ng-if="y.t === 'checkarr'">
                                 <div ng-if="y.a === 'email'"> <h4>Current Active actions</h4> </div> 
                                        <input  type="checkbox" ng-model="selectedItem[y.a]" ng-true-value="'1'" ng-false-value="'0'" ng-checked ='selectedItem[y.a]' > {{y.a}}
                                  </div>
                        </div>
                    </div>

                    <div class="col-md-2"></div>
                    <div class="col-md-5"></div>
                    <div class="col-md-5" style="margin-top:10px;">

                        <a href ng-click='saveaction();' class="btn btn-info btn-sm" style="color:white;"><i class='glyphicon glyphicon-save'></i> &nbsp;{{ buttonlabel }} </a><br />
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="app/components/notification/configuration_administration/notificationConfigurationAdministrationController.js"></script>