//app.controller('NotificationConfigurationAdController', ['$scope', '$http', '$timeout', 'bookService', function ($scope, $http, $timeout, bookService) {
 app.controller('NotificationConfigurationAdController', ['$scope', 'API', function ($scope, API) {
        $scope.paginator = new Pagination(25);
        $scope.listactionflag = true;
         
        $scope.nonefunc = function () {
        };
        $scope.selectedItem = [];
        
        
        $scope.Objnotify = function () {
            return {
                parent_group_action: '',
                group_action:'',
                notification_type:'',
                white_label:0,
                email: 1,
                sms: 1,
               'push_notification': false,
               'sms_premium': false,
                status: '',
                index: '',
                replicate: function (obj) {
                    for (var attr in this)
                        if (this.hasOwnProperty(attr) && typeof this[attr] !== 'function' && attr !== '$$hashKey')
                            this[attr] = obj[attr];
                    return this;
                },
            };
        };
       
        API.notifyconfiguration.getNotifytype().then(function (response) {
              var data =response.data.notify,category=[];
                     $scope.names = [];
  
                 for (var i = 0; i< data.length; i++) {
                       data[i].index =data[i].id;
                       data[i].email = (data[i].email ==='1') ? true : false;
                       data[i].sms =(data[i].sms === '1') ? true : false; 
                       data[i].push_notification=(data[i].push_notification === '1') ? true : false;
                       data[i].sms_premium=(data[i].sms_premium === '1') ? true : false; 
                      $scope.names.push(new $scope.Objnotify().replicate(data[i])); 
                 }
                 console.log(JSON.stringify($scope.names));
              
        });
           $scope.backlisting = function () {
            $scope.reset('listactionflag');
        };
          
         $scope.reset = function (item) {
            $scope.listactionflag = false;
            $scope.notifycreate = false;
            $scope[item] = true;
        };
          
        $scope.create = function () {
//            $scope.selectedItem = [{'white_label':false,'email':true,'sms':false,'sms_premium':true,'push_notiifcation':false}];
//            $scope.selectedItem=[];
            $scope.reset('notifycreate');
            $scope.buttonlabel = "Create Notiifcation Action";
            $scope.action = "create";
            return false;
        }

         var status = ['active', 'inactive'];
         //$scope.tabletitle = [{a: 'index', b: 'index', c: '', q: 'down', cc: 'black'}, {a: 'action', b: 'Action', c: '', q: 'down', cc: 'black'}, {a: 'label', b: 'Label', c: '', q: 'down', cc: 'black'}, {a: 'email', b: 'Email', c: '', q: 'down', cc: 'black'}, {a: 'sms', b: 'SMS', c: '', q: 'down', cc: 'black'}, {a: 'sms premium', b: 'SMS PREMIUM', c: '', q: 'down', cc: 'black'}, {a: 'push notification', b: 'PUSH NOTIFICATION', c: '', q: 'down', cc: 'black'}];
         $scope.tabletitleContent = [{a: 'parent_group_action', b: 'Parent Group', c: '', d: 'flag', t: 'inputtype', i: 0},{a: 'group_action', b: 'Group action', c: '', d: 'flag', t: 'inputtype', i: 0}, {a: 'notification_type', b: 'Notification Type', c: '', d: 'flag', t: 'inputtype', i: 0}, {a: 'status', b: 'Status', c: '', d: 'sort', t: 'dropdown', val: status, func: $scope.nonefunc},{a: 'white_label', b: 'White Label', c: '', d: 'flag', t: 'labelcheckarr'}, {a: 'email', b: 'Email ', c: '', d: 'flag', t: 'checkarr'},{a: 'sms', b: 'Sms ', c: '', d: 'flag', t: 'checkarr'},{a: 'sms_premium', b: 'Sms_Premium ', c: '', d: 'flag', t: 'checkarr'},{a: 'push_notification', b: 'Push_Notification ', c: '', d: 'flag', t: 'checkarr'}];
           
        var options = [{EMAIL: false, name: "email"}, {SMS: false, name: "sms"}, {SMSPREMIUM: false, name: "sms_premium"}, {PUSHNOTIFICATION: false, name: "push_notification"}];
        $scope.permission = {
            finalValue: options,
            roles: {},
            value: [],
        };
        $scope.notifystatus = function(val){
    
           //$scope.selectedItem['status']= val;
        }
        
       $scope.saveaction = function () {

        
            $scope.selectedItem.white_label = ($scope.selectedItem.white_label) ? true : false;
            $scope.selectedItem.email =($scope.selectedItem.email) ? true : false; 
            $scope.selectedItem.sms_premium=( $scope.selectedItem.sms_premium) ? true : false;
            $scope.selectedItem.sms=($scope.selectedItem.sms) ? true : false; 
            $scope.selectedItem.push_notiifcation=( $scope.selectedItem.push_notiifcation) ? true : false;

            API.notifyconfiguration.saveNotify( $scope.selectedItem).then(function (response) {
              console.log(JSON.stringify(response)); 
              $scope.backlisting();
            });

            //}

       };
//        //$scope.names= ['sms','email','push notiifcation'];
//        var status = ['active', 'inactive'];
//        $scope.updatetype = function () {
//            bookService.getNotifytype().then(function (response) {
//                var data = response.data.notify, names = [], category = [];
//                for (var i = 0; i < data.length; i++) {
//                    if (data[i].action === 'main') {
//                        names.push(data[i].type);
//                    }
//                    category.push(data[i].action);
//                }
//                $scope.names = names;
//                $scope.category = category;
//            });
//        };
//        $scope.levels = ["EMAIL", "SMS", "SMS PREMIUM", "PUSH NOTIFICATION"];
//        $scope.tabletitle = [{a: 'index', b: 'index', c: '', q: 'down', cc: 'black'}, {a: 'action', b: 'Action', c: '', q: 'down', cc: 'black'}, {a: 'label', b: 'Label', c: '', q: 'down', cc: 'black'}, {a: 'email', b: 'Email', c: '', q: 'down', cc: 'black'}, {a: 'sms', b: 'SMS', c: '', q: 'down', cc: 'black'}, {a: 'sms premium', b: 'SMS PREMIUM', c: '', q: 'down', cc: 'black'}, {a: 'push notification', b: 'PUSH NOTIFICATION', c: '', q: 'down', cc: 'black'}];
//        $scope.tabletitleContent = [{a: 'type', b: 'Notify Type', c: '', d: 'flag', t: 'inputtype', i: 0}, {a: 'action', b: 'Action', c: '', d: 'flag', t: 'inputtitle', i: 0}, {a: 'label', b: 'Label', c: '', d: 'flag', t: 'inputtitle', i: 0}, {a: 'status', b: 'Status', c: '', d: 'sort', t: 'dropdown', val: status, func: $scope.nonefunc}, {a: 'category', b: 'Category', c: '', d: 'flag', t: 'input', i: 0}, {a: 'label', b: 'Label', c: '', d: 'flag', t: 'input', i: 0}, {a: 'title', b: 'Title', c: '', d: 'flag', t: 'checkarr'}];
//        $scope.permission = {
//            finalValue: [],
//            roles: {},
//            value: [],
//        };
       
//        bookService.getNotifytype().then(function (response) {
//            var data = response.data.notify, category = [];
//            var items = $scope.levels;
//            $scope.selectedItem = [];
//            $scope.names = [];
//            $timeout(function () {
//                for (var i = 0; i < data.length; i++) {
//                    data[i].index = data[i].id;
//                    var permission = data[i].is_active;
//                    var displayLevel = permission.split('||');
//                    data[i].accesslevel = displayLevel;
//                    angular.forEach(items, function (value, key) {
//                        if (displayLevel.indexOf(value) > -1) {
//                            var k = value.toLowerCase();
//                            data[i][k] = true;
//
//                        } else {
//                            var k = value.toLowerCase();
//                            data[i][k] = false;
//                        }
//
//                    });
//                    var value = data[i];
//                    $scope.names.push(new $scope.Objevent().replicate(value));
//                }
//                $scope.names = data;
//
//            }, 200);
//
//        });
//
//        var options = [{EMAIL: false, name: "email"}, {SMS: false, name: "sms"}, {SMSPREMIUM: false, name: "sms premium"}, {PUSHNOTIFICATION: false, name: "push notification"}];
//        $scope.permission = {
//            finalValue: options,
//            roles: {},
//            value: [],
//        };
//        $scope.selectedItem = [];
//
//        $scope.saveaction = function () {
//            if ($scope.selectedItem.label === '' || $scope.selectedItem.category === "") {
//                alert("Please Complete All Required fields!");
//                return;
//            }
//
//            $scope.permission.value = [];
//
//            var values = $scope.permission.roles;
//            angular.forEach(values, function (value, key) {
//                if (value === '1') {
//                    $scope.permission.value.push(key);
//                }
//            });
//            if ($scope.action == "update") {
//                ind = $scope.findaccount($scope.selectedItem.index);
//                $scope.names.splice(ind, 1);
//                $scope.names.push($scope.selectedItem);
//
//            }
//            if ($scope.action == "create") {
//
//                $http.post("../api/notification/create",
//                        {
//                            'type': $scope.selectedItem.type,
//                            'category': $scope.selectedItem.action,
//                            'label': $scope.selectedItem.label,
//                            'status': $scope.selectedItem.status,
//                            'level': $scope.permission.value,
//                            'mode': 'create'
//                        }).then(function (response) {
//                    $scope.names.push($scope.selectedItem);
//                    alert("notiifcation action  has been created");
//                });
//
//            }
//            if ($scope.action == "update") {
//                $http.post("../api/notification/update",
//                        {
//                            'type': $scope.selectedItem.type,
//                            'category': $scope.selectedItem.action,
//                            'label': $scope.selectedItem.label,
//                            'status': $scope.selectedItem.status,
//                            'level': $scope.permission.value,
//                            'mode': 'update',
//                            'id': $scope.selectedItem.index
//                        }).then(function (response) {
//                    alert("notiifcation action  has been updated");
//                });
//                //window.location.reload();
//            }
//            $scope.backlisting();
//        }
//        $scope.findaccount = function (name) {
//            var data = $scope.names;
//            for (var i = 0; i < data.length; i++) {
//                if (data[i].id === name) {
//                    return i;
//                }
//            }
//            return -1;
//        };
//        $scope.cleaninput = function (ll) {
//            if (typeof $scope.selectedItem[ll] === 'string')
//                $scope.selectedItem[ll] = $scope.selectedItem[ll].replace(/\'|\"/g, '’');
//        };
//        $scope.backlisting = function () {
//            $scope.reset('listactionflag');
//        };
//        $scope.reset = function (item) {
//            $scope.listactionflag = false;
//            $scope.vieweventflag = false
//            $scope.createeventflag = false;
//            $scope[item] = true;
//        };
//        $scope.create = function () {
//            $scope.reset('createeventflag');
//            $scope.buttonlabel = "Create Notiifcation Action";
//            $scope.action = "create";
//            return false;
//        }
//
//        $scope.update = function (oo) {
//
//            $scope.permission.roles = [];
//            $scope.permission.value = [];
//            var obj = {};
//            var accesslevel = oo.accesslevel;
//
//            if (typeof accesslevel !== 'undefiend') {
//                for (var i = 0; i < $scope.permission.finalValue.length; i++) {
//                    values = $scope.permission.finalValue[i];
//                    for (var key in values) {
//                        if (values.hasOwnProperty(key)) {
//                            if (key !== 'name') {
//                                if (key === 'SMSPREMIUM') {
//                                    key = 'SMS PREMIUM';
//                                }
//                                if (key === 'PUSHNOTIFICATION') {
//                                    key = 'PUSH NOTIFICATION';
//                                }
//                                if (accesslevel.indexOf(key) > -1) {
//                                    var propkey = '' + key + '';
//                                    obj[propkey] = '1';
//                                    $scope.permission.finalValue[i][key] = true;
//                                    var propkey = '"' + key + '"'
//                                }
//                            }
//                        }
//                    }
//                }
//
//                $scope.permission.roles = obj;
//                $scope.selectedItem = new $scope.Objevent().replicate(oo);
//                $scope.permission.findValue = oo;
//                $scope.reset('createeventflag');
//                $scope.action = "update";
//                $scope.buttonlabel = "Update Notiifcation Action";
//            }
//
//
//        }


    }]);