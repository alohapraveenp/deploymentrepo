<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    $memdata = new WY_Member();
    $emailArr = $memdata->getSuperweeloyMember();
   $res = new WY_restaurant;
   $res->getRestaurant($theRestaurant);
    $smsnotify = ($res->smsNotifToRestaurant() > 0) ? 1 : 0;

   ?>

<div class="container">
    <div class="row">
        <div class="col-md-12 left-sec">
            <div ng-controller="NotificationController"  ng-init="moduleName='notification';" >
                <div id="listing">
                     <input type='hidden' id='restaurant' value ="<?php echo $theRestaurant ?>" />
                        <input type='hidden' id='email' value ="<?php echo $_SESSION['user_backoffice']['email'] ?>" />
                         <input type='hidden' id='smsnotify' value ="<?php echo $smsnotify ?>" />
                         
                       <div class="form-group"  style='margin-bottom:25px;'>
                        <div class="col-md-4">
                            <div class="input-group col-md-4">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                                <input type="text" class="form-control input-sm" ng-model="searchText" ng-change="paginator.setPage(0);" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" style='width:200px;'> 
                            </div>
                        </div>
                        <div class="col-md-2" style='font-size:12px;'>
                            selected Items: <strong> {{names.length}} </strong>
                        </div>
                        <div class="col-md-2">
                            <div class="btn-group">
                                <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style='font-size:11px;'>Page Size <span class="caret"></span></button>
                                <ul class="dropdown-menu" role="menu">
                                    <li ng-repeat="x in paginator.pagerange()"><a href ng-click="paginator.setRowperPage(x)">{{x}}</a></li>
                                </ul>
                            </div> 		
                        </div>
                        
                    </div>
                    <div style=" clear: both;"></div>
               <table width='100%' class="table table-condensed table-striped" style='font-size:12px;'>
			<thead>
				<tr>
                                <th ng-repeat="y in tabletitle"><tbtitle var="{{y.a}}" name='tabletitle' module='moduleName'/></th>
				</tr>
			 </thead>
                        <tbody style='font-family:helvetica;font-size:12px;'>
                        
				<tr ng-repeat="x in filteredEvent = (names | filter:searchText) | sizefilter:this | orderBy:predicate:reverse | slicefilter:paginator.getPage():paginator.getRowperPage():paginator.getItemCount() | limitTo:paginator.getRowperPage()" style='font-family:helvetica;font-size:11px;'>
                                
                                    <td ng-repeat="y in tabletitle">
                                        <span ng-if="y.a==='notification_type'">
                                            
                                         {{ x[y.a] | adatereverse:y.c }}
                                        </span>
                                        <span  ng-if="x[y.a]===true || x[y.a] ===false "  >
                                         <label>
                                              <span  ng-if="y.a==='email' " >
                                                  
                                                    <input type="checkbox" ng-model="x[y.a]" ng-checked ='{{x[y.a]}}'  ng-click="updnotifyaction(x,'email')"  />
                                               </span>
                                              <span ng-if="(y.a==='sms') " >

                                                <input type="checkbox" ng-model="x[y.a]" ng-checked ='{{x[y.a]}}'  value="smsds" ng-click="updnotifyaction(x,'sms')"  />

                                              </span>
                                            
                                              <span ng-if="(y.a==='sms_premium') " >
<!--                                                 <span ng-if=" x['notifypremium'] ===true " > -->
                                                       
                                                    
                                                <input type="checkbox" ng-model="x[y.a]" ng-checked ='{{x[y.a]}}'    value="smspremium1"  ng-click="updnotifyaction(x,'smspremium')"  />
<!--                                                </span>-->
<!--                                                 <span ng-if=" x['notifypremium'] !==true " > 
                                              
                                                    <input type="checkbox" ng-model="x[y.a]" ng-checked ='{{x[y.a]}}'   onclick="return false"   />
                                                </span>-->
                                              </span>
                                              <span ng-if="(y.a==='push_notification') " >
<!--                                                <span ng-if=" x['notifypushnotification'] === true " > -->
                                                <input type="checkbox" ng-model="x[y.a]" ng-checked ='{{x[y.a]}}'  ng-click="updnotifyaction(x,'apans')"  />
<!--                                                </span>-->
                                               
                                                   
<!--                                                    <input ng-if=" x['notifypushnotification'] !==true " type="checkbox" ng-model="x[y.a]" ng-checked ='{{x[y.a]}}'  onclick="return false"   />-->
                                              
                                              </span>

<!--                                            <span  ng-if="y==='email'"> 
                                          
                                                <input type="checkbox"  ng-model="x[y]" ng-checked ='{{x[y]}}' ng-true-value="'1'" ng-false-value="'1'" onclick="return false" />
                                            </span>
                                            <span  ng-if="y!=='email'"> 
                                           <input type="checkbox" ng-model="x[y]" ng-checked ='{{x[y]}}' ng-true-value="'1'" ng-false-value="'0'" ng-click="updnotifyaction(x)"  />
                                            </span>-->
                                       </label>
                                            
                                        </span>
                                    </td>
                              
				</tr><tr><td colspan='{{tabletitle.length + 3}}'></td></tr>
                                
                              
                                 
			</tbody>
		</table>
                     <div ng-if="filteredEvent.length >= paginator.getRowperPage()" class="col-md-12" align="center" style='font-size:11px' ng-include="'app/components/paginator.html'"></div>
                </div>
            </div>

        </div>
    </div>
</div>
<script type="text/javascript" src="app/components/notification/configuration/notiificationConfigurationController.js"></script>
