<div class="container">
    <div class="row">
        <div class="col-md-12 left-sec">
<div ng-controller="TagController" ng-init="moduleName='tag'; listTagFlag = true; viewTagFlag=false; createTagFlag=false;" >
	<div id='listing' ng-show='listTagFlag'>
                <div class="form-group"  style='margin-bottom:25px;'>
			<div class="col-md-4">
				<div class="input-group col-md-4">
					
				</div>
			</div>
			<div class="col-md-2"></div>
			<div class="col-md-4">
			<a href ng-click='create();' class="btn btn-success btn-sm" style='color:white;width:200px;'><span class='glyphicon glyphicon-certificate'></span> &nbsp;Add New Tag</a>
			</div>
		</div>

            <div style=" clear: both;"></div>
            <div class="col-md-3">
                <ul id="tag-list" for="tags" class="list-group "  style='height: auto;max-height: 350px; overflow-x: hidden;'
                    <li class="list-group-item active">List of Tags</li>
                    <li  class="list-group-item" title="drop tag get list of restaurant"
                       ng-repeat="tag in tagsArr"
                        >
                      <span ng-click="getRestaurant(tag,'tags')" id="tag-list" for="tags"  >
                        {{ tag }}
                      </span>
                        <span class='pull-right'>
                        <i class="glyphicon glyphicon-remove-sign "  style="color: red;cursor:pointer;" ng-click="removetag(tag);"></i>
                        </span>
                    </li> 
                </ul>
            </div>
            <div class="col-md-1">
                     <div style='float:left;margin-top:100px; font-size: 25px;'>
                         <label style="font-size: 12px;">{{curTag}}</label>
                        <a class='glyphicon glyphicon-arrow-right' id='add_restau' ></a>
                        <a class='glyphicon glyphicon-arrow-left' id='del_restau' style='display:none'></a>
                    </div>
            </div>
              <div class="col-md-4">
                <ul id="assignedtag" class="list-group"  style="height:320px; overflow-x: hidden;" for="assigned"
                    ad-drop="true"
                        ad-drop-over="onDragOver($data, $dragElement, $dropElement, $event);"
                         ad-drop-leave="onDragLeave($data, $dragElement, $dropElement, $event)"
                         ad-drop-end="onDrop($data, $dragElement, $dropElement, $event,'assignedtag')" >
                    <li class="list-group-item ">List of  Restaurant</li>
                    <li class="list-group-item" 
                         ad-drop="true"
                        ad-drop-over="onDragOver($data, $dragElement, $dropElement, $event);"
                         ad-drop-leave="onDragLeave($data, $dragElement, $dropElement, $event)"
                         ad-drop-end="onDrop($data, $dragElement, $dropElement, $event,'assignedtag')"
                        ng-repeat="rest in assigned" >
                      <span >
                        {{ rest }}
                      </span>
                        <span class='pull-right'>
                        <i class="glyphicon glyphicon-remove-sign "  style="color: red;cursor:pointer;" ng-click="removerestTag(rest);"></i>
                        </span>
                    </li>
                   
                </ul>
               </div>
            <div class="col-md-4" ng-if='shownontagList'>
                <ul id="notassignedtag" for="notassigned" class="list-group "  style='height: auto;max-height: 350px; overflow-x: hidden;'
                    ad-drag="true"
                        ad-drag-data="rest"
                        ad-drag-begin="onDragStart($data, $dragElement, $event,'restList');"
                        ad-drag-end="onDragEnd($data, $dragElement, $lastDropElement, $event);">
                    <li class="list-group-item active">List of  not assigned -{{curTag}} tag restaurant</li>
                    <li  id="list-tagid" class="list-group-item" title="drag retaurant"
                        ad-drag="true"
                        ad-drag-data="rest"
                        ad-drag-begin="onDragStart($data, $dragElement, $event,'restList');"
                        ad-drag-end="onDragEnd($data, $dragElement, $lastDropElement, $event);"
                        ng-repeat="rest in notassigned"">
                      <span>
                        {{ rest }}
                      </span>
                    </li>
                </ul>
               </div>

	</div>
	<div class="col-md-12" ng-show='viewTagFlag'>
	</div>
        <div class="col-md-12" ng-show='restaurantFlag'>
            <br /><a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a><br />
		
	</div>

	<div class="col-md-12" ng-show='createTagFlag'>
		<br />
		<a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a>
		<br />
		 <div class="col-md-2"></div>
		 <div class="col-md-8">
		   <div class="row" ng-repeat="y in tabletitleContent | filter: {t: '!dontshow' }"  style="margin: 0 0 20px 0;font-size:12px;font-familly:Roboto">
                            <div class="input-group"  ng-if="y.t === 'input'">
                                    <span class="input-group-addon input11"><i class="glyphicon glyphicon-{{y.d}} input14"></i>&nbsp;{{y.b}}</span>
                                    <input type="text" class="form-control input-sm" ng-model="selectedItem[y.a]" ng-change="cleaninput(y.a)"  >
                            </div>
                            <div class="input-group" ng-if="y.t === 'dropdown'">
                                        <div class='input-group-btn' dropdown >
                                        <button type='button' class='btn btn-default dropdown-toggle btn-sm input11' data-toggle='dropdown'>
                                                <i class="glyphicon glyphicon-{{y.d}} input13"></i>&nbsp; {{y.b}}<span class='caret'></span></button>
                                        <ul class='dropdown-menu scrollable-menu'style='height: auto;max-height:120px; overflow-x: hidden;' >
                                        <li ng-repeat="p in y.val"><a href ng-click="selectedItem[y.a]=p;y.func()">{{ p }}</a></li>
                                        </ul>
                                        </div>
                                        <input type='text' ng-model='selectedItem[y.a]' class='form-control input-sm' readonly >
                                </div>
		   </div><br />
		</div>
		<div class="col-md-2"></div>
		<div class="col-md-7"></div>
		<div class="col-md-5">
			<a href ng-click='saveitem();' class="btn btn-success btn-sm" style="color:white;width:150px"><i class='glyphicon glyphicon-save'></i> &nbsp;{{ buttonlabel }} </a><br />
		</div>
        </div>
</div>

</div>
</div>
</div>

<script>

<?php
    $mediadata = new WY_Media();
    $resdata = new WY_restaurant();
     $service = new WY_Service();
       $tagList = $service->getTaglist();
    $imgAr = $mediadata->getEventPictureNames($theRestaurant);
        $resAr= $resdata->getSimpleListRestaurant();
 

    printf("var imgEvent = [");
    for($i = 0, $sep = ""; $i < count($imgAr); $i++, $sep = ", "){
    	printf("%s '%s'", $sep, $imgAr[$i]);
    }
    printf("];");

    printf("var pathimg = '%s';", $mediadata->getFullPath('small'));


      printf("var resArr = [");
            for($i = 0, $sep = ""; $i < count($resAr); $i++, $sep = ", "){
                printf("%s '%s'", $sep, $resAr[$i]);
            }
    printf("];");
    
      
        printf("var tagsArr = [");
            for($i = 0, $sep = ""; $i < count($tagList); $i++, $sep = ","){
                  printf("%s '%s'", $sep, $tagList[$i]);
           
            }
        printf("];");
     
            
    
?>

var token = <?php echo "'".$_SESSION['user_backoffice']['token']."';"; ?>
var email =  <?php echo "'".$_SESSION['user_backoffice']['email']."';";?> 

app.controller('TagController', ['$scope','$http','$timeout', function($scope,$http,$timeout) {
	var todaydate = new Date();
	var aorder = (function() { var i, arr=[]; for(i = 0; i < 30; i++) arr.push(i); return arr; })();
	$scope.paginator = new Pagination(100);
	$scope.path = pathimg;
	$scope.restaurant = <?php echo "'".$theRestaurant."';"; ?>
	$scope.email = <?php echo "'" . $email . "';"; ?>
	$scope.predicate = '';
         $scope.tagsArr = tagsArr;
	$scope.reverse = false;
	$scope.nonefunc = function() {};
        $scope.resArr = resArr;

       var status=['active','inactive'];

	$scope.tabletitle = [ {a:'index', b:'ID', c:'' , q:'down', cc: 'black' }, {a:'tag', b:'Tag', c:'' , q:'down', cc: 'black' },{ a:'link', b:'Link', c:'' , q:'down', cc: 'black' }, , {a:'status', b:'Status', c:'' , q:'down', cc: 'black' }, {a:'is_mobile', b:'Mobile', c:'' , q:'down', cc: 'black' },{a:'morder', b:'Order', c:'' , q:'down', cc: 'black' },{a:'type', b:'Type', c:'' , q:'down', cc: 'black' },{a:'restCount', b:'RestCount', c:'' , q:'down', cc: 'black' } ];
	$scope.bckups = $scope.tabletitle.slice(0);
	$scope.tabletitleContent = [ { a:'tag', b:'Tag', c:'', d:'tower', t:'input', i:0 },{ a:'status', b:'Status', c:'', d:'sort', t:'dropdown', val:status,func: $scope.nonefunc}  ];
	$scope.imgEvent = imgEvent.slice(0);
        $scope.shownontagList=false;
        
          
        var mailArr = email.split('@');
        $scope.is_email =mailArr[1];
        //$scope.is_email='gmail.com';
        $scope.path ="https://media.weeloy.com/upload/restaurant/";
	$scope.Objevent = function() {
		return {
			restaurant: $scope.restaurant, 
			index: 0, 
			title: '', 
                        type:'',
			tag: '', 
                        link: '', 
                        tag:'',
                        restCount:'',
                        status:'',
                        is_mobile:'',
			description: '', 
			restaurant1: '', 
			image1: '', 
			restaurant2: '', 
			image2: '',
                        restaurant3: '',
                        image3:'',
                        morder:'',
                        city:'',
                        is_tag:'',
                        tags:'',

			remove: function() { 
				for(var i = 0; i < $scope.names.length; i++) 
					if($scope.names[i].name === this.name) {
						$scope.names.splice(i, 1);
						break;
						}
				},
				
			replicate: function(obj) {
				for (var attr in this)
		        	if(this.hasOwnProperty(attr) && typeof this[attr] !== 'function' && attr !== '$$hashKey') 
        				this[attr] = obj[attr];
				return this;
				},
					
			clean: function() {
				for (var attr in this)
		        	if(this.hasOwnProperty(attr) && typeof this[attr] !== 'function' && attr !== '$$hashKey') {
        				if(typeof this[attr] === 'string') {
        					this[attr] = this[attr].replace(/\'|\"/g, '’');
        					}
        				}
				return this;
				}

                };
        };




	$scope.cleaninput = function(ll) {
		if(typeof $scope.selectedItem[ll] === 'string')
			$scope.selectedItem[ll] =  $scope.selectedItem[ll].replace(/\'|\"/g, '’');
		};
		
	$scope.reset = function(item) {
		$scope.listTagFlag = false;
		$scope.viewTagFlag = false
		$scope.createTagFlag = false;
                $scope.restEventFlag = false;
		$scope[item] = true;	
		};

	$scope.backlisting = function() {
		$scope.reset('listTagFlag');
		};
		
	$scope.findtag = function(name) {
            var data =$scope.tagsArr;
		for(var i = 0; i < data.length; i++){
                    if(data[i].trim()===name.trim()){
                           return i;
                    }
                }
                return -1;
        };
        $scope.restaurants = function(tag){
             var url ="../api/restaurant/gettagrestaurant/"+tag;
            $http.get(url).then(function(response) {
                $scope.selectedItem=[];
                $scope.assigned=[]; 
                $scope.notassigned=[]; 
		data = response.data;
                if(response.data.data.assigned){ $scope.assigned = response.data.data.assigned;};
                if(response.data.data.notassigned){ $scope.shownontagList = true;$scope.notassigned = response.data.data.notassigned;};
                 
               
             //$scope.imagesArr= response.data.data.pictures;
          });
         // /restaurant/gettagrestaurant/  
            
        }
         $scope.currentDropElement = null;

        $scope.remove = function(l, o) {
           
            var index = l.indexOf(o);
            if (index > -1) {
              l.splice(index, 1);
            }
        };
        
    
        $scope.deletetag =function(o){
            var l=$scope.notassigned,t=$scope.assigned;
             var index = l.indexOf(o),iDx =t.indexOf(o);
             if (index > -1) {
              l.splice(index, 1);
            }
            if (iDx > -1) {
              t.splice(iDx, 1);
            }
        }

        $scope.onDragStart = function(data, dragElement, dropElement,label) {
                $scope.currentDropElement =label;   
        };

        $scope.onDragEnd = function(data, dragElement, dropElement,dragEvent) {
           
    
        };

        $scope.onDragOver = function(data, dragElement, dropElement) {

          
        };

        $scope.onDragLeave = function() {

          $scope.currentDropElement = null;
        };

        $scope.onDrop = function(data,dragElement,dropElement,event,label) {
 
            var lbl =$("#"+label).attr('for');

        if($scope.currentDropElement==='tagList'){$scope.curTag =data;}
            if (data) {

                  if(lbl==='assigned'){
                     if($scope.currentDropElement==='restList') {
                          $scope.assigned.push(data);
                          $scope.savetaginrest($scope.curTag,data);
                            $scope.remove($scope.notassigned, data);
                      }else{
                         $scope.restaurants(data);  
                      }
                  }
                  else{
                      if( $scope.assigned ===null){
                        $scope.assigned.push(data);
                     }else{$scope.assigned.push(data);}
                       $scope.remove($scope.notassigned, data);
                  }
                      return; 

            }
        };
        $scope.getRestaurant =function(data,label){
            $scope.restaurants(data);  
            $scope.curTag =data;
        }
        
        $scope.removetag =function(t){
           
          if(confirm("Are you sure you want to delete tag ") == false){return;}
          else{
   
                $http.post("../api/removetags",
			{ 'tag':t,'restaurant':'','type':'all'}).then(function() {
                            var l =$scope.tagsArr;
                       $scope.remove(l, t);
			alert( "tags has been deleted");
                    });
           
            
            }
            
        }
        $scope.removerestTag =function(t){
        if(confirm("Are you sure you want to delete tag in restaurant ") == false){return;}
          else{
            $http.post("../api/removetags",
            { 'restaurant':t,
              'tag':$scope.curTag,
              'type':'tag'}).then(function() {
                        var l =$scope.assigned;
                       $scope.remove(l, t);
			alert( "tags has been deleted");
                    });
           }
            
        }

	$scope.view = function(oo) {
            $scope.removedtagrest=[];
		$scope.selectedItem = oo;
		$scope.reset('viewTagFlag');
                $scope.currentTag =oo['tag'];
//                 $scope.restaurants(oo['tag']);
            };
	$scope.create = function() {
		$scope.selectedItem = new $scope.Objevent();
                $scope.selectedItem.is_tag = true;
		$scope.reset('createTagFlag');
		$scope.buttonlabel = "Save new Tag";
		$scope.action = "create";
		return false;
		}
                
				
	$scope.update = function(oo) {
                if(oo['tag']==="" ||  oo['tag']==='undefiend' ){
                     oo['is_tag']= false;
                 }else{oo['is_tag']=true;}
                  $scope.selectedItem = new $scope.Objevent().replicate(oo);
                    $scope.reset('createTagFlag');
                    $scope.buttonlabel = "Update categories";
                    $scope.action = "update";
		};
                
        $scope.savetaginrest =function(tag,restaurant){
             $http.post("../api/updaterestauranttags",
			{ 'tag':tag,'restaurant':restaurant}).then(function(response) {console.log(JSON.stringify(response));});
                        $scope.backlisting();	
            
        }
                
      
	$scope.saveitem = function() {
		var u, msg, apiurl, ind;
                $scope.selectedItem.type ="footer";
                $scope.selectedItem.description ="";
                if( $scope.selectedItem.tag ==="" || $scope.selectedItem.status==="" ){
                    alert("Please Complete All Required fields!");
                      return;
                }
		$scope.selectedItem.clean();
                


                    if($scope.action == "create") {
                        ind = $scope.findtag($scope.selectedItem.tag);
                        if(ind >= 0) {
				alert("name " + $scope.selectedItem.tag + " already exists. Please choose another name !");
				return;
                            }
			$scope.tagsArr.push($scope.selectedItem.tag);
                    }
		
                    if($scope.action === "create"){
                          $http.post("../api/tag/create",
			{
                           'tag'  : $scope.selectedItem.tag,
                           'status':$scope.selectedItem.status
			}).then(function(response) { alert("Tag has been created");});
                    }
			//bookService.createEvent($scope.restaurant, $scope.email, $scope.selectedItem ).then(function(response) { alert("Event has been created"); });
		else{
         
                     $http.post("../api/home/updatefootercategories",
			{ 'categories':$scope.selectedItem}).then(function(response) {alert("Categories has been created");});
                    //bookService.updateEvent($scope.restaurant, $scope.email, $scope.selectedItem ).then(function(response) { alert("Event has been updated"); });
                  }
 		$scope.backlisting();		
		};
                
                
            //upload tag image
            $scope.uploadFiles = function()	{
		var object_type = "footer";
		dom = $('#file_upload')[0];
		files = dom.files;  //get the file
		
		if(files.length == 0) {
			alert("Select/Drop one file");
			return false;
			}

		filename = files[0].name;
		ext = filename.substring(filename.lastIndexOf(".")+1).toLowerCase();
		if(ext != "jpg" && ext != "jpeg" && ext != "png" && ext != "gif") {
			alert("Invalid file type (jpg, jpeg, png, gif)");
			return false;
			}
		if(object_type == "") {
			alert("Please Choose a category");
			return false;
			}
			
		$scope.selectedItem.name = files[0].name;
		$scope.selectedItem.object_type = object_type;
		$scope.selectedItem.media_type = 'picture';
		$scope.selectedItem.path = '';
		$scope.selectedItem.status = 'active';
		$scope.selectedItem.description = '';

		var data = new FormData();
		$.each(files, function(key, value)
		{
			data.append(key, value);
		});

		data.append('token', token);
		data.append('restaurant', $scope.selectedItem.tag);
		data.append('category', 'category');
                console.log($scope.selectedItem.object_type);
                $.ajax({
                    url: 'saveimages.php?files',
                    type: 'POST',
                    data: data,
                    cache: false,
                    dataType: 'json',
                    processData: false, // Don't process the files
                    contentType: false, // Set content type to false as jQuery will tell the server its a query string request
                    success: function(data, textStatus, jqXHR)
                        {
                        if(typeof data.error === 'undefined'){
                            $scope.selectedItem.restaurant1 =$scope.selectedItem.tag
                            $scope.selectedItem.image1 =$scope.selectedItem.name; }
                                //$scope.submitForm(dom, data);
                        else {
                                alert(data.error);
                                $scope.selectedItem.remove();
                                $scope.$apply();
                                }
                     },
                    error: function(jqXHR, textStatus, errorThrown) {
                                alert(textStatus);		
                    }
                });
            };

   
}]);

	
</script>
