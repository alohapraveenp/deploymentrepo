<?php
  $res = new WY_restaurant;
  $res->getRestaurant($theRestaurant); 


?>
<div class="container">
    <div class="row">
        <div class="col-md-12 left-sec">`
<div ng-controller="PaymentController" ng-init="moduleName='payemnt'; listPaymentFlag = true;  createPaymentFlag=false;" >


    <div id='listing' ng-show='listPaymentFlag'>
        <div class="form-group"  style='margin-bottom:25px;'>
            <div class="col-md-4">
                <div class="input-group col-md-4">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                    <input type="text" class="form-control input-sm" ng-model="searchText" ng-change="paginator.setPage(0);" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" style='width:200px;'> 
                </div>
            </div>
            <div class="col-md-2"></div>
            <div class="col-md-4">
            <a href ng-click='create();' class="btn btn-success btn-sm" style='color:white;width:200px;'><span class='glyphicon glyphicon-certificate'></span> &nbsp;Create a New Details</a>
            </div>
        </div>
    
            <div style=" clear: both;"></div>
                <table width='100%' class="table table-condensed table-striped" style='font-size:12px;'>
                    <thead>
                        <tr>
                        <th ng-repeat="y in tabletitle"><tbtitle var="{{y.a}}" name='tabletitle' module='moduleName'/></th>
                        <th>update</th><th> &nbsp; </th>
                        <th>delete</th><th> &nbsp; </th>
                        </tr>
                    </thead>
            
                
                    <tbody style='font-family:helvetica;font-size:12px;'>
                            <tr  style='font-family:helvetica;font-size:11px;'> 

                                    <td ng-repeat="y in tabletitle"> {{ names[y.a]  }}</a></td>
                                    <td><a href ng-click="update(names)" style='color:blue;'><span class='glyphicon glyphicon-pencil'></span></a></td>
                              <td width='30'>&nbsp;</td>
                              <td><a href ng-click="delete(x)" style='color:red;'><span class='glyphicon glyphicon-trash'></span></a></td>
                              </tr>
                            <tr><td colspan='{{tabletitle.length + 5}}'></td></tr>

                    </tbody>

                </table>
<!--        <div ng-if="filteredEvent.length >= paginator.getRowperPage()" class="col-md-12" align="center" style='font-size:11px' ng-include="'inc/paginator.html'"></div>-->
    </div>

    

    <div class="col-md-12" ng-show='createPaymentFlag'>
        <br />
        <a href class='btn btn-info btn-sm customColor' ng-click="backlisting()"><span class='glyphicon glyphicon-step-backward'></span> &nbsp;Back to listing</a>
        <br />
         <div class="col-md-2"></div>
         <div class="col-md-8">
           <div class="row" ng-repeat="y in tabletitleContent | filter: {t: '!dontshow' }" style="margin: 0 0 20px 0;font-size:12px;font-familly:Roboto">

            <div class="input-group" ng-if="y.t === 'input'">
                <span class="input-group-addon input11"><i class="fa fa-{{y.d}}"></i>&nbsp; {{y.b}}</span>
                <input type="text" class="form-control input-sm" ng-model="selectedItem[y.a]" ng-change="cleaninput(y.a)"  >
            </div> 
                    

           </div><br />
        </div>
        <div class="col-md-2"></div>
        <div class="col-md-7"></div>
        <div class="col-md-5">
            <a href ng-click='savePayment();' class="btn btn-success btn-sm" style="color:white;width:150px"><i class='glyphicon glyphicon-save'></i> &nbsp;{{ buttonlabel }} </a><br />
        </div>
        </div>
</div>

</div>
</div>
</div>

<script>

var token = <?php echo "'".$_SESSION['user_backoffice']['token']."';"; ?>
var email =  <?php echo "'".$_SESSION['user_backoffice']['email']."';";?>


app.controller('PaymentController', ['$scope', 'adminServiceApi', '$http', function($scope, adminServiceApi, $http) {
       
    var todaydate = new Date();
     
  $scope.paginator = new Pagination(25);
  $scope.paginatorsub = new Pagination(25); 
    $scope.evbooking = [];
    $scope.names = [];
    
    $scope.restaurant = <?php echo "'".$theRestaurant."';"; ?>
    console.log("RESTUARNAT" +$scope.restaurant);
    $scope.email = <?php echo "'" . $email . "';"; ?>
   
    $scope.predicate = '';
    $scope.reverse = false;
    $scope.bkproduct ='';
    
    $scope.nonefunc = function() {};
    
    $scope.tabletitle = [ {a:'paypal_id', b:'Paypal', c:'' , l:'', q:'down', cc: 'fuchsia' }, 
                          {a:'stripe_public_key', b:'Stripe Public Key', c:'' , l:'25', q:'down', cc: 'black' }, 
                          {a:'stripe_secret_key', b:'Stripe Secret Key', c:'' , l:'', q:'down', cc: 'black' },
                          {a:'reddot_mid', b:'Reddot MID', c:'' , l:'25', q:'down', cc: 'black' },
                          {a:'reddot_key', b:'Reddot Key', c:'' , l:'25', q:'down', cc: 'black' },
                          {a:'reddot_secret_key', b:'Reddot Secret Key', c:'' , l:'25', q:'down', cc: 'black' },
                        ];    
    $scope.tabletitleContent = [{ a:'paypal_id', b:'Paypal Id', c:'', d:'flag', t:'input', i:0 }, 
                                         { a:'stripe_public_key', b:'Stripe Public Key', c:'', d:'money', t:'input', i:0 },
                                         { a:'stripe_secret_key', b:'Stripe Secret Key', c:'', d:'tag', t:'input' },
                                         { a:'reddot_mid', b:'Reddot MID', c:'', d:'tag', t:'input' }, 
                                         { a:'reddot_key', b:'Reddot Key', c:'', d:'tag', t:'input' }, 
                                         { a:'reddot_secret_key', b:'Reddot Secret Key', c:'', d:'tag', t:'input' }, 
                                         ];

 $scope.Objpolicy = function() {
     return {
         restaurant: $scope.restaurant, 
         id :'',
         index: 0, 
         paypal_id: '',
         stripe_public_key: '', 
         stripe_secret_key: '', 
         reddot_mid :'',
         reddot_key:'',
         reddot_secret_key:'',
         
          remove: function() { 
             for(var i = 0; i < $scope.names.length; i++) 
                 if($scope.names[i].name === this.name) {
                     $scope.names.splice(i, 1);
                     break;
                     }
             },
             
         replicate: function(obj) {
             for (var attr in this)
                 if(this.hasOwnProperty(attr) && typeof this[attr] !== 'function' && attr !== '$$hashKey') 
                     this[attr] = obj[attr];
             return this;
             }
         };
 };
 
 adminServiceApi.getPaymentDetails($scope.restaurant)
       .then(function (response) {

    if(response.data) {
           var data = response.data;
          $scope.names = new $scope.Objpolicy().replicate(data);
           
    } 
    console.log("NAMES" +$scope.names);
                    
 });
        
        
    $scope.initorder = function() {
        $scope.predicate = "vorder";
        $scope.reverse = true;
    };

    $scope.reorder = function(item, alter) {
        alter = alter || "";
        if (alter !== "")  item = alter;
        $scope.reverse = ($scope.predicate === item) ? !$scope.reverse : false;
        $scope.predicate = item;
    };
    $scope.create = function() {

            $scope.selectedItem = new $scope.Objpolicy();
//            $scope.mydata_start.setDate(todaydate);
//            $scope.mydata_endng.setDate(todaydate);
            $scope.reset('createPaymentFlag');
            $scope.buttonlabel = "Save new payment";
            $scope.action = "create";
            return false;
    };
    $scope.update = function(oo) {
        $scope.selectedItem = new $scope.Objpolicy().replicate(oo);
        
        $scope.reset('createPaymentFlag');
        $scope.buttonlabel = "Update  payment";
        $scope.action = "update";
    };
    $scope.findaccount = function(id) {
        for(var i = 0; i < $scope.names.length; i++)
                if($scope.names[i].id === id)
                        return i;
        return -1;
    };
    $scope.savePayment = function () {
        var oo = $scope.selectedItem;
        oo.restaurant_id = $scope.restaurant;
        if($scope.action === "create"){
                
            adminServiceApi.savePaymentDetails($scope.restaurant,$scope.selectedItem)
                .then(function (response) {
                    console.log(response);
                    if (response.status > 0) {
                        $scope.names = new $scope.Objpolicy().replicate(oo);
                       alert("SuccessFully Saved Your Changes.");
                    }
            });
        }else{
            adminServiceApi.updatePaymentDetails($scope.restaurant,oo).then(function(response) {
                console.log(response.status);
                if(response.status > 0) {
                    $scope.names = oo;
                    alert("Cancel policy has been updated");
                }
                });
            }
        $scope.backlisting();
    };
    $scope.delete = function(oo){

        adminServiceApi.deletePaymentDetails($scope.restaurant)
                .then(function (response) {
                    if (response.status > 0) {
                        $scope.names = '';
                        alert("Cancel policy has been Deleted");
                    }
       });
     
    };
        
        
     

    $scope.cleaninput = function(ll) {
        if(typeof $scope.selectedItem[ll] === 'string' && $scope.selectedItem[ll] !== '')
            $scope.selectedItem[ll] =  $scope.selectedItem[ll].replace(/\'|\"/g, '’');
        };
        
    $scope.reset = function(item) {
        $scope.listPaymentFlag = false;
        $scope.viewEventFlag = false
        $scope.createPaymentFlag = false;
        $scope.viewlistBkEventFlag = false;
        $scope.viewBkEventFlag = false;
        $scope[item] = true;    
        };

    $scope.backlisting = function(page) {
        if(page === undefined)
            $scope.reset('listPaymentFlag');
        else $scope.reset(page);
        };
 
}]);

    
</script>



