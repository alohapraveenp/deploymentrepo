<?php

$accountemail = $_SESSION['user']['email'];


?>
<div class="container" ng-controller="CampaignController" ng-init="moduleName = 'campaign';">
    <div class="row">
        <div class="col-md-12 left-sec">

            <div id="getitem" class="item-label" >
                
                <div id='accordion' class='panel-group'>
            <div class='panel panel-default'>
                <div class='panel-heading'>
                  <h4 class='panel-title'>
                    <a data-toggle='collapse' href='#extraction'> GET DETAILS </a>
                  </h4>
                </div>
                <div id='extraction' class='panel-collapse collapse in '>
                <div class='panel-body' >
                    <div class='form-group row'>
                    <div class='col-sm-6'>
                    <div class='input-group xls'>
                        <div class='input-group-btn'>
                            <button data-toggle='dropdown' class='btn btn-info btn-sm dropdown-toggle' type='button'>Extract Data
                            <span class='caret'></span>
                            </button>
                            <ul role='menu' class='dropdown-menu dropdown_itemlanguage scrollable-menu' style='height: auto;max-height: 350px; overflow-x: hidden;'>
                                <li ng-repeat="x in labelAr"><a id='data-extract' href class='dummy_itemextract' ng-click="listIndex(x.label,x.query,x.permission)">{{x.label}}</a></li>
                            </ul>
                        </div>
                    <input type='text' readonly='' for='' id='itemextract' name='itemextract' ng-model="itemlabel" class='form-control input-sm itemlanguage' > 
                   </div>
                   
                    
            </div> 
                 <div class="col-md-5">
<!--                     <button id='up-btn' type='button' class='btn btn-info get-btn form-control' ng-click="getList();" value ='add'  style='margin-right:15px;' >Get</button>-->
			<a id='up-btn' href ng-click="getList();" value ='add' class="btn btn-info btn-sm" style="color:white;width:150px"><i class='glyphicon glyphicon-save'></i> &nbsp;GET </a><br />
		</div>
            </div>
            </div>
            </div>
            </div>
            </div>
            </div>
            

            <div class="extract-details" ng-show="showdetails" >
                <h2 style="text-align:center;">Members List</h2> 

				<div class="col-md-4">
					<div class="input-group">
						<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
						<input type="text" class="form-control input-sm" ng-model="searchText" ng-change="paginator.setPage(0);" style='width:200px;' autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" > 
					</div>
				</div>

				<div class="col-md-3">
					<span class='infobk' style='margin-left:20px;'>selected data: <strong> {{cpList.length}} </strong></span>&nbsp&nbsp
<!--					<span class='infobk'>current selection: <strong> {{currentSelection}} </strong></span>-->
				   
				</div>
                                <div class="col-md-3">
				

					<form action="echo.php" id="extractForm" name="extractForm1" method="POST" target="_blank">
						<input type="hidden" value="testing" ng-model='content' name="content" id="content">
						<a href ng-click='extractSelection();' class="btn btn-success btn-sm" style='color:white;'><span class='glyphicon glyphicon-save'></span> &nbsp;Extract Selection</a>
					</form>
				</div>
				<div class="col-md-2">
					<div class="btn-group">
						<button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style='font-size:11px;'>Page Size <span class="caret"></span></button>
						<ul class="dropdown-menu" role="menu">
							<li ng-repeat="x in paginator.pagerange()"><a href ng-click="paginator.setRowperPage(x)">{{x}}</a></li>
						</ul>
					</div> 		
				</div>
				<div class="row">
				<br /><br /><br />
                                <form action="echo.php" id="extractForm" name="extractForm" method="POST" target="_blank">
				<table width='100%' class="table table-condensed table-striped" style='font-family:helvetica;font-size:11px;'>
					<thead><tr><th ng-repeat="y in tabletitle"><tbtitle var="{{y.a}}" name='tabletitle' module='moduleName'/></th></tr></thead>						
					<tr ng-repeat ="x in filteredMember = (cpList | filter:searchText) | sizefilter:this | orderBy:predicate:reverse | slicefilter:paginator.getPage():paginator.getRowperPage():paginator.getItemCount() | limitTo:paginator.getRowperPage() " style='font-family:helvetica;font-size:11px;'>
					<td ng-repeat ="z in tabletitle" nowrap>
						<span ><tb-listingbkg>{{ x[z.a]  | adatereverse:z.c}}</tb-listingbkg></span>
					</td>
					</tr>
					<tr><td colspan='{{ tabletitle.length }}'></td></tr>
				</table>
                                </form>
				</div>
				<div ng-if="filteredMember.length >= paginator.getRowperPage()" class="col-md-12" align="center" style='font-size:11px' ng-include="'app/components/paginator.html'"></div>
            </div>
        </div>
    </div>
</div>

<script>
    
    $(function () {
 
        
    });
    
    app.controller('CampaignController', ['$scope', '$http', function ($scope, $http) {
	$scope.paginator = new Pagination(100);
        $scope.showdetails = false;
	$scope.predicate = 'datetime';
	$scope.reverse = false;	
	$scope.cpList = []; 
        $scope.email ='<?php echo $accountemail?>';
        //$scope.labelAr = [{a:'Member Details',b:'member_details'},{a:'Member Email',b:'member_email'}];
        $scope.label=[];
        //$scope.itemlabel ="Member Details";
	//$scope.tabletitle = [{a:'firstname', b:'FirstName', c:'', q:'down', cc:'black', l:'' }, {a:'name', b:'LastName', c:'', q:'down', cc:'black', l:'' }, {a:'email', b:'Email', c:'', q:'down', cc:'black', l:'' }, { a:'mobile', b:'Mobile', c:'', q:'up', cc:'black', l:'' },{ a:'gender', b:'Gender', c:'', q:'down', cc:'black', l:'' },{ a:'country', b:'Country', c:'', q:'up', cc:'black', l:'' },{ a:'city', b:'City', c:'', q:'up', cc:'black', l:'' },{ a:'member_permission', b:'MemberPermission', c:'', q:'up', cc:'black', l:'' }];
            $scope.getLabelList = function(){
               $http.get('../api/data/getlabel?email='+$scope.email).success(function (response) {
                    var data = response.data;
                    $scope.labelAr =data;
                if(response.data.length>0){
                     $scope.itemlabel=$scope.labelAr[0].label; 
                    $scope.currentSelection =$scope.labelAr[0].label;
                    $scope.permission =$scope.labelAr[0].permission;
                }
 
                });
            };

	$scope.initorder = function() {
		$scope.predicate = "datetime";
		$scope.reverse = true;
	};
        $scope.getLabelList();

	$scope.reorder = function(item, alter) {
		alter = alter || "";
		if (alter !== "")  item = alter;
		$scope.reverse = ($scope.predicate == item) ? !$scope.reverse : false;
		$scope.predicate = item;
	};
         $scope.listIndex = function (tt,tl,tp) {
            $scope.itemlabel=tt; 
            $scope.currentSelection = tt;
            $scope.permission=tp;
         }
             var obj = {};
             var arr=[];
            $scope.getList = function () {
                  $http.get('../api/data/extract?query_name='+$scope.currentSelection+"&label="+$scope.itemlabel+"&permission="+$scope.permission+"&email="+$scope.email).success(function (response) {
                    var data = response.data;
                    if(response.errors!=null){
                     alert(response.errors);   
                     return;
                    }
                   obj={};
                    var key_array = []; 
                    angular.forEach(data, function(values, key) {
                         if(key===0){
                             arr=[];
                            angular.forEach(values, function(value, key) {
                                    obj ={
                                        'a':key,
                                        'b':key,
                                        'c':'',
                                        'q':'down',
                                        'cc':'black',
                                         'l':''
                                    }
                                arr.push(obj);
                            });
                              $scope.tabletitle=[];
                             
                             $scope.tabletitle = arr;
                           
                        }

                        
                    });
                 
                $scope.cpList = data;
               //$scope.currentSelection = "member";
                $scope.showdetails= true;
		$scope.paginator.setItemCount($scope.cpList.length);
		$scope.initorder();

                  });
            };
            $scope.createList = function () {
             
             $http({
			url: '../api/createlabel',
			method: "POST",
			data: $.param({
				label: $scope.labelname,
				query: $scope.query,
                                status:$scope.status
			}),
			headers:  {'Content-Type': 'application/x-www-form-urlencoded'}
		}).success(function(data, status, headers, config) {
                     
		  }).
		  error(function(data, status, headers, config) {s
			// called asynchronously if an error occurs
			// or server returns response with an error status.
		  }); 
          };
        
            $scope.extractSelection = function() {

		var maxlimit = 500; // ajax call will not support more data
		var tt, sep;
		var todayflg = ($scope.itemlabel == 'Member Details');
		var exportselect = $scope.filteredMember;
                var idx;
                var earr=[];
		var limit = exportselect.length;
		tt = sep = '';
		if (limit > maxlimit) limit = maxlimit;
                    var obj = {
                                "member": []
                        };
                    angular.forEach(exportselect, function(values, key) {
                        earr=[];
                            angular.forEach(values, function(value, key) {
                                idx=  '"'+ key +'":"'+ value +'"';
                                if(key!=='$$hashKey'){
                                    earr.push(idx);
                                }
                            });
               
                    tt += sep + '{ ' + earr +'}';
                    sep=',';
     
                });
        
		tt = '{ "member":[' + tt + '] }';
		$('#content').val(tt);
		$('#extractForm').submit();
		};



}]);
    
</script>