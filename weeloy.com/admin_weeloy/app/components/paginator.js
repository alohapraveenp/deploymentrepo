/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function Pagination(cn) {
	var rowperpage = cn;
	var itemcount;
	var page=0;
	var pgrange = [ 20, 40,80,100, 200,500 ];
	var pgoffset = 0;

	return {

		range: function() {
			var ret = [];
			var limit = this.pageCount();
                     
			for (i = 0; i < limit; i++) {
				ret.push(i);
				}
			return ret;
			},

		pagerange: function() {
			return pgrange;
			},
			
		setPageOffset: function(pgo) {
			pgoffset = pgo;
			},

		getPageOffset: function() {
			return pgoffset;
			},

		setItemCount: function(cn) {
                  
			itemcount = cn;
			},
			
		getItemCount: function() {
			return itemcount;
			},
			
		setRowperPage: function(cn) {
			return rowperpage = cn;
			},
			
		getRowperPage: function() {
			return rowperpage;
			},
			
		getPage: function() {
           
			return page;
			},
					
		setPage: function (apage) { 
			if (apage > this.pageCount()) { return; }
      
			page = apage;
                        console.log("Setpage"+page);
             
			},	
			
		nextPage: function () { 
			if (page < this.pageCount() - 1) 
				page++; 
			},
		
		prevPage: function () { 
			if (page > 0)
				page--; 
			},
			
		firstPage: function () { 
			page = 0;
			},
			
		lastPage: function () { 
			page = this.pageCount() - 1;
			},
			
		isFirstPage: function () { 
			return page == 0 ? "disabled" : "";
			},
			
		isLastPage: function () {
			return page == this.pageCount() - 1 ? "disabled" : "";
			},
			
		pageCount: function () {
       
			return Math.ceil(parseInt(itemcount) / parseInt(rowperpage));
			},
			
		prevPageDisabled: function() {
			return page === 0 ? "disabled" : "";
			},
		
		nextPageDisabled: function() {
			return page === this.pageCount() - 1 ? "disabled" : "";
			}
		};
	}

app.filter('sizefilter', function() {
	return function(input, scope) {
	if (input == undefined)return;
             scope.paginator.setItemCount(input.length);
           
        
	return input;
	}
});
app.filter('emsizefilter', function() {
	return function(input, scope) {
	if (input == undefined)return;
 
console.log("pageindirective"+input.page);
                scope.paginator.setItemCount(input.totalCount);
               


	return input;
	}
});
	
app.service('Paginator', function () { 
	this.page=0;
	this.rowsPerPage = 500; 
	this.itemCount = 0;

        this.range =function() {
			var ret = [];
			var limit = this.pageCount();
			for (i = 0; i < limit; i++) {
				ret.push(i);
				}
			return ret;
			};
        
	this.setPage = function (page) {
     
        if (page > this.pageCount()) { return; }
		this.page = page;
             
		};		
	this.nextPage = function () { 
		if (this.isLastPage()) { return; }
		this.page++; 
		};
	this.perviousPage = function () { 
		if (this.isFirstPage()) { return; }
		this.page--; 
		};
	this.firstPage = function () { 
		this.page = 0;
		};
	this.lastPage = function () { 
		this.page = this.pageCount() - 1;
		};
                
        this.getPage =function() {
			return this.page ;
        },
        this.setRowperPage =function(cn) {
			return this.rowsPerPage = cn;
			};
			
        this.getRowperPage= function() {
			return this.rowsPerPage;
			},
        this.setItemCount = function(cn) {
                this.itemCount = cn;
	};
        this.getItemCount =function() {
			return  this.itemCount;
			};
	this.isFirstPage = function () { 
		return this.page == 0;
		};
	this.isLastPage = function () {
		return this.page == this.pageCount() - 1;
		};
	this.pageCount = function () {
            console.log("Ss"+this.itemCount);
		return Math.ceil(parseInt(this.itemCount) / parseInt(this.rowsPerPage));
		}; 
	});

app.filter('slicepaginator', function(Paginator) {
	return function(input, rowsPerPage) { 
 
		if (!input) { return input; }
	if (rowsPerPage) { Paginator.rowsPerPage = rowsPerPage; }
	Paginator.itemCount = input.length;
	return input.slice(parseInt(Paginator.page * Paginator.rowsPerPage), parseInt((Paginator.page + 1) * Paginator.rowsPerPage + 1) - 1); }
});

app.directive('paginator', function factory() { 
	return {
		restrict: 'E',
		controller: function ($scope, Paginator) {
                   
		$scope.paginator = Paginator; },
		templateUrl: 'paginationControl.html' 
		};
});

app.filter('forLoop', function() {
	return function(input, start, end) {
          
	for (input = []; start < end; start++) { input.push(start); }
	return input; 
	}
});


