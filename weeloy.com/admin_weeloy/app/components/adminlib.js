 
 function toggleCarat(item, module, reverse) {
	var id = '#' + item + "_" + module;
	var titlecolor = (reverse) ? 'orange' : 'fuchsia';
	var content = $(id).text();
	$('th a span').css('color', 'black');
	$(id).css('color', titlecolor);
	if (reverse) $(id).html(content + "<i class='fa fa-caret-up'>");
	else $(id).html(content + "<i class='fa fa-caret-down'>");
}
app.filter('slicefilter', function() {
	return function(arr, currentPage, itemsPerPage) {
	if (arr == undefined) return;
	return arr.slice(currentPage * itemsPerPage, (currentPage + 1) * itemsPerPage);
	}
});


// offset. it starts at the current week
app.filter('slicefilterOffset', function() {
	return function(arr, currentPage, itemsPerPage) {
	if (arr == undefined) return;
	actualpage = currentPage - paginationOffset;
	return arr.slice(actualpage * itemsPerPage, (actualpage + 1) * itemsPerPage);
	}
});


app.filter('inversedate', function() {
	return function(input) {
	return input.substr(8,2) + "-" + input.substr(5,2) + "-" + input.substr(0,4);
	}
});
 
 
 app.directive('tbtitle', function(){
            return {
                    restrict: 'AE',
                    replace: true,
                    template: '<a href="javascript:;" id="{{y.a}}" ng-click="reorder(y.a);"><span id="{{y.a}}_{{moduleName}}" class="classbgwhite">{{ y.b }}<i class="fa fa-caret-down"></span></a>',
                    link: function(scope, elem, attrs) { }
		};
        });
        
 app.service('adminService', ['$http', function($http){
         	var self = this;
         
         this.ModalDataBooking = function() {

		var i, j, timeslotAr=[], persAr=[], hourslotAr=[], minuteslotAr=[];

		for(i = 1; i < 50; i++) 
			persAr.push(i);
		
		for(i = 9; i < 24; i++)
			for(j = 0; j < 60; j += 15) {
				timeslotAr.push((i < 10 ? '0' : '') + i + ':' + (j === 0 ? '0' : '') + j);
				}
			
		for(i = 9; i < 24; i++) 
			hourslotAr.push((i < 10 ? '0' : '') + i);
		
		for(i = 0; i < 60; i += 5) 
			minuteslotAr.push((i < 10 ? '0' : '') + i);
			
		return  {
				name: "", 
				pers: persAr, 
				timeslot: timeslotAr, 
				hourslot: hourslotAr,
				minuteslot: minuteslotAr,
				ntimeslot: 0,
				event: ['Birthday', 'Wedding', 'Reunion'],
				start_opened: false,
				opened: true,
				selecteddate: null,
				originaldate: null,
				dateFormated: "",
				theDate: null,
				minDate: null,
				maxDate: null,
				dateOptions: { formatYear: 'yyyy', startingDay: 1 },
				formats: ['dd-MM-yyyy', 'dd/MMMM/yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'dd-MM-yyyy', 'yyyy-MM-dd', 'dd.MM.yyyy', 'shortDate', 'yyyy-MM-dd', ],
				formatsel: "",
				func: null,
			
				setInit: function(ddate, rtime, func, format, minDate, maxDate) {
					
					this.theDate = null;
					this.func = func;
					this.formatsel = this.formats[format];			
					this.ntimeslot = rtime.substring(0, 5);
				
					if(ddate instanceof Date === false) {
						ddate = ddate.jsdate();
						}
					this.originaldate = ddate;
					this.selecteddate = ddate.getDateFormat('-');		
					this.minDate = minDate;
					this.maxDate = maxDate;
					
					if(navigator.userAgent.indexOf("Chrome") != -1 ) { 
						this.formatsel = this.formats[8]; 
						this.selecteddate = ddate.getDateFormatReverse('-'); 
						}	
					},
			
				setDate: function(ddate) {
					this.theDate = null;
					this.originaldate = ddate; // this is for the case that no data selected
					this.selecteddate = ddate.getDateFormat('-');
					if(navigator.userAgent.indexOf("Chrome") != -1) 
						this.selecteddate = ddate.getDateFormatReverse('-'); 
					},
					
				getDate: function(sep, mode) {
					if(typeof this.theDate !== null && typeof this.theDate !== undefined && this.theDate instanceof Date)
						return (mode !== 'reverse') ? this.theDate.getDateFormat(sep) : this.theDate.getDateFormatReverse(sep);
					return (mode !== 'reverse') ? this.originaldate.getDateFormat(sep) : this.originaldate.getDateFormatReverse(sep);
					},
					
				dateopen: function($event) {
					this.start_opened = true;
					this.opened = true;
					$event.preventDefault();
					$event.stopPropagation();
					},
				
				disabled: function(date, mode) {
					return false;
					},
			
				calendar: function() {
					this.theDate = this.selecteddate;
					this.dateFormated = this.theDate.getDateFormat('/');
					},
			
				onchange: function() {
					this.theDate = this.selecteddate;
					this.dateFormated = this.theDate.getDateFormat('/');

					if(this.func !== null)
						this.func();
					}	
				};
		};
         
 }]);

