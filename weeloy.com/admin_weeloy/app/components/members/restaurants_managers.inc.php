<style>
    .list_restaurant{
        margin-left: 20px;
        margin-right: 20px;
        min-width: 240px;
        width: 240px;
        display: inline;
        border: 1px solid #D8D8D8;
        padding: 2px;
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        -border-radius: 3px;
        border-radius: 3px;
        margin-top:10px;
    }
    
    .left{
        float:left;
    }
    .div_restaurant_list{
        width:35%; 
        float:left;
        margin-left: 20px;
}
</style>

<?php 

        require_once("lib/class.restaurant.inc.php");
        require_once("lib/class.member.inc.php");
        
        global $permlabelAr, $permvalueAr;
        
        $accountemail = $_SESSION['user']['email'];
        $theMember = $_POST['theMember'];
        $action_type = $_POST['action_type'];
        
        
	$perm_limited_reservation = (!empty($_POST['perm_limited_reservation'])) ? "checked" : "";
        
        $restrictedCountry = "";
        
        $memdata = new WY_Member();
        $memdata->getMember($accountemail);
        if($memdata->is_restrictedCountry($accountemail))
        	$restrictedCountry = $memdata->country;

       $allowCreateDelete = $memdata->is_allowCreateResto($accountemail);
  
        $memdata->getListMemberRestaurant();
        
        $emailAr = $memdata->email;
        $nameAr = $memdata->name;
        $firstnameAr = $memdata->firstname;
        $countryAr = $memdata->country;
        
        echo "<h2><a href='$action'> $title_btn Members</a></h2>
		  <form enctype='multipart/form-data' name='idFormsQuery' id='idFormsQuery' method='POST' action='$action'>
		  <input type='hidden' value='$navbar' id='navbar' name='navbar'>
		  <select onchange='idFormsQuery.submit();' id='theMember' name='theMember' class='selectpicker' data-style='btn-primary' data-width='50%' data-live-search='true'>";

		$limit = count($emailAr);
		for($i = 0; $i < $limit ; $i++) {
			if(empty($restrictedCountry) || ($restrictedCountry == $countryAr[$i] && $emailAr[$i] != $accountemail))
				printf("<option value='%s' %s>%s</option>", $emailAr[$i],  ($theMember == $emailAr[$i]) ? "selected" : "", $nameAr[$i] . ' ' . $firstnameAr[$i] . '&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  (' . $emailAr[$i] . ')');
			}
		
		echo "  </select><br />	<br />	
			<button type='submit' class='btn btn-info btn-sm search'><span class='glyphicon glyphicon-search'></span> &nbsp;search</button>
			</form><br />";
        
        if(isset($theMember) && (empty($_POST['initstate']) || $_POST['initstate'] != "83")) {
        	$perm = $memdata->read_permission($theMember);
  		foreach($permlabelAr as $key => $label) {
      			$_POST[$key] = $perm & $permvalueAr[$key];
      			}
      		}
        else {
        	error_log('IT DOES NOT MAKE SENSE');
        	$permflg = 0;
		foreach($permlabelAr as $key => $label) {
			if(!empty($_POST[$key]))
				$permflg += intval($_POST[$key]);
			}
        	$memdata->write_permission($theMember, $permflg);
        	}
        //processing datas
        if($_POST['navbar'] == 'ASSIGN_MEMBER_RESTAURANTS_LIST'  && !empty($theMember) && !empty($action_type)) {
        
            $member = new WY_Member();
            switch ($action_type) {
                case 'add_restau':
                    $member->addMembersRestaurants($theMember, $_POST['add_list']);
                break;
            
                case 'del_restau':
                    $member->deleteMembersRestaurants($theMember, $_POST['del_list']);
                break;
            	}
       		}
        
        // display datas
        if($_POST['navbar'] == 'ASSIGN_MEMBER_RESTAURANTS_LIST' && !empty($theMember)){
 
                $resdata_not_assigned = new WY_restaurant();
                $resdata_not_assigned->getListNotAssignedRestaurant($theMember);
                $restaurantNotAssignedAr = $resdata_not_assigned->restaurant;
                $titleNotAssignedAr = $resdata_not_assigned->title;

                $resdata_assigned = new WY_restaurant();
                $resdata_assigned->getListAssignedRestaurant($theMember);
                $restaurantAssignedAr = $resdata_assigned->restaurant;
                $titleAssignedAr = $resdata_assigned->title;
            
            echo "<form enctype='multipart/form-data' name='idFormsQuery3' id='idFormsQuery3' method='POST' action='$action' style='height: 260px;'>
                    
                    <input type='hidden' value='$navbar' id='navbar' name='navbar'>
                    <input type='hidden' value='' id='action_type' name='action_type'>
                    <input type='hidden' value='$theMember' id='theMember' name='theMember'>
                   <input type='hidden' value='83' id='initstate' name='initstate'>

                    <div class='div_restaurant_list'>
                   <span style='width:100%; '>Restaurants</span>
                   <select multiple id='add_list' name='add_list[]' style='height: 260px;' class='list_restaurant left'>";
                    $limit = count($restaurantNotAssignedAr);
                    for($i = 0; $i < $limit ; $i++)
                    printf("<option value='%s' %s>%s</option>", $restaurantNotAssignedAr[$i], "", ($titleNotAssignedAr[$i] != "") ? $titleNotAssignedAr[$i] : $restaurantNotAssignedAr[$i]);
                    echo "  </select>	
                    </div>
                    <div style='float:left;margin-top:100px; font-size: 25px;'>
                        <a class='glyphicon glyphicon-arrow-right' id='add_restau' ></a>
                        <a class='glyphicon glyphicon-arrow-left' id='del_restau' style='display:none'></a>
                    </div>
                    <div class='div_restaurant_list'>
                    <span style='width:100%; '>Assigned restaurants</span>
                    <select multiple id='del_list' name='del_list[]' style='height: 260px;' class='list_restaurant left' >";
                    $limit = count($restaurantAssignedAr);
                    for($i = 0; $i < $limit ; $i++)
                    printf("<option value='%s' %s>%s</option>", $restaurantAssignedAr[$i], "", ($titleAssignedAr[$i] != "") ? $titleAssignedAr[$i] : $restaurantAssignedAr[$i]);
                    echo "  </select>	
					</div>";
				
				$kk = 1;	
				foreach($permlabelAr as $key => $label) {
					if($_SESSION['user_backoffice']['member_type'] != 'admin' && $key == "perm_RestoCRD")
						if($allowCreateDelete == false || $accountemail == $theMember) 
							continue; // cannot set this priviledge for others unless you are admin
					$checked = (!empty($_POST[$key])) ? "checked" : "";	
					$kk  = $permvalueAr[$key];						
                    echo "<div class='form-inline input-sm'>
						<div class='input-group'>
							<label class='checkbox-inline'><input type='checkbox' value='$kk' id='$key' name='$key' $checked onchange='idFormsQuery3.submit();'>$label</label>
						</div>
					</div>";
					}
					
            echo "</form><br />";
        }
?>
<script type="text/javascript" src="../js/jquery.min.js"></script>
<script type="text/javascript">
 $(function(){

   
    $("body").on('change','#add_list',function(){
        $("#del_list").attr('selectedIndex', '-1').find("option:selected").removeAttr("selected");
        $("#add_restau").show();
        $("#del_restau").hide();
    });

    $("body").on('change','#del_list',function(){
        $("#add_list").attr('selectedIndex', '-1').find("option:selected").removeAttr("selected");
        $("#del_restau").show();
        $("#add_restau").hide();
    });  
    
    $("body").on('click','#add_restau',function(){
        $("#action_type").val('add_restau');
        $('#idFormsQuery3').submit();
    });

    $("body").on('click','#del_restau',function(){
        $("#action_type").val('del_restau');
        $('#idFormsQuery3').submit();        
    });
    
}); 


</script>
