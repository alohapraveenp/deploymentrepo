
<div class="container">
<div class="row">
<div class="col-xl-12 left-sec" ng-controller="printmemberController" ng-init="moduleName='printmember'; " >

        <h2><a href="{{ action }}"> Select Members </a></h2>
	<form action="{{ action }}" name='idFormsQuery' id='idFormsQuery' method='POST' enctype='multipart/form-data' >
		<input type='hidden' value="{{ navbar }}" id='navbar' name='navbar'>
		<label>Any: <input ng-model="searchText" ng-change="triggerSearch();" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" ></label> <br> <br>
		<table><tr>
		<td>Select the starting letter of email</td>
		<td><span style="left-margin: 20px">&nbsp &nbsp &nbsp &nbsp</span></td>
		<td>
		<select ng-model="aLetter"  ng-change="setLetter(aLetter);">
		<option ng-repeat="v in letters" value="{{ v }}">{{ v }}</option>
		</select>
		</td></tr>
		<td>Select the member </td>
		<td><span style="left-margin: 20px">&nbsp</span></td>
		<td>
		<select ng-model="theMember" id='theMember' name='theMember'  ng-change="triggerSubmit(theMember);"> <!-- class='selectpicker' data-style='btn-primary' data-width='50%' data-live-search='true' -->
		<option ng-repeat="x in members_view" value="{{ x.email }}" ng-disabled="x.type=='-'">{{ x.value }}</option>
		</select>
		</td></tr></table>
		<br />
                <button type='submit' class='btn btn-info btn-sm '><span class='glyphicon glyphicon-search'></span> &nbsp; Select</button>
		</form><br />
</div>
</div>
</div>

<script>

var token = <?php echo "'".$_SESSION['user_backoffice']['token']."';"; ?>
var email =  <?php echo "'".$_SESSION['user_backoffice']['email']."';";?>

app.controller('printmemberController', ['$scope', '$http', '$timeout', '$filter', 'adminServiceApi', function($scope, $http, $timeout, $filter, adminServiceApi) {
	$scope.members = [];               
	$scope.letters = [];             
	$scope.members_view = [];             
	
	$scope.action = <?php echo "'". $_SERVER['PHP_SELF'] ."'";?>;
	$scope.navbar = <?php echo "'". $_REQUEST['navbar'] ."'";?>;
	$scope.cmember = <?php echo "'". $_REQUEST['theMember'] ."'";?>;
	$scope.aMember = "";
	$scope.searchText = "";

	$('#idFormsQuery').attr('action', $scope.action);

	$scope.triggerSubmit = function() {
		$('#idFormsQuery').submit();
		};
	
	$scope.triggerSearch = function() {
		if(typeof $scope.searchText !== "string" || $scope.searchText.length < 3)
			return;
		
		$scope.members_view = $filter('filter')($scope.members, $scope.searchText);
		};
				
	adminServiceApi.readMember().then(function(response) {              
		var ctype, limit = 10000, data = response.data;
		$scope.members = [];             
		$scope.members_view = [];             
		$scope.letters = [];             

		if(response.status !== 1)
			return;
			
		data.map(function(oo, index, arr) { if(typeof oo.email === "string" && oo.email.length > 0) oo.email = oo.email.toLowerCase(); });
		data.sort(function(a, b) { 
			if(a.type !== b.type) return  a.type.localeCompare(b.type);
			return  a.email.localeCompare(b.email);
			});
		
		ctype = 'starting';	
		data.map(function(oo, index, arr) {
		        if (oo.type == "admin" || oo.type == "super_weeloy" || limit < 0 || typeof oo.email !== "string" || oo.email.length < 1)
		        	return;
		        limit--;
		        if(ctype !== oo.type) {
		        	ctype = oo.type;
		        	$scope.members.push({ type : '-', email: '', value: '' }); 
		        	$scope.members.push({ type : '-', email: '', value: ctype}); 
		        	$scope.members.push({ type : '-', email: '', value: '------------------' }); 
		        	}
		        
		        if($scope.letters.indexOf(oo.email.substr(0, 1)) < 0) $scope.letters.push(oo.email.substr(0, 1));
		        oo.value = oo.firstname+ ' ' +  oo.lastname+ ' ' +  '( '  + oo.email+ '  )';
			$scope.members.push(oo);                         
			});   

		$scope.letters.sort(function(a, b) { return a.charCodeAt(0) - b.charCodeAt(0); });
		if(typeof $scope.cmember  === "string" && $scope.cmember.length > 0) {
			$scope.setLetter($scope.cmember.charAt(0));
			$scope.theMember = $scope.cmember;
			console.log('MEMBER', $scope.theMember);
			}
        });

	$scope.setLetter = function(a) {
		var i, data = $scope.members, limit = data.length;
		$scope.members_view = []; 
		for(i = 0; i < limit; i++) {
			if(data[i].email.charAt(0) === a)
				break;
			}
		for(j = i; j < limit; j++) {
			if(data[j].email.charAt(0) !== a)
				break;
			}
		$scope.members_view = data.slice(i, j);
		$scope.aLetter = a;
		};
		
	$scope.schange = function() {
		idFormsQuery.submit(); 
		} 
	$timeout(function() {
		$scope.myColor = 4; // Yellow
	});

	
	function validateEmail(email) {
		if(typeof email !== "string" || email.length < 4)
			return false;
			
    		var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    		return re.test(email);
		}			
}]);	
</script>

  