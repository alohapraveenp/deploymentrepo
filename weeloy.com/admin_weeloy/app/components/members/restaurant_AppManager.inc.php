<style>
    .list_restaurant{
        margin-left: 20px;
        margin-right: 20px;
        min-width: 260px;
        width: 260px;
        display: inline;
        border: 1px solid #D8D8D8;
        padding: 2px;
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        -border-radius: 3px;
        border-radius: 3px;
        margin-top:10px;
    }
    
    .left{
        float:left;
    }
    .div_restaurant_list{
        width:40%; 
        float:left;
        margin-left: 20px;
}
</style>

<?php 
        require_once("lib/class.restaurant.inc.php");
        require_once("lib/class.member.inc.php");
        
        $accountemail = $_SESSION['user']['email'];
        $theMember = $_POST['theMember'];
        $action_type = $_POST['action_type'];
        
        $restrictedCountry = "";
        
        $memdata = new WY_Member();
        $memdata->getMember($accountemail);
        if($memdata->is_restrictedCountry($accountemail))
        	$restrictedCountry = $memdata->country;

		$allowCreateDelete = $memdata->is_allowCreateResto($accountemail);

        $memdata->getListMemberRestaurant();
        
        $emailAr = $memdata->email;
        $nameAr = $memdata->name;
        $countryAr = $memdata->country;
        $firstnameAr = $memdata->firstname;
        
        echo "<h2><a href='$action'> $title_btn Members</a></h2>
		  <form enctype='multipart/form-data' name='idFormsQuery' id='idFormsQuery' method='POST' action='$action'>
		  <input type='hidden' value='$navbar' id='navbar' name='navbar'>
		  <select onchange='idFormsQuery.submit();' id='theRestaurant' name='theMember'  class='selectpicker' data-style='btn-primary' data-width='50%' data-live-search='true'>";

		$limit = count($emailAr);
		for($i = 0; $i < $limit ; $i++) {
			if(empty($restrictedCountry) || ($restrictedCountry == $countryAr[$i] && $emailAr[$i] != $accountemail))
				printf("<option value='%s' %s>%s</option>", $emailAr[$i],  ($theMember == $emailAr[$i]) ? "selected" : "", $nameAr[$i] . ' ' . $firstnameAr[$i] . '&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  (' . $emailAr[$i] . ')');
			}
		
		echo "  </select><br />	<br />	
					<button type='submit' class='btn btn-info btn-sm search'><span class='glyphicon glyphicon-search'></span> &nbsp;search</button>
			</form><br />";
        
        //processing datas
        if($_POST['navbar'] == 'ASSIGN_MEMBER_APPMANAGER_RESTAURANTS_LIST'  && isset($theMember)  && !empty($theMember 
                && isset($action_type))){
        
            $member = new WY_Member();
            switch ($action_type){
                case 'add_restau':
                    // $member->addAppManagerRestaurants($theMember, $_POST['add_list']);
                    $member->addMembersRestaurants($theMember, $_POST['add_list']);
                break;
            
                case 'del_restau':
                    // $member->deleteAppManagerRestaurants($theMember, $_POST['del_list']);
                    $member->deleteMembersRestaurants($theMember, $_POST['del_list']);
                break;
            }
        }
        
        // display datas
        if($_POST['navbar'] == 'ASSIGN_MEMBER_APPMANAGER_RESTAURANTS_LIST' 
                && isset($theMember) 
                && !empty($theMember)){

 
                $resdata_not_assigned = new WY_restaurant();
                $resdata_not_assigned->getListNotAssignedAppManagerRestaurant($theMember);
                $restaurantNotAssignedAr = $resdata_not_assigned->restaurant;
                $titleNotAssignedAr = $resdata_not_assigned->title;

                $resdata_assigned = new WY_restaurant();
                $resdata_assigned->getListAssignedAppManagerRestaurant($theMember);
                $restaurantAssignedAr = $resdata_assigned->restaurant;
                $titleAssignedAr = $resdata_assigned->title;
            
            echo "<form enctype='multipart/form-data' name='idFormsQuery2' id='idFormsQuery2' method='POST' action='$action' style='height: 260px;'>
                    
                    <input type='hidden' value='$navbar' id='navbar' name='navbar'>
                    <input type='hidden' value='' id='action_type' name='action_type'>
                    <input type='hidden' value='$theMember' id='theMember' name='theMember'>

                    <div class='div_restaurant_list'>
                   <span style='width:100%; '>Restaurants</span>
                   <select multiple id='add_list' name='add_list[]' style='height: 260px;' class='list_restaurant left'>";
                    $limit = count($restaurantNotAssignedAr);
                    for($i = 0; $i < $limit ; $i++)
                    printf("<option value='%s' %s>%s</option>", $restaurantNotAssignedAr[$i], "", ($titleNotAssignedAr[$i] != "") ? $titleNotAssignedAr[$i] : $restaurantNotAssignedAr[$i]);
                    echo "  </select>	
                    </div>
                    <div style='float:left;margin-top:100px; font-size: 25px;'>
                        <a class='glyphicon glyphicon-arrow-right' id='add_restau' ></a>
                        <a class='glyphicon glyphicon-arrow-left' id='del_restau' style='display:none'></a>
                    </div>
                    <div class='div_restaurant_list'>
                    <span style='width:100%; '>Assigned restaurants</span>
                    <select multiple id='del_list' name='del_list[]' style='height: 260px;' class='list_restaurant left' >";
                    $limit = count($restaurantAssignedAr);
                    for($i = 0; $i < $limit ; $i++)
                    printf("<option value='%s' %s>%s</option>", $restaurantAssignedAr[$i], "", ($titleAssignedAr[$i] != "") ? $titleAssignedAr[$i] : $restaurantAssignedAr[$i]);
                    echo "  </select>	
                    </div>
        </form><br />";
        }
?>
<script type="text/javascript" src="../js/jquery.min.js"></script>
<script type="text/javascript">
 $(function(){

   
    $("body").on('change','#add_list',function(){
        $("#del_list").attr('selectedIndex', '-1').find("option:selected").removeAttr("selected");
        $("#add_restau").show();
        $("#del_restau").hide();
    });

    $("body").on('change','#del_list',function(){
        $("#add_list").attr('selectedIndex', '-1').find("option:selected").removeAttr("selected");
        $("#del_restau").show();
        $("#add_restau").hide();
    });  
    
    $("body").on('click','#add_restau',function(){
        $("#action_type").val('add_restau');
        $('#idFormsQuery2').submit();
    });

    $("body").on('click','#del_restau',function(){
        $("#action_type").val('del_restau');
        $('#idFormsQuery2').submit();
    });
});  
</script>
