<style>
      .highlightedText { background: yellow }
</style>
<div class="container">
    <div class="row">
        <div class="col-xl-12 left-sec">
<div ng-controller="gitConfigController">
<!--<div ng-init="moduleName='Git'; listGitFlag = true; viewGitFlag=false; createGitFlag=false;" >-->
	<div id='listing'>
		<div class="form-group"  style='margin-bottom:25px;'>
			<div class="col-md-4">
<!-- 				<div class="input-group col-md-4">
					<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
					<input type="text" class="form-control input-sm" ng-model="searchText" ng-change="paginator.setPage(0);" style='width:200px;' autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" > 
				</div> -->
				Search: <input type="text" ng-model="searchText">
			</div>
			<div class="col-md-2"></div>
			<div class="col-md-4">
			<a href ng-click='diff();' class="btn btn-success btn-sm" style='color:white;width:200px;'><span class='glyphicon glyphicon-certificate'></span> &nbsp;Dev-Master Diff</a>
			</div>
		</div>
	</div>
	<div style=" clear: both;"></div>
	<div id='result'>
    	<!-- <span ng-model="diffResult | filter:searchText| highlight:searchText">{{diffResult}}</span> -->
    	<span ng-bind-html="highlight(diffResult, searchText)"></span>
    </div>
</div>
</div>
</div>
</div>
<script type="text/javascript" src="app/components/git/gitConfigController.js"></script>