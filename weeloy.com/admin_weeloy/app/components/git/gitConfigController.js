app.controller('gitConfigController',  ['$scope', 'API', '$sce', function ($scope, API, $sce) {
  $scope.highlight = function(text, search) {
    if (!search)
        return $sce.trustAsHtml(text);
    return $sce.trustAsHtml(text.replace(new RegExp(search, 'gi'), '<span class="highlightedText">$&</span>'));
  };
  //window.location.reload(true);
  $scope.diff = function() {
    //alert("$scope.diff()");
    API.gitconfig.DevMasterDiff().then(function (response) {
      //console.log("gitConfigController.DevMasterDiff()");
      //alert("gitConfigController.DevMasterDiff() response: " + strResponse);
      if (response.status == 1) {
        //alert("gitConfigController.DevMasterDiff() successful! response: " + strResponse);
        console.log("SuccessFully Create a git diff: " + JSON.stringify(response));
        $scope.diffResult = JSON.stringify(response.data);
        return true;
      } else {
        //alert("gitConfigController.DevMasterDiff() error! response: " + strResponse);
        console.log("gitConfigController.devmasterdiff() failed: " + JSON.stringify(response));
        $scope.diffResult = JSON.stringify(response.errors);
      }
	 	});
	 	return false;
	 }
}]);