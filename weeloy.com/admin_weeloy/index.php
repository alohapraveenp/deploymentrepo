<?php
require_once("config.inc.php");
require_once("lib/class.login.inc.php");
require_once("lib/class.coding.inc.php");
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="author" content="weeloy. https://www.weeloy.com">
<meta name="copyright" content="weeloy. https://www.weeloy.com">  
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
<title>Weeloy - administration system</title>
<link rel="icon" href="../favicon.ico" type="image/gif" sizes="16x16">
<link href="../client/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="../client/bower_components/bootstrap/dist/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
<link href="../client/bower_components/jquery-ui/themes/base/jquery-ui.css" rel="stylesheet" type="text/css">
<link href="../client/bower_components/fontawesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="../css/admin-style.css?1" rel="stylesheet" type="text/css" />
<link href="../css/bootstrap-slider.css" rel="stylesheet">
<link href="../css/dropdown.css" rel="stylesheet">
<link href="../css/bootstrap-select.css" rel="stylesheet" >
<link href="../css/login.css" rel="stylesheet">
<link href="../css/bootstrap-social.css" rel="stylesheet" type="text/css"/>
<link href="../css/adapt-strap.min.css" rel="stylesheet" type="text/css"/>
<link href='https://fonts.googleapis.com/css?family=Dosis:400,600,700bold|Roboto:400,900,700italic,700,500italic,500,400italic' rel='stylesheet' type='text/css'>
<script type='text/javascript' src="../client/bower_components/jquery/dist/jquery.min.js"></script>
<script type='text/javascript' src="../client/bower_components/jquery-ui/jquery-ui.js"></script>
<script type='text/javascript' src="../client/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script type='text/javascript' src="../client/bower_components/angular/angular.min.js"></script>
<script type='text/javascript' src="../client/bower_components/angular/angular-animate.min.js"></script>
<script type='text/javascript' src="../client/bower_components/material/angular-material.min.js"></script>
<script type='text/javascript' src="../client/bower_components/angular/angular-aria.min.js"></script>
<script type='text/javascript' src="../client/bower_components/angular-sanitize/angular-sanitize.min.js"></script>
<script type='text/javascript' src="../client/bower_components/angular/angular-messages.min.js"></script>
<script type="text/javascript" src="../client/bower_components/moment/min/moment-with-locales.min.js"></script>
<script type="text/javascript" src="../client/bower_components/material/bootstrap-material-datetimepicker.js"></script>
<script type='text/javascript' src="../client/bower_components/ui-bootstrap-tpls-0.14.2.min.js"></script>
<script type='text/javascript' src="../js/bootstrap-slider.js"></script>
<script type='text/javascript' src="../js/datepicker.js"></script>
<script type='text/javascript' src="../js/alog.js"></script>
<script type='text/javascript' src="../js/base64code.js"></script>
<script type="text/javascript" src="../js/bootstrap-select.js"></script>
<script type='text/javascript' src="../js/adapt-strap.min.js"></script>
<script type='text/javascript' src="../js/adapt-strap.tpl.min.js"></script>

<script type="text/javascript" src="app/api/restaurant/VerifyEmailService.js"></script>
<script type="text/javascript" src="app/api/restaurant/CancelPolicyService.js"></script>
<script type="text/javascript" src="app/api/notification/NotificationConfigurationService.js"></script>
<script type="text/javascript" src="app/api/restaurant/SectionConfigService.js"></script>
<script type="text/javascript" src="app/api/git/GitConfigService.js"></script>
<script type='text/javascript' src="app/api/api.js"></script>
<script type="text/javascript" src="../client/bower_components/ng-file-upload/ng-file-upload-shim.min.js"></script>
<script type="text/javascript" src="../client/bower_components/ng-file-upload/ng-file-upload-all.min.js"></script>

<style>
body { overflow: scroll; }

.truncate { white-space: nowrap; overflow: hidden; text-overflow: ellipsis; }

.scrollable-menu { 
	height: auto; 
	max-height: 300px; 
	overflow-x: hidden; 
	}

.scrollable-menu::-webkit-scrollbar {
  -webkit-appearance: none;
  width: 10px; }

.scrollable-menu::-webkit-scrollbar-thumb {
  border-radius: 2px;
  background-color: black;
  -webkit-box-shadow: 0 0 1px rgba(255, 255, 255, 0.75); }


</style>

<!--        <script type='text/javascript' src="../js/facebook.js"></script> -->

<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<script src="js/selectivizr-min.js"></script>
<script src="js/modernizr.js"></script>
<link rel="stylesheet" type="text/css" href="css/ie.css" media="screen" />
<![endif]-->
<script>

var app = angular.module("admin_weeloy", ['ui.bootstrap','adaptv.adaptStrap','ngSanitize', 'app.api', 'ngFileUpload']);
</script>

<script type="text/javascript" src="../backoffice/inc/backofficelib.js?15"></script>
<script type='text/javascript' src="app/components/paginator.js"></script>
<script type='text/javascript' src="app/components/adminapi.js?16"></script>

</head>

    <body ng-app="admin_weeloy">

        <div class="ProfileBox">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 ProfileName">
                        <a href="javascript:location.reload(true);"><img src="../images/logo_puce.svg" width='120px'><hr />  </a>        
                        <nav class="navbar navbar-default white-bg" role="navigation" style='border: 0px; padding: 0px;'>
                            <div class="container-fluid">
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                </div>
<?php print_left_navbar($navbarAr); ?>
                            </div>
                        </nav>
                        <hr />
                        <h5></h5>
                        <p id="sessiontime" class = 'small'></p><hr/>
                    </div>
                </div>
            </div>
        </div>


        <div class="move">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="white-bg" style="width:1100px;">

                <?php print_body(); ?>    

                        </div>
                    </div>
                </div>
            </div>
        </div>

<script> 
<?php printf("var cookiename = '%s';", getCookiename('admin')); ?>
</script>


        <?php print_javascript(); ?>
        <?php print_div_login_modal(); ?>   
    
    </body>
    
</html>