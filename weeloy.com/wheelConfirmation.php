<?php

require_once("conf/conf.init.inc.php");
require_once("lib/wpdo.inc.php");
require_once("lib/wglobals.inc.php");
require_once("lib/class.images.inc.php");
require_once("lib/class.restaurant.inc.php");
require_once("lib/class.member.inc.php");
require_once("lib/class.booking.inc.php");
require_once("lib/class.mail.inc.php");
require_once("lib/class.sms.inc.php");
require_once "lib/class.pushnotif.inc.php";
require_once("lib/class.spool.inc.php");
require_once("lib/class.async.inc.php");
require_once("lib/Browser.inc.php");


$action = $_REQUEST['action'];
if($action === "submit"){
	echo submit();
}
else if($action === "verify"){
	echo verifyCode();
}

function verifyCode(){
	
	$verify = "fail";
	$codeConfirmation = (isset($_REQUEST['code-confirmation'])) ? $_REQUEST['code-confirmation'] : "";
	$restCode = (isset($_REQUEST['code-restaurant'])) ? $_REQUEST['code-restaurant'] : "";
	$membCode = (isset($_REQUEST['code-guest'])) ? $_REQUEST['code-guest'] : "";
	
	$bkg = new WY_Booking();
	return $bkg->verifyBookingWin($codeConfirmation, $restCode, $membCode);

}

function submit(){

	$verify = "fail";
	$codeConfirmation = (isset($_REQUEST['code-confirmation'])) ? $_REQUEST['code-confirmation'] : "";
	$restCode = (isset($_REQUEST['code-restaurant'])) ? $_REQUEST['code-restaurant'] : "";
	$membCode = (isset($_REQUEST['code-guest'])) ? $_REQUEST['code-guest'] : "";
	$spinsegment = (isset($_REQUEST['spinsegment'])) ? $_REQUEST['spinsegment'] : "";
        $spinwin = (isset($_REQUEST['spinwin'])) ? $_REQUEST['spinwin'] : "";
        $spindesc = (isset($_REQUEST['spindesc'])) ? $_REQUEST['spindesc'] : "";
        $spinsource = (isset($_REQUEST['spinsource'])) ? $_REQUEST['spinsource'] : "";

	$bkg = new WY_Booking();
	return $bkg->submitBookingCompleteWin($codeConfirmation, $restCode, $membCode, $spinsegment, $spinwin, $spindesc, $spinsource);
}

