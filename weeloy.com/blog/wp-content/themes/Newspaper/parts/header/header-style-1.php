<!--
Header style 1
-->
<div class="td-header-wrap td-header-style-1">

    <div class="td-header-top-menu-full">
        <div class="td-container td-header-row td-header-top-menu">
            <div class="td-header-top-logo">
                <a href="https://www.weeloy.com" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/logo_w_t_small1.png" /></a>
			</div>
					<div class="input-group input-group-lg search-bar">
                <input class="form-control restitle_free_text" id="restitle" name="restitle" placeholder="Restaurant name or Cuisine or Food" aria-label="..." type="text">
                        <div id="dd_city" class="input-group-addon search-drop">                   
                          <button type="button" data-toggle="dropdown" class="dropdown-toggles" style="height:23px;">
                        <span id="dropdown_title2" style="color: #444;">Singapore</span> <span class="caret"></span></button>
								<ul class="dropdown-menu dropdown-menu-right" id="dropdownMenu1" role="menu">
                                    <li><a tabindex="-1" href="#">Singapore</a></li>
                                    <li><a tabindex="-1" href="#">Bangkok</a></li>
                                    <li><a tabindex="-1" href="#">Phuket</a></li>
                                    <li style="z-index:99999"><a tabindex="-1" href="#">Hong Kong</a></li>
                                </ul>
                        </div>
                        <!-- /btn-group -->
                <div id="submit_search_home" class="input-group-addon search-btn submit_search_home">
                           <span class="">Search</span>
                        </div>
					</div>
					<script>
					jQuery.noConflict();
                jQuery(document).ready(function () {
					 jQuery(".dropdown-toggles").on("click", function () {
					 jQuery(".dropdown-menu").show();
                    });
					jQuery("#dropdownMenu1 li a").on("click", function () {
                        var platform = jQuery(this).text();
                        jQuery("#dropdown_title2").html(platform);
                        jQuery(".dropdown-menu").hide();
                        return false;
                    });
                    jQuery('.submit_search_home').on("click", function () {
                        submit_search_form();
                    });
                        //$('#printPlatform').html(platform);

                    });
                
                    function submit_search_form() {
                        var city = jQuery("#dropdown_title2").html();
                        var free_text = jQuery(".restitle_free_text").val();
                        var url = "https://www.weeloy.com/search/restaurant/" + (city.toLowerCase()).split(' ').join('') + "?restitle=" + free_text;
                        //document.location.open = url;
                        var win = window.open(url, '_blank');
                        win.focus();
                        return false;
                    }
					</script>
			<div class="td-header-top-right"><?php td_api_top_bar_template::_helper_show_top_bar() ?></div>
        </div>
    </div>

    <div class="td-banner-wrap-full td-logo-wrap-full">
        <div class="td-container td-header-row td-header-header">
            <div class="td-header-sp-logo">
                <?php locate_template('parts/header/logo.php', true); ?>
            </div>
            <div class="td-header-sp-recs">
                <?php locate_template('parts/header/ads.php', true); ?>
            </div>
        </div>
    </div>

    <div class="td-header-menu-wrap-full">
        <div class="td-header-menu-wrap td-header-gradient">
            <div class="td-container td-header-row td-header-main-menu">
                <?php locate_template('parts/header/header-menu.php', true); ?>
            </div>
        </div>
    </div>

</div>

<div class="mobile-logo">
	<?php locate_template('parts/header/logo.php', true);?>
</div>
