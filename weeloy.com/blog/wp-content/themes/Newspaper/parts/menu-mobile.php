<?php

/*  ----------------------------------------------------------------------------
    This is the mobile off canvas menu
 */

?>
<div id="td-mobile-nav">
    <!-- mobile menu close -->
    <div class="td-mobile-close">
        <a href="#"><?php _etd('CLOSE', TD_THEME_NAME); ?></a>
        <div class="td-nav-triangle"></div>
    </div>

    <div class="td-mobile-content">
        <?php /*
        wp_nav_menu(array(
            'theme_location' => 'header-menu',
            'menu_class'=> '',
            'fallback_cb' => 'td_wp_no_mobile_menu'
        ));

        //if no menu
        function td_wp_no_mobile_menu() {
            //this is the default menu
            echo '<ul class="">';
            echo '<li class="menu-item-first"><a href="' . esc_url(home_url( '/' )) . 'wp-admin/nav-menus.php">Click here - to use the wp menu builder</a></li>';
            echo '</ul>';
        }
		*/
        ?>
		<div class="input-group input-group-lg search-bar">
			<input class="form-control" id="restitle" name="restitle" placeholder="Restaurant name or Cuisine or Food" aria-label="..." type="text">
			<div id="dd_city" class="input-group-addon search-drop">                   
				  <button type="button" data-toggle="dropdown" class="dropdown-toggless" style="height:23px;">
						<span id="dropdown_title3">Singapore</span> <span class="caret"></span></button>
						<ul class="dropdown-menus dropdown-menu-right" id="dropdownMenu2" role="menu">
							<li><a tabindex="-1" href="#">Singapore</a></li>
							<li><a tabindex="-1" href="#">Bangkok</a></li>
							<li><a tabindex="-1" href="#">Phuket</a></li>
							<li style="z-index:99999"><a tabindex="-1" href="#">Hong Kong</a></li>
						</ul>
				</div>
			<!-- /btn-group -->
			<div id="submit_search_home" class="input-group-addon search-btn">
			   <span class="">Search</span>
			</div>
					<script>
					jQuery.noConflict();
					jQuery(document).ready(function(){
					 jQuery(".dropdown-toggless").on("click", function () {
					 jQuery(".dropdown-menus").show();
                    });
					jQuery("#dropdownMenu2 li a").on("click", function () {
                        var platform = jQuery(this).text();
                        jQuery("#dropdown_title3").html(platform);
                        jQuery(".dropdown-menus").hide();
                        //$('#printPlatform').html(platform);
                    });
                    });
					</script>
		</div>
    </div>
</div>

