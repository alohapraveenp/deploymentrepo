<?php
?>
<style>
.weeloy_frame {
    height: 40px;      /* equals max image height */
    width: 160px;
    white-space: nowrap;
    text-align: center; 
}

.weeloy_helper {
    display: inline-block;
    height: 100%;
    vertical-align: middle;
}

.weeloy_img {
    vertical-align: middle;
    max-height: 30px;
    max-width: 160px;
}
</style>

<div class="" style="height:40px;background: rgba(39, 103, 136, 0.8) none repeat scroll 0 0 !important">
    <div class="weeloy_frame">
        <span class="weeloy_helper"></span><img class="weeloy_img " src="images/logo_w_t_small.png" alt="">
    </div>
</div>
