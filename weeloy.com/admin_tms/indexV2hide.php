<?php
require_once("lib/Browser.inc.php");
$browser = new Browser();
$tablette = ($browser->isTablet() || $browser->isMobile());
$patform = $browser->getPlatform();
$windows = ($patform != "" && preg_match("/window/i", $patform));

$tmsversion = "1.90";

// don't move those line as cookie header need to send prior to anything
require_once("conf/conf.init.inc.php");
require_once("lib/wpdo.inc.php");
require_once("lib/gblcookie.inc.php");
require_once("lib/class.login.inc.php");
require_once("lib/class.coding.inc.php");
require_once("conf/conf.session.inc.php");
require_once("lib/class.media.inc.php");
	
require_once("lib/class.images.inc.php");	
require_once("../inc/utilities.inc.php");
require_once("lib/class.analytics.inc.php");
require_once("lib/class.restaurant.inc.php");
require_once("lib/class.member.inc.php");
require_once("lib/class.cluster.inc.php");

?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="author" content="weeloy. https://www.weeloy.com">
<meta name="copyright" content="2014 weeloy. All rights reserved. https://www.weeloy.com">  
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
<title>Weeloy - Table Management System</title>
<link rel="icon" href="../favicon.ico" type="image/gif" sizes="16x16">
<link href="../css/bootstrap33.min.css" rel="stylesheet" type="text/css" />
<link href="../css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
<link href="../css/jquery-ui.css" rel="stylesheet" type="text/css">
<link href="../client/bower_components/fontawesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="../css/admin-style.css" rel="stylesheet" type="text/css"/>
<link href="../css/dropdown.css" rel="stylesheet" type="text/css">
<link href="../css/bootstrap-social.css" rel="stylesheet" type="text/css"/>
<link href="../css/login.css" rel="stylesheet" type="text/css">
<link href="../css/modal.css" rel="stylesheet" type="text/css">
<link href="../css/animate.css" rel="stylesheet" type="text/css">
<link href="../css/login-modal.css" rel="stylesheet" type="text/css"/>
<link href="tms.css" rel="stylesheet" type="text/css"/>
<link href="../client/bower_components/moment/angular-moment-picker.css" rel="stylesheet">
<link href="../client/bower_components/glyphicons/css/glyphicons.css" rel="stylesheet">
<link href="../client/bower_components/fontawesome/css/font-awesome.min.css" rel="stylesheet">
<link href="../client/bower_components/printjs/dist/print.min.css" rel="stylesheet">

<link href='https://fonts.googleapis.com/css?family=Dosis:400,600,700bold|Roboto:400,900,700italic,700,500italic,500,400italic' rel='stylesheet' type='text/css'>

<script type="text/javascript" src="../client/bower_components/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="../client/bower_components/jquery-ui/jquery-ui.js"></script>
<script type="text/javascript" src="../client/bower_components/angular/angular.min.js"></script>
<script type="text/javascript" src="../client/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="../client/bower_components/ui-bootstrap-tpls-0.14.2.min.js"></script>
<script type="text/javascript" src="../client/bower_components/moment/moment-with-locales.js"></script>
<script type="text/javascript" src="../client/bower_components/moment/angular-moment-picker.js"></script>
<script type="text/javascript" src="../client/bower_components/printjs/dist/print.min.js"></script>

    
<style> 
.classgrey a { color: grey }
.classblack { color: black }
.classred { color: red }
.classyellow { color: yellow }
.classblue { color: blue }
.classpurple { color: purple }
.selectedline { background-color: pink !important }
.classgreen { color: green }
.classcyan { font-size: 20px; color: pink !important }
.classbgblack { background-color: black }
.classbgwhite { background-color: white }
.infobk { font-size:12px; padding-right:40px; }
.glyphiconsize  { font-size:12px; }
.chatclassred { color:red;font-size:18px; }
.chatclassblue { color:blue;font-size:18px; }

.cvip { color: red; background-color:yellow; font-size:13px;border-style: solid; border-width: 1px;}
.cevent { color: green; background-color:white; font-size:13px;border-style: solid; border-width: 1px;}
.csoigne { color: navy; background-color:white; font-size:13px;border-style: solid; border-width: 1px;}
.ctable { color: purple; background-color:white; font-size:13px;border-style: solid; border-width: 1px;}
.cchinese { color: orange; background-color:white; font-size:13px;border-style: solid; border-width: 1px;}
.cseated { color: darkturquoise; background-color:white; font-size:13px;border-style: solid; border-width: 1px;}

.scrollable-menu {
	font-size:14px;
    height: auto;
    max-height: 600px;
    overflow-x: hidden;
}

.scrollablesmall-menu {
	font-size:14px;
    height: auto;
    max-height: 600px;
    overflow-x: hidden;
}

.scrollable-menu-all {
    height: auto;
    overflow-x: hidden;
}

.scrollable-menu::-webkit-scrollbar {
    -webkit-appearance: none;
    width: 10px;        
}  

.scrollable-menu::-webkit-scrollbar-thumb {
    border-radius: 10px;
    background-color: #5285a0;
    -webkit-box-shadow: 0 0 1px rgba(255,255,255,.75);        
}

.scrollablesmall-menu::-webkit-scrollbar {
    -webkit-appearance: none;
    width: 10px;        
}  

.scrollablesmall-menu::-webkit-scrollbar-thumb {
    border-radius: 10px;
    background-color: #5285a0;
    -webkit-box-shadow: 0 0 1px rgba(255,255,255,.75);        
}

.template-separation {
padding:0 10px 10px 10px;
}

ul.dropdown-menu li { cursor: pointer; 	font-size:15px; }
ul.dropdown-menu li span.red { color: red; }
ul.dropdown-menu li span.green { color: green; }

#punch_chart {
	font: 300 100.1% "Helvetica Neue", Helvetica, "Arial Unicode MS", Arial, sans-serif;
	background: #333;
	color: #333;
	top: 50%;
	left: 50%;
	margin: 20px 20px 20px 20px;
	height: 340px;
	width: 820px;
}

.collapse, li {
  margin-left: 10px;
  padding-top: 5px !important;
}

.navbar-static-top {
   margin:0;
} 

.navbar-right > li {
  margin-right: 10px;
}

.spacing button {
  margin-right: 10px;
  margin-left: 10px;
}

.spacing td {
  text-align: center;
}

.dropdown-menu table {
  height: 150px;
  width: 150px;
  margin-right: 0;
  margin-left: 0;
  padding:0;
}

.dropdown-menu table td {
  margin-right: 0;
  margin-left: 0;
  padding:0;
}

.glyphicons {
font-size: 16px;
}

span.short-label {
font-size: 9px;
}

.modal-content {
    left: -200px;
}

@media screen and (max-width: 900px) {
   .smallhide {
   	display:none;
   	}
}

@media screen and (max-width: 1200px) {
   td .btn {
   	padding: 3px 3px;
   	}   	
   span.short-label {
	font-size: 8px;
   	}
}


<?php if($tablette): ?>	

.navbar-collapse.collapse {
display: block!important;
}

.navbar-nav>li, .navbar-nav {
float: left !important;
}

.navbar-nav.navbar-right:last-child {
margin-right: -15px !important;
}

.navbar-right {
float: right!important;
}
 
<?php endif; ?>

</style>

<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<script src="js/selectivizr-min.js"></script>
<script src="js/modernizr.js"></script>
<link rel="stylesheet" type="text/css" href="css/ie.css" media="screen" />
<![endif]-->
</head>


<script> var app = angular.module('backoffice',['ui.bootstrap', 'FacebookProvider', 'ngStorage', 'moment-picker']); </script>

<?php if($tablette == false): ?>
<body id='MainTMS' ng-app='backoffice' ng-controller='boHomeController' ng-init="moduleName='tms';" onresize='reinitwindow()' ng-cloak>
<?php else: ?>
<body id='MainTMS' ng-app='backoffice' ng-controller='boHomeController' ng-init="moduleName='tms';" onresize='reinitwindow()'>
<?php endif; ?>

<div id='navbartms' style='height: 100%'>
<div class="my-nav-bars navbar-default navbar-fixed-top" style="height: 143px;">
<nav class="navbar  navbar-default navbar-static-top bottom-border">
  <div class="container-fluid">
    <div class="navbar-header">
      <button class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand smallhide" href="javascript:location.reload(true);"><img src="../images/admin/logoweeloy.svg" alt="weeloy-best-restaurant-logo" width="60px"></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav" ng-if="logaction != 'login'">
		<li ng-if="editmode !== 1 && defaultLayout !=='' && runonlymode !== 99">
		  <button class="btn btn-primary dropdown-toggle" data-toggle="dropdown" style="font-size:14px;">RUN <span class="caret"></span></button>
		  <ul class="dropdown-menu">
			<li><a href ng-click="lyrun();">Start</a></li>
			<li ng-if="editmode === 0" class="divider"></li>
			<li ng-if="editmode === 0" ng-repeat="ll in viewerconnect.obj[0].label[2] track by $index"><a href ng-click="viewerconnect.toggle(0, $index);" ng-class="{ 'classgrey': true }">{{ ll }}</a></li>
			<li ng-if="editmode === 0 && viewerconnect.obj[1].show === 1" ng-repeat="ll in viewerconnect.obj[1].label[2] track by $index"><a href ng-click="viewerconnect.toggle(1, $index);">{{ ll }}</a></li>
			<li ng-if="editmode === 0" class="divider"></li>
			<li ng-if="editmode === 0" ><a href ng-click="lyattribution();">Set Attribution</a></li>
			<li ng-if="editmode === 0" ><a href ng-click="lyresetattribution();">Clear Attribution</a></li>
			<li ng-if="editmode === 0" ><a href ng-click="lysaveattribution();">Save configuration</a></li>
			<li ng-if="editmode === 0" class="divider"></li>
			<li ng-if="editmode === 0" ><a href ng-click="lypreferences('Bookings');">Booking Display</a></li>
			<li ng-if="editmode === 0" ><a href ng-click="lypreferences('Actions');">Actions/Confirmations</a></li>
			<li ng-if="editmode === 0 && hidefeatflg"><a href ng-click="lypreferences('HideFeatures');">Hide Features</a></li>
			<li ng-if="editmode === 0 && captainflg === 1 && layoutcaptain === 1" class="divider"></li>
			<li ng-if="editmode === 0 && captainflg === 1 && layoutcaptain === 1"><a href ng-click="lypreferences('Captains');">Manage Captains</a></li>
			<li ng-if="editmode === 0" class="divider"></li>
			<li ng-if="editmode === 0" ><a href ng-click="lyshowdayreport(1);">Show the Lunch Report</a></li>
			<li ng-if="editmode === 0" ><a href ng-click="lyshowdayreport(2);">Show the Dinner Report</a></li>
			<!--<li ng-if="editmode === 0" ><a href ng-click="lyanalytic(1);">Show Analytics</a></li> -->
			<li ng-if="editmode === 0 && runonlymode !== 1" class="divider"></li>
			<li ng-if="editmode === 0 && runonlymode !== 1" ><a href ng-click="restartedit();">Edit Mode</a></li>
		  </ul>
		</li>
		<li class="dropdown" ng-if="editmode !== 0 && editmode !== 99">
	    	  <button class="btn btn-primary dropdown-toggle" data-toggle="dropdown" style='font-size:14px;'>LAYOUT <span class="caret"></span></button>
		  <ul class="dropdown-menu">
			<li><a href ng-click="lyedit();">Edit Mode</a></li>
			<li ng-if="editmode === 1" class="divider"></li>
			<li ng-if="editmode === 1"><a href ng-click="lynewLayout();">New Layout</a></li>
			<li ng-if="floordata.length > 0" class="dropdown-submenu"><a href tabindex="-1" >Set Floor</a>
			    <ul class="dropdown-menu scrollable-menu">
                  <li ng-repeat="y in floordata track by $index"><a tabindex="-1" ng-click="lysetfloor(y);" ng-class="{ 'classcyan': y === floorname } " style="cursor:pointer;">{{y}}</a></li>
                </ul>
			</li>
			<li ng-if="editmode === 1 && layoutdata.length > 0" class="dropdown-submenu"><a href tabindex="-1" >Read</a>
			    <ul class="dropdown-menu scrollable-menu">
                  <li ng-repeat="y in layoutdata track by $index"><a tabindex="-1" ng-click="lyselectLayout(y.name);" ng-class="{ 'classcyan': y.name === nameLayout }" style="cursor:pointer;">{{y.name}}</a></li>
                </ul>
			</li>
			<li ng-if="editmode === 1 && indexLayout !== -1" class="dropdown-submenu"><a href tabindex="-1" >Cmd</a>
			    <ul class="dropdown-menu scrollable-menu">
                  		<li ng-repeat="y in layoutcmd track by $index"><a tabindex="-1" ng-click="lyLayoutcmd(y.action);" style="cursor:pointer;"><table style='height: 10px;margin:0 0 0 0'><tr><td>{{y.action}}</td><td style="text-align:right">{{y.keyboard}}</td></tr></table></a>
                  		</li>
                	</ul>
			</li>
			<li ng-if="showlayout"><a href ng-click="lydefault();">Set as default</a></li>
			<li ng-if="showlayout && indexLayout !== -1 && captainflg === 1"><a href ng-click="lysetcaptains();">Set Captains</a></li>
			<li ng-if="showlayout && indexLayout !== -1 && captainflg === 1"><a href ng-click="lysetoptions();">Set Options</a></li>
			<li ng-if="showlayout && indexLayout !== -1 && captainflg === 1"><a href ng-click="lyscheduler();">Scheduler</a></li>
			<li ng-if="showlayout" class="divider"></li>
			<li ng-if="showlayout"><a href ng-click="lysave();">Save</a></li>
			<li ng-if="showlayout && indexLayout !== -1"><a href ng-click="lyrename();">Rename</a></li>
			<li ng-if="showlayout && indexLayout !== -1"><a href ng-click="lyduplicate();">Duplicate</a></li>
			<li ng-if="showlayout && indexLayout !== -1"><a href ng-click="lydeleteLayout();">Delete</a></li>
			<li ng-if="showlayout" class="divider"></li>
			<li ng-if="showlayout && indexLayout !== -1"><a href ng-click="viewerconnect.toggle(0, 0);">{{ viewerconnect.obj[0].label[2][0] }}</a></li>
			<li ng-if="showlayout && indexLayout !== -1"><a href ng-click="lyclean();">Clean</a></li>
			<li ng-if="showlayout && indexLayout !== -1 && MFflg" class="divider"></li>
			<li ng-if="showlayout && indexLayout !== -1 && MFflg">&nbsp; &nbsp; &nbsp;   Multiple Floor </li>
			<li ng-if="showlayout && indexLayout !== -1 && MFflg"><a href ng-click="lynewMFloor();">New</a></li>
			<li ng-if="showlayout && indexLayout !== -1 && MFflg && multiplefloor.length > 0" class="dropdown-submenu"><a href tabindex="-1">Edit</a>
			    <ul class="dropdown-menu scrollable-menu">
                <li ng-repeat="y in multiplefloor track by $index"><a tabindex="-1" ng-click="lyeditMFloor(y.name);" style="cursor:pointer;">{{y.name}}</a></li>			
                </ul>
			</li>
			<li ng-if="showlayout" class="divider"></li>
			<li ng-if="showlayout"><a href ng-click="restartrun();">Run</a></li>
		  </ul>
		</li>
	  	<li ng-if="editmode === 0" >
		<button class="btn btn-primary" ng-click="lypreferences('Actions');" style='font-size:14px;'>PREFS </button>
	  	</li>
		<li>
		<button class="btn btn-primary dropdown-toggle" data-toggle="dropdown" style='font-size:14px;'>HELP <span class="caret"></span></button>
		  <ul class="dropdown-menu">
			<li ng-repeat="t in helpservice.labels"><a href ng-click="helpservice.showhelp(t);">{{ t }} </a></li>
	  	</ul>
	  	</li>
	  	<li ng-if="editmode === 0" >
		<div  style="width:50px;margin-left:10px;">
		<div class="lead" moment-picker="datePickerDoor.pickerdate" min-date='datePickerDoor.mindate' max-date='datePickerDoor.maxdate' today=true format="YYYY-MM-DD" start-view="month" locale="en" change="changeMPdate();"> <i class="glyphicon glyphicon-calendar"></i></div>
		</div>
		</li>
		<li ng-if="editmode === 0 && datePickerDoor.today === 0" >
		<button style="margin-left:-30px;width:20px;" class="btn btn-default btn-xs" ng-click="changeSubdate();"><span class="glyphicons glyphicons-arrow-left" style="font-size:18px;;margin-left:-13px;"></span></button>
		</li>
		<li ng-if="editmode === 0"  id='pickerdate' ><span style="margin-left:0px;font-size:10px">{{ datePickerDoor.originaldate.toLocaleDateString("en-US",{weekday: 'short', year: 'numeric', month: 'short', day: 'numeric'}) }}</span>
		</li>
		<li ng-if="editmode === 0" >
		<button style="margin-left:0;width:20px;" class="btn btn-default btn-xs" ng-click="changeAdddate();"><span class="glyphicons glyphicons-arrow-right" style="font-size:18px;margin-left:-13px;"></span></button>
		</li>
		<li ng-if="editmode === 0 && datePickerDoor.today === 0" >
		<button style="margin-left:5px;color:green" class="btn btn-default" ng-click="resetMPdate();">today</button>
		</li>
<!--
		<li ng-if="editmode === 0" >
		<button style="margin-left:35px" class="btn btn-default" ng-click="managewindow('left');"><i class="glyphicon glyphicon-arrow-left"></i></button>
		</li>
		<li ng-if="editmode === 0" >
		<button style="margin-left:5px" class="btn btn-default" ng-click="managewindow('right');"><i class="glyphicon glyphicon-arrow-right"></i></button>
		</li>		
-->
		<li ng-if="editmode === 0" >
		<button style="margin-left:5px;" class="btn btn-default btn-xs" ng-click="showevent('right');"><span class="glyphicons glyphicons-sunbath" style="color:indigo;font-size:18px;"></span></button>
		</li>
		<li ng-if="editmode === 0 && chatflg" >
		<button style="margin-left:5px;" class="btn btn-default btn-xs" ng-click="showchat();"><span class="glyphicons glyphicons-group-chat" ng-style="chatstyle"></span></button>
		</li>
		<li ng-if="editmode === 0 && stickynotesflg" >
		<button style="margin-left:5px;" class="btn btn-default btn-xs" ng-click="showstickynotes();"><span class="glyphicons glyphicons-notes-2" ng-style="stickynotesstyle"></span></button>
		</li>
		<li ng-if="editmode === 0" >
		<button style="margin-left:5px" class="btn btn-default btn-xs" ng-click="scrolltop()"><span class="glyphicons glyphicons-circle-arrow-top" style="font-size:18px;color:brown"></span></button>
		</li>
	  </ul>
 	  <ul class="nav navbar-nav navbar-right">
		  <li> <button class="btn btn-{{logcolor}} btn-xs" ng-click="loginout(logaction);"><span class="glyphicon glyphicon-log-{{logactionpict}}"></span> {{logaction}} </button></li>
		  <li ng-if="!mobilephone && logaction==='logout'" id="sessiontime" style='font-size:xx-small;'></li>
  	  	  <li ng-if="imglogo != '' && !mobilephone" ><img ng-src='{{imglogo}}' width='30'  class="smallhide"><span ng-if="restotitle != ''" style="font-size:9px;"  class="smallhide"><br/>{{restotitle}}</span></li>
	  </ul>
	</div><!--/.nav-collapse -->
  </div>
</nav>
<nav ng-if="editmode === 0" class="navbar  navbar-static-top bottom-border" style="margin-top:0; padding-top:0;position: fixed;height: 78px;top: 65px;width: 100%; z-index:1">
  <div class="container-fluid">
      <ul class="nav navbar-nav">
        <li>
	<table style='font-size:10px;margin:10px 0 10px 10px;'><tr class='spacing'>
	<td align='center'>
		<button class="btn btn-default" ng-click="refreshbooking();" id='refreshbooking'><span class="glyphicons glyphicons-transfer" style="color:red"></span></button><br><span class="short-label">refresh&nbsp;resa</span>
	</td>
	<td align='center'>
		<button class="btn btn-default" ng-click="bookingdetails();" id='bookingdetails'><span class="glyphicons glyphicons-shop" style="color:indigo"></span></button><br><span class="short-label">full&nbsp;booking</span>
	</td>
	<td ng-if="nofullcc === false">
		<button class="btn btn-default" ng-click="addbooking(0);" id='addbooking'><span class="glyphicons glyphicons-earphone" style="color:brown"></span></button><br><span class="short-label">callcenter</span>
	</td>
	<td ng-if="datePickerDoor.today === 1" align='center'>
		<button class="btn btn-default" ng-click="walkingbooking();" id='walkingbooking'>&nbsp;<span class="glyphicons glyphicons-person-walking" style="color:purple"></span></button><br><span class="short-label">walk-ins</span>
	</td>
	<td align='center'>
		<button class="btn btn-default" ng-click="lybooking(0);" id='lybooking'><span class="glyphicons glyphicons-dining-set" style="color:blue;"></span></button><br><span class="short-label">book&nbsp;view</span>
	</td>
	<td ng-if="waitinglist && gethidefeatval('waiting view')">
		<button class="btn btn-default" ng-click="lywaitingview()" id='lywaitingview'><span class="glyphicons glyphicons-hourglass" style="color:darkturquoise "></span></button><br><span class="short-label">waiting&nbsp;view</span>
	</td>
	<td ng-if="gethidefeatval('last bookings')">
		<button class="btn btn-default" ng-click="lylastbookingview();" id='lylastbookingview'><span class="glyphicons glyphicons-rabbit" style="color:firebrick "></span></button><br><span class="short-label">last&nbsp;bookings</span>
	</td>
	<td ng-if="gethidefeatval('timeline view')">
		<button class="btn btn-default" ng-click="lytimeline(1);" id='lytimeline'><span class="glyphicons glyphicons-align-left" style="color:teal "></span></button><br><span class="short-label">timeline&nbsp;view</span>
	</td>
	<td ng-if="icicle.mode && gethidefeatval('icicle view')">
		<button class="btn btn-default" ng-click="lyicicle();" id='lyicicle'><span class="glyphicons glyphicons-align-left rotate90" style="color:orange"></span></button><br><span class="short-label">icicle&nbsp;view</span>
	</div>
	<td ng-if="subindexLayout > -1 && gethidefeatval('next floor')">
		<button class="btn btn-default" ng-click="nextfloor();" id='nextfloor'><span class="glyphicons glyphicons-circle-arrow-right" style="color:purple"></span></button><br><span class="short-label">next&nbsp;floor</span>
	</td>
	<td ng-if="gethidefeatval('block tables')">
		<div class="btn-group" style="margin-left:10px;-webkit-overflow-scrolling:touch;">
		<button class="btn btn-default dropdown-toggle" data-toggle="dropdown" ><span class="glyphicons glyphicons-stop-sign" style="color:red"></span></button><br><span class="short-label">block tables</span>
		<ul class="dropdown-menu scrollable-menu" role="menu">
		<li ng-repeat="x in tablesAr" class='glyphiconsize'><a href ng-class="{ 'glyphicon glyphicon-ok glyphiconsize': BlockedTablesAr.indexOf(x)>= 0 }" ng-click="setblocktable(x)"><span style="font-size:12px">{{x}}</span></a></li>
		</ul>
		</div> 		
	</td>
	<td ng-if="captainlist.length > 0 && gethidefeatval('captain')">
		<div class="btn-group" style="margin-left:10px">
		<button class="btn btn-default dropdown-toggle" data-toggle="dropdown" ><span class="glyphicons glyphicons-anchor" style="color:navy"></span></button><br><span class="short-label">captain {{ currentcaptain }}</span>
		<ul class="dropdown-menu" role="menu">
		<li ng-repeat="x in captainlist" class='glyphiconsize'><a href ng-click="viewcaptain(x)"> {{x}}</a></li>
		</ul>
		</div> 		
	</td>
	<td>
		<button class="btn btn-default" ng-click="statistics();" id='statistics'><span class="glyphicons glyphicons-pie-chart" style="color:indigo"></span></button><br><span class="short-label">covers</span>
	</td>
	<td>
		<button class="btn btn-default" ng-click="viewprofile();" id='viewprofile'><span class="glyphicons glyphicons-group" style="color:green"></span></button><br><span class="short-label">show&nbsp;guest</span>
	</td>
	<td  ng-if="!epson && gethidefeatval('print guest')">
		<button class="btn btn-default" ng-click="printnewprofile();" id='printnewprofile'><span class="glyphicons glyphicons-shredder" style="color:brown"></span></button><br><span class="short-label">print&nbsp;guest</span>
	</td>
	<td  ng-if="epson && gethidefeatval('print guest')">
		<a type='button' id='printsummary' class='btn btn-default btn' href='weeloypro://print_receipt/' style='font-size:10px;' target='_blank'><span class="glyphicons glyphicons-shredder" style="color:brown"></span></a><br><span class="short-label">print&nbsp;guest</span>
	</td>
	</tr></table>
        </li>
      </ul>
      </div>
</nav>
</div>

<div id="threewaywindow" style="position:relative;top:143px;height:100%">
    <div id="left" style='overflow-y:scroll;max-height: 100vh;-webkit-overflow-scrolling:touch;background-color:white;'>
      	<div ng-if='tablette' id="draggableHandle" class="ui-widget-content">
        	<p align='right'><img src='icons/arrow-circle-all.svg' width='30'></p>
      </div>
 	<div id="booking" style='overflow-x: hidden;overflow-y:scroll;max-height:100vh;-webkit-overflow-scrolling:touch;background-color:white;'>
   	<div id='mydisplay'>
   		<ng-include src="bookingview"> </ng-include>
   	</div>
   	</div>
    </div>

    <div id="right" style='overflow-y:auto;max-height: 100vh'>
	<div id='raphael'></div>
	<content-item ng-repeat="item in translatedata" content="item" myTemplates="templateData"></content-item>
	</div>
    
    </div>
  	<div ng-include="btemplate" ng-if="logaction==='login'"></div>
</div>

<div id="fb-root"></div>

<script type="text/javascript" src="../js/raphael.js"></script>
<script type="text/javascript" src="touchUI.js"></script>
<script type="text/javascript" src="../js/facebookProvider.js"></script>
<script type="text/javascript" src="../js/facebookRun.js"></script>
<script type="text/javascript" src="../js/loginService.js?2"></script>
<script type="text/javascript" src="../js/ngStorage.min.js"></script>
<script type="text/javascript" src="../js/formControl.js"></script>
<script type="text/javascript" src="../modules/rltimeio/ortc.js"></script>

<?php 

printf("<script>");

printf("var opsyswindow = %s;", ($windows) ? 'true': 'false');
printf("var tmsversion = '%s';", $tmsversion);
printf("var tmsUI = 2;");
echo "var cookiename = '" . getCookiename('translation') . "';\n"; /* connected to the alog.js script file */ 
printf("var tablette = %s;", ($tablette) ? 'true': 'false');
printf("var mobilephone = %s;", ($browser->isMobile()) ? 'true': 'false');

printf("</script>");

printf("<script type='text/javascript' src='../js/alog.js?tmsv=%s'></script>", $tmsversion);
printf("<script type='text/javascript' src='../backoffice/inc/libService.js?tmsv=%s'></script>", $tmsversion);
printf("<script type='text/javascript' src='../backoffice/inc/profilelib.js?tmsv=%s'></script>", $tmsversion);
printf("<script type='text/javascript' src='weeloytmshide.js?tmsv=%s'></script>", $tmsversion);
?>

<script type="text/javascript" src="cp/cpcolors.js"></script>
<script type="text/javascript" src="cp/jqColorPicker.js"></script>
<script type="text/javascript" src="cp/cpstart.js"></script>

<?
// some template works if direct included in the file !!!
//  	<ng-include="'tmsTemplates.html'"></ng-include>

include("tmsTemplates.html");
include("../backoffice/profileTemplates.html");

?>

<script type="text/ng-template" id="emptyview.html"><img src="../images/admin/bo_slider_empty.gif"></script>
<script type="text/ng-template" id="timelineview.html"> <div id="timeline_drawing" style='background-color:black;' loadingDiv></div> </script>
<script type="text/ng-template" id="punchchartview.html"> <div id="punch_chart" style='background-color:black;'></div> </script>

<div ng-include="'../backoffice/inc/myModalModif.html'"></div>
<div ng-include="'aIcicle.html'"></div>
<div ng-include="'acaptains.html'"></div>
<div ng-include="'ascheduler.html'"></div>

<script type="text/ng-template" id="embededlogin.html">
<div class="modal-body">
<form ng-submit="modalOptions.submit(modalOptions.mydata)">
<iframe id='loginFrame' src='../alogin/index.php' width='100%' height='350' frameBorder='0' margin='0'></iframe>
<div>
</script>

<script>

var imagesRes = [];
var iconsRes = [];
loginTitleTormat = "small";


<?php 

function GetElement($dir) {

	$fNames = array();
	$handle = opendir($dir);
	while($file = readdir($handle))
		if(is_dir($dir . $file) == false) {
			$parts = pathinfo($dir . $file);
			if($parts['extension'] == "svg" || $parts['extension'] == "jpg")
				$fNames[] = $file;
			}
	closedir($handle);
	return $fNames;
}

$elementAr = GetElement("images/");
natsort($elementAr);

$sep = "";
echo "var elementsAr = [";
foreach($elementAr as $name) {
	echo $sep . " '" . $name . "'";
	$sep = ",";
	}
echo "];";

$iconsAr = GetElement("icons/");
$sep = "";
echo "\nvar iconsAr = [";
foreach($iconsAr as $name) {
	echo $sep . " '" . $name . "'";
	$sep = ",";
	}
echo "];";

?>

$(document).ready(function() { 
max = elementsAr.length; for(i = 0; i < max; i++) { imagesRes[i] = new Image(); imagesRes[i].src = 'images/' + elementsAr[i]; } 
max = iconsAr.length; for(i = 0; i < max; i++) { iconsRes[i] = new Image(); iconsRes[i].src = 'icons/' + iconsAr[i]; } 
$('[data-toggle="popover"]').popover();
}); 


</script>

</body>
</html>
