<?php
require_once("conf/conf.init.inc.php");
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv='expires' content='mon, 1 jan 1998 05:00:00 gmt'>
<meta http-equiv='pragma' content='no-cache'>
<meta http-equiv='pragma' content='cache-control: max-age=0'>
<meta http-equiv='last-modified' content='mon, 1 jan 1998 05:00:00 gmt'>
<meta http-equiv='cache-control' content='no-cache, must-revalidate'>
<meta name='robots' content='noindex, nofollow'>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="HandheldFriendly" content="true" />

<link href="/weeloy.com/css/bootstrap33.min.css" rel="stylesheet" type="text/css">
<link href="css/style.css" rel="stylesheet" type="text/css"> 
<link href="/weeloy.com/css/font-awesome.css" rel="stylesheet" type="text/css"> 
<link href="css/animate.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/weeloy.com/js/jquery.min.js"></script>
<script type="text/javascript" src="/weeloy.com/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/weeloy.com/js/custom.js"></script>
<script type="text/javascript" src="/weeloy.com/js/angular.min.js"></script>
<script type="text/javascript" src='http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js'></script>


<title>Page Divider</title>
<style>

html, body {
    height: 100%;
    margin: 0;
    padding: 0;
}

#content {
    position: relative;
    height: 100%;
}

#left, #right {
    float: left;
    width: 50%;
    height: 100%; 
    overflow: auto;
}

#left {
    background: #fee;
}
#right {
    background: #efe;
}

#divider {
    position: absolute;
    width: 2px;
    margin-left: -2px;
    top: 0;
    bottom: 0;
    background: #bbb;
    border: 1px outset #bbb;
    border-top: 0;
    border-bottom: 0;
    cursor: col-resize;
}

html.dragging * {
    cursor: col-resize !important;
}

</style>

</head>

<body ng-app="backoffice-page" ng-controller="boHomeController">

<script> var app = angular.module('backoffice-page',['ui.bootstrap', 'FacebookProvider', 'ngStorage']); </script>

<header id="header_wrapper">
  <div class="container">
    <div class="header_box">
      <div class="logo"><a href="#"><img width="90px" src="/weeloy.com/images/logo_w.png" alt="weeloy-best-restaurant-logo" /></a></div>
	  <nav class="navbar navbar-inverse" role="navigation">
      <div class="navbar-header">
        <button type="button" id="nav-toggle" class="navbar-toggle" data-toggle="collapse" data-target="#main-nav"> 
        <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
        </div>
	    <div id="main-nav" class="collapse navbar-collapse navStyle">
			<ul class="nav navbar-nav" id="mainNav">
			  <li><a href="#hero_section" class="scroll-link">Home</a></li>
			  <li><a href="#service" class="scroll-link">Services</a></li>
			  <li><a href="#Portfolio" class="scroll-link">Portfolio</a></li>
			  <li><a href="#clients" class="scroll-link">Clients</a></li>
			  <li><a href="#team" class="scroll-link">Team</a></li>
			  <li><a href="#contact" class="scroll-link">Contact</a></li>
			  <li><button class="btn btn-default btn-sm" ng-click="loginout(logaction)">{{logaction}} </button>
			  <li ng-if="logaction == 'logout'"><button class="btn btn-default btn-sm" ng-click="loginout('preferences')">Preferences </button></li>
        	  <li id="sessiontime" class="small"></li>
			</ul>
      </div>
	 </nav>
    </div>
  </div>
</header>

<div id="content">
    <div id="left">left</div>
    <div id="right">right</div>
</div>


<div id="fb-root"></div>

<script type="text/javascript" src="/weeloy.com/js/alog.js"></script>
<script type="text/javascript" src="/weeloy.com/js/facebookRun.js"></script>
<script type="text/javascript" src="/weeloy.com/js/facebookProvider.js"></script>
<script type="text/javascript" src="/weeloy.com/js/loginService.js"></script>
<script type="text/javascript" src="/weeloy.com/js/ngStorage.min.js"></script>
<script type="text/javascript" src="/weeloy.com/js/formControl.js"></script>
<script type="text/javascript" src="/weeloy.com/admin_tms/tmsService.js"></script>
<script type="text/javascript" src="/weeloy.com/admin_tms/tmsRaphael.js"></script>
<script type="text/javascript" src="/weeloy.com/admin_tms/boHomeController.js"></script>

<script type="text/ng-template" id="loginBackoffice.html">
	<content-item ng-repeat="item in interfaceLogin" content="item" myTemplates="templateData"></content-item>
</script>

<script type="text/ng-template" id="forgotBackoffice.html">
	<content-item ng-repeat="item in interfaceForgot" content="item" myTemplates="templateData"></content-item>
</script>

<script type="text/ng-template" id="chgpassBackoffice.html">
	<content-item ng-repeat="item in interfaceChange" content="item" myTemplates="templateData"></content-item>
</script>

<script type="text/ng-template" id="registerBackoffice.html"> 
	<content-item ng-repeat="item in interfaceRegister" content="item" myTemplates="templateData"></content-item>
</script>

<script>

<?php 

echo "var cookiename = '" . getCookiename('translation') . "';\n"; /* connected to the alog.js script file */ 

function GetElement($dir) {

	$fNames = array();
	$handle = opendir($dir);
	while($file = readdir($handle))
		if(is_dir($dir . $file) == false) {
			$parts = pathinfo($dir . $file);
			if($parts['extension'] == "svg")
				$fNames[] = $file;
			}
	closedir($handle);
	return $fNames;
}

$elementAr = GetElement("images/");
$sep = "";
echo "var elementsAr = [";
foreach($elementAr as $name) {
	echo $sep . " '" . $name . "'";
	$sep = ",";
	}
echo "];";

?>

var imagesRes = [];

$(document).ready(function() { max = elementsAr.length; for(i = 0; i < max; i++) { imagesRes[i] = new Image(); imagesRes[i].src = 'images/' + elementsAr[i]; } }); 


var content = document.getElementById('content');
var left = document.getElementById('left');
var right = document.getElementById('right');
var divider = document.createElement('div');
divider.id = 'divider';
content.appendChild(divider);

var leftPercent = 50;

function updateDivision() {
    divider.style.left = leftPercent + '%';
    left.style.width = leftPercent + '%';
    right.style.width = (100 - leftPercent) + '%';
}

updateDivision();

divider.addEventListener('mousedown', function(e) {
    e.preventDefault();
    var lastX = e.pageX;
    document.documentElement.className += ' dragging';
    document.documentElement.addEventListener('mousemove', moveHandler, true);
    document.documentElement.addEventListener('mouseup', upHandler, true);
    function moveHandler(e) {
        e.preventDefault();
        e.stopPropagation();
        var deltaX = e.pageX - lastX;
        lastX = e.pageX;
        leftPercent += deltaX / parseFloat(document.defaultView.getComputedStyle(content).width) * 100;
        updateDivision();
    }
    function upHandler(e) {
        e.preventDefault();
        e.stopPropagation();
        document.documentElement.className = document.documentElement.className.replace(/\bdragging\b/, '');
        document.documentElement.removeEventListener('mousemove', moveHandler, true);
        document.documentElement.removeEventListener('mouseup', upHandler, true);
    }
}, false);

</script>

</body>
</html>
