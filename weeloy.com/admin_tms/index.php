<?php
require_once("lib/Browser.inc.php");
$browser = new Browser();
$tablette = ($browser->isTablet() || $browser->isMobile());
$patform = $browser->getPlatform();
$windows = ($patform != "" && preg_match("/window/i", $patform));

$tmsversion = "1.90";

// don't move those line as cookie header need to send prior to anything
require_once("conf/conf.init.inc.php");
require_once("lib/wpdo.inc.php");
require_once("lib/gblcookie.inc.php");
require_once("lib/class.login.inc.php");
require_once("lib/class.coding.inc.php");
require_once("conf/conf.session.inc.php");
require_once("lib/class.media.inc.php");
	
require_once("lib/class.images.inc.php");	
require_once("../inc/utilities.inc.php");
require_once("lib/class.analytics.inc.php");
require_once("lib/class.restaurant.inc.php");
require_once("lib/class.member.inc.php");
require_once("lib/class.cluster.inc.php");

?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="author" content="weeloy. https://www.weeloy.com">
<meta name="copyright" content="2014 weeloy. All rights reserved. https://www.weeloy.com">  
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
<title>Weeloy - Table Management System</title>
<link rel="icon" href="../favicon.ico" type="image/gif" sizes="16x16">
<link href="../css/bootstrap33.min.css" rel="stylesheet" type="text/css" />
<link href="../css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
<link href="../css/jquery-ui.css" rel="stylesheet" type="text/css">
<link href="../client/bower_components/fontawesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="../css/admin-style.css" rel="stylesheet" type="text/css"/>
<link href="../css/dropdown.css" rel="stylesheet" type="text/css">
<link href="../css/bootstrap-social.css" rel="stylesheet" type="text/css"/>
<link href="../css/login.css" rel="stylesheet" type="text/css">
<link href="../css/modal.css" rel="stylesheet" type="text/css">
<link href="../css/animate.css" rel="stylesheet" type="text/css">
<link href="../css/login-modal.css" rel="stylesheet" type="text/css"/>
<link href="tms.css" rel="stylesheet" type="text/css"/>
<link href="../client/bower_components/moment/angular-moment-picker.css" rel="stylesheet">

<link href='https://fonts.googleapis.com/css?family=Dosis:400,600,700bold|Roboto:400,900,700italic,700,500italic,500,400italic' rel='stylesheet' type='text/css'>

<script type="text/javascript" src="../client/bower_components/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="../client/bower_components/jquery-ui/jquery-ui.js"></script>
<script type="text/javascript" src="../client/bower_components/angular/angular.min.js"></script>
<script type="text/javascript" src="../client/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="../client/bower_components/ui-bootstrap-tpls-0.14.2.min.js"></script>
<script type="text/javascript" src="../client/bower_components/moment/moment-with-locales.js"></script>
<script type="text/javascript" src="../client/bower_components/moment/angular-moment-picker.js"></script>

    
<style> 
.classgrey a { color: grey }
.classblack { color: black }
.classred { color: red }
.classyellow { color: yellow }
.classblue { color: blue }
.classpurple { color: purple }
.selectedline { background-color: pink !important }
.classgreen { color: green }
.classcyan { font-size: 20px; color: pink !important }
.classbgblack { background-color: black }
.classbgwhite { background-color: white }
.infobk { font-size:12px; padding-right:40px; }
.glyphiconsize  { font-size:12px; }


.cvip { color: red; background-color:yellow; font-size:13px;border-style: solid; border-width: 1px;}
.cseated { color: #00CED1; background-color:white; font-size:13px;border-style: solid; border-width: 1px;}
.cevent { color: green; background-color:white; font-size:13px;border-style: solid; border-width: 1px;}
.csoigne { color: navy; background-color:white; font-size:13px;border-style: solid; border-width: 1px;}
.ctable { color: purple; background-color:white; font-size:13px;border-style: solid; border-width: 1px;}
.cchinese { color: orange; background-color:white; font-size:13px;border-style: solid; border-width: 1px;}

.scrollable-menu {
	font-size:12px;
    height: auto;
    max-height: 300px;
    overflow-x: hidden;
}

.scrollablesmall-menu {
	font-size:10px;
    height: auto;
    max-height: 200px;
    overflow-x: hidden;
}

.scrollable-menu-all {
    height: auto;
    overflow-x: hidden;
}

.scrollable-menu::-webkit-scrollbar {
    -webkit-appearance: none;
    width: 10px;        
}  

.scrollable-menu::-webkit-scrollbar-thumb {
    border-radius: 10px;
    background-color: #5285a0;
    -webkit-box-shadow: 0 0 1px rgba(255,255,255,.75);        
}

.scrollablesmall-menu::-webkit-scrollbar {
    -webkit-appearance: none;
    width: 10px;        
}  

.scrollablesmall-menu::-webkit-scrollbar-thumb {
    border-radius: 10px;
    background-color: #5285a0;
    -webkit-box-shadow: 0 0 1px rgba(255,255,255,.75);        
}

ul.dropdown-menu li { cursor: pointer; }
ul.dropdown-menu li span.red { color: red; }
ul.dropdown-menu li span.green { color: green; }

.template-separation {
padding:0 10px 10px 10px;'
}

#punch_chart {
	font: 300 100.1% "Helvetica Neue", Helvetica, "Arial Unicode MS", Arial, sans-serif;
	background: #333;
	color: #333;
	top: 50%;
	left: 50%;
	margin: 20px 20px 20px 20px;
	height: 340px;
	width: 820px;
}

<?php if($tablette): ?>	

.navbar-collapse.collapse {
display: block!important;
}

.navbar-nav>li, .navbar-nav {
float: left !important;
}

.navbar-nav.navbar-right:last-child {
margin-right: -15px !important;
}

.navbar-right {
float: right!important;
}


<?php endif; ?>

</style>

<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<script src="js/selectivizr-min.js"></script>
<script src="js/modernizr.js"></script>
<link rel="stylesheet" type="text/css" href="css/ie.css" media="screen" />
<![endif]-->
</head>


<script> var app = angular.module('backoffice',['ui.bootstrap', 'FacebookProvider', 'ngStorage', 'moment-picker']); </script>

<body id="MainTMS" ng-app="backoffice" ng-controller="boHomeController" ng-init="moduleName='tms';" style="overflow:hidden"  onresize="reinitwindow()">

<div id='navbartms'>
<nav class="navbar navbar-default navbar-static-top bottom-border">
<div class="container-fluid">
<div class="navbar-header">
	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
		<span class="sr-only">Toggle navigation</span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
	</button>
</div>

  <div class="container">
	<div id="bs-example-navbar-collapse-1" class="navbar-collapse collapse" style='padding:10px 0 0 20px;'>
	  <ul class="nav navbar-nav" ng-if="logaction != 'login'">
	  	<li ng-if='!mobilephone'><a href="javascript:location.reload(true);"><img width="80px" src="../images/logo_w.png" alt="weeloy-best-restaurant-logo" /></a></li>
		<li>&nbsp; &nbsp; &nbsp; &nbsp;</li>
		<li ng-if="editmode !== 1 && defaultLayout !=='' && runonlymode !== 99">
		  <button class="btn btn-primary dropdown-toggle" type="button"  data-toggle="dropdown" style='font-size:14px;'>RUN <span class="caret"></span></button>
		  <ul class="dropdown-menu">
			<li><a href ng-click="lyrun();">Start</a></li>
			<li ng-if="editmode === 0" class="divider"></li>
			<li ng-if="editmode === 0" ><a href ng-click="lybooking(0);">Booking View</a></li>
			<li ng-if="editmode === 0" ><a href ng-click="lytimeline(1);">Timeline View</a></li>
			<li ng-if="editmode === 0" class="divider"></li>
			<li ng-if="showlayout && indexLayout !== -1"><a href ng-click="viewerconnect.toggle(0, 0);">{{ viewerconnect.obj[0].label[2][0] }}</a></li>
			<li ng-if="showlayout && indexLayout !== -1"><a href ng-click="viewerconnect.toggle(0, 2);">{{ viewerconnect.obj[0].label[2][2] }}</a></li>
			<li ng-if="editmode === 0" class="divider"></li>
			<li ng-if="editmode === 0" ><a href ng-click="lyattribution();">Set Attribution</a></li>
			<li ng-if="editmode === 0" ><a href ng-click="lyresetattribution();">Clear Attribution</a></li>
			<li ng-if="editmode === 0" ><a href ng-click="lysaveattribution();">Save configuration</a></li>
			<li ng-if="editmode === 0" class="divider"></li>
			<li ng-if="editmode === 0" ><a href ng-click="refreshbooking();">Refresh Booking View</a></li>
			<li ng-if="editmode === 0" ><a href ng-click="lypreferences('Bookings');">Booking Display</a></li>
			<li ng-if="editmode === 0" ><a href ng-click="lypreferences('Actions');">Actions/Confirmations</a></li>
			<li ng-if="editmode === 0 && captainflg === 1 && layoutcaptain === 1"><a href ng-click="lypreferences('Captains');">Manage Captains</a></li>
			<li ng-if="editmode === 0" class="divider"></li>
			<li ng-if="editmode === 0" ><a href ng-click="lyshowdayreport(1);">Show the Lunch Report</a></li>
			<li ng-if="editmode === 0" ><a href ng-click="lyshowdayreport(2);">Show the Dinner Report</a></li>
			<!--<li ng-if="editmode === 0" ><a href ng-click="lyanalytic(1);">Show Analytics</a></li> -->
			<li ng-if="editmode === 0 && runonlymode !== 1" class="divider"></li>
			<li ng-if="editmode === 0 && runonlymode !== 1" ><a href ng-click="restartedit();">Edit Mode</a></li>
		  </ul>
		</li>
		<li ng-if="editmode !== 1 && defaultLayout !==''">&nbsp; &nbsp; &nbsp; &nbsp;</li>
		<li class="dropdown" ng-if="editmode !== 0 && editmode !== 99">
    	  <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" style='font-size:14px;'>LAYOUT <span class="caret"></span></button>
		  <ul class="dropdown-menu">
			<li><a href ng-click="lyedit();">Edit Mode</a></li>
			<li ng-if="editmode === 1" class="divider"></li>
			<li ng-if="editmode === 1"><a href ng-click="lynewLayout();">New Layout</a></li>
			<li ng-if="floordata.length > 0" class="dropdown-submenu"><a href tabindex="-1" >Set Floor</a>
			    <ul class="dropdown-menu scrollable-menu">
                  <li ng-repeat="y in floordata track by $index"><a tabindex="-1" ng-click="lysetfloor(y);" ng-class="{ 'classcyan': y === floorname } " style="cursor:pointer;">{{y}}</a></li>
                </ul>
			</li>
			<li ng-if="editmode === 1 && layoutdata.length > 0" class="dropdown-submenu"><a href tabindex="-1" >Read</a>
			    <ul class="dropdown-menu scrollable-menu">
                  <li ng-repeat="y in layoutdata track by $index"><a tabindex="-1" ng-click="lyselectLayout(y.name);" ng-class="{ 'classcyan': y.name === nameLayout }" style="cursor:pointer;">{{y.name}}</a></li>
                </ul>
			</li>
			<li ng-if="editmode === 1 && indexLayout !== -1" class="dropdown-submenu"><a href tabindex="-1" >Cmd</a>
			    <ul class="dropdown-menu">
                  <li ng-repeat="y in layoutcmd track by $index"><a tabindex="-1" ng-click="lyLayoutcmd(y.action);" style="cursor:pointer;"><table><tr><td width='120px'>{{y.action}}</td><td>{{y.keyboard}}</td></tr></table></a></li>
                </ul>
			</li>
			<li ng-if="showlayout"><a href ng-click="lydefault();">Set as default</a></li>
			<li ng-if="showlayout && indexLayout !== -1 && captainflg === 1"><a href ng-click="lysetcaptains();">Set Captains</a></li>
			<li ng-if="showlayout && indexLayout !== -1 && captainflg === 1"><a href ng-click="lyscheduler();">Scheduler</a></li>
			<li ng-if="showlayout" class="divider"></li>
			<li ng-if="showlayout"><a href ng-click="lysave();">Save</a></li>
			<li ng-if="showlayout && indexLayout !== -1"><a href ng-click="lyrename();">Rename</a></li>
			<li ng-if="showlayout && indexLayout !== -1"><a href ng-click="lyduplicate();">Duplicate</a></li>
			<li ng-if="showlayout && indexLayout !== -1"><a href ng-click="lydeleteLayout();">Delete</a></li>
			<li ng-if="showlayout" class="divider"></li>
			<li ng-if="showlayout && indexLayout !== -1"><a href ng-click="viewerconnect.toggle(0, 0);">{{ viewerconnect.obj[0].label[2][0] }}</a></li>
			<li ng-if="showlayout && indexLayout !== -1"><a href ng-click="lyclean();">Clean</a></li>
			<li ng-if="showlayout && indexLayout !== -1 && MFflg" class="divider"></li>
			<li ng-if="showlayout && indexLayout !== -1 && MFflg">&nbsp; &nbsp; &nbsp;   Multiple Floor </li>
			<li ng-if="showlayout && indexLayout !== -1 && MFflg"><a href ng-click="lynewMFloor();">New</a></li>
			<li ng-if="showlayout && indexLayout !== -1 && MFflg && multiplefloor.length > 0" class="dropdown-submenu"><a href tabindex="-1">Edit</a>
			    <ul class="dropdown-menu scrollable-menu">
                <li ng-repeat="y in multiplefloor track by $index"><a tabindex="-1" ng-click="lyeditMFloor(y.name);" style="cursor:pointer;">{{y.name}}</a></li>			
                </ul>
			</li>
			<li ng-if="showlayout" class="divider"></li>
			<li ng-if="showlayout"><a href ng-click="restartrun();">Run</a></li>
		  </ul>
		</li>
		</li>
		<li>&nbsp; &nbsp; &nbsp; &nbsp;</li>
		<li style="margin-bottom:10px;">
		<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" style='font-size:14px;'>HELP <span class="caret"></span></button>
		  <ul class="dropdown-menu">
			<li ng-repeat="t in helpservice.labels"><a href ng-click="helpservice.showhelp(t);">{{ t }} </a></li>
	  	</ul>
	  	</li>
	  	<li ng-if="shortcuts && !mobilephone && editmode === 0">
	  		<table style='font-size:xx-small;margin-left:10px;'><tr>
			<td align='center'>
				&nbsp; <button type="button" class="btn btn-default" ng-click="refreshbooking();"><i class="glyphicon glyphicon-transfer"></i></button>&nbsp; <br>refresh resa
			</td>
			<td align='center' ng-if="nofullcc === false">
				&nbsp; <button  type="button" class="btn btn-default" ng-click="addbooking(0)"><i class="glyphicon glyphicon-earphone"></i></button>&nbsp; <br>callcenter
			</td>
			<td align='center'>
				&nbsp; <button  type="button" class="btn btn-default" ng-click="addbooking(17)"><i class="glyphicon glyphicon-phone-alt"></i></button>&nbsp; <br>walkin
			</td>
			<td align='center'>
				&nbsp; <button type="button" class="btn btn-default" ng-click="lybooking(0);"><i class="glyphicon glyphicon-certificate"></i></button>&nbsp; <br>bookview
			</td>
			<td align='center' ng-if="waitinglist === true">
				&nbsp; <button  type="button" class="btn btn-default" ng-click="lywaitingview()"><i class="glyphicon glyphicon-hourglass"></i></button>&nbsp; <br>waitingview
			</td>
			<td align='center'>
				&nbsp; <button  type="button" class="btn btn-default" ng-click="lytimeline(1);"><i class="glyphicon glyphicon-align-left"></i></button>&nbsp; <br>timeview
			</td>
			<td align='center' ng-if="icicle.mode">
				&nbsp; <button  type="button" class="btn btn-default" ng-click="lyicicle();"><i class="fa fa-align-left fa-rotate-90"></i></button>&nbsp; <br>Icicleview
			</div>
			<td align='center' ng-if="subindexLayout > -1">
				&nbsp; <button  type="button" class="btn btn-default" ng-click="nextfloor();"><i class="glyphicon glyphicon-expand"></i></button>&nbsp; <br>nextfloor
			</td>
			</tr></table>
		</li>
		
	  </ul>
 	  <ul class="nav navbar-nav navbar-right">
 	  <table style="font-size:12px;"><tr>
		  <td valign='top'><a href ng-click="loginout(logaction);"><span class="glyphicon glyphicon-log-{{logactionpict}}"></span><strong>{{logaction}}</strong></a></li>
		  <td valign='top'>&nbsp; &nbsp; &nbsp; &nbsp;</li>
		  <td ng-if="!mobilephone" valign='top'id="sessiontime" class="small"></li>
 	  	  <td ng-if="!mobilephone" valign='top'>&nbsp; &nbsp; &nbsp; &nbsp;</li>
 	  	  <td ng-if="imglogo != '' && !mobilephone" valign='top'><img ng-src='{{imglogo}}' width='50'><span ng-if="restotitle != ''" style="font-size:11px;"><br/>{{restotitle}}</span></li>
	  </tr></table>
	  </ul>
	</div><!--/.nav-collapse -->
  </div>
  </div>
</nav>

<div id="threewaywindow" style="background-color:white;">
    <div id="left">
 	<div id="leftup"><ng-include src="statsview"> </ng-include></div>
 	<div id="dividerH"></div>
      <div ng-if='tablette' id="draggableHandle" class="ui-widget-content">
        <p align='right'><img src='icons/arrow-circle-all.svg' width='30'></p>
      </div>
 	<div id="booking" style='overflow-x: hidden;overflow-y:scroll;max-height: 100vh;-webkit-overflow-scrolling:touch;background-color:white;'>
   	<div id='mydisplay' ng-class="{classbgblack: blackbg, classbgwhite: !blackbg }"><ng-include src="bookingview"> </ng-include></div>
<!--   	<div style='height:300px;'></div> -->
   	</div>
    </div>

    <div id="right" style='overflow-y:scroll+;max-height: 100vh;-webkit-overflow-scrolling:touch;background-color:white;'>
		<div id='raphael'></div>
			<content-item ng-repeat="item in translatedata" content="item" myTemplates="templateData"></content-item>
	</div>
    
    </div>
  	<div ng-include="btemplate" ng-if="logaction==='login'"></div>
</div>

<div id="fb-root"></div>

<script type="text/javascript" src="../js/raphael.js"></script>
<script type="text/javascript" src="touchUI.js"></script>
<script type="text/javascript" src="../js/facebookProvider.js"></script>
<script type="text/javascript" src="../js/facebookRun.js"></script>
<script type="text/javascript" src="../js/loginService.js?2"></script>
<script type="text/javascript" src="../js/ngStorage.min.js"></script>
<script type="text/javascript" src="../js/formControl.js"></script>
<script type="text/javascript" src="../modules/rltimeio/ortc.js"></script>

<?php 

printf("<script>");

printf("var opsyswindow = %s;", ($windows) ? 'true': 'false');
printf("var tmsversion = '%s';", $tmsversion);
printf("var tmsUI = 1;");
echo "var cookiename = '" . getCookiename('translation') . "';\n"; /* connected to the alog.js script file */ 
printf("var tablette = %s;", ($tablette) ? 'true': 'false');
printf("var mobilephone = %s;", ($browser->isMobile()) ? 'true': 'false');

printf("</script>");

printf("<script type='text/javascript' src='../js/alog.js?tmsv=%s'></script>", $tmsversion);
printf("<script type='text/javascript' src='../backoffice/inc/libService.js?tmsv=%s'></script>", $tmsversion);
printf("<script type='text/javascript' src='../backoffice/inc/profilelib.js?tmsv=%s'></script>", $tmsversion);
printf("<script type='text/javascript' src='weeloy_tms.min.js?tmsv=%s'></script>", $tmsversion);
?>

<script type="text/javascript" src="cp/cpcolors.js"></script>
<script type="text/javascript" src="cp/jqColorPicker.js"></script>
<script type="text/javascript" src="cp/cpstart.js"></script>

<?
// some template works if direct included in the file !!!
//  	<ng-include="'tmsTemplates.html'"></ng-include>

include("tmsTemplates.html");
include("../backoffice/profileTemplates.html");

?>

<script type="text/ng-template" id="emptyview.html"><img src="../images/admin/bo_slider_empty.gif"></script>
<script type="text/ng-template" id="timelineview.html"> <div id="timeline_drawing" style='background-color:black;' loadingDiv></div> </script>
<script type="text/ng-template" id="punchchartview.html"> <div id="punch_chart" style='background-color:black;'></div> </script>

<div ng-include="'../backoffice/inc/myModalModif.html'"></div>
<div ng-include="'aIcicle.html'"></div>
<div ng-include="'acaptains.html'"></div>
<div ng-include="'ascheduler.html'"></div>

<script type="text/ng-template" id="embededlogin.html">
<div class="modal-body">
<form ng-submit="modalOptions.submit(modalOptions.mydata)">
<iframe id='loginFrame' src='../alogin/index.php' width='100%' height='350' frameBorder='0' margin='0'></iframe>
<div>
</script>

<script>

var imagesRes = [];
var iconsRes = [];
loginTitleTormat = "small";


<?php 

function GetElement($dir) {

	$fNames = array();
	$handle = opendir($dir);
	while($file = readdir($handle))
		if(is_dir($dir . $file) == false) {
			$parts = pathinfo($dir . $file);
			if($parts['extension'] == "svg" || $parts['extension'] == "jpg")
				$fNames[] = $file;
			}
	closedir($handle);
	return $fNames;
}

$elementAr = GetElement("images/");
natsort($elementAr);

$sep = "";
echo "var elementsAr = [";
foreach($elementAr as $name) {
	echo $sep . " '" . $name . "'";
	$sep = ",";
	}
echo "];";

$iconsAr = GetElement("icons/");
$sep = "";
echo "\nvar iconsAr = [";
foreach($iconsAr as $name) {
	echo $sep . " '" . $name . "'";
	$sep = ",";
	}
echo "];";

?>

$(document).ready(function() { 
max = elementsAr.length; for(i = 0; i < max; i++) { imagesRes[i] = new Image(); imagesRes[i].src = 'images/' + elementsAr[i]; } 
max = iconsAr.length; for(i = 0; i < max; i++) { iconsRes[i] = new Image(); iconsRes[i].src = 'icons/' + iconsAr[i]; } 
$('[data-toggle="popover"]').popover();
}); 

var threewaywindow = document.getElementById('threewaywindow');
var left = document.getElementById('left');
var leftup = document.getElementById('leftup');
var right = document.getElementById('right');
var dividerH = document.getElementById('dividerH');
var booking = document.getElementById('booking');
var divider = document.createElement('div');
divider.id = 'divider';
threewaywindow.appendChild(divider);

function updateVDivision() {
	if(leftPercent > 90) leftPercent = 90;
	if(leftPercent < 10) leftPercent = 10;
	
    divider.style.left = leftPercent + '%';
    left.style.width = leftPercent + '%';
    right.style.width = (100 - leftPercent) + '%';
}

function updateHDivision() {
	if(leftUpPercent > 30) leftUpPercent = 30;
	if(leftUpPercent < 5) leftUpPercent = 5;

   	dividerH.style.bottom = leftUpPercent + '%';
   	leftup.style.width = leftUpPercent + '%';
   	booking.style.width = (100 - leftUpPercent) + '%';
}

divider.addEventListener('mousedown', function(e) {
    e.preventDefault();
    var lastX = e.pageX;
    document.documentElement.className += ' dragging';
    document.documentElement.addEventListener('mousemove', moveHandler, true);
    document.documentElement.addEventListener('mouseup', upHandler, true);
    function moveHandler(e) {
        e.preventDefault();
        e.stopPropagation();
        var deltaX = e.pageX - lastX;
        lastX = e.pageX;
        leftPercent += deltaX / parseFloat(document.defaultView.getComputedStyle(threewaywindow).width) * 100;
		if(leftPercent < 6) leftPercent = 6;
		else if(leftPercent > 95) leftPercent = 95;
        updateVDivision();
    }
    function upHandler(e) {
        e.preventDefault();
        e.stopPropagation();
        document.documentElement.className = document.documentElement.className.replace(/dragging/, '');
        document.documentElement.removeEventListener('mousemove', moveHandler, true);
        document.documentElement.removeEventListener('mouseup', upHandler, true);
    }
}, false);


dividerH.addEventListener('mousedown', function(e) {
    e.preventDefault();
    document.documentElement.className += ' draggingd';
    document.documentElement.addEventListener('mousemove', moveHHandler, true);
    document.documentElement.addEventListener('mouseup', upHHandler, true);
    function moveHHandler(e) {
        e.preventDefault();
        e.stopPropagation();
        if(e.pageY-offsettop > 25 && e.pageY-offsettop < statsmaxheight) 
        	openLeftup( (e.pageY-offsettop) - parseInt($('#leftup').css("height")) );
	        //$('#leftup').css("height",e.pageY-offsettop);
        }
    function upHHandler(e) {
        e.preventDefault();
        e.stopPropagation();
        document.documentElement.className = document.documentElement.className.replace(/draggingd/, '');
        document.documentElement.removeEventListener('mousemove', moveHHandler, true);
        document.documentElement.removeEventListener('mouseup', upHHandler, true);
    }
}, false);


function reinitwindow() {
	var value = parseInt($('body').css("height")) - parseInt($('#navbartms').css("height"));
	$('#right').css("height", value+'px');
	value -= parseInt($('#leftup').css("height"));
	$('#booking').css("height", value+'px');
}


function openLeftup(extraHeight) {
	var value, Height = parseInt($('#leftup').css("height")) + extraHeight;

	if(windowinitsize == -1) {
		value = parseInt($('#right').css("height")) - parseInt($('#navbartms').css("height"));
		$('#right').css("height", value+'px');
		windowinitsize = 1;
		}
		
	if(Height > statsmaxheight) {
		Height = statsmaxheight;
		}
		
	$('#leftup').css("height", Height+'px');
	value = parseInt($('#right').css("height")) - Height;	
	$('#booking').css("height", value+'px');
	}

function moveLeftup(Height) {
	var value, total, extraHeight = Height - tmsDivTop;
	
	total = parseInt($('#leftup').css("height")) + extraHeight;
	if(total > statsmaxheight) return;
	
	$('#leftup').css("height", total+'px');
	value = parseInt($('#right').css("height")) - total;	
	$('#booking').css("height", value+'px');
	tmsDivTop = Height;
	}

function moveLeftUpDown(Height) {
	var value;

	if(Height > statsmaxheight) Height = statsmaxheight;
	if(Height < 50) Height = 50;
	
	$('#leftup').css("height", Height+'px');
	value = parseInt($('#right').css("height")) - Height;	
	$('#booking').css("height", value+'px');
	
	tmsDivTop = 0;
	}
	
function moveDownright(Right) {
	if(tmsInitDivRight === 0) 
		tmsInitDivRight = 2 * parseInt($('#leftup').css("width"));

	var extraRight = Right - tmsDivRight;
	leftPercent = ((parseInt($('#leftup').css("width")) + extraRight) / tmsInitDivRight) * 100;
	if(leftPercent < 6) leftPercent = 6;
	else if(leftPercent > 90) leftPercent = 90;
	updateVDivision();		

	tmsDivRight = Right;
	}

function managewindow(mode) {
	if(mode === "right") { leftPercent = (leftPercent < 45) ? 50 : 90; updateVDivision(); tmsInitDivRight = 0; }
	else if(mode === "left")  { leftPercent = (leftPercent < 50) ? 15 : 45; updateVDivision(); tmsInitDivRight = 0; }
	else if(mode === "up")  moveLeftUpDown(20);
	else if(mode === "down")  moveLeftUpDown(300);
	}
		
//$('#leftup').html("<p style='font-size:7px;'>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>");

function windinit3way() {
	leftPercent = 50;
	leftUpPercent = 10;
	windowinitsize = -1;
	statsmaxheight = 400;
	tmsDivTop = 0;
	tmsDivRight = 0;

	offsettop = $('#threewaywindow').offset().top;
	$('#leftup').css("height", offsettop - 30);
	tmsInitDivRight = parseFloat(document.defaultView.getComputedStyle(threewaywindow).width);

	updateVDivision();
}

	
windinit3way();

if(tablette)
	$(window).bind('orientationchange', function(event) { 
		console.log('new orientation:' + event.orientation); 
		windinit3way();
		});
		
$(function() {
  $( "#draggableHandle" ).draggable({
	drag: function( event, ui ) { moveLeftup(ui.position.top); moveDownright(ui.position.left); ui.position.top = ui.position.left = 0; },
	stop: function( event, ui ) { tmsDivTop = tmsDivRight = 0; }
  });
});

function triggermodifyprofile(token, resto, id, email, phone) { 
	var scope = angular.element(document.getElementById("MainTMS")).scope();
	scope.$apply(function () { scope.updateCustomProfile(token, resto, id, email, phone); });
    }

</script>

</body>
</html>
