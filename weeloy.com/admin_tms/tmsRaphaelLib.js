/*
 *  ┌─────────────────────────────────────────────┐ 
 *  │ TMS 1.0 - JavaScript Vector Library             │ 
 *  ├─────────────────────────────────────────────┤ 
 *  │ Copyright © 2015 Weeloy. All rights reserved.   │ 
 *  ├─────────────────────────────────────────────┤ 
 *  │ Company: https://www.weeloy.com				  │ 
 *  ├─────────────────────────────────────────────┤ 
 *  │ Author Richard Kefs							  │ 
 *  └─────────────────────────────────────────────┘ 
 *
*/

Array.prototype.fdBooking = function(booking) {
	if(this === null) return -1;

	var i, limit = this.length;
	for(i = 0; i < limit; i++) 
		if(this[i].booking === booking || this[i].index === booking)
			return i;
	return -1;
};
	
Number.prototype.myPrecision = function() {
	var val = this;
	var val1;
	var n = 0;
	
	if(typeof val !== Number)
		val = parseFloat(val);
		
	if(arguments.length > 0)
		n = arguments[0];

	val1 = val.toFixed(n);
	if(val1 === Math.floor(val).toFixed(n))
		return Math.floor(val).toFixed(0);
	return val1;
};

function zeroplace(name) {
	var k, pattern = ["plant", "separator", "floor", "trash"];

	for(k = 0; k < pattern.length; k++) 
		if(name.indexOf(pattern[k]) >= 0)
			return true;
			
	return false;					
}

function turnMeBlue() {
	var rpcolor = (rpcolor === "#0793bb") ? 'red' : '#0793bb';
	this.attr("fill", rpcolor);     
}

function getMatrix(tbl) {
	var oo = tbl.obj, w = tbl.width, h = tbl.height, x, y, bb;
	var newdata = [ -1, -1, -1, -1, -1, -1, -1, -1 ];

	if(tbl.type === "c") {
		bb = tbl.obj.getBBox();
		tbl.width = bb.width;
		tbl.height = bb.height;
		w = tbl.width, h = tbl.height;
		x = bb.x;
		y = bb.y;
		return [x, y, x + w, y, x + w, y + h, x, y + h].slice(0); 			
		}

	x = oo.attr("x");
	y = oo.attr("y");
				
	newdata[0] = parseFloat(oo.matrix.x(x, y).myPrecision(3));
	newdata[1] = parseFloat(oo.matrix.y(x, y).myPrecision(3));
	newdata[2] = parseFloat(oo.matrix.x(x + w, y).myPrecision(3));
	newdata[3] = parseFloat(oo.matrix.y(x + w, y).myPrecision(3));
	newdata[4] = parseFloat(oo.matrix.x(x + w, y + h).myPrecision(3));
	newdata[5] = parseFloat(oo.matrix.y(x + w, y + h).myPrecision(3));
	newdata[6] = parseFloat(oo.matrix.x(x, y + h).myPrecision(3));
	newdata[7] = parseFloat(oo.matrix.y(x, y + h).myPrecision(3));
	
	return newdata.slice(0);
}

function NGon(x, y, N, side, angle) {

	var path = "",
		c, temp_x, temp_y, theta;

	for (c = 0; c <= N; c += 1) {
		theta = (c + 0.5) / N * 2 * Math.PI;
		temp_x = x + Math.cos(theta) * side;
		temp_y = y + Math.sin(theta) * side;
		path += (c === 0 ? "M" : "L") + temp_x + "," + temp_y;
	}
	return path;
}
		
var getBox = function(bxy, type, tbl, paper) {	
		var i, path, angle, oo;	
		if(type === 0 || type === 1) {	// rectangle around image
			oo = paper.rect(bxy[0], bxy[1], tbl.width, tbl.height).attr("stroke-width", 1);
			if(tbl.angle !== 0)
				oo.attr({transform: '...R'+tbl.angle+','+bxy[0]+','+bxy[1] });
				
			if(type === 0)
				oo.attr("stroke", 'white').attr({"stroke-dasharray":"-"});
			else oo.attr("stroke", 'pink');
			oo.attr("cursor", "move");	
			return oo;
			}
		
		if(type === 2) {	// grow circle around image, at the 3rd corner
			oo = paper.circle(bxy[4], bxy[5], 5).attr("fill", 'red');
			oo.cx = bxy[4];
			oo.cy = bxy[5];
			oo.attr("cursor", "nwse-resize");	
			return oo;
			}

		if(type === 3) {	// rotate circle around image, at the 1st corner
			oo = paper.circle(bxy[0], bxy[1], 5).attr("fill", 'yellow');
			oo.cx = bxy[0];
			oo.cy = bxy[1];
			oo.attr("cursor", "alias");	
			return oo;
			}
			
		if(type === 4) {
			path = 'M ' + bxy[0] + ',' + bxy[1];
			for(i = 2; i < 8; i += 2)
				path += " L " + bxy[i] + ',' + bxy[i+1];
			path += " L " + bxy[0] + ',' + bxy[1];
			oo = paper.path(path).attr("stroke-width", 1).attr("stroke", 'pink');			
			oo.attr("cursor", "move");	
			return oo;
			}
		return null;
		};

function Groupselection(Qo, paper) {

	var ctbl = null;
	var data = [];
	var square = [];
	
	return {
		setSelectedTbl: function(atbl) {
			ctbl = atbl;
			},
			
		getSelectedTbl: function() {
			return ctbl;
			},
						
		getSelectedId: function() {
			return (ctbl) ? ctbl.id : -1 ;
			},
						
		getSelectedObj: function() {
			var i, tbl, retAr = [];
			for(i = 0; i < data.length; i++) {
				tbl = data[i];
				retAr.push(tbl.obj);
				retAr.push(tbl.perimeter);
				}
			return retAr;
			},
			
		getAllSelectTbl: function() {
			return data.slice(0);
			},
						
		getnbSelectTbl: function() {
			return data.length;
			},
						
		squareSelect: function(x, y, w, h) {
			var i, tbl, tmpAr = [];
			
			Qo.resetSquare();
			if(typeof x === "undefined") { x = square[0]; y = square[1]; w = square[2]; h = square[3]; }
			for(i = 0; i < Qo.data.length; i++) {
				tbl = Qo.data[i];
				if((tbl.cx >= x && tbl.cx <= x + w) && (tbl.cy >= y && tbl.cy <= y + h)) {
					tbl.perimeter = getBox(tbl.bxy, 0, tbl, paper);
					tmpAr.push(tbl);
					}
				}
			if(tmpAr.length <= 0)
				return;
			
			square = [x, y, w, h];
			for(i = 0; i < tmpAr.length; i++) 
				this.insert(tmpAr[i]);
			},
		
		moveSelect: function(dx, dy) {
			if(data.length >= 1) {
				var i;
				for(i = 0; i < data.length; i++) {
					if(data[i].obj) data[i].obj.attr({ transform: '...T'+dx+','+dy} );
					if(data[i].perimeter) data[i].perimeter.attr({ transform: '...T'+dx+','+dy} );
					}
				}
			else if(ctbl) {
				if(ctbl.obj) ctbl.obj.attr({ transform: '...T'+dx+','+dy} );
				if(ctbl.perimeter) ctbl.perimeter.attr({ transform: '...T'+dx+','+dy} );
				}
			},
			
		alignSelect: function(direction) {
			var i, oo, dz, trans, tmpArray=[];
				
			for(i = 0; i < data.length; i++) {
				oo = data[i].obj.getBBox();
				data[i].cx = oo.x + (oo.width/2);
				data[i].cy = oo.y + (oo.height/2);
				data[i].cz = (direction === 'v') ? data[i].cx : data[i].cy;
				tmpArray.push(data[i]);
				}
								
			tmpArray.sort(function(a, b) { return (a.cz - b.cz); });
			for(i = 1; i < tmpArray.length; i++) {
				dz = tmpArray[0].cz - tmpArray[i].cz;
 				trans = (direction === 'v') ? '...T'+dz+','+0 : '...T0,'+dz;
 				tmpArray[i].obj.attr({ transform: trans} );
				tmpArray[i].perimeter.attr({ transform: trans} );
				}

			Qo.connectgrpShow();
			this.clear(); 
			this.squareSelect();
			},
			
		clear: function() {
			var i;
			for(i = data.length - 1; i >= 0 ; i--)
				this.erase(data[i], i);
			},
		
		search: function(id) {
			for(var i = 0; i < data.length; i++)
				if(data[i].id === id) return i;
			return -1;
			},
			
		insert: function(tbl) {
			if(this.search(tbl.id) >= 0)
				return;
			
			data.push(tbl);
			Qo.tblselect(tbl);					
			},
			
		erase: function(tbl, ind) {
			data.splice(ind, 1);
			Qo.tbldeselect(tbl);					
			},
		};
}

function Createconnection(tbl, paper, myalert) {

	var id = tbl.id;
	var conToTbl = [];
	var conFromTbl = [];
	var link = [];
	var data = [];
	
	return {
	
		mydata: function(field, value) {
			if(arguments.length === 1)
				return (typeof data[field] !== 'undefined') ? data[field] : "";
			data[field] = value;
			return value;
			},
			
		getstate: function(queueObj) {
			var i, action = conToTbl.length;
			for(i = 0; i < conToTbl.length; i++)
				action += "," + conToTbl[i].id;
			action += "\nfrom: " +conFromTbl.length;
			for(i = 0; i < conFromTbl.length; i++)
				action += "," + conFromTbl[i].id;
			return action;
			},
			
		checkCid: function(id) {
			var i;
			for(i = 0; i < conToTbl.length; i++)
				if(conToTbl[i].id === id)
					return i;
			return -1;
			},
		
		addFrom: function(aTbl) {
			conFromTbl.push(aTbl);
			},
		
		getToConnection: function() {
			var i, sep, str = '';
			for(i = 0, sep = ''; i < conToTbl.length; i++, sep=',')
				str += sep + conToTbl[i].label;
			return str;
			},
					
		getFromConnection: function() {
			var i, sep, str = '';
			for(i = 0, sep = ''; i < conFromTbl.length; i++, sep=',')
				str += sep + conFromTbl[i].label;
			return str;
			},
		
		isconnect: function(toTbl) {
			if(typeof toTbl === 'undefined') return false;				
			return (this.checkCid(toTbl.id) >= 0 || toTbl.conObj.checkCid(id) >= 0);
			},
						
		connect: function(toTbl) {
			if(typeof toTbl === 'undefined') return this;

			if(toTbl.parent.fixedviewmode === 1) 
				return this;
				
			if(this.checkCid(toTbl.id) >= 0 || toTbl.conObj.checkCid(id) >= 0) {
				myalert("Objects are alreayd connected");
				return this;
				}
			conToTbl.push(toTbl);
			toTbl.conObj.addFrom(tbl);
			if(tbl.parent.viewlinks)
				link.push(paper.connection(tbl.obj, toTbl.obj, "#fff", "#fff|5"));
			return this;
			},
		
		deconnect: function(toTbl) {
			var ind = -1, conObj = null;
			
			 if((ind = this.checkCid(toTbl.id)) >= 0) {
			 	this.deconnectTo(ind);
			 	toTbl.conObj.deconnectFrom(tbl.id);
			 	}
			 else if((ind = toTbl.conObj.checkCid(id)) >= 0) {
			 	toTbl.conObj.deconnectTo(ind);
			 	this.deconnectFrom(toTbl.id);
			 	}	
			else myalert("Objects are NOT connected");
				
			return this;		
			},
			
		deconnectTo: function(ind) {			
			link[ind].remove();
			link[ind] = null;
			link.splice(ind, 1);
			conToTbl.splice(ind, 1);
			return this;
			},
			
		deconnectFrom: function(id) {
			var ind;
			for(ind = 0; ind < conFromTbl.length; ind++)
				if(conFromTbl[ind].id === id)
					break;
			if(ind < conFromTbl.length) 
				conFromTbl.splice(ind, 1);
			else console.log('ERROR CONNECTION. No Origin ' + id);
			return this;
			},

		hide: function() {
			var i;
			for(i = 0; i < link.length; i++)
				link[i].hide();
			for(i = 0; i < conFromTbl.length; i++)
				conFromTbl[i].conObj.hideid(tbl.id);
			return this;
			},
			
		hideid: function(id) {
			var ind;
			
			if((ind = this.checkCid(id)) < 0)
				return;
			link[ind].hide();
			},

		//since everynode will be call for connection, don't worry for the from part
		partialshow	: function(tblIdAr) {
			var i, aTbl;
			for(i = conToTbl.length - 1; i >= 0; i--) {
				aTbl = conToTbl[i];
				this.deconnectTo(i);
				aTbl.conObj.deconnectFrom(tbl.id);
				this.connect(aTbl);
				}

			// reconnect from the fom, if the from not included in the group select
			// first get all the id

			if(tblIdAr.length <= 0) return;
			
			for(i = conFromTbl.length - 1; i >= 0; i--) {
				aTbl = conFromTbl[i];
				if(tblIdAr.indexOf(aTbl.id) === -1) {
					aTbl.conObj.deconnectTo(aTbl.conObj.checkCid(tbl.id));
					this.deconnectFrom(aTbl.id);
					aTbl.conObj.connect(tbl);
					}
				}
			},
					
		show: function() {
			var i, aTbl;
			for(i = conToTbl.length - 1; i >= 0; i--) {
				aTbl = conToTbl[i];
				this.deconnectTo(i);
				aTbl.conObj.deconnectFrom(tbl.id);
				this.connect(aTbl);
				}
				
			for(i = conFromTbl.length - 1; i >= 0; i--) {
				aTbl = conFromTbl[i];
				aTbl.conObj.deconnectTo(aTbl.conObj.checkCid(tbl.id));
				this.deconnectFrom(aTbl.id);
				aTbl.conObj.connect(tbl);
				}
			},
			
		showid: function(id) {
			var ind;
			
			if((ind = this.checkCid(id)) < 0)
				return;
			link[ind].show();
			},
			
		deconnectAll: function() {
			for(var i = 0; i < link.length; i++)
				link[i].remove();
			conToTbl = [];
			conFromTbl = [];
			},
						
		clear: function() {
			var i;
			for(i = 0; i < link.length; i++)
				link[i].remove();
				
			for(i = 0; i < conFromTbl.length; i++)
				conFromTbl[i].conObj.eraseid(tbl.id);
				
			conFromTbl = [];
			},

		eraseid: function(id) {
			var ind;
			if((ind = this.checkCid(id)) < 0)
				return;
			link[ind].remove();
			link.splice(ind, 1);
			conToTbl.splice(ind, 1);
			}

		};
}


function Createstate(tbl, etat, colorstate, paper, toggleImage) {
	var state = etat;
	var color = colorstate[state] || "black";
	var anim, latecomer, lateseater, signalerror, animstate = "";
	var mode = ""; // reassignResa or connect
	var papier;
	var svfconsole = null;
	var hlight = -1;
	var capt, duree, stoptime, seattime, flgerror;
	var conf='not-attributed', name='not-attributed', ncover=0, nxtbkg="";
	var q0 = tbl.parent;
	var trigevent = q0.getaPref('triggerevent');
	return {
		setresa: function(confirmation, guest, npers, next, hval, captain, duration, cleartime, seated, error) {
			var oo = tbl.obj.display;
			conf = confirmation;
			name = (typeof guest === "string") ? guest.substr(0, 6).trim() : "";
			ncover = npers;
			nxtbkg = next;
			capt = captain;
			duree = duration;
			stoptime = cleartime;
			seattime = seated;
			flgerror = error;

			if(q0.fixedviewmode === 1)
				return;
				
			if(typeof hval === 'number') hlight = hval;

			if(oo) {
				if(hlight === 1)
					oo.attr({ 'cursor': 'copy' });
				else this.setTlabelcursor();		
				if(npers > 0 && q0.editmode === 0) {
					oo.hover(function(e) { var tbl = this.data("wraper"); q0.tblHover(e, tbl, conf); }); 
					}
				}
			},
		
		sethlight: function(val) {
			hlight = val;
			},
			
		gethlight: function() {
			return hlight;
			},
				
		getresa: function() {
			return  [conf, name, ncover, nxtbkg, mode, capt, duree, stoptime, seattime ];
			},

		getstate: function() {
			return  colorstate[state] ? state : "";
			},

		setmode: function(val) {
			mode = (mode === val) ? "" : val;
			this.redraw();
			},
			
		showresa: function() {
			var pp, display, fontsize, text, font, corner, bgcolor, fontcolor, cursor, xsize, offx;
			
			if(svfconsole) { svfconsole[0].remove(); svfconsole[1].remove(); svfconsole.remove(); }
			svfconsole = paper.set();
			
			font = q0.fontstate;
			fontsize = 10;
			corner = 5;
			xsize = 80;
			offx = 35;
			cursor = "pointer";
			bgcolor = "white";
			fontcolor = "black";
			text = tbl.label+':'+name+':'+ncover;
			
			if(q0.fixedviewmode === 1 && tbl.fullbkg instanceof Array){
				var textAr = [];
				//fontsize = 9;
				font = q0.fontsmall;
				corner = 0;
				offx = 39;
				xsize = 120;
				tbl.fullbkg.forEach(function(oo) { textAr.push("[" + oo.npers + "]"); });
				text =  textAr.join(",");
				if(textAr.length > 0) {
					text =  textAr.length + "=" + text;
					switch(textAr.length) {
						case 1: bgcolor = "#ffccff"; break;
						case 2: bgcolor = "#ff4dff"; break;
						case 3: bgcolor = "#cc00cc"; fontcolor = "white"; break;
						default: bgcolor = "#660066"; fontcolor = "white"; break;
						}
					}
				}
			pp = paper.rect(tbl.cx - 40, tbl.cy - 30, xsize, 17, corner).attr("fill", bgcolor).attr({'cursor': cursor});
			display = paper.print(tbl.cx - offx, tbl.cy - 23, text, font, fontsize).attr({'fill': fontcolor, 'cursor': cursor});
			svfconsole.push(pp);
			svfconsole.push(display);
			svfconsole.data("wraper", tbl);
			
			if(q0.fixedviewmode === 1) {
				tbl.fullbkgdisplay = createFullBkg(tbl, paper, q0.fontstate);
				svfconsole.mouseover(function(e){ displayFullBkg(e, tbl, paper, q0.fontstate); });
				svfconsole.mouseout(function(e){ cleardisplayFullBkg(e, tbl); });
				svfconsole.dblclick(func_tochangestate(q0.scope, tbl));
				if(typeof flgerror  === "number" && flgerror === 1)
					this.signalerror();
				return this;
				}

			if(q0.tablette) {
				svfconsole.hover(function(e){ q0.triggerBooking(e, tbl, conf); });
				svfconsole.click(function(e){ q0.triggerBooking(e, tbl, conf); });
				svfconsole.dblclick(function(e){ q0.triggerBooking(e, tbl, conf); });
				}
			else {
				svfconsole.hover(function(e){ q0.tblHover(e, tbl, conf); });
				svfconsole.dblclick(function(e){ q0.triggerBooking(e, tbl, conf); });
				}
			},
		
		redraw: function() {
			this.showresa();
			if(hlight >= 0) 
				this.setbackgroundcolor(hlight);
			},
						
		front: function() {
			if(svfconsole) { svfconsole[0].toFront(); svfconsole[1].toFront(); }
			},
		
		setbackgroundcolor: function(val) {
			var c1, c2, cursor, oo = tbl.obj.display, cstate = q0.getselectedbooking("state");
			if(!svfconsole) 
				return this;
			if(q0.fixedviewmode === 1) 
				return this;
				
			hlight = val;
			if(hlight === 1 && mode === "") { c1 = "red"; c2 = "yellow"; }
			else if(hlight === 1 && mode === "connect") { c1 = "purple"; c2 = "yellow"; }
			else { c1 = "yellow"; c2 = "green"; mode = ""; }
			svfconsole[0].attr("fill", c1); 
			svfconsole[1].attr("fill", c2); 
			
			cursor = (hlight === 1 && cstate !== "no show") ? 'copy' : 'default';
			if(hlight === 1) {
				this.front();
				svfconsole.data("wraper", svfconsole);
				svfconsole.data("tbl", tbl);
				svfconsole.data("booking", conf);
				svfconsole.data("npers", ncover);
				svfconsole.drag(tbl.draglib.dragpsetmove, tbl.draglib.dragpsetstartlabel, tbl.draglib.dragpsetendObj);
				if(oo) oo.attr({ 'cursor': cursor });			
				}
			else if(oo) oo.attr({ 'cursor': cursor });
			return this;
			},
					
		setstate: function(etat) {	
			state = colorstate[etat] ? etat : 'clear';
			toggleImage(tbl, state);
			if(etat === 'clear') {
				conf='not-attributed'; name='not-attributed'; ncover=duree=0; nxtbkg=capt=stoptime="";
				this.erase();
				return this;
				}
			color = colorstate[state] || "black";
			return this;
			},
		
		show: function() {
			if(typeof colorstate[state] === 'undefined')
				return this;

			if(q0.fixedviewmode !== 1) 				
				if(papier === null)
					papier = this.create(color);
			},
			
		hide: function() {
			toggleImage(tbl, 'hide');
			if(papier) papier.hide();
			return this;
			},
									
		highlight: function() {
			if(typeof colorstate[state] === 'undefined')
				return this;
				
			toggleImage(tbl, state);
			if(papier) papier.remove();
			if(q0.fixedviewmode !== 1) 				
				if(papier = this.create(color))
					papier.animate(anim);
			return this;
			},
		
		triggeranim: function(mode) {
			if(trigevent === false)
				return;
			if(!mode || typeof mode !== "string" || ["", "latecomer", "lateseater"].indexOf(mode) < 0)
				return;			
			if(animstate !== "")
				papier.stop();
			animstate = mode;
			if(mode === "latecomer") papier.animate(latecomer);
			else if(mode === "lateseater") papier.animate(lateseater);
			},
			
		resume: function() {
			toggleImage(tbl, state);
			return this.highlight();
			},

		erase: function() {
			toggleImage(tbl, 'clear');
			if(tbl.fullbkgdisplay) tbl.fullbkgdisplay.remove();
			if(papier) papier.remove();
			if(svfconsole) svfconsole.remove();
			papier = null;
			svfconsole = null;
			state = 'none';
			this.setTlabelcursor();
			return this;
			},

		signalerror: function() {
			var sigerroranim = Raphael.animation({ "0%" : { r: 10, fill:'white' }, "30%": { r:2, fill: 'purple' }, "100%": { r:2 } }, 1000, "elastic").repeat("Infinity");
			papier = paper.circle(tbl.cx, tbl.cy+10, 5).attr("fill", color);
			papier.animate(sigerroranim);
			},
						
		create: function(color) {
			if(trigevent === false) 
				return;
			anim = Raphael.animation({ r:2, fill:color }, 2000, "elastic");
			latecomer = Raphael.animation({ "0%" : { r: 7, fill:'cyan' }, "30%": { r:2, fill: color }, "100%": { r:2 } }, 2000, "elastic").repeat("Infinity");
			lateseater = Raphael.animation({ "0%" : { r: 7, fill:'white' }, "30%": { r:2, fill: 'yellow' }, "100%": { r:2 } }, 2000, "elastic").repeat("Infinity");
			return paper.circle(tbl.cx, tbl.cy+10, 5).attr("fill", color);
			},
			
		setTlabelcursor: function() {		
			if(q0.fixedviewmode === 1) 
				return;

			var oo = tbl.obj.display, pref = q0.preferences;
			if(oo && !svfconsole) {
				var cursor, pax = q0.getselectedbooking("pax"), tbname = q0.getselectedbooking("tablename"), state = q0.getselectedbooking("state");
				if(typeof pref['unmatchtablesize'] === 'number' && pref['unmatchtablesize'] === 1) pax = 0; // don't check table size
				cursor = (tbname === "" && tbl.npers >= pax && state !== "no show" && conf === "not-attributed") ? 'copy' : 'default';
				oo.attr({ 'cursor': cursor });
				}
			}
		};
}

function func_tochangestate(scope, tbl) {
	return function() { scope.fixviewdbl(tbl.label); };
	}
	
function createFullBkg(tbl, paper, font) {
	var pp, fullbkgdisplay, 
		display, 
		data = tbl.fullbkg,
		sep = "",
		fontsize = 11,
		corner = 0,
		offx = 35,
		height = 5,
		lineheight = 22,
		cursor = "pointer",
		bgcolor = "white",
		fontcolor = "black",
		text = "";
		wordbeark = '    ' ;
		
	if(data instanceof Array && data.length > 0) {
		data.map(function(oo) { text += sep + oo.time + wordbeark + oo.fullname.substring(0, 10) + wordbeark + oo.npers + wordbeark + oo.seated + wordbeark + oo.duration + wordbeark + oo.state; sep = "\n\n"; });
		height += (data.length * lineheight);
		}
	pp = paper.rect(tbl.cx - 40, tbl.cy + 20, 250, height, corner).attr("fill", bgcolor).attr({'cursor': cursor});
	display = paper.print(tbl.cx - offx, tbl.cy + 30, text, font, fontsize).attr({'fill': fontcolor, 'cursor': cursor});
	fullbkgdisplay = paper.set();
	fullbkgdisplay.push(pp);
	fullbkgdisplay.push(display);
	fullbkgdisplay.hide();
	return fullbkgdisplay;
}

function displayFullBkg(e, tbl, paper, font) {

	if(tbl.fullbkgdisplay !== null) {
		tbl.fullbkgdisplay.toFront();
		tbl.fullbkgdisplay.show();
		}
}

function cleardisplayFullBkg(e, tbl) {
	if(tbl.fullbkgdisplay !== null) {
		tbl.fullbkgdisplay.hide();
		}
}


function livedisplay(tbl, action, queueObj) {
	
	if(!tbl) return;
	
	if(tbl.npers < 1 && queueObj.editmode < 1) return;
		
	if(queueObj.editmode > 0) {
		var x = tbl.bxy[0].myPrecision(0),
			y = tbl.bxy[1].myPrecision(0),
			w = tbl.width.myPrecision(0),
			h = tbl.height.myPrecision(0),
			a = tbl.angle.myPrecision(0),
			z = tbl.z,
			cx = tbl.cx.myPrecision(0),
			cy = tbl.cy.myPrecision(0);
		if(action === 'hover') 
			 queueObj.glbdisplay.attr('text', ' id : '+tbl.id+', type : '+tbl.type+'\ntable : '+tbl.label+'\ntable size : '+tbl.npers+nxtbkg);

		else queueObj.glbdisplay.attr('text', ' id : '+tbl.id+', type : '+tbl.type+'\ntable : '+tbl.label+'\npersons : '+tbl.npers+'\nx : '+x+' y : '+y+'\ncx : '+cx+' cy : '+cy+'\nwidth:'+w+ '--- height:'+h+ '\nangle:'+a+ '\nz:'+z);
		}
		
	else {
		var state = (tbl.objresa) ? tbl.objresa.getstate() : "",
				booking = (tbl.objresa) ? tbl.objresa.getresa() : "",
				connect = (tbl.conObj) ? tbl.conObj.getstate() : "",
				nextAr = (typeof tbl.nxtbkg === "string" && tbl.nxtbkg.length > 0) ? tbl.nxtbkg.split("\t") : [];
		
		var coninfo = (connect !== "0\nfrom: 0") ? '\nconnect:'+connect : "",
			stateinfo = (typeof state === "string" && state !== "") ? '\nstate:'+state : "",
			captext = "";
			confirmation = booking[0],
			guest = booking[1],
			npers = booking[2],
			captain = booking[5],
			duree = booking[6],
			stoptime = booking[7],
			seattime = booking[8],
			nxtbkg = "", 
			bookinginfo = "";
			
			
			if(tbl.nxtbkg !== "" && nextAr[0] !== confirmation)
				nxtbkg = "\n \n- - - - - - - NEXT BOOKING - - - - - - - \nname: " +  nextAr[2] + "\nparty size: " + nextAr[3] + "\nconf: " + nextAr[0] + "\ntime: " + nextAr[1];
			if(typeof confirmation === 'string' && confirmation !== '' && confirmation.indexOf('not-attr') === -1) {
				bookinginfo = "\nbooking:" + confirmation + "\nname:" + guest + "\nparty size:" + npers;
				if(typeof captain === 'string' && captain !== '') captext = "- - - CAPTAIN: " + captain.toUpperCase() + "  - - - - \n"
				if(typeof duree === 'number' && duree !== 0) bookinginfo += "\nlength:" + duree + "mm";
				if(typeof stoptime === 'string' && stoptime.length === 5) bookinginfo += ", clear:" + stoptime;
				if(typeof seattime === 'string' && seattime.length === 5) bookinginfo += ", seat:" + seattime;
				}

		if(action === 'hover') 
			 queueObj.glbdisplay.attr({'text': captext + 'table : '+tbl.label+'\ntable size : '+tbl.npers+bookinginfo+stateinfo+nxtbkg});
		else queueObj.glbdisplay.attr({'text': 'table : '+tbl.label+'\ntable size : '+tbl.npers+bookinginfo});
		if(queueObj.svfconsole.update !== null) queueObj.svfconsole.update();
		}
}

function processRemoteKey(action, qO) {

	switch(action) {
		case "grid" :
			qO.grid.toggle();
			break;

		case "rename" :
			qO.renameTbl();
			break;

		case "showname" :
			qO.shownameTbl();
			break;

		case "duplicate" :
			qO.duplicateObj();
			break;

		case "delete" :
			qO.deleteTbl();
			break;

		case "align-h" :
			qO.alignH();
			break;

		case "align-v" :
			qO.alignV();
			break;

		case "front" :
			qO.moveTbl(1, 0, 'front');
			break;

		case "back" :
			qO.moveTbl(-1, 0, 'front');
			break;

		case "connection" :
			qO.connectTbl();
			break;

		case "deconnection" :
			qO.deconnectTbl();
			break;

		case "transform" :
			//qO.triggerSave();
			break;
		}
		
}

function processKey(e, qO) {
	
	// you are in modal, no interference
	if(qO.keyCodeEnable === false || qO.getEditmode() !== 1)
		return;
		
	// since we are using jquery, the event is already normalize 
	// we want to prevent document scrolling when pressing the arrows: 

	//var arrowKeys = {"left": 37, "up": 38, "right": 39, "down": 40};
	var arrowKeys = {
					"lefta": 65, "upw": 87, "rightd": 68, "downs": 83, "left4": 100, "up8": 104, "right6": 102, "down2": 98, 
					"turn5": 101, "turn": 84,
					"save": 90, 
					"scaleupp": 80, "scaledownm": 77, "scaleup+": 107, "scaledown-": 109, 
					"front": 70, "back": 66, 
					"align-h": 72, "align-v": 86, 
					"bboxq":81,
					"grid":190, // .
					"delete":46, // true delete key, 'fn'+delete on short keyboard
					"delete1":8, // delete (<- on Chrome)
					"connectionc":67,
					"duplicate":49, // shift 1
					"rename":114, // shift 1
					"showname": 188, /* ',' */
					"hightlight-free": 74, "hightlight-tocome": 75, "hightlight-seated": 76, "hightlight-paying": 186, "hightlight-left": 191, "hightlight-noshow": 222, "hightlight-clear": 220
					};

	//alert(e.keyCode+", "+e.which+", "+e.shiftKey+", "+e.altKey+", "+e.metaKey+", "+e.ctrlKey);   
	e = $.event.fix(e);	
	//console.log('CODE', e.keyCode, e.shiftKey, e.altKey, e.metaKey, e.ctrlKey);

	switch(e.keyCode) {		
		case arrowKeys["save"] : 
			if(qO.getfloorname() === 'MULTIPLEFLOOR') {
				return alert("Object Editing/Save is disregarded for multiple floor layout");
				}
			if(e.shiftKey)
				qO.triggerSave();
			break;
		
		case arrowKeys["duplicate"] : 
			if(e.shiftKey)
				qO.duplicateObj();
			break;
		
		case arrowKeys["rename"] : 
			qO.renameTbl();
			break;
		
		case arrowKeys["showname"] : 
			qO.shownameTbl();
			break;
		
		case arrowKeys["delete"] : 
			qO.deleteTbl();
			break;
		
		case arrowKeys["delete1"] : 
			console.log('Stop');
			e.stopPropagation(); // for chrome
			e.preventDefault();
			qO.deleteTbl();
			break;
		
		case arrowKeys["connectionc"] : 
			if(e.shiftKey === false) qO.connectTbl();
			else qO.deconnectTbl();
			break;
		
		case arrowKeys["lefta"] : 
		case arrowKeys["left4"] : 
			qO.moveTbl(-1, 0, 'move');
			break;
		
		case arrowKeys["rightd"] : 
		case arrowKeys["right6"] : 
			qO.moveTbl(1, 0, 'move');
			break;

		case arrowKeys["upw"] : 
		case arrowKeys["up8"] : 
			qO.moveTbl(0, -1, 'move');
			break;

		case arrowKeys["downs"] : 
		case arrowKeys["down2"] : 
			qO.moveTbl(0, 1, 'move');
			break;

		case arrowKeys["scaleup+"] :
		case arrowKeys["scaleupp"] :
			if(e.shiftKey) 
				qO.moveTbl(1, 0, 'scale');
			else if(e.altKey) 
				qO.moveTbl(0, 1, 'scale');
			else qO.moveTbl(1, 1, 'scale');
			break;						
		case arrowKeys["scaledown-"] :
		case arrowKeys["scaledownm"] :
			if(e.shiftKey)
				qO.moveTbl(-1, 0, 'scale');
			else if(e.altKey)
				qO.moveTbl(0, -1, 'scale');
			else qO.moveTbl(-1, -1, 'scale');
			break;

		case arrowKeys["turn"] :
		case arrowKeys["turn5"] :
			if(e.shiftKey === false)
				qO.moveTbl(1, 0, 'turn');
			else qO.moveTbl(-1, 0, 'turn');
			break;

		case arrowKeys["grid"] :
			qO.grid.toggle();
			break;
			
		case arrowKeys["bboxq"] :
			qO.getBBox();
			break;
			
		case arrowKeys["align-h"] :
			qO.alignH();
			break;
		case arrowKeys["align-v"] :
			qO.alignV();
			break;
			
		case arrowKeys["front"] :
			qO.moveTbl(1, 0, 'front');
			break;
		case arrowKeys["back"] :
			qO.moveTbl(-1, 0, 'front');
			break;

		case arrowKeys["hightlight-free"] :
			qO.setstate(qO.groupObj.getSelectedId(), 'free');
			break;
		case arrowKeys["hightlight-left"] :
			qO.setstate(qO.groupObj.getSelectedId(), 'left');
			break;
		case arrowKeys["hightlight-tocome"] :
			qO.setstate(qO.groupObj.getSelectedId(), 'to come');
			break;
		case arrowKeys["hightlight-seated"] :
			qO.setstate(qO.groupObj.getSelectedId(), 'seated');
			break;
		case arrowKeys["hightlight-paying"] :
			qO.setstate(qO.groupObj.getSelectedId(), 'paying');
			break;
		case arrowKeys["hightlight-noshow"] :
			qO.setstate(qO.groupObj.getSelectedId(), 'no show');
			break;
		case arrowKeys["hightlight-clear"] :
			qO.setstate(qO.groupObj.getSelectedId(), 'clear');
			break;
		}

		// we want to prevent document scrolling when pressing the arrows: 
		//e.preventDefault();
	livedisplay(qO.getTblSelected(), 'keycode', qO);
}

Raphael.el.trigger = function(eventName){
    for(var i = 0, len = this.events.length; i < len; i++) {
        if (this.events[i].name === eventName) {
            this.events[i].f.call(this);
        }
    }
};

Raphael.fn.connection = function (obj1, obj2, line, bg) {
    if (obj1.line && obj1.from && obj1.to) {
        line = obj1;
        obj1 = line.from;
        obj2 = line.to;
    }
    var bb1 = obj1.getBBox(),
        bb2 = obj2.getBBox(),
        p = [{x: bb1.x + bb1.width / 2, y: bb1.y - 1},
        {x: bb1.x + bb1.width / 2, y: bb1.y + bb1.height + 1},
        {x: bb1.x - 1, y: bb1.y + bb1.height / 2},
        {x: bb1.x + bb1.width + 1, y: bb1.y + bb1.height / 2},
        {x: bb2.x + bb2.width / 2, y: bb2.y - 1},
        {x: bb2.x + bb2.width / 2, y: bb2.y + bb2.height + 1},
        {x: bb2.x - 1, y: bb2.y + bb2.height / 2},
        {x: bb2.x + bb2.width + 1, y: bb2.y + bb2.height / 2}],
        d = {}, dis = [];
    for (var i = 0; i < 4; i++) {
        for (var j = 4; j < 8; j++) {
            var dx = Math.abs(p[i].x - p[j].x),
                dy = Math.abs(p[i].y - p[j].y);
            if ((i === j - 4) || (((i !== 3 && j !== 6) || p[i].x < p[j].x) && ((i !== 2 && j !== 7) || p[i].x > p[j].x) && ((i !== 0 && j !== 5) || p[i].y > p[j].y) && ((i !== 1 && j !== 4) || p[i].y < p[j].y))) {
                dis.push(dx + dy);
                d[dis[dis.length - 1]] = [i, j];
            }
        }
    }
    if (dis.length === 0) {
        var res = [0, 4];
    } else {
        res = d[Math.min.apply(Math, dis)];
    }
    var x1 = p[res[0]].x,
        y1 = p[res[0]].y,
        x4 = p[res[1]].x,
        y4 = p[res[1]].y;
    dx = Math.max(Math.abs(x1 - x4) / 2, 10);
    dy = Math.max(Math.abs(y1 - y4) / 2, 10);
    var x2 = [x1, x1, x1 - dx, x1 + dx][res[0]].toFixed(3),
        y2 = [y1 - dy, y1 + dy, y1, y1][res[0]].toFixed(3),
        x3 = [0, 0, 0, 0, x4, x4, x4 - dx, x4 + dx][res[1]].toFixed(3),
        y3 = [0, 0, 0, 0, y1 + dy, y1 - dy, y4, y4][res[1]].toFixed(3);
    var path = ["M", x1.toFixed(3), y1.toFixed(3), "C", x2, y2, x3, y3, x4.toFixed(3), y4.toFixed(3)].join(",");
    if (line && line.line) {
        line.bg && line.bg.attr({path: path});
        line.line.attr({path: path});
    } else {
        var color = typeof line === "string" ? line : "#000";
        return {
            bg: bg && bg.split && this.path(path).attr({stroke: bg.split("|")[0], fill: "none", "stroke-width": bg.split("|")[1] || 3}),
            line: this.path(path).attr({stroke: color, fill: "none"}),
            from: obj1,
            to: obj2,
            hide: function() { if(this.bg) this.bg.hide(); if(this.line) this.line.hide(); },
            show: function() { if(this.bg) this.bg.show(); if(this.line) this.line.show(); },
            remove: function() { if(this.bg) this.bg.remove(); if(this.line) this.line.remove(); }
        };
    }
};

function hslToRgb(val) {
	var r, g, b, h, s, l, cc, valAr;

	if(typeof val !== "string" || val.length < 7)
		return "#000000";

	if(val.substr(0, 1) === "#")
		return val;
			
	if(val.indexOf("hsl") === -1) {
		return "#000000";
		}
		
	valAr = val.replace(/[^0-9\.,]/g, "").split(",");
	if(valAr.length < 3)
		return val;
	
	if(valAr[1] === "0") 
		return (valAr[2] === "0") ? "#000000" : "#ffffff";
	
	h = parseFloat(valAr[0]) / 360.0;
	s = parseFloat(valAr[1]) / 100.0;
	l = parseFloat(valAr[2]) / 100.0;
				
	var hue2rgb = function hue2rgb(p, q, t){
			if(t < 0) t += 1;
			if(t > 1) t -= 1;
			if(t < 1/6) return p + (q - p) * 6 * t;
			if(t < 1/2) return q;
			if(t < 2/3) return p + (q - p) * (2/3 - t) * 6;
			return p;
		};

	var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
	var p = 2 * l - q;
	r = hue2rgb(p, q, h + 1/3);
	g = hue2rgb(p, q, h);
	b = hue2rgb(p, q, h - 1/3);

	r = Math.round(r * 255).toString(16);
	g = Math.round(g * 255).toString(16);
	b = Math.round(b * 255).toString(16);
	cc = "#" + ((r.length < 2) ? "0" + r : r) + ((g.length < 2) ? "0" + g: g) +  ((b.length < 2) ? "0" + b: b);
		
	return cc;
}			

function hslToRgbRdnt(fill, radiant, type) {
	var f, r, clr;

	f = hslToRgb(fill);
	
	if(typeof radiant !== "string" || radiant.length < 7)
		return f;
		
	r = hslToRgb(radiant);
	clr = (type === "c") ? "r" + f + "-" + r : "0-" + f + "-" + r;
	return clr;
	
}

function opacity(val) {
	var op, valAr;
	if(typeof val !== "string" || val.length < 10 || val.indexOf("hsla") === -1)
		return 1;
	
	valAr = val.replace(/[^0-9\.,]/g, "").split(",");
	if(valAr.length < 4)
		return 1;
	
	op = parseFloat(valAr[3]);
	if(op === 0) op = 0.01; // don't want to be complete opaque as you cannot select the object
		
	return (op > 0 && op < 1) ? op : 1;
	}
	