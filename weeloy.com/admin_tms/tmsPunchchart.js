/*
 *  ┌─────────────────────────────────────────────┐ 
 *  │ TMS 1.0 - JavaScript Vector Library             │ 
 *  ├─────────────────────────────────────────────┤ 
 *  │ Copyright © 2015 Weeloy. All rights reserved.   │ 
 *  ├─────────────────────────────────────────────┤ 
 *  │ Company: https://www.weeloy.com				  │ 
 *  ├─────────────────────────────────────────────┤ 
 *  │ Author Richard Kefs							  │ 
 *  └─────────────────────────────────────────────┘ 
 *
*/

// few adjustment for backoffice or tms
// need to know why ?

app.service('tmsAnalytic', [ function() {

	function Punchobj() {

		var data =  [
					 0, 0, 40, 0, 15, 10, 0, 17, 0, 14, 12, 20, 16, 0, 0, 5, 28, 
					 0, 0, 4, 3, 0, 29, 5, 34, 0, 0, 26, 0, 0, 0, 0, 29, 8, 
					 0, 0, 1, 20, 0, 0, 21, 7, 5, 17, 23, 9, 0, 6, 0, 38, 27, 
					 0, 32, 0, 0, 7, 0, 0, 27, 0, 0, 18, 0, 1, 0, 39, 3, 0, 
					 2, 27, 22, 0, 0, 0, 0, 40, 38, 8, 0, 0, 8, 0, 9, 0, 38, 
					 0, 24, 0, 30, 0, 10, 0, 0, 0, 20, 30, 0, 0, 34, 0, 0, 30, 
					 0, 0, 0, 26, 3, 28, 23, 0, 0, 0, 21, 0, 0, 0, 20, 0, 0 ],
			axisx = ["7", "8", "9", "10", "11", "12pm", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11"],
			axisy = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"];

		var width = 800,
			height = 320,
			leftgutter = 40,
			bottomgutter = 20,
			r =null, 
			i, ii, o,
			X = 40,// (width - leftgutter) / axisx.length,
			XX = 30,
			Y = 40, // (height - bottomgutter) / axisy.length,
			color = "black",
			txt = { stroke: "none", fill: "#fff"},
			max = Math.round(XX / 2) - 1;
	
		return {
			clear: function() {
				if(r)
					r.remove();
				},
					
			draw: function(platform) {
				if(platform !== "tms") { Y = 25; txt = { color: "black"}; color = "white"; height = 450; }
			
				this.clear();
				r = Raphael("punch_chart", width, height);
				for (i = 0, ii = axisx.length; i < ii; i++) {
					r.text(leftgutter + X * (i + .5), Y * (axisy.length + .5), axisx[i]).attr(txt);	// 
					}
				for (i = 0, ii = axisy.length; i < ii; i++) {
					r.text(20, Y * (i + .5), axisy[i]).attr(txt);
					}
				
				o = 0;
				for (i = 0, ii = axisy.length; i < ii; i++) {
					for (j = 0, jj = axisx.length; j < jj; j++) {
						if(platform !== "tms") { X = 40; Y = 49.5; }
						R = data[o] && Math.min(Math.round(Math.sqrt(data[o] / Math.PI) * 4), max);
						if (R) {
							(function (dx, dy, R, value, yy) {
								if(platform === "tms") yy = dy;
								var color = "hsb(" + [(1 - R / max) * .5, 1, .75] + ")";
								var dt = r.circle(dx, dy, R).attr({stroke: "none", fill: color});
								if (R < 6) {
									var bg = r.circle(dx, dy, 6).attr({stroke: "none", fill: "#000", opacity: .4}).hide();
								}
								var lbl = r.text(dx, yy, value).attr(txt).hide();
								var dot = r.circle(dx, dy, max).attr({stroke: "none", fill: "#000", opacity: 0});
								dot[0].onmouseover = function () {
									if (bg) {
										bg.show();
									} else {
										var clr = Raphael.rgb2hsb(color);
										clr.b = .5;
										dt.attr("fill", Raphael.hsb2rgb(clr).hex);
									}
									lbl.show();
								};
								dot[0].onmouseout = function () {
									if (bg) {
										bg.hide();
									} else {
										dt.attr("fill", color);
									}
									lbl.hide();
								};
							})(leftgutter + X * (j + .5), Y * (i + .5), R, data[o], (Y * (i + 1)/2) - 11);
						}
						o++;
					}
				}
			}
		};
	}

	var myPunch = new Punchobj();

	this.drawchart = function(scope, platform) {
		
		var loaded = $('#mydisplay').html().indexOf('punch_chart');
        if(loaded  < 0) 
        	return false;
 
		myPunch.draw(platform); 
		};
}])
