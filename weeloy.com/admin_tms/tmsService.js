/*
 *  ┌─────────────────────────────────────────────┐ 
 *  │ TMS 1.0 - JavaScript Vector Library             │ 
 *  ├─────────────────────────────────────────────┤ 
 *  │ Copyright © 2015 Weeloy. All rights reserved.   │ 
 *  ├─────────────────────────────────────────────┤ 
 *  │ Company: https://www.weeloy.com				  │ 
 *  ├─────────────────────────────────────────────┤ 
 *  │ Author Richard Kefs							  │ 
 *  └─────────────────────────────────────────────┘ 
 *
*/

var FancyWebSocket = function(url)
{
	var callbacks = {};
	var ws_url = url;
	var conn;

	this.bind = function(event_name, callback){
		callbacks[event_name] = callbacks[event_name] || [];
		callbacks[event_name].push(callback);
		return this;// chainable
	};

	this.send = function(event_name, event_data){
		this.conn.send( event_data );
		return this;
	};

	this.connect = function() {
		if ( typeof(MozWebSocket) == 'function' )
			this.conn = new MozWebSocket(url);
		else
			this.conn = new WebSocket(url);

		// dispatch to the right handlers
		this.conn.onmessage = function(evt){
			dispatch('message', evt.data);
		};

		this.conn.onclose = function(){dispatch('close',null)}
		this.conn.onopen = function(){dispatch('open',null)}
	};

	this.disconnect = function() {
		this.conn.close();
	};

	var dispatch = function(event_name, message){
		var chain = callbacks[event_name];
		if(typeof chain == 'undefined') return; // no callbacks for this event
		for(var i = 0; i < chain.length; i++){
			chain[i]( message )
		}
	}
};

var realtimeio = function(channel, user, token, func, scope) {
	channel += "-" + window.location.hostname;
	var ortcClient = RealtimeMessaging.createClient();
	ortcClient.setClusterUrl('https://ortc-developers.realtime.co/server/ssl/2.1/');
	ortcClient.connect('5z10Qe', 'testToken');

	ortcClient.onConnected = function(ortc) {
		ortcClient.constatus = "Connected to " + ortcClient.getUrl() + ";channel = " + channel;

		ortcClient.process = func;
		ortcClient.senddata = function(msg) {
			if(typeof msg !== "string" || msg.length < 1)
				return;
			var myMessage = {
				data: msg.replace(/\'|\"/g, '`'),
				from: token,
				channel: channel
				};
 
				ortcClient.send(channel, JSON.stringify(myMessage));
				};

		ortcClient.subscribe(channel, true, function(ortc, stream, message) {
			try {
				var oo = JSON.parse(message);
			} catch(e) { console.error("JSON-PROFILE", e.message); }

			if(oo.from !== token && stream === channel)
				func(oo, scope);
			});
		};
	return ortcClient;
};

app.service('tmsService',['$http',function($http){

	var tmsService = this;
	this.tmsSections = function(params){
		return $http.post("../api/services.php/tms/gettmssection/", params)
    			.then(function(response) {return response.data;});
	};

	this.tmsgetCalendar = function(params, route){
		var prefix = (typeof route === "string") ? route : "";			
		return $http.post(prefix + "../api/services.php/tms/gettmscalendar/", params)
    			.then(function(response) {return response.data;});
	};

	this.tmssetCalendar = function(params, route){
		var prefix = (typeof route === "string") ? route : "";			
		return $http.post(prefix + "../api/services.php/tms/settmscalendar/", params)
    			.then(function(response) {return response.data;});
	};

	this.tmsSave = function(params){
		return $http.post("../api/services.php/tms/savetmslayout/", params)
    			.then(function(response) {return response.data;});
	};

	this.tmsDelete = function(params){
		return $http.post("../api/services.php/tms/deletetmslayout/", params)
    			.then(function(response) {return response.data;});
	};

	this.tmsRename = function(params){
		return $http.post("../api/services.php/tms/renametmslayout/", params)
    			.then(function(response) {return response.data;});
	};


	this.tmsSaveTableSetting = function(params){
		return $http.post("../api/services.php/tms/savetmstablesetting/", params)
    			.then(function(response) {return response.data;});
	};

	this.tmsGetblock = function(params, route){
		var prefix = (typeof route === "string") ? route : "";			
		return $http.post(prefix + "../api/services.php/tms/gettmsblock/", params)
    			.then(function(response) {return response.data;});
	};

	this.tmsSetblock = function(params, route){
		var prefix = (typeof route === "string") ? route : "";			
		return $http.post(prefix + "../api/services.php/tms/settmsblock/", params)
    			.then(function(response) {return response.data;});
	};

	this.tmsSyncInit = function(params, route){
		var prefix = (typeof route === "string") ? route : "";			
		return $http.post(prefix + "api/services.php/tms/syncninit/", params)
    			.then(function(response) {return response.data;});
	};

	this.tmsRltimeio = function(restaurant, user, token, func, scope) {
		var oo = realtimeio(restaurant, user, token, func, scope);
		return  {
			restaurant: restaurant,
			user: user,
			token: token,
			server:null,
			scope: scope,
			obj: oo,
			};
	};
		
	this.tmsSyncWebsocket = function(restaurant, user, token) {
		var host, route;
		if(window.location.href.indexOf("localhost") > -1) {
			route = "../";
			host = "ws://localhost:9300";
			}
		else if(window.location.href.indexOf("www.weeloy.com") > -1) {
			route = "../";
			host = "ws://dev.weeloy.com:9300";
			}
		else if(window.location.href.indexOf("dev.weeloy.com") > -1) {
			route = "../";
			host = "ws://dev.weeloy.com:9300";
			}
		else host = route = "";
			
		var syncObj =  {
			state: "init",
			server:null,
			connect: function() {
				if(host === "") {
					console.log("HOST not set");
					return;
					}
				tmsService.tmsSyncInit({restaurant: restaurant, user: user, token: token}, route)
					.then(function(response) { 
						console.log('SYNC', response);
						if(response.status === 1) {
							syncObj.state = "synced";
							syncObj.server = new FancyWebSocket(host);
							syncObj.server.bind('open', function() { console.log( "Connected." );	syncObj.state = "connected"; });						
							syncObj.server.bind('close', function( data ) { console.log( "Disconnected." ); syncObj.state = "closed"; });  //OH NOES! Disconnection occurred.
							syncObj.server.bind('message', function( payload ) { console.log( payload ); }); //Log any messages sent from server
							syncObj.server.connect();
							console.log('SYNC connect', syncObj);
							};
					});
				}, 

			send: function(text) {
				if(syncObj.state === "connected")
					syncObj.server.send( 'message', restaurant + ";" + user + ";" + token + ";" + text );
				console.log('SYNC Sending', text, syncObj.state); 
				},
			};
		return syncObj;				
		};
}]);

