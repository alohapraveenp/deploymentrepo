/*
 *  ┌─────────────────────────────────────────────┐ 
 *  │ TMS 1.0 - JavaScript Vector Library             │ 
 *  ├─────────────────────────────────────────────┤ 
 *  │ Copyright © 2015 Weeloy. All rights reserved.   │ 
 *  ├─────────────────────────────────────────────┤ 
 *  │ Company: https://www.weeloy.com				  │ 
 *  ├─────────────────────────────────────────────┤ 
 *  │ Author Richard Kefs							  │ 
 *  └─────────────────────────────────────────────┘ 
 *
*/

 
modalInstance = null;
token = '';
localcookie = 'weeloy_trad';

app.constant('URL', ['data/']);

app.service('interfaceModel', ['$http', 'URL', function ($http, URL) {

	this.translatestart = [ 
				{"content_type" : "image", "title" : "../images/admin/bo_slider_928_460.jpg", "id":"img" } 
				];
				
	this.translateblank = [ 
				{"content_type" : "blank", "title":"" } 
				];

	this.translateraphael = [ 
				{"content_type" : "raphael", "title":"" } 
				];
				
	this.translatedata = [
				{"content_type" : "text", "title":"email" },
				{"content_type" : "inputsm", "type":"text", "title" : "Email", "topmargin" : "0", "id":"email" }
				];
					   
	this.translatelist = [
				{"content_type" : "templatelist" }
				];

	this.translatenew = [
				{"content_type" : "templatenew" }
				];
					   
	this.login = [
				{"content_type" : "title", "title":"Login" },
				{"content_type" : "header1", "title":"Login", "title1":"Forgot password?", "title2": "Register", "arg1" : "forgot", "arg2" : "register" },
				{"content_type" : "input", "type":"text", "variable": "email", "title" : "Email", "topmargin" : "0", "glyphicon":"envelope", "id":"email" },
				{"content_type" : "password", "type":"password", "variable": "password", "title" : "Password", "topmargin" : "25px", "glyphicon":"lock", "id":"password" },
				{"content_type" : "showpass", "type":"checkbox", "variable": "showp", "title" : "Show password", "topmargin" : "5px", "glyphicon":"", "id":"showp" },
				{"content_type" : "submit", "type":"submit", "variable": "login", "title" : "Login", "topmargin" : "25px", "glyphicon":"", "id":"login" },
				{"content_type" : "facebookfooter", "type":"submit", "variable": "login", "title" : "Continue with Facebook", "topmargin" : "25px", "glyphicon":"", "id":"facebooklogin" }
				];

	this.change = [
				{"content_type" : "title", "title":"Change Password" },
				{"content_type" : "header2", "title":"Change password", "title1":"", "title2": "", "arg1" : "", "arg2" : "" },
				{"content_type" : "input", "type":"text", "variable": "email", "title" : "Email", "topmargin" : "0", "glyphicon":"envelope", "id":"email" },
				{"content_type" : "password", "type":"password", "variable": "password", "title" : "Password", "topmargin" : "25px", "glyphicon":"lock", "id":"password" },
				{"content_type" : "password", "type":"password", "variable": "npassword", "title" : "New Password", "topmargin" : "25px", "glyphicon":"lock", "id":"npassword" },
				{"content_type" : "password", "type":"password", "variable": "rpassword", "title" : "Retype password", "topmargin" : "25px", "glyphicon":"lock", "id":"rpassword" },
				{"content_type" : "showpass", "type":"checkbox", "variable": "showp", "title" : "Show password", "topmargin" : "5px", "glyphicon":"", "id":"showp" },
				{"content_type" : "empty", "type":"text", "variable": "", "title" : "", "topmargin" : "10px;", "glyphicon":"", "id":"empty" },
				{"content_type" : "submitfooter", "type":"submit", "variable": "change", "title" : "Change your Password", "topmargin" : "10px", "glyphicon":"", "id":"change" }
				];

	this.register = [
				{"content_type" : "title", "title":"Register" },
				{"content_type" : "header1", "title":"Register", "title1":"Login", "arg1" : "login", "title2": "Forgot password?", "arg2" : "forgot" },
				{"content_type" : "input", "type":"text", "variable": "email", "title" : "Email", "topmargin" : "0", "glyphicon":"envelope", "id":"email" },
				{"content_type" : "input", "type":"text", "variable": "firstname", "title" : "First Name", "topmargin" : "25px", "glyphicon":"user", "id":"firstname" },
				{"content_type" : "input", "type":"text", "variable": "lastname", "title" : "Last Name", "topmargin" : "25px", "glyphicon":"user", "id":"lastname" },
				{"content_type" : "input", "type":"text", "variable": "mobile", "title" : "Phone", "topmargin" : "25px", "glyphicon":"earphone", "id":"mobile" },
				{"content_type" : "password", "type":"password", "variable": "password", "title" : "New Password", "topmargin" : "25px", "glyphicon":"lock", "id":"password" },
				{"content_type" : "password", "type":"password", "variable": "rpassword", "title" : "Retype password", "topmargin" : "25px", "glyphicon":"lock", "id":"rpassword" },
				{"content_type" : "showpass", "type":"checkbox", "variable": "showp", "title" : "Show password", "topmargin" : "5px", "glyphicon":"", "id":"showp" },
				//{"content_type" : "empty", "type":"text", "variable": "", "title" : "", "topmargin" : "10px;", "glyphicon":"", "id":"empty" },
				{"content_type" : "submit", "type":"submit", "variable": "change", "title" : "Register", "topmargin" : "10px", "glyphicon":"", "id":"register" },
				{"content_type" : "facebookfooter", "type":"submit", "variable": "login", "title" : "Continue with Facebook", "topmargin" : "25px", "glyphicon":"", "id":"facebooklogin" }
				];

	this.forgot = [
				{"content_type" : "title", "title":"Forgot Password" },
				{"content_type" : "header1", "title":"Forgot Password", "title1":"Login", "arg1" : "login", "title2": "Register", "arg2" : "register" },
				{"content_type" : "input", "type":"text", "variable": "email", "title" : "Email", "topmargin" : "0", "glyphicon":"envelope", "id":"email" },
				{"content_type" : "empty", "type":"text", "variable": "", "title" : "", "topmargin" : "10px;", "glyphicon":"", "id":"empty" },
				{"content_type" : "submitfooter", "type":"submit", "variable": "forgot", "title" : "Retrieve", "topmargin" : "10px", "glyphicon":"", "id":"forgot" }
				];

	this.templateData = {
			  "blankTemplate": "",
			  "raphaelTemplate": "<div id='raphael'></div>",
			  "modalTitle": "<title>{{content.title}}</title>",
			  "modalHeader1": "<meta content='width=device-width, initial-scale=1' name='viewport'><div class='modal-header modal-perso'><div class='panel-title'><span class='headertitle'>{{content.title}} &nbsp;</span><span class='headersubtitle'><a href='javascript:;' ng-click='$parent.open(content.arg1);' class='mwhite'>{{content.title1}}</a> | <a href='javascript:;' ng-click='$parent.open(content.arg2);' class='mwhite'>{{content.title2}}</a></span></div></div><div class='modal-body' style='background-color:white;'>",
			  "modalHeader2": "<meta content='width=device-width, initial-scale=1' name='viewport'><div class='modal-header modal-perso'><div class='panel-title'><span class='headertitle'>{{content.title}} &nbsp;</span><span class='headersubtitle'><a href='javascript:;' ng-click='$parent.open(content.arg1);' class='mwhite'>{{content.title1}}</a></span></div></div><div class='modal-body' style='background-color:white;'>",
			  "inputTemplate": "<div class='input-group' style='margin: {{content.topmargin}} 10px 0 10px;'><span class='input-group-addon'><i class='glyphicon glyphicon-{{content.glyphicon}}'></i></span><input class='form-control' type='text' placeholder='{{content.title}}' ng-model=$parent.user_data[content.id] id='{{content.id}}' name='{{content.id}}' ng-keypress='$parent.myKeyPress($event);'></div>",
			  "inputsmTemplate": "<input class='form-control' type='text' placeholder='{{content.title}}' ng-model=$parent.langdata[content.id] id='{{content.id}}' name='{{content.id}}'>",
			  "passwordTemplate": "<div class='input-group' style='margin: {{content.topmargin}} 10px 0 10px;'><span class='input-group-addon'><i class='glyphicon glyphicon-{{content.glyphicon}}'></i></span><input class='form-control passtype' type='password' placeholder='{{content.title}}' ng-model=$parent.user_data[content.id] id='{{content.id}}' name='{{content.id}}' ng-keypress='$parent.myKeyPress($event);'></div>",
			  "showpassTemplate": "<div class='input-group input-sm' style='margin: {{content.topmargin}} 10px 0 10px;'><label class='checkbox-inline' style='font-size:11px;'><input type='checkbox' ng-click='$parent.genShowPassword(content.id);' id='{{content.id}}' name='{{content.id}}'>{{content.title}}</label></div>",
			  "emptyTemplate": "<div class='input-group input-sm' style='margin: {{content.topmargin}} 10px 0 10px;'><label class='checkbox-inline'></label></div>",
			  "textTemplate": "<p>{{title}}</p>",
			  "imageTemplate": "<img ng-src='{{ $parent.user_data[content.id] }}'>",
			  "submitTemplate": "<div class='form-inline' style='margin: {{content.topmargin}} 10px; 10px 10px;'><div class='input-group'><a class='btn btn-info' href='javascript:;' ng-click='$parent.genSubmit(content.id,user_data)' id='{{content.id}}' name='{{content.id}}'>{{content.title}}</a></div></div></div>",
			  "submitFooterTemplate": "</div><div class='modal-footer'><div class='form-inline' style='margin-top: {{content.topmargin}} 10px 0 10px;'><div class='input-group'><a class='btn btn-info' href='javascript:;' ng-click='$parent.genSubmit(content.id)' id='{{content.id}}' name='{{content.id}}'>{{content.title}}</a></div></div>",
			  "facebookFooterTemplate": "<div class='modal-footer'><div id='facebook'><a id='loginBtn' class='btn btn-sm btn-social btn-facebook' title='With your Facebook account' href='javascript:;' ng-click='$parent.fblogin();'><i class='fa fa-facebook'></i>{{content.title}}</a><div id='status_fb'></div></div></div>"
			};       			
}]);

app.directive('contentItem', ['$compile' ,function ($compile) {
    var getTemplate = function (templates, contentType) {
        var template = '';
		var titles = { title:'modaltitle', header1:'modalHeader1', header2:'modalHeader2', input:'inputTemplate', inputsm:'inputsmTemplate', text:'textTemplate', password:'passwordTemplate', showpass:'showpassTemplate', image:'imageTemplate', submit:'submitTemplate', submitfooter:'submitFooterTemplate', facebookfooter:'facebookFooterTemplate', templatelist:'langTemplate', templatenew:'newlangTemplate', empty:'emptyTemplate', blank:'blankTemplate', raphael:'raphaelTemplate' };

		if(contentType in titles)
			 template = templates[titles[contentType]];

        return template;
    };

    var linker = function (scope, element, attrs) {
            element.html(getTemplate(scope.$parent.templateData, scope.content.content_type));
            $compile(element.contents())(scope);
		    };

    return {
        link: linker,
    	scope: {
            content: '=',
            myTemplates: '='
            }            
        
    };
}]);


app.controller('loginModalController',['$scope', '$rootScope', '$uibModal', '$modalInstance', '$localStorage', 'interfaceModel', 'FormControl', 'loginService', 'tmsService', 'Facebook', function($scope, $rootScope, $uibModal, $modalInstance, $localStorage, interfaceModel, FormControl, loginService, tmsService, Facebook){
	$scope.user_data = {};
	$scope.interfaceAll = { "login":"", "forgot":"", "change":"", "register":""};
	$scope.templateData = interfaceModel.templateData;
	
	$scope.interfaceLogin = $scope.interfaceAll.login = interfaceModel.login;
	$scope.interfaceForgot = $scope.interfaceAll.forgot = interfaceModel.forgot;
	$scope.interfaceChange = $scope.interfaceAll.change = interfaceModel.change;
	$scope.interfaceRegister = $scope.interfaceAll.register = interfaceModel.register;
	
	if(typeof $scope.curTemplate === "undefined")
		$scope.curTemplate = 'login';

	if(typeof $localStorage.langdata !== "undefined" && $localStorage.langdata !== "")
		$scope.user_data.email = $localStorage.user_data_email;
		
	$scope.myKeyPress = function(keyEvent) {
	  if (keyEvent.which === 13)
	  	$scope.genSubmit($scope.curTemplate);
	};

	$scope.versionreload = function() {
		var reload = false;
	
		if(typeof $localStorage.tmsversion !== "string" || $localStorage.tmsversion === "")
			$localStorage.tmsversion = "";
		if(typeof localStorage.tmsversion !== "string" || localStorage.tmsversion === "")
			localStorage.tmsversion = "";
	
		if($localStorage.tmsversion !== tmsversion && localStorage.tmsversion !== tmsversion)
			reload = true;
	
		$localStorage.tmsversion = localStorage.tmsversion = tmsversion;
		console.log('Version ', $localStorage.tmsversion, localStorage.tmsversion, tmsversion);
	
		console.log('oldVersion ', $localStorage.tmsversion, localStorage.tmsversion, tmsversion, reload);
		if(reload) {
			//alert("There is a new version: "+tmsversion + ". Reloading the page (" + $localStorage.tmsversion + ")");
			$localStorage.tmsversion = localStorage.tmsversion = tmsversion;
			location.reload(true);
			}
		};
		
	$scope.myrender = function(dataelements) {
		angular.forEach(dataelements, function(data) {
			if(data.content_type === "input" || data.content_type === "password" || data.content_type === "textarea") {
				//console.log(data.id + ' = ' + $('#' + data.id).val());
				//angular.element(data.id).controller('ngModel').$render();
				//bug in angular js concerning autocomplete
				$scope.user_data[data.id] = $('#' + data.id).val();
				}
			});
    };
    
	$scope.open = function(type) {
		var size, template, modalInstance;
		
		size = 'sm';
		$modalInstance.close();

		template = 'loginBackoffice.html';		
		if(type === 'forgot') 
			template = 'forgotBackoffice.html';
		else if(type === 'change') 
			template = 'chgpassBackoffice.html';
		else if(type === 'register') 
			template = 'registerBackoffice.html';

		$scope.curTemplate = type;
		
		modalInstance = $uibModal.open({
			templateUrl: template,
			controller: 'loginModalController',
			size: size
		 });
	 };
	
	
	$scope.genShowPassword = function(id) {
		
		if ($('#password').attr('type') !== 'text') {
			$('.passtype').attr('type', 'text');
			$('.showpass').addClass('show');
		} 
		else {
			$('.passtype').attr('type', 'password');
			$('.showpass').removeClass('show');
		}
		return false;
	};
	
	$scope.genHidePassword = function() {
		$('.passtype').attr('type', 'password');
		$('.showpass').removeClass('show');
	};

	$scope.genSubmit = function(type) {
		var user;
		
		if(typeof $scope.interfaceAll[type] !== "undefined" && $scope.interfaceAll[type] !== null)
			$scope.myrender($scope.interfaceAll[type]);			
		
		switch(type) {
		 case 'login':
			$scope.user_data = FormControl.checkLogin($scope.user_data);
			if($scope.user_data.result > 0) {
				return loginService.login($scope.user_data.email, $scope.user_data.password, '', 'tms').then(function(response){
					if(response.status === 0) {
						alert(response.errors);
						return;
						}
					token = response.data.token;
					user = $scope.$parent.usremail = $scope.user_data.email;
					setCookieOnline(cookiename, response.data.cookie, response.data.duration+1);
					setCookieOnline(localcookie, ":token="+token+":user="+user, response.data.duration+1);			
					$localStorage.user_data_email = $scope.user_data.email;		
					$scope.$parent.usremail = $scope.user_data.email;
					$scope.$parent.updatestate();
					$modalInstance.close();
					$scope.versionreload();
					});
				}				
		 	break;
		 	
		 case 'change':
			$scope.user_data = FormControl.checkChange($scope.user_data);
			if($scope.user_data.result > 0) {
				return loginService.change($scope.user_data.email, $scope.user_data.password, $scope.user_data.npassword, token).then(function(response){
					if(response.data === '1')
						alert('Password has been changed');
					else alert(response.errors);
					$modalInstance.close();
					});
				}				
		 	break;
		 	
		 case 'forgot':
			$scope.user_data = FormControl.checkForgot($scope.user_data);
			if($scope.user_data.result > 0) {
				return loginService.forgot($scope.user_data.email).then(function(response){
					if(response.data === '1')
						alert('A new Password has been sent to ' + $scope.user_data.email);
					$modalInstance.close();
					});
				}				
		 	break;
		 	
		 case 'register':
			$scope.user_data = FormControl.checkRegister($scope.user_data);
			if($scope.user_data.result > 0) {
				alert('Register not implemented yet. Check status instead');
				return loginService.checkstatus($scope.user_data.email, token).then(function(response){
					//if(response.data === '1') alert('login'); else alert('logout');
					});
				}				
		 	break;
		 	
		 default:
		 	alert("Invalid command "+type);
		 	return;
		 }
		 
		if($scope.user_data.result < 0) {
			alert($scope.user_data.msg);
			return;
			}
	};
	
	$scope.genHidePassword() ; 

// facebook login
	
	$rootScope.session = {};
    $rootScope.$on("fb_statusChange", function (event, args) {
        $rootScope.$apply();
    });
    $rootScope.$on("fb_get_login_status", function () {
        Facebook.getLoginStatus();
    });
    $rootScope.$on("fb_login_failed", function () {
        console.log("fb_login_failed");
    });
    $rootScope.$on("fb_logout_succeded", function () {
        console.log("fb_logout_succeded");
    });
    $rootScope.$on("fb_logout_failed", function () {
        console.log("fb_logout_failed!");
    });

    $rootScope.$on("fb_connected", function (event, args) {
        /*
         If facebook is connected we can follow two paths:
         The users has either authorized our app or not.

         ---------------------------------------------------------------------------------
         http://developers.facebook.com/docs/reference/javascript/FB.getLoginStatus/

         the user is logged into Facebook and has authenticated your application (connected)
         the user is logged into Facebook but has not authenticated your application (not_authorized)
         the user is not logged into Facebook at this time and so we don't know if they've authenticated
         your application or not (unknown)
         ---------------------------------------------------------------------------------

         If the user is connected to facebook, his facebook_id will be enough to authenticate him in our app,
         the only thing we will have to do is to post his facebook_id to 'php/auth.php' and get his info
         from the database.

         If the user has a status of unknown or not_authorized we will have to do a facebook api call to force him to
         connect and to get some extra data we might need to unthenticated him.
         */

        var params = {};

		$rootScope.session.facebook_id = args.facebook_id;
		$rootScope.session.facebook_token = args.facebook_token;
		FB.api('/me', function(response) {
			var ses, user;
			if(response.name !== "") {
				$rootScope.session.email = response.email;
				$rootScope.session.first_name = response.first_name;
				$rootScope.session.last_name = response.last_name;
				$rootScope.session.name = response.name;
				$rootScope.session.timezone = response.timezone;
				ses = $rootScope.session;
            	params = {'email': ses.email, 'facebookid': ses.facebook_id, 'facebooktoken': ses.facebook_token };
            	loginService.loginfacebook(params).then(function(response){
            		token = response.data.token;
					user = $scope.$parent.usremail = ses.email;
					setCookieOnline(cookiename, response.data.cookie, response.data.duration+1);
					setCookieOnline(localcookie, ":token="+token+":user="+user, response.data.duration+1);			
					$scope.$parent.updatestate();
					$modalInstance.close();
            		});
				}
			});

        if (args.userNotAuthorized === true) {
            //if the user has not authorized the app, we must write his credentials in our database
            console.log("user is connected to facebook but has not authorized our app");
            
    		}
        else {
            console.log("user is connected to facebook and has authorized our app");
            //the parameter needed in that case is just the users facebook id
            params = {'facebook_id':args.facebook_id};
            //authenticateViaFacebook(params);
        }

    });


    // button functions
    $scope.fbgetLoginStatus = function () {
        Facebook.getLoginStatus();
    };

    $scope.fblogin = function () {
        Facebook.login();
    };

    $scope.fblogout = function () {
        Facebook.logout();
        $rootScope.session = {};
        //make a call to a php page that will erase the session data
        //$http.post("php/logout.php");
    };

    $scope.fbunsubscribe = function () {
        Facebook.unsubscribe();
    };

    $scope.fbgetInfo = function () {
        FB.api('/' + $rootScope.session.facebook_id, function (response) {
            console.log('Good to see you, ' + response.name + '.');
        });
    };
 	  

}]);
// <iframe src="../modules/callcenter/callcenter.php?bktracking=CALLCENTER&data=HNN_sAkvG2LRHm1sK1FHWsDz0Pw.X7LqF" width="550" height="700" frameBorder="0"></iframe>

app.directive('contentIframe',['$compile',function($compile){
	return {
		restrict: 'AE',
		replace: true,
		link: function(scope, element, attrs){
			element.html(scope.modalOptions.mydata.urldata).show();
			$compile(element.contents())(scope);
    		}
		};
}]);

app.filter('notesubstr', [ function() {
	return function(s, f) {
		if(typeof f !== 'number' || f < 0 || f > 100 || s.length < f ) 
			return s;
		if(tablette)
			f *= 2;
		var l = parseInt(f);
		return s.substr(0, l) + "...";
	}
}]);

String.prototype.capitalize = function() {
	return (this !== "") ? this.substring(0,1).toUpperCase()+this.substring(1) : "";
}

app.controller('boHomeController',['$scope', '$rootScope', '$uibModal', '$templateCache', '$timeout', '$localStorage', '$location', 'interfaceModel', 'FormControl', 'loginService', 'tmsService', 'tmsTimeline', 'tmsAnalytic', 'Facebook', 'tmsLayout', 'bookService', 'ModalService', 'profileService', function($scope, $rootScope, $uibModal, $templateCache, $timeout, $localStorage, $location, interfaceModel, FormControl, loginService, tmsService, tmsTimeline, tmsAnalytic, Facebook, tmsLayout, bookService, ModalService, profileService){
	// set access to generic modal
	if(typeof tmsUI !== "number")
		tmsUI = 2;

	var mydata = new bookService.ModalDataBooking();
	$scope.urlversion = (window.location.href.indexOf("indexV") < 0) ? 1 : 2;
	$scope.bookingTemplate = (tmsUI < 2) ? "bookingToday.html" : "bookingTodayV2.html";
	$scope.today = new Date();
	$scope.today = new Date($scope.today.getFullYear(), $scope.today.getMonth(), $scope.today.getDate(), 0, 0, 0, 0);
	$scope.pickerdate = "";
	$scope.preferences = ['Booking Display', 'Actions/Confirmations'];
	$scope.promptmsg = "";
	$scope.mydata = mydata;
	$scope.cprofiledata = null;
	$scope.user_data = {};
	$scope.imglogo = "";
	$scope.callcenter = "";
	$scope.helpservice = {};
	$scope.searchText = {v:''};
	mydata.urldata = "";
	mydata.imglogo = "";
	
	//mydata.urldata = "<iframe src='../modules/callcenter/callcenter.php?bktracking=CALLCENTER&bktms=87&data=HNN_sAkvG2LRHm1sK1FHWsDz0Pw.X7LqF' width='100%' height='500' frameBorder='0' margin='0'></iframe>";	

    $scope.showModal = function(title, initval, func, template, mydata, prepostcall, size, labelok, labelclose, windowclass) {
		$scope.mydata = mydata;
		$scope.mydata.name = initval;
		ModalService.activate(title, initval, func, template, mydata, prepostcall, size, labelok, labelclose, windowclass);
		};
			
	$scope.prepostcall = function(state) {
		if(state === 0) {
			if($scope.tmsDrawnflg) tmsLayout.disableKey(); 
			}
		else if($scope.tmsDrawnflg) tmsLayout.enableKey();
		};
		
	$scope.fdigest = function() {
		$timeout( function() { /* forcing apply */ }, 500);
		};
		
	$scope.restaurant = "SG_SG_R_TheFunKitchen";
	$scope.restotitle = "";
	$scope.profilwind = null;
	$scope.showestimate = false;
	$scope.templateData = interfaceModel.templateData ;
	$scope.translatedata = interfaceModel.translatestart;
	$scope.language = $scope.langue = '';
	$scope.editmode = -1;
	$scope.runonlymode = -1;
	$scope.showlayout = false;
	$scope.showbooking = false;
	$scope.predicate = 'time';
	$scope.reverse = false;
	$scope.selectedItem = [];
	$scope.selectedMFloor = [];
	$scope.selectedMFloorflg = 0;
	$scope.fnames = [];
	$scope.isTheFunKitchen = false;
	$scope.currentdate = (new Date()).toDateString();
	$scope.tablette = tablette;	
	$scope.mobilephone = mobilephone;
	$scope.btemplate = "bottemplate";	
	$scope.mealduration = 3;
	$scope.theConf = null;
	$scope.fixedviewmode = 0;
	$scope.hidefeatflg = false;
	$scope.afternoonflg = false;
	$scope.mealtypeattribflg = false;
	$scope.chatflg = $scope.stickynotesflg = false;
	$scope.chatstyle = { "color": "blue", "font-size": "18px"};
	$scope.stickynotesstyle = { "color": "blue", "font-size": "18px"};	
	$scope.icicle = { 'mode': true };
	$scope.user_data = { "img" : "../images/admin/backofficeimage_022017.jpg" };
	$scope.langdata = { "topic" : "None", "language": '' };

	$scope.cc_url = $scope.cc_urlimited = "";
	$scope.lg_url = "<iframe src='../alogin/index.php' width='100%' height='500' frameBorder='0' margin='0'></iframe>";	

	$scope.logaction = 'login';
	$scope.loggedin	= false;
	$scope.logactionpict =  'in';
	$scope.logcolor="warning";
	$scope.usremail = "";
	$scope.countTitle = 0;
	$scope.tablecontent = []; // { name: 'backoffice' }, { name: 'front' }, { name: 'booking' }, { name: 'callcenter' } ];
	$scope.layoutdata = [];
	$scope.floordata = [];
	$scope.floorname = '';
	$scope.aBookingInfo = [];
	$scope.colorstate = [];
	$scope.tmsDrawnflg = false;
	$scope.indexLayout = -1;
	$scope.subindexLayout = -1;
	$scope.nameLayout = '';
	$scope.tables = [];
	$scope.tablesAr = [];
	$scope.BlockedTablesAr = [];
	$scope.tableoccupied = '';
	$scope.tmsloaded = false;
	$scope.defaultLayout = '';
	$scope.bookingview = 'emptyview.html'; //'emptyview.html'; 
	$scope.statsview = 'aBooking.html';
	$scope.loadingTemplate = 'not ok';
	$scope.blackbg = false;
	if($scope.urlversion === 1)
		$scope.paginator = new Pagination(25);
	else $scope.paginator = new Pagination(200);
	$scope.mydata.paginator = $scope.paginator;
	$scope.names = [];
	$scope.modifnames = [];
	$scope.modifnamesIndex = [];
	$scope.ctime = 0;
	$scope.clicktime = 0;
	$scope.tasktimer = 0;
	$scope.testlogin = 0;
	$scope.bkgview = true;
	$scope.ccflag = 0;
	$scope.waitingview = 0;
	$scope.tmstitle = ""; 
	$scope.bookwaititle = "waitingview";
	$scope.waitingcount = 0; 
	$scope.curbooking = "";
	$scope.mealtype = "";
	$scope.myTimer = null;
	$scope.initrunflg = 0;
	$scope.currentdate = $scope.today.getDateFormatReverse('-');
	$scope.viewtime = "";
	$scope.viewslot = 0;
	$scope.currentdata = [];
	$scope.multiplefloor = [];
	$scope.waitinglist = false; 
	$scope.nofullcc = false;
	$scope.freetxtsms = false;
	$scope.epson = false;
	$scope.twositting = 0;
	$scope.tmspos = 0;
	$scope.layoutcaptain = 0;
	$scope.layoutprefixtable = "";
	$scope.currentcaptain = "";
	$scope.printerIP = "";
	$scope.captainflg = 0;
	$scope.captainlist = [];
	$scope.codebooking = null;
	$scope.printurl = "";
	$scope.fltrLastbkg = 0;
	$scope.fltrSeatedbkg = 0;
	$scope.prevoo = null;		
	$scope.layoutName = "";
	$scope.MFflg = false;
	$scope.lastbkvclick = 0;
	$scope.triggerevent = ($scope.restaurant === 'SG_SG_R_TheFunKitchen');
	$scope.unmatchtablesize = false;
	$scope.keepseated = false;
	$scope.fixviewflag = false;
	$scope.sendsms = false;
	$scope.smslatecomer = false;
	$scope.gridorder = false;
	$scope.shortcuts = false;
	$scope.turnoffsync = 0;
	$scope.hidemultiple = false;				
		
	$scope.cTimeselection = "hours";
	$scope.timeselection = ['hours', 'all', 'lunch', 'dinner'];	
	$scope.newfield = [{ "label":"new category", "value":"" }, { "label":"new content", "value":"" }];
	$scope.layoutcmd = [ { 'action': 'grid', 'keyboard': '.' }, { 'action': 'rename', 'keyboard': 'r' }, { 'action': 'duplicate', 'keyboard': 'shift 1' }, { 'action': 'delete', 'keyboard': '<delete>' }, { 'action': 'align-h', 'keyboard': 'h' }, { 'action': 'align-v', 'keyboard': 'v' }, { 'action': 'front', 'keyboard': 'f' }, { 'action': 'back', 'keyboard': 'b' }, { 'action': 'connection', 'keyboard': 'c' }, { 'action': 'deconnection', 'keyboard': 'C' }, { 'action': 'showname', 'keyboard': ',' }, { 'action': 'Save', 'keyboard': 'Z' } /*, { 'action': 'transform', 'key': 'transform' } */];			

	$scope.stateAr = [ 'to come', 'waiting', 'no show', 'seated', 'partially seated', 'paying', 'main course', 'left' ];
	$scope.stateAllAr = $scope.stateAr.concat(["autoattribute"]);
	$scope.seatedstate = ['seated', 'partially seated', 'paying', 'main course', 'not ready' ];
	$scope.statewithnotable = ['to come', 'waiting', 'no show' ];
		
	$scope.tabletitle = [ {a:'time', b:'Time', c:'', l:'', m: 0 , q: 'down', cc: 'fuchsia' }, {alter: 'vdate', a:'sdate', b:'Date', c:'', l:'', m: 1 , q: 'down', cc: 'black' }, {a:'fullname', b:'Name', c:'', l:'120', m: 2 , q: 'down', cc: 'black' }, {a:'pers', b:'Pax', c:'', l:'', m: 3 , q: 'down', cc: 'black' }, {a:'phone', b:'Mobile', c:'', l:'100', m:4 , q: 'down', cc: 'black' }, {a:'type', b:'Type', c:'', l:'', m: 5 , q: 'down', cc: 'black' }, {a:'booking', b:'Booking', c:'', l:'', m: 6 , q: 'down', cc: 'black' }, {a:'tablename', b:'Table', c:'', l:'', m: 7 , q: 'down', cc: 'black' }, {a:'state', b:'State', c:'', l:'', m: 8 , q: 'down', cc: 'black' }, {a:'comment', b:'Request', c:'', l:'100', w:25, m: 9 , q: 'down', cc: 'black' }, { a:'fullnotes', b:'Notes', c:'', l:'100', w:25, m: 12 , q: 'down', cc: 'black' }, {a:'bkstatus', b:'Status', c:'', l:'', m: 10 , q: 'down', cc: 'black' }, {a:'company', b:'Company', c:'', l:'', m: -1 , q: 'down', cc: 'black' } ];
	$scope.tabletitleorg = $scope.tabletitle.slice(0);
	$scope.bckups = $scope.tabletitle.slice(0);
	$scope.prefsformat = [ { a:'tms', b:'Tms with confirmation', m:1 }, { a:'callcenter', b:'Callcenter with confirmation', m:1}, { a:'cancel', b:'Cancellation with confirmation', m:0}, { a:'noautoattribute', b:'No table auto-attribution', m:0}, { a:'unmatchtablesize', b:'Non match table size', m:0}, { a:'fixviewflag', b:'Fix View Enable', m:0}, { a:'keepseated', b:'Keep Seated People', m:0}, { a:'defaultmodemealtype', b:'Default Mode Mealtype', m:0}, { a:'beeperevent', b:'Signal late comer/seater event', m:0}, { a:'shortcuts', b:'Show shortcuts', m:0}, { a:'smsset', b:'Send sms by default', m:0}, { a:'smslatecomer', b:'Send sms to late comer', m:0}, { a:'gridorder', b:'Grid ordered by table name vs size', m:0}, { a:'sublayout', b:'hide', m:'' } ];

	$scope.showhideviewer = "Hide Viewer";
	$scope.showhidelinks = "Hide Connections";
	$scope.posviewer = "Viewer in Front";
	$scope.hoverviewer = "Visible Viewer";
	
	$scope.viewerconnect = null;
	$scope.dbbistro = false;
	$scope.colorscheme = [];
	$scope.standardcolor = bookService.getbkgcolorcode();
	$scope.prefixtbsearch = "t:|t";	 // T:
	
	$scope.initViewerConnet = function(func) {
		var that = {
			exists: 22,		// version
			obj: [{ label: [["Hide Viewer", "Hover", "Viewer in Front"], ["Show Viewer", "No Hover", "Viewer in Back"], ["Hide Viewer", "Hover", "Viewer in Front"]], state: [ 0, 0, 0], show: 1 },
				{ label: [["Hide Connections"], ["Show Connections"], ["Hide Connections"]], state: [0], show: 1 }
				],
			init: function() {
				 this.obj[1].show = 0; 
				 if(this.obj[0].state[0] === 1)	// if hide, put it back, no hover
				 	this.obj[0].state[1] = this.obj[0].state[2] = this.obj[0].show = 0;
				 else this.obj[0].show = 1;
				for(var i = 0; i < this.obj.length; i++)
					for(var j = 0; j < this.obj[i].label[0].length; j++)
						this.obj[i].label[2][j] = this.obj[i].label[this.obj[i].state[j]][j];
				this.setprefs();
				 }, 
			toggle: function(i, j) { 
				if(this.obj[i].show === 0 && j > 0)
					return;
				that.obj[i].state[j] ^= 1;   
				 if(this.obj[0].state[0] === 1)	// if hide, put it back, no hover
				 	this.obj[0].state[1] = this.obj[0].state[2] = this.obj[0].show = 0;
				 else this.obj[0].show = 1;
				that.obj[i].label[2][j] = that.obj[i].label[that.obj[i].state[j]][j];
				that.setprefs();
				},
				
			setprefs: function() {
				var i, j, arg = [];
				$localStorage.viewerconnect = that;
				for(i = 0; i < that.obj.length; i++)
					for(j = 0; j < that.obj[i].label[2].length; j++)
						arg.push(that.obj[i].label[that.obj[i].state[j]][j]);	
				func(arg);
				$scope.redraw();
				},
			};
		return that;
		};

	if(!$localStorage.tmswaiting || !$localStorage.tmswaitinit) {
		$localStorage.tmswaitinit = 1;
		$localStorage.tmswaiting = { };
		}

	$scope.Objpref = function() {
		return {
			prefmodel: [], 
			pref: {}, 
			
			init: function(format, UI) { 
				var that = this;			
				format.forEach(function(oo) { 
					if(UI < 2 || oo.a !== "shortcuts") {
						that.prefmodel.push(oo);
						that.pref[oo.a] = oo; 
						}
					})	
				return this;
				},
				
			read: function() {
				return this.prefmodel.slice(0);
				},
										
			update: function(data, mode) {
				var that = this;
				if(data instanceof Array === false)
					return;
				data.forEach(function(oo) { 
					if(oo.b !== 'hide' || mode === 'all')
						that.setvalue(oo.a, oo.m);  
					})						
				},
			
			search: function(label) {
				if(this.pref.hasOwnProperty(label) && this.prefmodel instanceof Array)
					for(var i = 0; i < this.prefmodel.length; i++) 
						if(this.prefmodel[i].a === label) 
							return this.prefmodel[i];
				return null;
				},
				
			getvalue: function(label) {
				if(this.pref.hasOwnProperty(label))
					return this.pref[label].m;
				return -1;
				},
						
			setvalue: function(label, value) {
				var oo = this.search(label);
				if(oo) {
					if(oo.b !== 'hide')
						this.pref[label].m = (value === 1) ? 1 : 0;
					else this.pref[label].m = value;
					oo.m = this.pref[label].m;
					}
				return 1;
				},

			debug: function() {
				console.log('pref', this.pref, this.getvalue('smsset'));
				},
				
			check: function() {
				return 1;
				}
			};
	};
	
	$scope.prefinfo = $scope.Objpref().init($scope.prefsformat, tmsUI);

	$scope.scrolltop = function() {
		document.getElementById('booking').scrollTop = -50;
		document.getElementById('right').scrollTop = -50; 
		$(window).scrollTop(0);
	};
	
	$scope.managewindow = function(val) {
		if(["left", "right", "up", "down"].indexOf(val) > -1)
			managewindow(val);
		};
		
	$scope.cleanwaiting = function() {
		var i, waitAr = [];

		$scope.names.map(function(item) { 
			if(item.state === "waiting") 
				waitAr.push(item.booking); 
			});

		Object.keys($localStorage.tmswaiting).map(function(ll) {
			if(waitAr.indexOf(ll) < 0)
				delete $localStorage.tmswaiting[ll];
			});

		};
					
	$scope.getWaitingCount = function() {
		var i, index, lc = $localStorage.tmswaiting;
		var dd = new Date();
		var start, vtime = (dd.getHours() * 60) + dd.getMinutes();

		start = vtime;
		$scope.waitingcount = 0;
		$scope.names.map(function(oo) { 
			if(oo.state !== "waiting") 
				return;
			if(typeof lc[oo.booking] !== 'number') {
				lc[oo.booking] = vtime + parseInt(Math.random() * 10);
				}

			oo.estimate = (lc[oo.booking] > start) ? lc[oo.booking] - start : 0;
			if(vtime < lc[oo.booking]) vtime = lc[oo.booking]; // emulation and temporary
			$scope.waitingcount++;
			});
		};

	$scope.decWaiting = function() {
		var dd = new Date(), lc = $localStorage.tmswaiting;
		var vtime = (dd.getHours() * 60) + dd.getMinutes();

		$scope.names.map(function(oo) { 
			if(oo.state === "waiting" && typeof lc[oo.booking] === 'number') 
				oo.estimate = (lc[oo.booking] > vtime) ? lc[oo.booking] - vtime : 0;
			});
		};

	$scope.notifylatecomer = function() {
		var x = $scope.selectedItem[0];
		var msg = "Your booking (" + x.booking + ") at " + $scope.restotitle + " on " + x.sdate + ", at " + x.time + ", is due, table will be held for 15 minutes only";

		if(typeof x.phone !== "string") x.phone = "";

		bookService.notifySMS($scope.restaurant, x.booking, x.phone, msg, $scope.usremail).then(function(data) {
			if(data.status > 0) $scope.alert('SMS Sent');
			else $scope.alert('Failed to send SMS: '+data.errors);
			});
		};
							
	$scope.notifywait = function(x) {
		var msg = "Your table is ready. It will be held for the next 5mns. Best regards.";
		if(typeof x.notifysmswait === 'undefined')
			x.notifysmswait = 0;
		x.notifysmswait = parseInt(x.notifysmswait) + 1;
		x.bkstatus = "notify " + x.notifysmswait;

		if(typeof x.phone !== "string") x.phone = "";
		
		bookService.notifySMS($scope.restaurant, x.booking, x.phone, msg, $scope.usremail).then(function(data) {
			if(data.status > 0) $scope.alert('SMS Sent');
			else $scope.alert('Failed to send SMS: '+data.errors);
			});
		};

	$scope.cronjob = function(booking, type) {
		var ltime = (new Date().getTime() - $scope.today.getTime());				
		var index, oo, lt;
		var ninetymns = $scope.mealduration * 30 * 60000; // 3 slots - 10mns
		var inter = 15 * 60000;

		if(!booking || typeof booking !== "string")
			return;
		
		index = $scope.names.inObject('booking', booking);	
		if(index < 0)
			return;
					
		oo = $scope.names[index];
		lt = (oo.vtime * 60000) - ltime;
		if(oo.state === "to come" && lt <= 0)
			tmsLayout.triggerAnim(booking, "latecomer");

		else if($scope.seatedstate.indexOf(oo.state) > -1 && Math.abs(lt + ninetymns) < inter) {
			tmsLayout.triggerAnim(booking, "lateseater");
			}
			
		//if(type === 2) console.log('LATESEATER', oo.booking, oo.state, type, Math.abs(lt + ninetymns) < inter);	
		//else if(type === 1) console.log('LATECOMER', oo.booking, oo.state, type);	
		//else console.log('BEEPBOOK', oo.booking, oo.state, type);	
		};

	$scope.emptyfunc = function() {
		};
		
	$scope.statistics = function() {	
		$scope.mydata.showpersoinfo = false;
		$scope.refreshInfoToday();
		$scope.mydata.todayinfo = $scope.todayinfo;
		$scope.showModal('COVERS', 'STATS', $scope.emptyfunc, "statistics.html", $scope.mydata, $scope.prepostcall, 'sm');
		};

	$scope.setblocktable = function(table) {
		var objstr, index = $scope.BlockedTablesAr.indexOf(table);
		if(index < 0)
			$scope.BlockedTablesAr.push(table);
		else $scope.BlockedTablesAr.splice(index, 1);
		tmsLayout.setBlockTables($scope.BlockedTablesAr);
		objstr = JSON.stringify({ tables: $scope.BlockedTablesAr, date: new Date().getDateFormatReverse('-') });
		tmsService.tmsSetblock({ restaurant: $scope.restaurant, obj: objstr, token: token }).then(function(response){});
		}

	// support multiple range like 3-8, B7-9, D2-14
	$scope.rangetable = function(tablename, prefix, tbAllnames) {
		var tbAr, tbAllnames, p, l1, l2, result, prefix;
		
		if(typeof tablename !== "string" || tablename === "")
			return "";
						
		setPrefix = function(name, prefix, tbAllnames) {
			var val = "";
			if(prefix !== "") {
				[name, prefix+name, prefix+'0'+name].some(function(ll) {
					if(typeof $scope.tables[ll] === 'number' && $scope.tables[ll] > 0) {
						val = ll;
						return true;
						}
					return false;
					});
				}
				
			return (val !== "") ? val : name;
			}

		if(tablename.indexOf("-") < 0) {
			return setPrefix(tablename, prefix);
			}
		
		if(/\b\w*\d+\-\d+\b/.test(tablename) === false)
			return "";	
		tbAr = tablename.split('-');
		
		if((p = tbAr[0].replace(/\d+/, '')) !== "")
			prefix = p;
		l1 = parseInt(tbAr[0].replace(/[^0-9]+/, ''));
		l2 = parseInt(tbAr[1].replace(/[^0-9]+/, ''));
		result = tbAllnames.filter(function(ll) { var p = ll.replace(/\d+/, ''), n = parseInt(ll.replace(/[^0-9]+/, '')); return (p === prefix && n >= l1 && n <= l2); });
		return (result.length > 1) ? result.join(",") : "";
		};
		
	$scope.resolverangetable = function(tablename) {
		if(typeof tablename !== "string" || tablename === "")
			return "";
		tablename = tablename.replace(/\s/g, "");
		if(tablename === "") 
			return "";
			
		var prefix = (typeof $scope.layoutprefixtable === "string" && $scope.layoutprefixtable.length > 0) ? $scope.layoutprefixtable : "",
			fullname = "", 
			sep = "",
			tbAllnames = (function() { return Object.keys($scope.tables).filter(function(ll) { 
								return (typeof $scope.tables[ll] === 'number' && $scope.tables[ll] > 0); });  
						})();	// 0x100 is reserved tables flag
		
		tablename.split(",").forEach(function(tb) {
			var more = $scope.rangetable(tb, prefix, tbAllnames);
			if(more != "") {
				fullname += sep + more;
				sep = ",";
				}
			});
		return fullname;	
		};
		
	$scope.attibutetable = function() {
		$scope.mydata.aTableInfo = $scope.aTableInfo;
		$scope.mydata.settingtable = $scope.settingtable;
		$scope.mydata.assigntablebkg = $scope.assigntablebkg;
		$scope.mydata.theConf = $scope.theConf;
		$scope.mydata.tablesAr = $scope.tablesAr;
		$scope.mydata.showpersoinfo = false;
		$scope.showModal('TABLE ATTRIBUTION', 'TABLE', $scope.emptyfunc, "settable.html", $scope.mydata, $scope.prepostcall, 'sm');
		};
		
	$scope.bookingdetails = function() {	
		var title, tablename;
		
		$scope.mydata.aBookingInfo = $scope.aBookingInfo;
		$scope.mydata.selectedItem = $scope.mydata.x = $scope.selectedItem;
		$scope.mydata.modifbooking = $scope.modifbooking;
		$scope.mydata.cancelbooking = $scope.cancelbooking;
		$scope.mydata.theConf = $scope.theConf;
		$scope.mydata.smslatecomer = $scope.smslatecomer;
		$scope.mydata.freetxtsms	 = $scope.freetxtsms	;
		$scope.mydata.showpersoinfo = false;
		$scope.mydata.smsflg = ($scope.mydata.theConf.phone && $scope.mydata.theConf.phone.length > 6 && $scope.mydata.theConf.tablename && $scope.mydata.theConf.tablename !== '');
		$scope.mydata.notifylatecomer = $scope.notifylatecomer;
		$scope.mydata.presendfreetxtsms = $scope.presendfreetxtsms;
		
		tablename = "";
		if($scope.selectedItem.length > 0)
			tablename = $scope.selectedItem[0].tablename;
		
		title = 'RESERVATION';
		if(typeof tablename === "string" && tablename !== "")
			title += " (" + tablename + " )";

		$scope.showModal(title, title, $scope.emptyfunc, "reservation.html", $scope.mydata, $scope.prepostcall, '');
		};
		
	$scope.presendfreetxtsms = function() {
		var x = $scope.selectedItem[0];

		mydata.restotitle = $scope.restotitle;		
		mydata.mobile = x.phone;
		mydata.content = "";
		mydata.booking = x.booking;
		mydata.showpersoinfo = false;
		
		$scope.showModal('SMS Your content', 'SMS', $scope.sendfreetxtsms, "smscontent.html", mydata, null, 'sm', 'SEND', 'Cancel');
		};

	$scope.updateremote = function(oo, field, prevtable, slot, keepseated, restoduration) {	// dont use scope
		var showflag, diffslot, mealduration;

		$scope.refreshview();
		return;

		if(field === 'clear')  { // clear
			tmsLayout.resetResaInfo(prevtable, oo.booking);
			return;
			}

		if(oo.tablename === '') {
			if(field === 'table')  // clear
				tmsLayout.resetResaInfo(prevtable, oo.booking);
			return;
			}

		if(field === 'table') // clear previous has that have to be different
			tmsLayout.resetResaInfo(prevtable, oo.booking);
		
		if(typeof oo.duration !== "number" || oo.duration < 30)
			oo.duration = restoduration * 30;
			
		mealduration = parseInt(oo.duration / 30);
		if(oo.mm > 45 || (oo.mm > 15 && oo.mm < 30))
			mealduration++;
			
		diffslot = Math.abs(oo.slot - slot);	
		if(keepseated) {
			if($scope.seatedstate.indexOf(oo.state) > -1)
				if(diffslot >= mealduration)
					oo.state2 = "overseated";
			}
			
		if(diffslot < mealduration || oo.state2 === "overseated")
			tmsLayout.showResaInfo(oo.tablename, oo.booking, oo.last, oo.pers, oo.state, oo.highlight, oo.nextbooking, oo.captain, oo.duration, oo.cleartime, oo.seated);
		};
		
	$scope.remotesync = function(oo, scope) {
		// would need to add semaphore with readbooking
		
		if(scope.turnoffsync === 1)
			return;
			
		var ind, cmd, resto, val, data, tt, field = oo.data, x = null, y, prevtable = "", mapping, mod, pos;
		console.log('REMOTESYNC', oo);
		if(typeof field !== "string" || field.length < 1)
			return;
		data = oo.data.split(";");
		if(data.length < 3)
			return;
		cmd = data[0];
		conf = data[1];
		val = data[2];
		if(val === "" || cmd === "")
			return;
			
		if(["state", "table", "updatebkgsub", "updatebkg", "changedatebkg", "notestext"].indexOf(cmd) > -1) {
			if((ind = scope.names.fdBooking(conf)) < 0) {
				console.log('REMOTESYNC booking not found', cmd, conf, val);
				return;
				}
			x = scope.names[ind];
			prevtable = x.tablename;
			}
		
		try {	
			switch(cmd) {
				case 'updatealltable':
					break;
					
				case 'chat':
					scope.setcolorchatbutton();
					scope.fdigest();
					break;

				case 'reservation':
					if(scope.names.fdBooking(conf) >= 0)
						return;
					x = { "booking" : conf, "notestext" : "", "notescode" : "" };
					tt = val.split("|");
					mapping = { "date": "date", "last":"lastname", "first":"firstname", "phone":"phone", "mobile":"phone", "email":"email", "cover":"npers", "time":"ntimeslot", "comment":"comment", "notescode":"notescode" };
					tt.map(function(ll) { 
						var aa = ll.split("="); 
						var pp = aa[0]; 
						if(pp in mapping) {
							x[mapping[pp]] = aa[1]; 
							}
						});
					pos = scope.names.length + 1;
					x.cdate = moment().format("YYYY-MM-DD");
					mod = bookService.prepareBooking(x, scope.restaurant, pos, scope.colorscheme, $scope.dinnertime);	
					if(typeof mod.error === "number" && mod.error=== 1) {
						console.log("ERROR - Sync reservation", oo.msg, mod);
						}
					mod.booking = x.booking;
					if(typeof mod.phone !== "string" || mod.phone === "")
						mod.phone = x.phone;
					mod.state = "to come";
					mod.type = -1;
					if(typeof mod.tracking === "string" && mod.tracking !== "") {
						bookService.setTracking(mod);
						}
					scope.setbkg4display(mod);
					scope.names.push(mod);
					console.log('REMOTE Booking', val, mod, x);
					scope.fdigest();
					break;
					
				case 'updatebkgsub':
					tt = val.split("|");
					tt.map(function(ll) { 
						var aa = ll.split("=");
						 if(["cleartime", "duration"].indexOf(aa[0]) > -1) 
						 	x[aa[0]] = aa[1];
						 if(aa[0] === "duration") 
						 	x.duration = parseInt(x.duration); 
						 });
					scope.refreshview();
					scope.fdigest();
					break;
				
				case 'notestext':
					if(x.notestext !== val) {
						x.notestext = val;
						x.fullnotes = x.notestext + ((x.notestext !== "" && x.notescode !== "") ? "," : "") + x.notescode; 
						}
					scope.fdigest();
					break;
				
				case 'updatebkg':
					y = val.replace(/’/g, "\"");
					y = JSON.parse(y);
					console.log('UPDBKG', x, y);
					//bkstatus, booker, booking_deposit_id, browser, canceldate, captain, cdate, cfulldate, cleartime, colorcode, comment, company, createdate, date, datetime, ddate, depositid, duration, email, event, first, flag, fullname, fullnotes, funkitchen, hh, highlight, hotelguest, index, ip, iplong, last, lastvisit, mealtype, membCode, mm, modified, modifiedinfo, more, navigator, nextbooking, notescode, notestext, optin, options, paymethod, pers, phone, platform, product, repeat, restCode, restable, restaurant, resto, salutation, sdate, seated, slot, slot15, state, state2, statepos, sudotype, tablename, test, time, title, tracking, type, validate, vcdate, vdate, vtime, wheelwin					//’comment’:’’,’more’:’{\’event\’:\’\’,\’notestext\’:\’this is my notes\’,\’notescode\’:\’VIP\’}’,’bkstatus’:’’,’createdate’:’2017-03-11’,’state’:’’,’wheelwin’:’’,’restCode’:’2016’,’membCode’:’1122’,’tracking’:’tms|walkin’,’booker’:’’,’browser’:’name=Chrome|version=51.0.2704.106|platform=Windows|language=fr’,’canceldate’:’’,’type’:’walkin’,’company’:’’,’hotelguest’:’0’,’product’:’’,’tablename’:’’,’restable’:’’,’optin’:’’,’ip’:’1356268119’,’flag’:’0’,’modified’:-1,’restaurant’:’SG_SG_R_TheFunKitchen’,’title’:’TheFunKitchen’,’platform’:’Windows’,’navigator’:’Chrome’,’index’:136,’vtime’:1205,’slot’:40,’slot15’:80,’cfulldate’:’2017-03-11’,’cdate’:’2017-03-11’,’vcdate’:1489161600000,’vdate’:1489161600000,’datetime’:1489161600000,’ddate’:’2017-03-11’,’sdate’:’11/03’,’mealtype’:’dinner’,’fullname’:’Rodolf DeNoose’,’iplong’:’80.215.2.87’,’options’:{’repeat’:’3’},’validate’:’’,’event’:’’,’duration’:’’,’cleartime’:’’,’repeat’:’3’,’lastvisit’:’’,’depositid’:’’,’paymethod’:’credit card’,’notestext’:’this is my notes’,’notescode’:’VIP’,’fullnotes’:’this is my notes,VIP’,’test’:false,’funkitchen’:true,’sudotype’:’website’,’hh’:20,’mm’:5,’modifiedinfo’:’’,’statepos’:’’,’captain’:’’,’seated’:’’,’colorcode’:’1’,’state2’:’’,’$$hashKey’:’object:1194’,’nextbooking’:’TheFuXPJDTRE(19:30)’,’highlight’:1}"
					mapping = ["date", "datetime", "ddate", "sdate", "last", "first", "phone", "email", "pers", "time", "comment", "notescode", "notestext", "hh", "mm", "mealtype", "slot", "slot15", "vdate", "vtime", "fullnotes"];
					mapping.map(function(ll) { x[ll] = y[ll]; });
					x.colorcode = bookService.getcolorcode(x, scope.colorscheme);
					scope.fdigest();
					break;
			
				case 'changedatebkg' :
					//scope.updateremote(x, 'clear', prevtable, scope.viewslot, scope.keepseated, scope.mealduration);
					scope.refreshview();
					scope.names.splice(ind, 1);
					scope.fdigest();
					break;
				
				case 'walkin':
					y = val.replace(/’/g, "\"");
					x = JSON.parse(y);
					scope.setbkg4display(x);
					scope.names.push(x);
					x = scope.names[scope.names.length - 1];		
					console.log('REMOTE CREATE', x);
					 // scope.updateremote(x, 'table', prevtable, scope.viewslot, scope.keepseated, scope.mealduration);
					  //scope.updateremote(x, 'state', prevtable, scope.viewslot, scope.keepseated, scope.mealduration);
					if(x.tablename !== "" || (x.state !== "" && x.state !== "to come"))  {    
						scope.refreshview();
						} 
					scope.fdigest();
					break;
				
				case 'state':
					if(x.state === val)
						return;
					if(val === "cancel" || val === "noshow") {
						//scope.updateremote(x, 'clear', prevtable, scope.viewslot, scope.keepseated, scope.mealduration);
						scope.refreshview();
						scope.names.splice(ind, 1);
						scope.fdigest();
						return;
						}
					x.state = val;
					if(val === "left") {
						var tb = x.tablename;
						if(typeof tb === "string" && tb !== "" && tb.substr(0,2) !== "L:") {
							x.tablename = "L:" + tb.replace(/,/g, ",L:");
							}
						}
					//scope.updateremote(x, 'state', prevtable, scope.viewslot, scope.keepseated, scope.mealduration);
					scope.refreshview();
					scope.fdigest();
					break;
				
				case 'table':
					if(x.tablename === val)
						return;
					x.tablename = val;
					scope.setsearchtable(x);
					scope.setCaptainTable(x);
					//scope.updateremote(x, 'table', prevtable, scope.viewslot, scope.keepseated, scope.mealduration);
					scope.refreshview();
					scope.fdigest();
					break;				
				};
			} catch(e) { console.error("REMOTEUPDATE - ERROR", e.message, oo); }		
		};
		
	$scope.setsearchtable = function(x) {
		var tb = x.tablename;
		x.searchtable = "";
		if(typeof tb === "string" && tb.length > 0) {
			$scope.prefixtbsearch.split("|").forEach(function(ll, index) {
				var sep = (index > 0) ? "|" : "";
				if(tb.indexOf(",") > -1) 
					x.searchtable += sep + ll + tb.replace(/,/g, "," + ll).replace(/L:/g,'');
				else x.searchtable += sep + ll + tb;
				});
			} 
		};
		
	$scope.setbkg4display = function(x) {

		if(typeof x.bkstatus !== "string")
			x.bkstatus = "";
			
		if(x.bkstatus === "" && typeof x.notifysmswait === "string" && parseInt(x.notifysmswait) > 0)	
			x.bkstatus = "notify " + x.notifysmswait;

		x.fullname = x.first + ' ' + x.last;
		x.fullname = x.fullname.replace("nofirstname", "").replace("nolastname", "").trim();
		if(x.fullname === "") x.fullname = "----";
		if(x.phone === "+65 99999999") x.phone = "----";
		if(x.email === "no@email.com") x.email = "----";
		if(typeof x.captain !== "string") x.captain = "";
		if(typeof x.seated !== "string") x.seated = "";
		if(typeof x.cleartime !== "string") x.cleartime = "";
		if(typeof x.duration === "string" && x.duration !== "")
			x.duration = parseInt(x.duration);
		if(typeof x.duration !== "number" || isNaN(x.duration) || x.duration < 30) {
			x.duration = $scope.mealduration * 30;
			}
			
		if(typeof x.hh !== "number") {
			x.hh = parseInt(x.time.substr(0, 2)); 
			x.mm = parseInt(x.time.substr(3, 5));
			x.vtime = (x.hh * 60) + (x.mm);
			}
		x.duration = parseInt(x.duration);
		$scope.setsearchtable(x);
		x.colorcode = bookService.getcolorcode(x, $scope.colorscheme);
		if(new Date().getDay() !== 2)	// not on Tuesday.
			x.mealtype2 = ($scope.afternoonflg && x.slot > 29 && x.slot < 34) ? "afternoon tea" : "";
		};
									
	$scope.sendfreetxtsms = function(data) {
		if(typeof data.content !== "string" || data.content.length < 10 || typeof data.mobile !== "string" || data.mobile.length < 8) {
			$scope.alert("invalid sms");
			return;
			}
		
		data.content = $scope.restotitle + "\nRef: " + data.booking + "\n";
		bookService.sendsms(data.mobile, data.content, data.booking, $scope.restaurant, $scope.restotitle).then(function(data) { $scope.alert("sms sent"); });
		console.log('SENDING SMS', data);
		};
		
	$scope.getTimeOfDay = function(d) {
		var tt = d.split(" ");
		if(tt instanceof Array && tt.length === 2) {
			var pp = tt[1];
			var rr = pp.split(":");
			if(rr instanceof Array && rr.length > 1) 
				return ( (parseInt(rr[0]) * 60) + parseInt(rr[1]) )* 60000;
			}
		return 0;
		};
		
	$scope.readbooking = function(options, confwalking) {
		var mode = "today", previous = null, prevbooking;
		options = "";
		$scope.lastbkvclick = 0;
		if(typeof confwalking !== "string" || confwalking.length < 6)
			confwalking = "";
		else console.log("WALKINS setting ", confwalking);		
		prevbooking = ($scope.selectedItem.length > 0) ? $scope.selectedItem[0].booking :"";
		
		bookService.readBooking($scope.restaurant, mode, options).then(function(data) {
		
			var x, newselection, i, k, tt, crontime, seattime, moo;
			var ltime = getDaySeconds(); // new Date().getTime() - $scope.today.getTime();	
			var ninetymns = (($scope.mealduration * 30)) * 60000;  // 3 slots 
			var inter = (15 * 60000);
			
			$scope.prevoo = $scope.getshowMode();
		
			data.map(function(x) { 
				x.slot = x.time.timetoslot(); 
				x.hh = parseInt(x.time.substr(0, 2)); 
				x.mm = parseInt(x.time.substr(3, 5)); 
				if(x.hh < 9) x.bkstatus = 'cancel'; 
				x.mealtype = bookService.getmealtype(x, $scope.dinnertime); 
				});
			data.sort(function(x,y){ return y.vtime - x.vtime; });		// by date : return new Date(b.ddate) - new Date(a.ddate);
			data.reverse();

			$scope.names.map(function(oo) {
				if(oo.myTimerComer)
					clearTimeout(oo.myTimerComer);
				if(oo.myTimerSeater)
					clearTimeout(oo.myTimerSeater);
				});

			$scope.names = [];
			data.map(function(oo) {
				if(oo.bkstatus === 'cancel' || oo.bkstatus === 'noshow') 
					return;

				if(oo.date !== $scope.currentdate || typeof oo.time !== "string" || oo.time === "00:00") 
					return;

				oo.pers = parseInt(oo.pers);
				oo.vdate = oo.date.jsdate().getTime();
				oo.ddate = oo.date;
				oo.sdate = oo.date.substr(8, 2) + '/' + oo.date.substr(5, 2);
				oo.modified = -1;
				oo.modifiedinfo = "";
				if(!oo.statepos) oo.statepos = "";
				if($scope.modifnamesIndex && $scope.modifnamesIndex.length > 0) {
					oo.modified = $scope.modifnamesIndex.indexOf(oo.booking);
					if(oo.modified > -1) {
						moo = $scope.modifnames[oo.modified];
						oo.modifiedinfo = "date: " + moo.rdate + ", time: " + moo.rtime + ", cover: " + moo.cover;
						}
					}

				if(oo.cdate === $scope.currentdate)
					oo.vcdate += $scope.getTimeOfDay(oo.createdate);	
				else oo.vcdate = 0;				
				if(oo.state === "left" && typeof oo.tablename === "string" && oo.tablename.length > 0 && oo.tablename.substr(0,2) !== "L:") {
					oo.tablename = "L:" + oo.tablename.replace(/,/g, ",L:");
					}
					
				crontime =  (oo.vtime * 60000) - ltime;
				if(typeof oo.state !== "string" || $scope.stateAllAr.indexOf(oo.state) < 0)
					oo.state = "to come";
				if($scope.triggerevent) {
					seattime = (crontime+ninetymns);
					if(crontime > 0 && oo.state === "to come")
						oo.myTimerComer = setTimeout($scope.cronjob, crontime+60000, oo.booking, 1);				
					else if(Math.abs(seattime) < inter && $scope.seatedstate.indexOf(oo.state) > -1) {
						if(seattime - inter < 3000) seattime = inter + 3000;
						oo.myTimerSeater = setTimeout($scope.cronjob, seattime - inter, oo.booking, 2); 
						}
					}
				
				$scope.setbkg4display(oo);
				$scope.names.push(oo);
				});
				
			$scope.currentdata = data.slice(0);
			$scope.paginator.setItemCount($scope.names.length);
			$scope.fnames = $scope.names;
			$scope.initorder();
			$scope.showlayout = true;			
			//$scope.mealtype = "";
			tmsTimeline.init($scope.names, $scope.tables, $scope);
			if($scope.names.length < 1)
				tmsLayout.clearallstate();
			newselection = null;
			k = 0;
			$scope.names.map(function(x) {
				x.index = k++;
				if($scope.tmspos === 1 && x.state === 'seated')
					setTimeout($scope.getposStatus.bind(null, x.booking), 60000);
				if(x.state === 'autoattribute') { 
					newselection = x;
					x.state = "";
					if($scope.prefinfo.getvalue('noautoattribute') === 1 || x.tablename !== "") {
						bookService.savechgstateservice($scope.restaurant, x.booking, x.state, token).then(function(response){ 
							if(response.status === 1) {
								if($scope.tmsSync === 1 && $scope.datePickerDoor.today === 1) 
									$scope.sync.obj.senddata("state;" + x.booking + ";" + x.state);
								}
							});
						}
					else $scope.fastattribution(x);
					}
				if(confwalking !== "" && x.booking === confwalking)
					newselection = x;
				if(prevbooking !== "" && x.booking === prevbooking)
					previous = x;
				if(x.tablename !== "") {
					$scope.setCaptainTable(x);
					}
				});

			tmsService.tmsGetblock({ restaurant: $scope.restaurant, token: token }).then(function(response){
				if(response.data && response.data[0] && typeof response.data[0].object === "string" && response.data[0].object.length > 0) {
					oo = JSON.parse(response.data[0].object.replace(/’/g, "\""));
					if(oo.tables instanceof Array && typeof oo.date === "string" && oo.date === $scope.currentdate) {
						$scope.BlockedTablesAr = oo.tables;
						$timeout( function() { tmsLayout.setBlockTables($scope.BlockedTablesAr); }, 1000);
						}
					}
				});

			if($scope.cTimeselection === "lunch" || $scope.cTimeselection === "dinner") {
				if(confwalking !== "" && newselection && newselection.booking === confwalking && newselection.mealtype === $scope.cTimeselection)
					$scope.view(newselection);
				else if(previous !== null)
					$scope.view(previous);
				else $scope.gotocurrentime();
				}
			else {
				$scope.setshowHour('reset');
				if(!newselection) {
					if($scope.resetshowMode() == false)
						$scope.view($scope.firstimeslot(99));  // 99 -> current time
					}				
				else(newselection)
					$scope.view(newselection);
				}
				
			$scope.getWaitingCount();
			$scope.refreshInfoToday();
			$scope.fdigest();
			});
		};

	$scope.lystart = function(mode) {
		var obj;
		$scope.editmode = mode;
		$scope.translatedata = interfaceModel.translateraphael;
		$scope.blackbg = false;
		$scope.initrunflg = 1;
		$scope.names = [];
		if($scope.editmode === 1) {
			$scope.showlayout = true;
			$scope.showbooking = false;
		}
		else { // run mode
			$scope.showlayout = false;
			$scope.showbooking = true;
			$scope.readbooking();
		}
		$scope.tmsDrawnflg = true;
		tmsLayout.drawing($scope, mode, $scope.tablette);
		obj = tmsLayout.getFloors();
		$scope.floordata = obj.data;
		$scope.floorname = obj.name;	
		$scope.turnoffsync = 0;	
		$scope.colorstate = tmsLayout.getcolorPicker();
		$scope.viewerconnect = $scope.initViewerConnet(tmsLayout.setViewerconnect);
		if($scope.editmode !== 1) {	//   && $scope.tmsv2 === 1
			if($localStorage.viewerconnect && typeof $localStorage.viewerconnect.exists === "number" && $localStorage.viewerconnect.exists === $scope.viewerconnect.exists)
				$scope.viewerconnect.obj = $localStorage.viewerconnect.obj;
			$scope.viewerconnect.init();
			if($localStorage.fixedviewmode && typeof $localStorage.fixedviewmode === "number" && $localStorage.fixedviewmode >= 0 && $localStorage.fixedviewmode <= 1)
				$scope.fixedviewmode = $localStorage.fixedviewmode;
			if($scope.fixedviewmode === 1) {
				tmsLayout.setFixView($scope.fixedviewmode);
				}
			}
		};
		
	$scope.lyedit = function() {
		$scope.bookingview = 'emptyview.html'; //'emptyview.html'; 
		$scope.lystart(1);	// reset the drawing board
		if($scope.indexLayout >= 0 && $scope.indexLayout < $scope.layoutdata.length)
			$scope.setLayout($scope.layoutdata[$scope.indexLayout].object);
		};

	$scope.lyLayoutcmd = function(action) {
		tmsLayout.shortcuts(action);
		};
		
	$scope.toggleViewWB = function() {
		$scope.getWaitingCount();
	
		$scope.bkgview = !$scope.bkgview;
		if($scope.bkgview) {
			$scope.showestimate = false;
			$scope.waitingview = 0;
			$scope.tmstitle = "  "; 
			$scope.bookwaititle = "waitingview";
			if($scope.prevoo !== null)
				$scope.resetshowMode();
		} else {
			$scope.cleanwaiting();
			$scope.waitingview = 1;
			$scope.tmstitle = "WAITING LIST"; 
			$scope.bookwaititle = "bookview";
			$scope.prevoo = $scope.getshowMode();
			$scope.setshowMode('all');
			if($scope.restaurant === 'SG_SG_R_TheFunKitchen')
				$scope.showestimate = true;
			}
			
		$scope.fdigest();
		};
				
	$scope.lybooking = function(flg) {
		$scope.getWaitingCount();

		$scope.fltrLastbkg = 0;
		if(flg === 1) $scope.initrunflg = 0;
		$scope.bookingview = $scope.bookingTemplate;
		$scope.bkgview = true;
		$scope.waitingview = 0;
		$scope.tmstitle = "  "; 
		$scope.bookwaititle = "waitingview";
		if($scope.initrunflg === 0) 
			$scope.lystart($scope.editmode);
		else {
			$scope.translatedata = interfaceModel.translateraphael;
			$scope.blackbg = false;
			}
		if($scope.prevoo !== null)
			$scope.resetshowMode();
		};

	$scope.lywaitingview = function() {
		$scope.lybooking(0);
		$scope.toggleViewWB();
		};

	$scope.lylastbookingview = function() {
		var hh = new Date().getHours();
			
		$scope.names.map(function(x) { 
			x.last = 0;
			if(x.cdate !== $scope.currentdate)
				return;
			if(hh - x.hh < 2) {
				x.last = 1;
				}
			});
		$scope.lybooking(0);
		$scope.prevoo = $scope.getshowMode();
		$scope.setshowMode('all');
		$scope.fltrLastbkg = 1;
		};

	$scope.toggleseated = function() {
		$scope.fltrSeatedbkg ^= 1;
		if(["hours", "lunch", "dinner"].indexOf($scope.cTimeselection) > -1) {
			hh = ($scope.cTimeselection === "hours") ? $scope.ctime : new Date().getHours();
			$scope.setviewbyselection($scope.cTimeselection, hh);
			}
		};
		
	$scope.toggleFixView = function() {
		$scope.fixedviewmode ^= 1;
		$localStorage.fixedviewmode = $scope.fixedviewmode;
		if($scope.fixedviewmode === 1 && ["lunch", "dinner"].indexOf($scope.cTimeselection) <0) {
			$scope.gotocurrentime();
			}
		$scope.searchText.v = "";
		$scope.updateclock($scope.selectedItem[0]);
		tmsLayout.setFixView($scope.fixedviewmode);
		$scope.view($scope.selectedItem[0]);
		};

	$scope.monthlimit = function(d, l) {
		return (d <= l);
		};

	$scope.setschedbox = function(l, v) {
		if(v === 1) $("." + l).prop('checked', true);
		else $("." + l).prop('checked', false);
		};
				
	$scope.lyscheduler = function() {
		var dd = new Date().getFullYear();
		if($scope.captainflg !== 1)
			return;
		
		$scope.monthdays = [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31 ];
		$scope.monthobj = [{ n: 'January', z: 31 }, { n: 'February', z: 28 }, { n: 'March', z: 31 }, { n: 'April', z: 30 }, { n: 'Mai', z: 31 }, { n: 'June', z: 30 }, { n: 'July', z: 31 }, { n: 'August', z: 31 }, { n: 'September', z: 30 }, { n: 'October', z: 31 }, { n: 'November', z: 30 }, { n: 'December', z: 31 }];
		
		if(dd % 4 === 0 && (dd % 100 !== 0 || dd % 400 === 0))
			$scope.monthobj[1].z = 29;
		$scope.bookingview = 'scheduler.html';
		};

	$scope.lysetempty = function() {
		$scope.bookingview = 'emptyview.html';
		};
				
	$scope.lysetcaptains = function() {
		if($scope.captainflg !== 1)
			return;

		if($scope.indexLayout < 0 && $scope.indexLayout >= $scope.layoutdata.length) {
			return $scope.alert("Unable to set Captains. Please edit layout, or call Weeloy for assistance");
			}
		tmsLayout.enablecaptains();
		$scope.lysave();
		$scope.alert("Done");
		};
		
	$scope.lyicicle = function() {
					
		$scope.icicle = (function () {
			return { 
				'mode': true,
				'cursor': 0,
				'max': 0,
				'view90': false,
				'xview': [],
				'yview': [],
				'idata': [],
				'start': 0,
				'end': 0,
				
				setidata: function(option) {
					var i, j, bkg = $scope.names;
					var end, start, tt;
	
					option = option || "";
					start = 11;
					end = 24;
					this.idata = [];
					
					if(option === "lunch") { start = 11; end = 17; }
					else if(option === "dinner") { start = 17; end = 24; }
					this.start = start;
					this.end = end;
					for(i = start; i < end; i++) {
						ll = (i < 12) ? "am":"pm";
						tt = (i < 13) ? i : i - 12;
						this.idata.push({ 'label': tt + ll, 'data':[] });
						for(j = 15; j < 60; j+=15) {
							this.idata.push({ 'label': tt + ":" + j + ll, 'data':[] });
							}
						}
					
					tt = (start * 4); // 9 starting * 4 slots	
					for(i = 0; i < bkg.length; i++) {
						j = bkg[i].slot15 - tt;
						if(j >= 0 && j < this.idata.length)
							this.idata[j].data.push(bkg[i].pers);
						}

					for(i = 0; i < this.idata.length; i++) 
						if(this.idata[i].data instanceof Array)
							this.idata[i].data.sort();

					this.max = 0;
					for(i = 0; i < this.idata.length; i++) 
						if(this.idata[i].data.length > this.max)
							this.max = this.idata[i].data.length;
					},
				
				setxyview: function(n) {
					this.xview = [];
					this.yview = [];
					for(var i = 0; i < n; i++)
						this.xview.push(i);

					for(var i = 0; i < this.max; i++)
						this.yview.push(i);
					},
				
				toggle90 : function() {
					this.view90 = (this.view90) ? false: true;
					},
				
				lunch: function() {
					this.cursor = 0;
					this.setidata('lunch');
					},
												
				dinner: function() {
					this.cursor = 0;
					this.setidata('dinner');
					},
												
				print : function() {
					var innerContents = document.getElementById("icicletemplate").innerHTML;
					var popupWinindow = window.open('', '_blank', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
					popupWinindow.document.open();
					popupWinindow.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + innerContents + '</html>');
					popupWinindow.document.close();
					},
											
				move: function(direction) {
					if(direction === 'left') {
						this.cursor -= this.xview.length;
						if(this.cursor < 0)
							this.cursor = 0;
						}
					else {
						this.cursor += this.xview.length;
						if(this.cursor > this.idata.length)
							this.cursor = this.idata.length - this.xview.length;
						}
					}
				};
			})();
		$scope.icicle.setidata("");			
		$scope.icicle.setxyview(20);
		$scope.bookingview = 'icicle.html';
		};
		
	$scope.choosethatfloor = function(name) {
		var i, oo, sublayout, ind;
		if($scope.indexLayout < 0 && $scope.indexLayout >= $scope.layoutdata.length) {
			return $scope.alert("Configuration index error with a multiple floor layout. Please edit layout, or call Weeloy for assistance");
			}
		oo = $scope.layoutdata[$scope.indexLayout].object;
		if(oo.floor !== 'MULTIPLEFLOOR' || oo.data.length < 2)
			return;

		ind = oo.data.indexOf(name);
		sublayout = oo.data[ind];
		$scope.setthatfloor(sublayout);
		};
		
	$scope.nextfloor = function() {
		var i, oo, sublayout, ind;
		if($scope.indexLayout < 0 && $scope.indexLayout >= $scope.layoutdata.length) {
			return $scope.alert("Configuration index error with a multiple floor layout. Please edit layout, or call Weeloy for assistance");
			}
		oo = $scope.layoutdata[$scope.indexLayout].object;
		if(oo.floor !== 'MULTIPLEFLOOR' || oo.data.length < 2)
			return;

		if($scope.subindexLayout >= 0 && $scope.subindexLayout < $scope.layoutdata.length) {
			sublayout = $scope.layoutdata[$scope.subindexLayout].name;
			ind = oo.data.indexOf($scope.layoutdata[$scope.subindexLayout].name);
			if(++ind >= oo.data.length)
				ind = 0;
			sublayout = oo.data[ind];
			}
		else sublayout = oo.data[0];
		$scope.setthatfloor(sublayout);
		};

	$scope.setthatfloor = function(sublayout) {
		var i;
		for(i = 0; i < $scope.layoutdata.length; i++)
			if($scope.layoutdata[i].name === sublayout) 
				break;
		
		if(i >= $scope.layoutdata.length)
			return alert("Invalid configuration, unable to locate layout " + sublayout + ". Go to edit mode and select/default a layout.");

		$scope.subindexLayout = i;
		$scope.setLayout($scope.layoutdata[$scope.subindexLayout].object);
		$scope.prefinfo.setvalue('sublayout', sublayout);
		$scope.lyprefsavecustomization();
		$scope.lybooking(1);
		};
			
	$scope.setLayout = function(oo)	{
		var i, vv = {};
		$scope.layoutcaptain = (typeof oo.captains === "string" && oo.captains === "1") ? 1 : 0;
		$scope.layoutprefixtable = (typeof oo.prefixtable === "string" && oo.prefixtable.length > 0) ? oo.prefixtable : "";
		$scope.currentcaptain = "";
		$scope.captainlist = [];
		$scope.clayoutobject = oo;
		if($scope.captainflg === 1 && oo.captains === "1") {
			$scope.captainlist.push("reset");
			$scope.captainlist.push("-----");
			for(i = 0; i < oo.data.length; i++)
				if(typeof oo.data[i].captain === "string" && oo.data[i].captain !== "")
					if($scope.captainlist.indexOf(oo.data[i].captain) < 0)
						$scope.captainlist.push(oo.data[i].captain);
			if($scope.captainlist.length <= 2)
				$scope.captainlist = [];
			}
		$scope.layoutName = oo.layoutName;				
		tmsLayout.setLayout(oo);
		$scope.fulltables = tmsLayout.getTableInfo();
		$scope.tables = $scope.fulltables.data;
		$scope.fulldata = $scope.fulltables.fulldata;
		$scope.fulldata.map(function(oo) {var name = oo[1]; vv[name] = parseInt(name.replace(/[^\d]+/g, '')); } );
		$scope.fulldata.sort(function(a, b) { return vv[a[1]] - vv[b[1]]; } );
		console.log('SORTING TABLES', $scope.fulldata.slice(0));
		};
		
	$scope.lyrun = function() {
		$scope.bookingview = $scope.bookingTemplate;
		$scope.lystart(0);	// reset the drawing board
		if($scope.indexLayout < 0 && $scope.indexLayout >= $scope.layoutdata.length) {
			return $scope.alert("Configuration index error with a multiple floor layout. Please edit layout, or call Weeloy for assistance");
			}
			
		var oo = $scope.layoutdata[$scope.indexLayout].object;
		if(oo.floor !== 'MULTIPLEFLOOR') {
			$scope.setLayout(oo);
			}
		else if($scope.subindexLayout >= 0 && $scope.subindexLayout < $scope.layoutdata.length && 
			   oo.data.indexOf($scope.layoutdata[$scope.subindexLayout].name) >= 0) {
					$scope.setLayout($scope.layoutdata[$scope.subindexLayout].object);
			}
		else return $scope.alert("Configuration error with a multiple floor layout '" + oo.name + "'. Please edit layout, or call Weeloy for assistance");
		
		if(tmsUI === 2)
			$(document).keydown(function(e) { return processRunKey(e, $scope); });
			
		$scope.alert("Starting TMS "+$scope.layoutdata[$scope.indexLayout].name, 1);
		$scope.fdigest();
		};
	

	$scope.processKey = function(e) {
		e = $.event.fix(e);	
		};
		
	$scope.lyreset = function() {
		tmsLayout.reset();
		};
				
	$scope.lysave = function() {
		var obj = tmsLayout.getData();

		if(!obj || obj.layoutName === "") {
			return $scope.alert("the current layout has no name");
			}

		if(obj.floor === 'MULTIPLEFLOOR') {
			return $scope.alert("Object Editing/Save is disregarded for multiple floor layout");
			}
		
		$scope.lywrite(obj);
		};

	$scope.lywrite = function(obj) {
	
		var objstr = JSON.stringify(obj);
		tmsService.tmsSave({ obj: objstr, email: $scope.usremail, token: token }).then(function(response){
			var i, limit = $scope.layoutdata.length;
			if(obj.layoutName === '_defaultlayout') {
				return $scope.alert('setting default layout to ' + obj.data );
				}
			else if(obj.layoutName === '_userpreferences' || obj.layoutName === '_userpreferences_prompt') {
				return $scope.alert('setting user preferences' );
				}
			for(i = 0; i < limit; i++)
				if($scope.layoutdata[i].name === obj.layoutName) {
					$scope.alert('updating the layout ' + obj.layoutName );
					$scope.layoutdata[i].object = obj;
					return;
					}					
			$scope.layoutdata.push({ name: obj.layoutName, object: obj });
			$scope.alert('saving the layout ' + obj.layoutName );
			$scope.lyselectLayout(obj.layoutName);	// reset the drawing board
			});
		};

	$scope.lyduplicate = function() {
		if($scope.indexLayout < 0 || $scope.indexLayout >= $scope.layoutdata.length) {
			return $scope.alert('Please, select a Layout');
			}

		$scope.showModal('Layout', 'new name for duplication', $scope.createLayoutdup, "modalgen1.html", {name: ""}, $scope.prepostcall, 'sm');
		};
				
	$scope.lynewLayout = function() {		
		$scope.showModal('Layout', 'new layout', $scope.createLayout, "modalgen1.html", {name: ""}, $scope.prepostcall, 'sm');
		};      

	$scope.lynewMFloor = function() {		
		$scope.showModal('Layout', 'new mutiple floor', $scope.createMFLayout, "modalgen1.html", {name: ""}, $scope.prepostcall, 'sm');
		};      

	$scope.lyclean = function() {		
		tmsLayout.clean();
		};      

	$scope.lysetoptions = function() {
		if($scope.indexLayout < 0 || $scope.indexLayout >= $scope.layoutdata.length) {
			$scope.alert('Please, select a Layout');
			return;
			}
		
		var name = $scope.layoutdata[$scope.indexLayout].name;
		mydata.prefixtable = "";
		
		$scope.showModal('Layout', 'set options for '+ name, $scope.savelayoutoptions, "layoutsetoption.html", mydata, $scope.prepostcall, 'sm');
		};

	$scope.savelayoutoptions = function() {
		if(typeof mydata.prefixtable === "string" && mydata.prefixtable.length > 1) {
			tmsLayout.setLayoutPrefixtable(mydata.prefixtable);
			$scope.lysave();
			}
		};
		
	$scope.lyrename = function() {
		if($scope.indexLayout < 0 || $scope.indexLayout >= $scope.layoutdata.length) {
			$scope.alert('Please, select a Layout');
			return;
			}
		
		var name = $scope.layoutdata[$scope.indexLayout].name;
		$scope.showModal('Layout', 'rename layout '+ name, $scope.renameLayout, "modalgen1.html", {name: ""}, $scope.prepostcall, 'sm');
		};
				
    $scope.renameLayout = function (data) {
    	var i, ind, obj, objstr, oldname = $scope.layoutdata[$scope.indexLayout].name;
		data.name = $scope.cleantext(data.name); 
		if(data.name === "") 
			return;
						
		for(i = 0; i < $scope.layoutdata.length; i++)
			if($scope.layoutdata[i].name === data.name) {
				$scope.alert("layout name '" + data.name + "' already exists. Set a new name. Layout not renamed");
				return;
				}
				
		obj = tmsLayout.getData();
		obj.data = data.name;
		objstr = JSON.stringify(obj);
		tmsService.tmsRename({ obj: objstr, email: $scope.usremail, token: token }).then(function(response){
			if($scope.layoutdata[$scope.indexLayout].object.floor === 'MULTIPLEFLOOR') {
				for(i = 0; i < $scope.multiplefloor.length; i++)
					if($scope.multiplefloor[i].name === oldname) {
						$scope.multiplefloor[i].name = obj.data;
						$scope.multiplefloor[i].object.layoutName = obj.data;
						break;
						}
				}
			else {
				for(i = 0; i < $scope.multiplefloor.length; i++)
					if((ind = $scope.multiplefloor[i].object.data.indexOf(oldname)) > -1) {
						$scope.multiplefloor[i].object.data[ind] = obj.data;
						$scope.lywrite($scope.multiplefloor[i].object);
						}
				}
				
			$scope.layoutdata[$scope.indexLayout].name = obj.data;
			$scope.layoutdata[$scope.indexLayout].object.layoutName = obj.data;
			tmsLayout.setLayoutName(obj.data);
			$scope.alert('Layout '+ obj.layoutName + ' has been renamed to '+ obj.data);
			$scope.nameLayout = obj.data;
			});
			
		};
		
    $scope.createLayoutdup = function (data) {
    	data.dup = 1;
    	$scope.createLayout(data);
    	};
    	
    $scope.createLayout = function (data) {
    	var i, obj;
		data.name = $scope.cleantext(data.name); 
		if(data.name !== "") {
			for(i = 0; i < $scope.layoutdata.length; i++)
				if($scope.layoutdata[i].name === data.name) {
					$scope.alert("layout name already exists. Set a new name. Layout not set/saved");
					return;
					}

		// new or duplication
			if(typeof data.dup === "undefined" && data.dup !== 1) 
				obj = tmsLayout.getemptyData(data.name);
			else if($scope.layoutdata[$scope.indexLayout].object.floor !== 'MULTIPLEFLOOR') {
				obj = tmsLayout.getData();
				obj.layoutName = data.name;
				}
			else {
				obj = $scope.layoutdata[$scope.indexLayout].object;
				obj = JSON.parse(JSON.stringify(obj));
				obj.layoutName = data.name;
				$scope.multiplefloor.push({ name: data.name, object: obj });
				}
			$scope.lywrite(obj);
			}			
		};

    $scope.createMFLayout = function (data) {
    	var i, obj;
		data.name = $scope.cleantext(data.name); 
		if(data.name !== "") {
			for(i = 0; i < $scope.multiplefloor.length; i++)
				if($scope.multiplefloor[i].name === data.name) {
					return $scope.alert("mutliple floor name already exists. Set a new name. mutliple floor not set/saved");
					}
			obj = {
				"layoutName": data.name,
				"restaurant": $scope.restaurant,
				"nelement": 0,
				"floor": 'MULTIPLEFLOOR',
				"data": []
				};
			$scope.multiplefloor.push({ name: data.name, object: obj });
			$scope.lywrite(obj);
			}			
		};

   $scope.changeselect = function(data, d) {
   		var index = data.indexOf(d);
   		if(index > -1) data.splice(index, 1);
   		else data.push(d);
   		};
   		
   $scope.lyeditMFloor = function (name) {
    	var i, oo, obj, tmp = [], reserve = [];

		for(i = 0; i < $scope.multiplefloor.length; i++)
			if($scope.multiplefloor[i].name === name)
				break;

		if(i >= $scope.multiplefloor.length)
			return $scope.alert("mutliple floor '" + name + "' does not exists.");
		
		oo = $scope.multiplefloor[i];
		for(i = 0; i < $scope.multiplefloor.length; i++)
			reserve.push($scope.multiplefloor[i].name);

		for(i = 0; i < $scope.layoutdata.length; i++)
			if(reserve.indexOf($scope.layoutdata[i].name) < 0)
				tmp.push($scope.layoutdata[i].name);

		for(i = oo.object.data.length - 1; i >= 0; i--) {
			if(tmp.indexOf(oo.object.data[i]) < 0)
				oo.object.data.splice(i, 1);
			}
				
		$scope.mydata.id = name;
		$scope.mydata.aArray = tmp; // pax mode, autoattribution mode
		$scope.mydata.value = oo.object.data.slice(0);
		$scope.mydata.changeselect = $scope.changeselect;
		$scope.mydata.showpersoinfo = false;
		$scope.showModal('Layout', 'edit multiple layout', $scope.setmultiplefloor, "modaleditMF.html", $scope.mydata, $scope.prepostcall, 'sm');	// "modaltable.html"
    	};
    	
	$scope.setmultiplefloor = function(data) {	
   		var i, obj, objstr;
		for(i = 0; i < $scope.multiplefloor.length; i++)
			if($scope.multiplefloor[i].name === data.id) {
				$scope.multiplefloor[i].object.data = $scope.mydata.value.slice(0);
				break;
				}
		if(i >= $scope.multiplefloor.length)
			return;
			
		for(i = 0; i < $scope.layoutdata.length; i++)
			if($scope.layoutdata[i].name === data.id) {
				$scope.layoutdata[i].object.data = $scope.mydata.value.slice(0);
				break;
				}

		if(i >= $scope.layoutdata.length)
			return;

		$scope.lywrite($scope.layoutdata[i].object);
		};
		
	$scope.removelayout = function(name) {	
		var objstr = JSON.stringify({
			"layoutName": name,
			"restaurant": $scope.restaurant,
			"nelement": 1
			});	
				
		tmsService.tmsDelete({ obj: objstr, email: $scope.usremail, token: token }).then(function(response){
			var i, ind, limit, newselect;
			if($scope.layoutdata[$scope.indexLayout].object.floor === 'MULTIPLEFLOOR') {
				for(i = 0; i < $scope.multiplefloor.length; i++)
					if($scope.multiplefloor[i].name === name) {
						$scope.multiplefloor.splice(i, 1);
						break;
						}
				}
			else {
				for(i = 0; i < $scope.multiplefloor.length; i++)
					if((ind = $scope.multiplefloor[i].object.data.indexOf(name)) > -1) {
						$scope.multiplefloor[i].object.data.splice(ind, 1);
						$scope.lywrite($scope.multiplefloor[i].object);
						}
				}
					
			$scope.layoutdata.splice($scope.indexLayout, 1);
			$scope.indexLayout = -1;
			newselect = -1;
			limit = $scope.layoutdata.length;
			if(limit > 0) {
				if($scope.defaultLayout !== "") {
					for(i = 0; i < limit; i++)
						if($scope.layoutdata[i].name === $scope.defaultLayout) {
							newselect = i;
							break;
							}
					}
				if(newselect < 0) newselect = 0;
				$scope.lyselectLayout($scope.layoutdata[newselect].name);
				}
			 else document.location.reload(true);

			});
		};

	$scope.lydeleteLayout = function() {
		
		if($scope.indexLayout < 0 && $scope.indexLayout >= $scope.layoutdata.length) {
			$scope.alert("First, select a layout that you want to delete");
			return;
			}
		
		if($scope.confirm("Confirm that you want to delete layout '" + $scope.layoutdata[$scope.indexLayout].name + "'", 1) === false)
			return;
		
		$scope.removelayout($scope.layoutdata[$scope.indexLayout].name);
		};
		
	$scope.lyselectLayout = function(name) {
		for(var index = 0; index < 	$scope.layoutdata.length; index++)
			if($scope.layoutdata[index].name === name) {
				break;
				}		
		if(index >= $scope.layoutdata.length)
			return;
		
		if($scope.editmode === -1)	// if not set, set it to edit mode
			$scope.editmode = 1;
			
		$scope.lystart($scope.editmode);	// reset the drawing board
		if($scope.tmsDrawnflg === false)
			return;
		
		$scope.setLayout($scope.layoutdata[index].object);
		$scope.indexLayout = index;
		$scope.nameLayout = name;
		};

	$scope.lybkgreadviewcustomization = function(data) {
		var i, j;
		for(i = 0; i < $scope.tabletitle.length; i++) {
			for(j = 0; j < data.length; j++) 
				if($scope.tabletitle[i].b === data[j].b)
					$scope.tabletitle[i].m =  data[j].m;
			}						
		$scope.tabletitle.sort(function(a, b) { return a.m - b.m; });
		return $scope.tabletitle;
		};
		
	$scope.lybkgsetviewcustomization = function() {
		var layout, obj, oo;
		
		oo = $scope.tabletitle;
		oo.sort(function(a, b) { return a.m - b.m; });
		
		$scope.bckups = oo.slice(0);
		obj = oo.slice(0);
		
		layout = { layoutName : "_userpreferences", restaurant: $scope.restaurant, email: $scope.usremail, nelement: 1, data: obj };
		$scope.lywrite(layout);
		};

	$scope.setTmsPrompt = function() {
		tmsLayout.setPrompt($scope.prefinfo.getvalue('tms'));
		};

	$scope.setTmsTablematch = function() {
		tmsLayout.setPref('unmatchtablesize', $scope.prefinfo.getvalue('unmatchtablesize'));
		};
		
	$scope.setTmsTriggerevent = function(mode) {
		tmsLayout.setPref('triggerevent', mode);
		};
		
	$scope.lyprefreadcustomization = function(data) {
		$scope.prefinfo.update(data, 'all');
		$scope.lyprefstorecustomization();
		};
		
	$scope.lyprefsetcustomization = function() {
		$scope.prefinfo.update($scope.mydata.prefs, '');
		
		$scope.cc_url = '../modules/callcenter/callcenter.php?bktracking=CALLCENTER&bktms=87&bkrestaurant=' + $scope.restaurant + '&prompt=' + $scope.prefinfo.getvalue('callcenter') + '&data=' + $scope.callcenter + '&genflag='+$scope.ccflag;		
		$scope.lyprefstorecustomization();
		$scope.setTmsPrompt();
		$scope.callcenterurl();
		$scope.lyprefsavecustomization();
		};

	$scope.lyprefstorecustomization = function() {
		$scope.triggerevent = ($scope.prefinfo.getvalue('beeperevent') === 1);
		$scope.shortcuts = (tmsUI === 2 || $scope.prefinfo.getvalue('shortcuts') === 1);
		$scope.sendsms = ($scope.prefinfo.getvalue('smsset') === 1);
		$scope.smslatecomer = ($scope.prefinfo.getvalue('smslatecomer') === 1);
		$scope.gridorder = ($scope.prefinfo.getvalue('gridorder') === 1);
		$scope.unmatchtablesize = ($scope.prefinfo.getvalue('unmatchtablesize') === 1);
		$scope.keepseated = ($scope.prefinfo.getvalue('keepseated') === 1);
		$scope.defaultmodemealtype = ($scope.prefinfo.getvalue('defaultmodemealtype') === 1);
		$scope.fixviewflag = ($scope.prefinfo.getvalue('fixviewflag') === 1);
		if($scope.fixviewflag === false)
			$scope.fixedviewmode = $localStorage.fixedviewmode = 0;

		if($scope.defaultmodemealtype)
			$scope.gotocurrentime();
			
		$scope.setTmsTablematch();
		$scope.setTmsTriggerevent($scope.triggerevent ? 1 : 0);
		};
		
	$scope.lyprefsavecustomization = function() {
		var layout = { layoutName : "_userpreferences_prompt", restaurant: $scope.restaurant, email: $scope.usremail, nelement: 1, data: $scope.prefinfo.read() };
		$scope.lywrite(layout);
		};
		
	$scope.lypreferences = function(type) {
		switch(type) {
			case 'Bookings':
				$scope.lybkgcustomize();
				break;
				
			case 'Actions':
				$scope.lypromptcustomize();
				break;

			case 'HideFeatures':
				$scope.lyprompthidefeatures();
				break;
			
			case 'Captains':
				$scope.managecaptain();
				break;
			}
		};

	$scope.managecaptain = function() {
		$scope.bookingview = 'captains.html';
		};

	$scope.viewcaptain = function(name) {
		if(typeof name !== "string" || name === "" || $scope.captainlist.length < 1 || $scope.captainlist.indexOf(name) < 0 || name === "-----") {
			$scope.alert("Invalid Captain");
			return;
			}

		if(name === "reset") name = "";
		$scope.currentcaptain = name;
		tmsLayout.setLayoutCaptain($scope.clayoutobject, name);
		$scope.view($scope.selectedItem[0]);
		};
		
	$scope.savecaptain = function() {
		var i, j, name, log = 0, data = $scope.clayoutobject.data;
		var captain = $scope.fulldata;

		for(i = 0; i < data.length; i++) {
			name = data[i].label;
			for(j = 0; j < captain.length; j++) 
				if(name === captain[j][1]) {
					data[i].captain = captain[j][3];
					log = 1;
					break;
					}
			}
		if(log === 1) {	
			$scope.lywrite($scope.clayoutobject);
			$scope.setLayout($scope.clayoutobject);
			}
		$scope.lybooking(0);
		};
		
	$scope.cancelcaptain = function() {
		$scope.lybooking(0);
		};

	$scope.hidefeatdatalabel = ['waiting view', 'last bookings', 'timeline view', 'icicle view', 'next floor', 'block tables', 'captain', 'print guest'];
	$scope.hidefeatdataflg = (typeof $localStorage.hidefeatdataflg !== "number") ? 0 : $localStorage.hidefeatdataflg;
	$scope.gethidefeatval = function(l) {
		var index = $scope.hidefeatdatalabel.indexOf(l), val = (1 << index);
		return (!$scope.hidefeatflg || index < 0 || ($scope.hidefeatdataflg & val) !== val);
		};
		
	$scope.hidefeatread = function() {
		var ar = [];
		$scope.hidefeatdatalabel.forEach(function(l, index) {
			ar.push({ b: l, m: ($scope.hidefeatdataflg & (1 << index)) ? 1 : 0});
			});
		return ar;
		};
		
	$scope.hidefeatwrite = function() {
		var prefs = $scope.mydata.prefs, flg = 0;
		prefs.forEach(function(oo) {
			var index = $scope.hidefeatdatalabel.indexOf(oo.b);
			if(index >= 0 && oo.m === 1) 
				flg += (1 << index);
			});
		$scope.hidefeatdataflg = $localStorage.hidefeatdataflg = flg;
		};
		
	$scope.lyprompthidefeatures = function() {
		$scope.mydata.showpersoinfo = false;
		$scope.mydata.prefs = $scope.hidefeatread();
		$scope.showModal('Hide Features', 'Modify Order', $scope.hidefeatwrite, "userprefs.html", mydata, null, 'sm');	
		};
			
	$scope.lypromptcustomize = function() {
		$scope.mydata.showpersoinfo = false;
		$scope.mydata.prefs = $scope.prefinfo.read();
		$scope.showModal('Customize Preferences per Service', 'Modify Order', $scope.lyprefsetcustomization, "userprefs.html", mydata, null, 'sm');	
		};
						
	$scope.lybkgcustomize = function() {
		$scope.mydata.tabletitle = $scope.tabletitle;
		$scope.mydata.showpersoinfo = false;
		$scope.showModal('Customize Booking Display', 'Modify Order', $scope.lybkgsetviewcustomization, "customization.html", mydata, null, 'sm');		
		};
		
	$scope.lydefault = function() {
		if($scope.indexLayout < 0 || $scope.indexLayout >= $scope.layoutdata.length) {
			$scope.alert('Please, select a Layout');
			return;
			}
			
		$scope.defaultLayout = $scope.layoutdata[$scope.indexLayout].name;			
		$scope.lywrite({
			"layoutName": '_defaultlayout',
			"restaurant": $scope.restaurant,
			"nelement": 1,
			"captains": "",
			"data": $scope.defaultLayout
			});
		};
	
	$scope.lysetfloor = function(name) {

		if($scope.layoutdata[$scope.indexLayout].object.floor === 'MULTIPLEFLOOR') {
			return $scope.alert("Cannot rename a floor of a multiple floor layout");
			}

		for(var index = 0; index < 	$scope.floordata.length; index++)
			if($scope.floordata[index] === name) 
				break;
		if(index >= $scope.floordata.length)
			return;
				
		if($scope.editmode === -1)	// if not set, set it to edit mode
			$scope.editmode = 1;
			
		$scope.lystart($scope.editmode);	// reset the drawing board
		if($scope.tmsDrawnflg === false)
			return;
		
		tmsLayout.setfloorName(name);
		$scope.floorname = name;
		};

	$scope.alert = function(val, force) {
		if($scope.editmode === 1 || ($scope.prefinfo.getvalue('tms') === 1 && typeof force === "undefined"))			
			$timeout( function() { alert(val); }, 1);
		else {
			$scope.promptmsg +=  (($scope.promptmsg !== "") ? "; " : "") + val;
			$timeout( function() { $scope.promptmsg = ""; }, 10000);
			}
		$scope.fdigest();
		};

	$scope.confirm = function(val, force) {
		if($scope.editmode === 1 || $scope.prefinfo.getvalue('cancel') === 1 || ($scope.prefinfo.getvalue('tms') === 1 && typeof force === "undefined"))
			return confirm(val);
		return true;
		};

	$scope.getnxtbkg = function(abooking) {
		var i, ind, found, oo = $scope.names, minslot, maxslot, tbl;

		if(!abooking || typeof abooking !== "string" || abooking === "")
			return "";
			
		if((ind = oo.fdBooking(abooking)) < 0)
			return "";

		tbl = oo[ind].tablename;
		minslot = oo[ind].slot;
		maxslot = 99;
		for(i = 0, found = -1; i < oo.length; i++) 
			if(oo[i].tablename === tbl && oo[i].slot > minslot && oo[i].slot < maxslot) {
				found = i;
				maxslot = oo[found].slot;
				}
		return (found >= 0) ? oo[found].booking + "("+ oo[found].time + ")" : "";
		};


	$scope.setsubbooking = function() {
		var str, ptable, pnotestext, x = $scope.selectedItem[0];
		
		if(mydata.tablename === x.tablename && mydata.cleartime === x.cleartime && mydata.notestext === x.notestext && parseInt(mydata.duration) === x.duration)
			return;
		
		ptable = x.tablename;
		pnotestext = x.notestext;
			
		x.tablename = $scope.resolverangetable(mydata.tablename);
		x.cleartime = mydata.cleartime;
		x.duration = parseInt(mydata.duration);
		if(x.notestext !== mydata.notestext) {
			x.notestext = mydata.notestext;
			x.fullnotes = x.notestext + ((x.notestext !== "" && x.notescode !== "") ? "," : "") + x.notescode; 
			}
		
		if(x.tablename === "") x.captain = "";
		else $scope.setCaptainTable(x);

		if(ptable !== x.tablename) {
			$scope.localtitle = x.tablename;		//libservice multiple-select
			$scope.hidemultiple = true;
			}
		
		str = "cleartime=" + x.cleartime + "|" + "duration=" + x.duration;	// need to start with clear time
		if($scope.tmsSync === 1 && $scope.datePickerDoor.today === 1) 
			$scope.sync.obj.senddata("updatebkgsub;" + x.booking + ";" + str);
		$scope.theConf = null;
		
		bookService.savechgstateservice($scope.restaurant, x.booking, str, token).then(function(response){ 
			if(response.status === 1) {
				if($scope.tmsSync === 1 && $scope.datePickerDoor.today === 1) 
					$scope.sync.obj.senddata("state;" + x.booking + ";" + x.state);
				$scope.setgeneralInfo(x); 
				$scope.view(x); 
				}
			});
		if(ptable !== x.tablename) {
			bookService.savechgtableservice($scope.restaurant, [x.booking], [x.tablename], [x.captain], token).then(function(response){ 
			if(response.status === 1) { 
				if($scope.tmsSync === 1 && $scope.datePickerDoor.today === 1)
					$scope.sync.obj.senddata("table;" + x.booking + ";" + x.tablename);
				$scope.alert('Done'); 
				$scope.setgeneralInfo(x); 
				$scope.view(x); 
				} 
			});
			}
			
		if(pnotestext !== x.notestext) {
			bookService.savechgfieldservice($scope.restaurant, x.booking, x.notestext, "notestext", token).then(function(response){ 
				if(response.status === 1) { 
					if($scope.tmsSync === 1 && $scope.datePickerDoor.today === 1)
						$scope.sync.obj.senddata("notestext;" + x.booking + ";" + x.notestext);
					$scope.alert('Done'); 
					$scope.setgeneralInfo(x); 
					$scope.view(x); 
					} 
				});
			}
		};
										
	$scope.setbooking = function() {
		var objstr, ll, adate, x, booking, email, mod, sep, prevtime, prevdate, pos;

		x = $scope.selectedItem[0];
		booking = x.booking;
		prevdate = x.date;
		prevtime = x.time;
		email = x.email;
		adate = mydata.getDate('-', '');
		mydata.date = mydata.getDate('-', 'reverse');
		mydata.cdate = x.cdate;
		mydata.ctime = x.ctime;

		pos = $scope.names.length + 1;		
		mod = bookService.prepareBooking(mydata, $scope.restaurant, pos, $scope.colorscheme, $scope.dinnertime);	
		if(typeof mod.error === "number" && mod.error === 1) {
			return $scope.alert(mod.msg);
			}
		objstr = JSON.stringify( mod );

		if(prevdate !== mod.date) {
			mod.sdate = mod.date.substr(8, 2) + '/' + mod.date.substr(5, 2);

			bookService.checkAvail($scope.restaurant, adate, mod.time, mod.pers, booking).then(function(response) {
				if (response.data === 0) { 
					$scope.alert(response.errors + '. Unable to modify the reservation '+booking+'\nfor '+adate+'\nat '+mod.time+'\nfor '+mod.pers+' persons'); 
					return; 
					}

				bookService.modifullbooking($scope.restaurant, booking, email, objstr, false).then(function(response){ 
					if (response.status === 0) { 
						$scope.alert(response.errors + '. Unable to modify the reservation '+booking+'\nfor '+adate+'\nat '+mod.time+'\nfor '+mod.pers+' persons'); 
						return; 
						}
					Object.keys(mod).map(function(ll) { x[ll] = mod[ll]; });
					if($scope.tmsSync === 1 && $scope.datePickerDoor.today === 1) 
						$scope.sync.obj.senddata("changedatebkg;" + x.booking + ";none");
					$scope.refreshbooking();
					$scope.modifmsg(response.status, response.errors, booking); 
					});
				});
		} else {
			bookService.modifullbooking($scope.restaurant, booking, email, objstr, false).then(function(response){ 
				if (response.status === 0) { 
					$scope.alert(response.errors + '. Unable to modify the reservation '+booking+'\nfor '+adate+'\nat '+mod.time+'\nfor '+mod.pers+' persons'); 
					return; 
					}
				Object.keys(mod).map(function(ll) { x[ll] = mod[ll]; });
				$scope.setbkg4display(x);
				json = JSON.stringify(x).replace(/;/g, ".").replace(/\'|\"/g, "’");
				if($scope.tmsSync === 1 && $scope.datePickerDoor.today === 1) 
					$scope.sync.obj.senddata("updatebkg;" + booking + ";" + json);
				$scope.refreshbooking(booking);
				$scope.modifmsg(response.status, response.errors, booking); 
				//$scope.lybooking(0);
				});
			}
		};

	$scope.modifmsg = function(status, msg, booking) {
		if(status === 1) 
			$scope.alert('Reservation ' + booking + ' has been modified.'); 
		else $scope.alert('Reservation ' + booking + ' has NOT been modified (' + msg + ').' );
		};

	$scope.prefill = function() {
		var field = { lastname: "nolastname", firstname: "nofirstname", mobile:"+65 99999999", email: "no@email.com" };
		
		Object.keys(field).map(function(ll) { if(mydata[ll] === "") mydata[ll] = field[ll]; });
		mydata.showpersoinfo = false;
		};

	$scope.showpersonal = function() {
		var field = { lastname: "", firstname: "", mobile:"", email: "" };
		
		Object.keys(field).map(function(ll) { mydata[ll] = field[ll]; });
		mydata.showpersoinfo = true;
		};
						
	$scope.walkingbooking = function() {
		var oo, today = new Date(), hh = today.getHours(), mm = today.getMinutes(), tt;
		
		tt = ((hh < 10) ? "0" : "") + hh + ":" + ((mm < 10) ? "0" : "") + mm;
		mydata.setInit($scope.today, tt, null, 3, today, today);

		mydata.lastname = "";
		mydata.firstname = "";
		mydata.email = "";
		mydata.mobile = "";
		mydata.npers = "";
		mydata.name = "";
		mydata.comment = "";	
		mydata.nevent = "";
		mydata.tablename = "";
		mydata.contact = "";
		mydata.prefill = $scope.prefill;
		mydata.showpersonal = $scope.showpersonal;
		mydata.checkprevemail = $scope.checkprevemail;
		mydata.prefill();
	
		oo = ($scope.codebooking && $scope.codebooking instanceof Array) ? $scope.codebooking : [];	
		mydata.notescode = []; 
		for(i = 0; i < oo.length; i++)
			mydata.notescode.push({ label: oo[i].label, value: false });
	
		mydata.notestext = "";
		mydata.notesflag = 1;
		mydata.showpersoinfo = false;
		
		$scope.showModal('WALKING', 'Walking Booking', $scope.createWalkingbooking, "walkingBooking.html", mydata, $scope.prepostcall, 'sm', 'OK', 'CANCEL', 'position-modal');		
        };

	$scope.createWalkingbooking = function() {
		var objstr, oo, flg, index = -1, json, pos, dd, hh, mm, ctime;

		dd = new Date();
		hh = dd.getHours();
		mm = dd.getMinutes();
		ctime = ((hh < 10) ? "0" : "") + hh + ":" + ((mm < 10) ? "0" : "") + mm;
		
		mydata.prefill();
		if(mydata.npers === "" && mydata.tablename === "") {
			$scope.alert("Please enter valid number of cover, and/or table number. No walking created");
			return;
			}
			
		if(mydata.npers === "")
			mydata.npers = 2;
		pos = $scope.names.length + 1;
		mydata.tablename = $scope.resolverangetable(mydata.tablename);
		mydata.date = mydata.cdate = $scope.currentdate;
		mydata.ctime = ctime;
		oo = bookService.prepareBooking(mydata, $scope.restaurant, pos, $scope.colorscheme, $scope.dinnertime);
		if(oo.phone.length > 0 && oo.phone[0] !== "+") {
			oo.phone = oo.phone.replace(/[^\d]/g, '');
			if(oo.phone.length == 8)
				mydata.mobile = oo.phone = "+65 " + oo.phone;
			}
			
		$scope.setCaptainTable(oo);
		if(typeof oo.error === "number" && oo.error === 1) {
			return $scope.alert(oo.msg);
			}
		
		bookService.createWalking($scope.restaurant, oo).then(function(response){
			if(response.status === 1) {
				$scope.alert('Walk-ins for ' + oo.pers + ' pax, has been created ' + response.data.booking);
				oo.booking = response.data.booking;
				json = JSON.stringify(oo).replace(/;/g, ".").replace(/\'|\"/g, "’");
				if($scope.tmsSync === 1 && $scope.datePickerDoor.today === 1) 
				 	$scope.sync.obj.senddata("walkin;" + oo.booking + ";" + json);
				$scope.refreshbooking(oo.booking);
				}
			}); 
		};

	$scope.modifDblbooking = function(x) {
		$scope.selectedItem = [x].slice(0);
		$scope.modifbooking();
		};
				
	$scope.modifbooking = function() {
		var val, oo, x = $scope.selectedItem[0], title;
		var minDate = new Date(), maxDate = new Date();
		maxDate.setTime(maxDate.getTime() + (60 * 24 * 3600 * 1000));	// remainingday  120 days

		mydata.setInit(x.ddate.jsdate(), x.time, null, 3, minDate, maxDate);

		mydata.lastname = x.last;
		mydata.firstname = x.first;
		mydata.mobile = (x.phone !== "----") ? x.phone : "+65 99999999";
		mydata.email = (x.email !== "----") ? x.email : "no@email.com";
		mydata.npers = x.pers;
		mydata.name = x.confirmation;
		mydata.comment = x.comment;	
		mydata.tablename = x.tablename;	
		mydata.nevent = x.event;
		mydata.contact = "";
		mydata.showpersoinfo = false;
			
		oo = ($scope.codebooking && $scope.codebooking instanceof Array) ? $scope.codebooking : [];	
		val = (x.notescode && typeof x.notescode === "string" && x.notescode.length > 1) ? x.notescode.split(",") : [];
		mydata.notescode = []; 
		for(i = 0; i < oo.length; i++)
			if(oo[i].label !== "hide")
				mydata.notescode.push( { label: oo[i].label, value: (val.indexOf(oo[i].label) > -1) });
	
		mydata.notestext = (x.notestext && typeof x.notestext === "string") ? x.notestext : "";
		mydata.notesflag = 1;
		
		title = 'Modify Booking';
		if(typeof x.tablename === "string" && x.tablename !== "")
			title += " (" + x.tablename + " )";
			
		$scope.showModal(title, title, $scope.setbooking, "bookingModiftms.html", mydata, $scope.prepostcall, 'sm');		
        };

	$scope.submodifbooking = function() {
	
		var val, oo, title, x = $scope.selectedItem[0];
		var minDate = new Date(), maxDate = new Date();
		maxDate.setTime(maxDate.getTime() + (60 * 24 * 3600 * 1000));	// remainingday  120 days

		mydata.setInit(x.ddate.jsdate(), x.time, null, 3, minDate, maxDate);
		mydata.setInit(x.ddate.jsdate(), x.time, null, 3, minDate, maxDate);

		mydata.duration = x.duration;
		mydata.cleartime = x.cleartime;
		mydata.tablename = x.tablename;
		
		oo = ($scope.codebooking && $scope.codebooking instanceof Array) ? $scope.codebooking : [];	
		val = (x.notescode && typeof x.notescode === "string" && x.notescode.length > 1) ? x.notescode.split(",") : [];
		mydata.notescode = []; 
		for(i = 0; i < oo.length; i++)
			mydata.notescode.push( { label: oo[i].label, value: (val.indexOf(oo[i].label) > -1) });
	
		mydata.notestext = (x.notestext && typeof x.notestext === "string") ? x.notestext : "";
		mydata.notesflag = 1;
		mydata.showpersoinfo = false;
		
		title = 'Modify TMS Booking';
		if(typeof x.tablename === "string" && x.tablename !== "")
			title += " (" + x.tablename + " )";

		if(title.length > 50)
			title = title.substr(0, 50) + "...";
		$scope.showModal(title, title, $scope.setsubbooking, "subBookModiftms.html", mydata, $scope.prepostcall, 'sm');		
        };

	$scope.cancelbooking = function() {
		var x = $scope.selectedItem[0], email;
		if ($scope.confirm("Please confirm the cancellation of reservation " + x.booking + " ?", 1) === false)
			return;

		email = (x.email !== "----") ? x.email : "no@email.com";
		return bookService.cancelbackoffice($scope.restaurant, x.booking, email).then(function(response){
			if(response.status !== 1) {
				$scope.alert('Reservation ' + x.booking + ' has NOT been cancel. ' + response.errors);
				return;
				}
				
			x.wheelwin = x.bkstatus = 'cancel';
			$scope.alert('Reservation ' + x.booking + ' has been cancel ');
			if($scope.tmsSync === 1 && $scope.datePickerDoor.today === 1) 
				$scope.sync.obj.senddata("state;" + x.booking + ";cancel");
			$scope.refreshbooking();
			});
		};
		
	$scope.refreshbooking = function(x) {
		$scope.readbooking("", x);
		};

	$scope.showevent = function() {
		var modalsize;
		
		mydata.showpersoinfo = false;
		mydata.urldata = { value: '../modules/calendar/calendar.html?bkrestaurant=' + $scope.restaurant + '&token=' + token };
		mydata.closeButtonText = "Close";
		mydata.height = 750;
		modalsize = 'lg';
		if(screen.height < 800) {
			mydata.height = 550;
			modalsize = '';
			}
		$scope.showModal('CALENDAR', 'CALENDAR', $scope.emptyfunc, "embededcalendar.html", mydata, null, modalsize, "", "Close");	// size = '' => medium !!!
		};
	
	$scope.resetcolorchatbutton = function() {
		$scope.chatstyle = { "color": "blue", "font-size": "18px"};
		};

	$scope.setcolorchatbutton = function() {
		$scope.chatstyle = { "color": "red", "font-size": "18px"};
		};

	$scope.resetcolorstickynotebutton = function() {
		$scope.stickynotesstyle = { "color": "blue", "font-size": "18px"};
		};

	$scope.setcolorstickynotebutton = function() {
		$scope.stickynotesstyle = { "color": "red", "font-size": "18px"};
		};

	$scope.showchat = function() {
		var modalsize;
		
		mydata.showpersoinfo = false;
		mydata.urldata = { value: '../modules/chat/index.html?bkrestaurant=' + $scope.restaurant + '&email='  + $scope.usremail + '&token=' + token };
		mydata.closeButtonText = "Close";
		mydata.height = 750;
		modalsize = 'lg';
		if(screen.height < 800) {
			mydata.height = 550;
			modalsize = '';
			}
		
		if($scope.tmsSync === 1)
			$scope.sync.obj.senddata("chat;ring;none;");
		$scope.showModal('CHAT', 'CHAT', $scope.resetcolorchatbutton, "embededcalendar.html", mydata, null, modalsize, "", "Close");	// size = '' => medium !!!
		};
	
	$scope.showstickynotes = function() {
		var modalsize;
		
		mydata.showpersoinfo = false;
		mydata.urldata = { value: '../modules/chat/stickynotes.html?bkrestaurant=' + $scope.restaurant + '&email='  + $scope.usremail + '&token=' + token };
		mydata.closeButtonText = "Close";
		mydata.height = 750;
		modalsize = 'lg';
		if(screen.height < 800) {
			mydata.height = 550;
			modalsize = '';
			}
		
		$scope.showModal('STICKYNOTES', 'STICKYNOTES', $scope.resetcolorchatbutton, "embededcalendar.html", mydata, null, modalsize, "", "Close");	// size = '' => medium !!!
		};
	
		
	$scope.addbooking = function(flg) {
		var minDate = new Date(), maxDate = new Date();
		maxDate.setTime(maxDate.getTime() + (60 * 24 * 3600 * 1000));	// remainingday  120 days

		mydata.setInit(new Date(), '19:00', null, 3, minDate, maxDate);

		mydata.npers = 3;
		mydata.name = 'new confirmation';
		mydata.nevent = "";
		mydata.contact = "";
		mydata.showpersoinfo = false;
		if(flg !== 97) {	
			mydata.urldata = { value: $scope.cc_url };
			if(flg && flg === 17) {
				mydata.urldata = { value: $scope.cc_urlimited };
				}
			}
		mydata.closeButtonText = "Close";
		$scope.showModal('Create a Booking', 'Create Booking', $scope.postaddbooking, "embededcallcenter.html", mydata, null, '');	// size = '' => medium !!!
		};
	
	$scope.postaddbooking = function(arg) {
		$scope.refreshbooking();
		if(arg === 'more')
			$scope.addbooking(97);
		};
				
	$scope.growwindowonlyonce = 1;

	//$scope.start_opened = false;
	$scope.minDate = new Date();
	$scope.maxDate = new Date();
	$scope.maxDate.setTime($scope.maxDate.getTime() + (365 * 24 * 3600 * 1000));	// remainingday  365 days
	$scope.dateOptions = { startingDay: 1 };
	
	$scope.datePickerDoor = { today: 1, opened:false, originaldate: new Date(), pickerdate: new Date().toString() };
	$scope.datePickerDoor.mindate = $scope.minDate.getFullYear() + "-" + ($scope.minDate.getMonth()+1) + "-" + $scope.minDate.getDate();
	$scope.datePickerDoor.maxdate = $scope.maxDate.getFullYear() + "-" + ($scope.maxDate.getMonth()+1) + "-" + $scope.maxDate.getDate();

	$scope.dateopen = function($e) {
		$scope.datePickerDoor.opened = true;		
		$e.preventDefault();
		$e.stopPropagation();
	};

	$scope.dateOptions = {
		formatYear: 'yy',
		startingDay: 1,
   		showWeeks: false,
   		popupPlacement: 'left',
		ignoreReadonly: true
	};

	$scope.resetMPdate = function() {
		$scope.datePickerDoor.pickerdate = new Date().getDateFormatReverse('-');
		$scope.changeMPdate();
		};
		
	$scope.changeMPdate = function() {
	
		if($scope.datePickerDoor.pickerdate && typeof $scope.datePickerDoor.pickerdate === "string" && $scope.datePickerDoor.pickerdate.length > 9) {
			$scope.datePickerDoor.originaldate = $scope.datePickerDoor.pickerdate.jsdate();
			$scope.changedate ();
			}
		};

	$scope.changeAdddate = function() {
		$scope.datePickerDoor.originaldate = moment($scope.currentdate).add(1, "day").format("YYYY-MM-DD").jsdate();
		$scope.changedate();
		};
		
	$scope.changeSubdate = function() {
		if($scope.datePickerDoor.today === 0) {
			$scope.datePickerDoor.originaldate = moment($scope.currentdate).subtract(1, "day").format("YYYY-MM-DD").jsdate();
			$scope.changedate();
			}
		};
		
	$scope.changedate = function() {
		if($scope.datePickerDoor.originaldate instanceof Date === false)
			return;
			
		var i, d = new Date(), 
				d1 = $scope.datePickerDoor.originaldate, 
				data = $scope.currentdata, 
				ddate = $scope.datePickerDoor.originaldate.getDateFormatReverse('-');

		$scope.datePickerDoor.today = (d1.getFullYear() === d.getFullYear() && d1.getMonth() === d.getMonth() && d1.getDate() === d.getDate()) ? 1 : 0;

		$scope.names = data.filter(function(oo) { return (oo.date === ddate); });

		$scope.currentdate = ddate;
		$scope.today = new Date(ddate);
		$scope.today = new Date($scope.today.getFullYear(), $scope.today.getMonth(), $scope.today.getDate(), 0, 0, 0, 0);
		$scope.readbooking(ddate);
		};
					
	$scope.cleantext = function(obj) { 	
		obj = obj.replace(/[!@#$%^&*()=}{\]\[\"\':;><\?/|\\]/g, ''); 
		return obj; 
	};
	
	$scope.redraw = function() {
		if($scope.selectedItem.length > 0)
			$scope.showtablehour($scope.selectedItem[0]);
		};

	$scope.fixviewdbl = function(tablename) {
		var i, state, oo;
		for(i = 0; i < $scope.names.length; i++) {
			oo = $scope.names[i];
			if(oo.tablename === "" || $scope.seatedstate.indexOf(oo.state) < 0)
				continue;
				
			if(oo.tablename === tablename || oo.tablename.indexOf(tablename) > -1) {
				state = (oo.state === "not ready") ? "left" : "not ready";
				$scope.lysetstate(oo, state);
				return;
				}
			}
		};
		
	$scope.showtablefixview = function(x) {
		var i, y, k, slot, fpos, cc, firstcol, tablearr, nextable, mealduration, slot_limit, 
		showflag, stillseated, dinnertime = (new Date().getHours() > $scope.dinnertime.hh);

		slot_limit = 99;
		
		slot = ($scope.cTimeselection === "lunch") ? 18 : 34; 
		if(slot < 34) slot_limit = 35;

		tmsLayout.clearallstate();
		nextable = {};
		
		$scope.names.map(function(oo) {
			if(oo.state === "left" || oo.state === "no show" || typeof oo.tablename !== "string" || oo.tablename.length < 1) 
				return;
			var state3 = ($scope.seatedstate.indexOf(oo.state) > -1  && dinnertime && $scope.cTimeselection === "dinner" && oo.mealtype === "lunch");
			if((oo.slot > slot && oo.slot < slot_limit) || state3) {
				var tbl = (oo.tablename.indexOf(",") >= 0) ? oo.tablename.split(",") : [oo.tablename];
				oo.nextbooking = $scope.getnxtbkg(oo.booking);
				tbl.forEach(function(ll) {
					var state = (state3) ? "*" : "";
					ll = ll.trim();
					if(typeof nextable[ll] === "undefined" || nextable[ll] instanceof Array === false)
						nextable[ll] = [];
					nextable[ll].push({ booking: oo.booking, time: oo.time, fullname: oo.fullname, npers: oo.pers, seated: oo.seated, duration: oo.duration, state: state, obj: oo });
					});
				}
			});
						
		tmsLayout.setallnextbkg(nextable);
		
		Object.keys(nextable).forEach(function(tbl) {
			var vtime, w, oo, highlight = 0, state = '', flg;
			vtime = flg = 0;
			w = nextable[tbl];
			w.sort(function(u, v) { return u.obj.vtime - v.obj.vtime; });
			w.map(function(vv) {
				oo = vv.obj;
				if(vtime > 0 && vtime > oo.vtime)
					flg = 1;
				vtime = oo.vtime + oo.duration;
				if($scope.seatedstate.indexOf(oo.state) > -1)
					state = oo.state;
				});
			tmsLayout.showResaInfo(tbl, oo.booking, oo.last, oo.pers, state, highlight, oo.nextbooking, oo.captain, oo.duration, oo.cleartime, oo.seated, flg);
			});
		$scope.setgeneralInfo(x);
		};

	$scope.refreshview = function() {
		$scope.view($scope.selectedItem[0]);
		};
				
	$scope.showtablehour = function(x) {
		var i, y, k, slot, fpos, cc, firstcol, tablearr, nextable, mealduration, slot_limit,
			tbname, tbnameAr, showflag, stillseated, now = new Date().getHours();

		if(!x || !x.booking)
			return -1;

		if($scope.fixedviewmode === 1) 
			return $scope.showtablefixview(x);
		
		slot = x.slot;
		if(x.hh > 8 && x.hh < 24)
			$scope.setshowHour(x.hh);
	
		$scope.viewslot = x.slot;	
		cc = (x.hh > 12) ? (x.hh - 12) : x.hh;
		cc = (x.hh < 12) ? cc + " am" : cc + " pm";

		$scope.viewtime = cc;		
		x.highlight = 1; // 0->white, 1->red, 2->yellow

		firstcol = [];
		tablearr = [];
		fpos = -1;
		$scope.names.map(function(oo) {
			var mealduration;

			if(typeof oo.duration !== "number" || oo.duration < 30)
				oo.duration = $scope.mealduration * 30;
				
			mealduration = parseInt(oo.duration / 30);
			if(oo.mm > 45 || (oo.mm > 15 && oo.mm < 30))
				mealduration++;
				
			//if(mealduration !== $scope.mealduration) console.log('MEALDURATION', $scope.mealduration, mealduration);
			if(fpos < 0 && oo.hh >= (x.hh - 1))
				fpos = oo.index;

			oo.state2 = "";
			if($scope.keepseated && x.hh === now) {
				if($scope.seatedstate.indexOf(oo.state) > -1)
					if(Math.abs(oo.slot - slot) >= mealduration)
						oo.state2 = "overseated";
				}
				
			showflag = (Math.abs(oo.slot - slot) < mealduration || oo.state2 === "overseated");
			if(oo.tablename !== '' && showflag && tablearr.indexOf(oo.tablename) === -1) {
				if(oo.booking !== x.booking)
					oo.highlight = (oo.slot === slot) ? 0 : 2;
				firstcol.push(oo);
				tablearr.push(oo.tablename);
				}
			});

		tmsLayout.clearallstate();
		nextable = {};

		// compute the fixed view with the nextable object.
		// do it only for all lunch or all dinner
		// use highlight to show collusion
		// 34 is (35 - 1) as the comparaison is strictly
				
		$scope.names.map(function(oo) {
			if(oo.slot > slot && typeof oo.tablename === "string" && oo.tablename.length > 0) {
				var tbl = (oo.tablename.indexOf(",") >= 0) ? oo.tablename.split(",") : [oo.tablename];
				tbl.map(function(ll) {
					if(typeof nextable[ll] === "undefined" || nextable[ll] instanceof Array === false)
						nextable[ll] = [];
					nextable[ll].push({ booking: oo.booking, time: oo.time, fullname: oo.fullname, npers: oo.pers });
					});
				}
			});
						
		tmsLayout.setallnextbkg(nextable);

		firstcol.map(function(oo) {
			oo.nextbooking = $scope.getnxtbkg(oo.booking);
			tmsLayout.showResaInfo(oo.tablename, oo.booking, oo.last, oo.pers, oo.state, oo.highlight, oo.nextbooking, oo.captain, oo.duration, oo.cleartime, oo.seated);
			if($scope.triggerevent)
				$scope.cronjob(oo.booking, $scope.mealduration);
			});
						
		$scope.setgeneralInfo(x);
		$scope.paginator.showIndex(x.index - fpos);		
		};

	
	$scope.view = function(x, flg) {

		var cc =  new Date().getTime(), dd = $scope.lastbkvclick;
		$scope.lastbkvclick = cc;
		
		if(!x || !x.booking)
			return -1;

		// intempestive double click
		if(typeof flg === "number" && flg === 1 && dd > 0 && cc - dd < 1000) 
			return;
			
		$scope.hidemultiple = false;				
		
		if($scope.growwindowonlyonce === 1 && tmsUI === 1)
			openLeftup(250);
		
		$scope.growwindowonlyonce = 0;		
		$scope.classcolor = 'classblack';
		$scope.selectedItem = [x].slice(0);
		$scope.selectedItem[0].createdate = $scope.selectedItem[0].createdate.replace(/ .*/, '');
		x = $scope.selectedItem[0];
		x.nextbooking = $scope.getnxtbkg(x.booking);

		//if(x.tablename === '') { x.tablename = 'not assigned'; x.tblnpers = 'not known'; }
		if(x.tablename !== '' && typeof $scope.tables[x.tablename] !== 'undefined') x.tblnpers = $scope.tables[x.tablename] & 0xff;		
		if(x.bkstatus === '') x.bkstatus = ' ';
		if(x.state !== '' && typeof $scope.colorstate[x.state] !== undefined) $scope.classcolor = 'class' + $scope.colorstate[x.state];
		//for(y in x) if(x[y] === '' && y !== 'tablename') x[y] = null; // dont show null value			
		
		tmsLayout.setSelectedBooking( [x.booking, x.tablename, x.pers, x.state, x.nextbooking] );
		if($scope.BlockedTablesAr.length > 0)
			tmsLayout.setBlockTables($scope.BlockedTablesAr);
		$scope.showtablehour(x);
		$scope.updateclock(x);
		};

	$scope.updateclock = function(x) {
		if($scope.fixedviewmode === 0) {
			var cc = (x.hh > 12) ? (x.hh - 12) : x.hh;
			var dd = (x.mm >= 30) ? ":30" : "";
			cc = (x.hh < 12) ? cc + dd + " am" : cc + dd + " pm";
			tmsLayout.setClock(cc);
			}
		else if(["lunch", "dinner"].indexOf($scope.cTimeselection) > -1) {
			tmsLayout.setClock($scope.cTimeselection);
			}
		else tmsLayout.setClock("");
		}
		
	$scope.setviewbyselection = function(type, time) {
		var i, limit = $scope.names.length, 
		    found = -1, flg, vtime = 5000, mxtime = 0,
		    hh = $scope.dinnertime.hh;
		    
		if((type !== "hours" && type !== "lunch" && type !== "dinner") || time <= 0)
			return;

		if(type === "hours") {	
			mxtime = time * 60;
			for(i = 0; i < limit; i++)
				if($scope.names[i].vtime >= mxtime && $scope.names[i].vtime < vtime) {
					if($scope.fltrSeatedbkg === 0 || ["to come", "waiting"].indexOf($scope.names[i].state) > -1) {
						vtime = $scope.names[i].vtime;
						found = i;
						break;
						}
					}
			}
		else {
			flg = ((type === "dinner" && time >= hh) || (type === "lunch" && time < hh));
			for(i = 0; i < limit; i++)
				if($scope.names[i].mealtype === type && $scope.names[i].vtime < vtime && (flg === false || $scope.names[i].hh >= time) ) {
					if($scope.fltrSeatedbkg === 0 || ["to come", "waiting"].indexOf($scope.names[i].state) > -1) {
						vtime = $scope.names[i].vtime;
						found = i;
						break;
						}
					}
			}		

		if(found >= 0) {
			$scope.view($scope.names[found]);
			}
		};

	$scope.gotocurrentime = function() {
		var type, hh = $scope.dinnertime.hh, now = new Date().getHours();
		type = (now < hh) ? "lunch" : "dinner";
		
		$scope.searchText.v = "";
		$scope.setshowMode(type, 1);
		};
							
	$scope.triggerBooking = function(booking) {
		var ind = $scope.names.fdBooking(booking);

		if(ind < 0)
			return -1;
		$scope.view($scope.names[ind]);
		$scope.fdigest();
		};
		
	$scope.viewhour = function(hh) {
		$scope.view($scope.firstimeslot(hh));
		};
				
	$scope.firstimeslot = function(hh) {
		var i, tt;

		if(hh === 99) 
			hh = new Date().getHours();

		tt = hh * 60;
		for(i = 0; i < $scope.names.length; i++)
			if(tt <= $scope.names[i].vtime) {
				$scope.alert('changing hour ' + hh + ':00, ' + $scope.names[i].booking);
				return $scope.names[i];
				}
		return null;
		};
		
	$scope.setshowHour = function(hh) {
		var now = new Date().getHours();
		if(hh === 'activate') 
			$scope.ctime = hh = now;
		else if(hh === 'desactivate')
			$scope.ctime = -1;
		else if(hh ===  'reset') 
			hh = now;
		else if($scope.ctime === hh)
			return;

		if($scope.ctime < 0 || $scope.editmode === 1)
			return;
			
		$scope.mealtype = "";			
		$scope.ctime = hh;
		$scope.clicktime = now;
		$scope.updatectime();
		$scope.paginator.setPage(0);
		};

	$scope.scheduler = function() {
		var dd = new Date(), vtime;
		
		vtime = (dd.getHours() * 60) + dd.getMinutes();
		$scope.decWaiting();
		};
						
	$scope.updatectime = function() {		
		var now = new Date().getHours(), newselection;

		if($scope.names.length <= 0) 
			return;
		
		if(++$scope.tasktimer > 99) 
			$scope.tasktimer = 0;

		if($scope.ctime > 0)
			if($scope.clicktime !== now) {
				if(newselection = $scope.firstimeslot(now))
					$scope.view(newselection);
				}
		
		else if(["lunch", "dinner"].indexOf($scope.mealtype) > -1) {
			$scope.setviewbyselection($scope.mealtype, now);
			}
			
		$scope.scheduler();
		
		if($scope.myTimer) clearTimeout($scope.myTimer);
		$scope.myTimer = setTimeout( $scope.updatectime, 60000);
		};

	$scope.getshowMode = function() {
		if($scope.selectedItem instanceof Array && $scope.selectedItem.length > 0) {
			var x = $scope.selectedItem[0];
			return { a: $scope.cTimeselection, b: $scope.ctime, c: x.booking };
			}
		else return { a: "", b: "", c: "" };
		};
		
	$scope.resetshowMode = function() {
		var oo = $scope.prevoo;
		if(!oo || !oo.c || typeof oo.c !== "string" || oo.c === "") {
			$scope.prevoo = null;
			return false;
			}
			
		var ind = $scope.names.fdBooking(oo.c);
		if(ind < 0) {
			$scope.prevoo = null;
			return false;
			}
			
		$scope.cTimeselection = "";
		$scope.ctime = oo.b;
		$scope.setshowMode(oo.a);
		$scope.view($scope.names[ind]);
		$scope.prevoo = null;
		return true;
		};
		
	$scope.setshowMode = function(val, options) {
		var prev = $scope.cTimeselection, hh;
		
		if(typeof options !== "number" || [0,1].indexOf(options) < 0) 
			options = 0;
		
		val = val.toLowerCase();
		if(val === "hours") {
			$scope.setshowHour('activate');
			}
		else {
			$scope.mealtype = (val === "lunch" || val === "dinner") ? val : "";
			$scope.setshowHour('desactivate');
			}
		$scope.cTimeselection = val;
		$scope.paginator.setPage(0);
		if(["hours", "lunch", "dinner"].indexOf($scope.cTimeselection) > -1 && ($scope.cTimeselection !== prev || options === 1)) {
			hh = ($scope.cTimeselection === "hours") ? $scope.ctime : new Date().getHours();
			$scope.setviewbyselection($scope.cTimeselection, hh);
			if($scope.fixedviewmode === 1) 
				return $scope.showtablefixview($scope.selectedItem[0]);
			}
		};

	$scope.cleargeneralInfo = function() {
		$scope.aBookingInfo = $scope.aTableInfo = $scope.aProfilInfo = [];
		};

	$scope.setgeneralInfo = function(x) {	
		var i, tmpAr, tbname, hguest;

		if(!x || !x.booking)
			return -1;

		hguest = (x.hotelguest > 0) ? "yes" : "";
		$scope.curbooking = x.booking;
		$scope.aProfilInfo = [ {label: "Title", value: x.salutation }, {label: "First Name", value: x.first}, {label: "Last Name", value: x.last}, {label: "Email", value: x.email}, {label: "Mobile", value: x.phone}, {label: "Repeat", value: x.repeat }, {label: "Company", value: x.company }, {label: "Hotel Guest", value: hguest } ];	// {label: "Birthday", value: "Aug 8"} , {label: "food pref", value: "fish, chocolat"}
		$scope.aBookingInfo = [ {label: "Confirmation", value: x.booking, c:''}, {label: "Status", value: x.bkstatus, c:''}, {label:'Name', value:x.fullname, c:'' }, {label:'Email', value:x.email, c:'' }, {label:'Mobile', value:x.phone, c:'' }, {label: "Reservation Time", value: x.time.substr(0, 5), c:''}, {label: "Number of Persons", value: x.pers, c:''}, {label: "Reservation Date", value: x.ddate, c:'date'}, {label: "Table", value: x.tablename, c:''}, {label: "Request", value: x.comment, c:'', w:20 }, {label: "Notes", value: x.fullnotes, c:'', w:20}, {label: "Win", value: x.wheelwin, c:''}, {label: "ResCode", value: x.restCode, c:''}, {label: "Tracking", value: x.tracking, c:'', w:20}, {label: "Staff", value: x.booker, c:''}, {label: "Company", value: x.company, c:''}, {label: "Hotel Guest", value: hguest, c:''}, {label: "membCode", value: x.membCode , c:''}, {label: "Booking Type", value: x.type, c:''}, {label: "Create Date", value: x.createdate , c:'date'}, {label: "Special occasion", value: x.event , c:''}, {label: "Repeat Customer", value: x.repeat , c:''}, {label: "Contact Person", value: x.contact , c:''} ];

		$scope.theConf = x;
		
		$scope.tablesAr = $scope.getattriabletable(x, 1, 1); // pax mode, autoattribution mode
		if(x.tablename !== "") {
			tmpAr = x.tablename.split(",");
			for(i = 0; i < tmpAr.length; i++) {
				if($scope.tablesAr.indexOf(tmpAr[i]) === -1)
					$scope.tablesAr.push(tmpAr[i]);
				}
			}
		$scope.tablesAr.sort(function(a, b) { return parseInt(a) - parseInt(b) } );
		
		tbname = (x.tablename === "") ? "-" : x.tablename;
		if(x.state === "left")
 			$scope.aTableInfo = [ {label: "Confirmation", value: x.booking, href: 0 }, {label: "Table", value: tbname, href: 0 }, {label: "table size", value: x.tblnpers, href: 0 }, {label: "State", value: x.state, href: 0 } ];
		else if(x.tablename === "" && x.state !== "no show")
			$scope.aTableInfo = [ {label: "Confirmation", value: x.booking, href: 0 }, {label: "Table", value: tbname, href: 2 }, {label: "aut", value: "auto attribute table", href: 1 }, {label: "State", value: x.state, href: 1 } ];
		else if(x.tablename === "" && x.state === "no show")
			$scope.aTableInfo = [ {label: "Confirmation", value: x.booking, href: 0 }, {label: "State", value: x.state, href: 1 } ];
		else $scope.aTableInfo = [ {label: "Confirmation", value: x.booking, href: 0 }, {label: "Table", value: tbname, href: 2 }, {label: "table size", value: x.tblnpers, href: 0 }, {label: "State", value: x.state, href: 1 } ];
		
		$scope.TableInfoObj = { 'booking':x.booking, 'tbsize': x.tblnpers, 'state': x.state };
		
		$scope.printurl = "weeloypro://print_receipt/" + $scope.restaurant + "/" + x.booking + "/" + token;
		if($scope.printerIP !== "")
			$scope.printurl += "/TCP:" + $scope.printerIP;
			
		$("#printsummary").attr("href", $scope.printurl);
		$scope.showbooking = true;
		$scope.fdigest();
		};
		
	$scope.settingtable = function(label) {
		var action, x;
		x = $scope.selectedItem[0];
		$scope.mydata.id = x.booking;
		$scope.mydata.showpersoinfo = false;

		if(label === 'Table' || label === 'atb') {
			action = (label === 'Table') ? 'change' : 'add';
			$scope.mydata.action = action;
			$scope.mydata.booking = x;
			$scope.mydata.aArray = $scope.getattriabletable(x, 1, 1); // pax mode, autoattribution mode
			$scope.showModal('Layout', $scope.mydata.aArray[0], $scope.assigntable, "modalgen2.html", $scope.mydata, $scope.prepostcall, 'sm');	// "modaltable.html"
			}
		else if(label === 'State') {
			$scope.mydata.aArray = bookService.validState();
			$scope.showModal('State Assignment', x.state, $scope.assignstate, "modalgen2.html", $scope.mydata, $scope.prepostcall, 'sm');
			}
		else if(label === 'aut') 
			return $scope.fastattribution(x);
		};

	$scope.setCaptainTable = function(x) {
		if($scope.captainflg !== 1 || $scope.fulldata instanceof Array === false || $scope.fulldata.length < 1 || typeof x.tablename !== "string" || x.tablename.length < 1) 
			return;
		
		var ll, tbl = x.tablename, flg = (tbl.indexOf(",") > -1);	
		for(var i = 0; i < $scope.fulldata.length; i++) {
			ll = $scope.fulldata[i][1];
			if(ll === tbl || (flg && tbl.indexOf(ll) > -1) ) {
				x.captain = $scope.fulldata[i][3];
				return;
				}
			}
		};

	$scope.assigntablebkgfromlayout = function(booking, tablename, action) {
		$scope.localtitle = tablename;		//libservice multiple-select
		$scope.hidemultiple = true;
		$scope.assigntablebkg(booking, tablename, action);
		};
				
	$scope.assigntablebkg = function(booking, tablename, action) {
		var ind = $scope.names.fdBooking(booking);
		if(ind >= 0)
			$scope.lysettable($scope.names[ind], tablename, action);
		$scope.fdigest();
		};

	$scope.assigntable = function(data) { 
		var booking = data.id, tablename = data.name, action = data.action;
		
		return $scope.assigntablebkg(booking, tablename, action);
		};
					
	$scope.assignstate = function(data) { 
		var ind, state = data.name, booking = data.id;
		if((ind = $scope.names.fdBooking(booking)) < 0)
			return -1;
		$scope.lysetstate($scope.names[ind], state);
		};
					
	$scope.lysettable = function(x, tablename, action) {

		$scope.selectedItem = [x].slice(0);
		if(x.tablename !== "") {
			tmsLayout.resetResaInfo(x.tablename, x.booking);
			if(action === 'reset' || tablename === "" ) tablename = '';
			else if(action === "add")
				tablename = x.tablename + "," + tablename;
			else if(action === "delete" && x.tablename.length > 0) {
				tablename = x.tablename.replace(tablename, "").replace(",,", ",").replace(/^,|,$/, "");
				}
			else if(action === "set");
			}
		x.tablename = tablename;
		$scope.setsearchtable(x);

		if(x.tablename === "") x.captain = "";
		else $scope.setCaptainTable(x);
		
		if($scope.bookingview === 'timelineview.html')
			tmsTimeline.refresh();

		bookService.savechgtableservice($scope.restaurant, [x.booking], [x.tablename], [x.captain], token).then(function(response){ 
			if(response.status === 1) { 
				if($scope.tmsSync === 1 && $scope.datePickerDoor.today === 1) 
					$scope.sync.obj.senddata("table;" + x.booking + ";" + x.tablename);
				}
			$scope.view(x); 
			});
		};

	$scope.getposStatus = function(booking) {
		var x, pstate, index = $scope.names.inObject('booking', booking);
		console.log('getposStatus', booking, index);
		if(index < 0) return;
		x = $scope.names[index];
		if($scope.tmspos === 1 && x.state === "seated" && x.restable === '' && x.statepos !== 'done' && x.statepos !== 'error') {
			try {
			bookService.statusPOS($scope.restaurant, x.booking, token).then(function(response){
				console.log('POSGETSTATUS', response); 
				if(response.status !== 1 || !response.data.state || typeof response.data.state !== "string" || response.data.state === "error" ) {
					x.statepos = 'error';
					return;
					}
				pstate = response.data.state;
				if(pstate === "close" || pstate === "gotorder") {
					x.statepos = "done";
					}
				else if(pstate === "newtable") {
					x.statepos = "done";
					x.restable = response.data.new_table;
					}
				else if(pstate === "nochange") {
					setTimeout($scope.getposStatus.bind(null, x.booking), 600000);
					}
				});
			} catch(e) { console.error("POSUPDATE", e.message); }
			}
		};
		
	$scope.lysetstate = function(x, state) {
		var hh, mm, curtime = new Date();
		var ltime = curtime.getTime() - $scope.today.getTime();	
		var crontime =  (x.vtime * 60000) - ltime;
		var seattime, ninetymns = $scope.mealduration * 30 * 60000;
		var inter = 15 * 60000;
		var tb;
		
		x.state = state;
		if(x.state === "no show") {
			$scope.lysettable(x, "", "set");
			}
		else if(x.state === "left") {
			tb = x.tablename;
			if(typeof tb === "string" && tb !== "" && tb.substr(0,2) !== "L:") {
				tmsLayout.resetResaInfo("", x.booking);
				x.tablename = "L:" + tb.replace(/,/g, ",L:");
				}
			}
		else if($scope.seatedstate.indexOf(x.state) > -1) {			
			if(x.myTimerSeater)
				clearTimeout(x.myTimerSeater);
			
			seattime = crontime+ninetymns;
			if(Math.abs(seattime) < inter) {
				if(seattime - inter < 3000) seattime = inter + 3000;
				x.myTimerSeater = setTimeout($scope.cronjob, seattime, x.booking, 2);
				}
			}
		
		if(x.state === "seated" && x.seated === "") {
			hh = curtime.getHours();
			mm = curtime.getMinutes();
			x.seated = ((hh < 10) ? "0" : "") + hh + ":" + ((mm < 10) ? "0" : "") + mm;
			}
		if(x.state === "partially seated" && x.colorcode === '0') x.colorcode = '6';
		else if(x.state !== "partially seated" && x.colorcode === '6') x.colorcode = '0';
							
		bookService.savechgstateservice($scope.restaurant, x.booking, x.state, token).then(function(response){ 
			if(response.status === 1) {
				if($scope.tmsSync === 1 && $scope.datePickerDoor.today === 1) 
					$scope.sync.obj.senddata("state;" + x.booking + ";" + x.state);
				}
			$scope.view(x); 
				
			if($scope.tmspos === 1 && x.state === "seated" && x.restable === '' && x.statepos === '')
				try {
				bookService.openPOS($scope.restaurant, x.booking, token).then(function(response){
					console.log('POSOPEN', response); 
					if(response.status === 1) {
						x.statepos = 'open';
						setTimeout($scope.getposStatus.bind(null, x.booking), 60000);
						}
				});
				} catch(e) { console.error("POSOPEN", e.message); }
			});
		};

	$scope.lyanalytic = function(inc) {
 		if($scope.tmsDrawnflg === false || $scope.indexLayout < 0)
 			return $scope.alert('Unable to show the timeline before the layout drawing were activated');
 			
		if(inc < 2) {
			$scope.blackbg = true;
			$scope.bookingview = 'punchchartview.html';
			}
  		
		$scope.tmsAloaded = tmsAnalytic.drawchart($scope, "tms");

		if($scope.tmsAloaded === false && inc < 20)
			$timeout( function(){ $scope.lyanalytic(++inc); }, 100);
		};
		
	$scope.lytimeline = function(inc) {
  		var mode, cursorlunch, cursordinner, ctime;
  		
  		mode = $scope.defaultmodemealtype ? 1 : 0;
  		cursorlunch = 4;
  		cursordinner = 14;
  		ctime = $scope.defaultmodemealtype ? new Date().getHours() + ":00" : $scope.ctime;
  		
 		if($scope.tmsDrawnflg === false || $scope.indexLayout < 0)
 			return $scope.alert('Unable to show the timeline before the layout drawing were activated');
 			
		if(inc < 2) {
			$scope.blackbg = true;
			$scope.bookingview = 'timelineview.html';
			}
  		
  		// race condition: loading template and preparing raphael...
		$scope.tmsloaded = tmsTimeline.drawtimeline(mode, cursorlunch, cursordinner);
		$scope.refreshInfoToday();

		if($scope.tmsloaded === false && inc < 20)
			$timeout( function(){ $scope.lytimeline(++inc); }, 100);
		else tmsTimeline.sethour(ctime);
		};
	
	$scope.lysaveattribution = function() {
		var objstr, obj, i;
		
		obj = { "restaurant": $scope.restaurant, "data": [] };
		if($scope.names.length <= 0) {
			$scope.alert('no configuration to be saved');
			return;
			}
			
		for(i = 0; i < $scope.names.length; i++) 
			obj.data.push({ "confirmation": $scope.names[i].booking, "tablename": $scope.names[i].tablename });
				
		objstr = JSON.stringify(obj);
		tmsService.tmsSaveTableSetting({ obj: objstr, email: $scope.usremail, token: token }).then(function(response){
			$scope.alert('table configuration has been save for ' +  $scope.names.length + ' reservations');
			});
		};		

	$scope.lyresetattribution = function() {
		var i, newselection, limit = $scope.names.length;
		
		if($scope.confirm("Confirm that you want to reset table attribution ? This action will save the new configuration") === false)
			return;
		for(i = 0; i < limit; i++) {
			if($scope.names[i].state === "to come")
				$scope.names[i].tablename = "";
			}
			
		$scope.lysaveattribution();
		tmsTimeline.refresh();
		tmsLayout.clearallstate();
		newselection = ($scope.selectedItem.length > 0) ? $scope.selectedItem[0] : $scope.firstimeslot(99);
		if(newselection)
			$scope.view(newselection);
		};

	$scope.ResourceAttribution = function() {

		// starting a 8:00. One bit a slot of 1/2 hour => 11:30 is (11 - 8) * 2 + 1 = 7ieme bit
		// every booking is 3 slots
		// booking 11:30 will set to 0, 7, 8, 9 bit
		// our init mask is 7 or 111 (3 slots) 
			
		return {
			data: {},
			bookindex: [],
			svbooking: [],
			svtable: [],
			svcaptain: [],
			start: 9,
			getmask: function(val) {
				var t = val.split(':'),
					i = ((parseInt(t[0]) - this.start) * 2)  + (Math.floor(parseInt(t[1]) / 30));
					if(i > 30) return 0;
					return (7 << i);
				},				
			checkfree: function(index, tbname) {
				var mask, len, obj, flg;
				if(typeof this.data[tbname] === 'undefined')
					return false;

				obj = this.data[tbname];
				mask = this.getmask($scope.names[index].time);
				if(mask === 0) return false;
				len = obj.size - $scope.names[index].pers;
				flg = obj.avail & mask;
				return (flg === mask && len >= 0);
				},
				
			checkmatch: function(index, tbname, diff) {
				var mask, len, obj, flg;
				if(typeof this.data[tbname] === 'undefined')
					return false;
				obj = this.data[tbname];
				mask = this.getmask($scope.names[index].time);
				if(mask === 0) return false;
				len = obj.size - $scope.names[index].pers;
				flg = obj.avail & mask;
				return (flg === mask && len >= 0 && len <= diff);
				},
				
			preallocate: function(index, tbname) {
				var mask, len, obj, flg;
				if(typeof this.data[tbname] === 'undefined')
					return false;

				obj = this.data[tbname];
				mask = this.getmask($scope.names[index].time);
				if(mask === 0) 
					return false;
				len = obj.size - $scope.names[index].pers;
				flg = obj.avail & mask;
				if(flg === mask &&  len >= 0) {
					obj.avail ^= mask;
					return true;
					}
				else console.log('ERROR ALLOCATION: ', tbname, $scope.names[index].booking);
				return false;
				},
				
			allocate: function(index, tbname) {
				var x;
				if(this.preallocate(index, tbname) === true) {
					x = $scope.names[index];
					x.tablename = tbname;
					$scope.setCaptainTable(x);
					this.svbooking.push(x.booking);
					this.svtable.push(x.tablename);
					this.svcaptain.push(x.captain);
					}
				},
				
			free: function(index, tbname) {
				var mask, obj;
				if(typeof this.data[tbname] === 'undefined')
					return false;

				obj = this.data[tbname];
				mask = this.getmask($scope.names[index].time);
				if(mask === 0) return false;
				obj.avail |= mask;					
				},
			};
		};
					
	$scope.lyattribution = function(abooking) {
		var resource, ind, val, vv, tbname, tables, attribTables, limit, i, k, diff, first;

		resource = new $scope.ResourceAttribution();
					
		if($scope.tables === null) {
			$scope.alert("No tables.");
			return;
			}
		
		val = ~0;	// => -1 or ffffffffffff....
		tables = [];
		attribTables = [];
		Object.keys($scope.tables).map(function(tbname) { 
			if($scope.tables[tbname] > 0 && $scope.tables[tbname] < 0x100) {	// table size > 0 and not manually attributed
				resource.data[tbname] = {"size": $scope.tables[tbname], "avail": val };
				tables.push(tbname);
				}	
			});
		tables.sort(function(a, b) { return $scope.tables[a] - $scope.tables[b]; });	

		if(!abooking) { // global or 1 attribution ?
			limit = $scope.names.length;
			for(i = 0; i < limit; i++) {
				if($scope.names[i].state === "to come") {
					$scope.names[i].tablename = '';
					resource.bookindex.push(i);
					attribTables.push(i);
					}
				else resource.preallocate(i, $scope.names[i].tablename);
				}
			} else {
				if((ind = $scope.names.fdBooking(abooking)) < 0)
					return -1;
				$scope.names[ind].tablename = '';
				resource.bookindex.push(ind);
				attribTables.push(ind);
				}
		
		if(attribTables.length < 1) {
			$scope.alert('no booking to be attributed');
			return;
			}
			
		first = attribTables[0];
		vv = resource.bookindex.length;
		for(diff = 0; diff < 8 && resource.bookindex.length > 0; diff++) {
			for(i = 0; i < tables.length; i++) {
				tbname = tables[i];
				for(k = resource.bookindex.length - 1; k >= 0; k--) {
					if(resource.checkmatch(resource.bookindex[k], tbname, diff)) {
						resource.allocate(resource.bookindex[k], tbname);
						resource.bookindex.splice(k, 1);
						}
					}
				}
			}
		
		if(resource.svbooking.length > 0)
			bookService.savechgtableservice($scope.restaurant, resource.svbooking, resource.svtable, resource.svcaptain, token).then(function(response){ 
				if(response.status === 1) { 
					if($scope.tmsSync === 1 && $scope.datePickerDoor.today === 1) 
						$scope.sync.obj.senddata("updatealltable;none;none;");
					}				
				});

		$scope.alert("DONE. Remaining non attributed table : "+resource.bookindex.length);
		if($scope.bookingview === 'timelineview.html')
			$scope.lytimeline(0);
		if($scope.selectedItem.length > 0)
			$scope.view($scope.selectedItem[0]);
		$scope.names.map($scope.setsearchtable);
		};

	$scope.lyautoattribution = function(x) {

		if(x.tablename !== "") {
			$scope.alert("the reservation has already a table '" + x.tablename + "'.\nReset it if you wish to auto reattribute a table");
			return;
			}
		$scope.lyattribution(x.booking);
		if(x.tablename !== "")
			$scope.lysettable(x, x.tablename, 'set');

		};

	$scope.fastattribution = function(x) {
		var reserve = [], tbname, pax = x.pers, slot = x.slot, mealduration, mealtype = 0;

		if(x.tablename !== "") {
			$scope.alert("the reservation '" + x.booking + "' has already a table '" + x.tablename + "'.\nReset it if you wish to auto reattribute a table");
			return;
			}
		
		mealduration = (typeof x.duration === "number" && x.duration > ($scope.mealduration * 30)) ? parseInt(x.duration / 30) : $scope.mealduration;
		//if(mealduration !== $scope.mealduration) console.log('MEALDURATION-fastatt', $scope.mealduration, mealduration);
		$scope.names.map(function(oo) {
			if(oo.tablename !== "" && Math.abs(oo.slot - slot) < mealduration)
				reserve.push(oo.tablename); 
			});

		if($scope.mealtypeattribflg) {
			if(x.mealtype === "lunch") mealtype = 1;
			else if(x.mealtype === "dinner") mealtype = 2;
			else if(x.mealtype2 === "afternoon tea") mealtype = 4;
			else mealtype = 0;
			}
			
		if($scope.mealtypeattribflg && mealtype > 0) {
			$scope.fulldata.map(function(oo) {
				var tbname = oo[1], type = oo[4];
				if(type !== 0 && (type & mealtype) === 0 && reserve.indexOf(tbname) < 0) {
					console.log('RESERVED MEALTYPE', tbname);
					reserve.push(tbname); 
					}
				});
			}
			
		for(tbname in $scope.tables) {
			if($scope.tables.hasOwnProperty(tbname)) {
				if($scope.tables[tbname] >= pax && $scope.tables[tbname] < 0x100 && reserve.indexOf(tbname) < 0) {
					x.tablename = tbname;
					break;
					}
				}
			}
				
		if(x.tablename !== "") {
			x.state = "to come";
			bookService.savechgstateservice($scope.restaurant, x.booking, x.state, token).then(function(response){ 
				if(response.status === 1) {
					if($scope.tmsSync === 1 && $scope.datePickerDoor.today === 1) 
						$scope.sync.obj.senddata("state;" + x.booking + ";" + x.state);
					}
				});
		
			if(x.tablename === "") x.captain = "";
			else $scope.setCaptainTable(x);
			bookService.savechgtableservice($scope.restaurant, [x.booking], [x.tablename], [x.captain], token).then(function(response){ 
				if(response.status === 1) { 
					if($scope.tmsSync === 1 && $scope.datePickerDoor.today === 1) 
						$scope.sync.obj.senddata("table;" + x.booking + ";" + x.tablename);
					}
				$scope.view(x); 
				$scope.alert(x.booking + ' <-> ' + x.tablename); 
				});
			}
		};
		
	$scope.getattriabletable = function(x, paxmode, attribmode) {
		var reserve = [], tbname, tables=[], pax = x.pers, slot = x.slot;

		$scope.names.map(function(oo) {
			var mealduration = (typeof oo.duration === "number" && oo.duration > ($scope.mealduration * 30)) ? parseInt(oo.duration / 30) : $scope.mealduration;
			//if(mealduration !== $scope.mealduration) console.log('MEALDURATION-getatt', $scope.mealduration, mealduration);
			if(oo.tablename !== "" && Math.abs(oo.slot - slot) < mealduration) {
				oo.tablename.split(",").map(function(tbl) {  if(tbl.length > 0) reserve.push(tbl); });
				}
			});
				
		tables = Object.keys($scope.tables).filter(function(tbname) { 
			var val = $scope.tables[tbname];
			return (val > 0 &&  (paxmode || val >= pax) &&  (attribmode || val < 0x100) &&  reserve.indexOf(tbname) === -1); 	// table size > 0 and not manually attributed unless mode is 1
			});			
		tables.sort();
		return tables.slice(0);	
		};
		
	$scope.refreshInfoToday = function() {
		var tables, k, totalpax, totaltablepax, data;
		
		//$scope.statsview = 'emptyview.html';
		//$scope.statsview = 'aBooking.html';
			
		if($scope.names.length <= 0)
			return;

		tables = [];
		totaltablepax = 0;
		data = $scope.getStatsPax();
		totalpax = data.totalpax;
		Object.keys($scope.tables).map(function(ll) { 
			if($scope.tables[ll] > 0) { 
				tables.push(ll);
				totaltablepax += ($scope.tables[ll] & 0xff);
				} 
			});
		
		$scope.tableoccupied = "";	
		$scope.paxoccupied = "";	
		if(tables.length > 0) {
			k = (($scope.names.length * 3) / (tables.length * (24 - 9) * 2)) * 100;  // 24 - 9 = number of hour * 2 = number of slot per table
			$scope.tableoccupied = k.myPrecision(1) + "%";
			k = ((totalpax * 3) / (totaltablepax * (24 - 9) * 2)) * 100;  // 24 - 9 = number of hour * 2 = number of slot per table
			$scope.paxoccupied = k.myPrecision(1) + "%";
			}
			
		$scope.todayinfo = [ { label: "", value: $scope.today.toDateString() }] 
		//{ label: "Occupation Table", value: $scope.tableoccupied}, { label: "Occupation Pax", value: $scope.paxoccupied}, 
		
		$scope.todayinfo.push({ label: "", value: ""}, { label: "Total", value: ""},
								  { label: "Bookings", value: data.total }, { label: "Pax", value: totalpax }, 
								  { label: "Walk-ins Booking", value: data.daywalkins }, { label: "Walk-ins Pax", value: data.daywalkinspax });
								  
		$scope.todayinfo.push({ label: "", value: ""}, { label: "Lunch", value: ""},
								 { label: "Booking", value: data.totall }, { label: "Pax", value: data.totallpax }, 
								 { label: "Walk-ins Booking", value: data.lunchwalkins }, { label: "Walk-ins Pax", value: data.lunchwalkinspax });

		if($scope.afternoonflg && data.totalt > 0) {
				$scope.todayinfo.push({ label: "", value: ""}, { label: "Afternoon Tea", value: ""}, 
								   { label: "Booking", value: data.totalt }, { label: "Pax", value: data.totaltpax }, 
								   { label: "Walk-ins Booking", value: data.teawalkins }, { label: "Walk-ins Pax", value: data.teawalkinspax });
			}
			
		$scope.todayinfo.push({ label: "", value: ""}, { label: "Dinner", value: ""}, 
								 { label: "Booking", value: data.totald }, { label: "Pax", value: data.totaldpax }, 
								 { label: "Walk-ins Booking", value: data.dinnerwalkins }, { label: "Walk-ins Pax", value: data.dinnerwalkinspax });

		if($scope.twositting === 1) {
				$scope.todayinfo.push({ label: "P1 bookings", value: data.total2s1 },
									 { label: "P1 pax", value: data.total2s1pax },
									 { label: "P2 bookings", value: data.total2s2 },
									 { label: "P2 pax", value: data.total2s2pax } );
			}
		};

		$scope.emptyOrNull = function(item) { 
			var tmp = item.value;
			return !(tmp === null || typeof tmp === 'undefined' || (typeof tmp === 'string' && tmp.trim().length === 0));
		};

	$scope.checkprevemail = function(which, modaldata) {
		var oo, funct, email = modaldata.email, mobile = modaldata.mobile,  org = ["email", "mobile", "lastname", "firstname", "salutation", "company", "country"];
		var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;	

		if(which === 1) {
			funct = bookService.bookingemail;
			arg = email;
			if(email == "" || regex.test(email) == false) {
				$scope.alert("Invalid Email");
				return;
				}
		} else {
			if(typeof mobile !== "string" || mobile.length < 5) {
				$scope.alert("Invalid Mobile");
				return;
				}
			if(mobile[0] !== "+") {
				mobile = mobile.replace(/[^\d]/g, '');
				if(mobile.length == 8)
					modaldata.mobile = mobile = "+65 " + mobile;
				}

			funct = bookService.bookingmobile;
			arg = mobile;
			}
		
		funct($scope.restaurant, arg).then(function(response){ 
			if(response.status > 0) {
				oo = response.data;
				org.map(function(ll) { if(oo[ll] && typeof oo[ll] === "string" && oo[ll] !== "") modaldata[ll] = oo[ll]; });
				}
			});

		};

	$scope.helpservice = (function() { 
		var oo = {
			imglogo: "../images/logo_w.png",	

			labels: [],
			alltheme: {},
			
			helpgeneral: [
				{ a: "beginning", b: "to run the application, you first need to create a layout, and set it to default layout", c: "" },
				{ a: "start", b: "click on top menu 'LAYOUT->start'", c: "" },
				{ a: "mode", b: "there are 2 modes, LAYOUT for layout editing, RUN for table management (also called timeline)", c: "" },
				{ a: "prefs", b: "There are 2 prefs screens, first one under 'PREFS', the latter under 'RUN'. This provides functionality and display customizations", c: "" },
				{ a: "screen partitions", b: "there are 2 adjustable partitions, called screens: layout screen (right), service screens (left)", c: "" },
				{ a: "service screen", b: "the following service will use the service screens: booking, waiting, last booking, timeling, icicle, captain settings", c: "" },
				{ a: "modal Services", b: "Most other services are available through a 'modal' window: callcenter, calendar, sticky notes, chat, covers/analytics, profiles, printing, modifications, table attribution ...", c: "" },
				{ a: "navigation", b: "all the screens are accessable from the menu selection. You can go back and forth between screen, the system retain the information", c: "" },
				{ a: "saving", b: "the system does not save any information, unless you explicitly do so (layout and table attribution). Once saved, the information is there for your next session", c: "" }
				],

			helplayout:  [
				{ a: "new layout", b: "give a name to your future layout", c: "" },
				{ a: "read you layout", b: "click on top menu 'CONFIGURATION->read', and select a layout", c: "" },
				{ a: "save you layout", b: "click on top menu 'CONFIGURATION->save', and enter a name if not yet done", c: "" },
				{ a: "create table", b: "drag a table on the foor", c: "" },
				{ a: "table name/size", b: "double click on a table and enter the appropriate information", c: "" },
				{ a: "select table", b: "click on a table when table is on the floor", c: "" },
				{ a: "deselect anything", b: "click on the floor", c: "" },
				{ a: "drag info window", b: "click on info window and drag it to where you want", c: "" },
				{ a: "multiple select", b: "select one table, then then click on another table while pressing the shift key", c: "" },
				{ a: "multiple select", b: "double click on the floor, and drag the little blue square to increase its size. Object within the blue square will be selected", c: "" },
				{ a: "duplicate table", b: "select table/object, and shift 1", c: "" },
				{ a: "duplicate multiple tables", b: "select multiple tables/objects, and shift 1", c: "" },
				{ a: "delete table", b: "drag table to the trash, or select a table and click on <delete key>", c: "" },
				{ a: "delete multiple tables", b: "select multiple tables and then use same action as a standard delete", c: "" },
				{ a: "move table", b: "select a table and drag it to where you want", c: "" },
				{ a: "move multiple tables", b: "select multiple tables, and drag it to where you want", c: "" },
				{ a: "move table with keys", b: "select a table or multiple tables, and move right 'd', left 'a', up 'w', down 's'", c: "" },
				{ a: "move table with keys", b: "select a table or multiple tables, and move right '6', left '4', up '2', down '8'", c: "" },
				{ a: "align multiple table", b: "select multiple table, key 'h' for horizontal aligment (top table as reference), 'v' for vertical alignment", c: "" },
				{ a: "resize table", b: "select a table, click on the red mark and drag it", c: "" },
				{ a: "scale table with key", b: "select a table, key 'p' for plus, 'm' for minus", c: "" },
				{ a: "scale table with key<br>height/width", b: "as above. Use 'shift key' for height only, and 'alt key' for width only", c: "" },
				{ a: "turn table", b: "select a table, click on the yellow mark and spin it", c: "" },
				{ a: "turn table with key", b: "select a table, key 't' for clockwise turn, 'T' for counterclockwise", c: "" },
				{ a: "front/back view for table", b: "select a table, key 'f' for front view, 'b' back view", c: "" },
				{ a: "create a state table", b: "select a table, key 'j' for to come, or 'k' seated, or 'l'...", c: "" },
				{ a: "set state table", b: "key '/'", c: "" },
				{ a: "clear state table", b: "key '\\'", c: "" },
				{ a: "show the grid", b: "key '.'", c: "" },
				{ a: "show the name of the object any time", b: "key ',' . In effect, it sets the number of person to -1, instead of 0.", c: "" },
				{ a: "show a table box", b: "select a table, key 'q'", c: "" },
				{ a: "hide a table box", b: "key 'q' again", c: "" },
				{ a: "connect to table", b: "mutiple select JUST 2 tables, key 'c'", c: "" },
				{ a: "deconnect to table", b: "mutiple select JUST 2 tables, key 'C'", c: "" },
				{ a: "not on connected table", b: "single/multiple dragging will keep the connection", c: "" },
				{ a: "run mode", b: "in a run mode, you can hide the view (menu), display information to front (moving cursor of a table), reattribute a reservation by dragging their red label title on another 'empty' table", c: "" }
				],
			
			helpinformation:  [
				{ a: "general information", b: "the upper left screen display general statistic information, as well as a 'selected' booking, and its guest profil", c: "" },
				{ a: "modification", b: "you can modifiy fields in the 'Table Information', such as 'state' or 'table' (set or add)", c: "" },
				{ a: "state", b: "state can be changed to reflected table status: to come, seated, paying, leaving, no show... each state has a specific color. The layout reflects that color state", c: "" },
				{ a: "table attribution", b: "a table can be attributed for a reservation from the layout", c: "" },
				{ a: "add table", b: "this command allows a reservation to be split among several tables. The layout connects tables with white lines to indicate that they are attributed (split) for the same booking", c: "" },
				{ a: "modification", b: "any modification is reflected in the layout display, timeline screen, booking listing...", c: "" }
				],
			
			helpbooking: [
				{ a: "listing", b: "this is the listing of all booking of the day", c: "" },
				{ a: "ordering", b: "every column can be reordered by selecting the column title. The carat shows the direction of order. A pink color shows the current ordering setting. By default, it starts with the time column order", c: "" },
				{ a: "selecting", b: "An input field helps to select any information, such as all guests name, reservation number, table name, time slot, status, state ....", c: "" },
				{ a: "booking", b: "every booking is selectable by clicking on this confirmation number. This action updates the 'Information Screen' (upper left), and the layout shows all the table status for the time slot of the booking as follow: 'red' indicates the selected booking tables, 'white' are the tables allocation for the same time slot, 'yellow' are tables that are allocated during the course of the booking (3 slots). Implications: you cannot reattibute the booking table to any of the displayed color tables", c: "" },
				{ a: "layout", b: "in a run mode, you can hide the view (menu), display information to front (moving cursor of a table), reattribute a reservation by dragging their red label title on another 'empty' table", c: "" }
				],
			
			helptimeline: [
				{ a: "general", b: "this screen display all the table attributions for every booking, for every 1/2 hour slot. Axis on the top shows the time per 1/2 hour. Axis on the left shows the table names", c: "" },
				{ a: "navigation", b: "you can go to the beginning, or to the end, or move to next and previous slot. A double click on a booking, selectes it in the 'General Information' screen (upper left), and reset the layout screen (right) accordingly. That action moves the column slot cursor to that booking slot", c: "" },
				{ a: "attribution", b: "this action resets table attribution (no undo). Every booking is maped to the timeline per table. Brown color shows a perfect match (booking seats and table seats). Purple show larger tables than the booking seat requirement.", c: "" },
				{ a: "current column", b: "the current time slot is indicated with the cyan color (first column). The layout screen reflects that time slot accordingly", c: "" },
				{ a: "layout", b: "in a run mode, you can hide the view (menu), display information to front (moving cursor of a table), reattribute a reservation by dragging their red label title on another 'empty' table", 	c: "" }	
				],
			
			helpanalytic: [
				{ a: "Analytics", b: "this screen show the table occupency statistics for the all week, for every hour", c: "" }
				],
			
			helphotkeysmap: [
				{ a: (opsyswindow) ? "^ is Alt+Shift" : "^ is Ctrl", b: (opsyswindow) ? "use Alt+Shift+letter below" : "use Ctrl+letter  below", c: "" },
				{ a: "escape or ^Z", b: "Close modal window", c: "" },
				{ a: "^R", b: "Refresh the booking data", c: "" },
				{ a: "^K", b: "Full booking - modification", c: "" },
				{ a: "^M", b: "Mdification current booking", c: "" },
				{ a: "^Y", b: "Call Center", c: "" },
				{ a: "^W", b: "Walk-ins new booking", c: "" },
				{ a: "^B", b: "Booking view", c: "" },
				{ a: "^Q", b: "Waiting view (Q for queue)", c: "" },
				{ a: "^X", b: "Recent new bookings view", c: "" },
				{ a: "^T", b: "TimeLine view", c: "" },
				{ a: "^i", b: "Icicle view", c: "" },
				{ a: "^F", b: "Next floor", c: "" },
				{ a: "^S", b: "Statistics", c: "" },
				{ a: "^G", b: "Guest", c: "" },
				{ a: "^P", b: "Print guest", c: "" },
				{ a: "^A or ^1", b: "Show booking view in all mode", c: "" },
				{ a: (opsyswindow) ? "^2":"^L or ^2", b: "Show booking view for lunch", c: "" },
				{ a: (opsyswindow) ? "^3":"^D or ^3", b: "Show booking view for dinner", c: "" },
				{ a: "^H or ^4", b: "Show booking view in hour mode", c: "" }
				],
			
			helpcolorcodes: [
				{ a: "TABLE", b: "", c: "navy" },
				{ a: "table seated", b: "red", c: "red" },
				{ a: "table partially seated", b: "pink", c: "deeppink" },
				{ a: "table main course", b: "green", c: "green" },
				{ a: "table paying", b: "orange", c: "orange" },
				{ a: "table blocked", b: "black", c: "black" },
				{ a: "table available", b: "blue", c: "blue" },
				{ a: "", b: "", c: "" },
				{ a: "LABEL", b: "", c: "navy" },
				{ a: "red", b: "this is the selected booking, and its attributed table", c: "" },
				{ a: "white", b: "tables of booking that are expected to arrive at the same 1/2 hour", c: "" },
				{ a: "yellow", b: "tables of booking where the guests are already seated, or are expected to arrive. That table is not available for attribution of the selected booking", c: "" },
				{ a: "", b: "", c: "" }
				],
				
			showhelp: function(theme) {
				var myhelp, helpwin, content, i;
			
				if(typeof this.alltheme[theme] === 'undefined') {
					$scope.alert("Invalid help section "+theme);
					return;
					}
				
				mydata.showpersoinfo = false;
				mydata.helptheme = this.alltheme[theme];
				mydata.closeButtonText = "Close";
				$scope.showModal(theme, theme, null, "showhelp.html", mydata, null, '');	// size = '' => medium !!!
				},
				
			addkeysmap: function() {
				this.alltheme["Hot Keys"] = this.helphotkeysmap;
				this.labels = Object.keys(this.alltheme);
				}
			}
		oo.alltheme = { "General": oo.helpgeneral, "Layout": oo.helplayout, "Information": oo.helpinformation, "Booking": oo.helpbooking, "Timeline": oo.helptimeline, "Analytics": oo.helpanalytic, "Color codes": oo.helpcolorcodes };
		oo.labels = Object.keys(oo.alltheme);
		return oo;
		})();

	
	// tell callcenter to show waiting/walking... split genflg in 2 part 12bits each
	// most significant part for cmd to cc, 
	// least significant for value in cc, 
	// resolution handled by confirmation in class booking
	// example 0x1000 for walkin (included by default), 0x2000 for waiting, 0x4000 surprise
	// 0x1001 mean show walkin in cc, and walkin has been checked, use half way since included by default (just the result part)
	// 0x2002 mean show waiting in cc, and waiting has been checked
					
	$scope.callcenterurl = function() {
		var time = new Date().getTime();
		$scope.ccflag = 0;
		$scope.ccflag += parseInt("0x1000"); // walkin
		if($scope.waitinglist)
			$scope.ccflag += parseInt("0x2000"); 
		if($scope.nofullcc)
			$scope.ccflag += parseInt("0x4000"); 
		if($scope.sendsms)
			$scope.ccflag += parseInt("0x8000"); 
		
		$scope.cc_url = '../modules/callcenter/callcenter.php?bktracking=CALLCENTER&bktms=87&bkrestaurant=' + $scope.restaurant + '&prompt=' + $scope.prefinfo.getvalue('callcenter') + '&data=' + $scope.callcenter + '&genflag='+$scope.ccflag + '&vers='+time;
		$scope.cc_urlimited = '../modules/callcenter/callcenter.php?bktracking=CALLCENTER&bktms=87&bkrestaurant=' + $scope.restaurant + '&prompt=' + $scope.prefinfo.getvalue('callcenter') + '&data=' + $scope.callcenter + '&genflag='+ ($scope.ccflag | parseInt("0x4000")) + '&vers='+time;
		};

// get table content left menu after sign in

	$scope.checklogin = function() {

		console.log('CHECKlog', new Date());
		
		if(update_session_time() <= 0) 
			return $scope.logout();

 		if(typeof $scope.usremail !== "string" || $scope.usremail === null || $scope.usremail === "" || 
 		   typeof token !== "string" || token === null || token === "")  {
				$scope.usremail = getcookievalues(localcookie, "user");
				token = getcookievalues(localcookie, "token");
				}
			
 		if(typeof $scope.usremail !== "string" || $scope.usremail === null || $scope.usremail === "" ||
 		   typeof token !== "string" || token === null || token === "")  { 
 		   		return $scope.logout(); 
 		   		}

		tmsService.tmsSections({ email: $scope.usremail, token: token }).then(function(response){
			
			if(response.data === null) {
				alert($scope.usremail + ' is not registered for tms/session expires. Logout');
				return $scope.logout();
				}
			});	
			
		clearTimeout($scope.testlogin);
		$scope.testlogin = setTimeout($scope.checklogin, 600000);
 		};

	$scope.readCodeBooking = function() {
		bookService.readCodeBooking($scope.restaurant, $scope.usremail).then(function(response) {
			var content = response.data;
			
			function contcat2help() {
				var helpcolor = [], label = { purple: "purple over yellow", red: "red over yellow", green: "green over yellow", blue: "blue over yellow", orange: "orange over yellow" };
				
				$scope.colorscheme.forEach(function(oo) {
					var vv = {};
					if(oo.code.length < 1 || typeof label[oo.color] !== "string")
						return;
					vv.a = label[oo.color];
					vv.b = oo.code.join(", ");
					vv.c = oo.color;
					helpcolor.push(vv);
					});
				if(helpcolor.length > 0) // title of segment
					helpcolor.splice(0, 0, { a: "GUEST", b: "", c: "navy" });
				$scope.helpservice.helpcolorcodes = $scope.helpservice.helpcolorcodes.concat(helpcolor);
				$scope.helpservice.alltheme["Color codes"] = $scope.helpservice.helpcolorcodes;
				}
				
			if(response.status === 1 && typeof content === 'string' && content !== '') { 
				try {
					var oo = JSON.parse(content.replace(/’/g, "\""));
					$scope.codebooking = oo.data;
					if($scope.restaurant === "SG_SG_R_DbBistroOysterBar") {
						$scope.colorscheme = [	{ color: 'red', code:['CCAF', 'DL', 'IHG', 'PT', 'HBD', 'HA', 'SC', 'MG', 'S&D', 'SR', 'VIP'] },
												{ color: 'green', code: ['PDR'] },
												{ color: 'navy', code: ['PI', 'MDR', 'CAFÉ', 'NPT', 'POST', '(NA)', 'N/A', 'WN', 'VN'] },
												{ color: 'purple', code: ['HC', 'WT', 'BT', 'CT', 'QT', 'NT'] },
												{ color: 'orange', code: ['Miscellaneous'] }];				
					}
					else {
						var tmp = {};
						$scope.codebooking.map(function(oo) { 
							if(!oo.color || typeof oo.color !== "string" || oo.color.length < 3)
								return;
							if(!tmp[oo.color])
								tmp[oo.color] = [];
							tmp[oo.color].push(oo.label);
							});
						$scope.standardcolor.forEach(function(c) {
							if(tmp[c]) $scope.colorscheme.push( { code: tmp[c], color: c} );
							else $scope.colorscheme.push( { code: [], color: c} );
							});
						}
					contcat2help();			
				} catch(e) { console.error("JSON-PROFILE", e.message); }
			   }
			});
		};

	$scope.getstickynotesCalendar = function(func) {
		tmsService.tmsgetCalendar({ restaurant: $scope.restaurant, token: token }, "").then(function(response){
			var found, start = moment().format().substr(0, 10);
			if(response.data && response.data[0] && typeof response.data[0].object === "string" && response.data[0].object.length > 0) {
				try {
					$scope.events = JSON.parse(response.data[0].object.replace(/’/g, "\""));
				} catch(e) { console.error("JSON-TMS", e.message);  $scope.events = {}; }				
				found = $scope.events.some(function(oo) { 
					oo.start = oo.start.substr(0,10);
					if(oo.start === start && oo.title === "STICKYNOTES") {
						func();
						return true;
						}
					return false;
					}); 
				}
			});
		};

	$scope.toggleTurnoffSync = function() {
		$scope.turnoffsync ^= 1;
		};
				
	$scope.toggleMFView = function() {
		$scope.selectedMFloorflg ^= 1;
		};
		
	$scope.updatestate = function() {
		$scope.logaction =  'login';
		$scope.loggedin	= false;
		$scope.logactionpict =  'in';
		$scope.logcolor="warning";

		if(update_session_time() <= 0) { return $scope.logout(); }

		if(typeof $scope.usremail === "undefined" || $scope.usremail === null || true) {
			$scope.usremail = getcookievalues(localcookie, "user");
			token = getcookievalues(localcookie, "token");
			}
			
		tmsService.tmsSections({ email: $scope.usremail, token: token }).then(function(response){
			var i, k, obj, url, sublayout, orgsublayout;
			
			function getdinnertime(atime) {
				var oo = { hh: 16, mm: 0 };
				if(typeof atime  !== "string" || atime.length < 2)
					return oo;
				oo = { hh: parseInt(atime.substr(0,2)), mm: (atime.length > 4) ? parseInt(atime.substr(3,5)) : 0};
				if(oo.hh < 14 || oo.hh > 18) 
					oo.hh = 16;
				if(oo.mm < 0 || oo.mm > 59) 
					oo.mm = 0;
				return oo;
				}
				
			if(response.data === null) {
				alert($scope.usremail + ' is not registered for tms. Logout');
				return $scope.logout();
				}
				
			if(typeof response.data.restaurant !== "string" || response.data.restaurant.length < 8) 
				return alert("Invalid Restaurant. Please check your configuration or call Weeloy");

			$scope.logaction =  'logout';
			$scope.loggedin	= true;
			$scope.logactionpict =  'out';
			$scope.logcolor="danger";
			$scope.countTitle = response.data.count;
				
			$scope.callcenter = response.data.callcenter;
			$scope.restaurant = response.data.restaurant;
			$scope.dbbistro = (["SG_SG_R_DbBistroOysterBar", "SG_SG_R_TheFunKitchen", "SG_SG_R_TheOneKitchen"].indexOf($scope.restaurant) > -1);
			$scope.hidefeatflg = (["SG_SG_R_Pollen", "SG_SG_R_TheFunKitchen"].indexOf($scope.restaurant) > -1);
			$scope.afternoonflg = (["SG_SG_R_Pollen", "SG_SG_R_TheFunKitchen"].indexOf($scope.restaurant) > -1);
			$scope.mealtypeattribflg = (["SG_SG_R_Pollen", "SG_SG_R_TheFunKitchen"].indexOf($scope.restaurant) > -1);
			$scope.twositting = response.data.twositting;
			$scope.captainflg = response.data.captain;
			$scope.printerIP = response.data.PrinterIP;
			$scope.dinnertime = getdinnertime(response.data.dinnertime);
			$scope.tmsSync = parseInt(response.data.tmsSync);
			$scope.tmsv2 = response.data.tmsV2;			
			$scope.tmsv2 = parseInt($scope.tmsv2);
			if(typeof $scope.tmsv2 !== "number" || ($scope.tmsv2 !== 0 && $scope.tmsv2 !== 1)) $scope.tmsv2 = 0;
			if($scope.tmsv2 === 1 && window.location.href.indexOf("indexV2") < 0) {
				url = window.location.href.replace(/\/[^/]*$/, "")+ "/indexV2.php";
				window.location.assign(url);
				}
			
			if(window.location.href.indexOf("indexV2") >= -1)
				$scope.tmsv2 = 1;
			if($scope.tmsv2 === 1)
				$scope.helpservice.addkeysmap();
				
			if($scope.tmsSync === 1) {
				$scope.sync = tmsService.tmsRltimeio($scope.restaurant, $scope.usremail, token, $scope.remotesync, $scope);
				console.log('SYNCHRO - TMS', $scope.sync);
				$scope.chatflg = $scope.stickynotesflg = true;
				}
			else $scope.chatflg = $scope.stickynotesflg = false;

			$scope.getstickynotesCalendar($scope.setcolorstickynotebutton);			
			$scope.tmspos = 0; // response.data.tmspos; /* deactivate no direct API call from TMS to POS */
			$scope.mealduration = 3;
			if(typeof response.data.mealduration === "number" && $scope.mealduration >= 3 && $scope.mealduration <= 5)
				$scope.mealduration = response.data.mealduration;
			console.log('STANDART DURATION', $scope.mealduration * 30);
			if(response.data.restotitle)
				$scope.restotitle = response.data.restotitle;
			$scope.isTheFunKitchen = ($scope.restaurant.indexOf('TheFunKitchen') >= 0);
			$scope.icicle.mode = true; //$scope.isTheFunKitchen
			$scope.section = response.data.section.slice(0);
			$scope.waitinglist = ($scope.section.indexOf("Waitinglist") >= 0);
			$scope.MFflg = true; //($scope.section.indexOf("MultipleFloor") >= 0 || $scope.isTheFunKitchen);
			$scope.nofullcc = ($scope.section.indexOf("NOCC") >= 0);
			$scope.freetxtsms = ($scope.section.indexOf("SMS") >= 0);
			$scope.epson = (($scope.section.indexOf("EPSON") >= 0) && $scope.tablette);
			if($scope.section.indexOf("Layout") === -1)
				$scope.runonlymode = 1; 
			
			$scope.imglogo = mydata.imglogo = response.data.logo;
			$scope.tablecontent = [];
			$scope.layoutdata = [];
			$scope.multiplefloor = [];
			$scope.readCodeBooking();
			if($scope.dbbistro) {
				if((i = $scope.stateAr.indexOf("main course")) > -1) $scope.stateAr[i] = "not ready";
				if((i = $scope.seatedstate.indexOf("main course")) > -1) $scope.seatedstate[i] = "not ready";
				$scope.helpservice.helpcolorcodes.map(function(oo) { if(oo.a.search("main course") >= 0) { oo.a = oo.a.replace(/main course/, "not ready"); oo.b = oo.c = "marron"} });
				}
						
			if(typeof response.data.section !== 'undefined' && response.data.section instanceof Array)
				for(i = 0; i < response.data.section.length; i++) 
					$scope.tablecontent.push({ name: response.data.section[i] });
			
			$scope.tablecontent.push({ name: 'Help' });	
			
			for(i = 0; i < response.data.name.length; i++) {
				try {
				obj = JSON.parse(response.data.object[i].replace(/’/g, "\""));
				} catch(e) { console.error("JSON-TMS", e.message);  continue; }
				
				if(response.data.name[i] === '_defaultlayout') {
					$scope.defaultLayout = obj.data;
					continue;
					}
				if(response.data.name[i] === '_userpreferences') {
					if(obj.email === $scope.usremail) {
						$scope.tabletitle = $scope.lybkgreadviewcustomization(obj.data);
						$scope.bckups = $scope.tabletitle.slice(0);
						}
					continue;
					}
				if(response.data.name[i] === '_userpreferences_prompt') {
					if(obj.email === $scope.usremail) {
						$scope.lyprefreadcustomization(obj.data);
						}
					continue;
					}

				if(obj.floor && obj.floor === 'MULTIPLEFLOOR' && $scope.MFflg)
					$scope.multiplefloor.push({ name: response.data.name[i], object: obj });
				$scope.layoutdata.push({ name: response.data.name[i], object: obj });
				}
									
			$scope.subindexLayout = -1;	
			if($scope.defaultLayout !== '' && $scope.indexLayout === -1) {
				for(i = 0; i < $scope.layoutdata.length; i++)
					if($scope.layoutdata[i].name === $scope.defaultLayout) 
						break;
						
				if(i >= $scope.layoutdata.length)
					return alert("Invalid configuration, unable to locate layout " + $scope.defaultLayout + ". Go to edit mode and select/default a layout.");
					
				$scope.indexLayout = i;
				$scope.nameLayout = $scope.layoutdata[i].name;

				if($scope.layoutdata[i].object.floor === 'MULTIPLEFLOOR') {

					if($scope.layoutdata[i].object.data.length < 1)
						return alert("Invalid configuration on the multifloor layout " + $scope.nameLayout + ". Go to edit mode and add sub layout.");
					
					$scope.selectedMFloor = $scope.layoutdata[i].object.data.slice(0);
					$scope.selectedMFloorflg = 1;
							
					sublayout = orgsublayout = $scope.prefinfo.getvalue('sublayout');
					if(!sublayout || sublayout === "" ||  $scope.layoutdata[i].object.data.indexOf(sublayout) < 0)
						sublayout = $scope.layoutdata[i].object.data[0];

					for(k = 0; k < $scope.layoutdata.length; k++)
						if($scope.layoutdata[k].name === sublayout)
							break;
					
					if(k >= $scope.layoutdata.length)
						return alert("Invalid configuration on the multifloor layout " + $scope.nameLayout + ". Go to edit mode and edit sub layout.");
					
					$scope.subindexLayout = k;	
					if(sublayout !== orgsublayout) {
						$scope.prefinfo.setvalue('sublayout', sublayout);
						$scope.lyprefsavecustomization();
						}										
					}
				}
			
			$scope.callcenterurl();

			if($scope.runonlymode === 1) {
				console.log('LAUCHING RUN', $scope.section);
				$scope.lyrun();
				}
			
			$scope.modifnames = [];	
			$scope.modifnamesIndex = [];
			if($scope.restaurant && $scope.restaurant.length > 5)
				bookService.readmodifBooking($scope.restaurant, 'bydate').then(function(response) { 
					var i, data = response;
					$scope.modifnames = response; 
					for(i = 0; i < data.length; i++) {
						data[i].rdate = data[i].rdate.jsdate().getDateFormat('-');
						$scope.modifnamesIndex.push(data[i].booking);
						}
					});

			});			
		};



	if(window.location.href.indexOf("?a=run") >= 0) {
		$scope.editmode = 99;
		}
	else if(window.location.href.indexOf("?a=edit") >= 0) {
		$scope.runonlymode = 99;
		}


	$scope.restartrun = function() {
		$scope.documentreload('?a=run');
		};
		
	$scope.restartedit = function() {
		$scope.documentreload('?a=edit');
		};
		
	$scope.getTotalPax = function() {
		var i, total = 0;
		for(i = total = 0; i < $scope.names.length; i++) {
			if($scope.names[i].bkstatus === "")
				total += parseInt($scope.names[i].pers);
			}
		return total;
		};

	$scope.getStatsPax = function() {
		var oo, i, data, pax, tbname, size, total, totall, totald, totalpax, totallpax, totaldpax, tbtotalpax, tbtotallpax, tbtotaldpax, total2s1, total2s2, total2s1pax, total2s2pax, totalt, totaltpax, tbtotaltpax, teawalkins, teawalkinspax;

		total = totall = totald = totalt = totalpax = totallpax = totaldpax = tbtotalpax = totaltpax = tbtotallpax = tbtotaldpax = tbtotaltpax = total2s1 = total2s2 = total2s1pax = total2s2pax = daywalkins = daywalkinspax = 0;
		teawalkins = teawalkinspax = lunchwalkins = lunchwalkinspax = dinnerwalkins = dinnerwalkinspax = 0;
		
		data = $scope.names;
		if(data.length <= 0)
			return;
		if($scope.twositting === 1 && typeof data[0].twoStype !== 'number') {
			data.forEach(function(oo) {
				if(oo.slot >= 36 && oo.slot < 40) oo.twoStype = 1;
				else if(oo.slot >= 40) oo.twoStype = 2;
				else oo.twoStype = 0;
				});
			}
			
		data.forEach(function(oo) {
			if(oo.state === "no show")
				return;

			total++;
			tbname = oo.tablename;
			size = (tbname !== "" && $scope.tables[tbname] > 0) ? $scope.tables[tbname] : 0;
			tbtotalpax += size;
			pax = parseInt(oo.pers);
			totalpax += pax;
				
			if(oo.mealtype2 === "afternoon tea") { 
				totalt++;
				totaltpax += pax;
				tbtotaltpax += size;
				if(oo.tracking.indexOf("walkin") > -1) { 
					teawalkins++;
					teawalkinspax += pax;
					} 
			} else if(oo.mealtype === "lunch") { 
				totall++;
				totallpax += pax;
				tbtotallpax += size;
				if(oo.tracking.indexOf("walkin") > -1) { 
					lunchwalkins++;
					lunchwalkinspax += pax;
					} 
			} else {
				totald++;
				totaldpax += pax;
				tbtotaldpax += size;
				if(oo.tracking.indexOf("walkin") > -1) { 
					dinnerwalkins++;
					dinnerwalkinspax += pax;
					} 
				}
				
			if($scope.twositting === 1 && typeof oo.twoStype === 'number' && oo.twoStype > 0) {
				if(oo.twoStype < 2) { total2s1pax += pax; total2s1++; }
				else { total2s2pax += pax; total2s2++; }
				}
			});
		
		daywalkins = lunchwalkins + dinnerwalkins + teawalkins;
		daywalkinspax = lunchwalkinspax + dinnerwalkinspax + teawalkinspax;	
		return { "total": total, "totall": totall, "totald": totald, "totalt": totalt, "totalpax": totalpax, "totallpax": totallpax, "totaldpax": totaldpax, "totaltpax": totaltpax, "tbtotalpax": tbtotalpax,  "tbtotallpax": tbtotallpax,  "tbtotaldpax": tbtotaldpax,  "tbtotaltpax": tbtotaltpax, "total2s1": total2s1, "total2s1pax": total2s1pax, "total2s2": total2s2, "total2s2pax": total2s2pax, "daywalkins" : daywalkins, "daywalkinspax" : daywalkinspax, "lunchwalkins" : lunchwalkins, "lunchwalkinspax" : lunchwalkinspax, "dinnerwalkins" : dinnerwalkins, "dinnerwalkinspax" : dinnerwalkinspax, "teawalkins" : teawalkins, "teawalkinspax" : teawalkinspax };
		};

	$scope.lyshowdayreport = function(flg) {
		var url = './tmsreport/index.html?restaurant=' + $scope.restaurant + '&title=' + $scope.restotitle.replace(/ /g, "_") + '&email=' + $scope.usremail + '&token=' + token + '&meal=' + ((flg === 1) ? 'lunch' : 'dinner');
		win = window.open(url, "DayReport"+Math.random(), "toolbar=yes,location=no,directories=yes,menubar=yes,scrollbars=yes,width=800, height=800, left=100, top=25");
		};
		
	$scope.triggermodifynotes = function(conf, content) {
		var x, ind, pnotestext;
		if((ind = $scope.names.fdBooking(conf)) < 0)
			return -1;
		x = $scope.names[ind];
		pnotestext = x.notestext;
		if(x.notestext  !== "")
			 x.notestext =  x.notestext.replace(/\[[^]+\]/g, '').replace(/\s+/g, ' ');
		content.forEach(function(vv) { if(typeof vv.value === "string" && vv.value.length > 0) x.notestext += "  [" +  vv.label + ":" + vv.value + "]  "; });
		if(pnotestext !== x.notestext) {
			bookService.savechgfieldservice($scope.restaurant, x.booking, x.notestext, "notestext", token).then(function(response){ 
				if(response.status === 1) { 
					if($scope.tmsSync === 1 && $scope.datePickerDoor.today === 1)
						$scope.sync.obj.senddata("notestext;" + x.booking + ";" + x.notestext);
					} 
				});
			}
		console.log("PROFILE DATA", content, x.notestext);
		};

	$scope.viewprofile = function() {
		var ind, content, win, oo, email, phone, invemail, invphone, param, systemid = "";
		if((ind = $scope.names.fdBooking($scope.curbooking)) < 0)
			return -1;

		oo = $scope.names[ind];
		oo.lastvisit = "";
		email = oo.email;
		phone = oo.phone;
		invemail = (typeof email !== 'string' || email.substr(0, 3) === "no@" || email.search("@email.com") > -1);
		invphone = (typeof phone !== 'string' || phone.replace(/[^\d]/g, '').length < 8);
		if(invemail && invphone)
			return	$scope.alert("Invalid email "+email+ " and invalid phone number "+phone);

		if(invphone) phone = "";
		if(invemail) email = "";
		
		if($scope.profilwind) {
			$scope.profilwind.close();
			$scope.profilwind = null;
			}

		// bookService.summaryBooking($scope.restaurant, $scope.curbooking).then(function(response) { console.log('SUMMARY', response); });
			
		bookService.read1Profile($scope.restaurant, email, phone, $scope.usremail).then(function(response) {
			var data = response.data, attr, ppAr, qqAr, tt, label, systemid = "", codedata = "";
			
			if(response.status <= 0) {
				$scope.alert("No profile for "+email);
				return;
				}
			$scope.cprofiledata = response.data; 
			
			if(typeof data.lastvisit === "string")
				oo.lastvisit = data.lastvisit.datereverse();
			if(typeof data.firstvisit === "string")
				oo.firstvisit = data.firstvisit.datereverse();
			if(typeof data.systemid === "string")
				systemid = data.systemid;

			obj = profileService.profileObj(data, $scope.restaurant, systemid, token, "tms", email, phone);
			if(typeof obj.multipleresto !== "number" || [0,1].indexOf(obj.multipleresto) < 0)
				obj.multipleresto = 0;
			mydata.profile = [ {label: "Guest", value: oo.salutation + " " + oo.fullname }, {label: "Pax", value: oo.pers}, {label: "Date", value: oo.date}, {label: "Time", value: oo.time}, {label: "Last visit", value: oo.lastvisit}, {label: "First visit", value: oo.firstvisit} ];
			mydata.profileobj = obj;
			mydata.booking = oo.booking;
			mydata.systemid = systemid;
			mydata.showpersoinfo = false;
			mydata.triggermodifyprofile = $scope.updateCustomProfile.bind(null, token, $scope.restaurant, systemid, email, phone);
			mydata.triggermodifynotes = $scope.triggermodifynotes.bind(null, oo.booking, obj.content);
			$scope.showModal('PROFILE', 'PROFILE', $scope.emptyfunc, "profile.html", $scope.mydata, $scope.prepostcall, '');
			
			});
		};

	$scope.updateCustomProfile = function(token, resto, id, email, phone) {

		bookService.read1Profile($scope.restaurant, email, phone, $scope.usremail).then(function(response) {
			var data = response.data;
		
			if(response.status <= 0 || !data || !data.systemid) {
				$scope.alert("No profile for "+email);
				return;
				}
			$scope.cprofiledata = response.data;  
			return $scope.modifyprofile($scope.cprofiledata);
			});
		};

	$scope.modifyprofile = function(data) {
		var oo, ckey;
		
		mydata.profileusr = data;
		mydata.profilecnt = [];
		mydata.profilecode = [];
		mydata.profiletitle = [{ a: "Guest", b: data.salutation + " " + data.firstname + " " + data.lastname }, { a: "email", b: data.email }, { a: "email2", b: data.extraemail }, { a: "phone", b: data.mobile }, { a: "phone2", b: data.extramobile }];

		mydata.profilecode = profileService.codeformat(data);
		mydata.profilecnt = profileService.datacontent(data);
		ckey = profileService.codekey(data);
		mydata.profilecode.map(function(oo) { oo.value = (ckey.indexOf(oo.label) >= 0); });
		mydata.showpersoinfo = false;
		
		$scope.showModal('Update Profile', 'PROFILE', $scope.apimodifprofile, "modifprofile.html", mydata, null, '', 'Update', 'Cancel');
		};
	
	$scope.apimodifprofile = function() {
		var rtime, content = {};		
		
		if(mydata && mydata.profilecode && mydata.profilecnt && mydata.profileusr && mydata.profileusr.systemid) {
			mydata.profilecnt.map(function(oo) { content[oo.a] = oo.b; });
			profileService.updatesubProfile($scope.restaurant, $scope.usremail, mydata.profileusr.systemid, mydata.profilecode, content, token, bookService);
			}
		};
	
	$scope.printjs = function(func) {
		printJS('printJS-form', 'html');
		func();
		};
				
	$scope.printnewprofile = function() {
		var ind, oo, rtime, rdate;
		
		if((ind = $scope.names.fdBooking($scope.curbooking)) < 0)
			return -1;

		oo = $scope.names[ind];

		if(oo.hh < 12) rtime = oo.time + " AM";
		else if(oo.hh >12) rtime = (oo.hh - 12) + ":" + ((oo.mm < 10) ? "0":"") + oo.mm + " PM";
		else rtime = oo.time + " PM";
		rdate = oo.date.datereverse().replace(/-/g, "\/");
		
		mydata.title = oo.mealtype.capitalize();
		mydata.fullname = oo.salutation + " " + oo.fullname;
		mydata.field = [
			{label: "Date", value: rdate}, 
			{label: "Time", value: rtime}, 
			{label: "Party", value: oo.pers}, 
			{label: "Table", value: oo.tablename},
			{label: "Repeat", value: oo.repeat},
			{label: "Last visit", value: oo.lastvisit}
			];

		if(  (typeof oo.comment === "string" && oo.comment !== "") ||  (typeof oo.notestext === "string" && oo.notestext !== "") ||  (typeof oo.notescode === "string" && oo.notescode !== "")  ) {
			mydata.field.push({label: "hr", value: ''});
			mydata.field.push({label: "title", value: "Reservation Notes"});
			}
			
		if(typeof oo.comment === "string" && oo.comment !== "") 
			mydata.field.push({label: "Request", value: oo.comment});
		if(typeof oo.notestext === "string" && oo.notestext !== "") 
			mydata.field.push({label: "Notes", value: oo.notestext});
		if(typeof oo.notescode === "string" && oo.notescode !== "")
			mydata.field.push({label: "BKG Codes", value: oo.notescode});
			
		mydata.profile = [
			{label: "title", value: oo.mealtype.capitalize() }, 
			{label: "title", value: oo.salutation + " " + oo.fullname },
			{label: "Date", value: rdate}, 
			{label: "Time", value: rtime}, 
			{label: "Party", value: oo.pers}, 
			{label: "Table", value: oo.tablename},
			];
		if(typeof oo.lastvisit === "string" && oo.lastvisit !== "")
			mydata.profile.push({label: "Last visit", value: oo.lastvisit});
//		if(typeof oo.repeat === "string" && oo.repeat !== "")
			mydata.profile.push({label: "Repeat", value: oo.repeat});

		if(  (typeof oo.comment === "string" && oo.comment !== "") ||  (typeof oo.notestext === "string" && oo.notestext !== "") ||  (typeof oo.notescode === "string" && oo.notescode !== "")  ) {
			mydata.profile.push({label: "hr", value: ''});
			mydata.profile.push({label: "minititle", value: "Reservation Notes"});
			}
			
		if(typeof oo.comment === "string" && oo.comment !== "") 
			mydata.profile.push({label: "Request", value: oo.comment});
		if(typeof oo.notestext === "string" && oo.notestext !== "") 
			mydata.profile.push({label: "Notes", value: oo.notestext});
		if(typeof oo.notescode === "string" && oo.notescode !== "")
			mydata.profile.push({label: "BKG Codes", value: oo.notescode});

		//mydata.profile.push({label: "title", value: '\x1D' + 'Vm'});	// cut command to the POS printer

		mydata.showpersoinfo = false;
		mydata.printing = $scope.printjs; 
		$scope.showModal('PRINTING PROFILE', 'PRINTING PROFILE', $scope.emptyfunc, "printprofile.html", $scope.mydata, $scope.prepostcall, 'sm');
		};
			
	$scope.printprofile = function() {
		var ind, content, oo, docprint;
		
		if((ind = $scope.names.fdBooking($scope.curbooking)) < 0)
			return -1;
						
		docprint = window.open("", "Printing Reservation", "toolbar=yes,location=no,directories=yes,menubar=yes,scrollbars=yes,width=300, height=400, left=100, top=25");
		docprint.document.open();
		oo = $scope.names[ind];

		content = "";
		content += "<html><head><title>Weeloy System</title>";
		content += "<style>body { margin: 20px 20px 20px 20px } h5 { margin: 5px 0 5px 0 } table { font-family:monaco;font-size:12px;text-weight:bold; } td { align:center; } .truncate { white-space: nowrap; overflow: hidden; text-overflow: ellipsis; } </style></head>";
		content += "<body onLoad='self.print(); document.title=\"SUMMARY\"'>";
		content += "<center><h2>SUMMARY</h2></center>";
		content += "<h5>" + 'Guest' + ' : ' + oo.salutation + ' ' + oo.fullname + "</h5>";
		content += "<h5>" + 'Pax' + ' : ' + oo.pers + "</h5>";
		content += "<h5>" + 'Date' + ' : ' + oo.date + "</h5>";
		content += "<h5>" + 'Time' + ' : ' + oo.time + "</h5>";
		content += "<h5>" + 'Table' + ' : ' + oo.tablename + "</h5>";
		content += "<h5>" + 'Last visit' + ' : ' + oo.lastvisit + "</h5>";
		content += "<h5>" + 'Repeat Guest' + ' : ' + oo.repeat + "</h5>";
		content += "<hr>";
		content += "<h5>" + 'Request' + ' : ' + oo.comment + "</h5>";
		content += "<hr>";
		content += "<h5>" + 'Notes' + ' : ' + oo.notestext + "</h5>";
		content += "<h5>" + 'BKG Codes' + ' : ' + oo.notescode + "</h5>";
		content += "</body></html>";
		docprint.document.write(content);
		docprint.document.close();
		docprint.focus();
		};

	$scope.initorder = function() {
		$scope.tabletitle = $scope.bckups;
		$scope.predicate = "time";
		$scope.reverse = false;
	};

	$scope.reorder = function(item, alter) {
		alter = alter || "";
		if (alter !== "")  item = alter;
		$scope.reverse = ($scope.predicate === item) ? !$scope.reverse : false;
		$scope.predicate = item;
	};
					
	if(update_session_time() > 0)
		$scope.updatestate();
				
	clearTimeout($scope.testlogin);
	$scope.testlogin = setTimeout($scope.checklogin, 600000);
								
	$scope.loginout = function (type) { 
		switch(type) {
			case 'login':
				$scope.login('login');
				break;
			
			default:
			case 'logout':
				$scope.logout();
				break;				
			}
		};
	
	$scope.logout = function() {
		$scope.notEditingMode = true;
		$scope.shortcuts = false;
		$scope.editmode = -1;
		$scope.runonlymode = -1;
		$scope.showlayout = false;
		$scope.showbooking = false;
		$scope.tables = [];
		$scope.tablecontent = []; // { name: 'backoffice' }, { name: 'front' }, { name: 'booking' }, { name: 'callcenter' } ];
		$scope.layoutdata = [];
		$scope.names = [];
		$scope.translatedata = interfaceModel.translatestart;
		removecookie(cookiename);
		if($scope.usremail !== "") {
			return loginService.logout($scope.usremail, token).then(function(response){ 
				console.log('logout'); console.log(response); $scope.documentreload(''); 
				});
			}
		// $scope.documentreload('');
		};

	$scope.documentreload = function(arg) {	
		var url = window.location.href.replace(/\?.*$/g, '') + arg;
		//window.location.href = url;
		window.location.assign(url);
		};
		
	$scope.login = function (type) {
		var size;
		
		$scope.curTemplate = 'login'; 
		templateUrl = 'loginBackoffice.html';
		if(type === 'preferences') {
			$scope.curTemplate = 'change';
			templateUrl = 'chgpassBackoffice.html';
			}
				
		size = 'sm';
		if(modalInstance !== null)
			modalInstance.close();

		modalInstance = $uibModal.open({
			templateUrl: templateUrl,
			controller: 'loginModalController',
			size: size,
			scope: $scope
			});
	};
	
	function getDaySeconds() {
		var tmpDate = new Date(); 
		return ((tmpDate.getHours() * 3600) + (tmpDate.getMinutes() * 60) + tmpDate.getSeconds) * 1000;
		}

	if($scope.initrunflg === 0) {
		if(window.location.href.search("start21") > -1 && false)
			$scope.lyrun();
		else if(window.location.href.search("start22") > -1 && false)
			$scope.lyedit();
		}
		
function processRunKey(e, scope) {
	
	e = $.event.fix(e);	
	//console.log('CODE', e.keyCode, e.shiftKey, e.altKey, e.metaKey, e.ctrlKey, scope.bookingview);

	var flg = (opsyswindow && e.altKey && e.shiftKey) || (!opsyswindow && e.ctrlKey);
	if(flg) {
		var c = e.keyCode;
		e.preventDefault();
		e.stopPropagation();
		switch(c) {
			case 89:  // y => call center
				$('#addbooking').trigger('click');
				break;

			case 66:  // b => booking view
				$('#lybooking').trigger('click');
				break;
				
			case 73:  // i => booking view
				$('#lyicicle').trigger('click');
				break;
				
			case 70:  // f => floor 
				$('#nextfloor').trigger('click');
				break;
				
			case 75:  // k => bookingdetails - modification
				$('#bookingdetails').trigger('click');
				break;
				
			case 77:  // M => bookingdetails - modification
				var scope = angular.element(document.getElementById("MainTMS")).scope();
				scope.$apply(function () { scope.modifbooking(); });
				break;

			case 48:
				$('.btn-warning').click();
				break;
				
			case 49:  // 1 => toogle viewer				break;
				var scope = angular.element(document.getElementById("MainTMS")).scope();
				scope.$apply(function () { scope.setshowMode('all', 1); });
				break;
				
			case 50:  // 2 => lunch booking view
			case 76:  // L => lunch booking view
				var scope = angular.element(document.getElementById("MainTMS")).scope();
				scope.$apply(function () { scope.setshowMode('lunch', 1); });
				break;
				
			case 51:  // 3 => dinner booking view
			case 68:  // D => dinner booking view
				var scope = angular.element(document.getElementById("MainTMS")).scope();
				scope.$apply(function () { scope.setshowMode('dinner', 1); });
				break;
				
			case 52:  // 3 => dinner booking view
			case 72:  // H => hours booking view
				var scope = angular.element(document.getElementById("MainTMS")).scope();
				scope.$apply(function () { scope.setshowMode('hours', 1); });
				break;
				
			case 53:  // 3 => dinner booking view
				var scope = angular.element(document.getElementById("MainTMS")).scope();
				scope.$apply(function () { scope.gotocurrentime(); });
				break;

			case 65:  // A => all booking view
				var scope = angular.element(document.getElementById("MainTMS")).scope();
				scope.$apply(function () { scope.setshowMode('all', 1); });
				break;
				
			case 71:  // G => guest 
				$('#viewprofile').trigger('click');
				break;
				
			case 80:  // p => print 
				$('#printnewprofile').trigger('click');
				break;
				
			case 81:  // q => queue view 
				$('#lywaitingview').trigger('click');
				break;
				
			case 82:  // r => refresh mgt 
				$('#refreshbooking').trigger('click');
				break;
				
			case 83:  // s => statistics mgt 
				$('#statistics').trigger('click');
				break;
				
			case 84:  // t => time line mgt 
				$('#lytimeline').trigger('click');
				break;
				
			case 87:  // w => walking
				$('#walkingbooking').trigger('click');
				break;

			case 88:  // X => lastbooking 
				$('#lylastbookingview').trigger('click');
				break;
				
			case 90:  // close window
				$('#closewindow').trigger('click');
				break;

			}
		}
	}

}]);
