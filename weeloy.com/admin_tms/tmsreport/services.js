function Pagination(cn) {
	var rowperpage = cn;
	var itemcount;
	var page = 0;
	var pgrange = [ 25, 50, 100, 200 ];
	var pgoffset = 0;
	
	return {

		range: function() {
			var ret = [];
			var limit = this.pageCount();
			for (i = 0; i < limit; i++) {
				ret.push(i);
				}
			return ret;
			},

		pagerange: function() {
			return pgrange;
			},
			
		setPageOffset: function(pgo) {
			pgoffset = pgo;
			},

		getPageOffset: function() {
			return pgoffset;
			},

		setItemCount: function(cn) {
			itemcount = cn;
			},
			
		getItemCount: function() {
			return itemcount;
			},
			
		setRowperPage: function(cn) {
			this.setPage(0);
			return rowperpage = cn;
			},
			
		getRowperPage: function() {
			return rowperpage;
			},
			
		getPage: function() {
			return page;
			},
					
		setPage: function (apage) { 
			if (apage > this.pageCount()) { return; }
			page = apage;
			},	
			
		nextPage: function () { 
			if (page < this.pageCount() - 1) 
				page++; 
			},
		
		prevPage: function () { 
			if (page > 0)
				page--; 
			},
			
		firstPage: function () { 
			page = 0;
			},
			
		lastPage: function () { 
			page = this.pageCount() - 1;
			},
			
		isFirstPage: function () { 
			return page == 0 ? "disabled" : "";
			},
			
		isLastPage: function () {
			return page == this.pageCount() - 1 ? "disabled" : "";
			},
			
		pageCount: function () {
			return Math.ceil(parseInt(itemcount) / parseInt(rowperpage));
			},

		showIndex: function (n) {
			if(n <= 0 || n > itemcount) return this.setPage(0);
			return this.setPage(Math.floor(n / rowperpage));
			},
			
		prevPageDisabled: function() {
			return page === 0 ? "disabled" : "";
			},
		
		nextPageDisabled: function() {
			return page === this.pageCount() - 1 ? "disabled" : "";
			}
		};
	}

app.filter('sizefilter', function() {
	return function(input, scope) {
	if (input == undefined) return;
	scope.paginator.setItemCount(input.length);
	return input;
	}
});

	
app.service('Paginator', function () { 
	this.page = 0;
	this.rowsPerPage = 100; 
	this.itemCount = 0;

	this.setPage = function (page) { 
		if (page > this.pageCount()) { return; }
		this.page = page;
		};		
	this.nextPage = function () { 
		if (this.isLastPage()) { return; }
		this.page++; 
		};
	this.perviousPage = function () { 
		if (this.isFirstPage()) { return; }
		this.page--; 
		};
	this.firstPage = function () { 
		this.page = 0;
		};
	this.lastPage = function () { 
		this.page = this.pageCount() - 1;
		};
	this.isFirstPage = function () { 
		return this.page == 0;
		};
	this.isLastPage = function () {
		return this.page == this.pageCount() - 1;
		};
	this.pageCount = function () {
		return Math.ceil(parseInt(this.itemCount) / parseInt(this.rowsPerPage));
		}; 
	});

app.filter('slicepaginator', function(Paginator) {
	return function(input, rowsPerPage) { 
		if (!input) { return input; }
	if (rowsPerPage) { Paginator.rowsPerPage = rowsPerPage; }
	Paginator.itemCount = input.length;
	return input.slice(parseInt(Paginator.page * Paginator.rowsPerPage), parseInt((Paginator.page + 1) * Paginator.rowsPerPage + 1) - 1); }
});

app.directive('paginator', function factory() { 
	return {
		restrict: 'E',
		controller: function ($scope, Paginator) {
		$scope.paginator = Paginator; },
		templateUrl: 'paginator.html' 			// 'paginationControl.html' 
		};
});

app.filter('slicefilter', function() {
	return function(arr, currentPage, itemsPerPage) {
	if (arr == undefined) return;
	return arr.slice(currentPage * itemsPerPage, (currentPage + 1) * itemsPerPage);
	}
});


// offset. it starts at the current week
app.filter('slicefilterOffset', function() {
	return function(arr, currentPage, itemsPerPage) {
	if (arr == undefined) return;
	actualpage = currentPage - paginationOffset;
	return arr.slice(actualpage * itemsPerPage, (actualpage + 1) * itemsPerPage);
	}
});


app.service('extractService', function() {
	
	var self = this;
	var ifrm, ffrm, extrctdata, extrctfile;

	function writeiframe() {
		if(ifrm.contentWindow.foo === undefined)
			return setTimeout(writeiframe, 300);
		document.getElementById("iframeid").contentWindow.chgvalue(extrctfile, extrctdata);
		setTimeout(function() { alert("Extraction started, check download folder");}, 2000);
		}

	function downloadiframe(filename, data) {
	
		extrctfile = filename;
		extrctdata = data;
		
		if(ifrm)
			return writeiframe();
			
		ifrm = document.createElement("iframe");
        ifrm.setAttribute("src", "iframe.html");
        ifrm.setAttribute("id", "iframeid");
        ifrm.style.width = "1px";
        ifrm.style.height = "1px";
        document.body.appendChild(ifrm);
        if(ifrm.contentWindow.foo === undefined)
        	return setTimeout(writeiframe, 300);
    }

	function downloaddata(filename, content) {
        var link;
        filename = filename || 'export.csv';

        if (!content.match(/^data:text\/csv/i) || true) {
            content = 'data:text/csv;charset=utf-8,' + content;
        	}
        data = encodeURI(content);

        link = document.createElement('a');
        link.setAttribute('href', data);
        link.setAttribute('download', filename);
		document.body.appendChild(link);
		link.click();
		document.body.removeChild(link);
		}

	this.fullextract = function(req) {
		if(ffrm)
			ffrm.remove();
			
		ffrm = document.createElement("iframe");
        ffrm.setAttribute("src", "fullextract.php?" + req);
        ffrm.style.width = "1px";
        ffrm.style.height = "1px";
        document.body.appendChild(ffrm);
		setTimeout(function() { alert("Extraction started, check download folder");}, 2000);
		};
				
	this.browserVersion = function() {
		var ua = navigator.userAgent;
		var offset, name = "unknown", version = "";
		var i, p, nameAr = [ "MSIE", "Chrome", "Safari", "Firefox"], offAr = [ 5, 7, 7, 8];
	
		for(i = 0; i < nameAr.length; i++) 
			if ((offset=ua.indexOf(nameAr[i]))!=-1) {
				name = nameAr[i];
				version = ua.substring(offset+offAr[i]);
				if (nameAr[i] === "Safari" && (offset=ua.indexOf("Version"))!=-1) 
					version = ua.substring(offset+8);
				if(version !== "") version = parseInt(version.replace(/[^\d.]/g, ''));
				break;	 
				}
		return { name: name, version: version };
		};
	
	this.filter = function(s) {	
		if(!s) return "";	
		return (typeof s !== "string" || s.length < 1) ? s : s.replace(/[,\n\r\'\"]/g, " ");
		};

	this.save = function(filename, data) {
		var brswr = this.browserVersion();

		if( (brswr.name === "Chrome" && brswr.version > 45) ||
		    (brswr.name === "Firefox" && brswr.version > 40)
			)	
			return downloaddata(filename, data);
		
		else downloadiframe(filename, data);
		};
		
	this.saveObj = function(filename, obj) {
		if(!obj.booking || obj.booking.length < 1)
			return;
			
		var tmpAr = obj.booking, labelAr = Object.keys(tmpAr[0]), data = "";
		
		labelAr.map(function(label) { data += label + ","; });
		tmpAr.map(function(oo) { 
			labelAr.map(function(ll) { data +=  self.filter(oo[ll]) + ","; });
			});
			
		self.save(filename, data);
		};
	
});


app.service('ModalService', ['$uibModal', function ($uibModal) {

        var modalDefaults = {
			size: 'sm',
            backdrop: true,
            keyboard: true,
            modalFade: true,
			animation: true,
            templateUrl: 'modalgen.html'
        };

        var modalOptions = {
            closeButtonText: 'Close',
            actionButtonText: 'OK',
            headerText: 'Proceed?',
            bodyText: 'Perform this action?'
        };

		this.setTemplate = function(template) {
			modalDefaults.templateUrl = template;
			}
			
		this.setSize = function(size) {
			modalDefaults.size = size;
			}
			
        this.showModal = function (customModalDefaults, customModalOptions) {
            if (!customModalDefaults) customModalDefaults = {};
            customModalDefaults.backdrop = 'static';
            return this.show(customModalDefaults, customModalOptions);
        };

        this.show = function (customModalDefaults, customModalOptions) {
            //Create temp objects to work with since we're in a singleton service
            var tempModalDefaults = {};
            var tempModalOptions = {};

            //Map angular-ui modal custom defaults to modal defaults defined in service
            angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

            //Map modal.html $scope custom properties to defaults defined in service
            angular.extend(tempModalOptions, modalOptions, customModalOptions);

            if (!tempModalDefaults.controller) {
                tempModalDefaults.controller = function ($scope, $modalInstance) {
                    $scope.modalOptions = tempModalOptions;
                    $scope.modalOptions.ok = function (result) {
                        $modalInstance.close(result);
                    };
                    $scope.modalOptions.close = function (result) {
                        $modalInstance.dismiss('cancel');
                    };
                }
            }

            return $uibModal.open(tempModalDefaults);
        };
        
        this.activate = function (title, initval, func, template, mydata, prepostcall, size, labelok, labelclose) {
			if(template === null)
				template = "modalgen1.html";
		
			if(prepostcall !== null)
				prepostcall(0);
		
			if(typeof labelok !== "string" || labelok.length < 2)
				labelok = 'OK';
			if(typeof labelclose !== "string" || labelclose.length < 2)
				labelclose = 'Close';
			
			var modalOptions = {
				againButtonText: 'More',
				closeButtonText: labelclose,
				actionButtonText: labelok,
				headerText: title,
				mydata: mydata,
				size: size,
				submit:function(result){ $modalInstance.close(result); if(prepostcall !== null) prepostcall(1); },
				myclose:function(){ $modalInstance.dismiss('cancel'); if(prepostcall !== null) prepostcall(1); },
				myagain:function() { $modalInstance.close('more'); if(prepostcall !== null) prepostcall(1); }    
			};
		
			this.setTemplate(template);
			this.setSize(size);

			var $modalInstance = this.showModal({}, modalOptions);
			$modalInstance.result.then(function (result) {
				func(result);
			});
		};

	this.ModalDataBooking = function() {

		var i, j, timeslotAr=[], persAr=[], hourslotAr=[], minuteslotAr=[];

		for(i = 1; i < 50; i++) 
			persAr.push(i);
		
		for(i = 9; i < 24; i++)
			for(j = 0; j < 60; j += 15) {
				timeslotAr.push((i < 10 ? '0' : '') + i + ':' + (j === 0 ? '0' : '') + j);
				}
			
		for(i = 9; i < 24; i++) 
			hourslotAr.push((i < 10 ? '0' : '') + i);
		
		for(i = 0; i < 60; i += 5) 
			minuteslotAr.push((i < 10 ? '0' : '') + i);
			
		return  {
				name: "", 
				pers: persAr, 
				timeslot: timeslotAr, 
				hourslot: hourslotAr,
				minuteslot: minuteslotAr,
				ntimeslot: 0,
				event: ['Birthday', 'Wedding', 'Reunion'],
				start_opened: false,
				opened: true,
				selecteddate: null,
				originaldate: null,
				dateFormated: "",
				theDate: null,
				minDate: null,
				maxDate: null,
                deposit:null,
				formats: ['dd-MM-yyyy', 'dd/MMMM/yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'dd-MM-yyyy', 'yyyy-MM-dd', 'dd.MM.yyyy', 'shortDate', 'yyyy-MM-dd', ],
				dateOptions: { formatYear: 'yyyy', startingDay: 1 },
				formatsel: "",
				func: null,
			
				setInit: function(ddate, rtime, func, format, minDate, maxDate) {
					
					this.theDate = null;
					this.func = func;
					this.formatsel = this.formats[format];			
					this.ntimeslot = rtime.substring(0, 5);
				
					if(ddate instanceof Date === false) {
						ddate = ddate.jsdate();
						}
					this.originaldate = ddate;
					this.selecteddate = ddate.getDateFormat('-');		
					this.minDate = minDate;
					this.maxDate = maxDate;
					
					if(navigator.userAgent.indexOf("Chrome") != -1 ) { 
						this.formatsel = this.formats[8]; 
						this.selecteddate = ddate.getDateFormatReverse('-'); 
						}	
					},
			
				setDate: function(ddate) {
					this.theDate = null;
					this.originaldate = ddate; // this is for the case that no data selected
					this.selecteddate = ddate.getDateFormat('-');
					if(navigator.userAgent.indexOf("Chrome") != -1) 
						this.selecteddate = ddate.getDateFormatReverse('-'); 
					},
					
				getDate: function(sep, mode) {
					if(typeof this.theDate !== null && typeof this.theDate !== undefined && this.theDate instanceof Date)
						return (mode !== 'reverse') ? this.theDate.getDateFormat(sep) : this.theDate.getDateFormatReverse(sep);
					return (mode !== 'reverse') ? this.originaldate.getDateFormat(sep) : this.originaldate.getDateFormatReverse(sep);
					},
					
				dateopen: function($event) {
					this.start_opened = true;
					this.opened = true;
					$event.preventDefault();
					$event.stopPropagation();
					},
				
				disabled: function(date, mode) {
					return false;
					},
			
				calendar: function() {
					this.theDate = this.selecteddate;
					if(this.theDate instanceof Date)
						this.dateFormated = this.theDate.getDateFormat('/');
					},
			
				onchange: function() {
					this.theDate = this.selecteddate;
					if(this.theDate instanceof Date)
						this.dateFormated = this.theDate.getDateFormat('/');

					if(this.func !== null)
						this.func();
					}	
				};
		};

    }]);

