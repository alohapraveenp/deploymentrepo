if(tmsUI !== 1) {

var threewaywindow = document.getElementById('threewaywindow');
var left = document.getElementById('left');
var leftup = document.getElementById('leftup');
var right = document.getElementById('right');
//var dividerH = document.getElementById('dividerH');
var booking = document.getElementById('booking');
var divider = document.createElement('div');
divider.id = 'divider';
divider.style.zIndex = 0;
divider.style.height = '100%';
threewaywindow.appendChild(divider);

function updateVDivision() {
	if(leftPercent > 90) leftPercent = 90;
	if(leftPercent < 10) leftPercent = 10;
	
    divider.style.left = leftPercent + '%';
    left.style.width = leftPercent + '%';
    right.style.width = (100 - leftPercent) + '%';
}

function updateHDivision() {
	if(leftUpPercent > 30) leftUpPercent = 30;
	if(leftUpPercent < 5) leftUpPercent = 5;

   	//dividerH.style.bottom = leftUpPercent + '%';
   	leftup.style.width = leftUpPercent + '%';
   	booking.style.width = (100 - leftUpPercent) + '%';
}

divider.addEventListener('mousedown', function(e) {
    e.preventDefault();
    var lastX = e.pageX;
    document.documentElement.className += ' dragging';
    document.documentElement.addEventListener('mousemove', moveHandler, true);
    document.documentElement.addEventListener('mouseup', upHandler, true);
    function moveHandler(e) {
        e.preventDefault();
        e.stopPropagation();
        var deltaX = e.pageX - lastX;
        lastX = e.pageX;
        leftPercent += deltaX / parseFloat(document.defaultView.getComputedStyle(threewaywindow).width) * 100;
		if(leftPercent < 6) leftPercent = 6;
		else if(leftPercent > 95) leftPercent = 95;
        updateVDivision();
    }
    function upHandler(e) {
        e.preventDefault();
        e.stopPropagation();
        document.documentElement.className = document.documentElement.className.replace(/dragging/, '');
        document.documentElement.removeEventListener('mousemove', moveHandler, true);
        document.documentElement.removeEventListener('mouseup', upHandler, true);
    }
}, false);

/*
if(window.innerWidth < 1100) {
	$(".shortcut .btn-default").addClass("btn-xs");
	$("span.short-label").css("font-size", "8px");
	}
*/

function reinitwindow() {

	return;
	
	var value = parseInt($('body').css("height")) - parseInt($('#navbartms').css("height"));
	$('#right').css("height", value+'px');
}


function openLeftup(extraHeight) {
	return;
	
	var value, Height = getLeftcss() + extraHeight;

	if(windowinitsize == -1) {
		value = parseInt($('#right').css("height")) - parseInt($('#navbartms').css("height"));
		$('#right').css("height", value+'px');
		windowinitsize = 1;
		}
		
	if(Height > statsmaxheight) {
		Height = statsmaxheight;
		}
		
	setLeftcss(Height);
	}

function moveLeftup(Height) {
	}

function moveLeftUpDown(Height) {
	}
	
function moveDownright(Right) {
	if(tmsInitDivRight === 0) 
		tmsInitDivRight = 2 * parseInt($('#booking').css("width"));

	var extraRight = Right - tmsDivRight;
	leftPercent = ((parseInt($('#booking').css("width")) + extraRight) / tmsInitDivRight) * 100;
	if(leftPercent < 6) leftPercent = 6;
	else if(leftPercent > 90) leftPercent = 90;
	updateVDivision();		

	tmsDivRight = Right;
	}

function managewindow(mode) {
	if(mode === "right") { leftPercent = (leftPercent < 45) ? 50 : 90; updateVDivision(); tmsInitDivRight = 0; }
	else if(mode === "left")  { leftPercent = (leftPercent < 50) ? 15 : 45; updateVDivision(); tmsInitDivRight = 0; }
	else if(mode === "up")  moveLeftUpDown(20);
	else if(mode === "down")  moveLeftUpDown(300);
	}
		
//$('#leftup').html("<p style='font-size:7px;'>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>");

function setLeftcss(val) {
	return -1;
	$('#leftup').css("height", val+'px');
}
function getLeftcss(val) {
	return -1;
	return parseInt($('#leftup').css("height"));
}
	
function windinit3way() {
	leftPercent = 50;
	leftUpPercent = 10;
	windowinitsize = -1;
	statsmaxheight = 400;
	tmsDivTop = 0;
	tmsDivRight = 0;

	offsettop = $('#threewaywindow').offset().top;
	setLeftcss(offsettop - 30);
	tmsInitDivRight = parseFloat(document.defaultView.getComputedStyle(threewaywindow).width);

	updateVDivision();
}

	
windinit3way();

if(tablette)
	$(window).bind('orientationchange', function(event) { 
		console.log('new orientation:' + event.orientation); 
		windinit3way();
		});
		
$(function() {
  $( "#draggableHandle" ).draggable({
	drag: function( event, ui ) { moveLeftup(ui.position.top); moveDownright(ui.position.left); ui.position.top = ui.position.left = 0; },
	stop: function( event, ui ) { tmsDivTop = tmsDivRight = 0; }
  });
});

} 
