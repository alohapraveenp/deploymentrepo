<?php

// don't move those line as cookie header need to send prior to anything
require_once("conf/conf.init.inc.php");
require_once("lib/wpdo.inc.php");
require_once("lib/gblcookie.inc.php");
require_once("lib/class.login.inc.php");
require_once("lib/class.coding.inc.php");
require_once("conf/conf.session.inc.php");
require_once("lib/class.media.inc.php");
	
require_once("lib/class.images.inc.php");	
require_once("../inc/utilities.inc.php");
require_once("lib/class.analytics.inc.php");
require_once("lib/class.restaurant.inc.php");
require_once("lib/class.member.inc.php");
require_once("lib/class.cluster.inc.php");

$theRestaurant = "SG_SG_R_TheFunKitchen";

$isTheFunKitchen = (preg_match("/TheFunKitchen/", $theRestaurant)); 

$res = new WY_restaurant;
$res->getRestaurant($theRestaurant);
if($res->result <= 0) {
	$debug = new WY_debug;
	$debug->writeDebug("ERROR-BACKOFFICE", "REPORT", "Invalid Restaurant :" . $theRestaurant);
	exit;
	}
$ccextraction = $res->CCExtractionFormat();
$mediadata = new WY_Media($theRestaurant);
$logo = $mediadata->getLogo($theRestaurant);

?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
<title>Weeloy - Table Management System</title>
<link rel="icon" href="/favicon.ico" type="image/gif" sizes="16x16">
    <link href="../css/admin-style.css?1" rel="stylesheet" type="text/css"/>
    <link href="../css/dropdown.css" rel="stylesheet" type="text/css">
    <link href="../css/login.css" rel="stylesheet" type="text/css">
    <link href="../css/modal.css" rel="stylesheet" type="text/css">
    <link href="../css/animate.css" rel="stylesheet" type="text/css">

    <link href="../css/bootstrap33.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/jquery-ui.css" rel="stylesheet" type="text/css">
    <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/bootstrap-social.css" rel="stylesheet" type="text/css"/>
    <link href="../css/login-modal.css" rel="stylesheet" type="text/css"/>
    <link href="tms.css" rel="stylesheet" type="text/css"/>

    <link href='https://fonts.googleapis.com/css?family=Dosis:400,600,700bold|Roboto:400,900,700italic,700,500italic,500,400italic' rel='stylesheet' type='text/css'>

    <script type="text/javascript" src="../js/jquery.min.js"></script>
    <script type="text/javascript" src="../js/angular.min.js"></script>
    <script type="text/javascript" src="../js/jquery-ui.js"></script>
    <script type="text/javascript" src="../js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../js/ui-bootstrap-tpls-0.12.1.min.js"></script>
  	<script type="text/javascript" src='../js/raphael.js'></script>

<style> 
</style>

<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<script src="js/selectivizr-min.js"></script>
<script src="js/modernizr.js"></script>
<link rel="stylesheet" type="text/css" href="css/ie.css" media="screen" />
<![endif]-->
</head>


<body ng-app="backoffice" ng-controller="boHomeController">

<script> var app = angular.module('backoffice',['ui.bootstrap', 'FacebookProvider', 'ngStorage']); </script>

<nav class="navbar navbar-default navbar-static-top bottom-border">
   <div class="logo"><img width="90px" src="../images/logo_w.png" alt="weeloy-best-restaurant-logo" /></div>
  <div class="container">
	<div id="navbar" class="navbar-collapse collapse" style='padding:10px 0 0 0;'>
	  <ul class="nav navbar-nav">
		<li>&nbsp; &nbsp; &nbsp; &nbsp;</li>
		<li class="dropdown">
    	  <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" style='font-size:14px;'>CONFIGURATION <span class="caret"></span></button>
		  <ul class="dropdown-menu">
			<li ng-if="logaction == 'logout'"><a href ng-click="lynew();">New Layout</a></li>
			<li ng-if="logaction == 'logout' && layoutdata.length > 0" class="dropdown-submenu"><a href ng-click="lyread();" tabindex="-1" >Read</a>
			    <ul class="dropdown-menu">
                  <li ng-repeat="y in layoutdata track by $index"><a tabindex="-1" ng-click="lyselect($index);" >{{y.name}}</a></li>
                </ul>
			</li>
			<li ng-if="showlayout"><a href ng-click="lysave();">Save</a></li>
			<li role="separator" class="divider"></li>
			<li class="dropdown-header">Execution</li>
		  </ul>
		</li>
		<li ng-repeat="x in tablecontent">&nbsp; &nbsp; &nbsp; &nbsp;<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" ng-click="process(x.name);" style='font-size:14px;'>{{x.name | uppercase}}</li>
	  </ul>
	  <ul class="nav navbar-nav navbar-right">
		  <li ng-if="logaction == 'login'"><a href ng-click="loginout(logaction)"><span class="glyphicon glyphicon-log-in"></span> {{logaction}} </a></li>
		  <li ng-if="logaction != 'login'"><a href ng-click="loginout(logaction)"><span class="glyphicon glyphicon-log-out"></span> {{logaction}} </a></li>
		  <li>&nbsp; &nbsp; &nbsp; &nbsp;</li>
		  <li id="sessiontime" class="small"></li>
	  </ul>
	</div><!--/.nav-collapse -->
  </div>
</nav>

   
<div id="content">
    <div id="left">
 	<div id="leftup"></div>
 	<div id="dividerH"></div>
 	<div id="booking">
   	<div ng-include="'bookingToday.html'"></div>
   	</div>
    </div>

    <div id="right">
		<div id='raphael'></div>
			<content-item ng-repeat="item in translatedata" content="item" myTemplates="templateData"></content-item>
	</div>
    
    </div>
  	<div ng-include="'bottomTemplate.html'" ng-if="notEditingMode"></div>
</div>

<div id="fb-root"></div>

<script type="text/javascript" src="../js/alog.js"></script>
<script type="text/javascript" src="../js/facebookRun.js"></script>
<script type="text/javascript" src="../js/facebookProvider.js"></script>
<script type="text/javascript" src="../js/loginService.js"></script>
<script type="text/javascript" src="../js/ngStorage.min.js"></script>
<script type="text/javascript" src="../js/formControl.js"></script>
<script type="text/javascript" src="tmsService.js"></script>
<script type="text/javascript" src='tmsRaphaelLib.js'></script>
<script type="text/javascript" src='tmsRaphaelDrag.js'></script>
<script type="text/javascript" src="tmsRaphael.js"></script>
<script type="text/javascript" src="boHomeController.js"></script>
<script type="text/javascript" src='libService.js'></script>
<script type="text/javascript" src="../backoffice/inc/backofficelib.js"></script>

<script type="text/ng-template" id="loginBackoffice.html">
	<content-item ng-repeat="item in interfaceLogin" content="item" myTemplates="templateData"></content-item>
</script>

<script type="text/ng-template" id="forgotBackoffice.html">
	<content-item ng-repeat="item in interfaceForgot" content="item" myTemplates="templateData"></content-item>
</script>

<script type="text/ng-template" id="chgpassBackoffice.html">
	<content-item ng-repeat="item in interfaceChange" content="item" myTemplates="templateData"></content-item>
</script>

<script type="text/ng-template" id="registerBackoffice.html"> 
	<content-item ng-repeat="item in interfaceRegister" content="item" myTemplates="templateData"></content-item>
</script>

<script type="text/ng-template" id="modalgen.html">
<div class="modal-header"><h3 class="modal-title" style='font-family:Dosis;'>{{modalOptions.headerText}}</h3></div>
<form ng-submit="modalOptions.submit(modalOptions.mydata)">
<div class="modal-body">
<div class="input-group"  style='padding:20px 10px 10px 10px;'>
<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
<input type="text" ng-model="modalOptions.mydata.name" class="form-control input" placeholder="Layout Name">                                        
</div>		
</div>
<div class="modal-footer">
    <button type="submit" class="btn btn-primary">{{modalOptions.actionButtonText}}</button>
    <button type="button" class="btn btn-warning" ng-click="modalOptions.myclose()">{{modalOptions.closeButtonText}}</button>
</div>
</script>

<script type="text/ng-template" id="modalgen1.html">
<div class="modal-header"><h3 class="modal-title" style='font-family:Dosis;'>{{modalOptions.headerText}}</h3></div>
<form ng-submit="modalOptions.submit(modalOptions.mydata)">
<div class="modal-body">
<div class="input-group"  style='padding:20px 10px 10px 10px;'>
<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
<input type="text" ng-model="modalOptions.mydata.name" class="form-control input sm" placeholder="Layout Name">                                        
</div>		
<div class="input-group"  style='padding:20px 10px 10px 10px;'>
<div class='input-group-btn xls' dropdown >
<button type='button' class='btn btn-default dropdown-toggle sm' data-toggle='dropdown'>
&nbsp;<i class="glyphicon glyphicon-cutlery"></i>&nbsp;<span class='caret'></span>
</button>
<ul class='dropdown-menu scrollable-menu'>
<li ng-repeat="p in [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]"><a href="javascript:;" ng-click="modalOptions.mydata.npers=p;">{{ p }}</a></li>
</ul>
</div>
<input type='text' ng-model='modalOptions.mydata.npers' class='form-control input sm' placeholder="number of perons" readonly >
</div>
</div>
<div class="modal-footer">
    <button type="submit" class="btn btn-primary">{{modalOptions.actionButtonText}}</button>
    <button type="button" class="btn btn-warning" ng-click="modalOptions.myclose()">{{modalOptions.closeButtonText}}</button>
</div>
</script>

<script>

<?php 
echo "var cookiename = '" . getCookiename('translation') . "';\n"; /* connected to the alog.js script file */ 

function GetElement($dir) {

	$fNames = array();
	$handle = opendir($dir);
	while($file = readdir($handle))
		if(is_dir($dir . $file) == false) {
			$parts = pathinfo($dir . $file);
			if($parts['extension'] == "svg")
				$fNames[] = $file;
			}
	closedir($handle);
	return $fNames;
}

$elementAr = GetElement("images/");
$sep = "";
echo "var elementsAr = [";
foreach($elementAr as $name) {
	echo $sep . " '" . $name . "'";
	$sep = ",";
	}
echo "];";

?>

var imagesRes = [];
loginTitleTormat = "small";

$(document).ready(function() { max = elementsAr.length; for(i = 0; i < max; i++) { imagesRes[i] = new Image(); imagesRes[i].src = 'images/' + elementsAr[i]; } }); 

var content = document.getElementById('content');
var left = document.getElementById('left');
var leftup = document.getElementById('leftup');
var right = document.getElementById('right');
var dividerH = document.getElementById('dividerH');
var booking = document.getElementById('booking');
var divider = document.createElement('div');
divider.id = 'divider';
content.appendChild(divider);

var leftPercent = 50;
var leftUpPercent = 10;

function updateVDivision() {
    divider.style.left = leftPercent + '%';
    left.style.width = leftPercent + '%';
    right.style.width = (100 - leftPercent) + '%';
}

updateVDivision();

divider.addEventListener('mousedown', function(e) {
    e.preventDefault();
    var lastX = e.pageX;
    document.documentElement.className += ' dragging';
    document.documentElement.addEventListener('mousemove', moveHandler, true);
    document.documentElement.addEventListener('mouseup', upHandler, true);
    function moveHandler(e) {
        e.preventDefault();
        e.stopPropagation();
        var deltaX = e.pageX - lastX;
        lastX = e.pageX;
        leftPercent += deltaX / parseFloat(document.defaultView.getComputedStyle(content).width) * 100;
        updateVDivision();
    }
    function upHandler(e) {
        e.preventDefault();
        e.stopPropagation();
        document.documentElement.className = document.documentElement.className.replace(/dragging/, '');
        document.documentElement.removeEventListener('mousemove', moveHandler, true);
        document.documentElement.removeEventListener('mouseup', upHandler, true);
    }
}, false);

dividerH.addEventListener('mousedown', function(e) {
    e.preventDefault();
    document.documentElement.className += ' draggingd';
    document.documentElement.addEventListener('mousemove', moveHHandler, true);
    document.documentElement.addEventListener('mouseup', upHHandler, true);
    function moveHHandler(e) {
        e.preventDefault();
        e.stopPropagation();
          $('#leftup').css("height",e.pageY-45);
          $('#dividerH').css("top",e.pageY-45);
        }
    function upHHandler(e) {
        e.preventDefault();
        e.stopPropagation();
        document.documentElement.className = document.documentElement.className.replace(/draggingd/, '');
        document.documentElement.removeEventListener('mousemove', moveHHandler, true);
        document.documentElement.removeEventListener('mouseup', upHHandler, true);
    }
}, false);

$('#leftup').html("<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>");

</script>

</body>
</html>
