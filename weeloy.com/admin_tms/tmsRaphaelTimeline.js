/*
 *  ┌─────────────────────────────────────────────┐ 
 *  │ TMS 1.0 - JavaScript Vector Library             │ 
 *  ├─────────────────────────────────────────────┤ 
 *  │ Copyright © 2015 Weeloy. All rights reserved.   │ 
 *  ├─────────────────────────────────────────────┤ 
 *  │ Company: https://www.weeloy.com				  │ 
 *  ├─────────────────────────────────────────────┤ 
 *  │ Author Richard Kefs							  │ 
 *  └─────────────────────────────────────────────┘ 
 *
*/

/*
https://useiconic.com/open/

var limitImage = iconsRes.length, tt = [];
for(i = 0; i < limitImage; i++) if(typeof iconsRes[i] !== "undefined" && typeof iconsRes[i].naturalHeight !== "undefined") { width = iconsRes[i].naturalWidth; height = iconsRes[i].naturalHeight; tt.push(iconsAr[i]); tt.push(width); tt.push(height); if(width === 0 || height === 0) width = height = 8; } console.log(tt);

["account-login.svg", 8, 8, "account-logout.svg", 8, 8, "action-redo.svg", 8, 8, "action-undo.svg", 8, 8, "align-center.svg", 8, 8, "align-left.svg", 8, 8, "align-right.svg", 8, 8, "aperture.svg", 8, 8, "arrow-bottom.svg", 8, 8, "arrow-circle-bottom.svg", 8, 8, "arrow-circle-left.svg", 8, 8, "arrow-circle-right.svg", 8, 8, "arrow-circle-top.svg", 8, 8, "arrow-left.svg", 8, 8, "arrow-right.svg", 8, 8, "arrow-thick-bottom.svg", 8, 8, "arrow-thick-left.svg", 8, 8, "arrow-thick-right.svg", 8, 8, "arrow-thick-top.svg", 8, 8, "arrow-top.svg", 8, 8, "audio-spectrum.svg", 8, 8, "audio.svg", 8, 8, "badge.svg", 8, 8, "ban.svg", 8, 8, "bar-chart.svg", 8, 8, "basket.svg", 8, 8, "battery-empty.svg", 8, 8, "battery-full.svg", 8, 8, "beaker.svg", 8, 8, "bell.svg", 8, 8, "bluetooth.svg", 8, 8, "bold.svg", 8, 8, "bolt.svg", 8, 8, "book.svg", 8, 8, "bookmark.svg", 8, 8, "box.svg", 8, 8, "briefcase.svg", 8, 8, "british-pound.svg", 8, 8, "browser.svg", 8, 8, "brush.svg", 8, 8, "bug.svg", 8, 8, "bullhorn.svg", 8, 8, "calculator.svg", 8, 8, "calendar.svg", 8, 8, "camera-slr.svg", 8, 8, "caret-bottom.svg", 8, 8, "caret-left.svg", 8, 8, "caret-right.svg", 8, 8, "caret-top.svg", 8, 8, "cart.svg", 8, 8, "chat.svg", 8, 8, "check.svg", 8, 8, "chevron-bottom.svg", 8, 8, "chevron-left.svg", 8, 8, "chevron-right.svg", 8, 8, "chevron-top.svg", 8, 8, "circle-check.svg", 8, 8, "circle-x.svg", 8, 8, "clipboard.svg", 8, 8, "clock.svg", 8, 8, "cloud-download.svg", 8, 8, "cloud-upload.svg", 8, 8, "cloud.svg", 8, 8, "cloudy.svg", 8, 8, "code.svg", 8, 8, "cog.svg", 8, 8, "collapse-down.svg", 8, 8, "collapse-left.svg", 8, 8, "collapse-right.svg", 8, 8, "collapse-up.svg", 8, 8, "command.svg", 8, 8, "comment-square.svg", 8, 8, "compass.svg", 8, 8, "contrast.svg", 8, 8, "copywriting.svg", 8, 8, "credit-card.svg", 8, 8, "crop.svg", 8, 8, "dashboard.svg", 8, 8, "data-transfer-download.svg", 8, 8, "data-transfer-upload.svg", 8, 8, "delete.svg", 8, 8, "dial.svg", 8, 8, "document.svg", 8, 8, "dollar.svg", 8, 8, "double-quote-sans-left.svg", 8, 8, "double-quote-sans-right.svg", 8, 8, "double-quote-serif-left.svg", 8, 8, "double-quote-serif-right.svg", 8, 8, "droplet.svg", 8, 8, "eject.svg", 8, 8, "elevator.svg", 8, 8, "ellipses.svg", 8, 8, "envelope-closed.svg", 8, 8, "envelope-open.svg", 8, 8, "euro.svg", 8, 8, "excerpt.svg", 8, 8, "expand-down.svg", 8, 8, "expand-left.svg", 8, 8, "expand-right.svg", 8, 8, "expand-up.svg", 8, 8, "external-link.svg", 8, 8, "eye.svg", 8, 8, "eyedropper.svg", 8, 8, "file.svg", 8, 8, "fire.svg", 8, 8, "flag.svg", 8, 8, "flash.svg", 8, 8, "folder.svg", 8, 8, "fork.svg", 8, 8, "fullscreen-enter.svg", 8, 8, "fullscreen-exit.svg", 8, 8, "globe.svg", 8, 8, "graph.svg", 8, 8, "grid-four-up.svg", 8, 8, "grid-three-up.svg", 8, 8, "grid-two-up.svg", 8, 8, "hard-drive.svg", 8, 8, "header.svg", 8, 8, "headphones.svg", 8, 8, "heart.svg", 8, 8, "home.svg", 8, 8, "image.svg", 8, 8, "inbox.svg", 8, 8, "infinity.svg", 8, 8, "info.svg", 8, 8, "italic.svg", 8, 8, "justify-center.svg", 8, 8, "justify-left.svg", 8, 8, "justify-right.svg", 8, 8, "key.svg", 8, 8, "laptop.svg", 8, 8, "layers.svg", 8, 8, "lightbulb.svg", 8, 8, "link-broken.svg", 8, 8, "link-intact.svg", 8, 8, "list-rich.svg", 8, 8, "list.svg", 8, 8, "location.svg", 8, 8, "lock-locked.svg", 8, 8, "lock-unlocked.svg", 8, 8, "loop-circular.svg", 8, 8, "loop-square.svg", 8, 8, "loop.svg", 8, 8, "magnifying-glass.svg", 8, 8, "map-marker.svg", 8, 8, "map.svg", 8, 8, "media-pause.svg", 8, 8, "media-play.svg", 8, 8, "media-record.svg", 8, 8, "media-skip-backward.svg", 8, 8, "media-skip-forward.svg", 8, 8, "media-step-backward.svg", 8, 8, "media-step-forward.svg", 8, 8, "media-stop.svg", 8, 8, "medical-cross.svg", 8, 8, "menu.svg", 8, 8, "microphone.svg", 8, 8, "minus.svg", 8, 8, "monitor.svg", 8, 8, "moon.svg", 8, 8, "move.svg", 8, 8, "musical-note.svg", 8, 8, "paperclip.svg", 8, 8, "pencil.svg", 8, 8, "people.svg", 8, 8, "person.svg", 8, 8, "phone.svg", 8, 8, "pie-chart.svg", 8, 8, "pin.svg", 8, 8, "play-circle.svg", 8, 8, "plus.svg", 8, 8, "power-standby.svg", 8, 8, "print.svg", 8, 8, "project.svg", 8, 8, "pulse.svg", 8, 8, "puzzle-piece.svg", 8, 8, "question-mark.svg", 8, 8, "rain.svg", 8, 8, "random.svg", 8, 8, "reload.svg", 8, 8, "resize-both.svg", 8, 8, "resize-height.svg", 8, 8, "resize-width.svg", 8, 8, "rss-alt.svg", 8, 8, "rss.svg", 8, 8, "script.svg", 8, 8, "share-boxed.svg", 8, 8, "share.svg", 8, 8, "shield.svg", 8, 8, "signal.svg", 8, 8, "signpost.svg", 8, 8, "sort-ascending.svg", 8, 8, "sort-descending.svg", 8, 8, "spreadsheet.svg", 8, 8, "star.svg", 8, 8, "sun.svg", 8, 8, "tablet.svg", 8, 8, "tag.svg", 8, 8, "tags.svg", 8, 8, "target.svg", 8, 8, "task.svg", 8, 8, "terminal.svg", 8, 8, "text.svg", 8, 8, "thumb-down.svg", 8, 8, "thumb-up.svg", 8, 8, "timer.svg", 8, 8, "transfer.svg", 8, 8, "trash.svg", 8, 8, "underline.svg", 8, 8, "vertical-align-bottom.svg", 8, 8, "vertical-align-center.svg", 8, 8, "vertical-align-top.svg", 8, 8, "video.svg", 8, 8, "volume-high.svg", 8, 8, "volume-low.svg", 8, 8, "volume-off.svg", 8, 8, "warning.svg", 8, 8, "wifi.svg", 8, 8, "wrench.svg", 8, 8, "x.svg", 8, 8, "yen.svg", 8, 8, "zoom-in.svg", 8, 8, "zoom-out.svg", 8, 8] (tmsRaphaelTimeline.js, line 124)

*/

app.service('tmsTimeline', [ 'ModalService', function(ModalService) {

    var qtimelinedata = {
	scope: null,
	r: null,
	path: null,
	axisx: [],
	axisy: null,
	data: [],
	booking: [],
	firstcol: [],
	tables: {},	
	tablesname: [],
	width: 1700,
	height: 1700,
	twidth: 0,
	theight: 0,   
	cellwidth: 50,     
	cellheight: 20,     
	leftgutter: 0,
	topgutter: 10,
	titleX: 100,
	titleY: 20,
	bookingX: 3,	// 3 slots
	toptext: 0,
	leftext: 0,
	backward: null,
	forward: null,
	cursor: 0,
	cbooking: '',
	cursorigin: 18,	// 2 slot per hour -> 9h = 18
	cursorlunch: 4,
	cursordinner: 14,
	axesXattr: {"font": '10px Fontin-Sans, Arial', stroke: "none", fill: "orange", cursor: "pointer"},
	axesYattr: {"font": '10px Fontin-Sans, Arial', stroke: "none", fill: "#fff"},
	mealattr: {"font": '10px Fontin-Sans, Arial', stroke: "none", fill: "#000000", cursor: "pointer"},
	mode: 0,		// 0 -> standard, 1 lunch/dinner
 
 		init: function(mode, cursorlunch, cursordinner) {
 			var i;
 			
			this.mode = (typeof mode !== "number" || mode < 0 || mode > 1) ? 0 : mode;
			this.cursorlunch = (typeof cursorlunch !== "number" || cursorlunch < 0 || cursorlunch > 8) ? 4 : cursorlunch;
			this.cursordinner = (typeof cursordinner !== "number" || cursordinner < 14 || cursordinner > 20) ? 14 : cursordinner;
			if(this.mode === 1)
				this.cursor = this.cursorlunch;
				
			this.r = Raphael("timeline_drawing", this.width, this.height);
			for(i = this.cursorigin/2; i < 24; i++) {
				this.axisx.push(i + ":00");
				this.axisx.push(i + ":30");
				}
			this.axisx.push(i + ":00");		// show only the 1 last colonne
			this.toptext = this.cellheight / 2;
			this.lefttext = this.cellwidth / 2;
			this.theight = ((this.tablesname.length + 1) * this.cellheight) + this.topgutter;
			this.postinit(0);
   			},

		postinit: function(n) {
			var self = qtimelinedata;
			var ncol, X, Y, ht, start;
						
			X = self.cellwidth;
			Y = self.cellheight;
			ht = self.theight - (self.topgutter + Y);
			start = self.leftgutter + self.titleX + ((self.titleX/2) * n);

			if(self.path) self.path.remove();
        		self.path = self.r.set();
			self.path.push(self.r.rect(start, self.topgutter + Y, X, ht).attr({stroke: "white", fill: "cyan"}));
        		ncol = (self.axisx.length - self.cursor) + 1; // for title
        		self.twidth = (ncol * self.cellwidth) + self.leftgutter;
			},
		
		reset: function() {
			this.axisx = [];
			this.axisy = [];
			this.tablesname = [];
			this.tables = {};
			this.data = [];
			this.booking = [];
			},
				
		refresh: function(state, n) {
			var self = qtimelinedata;
			
			if(self.r === null) // this is not running. just stop.
				return;
			if(typeof n !== "number") 
				n = 0;
			self.postinit(n);
			self.draw_axes();       
			self.drawgrid();
			self.showbooking();	
			if(state === 1) 
				this.scope.triggerBooking(this.cbooking);	
			},

		setscope: function(scope) {
			this.scope = scope;
			},
					
		setcursor: function(c) {
 			c -= this.cursorigin;
			if(this.mode === 0) {
				if(c >= 0 && c < this.axisx.length)
					this.cursor = c;
				}
			else this.cursor = (c >= this.cursordinner) ? this.cursordinner : this.cursorlunch;
			},
							 			       
 		settables: function(tb) {
 			var tbname; 
 			this.tables = JSON.parse(JSON.stringify(tb));	// cloning
 			for(tbname in tb)
 				this.tablesname.push(tbname);
			this.tablesname.sort(function(a, b) { return tb[a] - tb[b]; });	
			},
			 			       
 		gettables: function() {
 			return this.tablesname;
			},
			 			       
 		setaxisy: function(label) {
 			this.axisy = label.slice(0);
 			if(this.scope.gridorder)
 				this.axisy.sort();
			},
			 			       
 		setbooking: function(booking) {
 			this.booking = booking.slice(0);
			},

	colorcol: function(v) {
		alert(v);
		},

        draw_axes: function() {
        	var i, limit, X, Y, top, ncol, left, r, oo, qO = this;
        	
  		if(this.mode === 0)
 			ncol = (this.axisx.length - this.cursor);
 		else ncol = (this.cursor >= this.cursordinner) ? (31 - this.cursordinner) : (this.cursordinner - this.cursorlunch)+3;
	       	this.twidth = (ncol * this.cellwidth) + this.leftgutter;
			X = this.cellwidth;
			Y = this.cellheight;
			left = this.leftgutter + this.titleX + (X / 2);
			top = this.topgutter + this.toptext;
			
			left = this.leftgutter + this.titleX + (X / 2);
			for (i = 0; i < ncol; i++) {
				r = this.r.text(left + (X * i), top, this.axisx[i + this.cursor]).attr(this.axesXattr);
				(function(k) { 
					r.click(function(e) { qO.refresh(0, k); });
					if(this.mode === 0)
						r.dblclick(function(e) { qO.cursor+=k; qO.refresh(1);});
					 } )(i);
				this.path.push(r);
				}
		
			left = this.leftgutter + (this.titleX / 2);
			for (i = 0, limit = this.axisy.length; i < limit; i++)
				this.path.push(this.r.text(left, top + (Y * (i+1)), this.axisy[i] + ' x' + this.tables[this.axisy[i]]).attr(this.axesYattr));
        	},
        	
        drawgrid: function() {
        	var i, limit, index, oo, ht, lg, limit, xx, yy, X, Y, ncol, qO = qtimelinedata;
        	
 		if(this.mode === 0)
 			ncol = (this.axisx.length - this.cursor) + 1;
 		else ncol = (this.cursor >= this.cursordinner) ? (32 - this.cursordinner) : (this.cursordinner - this.cursorlunch)+4;
        	this.twidth = (ncol+1) * this.cellwidth;
		X = this.cellwidth;
		Y = this.cellheight;

		ht = this.theight;
		lg = this.twidth;

		// colonnes
		this.path.push(this.r.path("M 1,"+this.topgutter+" L 0,"+ht).attr("stroke-dasharray", ".").attr("stroke-width", 1).attr("stroke", "grey"));
		for (i = 0; i < ncol; i++) {
			xx = this.leftgutter + this.titleX + (X * i);
			this.path.push(this.r.path("M "+xx+ ","+this.topgutter+" L "+xx+","+ht).attr("stroke-dasharray", ".").attr("stroke-width", 1).attr("stroke", "grey"));
			}
	
		// lines
		for (i = 0, limit = this.axisy.length + 2; i < limit; i++) {
			yy = this.topgutter + (Y * i);
			this.path.push(this.r.path("M "+0+","+yy+" L "+lg+","+yy).attr("stroke-dasharray", ".").attr("stroke-width", 1).attr("stroke", "grey"));
			}
		
		this.path.push(this.r.rect(this.leftgutter, this.topgutter, this.titleX, this.titleY).attr("fill", "white"));

		if(this.mode === 0) {
			index = iconsAr.indexOf("media-step-backward.svg");
			if(index >= 0) {
				this.backward = oo = this.r.image(iconsRes[index].src, this.leftgutter + 5, 13, 14, 14);
				oo.attr("cursor", "hand");	
				oo.click(function(e) { if(qO.cursor > 0 ) { qO.cursor = 0; qO.forward.show(); console.log('CURSOR', qO.axisx[qO.cursor]); } qO.refresh(1); });
				this.path.push(oo);
				}
			index = iconsAr.indexOf("media-skip-backward.svg");
			if(index >= 0) {
				this.backward = oo = this.r.image(iconsRes[index].src, this.leftgutter + 35, 13, 14, 14);
				oo.attr("cursor", "hand");	
				oo.click(function(e) { if(qO.cursor > 0 ) { qO.cursor--; qO.forward.show(); console.log('CURSOR', qO.axisx[qO.cursor]); qO.refresh(1); } else this.hide(); });
				this.path.push(oo);
				}
			index = iconsAr.indexOf("media-skip-forward.svg");	
			if(index >= 0) {
				this.forward = oo = this.r.image(iconsRes[index].src, (this.leftgutter + this.titleX) - 40, 13, 14, 14);
				oo.attr("cursor", "hand");	
				oo.click(function(e) { if(qO.cursor < qO.axisx.length - 1) { qO.cursor++; qO.backward.show(); console.log('CURSOR', qO.axisx[qO.cursor]); qO.refresh(1); } else this.hide(); });
				this.path.push(oo);
				}
			index = iconsAr.indexOf("media-step-forward.svg");
			if(index >= 0) {
				this.forward = oo = this.r.image(iconsRes[index].src, (this.leftgutter + this.titleX) - 15, 13, 14, 14)
				oo.attr("cursor", "hand");	
				oo.click(function(e) { if(qO.cursor < qO.axisx.length - 5) { qO.cursor = qO.axisx.length - 5; qO.backward.show(); console.log('CURSOR', qO.axisx[qO.cursor]); } qO.refresh(1); });
				this.path.push(oo);
				}

			if(this.cursor === 0 ) this.backward.hide();
			if(this.cursor === this.axisx.length - 1 ) this.forward.hide();
			}
		else {
			r = this.r.text(this.leftgutter + 22, 20, "LUNCH").attr(this.mealattr);
			r.click(function(e) { qO.cursor=qO.cursorlunch; qO.refresh(1);});
			this.path.push(r);
			r = this.r.text((this.leftgutter + this.titleX) - 25, 20, "DINNER").attr(this.mealattr);
			r.click(function(e) { qO.cursor=qO.cursordinner; qO.refresh(1);});
			this.path.push(r);
			}
        	},
        
        showbooking: function() {
		var k, i, j, t, X, Y, Z, L, tableAr, emptyChair, slot, ncol, top, 
			left, txt, oo, booking, colorbox, atable, toptext, txtatt, qO = qtimelinedata;

		txtatt = {"font": '10px Fontin-Sans, Arial', stroke: "none", fill: "yellow"};
			
 		if(this.mode === 0)
 			ncol = (this.axisx.length - this.cursor);
 		else ncol = (this.cursor >= this.cursordinner) ? (32 - this.cursordinner) : (this.cursordinner - this.cursorlunch) + 2;

		this.twidth = (ncol + 1) * this.cellwidth;
		X = this.cellwidth;
		Y = this.cellheight;
		toptext = this.topgutter + this.titleY + this.toptext;
		left = this.leftgutter + this.titleX;
		top = this.topgutter + this.titleY;
		
		this.cbooking = "";
		for(k = 0; k < this.booking.length; k++) {
			if(this.booking[k].tablename !== '') {
				slot = this.booking[k].time.timetoslot();
				if(this.mode === 1 && this.cursor < this.cursordinner && slot > (ncol + this.cursorigin)+4)
					continue;
					
				Z = X * this.bookingX;
				i = slot - (this.cursorigin + this.cursor);
				if(i < 0) {
					Z = X * (i + this.bookingX);
					if(Z > 0) i = 0;
					}
				L = left + (Z/2);
				tableAr = this.booking[k].tablename.split(',');
				for(t = 0; t < tableAr.length; t++) {
					atable = tableAr[t];
					j = this.axisy.indexOf(atable);	
					if(j >= 0 && i >= 0) {
						emptyChair = this.tables[atable] - this.booking[k].pers;
						colorbox = (emptyChair <= 1) ? "brown" : "purple";
						if(tableAr.length > 1) colorbox = "blue";
						booking = this.booking[k].booking.replace(/^[^_]+_/, '');
						booking = booking.substring(booking.length - 6);
						txt = (Z >= 2 * X && false) ? this.booking[k].last + ' x ' + this.booking[k].pers + '  :' + booking : this.booking[k].last + ' x ' + this.booking[k].pers;
						oo = this.r.rect(left + (X * i), top + (Y * j), Z, Y).attr({stroke: "white", fill: colorbox, cursor: "hand"});
						(function(conf, tt) { oo.click(function(e) { qO.showconf(conf, tt); }); } )(this.booking[k].booking, this.booking[k].time);
						this.path.push(oo);
						oo = this.r.text(L + (X * i), toptext + (Y * j), txt).attr(txtatt).attr("cursor", "hand");
						(function(conf, tt) { oo.click(function(e) { qO.showconf(conf, tt); }); } )(this.booking[k].booking, this.booking[k].time);
						this.path.push(oo);
						if(i === 0 || this.cbooking === "") this.cbooking = this.booking[k].booking;
						}
					}
				}
			}
		return true;
        	},
        
        showconf: function(booking, time) {
        	var cur = this.timetoslot(time);

 		this.setcursor(cur);
        	this.refresh(0);
         	return this.scope.triggerBooking(booking);
        	},
        
        sethour: function(time) {
        	var cur = this.timetoslot(time + ":00");
 		this.setcursor(cur);
        	this.refresh(0);
        	},
        	
        timetoslot: function(vv) {
		var t = vv.split(':'); 
		return (parseInt(t[0]) * 2) + Math.floor(parseInt(t[1]) / 30);
        	},
        				
        dummyend: null
        };


	this.init = function(bookings, tables, scope) {
 
        var tbname, qO, tbObj = {}; 

        qO = qtimelinedata;	
        qO.reset();
        for(tbname in tables) {
        	if(tables[tbname] > 0)
	        	tbObj[tbname] = tables[tbname] & 0xff;
 			}
 			
 		qO.setscope(scope);
 		qO.settables(tbObj);
 		qO.setaxisy(qO.gettables());
 		qO.setbooking(bookings);
 		qO.setscope(scope);
		};
		
	this.drawtimeline = function(mode, cursorlunch, cursordinner) {
		
		var qO, loaded = $('#mydisplay').html().indexOf('timeline_drawing');
		if(loaded  < 0) 
			return false;     		

		qO = qtimelinedata;		
		qO.init(mode, cursorlunch, cursordinner);	       	
		qO.draw_axes();       
		qO.drawgrid();
		qO.showbooking();	 	
		return true;
	};
	
	this.refresh = function() {
		qtimelinedata.refresh(0);
		};
		
	this.sethour = function(hour) {
		qtimelinedata.sethour(hour);
		};
				
}]);

