<html>
<html><head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="HandheldFriendly" content="true" />
<link href='https://fonts.googleapis.com/css?family=Dosis:400,600,700bold|Roboto:400,900,700italic,700,500italic,500,400italic' rel='stylesheet' type='text/css'>
<title>Terms and conditions of services Weeloy.com | Dine with discount</title>

<style>

body {
font-size:11px;
font-family:Roboto;
margin: 0 0 0 10px;
}

ol p {
margin: 20px 10px 10px 0;
text-justify:inter-word;
text-align: justify;
}

ol li {
margin: 10px 20px 0 0;
text-justify:inter-word;
text-align: justify;
}

ol {
margin: 10px 0 0 0;
}

h5 {
color:red;
text-transform:uppercase;
margin: 30px 0 10px 0;
}

#section {
margin: 0 0 0 -40;
}

ol { counter-reset:section; list-style-type:none; }
ol li { list-style-type:none; }
ol li ol { counter-reset:subsection; }
ol li ol li ol { counter-reset:subsubsection; }
ol li:before{
    counter-increment:section;
    content:counter(section) ". ";/*content:"Section " counter(section) ". ";*/
    font-weight:bold;
}
ol li ol li:before {
    counter-increment:subsection;
    content:counter(section) "." counter(subsection) " ";
}

ol li ol li ol li:before {
    counter-increment:subsubsection;
    content:counter(section) "." counter(subsection) "." counter(subsubsection) " ";
}

</style>
</head>
<body>

<?php


if(!empty($_REQUEST['restaurant'])) 
	if(!empty($_REQUEST['tracking']) && (strpos($_REQUEST['tracking'], "WEBSITE") !== false || strpos($_REQUEST['tracking'], "facebook") !== false  || strpos($_REQUEST['tracking'], "GRABZ") !== false) ) {
		require_once("conf/conf.init.inc.php");
		require_once("lib/wpdo.inc.php");
		require_once("lib/wglobals.inc.php");
		require_once('conf/conf.session.inc.php');
		require_once("lib/class.media.inc.php");
		require_once("lib/class.restaurant.inc.php");

		$res = new WY_restaurant;
		$res->getRestaurant($_REQUEST['restaurant']);

		echo "<div class='modal-body'>";
		echo preg_replace("/\r|\n/", "<br />", $res->restaurant_tnc);
		echo "<div class='modal-footer'> <button type='button' class='btn btn-primary' data-dismiss='modal'>Close</button></div>";
		echo "</body></html>";
		exit;
		}
?>

<ol>
<!--
<div class="modal-header">
	<button type="button" class="close btn-primary" data-dismiss="modal" aria-hidden="true">&times;</button>
	 <h4 class="modal-title">Terms and Conditions of Service</h4>
</div>
-->
<div class="modal-body">

<h1 align='center' style='margin:10px 0 40px 0;'> Terms and Conditions of Service</h1>
<p>
    Weeloy.com, Weeloy.Dining.com and its sub-domains (hereinafter collectively referred to as the "Sites") are powered by Weeloy Pte. Ltd. (hereinafter
    referred to as "<strong>Weeloy</strong>").
</p>
<p>
    Weeloy and/or affiliate(s) hereinafter collectively referred to as “<strong>Weeloy</strong>”; if the context requires or permits, “Weeloy” may refer to any
    one of Weeloy’s and/or affiliate(s) that also operate and provide services via, apart from the Sites, other media platforms or other websites (hereinafter
    referred to as the "Platforms") and applications including mobile applications (hereinafter referred to as the "Applications") which are developed in whole
    or in part by the Weeloy.
</p>
<p>
    The Sites, the Platforms and the Applications may be referred to collectively as the "Channels" hereinafter. The services provided through the Channels
    shall only be available to Users, registered or non-registered, uploading, posting, viewing, booking, forwarding and/or otherwise using the advertisements,
    promotional materials, views, comments and/or other information on the Channels (hereinafter collectively referred to as the “Users”).
</p>
<p>
    Access to and use of the contents and services provided on the Channels shall be subject to the WeeloyPrivacy Policy and the terms and conditions, which
    are stated below and hereinafter referred to as the "Terms and Conditions". By using the Channels and any other site and/or media platforms and/or
    applications accessed through such Channels, Users acknowledge and agree that the Weeloy Privacy Policy and the Terms and Conditions set out below are
    binding upon them. If a User does not accept either or both of the Weeloy Privacy Policy and the Terms and Conditions, please do not use the Channels.
</p>
<p>
    Weeloy reserves the right, at its own discretion, to revise the Weeloy Privacy Policy and the Terms and Conditions at any time without prior notice. Once
    posted on the Channels, the amended Weeloy Privacy Policy and the Terms and Conditions shall apply to all Users. Users are advised to visit this page
    periodically to review the latest Weeloy Privacy Policy and the Terms and Conditions.
</p>
<p>
    Continued use shall constitute acceptance of all changes and shall remain binding upon the User; unless User serves his/her notice to Weeloy that such
    changes are unacceptable at which time access to the Channels and the Services (as defined herein) will be terminated.
</p>
<p>
    By using the Channels, Users also acknowledge and agree that, if applicable, in addition to being bound by the Terms and Conditions stated herein, they
    will at the same time shall be bound by the terms and conditions of the relevant and applicable Channel(s) they use and the terms and conditions of the
    relevant and applicable Channel(s) operated by the Weeloy which their Materials (as defined in Section 3.1 below) are being or have been uploaded to or
    posted on.
</p>
<p>
    The terms “User" and “Users” herein refer to all individuals and/or entities accessing and/or using the Channels at anytime, in any country, for any reason
    or purpose.
</p>


	<li><strong>General Terms</strong>
	<ol>
	<li>Weeloy provides online platforms for Users to list the services or products related to food and beverage which they offer and provides online marketing
    (including but not limited to posting advertisements, promotional campaigns, marketing materials and hosting special events) as well as providing
    discussion forums and online platforms for Users to upload, post and forward their comments and photos related to their dining experiences (hereinafter
    referred to as the "Services").
	</li>
	<li>Weeloy is not a party to nor is it involved in any actual transaction between Users.
	</li>
	<li>Users agree that all promotional offers that are posted on Weeloy Channels are not applicable in conjunction with other discounts, 
	promotional offers or during special festive holidays, unless explicitly stated.
	</li>
	<li>Weeloy is committed to protect the privacy of the Users. Weeloy uses the information of the Users according to the terms as stated in the Weeloy Privacy
    Policy
	</li>
	</ol>
	</li>
	<li><strong>Prohibited Uses for all Users</strong>
	<ol>
    Users of the Channels, registered or non-registered, agree not to use any of the Channels for any of the following purposes which are expressly prohibited:

	<li>All Users are prohibited from violating or attempting to violate the security of the Channels including, without limitation, accessing data not intended
    for them or logging into a server or account which they are not authorized to access, attempting to probe, scan or test the vulnerability of a system or
    network or attempting to breach security or authentication measures without proper authorization, attempting to interfere with service to any user, host or
    network or sending unsolicited e-mail. Violation of system or network security may result in civil and/or criminal liabilities.
	</li>
	<li>A User shall not delete or revise any material or information posted by any other Users.
	</li>
	<li>All Users shall not use the Channels (1) for uploading, posting, publishing, transmitting, distributing, circulating or storing material in violation of
    any applicable laws or regulations; or (2) in any manner that will infringe the copyright, trademark, trade secrets or other intellectual property rights
    of others or violate the privacy or publicity or other personal rights of others; or (3) in any manner that is harmful, defamatory, libelous, obscene,
    discriminatory, harassing, threatening, abusive, hateful or is otherwise offensive or objectionable. In particular, all Users shall not print, download,
    duplicate or otherwise copy or use any personally identifiable information about other Users (if any). All unsolicited communications of any type to Users
    are strictly prohibited.
	</li>
	<li>Users shall not use the Channels if they do not have legal capacity to form legally binding contracts.
	</li>
	<li>Users shall not upload or post any advertisement or materials on the Channels which contains any false, inaccurate, misleading or libelous content or
    contains any computer viruses, trojan horses, worms, computed files or other materials that may interrupt, damage or limit the functionality of any
    computer software or hardware or telecommunication equipment. Also, the advertisement shall not be fraudulent or involve sale of illegal products.
	</li>
	<li>Users shall not engage in spamming, including but not limited to any form of emailing, posting or messaging that is unsolicited.
	</li>
	</ol>
	</li>
	<li><strong>Acceptable uses of the Channels</strong>
	<ol>
	<p>
    Specific uses - User(s) uploading or posting advertisements, photos, content, views, comments, messages and/or other information on the Channels
    (hereinafter collectively referred to as the “Material(s)”).  Such User(s) uploading or posting the Materials shall hereinafter be referred to as the
    "Posting User(s)".
    </p>
	<li>The Posting User agrees that he/she/it shall only use the Channels for lawful purposes and for enjoying the Services provided through the Channels.
    Weeloy reserves the right to edit, share, reject, disapprove, erase and delete any Materials posted on the Channels as it sees appropriate.
	</li>
	<li>In the event that the Posting User is an individual, he/she shall not post his/her identity card and/or passport number on the Channels.
	</li>
	<li>Although Weeloy shall use its reasonable endeavors to restrict access to the database of the Posting Users’ personal data only to the personnel of
    Weeloy, Weeloy does not guarantee that other parties will not, without Weeloy’s consent, gain access to such database. For the usage and protection of
    personal data provided by the Posting Users, please refer to the Weeloy Privacy Policy.
	</li>
	<li>Users who upload or post Materials on the Channels shall be solely responsible for the Materials uploaded, posted or shared by them and/or any web pages
    and/or media platforms and/or applications linked to the Channels posted by them. Weeloy reserves the right to edit, share, reject, erase, remove and
    delete any Materials and links to web pages and/or media platforms and/or applications as it sees appropriate.
	</li>
	<li>Weeloy shall have the right to terminate any services to any Posting Users at its sole discretion. If a User uploads or posts Materials on the Channels
    and subsequently deletes and/or removes the same or the User terminates his/her/its accounts, or that Weeloy deletes any and/or removes such uploaded or
    posted Materials, such Materials will no longer be accessible by the User who uploaded or posted the same via that User’s account; however, such deleted
    Materials may still persist and appear on any part of the Channels, and/or be used in any form by Weeloy.
	</li>
	<li>Weeloy reserves the right to request any User to cease using or to change his/her/its username immediately upon notice given to the relevant user
    without giving any reason as and when Weeloydeems appropriate to do so; if any User disagrees and refuses to abide by such request made by Weeloy, Weeloy
    may at any time at its sole discretion, deactivate that User’s account without prior notification to that User and without prejudice to all Weeloy’s other
    rights and remedies.
	</li>
	<li>This paragraph shall only be applicable to Users posting advertisements, promotional and marketing materials (“Advertiser(s)”):-  
	</li>
	<li>Upon payment of a service fee to Weeloy or upon acceptance of any free trial promotion offer, Users will be entitled to use the Sites and/or Platforms
    and/or Applications (as the case may be) to post advertisements and promotional materials (subject to the Terms and Conditions and any specific terms and
    conditions of any service agreement(s) entered into between Weeloy and the User(s), and in the event of any conflict between the two, the latter shall
    prevail).
	</li>
	<li>Weeloy also reserves the right to change the service fee or institute new charges or fees to be paid by Advertisers for posting advertisements and
    promotional materials on any of the Channels, as it deems appropriate.
	</li>
	<li>In the event that any Advertiser posting advertisements and promotional materials fails to pay the service fee or any other fees or charges due to the
    Weeloy, Weeloy reserves the right to suspend or terminate that Advertiser’s user account, advertisements and links to web pages and/or media platforms
    and/or applications without prejudice to all its other rights and remedies.
	</li>
	<li>This paragraph shall only be applicable to the use of the Applications:-  By using the Applications, Users acknowledge and agree to the following:-
	</li>
	<li>If Users use the Applications to upload, post and share materials, including but not limited to instantly uploading, posting and sharing photos taken,
    the Users are consenting to the Materials being shared;
	</li>
	<li>The Users’ use of the Applications may cause personally identifying information to be publicly disclosed and/or associated with the relevant Users,
    even if Weeloy has not itself provided such information; and
	</li>
	<li>The Users shall use the Applications at their own option and risk and at their own accord. The Users will hold Weeloy harmless for activities related
    to their use of the Applications.
	</li>
	<li>Specific uses - User(s) viewing the Materials posted on the Channels
	</li>
    <p>(hereinafter referred to as the "Viewer(s)") The Viewer agrees that he/she/it shall only use the Channels for lawful purposes and for enjoying the Services
    provided therein. The Viewer agrees that any personal data received from the Channels or Weeloy shall only be used for the purpose of identifying and/or
    locating advertisements or materials or any content therein or for the purpose of enjoying the Services provided through the Channels. Any personal data
    received which are irrelevant to the above purposes shall be disregarded and shall not be saved, stored, collected, processed, used, distributed,
    published, disclosed or transmitted in any way, including but not limited to for any commercial purpose. The Viewer also agrees that any personal data
    collected from the Channels or Weeloy shall be promptly and properly deleted when the above purposes have lapsed or been achieved. Weeloy shall not be
    responsible or held liable in any way if any Users, in breach of the Terms and Conditions or the terms and conditions of any relevant and applicable
    Channel(s), in any country, use the other Users’ personal data, information or materials (whether obtained from the Channels or not) for any purpose.
	</p>
	<li>All Users accept that all personal data, information or materials provided by them publicly on the Channels are voluntarily provided and are given
    entirely at their own risk. The Weeloy shall not bear the responsibility of protecting the personal data, information or materials so provided publicly
    therein.
	</li>
	<li>Specific uses - User(s) using online restaurant reservation service. For any User using online restaurant reservation service via    <a href="https://www.weeloy.com/">www.weeloy.com</a> which is operated by the Weeloy, the User is deemed to have read, understood and accepted the Terms and
    Conditions before using the said service.
	</li>
	</ol>
	</li>
	<li><strong>Content License</strong>
	<ol>
	<li>
    By uploading or posting Materials on the Channels, the User unconditionally grants Weeloy a non-exclusive, worldwide, irrevocable, royalty-free right to
    exercise the copyright, publicity and database rights (but no other rights) he/she/it has in the Materials in order that Weeloy can use, publish, host,
    display, promote, copy, download, forward, distribute, reproduce, transfer, edit, sell and re-use the Materials in any form and anywhere, with or without
    making any commercial gains or profits, and carry out the purposes set out in the Weeloy Privacy Policy and herein.
	</li>
	</ol>
	</li>
	<li><strong>Intellectual Property Rights</strong>
	<ol>
	<li>
    All contents of the Channels, including without limitation the text, images, information, comments, layout, database, graphics, photos, pictures, sounds or
    audio formats, software, brands and HTML are the intellectual properties of Weeloy or the Users (as the case may be) which are protected by applicable
    copyright and trademark laws and may not be downloaded or otherwise duplicated without the express written permission of Weeloy or the Users (as the case
    may be). Re-use of any of the foregoing is strictly prohibited and Weeloy reserves all its rights. Any use of any of such content other than those
    permitted under the Terms and Conditions, the terms and conditions of any specific Channel(s) and the terms and conditions of any service agreement(s)
    entered into between Weeloy and the User(s) is strictly prohibited and Weeloy reserves all its rights in this respect. For the avoidance of doubt, any
    purported consent of any third parties on the use of the contents and materials mentioned under this Clause shall not exonerate the Users from the
    restrictions/prohibitions imposed hereunder in whatsoever manner.
	</li>
	</ol>
	</li>
	<li><strong>Contents</strong>
	<ol>
	<li>Users acknowledge that the Wheel displayed on the restaurant page on the channels (and most specifically on the weeloy.com) is the current wheel of the
	participating restaurants at the time of reservations, and subject to change by the restaurant at the time of their visits.
	</li>
	<li>Users acknowledge that Weeloy may not pre-screen or pre-approve certain content posted on the Channels or any content sent through the Channels. In any
    event, Weeloy takes no responsibility whatsoever for the content on the Channels or any content sent through the Channels, or for any content lost and does
    not make any representations or warranties regarding the content or accuracy of any material therein.   
	</li>
	<li>Any Materials uploaded or posted on the Channels by the Users may be viewed by users of other web sites and/or media platforms and/or applications
    linked to the Channels and Weeloy is not responsible for any improper and/or illegal use by any user or third party from linked third party web sites
    and/or media platforms and/or applications of any data or materials posted on the Channels.
	</li>
	<li>Links to third party web sites and/or media platforms and/or applications provided on the Channels are provided solely as a convenience to the Users and
    as internet navigation tools, and not in any way an endorsement by the Weeloy of the contents on such third party web sites and/or media platforms and/or
    applications.
	</li>
	<li>Unless otherwise stated on the Channels, Weeloy has no control over or rights in such third party web sites and/or media platforms and/or applications
    and is not responsible for any contents on such third party web sites and/or media platforms and/or applications or any use of services provided by such
    third party web sites and/or media platforms and/or applications by the Users.
	</li>
	<li>All Users acknowledge and agree that they are solely responsible for the form, content and accuracy of any Materials, web page or other information
    contained therein placed by them.
	</li>
	<li>Weeloy is not responsible for the content of any third party web sites and/or media platforms and/or applications linked to the Channels, and does not
    make any representations or warranties regarding the contents or accuracy of materials on such third party web sites and/or media platforms and/or
    applications. If any User accesses any linked third party web sites and/or media platforms and/or applications, he/she/it does so entirely at his/her/its
    own risk.
	</li>
	<li>Weeloy shall have the right to remove any Materials uploaded or posted on the Channels at its sole discretion without any compensation or recourse to
    the Posting Users if Weeloy considers at its sole discretion that such Users have breached or is likely to breach any law or the Terms and Conditions or
    any terms and conditions of any specific Channel(s) or service agreement(s) entered into between Weeloy and the Posting User(s).
	</li>
	<li>In the event, Weeloy decides to remove any paid advertisement for any reasons not relating to any breach of law or the provisions herein, Weeloy may,
    after deducting any fees that may charged for the period that the advertisement has been posted on the Channels, refund the remaining fees (if any) to the
    related Posting User in accordance with the Terms and Conditions or the terms and conditions of any specific Channel(s) or service agreement(s) entered
    into between Weeloy and the related Posting User, without prejudice to Weeloy’s rights and remedies hereunder.  Users agree and consent that the Weeloy
    may, subject to the terms of the Weeloy Privacy Policy, use their personal data and/or other information provided to the Channels for purposes relating to
    the provision of Services and/or offered by the Weeloy and marketing services and/or special events of the Weeloy.
	</li>
	</ol>
	</li>
	<li><strong>Responsibility</strong>
	<ol>
	<li>Weeloy may not monitor the Channels at all times but reserves the right to do so.
	</li>
	<li>Weeloy does not warrant that any Materials or web page or application will be viewed by any specific number of Users or that it will be viewed by any
    specific User.
	</li>
	<li>Weeloy shall not in any way be considered an agent of any User with respect to any use of the Channels and shall not be responsible in any way for any
    direct or indirect damage or loss that may arise or result from the use of the Channels, for whatever reason made.
	</li>
	<li>Weeloy endeavors to provide quality service to all Users. However, Weeloy does not warrant that the Channels will operate without error at at all time
    and are free of viruses or other harmful mechanisms. If use of the Channels or their contents result in the need for servicing or replacement of equipment
    or data by any user, Weeloy shall not be liable for such costs. The Channels and their contents are provided on an “As Is” basis without any warranties of
    any kind. To the fullest extent permitted by law, Weeloy disclaims all warranties, including, without prejudice to the foregoing, any in respect of
    merchantability, non-infringement of third party rights, fitness for particular purpose, or about the accuracy, reliability, completeness or timeliness of
    the contents, services, text, graphics and links of the Channels.
	</li>
	</ol>
	</li>
	<li><strong>Own Risk</strong>
	<ol>
	<li>All Users shall use the Channels and any other web sites and/or media platforms and/or applications accessed through the Channels entirely at their own
    risk.
	</li>
	<li>ALL Users are responsible for the consequences of their postings. Weeloy does not represent or guarantee the truthfulness, accuracy or reliability of
    any Materials uploaded or posted by the Posting Users or endorses any opinions expressed by the Posting Users.
	</li>
	<li>Any reliance by any User on advertisements and materials posted by the other Users will be at their own risk. Weeloy reserves the right to expel any
    User and prevent his/her/its further access to the Channels, at any time for breaching this agreement or violating the law and also reserves the right to
    remove any Materials which is abusive, illegal, disruptive or inappropriate at Weeloy’s sole discretion.
	</li>
	</ol>
	</li>
	<li><strong>Indemnity</strong>
	<ol>
	<li>
    All Users agree to indemnify, and hold harmless Weeloy, its officers, directors, employees, agents, partners, representatives, shareholders, servants,
    attorneys, predecessors, successors and assigns from and against any claims, actions, demands, liabilities, losses, damages, costs and expenses (including
    legal fees and litigation expenses on a full indemnity basis) arising from or resulting from their use of the Channels or their breach of the terms of this
    Agreement or any terms and conditions of any specific Channel(s) or service agreement(s) entered into between Weeloy and the User(s). Weeloy will provide
    prompt notice of any such claim, suit or proceedings to the relevant User.
	</li>
	</ol>
	</li>
	<li><strong>Limitation of the Service</strong>
	<ol>
	<li>Weeloy shall have the right to limit the use of the Services, including the period of time that Materials will be posted on the Channels, the size,
    placement and position of the Materials, email messages or any other contents, which are transmitted by the Services.
	</li>
	<li>Weeloy reserves the right, at its sole discretion, to edit, modify, share, erase, delete or remove any Materials posted on the Channels, for any reason,
    without giving any prior notice or reason to the Users. The Users acknowledge that Weeloy shall not be liable to any party for any modification, suspension
    or discontinuance of the Services.
	</li>
	</ol>
	</li>
	<li><strong>Termination of Service</strong>
	<ol>
	<li>Weeloy shall have the right to delete or deactivate any account, or block the email or IP address of any User, or terminate the access of Users to the
    Services, and remove any Materials within the Services immediately without notice for any reason, including but not limited to the reason that the User
    breached any law or the Terms and Conditions or any terms and conditions of any specific Channel(s) or any service agreement(s) entered into between Weeloy
    and the User(s).
	</li>
	<li>Weeloy reserves the right at any time to take such action as it considers appropriate, desirable or necessary including but not limited to taking legal
    actions against any such User.
	</li>
	<li>Weeloy shall have no obligation to deliver any Materials posted on the Channels to any User at any time, both before or after cessation of the
	</li>
	<p>
    Services or upon removal of the related Material(s) from the Channels.
	</p>
	<li>The Terms and Conditions set out hereunder shall become inapplicable to the Users immediately upon the Users discontinuing their use of the Sites and/or
    the Platforms and/or Applications as the case may be.
	</li>
	</ol>
	</li>
	<li><strong>Disclaimer</strong>
	<ol>
	<li>Weeloy does not have control over and does not guarantee the truth or accuracy of listings of any Materials posted on the Channels or any content on
    third party web sites and/or media platforms and/or applications accessed via the Channels.
	</li>
	<li>In any event, Weeloy, its officers, directors, employees, agents, partners, representatives, shareholders, servants, attorneys, predecessors and
    successors shall not be liable for any losses, claims or damages suffered by any User whatsoever and howsoever arising or resulting from his/her/its use or
    inability to use the Channels and their contents, including negligence and disputes between any parties.
	</li>
	</ol>
	</li>
	<li><strong>Limitation of Liability</strong>
	<ol>
	<li>
    Without prejudice to the above and subject to the applicable laws, the aggregate liability of Weeloy to any User for all claims arising from their use of
    the Services and the Channels shall be limited to the amount of SGD100.
	</li>
	</ol>
	</li>
	<li><strong>Security Measures</strong>
	<ol>
	<li>Weeloy will use its reasonable endeavors to ensure that its officers, directors, employees, agents and/or contractors will exercise their prudence and
    due diligence in handling the personal data submitted by the Users, and the access to and processing of the personal data by such persons is on a
    "need-to-know" and "need-to-use" basis.
	</li>
	<li>Weeloy will use its reasonable endeavors to protect the personal data against any unauthorized or accidental access, processing or erasure of the
    personal data.
	</li>
	</ol>
	</li>
	<li><strong>Severability</strong>
	<ol>
	<li>The provisions of the Terms and Conditions shall be enforceable independently of each other and the validity of each provision shall not be affected if
    any of the others is invalid.
	</li>
	<li>In the event, any provision of the Terms and Conditions is determined to be illegal, invalid or unenforceable, the validity and enforceability of the
    remaining provisions of the Terms and Conditions shall not be affected and, in lieu of such illegal, invalid, or unenforceable provision, there shall be
    added as part of the Terms and Conditions one or more provisions as similar in terms as may be legal, valid and enforceable under the applicable law.
	</li>
	</ol>
	</li>
	<li><strong>Conflict</strong>
	<ol>
	<li>
    If there is any conflict between (1) the Terms and Conditions and/or specific terms of use appearing on the Platforms and/or the Applications and/or any
    term and (2) conditions of any service agreement(s) entered into between Weeloy and the User(s), and/or any specific terms and conditions of use in respect
    of any special events hosted by Weeloy, then the latter shall prevail.
    </li>
	</ol>
	</li>
	<li><strong>Governing Law and Dispute Resolutions</strong>
	<ol>
	<li>The Terms and Conditions and any dispute or matter arising from or incidental to the use of the Channels shall be governed by and construed in
    accordance with the laws of Singapore unless otherwise specified.   
	</li>
	<li>Any dispute, controversy or claim arising out of or relating to the Terms and Conditions including the validity, invalidity, breach or termination
    thereof, shall be settled by arbitration in accordance with the Arbitration Rules as at present in force and as may be amended by the rest of this Clause:
	</li>
	<li>The appointing authority shall be Singapore; or alternatively, an appointing authority may be appointed by Weeloy at its sole and absolute discretion in
    any country, which Weeloy considers as fit and appropriate.
	</li>
	<li>Any User(s) who are in dispute with Weeloy acknowledge(s) and agree(s) that the choice of the appointing authority nominated by Weeloy shall be final
    and conclusive.
	</li>
	<li>The place of arbitration shall be in Singapore; or alternatively, at any such arbitral body in any country as Weeloy considers fit and appropriate at
    its sole and absolute discretion.
	</li>
	<li>There shall be only one arbitrator.
	</li>
	<li>The language to be used in the arbitral proceedings shall be English.
	</li>
	<li>In the event of any breach of the Terms and Conditions by any one party, the other party shall be entitled to remedies in law and equity as determined
    by arbitration.
	</li>
	</ol>
	</li>
	<br><hr>
</ol>
    <ol>
        <div class="modal-body">
            <h1 align='center' style='margin:10px 0 40px 0;'> Weeloy Privacy Policy</h1>
            <p>
                I consent for my personal data to be collected, used, processed and stored by Weeloy (as defined below) in connection with my use of Weeloy’s online restaurant system and table reservation services.
            </p>
            <p>
                I would like to receive all latest dining trends, promotional offers and updates from Weeloy and Weeloy's participating merchants in accordance with the following terms and conditions. 
            </p>
            <p>
                In compliance with requirements of the Personal Data Protection Act 2012 ("PDPA"), Weeloy Pte. Ltd. and/or affiliates including but not limited to establishments and media partners that have entered into an “agreement” with Weeloy (collectively, “Weeloy”) have adopted this Privacy Policy. 
                Users are strongly recommended to read this Privacy Policy carefully to have an understanding of Weeloy’s policy and practices with regard to the treatment of personal data provided by Users using Weeloy.com, its sub-domains, any other websites, media platforms or applications including mobile applications operated/developed by Weeloy (collectively, "Channels"). This Privacy Policy is applicable to both registered and non-registered Users, and the provisions herein may be updated, revised, varied and/or amended from time to time as Weeloy deems necessary. 
                If Users have questions or concerns regarding this Privacy Policy, please contact Weeloy's Customer Service Department at +65 62211016 or email to info@weeloy.com 
            </p>
            <li><strong>Consent for Collection of Personal Data</strong>
                <ol>
                    <li>In the course of using the Channels, Users (registered or otherwise) may be requested to disclose to provide personal data in order to enjoy various services offered by the Channels. All personal data is provided voluntary. Users may refuse to provide such personal data or withdraw their consent for the collection, use disclosure and storage of their personal. In so doing, Weeloy would not be able to provide certain services on the Channels.  The following personal data may be requested: name, log-in ID and password, address, email address, telephone number, age, sex, date of birth, country of residence, nationality, education level and work experience that is/are not otherwise publicly available.  Occasionally, Weeloy may also collect additional personal data from Users in connection with contests, surveys, or special offers.</li>
                    <li>When a User provides personal data to Weeloy, the User is deemed to have consented to the collection, storage, use and disclosure of his/her personal data to Weeloy in accordance with this Privacy Policy. </li>
                    <li>If a User is under the age of 13 years ("minors"), Weeloy strongly recommends him/her to seek prior consent from a person with parental responsibility, e.g. parent or guardian, who may contact Weeloy at +65 6221 1016 or email to info@weeloy.com for registering the User as member of the Channels. Where collection of minors' personal data is made through persons claiming to be parents or guardians, Weeloy may (but is not obliged to) require proof to verify such claims.</li>
                </ol>
            </li>
            <li><strong>Consent for Collection of Personal Data</strong>
                <ol>
                    <li>Weeloy strives to only collect personal data, which is necessary and adequate but not excessive in relation to the purposes set out below. If Weeloy requires the use of Users’ personal data beyond the stated purposes, Weeloy may request the Users’ prescribed consent to the same or if a User is a minor, the prescribed consent should be given by his/her parent or guardian.</li>
                    <li>Weeloy’s purposes for the collection, use and disclosure of Users' personal data include but are not limited to the following (either independently by Weeloy or together with its establishments and media partners sited in and outside of Singapore):
                        <ol>
                            <li>Daily operation of the services provided to Users.
                            </li>
                            <li>Identify Users who have (i) posted advertisements, materials, messages, photos, views or comments or such other information (collectively “Information”) on the Channels; (ii) viewed any information posted on the Channels; or (iii) enjoyed their benefits as members of the Channels by receiving and using marketing and promotional materials.
                            </li>
                            <li>Provide Users with (i) marketing and promotional materials for their enjoyment of benefits as members of the Channels (as detailed in Paragraph 4 below); and (ii) a platform and forum for posting photos, sharing and discussing their insights in respect of services or products related to food and beverage.
                            </li>
                            <li>Allow members of the Channels to enjoy member benefits by enrolling for special events hosted by Weeloy.
                            </li>
                            <li>Allow Weeloy to compile and analyze aggregate statistics about Users’ use of the Channels and service usage, for Weeloy’s internal use.
                            </li>
                            <li>Facilitate in the provision of services offered by Weeloy (and the participating establishment and media partners) including special events and/or promotions.
                            </li>
                        </ol>
                    </li>
                </ol>
            </li>
            <li><strong>Disclosure and Transfer of Personal Data</strong>
                <ol>
                    <li>Weeloy shall not sell Users' personal data to third parties. Weeloy shall only disclose Users' personal data to third parties where Users have (or deemed to have) provided consent and in the situations expressly set out in in this Privacy Policy. If Users consent to receiving marketing information from Weeloy, Weeloy's strategic partners and business associates, Weeloy shall disclose Users' personal data to such third parties for various purposes, including but not limited to:
                        <ol>
                            <li>providing products and services requested by Users, including providing all marketing and other information about products and services which may be of interests to Users; 
                            </li>
                            <li>allowing Weeloy's third party suppliers or external service providers working on Weeloy's behalf and providing services such as hosting and maintenance services, analysis services, e-mail messaging services, delivery services, handling of payment transactions, solvency check and address check, and/or facilitating/improving the quality of services on the Channels; and
                            </li>
                            <li>allowing Weeloy to comply with legal obligations or industry requirements, including disclosures to legal, regulatory, governmental, tax and law enforcement authorities and Weeloy's professional advisers (e.g. accountants, lawyers, and auditors).
                            </li>
                        </ol>
                    </li>
                    <li>
                        Users fully understand and consent that Weeloy may transfer their personal data to any location outside of Singapore (or Users' country of domicile) for the purposes set out in this Privacy Policy. When transferring any personal data outside of Singapore, Weeloy shall protect Users' personal data to a standard comparable to the protection accorded to such personal data under the PDPA.
                    </li>
                </ol>
            </li>

            <li><strong>Newsletters and other Promotional Materials</strong>
                <ol>
                    <li>Users acknowledge that based on their personal data provided to Weeloy and unless they have expressly opted out, Weeloy (or its media partners) may from time to time send to Users newsletters, promotional materials and marketing materials on the following classes of services, products:
                        <ol>
                            <li>Food and beverage related products and services.
                            </li>
                            <li>Travel related products and services.
                            </li>
                            <li>Special events hosted by Weeloy for Users, including but not limited to courses, workshops, and competitions.
                            </li>
                            <li>Reward, loyalty or privileges programs and related products and services.
                            </li>
                            <li>Special offers including e-vouchers, coupons, discounts, group purchase offers and promotional campaigns.
                            </li>
                            <li>Products and services offered by Weeloy and its advertisers (the names of such entities can be found in the relevant advertisements and/or promotional or marketing materials for the relevant products and services, as the case may be).
                            </li>
                            <li>Donations and contributions for charitable and/or non-profit purposes.
                            </li>
                        </ol>
                    </li>
                    <li>Suitable measures are implemented to make available to Users the option to “opt-out” of receiving such materials. In this regard, Users may choose to sign up or unsubscribe for such materials by logging into the registration or user account maintenance webpage, or clicking on the automatic link appearing in each newsletter/message, or contact the Customer Service Representative of Weeloy at +65 62211016 or email to info@weeloy.com
                    </li>
                </ol>
            </li>
            <li><strong>Access to Personal Data</strong>
                <ol>
                    <li>All Users are entitled to access their personal data at Weeloy.com by signing in to make amendments or updates thereto, except that if a User wishes to change his/her account ID or has any question about the processing of his/her personal data or about this Privacy Policy, or does not accept the amended Privacy Policy, or  wishes to withdraw any consent that has been given to Weeloy, he/she must contact Weeloy Customer support at +65 62211016 or email to info@weeloy.com. Please note that Weeloy may be prevented by law from complying with any request that a User may have submitted hereunder. 
                    </li>
                    <li>Weeloy may also charge Users a fee for responding to their requests for access to their personal data as held by Weeloy. If a fee is to be charged, Weeloy shall inform the User of the amount beforehand and respond to the User's request after payment is received.
                    </li>

                </ol>
            </li>
            <li><strong>Cookies</strong>
                <ol>
                    <li>Weeloy does not collect any personally identifiable information from Users when they visit and browse the Channels, save and except where such information is expressly requested. When Users access the Channels, Weeloy records their visits only. The Channels’ server software records the domain name server address and track the pages the Users visit and store such information in “cookies”, and gathers and stores information like internet protocol (IP) addresses, browser type, referring/exit pages, operating system, date/time stamp, and clickstream data in log files. Weeloy and third-party vendors engaged by Weeloy use cookies (e.g. Google Analytics cookies) to inform, optimize and serve marketing materials based on Users’ past visits to the Channels. 
                    </li>
                    <li>Weeloy does not link the information and data automatically collected in the above manner to any personally identifiable information. Weeloy generally uses such automatically collected data to estimate the audience size of the Channels, gauge the popularity of various parts of the Channels, track Users’ movements and number of entries in Weeloy’s promotional activities and special events, measure Users’ traffic patterns and administer the Channels. Such automatically collected information and data shall not be disclosed save and except in accordance with this Privacy Policy.
                    </li>
                </ol>
            </li>
            <li><strong>Links to other Websites, Media Platforms and Applications</strong>
                <ol>
                    <li>The Channels may have the functionalities to allow Users to share with or disclose their personal data to third parties, and/or links to other websites, media platforms and applications of third parties not owned or controlled by Weeloy. Personal data from Users may be collected on these other websites, media platforms, applications when Users visit such websites, media platforms and applications and make use of the services provided therein. 
                    </li>
                    <li>Where and when Users decide to click on any advertisement or hyperlink on the Channels which grant Users access to another website, media platform and application, the protection of Users’ personal information and data which are deemed to be private and confidential may be exposed in these other websites, media platforms and applications. 
                    </li>
                    <li>Non-registered Users who gain access to the Channels via their accounts in online social networking tools (including but not limited to Facebook) are deemed to have consented to the terms of this Privacy Policy, and such Users’ personal data which they have provided to those networking tools may be obtained by Weeloy and be used by Weeloy and its authorized persons in and outside of the User’s country of domicile. 
                    </li>
                    <li>Users are responsible for their choice(s) and are deemed to have provided consent for any sharing of their personal data in the manner provided on or by the Channels.
                    </li>
                </ol>
            </li>
            <li><strong>Security of Personal Data</strong>
                <ol>
                    <li>The security of Users’ personal information and data is important to Weeloy. Weeloy shall use best efforts to ensure that Users’ personal data shall be protected against unauthorized access. Weeloy has implemented appropriate electronic and managerial measures in order to safeguard, protect and secure Users’ personal data. Users are reminded to safeguard his/her unique Username and Password by keeping it secret and confidential.
                    </li>
                    <li>Weeloy uses third party payment gateway service providers to facilitate electronic transactions on the Channels. Regarding sensitive information provided by Users, such as credit card number for completing any electronic transactions, the web browser and third party payment gateway communicate such information using secure socket layer technology (SSL). Weeloy follows generally accepted industry standards to protect the personal data submitted by Users to the Channels, both during transmission and once Weeloy receives it. However, no method of transmission over the Internet, or method of electronic storage, is 100% secure. Therefore, while Weeloy strives to protect Users’ personal data against unauthorized access, Weeloy cannot guarantee its absolute security.
                    </li>
                </ol>
            </li>
            <li><strong>Retention of Personal Data</strong>
                <ol>
                    <li>Weeloy has put in place measures such that Users' personal data in Weeloy's possession or control shall be destroyed and/or anonymised as soon as it is reasonable to assume that the purposes for which Users' personal data were collected are no longer being served by the retention thereof, and/or that retention thereof is no longer necessary for any other legal or business purposes.
                    </li>
                </ol>
            </li>
            <li><strong>Changes to this Privacy Policy</strong>
                <ol>
                    <li>Weeloy reserves the right to update, revise, modify or amend this Privacy Policy in the following manner at any time as Weeloy deems necessary. Users are strongly recommended to review this Privacy Policy frequently. If Weeloy decides to update, revise, modify or amend this Privacy Policy, Weeloy shall post those changes to this webpage and/or other places Weeloy deems appropriate so that Users would be aware of what information Weeloy collects, how Weeloy uses it, and under what circumstances, if any, Weeloy discloses it. If Weeloy makes material changes to this Privacy Policy, Weeloy shall notify Users on this webpage, by email, or by means of a notice on the home page of Weeloy.
                    </li>
                </ol>
            </li>
            <li><strong> Governing Law</strong>
                <ol>
                    <li>This Privacy Policy is governed by the laws of Singapore. All Users and Weeloy agree to submit to the exclusive jurisdiction of the  Singapore courts in any dispute relating to this Privacy Policy.
                    </li>
                </ol>
            </li>
        </div>

        <br>
        <br>
        <br>
        <br>
        <!-- /modal-footer -->
    </ol>

<div class="modal-footer">
	<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
</div>			<!-- /modal-footer -->
</body>
</html>
