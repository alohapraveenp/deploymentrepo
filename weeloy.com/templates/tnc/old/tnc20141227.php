
<style>
body {
font-family: Roboto; 
font-size: 12px;
}

ol { counter-reset:section; list-style-type:none; }
ol li { list-style-type:none; }
ol li ol { counter-reset:subsection; }
ol li ol li ol { counter-reset:subsubsection; }
ol li:before{
    counter-increment:section;
    content:counter(section) ". ";/*content:"Section " counter(section) ". ";*/
    font-weight:bold;
}
ol li ol li:before {
    counter-increment:subsection;
    content:counter(section) "." counter(subsection) " ";
}

ol li ol li ol li:before {
    counter-increment:subsubsection;
    content:counter(section) "." counter(subsection) "." counter(subsubsection);
}

</style>
    
<p align="center"><strong>Weeloy Dining MEMBERSHIP PROGRAM</strong></p>

<p><strong>TERMS AND CONDITIONS</strong></p>

<ol>
	<li><strong>Membership Agreement</strong>
	<ol>
		<li value="1.1">This Agreement is between you, the Member and Weeloy Pte. Ltd. , the operator of &nbsp;Weeloy Dining Program, and states the terms and conditions governing participation in and use of the &nbsp;Weeloy Dining Membership Program benefits and privileges which are binding on you.</li><p style='line-height:5px;'>
		<li value="1.2">These terms &amp; conditions shall be applicable and binding on all types of&nbsp; Weeloy Dining &nbsp;Membership programs, including any co-branding arrangements with our respective partners. The &nbsp;Weeloy Dining Membership Program is an e-marketing program whereby all members are issued a&nbsp; ID, which presented at participating Merchants, which have entered into a Participation Agreement with WEELOY.</li><p style='line-height:5px;'>
	</ol>
	</li><p style='line-height:5px;'>
<p>&nbsp;</p>
	<li><strong>Definitions</strong>
	<ol>
		<li value="2.1">To simplify this Agreement for you, the following definitions will apply:</li><p style='line-height:5px;'><p style='line-height:5px;'>
		<li value="2.2">&quot;Account&quot; - your online&nbsp; Weeloy Dining account containing your profile details, history of your visits to participating&nbsp; Weeloy Dining Merchants and status of your dining e-vouchers purchased.</li><p style='line-height:5px;'>
		<li value="2.3">&quot;WEELOY&rdquo; owner and operator of the &nbsp;Weeloy Dining Program</li><p style='line-height:5px;'>
		<li value="2.4">&quot; Weeloy Dining Website&quot; -&nbsp;<a href="http://www.weeloy.com">www.weeloy.com</a> hosted by WEELOY</li><p style='line-height:5px;'>
		<li value="2.5">&quot; Weeloy Dining Membership&quot; or &quot;Membership&quot; - the membership that you obtain when you join the &nbsp;Weeloy Dining Program</li><p style='line-height:5px;'>
		<li value="2.6">&ldquo;WeeloyBenefits Wheel&rdquo; &ndash; WEELOY&rsquo;s proprietary wheels, which are an integral part of the&nbsp; Weeloy Dining Program. The &ldquo;Standard Weeloy Benefits Wheel&rdquo; is based on a pre-defined range from 10%, 15%, 20% and 25% off total food bill. While, the &ldquo;Customized Weeloy Benefits Wheel&rdquo; enables the Merchant to determine their own promotional offers to be displayed on its wheel, which may be changed from time to time at their discretion.</li><p style='line-height:5px;'>
		<li value="2.7">&quot;Member Dines Free e-voucher&quot; &ndash; e-voucher which Member may purchase from WEELOY entitling Member to a pro-rata &ldquo;free&rdquo; meal portion when dining at a participating&nbsp; Weeloy Dining Merchant under specific terms and conditions imposed by the respective Merchant.&nbsp; A minimum of two (2) persons dining together is required to redeem a Member Dines Free e-voucher. The Member&rsquo;s free meal portion is determined by dividing the total food bill (before applicable tax and service charge) by the number of persons in the party and the resulting about is the amount of the Member&rsquo;s free meal portion, which is deducted from the total bill.</li><p style='line-height:5px;'>
		<li value="2.8">&quot; Weeloy Dining Membership ID&quot; - the identification number assigned to you under the &nbsp;Weeloy Dining Program, which may be your mobile number registered with the&nbsp; Weeloy Dining Program.</li><p style='line-height:5px;'>
		<li value="2.9">&quot;Member&quot; - an individual who has personally signed up for the &nbsp;Weeloy Dining Membership and been issued an ID under the &nbsp;Weeloy Dining Program</li><p style='line-height:5px;'>
		<li value="2.1">&quot;Merchant&quot; - a Hotel, food and beverage establishment, or any other person, firm or corporation that has signed the&nbsp; Weeloy Dining Participation Agreement</li><p style='line-height:5px;'>
		<li value="2.11">&quot;Transactions&quot; - all transactions effected through the use of the &nbsp;Weeloy Dining ID under the Membership</li><p style='line-height:5px;'>
		<li value="2.12">&quot;www.weeloy.com&quot; &ndash; WEELOY&rsquo;s general website</li><p style='line-height:5px;'>
		<li value="2.13">&quot;You&quot;, &quot;your&quot; or &quot;yours&quot; - you, being the person applying for the &nbsp;Weeloy Dining Membership Program to become a Member</li><p style='line-height:5px;'>
	</ol>
	</li><p style='line-height:5px;'>
<p>&nbsp;</p>
	<li value="3"><strong>Introduction</strong>

<ol>
	<li>By signing up for Membership under the &nbsp;Weeloy Dining Program, you warrant and represent that:<br />
	All the personal information provided by you to us is true, correct and complete; you acknowledge and agree that in order for us to provide the services and privileges under the &nbsp;Weeloy Dining Program, to manage your Account, and to enable you to participate in the &nbsp;Weeloy Dining Program, the sharing, retrieval, updating and processing of your personal data as well as the creation of member profiles and user accounts and updating of your personal details will occur between WEELOY, our affiliates and any third party contractor in connection with the provision of the &nbsp;Weeloy Dining Program services and you have consented to the same; and that you have read, understood and accepted all the terms and conditions contained in this Agreement and in connection with your Membership under the &nbsp;Weeloy Dining Program, and they are binding on you.
	</li><p style='line-height:5px;'>
</ol>
</li><p style='line-height:5px;'>
<p>&nbsp;</p>
	<li value="4"><strong>Application</strong>

	<ol>
		<li value="4.1">The &nbsp;Weeloy Dining Membership Program is open to all customers globally. You can apply for Membership online at the Website.</li><p style='line-height:5px;'>
		<li value="4.2">Each Member is entitled to one (1) Membership with one email ID, and telephone number.</li><p style='line-height:5px;'>
		<li value="4.3">Whilst the application for the Membership under the &nbsp;Weeloy Dining Program is open to all customers, we reserve the right to reject ANY application and/or to decline any application for the Membership at our sole and absolute discretion without assigning any reasons whatsoever, and our decision shall be final and conclusive.</li><p style='line-height:5px;'>
	</ol>
	</li><p style='line-height:5px;'>

<p>&nbsp;</p>
	<li value="5"><strong>Validity</strong>
	
	<ol>
		<li>The &nbsp;Weeloy Dining Program Membership is a lifetime membership program.</li><p style='line-height:5px;'>
	</ol>
	</li><p style='line-height:5px;'>

<p>&nbsp;</p>
	<li value="6"><strong>Use of Membership and &nbsp;Weeloy Dining ID</strong>

	<ol>
		<li value="6.1">Once you receive your &nbsp;Weeloy Dining ID, you may use it to earn &nbsp;Weeloy Dining benefits at any merchant that has been signed up WEELOY under the &nbsp;Weeloy Dining Program for points issuance. The Membership and &nbsp;Weeloy Dining ID is non-transferable and shall only be used by you as the Member.&nbsp;<br />
		You should frequently review your Account to check and verify your current &nbsp;Weeloy Dining e-vouchers purchased, which have been redeemed and available for redemption and other promotional offers made available to you.</li><p style='line-height:5px;'>
		<li value="6.2">By subscribing for the Membership, you agree and undertake:
		<ol>
			<li value="6.1.1">&nbsp;not to use the &nbsp;Weeloy Dining ID for any unlawful and/or fraudulent activities; and</li><p style='line-height:5px;'>
			<li value="6.1.2">&nbsp;to only use the &nbsp;Weeloy Dining ID in accordance with the terms and conditions of this Agreement.</li><p style='line-height:5px;'>
			<li value="6.1.3">&nbsp;If we find that your &nbsp;Weeloy Dining ID has been used for unlawful or fraudulent activity or purposes which are unacceptable to us, we shall be entitled to suspend, block and/or terminate your Membership; including forfeiture of&nbsp; Weeloy Dining Member Dines Free e-vouchers purchased immediately without prior notice to you.</li><p style='line-height:5px;'>
		</ol>
	</ol>
	</li><p style='line-height:5px;'>
<p>&nbsp;</p>
	<li value="7"><strong>Member</strong>
	<ol>
		<li value="7.1">The Member is the individual who personally applies for the Membership under the&nbsp; Weeloy Dining Membership Program and has been issued a &nbsp;Weeloy Dining ID. As a Member, you will be able to enjoy benefits at participating Merchants. As a Member, when patronizing a participating Merchant, you will be entitled to either spin the &nbsp;Weeloy Dining Benefits Wheel or redeem a &ldquo;Member Dines Free&rdquo; e-voucher, which you may purchase online at the WEELOY website. Only one (1) &ldquo;Member Dines Free&rdquo; e-voucher may be redeemed at each participating Merchant; unless the Merchant at their sole discretion makes additional &ldquo;Member Dines Free&rdquo; e-vouchers available to the Member.</li><p style='line-height:5px;'>
		<li value="7.2">From time to time, WEELOY and its participating Merchants may make available other promotional offers.&nbsp;</li><p style='line-height:5px;'>
	</ol>
	</li><p style='line-height:5px;'>

<p>&nbsp;</p>
	<li value="8"><strong>Services and/or Products</strong>

	<ol>
		<li value="8.1">We may from time to time introduce new services, offers or programs in relation to your Membership under the &nbsp;Weeloy Dining Program that offer promotional items or other special terms as we may apply from time to time to a category of transactions. We will tell you of the terms and conditions of such services, offers or programs at the time of the introduction of the services, offers or programs (if any). The terms and conditions of such services, offers and programs are binding on you as if the same are contained here. More than one service(s), offer or program may apply to your Membership under the &nbsp;Weeloy Dining Program at the same time. We are entitled to introduce and terminate any such service, offer or programs from time to time at our absolute discretion.</li><p style='line-height:5px;'>
		<li value="8.2">Without prejudice to the above, we shall not be responsible for the refusal of any Merchant to honor or accept your &nbsp;Weeloy Dining ID.</li><p style='line-height:5px;'>
		<li value="8.3">The special offers stated on the &nbsp;Weeloy Dining benefits may vary from Merchant to Merchant. The full list of Merchants and &nbsp;Weeloy Dining benefits are available on the Website. The &nbsp;Weeloy Dining benefits are subject to change from time to time as determined by WEELOY. And, WEELOY reserves the right to add, remove, substitute or change Merchants and the &nbsp;Weeloy Dining benefits structure from time to time at our sole and absolute discretion.</li><p style='line-height:5px;'>
		<li value="8.4">You acknowledge that WEELOY assume no contractual or legal responsibility to you and have specifically limited their legal obligations in their arrangements with the respective under the &nbsp;Weeloy Dining Program. The accumulation of &nbsp;Weeloy Dining Member Dines Free e-vouchers does not entitle Member to any vested rights after the expiration date of said e-voucher. Members understand that they may not rely on the continued availability of any award, premium, or other benefit. All &nbsp;Weeloy Dining benefits and purchased Member Dines Free e-vouchers will be reflected in the Members Account summary statement. Members may view their &nbsp;Weeloy Dining benefits summary statement at the Website. Any disputes arising over the &nbsp;Weeloy Dining benefits summary statement must be notified to us within one (1) month from the date of the relevant statement. You must provide us with supporting documents wherever applicable to assist us in settling any disputes. If we do not receive any notifications from you within the stipulated time frame of any disputes or inaccuracies in your &nbsp;Weeloy Dining benefits summary statement, the statement will be taken to be correct, final and binding on the Member. WEELOY&#39;s decision on any such dispute is final and conclusive.</li><p style='line-height:5px;'>
	</ol>
	</li><p style='line-height:5px;'>
<p>&nbsp;</p>
	<li value="9"><strong>Redemption</strong>

	<ol>
		<li value="9.1">A Member with Membership account in good standing, who has purchased Member&nbsp; Dines Free e-vouchers is eligible to redeem their e-voucher at any participating Merchant upon verification of Member ID. At the Merchant&rsquo;s discretion, Merchant may require Member to present an identification card in order to redeem a Member Dines Free e-voucher. Only one (1) Member Dines Free e-voucher is applicable per Merchant and per table; except when Merchant at their sole discretion makes available to Member additional Member Dines Free e-vouchers. Other term and conditions applicable to the redemption of Member Dines Free e-vouchers shall be stated on the e-voucher.</li><p style='line-height:5px;'>
		<li value="9.2">Suspected or actual fraud and/or suspected or actual abuse relating to the use of &nbsp;Weeloy Dining benefits under the &nbsp;Weeloy Dining Program may result in forfeiture of purchased &nbsp;Weeloy Dining Member Dines Free e-vouchers, cancellation of the Member&#39;s &nbsp;Weeloy Dining ID as well as cancellation of a Membership and the Member&#39;s participation in the &nbsp;Weeloy Dining Program.</li><p style='line-height:5px;'>
		<li value="9.3">WEELOY shall not be responsible for any defect or deficiency in the goods or services supplied to you by any Merchant;</li><p style='line-height:5px;'>
		<li value="9.4">Any dispute concerning goods and services received as a benefit under the &nbsp;Weeloy Dining Program shall be settled between the Member and the Merchant, which provided the goods or services. WEELOY will bear no responsibility for resolving such disputes, or for the dispute itself;</li><p style='line-height:5px;'>
		<li value="9.5">The &nbsp;Weeloy Dining benefits including the member dines free e-vouchers have no cash or monetary value and cannot be transferred to another person, aside from the Member;</li><p style='line-height:5px;'>
		<li value="9.6">Any Member Dines Free e-voucher or other promotional offer redemption request made by the Member, once accepted by the Merchant cannot be revoked, cancelled, changed, returned or exchanged by the Member. All &nbsp;Weeloy Dining benefits consumed and e-vouchers redeemed will be captured and made available for review in the Member&#39;s account;</li><p style='line-height:5px;'>
		<li value="9.7">WEELOY gives no representation or warranty with respect to the quality of the products or services offered to the Member by the Merchant for any purpose;</li><p style='line-height:5px;'>
		<li value="9.8">WEELOY assumes no responsibility and shall not be held liable for any claims, loss, costs, expenses or damages of whatever nature resulting from the redemption of any of the e-vouchers by Member;</li><p style='line-height:5px;'>
		<li value="9.9">Information and promotional offers supplied by the Merchant to WEELOY for the &ldquo;Customized WeeLoy Benefit Wheel&rdquo; and e-vouchers for redemption under the &nbsp;Weeloy Dining Program are provided to WEELOY for its administrative, public relations and marketing purposes;</li><p style='line-height:5px;'>
		<li value="9.1">All benefits and e-voucher redemption are subject to specific terms and conditions applicable to the offer as WEELOY or Merchant may impose the restrictions accordingly;</li><p style='line-height:5px;'>
		<li value="9.11">Member Dines Free e-vouchers that have been purchased by the Member are not exchangeable for other vouchers, not refundable, not replaceable and not transferable for cash or credit under any circumstances;</li><p style='line-height:5px;'>
		<li value="9.12">WEELOY will not replace any expired e-voucher under any circumstances whatsoever;</li><p style='line-height:5px;'>
		<li value="9.13">WEELOY&#39;s failure to enforce particular terms and conditions regarding the &nbsp;Weeloy Dining benefits and/or under the &nbsp;Weeloy Dining Program does not constitute a waiver of those terms and conditions by WEELOY.</li><p style='line-height:5px;'>
		<li value="9.14">WEELOY reserves the right to change, substitute or terminate any of the Merchant&rsquo;s Member Dines Free e-voucher under the &nbsp;Weeloy Dining Program at any time with or without prior notice.</li><p style='line-height:5px;'>
	</ol>
	</li><p style='line-height:5px;'>
<p>&nbsp;</p>
	<li value="10"><strong>&nbsp;&nbsp;Unauthorized Use of &nbsp;Weeloy Dining ID</strong>

	<ol>
		<li value="10.1">In the event of unauthorized use of your &nbsp;Weeloy Dining ID, you should call our Customer Experience Center to notify us. We may block the redemption function of your purchased &nbsp;Weeloy Dining e-vouchers upon receipt of your notification. You should then send a confirmation in writing to us. We shall not be responsible or be held liable for any disputes or loss suffered by you in relation to any transactions incurred from unauthorized usage of your &nbsp;Weeloy Dining ID.</li><p style='line-height:5px;'>
		<li value="10.2">You shall be and remain liable for all transactions incurred from unauthorized usage of the &nbsp;Weeloy Dining ID, where investigations made by us reveal that you are a party to any actions regarding any transactions effected through the use of your &nbsp;Weeloy Dining ID by any unauthorized person. Without prejudice to our rights and notwithstanding that you may have exercised all reasonable precaution to prevent the unauthorized use of your &nbsp;Weeloy Dining ID, you shall remain liable to us if you have acted fraudulently.</li><p style='line-height:5px;'>
	</ol>
	</li><p style='line-height:5px;'>
<p>&nbsp;</p>
	<li value="11"><strong>Disclosure of Passwords, Unauthorized Access</strong>

	<ol>
		<li value="11.1">We encourage you update your profile and personal data, including but not limited to, mailing address, contact details, phone numbers, email address, and other personal particulars online at our Website.</li><p style='line-height:5px;'>
		<li value="11.2">We shall not be responsible and shall not be liable for any loss suffered or in relation to the &nbsp;Weeloy Dining benefits transactions on your Account, if you disclose any of your passwords, security codes, security questions, security answers or any of your personal details to unauthorized parties or third parties.</li><p style='line-height:5px;'>
	</ol>
	</li><p style='line-height:5px;'>
<p>&nbsp;</p>
	<li value="12"><strong>Fees and Charges</strong>

	<ol>
		<li value="12.1">By entering into this Agreement, you agree and undertake to pay all fees arising out of and in connection with the &nbsp;Weeloy Dining Program, where and if applicable. We reserve the right to revise such fees or to charge additional fees (if applicable) for new services (which are optional) which may be offered by us from time to time.</li><p style='line-height:5px;'>
		<li value="12.2">All fees paid to or in connection with the &nbsp;Weeloy Dining Program to us are non-refundable under any circumstances whatsoever. For full details of any applicable fees, please refer to the Website.</li><p style='line-height:5px;'>
	</ol>
	</li><p style='line-height:5px;'>
<p>&nbsp;</p>
	<li value="13"><strong>Transaction History</strong>

	<ol>
<p>&nbsp;</p>
		<li value="13.1">A transaction history (i.e. Summary Statement) comprising of a statement detailing your Member Dines Free e-voucher purchases and redemptions, Merchant visits and benefits consumed and Merchant ratings and reviews shall be available online under your Account profile on the Website. The history account is ONLY accessible by the Member by using his/her own user ID, password (and security hint question where applicable). All entries in the transaction history are deemed true and accurate unless you inform us of any error, exception, dispute or unauthorized transaction within one (1) month from the date of the transaction. If we do not receive any written notification from you concerning any error in the transaction history within the stipulated timeframe, the summary statement shall be deemed true, complete and accurate, and you shall then be deemed to have accepted the entries in the transaction history made up to the date of the last entry in the transaction history as final and conclusive.&nbsp;</li><p style='line-height:5px;'>
	</ol>
	</li><p style='line-height:5px;'>
<p>&nbsp;</p>
	<li value="14"><strong>Communication</strong>

	<ol>
		<li value="14.1">We shall be entitled from time to time to contact you via electronic transmission &nbsp;(including, but not limited to email communication and mobile short messaging services (&quot;sms&quot;), or via telephone correspondence, and any other means of exchange communication in respect of, but not limited to, the confirmation of transactions, whether or not you transacted the same, status of your Membership, your Account, &nbsp;Weeloy Dining , and communication on promotions with regards the &nbsp;Weeloy Dining Program.</li><p style='line-height:5px;'>
		<li value="14.2">WEELOY shall also be entitled to send you offers, promotional and marketing material with regards the &nbsp;Weeloy Dining Program from time to time. Any telephone calls placed by us to you will not be deemed as unsolicited calls by you.</li><p style='line-height:5px;'>
		<li value="14.3">From time to time we may monitor and/or record telephone calls between you and us. You agree that the monitoring and/or recording may be done and that no additional notice to you or additional approval from you is required.</li><p style='line-height:5px;'>
	</ol>
	</li><p style='line-height:5px;'>
<p>&nbsp;</p>
	<li value="15"><strong>Events of Default</strong>

	<ol>
		<li value="15.1">We may suspend, block, revoke and/or or terminate your Membership, your participation in the &nbsp;Weeloy Dining Program, your &nbsp;Weeloy Dining ID and/or any of the services offered (including the forfeiture of purchased &nbsp;Weeloy Dining benefits) with or without prior notice notwithstanding that we may have waived our rights on some previous occasion upon the occurrences of any or more of the following events, including but not limited to:-&nbsp;<br />
		if you use your Membership and &nbsp;Weeloy Dining ID for any unlawful activities; or if you commit or threaten to commit a default or breach of any terms or conditions set out in this Agreement; or if you have furnished false or misleading information or data to us, including false information concerning your personal information and details; or if in our absolute opinion, your Account has not been operated satisfactorily or used for fraudulent purposes; or if we have noticed irregular, suspicious or unauthorized activity in relation to accumulation of &nbsp;Weeloy Dining benefits; or if an event or events has or have occurred or a situation exists which should or might in our opinion prejudice your ability to perform your obligations under this Agreement; or if we in our absolute discretion decide to cancel or terminate the use of your Membership, your participation in the &nbsp;Weeloy Dining Program, your &nbsp;Weeloy Dining ID, or if there has been suspected or actual fraud and/or suspected or actual abuse relating to the accumulation of &nbsp;Weeloy Dining benefits by you.</li><p style='line-height:5px;'>
		<li value="15.2">In such circumstances, any purchased &nbsp;Weeloy Dining benefits shall be forfeited and we shall cancel your Membership, your &nbsp;Weeloy Dining ID and, your participation in the &nbsp;Weeloy Dining Program.</li><p style='line-height:5px;'>
		<li value="15.3">We reserve the right to take any action against you as may be deemed necessary or as may be required under governing laws or by the relevant governing authorities.</li><p style='line-height:5px;'>
		<li value="15.4">We reserve all rights to decline or terminate your application for Membership under the &nbsp;Weeloy Dining Program at our sole discretion without any prior notice to you if we find that you have furnished false or misleading information to us concerning your personal information and details.</li><p style='line-height:5px;'>
	</ol>
	</li><p style='line-height:5px;'>
<p>&nbsp;</p>
	<li value="16"><strong>Voluntary Cancellation of Membership</strong>

	<ol>
		<li value="16.1">You may at any time of your own accord cancel your Membership by sending a notice in writing via email or mobile notification to us. We will then terminate your Membership and your participation under the &nbsp;Weeloy Dining Program, and your &nbsp;Weeloy Dining ID will no longer be valid for use.</li><p style='line-height:5px;'>
		<li value="16.2">Upon your request to cancel your Membership, you will have to redeem the balance of purchased Member Dines Free e-vouchers that you have in your Account within the stipulated time period as determined by us. If you don&#39;t redeem your balance of &nbsp;Weeloy Dining e-vouchers by the stipulated time period, your purchased &nbsp;Weeloy Dining e-vouchers will be forfeited.</li><p style='line-height:5px;'>
	</ol>
	</li><p style='line-height:5px;'>
<p>&nbsp;</p>
	<li value="17"><strong>Variation of Terms</strong>

	<ol>
		<li value="17.1">We may at any time vary, revise, amend, withdraw, substitute, add or remove any of the terms and conditions of this Agreement. The revised or new terms will apply, including, without limitation, to all future transactions using the Membership and the &nbsp;Weeloy Dining ID and redemptions of &nbsp;Weeloy Dining e-vouchers under the &nbsp;Weeloy Dining Program.</li><p style='line-height:5px;'>
		<li value="17.2">We may issue a notice concerning any variation, revision or change of the terms or post the same on the Website. Notwithstanding the foregoing, advance notice of any change may not be given if it is necessary to make any such change immediately. Any amendments, alterations, additions, withdrawals and/or termination changes may affect &nbsp;Weeloy Dining benefits or e-vouchers which a Member has already purchased as well as any future &nbsp;Weeloy Dining benefits.</li><p style='line-height:5px;'>
		<li value="17.3">The retention or use of your Membership or &nbsp;Weeloy Dining ID or redemption of&nbsp; Weeloy Dining e-vouchers after the effective date of any variation, revision or change of terms and conditions shall be deemed to constitute acceptance of such variation, revision or change without reservation by you.</li><p style='line-height:5px;'>
		<li value="17.4">If you do not accept the proposed variation, revision or change, you are entitled to terminate the use of your Membership under the &nbsp;Weeloy Dining Program by contacting us, and the provisions relating to termination above shall apply.</li><p style='line-height:5px;'>
		<li value="17.5">Notwithstanding the above, WEELOY specifically reserve the right to withdraw or terminate the Membership and the &nbsp;Weeloy Dining Program (in whole or in part), any program or benefit thereunder at any time with or without prior notice.</li><p style='line-height:5px;'>
		<li value="17.6">We advise you to&nbsp;check the terms and conditions regularly&nbsp;whenever you visit the Website to ensure that you are aware of our latest terms and conditions.</li><p style='line-height:5px;'>
	</ol>
	</li><p style='line-height:5px;'>
<p>&nbsp;</p>
	<li value="18"><strong>Privacy policy</strong>

	<ol>
		<li value="18.1">We will not share or disclose your information except as described in this privacy policy.</li><p style='line-height:5px;'>
		<li value="18.2">In order to &nbsp;offer you the best services and support, we use &nbsp;and share your personal information (including details related to your Membership, Account, &nbsp;Weeloy Dining ID, &nbsp;Weeloy Dining benefits and any details under the &nbsp;Weeloy Dining Program) to WEELOY,&nbsp; with our related corporations, associates, subsidiaries, affiliates, assignees, proposed assignees, agents, commercial partners, and service providers.</li><p style='line-height:5px;'>
		<li value="18.3">in order for the receiving and/or processing of Member benefits to Merchants and other parties involved in the &nbsp;Weeloy Dining Program and providing services in relation to the same, you further consent to disclosure of your Membership details and &nbsp;Weeloy Dining ID (and any other information which may be necessary to facilitate the authentication and/or verification of Member status .</li><p style='line-height:5px;'>
		<li value="18.4">In order to comply with any regulatory authority or governing body established by Singapore or any other authority having jurisdiction over us, we shall also be entitled to disclose and you irrevocably consent to our disclosure of any information pertaining to you, your Membership, your Account, your &nbsp;Weeloy Dining ID, your &nbsp;Weeloy Dining benefits and any other information relating to you under the &nbsp;Weeloy Dining Program.</li><p style='line-height:5px;'>
		<li value="18.5">In particular, you further understand and acknowledge that we are entitled and need to share and disclose your information to WEELOY for the purposes of the &nbsp;Weeloy Dining Program. If we do not have your personal information or you choose not to disclose or share any information that we may require, we may not be able to provide or continue to provide the services and benefits under the &nbsp;Weeloy Dining Program to you, and we will not be obliged, responsible or held liable for the failure to provide the same (and/or any disputes in relation thereto).</li><p style='line-height:5px;'>
		<li value="18.6">Any disclosure by us of the information referred to in this provision will not render us liable to you for any claim, loss, damage (including direct or indirect damages or loss of profits or savings) or liability howsoever arising whether in contract, negligence, or any other basis arising from or in relation to: the release or disclosure of the information by us; and/or the information being incorrect, erroneous or misstated; and/or reliance on the information, whether caused by us or other third party&#39;s omission, misstatement, negligence or default or by technical, hardware or software failures of any kind, interruption, error, omission, viruses, delay in updating or notifying any changes in the information or otherwise howsoever.</li><p style='line-height:5px;'>
	</ol>
	</li><p style='line-height:5px;'>
<p>&nbsp;</p>
	<li value="19"><strong>Costs and Expenses</strong>
<ol>

<li>You hereby agree that if we take legal proceedings against you because of a default in the terms of this Agreement, you shall be liable to pay to us all legal costs (including costs on a solicitor and client basis), charges and expenses which we may incur in enforcing or seeking to enforce any of the provisions of this Agreement against you.</li><p style='line-height:5px;'>

	</ol>
	</li><p style='line-height:5px;'>
<p>&nbsp;</p>
	<li value="20"><strong>Indemnity</strong>
<ol>
<li>You shall hold us harmless and indemnify us against any liability for loss, damage, costs and expenses (legal or otherwise including all costs on a solicitor and client basis) which we may incur by reason of the provisions herein or in the enforcement of our rights hereunder.</li><p style='line-height:5px;'>
	</ol>
	</li><p style='line-height:5px;'>
<p>&nbsp;</p>
	<li value="21"><strong>Service </strong>

	<ol>
		<li value="21.1">You hereby agree that if any action is begun in the courts in Singapore in respect of this Agreement, the legal process and other documents may be served by sending the documents to you at your last known and registered email address and/or mobile number via SMS in our records and such service shall be deemed to be good and sufficient service of such process or documents.</li><p style='line-height:5px;'>
		<li value="21.2">Any notice or other document to be given under this Agreement and all other communications by you to us with respect the legal process shall be in writing and may be given or sent by hand, or registered post to us at the address set out below or by email to <a href="mailto:support@weeloy.com">support@weeloy.com</a>&nbsp; or By Mail or Hand To:</li><p style='line-height:5px;'>
	</ol>
	</li><p style='line-height:5px;'>

<p style="margin-left:187.2pt;">Weeloy Pte LTD</p>
<p style="margin-left:187.2pt;">9 Temasek Boulevard</p>
<p style="margin-left:187.2pt;">#09-01 Suntec Tower Two</p>
<p style="margin-left:187.2pt;">Singapore 038989</p>
<p style="margin-left:187.2pt;">Telephone: 65 9651 7408</p>
<p style="margin-left:4.0cm;">&nbsp;</p>
<p style="margin-left:36.0pt;">&nbsp;</p>

<p>&nbsp;</p>
	<li value="22"><strong>Waiver</strong>
<ol>

<li>Our acceptance of any terms or any waiver by us of our rights or any indulgence granted to you shall not operate to prevent us from enforcing any of our rights hereunder this Agreement nor shall such acceptance operate as consent to the modification of any of the terms of this Agreement in any respect. We may, at our discretion from time to time without notice, waive our rights under this Agreement in certain circumstances. We can waive our rights without affecting our other rights. If we waive any right, we do not waive the same in other circumstances.&nbsp;<br />
The rights and remedies provided in this Agreement are cumulative and not exclusive of any rights or remedies provided by law.</li><p style='line-height:5px;'>

	</ol>
	</li><p style='line-height:5px;'>
<p>&nbsp;</p>
	<li value="23"><strong>Assignment</strong>
<ol>

<li>We shall be entitled at any time without your consent to assign the whole or any part of our rights or obligations under this Agreement with or without notice to you. This Agreement, Membership and &nbsp;Weeloy Dining ID shall not be assigned by you to any other third party without our written consent.</li><p style='line-height:5px;'>
	</ol>
	</li><p style='line-height:5px;'>
<p>&nbsp;</p>
	<li value="24"><strong>Severability of Provisions</strong>
<ol>

<li>If any of the provisions of this Agreement becomes or is determined to be invalid, illegal or unenforceable in any respect under any law, rule or regulation, the remaining terms of this Agreement shall not be affected, and all other provisions of this Agreement will still be valid and enforceable and this Agreement shall be interpreted as if the invalid terms had not been included in this Agreement.</li><p style='line-height:5px;'>

	</ol>
	</li><p style='line-height:5px;'>
<p>&nbsp;</p>
	<li value="25"><strong>Governing Law and Jurisdiction</strong>
	<ol>

<li>This Agreement is a contract made under the laws of Singapore and shall be governed by and construed in accordance with the laws of Singapore. You consent and agree to submit to the exclusive jurisdiction of the Courts of Singapore concerning all matters arising out of or in connection with your use of the Membership under this Agreement.</li><p style='line-height:5px;'>
	</ol>
	</li><p style='line-height:5px;'>
<p>&nbsp;</p>
	<li value="26"><strong>Notification of Change</strong>	
	<ol>

<li>You should always ensure that you provide us with your most updated and current personal particulars and information concerning your electronic mail address , mobile number, correspondence/permanent address and other details in order to ensure that your records with us are kept up to date, complete and accurate. If any information supplied by you changes during the course of your Membership with us, you shall promptly update and edit your profile at Member&rsquo;s website.</li><p style='line-height:5px;'>
	</li><p style='line-height:5px;'>
<p>&nbsp;</p>
	<li value="26.1">Failure to do so may result in information requested by you via email regarding the &nbsp;Weeloy Dining Program being sent to the wrong person or wrong address. Any notice given by us shall be deemed given to you if sent by electronic mail to the last known email address and/or SMS to your mobile number last registered by you and on record with us.</li><p style='line-height:5px;'>
	</ol>
	</li><p style='line-height:5px;'>

<p>&nbsp;</p>
	<li value="27"><strong>Conflict Between English Text And Other</strong>	
	<ol>
	<li>If there is any conflict or discrepancy between the English text of terms and conditions of this Agreement and any translation thereof, the English text shall prevail.</li><p style='line-height:5px;'>
	</ol>
	</li><p style='line-height:5px;'>

<p>&nbsp;</p>
	<li value="28"><strong>Use of www.weeloy.com Website</strong>

	<ol>

<li>Online Terms and Conditions. In addition to the foregoing terms and conditions, when you use our&nbsp;<a href="http://www.weeloy.com">www.weeloy.com</a> Website, the terms and conditions governing the use of the Website shall apply in addition to and not in substitution for any terms and conditions contained in this Agreement.</li><p style='line-height:5px;'>

	</ol>
	</li><p style='line-height:5px;'>
	<li value="29"><strong>Children Policy </strong></li>

	<li>We do not solicit any personal information from children. If you are not 18 or older, you are not authorized to use the Site. Parents should be aware that there are parental control tools available online that you can use to prevent your children from submitting information online without parental permission or from accessing material that is harmful to minors.</li><p style='line-height:5px;'>
</ol>

<p style="margin-left:18.0pt;">&nbsp;</p>

<p style="margin-left:61.2pt;">Weeloy Pte Ltd reserves the right to amend any of the policies above.</p>

<p style="margin-left:51.05pt;">&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>



