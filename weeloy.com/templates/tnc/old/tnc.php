<style>

    body {
        margin: 0 40px 0 40px;
        font-family: Roboto; 
        font-size: 14px;
    }

    p {
        margin: 30px 0 10px 0;
        text-justify:inter-word;
        text-align: justify;
    }

    li {
        margin: 20px 0 0 0;
        text-justify:inter-word;
        text-align: justify;
    }

    ol {
        margin: 20px 0 0 0;
    }

    h5 {
        color:red;
        text-transform:uppercase;
        margin: 30px 0 10px 0;
    }

    #section {
        margin: 0 0 0 -40;
    }

    ol { counter-reset:section; list-style-type:none; }
    ol li { list-style-type:none; }
    ol li ol { counter-reset:subsection; }
    ol li ol li ol { counter-reset:subsubsection; }
    ol li:before{
        counter-increment:section;
        content:counter(section) ". ";/*content:"Section " counter(section) ". ";*/
        font-weight:bold;
    }
    ol li ol li:before {
        counter-increment:subsection;
        content:counter(section) "." counter(subsection) " ";
    }

    ol li ol li ol li:before {
        counter-increment:subsubsection;
        content:counter(section) "." counter(subsection) "." counter(subsubsection) " ";
    }

</style>
</head>
<body>

    <div class='container'>
        <h1 align='center' style='margin:10px 0 40px 0;'> Weeloy Privacy Policy</h1>

        <h4>Registration based on “Opt-In” process:</h4>

        <p>
            I consent for the above personal data to be provided to Weeloy to enjoy the use of Weeloy’s online restaurant system and table reservation service. And, I
            agree to receive the latest dining trends, promotional offers and updates from Weeloy and Weeloy participating merchants.
        </p>
        <p>
            Registration signifies that you agree to the terms and conditions of <span style='color:red'>Weeloy Privacy Policy</span> and <span style='color:red'>Terms and Conditions</span>.
        </p>
        <h5>Weeloy Privacy Policy</h5>

        <p>
            In compliance with requirements of the Personal Data Protection Act (PDPA) of Singapore, Weeloy Pte. Ltd. and/or affiliates including but not limited to
            establishments and media partners that have entered into an “Agreement” with Weeloy (hereinafter collectively referred to as “Weeloy” have adopted this
            Personal Data Privacy Policy described herein.
        </p>
        <p>
            In doing so, Weeloy will ensure compliance by its staff to the strictest standards of security and confidentiality in respect of all personal information
            and data submitted by Users using Weeloy.com, its sub-domains, any other websites, media platforms or applications including mobile applications
            operated/developed by Weeloy (hereinafter collectively referred to as the "Channels") and Weeloy will not release such information to any other party
            without the prior consent of the relevant User(s) of the Channels (whether registered or not) (“User(s)”) except to the authorized persons listed under
            paragraph 3 below.
        </p>
        <p>
            Users are strongly recommended to read this Privacy Policy carefully to have an understanding of Weeloy’s policy and practices with regard to the treatment
            of personal information and data provided by the Users on the Channels. This Privacy Policy is applicable to both registered and non-registered Users, and
            the provisions herein may be updated, revised, varied and/or amended from time to time as Weeloy deems necessary.
        </p>
        <p>
            If Users have questions or concerns regarding the Weeloy Privacy Policy, they should contact Weeloy's Customer Service Department at +65 6221 1016 or email
            to info@weeloy.com
        </p>

        <div id='section'>
            <ol>
                <li><strong>Collection of Personal Data - Purpose</strong>
                    <ol>
                        <li>In the course of using the Channels, Users may disclose or be asked to provide personal information and/or data. In order to benefit and enjoy various
                            services offered by the Channels, it may be necessary for Users to provide Weeloy with their personal information and/or data. Although Users are not
                            obliged to provide the information and/or data as requested on the Channels, Weeloy will not be able to provide certain services on the Channels in the
                            event that Users elect not to provide such data.</li>
                        <li>Weeloy’s purpose for collection of information and data on the Channels include but not limited to the following:

                            <ol>
                                <li>Daily operation of the services provided to Users;
                                </li>
                                <li>Identify Users who have posted advertisements, materials, messages, photos, views or comments or such other information (collectively “Information”) on
                                    the Channels;
                                </li>
                                <li>Identify Users who have viewed the information posted on the Channels;
                                </li>
                                <li>Provide Users with marketing and promotional materials for their enjoyment of benefits as members of the Channels (for further details, please refer to
                                    paragraph 4 headed “Subscription of Newsletter/Promotional Materials/Marketing Materials” below);
                                </li>
                                <li>Identify Users who have enjoyed their benefits as members of the Channels by receiving and using marketing and promotional materials;
                                </li>
                                <li>Provide Users with a platform and forum for posting photos, sharing and discussing their insights in respect of services or products related to food and
                                    beverage;
                                </li>
                                <li>Allow members of the Channels to enjoy their benefits as members by enrolling for special events hosted by Weeloy;
                                </li>
                                <li>Design and provide products and services to Users in relation to the above purposes;
                                </li>
                                <li>Compile and analyze aggregate statistics about the Users’ use of the Channels and service usage by the Users for Weeloy’s internal use; and
                                </li>
                                <li>Facilitate Weeloy to use the Users’ personal data for purposes relating to the provision of services offered by the Weeloy and marketing services,
                                    special events and/or promotions of the Weeloy and/or its clients.
                                </li>
                            </ol>
                        </li>
                        <li>If the User is under the age of 13, Weeloy strongly recommends him/her to seek prior consent from a person with parental responsibility for him/her,
                            e.g. parent or guardian, who may contact Weeloy at +65 6221 1016 or email to <a href="mailto:info@weeloy.com">info@weeloy.com</a> for registering the User
                            as member of the Channels.
                        </li>
                        <li>Weeloy strives to only collect personal data, which is necessary and adequate but not excessive in relation to the purposes set out hereinabove. In the
                            event, Weeloy requires the use of Users’ personal data for a purpose other than stated herein above, Weeloy may request the Users’ prescribed consent to
                            the same. If User is a minor, the prescribed consent should be given by his/her parent or guardian.
                        </li>
                    </ol>
                </li>
                <li><strong>Collection of Personal Data</strong>
                    <ol>
                        <li>Weeloy may collect personal information and/or data about a User such as his/her name, log-in ID and password, address, email address, phone number,
                            age, sex, date of birth, country of residence, nationality, education level and work experience that is/are not otherwise publicly available. Occasionally,
                            Weeloy may also collect additional personal information and/or data from a User in connection with contests, surveys, or special offers. Only duly
                            authorized staff of Weeloy will be permitted to access the Users’ personal information and data, and Weeloy shall not release such personal information and
                            data to any third parties save and except for the circumstances listed out under the Paragraph 3 entitled “Disclosure or Transfer of Data”
                        </li>
                    </ol>
                <li><strong>Transfer of Data - Disclosure</strong>
                    <ol>
                        <li>Weeloy agrees to take all practicable steps to keep all personal information and data of Users confidential and/or undisclosed, subject to the
                            following;</li>
                        <li>Weeloy will only disclose and/or transfer Users’ personal information and/or data to Weeloy’s personnel and staff for the purpose of providing services
                            to Users. However, Weeloy may disclose and/or transfer such information and/or data to third parties under the following circumstances:
                            <ol>
                                <li>where the information and/or data is disclosed and/or transferred to any third party suppliers or external service providers who have been duly
                                    authorized by the Weeloy to use such information and/or data and who will facilitate the services on the Channels, under a duty of confidentiality;
                                </li>
                                <li>where the information and/or data is disclosed and/or transferred to any agents, associates of the Weeloy who have been duly authorized by the Weeloy to
                                    use such information and/or data;
                                </li>
                                <li>where Weeloy needs to protect and defend its rights and property;
                                </li>
                                <li>where Weeloy considers necessary to do so in order to comply with the applicable laws and regulations, including without limitation compliance with a
                                    judicial proceeding, court order, or legal process served on the Channels; and
                                </li>
                                <li>where Weeloy deems necessary in order to maintain and improve the services on the Channels.
                            </ol>
                        </li>
                        <li>Personal data collected via the Channels may be transferred, stored and processed in any country in which Weeloy operates. By using the Channels, Users
                            are deemed to have agreed, consented to and authorized Weeloy to disclose and/or transfer their personal information and data under the circumstances
                            stated above, as well as to any transfer of information (including the Information) outside of the Users’ country.
                        </li>
                    </ol>
                </li>
                <li><strong>Subscription of Newsletter - Promotional and Materials</strong>
                    <ol>
                        <li>Weeloy may from time to time send to members and Users of the Channels newsletters, promotional materials and marketing materials based on the personal
                            information and data that they have provided to the Weeloy. Weeloy may use Users’ data in direct marketing and Weeloy require the Users’ consent (which
                            includes an indication of no objection) for that purpose.
                        </li>
                        <li>In this connection, please note that: Name, log-in ID and password, contact details, age, sex, date of birth, country of residence, nationality,
                            education level and work experience of Users held by the Weeloy from time to time may be used by Weeloy and/or its authorized personnel or staff in direct
                            marketing;
                        </li>
                        <li>Following classes of services, products and subjects may be marketed:
                            <ol>
                                <li>Food and beverage related products and services;
                                </li>
                                <li>Travel related products and services;
                                </li>
                                <li>Special events hosted by Weeloy for Members and Users, including but not limited to courses, workshops, and competitions;
                                </li>

                                Reward, loyalty or privileges programs and related products and services;

                                <li>Special offers including e-vouchers, coupons, discounts, group purchase offers and promotional campaigns;
                                </li>
                                <li>Products and services offered by Weeloy and its advertisers (the names of such entities can be found in the relevant advertisements and/or promotional
                                    or marketing materials for the relevant products and services, as the case may be);
                                </li>
                                <li>Donations and contributions for charitable and/or non-profit making purposes;
                                </li>
                            </ol>
                        </li>

                        <li>The above products, services and subjects may be provided or (in the case of donations and contributions) solicited by Weeloy and/or:
                            <ol>
                                <li>third party service providers providing the products, services and subjects listed in paragraph 4.c. above; and
                                </li>
                                <li>charitable or non-profit marking organizations;
                                </li>
                            </ol>
                        </li>
                        <li>In addition to marketing the above services, products and subject itself, Weeloy also intends to provide the data described in paragraph 4.a. above to
                            all or any of the persons described in paragraph 4.d above for use by them in marketing those services, products and subjects, and the Weeloy requires the
                            Users’ written consent (which includes an indication of no objection) for that purpose;
                        </li>
                        <li>Weeloy may receive money or other property in return for providing the data to the other persons in paragraph 4.d. above and, when requesting the Users’
                            written consent as described in paragraph 4.d. above, the Weeloy will inform the Users if the Weeloy receives any money or other property in return for
                            providing the data to the other persons.
                        </li>
                        <li>Suitable measures are implemented to make available to Members the options to “opt-out” of receiving such materials. In this regard, Users may choose to
                            sign up or unsubscribe for such materials by logging into the registration or user account maintenance webpage, or clicking on the automatic link appearing
                            in each newsletter/message, or contact the Customer Service Representative of the Weeloy at at +65 6221 1016 or email to    <a href="mailto:info@weeloy.com">info@weeloy.com</a>
                        </li>
                    </ol>
                </li>
                <li><strong>Access to personal account</strong>

                    <ol>
                        Any User is entitled to access their personal account at Weeloy.com by signing-in to make amendments to his/her password, own personal information and data
                        kept with Weeloy. In the event, User wishes to change his/her account ID; User must contact Weeloy Customer support at at +65 6221 1016 or email to    <a href="mailto:info@weeloy.com">info@weeloy.com</a> for assistance.
                    </ol>
                </li>
                <li><strong>Cookies - Log Files</strong>
                    <ol>
                        Weeloy does not collect any personally identifiable information from any Users when visiting and browsing the Channels, save and except where such
                        information of the Users is expressly requested. When Users access the Channels, Weeloy records their visits only and do not collect their personal
                        information or data. The Channels’ server software will also record the domain name server address and track the pages the Users visit and store such
                        information in “cookies”, and gather and store information like internet protocol (IP) addresses, browser type, referring/exit pages, operating system,
                        date/time stamp, and clickstream data in log files. All these are done without the Users being aware that they are occurring. Weeloy and third-party
                        vendors engaged by Weeloy use Weeloy’s cookies and third-party cookies, such as the Google Analytics cookies, together to inform, optimize, and serve
                        marketing materials based on the Users’ past visits to the Channels. Weeloy does not link the information and data automatically collected in the above
                        manner to any personally identifiable information. Weeloygenerally uses such automatically collected information and data to estimate the audience size of
                        the Channels, gauge the popularity of various parts of the Channels, track Users’ movements and number of entries in Weeloy’s promotional activities and
                        special events, measure Users’ traffic patterns and administer the Channels. Such automatically collected information and data will not be disclosed save
                        and except in accordance with the Paragraph 3 entitled “Disclosure or Transfer of Data”.
                    </ol>
                </li>
                <li><strong>Links to Other Websites - Media Platforms - Applications</strong>
                    <ol>
                        The Channels may provide links to other websites, media platforms and applications, which are not owned or controlled by Weeloy. Personal information and
                        data from Users may be collected on these other websites, media platforms, applications when Users visit such websites, media platforms and applications
                        and make use of the services provided therein. Where and when Users decide to click on any advertisement or hyperlink on the Channels which grant Users
                        access to another website, media platform and application, the protection of Users’ personal information and data which are deemed to be private and
                        confidential may be exposed in these other websites, media platforms and applications.   Non-registered Users who gain access to the Channels via their
                        accounts in online social networking tools (including but not limited to Facebook) are deemed to have consented to the terms of this Privacy Policy, and
                        such Users’ personal data which they have provided to those networking tools may be obtained by the Weeloy and be used by the Weeloy and its authorized
                        persons in and outside of the User’s country for the purpose of providing services and marketing materials to the Users. These Users are deemed to have
                        consented to the Weeloy and its authorized personnel’s access and use of their personal data so obtained, subject to the other provisions of the Privacy
                        Policy. This Privacy Policy is only applicable to the Channels. Users are reminded that this Privacy Policy grants no protection to Users’ personal
                        information and data that may be exposed on websites / media platforms / applications other than the Channels, and Weeloy is not responsible for the
                        privacy practices of such other websites, media platforms and applications. Users are strongly recommended to refer to the privacy policy of such other
                        websites, media platforms and applications.
                    </ol>
                </li>
                <li><strong>Security of Users’ personal information and data</strong>
                    <ol>
                        <li>The security of Users’ personal information and data is important to Weeloy. Weeloy will always strive to ensure that Users’ personal information and
                            data will be protected against unauthorized access. Weeloy has implemented appropriate electronic and managerial measures in order to safeguard, protect
                            and secure Users’ personal information and data.   All personal information and data provided by Users are only accessible by the authorized personnel of
                            Weeloy or its authorized third parties, and such personnel shall be instructed to observe the terms of this Privacy Policy when accessing such personal
                            information and data. Registered Users are reminded to safeguard his/her unique Username and Password by keeping it secret and confidential.
                        </li>
                        <li>Weeloy uses third party payment gateway service providers to facilitate electronic transactions on the Channels. Regarding sensitive information
                            provided by Users, such as credit card number for completing any electronic transactions, the web browser and third party payment gateway communicate such
                            information using secure socket layer technology (SSL). Weeloy follows generally accepted industry standards to protect the personal information and data
                            submitted by Users to the Channels, both during transmission and once Weeloy receives it. However, no method of transmission over the Internet, or method
                            of electronic storage, is 100% secure. Therefore, while Weeloy strives to protect Users’ personal information and data against unauthorized access, the
                            Weeloy cannot guarantee its absolute security.
                        </li>
                    </ol>
                </li>
                <li><strong>Retention of Personal Data</strong>
                    <ol>
                        Once Weeloy has obtained a User’s personal information and/or data, it will be maintained securely in Weeloy’s system. Subject to legal requirements, the
                        personal information and/or data of Users will be retained by the Weeloy after deactivation of the relevant service until the User requests Weeloy in
                        writing to remove his/her own personal information and/or data from Weeloy's database or to terminate his/her use of the Channels.
                    </ol>
                </li>
                <li><strong>Changes to this Privacy Policy</strong>
                    <ol>
                        Weeloy reserves the right to update, revise, modify or amend this Privacy Policy in the following manner at any time as Weeloy deems necessary and Users
                        are strongly recommended to review this Privacy Policy frequently. If Weeloy decides to update, revise, modify or amend this Privacy Policy, Weeloy will
                        post those changes to this webpage and/or other places Weeloydeems appropriate so that Users would be aware of what information Weeloy collects, how Weeloy
                        uses it, and under what circumstances, if any, Weeloy discloses it. In the event, Weeloymakes material changes to this Privacy Policy, Weeloy will notify
                        Users on this webpage, by email, or by means of a notice on the home page of Weeloy.

                    </ol></li></ol>
        </div>

        <p>For any query, please contact our Customer Service Representative at at +65 6221 1016 or email to <a href="mailto:info@weeloy.com">info@weeloy.com</a></p>
        <br><br><br><br>
    </div>
