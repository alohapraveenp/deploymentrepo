<?php

	require_once("conf/conf.init.inc.php");
	require_once("lib/wpdo.inc.php");
        require_once 'lib/class.booking.inc.php';
        require_once 'lib/class.passbook.inc.php';
        require_once("lib/class.media.inc.php");

        //$booking_id = 'R_TheVx8MCEKKz';
        
        $params_encoded = filter_input(INPUT_GET, 'b', FILTER_SANITIZE_STRING);
        $params = explode( '|||' , base64_decode($params_encoded));

        
        
        $booking = new WY_Booking($params[0]);
        $booking->getBooking($params[0]);
        
        
        if($booking->email == $params[1]){
            
        
        //PASSBOOK
        
            // add passbook for all bookings on weeloy.com 
            
            $passbook = new WY_Passbook();
             $media = new WY_Media();
            $passbook->serialNumber = $booking->confirmation;
            $passbook->restaurant_title = $booking->restaurantinfo->title;
            
            $rdate_date_only = date("F j, Y", mktime(substr($booking->rtime, 0, 2), substr($booking->rtime, 3, 4), 0, intval(substr($booking->rdate, 5, 2)), intval(substr($booking->rdate, 8, 2)), intval(substr($booking->rdate, 0, 4))));
            $rdate_time_only = date("H:i", mktime(substr($booking->rtime, 0, 2), substr($booking->rtime, 3, 4), 0, intval(substr($booking->rdate, 5, 2)), intval(substr($booking->rdate, 8, 2)), intval(substr($booking->rdate, 0, 4))));
            $passbook->booking_date = $rdate_date_only;
            $passbook->booking_time = $rdate_time_only;
    
           
            $passbook->cover = $booking->cover;
            $passbook->address = $booking->restaurantinfo->address;
            $passbook->city = $booking->restaurantinfo->city;
            $passbook->zip = $booking->restaurantinfo->zip;
            $passbook->member_code = $booking->membCode;
            $passbook->qrcode_string = '{"membCode":"' . $booking->membCode . '"}';
            $passbook->status = $booking->status;

            if (!empty($booking->restaurantinfo->GPS)) {
                $location = explode(',', $booking->restaurantinfo->GPS);
                $passbook->longitude = $location[0];
                $passbook->latitude = $location[1];

                //  var_dump($passbook->latitude);
                //  var_dump($passbook->longitude);
            }
            $passbook->map = $booking->restaurantinfo->map;
             if (!isset($image_resto[$booking->restaurantinfo->restaurant])) {
                    $passbook->defImg = $media->getDefaultPicture($booking->restaurantinfo->restaurant);
                }
            //var_dump($passbook->map);        

            $res = $passbook->generatePassbook();
            
            $passbook->createPassbook(true);
        }
?>