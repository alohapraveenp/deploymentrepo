var subrealtimeio = function(channel, token, msg) {
	channel += "-" + window.location.hostname;
	var ortcClient = RealtimeMessaging.createClient();
	ortcClient.setClusterUrl('https://ortc-developers.realtime.co/server/ssl/2.1/');
	ortcClient.connect('5z10Qe', 'testToken');
	console.log('WAITING TO CONNECT', channel, token, msg);

	ortcClient.onConnected = function(ortc) {
		console.log('CONNECTED', channel, msg);
		if(typeof msg !== "string" || msg.length < 1)
			return;
		var myMessage = {
			data: msg.replace(/\'|\"/g, '`'),
			from: token,
			channel: channel
			};
		ortcClient.send(channel, JSON.stringify(myMessage));
		};
	return ortcClient;
};

