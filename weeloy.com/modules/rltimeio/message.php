<?

error_reporting(E_ALL);
session_start();
require('./ortc.php');
 
$URL = 'http://ortc-developers.realtime.co/server/2.1';
$AK = 'YOUR_APPLICATION_KEY';
$PK = 'YOUR_APPLICATION_PRIVATE_KEY';
$TK = 'dummyToken'; // token can be randomly generated
$CH = 'myChannel';
 
// Authenticating a token with write (w) permissions on myChannel
 
if( ! array_key_exists('ortc_token', $_SESSION) ){
    $_SESSION['ortc_token'] = $TK;
    $rt = new Realtime( $URL, $AK, $PK, $TK );
    $ttl = 180000;
    $result = $rt->auth(
        array(
            $CH => 'w'
        ),
        0,
        $ttl
    );
    print '<!-- auth status '.( $result ? 'ok' : 'fail' ).' -->\n';
}
 
// Send Hello World message
$result = $rt->send($CH, "Hello World", $response);
print '<!-- send status '.( $result ? 'ok' : 'fail' ).' -->\n';

?>