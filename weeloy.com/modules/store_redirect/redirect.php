<?php
require_once("conf/conf.init.inc.php");
require_once("lib/Browser.inc.php");

$browser = new Browser();

$platform = $browser->getPlatform();

if($platform == 'Android'){
    $redirection = 'https://play.google.com/store/apps/details?id=com.weeloy.client';
}else{
    $redirection = 'https://itunes.apple.com/app/id973030193';
}
header("Location: $redirection");