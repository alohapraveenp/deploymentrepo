var app = angular.module("myApp", []);
app.controller('MainController', function ($scope, $http) {

    $scope.tabletitle = [{'a': 'index', 'b': 'index'}, {'a': 'cdate', 'b': 'Create Date'},{'a': 'status', 'b': 'Status'}, {'a': 'lastname', 'b': 'Last Name'},  {'a': 'rdate', 'b': 'Booking Date'},{'a': 'rtime', 'b': 'Time'}];
    whenday = 30;
    tlocation = "" + location;
  
    if(tlocation.indexOf("report-HJdsLfhau7t32egdaPaqavbsaklgvdwaysxf") > -1){
        $scope.page = 'gourmand';
        $scope.bookingTitle = 'Gourmand Asia';
    }else if(tlocation.indexOf("report-saJds8dsaau7gr4egDfagDvfbgdsaLmdskII") > -1){
        $scope.page = 'marie-france';
        $scope.bookingTitle = 'Marie-France Asia';
    }
        
    $http.get("api/report/reworld/widget/" +  $scope.page).success(function (response) {
        
        $scope.rdata = response.data;
        
        
        $scope.gourmand_direct_bookings = [];
        $scope.gourmand_number_redirect = $scope.rdata.resultats.number_redirect;
        $scope.gourmand_indirect_bookings = [];
        $scope.custom_direct = false;
        $scope.custom_indirect = false;
        $scope.toggleCustom = function(booking_type) {
            if(booking_type === 'direct'){
                $scope.custom_direct = $scope.custom_direct === false ? true: false;
            }else{
                $scope.custom_indirect = $scope.custom_indirect === false ? true: false;
            }
        };
        for (i = 0; i < $scope.rdata.resultats.direct_bookings.length; i++) {
            			if($scope.rdata.resultats.indirect_bookings[i].restaurant == "SG_SG_R_TheFunKitchen")
				continue; 
			if($scope.rdata.resultats.indirect_bookings[i].restaurant == "SG_SG_R_TheOneKitchen")
				continue; 
			
			var fisrt = $scope.rdata.resultats.indirect_bookings[i].firstname.toLowerCase();
			var last = $scope.rdata.resultats.indirect_bookings[i].lastname.toLowerCase();
			var spec = $scope.rdata.resultats.indirect_bookings[i].specialrequest.toLowerCase();
            var dontcarefirst = ["jerome", "phil", "charlotte", "craig", "nicolas", "soraya", "richard", "diana", "test" ];
            var dontcarelast = [ "arbault", "bene", "donahue", "fong", "finck", "kefs", "kefs", "wong", "test" ];

            if(fisrt.indexOf(dontcarefirst) > -1 || last.indexOf(dontcarelast) > -1 ) || spec.indexOf("test") > -1)
				continue;
   
            var last = $scope.rdata.resultats.direct_bookings[i].lastname.toLowerCase();

            $scope.rdata.resultats.direct_bookings[i].index = i + 1;
            $scope.rdata.resultats.direct_bookings[i].cdate = $scope.rdata.resultats.direct_bookings[i].cdate.substr(0, 10);
            $scope.rdata.resultats.direct_bookings[i].rtime = $scope.rdata.resultats.direct_bookings[i].rtime.substr(0, 5);
            $scope.gourmand_direct_bookings.push($scope.rdata.resultats.direct_bookings[i]);
        }
        for (i = 0; i < $scope.rdata.resultats.indirect_bookings.length; i++) {
                      
			if($scope.rdata.resultats.indirect_bookings[i].restaurant == "SG_SG_R_TheFunKitchen")
				continue; 
			if($scope.rdata.resultats.indirect_bookings[i].restaurant == "SG_SG_R_TheOneKitchen")
				continue; 
                            
			var fisrt = $scope.rdata.resultats.indirect_bookings[i].firstname.toLowerCase();
			var last = $scope.rdata.resultats.indirect_bookings[i].lastname.toLowerCase();
			var spec = $scope.rdata.resultats.indirect_bookings[i].specialrequest.toLowerCase();
                        
            if(fisrt.indexOf(dontcarefirst) > -1 || last.indexOf(dontcarelast) > -1 ) || spec.indexOf("test") > -1)
				continue;
            
            var last = $scope.rdata.resultats.indirect_bookings[i].lastname.toLowerCase();

            $scope.rdata.resultats.indirect_bookings[i].index = i + 1;
            $scope.rdata.resultats.indirect_bookings[i].cdate = $scope.rdata.resultats.indirect_bookings[i].cdate.substr(0, 10);
            $scope.rdata.resultats.indirect_bookings[i].rtime = $scope.rdata.resultats.indirect_bookings[i].rtime.substr(0, 5);
            $scope.gourmand_indirect_bookings.push($scope.rdata.resultats.indirect_bookings[i]);
        }
        $scope.predicate = $scope.oldpredicate = "rdate";
        $scope.reverse = false;
        $scope.setContent("rdate");
    });

    $scope.reorder = function (item) {
        $scope.predicate = item;
        if ($scope.oldpredicate == item)
            $scope.reverse = !$scope.reverse;
        else
            $scope.reverse = (item != 'date') ? false : true;
        $scope.oldpredicate = item;
        $scope.setContent(item);
    };

    $scope.setContent = function (item) {
        titlecolor = ($scope.reverse) ? 'orange' : 'fuchsia';
        content = $('#' + item + "_span").text();
        $('th a span').css('color', 'black');
        $('#' + item + "_span").css('color', titlecolor);
        if ($scope.reverse)
            $('#' + item + "_span").html(content + "<i class='fa fa-caret-up'>");
        else
            $('#' + item + "_span").html(content + "<i class='fa fa-caret-down'>");
    };


});

app.directive('tbtitle', function () {
    return{
        restrict: 'AE',
        replace: true,
        template: '<a href="javascript:;" id="{{y.a}}" ng-click="reorder(y.a);"><span id="{{y.a}}_span">{{ y.b }}<i class="fa fa-caret-down"></span></a>',
        link: function (scope, elem, attrs) {
            elem.bind('click', function () {
                item = elem.attr('id');
                content = elem.text();
                //elem.css('color', scope.titlecolor);
                scope.$apply(function () {
                    scope.dummy = "white";
                });
            });
            elem.bind('mouseover', function () {
                elem.css('cursor', 'pointer');
            });
        }
    }
});
