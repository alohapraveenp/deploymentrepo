<?php

    require_once("conf/conf.init.inc.php");

    if (empty($_REQUEST["restaurant"])) {
        printf("Invalid request. Exiting");
        exit;
    }
    $restaurant = $_REQUEST["restaurant"];
    $url = __BASE_URL__;

?>


<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="author" content="weeloy. https://www.weeloy.com"/>
<meta name="copyright" content="weeloy. https://www.weeloy.com"/>  
<meta http-equiv='expires' content='mon, 1 jan 1998 05:00:00 gmt'/>
<meta http-equiv='pragma' content='no-cache'/>
<meta http-equiv='pragma' content='cache-control: max-age=0'/>
<meta http-equiv='last-modified' content='mon, 1 jan 1998 05:00:00 gmt'/>
<meta http-equiv='cache-control' content='no-cache, must-revalidate'/>
<meta name='robots' content='noindex, nofollow'/>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="HandheldFriendly" content="true" />
<title> Event Management</title>
<base href="<?php echo __ROOTDIR__; ?>/" />

<link href="client/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="client/bower_components/bootstrap/dist/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
<link href="client/bower_components/fontawesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="modules/booking/assets/css/css/bootstrap-select.css" rel="stylesheet" />
<link href="modules/booking/assets/css/css/bootstrap-social.css" rel="stylesheet" />
<link href="modules/booking/assets/css/css/famfamfam-flags.css" rel="stylesheet" />
<link href="modules/booking/assets/css/css/dropdown.css" rel="stylesheet" />
<link href="client/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />



<script type="text/javascript" src="client/bower_components/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>
<script type='text/javascript' src="client/bower_components/angular/angular.min.js"></script>
<script type="text/javascript" src="client/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="client/bower_components/ui-bootstrap-tpls-0.14.2.min.js"></script>
<script type="text/javascript" src="client/bower_components/moment/min/moment.min.js"></script>
<script type="text/javascript" src="client/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

<script type="text/javascript" src="js/ngStorage.min.js"></script>
<script type="text/javascript" src="js/mylocal.js"></script>
 <script> var app = angular.module("myApp", ['ui.bootstrap', 'ngLocale', 'ngStorage']);</script> 


<script type="text/javascript" src="client/app/models/Base.js"></script>
<script type="text/javascript" src="client/app/models/Restaurant.js"></script>
<script type="text/javascript" src="client/app/models/EventBooking.js"></script>
<style>
    #event-management {
        background: #F5F5F5;
    }
    .btn-creditcard{
        border-radius: 2px;
        box-shadow: 2px 2px 5px #bbb;
        color: #fff;
        font-size: 16px;
        padding: 4px 14px;
    }
    .btn-leftBottom-orange, .btn-leftBottom-orange-noborder {
        background: #FF872C;
        border: 1px solid #FF872C;
    }
</style>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="author" content="weeloy. https://www.weeloy.com"/>
        <meta name="copyright" content="weeloy. https://www.weeloy.com"/>  
        <meta http-equiv='expires' content='mon, 1 jan 1998 05:00:00 gmt'/>
        <meta http-equiv='pragma' content='no-cache'/>
        <meta http-equiv='pragma' content='cache-control: max-age=0'/>
        <meta http-equiv='last-modified' content='mon, 1 jan 1998 05:00:00 gmt'/>
        <meta http-equiv='cache-control' content='no-cache, must-revalidate'/>
        <meta name='robots' content='noindex, nofollow'/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <meta name="HandheldFriendly" content="true" />
        <title> Event Booking</title>
         <base href="<?php echo __ROOTDIR__; ?>/" />
        <link href="client/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="client/bower_components/bootstrap/dist/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
        <link href="client/bower_components/fontawesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="modules/booking/assets/css/css/bootstrap-select.css" rel="stylesheet" />
        <link href="modules/booking/assets/css/css/bootstrap-social.css" rel="stylesheet" />
        <link href="modules/booking/assets/css/css/famfamfam-flags.css" rel="stylesheet" />
        <link href="modules/booking/assets/css/css/dropdown.css" rel="stylesheet" />
        <link href="client/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />



        <script type="text/javascript" src="client/bower_components/jquery/dist/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui.js"></script>
        <script type='text/javascript' src="client/bower_components/angular/angular.min.js"></script>
        <script type="text/javascript" src="client/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="client/bower_components/ui-bootstrap-tpls-0.14.2.min.js"></script>
        <script type="text/javascript" src="client/bower_components/moment/min/moment.min.js"></script>
        <script type="text/javascript" src="client/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

        <script type="text/javascript" src="js/ngStorage.min.js"></script>
        <script type="text/javascript" src="js/mylocal.js"></script>
         <script> var app = angular.module("myApp", ['ui.bootstrap', 'ngLocale', 'ngStorage']);</script> 


        <script type="text/javascript" src="client/app/models/Base.js"></script>
        <script type="text/javascript" src="client/app/models/Restaurant.js"></script>
        <script type="text/javascript" src="client/app/models/EventBooking.js"></script>

        <style>
            body
            {
                width:60%;
                margin-left:auto;
                margin-right:auto;
            }
            #event-management {
                background: #F5F5F5;
            }
            .btn-creditcard{
                border-radius: 2px;
                box-shadow: 2px 2px 5px #bbb;
                color: #fff;
                font-size: 16px;
                padding: 4px 14px;
            }
            .btn-leftBottom-orange, .btn-leftBottom-orange-noborder {
                background: #FF872C;
                border: 1px solid #FF872C;
            }
            .error-msg {
                color:#af3030;
            }
            
            @media screen and (max-width: 768px) {
                    width:100%;
                    margin-left:auto;
                    margin-right:auto;
            }

            @media screen and (max-width: 500px) {
                width:100%;
                margin-left:auto;
                margin-right:auto;
            }
        </style>

      
    </head>
      
        <body ng-app="myApp">
        <section id='event-management'  ng-controller='PrivateEventController' ng-init="moduleName = 'privateevent';" style='margin-top:-80px;padding-top:60px; '> 
            <div id="checkout-info" class="event-booking">
                <div class="container" >

                    <div id="main-form" class="col-sm-12 col-md-8 col-lg-8" ng-class="{'col-sm-12 col-md-8 col-lg-8' :status !== 'confirmed','col-sm-12 col-md-8 col-lg-8' :status === 'confirmed'}" style="padding-bottom: 80px;">

                        <div id="event_header" class="text-center" ng-if="status === 'confirmed'">
                            <div class="img-logo">
                                <img ng-src="{{restaurant.getLogoImage()}}" />
                            </div>
                        </div>
                        <div ng-if="!event_id && !status">
                            <p class="checkout-header text-center">Event Request</p>
                            <div class="event-request-form">
                                <h1>
                                    Event Details</h1>
                                <form id="form-border" name="EventForm" ng-submit="SaveEventRequest(EventForm, EventObject)" novalidate>
                                    <div class='row'>
                                        <div class="form-group col-xs-12 col-sm12 col-md-6 col-lg-6">
                                            <label>Title*</label>
                                            <input type="text" class="form-control" name="title" ng-model="EventObject.title" required>
                                                <div class="error error-msg">
                                                    <span ng-show="EventForm.$submitted && EventForm.title.$error.required">Please enter event title</span>
                                                </div>
                                        </div>
                                        <div class="form-group col-xs-12 col-sm12 col-md-6 col-lg-6">
                                            <label>Name*</label>
                                            <input type="text" class="form-control" name="name" ng-model="EventObject.name" required>
                                                <div class="error error-msg ">
                                                    <span ng-show="EventForm.$submitted && EventForm.name.$error.required">Please enter your event name</span>
                                                </div>
                                        </div>
                                          <div class="form-group col-xs-12 col-sm12 col-md-6 col-lg-6">
                                            <label>Location*</label>
                                            <input type="text" class="form-control" name="lastname" ng-model="EventObject.location" required>
                                                <div class="error error-msg ">
                                                    <span ng-show="EventForm.$submitted && EventForm.location.$error.required">Please enter your event location</span>
                                                </div>
                                        </div>
                                          <div class="form-group col-xs-12 col-sm12 col-md-6 col-lg-6">
                                            <label>Max Pax*</label>
                                            <input type="text" class="form-control" name="lastname" ng-model="EventObject.maxpax" required>
                                                <div class="error error-msg ">
                                                    <span ng-show="EventForm.$submitted && EventForm.maxpax.$error.required">Please enter max pax</span>
                                                </div>
                                        </div>

                                    </div>
                                    <div class='row'>
                                        <div class="form-group col-xs-12 col-sm12 col-md-6 col-lg-6">
                                            <label>Start Date*</label>

                                            <input type="text" class="form-control" id="wtimepicker6" name="rdate"  ng-model="EventObject.rdate" jqdatepicker  required>        

                                                <div class="error error-msg">
                                                    <span ng-show="EventForm.$submitted && EventForm.rdate.$error.required">Please enter the booking date</span>
                                                </div>
                                        </div>
                                        <div class="form-group col-xs-12 col-sm12 col-md-6 col-lg-6">
                                            <label>
                                                End Date*</label>
                                            <input type="text" class="form-control" id="wtimepicker7" name="rtime" ng-model="EventObject.end" jtimedatepicker required>
                                                <div class="error error-msg">
                                                    <span ng-show="EventForm.$submitted && EventForm.end.$error.required">Please enter the booking time</span>
                                                </div>
                                        </div>
                                    </div>
                                    <div class='row'>
                                        <div class="form-group col-xs-12 col-sm12 col-md-6 col-lg-6">
                                            <label>Price*</label>
                                            <input type="text" class="form-control" name="lastname" ng-model="EventObject.price" required>
                                                <div class="error error-msg ">
                                                    <span ng-show="EventForm.$submitted && EventForm.price.$error.required">Please enter your event price</span>
                                                </div>
                                        </div>
                                        
                                          <div class="form-group col-xs-12 col-sm12 col-md-6 col-lg-6">
                                            <label>Terms & condition*</label>
                                            <input type="text" class="form-control" name="lastname" ng-model="EventObject.tnc" required>
                                                <div class="error error-msg ">
                                                    <span ng-show="EventForm.$submitted && EventForm.tnc.$error.required">Please enter your event tnc</span>
                                                </div>
                                        </div>
                                          <div class="form-group col-xs-12 col-sm12 col-md-6 col-lg-6">
                                            <label>Description</label>
                                            <textarea class="form-control remarks" ng-model="EventObject.description"></textarea>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="title col-xs-12 col-sm12 col-md-12 col-lg-12" style='padding-top:20px;'>
                                            <button type="submit" class="btn-creditcard btn-leftBottom-orange pull-right">Submit request</button>
                                        </div>
                                    </div>
                                   
                                </form>
                            </div>
                        </div>

                        
                    </div>
                </div>
            </div>
        </section>
        <script>
   
            app.controller('PrivateEventController', function ($scope, $http) {
                var RestaurantID = "<?php echo $restaurant ?>",
                    baseUrl ="<?php echo $url ?>";
           
                var EventObject = new EventBooking();
                    $scope.EventObject = EventObject;
                var API_URL = 'api/restaurantfullinfo/' + RestaurantID;
                $http.get(API_URL, {
                    cache: true,
                }).then(function (result) {
                    if (result.data.status === 1) {
                         //var restaurant = new Restaurant(result.data.data.restaurantinfo);
                        $scope.restaurant = new Restaurant(result.data.data.restaurantinfo);

                    }
                });
        
            $scope.SaveEventRequest = function (EventForm, EventObject) {
                
                EventObject.type ='private';
                EventObject.start = EventObject.rdate;
                EventObject.end = EventObject.end;
                
                EventObject.restaurant = RestaurantID;
                var eventManagerObject = new EventBooking(EventObject);
                eventManagerObject.setRestaurant(RestaurantID);

                 if (EventForm.$invalid) {
                    var top;
                    if (EventForm.name.$invalid || EventForm.title.$invalid || EventForm.description.$invalid) {
                        top = $('input[name="title"]').offset().top;
                        $(window).scrollTop(top - 200);
                    }
                    return false;
                }
                $scope.formObject = EventForm;
                var API_URL = 'api/services.php/event/create/private/';
                $http.post(API_URL, eventManagerObject).then(function (result) {
                  if(result && result.data){
                      var eventId =  result.data.data;
      
                     window.location.href = baseUrl+"/"+ $scope.restaurant.getInternalPath()+"/event/"+eventId+"?dspl_h=f&dspl_f=f";
                  }
                      $(window).scrollTop();
                });
             
            };
         
            });
             app.directive('jqdatepicker', function () {
                return {
                    restrict: 'A',
                    require: 'ngModel',
                     link: function (scope, element, attrs) {
                          var options = {
                                    locale: 'en',
                                    format: 'DD-MM-YYYY',
                                    minDate: new Date()
                                };
                                  var DateTimePicker = $(element).datetimepicker(options);

                                  DateTimePicker.on('dp.change', function (e) {
                                     var DateSelected = moment(e.date._d).format('DD-MM-YYYY');
                                     scope.EventObject.rdate = DateSelected;

                                });
                    }
                };
            });

            app.directive('jtimedatepicker', function () {
                return {
                    restrict: 'A',
                    require: 'ngModel',
                     link: function (scope, element, attrs) {
                          var options = {
                                    locale: 'en',
                                    format: 'DD-MM-YYYY',
                                    minDate: new Date()
                                };
                                  var DateTimePicker = $(element).datetimepicker(options);

                                  DateTimePicker.on('dp.change', function (e) {
                                     var DateSelected = moment(e.date._d).format('DD-MM-YYYY');
                                     scope.EventObject.end = DateSelected;

                                 });
                    }
                };
            });
        </script>


        </body>

</html>



