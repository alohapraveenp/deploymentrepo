<?php

namespace App\Database;

use Exception;
use Illuminate\Database\Capsule\Manager as Capsule;

class Database
{
    public static function create()
    {
        try {
            Capsule::schema()->create(EVENT_BOOKING_TABLE, function ($table) {
                $table->increments('id');
                $table->integer('event_id');
                $table->integer('orderID');
                $table->string('pax', 3);
                $table->string('amount', 10);
                $table->string('curency', 10);
                $table->dateTime('time');
                $table->text('receiver');
                $table->string('paykey', 40);
                $table->string('first_name', 40);
                $table->string('last_name', 40);
                $table->string('email', 40);
                $table->string('phone', 40);
                $table->string('company');
                $table->text('special_request');
                $table->string('status', 20);
                $table->timestamps();
            });
            Capsule::schema()->create(BOOKING_DEPOSIT_TABLE, function ($table) {
                $table->increments('id');
                $table->text('data');
                $table->string('amount', 10);
                $table->string('curency', 10);
                $table->dateTime('time');
                $table->text('receiver');
                $table->string('paykey', 40);
                $table->string('status', 20);
                $table->string('token', 32);
                $table->timestamps();
            });
            echo 'Create Table Success';
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
    public static function drop()
    {
        try {
            Capsule::schema()->drop(EVENT_BOOKING_TABLE);
            Capsule::schema()->drop(BOOKING_DEPOSIT_TABLE);
            echo 'Drop Table Success';
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
}
