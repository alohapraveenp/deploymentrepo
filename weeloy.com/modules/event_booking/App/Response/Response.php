<?php

namespace App\Response;

class Response
{
    public static function success($data = null)
    {
        if (isset($data)) {
            $response = [
                'status' => 1,
                'data'   => $data,
            ];
        } else {
            $response = [
                'status' => 1,
            ];
        }
        header("HTTP/1.1 200 OK");
        header("Content-Type: Application/json");
        echo json_encode($response);
    }
}
