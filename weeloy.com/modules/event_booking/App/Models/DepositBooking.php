<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DepositBooking extends Model
{
    protected $table = BOOKING_DEPOSIT_TABLE;
}
