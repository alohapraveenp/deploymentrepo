<?php                //init 

                require_once("conf/conf.init.inc.php");
		require_once("lib/wpdo.inc.php");
		require_once("conf/conf.session.inc.php"); 
                
		require_once("lib/class.page.inc.php");
                require_once("conf/conf.page.inc.php");
                
                require_once("conf/conf.twig_widget.inc.php");
                //require_once("ressources/analyticstracking.php");

                require_once("lib/composer/vendor/autoload.php"); 
                require_once("inc/utilities.inc.php");
            
                $page = filter_input(INPUT_GET, 'page', FILTER_SANITIZE_STRING);
                if($page == 'gourmand'){
                    require_once 'gourmand/index.php';
                }
                if($page == 'marie-france'){
                    require_once 'marie-france/index.php';
                }