<?php           //init 
                require_once 'lib/class.restaurant.inc.php';
		require_once("lib/class.member.inc.php");
                require_once 'lib/class.media.inc.php';
                require_once 'lib/wglobals.inc.php';

                
                
                $res = new WY_restaurant();
                $media = new WY_Media();
                
                $list_restaurant = $res->getGourmandRestaurants('SG',4);
                
                foreach ($list_restaurant as $key => $resto){
                    $media->getWidgetDefaultPicture($resto['restaurant']);
                    $list_restaurant[$key]['picture'] = $media->getFullPath('270');
                    
                    //$wheel = explode('|', $resto['wheel']);
                    //$list_restaurant[$key]['internal_path'] = $res->getRestaurantInternalPath($resto['restaurant']);
                    
                    // 
                    // 
                    // // TEST VERSION
                    $list_restaurant[$key]['internal_resto'] = base64_encode($resto['restaurant']);
                    //$list_restaurant[$key]['internal_resto'] = base64_encode('SG_SG_R_TheFunKitchen');
                    //$list_restaurant[$key]['wheel'] = $wheel[1];
                }
                
                $loader = new Twig_Loader_Filesystem('gourmand');
                $twig = new Twig_Environment($loader,array('debug' => true));
                $twig->addFilter( new Twig_SimpleFilter('split_cuisine', function ($stdClassObject) {
                    $cuisineAr = explode('|', $stdClassObject);
                    return $cuisineAr;
                }));
                echo $twig->render('gourmand.html.twig',array('data'=>$list_restaurant, 'path_adaptor'=>'../../'));
               