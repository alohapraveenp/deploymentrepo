<?php
if (empty($_REQUEST["rt"]) || empty($_REQUEST["bk"])) {
    printf("Invalid review. Exiting");
    exit;
}
$rating = $_REQUEST["rt"];
$confirmation = $_REQUEST["bk"];
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="author" content="weeloy. https://www.weeloy.com"/>
        <meta name="copyright" content="weeloy. https://www.weeloy.com"/>  
        <meta http-equiv='expires' content='mon, 1 jan 1998 05:00:00 gmt'/>
        <meta http-equiv='pragma' content='no-cache'/>
        <meta http-equiv='pragma' content='cache-control: max-age=0'/>
        <meta http-equiv='last-modified' content='mon, 1 jan 1998 05:00:00 gmt'/>
        <meta http-equiv='cache-control' content='no-cache, must-revalidate'/>
        <meta name='robots' content='noindex, nofollow'/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <meta name="HandheldFriendly" content="true" />
        <title> Review Post</title>



        <link href="../../client/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../../client/bower_components/bootstrap/dist/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
        <link href="../../client/bower_components/fontawesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

        <link href="../../modules/booking/assets/css/css/bootstrap-select.css" rel="stylesheet" />
        <link href="../../modules/booking/assets/css/css/bootstrap-social.css" rel="stylesheet" />

        <script type='text/javascript' src="../../client/bower_components/angular/angular.min.js"></script>
        <script type="text/javascript" src="../../client/bower_components/jquery/dist/jquery.min.js"></script>

        <script type="text/javascript" src="../../js/jquery-ui.js"></script>
        <script type="text/javascript" src="../../client/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="../../js/bootstrap-select.js"></script>
        <script type='text/javascript' src="../../js/ui-bootstrap-tpls-0.12.1.min.js"></script>
        <script type="text/javascript" src="../../js/ngStorage.min.js"></script>
        <script type="text/javascript" src="../../js/mylocal.js"></script>

        <script> var app = angular.module("myApp", ['ui.bootstrap', 'ngLocale', 'ngStorage']);</script> 
        <style>
            html, body, .container-table {
                height: 100%;
            }
            .container-table {
                display: table;
            }
            .center {
                margin-left: 40%;
                padding: 10px;
            } 
            .rating{
                color: #999999;
                margin: 0;
                padding: 0;
                font-size:35px;
            }

            ul.rating {

                display: inline-block;
            }

            .rating li {
                list-style-type: none;
                display: inline-block;
                padding: 1px;
                text-align: center;
                font-size:35px;
                font-weight: bold;
                cursor: pointer;
            }

            .rating .filled {
                color: #e3cf7a;
            }
            .rating li:hover:before, .rating li:hover~li:before {

                color:#e3cf7a;
            }
        </style>
        <body ng-app="myApp">
            <div class="wrapper" ng-controller='ReviewpostController' ng-init="moduleName = 'reviewpost';" style='margin-top:20px;'>
                <div class="container container-table"  >
                    <div class="row ">
                            <div class="container">
                                <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-2 col-lg-8 col-md-8 col-sm-8 col-xs-12 my-booking"> 
                                    <div id='msg-alert' class="alert" style="display:none;">
                                    </div>
                                    <div id='review-container' class ='panel-body' style='display:block;'>
                                        <div id="targetSectionRev" >
                                            <div id='review-msg' style='margin-left:15px;text-align:center;'> <h4>Thank you for your review.</h4></div>
                                            <div class='review-container' style ="text-align:center;">
                                                 
                                                 <div class='review-container' style ="text-align:center;" >
                                                   <div class="star-rating" star-rating rating-value="rating" data-max="5" on-rating-selected="rateFunction(rating)"> </div>
                                                 </div>
                                            </div>
                                            <div id='comment' style='margin-left:15px;text-align:center;'><h5>Do you want to leave a comment?</h5></div>
                                            <div class="col-lg-offset-2 col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                <textarea ng-model='comment' class='form-control' rows='4' ></textarea>
                                                <div class="text-center" style="margin-top:10px;">
                                                    <input type="button" ng-click="RatingNow('update')" class="btn btn_default btn_submit_review" value="Submit" style="border-radius:0px;" >
                                                </div>
                                            </div>
                                        </div>
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                    </div>


<!--             <review-section  id="targetSectionRev" class="currentpanel" rdata-review="reviewList" ></review-section>-->

                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
                                    <p></p>
                                </div>
                            </div>

                       
                    </div>
                </div>
            </div>

            <script>
                        app.controller('ReviewpostController', function ($scope, $http) {
                            var rating = <?php echo "'" . $rating . "';"; ?>
                            $scope.confirmation = <?php echo "'" . $confirmation . "';"; ?>;
                            $scope.showreviewpostList = false;
                            $scope.reviewRating = [{'ext': '5'}, {'very': '4'}, {'good': '3'}, {'average': '2'}, {'bellow': '1'}];
                            var curRating = rating.split("-");
                            $scope.rating = curRating[1];
                            $scope.grade = $scope.rating;
                            setTimeout(function () {

                                var API_URL = '../../api/bkconfirmation/' + $scope.confirmation;
                                $http.get(API_URL).then(function (response) {
                                    $scope.restaurant = response.data.data.resto;
                                    $scope.rest_title = response.data.title;
                                    $scope.bookingId = response.data.data.booking;
                                    $scope.user_id = response.data.data.email;
                                    $scope.reviewList = response.data.data;
                                    $scope.showreviewpostList = true;
                                    $scope.showreviewList = false;
                                    $scope.bookid = response.data.data.bookid;

                                    if(response.data.data.review.review_status === 'posted'){
                                        $("#review-container").css('display', 'none');
                                        $("#msg-alert").css('display', 'block');
                                        $(".alert").addClass('alert-info');
                                        $(".alert").html(' The review for this booking has already been posted.');
                                    }else{
                                        if(response.data.data.reviewcount === 0){
                                            $scope.RatingNow('create');
                                        }
                                    }

                                });

                            }, 1000);
                            
                            
                            $scope.rateFunction = function(rating) {
                                $scope.grade = rating;
                            };
                            $scope.RatingNow = function (type) {
                                var data = {
                                    confirmation_id:$scope.bookingId,
                                    user_id: $scope.user_id,
                                    restaurant_id: $scope.restaurant,
                                    grade: $scope.grade,
                                    comment: $scope.comment,
                                    type :type
                                };
                                $http.post("../../api/review/grade",
                                        {
                                            'confirmation_id':$scope.bookingId,
                                            'user_id':$scope.user_id,
                                            'restaurant_id':$scope.restaurant,
                                            'grade':$scope.grade,
                                            'comment':$scope.comment,
                                            'type' :type

                                        }).then(function (response) {

                                    if (response.data.data.id && response.data.data.id !== 'undefined') {
                                        if(type === 'update'){
                                            $("#review-container").css('display', 'none');
                                            $("#msg-alert").css('display', 'block');
                                            $(".alert").addClass('alert-success');
                                            $(".alert").html(' You has been  successfully posted the review for this booking.');
                                        }

                                    }

                                });


                            };


                        });
                        app.directive('starRating', function () {
                                return {
                                        restrict: 'A',
                                        template: '<span style="margin-left:15px;" > <strong> Your Rating :</strong> </span><ul class="rating"><li ng-repeat="star in stars" ng-class="star" ng-click="toggle($index)">\u2605 </li></ul>',

                                        scope: {
                                            ratingValue: '=',
                                            max: '=',
                                            onRatingSelected: '&'
                                        },
                                        link: function (scope, elem, attrs) {
                                            var updateStars = function () {
                                                scope.stars = [];
                                                for (var i = 0; i < scope.max; i++) {
                                                    scope.stars.push({
                                                        filled: i < scope.ratingValue
                                                    });
                                                }
                                            };

                                            scope.toggle = function (index) {
                                                scope.ratingValue = index + 1;
                                                scope.onRatingSelected({
                                                    rating: index + 1
                                                });
                                            };

                                            scope.$watch('ratingValue',
                                                    function (oldVal, newVal) {
                                                        if (newVal) {
                                                            updateStars();
                                                        }
                                                    }
                                            );
                                        }
                                    };
                     });
            </script>


        </body>
</html>
