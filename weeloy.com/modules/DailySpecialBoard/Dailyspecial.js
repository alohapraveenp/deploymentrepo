app.controller('DailyspecialController', ['$scope','$http','$timeout', 'Upload', function($scope,$http,$timeout, Upload) {
	
  $scope.predicate = '';
	$scope.reverse = false;

  $scope.path ="https://media.weeloy.com/upload/dsb/";
  $scope.restaurant = null;
  $scope.showCaptcha = window.location.toString().indexOf("admin_weeloy") < 0;
  
  
  $scope.cuisinelist = [];
  $scope.dsbimage = '';



  $scope.init = function() {
    $scope.restaurant = {
      restaurant: "",
      headline: "",
      description: "",
      status: "dsb",
      selectedOption: {
        cat1: "",
        cat2: "",
        cat3: ""
      },
      images: {
        img: {}
      },
      imgname: ""
    };

    $scope.login = {
      email: "",
      password: ""
    };
  }  

  $scope.getcuisine = function()
  {

    $http.get('api/cuisinelist').then( function(response) {
        
        $scope.cuisinelist = response.data.data.cuisine;
    }); 
  }



  $scope.file = null;
  $scope.init();
	$scope.getcuisine();

  $scope.cleaninput = function(ll) {
		if(typeof $scope[ll] === 'string')
			$scope[ll] =  $scope[ll].replace(/\'|\"/g, '’');
  };
  $scope.currentDropElement = null;
  $scope.onDragStart = function(data, dragElement, dropElement) {};
  $scope.onDragEnd = function(data, dragElement, dropElement) {};
  $scope.onDragOver = function(data, dragElement, dropElement) {
    $scope.currentDropElement = dropElement;
  };
  $scope.onDragLeave = function() {
    $scope.currentDropElement = null;
  };
  $scope.onDrop = function(data,dragElement,dropElement,event,label) {
    var lbl =$("#"+label).attr('for');
    console.log(lbl);
    console.log(data);
    if (data && $scope.currentDropElement) {
      if(lbl=="notassigned") {
        $scope.notassigned.push(data);
        $scope.remove($scope.assigned, data);
      } else {
        $scope.assigned.push(data);
        $scope.remove($scope.notassigned, data);
      }
    }
  };



	$scope.saveitem = function(Dailyspecialform) {
		var u, msg, apiurl, ind;

    Upload.upload({
        url: "api/md_marketing/dsb/login/",
        data: {'login': $scope.login }
      }).then(function(response) {
          logincheck = response.data.status;

          if (logincheck == 1)
          {
            restaurant = response.data.data.row;
            $scope.restaurant.restaurant = restaurant[0].content;

            var files = [];
            if (Dailyspecialform.$invalid) {
              var top;
              if (Dailyspecialform.email.$invalid || Dailyspecialform.password.$invalid || Dailyspecialform.headline.$invalid || RegistrationForm.description.$invalid) {
                top = $('input[name="email"]').offset().top;
                $(window).scrollTop(top - 200);
              }
              return false;

            }

            if (!angular.isUndefined($scope.restaurant.images['img'].toUpload) && $scope.restaurant.images['img'].toUpload !== null)
            {
              $scope.restaurant.imgname = $scope.restaurant.images['img'].name;
              files.push($scope.restaurant.images['img'].toUpload);
            }
            
            Upload.upload({
              url: "api/md_marketing/dsb/postdaily",
              data: {file: files, 'restaurant': $scope.restaurant}
            }).then(function(response) {
                if (typeof response !== "undefined" && response.status > 0) {
                    var msg = 'You have successfully Posted in DSB!';
                    console.log(response.data.data);
                    if(response.data.data!==null)
                      console.log("response.data.data: " + JSON.stringify(response.data.data));
                      $scope.restaurant.restaurant = "";
                      $scope.restaurant.headline = "";
                      $scope.restaurant.description = "";
                      $scope.login.email = "";
                      $scope.login.password = "";
                } else {
                   console.error("Failed to Post Daily Special Board!");
                  }
            });
            
          }
          else
          {
            alert(response.data.errors);
          }
    });


  };

  $scope.uploadImage = function(element)	{
    //console.log("uploadImage(): id: "+element.id);
    if(element.files.length == 0) {
      alert("Select/Drop one file");
      return false;
    }
    filename = element.files[0].name;
    ext = filename.substring(filename.lastIndexOf(".")+1).toLowerCase();
    if(ext != "jpg" && ext != "jpeg" && ext != "png" && ext != "gif") {
      alert("Invalid file type (jpg, jpeg, png, gif)");
      return false;
    }
    $scope.restaurant.images[element.id].name = element.files[0].name;
    $scope.restaurant.images[element.id].media_type = 'picture';
    $scope.restaurant.images[element.id].toUpload = element.files[0];
    console.log("uploadImage(): $scope.restaurant.images: "+JSON.stringify($scope.restaurant.images));
  };
}]);