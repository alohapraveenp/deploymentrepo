app.controller('MagazineController', ['$scope','$http','$timeout', 'Upload','ModalService', '$sce', function($scope,$http,$timeout, Upload, ModalService, $sce) {

  $scope.restaurant = null;
  
  $scope.items = [];
  $scope.itemstemp = [];
  $scope.temp=null;
  
  $scope.loading = true;
  $scope.magazinemodalcontent = null;
  $scope.magazinemodalcontentimages = null;  
  

  var index = 0;
  var clean_img = [];
  var clean_img_html = [];
  $scope.getdailyspecial = function()
  {

    $http.get('api/md_marketing/getMagazine/').then( function(response) {  
        $scope.dsblist = null;
        
        dsblistcheck = response.data.status;

        if(dsblistcheck == 1)
        {
          dsblist = response.data.data;
          
          for (i = 0; i < dsblist.length; i++) {
                cc = dsblist[i];
                $scope.itemstemp[i] = cc;

                //$scope.magazinemodalcontent = $sce.trustAsHtml(dsblist[0].post_content);
              }
              //alert(dsblist[0].post_content);
              $scope.more();  
        }
        else
        {
          $scope.loading = false;
          alert(response.data.errors);
        }
        
    }); 
  }

  $scope.getdailyspecial();

  // this function fetches a random text and adds it to array
  $scope.more = function()
  {
    $scope.loading = false;
    temp = $scope.itemstemp.slice(index, index+12);
        //alert(temp.length);
        for (x = 0; x < temp.length; x++) {
              cc = temp[x];
              $scope.items.push(cc);
        }
        index=index+12;

  };

  $scope.magazinedetails = function($id)
  {

    var result_obj = objectFindByKey($scope.itemstemp, 'ID', $id);
    //alert(result_obj.post_content);
    $scope.magazinemodalcontent = result_obj.post_content;
    //$scope.magazinemodalcontent = $sce.trustAsHtml(result_obj.post_content);

    var m, 
    str = $scope.magazinemodalcontent;
    
    
    rex = /<img\s[^>]*?src\s*=\s*['\"]([^'\"]*?)['\"][^>]*?>/g;
    while ( m = rex.exec( str ) ) {
        clean_img.push( m[1] );
    }

    clean_content = str.replace(/(<([^>]+)>)/ig,"");
    clean_content.replace(/&nbsp;/g, ""); 

    for(i=0; i < clean_img.length;i++)
    {
      clean_img_html[i] = "<div><img src="+clean_img[i]+" width = 300 Height = 300 /> <br> </div>";
      
    }

    clean_img_html = $sce.trustAsHtml(clean_img_html.toString());
    //clean_img_html
    
    $scope.show();


  };



  $scope.show = function() {
      ModalService.showModal({
          templateUrl: 'modal.html',
          controller: "MagazineController"
      }).then(function(modal) {
         //console.log(modal);
         
         

         modal.scope.magazinemodalcontentimages = clean_img_html;
         modal.scope.magazinemodalcontent = clean_content;
         //alert($scope.magazinemodalcontent);
          modal.element.modal();
      });
  };

// find post_content for magazine
function objectFindByKey(array, key, value) {
    for (var i = 0; i < array.length; i++) {
        if (array[i][key] === value) {
            return array[i];
        }
    }
    return null;
}
 

}]);

// we create a simple directive to modify behavior of <div>
app.directive("whenScrolled", function(){
  return{
    
    restrict: 'A',
    link: function(scope, elem, attrs){
    
      raw = elem[0];

      elem.bind("scroll", function(){
        if(raw.scrollTop+raw.offsetHeight+5 >= raw.scrollHeight){
          scope.loading = true;          
        // we can give any function which loads more elements into the list
          scope.$apply(attrs.whenScrolled);
        }
      });
    }
  }
});