<style>
body
{
  overflow:hidden;
}

.magazinecontainer {
    float: left;
    height: 300px;
    width: 260px;
    border: solid 2px #f2f2f2;
    margin-bottom: 15px;
    margin-right: 15px;
    cursor:pointer;
}

.magazineimage {
    height: 160px;
    width: auto;
    background-color: #f2f2f2;
    margin-bottom: 5px;
}

.magazinedesc {
    width: 255px;
    height: 70px;
    padding-left: 5px;
    padding-top: 2px;
    overflow: hidden;
    text-overflow: ellipsis;
    
}

.magazinetitle
{
    padding-left: 5px;
    padding-bottom: 5px;
    font-size: bold;
    font-weight: bold;
    height: 60px;
    width: 252px;
}

.clearboth 
{
  clear: both;
}

.custombtn 
{
  padding: 10px 116px;
}

.showdata
{
  height: 710px;
  width: auto;
  overflow-y: scroll;
    margin-left: 22px;
    margin-right: auto;
}

.showdata::-webkit-scrollbar {
  display: none;
}

.mainholder{
    height: 100%;
    width: 100%;
    overflow: hidden;
}

.magimg
{
  height: 100%;
  width: 100%;
}

.modal-dialog{
  width: 1200px !important;
}

.magazinetitledetails {
    float: left;
    width: 660px;
}

.magazineimagecontent {
    float: left;
    width: 500px;
}



</style>
<div ng-controller="MagazineController" ng-init="moduleName='dsb';">
    <div class="container-fluid">
    <br>
      <div class="mainholder">
        <div class = "showdata" when-scrolled ="more()">
          <div data-ng-repeat ="item in items">
            <div class="magazinecontainer" ng-click="magazinedetails(item.ID)">
              <div class="magazineimage avatar"><img class="img-responsive magimg" src="client/assets/images/35x28.png" style="background: url(https://d1zfy3x6jx1ipx.cloudfront.net/{{item.imageUrl}}); background-size:cover;"; class="responsive" /> </div>
              <div class="magazinetitle">{{item.post_title}}</div>
              <div class="magazinedesc">{{item.description}}</div>
            </div>
          </div>
          <div class="clearboth"></div>
          <div data-ng-show = "loading"> Loading... </div>
        </div>
      </div>
    </div>

    <!-- The actual modal template, just a bit o bootstrap -->
     <script type="text/ng-template" id="modal.html">
         <div class="modal fade">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
              </div>
              <div class="modal-body">
                <p>
                  <div class="magazineimagecontent" ng-bind-html="magazinemodalcontentimages">
                      
                  </div>
                  <div class="magazinetitledetails" ng-bind="magazinemodalcontent"></div>
                </p>
              </div>
              <div class="modal-footer">
                <button type="button" ng-click="close('No')" class="btn btn-default" data-dismiss="modal">No</button>
                <button type="button" ng-click="close('Yes')" class="btn btn-primary" data-dismiss="modal">Yes</button>
              </div>
            </div>
          </div>
        </div>
     </script>


</div>


