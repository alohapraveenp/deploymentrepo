app.controller('DSBRegistrationController', ['$scope','$http','$timeout', 'Upload', function($scope,$http,$timeout, Upload) {
	$scope.predicate = '';
	$scope.reverse = false;
	$scope.tabletitleContent = [
          { a:'title', b:'Title', c:'', d:'tower', t:'input', i:0, r:false, type:'text' },
          { a:'firstname', b:'First Name', c:'', d:'pencil', t:'input', i:0, r:true, type:'text' },
          { a:'lastname', b:'Last Name', c:'', d:'pencil', t:'input', i:0, r:true, type:'text' },
          { a:'email', b:'Email', c:'', d:'inbox', t:'input', i:0, r:true, type:'email' },
          { a:'phone', b:'Phone', c:'', d:'phone', t:'input', i:0, r:false, type:'text' },
          { a:'owner', b:'I am the authorized representative of this business', c:'', d:'user', t:'checkbox', i:0 },
          { a:'name', b:'Restaurant Name', c:'', d:'pencil', t:'input', i:0, r:true, type:'text' },
          { a:'address', b:'Street Address', c:'', d:'map-marker', t:'input', i:0, r:false, type:'text' },
          { a:'city', b:'City', c:'', d:'pencil', t:'input', i:0, r:true, type:'text' },
          { a:'country', b:'Country', c:'', d:'pencil', t:'input', i:0, r:true, type:'text' },
          { a:'postalcode', b:'Postal Code', c:'', d:'pencil', t:'input', i:0, r:false, type:'text' },
          { a:'restaurantemail', b:'Restaurant Email', c:'', d:'inbox', t:'input', i:0, r:true, type:'email' },
          { a:'restaurantphone', b:'Restaurant Phone', c:'', d:'phone', t:'input', i:0, r:false, type:'text' },
          { a:'cuisines', b:'Cuisines', c:'', d:'cutlery', t:'input', i:0, r:false, type:'text' },
          { a:'website', b:'Website', c:'', d:'home', t:'input', i:0, r:true, type:'url' },
          { a:'pricepoint', b:'Price Point', c:'', d:'usd', t:'input', i:0, r:false, type:'text' },
          { a:'description', b:'Description', c:'', d:'comment', t:'textarea', i:0 },
          { a:'logo', b:'Logo', c:'', d:'picture', t:'imagebutton' },
          { a:'cover', b:'Cover', c:'', d:'picture', t:'imagebutton' },
          { a:'type', b:'Select Listing Type', c:'', d:'list', t:'dropdown', val: ["Basic - Free"] },
        ];
  $scope.path ="https://media.weeloy.com/upload/restaurant/";
  $scope.restaurant = null;
  $scope.showCaptcha = window.location.toString().indexOf("admin_weeloy") < 0;

  $scope.countrycode = "";
  $scope.phonenumber = "";

  $scope.getcuisine = function()
    {
      var API_URL = 'https://dev.weeloy.asia/api/cuisinelist';
      $http.get(API_URL).then(function(response) {
        
        if(response.data.status == 1)
        {
          $scope.cuisinelist = response.data.data.cuisine;
        }
        else
        {
        //$scope.loading = false;
        alert(response.data.errors);
        }  
      })

    }

  $scope.getcuisine();

  $scope.init = function() {
    $scope.restaurant = {
      ID: "",
      code: "",
      title: "",
      firstname: "",
      lastname: "",
      email: "",
      phone: "",
      owner: false,
      name: "",
      address: "",
      city: "",
      country: "",
      postalcode: "",
      restaurantemail: "",
      restaurantphone: "",
      cuisines: "",
      website: "",
      pricepoint: "",
      statusupdate: "0",
      description: "",
      images: {
        logo: {},
        cover: {}
      },
      type: "Basic - Free",
      status: "pending"
    };
    $('#logo').files = [];
    $('#cover').files = [];

    $scope.selectedOption = {
      cat1: "",
      cat2: "",
      cat3: ""
    };

  }
  $scope.file = null;
  $scope.init();
	$scope.cleaninput = function(ll) {
		if(typeof $scope[ll] === 'string')
			$scope[ll] =  $scope[ll].replace(/\'|\"/g, '’');
  };
  $scope.currentDropElement = null;
  $scope.onDragStart = function(data, dragElement, dropElement) {};
  $scope.onDragEnd = function(data, dragElement, dropElement) {};
  $scope.onDragOver = function(data, dragElement, dropElement) {
    $scope.currentDropElement = dropElement;
  };
  $scope.onDragLeave = function() {
    $scope.currentDropElement = null;
  };
  $scope.onDrop = function(data,dragElement,dropElement,event,label) {
    var lbl =$("#"+label).attr('for');
    console.log(lbl);
    console.log(data);
    if (data && $scope.currentDropElement) {
      if(lbl=="notassigned") {
        $scope.notassigned.push(data);
        $scope.remove($scope.assigned, data);
      } else {
        $scope.assigned.push(data);
        $scope.remove($scope.notassigned, data);
      }
    }
  };
	$scope.saveitem = function(RegistrationForm) {
		var u, msg, apiurl, ind;

    $scope.restaurant.restaurantphone = $scope.countrycode+$scope.phonenumber;
    $scope.restaurant.cuisines = $scope.selectedOption.cat1+','+$scope.selectedOption.cat2+','+$scope.selectedOption.cat3;


    if($scope.showCaptcha && grecaptcha.getResponse() == "") {
        alert("You can't proceed!");
        return false;
    }
    if (RegistrationForm.$invalid) {
      var top;
      if (RegistrationForm.name.$invalid || RegistrationForm.country.$invalid || RegistrationForm.city.$invalid || RegistrationForm.email.$invalid || RegistrationForm.restaurantemail.$invalid || RegistrationForm.website.$invalid) {
        top = $('input[name="title"]').offset().top;
        $(window).scrollTop(top - 200);
      }
      return false;
    }
    // var files = [];
    // if (!angular.isUndefined($scope.restaurant.images['logo'].content) && $scope.restaurant.images['logo'].content !== null)
    //   //files.push($scope.restaurant.images['logo'].content);
    //   files.push($base64.encode($scope.restaurant.images['logo'].content));
    // if (!angular.isUndefined($scope.restaurant.images['cover'].content) && $scope.restaurant.images['cover'].content !== null)
    //   //files.push($scope.restaurant.images['cover'].content);
    //   files.push($base64.encode($scope.restaurant.images['cover'].content));
    //console.log(files.length+" files: "+JSON.stringify(files));
    var url = window.location.toString();
    var index = url.indexOf("weeloy.com");
    if (index >= 0)
      url = url.slice(0, index) + "weeloy.com/";
    else {
      index = url.indexOf("weeloy.asia");
      if (index >= 0)
        url = url.slice(0, index) + "weeloy.asia/";
    }
    $scope.restaurant.code ="";
    $scope.restaurant.ID = "1234";
    var params = {httpMethod: "POST", body: {'data': $scope.restaurant}};
    var xhr = new XMLHttpRequest();

    $http.post("api/dailyboard.php/md_restaurant/newdsbresto",{
        "httpMethod": "POST",
        "body":{
          "data": $scope.restaurant
        }               
    }).then(function(response) { 
      //send email for verification
      console.log(response);
      if(response.data.status == 1)
      {
          $http.post('api/dailyboard.php/md_restaurant/sendemail',{
                  "code":response.data.data,"email":$scope.restaurant.email
          }).then( function(response) {  
              alert("Registration successful!! Please Check Email to Verify!!");
              $scope.init();
          }); 
      }
      else
      {
          alert('Please Enter Correct Email Address');
      }
      
    
    });



    
    //xhr.open("POST", "https://api.weeloy.asia/dsbnewrestaurant", true);
    // xhr.onreadystatechange = function() {
    //     if (xhr.readyState == XMLHttpRequest.DONE)
    //         //console.log("Got a response from the API as follows: \n" + xhr.responseText);
    //       console.log("POST successful!");
    //       alert("Registration successful!!");
    // }
    // xhr.setRequestHeader("Content-type", "application/json");
    //console.log("params: "+JSON.stringify(params));
    //xhr.send(JSON.stringify(params));
//     Upload.upload({
//           //url: url+"api/restaurant/dsb/register",
//           //headers: {'Content-Type': 'application/json'},
//           headers: {'Content-Type': 'application/json'},
//           url: "https://shs1no3dk6.execute-api.ap-southeast-2.amazonaws.com/V1_0",
//           data: {file: files, 'restaurant': $scope.restaurant}
//         }, true).then(function(response) {
//             if (typeof response !== "undefined" && response.status > 0) {
//                 var msg = 'You have successfully registered a new restaurant for DSG!';
//                 //Notification.show('error', msg);
//                 console.log(response.data);
//                 if (!angular.isUndefined(response.data.data))
//                   console.log(response.data.data);
//                 if(response.data.data!==null)
//                   console.log("response.data.data: " + JSON.stringify(response.data.data));
// //                $scope.init();
//             } else {
//                console.error("Failed to register restaurant!");
//                //Notification.show('error', 'Oops!! Sorry Unexpected Error');
//             }
//     });
  };
  $scope.uploadImage = function(element)	{
    //console.log("uploadImage(): id: "+element.id);
    if(element.files.length == 0) {
      alert("Select/Drop one file");
      return false;
    }
    filename = element.files[0].name;    
    ext = filename.substring(filename.lastIndexOf(".")+1).toLowerCase();
    if(ext != "jpg" && ext != "jpeg" && ext != "png" && ext != "gif") {
      alert("Invalid file type (jpg, jpeg, png, gif)");
      return false;
    }
    var reader = new FileReader();
    $scope.restaurant.images[element.id].name = element.files[0].name;
    $scope.restaurant.images[element.id].media_type = 'picture';
    //console.log("uploadImage(): $scope.restaurant.images: "+JSON.stringify($scope.restaurant.images));
    reader.addEventListener("load", function () {
      //console.log("uploadImage(): reader.result: " + JSON.stringify(reader.result));
      $scope.restaurant.images[element.id].content = reader.result;      
    }, false);
    reader.readAsDataURL(element.files[0]);
    //console.log("uploadImage: read.result: "+JSON.stringify(reader));
    //console.log("uploadImage: element.files: "+JSON.stringify(element.files));

  };
}]);