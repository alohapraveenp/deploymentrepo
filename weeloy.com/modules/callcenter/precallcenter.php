<?php
require_once("conf/conf.init.inc.php");
require_once("lib/wpdo.inc.php");
require_once("lib/wglobals.inc.php");
require_once('conf/conf.session.inc.php');
require_once("lib/class.media.inc.php");
require_once("lib/class.restaurant.inc.php");
require_once("lib/class.booking.inc.php");
require_once("lib/class.member.inc.php");
require_once("lib/class.promotion.inc.php");
require_once("lib/class.analytics.inc.php");
require_once("lib/class.coding.inc.php");


$IPaddr = ip2long($_SERVER['REMOTE_ADDR']);

$arglist = array('bksalutation', 'bklast', 'bkfirst', 'bkemail', 'bkcover', 'bkdate', 'bktime', 'bkmobile', 'bkcountry', 'bkrestaurant', 'bktitle', 'bkparam', 'bkspecialrequest', 'brwsr_type', 'bkpage', 'bktracking', 'data', 'booker', 'company', 'hotelguest', 'dontnotify', 'ccrequired', 'bktms', 'genflag', 'bkerror', 'bkextra', 'bkproduct', 'bknotes');
foreach ($arglist as $label) {
    if (!isset($_REQUEST[$label]))
        $_REQUEST[$label] = "";
	$$label = $_REQUEST[$label];
	$$label = preg_replace("/\'|\"/", "’", $$label);
	$$label = preg_replace("/\s+/", ' ', $$label);
	$$label = preg_replace("/\'/", "`", $$label);
    }
    
$bkpage = intval($bkpage) + 1;

$fullfeature = true;

if(empty($bkrestaurant)) {
	header("location: https://www.weeloy.com");
	exit;
	}
	
$res = new WY_restaurant;
$res->getRestaurant($bkrestaurant);

if(empty($res->restaurant)) {
	header("location: https://www.weeloy.com");
	exit;
	}

// if the booking session is valid and not over 600s

$action = "callcenter.php"; 

$bkparamdecoded = detokenize($bkparam);
if(substr($bkparamdecoded, 0, 3) != date('M') || microtime(true) - floatval(substr($bkparamdecoded, 3)) > 600){
	header("Location: " . $action . "?bkrestaurant=" . $bkrestaurant . "&bktitle=" . $bktitle . "&bktracking=" . $bktracking . "&data=" . $data . "&timeout=y");
}

$mediadata = new WY_Media();
$logo = $mediadata->getLogo($bkrestaurant);

$restaurant_restaurant_tnc = $res->restaurant_tnc;
$typeBooking = "true";
$is_listing = false;
if(empty($res->is_wheelable) || !$res->is_wheelable){
    $is_listing = true;
}

$logger = new WY_log("website");
$loguserid = (isset($_SESSION['user']['id'])) ? $_SESSION['user']['id'] : "unknown";
$logger->LogEvent($loguserid, 802, '', $res->restaurant, '', date("Y-m-d H:i:s"));

$errmsg = "";
if($bkemail !== "no@email.com" &&  $bkmobile != "+65 99999999") {
	$bkg = new WY_Booking();
	$bkg->duplicate($bkrestaurant, $bkdate, $bkemail, $bkmobile);
	if($bkg->result > 0) {
		$errmsg = "Booking (" . $bkg->confirmation . ") for the same day, at " . substr($bkg->rtime, 0, 5) . " for " . $bkg->cover . " persons";
		}
}

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv='expires' content='mon, 1 jan 1998 05:00:00 gmt'>
<meta http-equiv='pragma' content='no-cache'>
<meta http-equiv='pragma' content='cache-control: max-age=0'>
<meta http-equiv='last-modified' content='mon, 1 jan 1998 05:00:00 gmt'>
<meta http-equiv='cache-control' content='no-cache, must-revalidate'>
<meta name='robots' content='noindex, nofollow'>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="HandheldFriendly" content="true" />
<title>Weeloy Code</title>
<link href="../../css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="../../css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
<link href="../../css/bootstrap-formhelpers.min.css" rel="stylesheet" media="screen">
<link href="../../css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="../../css/style.css" rel="stylesheet" type="text/css" />
<link href="bookstyle.css" rel="stylesheet" type="text/css" />
<link href='https://fonts.googleapis.com/css?family=Montserrat|Unkempt|Roboto:400,900,700italic,700,500italic,500,400italic' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="../../js/jquery.min.js"></script>
<script type="text/javascript" src="../../js/bootstrap.min.js"></script>
<script type='text/javascript' src="../../js/angular.min.js"></script>
<style>

<?php if ($brwsr_type != "mobile") echo ".mainbox { margin: 5px 5px 10px 15px; width:550px; } "; ?>

</style>
<?php include_once("ressources/analyticstracking.php") ?>
</head>
<body ng-app="myApp">
<form class='form-horizontal' action='confirmcc.php' role='form' id='book_form' name='book_form' method='POST' enctype='multipart/form-data'> 
	<?
	reset($arglist);
	while(list($index, $label) = each($arglist))
		printf("<input type='hidden' id='%s' name='%s' value='%s'>", $label, $label, $$label);
	?>

	<div id='booking' ng-controller='MainController' ng-init=" ;"  ng-cloak >

		<div class="container mainbox">    
			<div class="panel panel-info" >
				<div class="panel-heading">
					<div class="panel-title" style='width:300px;'>{{ bookingTitle}} </div>
				</div>     

				<p class="slogan"><b>Please confirm your information</b></p>
				<div style="padding-top:0px" class="panel-body" >

						<div ng-if="errmsg!=''" class="alert alert-danger"> <strong>Duplicates!</strong> {{ errmsg }}</div>
						<div class='col-xs-5 separation' style='height:330px;'>
							<center><img ng-src='{{imglogo}}' max-width='120' max-height='120'></center><br>
							<table width='90%'>
								<tr ng-repeat="x in glyphtag"><td><p class='glyphicon glyphicon-ok checkmark'></p><br/>&nbsp;</td><td class='pcheckmark'> {{x}} <br/>&nbsp;</td></tr>
							</table>
							<br /><br /><br /><br />
						</div>

						<div class='col-xs-7'  style="font-size:12px;">

						<div class="alert alert-info alert-label" style="padding-top:5px;padding-bottom:5px"> <strong>BOOKING DETAILS</strong></div>
						<div class='row'>&nbsp; Restaurant:  {{ btitle }}</div>
						<div class='row'>&nbsp; Date:    {{ bdate }}</div>
						<div class='row'>&nbsp; Time:   {{ btime }}</div>
						<div class='row'>&nbsp; Number of guests:   {{ bcover }}</div>
						<div class='row' ng-if="genflag === 1">&nbsp; walk-in guest</div>
						<div class="alert alert-info alert-label" style="padding-top:5px;padding-bottom:5px"> <strong>PERSONAL DETAILS</strong></div>
						<div class='row'>&nbsp; Name:   {{ bsalutation }} {{ bfirst }} {{ blast }}</div>
						<div class='row'>&nbsp; Email:   {{ bemail }}</div>
						<div class='row'>&nbsp; Mobile:   {{ bmobile }}</div>
						<div class='row' ng-if="bspecialrequest != ''">&nbsp; Request:   {{ bspecialrequest }}</div>
						<div class='row' ng-if="bnotes != ''">&nbsp; internal notes:   {{ bnotes }}</div>
						<div class='row'></div>
						<div class="row">
						<table width='100%'><tr><td><a href ng-click="submitonce();" class="btn {{buttonClass}}" style='padding:5px'> {{ buttonLabel}}  </a></td><td align='right'><a href ng-click="submitmodify();" class="btn btn-xs btn-info" >Modify</a></td></tr></table>
						</div>
						</div>
				</div>                     
			</div>  
		</div>
	</div>
</form>


<script>

<?php
echo "var typeBooking = $typeBooking;";
echo "var action = '$action';";
echo "var btitle = '$bktitle';";
echo "var bdate = '$bkdate';";
echo "var btime = '$bktime';";
echo "var bcover = '$bkcover';";
echo "var bsalutation = '$bksalutation';";
echo "var bfirst = '$bkfirst';";
echo "var blast = '$bklast';";
echo "var bemail = '$bkemail';";
echo "var bmobile = '$bkmobile';";
echo "var bspecialrequest = '$bkspecialrequest';";
echo "var bnotes = '$bknotes';";
echo "var errmsg = '$errmsg';";
echo "var logo = '$logo';";

printf("var is_listing = %s;", $is_listing ? 'true' : 'false');
?>

var app = angular.module("myApp", []);

app.controller('MainController', function ($scope, $http) {
	
	$scope.onlyonce = true;
	$scope.btitle = btitle;
	$scope.bdate = bdate;
	$scope.btime = btime;
	$scope.bcover = bcover;
	$scope.bsalutation = bsalutation;
	$scope.bfirst = bfirst;
	$scope.blast = blast;
	$scope.bemail = bemail;
	$scope.bmobile = bmobile;
	$scope.bspecialrequest = bspecialrequest;
	$scope.bnotes = bnotes;
	$scope.errmsg = errmsg;
	$scope.imglogo = logo;
	$scope.genflag = <? echo intval($genflag); ?>;
	
	$scope.bookingTitle = (typeBooking) ? "BOOKING SUMMARY" : "REQUEST SUMMARY";
	$scope.buttonLabel = (typeBooking) ? 'CONFIRM' : 'REQUEST YOUR WEELOY CODE';
	$scope.buttonClass = (typeBooking) ? 'btn-danger' : 'btn-green';
	$scope.listTags = (typeBooking) ? "Instant Reservation" : "No reservartion";
	$scope.listTags1 = "free booking";
	$scope.listTags2 = (typeBooking) ? "" : "First come, first serve";
	$scope.listTags3 = (!is_listing) ? "Spin the Wheel" : "Enjoy incredible";
	$scope.listTags4 = (!is_listing) ? "at the restaurant" : " promotion";

	$scope.glyphtag = [ $scope.listTags1, $scope.listTags + ' ' + $scope.listTags2, $scope.listTags3 + ' ' + $scope.listTags4 ];
	$scope.glyphtag.pop(); // don't show the 'spin the weel'
	
	$scope.submitonce = function() {

		if($scope.onlyonce === true)
			$('#book_form').submit();

		$scope.onlyonce = false;
	};

	$scope.submitmodify = function() {

		$('#book_form').attr('action', action);
		if($scope.onlyonce === true)
			$('#book_form').submit();

		$scope.onlyonce = false;
	};
});


</script>
    </body>
</html>

