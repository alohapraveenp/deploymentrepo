<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once("lib/class.member.inc.php");
require_once 'lib/class.booking.inc.php';
require_once("lib/class.media.inc.php");
require_once("lib/wpdo.inc.php");
    $qString_array = array();
    parse_str(@$_SERVER['QUERY_STRING'], $qString_array);

    $booking = new WY_Booking();

    $email = $booking->base64url_decode($qString_array['uid']);
    $confirmation_id = $booking->base64url_decode($qString_array['refid']);

    if(isset($email) && isset($confirmation_id)){
      $confirmation_id = $confirmation_id; 
      $email = $email;
      
    }
?>
<html><head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv='expires' content='mon, 1 jan 1998 05:00:00 gmt'>
        <meta http-equiv='pragma' content='no-cache'>
        <meta http-equiv='pragma' content='cache-control: max-age=0'>
        <meta http-equiv='last-modified' content='mon, 1 jan 1998 05:00:00 gmt'>
        <meta http-equiv='cache-control' content='no-cache, must-revalidate'>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <meta name="HandheldFriendly" content="true" />
        <title>Cancel Booking - Weeloy.com</title>
        <link href="../../css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../../css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
        <link href="../../css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="../../css/bootstrap-select.css" rel="stylesheet" >
        <script type="text/javascript" src="../../js/jquery.min.js"></script>
    </head>
    <body>
        <div class="col-center-block" >
            <h2>Cancel Booking </h2>
            <div class="in-container" style="padding:15px;">

                <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                    <input id='bkemail' type="text" class="form-control " name="email" placeholder="email"  value="<?php echo @$email ?>" /></br> 
                </div>
                <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                    <input type="text" class="form-control" id='bkconfirmation' name='bkconfirmation' placeholder="confirmation" value="<?php echo @$confirmation_id ?>" />                                        
                </div>
            </div>
            <div id="invite-alert"></div>
            <div id="confirm-container">
            </div>
            <div class="survey-conatiner">
                <p style="color:#494949">Please select the reason for your cancel here. If you have more than one reason for cancellation, please tell us the main reason.</p></br>  <input type="hidden" id="survey" value="" />
                <input type="radio" name="cl-servey" value="Unable to attend on this date" onclick="reset_msg();"> Unable to attend on this date </br></br>
                <input type="radio" name="cl-servey" value="My Friend(s) cancelled our Date" onclick="reset_msg();"> My Friend(s) cancelled our Date </br></br>
                <input type="radio" name="cl-servey" value="I no longer want to dine at this Restaurant" onclick="reset_msg();"> I no longer want to dine at this Restaurant </br></br>
                <input type="radio" name="cl-servey" value=" I wish to change my Booking" onclick="reset_msg();">  I wish to change my Booking </br></br>
                <input type="radio" name="cl-servey" value="Other" onclick="reset_msg();" /> Other : &nbsp<input id="othermsg" value="" type="text" name="other" />
                <button type="button" class="btn btncancel btn-default" onclick="getConfirmation();" />Cancel Booking</button>
            </div>
            <div class="success-conatiner" style="display:none;">
                <h2>Your reservation has been cancelled.</h2>
           </div>

        </div>
    </body>
</html>
<style>
    body{

        padding-top:1%;
        position: relative;

    }
    #confirm-container p{
        text-align:center;
        margin-top: 0%;
        font-size:18px;
    }
    ul{
        list-style-type: none
    }


    #invite-alert{
        margin-left:40px;
        margin-bottom:20px;  
    }
    .col-center-block {
        margin-top: 30px;
        width:600px;
        height:550px;
        margin-left: auto ;
        margin-right: auto ;
        padding: 2px 2px;
        border: 1px solid #aaa;
        margin-bottom:30px;
    }
    .col-center-block  h2{
       text-align:center;   
    }
    input[type="radio"]  {
        display:inline-block;

        margin:-1px 4px 0 0;
        cursor:pointer;
    }
    .input-group{
        margin-bottom: 8px;
    }
    .survey-conatiner{

        margin-bottom: 10px;
        margin-left: 20px;
        margin-right: 20px;
    }
    .confirm-container{
        margin:30px;
        height:450px;
    }
    .btncancel,.btnconfirmCancel{
       
       
        border-radius: 2px;
        margin-bottom: 10px;
        margin-top:30px;
        float:right;
        padding-top:10px;
        padding-right:10px;
        padding-bottom:10px;
        margin-right:10px;
    }
    .error-msg {
        color: #a94442 !important;
        background-color: #f2dede;
        border-color: #ebccd1;
        padding-left:2%;
        padding-bottom:1%;
        padding-top:1%;
    }
    .success-msg {
        color: #3c763d !important;
        background-color: #dff0d8;
        border-color: #d6e9c6;
        padding-left:2%;
        padding-bottom:1%;
        padding-top:1%;
    }
</style>
<script type="text/javascript">
    function reset_msg() {
        document.getElementById('invite-alert').innerHTML = '';
        $("#invite-alert").removeClass();

    }
    function close_msg() {
        setTimeout(function () {
            $('#invite-alert').fadeOut('slow');
        }, 10000); // <-- time in milliseconds  
    }
    /***************************************************/
    //cancel  ajax api post
    /****************************************************/

    function getConfirmation() {

        var msg = "Please select the main reason for your cancellation.!!",
                keymsg = "Email and Booking confirmation Id Required!!",
                id = $("#survey").val(),
                confirmation = $('#bkconfirmation').val(),
                userEmail = $('#bkemail').val(),
                confUrl = "../../api/visit/cancelconfirm/" + confirmation + "/" + userEmail,
                survey = $("input[type='radio'][name='cl-servey']:checked").val();

        if (typeof confirmation === 'undefined' || $('#bkconfirmation').val() === "" || userEmail === 'undefined' || $('#bkemail').val() === "") {
            $("#invite-alert").addClass('error-msg');
            $("#invite-alert").text(keymsg);
            close_msg();
            return false;
        }
        if (typeof survey === 'undefined') {
            $("#invite-alert").addClass('error-msg');
            $("#invite-alert").text(msg);
            close_msg();
            return false;
        }

        if (survey === 'Other') {
            if ($("#othermsg").val() == "") {
                $("#invite-alert").addClass('error-msg');
                $("#invite-alert").text(msg);
                close_msg();
                return false;
            }
            else {
                survey = $("#othermsg").val();
            }

        }
        $.ajax({
            url: confUrl,
            dataType: "json",
            type: "GET",
            data: {},
            success: function (res) {
                console.log(JSON.stringify(res));

                var confirmationid = res.data.booking,
                        title = res.data.resto,
                        email = res.data.email,
                        resName = res.data.title;
                if (res.status == '1') {
                    $(".survey-conatiner").css('display', 'none');
                    $(".btncancel").css('display', 'none');
                    $(".in-container").css('display', 'none');
                    $(".btnconfirmCancel").css('display', 'block');
                    if (res.data.date) {
                        var datefr = res.data.date;
                        datefr = datefr.split('-');
                        res.data.date = datefr[2] + '-' + datefr[1] + '-' + datefr[0]

                    }
                    var result = "<ul>";
                    result += "<p style='padding-top: 10%;color:#494949;'>Booking Details</p></br>";
                    result += "<input type='hidden' id='op-surey' value='" + survey + "' for='" + res.data.email + "'/>";
                    result += "<li>&nbsp; Restaurant Name : <span id='res_name'>" + resName + "</span></li></br>";
                    result += "<li>&nbsp; Restaurant : <span id='res_title'>" + title + "</span></li></br>";
                    result += "<li>&nbsp; Confirmation : <span id='res_confirmid'>" + confirmationid + "</span></li></br>";
                    result += "<li>&nbsp; Date : " + res.data.date + "</li></br>";
                    result += "<li>&nbsp; Time : " + res.data.time + "</li></br>";
                    result += "<li>&nbsp; Number of guests : " + res.data.pers + "</li></br>";
                    result += "</ul>";
                    result += "<button type='button' class='btnconfirmCancel btn  btn-default' onclick='cancelConfirmation();'>Confirm Cancel</button>";
                    $("#confirm-container").html(result);
                }
                if(res.data==='FAIL'){
              
                    $("#invite-alert").addClass('error-msg');
                    $("#invite-alert").text(res.errors);
                    close_msg();
                    return false;
                }

            },
            complete: function () {
            }
        });
        return false;

    }
    function cancelConfirmation() {
        var confirmation = $("#res_confirmid").text(),
                title = $("#res_title").text(),
                userEmail = $("#op-surey").attr('for');

        var url = '../../api/visit/cancel/' + title + '/' + confirmation + '/' + userEmail;


        $.ajax({
            url: url,
            context: $(this),
            dataType: "json",
            type: "POST",
            data: {
                reason: $("#op-surey").val(),
                restaurant: title,
                confirmation: confirmation,
                email: userEmail
            },
            success: function (data) {
                 if (data.data === "OK") {
//                    $(".survey-conatiner").css('display', 'block');
//                    $(".btncancel").css('display', 'block');
//                    $(".in-container").css('display', 'block');
                $(".success-conatiner").css('display', 'block');
                    $("#confirm-container").html('');
//                    $("#invite-alert").addClass('success-msg');
//                    $("#invite-alert").text(data.errors);
                    $(".form-control").val('');
                    $("input[type='radio'][name='cl-servey']").prop("checked", false);
                    close_msg();
                }
                else if (data.data === "FAIL") {
                  
                    $("#invite-alert").addClass('error-msg');
                    $("#invite-alert").text(data.errors);
                    close_msg();
                    return false;
                }

            },
            error: function (data) {
                if (data.data === "FAIL") {
                    $("#invite-alert").addClass('error-msg');
                    $("#invite-alert").text(data.errors);
                    close_msg();
                    return false;
                }
                //window.location.reload();


            },
            complete: function () {
            }
        });
        return false;
    }
</script>

