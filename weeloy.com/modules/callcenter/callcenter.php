<?php
require_once("lib/Browser.inc.php");
$browser = new Browser();

require_once("conf/conf.init.inc.php");
require_once("lib/wpdo.inc.php");
//require_once("lib/class.session.inc.php");
require_once("conf/conf.session.inc.php");
require_once("lib/wglobals.inc.php");
require_once("lib/class.media.inc.php");
require_once("lib/class.restaurant.inc.php");
require_once("lib/class.member.inc.php");
require_once("lib/class.promotion.inc.php");
require_once("lib/class.analytics.inc.php");
require_once("lib/class.coding.inc.php");
require_once("lib/class.login.inc.php");
require_once("lib/class.cluster.inc.php");

$arglist = array('bksalutation', 'bklast', 'bkfirst', 'bkemail', 'bkcover', 'bkdate', 'bktime', 'bkmobile', 'bkcountry', 'bkrestaurant', 'bktitle', 'bkparam', 'bkspecialrequest', 'brwsr_type', 'bkpage', 'bktracking', 'data', 'booker', 'company', 'hotelguest', 'bktms', 'genflag', 'dontnotify', 'bkextra', 'bkerror', 'bkproduct', 'bknotes');

foreach ($arglist as $label) {
	if(empty($_REQUEST[$label])) {
            
		$_REQUEST[$label] = $$label = "";
		if($label == "dontnotify" && !empty($_REQUEST["bktms"])) {
			$_REQUEST["dontnotify"] = $dontnotify = ((intval($genflag) & 0x8000) === 0) ? '1' : '';
			}
   	} 
    else $$label = preg_replace("/\'|\"/", "’", $_REQUEST[$label]);
    }

$referrer = $_SERVER["HTTP_REFERER"];
if(!empty($referrer) && empty($bkrestaurant)) {
	if(preg_match("/href=1/", $referrer) && preg_match("/bkrestaurant=([^&]+)/", $referrer, $match)) {
		$found = $match[1];
		if(WY_Restaurant::isrestaurant($found))         
			$bkrestaurant = $found;
		}
	}


$cstate = (empty($bkcover)) ? "new" : "again"; 

$tokendata = $data;
$selectresto = $selectbooker = "";

$res = new WY_restaurant;
$pabx = $res->checkPabx();
list($cret, $selectresto, $selectbooker, $email, $bkrestaurant, $bktitle, $restoAr, $fixline, $minpax, $maxpax) = $res->getClusterCallCenter(detokenize($tokendata), $bkrestaurant, $bktitle);
if($cret == -1) {
	echo "ERROR CONFIGURATION CALL CENTER <br /> UNABLE TO CONTINUE. $email";
	exit;
	}

if($pabx == 0) $fixline = "";
	
$restogroup = (count($restoAr) > 1) ? implode(",", $restoAr) : "";
if(!empty($selectresto))
	$selectresto = "<select name='restaurant' id='restaurant' class='selectpicker' data-style='btn-primary btn-sm' onchange='reloadcc(this.value);' >" . $selectresto . "</select>";
if(!empty($selectbooker))
	$selectbooker = "<select name='booker' id='booker' class='selectpicker' data-style='btn-warning btn-sm'>" . $selectbooker . "</select>";

if(empty($bkrestaurant)) {
	header("location: https://www.weeloy.com");
	exit;
	}

$bkparam = date('M') . strval(microtime(true));
$bkparam = tokenize($bkparam);
$timeout = (!empty($_REQUEST['timeout'])) ? $_REQUEST['timeout'] : "";

$res->getRestaurant($bkrestaurant);
if(empty($res->restaurant)) {
	header("location: https://www.weeloy.com");
	exit;
	}

$limited = (preg_match("/limited/", $bktracking)) ? 1 : 0;
if($bkrestaurant == "SG_SG_R_MadisonRooms") {
	if(!empty($res->restogeneric)) {	//  && preg_match("/website|facebook/i", $bktracking)
		$obj = preg_replace("/’/", "\"", $res->restogeneric);
		$objAr = json_decode($obj, true);
		if(isset($objAr["frontbook"]) && strlen($objAr["frontbook"]) > 5) {
			$frontinfo = preg_replace("/\'|\"/", "’", $objAr["frontbook"]);
			$frontinfo = preg_replace("/\r|\n/", ", ", $frontinfo);
			}
		if($bkrestaurant == "SG_SG_R_MadisonRooms" && isset($objAr["listarea"]) && strlen($objAr["listarea"]) > 5)
			$listarea = $objAr["listarea"];
		if($bkrestaurant == "SG_SG_R_MadisonRooms" && isset($objAr["listpurpose"]) && strlen($objAr["listpurpose"]) > 5)
			$listpurpose = $objAr["listpurpose"];
		}
	}

if(!isset($listarea)) $listarea = "";
if(!isset($listpurpose)) $listpurpose = "";

$bookingwindow = $res->getBookingwindow('CALLCENTER');

$minpax = ($minpax != "") ? intval($minpax) : 1;
$maxpax = ($maxpax != "") ? intval($maxpax) : 30;

if($minpax < 0 || $minpax > 3) $minpax = 1;
if($maxpax < 4 || $maxpax > 99) $maxpax = 30;

if(empty($bkcover)) $bkcover = $minpax;

/*
$maxpax = $res->dfmaxpers;
$minpax = $res->dfminpers;

if($minpax < 1 || $minpax > 3) $minpax = 1; 
if($maxpax < 30) $maxpax = 30; 
*/
	
$AvailperPax = ($res->perPaxBooking()) ? "1":"0";

//tracking 
//test tracking cookie
$logger = new WY_log("website");
$loguserid = (isset($_SESSION['user']['id'])) ? $_SESSION['user']['id'] : "unknown";
$logger->LogEvent($loguserid, 801, '', $res->restaurant, '', date("Y-m-d H:i:s"));


// book / request process
$action = 'book';
if(filter_input(INPUT_GET, $action, FILTER_SANITIZE_STRING) == 'request'){
    $action = 'request';
    }


$fullfeature =  true;
$typeBooking = true;
$is_listing = false;

$nginit = "typeBooking=" . $typeBooking . ";";

$mediadata = new WY_Media($bkrestaurant);
$logo = $mediadata->getLogo($bkrestaurant);

$restaurant_restaurant_tnc = $res->restaurant_tnc;

$date = date_create(null, timezone_open("Asia/Singapore"));
$data_min = date_format($date, "d/m/Y");
date_add($date, date_interval_create_from_date_string("60 days"));
$data_max = date_format($date, "d/m/Y");

date_default_timezone_set("Asia/Singapore");
$todaytime = date("H:i");
$todaydate = date("d/m/Y");

if (empty($bkdate))
    $bkdate = $data_min;

if (empty($bktime))
    $bktime = "19:00";

$bkjsDate = substr($bkdate, 6, 4) . "-" . substr($bkdate, 3, 2) . "-" . substr($bkdate, 0, 2);
$objToday = new DateTime(date("Y-m-d"));
$objDateTime = new DateTime($bkjsDate);
$ndays = $objToday->diff($objDateTime)->format('%a');

$actionfile = $_SERVER['PHP_SELF'];

$brwsr_type = ($browser->isMobile() || (isset($_REQUEST['brwsr_type']) && $_REQUEST['brwsr_type'] == "mobile")) ? "mobile" : "";
if($browser->isTablet()) $brwsr_type = "tablette";

$iphoneflg = (strtolower($browser->getPlatform()) == "iphone");

$countriesAr = array( array( 'a' => 'Australia', 'b' => 'au', 'c' => '+61' ), array( 'a' => 'China', 'b' => 'cn', 'c' => '+86' ), array( 'a' => 'Hong Kong', 'b' => 'hk', 'c' => '+852' ), array( 'a' => 'India', 'b' => 'in', 'c' => '+91' ), array( 'a' => 'Indonesia', 'b' => 'id', 'c' => '+62' ), array( 'a' => 'Japan', 'b' => 'jp', 'c' => '+81' ), array( 'a' => 'Malaysia', 'b' => 'my', 'c' => '+60' ), array( 'a' => 'Myanmar', 'b' => 'mm', 'c' => '+95' ), array( 'a' => 'New Zealand', 'b' => 'nz', 'c' => '+64' ), array( 'a' => 'Philippines', 'b' => 'ph', 'c' => '+63' ), array( 'a' => 'Singapore', 'b' => 'sg', 'c' => '+65' ), array( 'a' => 'South Korea', 'b' => 'kr', 'c' => '+82' ), array( 'a' => 'Thailand', 'b' => 'th', 'c' => '+66' ), array( 'a' => 'Vietnam', 'b' => 'vn', 'c' => '+84' ), array( 'a' => '', 'b' => '', 'c' => '' ), array( 'a' => 'Canada', 'b' => 'ca', 'c' => '+1' ), array( 'a' => 'France', 'b' => 'fr', 'c' => '+33' ), array( 'a' => 'Germany', 'b' => 'de', 'c' => '+49' ), array( 'a' => 'Italy', 'b' => 'it', 'c' => '+39' ), array( 'a' => 'Russia', 'b' => 'ru', 'c' => '+7' ), array( 'a' => 'Spain', 'b' => 'es', 'c' => '+34' ), array( 'a' => 'Sweden', 'b' => 'se', 'c' => '+46' ), array( 'a' => 'Switzerland', 'b' => 'ch', 'c' => '+41' ), array( 'a' => 'UnitedKingdom', 'b' => 'gb', 'c' => '+44' ), array( 'a' => 'UnitedStates', 'b' => 'us', 'c' => '+1' ), array( 'a' => '', 'b' => '', 'c' => '' ), array( 'a' => 'Afghanistan', 'b' => 'af', 'c' => '+93' ), array( 'a' => 'Albania', 'b' => 'al', 'c' => '+355' ), array( 'a' => 'Algeria', 'b' => 'dz', 'c' => '+213' ), array( 'a' => 'Andorra', 'b' => 'ad', 'c' => '+376' ), array( 'a' => 'Angola', 'b' => 'ao', 'c' => '+244' ), array( 'a' => 'Antarctica', 'b' => 'aq', 'c' => '+672' ), array( 'a' => 'Argentina', 'b' => 'ar', 'c' => '+54' ), array( 'a' => 'Armenia', 'b' => 'am', 'c' => '+374' ), array( 'a' => 'Aruba', 'b' => 'aw', 'c' => '+297' ), array( 'a' => 'Austria', 'b' => 'at', 'c' => '+43' ), array( 'a' => 'Azerbaijan', 'b' => 'az', 'c' => '+994' ), array( 'a' => 'Bahrain', 'b' => 'bh', 'c' => '+973' ), array( 'a' => 'Bangladesh', 'b' => 'bd', 'c' => '+880' ), array( 'a' => 'Belarus', 'b' => 'by', 'c' => '+375' ), array( 'a' => 'Belgium', 'b' => 'be', 'c' => '+32' ), array( 'a' => 'Belize', 'b' => 'bz', 'c' => '+501' ), array( 'a' => 'Benin', 'b' => 'bj', 'c' => '+229' ), array( 'a' => 'Bhutan', 'b' => 'bt', 'c' => '+975' ), array( 'a' => 'Bolivia', 'b' => 'bo', 'c' => '+591' ), array( 'a' => 'BosniaandHerzegovina', 'b' => 'ba', 'c' => '+387' ), array( 'a' => 'Botswana', 'b' => 'bw', 'c' => '+267' ), array( 'a' => 'Brazil', 'b' => 'br', 'c' => '+55' ), array( 'a' => 'Brunei', 'b' => 'bn', 'c' => '+673' ), array( 'a' => 'Bulgaria', 'b' => 'bg', 'c' => '+359' ), array( 'a' => 'BurkinaFaso', 'b' => 'bf', 'c' => '+226' ), array( 'a' => 'Burundi', 'b' => 'bi', 'c' => '+257' ), array( 'a' => 'Cambodia', 'b' => 'kh', 'c' => '+855' ), array( 'a' => 'Cameroon', 'b' => 'cm', 'c' => '+237' ), array( 'a' => 'CapeVerde', 'b' => 'cv', 'c' => '+238' ), array( 'a' => 'CentralAfricanRepublic', 'b' => 'cf', 'c' => '+236' ), array( 'a' => 'Chad', 'b' => 'td', 'c' => '+235' ), array( 'a' => 'Chile', 'b' => 'cl', 'c' => '+56' ), array( 'a' => 'ChristmasIsland', 'b' => 'cx', 'c' => '+61' ), array( 'a' => 'CocosIslands', 'b' => 'cc', 'c' => '+61' ), array( 'a' => 'Colombia', 'b' => 'co', 'c' => '+57' ), array( 'a' => 'Comoros', 'b' => 'km', 'c' => '+269' ), array( 'a' => 'CookIslands', 'b' => 'ck', 'c' => '+682' ), array( 'a' => 'CostaRica', 'b' => 'cr', 'c' => '+506' ), array( 'a' => 'Croatia', 'b' => 'hr', 'c' => '+385' ), array( 'a' => 'Cuba', 'b' => 'cu', 'c' => '+53' ), array( 'a' => 'Curacao', 'b' => 'cw', 'c' => '+599' ), array( 'a' => 'Cyprus', 'b' => 'cy', 'c' => '+357' ), array( 'a' => 'CzechRepublic', 'b' => 'cz', 'c' => '+420' ), array( 'a' => 'DemocraticRepCongo', 'b' => 'cd', 'c' => '+243' ), array( 'a' => 'Denmark', 'b' => 'dk', 'c' => '+45' ), array( 'a' => 'Djibouti', 'b' => 'dj', 'c' => '+253' ), array( 'a' => 'EastTimor', 'b' => 'tl', 'c' => '+670' ), array( 'a' => 'Ecuador', 'b' => 'ec', 'c' => '+593' ), array( 'a' => 'Egypt', 'b' => 'eg', 'c' => '+20' ), array( 'a' => 'ElSalvador', 'b' => 'sv', 'c' => '+503' ), array( 'a' => 'EquatorialGuinea', 'b' => 'gq', 'c' => '+240' ), array( 'a' => 'Eritrea', 'b' => 'er', 'c' => '+291' ), array( 'a' => 'Estonia', 'b' => 'ee', 'c' => '+372' ), array( 'a' => 'Ethiopia', 'b' => 'et', 'c' => '+251' ), array( 'a' => 'FalklandIslands', 'b' => 'fk', 'c' => '+500' ), array( 'a' => 'FaroeIslands', 'b' => 'fo', 'c' => '+298' ), array( 'a' => 'Fiji', 'b' => 'fj', 'c' => '+679' ), array( 'a' => 'Finland', 'b' => 'fi', 'c' => '+358' ), array( 'a' => 'FrenchPolynesia', 'b' => 'pf', 'c' => '+689' ), array( 'a' => 'Gabon', 'b' => 'ga', 'c' => '+241' ), array( 'a' => 'Gambia', 'b' => 'gm', 'c' => '+220' ), array( 'a' => 'Georgia', 'b' => 'ge', 'c' => '+995' ), array( 'a' => 'Ghana', 'b' => 'gh', 'c' => '+233' ), array( 'a' => 'Gibraltar', 'b' => 'gi', 'c' => '+350' ), array( 'a' => 'Greece', 'b' => 'gr', 'c' => '+30' ), array( 'a' => 'Greenland', 'b' => 'gl', 'c' => '+299' ), array( 'a' => 'Guatemala', 'b' => 'gt', 'c' => '+502' ), array( 'a' => 'Guernsey', 'b' => 'gg', 'c' => '+44-1481' ), array( 'a' => 'Guinea', 'b' => 'gn', 'c' => '+224' ), array( 'a' => 'Guinea-Bissau', 'b' => 'gw', 'c' => '+245' ), array( 'a' => 'Guyana', 'b' => 'gy', 'c' => '+592' ), array( 'a' => 'Haiti', 'b' => 'ht', 'c' => '+509' ), array( 'a' => 'Honduras', 'b' => 'hn', 'c' => '+504' ), array( 'a' => 'Hungary', 'b' => 'hu', 'c' => '+36' ), array( 'a' => 'Iceland', 'b' => 'is', 'c' => '+354' ), array( 'a' => 'Iran', 'b' => 'ir', 'c' => '+98' ), array( 'a' => 'Iraq', 'b' => 'iq', 'c' => '+964' ), array( 'a' => 'Ireland', 'b' => 'ie', 'c' => '+353' ), array( 'a' => 'IsleofMan', 'b' => 'im', 'c' => '+44-1624' ), array( 'a' => 'Israel', 'b' => 'il', 'c' => '+972' ), array( 'a' => 'IvoryCoast', 'b' => 'ci', 'c' => '+225' ), array( 'a' => 'Jersey', 'b' => 'je', 'c' => '+44-1534' ), array( 'a' => 'Jordan', 'b' => 'jo', 'c' => '+962' ), array( 'a' => 'Kazakhstan', 'b' => 'kz', 'c' => '+7' ), array( 'a' => 'Kenya', 'b' => 'ke', 'c' => '+254' ), array( 'a' => 'Kiribati', 'b' => 'ki', 'c' => '+686' ), array( 'a' => 'Kosovo', 'b' => 'xk', 'c' => '+383' ), array( 'a' => 'Kuwait', 'b' => 'kw', 'c' => '+965' ), array( 'a' => 'Kyrgyzstan', 'b' => 'kg', 'c' => '+996' ), array( 'a' => 'Laos', 'b' => 'la', 'c' => '+856' ), array( 'a' => 'Latvia', 'b' => 'lv', 'c' => '+371' ), array( 'a' => 'Lebanon', 'b' => 'lb', 'c' => '+961' ), array( 'a' => 'Lesotho', 'b' => 'ls', 'c' => '+266' ), array( 'a' => 'Liberia', 'b' => 'lr', 'c' => '+231' ), array( 'a' => 'Libya', 'b' => 'ly', 'c' => '+218' ), array( 'a' => 'Liechtenstein', 'b' => 'li', 'c' => '+423' ), array( 'a' => 'Lithuania', 'b' => 'lt', 'c' => '+370' ), array( 'a' => 'Luxembourg', 'b' => 'lu', 'c' => '+352' ), array( 'a' => 'Macao', 'b' => 'mo', 'c' => '+853' ), array( 'a' => 'Macedonia', 'b' => 'mk', 'c' => '+389' ), array( 'a' => 'Madagascar', 'b' => 'mg', 'c' => '+261' ), array( 'a' => 'Malawi', 'b' => 'mw', 'c' => '+265' ), array( 'a' => 'Maldives', 'b' => 'mv', 'c' => '+960' ), array( 'a' => 'Mali', 'b' => 'ml', 'c' => '+223' ), array( 'a' => 'Malta', 'b' => 'mt', 'c' => '+356' ), array( 'a' => 'MarshallIslands', 'b' => 'mh', 'c' => '+692' ), array( 'a' => 'Mauritania', 'b' => 'mr', 'c' => '+222' ), array( 'a' => 'Mauritius', 'b' => 'mu', 'c' => '+230' ), array( 'a' => 'Mayotte', 'b' => 'yt', 'c' => '+262' ), array( 'a' => 'Mexico', 'b' => 'mx', 'c' => '+52' ), array( 'a' => 'Micronesia', 'b' => 'fm', 'c' => '+691' ), array( 'a' => 'Moldova', 'b' => 'md', 'c' => '+373' ), array( 'a' => 'Monaco', 'b' => 'mc', 'c' => '+377' ), array( 'a' => 'Mongolia', 'b' => 'mn', 'c' => '+976' ), array( 'a' => 'Montenegro', 'b' => 'me', 'c' => '+382' ), array( 'a' => 'Morocco', 'b' => 'ma', 'c' => '+212' ), array( 'a' => 'Mozambique', 'b' => 'mz', 'c' => '+258' ), array( 'a' => 'Namibia', 'b' => 'na', 'c' => '+264' ), array( 'a' => 'Nauru', 'b' => 'nr', 'c' => '+674' ), array( 'a' => 'Nepal', 'b' => 'np', 'c' => '+977' ), array( 'a' => 'Netherlands', 'b' => 'nl', 'c' => '+31' ), array( 'a' => 'NetherlandsAntilles', 'b' => 'an', 'c' => '+599' ), array( 'a' => 'NewCaledonia', 'b' => 'nc', 'c' => '+687' ), array( 'a' => 'Nicaragua', 'b' => 'ni', 'c' => '+505' ), array( 'a' => 'Niger', 'b' => 'ne', 'c' => '+227' ), array( 'a' => 'Nigeria', 'b' => 'ng', 'c' => '+234' ), array( 'a' => 'Niue', 'b' => 'nu', 'c' => '+683' ), array( 'a' => 'NorthKorea', 'b' => 'kp', 'c' => '+850' ), array( 'a' => 'Norway', 'b' => 'no', 'c' => '+47' ), array( 'a' => 'Oman', 'b' => 'om', 'c' => '+968' ), array( 'a' => 'Pakistan', 'b' => 'pk', 'c' => '+92' ), array( 'a' => 'Palau', 'b' => 'pw', 'c' => '+680' ), array( 'a' => 'Palestine', 'b' => 'ps', 'c' => '+970' ), array( 'a' => 'Panama', 'b' => 'pa', 'c' => '+507' ), array( 'a' => 'PapuaNewGuinea', 'b' => 'pg', 'c' => '+675' ), array( 'a' => 'Paraguay', 'b' => 'py', 'c' => '+595' ), array( 'a' => 'Peru', 'b' => 'pe', 'c' => '+51' ), array( 'a' => 'Pitcairn', 'b' => 'pn', 'c' => '+64' ), array( 'a' => 'Poland', 'b' => 'pl', 'c' => '+48' ), array( 'a' => 'Portugal', 'b' => 'pt', 'c' => '+351' ), array( 'a' => 'Qatar', 'b' => 'qa', 'c' => '+974' ), array( 'a' => 'RepublicCongo', 'b' => 'cg', 'c' => '+242' ), array( 'a' => 'Reunion', 'b' => 're', 'c' => '+262' ), array( 'a' => 'Romania', 'b' => 'ro', 'c' => '+40' ), array( 'a' => 'Rwanda', 'b' => 'rw', 'c' => '+250' ), array( 'a' => 'SaintBarthelemy', 'b' => 'bl', 'c' => '+590' ), array( 'a' => 'SaintHelena', 'b' => 'sh', 'c' => '+290' ), array( 'a' => 'SaintMartin', 'b' => 'mf', 'c' => '+590' ), array( 'a' => 'Samoa', 'b' => 'ws', 'c' => '+685' ), array( 'a' => 'SanMarino', 'b' => 'sm', 'c' => '+378' ), array( 'a' => 'SaudiArabia', 'b' => 'sa', 'c' => '+966' ), array( 'a' => 'Senegal', 'b' => 'sn', 'c' => '+221' ), array( 'a' => 'Serbia', 'b' => 'rs', 'c' => '+381' ), array( 'a' => 'Seychelles', 'b' => 'sc', 'c' => '+248' ), array( 'a' => 'SierraLeone', 'b' => 'sl', 'c' => '+232' ), array( 'a' => 'Slovakia', 'b' => 'sk', 'c' => '+421' ), array( 'a' => 'Slovenia', 'b' => 'si', 'c' => '+386' ), array( 'a' => 'SolomonIslands', 'b' => 'sb', 'c' => '+677' ), array( 'a' => 'Somalia', 'b' => 'so', 'c' => '+252' ), array( 'a' => 'SouthAfrica', 'b' => 'za', 'c' => '+27' ), array( 'a' => 'SouthSudan', 'b' => 'ss', 'c' => '+211' ), array( 'a' => 'SriLanka', 'b' => 'lk', 'c' => '+94' ), array( 'a' => 'Sudan', 'b' => 'sd', 'c' => '+249' ), array( 'a' => 'Suriname', 'b' => 'sr', 'c' => '+597' ), array( 'a' => 'Swaziland', 'b' => 'sz', 'c' => '+268' ), array( 'a' => 'Syria', 'b' => 'sy', 'c' => '+963' ), array( 'a' => 'Taiwan', 'b' => 'tw', 'c' => '+886' ), array( 'a' => 'Tajikistan', 'b' => 'tj', 'c' => '+992' ), array( 'a' => 'Tanzania', 'b' => 'tz', 'c' => '+255' ), array( 'a' => 'Togo', 'b' => 'tg', 'c' => '+228' ), array( 'a' => 'Tokelau', 'b' => 'tk', 'c' => '+690' ), array( 'a' => 'Tonga', 'b' => 'to', 'c' => '+676' ), array( 'a' => 'Tunisia', 'b' => 'tn', 'c' => '+216' ), array( 'a' => 'Turkey', 'b' => 'tr', 'c' => '+90' ), array( 'a' => 'Turkmenistan', 'b' => 'tm', 'c' => '+993' ), array( 'a' => 'Tuvalu', 'b' => 'tv', 'c' => '+688' ), array( 'a' => 'Uganda', 'b' => 'ug', 'c' => '+256' ), array( 'a' => 'Ukraine', 'b' => 'ua', 'c' => '+380' ), array( 'a' => 'UnitedArabEmirates', 'b' => 'ae', 'c' => '+971' ), array( 'a' => 'Uruguay', 'b' => 'uy', 'c' => '+598' ), array( 'a' => 'Uzbekistan', 'b' => 'uz', 'c' => '+998' ), array( 'a' => 'Vanuatu', 'b' => 'vu', 'c' => '+678' ), array( 'a' => 'Vatican', 'b' => 'va', 'c' => '+379' ), array( 'a' => 'Venezuela', 'b' => 've', 'c' => '+58' ), array( 'a' => 'WallisandFutuna', 'b' => 'wf', 'c' => '+681' ), array( 'a' => 'WesternSahara', 'b' => 'eh', 'c' => '+212' ), array( 'a' => 'Yemen', 'b' => 'ye', 'c' => '+967' ), array( 'a' => 'Zambia', 'b' => 'zm', 'c' => '+260' ), array( 'a' => 'Zimbabwe', 'b' => 'zw', 'c' => '+263' ) );

if(empty($bkcountry)) {
	$bkcountry = "Singapore";
	}

$phoneIndex = "";
$startingPage = (empty($bkpage)) ? 1 : 0;
	
if(empty($bkpage)) {
	$bkpage = 1;
}

$require_deposit = 0;
if($res->checkbkdeposit() > 0) {
	$require_deposit = 1;
	$dontnotify = '';
	}

?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<html><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv='expires' content='mon, 11 oct 2016 05:00:00 gmt'>
<meta http-equiv='pragma' content='no-cache'>
<meta http-equiv='pragma' content='cache-control: max-age=0'>
<meta http-equiv='last-modified' content='mon, 11 oct 2016 05:00:00 gmt'>
<meta http-equiv='cache-control' content='no-cache, must-revalidate'>
<meta name='robots' content='noindex, nofollow'>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="HandheldFriendly" content="true" />
<title>Book your table now - Weeloy Code - Weeloy.com</title>

<base href="<?php echo __ROOTDIR__; ?>/" />


<link href="client/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="client/bower_components/bootstrap/dist/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
<link href="client/bower_components/jquery-ui/themes/base/jquery-ui.css" rel="stylesheet" type="text/css">
<link href="client/bower_components/fontawesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

<link href="css/bootstrap-select.css" rel="stylesheet" >
<link href="css/style.css" rel="stylesheet" >
<link href="css/famfamfam-flags.css" rel="stylesheet" >
<link href="css/dropdown.css" rel="stylesheet" >
<link href="css/bookstyle.css" rel="stylesheet" >
<script type="text/javascript" src="client/bower_components/jquery/dist/jquery.min.js"></script>
<script type='text/javascript' src="client/bower_components/jquery-ui/jquery-ui.js"></script>
<script type='text/javascript' src="client/bower_components/angular/angular.min.js"></script>
<script type="text/javascript" src="client/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="client/bower_components/ui-bootstrap-tpls-0.14.2.min.js"></script>
<script type="text/javascript" src="js/bootstrap-select.js"></script>
<script type="text/javascript" src="js/ngStorage.min.js"></script>
<script type="text/javascript" src="js/dayroutine.js"></script>
<style>
.checkbox-inline{
	padding-top: 0px!important;
}
.reduce { height:18px; font-size:12px; }

</style>

<?php include_once("ressources/analyticstracking.php") ?>

</head>

<body id="MainCallCenter" ng-app="myApp" ng-controller='MainController' ng-init="typeBooking = true;" ng-cloak style="padding-top:5px" >
<form class='form-horizontal' action='<?php echo $actionfile ?>' role='form' id='book_form' name='book_form' method='POST' enctype='multipart/form-data'>

	<div id='booking'>
		<input type='hidden' id='bkrestaurant' name='bkrestaurant' value='<?php echo $bkrestaurant ?>'>
		<input type='hidden' id='bkpage' name='bkpage' value='<?php echo $bkpage ?>'>
		<input type='hidden' id='bktitle' name='bktitle' value='<?php echo $bktitle ?>'>
		<input type='hidden' id='bkparam' name='bkparam' value='<?php echo $bkparam ?>'>
		<input type='hidden' id='bktracking' name='bktracking' value='<?php echo $bktracking ?>'>
		<input type='hidden' id='brwsr_type' name='brwsr_type' value='<?php echo $brwsr_type ?>'>
		<input type='hidden' id='bkextra' name='bkextra' value='<?php echo $bkextra ?>'>
		<input type='hidden' id='data' name='data' value='<?php echo $data ?>'>
		<input type='hidden' id='bkdate' name='bkdate' value=''>
		<input type='hidden' id='bktms' name='bktms' value='<?php echo $bktms ?>'>
		<input type='hidden' id='genflag' name='genflag' value={{genflag}}>
		<div id='extrahidden'></div>

	<div class="container mainbox">    
		<div class="panel panel-info">
			<div class="panel-heading">
				<div class="panel-title" > {{ bookingTitle}} {{ bkerror }}</div>
			</div>     
			<div style="padding-top:5px" class="panel-body" >

				<div class='col-xs-12' ng-if="tmsflg !== '87';">
					<center><img ng-src='{{imglogo}}' max-width='80' max-height='80' id='theLogo' name='theLogo' style="width:80px;"></center>				
					<center ng-if="profileflg"><button type='button' class='btn btn-primary btn-sm' ng-click='viewprofile()' style='font-size:12px;'>CUSTOMER PROFILE</button></center>
					<hr />
				</div>
				<div ng-show="simplecallcenter === false">
				<div class='col-xs-6'>RESTAURANTS<br/><?php echo $selectresto ?></div>
				<div class='col-xs-6'>STAFF<br/><?php echo $selectbooker ?></div>
				<div class='col-xs-12'>&nbsp;</div>
				</div>

				<div ng-show="simplecallcenter === false" class='col-xs-6'>
					<div class="input-group input-group-sm">
						<span class="input-group-addon" ng-click="open($event)"><i class="glyphicon glyphicon-calendar"></i> &nbsp; <span class='caret'></span></span>
						<input type="text" readonly class="form-control input-sm" id='ngdate' name='ngdate' uib-datepicker-popup="{{format}}" ng-model="ngdate" ng-focus="open($event)" 
						is-open="start_opened" min-date="minDate" max-date="maxDate" datepicker-options="dateOptions" date-disabled="disabled(date, mode)" 
						ng-required="true" ng-change='updatendays();' close-text="Close" readonly />
					</div>

					<div class='input-group input-group-sm'>
						<div class='input-group-btn' uib-dropdown >
							<button type='button' id='itemdftime' class='btn btn-default btn-sm' uib-dropdown-toggle >
								&nbsp;<i class="glyphicon glyphicon-time"></i> &nbsp; <span class='caret'></span>
							</button>
								<ul class='dropdown-menu scrollable-menu' uib-dropdown-menu >
									<li style='color:#5285a0'>&nbsp; Lunch  &nbsp; </li>
									<li ng-repeat="x in lunch"><a href="javascript:;" ng-click="setmealtime(x, 1);">{{ x}}</a></li>
									<li>&nbsp;  -------------------- </li>
									<li style='color:#5285a0'>&nbsp;  Dinner  &nbsp; </li>
									<li ng-repeat="y in dinner"><a href="javascript:;" ng-click="setmealtime(y, 1);">{{ y}}</a></li>
								</ul>
						</div>
						<input type='text' ng-model='bktime' value='' class='form-control input-sm' id='bktime' name='bktime' readonly>
					</div>

					<div class='input-group input-group-sm'>
						<div class='input-group-btn' uib-dropdown >
							<button type='button' id='itemdfminpers' class='btn btn-default btn-sm' uib-dropdown-toggle >
								&nbsp;<i class="glyphicon glyphicon-cutlery"></i>&nbsp; Guests <span class='caret'></span>
							</button>
							<ul class='dropdown-menu scrollable-menu' uib-dropdown-menu >
								<li ng-repeat="p in npers"><a href ng-click="setmealtime(p, 2);">{{ p }}</a></li>
							</ul></div>
						<input type='text' ng-model='bkcover' class='form-control input-sm' id='bkcover' name='bkcover' readonly >
					</div>
					<div class="input-group">
						<span class="input-group-addon"><i class="glyphicon glyphicon-info-sign"></i></span>
						<textarea type="text" ng-model='bkspecialrequest' class="form-control input-sm" id='bkspecialrequest' name='bkspecialrequest' rows="1"  placeholder="Special requests" ng-change='bkspecialrequest=cleantext(bkspecialrequest);'></textarea>
					</div>

					<div class="input-group">
						<span class="input-group-addon"><i class="glyphicon glyphicon-info-sign"></i></span>
						<textarea type="text" ng-model='bknotes' class="form-control input-sm" id='bknotes' name='bknotes' rows="1"  placeholder="restaurant notes" ng-change='bknotes=cleantext(bknotes);'></textarea>
					</div>

					<div class="input-group input-group-sm" style="margin-bottom: 0;">
						<span class="input-group-addon"><i class="glyphicon glyphicon-flag"></i></span>
						<input type="text" ng-model='company' class="form-control input-sm" id='company' name='company' placeholder="Company" ng-change='company=cleantext(company);'>                                        
					</div>
					<div>&nbsp;</div>
					<div class="form-inline input-sm reduce"  ng-if="limited !== '1'">
						<div class="input-group">
							<label class="checkbox-inline"><input type="checkbox" value="1" id='walkin' name='walkin' ng-checked="wkinflg == 1" ng-click='walkin()'><strong>walk-in</strong></label>
						</div>
					</div>
					<div class="form-inline input-sm reduce" ng-if="genflagfunc('0x2000')" >
						<div class="input-group">
							<label class="checkbox-inline"><input type="checkbox" value="2" id='waiting' name='waiting' ng-checked="wtngflg == 2" ng-click='waiting()'><strong>waiting-list</strong></label>
						</div>
					</div>
					<div class="form-inline input-sm reduce"  >
						<div class="input-group">
							<label class="checkbox-inline"><input type="checkbox" value="17" id='dontnotify' name='dontnotify' class='dontnotify'>don`t notify</label>
						</div>
					</div>
					<div ng-if="forceit" class="form-inline input-sm reduce">
						<div class="input-group">
							<label class="checkbox-inline"><input type="checkbox" value="17" ng-click='prefill()'>force info</label>
						</div>
					</div>
					<div  class="form-inline input-sm reduce" ng-if="limited !== '1'" >
						<div class="input-group">
							<label class="checkbox-inline"><input type="checkbox" ng-value="1" ng-checked="overwrite === 1" ng-click='setoverwrite()'>overwrite</label>
						</div>
					</div>
					<div ng-if="tmsflg === '87' && profileflg" style="margin-top: 15px;">
						<button type='button' class='btn btn-primary btn-xs' ng-click='viewprofile()' style='font-size:12px;'>CUSTOMER PROFILE</button>
					</div>

					<div class="form-inline input-sm reduce" ng-show="require_deposit === '1'">
						<div class="input-group">
							<label class="checkbox-inline"><input type="checkbox" id='ccrequired' ng-model ='ccrequired' name='ccrequired' ng-true-value="'1'" ng-checked ='ccrequired' ng-change="chknotify();" ng-value="1">Require Credit card</label>
						</div>
					</div>

				</div>

				<div class='col-xs-6'>
<?php if ($bkrestaurant == "SG_SG_R_MadisonRooms") : ?>
					<div class="input-group input-group-sm" style='width:100%'>
						<div class='input-group-btn' uib-dropdown is-open="madisonemail">
							<button type='button' class='btn btn-default btn-sm' uib-dropdown-toggle  style='color:red;'>
								&nbsp;<i class="glyphicon glyphicon-envelope"></i>&nbsp; <span class='caret'></span>
							</button>
							<ul class='dropdown-menu' uib-dropdown-menu >
								<li ng-repeat="s in allemail = (allemailorg | filter:bkemail) track by $index" style="font-size:10px"><a href ng-click="setvalue('bkemail', s);">{{ s}}</a></li>
							</ul></div>
						<input type='text' ng-model='bkemail' class='form-control input-sm' id='bkemail' name='bkemail' placeholder="email" autocomplete="off">
					</div>

<?php else : ?>
					<div class="input-group input-group-sm">
						<span class="input-group-addon"><a href ng-click="checkprevemail(1)" style='color:red;'><i class="glyphicon glyphicon-envelope"></i></a></span>
						<input type="text" ng-model='bkemail' class="form-control input-sm" id='bkemail' name='bkemail' placeholder="email" ng-change='bkemail=cleanemail(bkemail);' autocomplete="off">                                        
					</div>
<?php endif; ?>

					<div class="input-group input-group-sm">
						<div class='input-group-btn' uib-dropdown>
							<button type='button' id='itemdfsalut' class='btn btn-default btn-sm' uib-dropdown-toggle >
								&nbsp;<i class="glyphicon glyphicon-tag"></i>&nbsp; <span class='caret'></span>
							</button>
							<ul class='dropdown-menu' uib-dropdown-menu >
								<li ng-repeat="s in salutations"><a href ng-click="setmealtime(s, 3);">{{ s}}</a></li>
							</ul></div>
						<input type='text' ng-model='bksalutation' class='form-control input-sm' id='bksalutation' name='bksalutation' placeholder="title" readonly >
					</div>

					<div class="input-group input-group-sm">
						<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
						<input type="text" ng-model='bkfirst' class="form-control input-sm" id='bkfirst' name='bkfirst' placeholder="firstname" ng-change='bkfirst=cleantext(bkfirst);' autocomplete="off">                                        
					</div>

					<div class="input-group input-group-sm">
						<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
						<input type="text" ng-model='bklast' class="form-control input-sm" id='bklast' name='bklast' placeholder="lastname" ng-change='bklast=cleantext(bklast);' autocomplete="off">                                        
					</div>

					<div class="input-group input-group-sm">
						<div class='input-group-btn' uib-dropdown >
							<button type='button' id='itemdfcountry' class='btn btn-default btn-sm' uib-dropdown-toggle >
								&nbsp;<i class='{{curflgicon}}'></i>&nbsp;<span class='caret'></span>
							</button>
							<ul class='dropdown-menu scrollable-menu' uib-dropdown-menu style='font-size:12px;'>
								<li ng-repeat="s in countries"><a ng-if="s.b != ''" href ng-click="setmealtime(s.a, 4);" ><i class='famfamfam-flag-{{s.b}}'></i> {{s.a}}</a><hr ng-if="s.b == '';" /></li>
							</ul></div>
						<input type='text' ng-model='bkcountry' class='form-control input-sm' id='bkcountry' name='bkcountry' readonly >
					</div>

<?php if ($bkrestaurant == "SG_SG_R_MadisonRooms") : ?>
					<div class="input-group input-group-sm" style='margin-bottom:5px;width:100%'>
						<div class='input-group-btn' uib-dropdown is-open="madisonmobile">
							<button type='button' class='btn btn-default btn-sm' uib-dropdown-toggle style='color:red;'>
								&nbsp;<i class="glyphicon glyphicon-earphone"></i>&nbsp; <span class='caret'></span>
							</button>
							<ul class='dropdown-menu' uib-dropdown-menu >
								<li ng-repeat="s in allmobile = (allmobileorg | filter:bkmobile) track by $index" style="font-size:10px"><a href ng-click="setvalue('bkmobile', s);">{{ s}}</a></li>
							</ul></div>
						<input type='text' ng-model='bkmobile' class='form-control input-sm' id='bkmobile' name='bkmobile' placeholder="mobile" autocomplete="off">
					</div>

<?php else : ?>
					<div class="input-group" style='margin-bottom:5px;'>
						<span class="input-group-addon"><a href ng-click="checkprevemail(2)" style='color:red;'><i class="glyphicon glyphicon-earphone"></i></a></span>
						<input type="text" ng-model='bkmobile' class="form-control input-sm" id='bkmobile' name='bkmobile' placeholder="mobile" ng-change='bkmobile=cleantel(bkmobile);' ng-blur='checkvalidtel();' autocomplete="off">                            
					</div>
<?php endif; ?>
					
					<table ng-if="showarea && showpurpose" class="nav navbar-nav" style='margin-left:0px;'><tr>
					<td><select ng-model="bkarea" id='bkarea' ng-options="p for p in areaobj.data" style="width:80px" ng-change="setselect(bkarea, 'area');"><option value="">area </option></select></td><td width='10'>&nbsp; </td>
					<td><select id='bkpurpose' ng-model="bkpurpose" ng-options="p for p in purposeobj.data" style="width:80px" ng-change="setselect(bkpurpose, 'purpose');"><option value="">purpose </option></select></td>
					</tr><tr><td colspan='2'>&nbsp;</td></tr></table>

					<div ng-show="simplecallcenter" class='input-group input-group-sm' style='margin-top:15px'>
						<div class='input-group-btn' uib-dropdown >
							<button type='button' class='btn btn-default btn-sm' uib-dropdown-toggle >
								&nbsp;<i class="glyphicon glyphicon-cutlery"></i> <span class='caret'></span>
							</button>
							<ul class='dropdown-menu scrollable-menu' uib-dropdown-menu >
								<li ng-repeat="p in npers"><a href ng-click="setpax(p);">{{ p }}</a></li>
							</ul>
						</div>
						<input type='text' ng-model='bkpax' class='form-control input-sm' id='pax' name='pax' readonly >
					</div>
					
					<div ng-show="simplecallcenter && forceit" class="form-inline input-sm reduce">
					<div class="input-group">
						<label class="checkbox-inline"><input type="checkbox" value="17" ng-click='prefill()'>force info</label>
					</div>
					</div>
					<div ng-show="simplecallcenter" class="form-inline input-sm reduce" >
					<div class="input-group">
						<label class="checkbox-inline"><input type="checkbox" id='dontnotify1' name='dontnotify1' class='dontnotify'>don`t notify</label>
					</div>
					</div>

					</div>

					<div class="input-group xls"  style='margin-bottom:5px;' ng-if="notesflag && notesflag === 1">
					<label ng-repeat="oo in notescode | filter: { label:'!hide'}" style="font-size:12px;margin-left:10px;"><input type="checkbox" ng-model="oo.value" /> {{oo.label}} </label>
					</div>

					<div>
						<button type='button' ng-click='checkSubmit(1);' class="btn {{buttonClass}}"> {{ buttonLabel}} </button>
						<button ng-if="fixlineflg" type='button' class='btn btn-info btn-xs' ng-click='pabx()' style='font-size:11px;margin-left:20px;'>PABX</button>
					</div>

			</div>
			</div>  
		</div>
	</div>

	</div>

</form>

<script> var app = angular.module("myApp", ['ui.bootstrap', 'ngLocale', 'ngStorage']); </script>
<script type="text/javascript" src="modules/booking/book_form.js?21"></script>
<script type="text/javascript" src="backoffice/inc/profilelib.js?21"></script> 
<script type="text/javascript" src="backoffice/inc/libService.js?21"></script> 

<?php
include("../../backoffice/profileTemplates.html");
?>

<script>

<?php
echo "var tracking = '$bktracking';";
echo "var limited = '$limited';";
printf("var fullfeature = %s;", ($fullfeature) ? "true" : "false"); 
echo "var restaurant = '$bkrestaurant';";
echo "var restogroup = '$restogroup';";
echo "var genflag = '$genflag';";
echo "var tokendata = '$tokendata';";
printf("var token = '%s';", $_SESSION['user_backoffice']['token']);
echo "var minpax = $minpax;";
echo "var maxpax = $maxpax;";
echo "var maxpaxcc = 99;";
echo "var cstate = '$cstate';";

echo "var curndays = '$ndays';";
echo "var bkdate = '$bkdate';";
echo "var bktime = '$bktime';";
echo "var bkjsDate = '$bkjsDate';";
echo "var todaydate = '$todaydate';";
echo "var todaytime = '$todaytime';";

echo "var curcountry = '$bkcountry';";
echo "var typeBooking = $typeBooking;";
echo "var timeout='$timeout';";
echo "var brwsr_type = '$brwsr_type';";
echo "var imglogo = '$logo';";
echo "var AvailperPax = '$AvailperPax';";	// true or false
echo "var startingPage = '$startingPage';";	// true or false
echo "var bktms = '$bktms';";	// true or false
echo "var fixline = '$fixline';";	// true or false


echo "var require_deposit = '$require_deposit';";


printf("var bkextra = '%s';", $bkextra);

printf("var is_listing = %s;", $is_listing ? 'true' : 'false');
printf("var remainingday = %d;", $bookingwindow - 1);	// 90 or 180 days

echo "var arglist = [";
$sep = "";
for (reset($arglist); list($index, $label) = each($arglist); $sep = ", ") {
    echo $sep . "'" . $label . "', '" . $$label . "'";
}
echo "];";

?>

Array.prototype.searchFor = function(needle) {
    for (var i=0; i<this.length; i++)
        if (this[i].indexOf(needle) == 0)
            return i;
    return -1;
};

String.prototype.datereverse = function() {

	if(this.length !== 10 || this.indexOf('-') === -1) 
		return this;
	s = this.substring(0, 10);
	tt = s.split('-');
	return tt[2] + "-" + tt[1] + "-" + tt[0];
	};

$(document).ready(function() { 	$('.selectpicker').selectpicker(); });

function triggermodifyprofile(token, resto, id, email, phone) { 
	var scope = angular.element(document.getElementById("MainCallCenter")).scope();
	scope.$apply(function () { scope.updateCustomProfile(token, resto, id, email, phone); });
    }

app.controller('MainController', ['$scope', '$http', '$location', '$filter', 'HttpServiceAvail', 'profileService', 'ModalService', function($scope, $http, $location, $filter, HttpServiceAvail, profileService, ModalService) {

	$scope.restaurant = restaurant;
	$scope.platform = "CALLCENTER";
	$scope.limited = limited;
	$scope.fixline = fixline;
	$scope.fixlineflg = (typeof fixline === "string" && fixline.length > 7);
	$scope.onlyonce = true;
	$scope.email = "call@center.com";
	$scope.restogroup = (typeof restogroup === "string" && restogroup.length > 1 && restogroup.search(",") > 0) ? restogroup : "";
	$scope.cstate = cstate;
	$scope.forceit = (cstate ==='new');
	$scope.imglogo = imglogo;
	$scope.tmsflg = bktms;
	$scope.available = 1;
	$scope.overwrite = (typeof tracking === "string" && tracking.length > 1 && tracking.search("overwrite") > -1) ? 1 : 0;
	$scope.wkinflg = $scope.wtngflg = 0;
	$scope.genflag = parseInt(genflag) & ~0xfff;
	$scope.bookingTitle = "CALL CENTER";
	if(timeout == "y") $scope.bookingTitle = $scope.bookingTitle + " (time out)";
	$scope.buttonLabel = (typeBooking) ? 'BOOK NOW' : 'REQUEST YOUR WEELOY CODE';
	$scope.buttonClass = (typeBooking) ? 'btn-danger' : 'btn-green';
	$scope.listTags = (typeBooking) ? "Instant Reservation" : "No reservartion";
	$scope.listTags2 = (typeBooking) ? "" : "First come, first serve";
	$scope.listTags3 = (!is_listing) ? "Spin the Wheel" : "Enjoy incredible";
	$scope.listTags4 = (!is_listing) ? "at the restaurant" : " promotion";
	var mydata = new ModalService.ModalDataBooking();
	$scope.mydata = mydata;
	$scope.usremail = <?php printf("'%s';", $_SESSION['user_backoffice']['email']); ?>
	$scope.usrtoken = <?php printf("'%s';", $_SESSION['user_backoffice']['token']); ?>

	$scope.codebooking = null;
	$scope.notescode = [];
	$scope.notesflag = 0;
	$scope.bknotescode = "";

	$scope.profilwind = null;
	$scope.profileflg = false;
	
	$scope.simplecallcenter = false;
	if(AvailperPax === '1' && startingPage == "1") {
		$scope.bkcover = "";
		$scope.available = 0;
		}

	$scope.bkmobile = "";
	$scope.bkemail = "";
	$scope.bkproduct = "";
	$scope.pax = 1;
	$scope.bktime = bktime; // in case it is closed
	$scope.bkdate = bkdate; // in case it is closed
	$scope.todaydate = todaydate;
	$scope.todaytime = todaytime;
        
	$scope.require_deposit = require_deposit;
	$scope.ccrequired = $scope.require_deposit;
	console.log("DSADs"+$scope.ccrequired);
	$scope.bkextra = bkextra;
	$scope.bkarea = $scope.bkpurpose = "";
	
	$scope.listarea = <? printf("'%s';", $listarea); ?>
	$scope.listpurpose = <? printf("'%s';", $listpurpose); ?>
		
	for(var i = 0; i < arglist.length; i += 2) 
	  if (arglist[i + 1] != '') {
		if(arglist[i] === "dontnotify")
                        
			$('.' + arglist[i]).click();
		else if($('#' + arglist[i]).is(':checkbox'))
			$('#' + arglist[i]).click();
		else $scope[arglist[i]] = arglist[i + 1];
		}
             
      
	if($scope.bkerror && typeof $scope.bkerror === "string" && $scope.bkerror !== "")
		$scope.bkerror = "( " + $scope.bkerror + " )";

	//$scope.autocomplete = ($scope.restaurant === 'SG_SG_R_MadisonRooms');
	$scope['madisonemail'] = $scope['madisonmobile'] = false;
	$scope.setvalue = function(label, val) {
		$scope[label] = val;
		if(label === 'bkmobile') $scope.checkprevemail(2);
		else $scope.checkprevemail(1);
		};
			
	$scope.getResto = getResto;
	$scope.switchResto = switchResto;

	$scope.filtervalue = function(type) {
		if(type === 1) 
			$scope.allemail = $filter('filter')($scope.allemailorg, $scope.bkemail);
		else if(type === 2) 
			$scope.allmobile = $filter('filter')($scope.allmobileorg, $scope.bkmobile);
		}
				
	$scope.getselection = function(data) {
		var tmp, label, oo;
		
		tmp = data.split(",");
		label = tmp[0];
		tmp.splice(0, 1);
		oo = { title: label, data: tmp.slice(0) }
		return oo;
		};
		
	if($scope.bkextra !== "") {
		tmp = $scope.bkextra.split("|");
		i = tmp.searchFor('area=') ;
		if(i >= 0) $scope.bkarea = tmp[i].replace('area=', '');
		i = tmp.searchFor('purpose=') ;
		if(i >= 0) $scope.bkpurpose = tmp[i].replace('purpose=', '');
		i = tmp.searchFor('notescode=') ;
		if(i >= 0) $scope.bknotescode = tmp[i].replace('notescode=', '');
		}


	$scope.chknotify = function() {
		if ($scope.ccrequired === '1') {
			$(".dontnotify").attr("disabled", true);
			$(".dontnotify").attr('checked', false);
		}else{
			$(".dontnotify").attr("disabled", false);
		}

	};

	if($scope.ccrequired === '1'){
		  $(".dontnotify").attr("disabled", true);
	}
               
	if($scope.restaurant === "SG_SG_R_MadisonRooms") {
		HttpServiceAvail.readmadisonmember($scope.restaurant).then(function(response) {
			var i;
			$scope.allemailorg = [];
			$scope.allmobileorg = [];
			for(i = 0; i < response.data.mobiles.length; i++)
				$scope.allmobileorg.push(response.data.mobiles[i].mobilephone);
			for(i = 0; i < response.data.emails.length; i++)
				$scope.allemailorg.push(response.data.emails[i].email);
			//console.log('SG_SG_R_MadisonRooms', $scope.allemailorg, $scope.allmobileorg);
			});
		if($scope.listarea !== "") {
			$scope.showarea = true;
			$scope.areaobj = $scope.getselection($scope.listarea);
			}
		if($scope.listpurpose !== "") {
			$scope.showpurpose = true;
			$scope.purposeobj = $scope.getselection($scope.listpurpose);
			}
		}
	
	$scope.bkpax = minpax;
	$scope.curday = 0;
	$scope.lunchdata = ""; 
	$scope.dinnerdata = ""; 
	$scope.npers = [];
	for(i = minpax; i <= maxpax; i++) $scope.npers.push(String(i));
	if(maxpax === 30)
		$scope.npers.push('more...');
	
	$scope.salutations = ['Mr.', 'Mrs.', 'Ms.', 'Dr.', 'Mdm.'];
	$scope.countries = [ { 'a':'Australia', 'b':'au', 'c':'+61' }, { 'a':'China', 'b':'cn', 'c':'+86' }, { 'a':'Hong Kong', 'b':'hk', 'c':'+852' }, { 'a':'India', 'b':'in', 'c':'+91' }, { 'a':'Indonesia', 'b':'id', 'c':'+62' }, { 'a':'Japan', 'b':'jp', 'c':'+81' }, { 'a':'Malaysia', 'b':'my', 'c':'+60' }, { 'a':'Myanmar', 'b':'mm', 'c':'+95' }, { 'a':'New Zealand', 'b':'nz', 'c':'+64' }, { 'a':'Philippines', 'b':'ph', 'c':'+63' }, { 'a':'Singapore', 'b':'sg', 'c':'+65' }, { 'a':'South Korea', 'b':'kr', 'c':'+82' }, { 'a':'Thailand', 'b':'th', 'c':'+66' }, { 'a':'Vietnam', 'b':'vn', 'c':'+84' }, { 'a':'', 'b':'', 'c':'' }, { 'a':'Canada', 'b':'ca', 'c':'+1' }, { 'a':'France', 'b':'fr', 'c':'+33' }, { 'a':'Germany', 'b':'de', 'c':'+49' }, { 'a':'Italy', 'b':'it', 'c':'+39' }, { 'a':'Russia', 'b':'ru', 'c':'+7' }, { 'a':'Spain', 'b':'es', 'c':'+34' }, { 'a':'Sweden', 'b':'se', 'c':'+46' }, { 'a':'Switzerland', 'b':'ch', 'c':'+41' }, { 'a':'UnitedKingdom', 'b':'gb', 'c':'+44' }, { 'a':'UnitedStates', 'b':'us', 'c':'+1' }, { 'a':'', 'b':'', 'c':'' }, { 'a':'Afghanistan', 'b':'af', 'c':'+93' }, { 'a':'Albania', 'b':'al', 'c':'+355' }, { 'a':'Algeria', 'b':'dz', 'c':'+213' }, { 'a':'Andorra', 'b':'ad', 'c':'+376' }, { 'a':'Angola', 'b':'ao', 'c':'+244' }, { 'a':'Antarctica', 'b':'aq', 'c':'+672' }, { 'a':'Argentina', 'b':'ar', 'c':'+54' }, { 'a':'Armenia', 'b':'am', 'c':'+374' }, { 'a':'Aruba', 'b':'aw', 'c':'+297' }, { 'a':'Austria', 'b':'at', 'c':'+43' }, { 'a':'Azerbaijan', 'b':'az', 'c':'+994' }, { 'a':'Bahrain', 'b':'bh', 'c':'+973' }, { 'a':'Bangladesh', 'b':'bd', 'c':'+880' }, { 'a':'Belarus', 'b':'by', 'c':'+375' }, { 'a':'Belgium', 'b':'be', 'c':'+32' }, { 'a':'Belize', 'b':'bz', 'c':'+501' }, { 'a':'Benin', 'b':'bj', 'c':'+229' }, { 'a':'Bhutan', 'b':'bt', 'c':'+975' }, { 'a':'Bolivia', 'b':'bo', 'c':'+591' }, { 'a':'BosniaandHerzegovina', 'b':'ba', 'c':'+387' }, { 'a':'Botswana', 'b':'bw', 'c':'+267' }, { 'a':'Brazil', 'b':'br', 'c':'+55' }, { 'a':'Brunei', 'b':'bn', 'c':'+673' }, { 'a':'Bulgaria', 'b':'bg', 'c':'+359' }, { 'a':'BurkinaFaso', 'b':'bf', 'c':'+226' }, { 'a':'Burundi', 'b':'bi', 'c':'+257' }, { 'a':'Cambodia', 'b':'kh', 'c':'+855' }, { 'a':'Cameroon', 'b':'cm', 'c':'+237' }, { 'a':'CapeVerde', 'b':'cv', 'c':'+238' }, { 'a':'CentralAfricanRepublic', 'b':'cf', 'c':'+236' }, { 'a':'Chad', 'b':'td', 'c':'+235' }, { 'a':'Chile', 'b':'cl', 'c':'+56' }, { 'a':'ChristmasIsland', 'b':'cx', 'c':'+61' }, { 'a':'CocosIslands', 'b':'cc', 'c':'+61' }, { 'a':'Colombia', 'b':'co', 'c':'+57' }, { 'a':'Comoros', 'b':'km', 'c':'+269' }, { 'a':'CookIslands', 'b':'ck', 'c':'+682' }, { 'a':'CostaRica', 'b':'cr', 'c':'+506' }, { 'a':'Croatia', 'b':'hr', 'c':'+385' }, { 'a':'Cuba', 'b':'cu', 'c':'+53' }, { 'a':'Curacao', 'b':'cw', 'c':'+599' }, { 'a':'Cyprus', 'b':'cy', 'c':'+357' }, { 'a':'CzechRepublic', 'b':'cz', 'c':'+420' }, { 'a':'DemocraticRepCongo', 'b':'cd', 'c':'+243' }, { 'a':'Denmark', 'b':'dk', 'c':'+45' }, { 'a':'Djibouti', 'b':'dj', 'c':'+253' }, { 'a':'EastTimor', 'b':'tl', 'c':'+670' }, { 'a':'Ecuador', 'b':'ec', 'c':'+593' }, { 'a':'Egypt', 'b':'eg', 'c':'+20' }, { 'a':'ElSalvador', 'b':'sv', 'c':'+503' }, { 'a':'EquatorialGuinea', 'b':'gq', 'c':'+240' }, { 'a':'Eritrea', 'b':'er', 'c':'+291' }, { 'a':'Estonia', 'b':'ee', 'c':'+372' }, { 'a':'Ethiopia', 'b':'et', 'c':'+251' }, { 'a':'FalklandIslands', 'b':'fk', 'c':'+500' }, { 'a':'FaroeIslands', 'b':'fo', 'c':'+298' }, { 'a':'Fiji', 'b':'fj', 'c':'+679' }, { 'a':'Finland', 'b':'fi', 'c':'+358' }, { 'a':'FrenchPolynesia', 'b':'pf', 'c':'+689' }, { 'a':'Gabon', 'b':'ga', 'c':'+241' }, { 'a':'Gambia', 'b':'gm', 'c':'+220' }, { 'a':'Georgia', 'b':'ge', 'c':'+995' }, { 'a':'Ghana', 'b':'gh', 'c':'+233' }, { 'a':'Gibraltar', 'b':'gi', 'c':'+350' }, { 'a':'Greece', 'b':'gr', 'c':'+30' }, { 'a':'Greenland', 'b':'gl', 'c':'+299' }, { 'a':'Guatemala', 'b':'gt', 'c':'+502' }, { 'a':'Guernsey', 'b':'gg', 'c':'+44-1481' }, { 'a':'Guinea', 'b':'gn', 'c':'+224' }, { 'a':'Guinea-Bissau', 'b':'gw', 'c':'+245' }, { 'a':'Guyana', 'b':'gy', 'c':'+592' }, { 'a':'Haiti', 'b':'ht', 'c':'+509' }, { 'a':'Honduras', 'b':'hn', 'c':'+504' }, { 'a':'Hungary', 'b':'hu', 'c':'+36' }, { 'a':'Iceland', 'b':'is', 'c':'+354' }, { 'a':'Iran', 'b':'ir', 'c':'+98' }, { 'a':'Iraq', 'b':'iq', 'c':'+964' }, { 'a':'Ireland', 'b':'ie', 'c':'+353' }, { 'a':'IsleofMan', 'b':'im', 'c':'+44-1624' }, { 'a':'Israel', 'b':'il', 'c':'+972' }, { 'a':'IvoryCoast', 'b':'ci', 'c':'+225' }, { 'a':'Jersey', 'b':'je', 'c':'+44-1534' }, { 'a':'Jordan', 'b':'jo', 'c':'+962' }, { 'a':'Kazakhstan', 'b':'kz', 'c':'+7' }, { 'a':'Kenya', 'b':'ke', 'c':'+254' }, { 'a':'Kiribati', 'b':'ki', 'c':'+686' }, { 'a':'Kosovo', 'b':'xk', 'c':'+383' }, { 'a':'Kuwait', 'b':'kw', 'c':'+965' }, { 'a':'Kyrgyzstan', 'b':'kg', 'c':'+996' }, { 'a':'Laos', 'b':'la', 'c':'+856' }, { 'a':'Latvia', 'b':'lv', 'c':'+371' }, { 'a':'Lebanon', 'b':'lb', 'c':'+961' }, { 'a':'Lesotho', 'b':'ls', 'c':'+266' }, { 'a':'Liberia', 'b':'lr', 'c':'+231' }, { 'a':'Libya', 'b':'ly', 'c':'+218' }, { 'a':'Liechtenstein', 'b':'li', 'c':'+423' }, { 'a':'Lithuania', 'b':'lt', 'c':'+370' }, { 'a':'Luxembourg', 'b':'lu', 'c':'+352' }, { 'a':'Macao', 'b':'mo', 'c':'+853' }, { 'a':'Macedonia', 'b':'mk', 'c':'+389' }, { 'a':'Madagascar', 'b':'mg', 'c':'+261' }, { 'a':'Malawi', 'b':'mw', 'c':'+265' }, { 'a':'Maldives', 'b':'mv', 'c':'+960' }, { 'a':'Mali', 'b':'ml', 'c':'+223' }, { 'a':'Malta', 'b':'mt', 'c':'+356' }, { 'a':'MarshallIslands', 'b':'mh', 'c':'+692' }, { 'a':'Mauritania', 'b':'mr', 'c':'+222' }, { 'a':'Mauritius', 'b':'mu', 'c':'+230' }, { 'a':'Mayotte', 'b':'yt', 'c':'+262' }, { 'a':'Mexico', 'b':'mx', 'c':'+52' }, { 'a':'Micronesia', 'b':'fm', 'c':'+691' }, { 'a':'Moldova', 'b':'md', 'c':'+373' }, { 'a':'Monaco', 'b':'mc', 'c':'+377' }, { 'a':'Mongolia', 'b':'mn', 'c':'+976' }, { 'a':'Montenegro', 'b':'me', 'c':'+382' }, { 'a':'Morocco', 'b':'ma', 'c':'+212' }, { 'a':'Mozambique', 'b':'mz', 'c':'+258' }, { 'a':'Namibia', 'b':'na', 'c':'+264' }, { 'a':'Nauru', 'b':'nr', 'c':'+674' }, { 'a':'Nepal', 'b':'np', 'c':'+977' }, { 'a':'Netherlands', 'b':'nl', 'c':'+31' }, { 'a':'NetherlandsAntilles', 'b':'an', 'c':'+599' }, { 'a':'NewCaledonia', 'b':'nc', 'c':'+687' }, { 'a':'Nicaragua', 'b':'ni', 'c':'+505' }, { 'a':'Niger', 'b':'ne', 'c':'+227' }, { 'a':'Nigeria', 'b':'ng', 'c':'+234' }, { 'a':'Niue', 'b':'nu', 'c':'+683' }, { 'a':'NorthKorea', 'b':'kp', 'c':'+850' }, { 'a':'Norway', 'b':'no', 'c':'+47' }, { 'a':'Oman', 'b':'om', 'c':'+968' }, { 'a':'Pakistan', 'b':'pk', 'c':'+92' }, { 'a':'Palau', 'b':'pw', 'c':'+680' }, { 'a':'Palestine', 'b':'ps', 'c':'+970' }, { 'a':'Panama', 'b':'pa', 'c':'+507' }, { 'a':'PapuaNewGuinea', 'b':'pg', 'c':'+675' }, { 'a':'Paraguay', 'b':'py', 'c':'+595' }, { 'a':'Peru', 'b':'pe', 'c':'+51' }, { 'a':'Pitcairn', 'b':'pn', 'c':'+64' }, { 'a':'Poland', 'b':'pl', 'c':'+48' }, { 'a':'Portugal', 'b':'pt', 'c':'+351' }, { 'a':'Qatar', 'b':'qa', 'c':'+974' }, { 'a':'RepublicCongo', 'b':'cg', 'c':'+242' }, { 'a':'Reunion', 'b':'re', 'c':'+262' }, { 'a':'Romania', 'b':'ro', 'c':'+40' }, { 'a':'Rwanda', 'b':'rw', 'c':'+250' }, { 'a':'SaintBarthelemy', 'b':'bl', 'c':'+590' }, { 'a':'SaintHelena', 'b':'sh', 'c':'+290' }, { 'a':'SaintMartin', 'b':'mf', 'c':'+590' }, { 'a':'Samoa', 'b':'ws', 'c':'+685' }, { 'a':'SanMarino', 'b':'sm', 'c':'+378' }, { 'a':'SaudiArabia', 'b':'sa', 'c':'+966' }, { 'a':'Senegal', 'b':'sn', 'c':'+221' }, { 'a':'Serbia', 'b':'rs', 'c':'+381' }, { 'a':'Seychelles', 'b':'sc', 'c':'+248' }, { 'a':'SierraLeone', 'b':'sl', 'c':'+232' }, { 'a':'Slovakia', 'b':'sk', 'c':'+421' }, { 'a':'Slovenia', 'b':'si', 'c':'+386' }, { 'a':'SolomonIslands', 'b':'sb', 'c':'+677' }, { 'a':'Somalia', 'b':'so', 'c':'+252' }, { 'a':'SouthAfrica', 'b':'za', 'c':'+27' }, { 'a':'SouthSudan', 'b':'ss', 'c':'+211' }, { 'a':'SriLanka', 'b':'lk', 'c':'+94' }, { 'a':'Sudan', 'b':'sd', 'c':'+249' }, { 'a':'Suriname', 'b':'sr', 'c':'+597' }, { 'a':'Swaziland', 'b':'sz', 'c':'+268' }, { 'a':'Syria', 'b':'sy', 'c':'+963' }, { 'a':'Taiwan', 'b':'tw', 'c':'+886' }, { 'a':'Tajikistan', 'b':'tj', 'c':'+992' }, { 'a':'Tanzania', 'b':'tz', 'c':'+255' }, { 'a':'Togo', 'b':'tg', 'c':'+228' }, { 'a':'Tokelau', 'b':'tk', 'c':'+690' }, { 'a':'Tonga', 'b':'to', 'c':'+676' }, { 'a':'Tunisia', 'b':'tn', 'c':'+216' }, { 'a':'Turkey', 'b':'tr', 'c':'+90' }, { 'a':'Turkmenistan', 'b':'tm', 'c':'+993' }, { 'a':'Tuvalu', 'b':'tv', 'c':'+688' }, { 'a':'Uganda', 'b':'ug', 'c':'+256' }, { 'a':'Ukraine', 'b':'ua', 'c':'+380' }, { 'a':'UnitedArabEmirates', 'b':'ae', 'c':'+971' }, { 'a':'Uruguay', 'b':'uy', 'c':'+598' }, { 'a':'Uzbekistan', 'b':'uz', 'c':'+998' }, { 'a':'Vanuatu', 'b':'vu', 'c':'+678' }, { 'a':'Vatican', 'b':'va', 'c':'+379' }, { 'a':'Venezuela', 'b':'ve', 'c':'+58' }, { 'a':'WallisandFutuna', 'b':'wf', 'c':'+681' }, { 'a':'WesternSahara', 'b':'eh', 'c':'+212' }, { 'a':'Yemen', 'b':'ye', 'c':'+967' }, { 'a':'Zambia', 'b':'zm', 'c':'+260' }, { 'a':'Zimbabwe', 'b':'zw', 'c':'+263' } ];	
	$scope.currentcc = -1;

	$scope.readCodeBooking = function() {
		HttpServiceAvail.readcodebooking($scope.restaurant, $scope.email).then(function(response) {
			var value, content = response.data;
			if(response.status !== 1 || typeof content !== 'string' || content === '') 
				return;

			try {
				var oo = JSON.parse(content.replace(/’/g, "\""));
				$scope.codebooking = oo.data;
				$scope.notesflag = 1;
				if($scope.codebooking && $scope.codebooking instanceof Array) {
					$scope.notescode = []; 
					for(i = 0; i < $scope.codebooking.length; i++) {
						value = ($scope.bknotescode !== "" && $scope.bknotescode.indexOf($scope.codebooking[i].label) > -1);
						$scope.notescode.push( { label: $scope.codebooking[i].label, value: value });
						}
					}
					
				} catch(e) { console.error("JSON-PROFILE", e.message); }
			});
		};
		
	HttpServiceAvail.readAllote($scope.restaurant, $scope.pax, $scope.bkproduct, $scope.platform).then(function(response) { 
		$scope.lunchdata = response.data.lunchdata;
		$scope.dinnerdata = response.data.dinnerdata;
		$scope.updateTimerScope(curndays);
		$scope.readCodeBooking();
		});

	//HttpServiceAvail.checkDayAvail($scope.restaurant, "Chef Table", "17:00", 4).then(function(response) { console.log('checkDayAvail', response); });

	$scope.prefill = function() {
		var filldata = { bkemail:'no@email.com', bklast:'nolastname', bkfirst:'nofirstname', bksalutation:'Mr.', bkcountry:'Singapore', bkmobile:'+65 99999999'} 
		for(ll in filldata) {
			if($scope[ll] === '' || typeof $scope[ll] === 'undefined') $scope[ll] = filldata[ll];
			}
		$scope.forceit = false;
		};

	$scope.genflagfunc = function(mask) {
		mask = parseInt(mask);
		return (($scope.genflag & mask) === mask);
		};

	// walkin, waiting, ... power of 2, setting the right bit
	$scope.walkin = function() {
		$scope.wkinflg ^= 1;
		$scope.addgenflag($scope.wkinflg, 1);
		};
	
	$scope.waiting = function() {
		$scope.wtngflg ^= 2;
		$scope.addgenflag($scope.wtngflg, 2);
		};
	
	$scope.addgenflag = function(val, mask) {
		var d = new Date();
		var mm = d.getMinutes();

		$scope.genflag = ($scope.genflag & ~mask) + val;
		if(val)
			$scope.bktime = d.getHours() + ":" + ((mm < 10) ? "0" : "") + mm;
		else $scope.bktime = "";
		};

	// check if simple cc
	$scope.simplecallcenter = $scope.genflagfunc('0x4000');
	if($scope.simplecallcenter) {
		$scope.wkinflg = 1;
		$scope.addgenflag(1, 1);
		$scope.bookingTitle = "BOOKING FORM";
		};
	
	$scope.setpax = function(p) {
		$scope.bkpax = p;
		};
		
	$scope.phoneindex = function(code) {
		val = $scope.bkmobile;
		if(!val || val === '')
			return;
			
		val = val.trim();
		val = val.replace(/[^0-9 \+]/g, "");
		val = val.replace(/[ ]+/g, " ");

		if(val.indexOf(code + ' ') == 0) {			
			val = val.replace(/^\+(\d{2,3})[ ]*\1[ ]*/, '+$1 '); // replace repetition of contry code
			return $scope.bkmobile = val.trim(); 
			}
					
		if(val.indexOf(code) == 0) {
			val = code + ' ' + val.substring(code.length);			
			val = val.replace(/^\+(\d{2,3})[ ]*\1[ ]*/, '+$1 '); // replace repetition of contry code
			return $scope.bkmobile = val.trim(); 
			}
			
		if((res = val.match(/^\+\d{2,3} /)) != null) {
			val = val.replace(/^\+\d{2,3} /, "");
			val = code + ' ' + val;
			val = val.replace(/^\+(\d{2,3})[ ]*\1[ ]*/, '+$1 '); // replace repetition of contry code
			return $scope.bkmobile = val.trim(); 
			}
			
		if((res = val.match(/^\+\d{2,3}$/)) != null) {
			val = val.replace(/^\+\d{2,3}/, "");
			val = code + ' ' + val;
			val = val.replace(/^\+(\d{2,3})[ ]*\1[ ]*/, '+$1 '); // replace repetition of contry code
			return $scope.bkmobile = val.trim(); 
			}
			
		if(val.match(/^\+/) != null) {
			val = code + ' ' + val.substring(1);
			val = val.replace(/^\+(\d{2,3})[ ]*\1[ ]*/, '+$1 '); // replace repetition of contry code
			return $scope.bkmobile = val.trim(); 
			}

		val = val.replace(/\+ /, "");
		val = code + ' ' + val;
		val = val.replace(/^\+(\d{2,3})[ ]*\1[ ]*/, '+$1 '); // replace repetition of contry code
		return $scope.bkmobile = val.trim(); 
		};
				
	if(curcountry == '')
		curcountry = 'Singapore';
		
	for(i = 0; i < $scope.countries.length; i++) {
		if($scope.countries[i].a == curcountry) {
			$scope.currentcc = i;
			$scope.curflgicon = "famfamfam-flag-" + $scope.countries[$scope.currentcc].b;
			$scope.phoneindex($scope.countries[$scope.currentcc].c);
			break;	
			}
		}
								
	$scope.numberOfDay = function(aDate) {
		var dateAr = aDate.split('/');
		var date = new Date(dateAr[2], parseInt(dateAr[1]) - 1, dateAr[0], 23, 59, 59);
		return Math.floor(((date.getTime() - Date.now()) / (24 * 60 * 60 * 1000)));
	}

	$scope.updatendays = function() {
		var nday;
		
		if($scope.ngdate instanceof Date === false)
			return;
		
		
		$scope.bkdate = HttpServiceAvail.normDate($scope.ngdate, false, '/');
		nday = $scope.numberOfDay($scope.bkdate);
		$scope.updateTimerScope(nday);

		if(AvailperPax === '1') 
			$scope.checkPerPaxData();
	};

	$scope.updateTimerScope = function(n) {
		var i, k, log, index, nchar_lunch, nchar_dinner, patlunch, patdinner,
			found, curval, localtime, limit, ht;
			
		$scope.curday = n;
		$scope.lunch = [];
		$scope.dinner = [];
		nchar_lunch = 4;
		nchar_dinner = 4;
		index = n * nchar_lunch;
		patlunch = parseInt('0x' + $scope.lunchdata.substring(index, index + nchar_lunch));
		index = n * nchar_dinner;
		patdinner = parseInt('0x' + $scope.dinnerdata.substring(index, index + nchar_dinner));
		curval = $scope.bktime;
		found = 0;
		localtime = "";
		limit = nchar_lunch * 4;
		for (log = i = 0, k = 1; i < limit; i++, k *= 2) 
			if ((patlunch & k) == k) {
				log++;
				ht = (9 + Math.floor(i / 2)) + ':' + ((i % 2) * 3) + '0';
				if(ht == curval) found = 1;
				$scope.lunch.push(ht);
				
				ht = (9 + Math.floor(i / 2)) + ':' + (((i % 2) * 3)+1) + '5';
				if(ht == curval) found = 1;
				$scope.lunch.push(ht);
				}
		if(log == 0) {
			$scope.lunch.push('not available for lunch');
			localtime = 'not available for lunch';
			}
		
		limit = nchar_dinner * 4;
		//if(limit > 17) limit = 16;	// minight
		for (log = i = 0, k = 1; i < limit; i++, k *= 2) 
			if ((patdinner & k) == k) {
				log++;
				ht = (16 + Math.floor(i / 2)) + ':' + ((i % 2) * 3) + '0';
				if(ht == curval) found = 1;
				$scope.dinner.push(ht);

				ht = (16 + Math.floor(i / 2)) + ':' + (((i % 2) * 3)+1) + '5';
				if(ht == curval) found = 1;
				$scope.dinner.push(ht);
				}
		if(log == 0) {
			$scope.dinner.push('not available for dinner');
			localtime = 'not available for lunch';
			}

		curval = $scope.bktime;
		if(localtime.substr(0,5) == 'close') {
			$scope.bktime = localtime;
			}
		else if(curval.substr(0,5) == 'close' || found == 0) {
			$scope.bktime = '';
			}
		};	
		
	$scope.setmealtime = function(tt, section) {
		switch(section) {
			case 1: 	// time
				$scope.bktime = tt;
				if(AvailperPax === '1') 
					$scope.checkPerPaxData();					
				break;
				
			case 2: 	// number of pers
				if(tt.substring(0, 4) == "more") {
					if((pers = prompt("How many persons ?", 10)) != null) {
						pers = parseInt(pers);
						if(pers > 9 && pers < maxpaxcc) tt = pers;
						else tt = $scope.bkcover;
						}
					}
				$scope.bkcover = String(tt);
				if(AvailperPax === '1') 
					$scope.checkPerPaxData();
				break;
				
			case 3: 	// salutation
				$scope.bksalutation = tt;
				 break;
				 
			case 4: 	// phone
				for(i = 0; i < $scope.countries.length; i++) 
					if($scope.countries[i].a == tt) {
						$scope.currentcc = i;
						break;
						}
						
				if(i >= $scope.countries.length) {
					alert('Invalid Country Code -' + tt + '-');
					$scope.currentcc = 10; // Singapore
					}
				$scope.curflgicon = "famfamfam-flag-" + $scope.countries[$scope.currentcc].b;
				$scope.bkcountry = $scope.countries[$scope.currentcc].a;
				$scope.phoneindex($scope.countries[$scope.currentcc].c);
				break;
			default:
			}
		};
	
		
	$scope.checkprevemail = function(which) {
		var resto = $scope.getResto();
				
		switch(which) {
			case 1:
				if($scope.bkemail == "" || $scope.IsEmail($scope.bkemail) == false) {
					alert("Invalid Email");
					return;
					}
				return HttpServiceAvail.bookingemail(resto, $scope.bkemail).then(function(response){ 
					//console.log(response);
					if(response.status > 0) {
						$scope.copydata(response.data);
						}
					return response;					
					});
				break;
				
			case 2:
				if(typeof $scope.bkmobile !== "string" || $scope.bkmobile.length < 5) {
					alert("Invalid Mobile");
					return;
					}
				return HttpServiceAvail.bookingmobile(resto, $scope.bkmobile).then(function(response){ 
					//console.log(response);
					if(response.status > 0) {
						$scope.copydata(response.data);
						}
					return response;
					});
				break;
				
			default:
				break;
			}
		};

	$scope.copydata = function(data) {
		var i, oo, org = ["email", "mobile", "lastname", "firstname", "salutation", "company", "country"],
			des = ["bkemail", "bkmobile", "bklast", "bkfirst", "bksalutation", "company", "bkcountry"];
			
		for(i = 0; i < org.length; i++) {
			oo = data[org[i]];
			if(oo && typeof oo === "string" && oo !== "") {
				$scope[des[i]] = oo;
				$('#'+des[i]).val(oo);
				if(org[i] === "country")
					$scope.setmealtime(oo, 4); // for the country
				}
			}
		if($scope.validprofilemail())
			$scope.profileflg = true;					
		};
				
	$scope.checkvalidtel = function() {
		if(!$scope.bkmobile || $scope.bkmobile === "") {
			$scope.bkmobile == $scope.countries[$scope.currentcc].c.trim() + ' ';
			return;
			}
		
		$scope.curflgicon = "famfamfam-flag-" + $scope.countries[$scope.currentcc].b;
		$scope.bkcountry = $scope.countries[$scope.currentcc].a;
		$scope.phoneindex($scope.countries[$scope.currentcc].c);
		};
			
	$scope.formats = ['dd-MM-yyyy', 'dd/MMMM/yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'dd.MM.yyyy', 'shortDate'];
	$scope.format = $scope.formats[3];
	$scope.minDate = new Date();
	$scope.maxDate = new Date();
	$scope.maxDate.setTime($scope.maxDate.getTime() + (remainingday * 24 * 3600 * 1000));	// remainingday  120 days
	$scope.ngdate = new Date(bkjsDate);

	$scope.disabled = function(date, mode) {
		return false;
	//return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
	};

	$scope.open = function($event) {
		$("#bkspecialrequest").trigger( "click" );
		$scope.start_opened = true;

		$event.preventDefault();
		$event.stopPropagation();

	};

	$scope.dateOptions = {
		formatYear: 'yy',
		startingDay: 1,
   		showWeeks: false,
   		popupPlacement: 'left',
		ignoreReadonly: true
	};

	$scope.setoverwrite = function() {
		var cc;
		if(cc = prompt("Set the time of the reservation (hh:mm) in 24h format")) {
			if(cc == "" || cc.length !== 5 || cc.search(":") < 0) {
				$scope.overwrite = 0;
				$scope.bktime = "";
				return alert("invalid time format");
				}
			ccAr = cc.split(":");
			if(parseInt(ccAr[0]) < 9 || parseInt(ccAr[0]) > 23) ccAr[0] = "09";
			if(parseInt(ccAr[1]) < 0 || parseInt(ccAr[1]) > 59) ccAr[1] = "00";
			$scope.bktime = ccAr[0] + ":" + ccAr[1];
			$scope.overwrite = 1;
			}
		}
	
	$scope.setselect = function(dd, type) {
		if(type === 'area') $scope.bkarea = dd;
		if(type === 'purpose') $scope.bkpurpose = dd;
		};
			
	$scope.checkSubmit = function(n) {
		var i, tt, val, mandatoryAr, mandatoryMsgAr, nday, filldata, notescode, sep;
		
		notescode = sep = "";
		if($scope.notesflag === 1) {
			for(i = 0; i < $scope.notescode.length; i++)
				if($scope.notescode[i].value) {
					notescode += sep + $scope.notescode[i].label;
					sep = ",";
					}
			notescode = (typeof notescode === "string" && notescode !== "") ? notescode.replace(/'|"/g, '`') : ""; // ’`
			}


		$('#book_form input[type=text], input[type=password]').each(function() {
			this.value = this.value.trim();
			});

		$scope.bkextra = "";			
		if(typeof $scope.bkarea === 'string' && $scope.bkarea !== "")
			$scope.bkextra += (($scope.bkextra !== "") ? "|" : "") + "area="+$scope.bkarea;
		if(typeof $scope.bkpurpose === 'string' && $scope.bkpurpose !== "")
			$scope.bkextra += (($scope.bkextra !== "") ? "|" : "") + "purpose="+$scope.bkpurpose;
		if(typeof notescode === 'string' && notescode !== "")
			$scope.bkextra += (($scope.bkextra !== "") ? "|" : "") + "notescode="+notescode;

		$('#bkextra').val($scope.bkextra);

        $('#genflag').val($scope.genflag);
	    $('#bkdate').val($scope.bkdate);

		if($scope.simplecallcenter) {
			filldata = { bkemail:'no@email.com', bklast:'nolastname', bkfirst:'nofirstname', bksalutation:'Mr.', bkcountry:'Singapore', bkmobile:'+65 99999999' } 
	
	        $scope.available = 1;
	        $scope.bkdate = $scope.todaydate;
	        $scope.bktime = $scope.todaytime;
	        $scope.bkcover = $scope.bkpax;
			$scope.genflag |= 0x1001;	// turn walking
        	$('#genflag').val($scope.genflag);
	        $('#bkdate').val($scope.bkdate);
	        $('#bktime').val($scope.bktime);
	        $('#bkcover').val($scope.bkpax);
			val = ($('#dontnotify1').is(':checked')) ? 17 : '';
			$('#dontnotify').val(val);	

	        if($scope.IsEmail($scope.bkemail) === false)  { $scope.bkemail = filldata.bkemail; $('#bkemail').val($scope.bkemail); }
	        if(typeof $scope.bkmobile !== "string" || $scope.bkmobile.length < 5) { $scope.bkmobile = filldata.bkmobile; $('#bkmobile').val($scope.bkmobile); }
	        if(typeof $scope.bklast !== "string" || $scope.bklast === "") { $scope.bklast = filldata.bklast; $('#bklast').val($scope.bklast); }
	        if(typeof $scope.bkfirst !== "string" || $scope.bkfirst === "") { $scope.bkfirst = filldata.bkfirst; $('#bkfirst').val($scope.bkfirst); }
	        if(typeof $scope.bkcover !== "string" || $scope.bkcover === "") { $scope.bkcover = $scope.bkpax; }
			}

		mandatoryAr = [ 'bkdate', 'bktime', 'bkcover', 'bkemail', 'bklast', 'bkfirst', 'bkmobile' ];
		mandatoryMsgAr = [ 'date', 'time', 'number of persons', 'email', 'last name', 'first name', 'mobile' ];


		for (i = 0; i < mandatoryAr.length; i++) {	// manage the autocomplete !!! 
			$scope[mandatoryAr[i]] = $('#' + mandatoryAr[i]).val();
		}

		for (i = 0; i < mandatoryAr.length; i++) {
			if (!$scope[mandatoryAr[i]] || $scope[mandatoryAr[i]] == "")
				return $scope.invalid($scope[mandatoryAr[i]], "Invalid " + mandatoryMsgAr[i], $scope.bktracking);
		}

		nday = $scope.numberOfDay($scope.bkdate);
		if(isNaN(nday) || nday > remainingday || nday < 0) {
			return $scope.invalid('bkdate', 'Invalid date, try again');
			}

		if($scope.bktime.length < 2) return $scope.invalid('bktime', 'Choose a time for the reservation');  
		if($scope.bktime.indexOf("not available") > -1) return $scope.invalid('bktime', 'The restaurant is not available at the chosen time. Please, select another time for the reservation');        
		if($scope.bktime.substr(0,6) == "closed") return $scope.invalid('bktime', 'The restaurant is close that day, try another time/day');
		if($scope.bktime.length < 4) return $scope.invalid('bktime', 'Invalid time, -' + $scope.bktime + '-, try again');  
		if(!$scope.IsEmail($scope.bkemail)) return $scope.invalid('bkemail', 'Invalid email, try again');
		if($scope.bkmobile.length < 5) return $scope.invalid('bkmobile', 'Invalid mobile number, try again');

		for(i = 0; i < mandatoryAr.length; i++) {
			if(typeof $scope[mandatoryAr[i]] === 'undefined' || $scope[mandatoryAr[i]] == "")
				return $scope.invalid($scope[mandatoryAr[i]], 'Invalid ' + mandatoryMsgAr[i] + ', try again');
			}	


		// reactualise the variable is set to 0
		if($scope.available === 0) 
			$scope.checkPerPaxData();
			
		if($scope.available === 0)
			return $scope.invalid("", "Not enough seats available for the current request. Modify your search");			
	
		if(tracking != '') {
			removelgcookie("weeloy_be_tracking");
			setlgcookie("weeloy_be_tracking", tracking, 1);
			}

		if($scope.overwrite > 0) {
			val = $('#bktracking').val();
			if(!val || val === "")
				$('#bktracking').val("overwrite");
			else if(val.search("overwrite") < 0) {
	        	$('#bktracking').val(val + "|overwrite");
	        	}
			}
		
		["bkspecialrequest", "bknotes"].forEach(function(ll) {
			if(typeof $scope[ll] === "string" && $scope[ll].length > 1)
				$scope[ll] = $scope[ll].replace(/\s+/g, ' ');
				});
			
		// angular does not always update hidden field properly
		action = (window.location.href.indexOf("&prompt=0") < 0) ? 'modules/callcenter/precallcenter.php' : 'modules/callcenter/confirmcc.php';
        $('#book_form').attr('action', action);

		if($scope.onlyonce === true)
			book_form.submit();

		$scope.onlyonce = false;
		return false;	
		};

	$scope.IsEmail = function(email) { var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/; return regex.test(email); }
	$scope.cleanemail = function(obj) { obj = obj.replace(/[!#$%^&*()=}{\]\[\"\':;><\?/|\\]/g, '');  return obj; }
	$scope.cleantel = function(obj) { obj = obj.replace(/[^0-9 \+]/g, '');  return obj; }
	$scope.cleanpass = function(obj) { obj = obj.replace(/[\'\"]/g, '');  return obj; }
	$scope.cleantext = function(obj) { obj = obj.replace(/[!@#$%^*()=}{\]\[\"\':;><\?/|\\]/g, ''); return obj; }	
	$scope.invalid = function(id, str) { alert(str); if(id != "") $('#' + id).focus(); }	

	$scope.checkPerPaxData = function() {
		var dateflg, timeflg, persflg, tt;

		tt = $scope.bktime.replace(/[^0-9:]+/, "");
		if(tt === "" || typeof tt !== 'string')
			return;
			        
		else if(tt.length < 4)
			return $scope.invalid('bktime', "Invalid time, set a valid time");        
	
		if($scope.overwrite === 1) {
			return $scope.available = 1;
			}
			
		$scope.available = 0;
		dateflg = ($scope.bkdate && (($scope.bkdate instanceof Date) || (typeof $scope.bkdate === "string" && $scope.bkdate.length > 7)));
		timeflg = (typeof $scope.bktime === "string" && $scope.bktime.length > 3);
		persflg = (typeof $scope.bkcover === "string" && parseInt($scope.bkcover) > 0);
		if((dateflg && timeflg && persflg) == false) {
			return;
			}
			

		if($scope.wkinflg || $scope.wtngflg || parseInt($scope.bkcover) === 1) {

			$scope.available = 1;
			return;
			}

		HttpServiceAvail.checkAvail($scope.restaurant, $scope.bkdate, $scope.bktime, $scope.bkcover, $scope.bkproduct, $scope.platform).then(function(response) {  
			if(response.data == "1") return $scope.available = 1; 
			else if(response.count > 0) { if(confirm("Too many guests. Only " + response.count + " seats left\nDo you want to overwrite ?")) { return $scope.available = $scope.overwrite = 1; } }
			else  { if(confirm("Too many guests (" + $scope.bkcover + ")..\nDo you want to overwrite ?")) { return $scope.available = $scope.overwrite = 1; } }
			console.log(response); 
			$scope.bkcover = "1";
			});
		};

	$scope.validprofilemail = function () {
		return (typeof $scope.bkemail === 'string' && $scope.bkemail.substr(0, 3) !== "no@" && $scope.bkemail.search("@email.com") === -1  && $scope.IsEmail($scope.bkemail));
		};

	$scope.pabx = function() {
		HttpServiceAvail.readPabx($scope.fixline).then(function(response) {
			var data = response.data;
			var resto = $scope.getResto();
			if(response.status === 1) {
				if(typeof data.fromline !== "string" || data.fromline.length < 8 || 
				   typeof data.restaurant !== "string" || data.restaurant.length < 8 || resto.search(data.restaurant) < 0)
					return;
				if(data.fromline[0] !== "+")
					data.fromline = "+" + data.fromline.substring(0, 2) + " " + data.fromline.substring(2);
				$scope.bkmobile = data.fromline;					
				return HttpServiceAvail.bookingmobile($scope.getResto(), $scope.bkmobile).then(function(response){ 
					//console.log(response);
					if(response.status > 0) {
						$scope.copydata(response.data);
						if(data.restaurant !== $scope.restaurant)
							return $scope.switchResto(data.restaurant);
						}
					return response;
					});
				}
			});
		};
						
	$scope.viewprofile = function() {
		var content, win, oo, email, phone, invphone, invemail;

		email = $scope.bkemail;
		phone = $scope.bkmobile;

		invemail = !$scope.validprofilemail();
		invphone = (typeof phone !== 'string' || phone.replace(/[^\d]/g, '').length < 8);
		
		if(invemail && invphone) {
			alert("Invalid email " + $scope.bkemail + " and invalid phone number " + $scope.bkmobile);
			return;
			}
		
		if(invemail) email = "";
		if(invphone) phone = "";
		
		if($scope.profilwind) {
			$scope.profilwind.close();
			$scope.profilwind = null;
			}
			
		HttpServiceAvail.read1Profile($scope.restaurant, email, phone).then(function(response) {
			var data = response.data, attr, tt, label, codedata = '';
			if(response.status <= 0) {
				alert("No profile for "+$scope.bkemail);
				return;
				}

			win = window.open("", "Profile"+Math.random(), "toolbar=yes,location=no,directories=yes,menubar=yes,scrollbars=yes,width=400, height=400, left=100, top=25");
		
			content = "";
			content += "<html><head><title>Weeloy System</title>";
			content += "<style>body { margin: 20px 20px 20px 20px } h5 { margin: 5px 0 5px 0 } table { font-family:helvetica;font-size:10px; width:100%; border-collapse: collapse; border: 0; } tr:nth-child(even) {background-color: #f2f2f2} th, td { text-align:center; height: 20px; border-bottom: 1px solid #ddd;}  .truncate { white-space: nowrap; overflow: hidden; text-overflow: ellipsis; } </style></head>";
			content += "<body onLoad='document.title=\"PROFILE\"'>";
			content += "<center><h2>PROFILE</h2></center>";
			content += "<h5>" + 'Guest' + ' : ' + data.salutation + ' ' + data.firstname + " " + data.lastname + "</h5>";
			if(typeof data.lastvisit === "string" && data.lastvisit.length > 8)
				content += "<h5>" + 'Last Visit' + ' : ' + data.lastvisit.datereverse() + "</h5>";
			if(typeof data.firstvisit === "string" && data.firstvisit.length > 8)
				content += "<h5>" + 'First Visit' + ' : ' + data.firstvisit.datereverse() + "</h5>";
			content += "<hr>";
			
			if(typeof data.systemid === "string")
				systemid = data.systemid;

			label = ($scope.tmsflg === '87') ? "callcenter" : "standalonecc";
			content += profileService.printHtml(data, $scope.restaurant, systemid, $scope.usrtoken, label, email, phone);

			content += "</body></html>";
			win.document.write(content);
			$scope.profilwind = win;
			});
		};
	
	function getResto() {
		return ($scope.restogroup !== "" && $scope.restogroup.search(",") > 0 && $scope.restaurant !== "SG_SG_R_MadisonRooms") ? $scope.restogroup : $scope.restaurant;
		}

	function switchResto(resto) {
		return reloadcc(resto);
		}

    $scope.showModal = function(title, initval, func, template, mydata, prepostcall, size, labelok, labelclose) {
		$scope.mydata = mydata;
		$scope.mydata.name = initval;
		ModalService.activate(title, initval, func, template, mydata, prepostcall, size, labelok, labelclose);
		};
			
	$scope.updateCustomProfile = function(token, resto, id, email, phone) {

		if(token !== $scope.usrtoken) {
			return alert("Invalid login token");
			}
			
		HttpServiceAvail.read1Profile($scope.restaurant, email, phone).then(function(response) {
			var data = response.data;
		
			if(response.status <= 0 || !data || !data.systemid) {
				$scope.alert("No profile for "+email);
				return;
				}
			$scope.cprofiledata = response.data;  
			return $scope.modifyprofile($scope.cprofiledata);
			});
		};

	$scope.modifyprofile = function(data) {
		var oo, ckey;
		
		mydata.profileusr = data;
		mydata.profilecnt = [];
		mydata.profilecode = [];
		mydata.profiletitle = [{ a: "Guest", b: data.salutation + " " + data.firstname + " " + data.lastname }, { a: "email", b: data.email }, { a: "email2", b: data.extraemail }, { a: "phone", b: data.mobile }, { a: "phone2", b: data.extramobile }];

		mydata.profilecode = profileService.codeformat(data);
		mydata.profilecnt = profileService.datacontent(data);
		ckey = profileService.codekey(data);
		mydata.profilecode.map(function(oo) { oo.value = (ckey.indexOf(oo.label) >= 0); });
		
		$scope.showModal('Update Profile', 'PROFILE', $scope.apimodifprofile, "modifprofile.html", mydata, null, '', 'Update', 'Cancel');
		};
	
	$scope.apimodifprofile = function() {
		var content = {};		
		
		if(mydata && mydata.profilecode && mydata.profilecnt && mydata.profileusr && mydata.profileusr.systemid) {
			mydata.profilecnt.map(function(oo) { content[oo.a] = oo.b; });
			profileService.updatesubProfile($scope.restaurant, $scope.usremail, mydata.profileusr.systemid, mydata.profilecode, content, $scope.usrtoken, HttpServiceAvail).then(function(response) {
				console.log('updatesubProfile', response);
				if(response.status === 1)
					alert("Profile has been updated");
				});
			}
		};
			

}]);

</script>
</body>
</html>
