<?php

require_once("lib/class.mail.inc.php");
require_once("lib/class.debug.inc.php");

$validRecipient = array(
			"enosmas@neo.nc", 
			"conseil@neo.nc",
			"support@weeloy.com", 
			"info@weeloy.com", 
			"sales@weeloy.com", 
			"richard@kefs.me", 
			"soraya.kefs@weeloy.com", 
			"richard.kefs@weeloy.com", 
			"richardkefs@gmail.com", 
			"mis.sg@amarahotels.com", 
			"LiangPeng.Seng@agilysys.com",
			"partner@weeloy.com"
			);

$arglist = array('sender', 'senderdesc', 'destinataire', 'sujet', 'contenu', 'nostatus');
foreach ($arglist as $label) {
    if (!isset($_REQUEST[$label]))
        $_REQUEST[$label] = "";
    $$label = $_REQUEST[$label];
    $$label = preg_replace("/\'|\"/", "’", $$label);
    $$label = preg_replace("/\s+/", " ", $$label);
}


	$sender="";
        $opts['from'] = array('support@weeloy.com' => 'Site Notification');
        if(!empty($sender)) {
        	if(!empty($senderdesc)) $opts['from'] = array($sender => $senderdesc);
		else $opts['from'] = array($sender);
		}
	$recipient = $destinataire;
	$subject = substr($sujet, 0, 30);
	$body = substr($contenu, 0, 256);
	$body = preg_replace("/\|\|\|/", "<br><br>", $body);


	if(!in_array($recipient, $validRecipient)) {
		echo "wrong recipient";
		exit;
		}
 	WY_debug::recordDebug("TEST", "RMAIL1", "recipient " . $recipient . ", subject =  " . $subject . ", body = " . $body);

    $mm = new JM_Mail;
    $mm->swifttest($recipient, $subject, $body, $opts);
    if(empty($nostatus))
    	echo "Done";
?>