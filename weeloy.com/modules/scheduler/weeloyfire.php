<?php

require_once("conf/conf.init.inc.php");
require_once("lib/wpdo.inc.php");
require_once 'lib/class.scheduler.inc.php';


//$task = filter_input(INPUT_GET, 'task', FILTER_SANITIZE_STRING);

$task = htmlspecialchars($_REQUEST['task'], ENT_QUOTES);

$scheduler = new WY_Scheduler($task);
$scheduler->run();