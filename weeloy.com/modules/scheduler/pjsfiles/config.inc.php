<?php

require_once 'conf/conf.init.inc.php';
require_once 'conf/conf.mysql.inc.php';

// ---------------------------------------------------------
 $app_name = "phpJobScheduler";
 $phpJobScheduler_version = "3.9";
// ---------------------------------------------------------

define('DBHOST', $dbhost);

define('DBNAME', $dbname);
define('DBUSER', $dbuser);
define('DBPASS', $dbpass);

define('DEBUG', false);// set to false when done testing