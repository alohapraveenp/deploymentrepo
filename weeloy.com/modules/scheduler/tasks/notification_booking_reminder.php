<?php

//scheduled task - Send notification reminder on coming soon bookings

require_once("conf/conf.init.inc.php");
require_once("lib/wpdo.inc.php");

require_once("lib/class.booking.inc.php");
require_once("lib/class.notification.inc.php");
require_once("lib/class.mail.inc.php");
require_once("lib/class.sms.inc.php");
require_once("lib/class.pushnotif.inc.php");
require_once("lib/class.async.inc.php");
require_once("lib/class.spool.inc.php");
require_once 'conf/conf.sqs.inc.php';
require_once("lib/class.analytics.inc.php");
require_once("conf/conf.session.inc.php");

$datetime = new DateTime('today');
$datetime->modify('+1 day');
//echo $datetime->format('Y-m-d H:i:s');


$sql = "SELECT confirmation FROM booking WHERE status LIKE '' AND rdate = '" . $datetime->format('Y-m-d') . "'  ";


$booking_list = pdo_multiple_select($sql);



$booking = new WY_Booking();
$notification = new WY_Notification();

//AWS SQS 
$totalcount = count($booking_list);




//// SHOULD BE at the end after execution 
//// $totalcount  must be the number of success 
$message_sqs = new SQS();

$message_sqs->setQueue('activitylog-booking-reminder');
$message_sqs->setBody('Booking confirmation reminder');
$message_sqs->setAttribute('type', 'booking-reminder', 'String');
$message_sqs->setAttribute('category', 'Email', 'String');
$message_sqs->setAttribute('count', $totalcount, 'String');
$message_sqs->sendMessage();


$logger = new WY_log("website");
$loguserid = (isset($_SESSION['user']['user_id'])) ? $_SESSION['user']['user_id'] : "0";

foreach ($booking_list as $b) {
    
    $logger->LogEvent($loguserid, 710, $b['confirmation'], 'website', '', date("Y-m-d H:i:s"));
    $booking->getBooking($b['confirmation']);
    $notification->notify($booking, 'reminder_booking');
}
