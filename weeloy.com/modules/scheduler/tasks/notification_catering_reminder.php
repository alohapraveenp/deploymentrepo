<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//scheduled task - Send notification reminder on coming soon bookings

	require_once("conf/conf.init.inc.php");
	require_once("lib/wpdo.inc.php");
        
        require_once("lib/class.booking.inc.php");
        require_once("lib/class.notification.inc.php");        
        require_once("lib/class.mail.inc.php");
	require_once("lib/class.sms.inc.php");
        require_once("lib/class.pushnotif.inc.php");
	require_once("lib/class.async.inc.php");
	require_once("lib/class.spool.inc.php");
	
        $datetime = new DateTime('today');
        $datetime->modify('+2 day');


    $sql = "SELECT online_order.ID, 
                online_order.restaurant_id, 
                online_order.amount, 
                online_order.currency, 
                online_order.email,
                online_order.first_name,
                online_order.last_name,
                online_order.phone,
                online_order.total_items, 
                online_order.delivery_time, 
                online_order.delivery_date, 
                online_order.payment_status, 
                online_order.created_at, 
                online_order_item.quantity, 
                online_order_item.item_id,
                restaurant.title ,
                menu.item_title, 
                menu.price
                FROM online_order AS online_order 
                LEFT JOIN 
                  ( SELECT restaurant,title FROM restaurant )  AS restaurant 
                ON online_order.restaurant_id = restaurant.restaurant 
                 LEFT JOIN 
                  (SELECT quantity,item_id,order_id FROM online_order_item ) as  online_order_item
                ON online_order_item.order_id = online_order.ID 
                LEFT JOIN 
                (SELECT ID,item_title,price FROM menu) as menu
                ON online_order_item.item_id = menu.ID 
                WHERE  online_order.delivery_date = '".$datetime->format('Y-m-d')."'
                GROUP BY online_order.ID";
        $order_list = pdo_multiple_select($sql);
       
            $temArray =[];
          $notification = new WY_Notification();

        foreach($order_list as $b){
            $notification->notifyreminderCatering ($b);
 
        }
      
   echo 'success';

