<?php

require_once("conf/conf.init.inc.php");
require_once("lib/wpdo.inc.php");
require_once('conf/conf.sqs.inc.php');

if (strstr($_SERVER['HTTP_HOST'], 'localhost:8888') === false)   
	require_once(__ROOTDIR__ . "modules/crawler/crawler_email.php");
else require_once($_SERVER['DOCUMENT_ROOT'] . __ROOTDIR__ . "/modules/crawler/crawler_email.php");

$message_sqs = new SQS();

$message_sqs->setQueue('activitylog-crawler-email');
$message_sqs->setBody('Email crawler task launched');
//$message_sqs->setAttribute('type', 'booking-reminder', 'String');
//$message_sqs->setAttribute('category', 'Email', 'String');
//$message_sqs->setAttribute('count',$totalcount , 'String');
$message_sqs->sendMessage();

