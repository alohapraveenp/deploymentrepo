<?php
require_once("conf/conf.init.inc.php");
require_once("lib/wpdo.inc.php");
require_once("conf/conf.session.inc.php");
require_once("lib/wglobals.inc.php");
require_once("lib/class.images.inc.php");
require_once("lib/class.media.inc.php");
require_once("lib/class.restaurant.inc.php");
require_once("lib/class.member.inc.php");


require_once("lib/Browser.inc.php");

$browser = new Browser();

	$arglist = array('bkemail', 'bkconfirmation', 'bkrestaurant', 'bktitle', 'bkaction');
	foreach ($arglist as $label)
		if(isset($_REQUEST[$label]))
			$$label = preg_replace("/\'|\"/", "’", $_REQUEST[$label]);

	if(empty($bkaction))
		$bkaction = "cancel";
	
	$restaurantinit = "";
	$media = (AWS) ? __S3HOST__ . __S3DIR__ : __SHOWDIR__;
	
	if(!empty($bkrestaurant)) {
		$res = new WY_restaurant;
		$res->getRestaurant($bkrestaurant);	
		//$imgdata = new WY_Images($bkrestaurant);
        $mediadata = new WY_Media();
		$logo = $mediadata->getLogo($bkrestaurant);
		$restaurantinit = "restaurant='$bkrestaurant';";
		}
	else $logo = "images/logo_w.png";

	$stateinputinit = "stateinput=0;";
	$labelaction = ($bkaction == "cancel") ? "labelaction='Cancel your booking';" : "labelaction='Your booking';";
	$action = ($bkaction == "cancel") ? "action='cancel';" : "action='review';";
	$mediainit = "media='$media';";
	
	$init = "\"" . $restaurantinit . $stateinputinit . $labelaction . $action . $mediainit . "\"";

	$actionfile = $_SERVER['PHP_SELF'];
	
	$brwsr_type = ($browser->isMobile() || (isset($_REQUEST['brwsr_type']) && $_REQUEST['brwsr_type'] == "mobile")) ? "mobile" : "";
        
    $confirmation_id = (isset($_REQUEST['confirmation'])) ? $_REQUEST['confirmation'] : "";

$termsconditions = "\"" . __ROOTDIR__ . "/modules/templates/tnc/cancelconditions.php" . "\"";
?>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv='expires' content='mon, 1 jan 1998 05:00:00 gmt'>
<meta http-equiv='pragma' content='no-cache'>
<meta http-equiv='pragma' content='cache-control: max-age=0'>
<meta http-equiv='last-modified' content='mon, 1 jan 1998 05:00:00 gmt'>
<meta http-equiv='cache-control' content='no-cache, must-revalidate'>
<meta name='robots' content='noindex, nofollow'>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="HandheldFriendly" content="true" />
<title>Manage your booking - Weeloy Code - Weeloy.com</title>
<link href="<?php echo __ROOTDIR__?>/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo __ROOTDIR__?>/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo __ROOTDIR__?>/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo __ROOTDIR__?>/css/dropdown.css" rel="stylesheet" type="text/css" />
<link href='https://fonts.googleapis.com/css?family=Montserrat|Unkempt|Roboto:400,900,700italic,700,500italic,500,400italic' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="<?php echo __ROOTDIR__?>/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo __ROOTDIR__?>/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo __ROOTDIR__?>/js/bootstrap-formhelpers.js"></script>
<script type='text/javascript' src="<?php echo __ROOTDIR__?>/js/angular.min.js"></script>
<style>

body {
font-family: Roboto; 
    font-size: 14px;
    width:90%;
}

div .input-group {
margin-bottom: 15px;
}

img {
display:block;
margin:0 auto;
padding-bottom:20px;
}

a#buttonterms {
margin: 10px 30px 20px 55px;
}

.termsconditions {
font-family: Roboto; 
font-size: 12px;
}

div .separation {
vertical-align:middle;
border-right:4px solid navy;
height:100%;
}

<?php if($brwsr_type != "mobile") echo ".mainbox { margin: 10px 30px 20px 55px; width:550px; } "; ?>

.panel{
	border-color:#5285a0 !important;
}

.panel-heading{
	background-image:none !important;
	background-color:#5285a0 !important;
	color:#fff !important;
	border-color:#5285a0 !important;
	border-radius:0;
}

.btn-default{
	background-image:linear-gradient(to bottom, #eee 0px, #e0e0e0 100%)!important;
}


</style>

<?php include_once("ressources/analyticstracking.php") ?>
</head>

<body ng-app="myApp">
<form class='form-horizontal' action='<?php echo $actionfile ?>' role='form' id='book_form' name='book_form' method='POST' enctype='multipart/form-data'>

<div id='booking' ng-controller='MainController' ng-init = <?php echo $init; ?> >

<div class="container mainbox">    
<div class="panel panel-info">
		<div class="panel-heading">
			<div class="panel-title" >{{ labelaction }}</div>
		</div>     
		
		<div style="padding-top:20px" class="panel-body" >
				<input type='hidden' id='bkrestaurant' name='bkrestaurant' value='<?php echo $bkrestaurant ?>'>
				<input type='hidden' id='bktitle' name='bktitle' value='<?php echo $bktitle ?>'>
				<input type='hidden' id='brwsr_type' name='brwsr_type' value='<?php echo $brwsr_type ?>'>

				<p><?php echo "<img src='../../$logo' width='120' id='logo'>"; ?></p>				

<div ng-show="stateinput==0">
<?php if($brwsr_type != "mobile") : ?>				
				<div class='col-xs-5 separation'>
                                    
				<ul style="font-family:Unkempt;font-size:14px;margin:0px;list-style:none">
				<li>No Booking fee</li><br>
				<li>Instant Reservation</li><br>
				<li>Spin the Wheel<br>at the restaurant</li><br>
				</ul>
                <div style='list-style-type: none;background-color:yellow;font-size: 13px;' ng-hide="action == 'cancel'"><a ng-click='newlocation(confirmation, guestemail, "cancel")'>&nbsp; CANCEL A RESERVATION</div>
				<div style='list-style-type: none;background-color:orange;font-size: 13px;' ng-show="action == 'cancel'"><a ng-click='newlocation(confirmation, guestemail, "view")'>&nbsp; VIEW A RESERVATION</div>
				</div>
				
				<div class='col-xs-7'>
<?php endif; ?>


				<div class="modal fade" id="remoteModal" tabindex="-1" role="dialog" aria-labelledby="remoteModalLabel" aria-hidden="true">  
					<div class="modal-dialog" style="background-color: #fff">  
						<div class="modal-content" style='font-size:11px;'></div>  
					</div>  
				</div>  

				<div class="input-group">
					<span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                    <input type="text" class="form-control input" value="" id='bkconfirmation' name='bkconfirmation' placeholder="confirmation" ng-model="confirmation">                                        
				</div>
				<br/>
				<div class="input-group">
					<span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
					<input type="text" class="form-control input" value="" id='bkemail' name='bkemail' placeholder="email" ng-model="guestemail">                                        
				</div>
                                <br/>
				<div class="input-group">
					<a href="javascript:;" class="btn btn-success" ng-click="getConfirmation();">{{ labelaction }}</a>
				</div>

				<div ng-show="action=='cancel'">
					<div class="form-inline">
					<div class="input-group">
						<label class="checkbox-inline termsconditions"><input type="checkbox" value="" id='bkchtc' name='bkchtc' ng-model="TC" ng-checked="action!='cancel'">I agree to the terms and conditions </label>
						<a href=<?php echo $termsconditions ?> id='buttonterms' data-toggle="modal" class="btn btn-xs btn-info" data-target="#remoteModal" style='font-size:11px;'>view cancel conditions</a>
					</div>
					</div>
				</div>	


<?php if($brwsr_type != "mobile") : ?>				
				</div>
<?php else : ?>
				<div class='row' style="padding-left:20px" >
				<hr><br>
				<ul style="font-family:Unkempt;font-size:14px;margin:0px;">
				<li>No Booking fee</li><br>
				<li>Instant Reservation</li><br>
				<li>Spin the Wheel<br>at the restaurant</li><br>
				</ul>
                <div style='list-style-type: none;background-color:yellow;font-size: 13px;' ng-hide="action == 'cancel'"><a ng-click='newlocation(confirmation, guestemail, "cancel")'>&nbsp; CANCEL A RESERVATION</div>
				<div style='list-style-type: none;background-color:orange;font-size: 13px;' ng-show="action == 'cancel'"><a ng-click='newlocation(confirmation, guestemail, "view")'>&nbsp; VIEW A RESERVATION</div>
				</div>
<?php endif; ?>
</div>
<div ng-show="stateinput==1 || stateinput==2">
<ul>
<li>&nbsp; Restaurant:  {{ names.title }}</li>
<li>&nbsp; Confirmation:  {{ names.booking }}</li>
<li>&nbsp; Date:    {{ names.date }}</li>
<li>&nbsp; Time:   {{ names.time }}</li>
<li>&nbsp; Number of guests:   {{ names.pers }}</li>
<li>&nbsp; Name:   {{ names.salutation }} {{ names.first }} {{ names.last }}</li>
<!-- <li>&nbsp; Email:  {{ names.email }}</li> -->
<li>&nbsp; Mobile:   {{ names.phone }}</li>
</div>
<div ng-show="stateinput==2">
<div class="input-group">
	<a class="btn btn-danger" ng-click="cancelConfirmation();">Do you still want to cancel this reservation</a>
	&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
	<a class="btn btn-info" ng-click="stateinput=0;">Back</a>
</div>
</div>
<div ng-show="stateinput==1">
<div class="input-group">
	&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
	<a href="javascript:;" class="btn btn-info" ng-click="stateinput=0;">Back</a>
</div>
</div>
<div ng-show="stateinput==3">
<h3>{{ names.booking }} made by {{ names.first }} {{ names.last }} has been deleted</h3>
</div>
		</div>  
		</div>
</div>
</div>

</form>
<script>
$(document).ready(function() {

	updatefield();
});

var ccconfirmation = <?php echo "'" . $confirmation_id . "'"; ?>;
var ccemail = <?php echo "'" . $bkemail . "'"; ?>;
var cdate = new Date();
var app = angular.module("myApp", []);


app.controller('MainController', function($scope, $http) {

  $scope.confirmation = ccconfirmation;
  $scope.guestemail = ccemail;
  
  $scope.getConfirmation = function() {

	if(checkSubmit() == false)
		return;

	$scope.guestemail = $('#bkemail').val();
	
 	var url = '../../api/confirmation/' + $scope.confirmation + '/' + $scope.guestemail;    

	console.log(url);
        
	$http.get(url).success(function(response) { // 			else alert('Unable to cancel the reservation ' + $scope.confirmation + ". Error : " + response.errors);
		if(response.status == 1) {
			$scope.names = response.data;
			console.log(response.data);
			if(response.data.booking != $scope.confirmation) {
				alert("Unknow error");
				return;
				}
			$scope.restaurant = $scope.theResto = $scope.names.resto;
			$scope.names.time = $scope.names.time.substr(0, 5);
			$('#logo').attr("src", $scope.media + $scope.names.resto + '/' + $scope.names.logo);
			if($scope.action == 'cancel' && $scope.names.bkstatus == 'cancel')
				alert("This reservation has already been canceled on " + $scope.names.canceldate);
			else if($scope.action == 'cancel' && $scope.checkdate() == false)
				alert("Unable to cancel a reservation which date had already been passed (" + $scope.names.date + ")");
			else if($scope.action == 'cancel' && $scope.names.wheelwin != '')
				alert("Unable to cancel a reservation which had won a price (" + $scope.names.wheelwin  + ")");
			else $scope.stateinput = ($scope.action != 'cancel') ? 1 : 2;
			}
		else alert('Unable to cancel the reservation ' + $scope.confirmation + ". Error : " + response.errors);	
		});
  	}
		
  $scope.cancelConfirmation = function() {

        var url = '../../api/visit/cancel/' + $scope.restaurant + '/' + $scope.confirmation + '/' + $scope.guestemail;
        $http.get(url).success(function(response) { 
			if(response.status == "1") {
				$scope.stateinput = 3;
				alert('The reservation ' + $scope.confirmation + "for restaurant " + $scope.names.title + " has been canceled.");
				}
			else alert('Unable to cancel the reservation ' + $scope.confirmation + ". Error : " + response.errors);
			});
	}
	
	$scope.checkdate = function() {
		var month = cdate.getUTCMonth() + 1; //months from 1-12
		var day = cdate.getUTCDate();
		var year = cdate.getUTCFullYear();
		dd = $scope.names.date.split('-');
		year1 = parseInt(dd[0]); month1 = parseInt(dd[1]); day1 = parseInt(dd[2]);
		return (year1 > year || (year1 == year && (month1 > month || (month1 == month && day1 >= day))));
		} 
		
	$scope.newlocation = function(confirmation, email, state) {

		if(state == "cancel") {
			$scope.stateinput = 0;
			$scope.labelaction = 'Cancel your booking';
			$scope.action = 'cancel';
			//window.location.assign("visitcancel.php?confirmation=" + confirmation + "&bkemail=" + email);
			}
		else {
			$scope.stateinput = 0;
			$scope.labelaction = 'Your booking';
			$scope.action = 'review';
			//window.location.assign("visitview.php?confirmation=" + confirmation + "&bkemail=" + email);
			}
		}

});

function setLogo(resto) {

	} 


function getConfirmation(n){
    var scope = angular.element(document.getElementById("booking")).scope();
        scope.$apply(function() {
        scope.curday = n;
    });
}			

function termsconditions() {  $('#remember').toggleClass('show');   }

function IsEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}

<?php
	echo "var arglist = [";
	$sep = "";
	
	for(reset($arglist); list($index, $label) = each($arglist); $sep = ", ") {
		echo $sep . "'" . $label . "', '" . $$label . "'";
		}
	echo "];";
?>

function updatefield() { 
	for(i = 0; i < arglist.length; i+=2) if(arglist[i+1] != '') {
		$('#'+arglist[i]).val(arglist[i+1]); 
		}
	}
	
function CleanEmail(obj) { obj.value = obj.value.replace(/[!#$%^&*()=}{\]\[:;><\?/|\\]/g, ''); }
function CleanTel(obj) { obj.value = obj.value.replace(/[^0-9 \+]/g, ''); }
function CleanPass(obj) { obj.value = obj.value.replace(/[^0-9A-Za-z]/g, ''); }
function CleanText(obj) { obj.value = obj.value.replace(/[!@#$%^&*()=}{\]\[\"\':;><\?/|\\]/g, ''); }
function invalid(id, str) { alert(str); $(id).focus(); return false; }
function checkSubmit() {

    $('#book_form input[type=text], input[type=password]').each( function() { 
    	this.value = this.value.trim();
		});
		
	if($('#bkconfirmation').val() == "")
		return invalid('#bkconfirmation', 'Invalid confirmation, try again');
	if($('#bkemail').val() == "" || !IsEmail($('#bkemail').val()))
		return invalid('#bkemail', 'Invalid email, try again');
	if($('#bkchtc').is(':checked') == false)
		return invalid('#bkchtc', 'You need to approve the terms and conditions in order to reserve a table');

	return true;	
}

</script>
</body>
</html>

