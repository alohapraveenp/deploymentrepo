<?php

require_once("conf/conf.init.inc.php");
require_once("lib/wpdo.inc.php");
require_once("lib/wglobals.inc.php");
require_once("lib/class.booking.inc.php");
require_once("lib/Browser.inc.php");

$type = 0;	
$arglist = array('bkconfirmation', 'bkrestaurant', 'type', 'bktracking');
foreach ($arglist as $label)
	$$label = preg_replace("/\'|\"/", "’", $_REQUEST[$label]);

$type = intval($type);

$res = new WY_Booking($bkrestaurant, "", "");
if($res->getBooking($bkconfirmation) == false)
	exit;

	$is_white_label = false;
	if($bktracking == 'CALLCENTER' || $bktracking == 'WEBSITE' || $bktracking == 'facebook' || $bktracking == 'GRABZ'){
		$is_white_label = true;
	}
        

$timeAr = explode(":", $res->rtime);
$hour = $timeAr[0];
$minutes = intval($timeAr[1]);
$timestr = sprintf("T%02d%02d00", $hour, $minutes);

$rdate = preg_replace("/-/", "", $res->rdate);	// be carefull msqyl Y-m-d, html d/m/Y  qrc Ymd

$size="300x300";

$note = "confirmation=" . $res->confirmation . ", " . $res->firstname . " " . $res->lastname . ", " . "date=" . $res->rdate . ", " . "time=" . $res->rtime . ", " . "MemCode=" . $res->membCode;
$name = $res->restaurantinfo->title;
$adr = $res->restaurantinfo->address . ", " . $res->restaurantinfo->zip . ", " . $res->restaurantinfo->city . ", " . $res->restaurantinfo->email . ",  " . $res->restaurantinfo->url;
$tel = $res->restaurantinfo->tel;
$email = $res->restaurantinfo->email;
$url = $res->restaurantinfo->url;

$datarcad =	"MECARD:"
			. "NOTE:" . $note
	  		. ";N:" . $name
	  		. ";ADR:" . $adr
	  		. ";TEL:" . $tel
	  		. ";EMAIL:" . $email
	  		. ";URL:" . $url
	  		. ";;";

$title = "Dining at " . $res->restaurantinfo->title;
$arrival = $rdate . $timestr;
$address = $res->restaurantinfo->address . ", " . $res->restaurantinfo->zip . ", " . $res->restaurantinfo->city . ", " . $res->restaurantinfo->tel . ", " . $res->restaurantinfo->email . ",  " . $res->restaurantinfo->url;
if(is_white_label){
    $description = "confirmation=" . $res->confirmation . ",  " . "Number of guests=" . $res->cover;
}else{
    $description = "confirmation=" . $res->confirmation . ",  " . "Spin Code =" . $res->membCode . ",  " . "Number of guests=" . $res->cover;
}
$dataevent = "BEGIN:VEVENT"
			. ";" . "SUMMARY:" . $title
			. ";" . "DTSTART:" . $arrival
			. ";" . "LOCATION:" . $address
			. ";" . "DESCRIPTION:" . $description
			. ";END:VEVENT";
?>

<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>angular-qr</title>
  <style>

[ng-cloak] { display: none; }
body {
	margin:10px;
	}
	
.container {
  display: flex;
  justify-content: center;
}
.center {
  width: 300px;
}

    </style>
</head>
<body class="container" ng-app="app">

<div ng-controller="qrCtrl" class="container">
<div class="center">
<h3> {{Qtitle }}</h3>
<qr text="qrcodeString" type-number="typeNumber" correction-level="correctionLevel" size="size" input-mode="inputMode" image="image">
</qr></div>

</div>

<script src="../../client/bower_components/angular/angular.min.js"></script>
<script src="../../client/bower_components/angular/angular-animate.min.js"></script>
<script src="../../client/bower_components/angular-qr/lib/qrcode.js"></script>
<script src="../../client/bower_components/angular-qr/src/angular-qr.js"></script>

<script>
angular.module('app', ['ja.qr']).

controller('qrCtrl', function ($scope) {
   var data, type = parseInt(<? echo "'" . $type . "'" ?>);
    
   $scope.Qtitle = "Agenda"; 
   data = <? echo "'" . $dataevent . "'" ;?>;
   data = data.replace(/;/g, "\r\n");
   if(type == 0) {
	   $scope.Qtitle = "Contact"; 
	   data = <? echo "'" . $datarcad . "'"; ?>;
	   }

	console.log('DATA', data);	   
  //$scope.qrcodeString = 'MECARD:NOTE:confirmation=TheFuADCHAAX, Richard Kefs, date=2016-09-01, time=12:00, MemCode=1122;N:The Fun Kitchen;ADR:10 Bras Basah Road, 0124653, Singapore, support@weeloy.com,  ;TEL:+65 6339 7777;EMAIL:support@weeloy.com;URL:;;';
  $scope.qrcodeString = data;
  $scope.size = 200;
  $scope.correctionLevel = '';
  $scope.typeNumber = 0;
  $scope.inputMode = '';
  $scope.image = true;
});

</script>
</body>
</html>