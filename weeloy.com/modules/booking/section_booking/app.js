(function (app) {
    app.controller('SectionBookingCtrl', SectionBookingCtrl);
    SectionBookingCtrl.$inject = ['$rootScope', '$scope', '$window', '$q', '$http', '$localStorage','deviceDetector'];

    function SectionBookingCtrl($rootScope, $scope, $window, $q, $http, $localStorage, deviceDetector) {
        var hasDeposit = $("#isDeposit").val();
        
        
        $scope.isMobile = false; //deviceDetector.isMobile();
        
        
        $scope.hasDeposit = hasDeposit;
        
        
       
        if (getURLParameter('p')) {
              params = getURLParameters();
                console.log("SDASd"+params.bktracking.indexOf('nopayment'));
                if(params.bktracking.indexOf('nopayment') ===  -1){
                     $scope.date = params.bkdate;
                    var d = moment($scope.date, "DD/MM/YYYY");
                    $rootScope.selectedDate = moment(d).format('YYYY-MM-DD');
                }
        } 
        if (getURLParameter('bkrestaurant')) {
            $scope.restaurant = getURLParameter('bkrestaurant');
        } else {
            $scope.restaurant = $("#restaurant").val();
            //$scope.restaurant = $window.bkrestaurant;
        }

        if (getURLParameter('bktracking')) {
            $scope.bktracking = getURLParameter('bktracking');
        }else{
            $scope.bktracking = 'WEBSITE';
        }
        var params={};
       
        function getPaxArray(start, limit) {
            var i, ar = [];
            for (i = start; i <= limit; i++)
                ar.push(i);
            return ar;
        }
        
        (function() {  // init
  
          
            var API_URL = 'api/restaurant/section/'+$scope.restaurant;
         
			$http.get(API_URL).then(function (response) {
             	 var ccflg = ($scope.bktracking.indexOf('callcenter|private') > -1 );
             	 
                 if(response.data){
                      $scope.sections = response.data.data;
                    
                 /**
                * Initial Type
                */
                $scope.sections.forEach(function(oo) {
			oo.footer = (oo.additional_info && oo.additional_info.footer && oo.additional_info.footer.content) ? oo.additional_info.footer.content : "";
			oo.reminder = (oo.additional_info && oo.additional_info.section && oo.additional_info.section.content) ? oo.additional_info.section.content : "";
			oo.min_pax = parseInt(oo.min_pax);
			oo.max_pax = parseInt(oo.max_pax);
			oo.paxs = getPaxArray(oo.min_pax, oo.max_pax);
			oo.laphour = ( oo.additional_info && oo.additional_info.section && typeof oo.additional_info.section.laptime === "string") ? parseInt(oo.additional_info.section.laptime) : 0;
			oo.laptime = Math.floor(oo.laphour / 24);
			oo.laphour = oo.laphour % 24;
			oo.image = ( oo.additional_info && oo.additional_info.section && typeof oo.additional_info.section.image === "string") ? oo.additional_info.section.image : "burnt_ends_layout.png";
            		oo.computecharge = ( oo.additional_info && oo.additional_info.section && typeof oo.additional_info.section.computecharge === "string") ? parseInt(oo.additional_info.section.computecharge) : 0;
			oo.oh = { lunch: {}, dinner: {} } 
			oo.ohe = { lunch: {}, dinner: {} } 
			oo.getoh = (ccflg) ? oo.ohe : oo.oh;
			oo.types = [];
			oo.open_hour.forEach(function(vv) { 
				var l = vv.name;
				if(["lunch", "dinner"].indexOf(l) > -1) {
					oo.types.push(l);
					oo.oh[l].times = vv.times.slice(0);
					oo.oh[l].cancel_charge = vv.cancel_charge;
					}
				});
			oo.open_hour_extended.forEach(function(vv) { 
				var l = vv.name;
				if(["lunch", "dinner"].indexOf(l) > -1) {
					oo.ohe[l].times = vv.times.slice(0);
					oo.ohe[l].cancel_charge = vv.cancel_charge;
					}
				});
		});

		$scope.section = $scope.sections[0];
		$scope.sectionID = $scope.sections[0].id;    
		$scope.types =  $scope.section.types;

		$scope.paxs =  $scope.section.paxs;
		$scope.pax = $scope.paxs[0];

		$scope.mealtype = $scope.types[0]
		$scope.times = $scope.section.getoh[$scope.mealtype].times;
		$scope.time = $scope.times[0];
		$scope.rconfbarseat = false;
		
		//$scope.selectedDate = moment().format('YYYY-MM-DD');
		//$scope.checkDateAfterChangePax = angular.copy($scope.selectedDate);


		loadParamterValues();
		if($scope.bkerror){                    
			$scope.invalid($scope.bkerror);
		}
               

               $scope.deposit_amount = $scope.section.oh[$scope.mealtype].cancel_charge * $scope.pax;
               //$scope.deposit_amount = $scope.section.oh[$scope.mealtype].cancel_charge * $scope.pax;	
                 if($scope.section.computecharge === 12){
                    $scope.deposit_amount = $scope.section.oh[$scope.mealtype].cancel_charge;
                 }
                 console.log("MEAL TYPE " + $scope.mealtype);
                 console.log("DEPSOIT AMOUNT " + $scope.deposit_amount + "MEAL TYPE" + $scope.section.oh[$scope.mealtype].cancel_charge);
               params = {
                   restaurant: $scope.restaurant,
                   product: $scope.section.name,
                   time: $scope.time,
                   pers: $scope.pax,
                   mealtype: $scope.mealtype,
                   laptime: $scope.section.laptime,
                   laphour: $scope.section.laphour
               };
               
               getDayavailable(params);
               $scope.getParameterValidation();
                    
                }
            });
        })();

	var cstate = '';

    /**
         * Initial Section
         */  $scope.visible_section = 1;

        $scope.salutations = [{
                id: 1,
                salutation: 'Mr.'
            }, {
                id: 2,
                salutation: 'Mrs.'
            }, {
                id: 3,
                salutation: 'Ms.'
            }, {
                id: 4,
                salutation: 'Dr.'
            }];

        $scope.countries = [{'a': 'Singapore', 'b': 'sg', 'c': '+65'}, {'a': 'Australia', 'b': 'au', 'c': '+61'}, {'a': 'China', 'b': 'cn', 'c': '+86'}, {'a': 'Hong Kong', 'b': 'hk', 'c': '+852'}, {'a': 'India', 'b': 'in', 'c': '+91'}, {'a': 'Indonesia', 'b': 'id', 'c': '+62'}, {'a': 'Japan', 'b': 'jp', 'c': '+81'}, {'a': 'Malaysia', 'b': 'my', 'c': '+60'}, {'a': 'Myanmar', 'b': 'mm', 'c': '+95'}, {'a': 'New Zealand', 'b': 'nz', 'c': '+64'}, {'a': 'Philippines', 'b': 'ph', 'c': '+63'}, {'a': 'South Korea', 'b': 'kr', 'c': '+82'}, {'a': 'Thailand', 'b': 'th', 'c': '+66'}, {'a': 'Vietnam', 'b': 'vn', 'c': '+84'}, {'a': '', 'b': '', 'c': ''}, {'a': 'Canada', 'b': 'ca', 'c': '+1'}, {'a': 'France', 'b': 'fr', 'c': '+33'}, {'a': 'Germany', 'b': 'de', 'c': '+49'}, {'a': 'Italy', 'b': 'it', 'c': '+39'}, {'a': 'Russia', 'b': 'ru', 'c': '+7'}, {'a': 'Spain', 'b': 'es', 'c': '+34'}, {'a': 'Sweden', 'b': 'se', 'c': '+46'}, {'a': 'Switzerland', 'b': 'ch', 'c': '+41'}, {'a': 'UnitedKingdom', 'b': 'gb', 'c': '+44'}, {'a': 'UnitedStates', 'b': 'us', 'c': '+1'}, {'a': '', 'b': '', 'c': ''}, {'a': 'Afghanistan', 'b': 'af', 'c': '+93'}, {'a': 'Albania', 'b': 'al', 'c': '+355'}, {'a': 'Algeria', 'b': 'dz', 'c': '+213'}, {'a': 'Andorra', 'b': 'ad', 'c': '+376'}, {'a': 'Angola', 'b': 'ao', 'c': '+244'}, {'a': 'Antarctica', 'b': 'aq', 'c': '+672'}, {'a': 'Argentina', 'b': 'ar', 'c': '+54'}, {'a': 'Armenia', 'b': 'am', 'c': '+374'}, {'a': 'Aruba', 'b': 'aw', 'c': '+297'}, {'a': 'Austria', 'b': 'at', 'c': '+43'}, {'a': 'Azerbaijan', 'b': 'az', 'c': '+994'}, {'a': 'Bahrain', 'b': 'bh', 'c': '+973'}, {'a': 'Bangladesh', 'b': 'bd', 'c': '+880'}, {'a': 'Belarus', 'b': 'by', 'c': '+375'}, {'a': 'Belgium', 'b': 'be', 'c': '+32'}, {'a': 'Belize', 'b': 'bz', 'c': '+501'}, {'a': 'Benin', 'b': 'bj', 'c': '+229'}, {'a': 'Bhutan', 'b': 'bt', 'c': '+975'}, {'a': 'Bolivia', 'b': 'bo', 'c': '+591'}, {'a': 'BosniaandHerzegovina', 'b': 'ba', 'c': '+387'}, {'a': 'Botswana', 'b': 'bw', 'c': '+267'}, {'a': 'Brazil', 'b': 'br', 'c': '+55'}, {'a': 'Brunei', 'b': 'bn', 'c': '+673'}, {'a': 'Bulgaria', 'b': 'bg', 'c': '+359'}, {'a': 'BurkinaFaso', 'b': 'bf', 'c': '+226'}, {'a': 'Burundi', 'b': 'bi', 'c': '+257'}, {'a': 'Cambodia', 'b': 'kh', 'c': '+855'}, {'a': 'Cameroon', 'b': 'cm', 'c': '+237'}, {'a': 'CapeVerde', 'b': 'cv', 'c': '+238'}, {'a': 'CentralAfricanRepublic', 'b': 'cf', 'c': '+236'}, {'a': 'Chad', 'b': 'td', 'c': '+235'}, {'a': 'Chile', 'b': 'cl', 'c': '+56'}, {'a': 'ChristmasIsland', 'b': 'cx', 'c': '+61'}, {'a': 'CocosIslands', 'b': 'cc', 'c': '+61'}, {'a': 'Colombia', 'b': 'co', 'c': '+57'}, {'a': 'Comoros', 'b': 'km', 'c': '+269'}, {'a': 'CookIslands', 'b': 'ck', 'c': '+682'}, {'a': 'CostaRica', 'b': 'cr', 'c': '+506'}, {'a': 'Croatia', 'b': 'hr', 'c': '+385'}, {'a': 'Cuba', 'b': 'cu', 'c': '+53'}, {'a': 'Curacao', 'b': 'cw', 'c': '+599'}, {'a': 'Cyprus', 'b': 'cy', 'c': '+357'}, {'a': 'CzechRepublic', 'b': 'cz', 'c': '+420'}, {'a': 'DemocraticRepCongo', 'b': 'cd', 'c': '+243'}, {'a': 'Denmark', 'b': 'dk', 'c': '+45'}, {'a': 'Djibouti', 'b': 'dj', 'c': '+253'}, {'a': 'EastTimor', 'b': 'tl', 'c': '+670'}, {'a': 'Ecuador', 'b': 'ec', 'c': '+593'}, {'a': 'Egypt', 'b': 'eg', 'c': '+20'}, {'a': 'ElSalvador', 'b': 'sv', 'c': '+503'}, {'a': 'EquatorialGuinea', 'b': 'gq', 'c': '+240'}, {'a': 'Eritrea', 'b': 'er', 'c': '+291'}, {'a': 'Estonia', 'b': 'ee', 'c': '+372'}, {'a': 'Ethiopia', 'b': 'et', 'c': '+251'}, {'a': 'FalklandIslands', 'b': 'fk', 'c': '+500'}, {'a': 'FaroeIslands', 'b': 'fo', 'c': '+298'}, {'a': 'Fiji', 'b': 'fj', 'c': '+679'}, {'a': 'Finland', 'b': 'fi', 'c': '+358'}, {'a': 'FrenchPolynesia', 'b': 'pf', 'c': '+689'}, {'a': 'Gabon', 'b': 'ga', 'c': '+241'}, {'a': 'Gambia', 'b': 'gm', 'c': '+220'}, {'a': 'Georgia', 'b': 'ge', 'c': '+995'}, {'a': 'Ghana', 'b': 'gh', 'c': '+233'}, {'a': 'Gibraltar', 'b': 'gi', 'c': '+350'}, {'a': 'Greece', 'b': 'gr', 'c': '+30'}, {'a': 'Greenland', 'b': 'gl', 'c': '+299'}, {'a': 'Guatemala', 'b': 'gt', 'c': '+502'}, {'a': 'Guernsey', 'b': 'gg', 'c': '+44-1481'}, {'a': 'Guinea', 'b': 'gn', 'c': '+224'}, {'a': 'Guinea-Bissau', 'b': 'gw', 'c': '+245'}, {'a': 'Guyana', 'b': 'gy', 'c': '+592'}, {'a': 'Haiti', 'b': 'ht', 'c': '+509'}, {'a': 'Honduras', 'b': 'hn', 'c': '+504'}, {'a': 'Hungary', 'b': 'hu', 'c': '+36'}, {'a': 'Iceland', 'b': 'is', 'c': '+354'}, {'a': 'Iran', 'b': 'ir', 'c': '+98'}, {'a': 'Iraq', 'b': 'iq', 'c': '+964'}, {'a': 'Ireland', 'b': 'ie', 'c': '+353'}, {'a': 'IsleofMan', 'b': 'im', 'c': '+44-1624'}, {'a': 'Israel', 'b': 'il', 'c': '+972'}, {'a': 'IvoryCoast', 'b': 'ci', 'c': '+225'}, {'a': 'Jersey', 'b': 'je', 'c': '+44-1534'}, {'a': 'Jordan', 'b': 'jo', 'c': '+962'}, {'a': 'Kazakhstan', 'b': 'kz', 'c': '+7'}, {'a': 'Kenya', 'b': 'ke', 'c': '+254'}, {'a': 'Kiribati', 'b': 'ki', 'c': '+686'}, {'a': 'Kosovo', 'b': 'xk', 'c': '+383'}, {'a': 'Kuwait', 'b': 'kw', 'c': '+965'}, {'a': 'Kyrgyzstan', 'b': 'kg', 'c': '+996'}, {'a': 'Laos', 'b': 'la', 'c': '+856'}, {'a': 'Latvia', 'b': 'lv', 'c': '+371'}, {'a': 'Lebanon', 'b': 'lb', 'c': '+961'}, {'a': 'Lesotho', 'b': 'ls', 'c': '+266'}, {'a': 'Liberia', 'b': 'lr', 'c': '+231'}, {'a': 'Libya', 'b': 'ly', 'c': '+218'}, {'a': 'Liechtenstein', 'b': 'li', 'c': '+423'}, {'a': 'Lithuania', 'b': 'lt', 'c': '+370'}, {'a': 'Luxembourg', 'b': 'lu', 'c': '+352'}, {'a': 'Macao', 'b': 'mo', 'c': '+853'}, {'a': 'Macedonia', 'b': 'mk', 'c': '+389'}, {'a': 'Madagascar', 'b': 'mg', 'c': '+261'}, {'a': 'Malawi', 'b': 'mw', 'c': '+265'}, {'a': 'Maldives', 'b': 'mv', 'c': '+960'}, {'a': 'Mali', 'b': 'ml', 'c': '+223'}, {'a': 'Malta', 'b': 'mt', 'c': '+356'}, {'a': 'MarshallIslands', 'b': 'mh', 'c': '+692'}, {'a': 'Mauritania', 'b': 'mr', 'c': '+222'}, {'a': 'Mauritius', 'b': 'mu', 'c': '+230'}, {'a': 'Mayotte', 'b': 'yt', 'c': '+262'}, {'a': 'Mexico', 'b': 'mx', 'c': '+52'}, {'a': 'Micronesia', 'b': 'fm', 'c': '+691'}, {'a': 'Moldova', 'b': 'md', 'c': '+373'}, {'a': 'Monaco', 'b': 'mc', 'c': '+377'}, {'a': 'Mongolia', 'b': 'mn', 'c': '+976'}, {'a': 'Montenegro', 'b': 'me', 'c': '+382'}, {'a': 'Morocco', 'b': 'ma', 'c': '+212'}, {'a': 'Mozambique', 'b': 'mz', 'c': '+258'}, {'a': 'Namibia', 'b': 'na', 'c': '+264'}, {'a': 'Nauru', 'b': 'nr', 'c': '+674'}, {'a': 'Nepal', 'b': 'np', 'c': '+977'}, {'a': 'Netherlands', 'b': 'nl', 'c': '+31'}, {'a': 'NetherlandsAntilles', 'b': 'an', 'c': '+599'}, {'a': 'NewCaledonia', 'b': 'nc', 'c': '+687'}, {'a': 'Nicaragua', 'b': 'ni', 'c': '+505'}, {'a': 'Niger', 'b': 'ne', 'c': '+227'}, {'a': 'Nigeria', 'b': 'ng', 'c': '+234'}, {'a': 'Niue', 'b': 'nu', 'c': '+683'}, {'a': 'NorthKorea', 'b': 'kp', 'c': '+850'}, {'a': 'Norway', 'b': 'no', 'c': '+47'}, {'a': 'Oman', 'b': 'om', 'c': '+968'}, {'a': 'Pakistan', 'b': 'pk', 'c': '+92'}, {'a': 'Palau', 'b': 'pw', 'c': '+680'}, {'a': 'Palestine', 'b': 'ps', 'c': '+970'}, {'a': 'Panama', 'b': 'pa', 'c': '+507'}, {'a': 'PapuaNewGuinea', 'b': 'pg', 'c': '+675'}, {'a': 'Paraguay', 'b': 'py', 'c': '+595'}, {'a': 'Peru', 'b': 'pe', 'c': '+51'}, {'a': 'Pitcairn', 'b': 'pn', 'c': '+64'}, {'a': 'Poland', 'b': 'pl', 'c': '+48'}, {'a': 'Portugal', 'b': 'pt', 'c': '+351'}, {'a': 'Qatar', 'b': 'qa', 'c': '+974'}, {'a': 'RepublicCongo', 'b': 'cg', 'c': '+242'}, {'a': 'Reunion', 'b': 're', 'c': '+262'}, {'a': 'Romania', 'b': 'ro', 'c': '+40'}, {'a': 'Rwanda', 'b': 'rw', 'c': '+250'}, {'a': 'SaintBarthelemy', 'b': 'bl', 'c': '+590'}, {'a': 'SaintHelena', 'b': 'sh', 'c': '+290'}, {'a': 'SaintMartin', 'b': 'mf', 'c': '+590'}, {'a': 'Samoa', 'b': 'ws', 'c': '+685'}, {'a': 'SanMarino', 'b': 'sm', 'c': '+378'}, {'a': 'SaudiArabia', 'b': 'sa', 'c': '+966'}, {'a': 'Senegal', 'b': 'sn', 'c': '+221'}, {'a': 'Serbia', 'b': 'rs', 'c': '+381'}, {'a': 'Seychelles', 'b': 'sc', 'c': '+248'}, {'a': 'SierraLeone', 'b': 'sl', 'c': '+232'}, {'a': 'Slovakia', 'b': 'sk', 'c': '+421'}, {'a': 'Slovenia', 'b': 'si', 'c': '+386'}, {'a': 'SolomonIslands', 'b': 'sb', 'c': '+677'}, {'a': 'Somalia', 'b': 'so', 'c': '+252'}, {'a': 'SouthAfrica', 'b': 'za', 'c': '+27'}, {'a': 'SouthSudan', 'b': 'ss', 'c': '+211'}, {'a': 'SriLanka', 'b': 'lk', 'c': '+94'}, {'a': 'Sudan', 'b': 'sd', 'c': '+249'}, {'a': 'Suriname', 'b': 'sr', 'c': '+597'}, {'a': 'Swaziland', 'b': 'sz', 'c': '+268'}, {'a': 'Syria', 'b': 'sy', 'c': '+963'}, {'a': 'Taiwan', 'b': 'tw', 'c': '+886'}, {'a': 'Tajikistan', 'b': 'tj', 'c': '+992'}, {'a': 'Tanzania', 'b': 'tz', 'c': '+255'}, {'a': 'Togo', 'b': 'tg', 'c': '+228'}, {'a': 'Tokelau', 'b': 'tk', 'c': '+690'}, {'a': 'Tonga', 'b': 'to', 'c': '+676'}, {'a': 'Tunisia', 'b': 'tn', 'c': '+216'}, {'a': 'Turkey', 'b': 'tr', 'c': '+90'}, {'a': 'Turkmenistan', 'b': 'tm', 'c': '+993'}, {'a': 'Tuvalu', 'b': 'tv', 'c': '+688'}, {'a': 'Uganda', 'b': 'ug', 'c': '+256'}, {'a': 'Ukraine', 'b': 'ua', 'c': '+380'}, {'a': 'UnitedArabEmirates', 'b': 'ae', 'c': '+971'}, {'a': 'Uruguay', 'b': 'uy', 'c': '+598'}, {'a': 'Uzbekistan', 'b': 'uz', 'c': '+998'}, {'a': 'Vanuatu', 'b': 'vu', 'c': '+678'}, {'a': 'Vatican', 'b': 'va', 'c': '+379'}, {'a': 'Venezuela', 'b': 've', 'c': '+58'}, {'a': 'WallisandFutuna', 'b': 'wf', 'c': '+681'}, {'a': 'WesternSahara', 'b': 'eh', 'c': '+212'}, {'a': 'Yemen', 'b': 'ye', 'c': '+967'}, {'a': 'Zambia', 'b': 'zm', 'c': '+260'}, {'a': 'Zimbabwe', 'b': 'zw', 'c': '+263'}];
        $scope.currentcc = 0;

        $scope.salutation = $scope.salutations[0];


        function loadParamterValues(){
            
            params = getURLParameters();
            if(!params){
                return;
            }
            
            if(params.bktracking){
                $scope.bktracking = params.bktracking; 
                if($scope.bktracking.indexOf('nopayment') !==-1)
                { 
                    $scope.hasDeposit=0;
                }
            }
            
            if(params.bkproductid){
                $scope.sectionID = parseInt(params.bkproductid);
                if($scope.sectionID === 3){
                    $scope.rconfbarseat = true;
                }
            }
            
            
            if(params.bkproduct){
                $scope.section = getSectionObject(params.bkproduct);         
            }
                        
            if(params.mealtype){
                $scope.mealtype = getTypeObject(params.mealtype);
            }
            
            $scope.times = $scope.section.getoh[$scope.mealtype].times;
            
            if(params.bkcover){
                $scope.pax = parseInt(params.bkcover);
                $scope.paxs = $scope.section.paxs;
            }
            if(params.bkdate){
                $scope.date = params.bkdate;
                var d = moment($scope.date, "DD/MM/YYYY");
                $rootScope.selectedDate = moment(d).format('YYYY-MM-DD');
            }
            if(params.bktime){$scope.time = params.bktime;}
            if(params.mealtype){$scope.mealtype = getTypeObject(params.mealtype);}
            if(params.salutation){$scope.salutation = getSalutationObject(params.salutation);}
            if(params.bkfirst){$scope.bkfirst = params.bkfirst;}
            if(params.bklast){$scope.bklast = params.bklast;}
            if(params.bkemail){$scope.bkemail = params.bkemail;}
            if(params.bkmobile){$scope.bkmobile = params.bkmobile;}
            
            if(params.bkerror){$scope.bkerror = params.bkerror;}
          

            if(params.bkspecialrequest){$scope.bkspecialrequest = params.bkspecialrequest;}
            $scope.validation_continue = true;
            $scope.validation_booknow = true;
            
        }
        
        function getSectionObject(name){
            for (var i = 0; i < $scope.sections.length; i++) {
                if($scope.sections[i].name === name){return $scope.sections[i];}
            };
            return false;    
        }
        
        function getTypeObject(type_name){
            for (var i = 0; i < $scope.types.length; i++) {
                if($scope.types[i] === type_name){return $scope.types[i];}
            };
            return false;    
        }

        function getSalutationObject(salutation_name){
            for (var i = 0; i < $scope.types.length; i++) {
                if($scope.salutations[i].salutation === salutation_name){return $scope.salutations[i];}
            };
            return false;    
        }
        
        function getToken() {
            var API_URL = 'api/v2/system/secure/token';
            var defferred = $q.defer();
            $http.get(API_URL, params, {
                cache: false,
            }).success(function (response) {
                $scope.token = response.data.token;
            }).error(function (err) {
                console.log(err);
            });
        }


        function getDayavailable(params) {
            
            if(typeof params.laptime !== "number") 
            	params.laptime = 0;
            	
            // HACK chef table
            if($scope.sectionID === 1){
                params.pers = 8;
            }
            
            var API_URL = 'api/restaurant/dayavailable/';
            var defferred = $q.defer();
            $http.post(API_URL, params, {
                cache: false,
            }).success(function (response) {
                generateAvailableDatePicker(response, params.laptime, params.laphour, params.mealtype);
            }).error(function (err) {
                console.log(err);
            });
        }
        
        //getDayavailable(params);

        function getURLParameter(sParam) {
            var sPageURL = window.location.search.substring(1);

            var sURLVariables = sPageURL.split('&');
            for (var i = 0; i < sURLVariables.length; i++) {
                var sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] == sParam) {
                    var myParams = '';
                    for (var j = 1; j < sParameterName.length; j++){
                        myParams += sParameterName[j];
                    }
                    return myParams;
                }
            }
            return false;
        }

        function getURLParameters() {
            var res = {};
            var sParams = getURLParameter('p');
            if(!sParams){return false;};
            
            var sPageURL = atob(sParams);
        
     
            var sURLVariables = sPageURL.split('&');
            for (var i = 0; i < sURLVariables.length; i++) {
                var sParameterName = sURLVariables[i].split('=');
                var myParams  = '';
                for (var j = 1; j < sParameterName.length; j++){
                    myParams += sParameterName[j];
                }
                res[sParameterName[0]] = myParams;
            }
            return res;
        }

        function generateAvailableDatePicker(dayavailable, laptime, laphour, mealtype) {
            var enabledDates = [];

            var uncheck = true;
            if (dayavailable.data.availability !== undefined) {
                var daylength = dayavailable.data.availability.length;
                var addMoreDay = 0;
                var day, ctime;
                if(mealtype !== "" && laphour > 0 && ['lunch', 'dinner'].indexOf(mealtype) > -1) {
                	ctime = new Date().getHours();
                	if(mealtype === "lunch") {
                		if(ctime + laphour >= 12) 
                			laptime++;
                		}
                	else if(mealtype === "dinner") {
                		if(ctime + laphour >= 18) 
                			laptime++;
                		}
                	}
                	
                for (var i = 0; i < daylength; i++) {
                    if (i >= laptime && dayavailable.data.availability.charAt(i) === '1') {
                        day = moment().add(i, 'days');
                        if(uncheck === true && $scope.selectedDate && $scope.selectedDate === moment(day).format('YYYY-MM-DD')){
                            uncheck = false;
                        }
                        enabledDates.push(day);
                    }
                }
            }
            $scope.enabledDates = enabledDates;
            if($scope.selectedDate && uncheck === true){
                $scope.validation_continue = false;
            }
        }

        
        $scope.overwriteAvb = function(avb){
            if(avb === true){
                var now = moment().format("YYYY-MM-DD");
                var overwritedata = {
                        data:{availability: "111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111", stardate: now}
                };
                generateAvailableDatePicker(overwritedata, 0, 0, '');
            }
        }
        
        $scope.changeSectionRadio = function (section) {
            
		$scope.section = section;
		$scope.sectionID = section.id;
		$scope.footer = section.footer;
		params.product = section.name;
		params.laptime = 0;
	    
	    	$scope.types = section.types;
	    	if(section.types.indexOf($scope.mealtype) < 0)
			$scope.mealtype =section.types[0];

		$scope.times = section.oh[$scope.mealtype].times;
		if($scope.times.indexOf($scope.time) < 0)
			$scope.time = $scope.times[0];

		$scope.paxs = section.paxs;
		if($scope.paxs.indexOf($scope.pax) < 0)
			$scope.pax = $scope.paxs[0];
			
		params.time = $scope.time;
		params.laptime = section.laptime;
		params.pers = $scope.pax;
		params.mealtype = $scope.mealtype;

		$scope.times =$scope.section.getoh[$scope.mealtype].times;
		if($scope.times.indexOf($scope.time) < 0) 
			$scope.time = $scope.times[0];
		$scope.deposit_amount = $scope.section.oh[$scope.mealtype].cancel_charge * $scope.pax;	
                if($scope.section.computecharge === 12){
                    $scope.deposit_amount = $scope.section.oh[$scope.mealtype].cancel_charge;
                }
                    console.log("DEPOSIT AMOUNT " + $scope.deposit_amount  +"SECTION NAME " + $scope.section.name.toLowerCase().trim());

		$scope.rconfbarseat = false;
		$scope.getParameterValidation();
		getDayavailable(params);
        };

        $scope.changeRequiredButton = function () {
           	$scope.rconfbarseat = !$scope.rconfbarseat;
 		$scope.getParameterValidation();
        }

        $scope.changePax = function (pax) {
            $scope.pax = pax;
            params.pers = pax;
            $scope.deposit_amount = $scope.section.oh[$scope.mealtype].cancel_charge * $scope.pax;	
		
            if($scope.section.computecharge === 12){
                   $scope.deposit_amount = $scope.section.oh[$scope.mealtype].cancel_charge;
            } 
            console.log("DEPOSIT AMOUNT " + $scope.deposit_amount  +"SECTION NAME " + $scope.section.name.toLowerCase().trim());
            $scope.getParameterValidation();
            getDayavailable(params);
        };

        $rootScope.$on('selectedDate', function (event, selectedDate) {
            $scope.cleardropdown('date');
            $scope.selectedDate = selectedDate;
            $scope.validation_continue = selectedDate && (!$scope.section.require_confirmation || $scope.rconfbarseat);
            $scope.$apply(); //this triggers a $digest
        });

        $scope.changeType = function (mealtype) {
            $scope.mealtype = params.mealtype = mealtype;
            $scope.times = $scope.section.getoh[$scope.mealtype].times;
            $scope.time = $scope.times[0];
            $scope.deposit_amount = $scope.section.oh[$scope.mealtype].cancel_charge * $scope.pax;	
            if($scope.section.name.toLowerCase().trim() === 'pdr (3rd floor)'){
               $scope.deposit_amount = $scope.section.oh[$scope.mealtype].cancel_charge;
            }
             console.log("DEPOSIT AMOUNT " + $scope.deposit_amount  +"SECTION NAME " + $scope.section.name.toLowerCase().trim());
            params.time = $scope.time;
            $scope.getParameterValidation();
            getDayavailable(params);
        };

        $scope.changeTime = function (time) {
            $scope.time = time;
            params.time = time;
            $scope.getParameterValidation();
            getDayavailable(params);
        };

        $scope.changeSalutation = function (salutation) {
            $scope.salutation = salutation;
        };

        $scope.getParameterValidation = function(){
            $scope.validation_continue = $scope.selectedDate && (!$scope.section.require_confirmation || $scope.rconfbarseat);
        }
        
        
        
        var dropdown_label = [ 'time', 'cover', 'type','title'];
        $scope.dropdownLabel = dropdown_label;
	for (i = 0; i < dropdown_label.length; i++)
		$scope[dropdown_label[i] + 'opened'] = false;
        $scope.cleardropdown = function (selector) {
            for (i = 0; i < dropdown_label.length; i++)
                    if (selector != dropdown_label[i]) {
                            $scope[dropdown_label[i] + 'opened'] = false;
          }
	}

        $scope.opendropdown = function ($event, selector) {
 		$scope.cleardropdown(selector);	// close all the others	
		$scope[selector + 'opened'] = ($scope[selector + 'opened']) ? false : true;
                $event.preventDefault();
                $event.stopPropagation();
		
	};
        // launch first time on init
        //$scope.getParameterValidation();
        
        
       
//form validation 

//OLD code
        $scope.phoneindex = function (code) {
            var i, val;

            val = $scope.bkmobile;
            if (typeof val === 'undefined' || val === '')
                return;

            val = val.trim();
            val = val.replace(/[^0-9 \+]/g, "");
            val = val.replace(/[ ]+/g, " ");

            if (val.indexOf(code + ' ') == 0) {
                val = val.replace(/^\+(\d{2,3})[ ]*\1[ ]*/, '+$1 '); // replace repetition of contry code
                return $scope.bkmobile = val.trim();
            }

            if (val.indexOf(code) == 0) {
                val = code + ' ' + val.substring(code.length);
                val = val.replace(/^\+(\d{2,3})[ ]*\1[ ]*/, '+$1 '); // replace repetition of contry code
                return $scope.bkmobile = val.trim();
            }

            if ((res = val.match(/^\+\d{2,3} /)) != null) {
                val = val.replace(/^\+\d{2,3} /, "");
                val = code + ' ' + val;
                val = val.replace(/^\+(\d{2,3})[ ]*\1[ ]*/, '+$1 '); // replace repetition of contry code
                return $scope.bkmobile = val.trim();
            }

            if ((res = val.match(/^\+\d{2,3}$/)) != null) {
                val = val.replace(/^\+\d{2,3}/, "");
                val = code + ' ' + val;
                val = val.replace(/^\+(\d{2,3})[ ]*\1[ ]*/, '+$1 '); // replace repetition of contry code
                return $scope.bkmobile = val.trim();
            }

            if (val.match(/^\+/) != null) {
                val = code + ' ' + val.substring(1);
                val = val.replace(/^\+(\d{2,3})[ ]*\1[ ]*/, '+$1 '); // replace repetition of contry code
                return $scope.bkmobile = val.trim();
            }

            val = val.replace(/\+ /, "");
            val = code + ' ' + val;
            val = val.replace(/^\+(\d{2,3})[ ]*\1[ ]*/, '+$1 '); // replace repetition of contry code
            return $scope.bkmobile = val.trim();
        };

        $scope.setMobilePhone = function (tt) {
            for (i = 0; i < $scope.countries.length; i++)
                if ($scope.countries[i].a == tt) {
                    $scope.currentcc = i;
                    break;
                }

            if (i >= $scope.countries.length) {
                $scope.invalid('Invalid Country Code');
                $scope.currentcc = 10; // Singapore
            }
            $scope.curflgicon = "famfamfam-flag-" + $scope.countries[$scope.currentcc].b;
            $('#bkcountry').val($scope.countries[$scope.currentcc].a);
            $scope.phoneindex($scope.countries[$scope.currentcc].c);
        }


        $scope.invalid = function (str, id) {
		if($scope.bktracking != 'WEBSITE' && $scope.bktracking != 'facebook' && $scope.bktracking != 'GRABZ' && false){
			alert(str);
		}else{
                    console.log("Error str =" +str);
			$('#error-message').text(str);
			$('#error-message').show();
		}	
                
		if (typeof id === "string" && id !== "")
			$('#' + id).focus();
		
		//$timeout( function() { $('#error-message').text(""); }, 10000);
	}
        
        

        $scope.checkvalidtel = function () {
            $scope.bkmobile = $scope.cleantel($scope.bkmobile);
            if ($scope.bkmobile == "") {
                $scope.bkmobile == $scope.countries[$scope.currentcc].c.trim() + ' ';
                return;
            }

            $scope.curflgicon = "famfamfam-flag-" + $scope.countries[$scope.currentcc].b;
            $('#bkcountry').val($scope.countries[$scope.currentcc].a);
            $scope.phoneindex($scope.countries[$scope.currentcc].c);
        };


        $scope.IsEmail = function (email) {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return regex.test(email);
        }
        $scope.cleanemail = function (obj) {
            obj = obj.replace(/[!#$%^&*()=}{\]\[\"\':;><\?/|\\]/g, '');
            $scope.is_valid_form();
            return obj;
        }
        $scope.cleantel = function (obj) {
            obj = obj.replace(/[^0-9 \+]/g, '');
            $scope.is_valid_form();
            return obj;
        }
        $scope.cleanpass = function (obj) {
            obj = obj.replace(/[\'\"]/g, '');
            $scope.is_valid_form();
            return obj;
        }
        $scope.cleantext = function (obj) {
            obj = obj.replace(/[!@#$%^&*()=}{\]\[\"\':;><\?/|\\]/g, '');
            $scope.is_valid_form();
            return obj;
        }

// OLD code

        $scope.continueForm = function () {
            if($scope.validation_continue === false){
                return false;
            }
            //s$cope.validation_continue = false;
            if (!$scope.selectedDate) {
                //Notification.show('warning', 'Please confirm your choice');
                return false;
            }
            
            if (typeof $scope.sectionID !== "number") {
                //Notification.show('warning', 'Please select section');
                return false;
            }

            if ($scope.section.require_confirmation && !$scope.rconfbarseat) {
                //Notification.show('warning', 'Please confirm your choice');
                return false;
            }

            if ($scope.selectedDate === undefined | $scope.selectedDate === null) {
                //Notification.show('warning', 'Please select date');
                return false;
            }
            if (typeof $scope.pax !== "number") {
                //Notification.show('warning', 'Please select pax');
                return false;
            }
            if (typeof $scope.mealtype !== "string") {
                return false;
            }
            if (typeof $scope.time !== "string") {
                //Notification.show('warning', 'Please select time');
                return false;
            }

            $scope.booking_date2 = moment($scope.selectedDate).format('DD/MM/YYYY');
            $scope.visible_section = 2;
        }

        $scope.go_back = function () {
            $scope.visible_section = 1;
        }

        $scope.is_valid_form = function(){
            $scope.validation_booknow = $scope.bkfirst && $scope.bklast && $scope.IsEmail($scope.bkemail) && $scope.bkmobile && $scope.bkmobile.length > 5 ;
        }
        $scope.force_on_auto_fill = function(){
            $scope.bkemail = $('#bkemail').val();
            $scope.bkfirst = $('#bkfirst').val();
            $scope.bklast = $('#bklast').val();
            $scope.bkmobile = $('#bkmobile').val();
            $scope.bkspecialrequest = $('#bkspecialrequest').val();
            $scope.is_valid_form();
        }

        $scope.bookNow = function () {

            if (!$scope.IsEmail($scope.bkemail)) {
                //Notification.show('warning', 'Invalid email');
                return false;
            }

            $scope.phoneindex($scope.countries[$scope.currentcc].c);

            $('#booking_form input[type=text], input[type=password]').each(function () {
                this.value = this.value.trim();
            });

            $scope.bkextra = $scope.bkarea = $scope.bkpurpose = "";
            if (false) {
                if (typeof $scope.bkarea === 'string' && $scope.bkarea !== "")
                    $scope.bkextra += (($scope.bkextra !== "") ? "|" : "") + "area=" + $scope.bkarea;
                if (typeof $scope.bkpurpose === 'string' && $scope.bkpurpose !== "")
                    $scope.bkextra += (($scope.bkextra !== "") ? "|" : "") + "purpose=" + $scope.bkpurpose;
            }

            $('#bkextra').val($scope.bkextra);

            if ($scope.bktracking != '') {
                removelgcookie("weeloy_be_tracking");
                //setlgcookie("weeloy_be_tracking", tracking, 1);
            }

            document.getElementById("booking_form").submit();
        };
        
        
        // HACK FOR BURNT ENDS
        typeBooking = true;
        is_listing = false;
        
        
	$scope.buttonLabel = (typeBooking) ? 'book now' : 'request your spin code';
	$scope.bookingTitle = (typeBooking) ? "book your table" : "get your spin code";
	$scope.buttonConfirm = 'Confirm';
	$scope.timeouttag = "time out";
	$scope.listTags0 = "Free Booking";
	$scope.listTags1 = "Free booking";
	$scope.listTags2 = (typeBooking) ? "Instant Reservation" : "No reservartion";
	$scope.listTags3 = (typeBooking) ? "" : "First come, first serve";
	$scope.listTags4 = (!is_listing) ? "Win for every booking" : "Enjoy incredible promotion";
        $scope.bookingtitlecf = (typeBooking) ? "BOOKING CONFIRMED" : "REQUESTED CONFIRMED";
	$scope.confirmsg = (typeBooking) ? "Your booking is confirmed at " : "Your request has been sent to ";
        $scope.confirmsg2 = (typeBooking) ? "We are happy to confirm your reservation" : "Your reservation number is ";
        $scope.confirmsg3 = "You will receive the details of your reservation by email and SMS";
    

	$scope.tmc1 = "By confirming, I agree to the ";
	$scope.tmc2 = "terms and conditions";
        $scope.canceltmc = "the cancellation fee";
	
	$scope.restau_conditions = "Conditions";

        $scope.slogan = 'Verify your information';
        $scope.slogan_sub = 'To confirm your booking, you must accept terms and conditions';
        	
	$scope.langdata = $localStorage.langdata;


	if (typeof $scope.zang === "undefined") {
		$scope.zang = {
                    
			'restaurants':  {"lb": "restaurant", "vl": "restaurant", "f": "c"},
			'version': {"lb": $scope.restaurant, "vl": $scope.bkdate, "f": "c"},
			'lunchtag': {"lb": "lunch", "vl": "Lunch", "f": "c"}, "dinnertag": {"lb": "dinner", "vl": "Dinner", "f": "c"}, "guesttag": {"lb": "guests", "vl": "Guests", "f": "c"}, "titletag": {"lb": "title", "vl": "Title", "f": "c"}, "firsttag": {"lb": "firstname", "vl": "First name", "f": "c"},
			'datetag': {"lb": "Date", "vl": "Date", "f": "c"}, 'timetag': {"lb": "time", "vl": "time", "f": "c"}, "nametag": {"lb": "name", "vl": "name", "f": "c"},
			'lasttag': {"lb": "lastname", "vl": "Last name", "f": "c"}, "emailtag": {"lb": "email", "vl": "Email", "f": "c"}, "mobiletag": {"lb": "mobile", "vl": "Mobile", "f": "c"}, "requesttag": {"lb": "special request", "vl": "Special request", "f": "c"}, "requestpromotag": {"lb": "special request / Promotion Codes", "vl": "Special Request / Promotion Codes", "f": "c"},
			'today': {"lb": "today", "vl": "Today", "f": "c"}, 'clear': {"lb": "clear", "vl": "clear", "f": "c"}, 'close': {"lb": "close", "vl": "close", "f": "c"},
			'bookingdetail': {"lb": "booking details", "vl": "booking details", "f": "u"},
			'personaldetail': {"lb": "personal details", "vl": "personal details", "f": "u"},
                        'confinfo': {lb: "Please confirm your information", vl: "Please confirm your information", f: "c"},
                     
//                        'depositdetail': {"lb": "deposit", "vl": "deposit", "f": "u"},
                       // 'depositbookform1': {"lb": "This restaurant requires a deposit to confirm the booking (per pax):", "vl": "This restaurant requires a deposit to confirm the booking (per pax):", "f": "c"},
			//'depositbookform2': {"lb": "This restaurant requires a deposit to confirm the booking :", "vl": "This restaurant requires a deposit to confirm the booking :", "f": "c"},
                        
//                        need to create translation for this section
                        'depositdetail': {"lb": "cancellation policy", "vl": "cancellation policy", "f": "u"},
                        'depositbookform1': {"lb": "The restaurant reserve the right to charge a cancellation fee based on this policy.", "vl": "The restaurant retrieve the right to change a cancellation fee based on this policy.", "f": "c"},
			'depositbookform2': {"lb": "The restaurant reserve the right to charge a cancellation fee based on this policy.", "vl": "The restaurant retrieve the right to change a cancellation fee based on this policy.s", "f": "c"},
			//'confinfo': {"lb": $scope.slogan, "vl": $scope.slogan, "f": "c"},
			'bookingtitlecf': {"lb": $scope.bookingtitlecf, "vl": $scope.bookingtitlecf, "f": "u"},
			'facebookfillin': {"lb": "Book with Facebook", "vl": "Book with Facebook", "f": "c"},
			'buttonbook': {"lb": $scope.buttonLabel, "vl": $scope.buttonLabel, "f": "u"},
			'buttonconfirm': {"lb": $scope.buttonConfirm, "vl": $scope.buttonConfirm, "f": "u"},
			'bookingtitle': {"lb": $scope.bookingTitle, "vl": $scope.bookingTitle, "f": "u"},
			'buttonmodify': {"lb": "modify booking", "vl": "modify booking", "f": "n"},
			'timeouttag': {"lb": "time out", "vl": "time out", "f": "u"},
			'listtag1': {"lb": $scope.listTags1, "vl": $scope.listTags1, "f": "c"},
			'listtag2': {"lb": $scope.listTags2, "vl": $scope.listTags2, "f": "c"},
			'listtag3': {"lb": $scope.listTags3, "vl": $scope.listTags3, "f": "c"},
			'listtag4': {"lb": $scope.listTags4, "vl": $scope.listTags4, "f": "c"},
			'listtag5': {"lb": $scope.listTags5, "vl": $scope.listTags5, "f": "n"},
			'confirmsg': {"lb": $scope.confirmsg, "vl": $scope.confirmsg, "f": "n"},
			'confirmsg2': {"lb": $scope.confirmsg2, "vl": $scope.confirmsg2, "f": "n"},
			'confirmsg3': {"lb": $scope.confirmsg3, "vl": $scope.confirmsg3, "f": "n"},
			'confirmsg4': {"lb": $scope.confirmsg4, "vl": $scope.confirmsg4, "f": "n"},
			'bacchanalia': {"lb": $scope.bacchanalia, "vl": $scope.bacchanalia, "f": "n"},
			'restau_conditions': {"lb": "Conditions", "vl": "Conditions", "f": "u"},
			'mr': {"lb": "Mr.", "vl": "Mr.", "f": "c"},
			'mrs': {"lb": "Mrs.", "vl": "Mrs.", "f": "c"},
			'ms': {"lb": "Ms.", "vl": "Ms.", "f": "c"},
                        'miss': {"lb": "Miss.", "vl": "Miss.", "f": "c"},
			'msg1': {"lb": $scope.msg1, "vl": $scope.msg1, "f": "n"},
			'error0': {"lb": $scope.error0, "vl": $scope.error0, "f": "c"},
			'error1': {"lb": $scope.error1, "vl": $scope.error1, "f": "c"},
			'error2': {"lb": $scope.error2, "vl": $scope.error2, "f": "c"},
			'error3': {"lb": $scope.error3, "vl": $scope.error3, "f": "c"},
			'error4': {"lb": $scope.error4, "vl": $scope.error4, "f": "c"},
			'error5': {"lb": $scope.error5, "vl": $scope.error5, "f": "c"},
			'error6': {"lb": $scope.error6, "vl": $scope.error6, "f": "c"},
			'error7': {"lb": $scope.error7, "vl": $scope.error7, "f": "c"},
			'error8': {"lb": $scope.error8, "vl": $scope.error8, "f": "c"},
			'error9': {"lb": $scope.error9, "vl": $scope.error9, "f": "c"},
			'error10': {"lb": $scope.error10, "vl": $scope.error10, "f": "c"},
			'error11': {"lb": $scope.error11, "vl": $scope.error11, "f": "c"},
			'error12': {"lb": $scope.error12, "vl": $scope.error12, "f": "n"},
			'error13': {"lb": $scope.error13, "vl": $scope.error13, "f": "n"},
			'tmc1': {"lb": $scope.tmc1, "vl": $scope.tmc1, "f": "c"},
			'tmc2': {"lb": $scope.tmc2, "vl": $scope.tmc2, "f": "c"},
                        'step': {"lb": "Step ", "vl": "Step ", "f": "n"},
                        'slogan': {"lb": $scope.slogan, "vl": $scope.slogan, "f": "n"},
                        'slogan_sub': {"lb": $scope.slogan_sub, "vl": $scope.slogan_sub, "f": "n"}
		};
		for (ele in $scope.zang) {
			$scope.zang[ele].vl = $scope.zang[ele].lb;
                        
			if ($scope.zang[ele].f == 'c')
				//$scope.zang[ele].vl = $scope.zang[ele].vl.capitalize();
                                $scope.zang[ele].vl = $scope.zang[ele].vl;

			else if ($scope.zang[ele].f == 'u')
				//$scope.zang[ele].vl = $scope.zang[ele].vl.toUpperCase();
                                $scope.zang[ele].vl = $scope.zang[ele].vl;
		}
	}

	$localStorage.langdata = $scope.zang;
	$scope.langue = 'en';
	$scope.pcheckmark = ($scope.langue != 'cn' && $scope.langue != 'th') ? 'pcheckmark' : 'pcheckmarkcn';

                
        
    }

//Removes the cookies
function removelgcookie(name) {
	setlgcookie(name, '', -1);
}

function setlgcookie(name, value, duration) {
	value = escape(value);
	if (duration) {
		var date = new Date();
		date.setTime(date.getTime() + (duration * 1 * 3600 * 1000));
		value += "; expires=" + date.toGMTString();
	}
	document.cookie = name + "=" + value;
}


function getlgcookie(name) {
	value = document.cookie.match('(?:^|;)\\s*' + name + '=([^;]*)');
	return value ? unescape(value[1]) : "";
}
    app.directive('datePicker', ['$rootScope', function ($rootScope) {
            return {
                strict: 'AE',
                scope: {
                    enabledDates: '=',
                    selectedDate: '=',
                    dropdownLabel: '='
                },
                link: function (scope, elem, attrs) {
              
                    var options = {
                        locale: 'en',
                        format: 'DD MMM, YYYY',
                        useCurrent: false,
                        showTodayButton: false,
                        inline: true,
                        keepOpen: true,
                        focusOnShow: false
                    };
                  
                    if($rootScope.selectedDate){options['defaultDate'] =$rootScope.selectedDate;}
                   
                    var DateTimePicker = $(elem).datetimepicker(options);
            

                    scope.$watch('enabledDates', function (enabledDates) {

                        if ($(elem).data('DateTimePicker') !== undefined && enabledDates !== undefined) {
                            if (enabledDates.length == 0) {
                                disableAllDate(enabledDates);
                            } else {
                                $(elem).data('DateTimePicker').enabledDates(enabledDates);
                            }
                            $(elem).on("dp.change", function (e) {
                               
                                var DateSelected = moment(e.date._d).format('YYYY-MM-DD');
                                $rootScope.$broadcast('selectedDate', DateSelected);
                            });
                        }
                    });
                    

                    DateTimePicker.on('dp.update', function (e) {
                        if (scope.enabledDates.length == 0) {
                            disableAllDate(scope.enabledDates);
                        }
                    });

                    function disableAllDate(enabledDates) {
                        enabledDates = [moment()];
                        $(elem).data('DateTimePicker').enabledDates(enabledDates);
                        $('.today').addClass('disabled');
                    }
                   
                }
            };
        }]);
    app.directive('noSpecialChar', function () {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function (scope, element, attrs, modelCtrl) {
                modelCtrl.$parsers.push(function (inputValue) {
                    if (inputValue == null)
                        return ''
                    cleanInputValue = inputValue.replace(/[^\w\s]/gi, '');
                    if (cleanInputValue != inputValue) {
                        modelCtrl.$setViewValue(cleanInputValue);
                        modelCtrl.$render();
                    }
                    return cleanInputValue;
                });
            }
        }
    });
    
})(angular.module('app', ['ui.bootstrap', 'ngLocale','ngAnimate', 'ngStorage', 'ng.deviceDetector']));
