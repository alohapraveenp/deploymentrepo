<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */




?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv='expires' content='mon, 1 jan 1998 05:00:00 gmt'/>
<meta http-equiv='pragma' content='no-cache'/>
<meta http-equiv='pragma' content='cache-control: max-age=0'/>
<meta http-equiv='last-modified' content='mon, 1 jan 1998 05:00:00 gmt'/>
<meta http-equiv='cache-control' content='no-cache, must-revalidate'/>
<meta name='robots' content='noindex, nofollow'/>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="HandheldFriendly" content="true" />
<title>Booking Confirmation - Weeloy</title>
<base href="<?php echo __ROOTDIR__; ?>/"/>
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap-select.css" rel="stylesheet" />
<link href="css/bootstrap-social.css" rel="stylesheet" />
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="modules/booking/assets/css/bookingform.css?v=1" rel="stylesheet" >

<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type='text/javascript' src="js/angular.min.js"></script>
<script type="text/javascript" src="js/ngStorage.min.js"></script>
<style>
    
</style>
<div id='booking'   >

	<div class="container mainbox">    
		<div class="panel panel-info"  >
			<div class="panel-heading">
				<div class="panel-title" style='width:300px;'> BOOKING DEPOSIT PAYMENT </div>
			</div>     

			
                    <p>To confirm the booking, please proceed to </p>
				<br /><span style="font-size:11px;" ng-if='flgsg'><a href='https://www.weeloy.com' target='_blank'></a> </span> 
			
		   

			<div class="panel-body" >
                            <div  class='row' style="margin-left:5px" ng-if="!hasPayment">
                                <h1 >PAYMENT DETAILS</h1>
                                 <div class="Payment_details">
                                 <span>Deposit Amount:</span><strong> SGD </strong><br /><br />
                                 <span>Restaurant:</span> <br /><br />
                                 <span>Date:</span> <br /><br />
                                 <span>Time:</span> <br /><br />
                                 <span>cover:</span> <br /><br />
                           </div>
                            <div  class='row' style="margin-left:5px">
                                <h1 >PAYMENT METHOD</h1>
                                 <div class="Payment_method">
                                     <div style="margin-top: 20px">
                                        <a  ng-click="depositCheckout()"    class="book-button-sm " ><img src="images/paypal.png" alt="" width="120px;" />  </a>
                                    </div>
                                
                                </div>
                            </div>
				</div>
<!--                            <div   style="margin-left:5px" ng-if="hasPayment">
                                 <p class="slogan">Your payment is being  processed.<br />
                                    Please  do not use back or forward button on your browser  as this end your transaction.<br />
                                    Please be patient until the transaction to be completed.</p><br />
                                <img src ="images/smallloader.gif" />
                                
                            </div>-->
			</div>
                
	</div>  
        </div>
</div>

