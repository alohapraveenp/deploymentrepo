<?
$colorcustomAr = array("bkbgdcolor", "bklegacy", "bkfontcolor", "bkbuttoncolor");
$bkbgdcolor = "black";
$bklegacy = "";
$bkfontcolor = "white";
$bkbuttoncolor = "#FF872C";

if (strlen($bkgcustomcolor) > 3 && preg_match("/\|/", $bkgcustomcolor)) {
	$datacustom = explode("|", $bkgcustomcolor);
	$limit = (count($datacustom) <= count($colorcustomAr)) ? count($datacustom) : count($colorcustomAr);
	for($i = 0; $i < $limit; $i++)
		if (strlen($datacustom[$i]) > 2) 
			${$colorcustomAr[$i]} = $datacustom[$i]; 
}

echo <<<DELIMITER
	h1 { background-color: $bkbgdcolor; color: $bkfontcolor; }
	.panel { border-color: $bkbgdcolor !important;  }
	.panel-heading { background-color: $bkbgdcolor !important; border-color: $bkbgdcolor !important; }
	.panel-title { color: $bkfontcolor; }
	.btn-leftBottom-orange { background-color: $bkbuttoncolor; border-color: $bkbuttoncolor; }
        @media (min-width: 530px) {
            .separation { border-right: 4px solid $bkbgdcolor; height: 100%; }
            .separation-left { border-left: 4px solid $bkbgdcolor; }
        }
DELIMITER;
?>
