<?php
require_once("conf/conf.init.inc.php");
require_once("lib/wpdo.inc.php");
require_once('conf/conf.session.inc.php');
require_once("lib/wglobals.inc.php");
require_once("lib/class.images.inc.php");
require_once("lib/class.restaurant.inc.php");
require_once("lib/class.booking.inc.php");
require_once("lib/class.mail.inc.php");
require_once("lib/class.sms.inc.php");
require_once("lib/class.pushnotif.inc.php");
require_once("lib/class.spool.inc.php");
require_once("lib/class.async.inc.php");
require_once("lib/Browser.inc.php");
require_once("lib/class.qrcode.inc.php");
require_once "lib/class.notification.inc.php";
require_once("lib/class.analytics.inc.php");
require_once("lib/class.media.inc.php");
require_once("lib/class.coding.inc.php");
require_once("lib/class.login.inc.php");
require_once("lib/class.transitory.inc.php");
require_once("lib/class.member.inc.php");
require_once("lib/class.allote.inc.php");
require_once("lib/class.marketing_campaign.inc.php");

$arglist = array('bklangue', 'bksalutation', 'bklast', 'bkfirst', 'bkemail', 'bkcover', 'bkdate', 'bktime', 'bkmobile', 'bkcountry', 'bkrestaurant', 'bktitle', 'bkparam', 'bkspecialrequest', 'brwsr_type', 'bkpage', 'bktracking', 'data', 'booker', 'company', 'hotelguest', 'babychair', 'signed_request', 'optin', 'booking_deposit_id', 'resBookingDeposit', 'resCurrency', 'bkextra', 'bkchild', 'bkproduct', 'bkproductid', 'bkpage1');
foreach ($arglist as $label) {
    if (!isset($_REQUEST[$label]))
        $_REQUEST[$label] = "";
    $$label = preg_replace("/\'|\"/", "’", $_REQUEST[$label]);
}

if($bkextra !== "") {
	$bkextra = preg_replace("/area\=|purpose=/", "", $bkextra);
	$bkextra = preg_replace("/\|/", ", ", $bkextra);
	$bkspecialrequest .= " " . $bkextra;
	}

$fullfeature = preg_match("/TheFunKitchen/", $bkrestaurant) || in_array($_SESSION['user']['member_type'], $fullfeature_member_type_allowed) || ($_SESSION['user']['member_type'] == 'weeloy_sales' && $res->status == 'demo_reference' || $res->status == 'active');

if ($fullfeature)
    $fullfeatflag = "'fullfeature=true;'";
else
    $fullfeatflag = "'fullfeature=false;'";

if (empty($bkrestaurant)) {
    header("location: https://www.weeloy.com");
    exit;
}
$fbId = WEBSITE_FB_APP_ID;
$res = new WY_restaurant;
$getRest = $res->getRestaurant($bkrestaurant);
$partialdesc = mb_strimwidth($getRest['description'], 0, 200, "...");
if ($partialdesc !== "")
    $partialdesc = preg_replace("/\r\n/", "", $partialdesc);

$mediadata = new WY_Media($bkrestaurant);
$logo = $mediadata->getLogo($bkrestaurant);
$notification = new WY_Notification();

if (empty($res->restaurant)) {
    header("location: https://www.weeloy.com");
    exit;
}
 if($bkpage1 == 'booking_form_section'){
    $action = __ROOTDIR__ . "/modules/booking/book_form_section.php";
 }else{
    $action = __ROOTDIR__ . "/modules/booking/book_form.php";
 }
$booker = "";
$logger = new WY_log("website");
$loguserid = (isset($_SESSION['user']['user_id'])) ? $_SESSION['user']['user_id'] : "unknown";
$logger->LogEvent($loguserid, 803, $res->ID, '', '', date("Y-m-d H:i:s"));

//booking lifecycle lof=g event

$token = tokenize(date("Y-m-d") . ";" . $bkemail);

$bkparamdecoded = detokenize($bkparam);
if (substr($bkparamdecoded, 0, 3) != date('M') || microtime(true) - floatval(substr($bkparamdecoded, 3)) > 600)
    header("Location: " . $action . "?bkrestaurant=" . $bkrestaurant . "&bktitle=" . $bktitle . "&bktracking=" . $bktracking . "&bkproduct=" . $bkproduct . "&data=" . $data . "&signed_request=" . $signed_request . "&timeout=y" . '&langue=' . $bklangue . '&bksalutation=' . $bksalutation . '&bklast=' . $bklast . '&bkfirst=' . $bkfirst . '&bkemail=' . $bkemail . '&bkcover=' . $bkcover . '&bkdate=' . $bkdate . '&bktime=' . $bktime . '&bkmobile=' . $bkmobile);

$restaurant_restaurant_tnc = $res->restaurant_tnc;

$booking = new WY_Booking();

$transitory = new WY_Transitory;
$transitory->read('BOOKING-' . $bkparam);

if ($transitory->result > 0) {
    $prev_confirmation = preg_replace("/BOOKING-/", "", $transitory->content);
    $booking->getBooking($prev_confirmation);
}

$dateAr = explode("/", $bkdate);
$bkDBdate = $dateAr[2] . "-" . $dateAr[1] . "-" . $dateAr[0];  // be carefull msqyl Y-m-d, html d/m/Y  qrc Ymd
$todayflg = ( intval(date("Y")) == intval($dateAr[2]) && intval(date("m")) == intval($dateAr[1]) && intval(date("d")) == intval($dateAr[0]) );
$faceblpxl = 0;

if(empty($booking->restaurant)) {

    $booking->restaurant = $bkrestaurant;
    $language = $bklangue;


    if (empty($bktracking) && !empty($_COOKIE['weeloy_be_tracking'])){
        $bktracking = $_COOKIE['weeloy_be_tracking'];
    }

    if(isset($_COOKIE['weelredir_' . $bkrestaurant])) {
        $bktracking = (empty($bktracking)) ? "redirect" : $bktracking . "|" . "redirect";
    }

    //if(isset(filter_input(INPUT_COOKIE, 'weelredir30', FILTER_SANITIZE_STRING))) {
    if(isset($_COOKIE['weelredir30'])){
         $campaign = new WY_MarketingCampaign();
        //$campaign_id = 'EDM1'; //need to extarct campaign id
         $token = $_COOKIE['weelredir30_bis'];
         $campaign_id = $campaign->getCampaignId($token, $bkemail);
       
        $bktracking = (empty($bktracking)) ? "marketing" . "$campaign_id" : $bktracking . "|" . "marketing" . "$campaign_id";
    }    

	$bktype = "booking";
	$bkstatus = "";
	$bkstate = "to come";
    $status = $booking->createBooking($bkrestaurant, $bkDBdate, $bktime, $bkemail, $bkmobile, $bkcover, $bksalutation, $bkfirst, $bklast, $bkcountry, $language, $bkspecialrequest, $bktype, $bktracking, $booker, $company, $hotelguest, $bkstate, $optin, $booking_deposit_id, $bkstatus, $bkextra, $bkproduct);

    if ($status < 0) {
        header("Location: " . $action . "?bkrestaurant=" . $bkrestaurant . "&bktitle=" . $bktitle . "&bktracking=" . $bktracking . "&bkproduct=" . $bkproduct . "&data=" . $data . "&signed_request=" . $signed_request . "&bkerror=" . $booking->msg . "&langue=" . $language . '&bksalutation=' . $bksalutation . '&bklast=' . $bklast . '&bkfirst=' . $bkfirst . '&bkemail=' . $bkemail . '&bkcover=' . $bkcover . '&bkdate=' . $bkdate . '&bktime=' . $bktime . '&bkmobile=' . $bkmobile);
        exit;
    }

    $bkconfirmation = $booking->confirmation;
    if ($booking->getBooking($bkconfirmation) == false) {
        header("Location: " . $action . "?bkrestaurant=" . $bkrestaurant . "&bktitle=" . $bktitle . "&bktracking=" . $bktracking . "&bkproduct=" . $bkproduct . "&data=" . $data . "&signed_request=" . $signed_request . "&bkerror=confirmationinvalid" . "&langue=" . $language . '&bklangue=' . $bklangue . '&bksalutation=' . $bksalutation . '&bklast=' . $bklast . '&bkfirst=' . $bkfirst . '&bkemail=' . $bkemail . '&bkcover=' . $bkcover . '&bkdate=' . $bkdate . '&bktime=' . $bktime . '&bkmobile=' . $bkmobile);
        exit;
    }

    $transitory->create('BOOKING-' . $bkparam, $booking->confirmation);

    //qrcode generartion
    $path_tmp = explode(':', get_include_path());
    $qrcode_filename = $path_tmp[count($path_tmp) - 1] . 'tmp/' . $bkconfirmation . '.png';
    $qrcode = new QRcode();
    $qrcode->png('{"membCode":"' . $booking->membCode . '"}', $qrcode_filename);
    //file_put_contents($qrcode_filename,$qrcode->image(8));
    //$booking->notifyBooking($qrcode_filename);
    //$notification->notifyBooking($booking,$qrcode_filename);
    

	if(preg_match("/WEBSITE/", $bktracking) == false)
		$faceblpxl = 1;
		
    $logger->LogEvent($loguserid, 701, 0, $bkconfirmation, '', date("Y-m-d H:i:s"));
    
	$notification->notify($booking, 'booking');

    $logger->LogEvent($loguserid, 701, 0, $bkconfirmation, 'after', date("Y-m-d H:i:s"));
    
    
} else {
    // this is a reload
    $bkconfirmation = $booking->confirmation;
    $bktime = $booking->rtime;
    $bkemail = $booking->email;
    $bkmobile = $booking->mobile;
    $bkcover = $booking->cover;
    $bksalutation = $booking->salutation;
    $bkfirst = $booking->firstname;
    $bklast = $booking->lastname;
    $bkcountry = $booking->country;
    $language = $booking->language;
    $bkspecialrequest = $booking->specialrequest;
}

//kala added this line for fbshare date formate
if (@$bkdate) {
    $fbShareDate = date('Y,M,d', strtotime($bkDBdate));
}

//qrcode delete
//unlink($qrcode_filename);
//error_log("<br><br>BOOKING = " . $status . " " . $booking->msg);

$QRCodeArg = "&bkrestaurant=$bkrestaurant&bkconfirmation=$bkconfirmation&bktracking=$bktracking";

$typeBooking = ($res->is_bookable && ($res->status == 'active' || $res->status == 'demo_reference')) ? "true " : "false ";
$is_listing = false;
if (empty($res->is_wheelable) || !$res->is_wheelable) {
    $is_listing = true;
}

$bkgcustomcolor = "";
if (!empty($res->bkgcustomcolor) && preg_match("/website|facebook/i", $bktracking)) {
	$bkgcustomcolor = $res->bkgcustomcolor;
}

    
$restoSync = ($res->checkTMSSync() != 0 && $todayflg) ? 1 : 0;

?>


<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv='expires' content='mon, 1 jan 1998 05:00:00 gmt'/>
<meta http-equiv='pragma' content='no-cache'/>
<meta http-equiv='pragma' content='cache-control: max-age=0'/>
<meta http-equiv='last-modified' content='mon, 1 jan 1998 05:00:00 gmt'/>
<meta http-equiv='cache-control' content='no-cache, must-revalidate'/>
<meta name='robots' content='noindex, nofollow'/>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="HandheldFriendly" content="true" />
<title>Booking Confirmation - Weeloy</title>
<base href="<?php echo __ROOTDIR__; ?>/"/>
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap-select.css" rel="stylesheet" />
<link href="css/bootstrap-social.css" rel="stylesheet" />
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="modules/booking/assets/css/bookingform.css?v=1" rel="stylesheet" >

<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type='text/javascript' src="js/angular.min.js"></script>
<script type="text/javascript" src="js/ngStorage.min.js"></script>
<?
if($restoSync) {
printf("<script type='text/javascript' src='modules/rltimeio/ortc.js?1'></script>");
printf("<script type='text/javascript' src='modules/rltimeio/lib.js?1'></script>");
}
?>
<style>


<?php //if ($brwsr_type != "mobile") echo ".mainbox { margin: 10px 30px 20px 1px; width:550px; } ";  ?>

<?php
        if (!empty($bkgcustomcolor)) {
		include_once('book_custom.php');
            }
?>

</style>


</head>
<body ng-app="myApp"  style="background: transparent none repeat scroll 0 0;">

<!-- tracking code for GTM -->

<?php
$isTest = false;
if (strpos($bkspecialrequest, 'test') !== false) {
	$isTest = true;
}
if (strpos($bkfirst, 'test') !== false) {
	$isTest = true;
}
if (strpos($bklast, 'test') !== false) {
	$isTest = true;
}
if (strpos($bkrestaurant, 'SG_SG_R_TheFunKitchen') !== false) {
	$isTest = true;
}
if (strpos($bkrestaurant, 'SG_SG_R_TheOneKitchen') !== false) {
	$isTest = true;
}
if (!$isTest) {
	?>
	<script>

				dataLayer = [];
				dataLayer.push({
					'pax': <?php echo $bkcover; ?>,
					'reservation_id': '<?php echo $bkconfirmation; ?>',
					'reserved_by': '<?php
if (isset($_SESSION['user']['email']))
	echo 'user';
else
	echo 'guest';
?>',
					'reserver_name': '<?php echo $bkfirst . ' ' . $bklast; ?>',
					'reserver_loginid': '<?php echo $bkemail; ?>',
					'reservation_date': '<?php echo $bkdate; ?>',
					'reservation_time': '<?php echo $bktime; ?>',
					'restaurant_name': '<?php echo $bktitle; ?>',
					'restaurant_id': '<?php echo $bkrestaurant; ?>',
					'tracking': '<?php echo $bktracking; ?>'
				});
	</script>
<?php } ?>

<?php include_once("ressources/analyticstracking.php") ?>


<div id='booking' ng-controller='MainController' ng-cloak >

	<div class="container mainbox">    
		<div class="panel panel-info" >
			<div class="panel-heading">
				<div class="panel-title" style='width:300px;'> {{ zang['bookingtitlecf'].vl | uppercase}} </div>
			</div>     

			<p class="slogan">{{ zang['confirmsg'].vl}} {{hoteltitle}}<br /><br />
				<strong>{{ zang['confirmsg2'].vl}} {{confirmation}} {{spincode}} </strong>
				<br/>{{ zang['confirmsg3'].vl}}
				<br /><span style="font-size:11px;" ng-if='flgsg'><a href='https://www.weeloy.com' target='_blank'>{{ zang['confirmsg4'].vl}}</a> </span> 
			</p>


			<div class="panel-body" >

				<div class='left-column'>
						<img ng-src='{{imglogo}}' height='120px' id='theLogo' name='theLogo'>
							<div class="book-form-lg">
								<table width='90%'>
									<tr ng-repeat="x in checkmark">
										<td>
											<span class='glyphicon glyphicon-ok checkmark'></span> 
											<br />&nbsp;
										</td>
										<td ng-class='pcheckmark'>
											<span ng-bind="x.label1"></span>
											<span ng-bind="x.label2"></span> 
											<br />&nbsp;
										</td>
								</table>
							</div>
					</div>

				<div class='right-column separation-left no-mobile'>

							<div ng-repeat="x in bookinginfo" class='row' style="margin-left:5px">
								<div class="input-group" ng-if="x.value != '' && x.label != 'divider'">
									<span  ng-if ='x.value'> {{x.label}} : {{x.value}}  </span>
						       </div>
								<h1 ng-if="x.label == 'divider'" ng-bind="x.value | uppercase"></h1>
					</div>
							<div class='col-xs-12' separation-left style="font-size:12px" ng-if="hasDeposit">
								<p>You deposit of {{resCurrency}}{{resBookingDeposit}} has been accepted</p>
								<p>Thank you ...</p>
							</div>

					<table>
						<tr>
									<td><a href='javascript:openWin(1);'><br/>QRCode Agenda</a></td>
									<td width='50'>&nbsp;</td>
									<td><a href='javascript:openWin(0);'><br/>QRCode Contact</a></td>
						</tr>
					</table>
						</div>
				</div>
			</div>
		</div>                     
	</div>  
</div>



<script> var app = angular.module("myApp", ['ngStorage']); </script>
<script type="text/javascript" src="modules/booking/book_translate.js?23"></script>

<script>

<?php
echo "var confirmation = '" . $booking->confirmation . "';";
echo "var tracking = '" . $booking->tracking . "';";
echo "var hoteltitle = '" . $booking->restaurantinfo->title . "';";
echo "var restaurant = '$bkrestaurant';";
echo "var typeBooking = $typeBooking;";
echo "var brwsr_type = '$brwsr_type';";
echo "var imglogo = '$logo';";
echo "var pdate = '$bkdate';";
echo "var ptime = '$bktime';";
echo "var pcover = '$bkcover';";
echo "var bkchild = '$bkchild';";
echo "var pfirst = '$bkfirst';";
echo "var plast = '$bklast';";
echo "var pemail = '$bkemail';";
echo "var pmobile = '$bkmobile';";
echo "var prequest = '$bkspecialrequest';";
echo "var langue = '$bklangue';";
echo "var optin = '$optin';";
echo "var topic = 'BOOKING';";
echo "var partialpath = '" . $getRest['internal_path'] . "';";
echo "var partialdesc = '" . $partialdesc . "';";
echo "var fbId = '" . $fbId . "';";
echo "var mcode = '" . $booking->membCode . "';";
echo "var restoSync = '" . $restoSync . "';";
echo "var token = '" . $token . "';";
echo "var resBookingDeposit;";
echo "var resCurrency;";
if(isset($_POST['resBookingDeposit']) && isset($_POST['resCurrency'])) {
    echo "resBookingDeposit = '" . $_POST['resBookingDeposit'] . "';";
    echo "resCurrency = '" . $_POST['resCurrency'] . "';";
} 

printf("var flgsg = %s;", (!preg_match('/CALLCENTER|WEBSITE|facebook|GRABZ/', $bktracking)) ? 'true' : 'false');
printf("var is_listing = %s;", $is_listing ? 'true' : 'false');
?>



app.controller('MainController', function ($scope, $http, $locale, $timeout, $localStorage, booktranslate) {

		if(resBookingDeposit !== undefined) {
			$scope.resBookingDeposit = resBookingDeposit;
			$scope.resCurrency = resCurrency;
			$scope.hasDeposit = true;
		}

		$scope.restaurant = restaurant;
		restoSync = parseInt(restoSync);
		$scope.langue = langue;
		$scope.pcheckmark = ($scope.langue != 'cn') ? 'pcheckmark' : 'pcheckmarkcn';
		$scope.imglogo = imglogo;
		$scope.brwsr_type = brwsr_type;
		$scope.hoteltitle = hoteltitle;
		$scope.confirmation = confirmation;
		$scope.flgsg = flgsg;
		$scope.spincode = (flgsg && mcode != '' && mcode.length == 4 && mcode != '0000') ? '(spin:' + mcode + ')' : '';

		$scope.bookingtitlecf = (typeBooking) ? "BOOKING CONFIRMED" : "REQUESTED CONFIRMED";
		$scope.confirmsg = (typeBooking) ? "Your booking is confirmed at " : "Your request has been sent to ";

		$scope.confirmsg2 = (typeBooking) ? "We are happy to confirm your reservation" : "Your reservation number is ";
		$scope.confirmsg3 = "You will receive the details of your reservation by email and SMS";
		$scope.confirmsg4 = "You can sign in using your email to view all your bookings and leave your reviews";
		$scope.bacchanalia = "Your table has been blocked, we are sending you an e-mail to reconfirm the booking";
		$scope.listTags0 = "Free Booking";
		$scope.listTags1 = "";
		$scope.listTags2 = (typeBooking) ? "Instant Reservation" : "No reservartion";
		$scope.listTags3 = (typeBooking) ? "" : "First come, first serve";
		$scope.listTags4 = (!is_listing) ? "Win" : "Enjoy incredible";
		$scope.listTags5 = (!is_listing) ? "for every bookings" : " promotion";

		if(typeof prequest === "string" && prequest.length > 0)
			prequest = prequest.replace(/\'|\"/g, "’");

		$scope.pdate = pdate;
		$scope.ptime = ptime;
		$scope.pcover = pcover;
		$scope.pfirst = pfirst;
		$scope.plast = plast;
		$scope.pemail = pemail;
		$scope.pmobile = pmobile;
		$scope.prequest = prequest;
		$scope.pname = pfirst + ' ' + plast;

		$scope.langdata = $localStorage.langdata;
		$scope.zang = null;
		if ($scope.langdata && $scope.langdata.version && $scope.langdata.version.lb) {
			if ($scope.langdata.version.lb === restaurant)
				$scope.zang = $scope.langdata;
		}

		if (!$scope.zang) {
			$scope.zang = booktranslate.getobj($scope);
			}

		booktranslate.checkmark($scope, flgsg);
		$scope.bookinginfo = booktranslate.bookinginfo($scope);

		if(restoSync === 1) {
			pdate = pdate.split("/").reverse().join("-");
			var msg = "reservation;" + confirmation + ";date=" + pdate + "|time=" + ptime + "|cover=" + pcover + "|last=" + plast + "|first=" + pfirst + "|email=" + pemail + "|phone=" + pmobile + "|comment=" + prequest + "|tracking=" + tracking;
			var sync = subrealtimeio($scope.restaurant, token, msg);
			}

		$scope.fbshareclik = function () {

			var fbObj = fbshare();
			FB.ui(fbObj, function (response) {
				console.log(JSON.stringify(response));
			});

		}
		function fbshare() {
			var obj = {
				method: 'feed',
				link: "https://www.weeloy.com/" + partialpath,
				picture: $scope.imglogo,
				name: $scope.hoteltitle,
				caption: 'New booking at ' + $scope.hoteltitle + '. Now enjoy rewards with Weeloy',
				description: partialdesc,
				display: 'popup'
			};
			return obj;

		}
		function sortMethod(a, b) {
			var x = a.name.toLowerCase();
			var y = b.name.toLowerCase();
			return ((x < y) ? -1 : ((x > y) ? 1 : 0));
		}


	});

	String.prototype.capitalize = function () {
		return this.replace(/^./, function (match) {
			return match.toUpperCase();
		});
	};

	function openWin(type) {
		var arg = <?php echo "'$QRCodeArg'"; ?>;
		window.open('modules/qrcode/newQRCode.php?type=' + type + arg, 'QRCode', 'toolbar=no, scrollbars=no, resizable=no, top=30, left=30, width=330, height=330');
	}

	$(document).ready(function () {

		window.fbAsyncInit = function () {
			FB.init({
				appId: fbId, //weeloy localhost fb appId
				status: true,
				cookie: true,
				xfbml: true
			});

		}; //end fbAsyncInit

		// Load the SDK Asynchronously
		(function (d) {
			var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
			if (d.getElementById(id)) {
				return;
			}
			js = d.createElement('script');
			js.id = id;
			js.async = true;
			js.src = "//connect.facebook.net/en_US/all.js";
			ref.parentNode.insertBefore(js, ref);
		}(document)); //end loadSDF


	});

</script>

<?
if($faceblpxl == 1)
printf("<script type='text/javascript' src='modules/booking/facebkpxl.js?23'></script>");
?>

</body>
</html>

