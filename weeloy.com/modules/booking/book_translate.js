app.service('booktranslate', function ($locale) {

	this.getobj = function($scope) {
		var obj = {
			'restaurants':  {"lb": "Select your Restaurant", "vl": "Select your Restaurant", "f": "c"},
			'version': {"lb": $scope.restaurant, "vl": $scope.bkdate, "f": "c"},
			'lunchtag': {"lb": "lunch", "vl": "Lunch", "f": "c"}, 
			"dinnertag": {"lb": "dinner", "vl": "Dinner", "f": "c"}, 
			"teatimetag": {"lb": "Afternoon Tea", "vl": "Afternoon Tea", "f": "c"}, 
			"guesttag": {"lb": "guests", "vl": "Guests", "f": "c"}, 
			"titletag": {"lb": "title", "vl": "Title", "f": "c"}, 
			"firsttag": {"lb": "firstname", "vl": "First name", "f": "c"},
			'datetag': {"lb": "Date", "vl": "Date", "f": "c"}, 'timetag': {"lb": "time", "vl": "time", "f": "c"}, "nametag": {"lb": "name", "vl": "name", "f": "c"},
			'lasttag': {"lb": "lastname", "vl": "Last name", "f": "c"}, "emailtag": {"lb": "email", "vl": "Email", "f": "c"}, "mobiletag": {"lb": "mobile", "vl": "Mobile", "f": "c"}, "requesttag": {"lb": "special request", "vl": "Special request", "f": "c"}, "requestpromotag": {"lb": "special request / Promotion Codes", "vl": "Special Request / Promotion Codes", "f": "c"},
			'today': {"lb": "today", "vl": "Today", "f": "c"}, 'clear': {"lb": "clear", "vl": "clear", "f": "c"}, 'close': {"lb": "close", "vl": "close", "f": "c"},
			'bookingdetail': {"lb": "booking details", "vl": "booking details", "f": "u"},
			'personaldetail': {"lb": "personal details", "vl": "personal details", "f": "u"},
                     
//                        'depositdetail': {"lb": "deposit", "vl": "deposit", "f": "u"},
                       // 'depositbookform1': {"lb": "This restaurant requires a deposit to confirm the booking (per pax):", "vl": "This restaurant requires a deposit to confirm the booking (per pax):", "f": "c"},
			//'depositbookform2': {"lb": "This restaurant requires a deposit to confirm the booking :", "vl": "This restaurant requires a deposit to confirm the booking :", "f": "c"},
                        
//                        need to create translation for this section
			'depositdetail': {"lb": "cancellation policy", "vl": "cancellation policy", "f": "u"},
			'depositbookform1': {"lb": "The restaurant reserve the right to charge a cancellation fee based on this policy.", "vl": "The restaurant retrieve the right to change a cancellation fee based on this policy.", "f": "c"},
			'depositbookform2': {"lb": "The restaurant reserve the right to charge a cancellation fee based on this policy.", "vl": "The restaurant retrieve the right to change a cancellation fee based on this policy.s", "f": "c"},
			'step': {"lb": "Step ", "vl": "Step ", "f": "n"},
			'slogan_sub': {"lb": $scope.slogan_sub, "vl": $scope.slogan_sub, "f": "n"},
			'slogan': {"lb": $scope.slogan, "vl": $scope.slogan, "f": "n"},
			'confinfo': {"lb": $scope.slogan, "vl": $scope.slogan, "f": "c"},
			'bookingtitlecf': {"lb": $scope.bookingtitlecf, "vl": $scope.bookingtitlecf, "f": "u"},
			'facebookfillin': {"lb": "Book with Facebook", "vl": "Book with Facebook", "f": "c"},
			'buttonbook': {"lb": $scope.buttonLabel, "vl": $scope.buttonLabel, "f": "u"},
			'buttonconfirm': {"lb": $scope.buttonConfirm, "vl": $scope.buttonConfirm, "f": "u"},
			'bookingtitle': {"lb": $scope.bookingTitle, "vl": $scope.bookingTitle, "f": "u"},
			'buttonmodify': {"lb": "modify booking", "vl": "modify booking", "f": "n"},
			'timeouttag': {"lb": "time out", "vl": "time out", "f": "u"},
			'listtag1': {"lb": "free booking", "vl": "free booking", "f": "c"},
			'listtag2': {"lb": $scope.listTags2, "vl": $scope.listTags2, "f": "c"},
			'listtag3': {"lb": $scope.listTags3, "vl": $scope.listTags3, "f": "c"},
			'listtag4': {"lb": $scope.listTags4, "vl": $scope.listTags4, "f": "c"},
			'listtag5': {"lb": $scope.listTags5, "vl": $scope.listTags5, "f": "n"},
			'confirmsg': {"lb": $scope.confirmsg, "vl": $scope.confirmsg, "f": "n"},
			'confirmsg2': {"lb": $scope.confirmsg2, "vl": $scope.confirmsg2, "f": "n"},
			'confirmsg3': {"lb": $scope.confirmsg3, "vl": $scope.confirmsg3, "f": "n"},
			'confirmsg4': {"lb": $scope.confirmsg4, "vl": $scope.confirmsg4, "f": "n"},
			'bacchanalia': {"lb": $scope.bacchanalia, "vl": $scope.bacchanalia, "f": "n"},
			'restau_conditions': {"lb": "Conditions", "vl": "Conditions", "f": "u"},
			'mr': {"lb": "Mr.", "vl": "Mr.", "f": "c"},
			'mrs': {"lb": "Mrs.", "vl": "Mrs.", "f": "c"},
			'ms': {"lb": "Ms.", "vl": "Ms.", "f": "c"},
            'miss': {"lb": "Miss.", "vl": "Miss.", "f": "c"},
            'doctor': {"lb": "Dr.", "vl": "dr.", "f": "c"},
            'madam': {"lb": "Mdm.", "vl": "Mdm.", "f": "c"},
			'msg1': {"lb": $scope.msg1, "vl": $scope.msg1, "f": "n"},
			'error0': {"lb": $scope.error0, "vl": $scope.error0, "f": "c"},
			'error1': {"lb": $scope.error1, "vl": $scope.error1, "f": "c"},
			'error2': {"lb": $scope.error2, "vl": $scope.error2, "f": "c"},
			'error3': {"lb": $scope.error3, "vl": $scope.error3, "f": "c"},
			'error4': {"lb": $scope.error4, "vl": $scope.error4, "f": "c"},
			'error5': {"lb": $scope.error5, "vl": $scope.error5, "f": "c"},
			'error6': {"lb": $scope.error6, "vl": $scope.error6, "f": "c"},
			'error7': {"lb": $scope.error7, "vl": $scope.error7, "f": "c"},
			'error8': {"lb": $scope.error8, "vl": $scope.error8, "f": "c"},
			'error9': {"lb": $scope.error9, "vl": $scope.error9, "f": "c"},
			'error10': {"lb": $scope.error10, "vl": $scope.error10, "f": "c"},
			'error11': {"lb": $scope.error11, "vl": $scope.error11, "f": "c"},
			'error12': {"lb": $scope.error12, "vl": $scope.error12, "f": "n"},
			'error13': {"lb": $scope.error13, "vl": $scope.error13, "f": "n"},
			'error14': {"lb": $scope.error14, "vl": $scope.error14, "f": "n"},
			'tmc1': {"lb": $scope.tmc1, "vl": $scope.tmc1, "f": "c"},
			'tmc2': {"lb": $scope.tmc2, "vl": $scope.tmc2, "f": "c"}
			};
		
		for (ele in obj) {
			obj[ele].vl = obj[ele].lb;
			if (obj[ele].f == 'c' && typeof obj[ele].vl === "string")
				obj[ele].vl = obj[ele].vl.capitalize();
			else if (obj[ele].f == 'u' && typeof obj[ele].vl === "string")
				obj[ele].vl = obj[ele].vl.toUpperCase();
			}
		return obj;
		};
		
	this.setlocal = function(lg) {
		angular.copy(locales[lg], $locale);
		};
		
	this.checkmark = function($scope, flgsg) {
		$scope.checkmark = [{"label1": $scope.zang['listtag1'].vl, "label2": " ", "glyph": true}, {"label1": $scope.zang['listtag2'].vl, "label2": $scope.zang['listtag3'].vl, "glyph": true}, {"label1": $scope.zang['listtag4'].vl, "label2": $scope.zang['listtag5'].vl, "glyph": true}];
		if (flgsg == false)
			$scope.checkmark.pop(); // don't show "win everytime

		$scope.pcheckmark = ($scope.langue != 'cn' && $scope.langue != 'th') ? 'pcheckmark' : 'pcheckmarkcn';
		};
		
	this.salutations = function($scope) {
		if ($scope.langue != 'de')
			$scope.salutations = [$scope.zang['mr'].vl, $scope.zang['mrs'].vl, $scope.zang['ms'].vl, $scope.zang['doctor'].vl, $scope.zang['madam'].vl];
		else $scope.salutations = [$scope.zang['mr'].vl, $scope.zang['mrs'].vl, $scope.zang['doctor'].vl, $scope.zang['madam'].vl];
		};
		
	this.bookinginfo = function($scope) {
		return [{"label": "divider", "value": $scope.zang['bookingdetail'].vl}, {"label": 'Section', "value": $scope.pproduct},{"label": $scope.zang['datetag'].vl, "value": $scope.pdate}, {"label": $scope.zang['timetag'].vl, "value": $scope.ptime}, {"label": $scope.zang['guesttag'].vl, "value": $scope.pcover}, {"label": "divider", "value": $scope.zang['personaldetail'].vl}, {"label": $scope.zang['nametag'].vl, "value": $scope.pname}, {"label": $scope.zang['emailtag'].vl, "value": $scope.pemail}, {"label": $scope.zang['mobiletag'].vl, "value": $scope.pmobile}, {"label": $scope.zang['requesttag'].vl, "value": $scope.prequest}];
		};
});
