<?php

$source_form = 'ext_iframe';
include_once 'book_form.inc.php';

$mutipleProdAllote = ($res->extraflag & MUTIPLEPRODUCTALLOTE) ? 1 : 0;

if ($mutipleProdAllote === 0) {
    $action = __BASE_URL__ . '/modules/booking/book_form.php?bkrestaurant=' . $res->restaurant;
    echo "<script> window.top.location.href ='$action';</script>";
}
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="author" content="weeloy. https://www.weeloy.com"/>
        <meta name="copyright" content="weeloy. https://www.weeloy.com"/>  
        <meta http-equiv='expires' content='mon, 1 jan 1998 05:00:00 gmt'/>
        <meta http-equiv='pragma' content='no-cache'/>
        <meta http-equiv='pragma' content='cache-control: max-age=0'/>
        <meta http-equiv='last-modified' content='mon, 1 jan 1998 05:00:00 gmt'/>
        <meta http-equiv='cache-control' content='no-cache, must-revalidate'/>
        <meta name='robots' content='noindex, nofollow'/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <meta name="HandheldFriendly" content="true" />
        <title>Book your table now - Weeloy Code - Weeloy.com</title>

        <base href="<?php echo __ROOTDIR__; ?>/" />

        <link href="client/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="client/bower_components/bootstrap/dist/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
        <link href="client/bower_components/fontawesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="modules/booking/assets/css/section_booking.css"/>
        <link href="modules/booking/assets/css/css/bootstrap-select.css" rel="stylesheet" />
        <link href="modules/booking/assets/css/css/bootstrap-social.css" rel="stylesheet" />
        <link href="modules/booking/assets/css/css/famfamfam-flags.css" rel="stylesheet" />
        <link href="modules/booking/assets/css/css/dropdown.css" rel="stylesheet" />
        <link href="modules/booking/assets/css/bookingform.css?v=1" rel="stylesheet" />

        <script type='text/javascript' src="client/bower_components/angular/angular.min.js"></script>
        <script type="text/javascript" src="client/bower_components/jquery/dist/jquery.min.js"></script>
<!--        <script type='text/javascript' src="client/bower_components/angulartics/src/angulartics.js"></script>
        <script type='text/javascript' src="client/bower_components/angulartics/src/angulartics-gtm.js"></script>-->
        <script type='text/javascript' src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.3/angular-animate.js"></script>
        <script type="text/javascript" src="js/jquery-ui.js"></script>
        <script type="text/javascript" src="client/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="client/bower_components/ui-bootstrap-tpls-0.14.2.min.js"></script>
        <script type='text/javascript' src="client/bower_components/re-tree/re-tree.js"></script>
        <script type='text/javascript' src="client/bower_components/ng-device-detector/ng-device-detector.js"></script>
        <script type="text/javascript" src="js/bootstrap-select.js"></script>
        <script type="text/javascript" src="js/dayroutine.js"></script>
        <script type="text/javascript" src="js/ngStorage.min.js"></script>
        <script type="text/javascript" src="js/mylocal.js"></script>

        <link rel="stylesheet" href="modules/booking/assets/css/section_booking.css"/>


        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.6/js/materialize.min.js"></script>

        <script type="text/javascript" src="client/bower_components/moment/min/moment.min.js"></script>
        <script type="text/javascript" src="client/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
        <script type="text/javascript" src="client/bower_components/lodash/dist/lodash.min.js"></script>
        <link rel="stylesheet" href="client/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css"/>

        <style>

	    .redinput { color: red; font-size:12px;}
	    .blackinput { color: black; font-size:12px; }
	    
            .animate-show.ng-hide-add {
                transition: all linear 0.1s;
            }

            .animate-show.ng-hide-remove {
                transition: all linear 0.01s;
            }

            .animate-show.ng-hide {
                line-height: 0;
                opacity: 0;
                padding: 0 10px;
            }
<?php
//if ($_REQUEST['krht'] == "83") echo ".container  { margin: 0 0 0 0; }" 
// booking customization
if (!empty($bktracking) && preg_match("/GRABZ/i", $bktracking)) {
    $bkgcustomcolor = '#000000';
}
if (!empty($bkgcustomcolor)) {
    echo <<<DELIMITER
	h1 { background-color: $bkgcustomcolor; }
	.panel { border-color: $bkgcustomcolor !important; }
	.panel-heading { background-color: $bkgcustomcolor !important; border-color: $bkgcustomcolor !important; }
	.caret { color: $bkgcustomcolor; }
        @media (min-width: 530px) {
            .separation { border-right: 4px solid $bkgcustomcolor; height: 100%; }
            .separation-left { border-left: 4px solid $bkgcustomcolor; }
        }
         
DELIMITER;
}
?>

        </style>
        <?php include_once("ressources/analyticstracking.php") ?>
    </head>

    <body ng-app="app" ng-controller="SectionBookingCtrl" class="ng-cloak "  style="background: transparent;" >

        <input id='isDeposit' type="hidden" name="hasdeposit" value="<?php echo $isDeposit ?>" />
        <input id="restaurant" type="hidden"  value="<?php echo $bkrestaurant ?>" />
        <div id='bookingCntler'>
            <div class="container mainbox" style="padding-left:7px;padding-right:7px;">    
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="panel-title" > 
                            <div id="parent">
                                <div class="left">
                                    <span ng-bind="zang['bookingtitle'].vl | uppercase"></span>
<!--                                    <span>Book at Burnt Ends</span> -->
                                    <span ng-if='timeout'>
                                        (
                                        <span ng-bind="zang['timeouttag'].vl"></span>
                                        )
                                    </span>
                                </div>  
                            </div>
                        </div>
                    </div>  
                    <div class="panel-body" >
                        <form id="booking_form" method="POST" action="modules/booking/prebook.php">
                            <div class="section_selection row">
                                <div class="section-item col-sm-12 animate-show"  ng-repeat="item in sections"  style='margin-bottom:15px;' ng-show="visible_section === 1">
                                    <div class="col-sm-12">
                                        <div class="section-title ">
                                            <input id="section-input-{{ $index}}" type="radio" name="input-section"  ng-checked="$index === sectionID - 1" ng-value="item.id" ng-click="changeSectionRadio(item)"/>
                                            <label for="section-input-{{ $index}}" style="margin-left: 20px;">{{item.name}}</label>
                                        </div>
                                        <div class="section-desc "  style="font-size: 12px;">
                                            <span>{{item.description}}</span>
                                        </div>
                                        <div ng-show="item.id === sectionID && sectionID === 3" class="section-desc col-sm-12">
                                            <input type="checkbox" ng-checked='rconfbarseat' ng-click="changeRequiredButton(item)"/>
                                            <span ng-class = "{ 'redinput' : !rconfbarseat, 'blackinput': rconfbarseat}" >{{item.require_confirmation_text}}</span>
                                        </div>
                                        <div ng-show="item.id === sectionID && item.reminder !== '' && pax > 5" class="section-desc col-sm-12">
                                            <span style=" font-size: 12px;font-style: italic;" style="color:black"><br/>{{ item.reminder }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="section_selection row" ng-show="visible_section === 1">
                                <img style="max-height:100%;max-width:100%;" ng-src='https://website-external.s3.amazonaws.com/media/burnt_ends/{{section.image}}' />
                            </div>

                            <div class="overwrite-allotment">

                            </div>
                            <div ng-if="bktracking.indexOf('callcenter|private') > -1" ng-show="visible_section === 1"  class="col-sm-12" id='overwrite-allotment' ng-click="overwriteAvb(overwritecheckbox)">
                                <input ng-model="overwritecheckbox" type="checkbox" />
                                <span>Force availabilities</span>
                            </div>

                            <div   ng-show="!isMobile && visible_section === 1" class="row animate-show">
                                <div class='col-xs-6 col-sm-6'>
                                    <div class='input-group '>
                                        <div class='input-group-btn' dropdown is-open="coveropened">
                                            <button class='btn book-dropdown' data-toggle='dropdown' ng-click="opendropdown($event, 'cover')" >
                                                <i class="glyphicon glyphicon-user"></i>
                                                <span class='caret'></span>
                                            </button>
                                            <ul class='dropdown-menu scrollable-menu'>
                                                <li ng-repeat="p in paxs" ng-click="changePax(p)" >{{p}}</li>
                                            </ul>
                                        </div>
                                        <input type='text' name="bkcover" id="bkcover" ng-model='pax' class='form-control input' ng-click="opendropdown($event, 'cover')" readonly />
                                    </div>
                                </div>

                                <div class='col-xs-6 col-sm-6'>
                                    <div class='input-group'>
                                        <div class='input-group-btn' dropdown is-open="typeopened">
                                            <button class='btn book-dropdown animate-show' data-toggle='dropdown' ng-click="opendropdown($event, 'type')">
                                                <i class="glyphicon glyphicon-cutlery"></i>
                                                <span class='caret'></span>
                                            </button>
                                            <ul class='dropdown-menu scrollable-menu'>
                                                <li ng-repeat="t in types" ng-click="changeType(t)">{{t}}</li>
                                            </ul>
                                        </div>
                                        <input type='text' ng-model='mealtype' class='form-control input' ng-click="opendropdown($event, 'type')" readonly />
                                    </div>
                                </div>
                            </div>
                            
                            <div   ng-show="isMobile" class="row animate-show" ng-show="visible_section === 1">
                                <div class='col-xs-6 col-sm-6' style="text-align: center">
                                    <i class="glyphicon glyphicon-user" style="display:inline;padding:0px 7px;"></i>                                        
                                    <select class='form-control' ng-model="pax"  ng-options="pax for pax in paxs"  ng-change="changePax(pax)"  style='display:inline;width: 70%; '>
                                        <option value="" ng-if="false"></option>
                                    </select>
                                </div>

                                <div ng-if="isMobile" class='col-xs-6 col-sm-6' style="text-align: center">
                                    <i class="glyphicon glyphicon-cutlery" style="display:inline;padding:0px 7px;"></i>   
                                    <select class='form-control' ng-model="mealtype" ng-options="type for type in types"  ng-change="changeType(type)" style='display:inline;width: 70%; '>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="book-order row animate-show" ng-show="visible_section === 1">
                                
                                <div ng-if="sectionID === 1 || sectionID === 2 || sectionID === 4 && pax > 5" class="section-desc  col-xs-12 col-sm-12">
                                    <p style=" font-size: 12px;font-style: italic;color:red;" style="color:black">{{ footer }}</p>
                                </div>
                                
                                <div class="date-picker" style="width:100%; margin-top:10px;margin-bottom:10px;" >
                                    <div class="form-group">
                                        <div date-picker id='datepicker' dropdown_label="dropdownLabel" enabled-dates="enabledDates" selected-date="selectedDate" class="input-group date datetime-picker"  style="width:80%; margin: auto"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="row animate-show" ng-show="visible_section === 1">

                                <div  ng-show="!isMobile" class='col-xs-6 col-sm-6'>
                                    <div class='input-group dropup'>
                                        <div class='input-group-btn' dropdown is-open="timeopened" >
                                            <button class='btn book-dropdown' data-toggle='dropdown'  ng-click="opendropdown($event, 'time')" >
                                                <i class="glyphicon glyphicon-time"></i>
                                                <span class='caret'></span>
                                            </button>
                                            <ul class='dropdown-menu scrollable-menu'>
                                                <li ng-repeat="t in times" ng-click="changeTime(t)">{{t}}</li>
                                            </ul>
                                        </div>
                                        <input type='text' name="bktime" id="bktime" ng-model='time' class='form-control input' ng-click="opendropdown($event, 'time')"  readonly />
                                    </div>
                                </div>
                                
                                <div ng-if="isMobile" class='col-xs-6 col-sm-6' style="text-align: center">
                                    <i class="glyphicon glyphicon-time" style="display:inline;padding:0px 7px;"></i>   
                                    <select class='form-control' ng-model="time" ng-options="time for time in times"  ng-change="changeTime(time)" style='display:inline;width: 70%; '>
                                        <option value="" ng-if="false"></option>
                                    </select>
                                </div>
                                
                                
                                <div class="col-xs-6 col-sm-6" ng-show="visible_section === 1">
                                    <label id='error-message' name='error-message' style=" display:none;color:red; margin-bottom: 25px;">Required: date, time, pax, email, first name, last name, mobile </label>    
                                    <div class="book-button right-align" style="width:100%;padding:0px;text-align:right;">
                                        <a type="button" ng-class="validation_continue  ? 'book-button-sm btn-leftBottom-orange':'book-button-sm btn-leftBottom-deactivated'"  ng-click="continueForm()">Continue</a>
                                    </div>


                                </div>
                            </div>


                            <!--                            <div class="row animate-show" ng-show="visible_section === 1">
                                                            <div class="book-button right-align" style="width:100%;text-align:right;margin-top: 20px;">
                                                                <a type="button" ng-class="validation_continue  ? 'book-button-sm btn-leftBottom-orange':'book-button-sm btn-leftBottom-deactivated'"  ng-click="continueForm()">Continue</a>
                                                            </div>
                                                        </div>-->

                            <div class="row animate-show" ng-show="visible_section === 2" style="background:#eee; margin-bottom:10px; border-radius: 2px; box-shadow: 2px 2px 3px #ccc;">
                                <div class="section-item "  >
                                    <div class="section-title col-sm-12" style=" margin-top: 20px;">
                                        <label>{{section.name}}</label>
                                    </div>
                                    <div class="col-sm-12"  style="font-size: 12px; margin-bottom: 10px;">
                                        <span>{{section.description}}</span>
                                    </div>

                                </div>

                                <div class='col-xs-3 col-sm-3'>
                                    <div class='input-group' style="text-align: center;width: 100%;">
                                        <label name="bkdate" ><i class="glyphicon glyphicon-calendar" style="margin-right:5px"></i>{{booking_date2}}</label>
                                    </div>  
                                </div>
                                <div class='col-xs-3 col-sm-3'>
                                    <div class='input-group' style="text-align: center;width: 100%;">
                                        <label><i class="glyphicon glyphicon-time" style="margin-right:5px"></i>{{time}}</label>
                                    </div>  
                                </div>
                                <div class='col-xs-3 col-sm-3'>
                                    <div class='input-group' style="text-align: center;width: 100%;">
                                        <label><i class="glyphicon glyphicon-user" style="margin-right:5px"></i>{{pax}}</label>
                                    </div>
                                </div>
                                <div class="col-xs-3 col-sm-3 animate-show"  ng-show="visible_section === 2">
                                    <a style="float: right; margin: 0 20px 10px 0;" type="button" ng-click="go_back()">modify</a>
                                </div>
                            </div>

                            <div class='input-group animate-show'  ng-show="visible_section === 2">

                                <div class='input-group-btn ' dropdown is-open="titleopened">
                                    <button type='button' id='itemdfsalut' class='btn book-dropdown' data-toggle='dropdown'>
                                        <i class="glyphicon glyphicon-tag"></i><span class='caret'></span></button>
                                    <ul class='dropdown-menu'>
                                        <li ng-repeat="salutation in salutations" ng-click="changeSalutation(salutation);">{{ salutation.salutation}}</li>
                                    </ul>
                                </div>
                                <input type='text' ng-model='salutation.salutation' class='form-control input' id='bksalutation' name='bksalutation' ng-click="opendropdown($event, 'title')" readonly />
                            </div>

                            <div class='input-group animate-show'  ng-show="visible_section === 2">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                <input type="text" ng-model='bkfirst' class="form-control input" id='bkfirst' name='bkfirst' placeholder="First name" ng-blur="force_on_auto_fill()"  ng-change='bkfirst = cleantext(bkfirst);'>                                        
                            </div>

                            <div class='input-group animate-show'  ng-show="visible_section === 2">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                <input type="text" ng-model='bklast' class="form-control input" id='bklast' name='bklast' placeholder="Last name" ng-blur="force_on_auto_fill()" ng-change='bklast = cleantext(bklast);'>
                            </div>

                            <div class='input-group animate-show'  ng-show="visible_section === 2">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                                <input type="text" ng-model='bkemail' class="form-control input" id='bkemail' name='bkemail' placeholder="Email" ng-blur="force_on_auto_fill()" ng-change='bkemail = cleanemail(bkemail);' />
                            </div>

                            <label id='error-bkemail-empty' name='error-bkemail-empty' style="display:none; color:red; margin-bottom: 25px;">Empty email</label>

                            <div class='input-group animate-show'  ng-show="visible_section === 2">
                                <div class='input-group-btn label-form'>
                                    <button type='button' id='itemdfcountry' class='btn book-dropdown' data-toggle='dropdown'><i class="glyphicon glyphicon-earphone"></i><span class='caret'></span></button>
                                    <ul class='dropdown-menu scrollable-menu' style='font-size:12px;'>
                                        <li ng-repeat="country in countries"><a ng-if="country.b != '';" ng-click="setMobilePhone(country.a);" ><i class='famfamfam-flag-{{country.b}}'></i> {{country.a}}</a><hr ng-if="country.b == '';" /></li>
                                    </ul>
                                </div>
                                <input type="text" ng-model='bkmobile' class="form-control input" id='bkmobile' name='bkmobile' placeholder="Mobile" ng-change='checkvalidtel();'/>
                                <input type="hidden" ng-model='bkcountry' id='bkcountry' name='bkcountry'/>
                            </div>

                            <div class='input-group animate-show'  ng-show="visible_section === 2">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-info-sign"></i></span>
                                <textarea type="text" ng-model='bkspecialrequest' class="form-control input" id='bkspecialrequest' name='bkspecialrequest' rows="2"  placeholder="Special request" ng-blur="force_on_auto_fill()" ng-change='bkspecialrequest = cleantext(bkspecialrequest);'></textarea>
                            </div>

                            <div class="row animate-show"  ng-show="visible_section === 2">
                                <div class="book-button right-align" style="width:100%;text-align:right;margin-top: 20px;">
                                    <a type="button" ng-class="validation_booknow  ? 'book-button-sm btn-leftBottom-orange':'book-button-sm btn-leftBottom-deactivated'" ng-click="bookNow()">Book Now</a>
                                </div>
                            </div>


                            <?php //if($isDeposit ===1) { ?> 


                            <div ng-if="hasDeposit == 1">

                                <input type="hidden" name="resBookingDeposit" ng-value="deposit_amount" />
                                <input type="hidden" name="resCurrency" value="SGD" />
                            </div>


                            <input type="hidden" name="bkproduct" ng-value="section.name" />
                            <input type="hidden" name="bkproductid" ng-value="sectionID" />
                            <input type="hidden" name="mealtype" ng-value="mealtype" />
                            <input type="hidden" name="bkpage1" value="booking_form_section" />
                            <input type="hidden" name="bkdate" ng-value="booking_date2" />
                            <input type="hidden" name="bkrestaurant" ng-value="restaurant"/>
                            <input type="hidden" name="bktracking" ng-value="bktracking"/>

                            <input type="hidden" name="bkparam" value="<?php echo $bkparam ?>"/>


                        </form>


                    </div>  
                </div>
            </div>
        </div>
        <script text ="javascript" src="modules/booking/section_booking/app.js?11"></script>
    </body>
</html>