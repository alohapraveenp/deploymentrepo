<?php
//include 'angular-client/includes/header.php';
require_once("lib/class.booking.inc.php");
require_once "lib/class.notification.inc.php";
require_once("lib/class.restaurant.inc.php");
require_once("lib/class.qrcode.inc.php");
require_once("lib/class.analytics.inc.php");
require_once("lib/class.media.inc.php");
require_once("lib/class.coding.inc.php");
require_once('lib/class.paypal.inc.php');
require_once('lib/class.payment.inc.php');
require_once("conf/conf.session.inc.php");

$booking = new WY_Booking();
$payment = new WY_Payment();
$logger = new WY_log("website");
$notification = new WY_Notification();
$res = new WY_restaurant;

$params = $_REQUEST;
$fbId = WEBSITE_FB_APP_ID;
$paymentShow = false;
$showCancel = false;
$showpending = false;
$showconfirmation = false;
$is_reddot = 2;
$loguserid = (isset($_SESSION['user']['user_id'])) ? $_SESSION['user']['user_id'] : "unknown";
$baseUrl = __BASE_URL__;
$iswhitelabel = false;



if(isset($params['order_number']) && !empty($params['order_number'])){
    
    $params['bkconfirmation'] =  $params['order_number'] ;
    $is_reddot = 3;
    if($params['result'] == 'Paid'){
        $payment_id = $params['transaction_id'];
        $payment->saveReddotPaymentDetails($params['bkconfirmation'],$payment_id,$params['result']);
    }else{
         $display_status = 'PENDING';  
         $payment->saveReddotPaymentStatus($params['bkconfirmation'],$params['result']);
    }
}


//$params['bkconfirmation'] = 'TheOnUCBGDKD';
$display_status = 'PENDING';

if($params['bkconfirmation']){
    $booking->getBooking($params['bkconfirmation']);
}


if($booking->restaurant){
    $bkrestaurant = $booking->restaurant;
    $getRest = $res->getRestaurant( $booking->restaurant);
}
if($booking->deposit_id){
    $bk_deposit = $payment->geDepositById($booking->deposit_id);
    
    if($bk_deposit['paykey'] && $booking->status == ''){
        $display_status = 'CONFIRMED';
    }
    
}


$bkcountry = $booking->country;
$language = $booking->language;
$bkspecialrequest = $booking->specialrequest;
$bkfirst = $booking->firstname;
$bklast = $booking->lastname;
$isCreditCardInfoActive ='';
 $bookingid = (isset($booking->bookid)) ? $booking->bookid : $booking->confirmation;
 $logger->LogEvent($loguserid, 701, $booking->confirmation, '', '', date("Y-m-d H:i:s"));

$QRCodeArg = "&bkrestaurant=$bkrestaurant&bkconfirmation=$booking->confirmation&bktracking=$booking->tracking";

$paymentShow = true;
$typeBooking = ($res->is_bookable && ($res->status == 'active' || $res->status == 'demo_reference')) ? "true " : "false ";
$partialdesc = mb_strimwidth($getRest['description'], 0, 200, "...");
if ($partialdesc !== "")
    $partialdesc = preg_replace("/\r\n/", "", $partialdesc);


$resBookingDeposit = '';
$showconfirmation = true;
$bktracking = $booking->tracking;
$mediadata = new WY_Media($booking->restaurant);
$logo = $mediadata->getLogo($booking->restaurant);

$isCreditCardInfoActive = ($res->checkCreditCardDetails() > 0) ? 1 : 0;
if(!empty($res->currency)){
  $resCurrency->$res->currency;  
}else{$resCurrency = 'SGD';}

$typeBooking = ($res->is_bookable && ($res->status == 'active' || $res->status == 'demo_reference')) ? "true " : "false ";
$is_listing = false;
if (empty($res->is_wheelable) || !$res->is_wheelable) {
    $is_listing = true;
}

$bkgcustomcolor = "";
if (!empty($res->bkgcustomcolor) && preg_match("/website|facebook/i", $bktracking)) {
    if (strlen($res->bkgcustomcolor) > 3 && preg_match("/\|/", $res->bkgcustomcolor)) {
        $tt = explode("|", $res->bkgcustomcolor);
        if (strlen($tt[0]) > 2) {
            $bkgcustomcolor = $tt[0];
        }
    }
}


?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv='expires' content='mon, 1 jan 1998 05:00:00 gmt'/>
        <meta http-equiv='pragma' content='no-cache'/>
        <meta http-equiv='pragma' content='cache-control: max-age=0'/>
        <meta http-equiv='last-modified' content='mon, 1 jan 1998 05:00:00 gmt'/>
        <meta http-equiv='cache-control' content='no-cache, must-revalidate'/>
        <meta name='robots' content='noindex, nofollow'/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <meta name="HandheldFriendly" content="true" />
        <title>Booking Confirmation - Weeloy</title>
        <base href="<?php echo __ROOTDIR__; ?>/"/>
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
        <link href="css/bootstrap-select.css" rel="stylesheet" />
        <link href="css/bootstrap-social.css" rel="stylesheet" />
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="modules/booking/assets/css/bookingform.css?v=1" rel="stylesheet" >



            <script type="text/javascript" src="js/jquery.min.js"></script>
            <script type="text/javascript" src="js/bootstrap.min.js"></script>
            <script type='text/javascript' src="js/angular.min.js"></script>
            <script type="text/javascript" src="js/ngStorage.min.js"></script>
            <style>


<?php //if ($brwsr_type != "mobile") echo ".mainbox { margin: 10px 30px 20px 1px; width:550px; } ";   ?>

<?php

if(!empty($bktracking) && preg_match("/GRABZ/i", $bktracking)){$bkgcustomcolor = '#000000';}
// booking customization
if (!empty($bkgcustomcolor)) {

    echo <<<DELIMITER
h1 { background-color: $bkgcustomcolor; }
.panel { border-color: $bkgcustomcolor !important; }
.panel-heading { background-color: $bkgcustomcolor !important; border-color: $bkgcustomcolor !important; }
.caret { color: $bkgcustomcolor; }
@media (min-width: 530px) {
	.separation { border-right: 4px solid $bkgcustomcolor; height: 100%; }
	.separation-left { border-left: 4px solid $bkgcustomcolor; }
}
DELIMITER;
}
?>

 </style>
 <!-- Facebook Pixel Code -->
        <script>
        !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
        n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
        document,'script','https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '1748228675404519'); // Insert your pixel ID here.
        fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none"
        src="https://www.facebook.com/tr?id=1748228675404519&ev=PageView&noscript=1"
        /></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->

    </head>
    <body ng-app="myApp"  style="background: transparent none repeat scroll 0 0;" >


        <!-- tracking code for GTM -->

                <?php
                $isTest = false;
                if (strpos($bkspecialrequest, 'test') !== false) {
                    $isTest = true;
                }
                if (strpos($bkfirst, 'test') !== false) {
                    $isTest = true;
                }
                if (strpos($bklast, 'test') !== false) {
                    $isTest = true;
                }
                if (strpos($bkrestaurant, 'SG_SG_R_TheFunKitchen') !== false) {
                    $isTest = true;
                }
                if (strpos($bkrestaurant, 'SG_SG_R_TheOneKitchen') !== false) {
                    $isTest = true;
                }
                if (!$isTest && $showCancel === false && $showpending === false) {
                    ?>
            <script>

                        dataLayer = [];

                        dataLayer.push({
                            'pax': <?php echo $booking->cover; ?>,
                            'reservation_id': '<?php echo $booking->confirmation; ?>',
                            'reserved_by': '<?php
            if (isset($_SESSION['user']['email']))
                echo 'user';
            else
                echo 'guest';
            ?>',
                            'reserver_name': '<?php echo $booking->firstname . ' ' . $booking->last; ?>',
                            'reserver_loginid': '<?php echo $booking->email; ?>',
                            'reservation_date': '<?php echo $booking->rdate; ?>',
                            'reservation_time': '<?php echo $booking->rtime; ?>',
                            'restaurant_name': '<?php echo $booking->restaurantinfo->title; ?>',
                            'restaurant_id': '<?php echo $booking->restaurant; ?>',
                            'tracking': 'WEBSITE'
                        });
            </script>
<?php } ?>

<?php include_once("ressources/analyticstracking.php") ?>
<?php include_once("_bkdepositform.php") ?>


        <script>

<?php
echo "var confirmation = '" . $booking->confirmation . "';";
echo "var tracking = '" . $booking->tracking . "';";
echo "var hoteltitle = '" . $booking->restaurantinfo->title . "';";
echo "var restaurant = '$booking->restaurant';";
echo "var typeBooking = $typeBooking;";
echo "var brwsr_type = '$brwsr_type';";

echo "var imglogo = '$logo';";
echo "var pproduct = '$booking->product';";
echo "var pdate = '$booking->rdate';";
echo "var ptime = '$booking->rtime';";
echo "var pcover = '$booking->cover';";
echo "var pfirst = '$booking->firstname';";
echo "var plast = '$booking->lastname';";
echo "var pemail = '$booking->email';";
echo "var pmobile = '$booking->mobile';";
echo "var prequest = '$booking->specialrequest';";
echo "var langue = '$booking->language';";
echo "var topic = 'BOOKING';";
echo "var partialpath = '" . $getRest['internal_path'] . "';";
echo "var partialdesc = '" . $partialdesc . "';";
echo "var fbId = '" . $fbId . "';";
echo "var mcode = '" . $booking->membCode . "';";
echo "var resBookingDeposit ='" . $resBookingDeposit . "';";
echo "var resCurrency ='" . $resCurrency . "';";
echo "var isCancel = '" . $showCancel . "';";
echo "var transaction_id = '$booking->deposit_id';";
echo "var iswhitelabel = '" . $iswhitelabel . "';";
echo "var isCreditCardInfoActive = '$isCreditCardInfoActive';";
echo "var showconfirmation = '" . $showconfirmation . "';";
echo "var showpending = '" . $showpending . "';";
echo "var payment_status = '$display_status';";




printf("var flgsg = %s;", (!preg_match('/CALLCENTER|WEBSITE|facebook|GRABZ/', $bktracking)) ? 'true' : 'false');
printf("var is_listing = %s;", $is_listing ? 'true' : 'false');
?>

                    var app = angular.module("myApp", ['ngStorage']);

                    app.controller('MainController', function ($scope, $http, $locale, $timeout, $localStorage) {
                        $scope.showCancel = false;
                        //angular.copy(locales['en'], $locale);
                        if (resBookingDeposit !== undefined) {
                            $scope.resBookingDeposit = resBookingDeposit;
                            $scope.resCurrency = resCurrency;
                            $scope.hasDeposit = true;
                        }
                        if (isCancel === '1') {
                            $scope.showCancel = true;
                        }
                        if (showconfirmation === '1') {
                            $scope.showconfirmation = true;
                        }
                        if (showpending === '1') {
                            $scope.showpending = true;
                        }

                        $scope.transactionid = transaction_id;

                        $scope.restaurant = restaurant;
                        $scope.langue = langue;
                        $scope.pcheckmark = ($scope.langue != 'cn') ? 'pcheckmark' : 'pcheckmarkcn';
                        $scope.imglogo = imglogo;
                        $scope.brwsr_type = brwsr_type;
                        $scope.hoteltitle = hoteltitle;
                        $scope.confirmation = confirmation;
                        $scope.flgsg = flgsg;
                        $scope.spincode = (flgsg && mcode != '' && mcode.length == 4 && mcode != '0000') ? '(spin:' + mcode + ')' : '';

                        $scope.bookingtitlecf = (typeBooking) ? "BOOKING CONFIRMED" : "REQUESTED CONFIRMED";
                        $scope.confirmsg = (typeBooking) ? "Your booking is confirmed at " : "Your request has been sent to ";
                        $scope.confirmsg5 = (typeBooking) ? "Your booking is pending at " : "Your request has been sent to ";

                        $scope.confirmsg2 = (typeBooking) ? "We are happy to confirm your reservation" : "Your reservation number is ";
                        $scope.confirmsg3 = "You will receive the details of your reservation by email and SMS";
                        $scope.confirmsg4 = "You can sign in using your email to view all your bookings and leave your reviews";
                        $scope.bacchanalia = "Your table has been blocked, we are sending you an e-mail to reconfirm the booking";
                        $scope.listTags0 = "Free Booking";
                        $scope.listTags1 = "";
                        $scope.listTags2 = (typeBooking) ? "Instant Reservation" : "No reservartion";
                        $scope.listTags3 = (typeBooking) ? "" : "First come, first serve";
                        $scope.listTags4 = (!is_listing) ? "Win" : "Enjoy incredible";
                        $scope.listTags5 = (!is_listing) ? "for every bookings" : " promotion";

                        $scope.pproduct = pproduct;
                        $scope.pdate = pdate;
                        $scope.ptime = ptime;
                        $scope.pcover = pcover;
                        $scope.pfirst = pfirst;
                        $scope.plast = plast;
                        $scope.pemail = pemail;
                        $scope.pmobile = pmobile;
                        $scope.prequest = prequest;
                        $scope.pname = pfirst + ' ' + plast;
                        $scope.iswhitelabel = (iswhitelabel === '1') ? true : false;
                        $scope.isCreditCardInfoActive = isCreditCardInfoActive;
                        $scope.payment_status = payment_status;



                        $scope.cancelpolicy = function () {
                            var payment_method = ($scope.isCreditCardInfoActive === 1) ? 'carddetails' : "credit card";


                            return $http.post("api/services.php/cancelpolicy/list",
                                    {
                                        'restaurant': $scope.restaurant,
                                        'type': payment_method,
                                        'amount': $scope.resBookingDeposit,
                                    }).then(function (response) {
                                var data = response.data.data;

                                if (typeof data !== 'undefined') {
                                    $scope.tnc = data.message;
                                    $scope.cancelpolicy = data.range;
                                    $scope.freerange = data.lastRange;
                                }
                            });

                        };

                        $scope.cancelpolicy();
                        //change the date formate
                        var date = new Date($scope.pdate);
                        var dobArr = date.toDateString().split(' ');
                        var rdate = dobArr[2] + ' ' + dobArr[1] + ' ' + dobArr[3];
                        $scope.pdate = rdate;
                        //                var rdate = $scope.pdate.split('-');
                        //                $scope.pdate = rdate[0]+"/"+rdate[1]+"/"+rdate[2];
                        //rdate[0]), parseInt(rdate[1] - 1), parseInt(rdate[2]), rtime[0], rtime[1]

                        $scope.langdata = $localStorage.langdata;
                        if (typeof $scope.langdata !== "undefined" && $scope.langdata.version !== "undefined" && $scope.langdata.version.lb !== "undefined") {
                            if ($scope.langdata.version.lb == restaurant)
                                $scope.zang = $scope.langdata;
                        }


// HACK MULTI LANGUE  "|| true"
                        if (typeof $scope.zang === "undefined" || true)
                            $scope.zang = {
                                'datetag': {lb: "Date", vl: "Date", f: "c"}, 'timetag': {lb: "time", vl: "time", f: "c"}, 'lunchtag': {lb: "lunch", vl: "Lunch", f: "c"}, "dinnertag": {"lb": "dinner", "vl": "Dinner", f: "c"}, "guesttag": {"lb": "number of guests", "vl": "number of guests", f: "c"}, "titletag": {"lb": "title", "vl": "Title", f: "c"}, "firsttag": {"lb": "firstname", "vl": "First name", f: "c"}, "lasttag": {"lb": "lastname", "vl": "Last name", f: "c"}, "nametag": {"lb": "name", "vl": "Name", f: "c"}, "emailtag": {"lb": "email", "vl": "Email", f: "c"}, "mobiletag": {"lb": "mobile", "vl": "Mobile", f: "c"}, "requesttag": {"lb": "special request", "vl": "Special request", f: "c"},
                                'bookingdetail': {lb: "booking details", vl: "booking details", f: "u"},
                                'personaldetail': {lb: "personal details", vl: "personal details", f: "u"},
                                'buttonmodify': {lb: "modify", vl: "modify", f: "n"},
                                'confinfo': {lb: "Please confirm your information", vl: "Please confirm your information", f: "c"},
                                'buttonbook': {lb: $scope.buttonLabel, vl: $scope.buttonLabel, f: "u"},
                                'bookingtitlecf': {lb: $scope.bookingtitlecf, vl: $scope.bookingtitlecf, f: "u"},
                                'confirmsg': {lb: $scope.confirmsg, vl: $scope.confirmsg, f: "n"},
                                'confirmsg2': {lb: $scope.confirmsg2, vl: $scope.confirmsg2, f: "n"},
                                'confirmsg3': {lb: $scope.confirmsg3, vl: $scope.confirmsg3, f: "n"},
                                'confirmsg4': {lb: $scope.confirmsg4, vl: $scope.confirmsg4, f: "n"},
                                'bacchanalia': {lb: $scope.bacchanalia, vl: $scope.bacchanalia, f: "n"},
                                'timeouttag': {lb: "timeout", vl: "timeout", f: "u"},
                                'listtag1': {lb: "free booking", vl: "free booking", f: "c"},
                                'listtag2': {lb: $scope.listTags2, vl: $scope.listTags2, f: "c"},
                                'listtag3': {lb: $scope.listTags3, vl: $scope.listTags3, f: "c"},
                                'listtag4': {lb: $scope.listTags4, vl: $scope.listTags4, f: "c"},
                                'listtag5': {lb: $scope.listTags5, vl: $scope.listTags5, f: "n"}
                            };

                        $scope.checkmark = [{"label1": $scope.zang['listtag1'].vl, "label2": " ", "glyph": true}, {"label1": $scope.zang['listtag2'].vl, "label2": $scope.zang['listtag3'].vl, "glyph": true}, {"label1": $scope.zang['listtag4'].vl, "label2": $scope.zang['listtag5'].vl, "glyph": true}];
                        if (flgsg == false)
                            $scope.checkmark.pop(); // don't show "win everytime
                        $scope.bookinginfo = [{"label": "divider", "value": $scope.zang['bookingdetail'].vl}, {"label": 'Section', "value": $scope.pproduct}, {"label": $scope.zang['datetag'].vl, "value": $scope.pdate}, {"label": $scope.zang['timetag'].vl, "value": $scope.ptime}, {"label": $scope.zang['guesttag'].vl, "value": $scope.pcover}, {"label": "divider", "value": $scope.zang['personaldetail'].vl}, {"label": $scope.zang['nametag'].vl, "value": $scope.pname}, {"label": $scope.zang['emailtag'].vl, "value": $scope.pemail}, {"label": $scope.zang['mobiletag'].vl, "value": $scope.pmobile}, {"label": $scope.zang['requesttag'].vl, "value": $scope.prequest}];
                        $scope.fbshareclik = function () {

                            var fbObj = fbshare();
                            FB.ui(fbObj, function (response) {
                                console.log(JSON.stringify(response));
                            });

                        }
                        function fbshare() {
                            var obj = {
                                method: 'feed',
                                link: "https://www.weeloy.com/" + partialpath,
                                picture: $scope.imglogo,
                                name: $scope.hoteltitle,
                                caption: 'New booking at ' + $scope.hoteltitle + '. Now enjoy rewards with Weeloy',
                                description: partialdesc,
                                display: 'popup'
                            };
                            return obj;

                        }
                        function sortMethod(a, b) {
                            var x = a.name.toLowerCase();
                            var y = b.name.toLowerCase();
                            return ((x < y) ? -1 : ((x > y) ? 1 : 0));
                        }


                    });

                    String.prototype.capitalize = function () {
                        return this.replace(/^./, function (match) {
                            return match.toUpperCase();
                        });
                    };

                    function openWin(type) {
                        var arg = <?php echo "'$QRCodeArg'"; ?>;
                        window.open('modules/qrcode/QRCode.htm?type=' + type + arg, 'QRCode', 'toolbar=no, scrollbars=no, resizable=no, top=30, left=30, width=330, height=330');
                    }

                    $(document).ready(function () {
                        //            var sep = (window.location.href.indexOf("?")===-1)?"?":"&";
                        //            window.location.href = window.location.href + sep + "foo=bar";
                        //            $(document).ready(function() {
                        //                $.ajaxSetup({ cache: false }); // This part addresses an IE bug.  without it, IE will only load the first number and will never refresh
                        //                setInterval(function() {
                        //                  $('#notice_div').load('response.php');
                        //                }, 3000); // the "3000" 
                        //            });

                        window.fbAsyncInit = function () {
                            FB.init({
                                appId: fbId, //weeloy localhost fb appId
                                status: true,
                                cookie: true,
                                xfbml: true
                            });

                        }; //end fbAsyncInit

                        // Load the SDK Asynchronously
                        (function (d) {
                            var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
                            if (d.getElementById(id)) {
                                return;
                            }
                            js = d.createElement('script');
                            js.id = id;
                            js.async = true;
                            js.src = "//connect.facebook.net/en_US/all.js";
                            ref.parentNode.insertBefore(js, ref);
                        }(document)); //end loadSDF


                    });

        </script>


    </body>
</html>


