<?

require_once("conf/conf.init.inc.php");

$arglist = array('error_no');
foreach ($arglist as $label) {
    if (!isset($_REQUEST[$label]))
        $_REQUEST[$label] = "";
    $$label = preg_replace("/\'|\"/", "’", $_REQUEST[$label]);
    $url_get_parameters .= '&'.$label.'=' . $$label;
}

$error_no = intval($error_no);

?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv='expires' content='mon, 1 jan 1998 05:00:00 gmt'/>
<meta http-equiv='pragma' content='no-cache'/>
<meta http-equiv='pragma' content='cache-control: max-age=0'/>
<meta http-equiv='last-modified' content='mon, 1 jan 1998 05:00:00 gmt'/>
<meta http-equiv='cache-control' content='no-cache, must-revalidate'/>
<meta name='robots' content='noindex, nofollow'/>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="HandheldFriendly" content="true" />
<title>Payment Method - Weeloy - Error</title>
<base href="<?php echo __ROOTDIR__; ?>/"/>
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap-select.css" rel="stylesheet" />
<link href="css/bootstrap-social.css" rel="stylesheet" />
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="modules/booking/assets/css/bookingform.css?v=1" rel="stylesheet" />

<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type='text/javascript' src="js/angular.min.js"></script>
<script type="text/javascript" src="js/ngStorage.min.js"></script>
<style>

h1 { background-color: black; }
.panel { border-color: black !important; }
.panel-heading { background-color: black !important; border-color: black !important; }
.caret { color: black; }
@media (min-width: 530px) {
.separation { border-right: 4px solid black; height: 100%; }
.separation-left { border-left: 4px solid black; }
}

fieldset.optionGroup
{
	float: none;
	margin-left: 25%;
	margin-bottom: 15px;
}
fieldset.optionGroup label
{
	display: inline;
	float: none;
	width: 100px;
}
fieldset.optionGroup input
{
	float:none;
	margin: 0px;
	width: 20px;
	position:relative;
	top:-16px;
}
fieldset.optionGroup div
{
	margin-bottom: 10px;
}
fieldset.optionGroup img
{
	display:inline; 
	margin: auto;
	padding-bottom: 0px; 
	vertical-align: inherit;
}
</style>
</head>
<body ng-app="myApp"  style="background: transparent none repeat scroll 0 0;"  class="ng-cloak">

<div id='booking' ng-controller='MainController'>
<div class="container mainbox">    
	<div class="panel panel-info"  >
		<div class="panel-heading">
			<div class="panel-title" style='width:300px;'> PAYMENT METHOD </div>
		</div>  
		<div> 
			<p class="slogan"  class='text-warning'>{{ errormsg }}</p>
		</div>
	</div>  
</div>
</div>

<script>

var app = angular.module("myApp", ['ngStorage']);

app.controller('MainController', function ($scope) {

	$scope.errormsg = "We are unable to process your credit card payment. Your Reservation session expired. Please make a new rservation.";
	$scope.errornb = <?php echo $error_no ?>;
        switch($scope.errornb) {
        	case 1:
			$scope.errormsg = "We are unable to process your credit card payment. Your Reservation session expired. Please make a new rservation.";
			break;
        	case 2:
			$scope.errormsg = "We are unable to process your credit card payment. Your Reservation is invalid. Please make a new rservation.";
			break;
		default:
			$scope.errormsg = "Unknow error. Please make a new rservation.";
			break;
        	}
        	
});

</script>

</body>
</html>


