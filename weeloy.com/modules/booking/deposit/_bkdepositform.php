<div id='booking' ng-controller='MainController'  >
    
    <div class="container mainbox">    
        <div class="panel panel-info" ng-if='showconfirmation ' >
            <div class="panel-heading" >
                <div class="panel-title" style='width:300px;'> {{ zang['bookingtitlecf'].vl }} </div>
            </div>     
           
            <p  class="slogan">
                <span ng-if="payment_status == 'CONFIRMED'" >
                    {{ zang['confirmsg'].vl}} {{hoteltitle}}<br /><br />
                    <strong>{{ zang['confirmsg2'].vl}} {{confirmation}} {{spincode}} </strong>
                    <br/>{{ zang['confirmsg3'].vl}}
                </span>
                <span ng-if="payment_status == 'PENDING'" >
                     {{confirmsg5}} {{hoteltitle}}<br /><br />
<!--                    <strong>{{ zang['confirmsg2'].vl}} {{confirmation}} {{spincode}} </strong>-->
              {{ zang['confirmsg3'].vl}}
                </span> 
   
            </p>
<!--            <p ng-show="restaurant === 'SG_SG_R_Bacchanalia'" class="slogan">{{ zang['bacchanalia'].vl}}<br /><br /></p>-->

            <div class="panel-body" >

                <div class='left-column'>
                    <img ng-src='{{imglogo}}' height='120px' id='theLogo' name='theLogo'>
                    <div class="book-form-lg">
                        <table width='90%'>
                            <tr ng-repeat="x in checkmark">
                                <td>
                                    <span class='glyphicon glyphicon-ok checkmark'></span> 
                                    <br />&nbsp;
                                </td>
                                <td ng-class='pcheckmark'>
                                    <span ng-bind="x.label1"></span>
                                    <span ng-bind="x.label2"></span> 
                                    <br />&nbsp;
                                </td>
                        </table>
                    </div>
                </div>

                <div class='right-column separation-left no-mobile'>

                    <div ng-repeat="x in bookinginfo" class='row' style="margin-left:5px">
                        <div class="input-group" ng-if="x.value != '' && x.label != 'divider'">
                            <span ng-if ='x.value' > {{x.label}} : {{x.value}} </span>
                        </div>
                        <h1 ng-if="x.label == 'divider'" ng-bind="x.value | uppercase" ></h1>
                    </div>
                   
<!--                    <div ng-if="isCreditCardInfoActive===0">
                    <h1>Deposit</h1>
                        <p>Your deposit of  SGD  {{resBookingDeposit}} has been approved</p>
                        <div class="input-group">
                            <span > Transaction : {{transactionid}} </span>
                        </div>
                    </div>-->
                    <div ng-if="false" >
                    <h1>CANCELLAION POLICY</h1>
                      <div  ng-repeat ="p in cancelpolicy">
                        <p ng-if="p['percentage'] == 100">{{p['duration']}}{{resCurrency}} {{resBookingDeposit}}. 
                        <p ng-if="p['percentage'] == 0">{{p['duration']}}</p>
                        <p ng-if="p['percentage'] != 0 && p['percentage']!=100">{{p['duration']}} </p>
                       </div>
                    </div>

                    
                    <table>
                        <tr>
                            <td><a href='javascript:openWin(1);'><br/>QRCode Agenda</a></td>
                            <td width='50'>&nbsp;</td>
                            <td><a href='javascript:openWin(0);'><br/>QRCode Contact</a></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="panel panel-info" ng-if='showpending' >
            <div class="panel-heading" >
                <div class="panel-title" style='width:300px;'> BOOKING CONFIRMATION PENDING </div>
            </div>     
            <p ng-show="restaurant !== 'SG_SG_R_Bacchanalia'" class="slogan">Your booking is pending at {{hoteltitle}}<br /><br />
<!--                <strong>{{ zang['confirmsg2'].vl}} {{confirmation}} {{spincode}} </strong>-->
<!--                <br/>{{ zang['confirmsg3'].vl}}-->
                You will receive the details of your pending reservation by email 
                <br /><span style="font-size:11px;" ng-if='flgsg'><a href='https://www.weeloy.com' target='_blank'>{{ zang['confirmsg4'].vl}}</a> </span> 
            </p>
            <p ng-show="restaurant === 'SG_SG_R_Bacchanalia'" class="slogan">{{ zang['bacchanalia'].vl}}<br /><br /></p>

            <div class="panel-body" >

                <div class='left-column'>
                    <img ng-src='{{imglogo}}' height='120px' id='theLogo' name='theLogo'>
                    <div class="book-form-lg">
                        <table width='90%'>
                            <tr ng-repeat="x in checkmark">
                                <td>
                                    <span class='glyphicon glyphicon-ok checkmark'></span> 
                                    <br />&nbsp;
                                </td>
                                <td ng-class='pcheckmark'>
                                    <span ng-bind="x.label1"></span>
                                    <span ng-bind="x.label2"></span> 
                                    <br />&nbsp;
                                </td>
                        </table>
                    </div>
                </div>

                <div class='right-column separation-left no-mobile'>

                    <div ng-repeat="x in bookinginfo" class='row' style="margin-left:5px">
                        <div class="input-group" ng-if="x.value != '' && x.label != 'divider'">
                            <span > {{x.label}} : {{x.value}} </span>
                        </div>
                        <h1 ng-if="x.label == 'divider'" ng-bind="x.value | uppercase" style="background-color:black !important"></h1>
                    </div>
                   
<!--                    <div ng-if="isCreditCardInfoActive===0">
                    <h1>Deposit</h1>
                        <p>Your deposit of  SGD  {{resBookingDeposit}} has been approved</p>
                        <div class="input-group">
                            <span > Transaction : {{transactionid}} </span>
                        </div>
                    </div>-->
                    <div >
                    <h1>CANCELLAION POLICY</h1>
                      <div  ng-repeat ="p in cancelpolicy">
                        <p ng-if="p['percentage']==100">{{p['duration']}}SGD {{resBookingDeposit}}. 
                        <p ng-if="p['percentage']==0">{{p['duration']}}</p>
                       </div>
                    </div>
                    <table>
                        <tr>
                            <td><a href='javascript:openWin(1);'><br/>QRCode Agenda</a></td>
                            <td width='50'>&nbsp;</td>
                            <td><a href='javascript:openWin(0);'><br/>QRCode Contact</a></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>

        
        <div class="panel panel-info" ng-if='showCancel' >
            <div class="panel-heading" >
                <div class="panel-title" style='width:300px;'> {{ zang['bookingtitlecf'].vl | uppercase}} </div>
            </div>     

            <p class="slogan">Your payment transaction has been CANCELLED.</p>
            
            <div class="panel-body" >

                <div class='left-column'>
                    <img ng-src='{{imglogo}}' height='120px' id='theLogo' name='theLogo'>
                    <div class="book-form-lg">
                        <table width='90%'>
                            <tr ng-repeat="x in checkmark">
                                <td>
                                    <span class='glyphicon glyphicon-ok checkmark'></span> 
                                    <br />&nbsp;
                                </td>
                                <td ng-class='pcheckmark'>
                                    <span ng-bind="x.label1"></span>
                                    <span ng-bind="x.label2"></span> 
                                    <br />&nbsp;
                                </td>
                        </table>
                    </div>
                </div>

                <div class='right-column separation-left no-mobile'>

                    <div ng-repeat="x in bookinginfo" class='row' style="margin-left:5px">
                        <div class="input-group" ng-if="x.value != '' && x.label != 'divider'">
                            <span > {{x.label}} : {{x.value}} </span>
                        </div>
                        <h1 ng-if="x.label == 'divider'" ng-bind="x.value | uppercase"></h1>
                    </div>
                    <div>
                    <h1>Deposit</h1>
                        Your deposit has been REJECTED<br />
                        <span ng-if="iswhitelabel===false">
                            You can retry to deposit from <a target="_top" href ='<?php echo $baseUrl ?>/mybookings'>Mybookings section</a> within 20 mins. 
                        </span>
                    </div>
                </div>
            </div>
        </div>
            
    
        </div>

    </div>                     
 