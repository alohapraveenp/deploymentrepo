<?php

require_once("lib/Browser.inc.php");
$browser = new Browser();

require_once("conf/conf.init.inc.php");
require_once("lib/wpdo.inc.php");
//require_once("lib/class.session.inc.php");
require_once("conf/conf.session.inc.php");
require_once("lib/wglobals.inc.php");
require_once("lib/class.media.inc.php");
require_once("lib/class.restaurant.inc.php");
require_once("lib/class.promotion.inc.php");

require_once("lib/class.coding.inc.php");
require_once("lib/class.member.inc.php");
require_once("lib/class.cluster.inc.php");
require_once("lib/class.restaurant_cancel_policy.inc.php");


if(empty($source_form ) || $source_form != 'ext_iframe'){
    require_once("lib/class.analytics.inc.php");
}

// 2 facebook ID, 1 for our web weeloy, and 2 for our facebook
// the 2 ID have to be coherent with the class.login wich will use the facebook
// the library will check the tracking to choose the right ID.

$referrer = (isset($_SERVER["HTTP_REFERER"])) ? $_SERVER["HTTP_REFERER"] : "";
if(!empty($referrer) && empty($_REQUEST['bkrestaurant'])) {
	if(preg_match("/href=1/", $referrer) && preg_match("/bkrestaurant=([^&]+)/", $referrer, $match)) {
		$found = $match[1];
		if(WY_Restaurant::isrestaurant($found))         
			$_REQUEST['bkrestaurant'] = $found;
		}
	}

if (isset($_REQUEST['bkrestaurant'])) {
    $_REQUEST['bkrestaurant'] = clean_text($_REQUEST['bkrestaurant']);
}


$emailcluster = "";
$res = new WY_restaurant;
$data = (isset($_REQUEST['data'])) ? $_REQUEST['data'] : "";
$bkrestaurant = (isset($_REQUEST['bkrestaurant'])) ? $_REQUEST['bkrestaurant'] : "";
$bktitle = (isset($_REQUEST['bktitle'])) ? $_REQUEST['bktitle'] : "";
if (!empty($data)) {
    $token = $data;
    $emailcluster = WY_Cluster::getClusterContent($token, "BOOKINGGROUP");
//    if($token === "ImYjvwUOLIYOYO2g_RZcZZp3INiXZSUybURFUpbkdzsU")
//    	$emailcluster = "michael.dipalma@pscafe.com";
}

if (!empty($_REQUEST['city']) && !empty($_REQUEST['country']) && !empty($_REQUEST['bktitle']) && empty($_REQUEST['bkrestaurant'])) {
    //generate restaurant id
    $country = $_GET['country'];
    $city = $_GET['city'];
    $restaurant_name = $_GET['bktitle'];
    //SELECT * FROM `geo_city`
    $countryAr = $cityAr = array();
    $areaAr = $res->getCountryCityList();
    if(count($areaAr) > 1) {
    	$countryAr = $areaAr['country'];
    	$cityAr = $areaAr['city'];
    	} 
    //$countryAr = array('singapore' => 'SG', 'hong-kong' => 'HK', 'thailand' => 'TH', 'bangkok' => 'TH', 'phuket' => 'TH', 'france' => 'FR', 'paris'=> 'FR');
    //$cityAr = array('singapore' => 'SG', 'hong-kong' => 'HK', 'bangkok' => 'BK', 'phuket' => 'PK', 'kuala-lumpur' => 'KL', 'paris'=> 'PR');
    $RestaurantID = (array_key_exists($country, $countryAr)) ? $countryAr[$country] : 'SG';
    $RestaurantID .= '_';
    $RestaurantID .= (array_key_exists($city, $cityAr)) ? $cityAr[$city] : 'SG';
    $RestaurantID .= '_R_';

    $restaurant_name = ucfirst($restaurant_name);
    $restaurant_name = preg_replace_callback('/-[a-z]/', function ($matches) { return strtoupper($matches[0]); }, $restaurant_name);
    $restaurant_name = str_replace('-', '', $restaurant_name);
    $RestaurantID .= $restaurant_name;

    $_REQUEST['bkrestaurant'] = $RestaurantID;
}

$restaurant_code = '';
$signed_request = '';
if (isset($_REQUEST['signed_request'])) {
    $signed_request = $_REQUEST['signed_request'];
}

$facebookappid = "1590587811210971";

if (!empty($signed_request)) {

    $facebookappid = "755782447874157";
    $_REQUEST['bktracking'] = "facebook";

    $data_signed_request = explode('.', $signed_request); // Get the part of the signed_request we need.
    $jsonData = base64_decode($data_signed_request['1']); // Base64 Decode signed_request making it JSON.
    $objData = json_decode($jsonData, true); // Split the JSON into arrays.
    $pageData = $objData['page'];
    $liked = $pageData['liked']; //you have the liked boolean
    $sourceData = $objData['app_data']; // yay you got the damn data in an string, slow clap for you
    $appDataArray = explode(';', $sourceData); // explode the app data because you hate it as a string, only do this if you are sending an array of variables through the app_data
    // go to fangate bc they dont like you, use a script so you don't get header errors
    if ($liked === false) {
        //page was not liked
        // go to other page bc they liked and send the app_data in a string
    } else {
        //all good
        //print_r($objData);
        //identify restaurant by page_id
        if (isset($objData['page']['id'])) {
            $facebook_pid = $objData['page']['id'];
            $info = $res->getRestaurantByFacebookPageId($facebook_pid);
            if (count($info) > 0 && isset($info['restaurant']) && $res->restaurant == $info['restaurant']) {
                $_REQUEST['bkrestaurant'] = $res->restaurant;
                $_REQUEST['bktitle'] = $res->title;
                if (isset($res->cluster)) {
                    $emailcluster = $res->cluster;
                }
            }

            if (empty($_REQUEST['bkrestaurant'])) {
                $_REQUEST['bkrestaurant'] = 'SG_SG_R_TheFunKitchen';
            }
        }

        $_SESSION['user'] = array();
        $_SESSION['user']['fbid'] = $objData['user_id'];

        //init user country
        if (!empty($objData['user']) && isset($objData['user']['country']) && empty($_REQUEST['bkcountry'])) {
            $facebook_user_country = strtoupper($objData['user']['country']);
            if (in_array($facebook_user_country, array('SG', 'TH', 'MY', 'HK', 'CN', 'ID', 'JP'))) {
                $_REQUEST['bkcountry'] = $facebook_user_country;
                //echo 'found country ' . $facebook_user_country;
            }
        }
    }
}

if (!empty($emailcluster)) {
    $selectresto = $selectbooker = "";

    list($cret, $selectresto, $selectbooker, $email, $bkrestaurant, $bktitle) = $res->getClusterCallCenter($emailcluster, $bkrestaurant, $bktitle);
    if ($cret == -1) {
        echo "ERROR CONFIGURATION CALL CENTER <br /> UNABLE TO CONTINUE. $email";
        exit;
    }
    
    
    $empty_option = '';
    if(!empty($_REQUEST['restaurantselected'])){
        $restaurantselected = intval($_REQUEST['restaurantselected']);
    }else{
        $empty_option = '<option style="color:red!important" selected>Select your restaurant</option>';
    }
    
    
    if (!empty($selectresto)) {
        $selectresto = "<select name='restaurant' id='restaurant' class='selectpicker' data-width='290px' data-style='btn' onchange='reloadbk(this.value, tracking);'>$empty_option" . $selectresto . "</select>";
        
        //localhost.weeloy.com:8888/modules/booking/book_form.php?data=ImYjvwUOLIYOYO2g_RZcZZp3INiXZSUybURFUpbkdzsU&bktracking=WEBSITE&bksourcetype=form
        
        if( empty($restaurantselected) ){ $selectresto = str_replace('selected', '', $selectresto); }
        $_REQUEST['bkrestaurant'] = $bkrestaurant;
        $_REQUEST['bktitle'] = $bktitle;
    }
}

$arglist = array('bklangue', 'bksalutation', 'bklast', 'bkfirst', 'bkemail', 'bkcover', 'bkdate', 'bktime', 'bkmobile', 'bkcountry', 'bkrestaurant', 'bktitle', 'bkparam', 'bkspecialrequest', 'brwsr_type', 'bkpage', 'bktracking', 'data', 'booker', 'company', 'hotelguest', 'babychair', 'bkextra', 'bkchild', 'mealtype', 'bkerror', 'bkproduct', 'restaurantselected');
foreach ($arglist as $label) {
    if (empty($_REQUEST[$label])) {
        $_REQUEST[$label] = $$label = "";
    } else {
        $$label = preg_replace("/\'|\"/", "’", $_REQUEST[$label]);
    }
}

if (empty($_REQUEST['bkrestaurant'])) {
    $_REQUEST['bkrestaurant'] = 'SG_SG_R_TheFunKitchen';
    $_REQUEST['bktitle'] = 'The Fun Kitchen';
}

// CHECK IF TRACKING ACTIVE
if ($_REQUEST['bktracking'] != 'CALLCENTER' && $_REQUEST['bktracking'] != 'WEBSITE' && $_REQUEST['bktracking'] != 'facebook' && $_REQUEST['bktracking'] != 'GRABZ' ) {
    $_REQUEST['bktracking'] = filter_input(INPUT_COOKIE, 'weelredir', FILTER_SANITIZE_STRING);
}

if (empty($bkrestaurant)) {
    header("location: https://www.weeloy.com");
    exit;
}

if(empty($bkcover)) {
	$cstate = "new";
	$bkcover = 2;
	}
else $cstate = "again";

if (empty($bklangue)) {
    $bklangue = "en";
}

$bkparam = date('M') . strval(microtime(true));
$bkparam = tokenize($bkparam);
$timeout = (!empty($_REQUEST['timeout'])) ? $_REQUEST['timeout'] : "";

$res->getRestaurant($bkrestaurant);
$promocode = $res->promocodeBkg();
$optinbkg = $res->optinBkg();
$bookingwindow = $res->getBookingwindow();
$isBooking15mn =($res->checkBooking15mn() != 0) ? 1 : 0;  

$Bookchild = ($res->bookingwithChildren() != 0) ? 1 : 0;
$frontinfo = $listarea = $listpurpose = "";



if(!empty($res->restogeneric)) {	//  && preg_match("/website|facebook/i", $bktracking)
	$obj = preg_replace("/’/", "\"", $res->restogeneric);
	$objAr = json_decode($obj, true);
	if(isset($objAr["frontbook"]) && strlen($objAr["frontbook"]) > 5) {
		$frontinfo = preg_replace("/\'|\"/", "’", $objAr["frontbook"]);
		$frontinfo = preg_replace("/\r|\n/", ", ", $frontinfo);
		}
	if(isset($objAr["choice"]) && strlen($objAr["choice"]) > 5) {
		$choice = preg_replace("/\'|\"/", "’", $objAr["choice"]);
		$choice = preg_replace("/\r|\n/", ", ", $choice);
		}
	if($bkrestaurant == "SG_SG_R_MadisonRooms" && isset($objAr["listarea"]) && strlen($objAr["listarea"]) > 5)
		$listarea = $objAr["listarea"];
	if($bkrestaurant == "SG_SG_R_MadisonRooms" && isset($objAr["listpurpose"]) && strlen($objAr["listpurpose"]) > 5)
		$listpurpose = $objAr["listpurpose"];
	}

  $resCountry = $res->country;
  $isDeposit =($res->checkbkdeposit() != 0) ? 1 : 0;  
                
$resBookingDepositPerPax = null;
$resBookingDepositPerPaxflg = 0;




if($isDeposit==true){
    if($bkrestaurant === 'SG_SG_R_TheFunKitchen' || $bkrestaurant === 'SG_SG_R_Bacchanalia' ){
        $respolicy = new WY_Restaurant_policy;
        $depositAmount = $respolicy->getDefaultPrice($bkrestaurant);
         if(count($depositAmount)>0){
             $resBookingDepositPerPax = array('lunch'=>$depositAmount['lunch_charge'], 'dinner'=>$depositAmount['dinner_charge']);
             $resBookingDepositPerPaxflg = 97;
         }
        
    }else{
        //deposit amount 
        $depositAmount = $res->getResDepositAmount($bkrestaurant);
        if(count($depositAmount)>0 && (!empty($depositAmount['lunch_charge']) || !empty($depositAmount['dinner_charge']) )){
            $resBookingDepositPerPax = array('lunch'=>$depositAmount['lunch_charge'], 'dinner'=>$depositAmount['dinner_charge']);
            $resBookingDepositPerPaxflg = 97;
        }
        
    }
    


}
    $isCreditCardInfoActive = ($res->checkCreditCardDetails() != 0) ? 1 : 0;
    $isPaypalActive = ($res->checkPaypalActive() > 0) ? 1 : 0;

$resCurrency = $res->currency;

if (empty($res->restaurant)) {
    header("location: https://www.weeloy.com");
    exit;
}

$maxpax = $res->dfmaxpers;
$minpax = $res->dfminpers;

if ($minpax < 1 || $minpax > 4) {
    $minpax = 1;
}

if ($maxpax < 4) {
    $maxpax = 4;
}

if($bkcover < $minpax) $bkcover = $minpax;
else if($bkcover > $maxpax) $bkcover = $maxpax;

$AvailperPax = ($res->perPaxBooking() != 0) ? "1" : "0";

//tracking
//test tracking cookie
$logger = new WY_log("website");
$loguserid = (isset($_SESSION['user']['user_id'])) ? $_SESSION['user']['user_id'] : "unknown";
$logger->LogEvent($loguserid, 801, $res->ID,'' , '', date("Y-m-d H:i:s"));

// book / request process
$action = 'book';
if (filter_input(INPUT_GET, $action, FILTER_SANITIZE_STRING) == 'request') {
    $action = 'request';
}

$fullfeature = preg_match("/TheFunKitchen/", $bkrestaurant) || preg_match("/TheOneKitchen/", $bkrestaurant) || in_array($_SESSION['user']['member_type'], $fullfeature_member_type_allowed) || ($_SESSION['user']['member_type'] == 'weeloy_sales' && $res->status == 'demo_reference' || $res->status == 'active');

$typeBooking = ($res->is_bookable && ($res->status == 'active' || $res->status == 'demo_reference')) ? "true" : "false";
$is_listing = false;
if (empty($res->is_wheelable) || !$res->is_wheelable) {
    $is_listing = true;
}

$nginit = "typeBooking=" . $typeBooking . ";";

$mediadata = new WY_Media($bkrestaurant);
$logo = $mediadata->getLogo($bkrestaurant);

$restaurant_restaurant_tnc = $res->restaurant_tnc;

$date = date_create(null, timezone_open("Asia/Singapore"));
$data_min = date_format($date, "d/m/Y");
date_add($date, date_interval_create_from_date_string("60 days"));
$data_max = date_format($date, "d/m/Y");

if (empty($bkdate)) {
    $bkdate = $data_min;
}

if (empty($bktime)) {
    $bktime = "19:00";
}

$pickDate = substr($bkdate, 6, 4) . "-" . substr($bkdate, 3, 2) . "-" . substr($bkdate, 0, 2);
$objToday = new DateTime(date("Y-m-d"));
$objDateTime = new DateTime($pickDate);
$ndays = $objToday->diff($objDateTime)->format('%a');

$actionfile = $_SERVER['PHP_SELF'];

$brwsr_type = ($browser->isMobile() || (isset($_REQUEST['brwsr_type']) && $_REQUEST['brwsr_type'] == "mobile")) ? "mobile" : "";
if ($browser->isTablet()) {
    $brwsr_type = "tablette";
}

$iphoneflg = (strtolower($browser->getPlatform()) == "iphone");
$termsconditions = "../templates/tnc/termsservices.php?closebut=" . time(); // force to reload. Refresh does not force it on modal.

$countriesAr = array(array('a' => 'Australia', 'b' => 'au', 'c' => '+61'), array('a' => 'China', 'b' => 'cn', 'c' => '+86'), array('a' => 'Hong Kong', 'b' => 'hk', 'c' => '+852'), array('a' => 'India', 'b' => 'in', 'c' => '+91'), array('a' => 'Indonesia', 'b' => 'id', 'c' => '+62'), array('a' => 'Japan', 'b' => 'jp', 'c' => '+81'), array('a' => 'Malaysia', 'b' => 'my', 'c' => '+60'), array('a' => 'Myanmar', 'b' => 'mm', 'c' => '+95'), array('a' => 'New Zealand', 'b' => 'nz', 'c' => '+64'), array('a' => 'Philippines', 'b' => 'ph', 'c' => '+63'), array('a' => 'Singapore', 'b' => 'sg', 'c' => '+65'), array('a' => 'South Korea', 'b' => 'kr', 'c' => '+82'), array('a' => 'Thailand', 'b' => 'th', 'c' => '+66'), array('a' => 'Vietnam', 'b' => 'vn', 'c' => '+84'), array('a' => '', 'b' => '', 'c' => ''), array('a' => 'Canada', 'b' => 'ca', 'c' => '+1'), array('a' => 'France', 'b' => 'fr', 'c' => '+33'), array('a' => 'Germany', 'b' => 'de', 'c' => '+49'), array('a' => 'Italy', 'b' => 'it', 'c' => '+39'), array('a' => 'Russia', 'b' => 'ru', 'c' => '+7'), array('a' => 'Spain', 'b' => 'es', 'c' => '+34'), array('a' => 'Sweden', 'b' => 'se', 'c' => '+46'), array('a' => 'Switzerland', 'b' => 'ch', 'c' => '+41'), array('a' => 'UnitedKingdom', 'b' => 'gb', 'c' => '+44'), array('a' => 'UnitedStates', 'b' => 'us', 'c' => '+1'), array('a' => '', 'b' => '', 'c' => ''), array('a' => 'Afghanistan', 'b' => 'af', 'c' => '+93'), array('a' => 'Albania', 'b' => 'al', 'c' => '+355'), array('a' => 'Algeria', 'b' => 'dz', 'c' => '+213'), array('a' => 'Andorra', 'b' => 'ad', 'c' => '+376'), array('a' => 'Angola', 'b' => 'ao', 'c' => '+244'), array('a' => 'Antarctica', 'b' => 'aq', 'c' => '+672'), array('a' => 'Argentina', 'b' => 'ar', 'c' => '+54'), array('a' => 'Armenia', 'b' => 'am', 'c' => '+374'), array('a' => 'Aruba', 'b' => 'aw', 'c' => '+297'), array('a' => 'Austria', 'b' => 'at', 'c' => '+43'), array('a' => 'Azerbaijan', 'b' => 'az', 'c' => '+994'), array('a' => 'Bahrain', 'b' => 'bh', 'c' => '+973'), array('a' => 'Bangladesh', 'b' => 'bd', 'c' => '+880'), array('a' => 'Belarus', 'b' => 'by', 'c' => '+375'), array('a' => 'Belgium', 'b' => 'be', 'c' => '+32'), array('a' => 'Belize', 'b' => 'bz', 'c' => '+501'), array('a' => 'Benin', 'b' => 'bj', 'c' => '+229'), array('a' => 'Bhutan', 'b' => 'bt', 'c' => '+975'), array('a' => 'Bolivia', 'b' => 'bo', 'c' => '+591'), array('a' => 'BosniaandHerzegovina', 'b' => 'ba', 'c' => '+387'), array('a' => 'Botswana', 'b' => 'bw', 'c' => '+267'), array('a' => 'Brazil', 'b' => 'br', 'c' => '+55'), array('a' => 'Brunei', 'b' => 'bn', 'c' => '+673'), array('a' => 'Bulgaria', 'b' => 'bg', 'c' => '+359'), array('a' => 'BurkinaFaso', 'b' => 'bf', 'c' => '+226'), array('a' => 'Burundi', 'b' => 'bi', 'c' => '+257'), array('a' => 'Cambodia', 'b' => 'kh', 'c' => '+855'), array('a' => 'Cameroon', 'b' => 'cm', 'c' => '+237'), array('a' => 'CapeVerde', 'b' => 'cv', 'c' => '+238'), array('a' => 'CentralAfricanRepublic', 'b' => 'cf', 'c' => '+236'), array('a' => 'Chad', 'b' => 'td', 'c' => '+235'), array('a' => 'Chile', 'b' => 'cl', 'c' => '+56'), array('a' => 'ChristmasIsland', 'b' => 'cx', 'c' => '+61'), array('a' => 'CocosIslands', 'b' => 'cc', 'c' => '+61'), array('a' => 'Colombia', 'b' => 'co', 'c' => '+57'), array('a' => 'Comoros', 'b' => 'km', 'c' => '+269'), array('a' => 'CookIslands', 'b' => 'ck', 'c' => '+682'), array('a' => 'CostaRica', 'b' => 'cr', 'c' => '+506'), array('a' => 'Croatia', 'b' => 'hr', 'c' => '+385'), array('a' => 'Cuba', 'b' => 'cu', 'c' => '+53'), array('a' => 'Curacao', 'b' => 'cw', 'c' => '+599'), array('a' => 'Cyprus', 'b' => 'cy', 'c' => '+357'), array('a' => 'CzechRepublic', 'b' => 'cz', 'c' => '+420'), array('a' => 'DemocraticRepCongo', 'b' => 'cd', 'c' => '+243'), array('a' => 'Denmark', 'b' => 'dk', 'c' => '+45'), array('a' => 'Djibouti', 'b' => 'dj', 'c' => '+253'), array('a' => 'EastTimor', 'b' => 'tl', 'c' => '+670'), array('a' => 'Ecuador', 'b' => 'ec', 'c' => '+593'), array('a' => 'Egypt', 'b' => 'eg', 'c' => '+20'), array('a' => 'ElSalvador', 'b' => 'sv', 'c' => '+503'), array('a' => 'EquatorialGuinea', 'b' => 'gq', 'c' => '+240'), array('a' => 'Eritrea', 'b' => 'er', 'c' => '+291'), array('a' => 'Estonia', 'b' => 'ee', 'c' => '+372'), array('a' => 'Ethiopia', 'b' => 'et', 'c' => '+251'), array('a' => 'FalklandIslands', 'b' => 'fk', 'c' => '+500'), array('a' => 'FaroeIslands', 'b' => 'fo', 'c' => '+298'), array('a' => 'Fiji', 'b' => 'fj', 'c' => '+679'), array('a' => 'Finland', 'b' => 'fi', 'c' => '+358'), array('a' => 'FrenchPolynesia', 'b' => 'pf', 'c' => '+689'), array('a' => 'Gabon', 'b' => 'ga', 'c' => '+241'), array('a' => 'Gambia', 'b' => 'gm', 'c' => '+220'), array('a' => 'Georgia', 'b' => 'ge', 'c' => '+995'), array('a' => 'Ghana', 'b' => 'gh', 'c' => '+233'), array('a' => 'Gibraltar', 'b' => 'gi', 'c' => '+350'), array('a' => 'Greece', 'b' => 'gr', 'c' => '+30'), array('a' => 'Greenland', 'b' => 'gl', 'c' => '+299'), array('a' => 'Guatemala', 'b' => 'gt', 'c' => '+502'), array('a' => 'Guernsey', 'b' => 'gg', 'c' => '+44-1481'), array('a' => 'Guinea', 'b' => 'gn', 'c' => '+224'), array('a' => 'Guinea-Bissau', 'b' => 'gw', 'c' => '+245'), array('a' => 'Guyana', 'b' => 'gy', 'c' => '+592'), array('a' => 'Haiti', 'b' => 'ht', 'c' => '+509'), array('a' => 'Honduras', 'b' => 'hn', 'c' => '+504'), array('a' => 'Hungary', 'b' => 'hu', 'c' => '+36'), array('a' => 'Iceland', 'b' => 'is', 'c' => '+354'), array('a' => 'Iran', 'b' => 'ir', 'c' => '+98'), array('a' => 'Iraq', 'b' => 'iq', 'c' => '+964'), array('a' => 'Ireland', 'b' => 'ie', 'c' => '+353'), array('a' => 'IsleofMan', 'b' => 'im', 'c' => '+44-1624'), array('a' => 'Israel', 'b' => 'il', 'c' => '+972'), array('a' => 'IvoryCoast', 'b' => 'ci', 'c' => '+225'), array('a' => 'Jersey', 'b' => 'je', 'c' => '+44-1534'), array('a' => 'Jordan', 'b' => 'jo', 'c' => '+962'), array('a' => 'Kazakhstan', 'b' => 'kz', 'c' => '+7'), array('a' => 'Kenya', 'b' => 'ke', 'c' => '+254'), array('a' => 'Kiribati', 'b' => 'ki', 'c' => '+686'), array('a' => 'Kosovo', 'b' => 'xk', 'c' => '+383'), array('a' => 'Kuwait', 'b' => 'kw', 'c' => '+965'), array('a' => 'Kyrgyzstan', 'b' => 'kg', 'c' => '+996'), array('a' => 'Laos', 'b' => 'la', 'c' => '+856'), array('a' => 'Latvia', 'b' => 'lv', 'c' => '+371'), array('a' => 'Lebanon', 'b' => 'lb', 'c' => '+961'), array('a' => 'Lesotho', 'b' => 'ls', 'c' => '+266'), array('a' => 'Liberia', 'b' => 'lr', 'c' => '+231'), array('a' => 'Libya', 'b' => 'ly', 'c' => '+218'), array('a' => 'Liechtenstein', 'b' => 'li', 'c' => '+423'), array('a' => 'Lithuania', 'b' => 'lt', 'c' => '+370'), array('a' => 'Luxembourg', 'b' => 'lu', 'c' => '+352'), array('a' => 'Macao', 'b' => 'mo', 'c' => '+853'), array('a' => 'Macedonia', 'b' => 'mk', 'c' => '+389'), array('a' => 'Madagascar', 'b' => 'mg', 'c' => '+261'), array('a' => 'Malawi', 'b' => 'mw', 'c' => '+265'), array('a' => 'Maldives', 'b' => 'mv', 'c' => '+960'), array('a' => 'Mali', 'b' => 'ml', 'c' => '+223'), array('a' => 'Malta', 'b' => 'mt', 'c' => '+356'), array('a' => 'MarshallIslands', 'b' => 'mh', 'c' => '+692'), array('a' => 'Mauritania', 'b' => 'mr', 'c' => '+222'), array('a' => 'Mauritius', 'b' => 'mu', 'c' => '+230'), array('a' => 'Mayotte', 'b' => 'yt', 'c' => '+262'), array('a' => 'Mexico', 'b' => 'mx', 'c' => '+52'), array('a' => 'Micronesia', 'b' => 'fm', 'c' => '+691'), array('a' => 'Moldova', 'b' => 'md', 'c' => '+373'), array('a' => 'Monaco', 'b' => 'mc', 'c' => '+377'), array('a' => 'Mongolia', 'b' => 'mn', 'c' => '+976'), array('a' => 'Montenegro', 'b' => 'me', 'c' => '+382'), array('a' => 'Morocco', 'b' => 'ma', 'c' => '+212'), array('a' => 'Mozambique', 'b' => 'mz', 'c' => '+258'), array('a' => 'Namibia', 'b' => 'na', 'c' => '+264'), array('a' => 'Nauru', 'b' => 'nr', 'c' => '+674'), array('a' => 'Nepal', 'b' => 'np', 'c' => '+977'), array('a' => 'Netherlands', 'b' => 'nl', 'c' => '+31'), array('a' => 'NetherlandsAntilles', 'b' => 'an', 'c' => '+599'), array('a' => 'NewCaledonia', 'b' => 'nc', 'c' => '+687'), array('a' => 'Nicaragua', 'b' => 'ni', 'c' => '+505'), array('a' => 'Niger', 'b' => 'ne', 'c' => '+227'), array('a' => 'Nigeria', 'b' => 'ng', 'c' => '+234'), array('a' => 'Niue', 'b' => 'nu', 'c' => '+683'), array('a' => 'NorthKorea', 'b' => 'kp', 'c' => '+850'), array('a' => 'Norway', 'b' => 'no', 'c' => '+47'), array('a' => 'Oman', 'b' => 'om', 'c' => '+968'), array('a' => 'Pakistan', 'b' => 'pk', 'c' => '+92'), array('a' => 'Palau', 'b' => 'pw', 'c' => '+680'), array('a' => 'Palestine', 'b' => 'ps', 'c' => '+970'), array('a' => 'Panama', 'b' => 'pa', 'c' => '+507'), array('a' => 'PapuaNewGuinea', 'b' => 'pg', 'c' => '+675'), array('a' => 'Paraguay', 'b' => 'py', 'c' => '+595'), array('a' => 'Peru', 'b' => 'pe', 'c' => '+51'), array('a' => 'Pitcairn', 'b' => 'pn', 'c' => '+64'), array('a' => 'Poland', 'b' => 'pl', 'c' => '+48'), array('a' => 'Portugal', 'b' => 'pt', 'c' => '+351'), array('a' => 'Qatar', 'b' => 'qa', 'c' => '+974'), array('a' => 'RepublicCongo', 'b' => 'cg', 'c' => '+242'), array('a' => 'Reunion', 'b' => 're', 'c' => '+262'), array('a' => 'Romania', 'b' => 'ro', 'c' => '+40'), array('a' => 'Rwanda', 'b' => 'rw', 'c' => '+250'), array('a' => 'SaintBarthelemy', 'b' => 'bl', 'c' => '+590'), array('a' => 'SaintHelena', 'b' => 'sh', 'c' => '+290'), array('a' => 'SaintMartin', 'b' => 'mf', 'c' => '+590'), array('a' => 'Samoa', 'b' => 'ws', 'c' => '+685'), array('a' => 'SanMarino', 'b' => 'sm', 'c' => '+378'), array('a' => 'SaudiArabia', 'b' => 'sa', 'c' => '+966'), array('a' => 'Senegal', 'b' => 'sn', 'c' => '+221'), array('a' => 'Serbia', 'b' => 'rs', 'c' => '+381'), array('a' => 'Seychelles', 'b' => 'sc', 'c' => '+248'), array('a' => 'SierraLeone', 'b' => 'sl', 'c' => '+232'), array('a' => 'Slovakia', 'b' => 'sk', 'c' => '+421'), array('a' => 'Slovenia', 'b' => 'si', 'c' => '+386'), array('a' => 'SolomonIslands', 'b' => 'sb', 'c' => '+677'), array('a' => 'Somalia', 'b' => 'so', 'c' => '+252'), array('a' => 'SouthAfrica', 'b' => 'za', 'c' => '+27'), array('a' => 'SouthSudan', 'b' => 'ss', 'c' => '+211'), array('a' => 'SriLanka', 'b' => 'lk', 'c' => '+94'), array('a' => 'Sudan', 'b' => 'sd', 'c' => '+249'), array('a' => 'Suriname', 'b' => 'sr', 'c' => '+597'), array('a' => 'Swaziland', 'b' => 'sz', 'c' => '+268'), array('a' => 'Syria', 'b' => 'sy', 'c' => '+963'), array('a' => 'Taiwan', 'b' => 'tw', 'c' => '+886'), array('a' => 'Tajikistan', 'b' => 'tj', 'c' => '+992'), array('a' => 'Tanzania', 'b' => 'tz', 'c' => '+255'), array('a' => 'Togo', 'b' => 'tg', 'c' => '+228'), array('a' => 'Tokelau', 'b' => 'tk', 'c' => '+690'), array('a' => 'Tonga', 'b' => 'to', 'c' => '+676'), array('a' => 'Tunisia', 'b' => 'tn', 'c' => '+216'), array('a' => 'Turkey', 'b' => 'tr', 'c' => '+90'), array('a' => 'Turkmenistan', 'b' => 'tm', 'c' => '+993'), array('a' => 'Tuvalu', 'b' => 'tv', 'c' => '+688'), array('a' => 'Uganda', 'b' => 'ug', 'c' => '+256'), array('a' => 'Ukraine', 'b' => 'ua', 'c' => '+380'), array('a' => 'UnitedArabEmirates', 'b' => 'ae', 'c' => '+971'), array('a' => 'Uruguay', 'b' => 'uy', 'c' => '+598'), array('a' => 'Uzbekistan', 'b' => 'uz', 'c' => '+998'), array('a' => 'Vanuatu', 'b' => 'vu', 'c' => '+678'), array('a' => 'Vatican', 'b' => 'va', 'c' => '+379'), array('a' => 'Venezuela', 'b' => 've', 'c' => '+58'), array('a' => 'WallisandFutuna', 'b' => 'wf', 'c' => '+681'), array('a' => 'WesternSahara', 'b' => 'eh', 'c' => '+212'), array('a' => 'Yemen', 'b' => 'ye', 'c' => '+967'), array('a' => 'Zambia', 'b' => 'zm', 'c' => '+260'), array('a' => 'Zimbabwe', 'b' => 'zw', 'c' => '+263'));

if (empty($bkcountry)) {
    $bkcountry = $resCountry;
}

$phoneIndex = "";
$startingPage = (empty($bkpage)) ? 1 : 0;


if(!empty($bkextra)) {
        $bkextra = preg_replace("/choice\=/", "", $bkextra);
        $bkextra = preg_replace("/choice\:/", "", $bkextra);
}


if (empty($bkpage)) {

	
    $userlastname = (isset($_SESSION['user']['name'])) ? $_SESSION['user']['name'] : "";
    $userfirstname = (isset($_SESSION['user']['firstname'])) ? $_SESSION['user']['firstname'] : "";
    $useremail = (isset($_SESSION['user']['email'])) ? $_SESSION['user']['email'] : "";
    $usermobile = (isset($_SESSION['user']['mobile'])) ? $_SESSION['user']['prefix'] . ' ' . $_SESSION['user']['mobile'] : "";
    $gender = (isset($_SESSION['user']['gender']) && $_SESSION['user']['gender'] == 'Female') ? "Mrs." : "Mr.";

    $usergender = (isset($_SESSION['user']['salutation'])) ? $_SESSION['user']['salutation'] :$gender; 

    if (isset($_SESSION['user']['country'])) {
        $pat = strtolower($_SESSION['user']['country']);
        $limit = count($countriesAr);
        for ($i = 0; $i < $limit; $i++) {
            if ($pat == $countriesAr[$i]['b']) {
                $bkcountry = $countriesAr[$i]['a'];
                $phoneIndex = $countriesAr[$i]['c'];
            }
        }
    }

    if (empty($bkfirst) && !empty($userfirstname)) {
        $bkfirst = $userfirstname;
    }

    if (empty($bklast) && !empty($userlastname)) {
        $bklast = $userlastname;

        if (empty($bkemail) && !empty($useremail))
            $bkemail = $useremail;
    }

    if (empty($bkmobile) && !empty($usermobile)) {
        $bkmobile = $usermobile;
    }

    if (!empty($phoneIndex)) {
        if (empty($bkmobile)) {
            $bkmobile = $phoneIndex;
        }

        if (substr($bkmobile, 0, strlen($phoneIndex)) == $phoneIndex) {
            $bkmobile = $phoneIndex . " " . substr($bkmobile, strlen($phoneIndex));
        } else if (preg_match("/\+\d{2,3} /", $bkmobile)) {
            $bkmobile = $phoneIndex . " " . preg_replace("/\+\d{2,3} /", "", $bkmobile);
        } else if (preg_match("/^\+/", $bkmobile)) {
            $bkmobile = $phoneIndex . " " . substr($bkmobile, 1);
        }
    }

    if (empty($bksalutation) && !empty($usergender)) {
        $bksalutation = $usergender;
    }

    $bkpage = 1;
}

if ($is_listing) {
    $promo = new WY_Promotion($bkrestaurant);
    $promo_details = $promo->getActivePromotionDetails();
    $phpdate = $phpdate = "";
    if(!empty($promo_details['end'])) {
	$phpdate = strtotime($promo_details['end']);
	$promo_end_date = date('d-m-Y', $phpdate);
	}
}

$hiddenlist = array('fbtoken' => '', 'fbemail' => '', 'fbname' => '', 'fblastname' => '', 'fbfirstname' => '', 'fbuserid' => '', 'fbtimezone' => '', 'userid' => '', 'token' => '', 'timezone' => '', 'application' => 'html', 'socialname' => '', 'bkrestaurant' => $bkrestaurant, 'data' => $data, 'bkpage' => $bkpage, 'bktitle' => $bktitle, 'bklangue' => $bklangue, 'bkparam' => $bkparam, 'bktracking' => $bktracking, 'brwsr_type' => $brwsr_type, 'bkextra' => $bkextra);


//section booking page 
if(isset($_REQUEST['p'])){
    $decocedData = base64_decode($_REQUEST['p']);
    parse_str($decocedData, $get_array);
    $bktracking = $get_array['bktracking'];
}

$bkgcustomcolor = "";
if (!empty($res->bkgcustomcolor) && preg_match("/website|facebook/i", $bktracking)) {
	$bkgcustomcolor = $res->bkgcustomcolor;
}

/**
 * if $_GET['type']="event" then show event booking form
 */
if (isset($_GET['type']) && $_GET['type'] == 'event') {
    $showEventBooking = 'true';
} else {
    $showEventBooking = 'false';
}


//	Madison
//	{ ’listarea’:’Selection of area,Working area,Light Verandah,Dark Verandah,Den,Dining Room,Bar,Anteroom,Pantry,Library’, ’listpurpose’:’Purpose of Visit,Business Meeting,Hang out/Drinks,Normal,Special Occasion,Private Event’ }
//


?>