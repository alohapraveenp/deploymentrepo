<?php
require 'conf/conf.init.inc.php';
require 'conf/conf.mysql.inc.php';

$status =$_REQUEST["state"];
$payment_type =$_REQUEST["payment_type"];



?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Secure Payment Form</title>
        <link rel="stylesheet" href="css/bootstrap-min.css">
        <link rel="stylesheet" href="css/bootstrap-formhelpers-min.css" media="screen">
        <link rel="stylesheet" href="css/bootstrapValidator-min.css"/>
        <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" />
        <link rel="stylesheet" href="css/bootstrap-side-notes.css" />
        <style type="text/css">
            .col-centered {
                display:inline-block;
                float:none;
                text-align:left;
                margin-right:-4px;
            }
            .row-centered {
                margin-left: 9px;
                margin-right: 9px;
            }
            .date-picker-form{
                max-width:49%;
                display:inline!important;
            }
            .form-control{
                border: 1px solid #ccc!important;
                border-radius: 2px;
            }
            .form{
                background: #fafafa none repeat scroll 0 0;
                border-radius: 5px;
                margin: 15px 3px 3px 3px;
                padding: 35px 3px;
            }
            .btn-success{
                 border-radius: 2px;
            }
        </style>
        <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script src="js/bootstrap-min.js"></script>
        <script src="js/bootstrap-formhelpers-min.js"></script>
        <script type="text/javascript" src="js/bootstrapValidator-min.js"></script>

    </head>
    <body>
        
        <form  action=""  method="POST" id="payment-form" class="form-horizontal form" style='max-width:500px; margin: auto'>
            <div class="row row-centered" >
                <div style="text-align: center">
                    <?php if ($status ==='confirmed') { ?>
                      <?php if($payment_type ==='carddetails') { ?>
                            <h2>Transaction completed</h2><br/>
                            <p>Your credit card information has been saved
                            and the transaction details have been sent by email.</p>
                            <p>You can now close this tab and return to restaurant website tab.</p>
                         <?php } else {?>
                            <p> Your payment transaction has been completed.</p>
                        <?php } ?>
                     <?php }  else  { ?>
                            <p>Your payment transaction has been CANCELLED.</p>
                             <p>If you wish to retry to provide credit card details, follow instructions sent by email.</p>
                      <?php } ?>
                    <!--<p>Your booking is confirmed.</p>-->
                    <p>Thank you</p>
                </div>
            </div>
        </form>
    </body>
</html>
