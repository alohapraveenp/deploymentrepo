<?php

require_once 'conf/conf.init.inc.php';
require_once 'conf/conf.mysql.inc.php';
require_once("lib/wpdo.inc.php");
require_once 'lib/class.payment_stripe.inc.php';
require_once 'lib/class.restaurant.inc.php';
require_once("lib/class.analytics.inc.php");
require_once("lib/class.booking.inc.php");
require_once("lib/class.event.inc.php");
require_once("lib/class.event_management.inc.php");

$booking = new WY_Booking();
$res = new WY_restaurant;
$event = new WY_Event();
$stripe = new WY_Payment_Stripe();


$logger = new WY_log("website");
$loguserid = (isset($_SESSION['user']['user_id'])) ? $_SESSION['user']['user_id'] : "0";
//if (isset($_POST['action'])) {
$orderId = $event->getUniqueCodeByEvent();
$restaurant_id = $_POST['restaurant_id'];
$url = $_POST['returnUrl'];
$event_id = $_POST['event_id'];
$amount = $_POST['amount'];
$email = $_POST['email'];
$token = $_POST['stripeToken'];
$controller = $_POST['controller'];
    if (isset($_POST['action'])) {
       if($_POST['action'] == 'private_event')
         $ref_id = $event_id ;
        else
        $ref_id = $event->saveEventBooking($event_id, $_POST['pax'], $_POST['amount'], $_POST['curency'], $_POST['DeliveryDate'], $restaurant_id, '', $_POST['first_name'], $_POST['last_name'], $_POST['email'], $_POST['phone'], $_POST['company'], $_POST['special_request'], 'pending', $orderId, $_POST['action']);
    }
    
    if (isset($ref_id) && $ref_id != '') {
        $result = $stripe->createEventPayment($restaurant_id, $event_id, $email, $orderId, $amount, $ref_id, $token, $_POST['action'], $_POST['bktracking']);
    }
    if (isset($_POST['action']) && $_POST['action'] == 'private_event') {
        $ref_id = $event_id . '?dspl_h=f&dspl_f=f';
        $event_management = new WY_EventManagement($restaurant_id);
        $status = (isset($result['status']) && $result['status'] == 'COMPLETED') ? 'confirmed' : $result['status'];
        if($status === 'confirmed'){
            $event_management->updatePaymentStatus($event_id, $email, $status);
        }
    } else {
        if (isset($_POST['bktracking']) && $_POST['bktracking'] == 'WEBSITE') {
            $ref_id = $orderId . '?dspl_h=f&dspl_f=f&bktracking=WEBSITE';
        }else{
             $ref_id = $orderId ;
        }
    }
    if (count($result) > 0) {
         $return_url = $url . $ref_id;
        if($controller === 'white_label'){
            $return_url =  "../../event_management/menu-selection.php?event_id=".$event_id;
             header("Location: $return_url");
            //echo "<script> window.location.href ='$return_url';</script>";
        }else{
            echo "<script> window.top.location.href ='$return_url';</script>"; 
        }
       
    
    }
//}
//    if (isset($_POST['stripeToken'])) {
//      
//        $paymentType='creditcard';
//    
//        $stripe = new WY_Payment_Stripe();
//
//        $amount =strval($_POST['amount']);
//     
//        $return_url = $_POST['return_url'].$_POST['ref_id']; //$_POST['$restaurant']
//        $restaurant_id=$_POST['restaurant_id'];
//        $cardnumber = trim($_POST['cardnumber']);
//
//        $result = $stripe->createEventPayment($restaurant_id,$_POST['event_id'],$cardnumber,$_POST['expmonth'], $_POST['expyear'], $_POST['cvv'],$_POST['email'],$_POST['cardholdername'],$_POST['orderId'],$amount,$_POST['ref_id']);
//     
//
//        if(count($result)>0){
//   
//    
//            echo "<script> window.top.location.href ='$return_url';</script>";
//        }
//        //
//    }
?>

