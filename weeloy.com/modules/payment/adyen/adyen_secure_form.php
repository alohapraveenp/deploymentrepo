<?php
    require_once 'conf/conf.init.inc.php';
    require_once 'conf/conf.mysql.inc.php';
    require_once("lib/wpdo.inc.php");
    require_once 'lib/class.payment_new.inc.php';
    require_once 'lib/class.restaurant.inc.php';
    require_once("lib/class.analytics.inc.php");
    require_once("lib/class.booking.inc.php");
    require_once('conf/conf.session.inc.php');
    date_default_timezone_set('UTC');
    $objDateTime = date('Y-m-d H:i:s');
    $date_gcalender = gmdate("Y-m-d\TH:m:s.s\Z", strtotime($objDateTime));  

    if (isset($_GET['action'])) {
        $confirmation = $_GET['confirmation'];
        $amount = $_GET['resBookingDeposit'];
    }

    if(isset($_POST['type']) && $_POST['type'] == 'postdata'){
        if(isset($_POST['adyen-encrypted-data'])){
            $booking = new WY_Booking();
            $booking->getBooking($_POST['confirmation']);

            $token = $_POST['adyen-encrypted-data']; 
            $params = array(
                'payment_type' => 'adyen',
                'reference_id' => $_POST['confirmation'],
                'restaurant'   => $booking->restaurant,
                'amount'       => $_POST['amount'],
                'token'        => $token,
                'mode'         => 'booking'
            );
            $payment = new WY_Payment_new($params);
            $returnObj = $payment->doDeposit();
            if(isset($returnObj)){
                $state = $returnObj['status'] == 'COMPLETED' ? 'confirmed'  : 'pending';
                if($state == 'pending'){
                    $url =  __BASE_URL__ ."/modules/booking/deposit/deposit_payment_method.php?action=bktrack_pendingpayment&bkemail=".$email.'&refid='.$ref_id;
                }else{
                    $url =  __BASE_URL__."/modules/booking/deposit/deposit_confirmation_confirmed.php?bkconfirmation=".$confirmation;
                }
                header("Location: ".$url);
            }

        } else{
            $url =  __ROOTDIR__ ."/modules/booking/deposit/deposit_payment_method.php?action=bktrack_pendingpayment&bkemail=".$booking->email.'&refid='.$confirmation;
            header("Location: ".$url);
        }  
    }
?>

<html lang="en">
    <head>
        <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Secure Payment Form</title>
        <link rel="stylesheet" href="../stripe/css/bootstrap-min.css">
        <link rel="stylesheet" href="../stripe/css/bootstrap-formhelpers-min.css" media="screen">
        <link rel="stylesheet" href="../stripe/css/bootstrapValidator-min.css"/>
        <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" />
        <link rel="stylesheet" href="../stripe/css/bootstrap-side-notes.css" />
        <style type="text/css">
            .col-centered {
                display:inline-block;
                float:none;
                text-align:left;
                margin-right:-4px;
            }
            .row-centered {
                margin-left: 9px;
                margin-right: 9px;
            }
            .date-picker-form{
                max-width:49%;
                display:inline!important;
            }
            .form-control{
                border: 1px solid #ccc!important;
                border-radius: 2px;
            }
            .form{
                background: #fafafa none repeat scroll 0 0;
                border-radius: 5px;
                margin: 15px 3px 3px 3px;
                padding: 35px 3px;
            }
            .btn-success{
                 border-radius: 2px;
            }
        </style>
        </head>
    <body>
        <form method="POST" id="admen-encrypted-form" action="" name='adyen-encrypted-form' class="form-horizontal form" style='max-width:500px; margin: auto'>
            <div class="row row-centered" >
                <div class="col-sm-12 ">
                    <noscript>
                        <div class="bs-callout bs-callout-danger">
                            <h4>JavaScript is not enabled!</h4>
                            <p>This payment form requires your browser to have JavaScript enabled. Please activate JavaScript and reload this page. Check <a href="http://enable-javascript.com" target="_blank">enable-javascript.com</a> for more informations.</p>
                        </div>
                    </noscript>

                    <div class="alert alert-danger" id="a_x200" style="display: none;"> <strong>Error!</strong> <span class="payment-errors"></span> </div>
                   
                    
                    <legend style="border:none;margin-bottom: 0px;"><img style="margin-right: 20px;width: 40px" src="../../../images/secured4.png">Credit card details</legend>
                    
                    <fieldset style="margin:15px 0px;border-top:1px solid #aaa;border-bottom:1px solid #aaa">
                        <label style="font-size:13px;margin-top:10px;">This form is powered by <a href="http://www.adyen.com" target="_blank">adyen.com</a>.</label>
                        
                        <label style="font-size:13px;margin-top:10px;">To guarantee the security, the restaurant staffs do not have access to credit card details.</label>
                    </fieldset> 
                    <legend>Card Details</legend>
                        <fieldset>
                        <div class="form-group">
                            <label class="col-sm-4 control-label" for="textinput">Card Number</label>
                            <div class="col-sm-8">
                                <input type="text" data-encrypted-name="number"  id='cardnumber' size="20" placeholder="Card Number" class="card-number form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label"  for="textinput">Card Holder's Name</label>
                            <div class="col-sm-8">
                              <input type="text" data-encrypted-name="holderName"  size="20" placeholder="Card Holder Name" class="card-holder-name form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label" for="textinput">Card Expiry Date</label>
                            <div class="col-sm-8">
                                <div class="form-inline">
                                    <input type="text" autocomplete="off" data-encrypted-name="expiryMonth" size="2" maxlength="2"/>
                                    <input type="text" autocomplete="off" data-encrypted-name="expiryYear" size="4" maxlength="4"/>
 
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label" for="textinput">CVV/CVV2</label>
                            <div class="col-sm-3">
                                 <input type="text" autocomplete="off" data-encrypted-name="cvc" size="4" maxlength="4" class="card-cvc form-control"/>
<!--                                <input data-encrypted-name="cvc" type="text" id="cvv" placeholder="CVV" size="4" maxlength="4" class="card-cvc form-control">-->
                            </div>
                        </div>
<!--                        <input type="text" autocomplete="off" data-encrypted-name="number" size="20"/>
                        <input type="text" autocomplete="off" data-encrypted-name="holderName" size="20"/>-->
<!--                        <input type="text" autocomplete="off" data-encrypted-name="expiryMonth" size="2" maxlength="2"/>
                        <input type="text" autocomplete="off" data-encrypted-name="expiryYear" size="4" maxlength="4"/>-->
<!--                        <input type="text" autocomplete="off" data-encrypted-name="cvc" size="4" maxlength="4"/>-->
                        <input type="hidden" value="<?php echo $date_gcalender ?>" data-encrypted-name="generationtime"/>
                        <input type="hidden" value="postdata" name="type"/>
                        <input type="hidden" name="confirmation" value="<?php echo $confirmation ?>" class="form-control" />
                        <input type="hidden" name="amount" value="<?php echo $amount ?>" class="form-control" />
                        <div class="control-group">
                            <div class="controls">
                                <center>
                                    <button class="btn btn-success submit-button" type="submit">Submit</button>
                                </center>
                            </div>
                        </div>
                        </fieldset>
<!--                       <input type="submit" value="Create payment" />-->
               
                </div>
         
            </div>
        </form>
             
    
    </html>


<!--<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Secure Payment Form</title>
        <link rel="stylesheet" href="../stripe/css/bootstrap-min.css">
        <link rel="stylesheet" href="../stripe/css/bootstrap-formhelpers-min.css" media="screen">
        <link rel="stylesheet" href="../stripe/css/bootstrapValidator-min.css"/>
        <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" />
        <link rel="stylesheet" href="../stripe/css/bootstrap-side-notes.css" />
    
        <style type="text/css">
            .col-centered {
                display:inline-block;
                float:none;
                text-align:left;
                margin-right:-4px;
            }
            .row-centered {
                margin-left: 9px;
                margin-right: 9px;
            }
            .date-picker-form{
                max-width:49%;
                display:inline!important;
            }
            .form-control{
                border: 1px solid #ccc!important;
                border-radius: 2px;
            }
            .form{
                background: #fafafa none repeat scroll 0 0;
                border-radius: 5px;
                margin: 15px 3px 3px 3px;
                padding: 35px 3px;
            }
            .btn-success{
                 border-radius: 2px;
            }
        </style>
         
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script src="../stripe/js/bootstrap-min.js"></script>

        <script type="text/javascript" src="../stripe/js/bootstrapValidator-min.js"></script>
    </head>
    <body>
     
        <form  action=""  method="POST" id="admen-encrypted-form"  action="" name ='adyen-encrypted-form' class="form-horizontal form" style='max-width:500px; margin: auto'>
            <div class="row row-centered" >
                <div class="col-sm-12 ">
                    <noscript>
                    <div class="bs-callout bs-callout-danger">
                        <h4>JavaScript is not enabled!</h4>
                        <p>This payment form requires your browser to have JavaScript enabled. Please activate JavaScript and reload this page. Check <a href="http://enable-javascript.com" target="_blank">enable-javascript.com</a> for more informations.</p>
                    </div>
                    </noscript>

                    <div class="alert alert-danger" id="a_x200" style="display: none;"> <strong>Error!</strong> <span class="payment-errors"></span> </div>
                   
                    
                    <legend style="border:none;margin-bottom: 0px;"><img style="margin-right: 20px;width: 40px" src="../../../images/secured4.png">Credit card details</legend>
                    
                    <fieldset style="margin:15px 0px;border-top:1px solid #aaa;border-bottom:1px solid #aaa">
                        <label style="font-size:13px;margin-top:10px;">This form is powered by <a href="http://www.adyen.com" target="_blank">adyen.com</a>.</label>
                        
                        <label style="font-size:13px;margin-top:10px;">To guarantee the security, the restaurant staffs do not have access to credit card details.</label>
                    </fieldset> 
                    
                    
                  <fieldset>
                            <div class="form-group">
                            <label class="col-sm-4 control-label" for="textinput">Email</label>
                            <div class="col-sm-8">
                                <input type="text" name="email" maxlength="65" placeholder="Email" class="email form-control" value='<?php echo $email ?>'>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <legend>Card Details</legend>

                        <div class="form-group">
                            <label class="col-sm-4 control-label"  for="textinput">Card Holder's Name</label>
                            <div class="col-sm-8">
                              <input type="text" data-encrypted-name="holderName"  size="70" placeholder="Card Holder Name" class="card-holder-name form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label" for="textinput">Card Number</label>
                            <div class="col-sm-8">
                                <input type="text" data-encrypted-name="number"  id='cardnumber' size="19" placeholder="Card Number" class="card-number form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label" for="textinput">Card Expiry Date</label>
                            <div class="col-sm-8">
                                <div class="form-inline">
                                    <input type="text" autocomplete="off" data-encrypted-name="expiryMonth" placeholder="Expiry Month" size="2" maxlength="2"/>
                                    <input type="text" autocomplete="off" data-encrypted-name="expiryYear" placeholder="Expiry Year" size="4" maxlength="4"/>
 
                                </div>
                            </div>
                        </div>

                         CVV 
                        <div class="form-group">
                            <label class="col-sm-4 control-label" for="textinput">CVV/CVV2</label>
                            <div class="col-sm-3">
                                <input data-encrypted-name="cvc" type="text" id="cvv" placeholder="CVV" size="4" maxlength="4" class="card-cvc form-control">
                            </div>
                        </div>
    
                        <button class="btn btn-success submit-button" type="submit">Submit</button>
                        <div class="control-group">
                            <div class="controls">
                                <center>
                                    <button class="btn btn-success submit-button" type="submit">Submit</button>
                                </center>
                            </div>
                        </div>
                    </fieldset>
                    
                    <fieldset style="margin-top:15px;border-top:1px solid #aaa">
                    </fieldset>
                    <?php if($action !== 'formdata') { ?>
                    <fieldset>
                            <input type="hidden" value="generate-this-server-side" data-encrypted-name="generationtime"/>
                            <input type="hidden" name="resBookingDeposit" value="<?php echo $resBookingDeposit ?>" class="form-control" />
                            <input type="hidden" name="resCurrency" value="<?php echo $resCurrency ?>" class="form-control" />
                            <input type="hidden" name="restaurant" value="<?php echo $restaurant ?>" class="form-control" />
                            <input type="hidden" name="confirmation" value="<?php echo $confirmation ?>" class="form-control" />
                            <input type="hidden" name="bkemail" value="<?php echo $email ?>" class="form-control" />
                            <input type="hidden" name="tracking" value="<?php echo $tracking ?>"/>
                            <input type="hidden" name="city" value="<?php echo $city ?>"/>
                            <input type="hidden" name="title" value="<?php echo $title ?>"/>
                            <input type="hidden" name="payment_type" value="<?php echo $paymentType ?>"/>
                            <input id ="bkdate" type="hidden" name="bkdate" value="<?php echo $bkdate ?>"/>
                   </fieldset>
                   <?php } ?>  
                </div>
            </div>
        </form>
    </body>
</html>-->
<script type="text/javascript" src="https://test.adyen.com/hpp/cse/js/7614792607978406.shtml"></script>
<script type="text/javascript">
    


// The form element to encrypt.
//var form    = document.getElementById('adyen-encrypted-form');
//// See adyen.encrypt.simple.html for details on the options to use
//var options = {}; 
//
//// Create the form.
//// Note that the method is on the Adyen object, not the adyen.encrypt object.
//var  data = adyen.createEncryptedForm( form, options); 
//console.log(data);


//var form    = document.getElementById('adyen-encrypted-form');
 var options = {}, 
     oForm = document.forms["adyen-encrypted-form"];
 var encryptedForm = adyen.createEncryptedForm( oForm, options); 

</script>
