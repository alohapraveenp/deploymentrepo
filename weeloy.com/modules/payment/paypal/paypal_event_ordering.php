<?php

require_once 'lib/class.payment_paypal.inc.php';
require_once 'lib/class.payment.inc.php';
require_once("lib/class.event_management.inc.php");
    
    if(empty($_REQUEST["refid"])) {
	printf("Invalid Payment");
	exit;	
    }
    $payment = new WY_Payment();
    $event_management = new WY_EventManagement();
    $response = $payment->getPaymentDetails($_REQUEST["refid"],'object_id');
    if(isset($response)){
        $pay_object = new WY_Payment_Paypal($response['restaurant'], $response['amount']);
        $paymentStatus = $pay_object->getPaymentResponse($response['payment_id']);
      
        if($paymentStatus == 'COMPLETED'){
           $payment->updatePaypalStatus($paymentStatus,$_REQUEST["refid"]); 
                $data = array();
                $data['event_id'] = $_REQUEST["refid"] ;
                $data['type'] = 'creditcard';
                $data['card_id'] = '';
                $data['status'] = 'confirmed';
                $data['amount'] = $response['amount'];
                $updatePaymentDetails = $event_management::saveEvPaymentDetails($data);
                $args = $event_management::getEventProjet(array('event_id' => $_REQUEST["refid"]));
                $event_management::notifyEventMangement($args, 'event_management');
                header("Location: ../../event_management/menu-selection.php?event_id=".$_REQUEST["refid"]);
        }else{
        
             header("Location: ../../event_management/menu-selection.php?event_id=".$_REQUEST["refid"]);
        }
        
       
    }

    ?>