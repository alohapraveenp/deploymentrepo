<?php
require_once("lib/class.event_management.inc.php");
require_once("conf/conf.init.inc.php");

if (empty($_REQUEST["event_id"])) {
    printf("Invalid request. Exiting");
    exit;
}
$event_id = $_REQUEST["event_id"];
$ev_object = WY_EventManagement::getEventProjet(array('event_id' => $event_id));

$restaurant = $ev_object['restaurant'];

$BASE_URL = __BASE_URL__;
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="author" content="weeloy. https://www.weeloy.com"/>
        <meta name="copyright" content="weeloy. https://www.weeloy.com"/>  
        <meta http-equiv='expires' content='mon, 1 jan 1998 05:00:00 gmt'/>
        <meta http-equiv='pragma' content='no-cache'/>
        <meta http-equiv='pragma' content='cache-control: max-age=0'/>
        <meta http-equiv='last-modified' content='mon, 1 jan 1998 05:00:00 gmt'/>
        <meta http-equiv='cache-control' content='no-cache, must-revalidate'/>
        <meta name='robots' content='noindex, nofollow'/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <meta name="HandheldFriendly" content="true" />
        <title> Event Management</title>

        <link href="../../client/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../../client/bower_components/bootstrap/dist/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
        <link href="../../client/bower_components/fontawesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

        <link href="../../modules/booking/assets/css/css/bootstrap-select.css" rel="stylesheet" />
        <link href="../../modules/booking/assets/css/css/bootstrap-social.css" rel="stylesheet" />

        <script type='text/javascript' src="../../client/bower_components/angular/angular.min.js"></script>
        <script type="text/javascript" src="../../client/bower_components/jquery/dist/jquery.min.js"></script>

        <script type="text/javascript" src="../../js/jquery-ui.js"></script>
        <script type="text/javascript" src="../../client/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="../../js/bootstrap-select.js"></script>
        <script type='text/javascript' src="../../js/ui-bootstrap-tpls-0.12.1.min.js"></script>
        <script type="text/javascript" src="../../js/ngStorage.min.js"></script>
        <script type="text/javascript" src="../../js/mylocal.js"></script>
        <script type="text/javascript" src="../../client/app/models/Base.js"></script>
        <script type="text/javascript" src="../../client/app/models/Restaurant.js"></script>
        <script type="text/javascript" src="../../client/app/models/EventManagement.js"></script>

        <style>
            body{
                font-family: arial;
                margin: 8px;
                background-color: #ffffff;
                color: #333333;
                line-height: unset;
            }
            .printarea{
                height: 1100px;
                width: 790px;
            }
            #header{
                width: 100%;
                text-align: center;
            }  
            #title{
                width: 100%;
                text-align: center;
                margin-bottom: 10px;
                font-size: 16px;
            } 
            #footer{
                bottom: 0px;
                /*position: absolute;*/
                font-size: 11px;
                width: 100%;
                text-align: center;
            }  
            .content-staff{
                height: 1000px;
                font-size: 12px;
            }
            .content-staff table {
                width : 100%;
                height: 98%;
                padding: 0;
                margin: 0;
                border-collapse:collapse;
            }
            .content-staff tr {
                padding: 0;
                margin: 0;

            }
            .content-staff th {
                background-color: #dddddd;
                -webkit-print-color-adjust: exact; 
                height: 20px;
                text-align: center;

            }
            .content-staff td {
                vertical-align: top;
                vertical-align: top;
                padding: 0;
                margin: 0;
                overflow: hidden;
            }
            .add-border-all{
                border-top: 1px solid black; 
                border-bottom: 1px solid black;  
                border-left: 1px solid black; 
                border-right: 1px solid black; 
            }
            .add-border-top{
                border-top: 1px solid black; 
            }
            .add-border-bottom{
                border-bottom: 1px solid black;  
            }
            .add-border-left{
                border-left: 1px solid black; 
            }
            .add-border-right{
                border-right: 1px solid black; 
            }
            .padding-cell{
                padding:5px;
            }
            .content-staff > table tr td table tr td{
                padding:5px;
            }
            .content-small-padding{
                padding:1px 3px!important;
                white-space: nowrap;
            }
            .content-valign-middle{
                vertical-align: middle!important;
            }
            label{
                font-weight: normal!important;
            }
            .payment-section {
                margin-left:40px;
            }
            .header-img{
                width :auto;
                max-height: 39px;

            }


            @media screen{
                #header{
                    display: none;
                }
                #footer{
                    display: none;
                }
            }
            @media print {
                #header{
                    display: block;
                    margin-bottom: 20px;
                }
                #footer{
                    bottom: 0px;
                    position: absolute;
                    font-size: 11px;
                    width: 100%;
                    text-align: center;
                }
            }
        </style>
        <script> var app = angular.module("myApp", ['ui.bootstrap', 'ngLocale', 'ngStorage']);</script> 
        <body ng-app="myApp">
            <section ng-controller='EventManagementController' ng-init="moduleName = 'eventmsanagement';">
                <div class="printarea">
                    <div>
                        <div class="" id="header">
                            <img class="header-img" ng-src="{{restaurant.getLogoImage()}}"  getLogoImage/>
                        </div>
                        <div class="" id="title">
                            <div class="">Event Order</div>
                        </div>
                        <div class="content-staff">
                            <table style="height:100%">
                                <tr style="height:10%">
                                    <td colspan=2>
                                        <table width='100%'>
                                            <tr>
                                                <td class='add-border-all padding-cell' width="50%">
                                                    <table>
                                                        <tr>
                                                            <td class='content-small-padding'  style="width:30%">Client:</td>
                                                            <td class='content-small-padding' ><label ng-bind="EventObject.fullname"></label></td>
                                                        </tr>
                                                        <tr>
                                                            <td class='content-small-padding'  style="width:30%">Phone:</td>
                                                            <td class='content-small-padding' ><label ng-bind="EventObject.phone"></label></td>
                                                        </tr>
                                                        <tr>
                                                            <td class='content-small-padding'  style="width:30%">Email:</td>
                                                            <td class='content-small-padding' ><label ng-bind="EventObject.email"></label></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td class='add-border-all padding-cell' width="50%">
                                                    <table>
                                                        <tr>
                                                            <td class='content-small-padding' style="width:30%">Date:</td>
                                                            <td class='content-small-padding' ><label ng-bind="EventObject.rdate"></label></td>
                                                        </tr>
                                                        <tr>
                                                            <td class='content-small-padding' style="width:30%">Event id#:</td>
                                                            <td  class='content-small-padding' ng-bind="EventObject.more.beo"></td>

                                                        </tr>
                                                        <tr>
                                                            <td class='content-small-padding' style="width:30%">Booked by:</td>
                                                            <td class='content-small-padding'  ng-bind="EventObject.booked_by"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class='content-small-padding' style="width:30%">Phone:</td>
                                                            <td class='content-small-padding' ng-bind="restaurant.getPhone()" ></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr  style="height:10%">
                                    <td colspan=2>
                                        <table >
                                            <tr >
                                                <th class='add-border-all '>Date</th>
                                                <th class='add-border-all '>Time</th> 
                                                <th class='add-border-all '>Function</th>
                                                <th class='add-border-all '>Venue</th>
                                                <th class='add-border-all '>Guaranteed</th> 
                                                <th class='add-border-all '>Expected</th>
                                            </tr>
                                       
                                            <tr ng-repeat="timing_item in EventObject.more.timing">
                                                <td class='add-border-all content-small-padding'  style="width:20%;text-align:center; vertical-align: middle;" ng-bind="EventObject.rdate"></td>
                                                <td class='add-border-all content-small-padding'  style="width:10%;" ng-bind="timing_item.name"></td> 
                                                <td class='add-border-all content-small-padding'  style="width:40%;" ng-bind="timing_item.function"></td>
                                                <td class='add-border-all content-small-padding'  style="width:10%;text-align:center; vertical-align: middle;" ng-bind="timing_item.venue"></td> 
                                                <td class='add-border-all content-small-padding'  style="width:10%;text-align:center; vertical-align: middle;" ng-bind="timing_item.guarented"></td>
                                                <td class='add-border-all content-small-padding'  style="width:10%;text-align:center; vertical-align: middle;" ng-bind="timing_item.expected"></td>
                                                <tr/>
                                        </table>
                                    </td>
                                </tr>
                                <tr  style="height:70%"  >
                                    <td width="50%" >
                                        <table style="height: 100%;" class="add-border-all ">
                                            <tr>
                                                <th  class='add-border-bottom' style="padding:0px" >
                                                    Food & beverage
                                                </th> 
                                            </tr>
                                            <tr>
                                                <td style="">

                                                    <div class="" ng-class="" ng-repeat="menu in menus">
                                                        <div class="col-xs-12 fixed">
                                                            <h4 ng-if='menu.qte > 0' class="fixed" ng-bind="menu.notes"></h4>

                                                            <div class="row" ng-class="" ng-repeat="item in menu.items" >
                                                                <div  ng-if="menu.menuID === item.displayID" class="fixed" style='display: table;'>
                                                                    <div class="category-item-info" style='display: table-cell' >
                                                                        <div class="location-icon">
                                                                            <div class="location-text" style="margin-top: 5px;" ng-if="item.qte > 0 || menu.setmeal">
                                                                                <p class="col-xs-9 fixed" style='font-weight: bold;margin-top:15px;' ng-if="item.item_description == 'section' || item.item_description == 'section_or'"><span>{{item.item_title}}</span></p>
                                                                                <p class="col-xs-9 fixed " style='font-size: 12px;' ng-if="item.item_description != 'section' && item.item_description != 'section_or'"><span>{{item.item_title}}</span></p>
                                                                                <label class="col-xs-3 fixed" type="text" readonly ng-value=" {{item.qte|| 0}}" ng-if="item.item_description != 'section' && item.item_description != 'section_or'" ><span ng-if="item.qte > 0">{{item.qte}} portions</span><span ng-if="item.qte < 1 && menu.setmeal">included</span></label>
                                                                            </div>
                                                                            <div ng-if="item.section === 'section_or' && item.item_description != 'section' && item.item_description != 'section_or'" class="location-text" style="margin-top: 5px;">
                                                                                <p class="col-xs-12 fixed" >---- OR ----</span> </p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                </td> 
                                            </tr>
                                        </table>
                                    </td>
                                    <td  width="50%" >
                                        <table style="height:10%" class="add-border-top add-border-right">
                                            <tr>
                                                <th class='add-border-bottom' style="padding:0px" >
                                                    Set up
                                                </th> 
                                            </tr>
                                            <tr>
                                                <td>
                                                    <span ng-bind="EventObject.setup"></span>
                                                </td> 
                                            </tr>
                                        </table>
                                    
                                        <table style="height:45%" class="add-border-top add-border-right">
                                            <tr>
                                                <th class='add-border-top add-border-bottom' style="padding:0px" >
                                                    Billing
                                                </th> 
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div style="white-space:pre-wrap;">
                                                        <label ng-bind="EventObject.billing"></label>
                                                    </div>
                                                    <div style="white-space:pre-wrap;">
                                                        <label ng-if='EventObject.globalspltnc' ng-bind="EventObject.globalspltnc"></label>
                                                    </div>

                                                </td> 
                                            </tr>
                                        </table>
                                        <table style="height:45%" class="add-border-top add-border-right ">
                                            <tr>
                                                <th class='add-border-top add-border-bottom' style="padding:0px" >
                                                    Payment
                                                </th> 
                                            </tr>
                      
                                            <tr class="add-border-bottom "> 
                                                <td>
                                                    <h5 ng-if='EventObject.tnc_cancel' class='text-center' >Tncs</h5>
                                                    <div style="white-space:pre-wrap;">
                                                        <label ng-bind="EventObject.tnc_cancel"></label>
                                                    </div>
                                                     <div style="white-space:pre-wrap;">
                                                        <label ng-if='EventObject.globaltnc' ng-bind="EventObject.globaltnc"></label>
                                                    </div>
                                                    <h5  style='margin-left :60px;'>Transaction Details</h5>
                                                    <div class='payment-section' >
                                                        <p  ng-if='totalamount > 0'>Total Amount : SGD <span ng-bind="totalamount | number:2"></span></p>
                                                        <p  ng-if="EventObject.payment_details.type && EventObject.more.deposit !== 'no deposit'">Payment Type : <span ng-bind="EventObject.payment_details.type"></span></p>
                                                        <p  ng-if="EventObject.payment_details.payment_id && EventObject.more.deposit !== 'no deposit'">Transaction Id : <span ng-bind="EventObject.payment_details.payment_id"></span></p>
                                                        <p  ng-if="EventObject.payment_details.deposit_amount && EventObject.more.deposit !== 'no deposit'&& status ==='confirmed'">Deposited Amount : SGD <span ng-bind="EventObject.payment_details.deposit_amount | number:2"></span></p>
                                                    </div>
                                                </td> 
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr style="height:5%">
                                    <td colspan=2>
                                        <table width='100%'>
                                            <tr >
                                                <td colspan=2 class='content-valign-middle' >By submitting the credit card details, the client accepted terms and conditions</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
<!--                                <tr style="height:5%">
                                    <td colspan=2>
                                        <table width='100%'>
                                            <tr >
                                                <td class='content-valign-middle' width="50%"></td>
                                                                                    <td class='content-valign-middle' width="50%">Identity Card/Passport No: ........................................................</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>-->
                            </table>
                        </div>
<!--                        <div class="" id="footer">Flower Dome, Garden By The Bay 18 Marina Gardens Drive #01-09, Singapore, 018953 +65 6604 9988 www.pollen.com </div>-->
                    </div>
                </div>
            </section>
            <script>

                        app.controller('EventManagementController', function ($scope, $http) {
                            var script = document.createElement('script');
                            script.src = '//checkout.stripe.com/checkout.js';
                            document.body.appendChild(script);
                            script.src = '//js.stripe.com/v2/';

                            document.body.appendChild(script);
                            var RestaurantID = "<?php echo $restaurant ?>";
                            var event_id = "<?php echo $event_id ?>";
                            var BASE_URL = "<?php echo $BASE_URL ?>";
                            $scope.selectioncomplete = false;
                            $scope.menus = [];
                            $scope.items = [];
                            $scope.counters = [];
                            $scope.tncsaccepted = false;
                            $scope.mysection = '';
                            $scope.swiftcode = '';
                            $scope.paytype = 'manual';
                            $scope.isRequiredcc = false;
                            $scope.depositamount = 0;
                            $scope.pre_totalamount = 0;
                            $scope.payable_amount = 0;
                            $scope.pre_depositamount = 0;
                            $scope.amount = 0;
                            $scope.multipledata = ['media', 'timing', 'additional', 'payment'];
                            //$scope.payment_method = ['creditcard', 'banktransfer', 'manual','nodeposit'];
                            $scope.payment_method = [{'creditcard': false, 'banktransfer': false, 'manual': false, 'nodeposit': false}];
                            $scope.multiplemodel = {
                                media: {name: ''},
                                payment: {name: ''},
                                additional: {name: ''},
                                timing: {name: '', funct: '', venue: '', garantee: '', expected: ''}
                            };

                            $scope.onlyNumbers = /^\d+$/;

                            $scope.dietaries = [
                                {'title': 'Vegetarian'},
                                {'title': 'Vegetarian, no egg'},
                                {'title': 'Gluten Free'},
                                {'title': 'No raw food'},
                                {'title': 'No Crustaceans'}
                            ];

                            $scope.totalamount = 0;
                            $scope.minimumorder = 0;
                            //                $scope.EventObject = EventObject;
                            var API_URL = '../../api/restaurantfullinfo/' + RestaurantID;
                            $http.get(API_URL, {
                            }).then(function (result) {
                                if (result.data.status === 1) {
                                    var restaurant = new Restaurant(result.data.data.restaurantinfo);
                                    $scope.restaurant = result.data.data.restaurantinfo;
                                    $scope.logo = restaurant.getLogoImage();

                                }
                            });

                            if (event_id) {
                                $scope.event_id = event_id;
                                var API_URL = '../../api/v2/event/management/' + $scope.event_id + '/';
                                $http.get(API_URL).then(function (result) {

                                    $scope.status = result.data.data.event.status;
                                    var ev = result.data.data.event;
                                    $scope.status = result.data.data.event.status;
                                    $scope.amount = parseInt(result.data.data.event.total_amount);
                                    var amount = parseInt(result.data.data.event.total_amount);
                                    $scope.pre_totalamount = result.data.data.event.total_amount;
                                    $scope.totalamount = result.data.data.event.amount;

                                    $scope.multipledata.map(function (ll) {
                                        if (!ev.more[ll] || ev.more[ll] instanceof Array === false)
                                            ev.more[ll] = [];
                                        else
                                            ev.more[ll].map(function (pp) {
                                                pp.name = pp.name.replace(/\|\|\|/g, '\n');
                                            });
                                    });
                                    ev.fullname = ev.title +" " +ev.firstname + " " + ev.lastname ;

                                    $scope.EventObject = new EventManagement(ev);
                                    console.log("OBJECT " +JSON.stringify($scope.EventObject));
                                  


                                    var evdate = $scope.EventObject.rdate.split('-');
                                    $scope.EventObject.rdate = evdate[2] + "-" + evdate[1] + "-" + evdate[0];
                                    if ($scope.EventObject.more) {
                                        $scope.EventObject.more = JSON.parse($scope.EventObject.more.replace(/’/g, "\""));
                                        $scope.minimumorder = $scope.EventObject.more.minimumprice;
                                        if ($scope.EventObject.more.deposit != 'nodeposit') {
                                            $scope.EventObject.more.deposit = parseInt($scope.EventObject.more.deposit);
                                        }

                                    }

                                    //$scope.getEvModifyDetails();

                                    if ($scope.EventObject.payment_details) {
                                        $scope.EventObject.payment_details = JSON.parse($scope.EventObject.payment_details.replace(/’/g, "\""));
                                        $scope.depositamount = $scope.EventObject.payment_details.deposit_amount;
                                    }
                                    if ($scope.EventObject.payment_method) {
                                        $scope.EventObject.payment_method = JSON.parse($scope.EventObject.payment_method.replace(/’/g, "\""));
                                        var evp = $scope.EventObject.payment_method;
                                        if (evp.nodeposit && evp.nodeposit === true) {
                                            $scope.isRequiredcc = false;
                                            $scope.paytype = 'waived';
                                        }
                                        else if (evp.creditcard === true && evp.nodeposit === false) {
                                            $scope.isRequiredcc = true;
                                        } else if (evp.creditcard === true && evp.banktransfer === true && evp.manual === true) {
                                            $scope.isRequiredcc = false;
                                        } else if (evp.creditcard === true && evp.banktransfer === true) {
                                            $scope.isRequiredcc = true;
                                        }
                                        else if (evp.manual === true && evp.banktransfer === true) {
                                            $scope.isRequiredcc = false;

                                        }
                                        else if (evp.creditcard === true || evp.manual === true) {
                                            $scope.isRequiredcc = false;
                                        }

                                        else {
                                            $scope.isRequiredcc = true;
                                        }
                                    }
                                    if ($scope.depositamount > 0) {
                                        $scope.isRequiredcc = false;
                                        evp.creditcard = false;
                                    }
                                    console.log("SECOND   " + $scope.totalamount + "AMOUNT " + $scope.amount);

                                });

                            }
                            if (event_id) {
                                //console.log("THIRD" + $scope.totalamount + "AMOUNT " + $scope.amount );

                                $scope.event_id = event_id;
                                var API_URL = '../../api/v2/event/management/menu/' + $scope.event_id + '/';

                                $http.get(API_URL).then(function (result) {

                                    $scope.menus = result.data.data.menu;

                                    $scope.menus.forEach(function (item, index) {
                                        var section = '';
                                        item.items.forEach(function (item2, index2) {
                                            if (item2.item_description === 'section_or' || item2.item_description === 'section') {
                                                section = item2.item_description;
                                            } else {
                                                $scope.menus[index].items[index2].section = section;
                                                if (section === 'section_or' && (($scope.menus[index].items[index2 + 1] && $scope.menus[index].items[index2 + 1].item_description === 'section') || !$scope.menus[index].items[index2 + 1])) {
                                                    $scope.menus[index].items[index2].section = 'section_or_nd';
                                                }
                                            }
                                            item2.vegetarian = (item2.extraflag & 8) == 8;
                                        });
                                        $scope.menus[index].food = (item.typeflag & 1) == 1;
                                        $scope.menus[index].drink = (item.typeflag & 2) == 2;
                                        $scope.menus[index].expend = (item.typeflag & 4) == 4;
                                        $scope.menus[index].setmeal = (item.typeflag & 8) == 8;
                                        $scope.menus[index].option = (item.typeflag & 16) == 16;
                                    });
                                    //$scope.setQte();
                                    //console.log("AFTERR TOTLA Event  " + $scope.totalamount +"AMOUNT " + $scope.amount );

                                });


                            }


                            // make a test on status
                            var API_URL = '../../api/payment/stripe/credentials/' + RestaurantID;
                            $http.get(API_URL).then(function (response) {


                                if (response.data.status === 1) {
                                    $scope.publishable_key = response.data.data.publishable_key;
                                }
                            });

                            $scope.logIt = function (value) {
                                $scope.swiftcode = value;
                                $scope.paytype = value;
                            };

                            $scope.status = '';

                            // Close Checkout on page navigation:
                            $(window).on('popstate', function () {
                                handler.close();
                            });


                            $scope.selectItem = function (item, evt) {
                                if ($scope.items.indexOf(item) < 0) {
                                    $scope.items.push(item);

                                    if (typeof $scope.counters[item.displayID] === 'undefined') {
                                        $scope.counters[item.displayID] = 1;
                                    } else {
                                        $scope.counters[item.displayID] = $scope.counters[item.displayID] + 1;
                                    }

                                    $scope.checkContinue();
                                }

                            };


                            $scope.setQte = function (item, evt, type) {
                                $scope.totalamount = 0;
                                $scope.menus.forEach(function (menu, key) {
                                    var sum = 0;
                                    var first_item = true;
                                    menu.totalprice = 0;
                                    //menu.qte = 0;
                                    if (menu.items) {
                                        menu.items.forEach(function (item, key) {
                                            item.qte = parseInt(item.qte);
                                            if (!item.qte) {
                                                item.qte = 0;
                                            }
                                            sum = sum + parseInt(item.qte);

                                            if (item.price) {
                                                item.totalprice = parseInt(item.price) * parseInt(item.qte);
                                                menu.totalprice = parseInt(menu.totalprice) + parseInt(item.totalprice);
                                            }
                                            if (first_item) {
                                                menu.qte = sum;
                                            }
                                            if (item.item_description === 'section_or') {
                                                if (menu.qte < sum) {
                                                    menu.qte = sum;
                                                    console.log("MENU Q TE " + menu.qte);
                                                }
                                                sum = 0;
                                                if (first_item) {
                                                    first_item = false;
                                                }
                                            }
                                        });
                                    }
                                    if (menu.qte < sum) {
                                        menu.qte = sum;
                                    }
                                    if (menu.setmeal) {

                                        if (menu.qte === 0) {
                                            menu.totalprice = menu.price * $scope.EventObject.pax;
                                            menu.qte = $scope.EventObject.pax;
                                        } else {
                                            menu.totalprice = menu.price * menu.qte;
                                        }

                                    }
                                    if (menu.totalprice) {

                                        $scope.totalamount = parseInt(menu.totalprice) + parseInt($scope.totalamount);
                                    }
                                });

                                if ($scope.items.indexOf(item) < 0) {
                                    $scope.items.push(item);
                                }

                                $scope.items = $scope.menus;
                            };



                            $scope.checkContinue = function () {

                                $scope.selectioncomplete = true;
                                $scope.menus.forEach(function (menu, key) {

                                    if ($scope.selectioncomplete && ($scope.counters[menu.menuID] !== parseInt(menu.item_limit))) {
                                        $scope.selectioncomplete = false;
                                    }
                                });
                            };



                            $scope.saveEventMenu = function (status, EventObject) {
                                var event_id = '';
                                var selected_items = [];
                                var amount = $scope.totalamount, service_charge = 0, gst = 0;
                                if ($scope.totalamount !== 0) {
                                    amount = $scope.totalamount * 1.10 * 1.07;
                                    $scope.amount = amount;
                                    service_charge = $scope.totalamount * 0.10;
                                    gst = $scope.totalamount * 0.07;
                                }
                                if ($scope.pre_totalamount > 0) {
                                    if (parseInt($scope.pre_totalamount) >= parseInt(amount)) {
                                        $scope.isRequiredcc = false;
                                        $scope.EventObject.payment_method.creditcard = false;
                                        $scope.EventObject.payment_method.banktransfer = false;

                                    } else {
                                        $scope.payable_amount = parseInt(amount) - parseInt($scope.pre_totalamount);
                                    }

                                }

                                $scope.items.forEach(function (value, key) {
                                    event_id = value.eventID;
                                    var data = {"id": value.menuItemID, "qte": value.qte};
                                    selected_items.push(data);

                                });
                                var API_URL = '../../api/v2/event/management/menu/' + event_id + '/save/';

                                $http.post(API_URL, {
                                    items: selected_items,
                                    status: status
                                }).then(function (result) {

                                });

                                $http.post("../../api/v2/event/management/updateprice/",
                                        {
                                            'restaurant': RestaurantID,
                                            'event_id': $scope.EventObject.eventID,
                                            'total_amount': $scope.totalamount,
                                            'amount': amount,
                                            'gst': gst,
                                            'service_charge': service_charge,
                                            'status': 'menu_selected',
                                            'email': $scope.EventObject.email,
                                        }).then(function (response) {
                                    return response.data.data;
                                });
                                //API.eventmanagement.updatePrice(RestaurantID,$scope.EventObject.eventID,$scope.EventObject.email,$scope.totalamount,gst,service_charge,amount,'menu_selected').then(function (result) {});
                                //$scope.getEvModifyDetails();
                                $scope.status = 'menu_selected';
                            };
                            $scope.notifySaveMenuSelection = function () {
                                $scope.EventObject.notify_type = 'event_menu_selected';
                                var API_URL = '../../api/v2/event/management/menu/notify/';
                                $http.post(API_URL, {
                                    items: $scope.EventObject,
                                }).then(function (result) {

                                });
                            };

                            //payment creation
                            $scope.submitCreditCardDetails = function () {
                                //$scope.setQte();
                                var amount = $scope.totalamount * 1.10 * 1.07,
                                        mode = $("input:radio[name=optionsPaymentRadios]:checked").val();

                                if ($scope.payable_amount > 0) {
                                    amount = $scope.payable_amount;
                                }
                                if ($scope.EventObject.more.deposit != 'nodeposit') {
                                    amount = amount * ($scope.EventObject.more.deposit / 100);
                                }

                                amount = Math.round(amount);
                                var data = $scope.EventObject;

                                data.restaurant_id = RestaurantID;
                                data.event_id = data.eventID;
                                data.amount = amount;
                                data.curency = 'SGD';
                                data.action = 'private_event';
                                data.controller = 'white_label';
                                data.returnUrl = '../event-management/menu-selection.php?event_id=';

                                if (mode === 'creditcard') {

                                    var handler = StripeCheckout.configure({
                                        key: $scope.publishable_key, //'pk_test_aG4RNDkBJI81YsT2ljQGVnuW',
                                        token: function (token) {
                                            data.stripeToken = token.id;
                                            var form = document.createElement('form');
                                            form.action = '../payment/stripe/stripe_event_payment_form.php';
                                            form.method = 'POST';
                                            $.each(data, function (key, value) {
                                                var element = document.createElement("Input");
                                                element.setAttribute("type", "hidden");
                                                element.setAttribute("name", key);
                                                element.setAttribute("value", value);
                                                var div = document.createElement("div");
                                                form.appendChild(element);
                                            });

                                            document.body.appendChild(form);
                                            form.submit();
                                            // Use the token to create the charge with a server-side script.
                                            // You can access the token ID with `token.id`
                                        }
                                    });
                                    handler.open({
                                        email: $scope.EventObject.email,
                                        panelLabel: 'Submit'
                                    });
                                }
                                if (mode === 'paypal') {
                                    $scope.depositpayapl(amount);

                                } else {

                                    $http.post("../../api/v2/event/management/paymentdetails/",
                                            {
                                                'restaurant': RestaurantID,
                                                'event_id': $scope.EventObject.eventID,
                                                'type': $scope.paytype,
                                                'amount': amount,
                                                'card_id': '',
                                                'payment_id': '',
                                                'status': 'pending_payment',
                                            }).then(function (response) {
                                        $scope.status = 'pending_payment';
                                        response.data.data;
                                    });

                                    //                API.eventmanagement.savepaymentdetails(RestaurantID, $scope.EventObject.eventID, 'pending_payment',$scope.paytype,amount,'','').then(function (result) {
                                    //                    $scope.status = 'pending_payment';
                                    //                });
                                    $scope.notifySaveMenuSelection();
                                }
                            };
                            $scope.depositpayapl = function (amount) {
                                var API_URL = "../../api/v2/payment/makepayment/deposit";
                                $http({
                                    url: API_URL,
                                    method: "POST",
                                    data: $.param({
                                        'restaurant': RestaurantID,
                                        'payment_type': 'paypal',
                                        'amount': amount,
                                        'reference_id': $scope.EventObject.eventID,
                                        'mode': 'event_ordering'
                                    }),
                                    headers: {
                                        'Content-Type': 'application/x-www-form-urlencoded'
                                    }
                                }).then(function (response) {
                                    var result = response.data;
                                    if (result.data.payment) {
                                        var url = result.data.payment.paypalUrl;
                                        var form = document.createElement('form');
                                        form.action = url;
                                        form.method = 'POST';
                                        document.body.appendChild(form);
                                        form.submit();
                                        //                            document.cookie = 'wee_navigation=p; expires=Fri, 3 Aug 2031 20:47:11 UTC; path=/';
                                        //                            var newWin = window.open(url, '_blank', 'toolbar=no, scrollbars=yes, copyhistory=no, resizable=no, top=100, left=100, width=950, height=785');
                                        //                            if (!newWin || newWin.closed || typeof newWin.closed == 'undefined') {
                                        //                                    document.cookie = 'wee_navigation=np; expires=Fri, 3 Aug 2031 20:47:11 UTC; path=/';
                                        //                                    var newWin2 = window.open(url, '_top', 'toolbar=no, scrollbars=yes, copyhistory=no, resizable=no, top=100, left=100, width=950, height=785');
                                        //                                    if (!newWin2 || newWin2.closed || typeof newWin2.closed == 'undefined') {
                                        //                                            alert('Hey! We have detected a pop-blocker. For this transaction, we need to open the paypal secured form in a pop-up. Please allow pop-up for this transaction and click ok');
                                        //                                    }
                                        //                            }

                                    }

                                });

                            }

                            $scope.getEvModifyDetails = function () {

                                var API_URL = "../../api/v2/event/management/modifydetails/" + $scope.event_id + "/";
                                $http.get(API_URL).then(function (result) {
                                    console.log("TOTAL " + JSON.stringify(result.data.status));
                                    console.log("TOTAL " + result.data.data.details.total_amount);
                                    if (result.data.status === 1) {

                                        if (result.data.data.details.total_amount > 0) {
                                            $scope.pre_totalamount = result.data.data.details.total_amount;
                                            $scope.depositamount = result.data.data.details.total_deposit;
                                            $scope.pre_depositamount = result.data.data.details.total_deposit;
                                            $scope.payable_amount = parseInt($scope.amount) - parseInt(result.data.data.details.last_total);
                                            console.log("SDSDSDs" + $scope.payable_amount);
                                            if (result.data.data.details.last_total > parseInt($scope.amount)) {
                                                $scope.isRequiredcc = false;
                                                $scope.EventObject.payment_method.creditcard = false;
                                                $scope.EventObject.payment_method.banktransfer = false;
                                            }

                                        }
                                    }



                                });

                            };


                            if ($('#sidebar-right') && $('#sidebar-right').offset()) {

                                // make right side bar affix
                                var sidebar = $('#sidebar-right');
                                var sidebar_affix_top = -330;
                                sidebar_offset = sidebar.offset();
                                sidebar.css({
                                    position: 'fixed',
                                    top: sidebar_offset.top + 'px',
                                    left: sidebar_offset.left + 'px',
                                    'max-width': '500px',
                                });
                                $(window).resize(function () {
                                    sidebar.css({
                                        position: 'relative',
                                        left: '0px',
                                    });
                                    sidebar.css({
                                        left: sidebar.offset().left + 'px',
                                        position: 'fixed',
                                    });
                                });
                                $(window).scroll(function () {
                                    if ($(window).scrollTop() + sidebar.height() + sidebar_affix_top > $('.event-booking').height()) {
                                        sidebar.css({
                                            top: ($('.event-booking').height() - $(window).scrollTop() - sidebar.height()) + 'px',
                                        });
                                    } else {
                                        if ($(window).scrollTop() + sidebar_affix_top > sidebar_offset.top) {

                                            sidebar.css({
                                                position: 'fixed',
                                                top: sidebar_affix_top + 'px',
                                            });
                                        } else {
                                            sidebar.css({
                                                position: 'fixed',
                                                top: (sidebar_offset.top - $(window).scrollTop()) + 'px'
                                            });
                                        }
                                    }

                                });
                            }
                        });

                        app.directive('jqdatepicker', function () {
                            return {
                                restrict: 'A',
                                require: 'ngModel',
                                link: function (scope, element, attrs) {
                                    var options = {
                                        locale: 'en',
                                        format: 'DD-MM-YYYY',
                                        minDate: new Date()
                                    };
                                    var DateTimePicker = $(element).datetimepicker(options);

                                    DateTimePicker.on('dp.change', function (e) {
                                        var DateSelected = moment(e.date._d).format('DD-MM-YYYY');
                                        scope.EventObject.rdate = DateSelected;

                                    });
                                }
                            };
                        });

                        app.directive('jtimedatepicker', function () {
                            return {
                                restrict: 'A',
                                require: 'ngModel',
                                link: function (scope, element, attrs) {
                                    var options = {
                                        locale: 'en',
                                        format: 'HH:mm',
                                        stepping: 15,
                                        defaultDate: new Date().setHours(19)
                                    };
                                    var DateTimePicker = $(element).datetimepicker(options);

                                    DateTimePicker.on('dp.change', function (e) {
                                        var DateSelected = moment(e.date._d).format('HH:mm');
                                        scope.EventObject.rtime = DateSelected;

                                    });
                                }
                            };
                        });
                        app.directive('cartevent', function () {
                            return {
                                restrict: 'E',
                                templateUrl: '_shopping_cart_event.html',
                                scope: {
                                    items: '=',
                                    menus: '=',
                                    counters: '=',
                                    categories: '=',
                                    selectioncomplete: '=',
                                    status: '=',
                                    totalamount: '=',
                                    minimumorder: '=',
                                    deposit: '=',
                                },
                                link: function (scope, element, attrs) {
                                    console.log("SDSDs" + JSON.stringify(scope.items));
                                    if (scope.items === undefined) {
                                        scope.items = [];
                                    }
                                    if (scope.menus === undefined) {
                                        scope.menus = [];
                                    }
                                    scope.removeItem = function (item) {
                                        scope.items.forEach(function (value, key) {
                                            var index = scope.items.indexOf(item);
                                            if (index > -1) {
                                                scope.items.splice(index, 1);
                                                scope.counters[item.displayID] = scope.counters[item.displayID] - 1;
                                                scope.selectioncomplete = false;
                                            }
                                        });
                                    };




                                }
                            };
                        });


            </script>


        </body>
</html>

