
<?php

require_once("lib/class.event_management.inc.php");
require_once("conf/conf.init.inc.php");

    if (empty($_REQUEST["event_id"])) {
        printf("Invalid request. Exiting");
        exit;
    }
    $event_id = $_REQUEST["event_id"];
    $ev_object = WY_EventManagement::getEventProjet(array('event_id' =>$event_id)); 

    $restaurant = $ev_object['restaurant'] ; 
    
    $BASE_URL = __BASE_URL__ ;

?>


<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="author" content="weeloy. https://www.weeloy.com"/>
        <meta name="copyright" content="weeloy. https://www.weeloy.com"/>  
        <meta http-equiv='expires' content='mon, 1 jan 1998 05:00:00 gmt'/>
        <meta http-equiv='pragma' content='no-cache'/>
        <meta http-equiv='pragma' content='cache-control: max-age=0'/>
        <meta http-equiv='last-modified' content='mon, 1 jan 1998 05:00:00 gmt'/>
        <meta http-equiv='cache-control' content='no-cache, must-revalidate'/>
        <meta name='robots' content='noindex, nofollow'/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <meta name="HandheldFriendly" content="true" />
        <title> Event Management</title>

        <link href="../../client/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../../client/bower_components/bootstrap/dist/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
        <link href="../../client/bower_components/fontawesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

        <link href="../../modules/booking/assets/css/css/bootstrap-select.css" rel="stylesheet" />
        <link href="../../modules/booking/assets/css/css/bootstrap-social.css" rel="stylesheet" />

        <script type='text/javascript' src="../../client/bower_components/angular/angular.min.js"></script>
        <script type="text/javascript" src="../../client/bower_components/jquery/dist/jquery.min.js"></script>

        <script type="text/javascript" src="../../js/jquery-ui.js"></script>
        <script type="text/javascript" src="../../client/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="../../js/bootstrap-select.js"></script>
        <script type='text/javascript' src="../../js/ui-bootstrap-tpls-0.12.1.min.js"></script>
        <script type="text/javascript" src="../../js/ngStorage.min.js"></script>
        <script type="text/javascript" src="../../js/mylocal.js"></script>
        <script type="text/javascript" src="../../client/app/models/Base.js"></script>
        <script type="text/javascript" src="../../client/app/models/Restaurant.js"></script>
        <script type="text/javascript" src="../../client/app/models/EventManagement.js"></script>
        <style>
            #event-management {
                background: #F5F5F5;
            }
            .btn-creditcard{
                border-radius: 2px;
                box-shadow: 2px 2px 5px #bbb;
                color: #fff;
                font-size: 16px;
                padding: 4px 14px;
            }
            .btn-leftBottom-orange, .btn-leftBottom-orange-noborder {
                background: #FF872C;
                border: 1px solid #FF872C;
            }
            
            #checkout-info {
            margin-top: 80px;
            font-family: 'proximanova';
            }
            .sidebar-contant {
                margin-top: 60px;
                margin-bottom: 60px;
                margin-left: 15px;
                margin-right: 15px;
            }
            #main-form > h3 {
                color: #999999;
            }
            .checkout-header {
            text-transform: uppercase;
            font-size: 20px;
            font-weight: bold;
            margin-bottom: 20px;
            line-height: 50px;
            font-family: 'proximanova-bold';
            }
            .checkout-header:after {
                display: block;
                content: '';
                width: 100%;
                border-top: 2px solid #333;
                margin: 0px auto;
                max-width: 80px;
            }
            .remarks {
                resize: none;
                min-height: 120px;
                border-radius: 2px;
            }
            .img-logo {
                text-align: center;
            }
        .back-to-cart {
            .fa {
                span {
                    margin-right: 5px;
                }
            }
        }
        </style>

        <script> var app = angular.module("myApp", ['ui.bootstrap', 'ngLocale', 'ngStorage']);</script> 
        <body ng-app="myApp">
        <section id='event-management'  ng-controller='EventManagementController' ng-init="moduleName = 'eventmsanagement';" style='margin-top:-80px;padding-top:60px; '> 
            <div id="checkout-info" class="event-booking">
                <div class="container" >
                    <div ng-class="{'col-md-2 col-lg-2' :status === 'confirmed' }"></div>
                    <div id="main-form" class="col-sm-12 col-md-8 col-lg-8" ng-class="{'col-sm-12 col-md-8 col-lg-8' :status !== 'confirmed','col-sm-12 col-md-8 col-lg-8' :status === 'confirmed'}" style="padding-bottom: 80px;">

                        <div id="event_header" class="text-center" ng-if="status === 'confirmed'">
                            <div class="img-logo">
                                <img ng-src="{{restaurant.getLogoImage()}}" />
                            </div>
                        </div>
                        <div style="padding-top:30px; padding-bottom: 30px;" class='' ng-if="status == 'request_sent' || status == 'menu_built' || status == 'menu_pre_selected' || status == 'menu_selected' || status == 'confirmed' || status == 'pending_payment'">
                            <p class="checkout-header text-center">Event Reservation Request</p>
                            <div class="event-request-form">
                                <div class='row'>
                                    <div class="form-group col-xs-12 col-sm12 col-md-6 col-lg-6">
                                        <label>Event date: </label>
                                        <label ng-bind="EventObject.rdate"></label>
                                    </div>
                                    <div class="form-group col-xs-12 col-sm12 col-md-6 col-lg-6">
                                        <label>Time: </label>
                                        <label ng-bind="EventObject.rtime"></label>
                                    </div>

                                    <div class="form-group col-xs-12 col-sm12 col-md-6 col-lg-6">
                                        <label>Number of people: </label>
                                        <label ng-bind="EventObject.pax"></label>
                                    </div>
                                </div>
                                <br/>
                                <div class='row'>
                                    <div class="form-group col-xs-12 col-sm12 col-md-6 col-lg-6">
                                        <label>First name: </label>
                                        <label ng-bind="EventObject.firstname"></label>
                                    </div>
                                    <div class="form-group col-xs-12 col-sm12 col-md-6 col-lg-6">
                                        <label>Last name: </label>
                                        <label ng-bind="EventObject.lastname"></label>
                                    </div>

                                    <div class="form-group col-xs-12 col-sm12 col-md-6 col-lg-6">
                                        <label>Company name(Optional)</label>
                                        <label ng-bind="EventObject.company"></label>
                                    </div>
                                </div>
                                <br/>
                                <div class='row'>
                                    <div class="form-group col-xs-12 col-sm12 col-md-6 col-lg-6">
                                        <label>Email: </label>
                                        <label ng-bind="EventObject.email"></label>
                                    </div>
                                    <div class="form-group col-xs-12 col-sm12 col-md-6 col-lg-6">
                                        <label>Phone: </label>
                                        <label ng-bind="EventObject.phone"></label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-xs-12 col-sm12 col-md-12 col-lg-12">
                                        <label>Occasion: </label>
                                        <label ng-bind="EventObject.occasion"></label>
                                    </div>
                                    <div class="form-group col-xs-12 col-sm12 col-md-6 col-lg-6">
                                        <label>Event name: </label>
                                        <label ng-bind="EventObject.name"></label>
                                    </div>
                                </div>

                                <div class="row" ng-if="EventObject.special_requests">
                                    <div class="title col-xs-12 col-sm12 col-md-12 col-lg-12">
                                        <h4>Special requests</h4>
                                        <label ng-bind="EventObject.special_requests"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="row" ng-if="status == 'request_sent'">
                                <div class="title col-xs-12 col-sm12 col-md-12 col-lg-12">
                                    <h4>Your request has been submitted to the restaurant. The restaurant will get back to you for further information.</h4>
                                </div>
                            </div>

                        </div>
                        
                   <div ng-if="status == 'menu_built' && EventObject.eventID">
                    <p class="checkout-header text-center">Menu selection</p>
                    <div ng-if="status != 'menu_selected'"  class="" ng-class="" ng-repeat="menu in menus" style="margin-bottom: 30px" >
                        <!--display food-->
                        <div ng-if="menu.food && !menu.drink && !menu.option" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding:20px; background-color: #eee">
                            <input type="checkbox" ng-model="setmenu[menu.menuID]" ng-change="stateChanged(menu.menuID)" ng-init="setmenu[menu.menuID]=true" style="float:left; margin-right: 20px;" />
                                        
                            <h4 style="float: left;" ng-bind="menu.notes"></h4>

                            <h5 style="float: left; margin-left:30px; ">(SGD {{menu.price}})</h5>
                            <h5 style="float: left; margin-left:30px; " ng-if='false'>Total {{menu.qte}}</h5>
                            <!--<input ng-model="menu.qte" class="col-lg-3"  type="text">-->
                            <div class="clear-both"></div>
                            <div class="row" ng-class="" ng-repeat="item in menu.items" >
                                <div  ng-if="menu.menuID === item.displayID" ng-class="{'col-xs-10 col-sm-10 col-md-10 col-lg-10' :status !== 'confirmed','col-sm-12 col-md-12 col-lg-12' :status === 'confirmed'}" style='display: table'>
                                    <div class="category-item-info" style='display: table-cell' >
                                        <div class="location-icon">
                                            <div class="location-text" style="margin-top: 5px;">
                                                <p class="col-lg-9" style='font-weight: bold;margin-top:15px;' ng-if="item.item_description == 'section' || item.item_description == 'section_or'"><span>{{item.item_title}}</span></p>
                                                <p class="col-lg-9" style='font-size: 12px' ng-if="item.item_description != 'section' && item.item_description != 'section_or'"><span>{{item.item_title}}</span><img style="width: 20px;margin-left: 20px;" ng-if="item.vegetarian" ng-src="images/restaurant_icons/vegi.png"></p>
                                                <p class="col-lg-9" style='font-size: 12px' ng-if="item.item_description != 'section' && item.item_description != 'section_or'"><span>{{item.item_description}}</span></p>
                                                <input ng-change="setQte(item, $event)" ng-model="item.qte" class="col-lg-3"  type="text" ng-if="(item.section === 'section_or' || item.section === 'section_or_nd') && item.item_description != 'section' && (item.item_description != 'section_or' || item.item_description != 'section_or_nodisplay')">
                                            </div>
                                            <div>
                                           </div>
                                            <span style="padding-left:80px" class="col-lg-12" ng-if="item.section === 'section_or' && item.item_description != 'section' && item.item_description != 'section_or' ">---- OR ----</span>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                        </div>
                    </div>

                    <div ng-if="(menu.drink || menu.option) && status != 'menu_selected'"  class="" ng-class="" ng-repeat="menu in menus" style="margin-bottom: 30px" >

                        <!--display drinks-->
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <!--                            <h1 ng-bind="menu.notes"></h1>-->
                            <h4 style="float: left;" ng-bind="menu.notes"></h4>
                            <!--<h5 style="float: left; margin-left:30px; ">(SGD {{menu.price}})</h5>-->
                            <h5 style="float: left; margin-left:30px; " ng-if="false && menu.qte > 0">Total {{menu.qte}}</h5>
                            <div style="clear: both"></div>
                            <div class="row" ng-class="" ng-repeat="item in menu.items" style='margin-bottom: 30px;'>

                                <div  ng-if="menu.menuID === item.displayID" ng-class="{'col-xs-10 col-sm-10 col-md-10 col-lg-10' :status !== 'confirmed','col-sm-12 col-md-12 col-lg-12' :status === 'confirmed'}" style='display: table'>
                                    <div class="category-item-info" style='display: table-cell' >
                                        <div class="location-icon">
                                            <div class="location-text" style="margin-top: 5px;">
                                                <!--<input class="col-lg-1" type="checkbox" >-->
                                                <!--<div class="col-lg-8" >-->
                                                <p class="col-lg-9" style='font-weight: bold;margin-top:15px;'><span>{{item.item_title}}</span></p>
                                                <p class="col-lg-9" style='font-size: 12px' ><span>{{item.item_description}}</span></p>
                                                <p ng-if="!menu.setmeal && item.price > 0" class="col-lg-9" style='font-size: 12px' ><span>SGD {{item.price}}</span></p>
                                                <!--</div>-->
                                                <p ng-if="menu.drink" >
                                                    <input  ng-change="setQte(item, $event)"  class="col-lg-3" type="text"  ng-model="item.qte" ng-if="item.price && item.price > 0">
                                            
                                                    <p  class="col-lg-3"  ng-if="!item.price || item.price < 1">INCLUDED</p>
                                                </p>
                                                <span ng-if='menu.option' >
                                                    <input  ng-change="setQte(item, $event)"  class="col-lg-3" type="text"  ng-model="item.qte" ng-if="item.price && item.price >= 0">
                                                </span>
                                            </div>
                                            <div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div>
                        <button class="btn-creditcard btn-leftBottom-orange pull-right" ng-click="saveEventMenu('selected', EventObject)">Continue</button>
                    </div>

                </div>
                        <div style='clear: both'></div>
                
                    <div ng-if="status === 'menu_pre_selected' || status === 'menu_selected' || status === 'confirmed'" >
                    <p style="margin-top:20px"  class="checkout-header text-center">Menu selection</p>

                    <div class="" ng-class="" ng-repeat="menu in menus">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" ng-if="menu.status === 'selected'">
                            <h4 ng-if='menu.qte > 0' class="col-lg-6" ng-bind="menu.notes"></h4>
                            <!--<h5 style="float: left; margin-left:30px; ">(SGD 110)</h5>-->
                           <h5 class="col-lg-3" ng-if="menu.setmeal" style="float: left; margin-left:30px; ">(SGD {{menu.price}})</h5>
                               <div class="clear-both"></div>
                            <div class="row" ng-class="" ng-repeat="item in menu.items" >
                                <div  ng-if="menu.menuID === item.displayID" ng-class="{'col-xs-10 col-sm-10 col-md-10 col-lg-10' :status !== 'confirmed','col-sm-12 col-md-12 col-lg-12' :status === 'confirmed'}" style='display: table'>
                                    <div class="category-item-info" style='display: table-cell' >
                                        <div class="location-icon">
                                            <div class="location-text" style="margin-top: 5px;" ng-if="item.qte > 0 || menu.setmeal">
                                                <p class="col-lg-9" style='font-weight: bold;margin-top:15px;'><span>{{item.item_title}}</span></p>
<!--                                                <p class="col-lg-9" style='font-size: 12px' ng-if="item.item_description != 'section' && item.item_description != 'section_or'"><span>{{item.item_description}}</span></p>-->
                                                <label class="col-lg-3" type="text" readonly ng-value="{{item.qte || 0}}" ng-if="item.item_description != 'section' && item.item_description != 'section_or'"><span ng-if="item.qte > 0">{{item.qte}} portions</span><span ng-if="item.qte < 1 && menu.setmeal">included</span></label>
                                                <!--                                                <input class="col-lg-3" type="text" readonly ng-value="5 portions" ng-if="item.item_description != 'section' && item.item_description != 'section_or'">-->
                                            </div>
                                            <div>

                                            </div>
                                            <span style="padding-left:80px" class="col-lg-12" ng-if="item.section === 'section_or' && item.item_description != 'section' && item.item_description != 'section_or' && item.item_description !='' ">---- OR ----</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    

                    <div class="order-now"  ng-if="status === 'menu_pre_selected'" style="margin-top:20px;">
                        <label style="margin-top: 30px;"> By clicking Save your selection your menu selection will be send to the restaurant</label>
                        <button style="margin-top: 20px;" class="btn-creditcard btn-leftBottom-orange pull-right" ng-click="saveEventMenu('selected')">Save you selection</button>
                    </div>
                    <div style='clear: both'></div>
                    <div  ng-if="(status === 'menu_selected' || status == 'menu_pre_selected' ||  status === 'confirmed') && EventObject.more.media.length > 0 " >
                        <p style="margin-top:50px" class="checkout-header text-center">Media</p>

                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" >
                            <div class='col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12' ng-repeat="evt in EventObject.more.media" >
                                <a href="{{mediaServer}}/upload/restaurant/{{EventObject.restaurant}}/700/{{evt.name}}" class="fresco" >
                                    <img ng-src="{{ mediaServer}}/upload/restaurant/{{EventObject.restaurant}}/180/{{evt.name}}" class="img-responsive"  style='max-height: 300px;'>

                                </a>
                            </div>
                        </div>
                    </div>
                    <div style='clear: both'></div>
                    

                    <div style='' ng-if="EventObject.billing && (status === 'menu_selected' || status == 'confirmed') ">
                        <p style="margin-top:50px" class="checkout-header text-center">Billing</p>
                        <div class="" ng-class="">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <!--                                <h4>Tncs</h4>-->
                                <div class="row" ng-class="" style="white-space:pre-wrap;margin-top: -20px;">
                                    <p ng-bind="EventObject.billing"></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style='clear: both'></div>
                    <div style='' ng-if="(status === 'menu_selected' || status == 'confirmed')">
                        <p style="margin-top:50px" class="checkout-header text-center">Tncs</p>
                        <div class="" ng-class="" ng-if='EventObject.tnc_cancel'>
<!--                            <h3 >Cancel Tncs</h3>-->
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="row" ng-class="" style="white-space:pre-wrap;margin-top: -20px;">
                                    <p ng-bind="EventObject.tnc_cancel"></p>
                                </div>

                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
                            <div class="row" ng-class="" style="margin-top: -20px;">
                                <h4 ng-if='EventObject.globalspltnc' >Billings</h4>
                                <p ng-if='EventObject.globalspltnc' ng-bind="EventObject.globalspltnc"></p>
                                 <h4 ng-if = "EventObject.globaltnc">Event T&C</h4>
                                 <p  ng-bind="EventObject.globaltnc"></p>
                            </div>
                        </div>
                   
                    </div>
                    <div style='clear: both'></div>
                    <div style='' ng-if="EventObject.more.additionalnote && (status === 'menu_selected' || status == 'confirmed') ">
                        <p style="margin-top:50px" class="checkout-header text-center">Additional Note</p>
                        <div class="" ng-class="">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <!--                                <h4>Tncs</h4>-->
                                <div class="row" ng-class="" style="white-space:pre-wrap;margin-top: -20px;">
                                    <p ng-bind="EventObject.more.additionalnote"></p>
                                </div>
                            </div>
                        </div>
                    </div>
        
                        <div style='clear: both'></div>
                    <div  style='' ng-if="status === 'menu_selected'">
                        <p class="checkout-header text-center">Payment </p>
                        <div class="" ng-class="">
                            <div  ng-if ='isRequiredcc && amount > 0'>
                                <p >
                      
                                    <h4 > Restaurant required {{EventObject.more.deposit}}%  deposit amount </h4>
                                     <span  style="margin-left:20px;" > Required deposit amount  : (SGD {{ amount * (EventObject.more.deposit / 100)  | number:2 }} )</span>
                                     <span ng-if='depositamount > 0 && isRequiredcc' style="margin-left:20px;"> Paid : SGD {{depositamount | number:2}} </span>
<!--                                    <span ng-if='payable_amount > 0' style="margin-left:50px;"> Balance Amount : SGD {{payable_amount * (EventObject.more.deposit / 100) | number:2 }} </span>-->
                                </p>
  
                            </div>
                                <p ng-if ="EventObject.more.deposit === 'no deposit'">
                                    Due to the restaurant tnc, this event booking is not required deposit.
                                </p>
                            <div class="form-group checkbox col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <!--<h4>Cancellation policies</h4>-->
                                <label ng-show ="EventObject.payment_method.creditcard === true && !restaurant.paypalflg">
                                    <input  ng-if ='!restaurant.paypalflg' class="payment_mode" ng-model="swiftcode" type="radio"  name="optionsPaymentRadios" id="optionsPaymentRadios1" ng-checked="isRequiredcc" value="creditcard" ng-change="logIt('creditcard')" > Credit Card
                                   
                             </label>
                                 <label ng-show ="EventObject.payment_method.creditcard === true && restaurant.paypalflg">
                                    <input class="payment_mode" ng-model="swiftcode" type="radio" name="optionsPaymentRadios" id="optionsPaymentRadios2"  value="paypal" ng-checked="isRequiredcc" ng-change="logIt('paypal')"> Paypal
                                </label>

                                <label ng-show ="EventObject.payment_method.banktransfer === true">
                                    <input  ng-model="swiftcode" class="payment_mode" type="radio"  name="optionsPaymentRadios" id="optionsPaymentRadios1" value="banktransfer" ng-change="logIt('banktransfer')" >  Bank Transfer
                                    <label style='padding-left:30px;' ng-show="swiftcode === 'banktransfer'"> <strong> Swift Code : AAAA BB CC DDD </strong></label>
                                </label>
                            </div>

                            <form name="myForm"  ng-if="status !== 'confirmed'">
                                <label style="margin: 20px;">
                                    <input type="checkbox" ng-model="tncsaccepted" style="margin: 20px;">I accept terms and conditions
                                </label><br/>

                                <div class="order-now" ng-show ='isRequiredcc === true'  >
                                    <button ng-if="status === 'menu_selected' && tncsaccepted && (swiftcode === 'creditcard' || swiftcode === 'banktransfer' || swiftcode === 'paypal')" class="btn-creditcard btn-leftBottom-orange pull-right" ng-click="submitCreditCardDetails()">Submit your informations</button>
                                </div>
                                <div class="order-now" ng-show ='isRequiredcc === false'  >
                                    <button ng-if="status === 'menu_selected' && tncsaccepted" class="btn-creditcard btn-leftBottom-orange pull-right" ng-click="submitCreditCardDetails()">Submit your informations</button>
                                </div>
                            </form>
                        </div>
                    </div>    
                </div>
       
                <div style="padding-bottom: 30px"  ng-if="status === 'menu_selected+++' || status === 'pending_payment' || status == 'confirmed'">
                    <p class="checkout-header text-center">Payment Details</p>
                    <div class='' ng-if="status === 'menu_selected+++'">
                        <div class="event-request-form">
                            <h2>Thank you, your request has  been sent to the restaurant</h2>
                        </div>
                    </div>
                    <div style='clear: both'></div>
                        <div class='' ng-if="status === 'confirmed'" >
                            <div class="event-request-form" style='margin-left:30px;'>
                      
                                <p ng-if='status'>Status : <span ng-bind="status"></span></p>
                                <p ng-if='totalamount > 0 '>Total Amount : SGD <span ng-bind="(amount | number:2)"></span></p>
                                <p ng-if='EventObject.payment_details.payment_id'>Transaction Id : <span ng-bind="EventObject.payment_details.payment_id"></span></p>
                                <p ng-if='pre_depositamount > 0'>Previous Deposit Amount : SGD <span ng-bind="(pre_depositamount ) | number:2"></span></p>
                                <p ng-if='EventObject.payment_details.deposit_amount'>Deposited Amount : SGD <span ng-bind="EventObject.payment_details.deposit_amount | number:2"></span></p>
                            </div>
                        </div>
                    <div class='' ng-if="status === 'pending_payment'">
                        <div class="event-request-form">
                            <h2>Restaurant waiting for payment confirmation. </h2>
                        </div>
                    </div>
                </div>

                <div id="event_footer" ng-if="status === 'confirmed'">
                    <h3 class="text-center" ng-bind="restaurant.getTitle()"></h3>

                    <p class="text-center"><span ng-bind="restaurant.getAddress()"></span>, <span ng-bind="restaurant.getZip()"></span></p>

                    <p class="text-center"><span ng-bind="restaurant.getPhone()"></span></p>

                    <p class="text-center"><span ng-bind="restaurant.getUrl()"></span></p>
                </div>

            </div>

            <div ng-class="{'col-md-2 col-lg-2' :status === 'confirmed' }"></div>

            <div ng-show="true && status !== 'confirmed'" id="sidebar-right" class='hidden-xs hidden-sm  col-md-4 sidebar' ng-class="{'hidden-xs hidden-sm col-md-4 sidebar':status !== 'confirmed', 'hidden-xs hidden-sm col-md-12':status === 'confirmed'}" style="top:60px;box-shadow: 2px 2px 5px 2px rgba(204, 204, 204, 1);border-radius: 2px;">
        
                <div class="sidebar-contant">
                    <div class="img-logo">
                        <img ng-src="{{logo}}" />
                    </div>
                    <h3 class="text-center" ng-bind="restaurant.title"></h3>

                    <p class="text-center" ng-if="restaurant.address"><span ng-bind="restaurant.address"></span>, <span ng-bind="restaurant.zip"></span></p>

                    <p class="text-center"><span ng-bind="restaurant.tel"></span></p>

                    <p class="text-center"><span ng-bind="restaurant.url"></span></p>
                    <!--                <p ng-bind="DeliveryDate | timeFormat:'DD-MM-YYYY'"></p>
                                    <p><span>Number of people</span>: <span ng-bind="EventObject.pax"></span></p>
                                    <p><span>Price</span>: SGD <span ng-bind="RestaurantEvent.getPrice()"></span></p>
                                    <p><span>Total</span>: SGD <span ng-bind="EventObject.pax :  RestaurantEvent.getPrice()"></span></p>-->
                    <hr/>
      
                    <cartevent ng-show="items.length >0" items="items" status="status"  ng-attr-minorder="EventObject.more.minimumprice" deposit="EventObject.more.deposit"  totalamount="totalamount" menus="menus" counters="counters" minimumorder ="minimumorder" selectioncomplete="selectioncomplete"></cartevent>
                </div>

            </div>
                </div>
            </div>
        </section>
        <script>
   
        app.controller('EventManagementController', function ($scope, $http) {
          var script = document.createElement('script');
            script.src = '//checkout.stripe.com/checkout.js';
            document.body.appendChild(script);
            script.src = '//js.stripe.com/v2/';
        
            document.body.appendChild(script);
                var RestaurantID = "<?php echo $restaurant ?>";
                var event_id = "<?php echo $event_id ?>";
                var BASE_URL = "<?php echo $BASE_URL ?>";
                $scope.selectioncomplete = false;
                $scope.menus = [];
                $scope.items = [];
                $scope.counters = [];
                $scope.tncsaccepted = false;
                $scope.mysection = '';
                $scope.swiftcode ='';
                $scope.paytype ='manual';
                $scope.isRequiredcc = false;
                $scope.depositamount = 0;
                $scope.pre_totalamount = 0;
                $scope.payable_amount = 0;
                $scope.pre_depositamount = 0;
                $scope.amount = 0;
                $scope.multipledata = ['media', 'timing', 'additional', 'payment'];
                $scope.setmenu =[];
                $scope.netamount = 0;
                $scope.ids = [];

                //$scope.payment_method = ['creditcard', 'banktransfer', 'manual','nodeposit'];
                $scope.payment_method = [{'creditcard' :false, 'banktransfer' :false, 'manual' :false,'nodeposit' :false}];
                $scope.multiplemodel = {
                    media: {name: ''},
                    payment: {name: ''},
                    additional: {name: ''},
                    timing: {name: '', funct: '', venue: '', garantee: '', expected: ''}
                };

                $scope.onlyNumbers = /^\d+$/;

                $scope.dietaries = [
                    {'title': 'Vegetarian'},
                    {'title': 'Vegetarian, no egg'},
                    {'title': 'Gluten Free'},
                    {'title': 'No raw food'},
                    {'title': 'No Crustaceans'}
                ];

                $scope.totalamount = 0;
                $scope.minimumorder = 0;
//                $scope.EventObject = EventObject;
                var API_URL = '../../api/restaurantfullinfo/' + RestaurantID;
                $http.get(API_URL, {
                }).then(function (result) {
                    if (result.data.status === 1) {
                         var restaurant = new Restaurant(result.data.data.restaurantinfo);
                        $scope.restaurant = result.data.data.restaurantinfo;
                        $scope.logo = restaurant.getLogoImage();
           
                    }
                });

            if (event_id) {
                $scope.event_id = event_id;
                var API_URL = '../../api/v2/event/management/' + $scope.event_id + '/';
                $http.get(API_URL).then(function (result) {
                   
                        $scope.status = result.data.data.event.status;
                        var ev = result.data.data.event;
                       
                        $scope.status = result.data.data.event.status;
                        $scope.amount = parseInt(result.data.data.event.total_amount);
                        var amount = parseInt(result.data.data.event.total_amount);
                            $scope.pre_totalamount = result.data.data.event.total_amount;
                            $scope.totalamount = result.data.data.event.amount;
                            $scope.netamount = result.data.data.event.amount;

                        $scope.multipledata.map(function (ll) {
                            if (!ev.more[ll] || ev.more[ll] instanceof Array === false)
                                ev.more[ll] = [];
                            else
                                ev.more[ll].map(function (pp) {
                                    pp.name = pp.name.replace(/\|\|\|/g, '\n');
                                });
                        });
                        $scope.EventObject = new EventManagement(ev);
                         console.log(JSON.stringify(  $scope.EventObject.globaltnc));
                       
                         var evdate  = $scope.EventObject.rdate.split('-');
                            $scope.EventObject.rdate = evdate[2]+"-"+evdate[1]+"-"+evdate[0];
                        if ($scope.EventObject.more) {
                            $scope.EventObject.more = JSON.parse($scope.EventObject.more.replace(/’/g, "\""));
                            $scope.minimumorder = $scope.EventObject.more.minimumprice;
                            
                            if($scope.EventObject.more.deposit && $scope.EventObject.more.deposit != 'no deposit'){
                                $scope.EventObject.more.deposit = parseInt($scope.EventObject.more.deposit);
                            }
                        }

                        //$scope.getEvModifyDetails();
                        
                        if($scope.EventObject.payment_details){
                             $scope.EventObject.payment_details = JSON.parse($scope.EventObject.payment_details.replace(/’/g, "\""));
                             $scope.depositamount = $scope.EventObject.payment_details.deposit_amount;
                        }
                        
                        if ($scope.EventObject.payment_method) {
                            $scope.EventObject.payment_method = JSON.parse($scope.EventObject.payment_method.replace(/’/g, "\""));
                              var evp = $scope.EventObject.payment_method;
                              console.log("PAYMENT METHOD" + JSON.stringify($scope.EventObject.payment_method));
                              
                            if (evp.nodeposit && evp.nodeposit === true ) {
                                 $scope.isRequiredcc = false; 
                                 $scope.paytype = 'waived';
                            }
                            else if(evp.creditcard === true  && evp.nodeposit === false  ){
                                 $scope.isRequiredcc = true; 
                            }else if(evp.creditcard === true  && evp.banktransfer === true && evp.manual === true ){
                                   $scope.isRequiredcc = false; 
                            }else if(evp.creditcard === true  && evp.banktransfer === true){
                                   $scope.isRequiredcc = true; 
                            }
                            else if(evp.manual === true  && evp.banktransfer === true  ){
                                 $scope.isRequiredcc = false; 
 
                            }
                            else if(evp.creditcard === true  || evp.manual === true  ){
                                 $scope.isRequiredcc = false; 
                            }
                            
                            else{
                                    $scope.isRequiredcc = true; 
                            } 
                        }
                        if($scope.depositamount > 0){
                            $scope.isRequiredcc = false; 
                            evp.creditcard = false;
                        }
                        
                    });
                 console.log("TEST" + $scope.totalamount);    
                      
            }
            if (event_id) {
                //console.log("THIRD" + $scope.totalamount + "AMOUNT " + $scope.amount );

                $scope.event_id = event_id;
                var API_URL = '../../api/v2/event/management/menu/' + $scope.event_id + '/';

                $http.get(API_URL).then(function (result) {

                    $scope.menus = result.data.data.menu;

                    $scope.menus.forEach(function (item, index) {
                        var section = '';
                        item.items.forEach(function (item2, index2) {
                            if (item2.item_description === 'section_or' || item2.item_description === 'section') {
                                section = item2.item_description;
                            } else {
                                $scope.menus[index].items[index2].section = section;
                            if (section === 'section_or' && (($scope.menus[index].items[index2 + 1] && $scope.menus[index].items[index2 + 1].item_description === 'section') || !$scope.menus[index].items[index2 + 1])) {
                                $scope.menus[index].items[index2].section = 'section_or_nd';
                            }
                        }
                        item2.vegetarian = (item2.extraflag & 8) == 8;
                    });
                    $scope.menus[index].food = (item.typeflag & 1) == 1;
                    $scope.menus[index].drink = (item.typeflag & 2) == 2;
                    $scope.menus[index].expend = (item.typeflag & 4) == 4;
                    $scope.menus[index].setmeal = (item.typeflag & 8) == 8;
                    $scope.menus[index].option = (item.typeflag & 16) == 16;
                    if($scope.ids.indexOf(item.menuID) < 0)
                        $scope.ids.push(item.menuID);
                });
                $scope.setQte();
                //console.log("AFTERR TOTLA Event  " + $scope.totalamount +"AMOUNT " + $scope.amount );

            });
           
                    
        }
        $scope.findaccount = function(id) {
		for(var i = 0; i < $scope.ids.length; i++)
			if($scope.ids[i] === id)
				return i;
		return -1;
        };

       
        $scope.stateChanged = function(id){
            if($scope.setmenu[id]){
                if($scope.ids.indexOf(id) < 0)
                    $scope.ids.push(id);
            }else{
                var ind = $scope.findaccount(id);	
                    if(ind >= 0) $scope.ids.splice(ind, 1);
            }
            $scope.setQte();
                
        };
    

        // make a test on status
        var API_URL = '../../api/payment/stripe/credentials/' + RestaurantID;
        $http.get(API_URL).then(function (response) {


            if (response.data.status === 1) {
                $scope.publishable_key = response.data.data.publishable_key;
            }
        });
        
         $scope.logIt = function (value) {
          $scope.swiftcode = value;
          $scope.paytype = value;
        };

        $scope.status = '';

        // Close Checkout on page navigation:
        $(window).on('popstate', function () {
            handler.close();
        });


        $scope.selectItem = function (item, evt) {
            if ($scope.items.indexOf(item) < 0) {
                $scope.items.push(item);
                if (typeof $scope.counters[item.displayID] === 'undefined') {
                    $scope.counters[item.displayID] = 1;
                } else {
                    $scope.counters[item.displayID] = $scope.counters[item.displayID] + 1;
                }

                $scope.checkContinue();
            }

        };
           
        $scope.setQte = function (item, evt) {
            $scope.totalamount = 0;
            $scope.menus.forEach(function (menu, key) {
                var sum = 0;
                var first_item = true;
                   
                if(menu.totalprice != 'undefined'){
                    menu.totalprice = 0;
                }else{
                   menu.totalprice = 0; 
                }
                
                if(menu.setmeal){
                    menu.isShow = false;
                    if($scope.ids.indexOf(menu.menuID)>=0){
                        menu.isShow = true;
                        menu.status = "selected" ;
                    }
                    if($scope.status === 'menu_selected'){
                        menu.isShow = (menu.status == "selected") ? true : false; 
                    }
                }else{
                     menu.isShow = true;
                }
                
                //menu.qte = 0;
                if (menu.items) {
                    menu.items.forEach(function (item, key) {
                        item.qte = parseInt(item.qte);
                        if(!item.qte){
                            item.qte = 0;
                        }
                        sum = sum + parseInt(item.qte);
                       
                        if (item.price) {
                            item.totalprice = parseInt(item.price) * parseInt(item.qte);
                            menu.totalprice = parseInt(menu.totalprice) + parseInt(item.totalprice);
                        }
                        if (first_item) {
                            menu.qte = sum;
                        }
                        if (item.item_description === 'section_or') {
                            if (menu.qte < sum) {
                                menu.qte = sum;
                                 console.log("MENU Q TE " + menu.qte );
                            }
                            sum = 0;
                            if (first_item) {
                                first_item = false;
                            }
                        }
                    });
                }
                if (menu.qte < sum) {
                    menu.qte = sum;
                }

                if (menu.setmeal && menu.isShow) {
                    if(menu.qte === 0){
                        $scope.EventObject.pax = (typeof ($scope.EventObject.pax) !== "undefined") ? $scope.EventObject.pax : 1;  
                        menu.totalprice = menu.price * $scope.EventObject.pax;
                        menu.qte = $scope.EventObject.pax;
                    }else{
                        menu.totalprice = menu.price * menu.qte;
                    }

                }
                if (menu.totalprice) {
                    $scope.totalamount = parseInt(menu.totalprice) + parseInt($scope.totalamount);
                    console.log("TOTAL AMOUNT " + $scope.totalamount);
//                    if($scope.netamount > 0 && ($scope.netamount !== $scope.totalamount )){
//                       $scope.totalamount = $scope.netamount;
//                    }
                 
                }
            });

            if ($scope.items.indexOf(item) < 0) {
                $scope.items.push(item);
            }

            $scope.items = $scope.menus;

//            if ($scope.items.indexOf(item) < 0) {
//                $scope.items.push(item);
//            }

            //console.log($scope.items);   
            

        };


  
        $scope.checkContinue = function () {

            $scope.selectioncomplete = true;
            $scope.menus.forEach(function (menu, key) {

                if ($scope.selectioncomplete && ($scope.counters[menu.menuID] !== parseInt(menu.item_limit))) {
                    $scope.selectioncomplete = false;
                }
            });
        };

       

        $scope.saveEventMenu = function (status, EventObject) {
            var event_id = '';
            var selected_items = [];
            var amount = $scope.totalamount,service_charge = 0,gst = 0 ;  
             if($scope.totalamount !== 0){
                service_charge = $scope.totalamount * 0.10;
                gst = $scope.totalamount * 0.07 ;
                amount = $scope.totalamount ;
                $scope.amount = amount + gst + service_charge ;
               
             }  
             if($scope.pre_totalamount > 0 ){
                 if(parseInt($scope.pre_totalamount) >= parseInt(amount)) { 
                     $scope.isRequiredcc = false;
                     $scope.EventObject.payment_method.creditcard = false;
                     $scope.EventObject.payment_method.banktransfer = false;

                 }else{
                     $scope.payable_amount =  parseInt(amount) - parseInt($scope.pre_totalamount);
                 }
                    
             }
       
  
            $scope.items.forEach(function (value, key) {
                if(value){
                    event_id = value.eventID;
                    var data = { "id": value.menuItemID, "qte": value.qte };
                    if(value.isShow)
                   selected_items.push(data);  
                }   
                    
            });
            
             var API_URL = '../../api/v2/event/management/menu/' + event_id + '/save/';
        if(selected_items.length > 0){
                $http.post(API_URL, {
                        items: selected_items,
                        status: status
                    }).then(function (result) {

                });
           
             $http.post("../../api/v2/event/management/updateprice/", 
                    {
                        'restaurant': RestaurantID,
                        'event_id': $scope.EventObject.eventID,
                        'total_amount' : $scope.totalamount,
                        'amount' : amount,
                        'gst' : gst,
                        'service_charge' : service_charge,
                        'status' : 'menu_selected',
                        'email' : $scope.EventObject.email,
                    }).then(function(response) {
                        return response.data.data;
                   });
            //API.eventmanagement.updatePrice(RestaurantID,$scope.EventObject.eventID,$scope.EventObject.email,$scope.totalamount,gst,service_charge,amount,'menu_selected').then(function (result) {});
            //$scope.getEvModifyDetails();
            $scope.status = 'menu_selected';
        }
        };
        $scope.notifySaveMenuSelection = function () {
            $scope.EventObject.notify_type = 'event_menu_selected';
            var API_URL = '../../api/v2/event/management/menu/notify/';
            $http.post(API_URL, {
                    items: $scope.EventObject,
                }).then(function (result) {

            });
        };

        //payment creation
        $scope.submitCreditCardDetails = function () {
            //$scope.setQte();
            var amount = $scope.totalamount * 1.10 * 1.07 ,
                mode = $("input:radio[name=optionsPaymentRadios]:checked").val();
            if($scope.payable_amount > 0){
                amount = $scope.payable_amount;
            }
            if($scope.EventObject.more.deposit && $scope.EventObject.more.deposit != 'no deposit'){
                amount = amount * ($scope.EventObject.more.deposit / 100);
            }
           
            amount = Math.round(amount);
            var data = $scope.EventObject;
              
                data.restaurant_id = RestaurantID;
                data.event_id = data.eventID;
                data.amount = amount;
                data.curency = 'SGD';
                data.action = 'private_event';
                data.controller = 'white_label';
                data.returnUrl = '../event-management/menu-selection.php?event_id=';
         
            if(mode === 'creditcard'){
    
                var handler = StripeCheckout.configure({
                    key: $scope.publishable_key, //'pk_test_aG4RNDkBJI81YsT2ljQGVnuW',
                    token: function (token) {
                          data.stripeToken = token.id;
                        var form = document.createElement('form');
                        form.action = '../payment/stripe/stripe_event_payment_form.php';
                        form.method = 'POST';
                        $.each(data, function (key, value) {
                            var element = document.createElement("Input");
                            element.setAttribute("type", "hidden");
                            element.setAttribute("name", key);
                            element.setAttribute("value", value);
                            var div = document.createElement("div");
                            form.appendChild(element);
                        });

                        document.body.appendChild(form);
                        form.submit();
                        // Use the token to create the charge with a server-side script.
                        // You can access the token ID with `token.id`
                    }
                });
                handler.open({
                    email: $scope.EventObject.email,
                    panelLabel: 'Submit'
                });
            }
            else if(mode === 'paypal'){
              $scope.depositpayapl(amount);
            
            }else {
                
                $http.post("../../api/v2/event/management/paymentdetails/", 
                    {
                        'restaurant': RestaurantID,
                        'event_id': $scope.EventObject.eventID,
                        'type' : $scope.paytype,
                        'amount' : amount,
                        'card_id' : '',
                        'payment_id' : '',
                        'status'  :'pending_payment',
                    }).then(function(response) {
                      $scope.status = 'pending_payment';
                      response.data.data;
                   });
                 
//                API.eventmanagement.savepaymentdetails(RestaurantID, $scope.EventObject.eventID, 'pending_payment',$scope.paytype,amount,'','').then(function (result) {
//                    $scope.status = 'pending_payment';
//                });
                $scope.notifySaveMenuSelection(); 
            }
        };
        $scope.depositpayapl = function (amount) {
            var API_URL = "../../api/v2/payment/makepayment/deposit" ;
            $http({
                    url: API_URL,
                    method: "POST",
                    data: $.param({
                            'restaurant':RestaurantID,
                            'payment_type': 'paypal',
                            'amount':amount,
                            'reference_id':$scope.EventObject.eventID,
                            'mode'        :'event_ordering'
                        }),
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        }
                    }).then(function (response) {
                        var result = response.data;
                        if(result.data.payment){
                            var url = result.data.payment.paypalUrl;
                            var form = document.createElement('form');
                                form.action = url;
                                form.method = 'POST';
                                document.body.appendChild(form);
                                form.submit();
//                            document.cookie = 'wee_navigation=p; expires=Fri, 3 Aug 2031 20:47:11 UTC; path=/';
//                            var newWin = window.open(url, '_blank', 'toolbar=no, scrollbars=yes, copyhistory=no, resizable=no, top=100, left=100, width=950, height=785');
//                            if (!newWin || newWin.closed || typeof newWin.closed == 'undefined') {
//                                    document.cookie = 'wee_navigation=np; expires=Fri, 3 Aug 2031 20:47:11 UTC; path=/';
//                                    var newWin2 = window.open(url, '_top', 'toolbar=no, scrollbars=yes, copyhistory=no, resizable=no, top=100, left=100, width=950, height=785');
//                                    if (!newWin2 || newWin2.closed || typeof newWin2.closed == 'undefined') {
//                                            alert('Hey! We have detected a pop-blocker. For this transaction, we need to open the paypal secured form in a pop-up. Please allow pop-up for this transaction and click ok');
//                                    }
//                            }
 
                        }
                
                });
		
	}
        
        $scope.getEvModifyDetails = function(){

        var API_URL = "../../api/v2/event/management/modifydetails/"+ $scope.event_id +"/";
                $http.get(API_URL).then(function (result) {
                    if(result.data.status === 1){
               
                        if(result.data.data.details.total_amount > 0){
                            $scope.pre_totalamount = result.data.data.details.total_amount;
                            $scope.depositamount = result.data.data.details.total_deposit;
                            $scope.pre_depositamount = result.data.data.details.total_deposit;
                            $scope.payable_amount =  parseInt($scope.amount) - parseInt(result.data.data.details.last_total);
                            if(result.data.data.details.last_total > parseInt($scope.amount) ){
                                $scope.isRequiredcc = false;
                                $scope.EventObject.payment_method.creditcard = false;
                                $scope.EventObject.payment_method.banktransfer = false; 
                            }
                                
                         }
                    }
    
                      
                    
             });
            
        };
       

       if ($('#sidebar-right') && $('#sidebar-right').offset()) {
            
            // make right side bar affix
            var sidebar = $('#sidebar-right');
            var sidebar_affix_top = -330;
            sidebar_offset = sidebar.offset();
            sidebar.css({
                position: 'fixed',
                top:  sidebar_offset.top + 'px',
                left: sidebar_offset.left + 'px',
                'max-width': '500px',
              
            });
            $(window).resize(function () {
                sidebar.css({
                    position: 'relative',
                    left: '0px',
                });
                sidebar.css({
                    left: sidebar.offset().left + 'px',
                    position: 'fixed',
                });
            });
            $(window).scroll(function () {
                if ($(window).scrollTop() + sidebar.height() + sidebar_affix_top > $('.event-booking').height()) {
                    sidebar.css({
                        top: ($('.event-booking').height() - $(window).scrollTop() - sidebar.height()) + 'px',
                    });
                } else {
                    if ($(window).scrollTop() + sidebar_affix_top > sidebar_offset.top) {
                      
                        sidebar.css({
                            position: 'fixed',
                            top: sidebar_affix_top + 'px',
                        });
                    } else {
                        sidebar.css({
                            position: 'fixed',
                            top: (sidebar_offset.top - $(window).scrollTop()) + 'px'
                        });
                    }
                }

            });
        }
          });
 
            app.directive('jqdatepicker', function () {
                return {
                    restrict: 'A',
                    require: 'ngModel',
                     link: function (scope, element, attrs) {
                          var options = {
                                    locale: 'en',
                                    format: 'DD-MM-YYYY',
                                    minDate: new Date()
                                };
                                  var DateTimePicker = $(element).datetimepicker(options);

                                  DateTimePicker.on('dp.change', function (e) {
                                     var DateSelected = moment(e.date._d).format('DD-MM-YYYY');
                                     scope.EventObject.rdate = DateSelected;

                                });
                    }
                };
            });

            app.directive('jtimedatepicker', function () {
                return {
                    restrict: 'A',
                    require: 'ngModel',
                     link: function (scope, element, attrs) {
                          var options = {
                                    locale: 'en',
                                    format: 'HH:mm',
                                    stepping: 15,
                                    defaultDate: new Date().setHours(19)
                                };
                                  var DateTimePicker = $(element).datetimepicker(options);

                                  DateTimePicker.on('dp.change', function (e) {
                                     var DateSelected = moment(e.date._d).format('HH:mm');
                                     scope.EventObject.rtime = DateSelected;

                                });
                    }
                };
            });
            app.directive('cartevent',function () {
                return {
                    restrict: 'E',
                    templateUrl: '_shopping_cart_event.html',
                scope: {
                    items: '=',
                    menus: '=',
                    counters: '=',
                    categories: '=',
                    selectioncomplete: '=',
                    status: '=',
                    totalamount: '=',
                    minimumorder: '=',
                    deposit: '=',


                },
            link: function (scope, element, attrs) {
                console.log("SDSDs" +JSON.stringify(scope.items));
                if (scope.items === undefined) {
                    scope.items = [];
                }
                if (scope.menus === undefined) {
                    scope.menus = [];
                }
                scope.removeItem = function (item) {
                    scope.items.forEach(function (value, key) {
                        var index = scope.items.indexOf(item);
                        if (index > -1) {
                            scope.items.splice(index, 1);
                            scope.counters[item.displayID] = scope.counters[item.displayID] - 1;
                            scope.selectioncomplete = false;
                        }
                    });
                };

                


            }
        };
    });

          
        </script>


        </body>
</html>




