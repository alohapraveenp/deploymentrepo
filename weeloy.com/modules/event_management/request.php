<?php

    require_once("conf/conf.init.inc.php");

    if (empty($_REQUEST["restaurant"])) {
        printf("Invalid request. Exiting");
        exit;
    }
    $restaurant = $_REQUEST["restaurant"];

?>


<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="author" content="weeloy. https://www.weeloy.com"/>
<meta name="copyright" content="weeloy. https://www.weeloy.com"/>  
<meta http-equiv='expires' content='mon, 1 jan 1998 05:00:00 gmt'/>
<meta http-equiv='pragma' content='no-cache'/>
<meta http-equiv='pragma' content='cache-control: max-age=0'/>
<meta http-equiv='last-modified' content='mon, 1 jan 1998 05:00:00 gmt'/>
<meta http-equiv='cache-control' content='no-cache, must-revalidate'/>
<meta name='robots' content='noindex, nofollow'/>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="HandheldFriendly" content="true" />
<title> Event Management</title>
<base href="<?php echo __ROOTDIR__; ?>/" />

<link href="client/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="client/bower_components/bootstrap/dist/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
<link href="client/bower_components/fontawesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="modules/booking/assets/css/css/bootstrap-select.css" rel="stylesheet" />
<link href="modules/booking/assets/css/css/bootstrap-social.css" rel="stylesheet" />
<link href="modules/booking/assets/css/css/famfamfam-flags.css" rel="stylesheet" />
<link href="modules/booking/assets/css/css/dropdown.css" rel="stylesheet" />
<link href="client/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />



<script type="text/javascript" src="client/bower_components/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>
<script type='text/javascript' src="client/bower_components/angular/angular.min.js"></script>
<script type="text/javascript" src="client/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="client/bower_components/ui-bootstrap-tpls-0.14.2.min.js"></script>
<script type="text/javascript" src="client/bower_components/moment/min/moment.min.js"></script>
<script type="text/javascript" src="client/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

<script type="text/javascript" src="js/ngStorage.min.js"></script>
<script type="text/javascript" src="js/mylocal.js"></script>
 <script> var app = angular.module("myApp", ['ui.bootstrap', 'ngLocale', 'ngStorage']);</script> 


<script type="text/javascript" src="client/app/models/Base.js"></script>
<script type="text/javascript" src="client/app/models/Restaurant.js"></script>
<script type="text/javascript" src="client/app/models/EventManagement.js"></script>
<style>
    #event-management {
        background: #F5F5F5;
    }
    .btn-creditcard{
        border-radius: 2px;
        box-shadow: 2px 2px 5px #bbb;
        color: #fff;
        font-size: 16px;
        padding: 4px 14px;
    }
    .btn-leftBottom-orange, .btn-leftBottom-orange-noborder {
        background: #FF872C;
        border: 1px solid #FF872C;
    }
</style>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="author" content="weeloy. https://www.weeloy.com"/>
        <meta name="copyright" content="weeloy. https://www.weeloy.com"/>  
        <meta http-equiv='expires' content='mon, 1 jan 1998 05:00:00 gmt'/>
        <meta http-equiv='pragma' content='no-cache'/>
        <meta http-equiv='pragma' content='cache-control: max-age=0'/>
        <meta http-equiv='last-modified' content='mon, 1 jan 1998 05:00:00 gmt'/>
        <meta http-equiv='cache-control' content='no-cache, must-revalidate'/>
        <meta name='robots' content='noindex, nofollow'/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <meta name="HandheldFriendly" content="true" />
        <title> Event Management</title>
         <base href="<?php echo __ROOTDIR__; ?>/" />
        <link href="client/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="client/bower_components/bootstrap/dist/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
        <link href="client/bower_components/fontawesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="modules/booking/assets/css/css/bootstrap-select.css" rel="stylesheet" />
        <link href="modules/booking/assets/css/css/bootstrap-social.css" rel="stylesheet" />
        <link href="modules/booking/assets/css/css/famfamfam-flags.css" rel="stylesheet" />
        <link href="modules/booking/assets/css/css/dropdown.css" rel="stylesheet" />
        <link href="client/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />



        <script type="text/javascript" src="client/bower_components/jquery/dist/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui.js"></script>
        <script type='text/javascript' src="client/bower_components/angular/angular.min.js"></script>
        <script type="text/javascript" src="client/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="client/bower_components/ui-bootstrap-tpls-0.14.2.min.js"></script>
        <script type="text/javascript" src="client/bower_components/moment/min/moment.min.js"></script>
        <script type="text/javascript" src="client/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

        <script type="text/javascript" src="js/ngStorage.min.js"></script>
        <script type="text/javascript" src="js/mylocal.js"></script>
         <script> var app = angular.module("myApp", ['ui.bootstrap', 'ngLocale', 'ngStorage']);</script> 


        <script type="text/javascript" src="client/app/models/Base.js"></script>
        <script type="text/javascript" src="client/app/models/Restaurant.js"></script>
        <script type="text/javascript" src="client/app/models/EventManagement.js"></script>

        <style>
            #event-management {
                background: #F5F5F5;
            }
            .btn-creditcard{
                border-radius: 2px;
                box-shadow: 2px 2px 5px #bbb;
                color: #fff;
                font-size: 16px;
                padding: 4px 14px;
            }
            .btn-leftBottom-orange, .btn-leftBottom-orange-noborder {
                background: #FF872C;
                border: 1px solid #FF872C;
            }
            .error-msg {
                color:#af3030;
            }
        </style>

      
    </head>
      
        <body ng-app="myApp">
        <section id='event-management'  ng-controller='EventManagementController' ng-init="moduleName = 'eventmsanagement';" style='margin-top:-80px;padding-top:60px; '> 
            <div id="checkout-info" class="event-booking">
                <div class="container" >
                    <div ng-class="{'col-md-2 col-lg-2' :status === 'confirmed' }"></div>
                    <div id="main-form" class="col-sm-12 col-md-8 col-lg-8" ng-class="{'col-sm-12 col-md-8 col-lg-8' :status !== 'confirmed','col-sm-12 col-md-8 col-lg-8' :status === 'confirmed'}" style="padding-bottom: 80px;">

                        <div id="event_header" class="text-center" ng-if="status === 'confirmed'">
                            <div class="img-logo">
                                <img ng-src="{{restaurant.getLogoImage()}}" />
                            </div>
                        </div>
                        <div ng-if="!event_id && !status">
                            <p class="checkout-header text-center">Event Request</p>
                            <div class="event-request-form">
                                <h1>
                                    Event Details</h1>
                                <form id="form-border" name="EventForm" ng-submit="SaveEventRequest(EventForm, EventObject)" novalidate>
                                    <div class='row'>
                                        <div class="form-group col-xs-12 col-sm12 col-md-6 col-lg-6">
                                            <label>Date*</label>

                                            <input type="text" class="form-control" id="wtimepicker6" name="rdate"  ng-model="EventObject.rdate" jqdatepicker  required>        

                                                <div class="error error-msg">
                                                    <span ng-show="EventForm.$submitted && EventForm.rdate.$error.required">Please enter the booking date</span>
                                                </div>
                                        </div>
                                        <div class="form-group col-xs-12 col-sm12 col-md-6 col-lg-6">
                                            <label>Time*</label>
                                            <input type="text" class="form-control" id="wtimepicker7" name="rtime" ng-model="EventObject.rtime" jtimedatepicker required>
                                                <div class="error error-msg">
                                                    <span ng-show="EventForm.$submitted && EventForm.rtime.$error.required">Please enter the booking time</span>
                                                </div>
                                        </div>

                                        <div class="form-group col-xs-12 col-sm12 col-md-6 col-lg-6">
                                            <label>Number of people*</label>
                                            <input type="text" class="form-control" ng-model="EventObject.pax" name="pax" required >
                                                <div class="error error-msg">
                                                    <span ng-show="EventForm.$submitted && EventForm.pax.$error.required">Please enter number of people</span>
                                                </div>
                                        </div>
                                    </div>
                                    <div class='row'>
                                    <div class="form-group col-xs-12 col-sm12 col-md-6 col-lg-6" uib-dropdown >
                                            <label>Title</label>
                                            <select ng-model="EventObject.salutation" class="form-control" required style='border-radius: 0px;'>
                                                <option ng-value="Mr">Mr.</option>
                                                <option ng-value="Mrs">Mrs.</option>
                                                <option ng-value="Ms">Ms.</option>
                                                <option ng-value="Dr">Dr.</option>
                                                <option ng-value="Mdm">Mdm.</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-xs-12 col-sm12 col-md-6 col-lg-6">
                                            <label>First name*</label>
                                            <input type="text" class="form-control" name="firstname" ng-model="EventObject.firstname" required>
                                                <div class="error error-msg">
                                                    <span ng-show="EventForm.$submitted && EventForm.firstname.$error.required">Please enter your first name</span>
                                                </div>
                                        </div>
                                        <div class="form-group col-xs-12 col-sm12 col-md-6 col-lg-6">
                                            <label>Last name*</label>
                                            <input type="text" class="form-control" name="lastname" ng-model="EventObject.lastname" required>
                                                <div class="error error-msg ">
                                                    <span ng-show="EventForm.$submitted && EventForm.lastname.$error.required">Please enter your last name</span>
                                                </div>
                                        </div>

                                        <div class="form-group col-xs-12 col-sm12 col-md-6 col-lg-6">
                                            <label>Company name(Optional)</label>
                                            <input type="text" class="form-control" ng-model="EventObject.company">
                                        </div>
                                    </div>
                                    <div class='row'>
                                        <div class="form-group col-xs-12 col-sm12 col-md-6 col-lg-6">
                                            <label>Email*</label>
                                            <input type="email" class="form-control" name="email" ng-model="EventObject.email" required>
                                                <div class="error error-msg">
                                                    <span ng-show="EventForm.$submitted && EventForm.email.$error.required">Please enter your email</span>
                                                    <span ng-show="EventForm.$submitted && EventForm.email.$error.email">Please enter a valid email</span>
                                                </div>
                                        </div>
                                        <div class="form-group col-xs-12 col-sm12 col-md-6 col-lg-6">
                                            <label>Phone*</label>
                                            <input type="text" class="form-control" name="phone" ng-model="EventObject.phone" ng-pattern="/[+|0-9|\s]+$/" required>
                                                <div class="error error-msg">
                                                    <span ng-show="EventForm.$submitted && EventForm.phone.$error.required">Please enter your phone number</span>
                                                    <span ng-show="EventForm.$submitted && EventForm.phone.$error.pattern">Please enter a valid phone number</span>
                                                </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-xs-12 col-sm12 col-md-6 col-lg-6">
                                            <label>Occasion *</label>
                                            <!--<textarea class="form-control remarks" ng-model="EventObject.occasion" name="occasion" required></textarea>-->
                                            <select ng-model="EventObject.occasion" class="form-control" required style='border-radius: 0px;'>
                                                <option ng-value="corporate" >Corporate dinner</option>
                                                <option ng-value="meeting">Meeting and Seminars</option>
                                                <option ng-value="social">Social ie Anniversary / Birthday</option>
                                                <option ng-value="solemnisation">Solemnisation Ceremony</option>
                                                <option ng-value="wedding">Weddings</option>
                                                <option ng-value="other">Others (please specify in special requests)</option>
                                            </select>

                                            <div class="error">
                                                <span ng-show="EventForm.$submitted && EventObject.occasion == '' || EventForm.$submitted && EventForm.occasion.$error.required">Please enter the occasion of the event</span>
                                            </div>
                                        </div>
                                        <div class="form-group col-xs-12 col-sm12 col-md-6 col-lg-6">
                                            <label>Event Name*</label>
                                            <input type="text" class="form-control" ng-model="EventObject.name" name="name" required >
                                                <div class="error error-msg">
                                                    <span ng-show="EventForm.$submitted && EventForm.name.$error.required">Please enter event name</span>
                                                </div>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="title col-xs-12 col-sm12 col-md-12 col-lg-12">
                                            <h4>Special requests</h4>
                                            <textarea class="form-control remarks" ng-model="EventObject.special_requests"></textarea>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="title col-xs-12 col-sm12 col-md-12 col-lg-12" style='padding-top:20px;'>
                                            <button type="submit" class="btn-creditcard btn-leftBottom-orange pull-right">Submit request</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div style="padding-top:30px; padding-bottom: 30px;" class='' ng-if="status == 'request_sent' || status == 'menu_built' || status == 'menu_pre_selected' || status == 'menu_selected' || status == 'confirmed' || status == 'pending_payment'">
                            <p class="checkout-header text-center">Event Reservation Request</p>
                            <div class="event-request-form">
                                <div class='row'>
                                    <div class="form-group col-xs-12 col-sm12 col-md-6 col-lg-6">
                                        <label>Event date: </label>
                                        <label ng-bind="EventObject.rdate"></label>
                                    </div>
                                    <div class="form-group col-xs-12 col-sm12 col-md-6 col-lg-6">
                                        <label>Time: </label>
                                        <label ng-bind="EventObject.rtime"></label>
                                    </div>

                                    <div class="form-group col-xs-12 col-sm12 col-md-6 col-lg-6">
                                        <label>Number of people: </label>
                                        <label ng-bind="EventObject.pax"></label>
                                    </div>
                                </div>
                                <br/>
                                <div class='row'>
                                    <div class="form-group col-xs-12 col-sm12 col-md-6 col-lg-6">
                                        <label>Title: </label>
                                        <label ng-bind="EventObject.salutation"></label>
                                    </div>
                                    <div class="form-group col-xs-12 col-sm12 col-md-6 col-lg-6">
                                        <label>First name: </label>
                                        <label ng-bind="EventObject.firstname"></label>
                                    </div>
                                    <div class="form-group col-xs-12 col-sm12 col-md-6 col-lg-6">
                                        <label>Last name: </label>
                                        <label ng-bind="EventObject.lastname"></label>
                                    </div>

                                    <div class="form-group col-xs-12 col-sm12 col-md-6 col-lg-6">
                                        <label>Company name(Optional)</label>
                                        <label ng-bind="EventObject.company"></label>
                                    </div>
                                </div>
                                <br/>
                                <div class='row'>
                                    <div class="form-group col-xs-12 col-sm12 col-md-6 col-lg-6">
                                        <label>Email: </label>
                                        <label ng-bind="EventObject.email"></label>
                                    </div>
                                    <div class="form-group col-xs-12 col-sm12 col-md-6 col-lg-6">
                                        <label>Phone: </label>
                                        <label ng-bind="EventObject.phone"></label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-xs-12 col-sm12 col-md-12 col-lg-12">
                                        <label>Occasion: </label>
                                        <label ng-bind="EventObject.occasion"></label>
                                    </div>
                                    <div class="form-group col-xs-12 col-sm12 col-md-6 col-lg-6">
                                        <label>Event name: </label>
                                        <label ng-bind="EventObject.name"></label>
                                    </div>
                                </div>

                                <div class="row" ng-if="EventObject.special_requests">
                                    <div class="title col-xs-12 col-sm12 col-md-12 col-lg-12">
                                        <h4>Special requests</h4>
                                        <label ng-bind="EventObject.special_requests"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="row" ng-if="status == 'request_sent'">
                                <div class="title col-xs-12 col-sm12 col-md-12 col-lg-12">
                                    <h4>Your request has been submitted to the restaurant. The restaurant will get back to you for further information.</h4>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <script>
   
            app.controller('EventManagementController', function ($scope, $http) {
                var RestaurantID = "<?php echo $restaurant ?>";
                var EventObject = new EventManagement();
                    $scope.EventObject = EventObject;
                var API_URL = 'api/restaurantfullinfo/' + RestaurantID;
                $http.get(API_URL, {
                    cache: true,
                }).then(function (result) {

                    if (result.status === 1) {
                         var restaurant = new Restaurant(result.data.restaurantinfo);
                        $scope.restaurant = result;
                    }
                });
            $scope.SaveEventRequest = function (EventForm, EventObject) {
            
                var eventManagerObject = new EventManagement(EventObject);
                eventManagerObject.setRestaurant(RestaurantID);

                if (eventManagerObject.getNumberPeople > $scope.balPax) {
                    alert("This event is currently available for " + $scope.balPax + " people only");
                    return false;
                }

                if (EventForm.$invalid) {
                    var top;
                    if (EventForm.firstname.$invalid || EventForm.lastname.$invalid || EventForm.email.$invalid || EventForm.phone.$invalid) {
                        top = $('input[name="firstname"]').offset().top;
                        $(window).scrollTop(top - 200);
                    }
                    return false;
                }
                $scope.formObject = EventForm;
                var API_URL = 'api/v2/event/management/create/';
                $http.post(API_URL, eventManagerObject).then(function (result) {
                    console.log("CREATE"+ JSON.stringify(result));
                      $scope.status = 'request_sent';
                      eventManagerObject.setStatus('request_sent');
                                //$scope.restaurant = result;
                      $(window).scrollTop();
                });
             
            };
         
            });
             app.directive('jqdatepicker', function () {
                return {
                    restrict: 'A',
                    require: 'ngModel',
                     link: function (scope, element, attrs) {
                          var options = {
                                    locale: 'en',
                                    format: 'DD-MM-YYYY',
                                    minDate: new Date()
                                };
                                  var DateTimePicker = $(element).datetimepicker(options);

                                  DateTimePicker.on('dp.change', function (e) {
                                     var DateSelected = moment(e.date._d).format('DD-MM-YYYY');
                                     scope.EventObject.rdate = DateSelected;

                                });
                    }
                };
            });

            app.directive('jtimedatepicker', function () {
                return {
                    restrict: 'A',
                    require: 'ngModel',
                     link: function (scope, element, attrs) {
                          var options = {
                                    locale: 'en',
                                    format: 'HH:mm',
                                    stepping: 15,
                                    defaultDate: new Date().setHours(19)
                                };
                                  var DateTimePicker = $(element).datetimepicker(options);

                                  DateTimePicker.on('dp.change', function (e) {
                                     var DateSelected = moment(e.date._d).format('HH:mm');
                                     scope.EventObject.rtime = DateSelected;

                                });
                    }
                };
            });
        </script>


        </body>

</html>



