<?php
require_once("conf/conf.init.inc.php");
require_once("lib/wpdo.inc.php");
require_once("lib/wglobals.inc.php");
require_once('conf/conf.session.inc.php');
require_once("lib/class.media.inc.php");
require_once("lib/class.restaurant.inc.php");
require_once("lib/class.promotion.inc.php");
require_once("lib/class.analytics.inc.php");
require_once("lib/class.coding.inc.php");
require_once("lib/class.member.inc.php");


$IPaddr = ip2long($_SERVER['REMOTE_ADDR']);

$arglist = array('bksalutation', 'bklast', 'bkfirst', 'bkemail', 'bkcover', 'bkdate', 'bktime', 'bkmobile', 'bkmobilecountry', 'bkcountry', 'bkphone', 'bkrestaurant', 'bktitle', 'bkparam', 'bkspecialrequest', 'brwsr_type', 'bkpage', 'bktracking', 'data', 'booker', 'company', 'hotelguest', 'babychair');
foreach ($arglist as $label)
    $$label = preg_replace("/\'|\"/", "’", $_REQUEST[$label]);


$bkpage = intval($bkpage) + 1;

$fullfeature = preg_match("/TheFunKitchen/", $bkrestaurant) || preg_match("/TheOneKitchen/", $bkrestaurant) || in_array($_SESSION['user']['member_type'], $fullfeature_member_type_allowed) || ($_SESSION['user']['member_type'] == 'weeloy_sales' && $res->status == 'demo_reference' || $res->status == 'active');

if (empty($bkrestaurant)) {
    header("location: https://www.weeloy.com");
    exit;
}

$res = new WY_restaurant;
$res->getRestaurant($bkrestaurant);

if (empty($res->restaurant)) {
    header("location: https://www.weeloy.com");
    exit;
}

// if the booking session is valid and not over 600s

$action = __ROOTDIR__ . "/modules/booking/book_form.php";

$bkparamdecoded = detokenize($bkparam);
if (substr($bkparamdecoded, 0, 3) != date('M') || microtime(true) - floatval(substr($bkparamdecoded, 3)) > 600)
    header("Location: " . $action . "?bkrestaurant=" . $bkrestaurant . "&bktitle=" . $bktitle . "&bktracking=" . $bktracking . "&data=" . $data . "&timeout=y");

$mediadata = new WY_Media();
$logo = $mediadata->getLogo($bkrestaurant);

$restaurant_restaurant_tnc = $res->restaurant_tnc;
$typeBooking = ($res->is_bookable && ($res->status == 'active' || $res->status == 'demo_reference')) ? "true " : "false ";
$is_listing = false;
if (empty($res->is_wheelable) || !$res->is_wheelable) {
    $is_listing = true;
}

$logger = new WY_log("website");
$logger->LogEvent($_SESSION['user']['id'], 802, '', $res->restaurant, '', date("Y-m-d H:i:s"));
?>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html><head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv='expires' content='mon, 1 jan 1998 05:00:00 gmt'>
        <meta http-equiv='pragma' content='no-cache'>
        <meta http-equiv='pragma' content='cache-control: max-age=0'>
        <meta http-equiv='last-modified' content='mon, 1 jan 1998 05:00:00 gmt'>
        <meta http-equiv='cache-control' content='no-cache, must-revalidate'>
        <meta name='robots' content='noindex, nofollow'>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <meta name="HandheldFriendly" content="true" />
        <title>Weeloy Code</title>
        <link href="../../css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../../css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
        <link href="../../css/bootstrap-formhelpers.min.css" rel="stylesheet" media="screen">
        <link href="../../css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="../../css/style.css" rel="stylesheet" type="text/css" />
        <link href='https://fonts.googleapis.com/css?family=Montserrat|Unkempt|Roboto:400,900,700italic,700,500italic,500,400italic' rel='stylesheet' type='text/css'>
<script type='text/javascript' src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular.min.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.0.2/js/bootstrap.min.js"></script>
<script type='text/javascript' src="../../js/ui-bootstrap-tpls-0.12.1.min.js"></script>


        <style>
            body {
                font-family: Roboto; 
                font-size: 14px;
                width:90%;
            }
            div .input-group {
                margin-bottom: 15px;
            }

            div .row {
                margin-bottom: 10px;
            }

            a#buttonterms {
                margin: 10px 30px 20px 55px;
            }

            .termsconditions {
                font-family: Roboto; 
                font-size: 12px;
            }

            img {
                display:block;
                margin:0 auto;
                padding-bottom:20px;
            }


            div .separation {
                vertical-align:middle;
                border-right:4px solid #5285a0;
                height:100%;
            }

<?php if ($brwsr_type != "mobile") echo ".mainbox {  } "; ?>

            .panel{
                border-color:#5285a0 !important;
            }

            .panel-heading{
                background-image:none !important;
                background-color:#5285a0 !important;
                color:#fff !important;
                border-color:#5285a0 !important;
                border-radius:0;
            }

            .btn-default{
                background-image:linear-gradient(to bottom, #eee 0px, #e0e0e0 100%)!important;
            }

        </style>
<?php include_once("ressources/analyticstracking.php") ?>
    </head>
    <body ng-app="myApp"  style="width: 100%;min-height: 410px;background-color: transparent;">
        <form class='form-horizontal' action='confirmation.php' role='form' id='book_form' name='book_form' method='POST' enctype='multipart/form-data'> 
            <?php
            reset($arglist);
            while(list($index, $label) = each($arglist))
            printf("<input type='hidden' id='%s' name='%s' value='%s'>", $label, $label, $$label);
            ?>

            <div id='booking' ng-controller='MainController' ng-init=" ;" >

                <div class="container mainbox" style=" padding-left: 0px;padding-right: 0px;">
                    <div class="panel panel-info" style='margin-bottom: 0px;'>
                        <div style=" background: #303030 none repeat scroll 0 0;color: white;" class="panel-body" >
                            <div class='row'><h3 style='padding-bottom:3px;margin-bottom:3px;'>Review and Confirm</h3></div>
                            
                            <div class='row'>&nbsp; <b>Restaurant</b>:  <?php echo $bktitle ?></div>
                            <div class='row'>&nbsp; <b>Date</b>:  <?php echo $bkdate ?></div>
                            <div class='row'>&nbsp; <b>Time</b>: <?php echo $bktime ?></div>
                            <div class='row'>&nbsp; <b>Number of guests</b>: <?php echo $bkcover ?></div>
                            <div class='row'>&nbsp; <b>Name</b>: <?php echo $bksalutation . " " . $bkfirst . " " . $bklast ?></div>
                            <div class='row'>&nbsp; <b>Email</b>: <?php echo $bkemail ?></div>
                            <div class='row'>&nbsp; <b>Mobile</b>: <?php echo $bkmobilecountry . ' ' . $bkmobile ?></div>

                            <?php if (!empty($bkspecialrequest)) { ?>
                                <div class='row'>&nbsp; <b>Request</b>: <?php echo $bkspecialrequest ?></div>
                            <?php } ?>


                            <div class="input-group" style='margin-bottom: 5px!important;'>
                                <a style='padding:5px' href="javascript:book_form.submit();" class="btn {{buttonClass}}"> {{ buttonLabel}} </a>
                                <label class="checkbox-inline termsconditions"  style='color:#fff; margin-left:15px' >
                                    <a href="javascript:modify();"  style='font-size:13px;'>click to modify </a>
                                </label>
                            </div>
                                
                        </div>                     
                    </div>  
                </div>
            </div>
        </form>
        <script>

<?php
echo "var typeBooking = $typeBooking;";
echo "var action = '$action';";
printf("var is_listing = %s;", $is_listing ? 'true' : 'false');
?>

            var app = angular.module("myApp", []);

            app.controller('MainController', function ($scope, $http) {

                $scope.bookingTitle = (typeBooking) ? "BOOKING SUMMARY" : "REQUEST SUMMARY";
                $scope.buttonLabel = (typeBooking) ? 'BOOK NOW' : 'REQUEST YOUR WEELOY CODE';
                $scope.buttonClass = (typeBooking) ? 'btn-danger' : 'btn-green';
                $scope.listTags = (typeBooking) ? "Instant Reservation" : "No reservartion";
                $scope.listTags2 = (typeBooking) ? "" : "First come, first serve";
                $scope.listTags3 = (!is_listing) ? "Spin the Wheel" : "Enjoy incredible";
                $scope.listTags4 = (!is_listing) ? "at the restaurant" : " promotion";

            });

            function modify() {

                $('#book_form').attr('action', action);
                $('#book_form').submit();
            }

            $(document).ready(function () {

            });

        </script>
    </body>
</html>

