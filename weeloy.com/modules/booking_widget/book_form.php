<?php
require_once("lib/Browser.inc.php");
$browser = new Browser();

require_once("conf/conf.init.inc.php");
require_once("lib/wpdo.inc.php");
//require_once("lib/class.session.inc.php");
require_once("conf/conf.session.inc.php");
require_once("lib/wglobals.inc.php");
require_once("lib/class.media.inc.php");
require_once("lib/class.restaurant.inc.php");
require_once("lib/class.promotion.inc.php");
require_once("lib/class.analytics.inc.php");
require_once("lib/class.coding.inc.php");
require_once("lib/class.member.inc.php");

$arglist = array('bksalutation', 'bklast', 'bkfirst', 'bkemail', 'bkcover', 'bkdate', 'bktime', 'bkmobile', 'bkcountry', 'bkphone', 'bkrestaurant', 'bktitle', 'bkparam', 'bkspecialrequest', 'brwsr_type', 'bkpage', 'bktracking', 'data', 'booker', 'company', 'hotelguest', 'babychair');
foreach ($arglist as $label) {
	if(empty($_REQUEST[$label])) $_REQUEST[$label] = $$label = "";
    else $$label = preg_replace("/\'|\"/", "’", $_REQUEST[$label]);
    }

if(empty($bkrestaurant)) {
	header("location: https://www.weeloy.com");
	exit;
	}

$bkparam = date('M') . strval(microtime(true));
$bkparam = tokenize($bkparam);
$timeout = (!empty($_REQUEST['timeout'])) ? $_REQUEST['timeout'] : "";

$res = new WY_restaurant;
$res->getRestaurant($bkrestaurant);

if(empty($res->restaurant)) {
	header("location: https://www.weeloy.com");
	exit;
	}

$maxpax = $res->dfmaxpers;
$minpax = $res->dfminpers;

if ($minpax < 1 || $minpax > 3) $minpax = 1;
if ($maxpax < 4) $maxpax = 4;
	
$AvailperPax = ($res->perPaxBooking()) ? "1":"0";
	
//tracking 
//test tracking cookie
$logger = new WY_log("website");
$logger->LogEvent($_SESSION['user']['id'], 801, '', $res->restaurant, '', date("Y-m-d H:i:s"));


// book / request process
$action = 'book';
if(filter_input(INPUT_GET, $action, FILTER_SANITIZE_STRING) == 'request'){
    $action = 'request';
}

//get mobile international
$bkmobile = filter_input(INPUT_POST, 'bkmobile', FILTER_SANITIZE_STRING);
$bkmobilecountry2 = filter_input(INPUT_POST, 'bkmobilecountry', FILTER_SANITIZE_STRING);
$bkmobile_international = $bkmobilecountry2 . $bkmobile;



$fullfeature = preg_match("/TheFunKitchen/", $bkrestaurant) || preg_match("/TheOneKitchen/", $bkrestaurant) || in_array($_SESSION['user']['member_type'], $fullfeature_member_type_allowed) || ($_SESSION['user']['member_type'] == 'weeloy_sales' && $res->status == 'demo_reference' || $res->status == 'active');

$typeBooking = ($res->is_bookable && ($res->status == 'active' || $res->status == 'demo_reference')) ? "true" : "false";
$is_listing = false;
if(empty($res->is_wheelable) || !$res->is_wheelable){
    $is_listing = true;
}

$nginit = "typeBooking=" . $typeBooking . ";";

$mediadata = new WY_Media($bkrestaurant);
$logo = $mediadata->getLogo($bkrestaurant);

$restaurant_restaurant_tnc = $res->restaurant_tnc;

$date = date_create(null, timezone_open("Asia/Singapore"));
$data_min = date_format($date, "d/m/Y");
date_add($date, date_interval_create_from_date_string("60 days"));
$data_max = date_format($date, "d/m/Y");

if (empty($bkdate))
    $bkdate = $data_min;

if (empty($bktime))
    $bktime = "19:00";

$pickDate = substr($bkdate, 6, 4) . "-" . substr($bkdate, 3, 2) . "-" . substr($bkdate, 0, 2);
$objToday = new DateTime(date("Y-m-d"));
$objDateTime = new DateTime($pickDate);
$ndays = $objToday->diff($objDateTime)->format('%a');

$actionfile = $_SERVER['PHP_SELF'];

$brwsr_type = ($browser->isMobile() || (isset($_REQUEST['brwsr_type']) && $_REQUEST['brwsr_type'] == "mobile")) ? "mobile" : "";
if($browser->isTablet()) $brwsr_type = "tablette";

$iphoneflg = (strtolower($browser->getPlatform()) == "iphone");
$termsconditions = "https://www.weeloy.com/modules/templates/tnc/fullterms.php"; // force to reload. Refresh does not force it on modal.

//$validISOCode = array('CN' => "China", 'HK' => "Hong Kong", 'ID' => "indonesia", 'JP' => "Japan", 'ML' => "Malaysia", 'SG' => "Singapore", 'TH' => "Thailand");
//$countrycode = array('86' => 'China', '852' => 'Hong Kong', '62' => 'indonesia', '81' => 'Japan', '60' => 'Malaysia', '65' => 'Singapore', '66' => 'Thailand');


$startingPage = (empty($bkpage)) ? 1 : 0;

if(empty($bkpage)) {
	$userlastname = (isset($_SESSION['user']['lastname'])) ? $_SESSION['user']['lastname'] : "";
	$userfirstname = (isset($_SESSION['user']['lastname'])) ? $_SESSION['user']['firstname'] : "";
	$useremail = (isset($_SESSION['user']['email'])) ? $_SESSION['user']['email'] : "";
	$usermobile = (isset($_SESSION['user']['mobile'])) ? $_SESSION['user']['mobile'] : "";
	$usergender = (isset($_SESSION['user']['gender']) && $_SESSION['user']['gender'] == 'f') ? "Mrs." : "Mr.";
//	if(isset($_SESSION['user']['country']) && in_array($_SESSION['user']['country'], array_keys($validISOCode)))
//		$bkcountry = $validISOCode[$_SESSION['user']['country']];

	
	if (empty($bkfirst) && !empty($userfirstname))
		$bkfirst = $userfirstname;
	if (empty($bklast) && !empty($userlastname))
		$bklast = $userlastname;
	if (empty($bkemail) && !empty($useremail))
		$bkemail = $useremail;
//	if (empty($bkmobile) && !empty($usermobile)) 
//		if(preg_match('/\+(86|852|62|81|60|65|66) /', $usermobile, $match)) {
//			$bkcountry = $countrycode[$match[1]];
//			$bkmobile = $usermobile;
//			}
//	if(empty($bkmobile) && !empty($bkcountry))
//		$bkmobile = "+" . array_search($bkcountry, $countrycode) . " ";
	if (empty($bksalutation) && !empty($usergender))
		$bksalutation = $usergender;
	$bkpage = 1;
}

?>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv='expires' content='mon, 1 jan 1998 05:00:00 gmt'>
<meta http-equiv='pragma' content='no-cache'>
<meta http-equiv='pragma' content='cache-control: max-age=0'>
<meta http-equiv='last-modified' content='mon, 1 jan 1998 05:00:00 gmt'>
<meta http-equiv='cache-control' content='no-cache, must-revalidate'>
<meta name='robots' content='noindex, nofollow'>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="HandheldFriendly" content="true" />
<title>Book your table now - Weeloy Code - Weeloy.com</title>

<link href="../../css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="../../css/style.css" rel="stylesheet" >
<link href="../../css/bootstrap-social.css" rel="stylesheet" >

<link href="build/css/intlTelInput.css" rel="stylesheet">

<script type='text/javascript' src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular.min.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.0.2/js/bootstrap.min.js"></script>
<script type='text/javascript' src="../../js/ui-bootstrap-tpls-0.12.1.min.js"></script>




<style>

body {
	font-family: Roboto; 
	font-size: 14px;
	width:90%;
}

div .input-group {
	margin-bottom: 15px;
}

img {
	display:block;
	margin:0 auto;
	padding-bottom:20px;
}

a#buttonterms {
	margin: 10px 30px 20px 55px;
}

.termsconditions {
	font-family: Roboto; 
	font-size: 12px;
}

div .separation {
	vertical-align:middle;
	border-right:4px solid #5285a0;
	height:100%;
}
.mainbox { margin: 0px; width:300px; }

.panel{
	border-color:#5285a0 !important;
}

.panel-heading{
	background-image:none !important;
	background-color:#5285a0 !important;
	color:#fff !important;
	border-color:#5285a0 !important;
	border-radius:0;
}

.btn-default{
	background-image:linear-gradient(to bottom, #eee 0px, #e0e0e0 100%)!important;
}
.dropdown-menu {
	min-width: 107px;
}


.dropdown-submenu>.dropdown-menu {
	top: 0;
	left: 100%;
	margin-top: -6px;
	margin-left: -1px;
	-webkit-border-radius: 0 6px 6px 6px;
	-moz-border-radius: 0 6px 6px;
	border-radius: 0 6px 6px 6px;
}

.dropdown-submenu:hover>.dropdown-menu {
	display: block;
}

.dropdown-submenu>a:after {
	display: block;
	content: " ";
	float: right;
	width: 0;
	height: 0;
	border-color: transparent;
	border-style: solid;
	border-width: 5px 0 5px 5px;
	border-left-color: #ccc;
	margin-top: 5px;
	margin-right: -10px;
}

.dropdown-submenu:hover>a:after {
	border-left-color: #fff;
}

.dropdown-submenu.pull-left {
	float: none;
}

.dropdown-submenu.pull-left>.dropdown-menu {
	left: -100%;
	margin-left: 10px;
	-webkit-border-radius: 6px 0 6px 6px;
	-moz-border-radius: 6px 0 6px 6px;
	border-radius: 6px 0 6px 6px;
}
#input-group-date-picker > ul{
    left:0px!important;
    
}

#input-group-date-picker .btn-sm{
    padding: 5px 9px;
}

#input-group-date-picker .btn-default{
    background-image:linear-gradient(to bottom, #eee 0px, #eee 100%)!important;
}

.scrollable-menu {
    height: auto;
    max-height: 250px;
    overflow-x: hidden;
}

</style>
<?php include_once("ressources/analyticstracking.php") ?>
</head>

    <body ng-app="myApp">
        <form class='form-horizontal' action='<?php echo $actionfile ?>' role='form' id='book_form' name='book_form' method='POST' enctype='multipart/form-data'>

            <div id='booking' ng-controller='MainController' ng-init="typeBooking = true;" >

                <div class="container mainbox" style="padding:0px;">    
                    <div class="panel panel-info" style='margin-bottom:0px'>
                        
                        <div style="background-color: #303030; padding-top:5px; padding-bottom:2px; " class="panel-body" >
							<input type='hidden' id='fbtoken' name='fbtoken'>
							<input type='hidden' id='fbemail' name='fbemail'>
							<input type='hidden' id='fbname' name='fbname'>
							<input type='hidden' id='fblastname' name='fblastname'>
							<input type='hidden' id='fbfirstname' name='fbfirstname'>
							<input type='hidden' id='fbuserid' name='fbuserid'>
							<input type='hidden' id='fbtimezone' name='fbtimezone'>
							<input type='hidden' id='userid' name='userid'>
							<input type='hidden' id='token' name='token'>
							<input type='hidden' id='timezone' name='timezone'>
							<input type='hidden' id='application' name='application' value='html'>
							<input type='hidden' id='socialname' name='socialname' value=''>
                                                        
                                                        <input type='hidden' id='bkmobilecountry' name='bkmobilecountry' value=''>
                                                        
                            <input type='hidden' id='bkrestaurant' name='bkrestaurant' value='<?php echo $bkrestaurant ?>'>
                            <input type='hidden' id='bkpage' name='bkpage' value='<?php echo $bkpage ?>'>
                            <input type='hidden' id='bktitle' name='bktitle' value='<?php echo $bktitle ?>'>
                            <input type='hidden' id='bkphone' name='bkphone' value='<?php echo $bkphone ?>'>
                            <input type='hidden' id='bkparam' name='bkparam' value='<?php echo $bkparam ?>'>
                            <input type='hidden' id='bktracking' name='bktracking' value='<?php echo $bktracking ?>'>
                            <input type='hidden' id='brwsr_type' name='brwsr_type' value='<?php echo $brwsr_type ?>'>


                    <div class="row">
                    
                    <div class="input-group" id='input-group-date-picker'>
                            <span class="input-group-addon" ng-click="open($event)"><i class="glyphicon glyphicon-calendar"></i> &nbsp; <span class='caret'></span></span>
                            <input type="text" style='left:-1px!important' class="form-control input" id='bkdate' name='bkdate' datepicker-popup="{{format}}" ng-model="bkdate" ng-focus="open($event)" 
                            is-open="start_opened" min-date="minDate" max-date="maxDate" datepicker-options="dateOptions" date-disabled="disabled(date, mode)" 
                            ng-required="true" ng-change='updatendays();' close-text="Close" readonly />
                    </div></div>


<?php if ($brwsr_type != "mobile" && $brwsr_type != "tablette" ) : ?>				
                                    <div class='input-group'>
                                        <div class='input-group-btn '>
                                            <button type='button' id='itemdftime' class='btn btn-default btn dropdown-toggle' data-toggle='dropdown'>
                                                &nbsp;<i class="glyphicon glyphicon-time"></i> &nbsp; <span class='caret'></span>
                                            </button>
                                            <ul class="dropdown-menu multi-level dropdown_itemdftime" role="menu" aria-labelledby="dropdownMenu">
                                                <li class="dropdown-submenu">
                                                    <a href="javascript:;">Lunch</a>
                                                    <ul class="dropdown-menu scrollable-menu">
                                                        <li ng-repeat="x in lunch"><a href="javascript:;" ng-click="setmealtime(x, 1);">{{ x }}</a></li>
                                                    </ul>
                                                </li>
                                                <li class="dropdown-submenu">
                                                    <a href="javascript:;">Dinner</a>
                                                    <ul class="dropdown-menu scrollable-menu">
                                                        <li ng-repeat="y in dinner"><a href="javascript:;" ng-click="setmealtime(y, 1);">{{ y }}</a></li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>
                                        <input type='text' value='' class='form-control input' id='bktime' name='bktime' ng-model='ngbktime' readonly>
                                    

<?php else : ?>				
                                <div class='input-group'>
                                    <div class='input-group-btn '>
                                        <button type='button' id='itemdftime' class='btn btn-default btn dropdown-toggle' data-toggle='dropdown'>
                                             &nbsp;<i class="glyphicon glyphicon-time"></i><span class='caret'></span>
                                        </button>
                                        <ul class='dropdown-menu dropdown_itemdftime scrollable-menu'>
                                            <li ng-repeat="x in lunch"><a href="javascript:;" ng-click="setmealtime(x, 1);">{{ x }}</a></li>
                                            <li>   -----------------------------  </li>
                                            <li ng-repeat="y in dinner"><a href="javascript:;" ng-click="setmealtime(y, 1);">{{ y }}</a></li>
                                        </ul>
                                    </div>
                                    <input type='text' class='form-control input' id='bktime' name='bktime' readonly >
                                
<?php endif; ?>

                                 
                                    <div class='input-group-btn' style="padding-left:20px;">
                                        <button type='button' id='itemdfminpers' class='btn btn-default btn dropdown-toggle' data-toggle='dropdown' style="margin-right: -1px;border-bottom-right-radius: 0;border-top-right-radius: 0;">
                                            &nbsp;<i class="glyphicon glyphicon-cutlery"></i> <span class='caret'></span>
                                        </button>
                                        <ul class='dropdown-menu dropdown_itemdfminpers'>
                                            <li ng-repeat="p in npers"><a href="javascript:;" ng-click="setmealtime(p, 2);">{{ p }}</a></li>
                                        </ul></div>
                                    <input type='text' ng-model='bkcover' class='form-control input' id='bkcover' name='bkcover' readonly >
                                </div>

                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                                    <input type="text" class="form-control input sm" id='bkemail' name='bkemail' placeholder="email" onchange='CleanEmail(this);'>                                        
                                </div>

                                <!--<div class='input-group'>
                                    <div class='input-group-btn '>
                                        <button type='button' id='itemdfsalut' class='btn btn-default btn dropdown-toggle' data-toggle='dropdown'>
                                            Salutation <span class='caret'></span>
                                        </button>
                                        <ul class='dropdown-menu dropdown_itemdfsalut'>
                                            <li ng-repeat="s in salutations"><a href="javascript:;" ng-click="setmealtime(s, 3);">{{ s}}</a></li>
                                        </ul></div>
                                    <input type='text' class='form-control input' id='bksalutation' name='bksalutation' readonly >
                                </div>-->

                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                    <input type="text" class="form-control input" id='bkfirst' name='bkfirst' placeholder="firstname" onchange='CleanText(this);'>                                        
                                </div>

                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                    <input type="text" class="form-control input" id='bklast' name='bklast' placeholder="lastname" onchange='CleanText(this);'>                                        
                                </div>

                                
                            
                                <div class="input-group" style='margin-bottom:15px;width: 100%;'>
                                     <input  id='bkmobile' name='bkmobile' type="tel">
                                </div>                       
      

                                <div class="input-group" style='margin-bottom:15px;'>
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-info-sign"></i></span>
                                    <textarea type="text" class="form-control input" id='bkspecialrequest' name='bkspecialrequest' rows="1"  placeholder="Special requests" onchange='CleanText(this);'></textarea>
                                </div>
                                <div class="input-group" style='margin-bottom: 5px!important;'>
                                    <a style='padding:5px' href="javascript:checkSubmit();" class="btn {{buttonClass}}"> {{ buttonLabel}} </a>
                                        <label class="checkbox-inline termsconditions"  style='color:#fff; margin-left:15px' >
                                            <input type="checkbox" value="" checked id='bkchtc' name='bkchtc' style="display:none" >By clicking you agree to the <br/>
                                            <a target='_blank' href= <?php echo "'" . $termsconditions . "'"; ?> id='' style='font-size:13px;'>terms and conditions </a>
                                        </label>
                                        
                                    </div>
                                </div>
                        </div>  
                    </div>
                </div>

            </div>

        </form>

<script>

<?php
echo "var tracking = '$bktracking';";
printf("var fullfeature = %s;", ($fullfeature) ? "true" : "false"); 
echo "var resto = '$bkrestaurant';";
echo "var minpax = '$minpax';";
echo "var maxpax = '$maxpax';";
echo "var curndays = '$ndays';";
echo "var curdate = '$bkdate';";
echo "var curtime = '$bktime';";
//echo "var curcountry = '$bkcountry';";
echo "var typeBooking = $typeBooking;";
echo "var timeout='$timeout';";
echo "var AvailperPax = '$AvailperPax';";	// true or false
echo "var startingPage = '$startingPage';";	// true or false

printf("var is_listing = %s;", $is_listing ? 'true' : 'false');
printf("var remainingday = %d; ", 364 - intval(date("z")));

?>

$(document).ready(function() { updatefield(); });

var app = angular.module("myApp", ['ui.bootstrap']);

Date.prototype.getDateFormat = function(sep) {
	var d = this.getDate(), m = this.getMonth()+1, y = this.getFullYear();
	if(typeof sep !== 'string' || (sep !== '/' && sep !== '-')) sep = '-';
	return ((d <= 9) ? '0' : '') + d + sep + ((m <= 9) ? '0' : '') + m + sep + y;
}

Date.prototype.getDateFormatReverse = function(sep) {
	var d = this.getDate(), m = this.getMonth()+1, y = this.getFullYear();
	if(typeof sep !== 'string' || (sep !== '/' && sep !== '-')) sep = '-';
	return y + sep + ((m <= 9) ? '0' : '') + m + sep + ((d <= 9) ? '0' : '') + d;
}
	
app.service('HttpServiceAvail',['$http',function($http){
	this.checkAvail = function(date, time, pers) {

		date = this.normDate(date, true, '-');
		return $http.post("../../api/restaurant/availslot/",
			{
				'restaurant': restaurant,
				'date' 	: date,
				'time' 	: time,
				'pers' 	: pers,
				'token'	: token
			}).then(function(response) { return response.data;})
		};
		
	this.normDate = function (dd, reverse, sep) {
		if(sep !== '-' && sep !== '/') sep = '-';
		if (typeof dd !== "string")
			return (reverse) ? dd.getDateFormatReverse(sep) : dd.getDateFormat(sep);
			
		dd = (sep == '-') ? dd.replace(/\//g, "-") : dd.replace(/-/g, "/");
		aa = dd.split(sep);
		return (reverse) ? aa[2] + sep + aa[1] + sep + aa[0] :  aa[0] + sep + aa[1] + sep + aa[2];
	};
	
}]);


app.controller('MainController', function($scope, $http, HttpServiceAvail) {

	$scope.bookingTitle = (typeBooking) ? "BOOK YOUR TABLE" : "GET YOUR WEELOY CODE";
	if(timeout == "y") $scope.bookingTitle = $scope.bookingTitle + " (time out)";
	$scope.buttonLabel = (typeBooking) ? 'BOOK NOW' : 'REQUEST YOUR WEELOY CODE';
	$scope.buttonClass = (typeBooking) ? 'btn-danger' : 'btn-green';
	$scope.listTags = (typeBooking) ? "Instant Reservation" : "No reservartion";
	$scope.listTags2 = (typeBooking) ? "" : "First come, first serve";
	$scope.listTags3 = (!is_listing) ? "Spin the Wheel" : "Enjoy incredible";
	$scope.listTags4 = (!is_listing) ? "at the restaurant" : " promotion";
	curdateAr = curdate.split('/');
	
	$scope.bkcover = (minpax < 2) ? 2 : minpax;

	if(AvailperPax == '1' && startingPage == "1") {
		$scope.bkcover = "";
		$scope.available = 0;
		}

	$scope.curday = 0;
	$scope.lunchdata = ""; 
	$scope.dinnerdata = ""; 
	$scope.npers = [];
	for(i = minpax; i <= maxpax; i++) $scope.npers.push(String(i));
	$scope.salutations = ['Mr.', 'Mrs.', 'Ms.'];
	//$scope.countries = [<?php $sep = ""; foreach($validISOCode as $key => $value) { echo $sep . "'" . $value . "'"; $sep = ", "; } ?> ];
	//$scope.countrycode = [86, 852, 62, 81, 60, 65, 66 ];
	$scope.currentcc = -1;

//	if(curcountry != '')
//		$scope.currentcc = $scope.countries.indexOf(curcountry);
				
	$http.get("../../api/restaurant/allote/" + resto).success(function(response) { 
		//alert(response.data.lunchdata);
		$scope.lunchdata = response.data.lunchdata;
		$scope.dinnerdata = response.data.dinnerdata;
		$scope.updateTimerScope(curndays);
		});
	
	$scope.numberOfDay = function(aDate) {
		var dateAr = aDate.split('/');
		var date = new Date(dateAr[2], parseInt(dateAr[1]) - 1, dateAr[0], 23, 59, 59);
		return Math.floor(((date.getTime() - Date.now()) / (24 * 60 * 60 * 1000)));
	}

	$scope.updatendays = function() {
		var nday;
		
		$scope.bkdate = HttpServiceAvail.normDate($scope.bkdate, false, '/');
		nday = $scope.numberOfDay($scope.bkdate);
		$scope.updateTimerScope(nday);

		if(AvailperPax == '1') 
			$scope.checkPerPaxData();
	};
		
	$scope.updateTimerScope = function(n) {
	
		$scope.curday = n;
		$scope.lunch = [];
		$scope.dinner = [];
		nchar_lunch = 4;
		nchar_dinner = 4;
		index = n * nchar_lunch;
		patlunch = parseInt('0x' + $scope.lunchdata.substring(index, index + nchar_lunch));
		index = n * nchar_dinner;
		patdinner = parseInt('0x' + $scope.dinnerdata.substring(index, index + nchar_dinner));

		curval = $('#bktime').val();
		found = 0;
		localtime = "";
		limit = nchar_lunch * 4;
		for (log = i = 0, k = 1; i < limit; i++, k *= 2) 
			if ((patlunch & k) == k) {
				log++;
				ht = (9 + Math.floor(i / 2)) + ':' + ((i % 2) * 3) + '0';
				if(ht == curval) found = 1;
				$scope.lunch.push(ht);
				}
		if(log == 0) {
			$scope.lunch.push('not available for lunch');
			localtime = 'not available for lunch';
			}
		
		limit = nchar_dinner * 4;
		//if(limit > 17) limit = 16;	// minight
		for (log = i = 0, k = 1; i < limit; i++, k *= 2) 
			if ((patdinner & k) == k) {
				log++;
				ht = (16 + Math.floor(i / 2)) + ':' + ((i % 2) * 3) + '0';
				if(ht == curval) found = 1;
				$scope.dinner.push(ht);
				}
		if(log == 0) {
			$scope.dinner.push('not available for dinner');
			localtime = 'not available for lunch';
			}

		curval = $('#bktime').val();
		if(localtime.substr(0,5) == 'close') {
			curtime = localtime;
			$('#bktime').val(curtime);
			}
		else if(curval.substr(0,5) == 'close' || found == 0) {
			$('#bktime').val('');
			}
		};	
		
	$scope.setmealtime = function(tt, section) {
		switch(section) {
			case 1: 
				$('#bktime').val(tt); 
				$scope.bktime = tt;
				if(AvailperPax == '1') 
					$scope.checkPerPaxData();					
				break;
			case 2: 
				if(tt.substring(0, 4) == "more")
					if((pers = prompt("How many personnes ?", 10)) != null) {
						pers = parseInt(pers);
						if(pers > 9 && pers < 40)
							tt = pers;
						}
				$('#bkcover').val(tt);
				$scope.bkcover = String(tt);
				if(AvailperPax == '1') 
					$scope.checkPerPaxData();					
				break;
			case 3: 
				$('#bksalutation').val(tt);
				 break;
			case 4: 
				if(tt < 0 && tt >= $scope.countrycode.length) {
					alert('Invalid Country Code');
					return false;
					}
				$scope.currentcc = tt;
				/*$('#bkcountry').val($scope.countries[tt]);
				val = $('#bkmobile').val();
				val = val.replace(/^[0-9] \+/g, "");
				if(val.match(/^\+\d{2,3}/) != null) {
					val = val.replace(/\+(86|852|62|81|60|65|66)/, "");
					val = '+' + $scope.countrycode[tt] + '  ' + val;
					}
				else if(val.match(/^[^\+]/) != null) {
					val = val.replace(/\+/g, "");
					val = '+' + $scope.countrycode[tt] + ' ' + val;
					}
				val = val.replace(/ +/g, " ");
				$('#bkmobile').val(val); */
				break;
			default:
			}
		}
	
	$scope.checkPerPaxData = function() {

		$scope.available = 0;			
		dateflg = (typeof $scope.bkdate !== "undefined" && (($scope.bkdate instanceof Date) || (typeof $scope.bkdate === "string" && $scope.bkdate.length > 7)));
		timeflg = (typeof $scope.bktime === "string" && $scope.bktime.length > 3);
		persflg = (typeof $scope.bkcover === "string" && parseInt($scope.bkcover) > 0);
		if((dateflg && timeflg && persflg) == false)
			return;
			
		HttpServiceAvail.checkAvail($scope.bkdate, $scope.bktime, $scope.bkcover).then(function(response) {  
			if(response.data == "1") $scope.available = 1; 
			else if(response.count > 0) { alert($scope.zang['error10'].vl + ". " + response.count + " " + $scope.zang['error11'].vl); }
			else { alert($scope.zang['error9'].vl); }
			console.log(response); 
			});
		}
		
	$scope.checkvalidtel = function() {
		val = $('#bkmobile').val();
		if(val == "") return;
		
		tt = $scope.currentcc;
		//$('#bkcountry').val($scope.countries[tt]);
		/*val = $('#bkmobile').val();
		val = val.replace(/^[0-9] \+/g, "");
		if(val.match(/\+(86|852|62|81|60|65|66)/) != null) {
			val = val.replace(/\+(86|852|62|81|60|65|66)/, "");
			val = '+' + $scope.countrycode[tt] + '  ' + val;
			}
		else  {
			val = val.replace(/\+/g, "");
			val = '+' + $scope.countrycode[tt] + ' ' + val;
			}
		val = val.replace(/ +/g, " ");
		$('#bkmobile').val(val); */
		}
			
	$scope.formats = ['dd-MM-yyyy', 'dd/MMMM/yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'dd.MM.yyyy', 'shortDate'];
	$scope.format = $scope.formats[3];
	$scope.minDate = new Date();
	$scope.maxDate = new Date();
	$scope.maxDate.setTime($scope.maxDate.getTime() + (60 * 24 * 3600 * 1000));	// remainingday  120 days
	$scope.bkdate = curdate;

	$scope.disabled = function(date, mode) {
		return false;
	//return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
	};

	$scope.open = function($event) {
		$( "#bkspecialrequest").trigger( "click" );
               // $( "#bk").trigger( "click" );
		$scope.start_opened = true;

		$event.preventDefault();
		$event.stopPropagation();

	};

	$scope.dateOptions = {
		formatYear: 'yy',
		startingDay: 1
	};
	
});

</script>

<!--
<script>

submitflg = false;
function getUserData() {
	FB.api('/me', function(response) {
		$('#status_fb').val('Hello ' + response.name);
		if(response.name != "") {
			console.log(response);
			$('#fbuserid').val(response.id);
			$('#fbemail').val(response.email);
			$('#fbname').val(response.name);
			$('#fbfirstname').val(response.first_name);
			$('#fblastname').val(response.last_name);
			$('#fbtimezone').val(response.timezone);
			}
		if($('#fbuserid').val() != '' && submitflg) {
			$('#application').val('facebook');
			updateinput();
			getWeeloyUser(); 
			}
	});
}
 
window.fbAsyncInit = function() {
	//SDK loaded, initialize it
	FB.init({
		appId      : '1590587811210971',
		xfbml      : true,
		version    : 'v2.2'
	});
 
	//check user session and refresh it
	FB.getLoginStatus(function(response) {
		if (response.status === 'connected') {
			//user is authorized
			//document.getElementById('loginBtn').style.display = 'none';
			if(typeof response.authResponse !== 'undefined')
				$('#fbtoken').val(response.authResponse.accessToken);

			submitflg = false;
			getUserData();
		} else {
			//user is not authorized
		}
	});
};
 
//load the JavaScript SDK
(function(d, s, id){
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) {return;}
	js = d.createElement(s); js.id = id;
	js.src = "//connect.facebook.net/en_US/sdk.js";
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
 
//add event listener to login button
document.getElementById('loginBtn').addEventListener('click', function() {
	//do the login
	FB.login(function(response) {
		if (response.authResponse) {
			//user just authorized your app
			//document.getElementById('loginBtn').style.display = 'none';
			if(typeof response.authResponse !== 'undefined')
				$('#fbtoken').val(response.authResponse.accessToken);

			if($('#fbtoken').val().length > 10)
				submitflg = true;
			getUserData();
		}
	}, {scope: 'email,public_profile', return_scopes: true});
}, false);

</script>
-->
<script>

function updateinput() { 
    	$('#bkemail').val($('#fbemail').val());
    	$('#bkfirst').val($('#fbfirstname').val());
    	$('#bklast').val($('#fblastname').val());
    	$('#userid').val($('#fbuserid').val());
    	$('#token').val($('#fbtoken').val());
}

function getWeeloyUser() {

	email = $('#bkemail').val();
	userid = $('#userid').val();
	token = $('#token').val();
	
	apiurl = "../../api/facebookuser/" + email + "/" + userid + "/" + token;
	console.log(apiurl);
	$.ajax({
		url : apiurl,
		type: "GET",
		success: function(data, textStatus, jqXHR) { 
				if(data.data.mobile.length > 7) {
					mobile = data.data.mobile.substring(0, 3) + ' ' + data.data.mobile.substring(3);
		    		$('#bkmobile').val(mobile);
                                
		    		}
		    	country = data.data.country;
		    	if(country.length > 5) 
		    		if(country == 'Singapore' || country == 'Thailand' || country == 'Malaysia' || country == 'Hong Kong')
		    			$('#bkcountry').val(country);
		 		},
			error: function (jqXHR, textStatus, errorThrown) { /* alert("Unknown Error. " + textStatus); */ }
		});		
}

function termsconditions() {  $('#remember').toggleClass('show'); }

function IsEmail(email) {
var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		return regex.test(email);
}

<?php
echo "var arglist = [";
$sep = "";
for (reset($arglist); list($index, $label) = each($arglist); $sep = ", ") {
    echo $sep . "'" . $label . "', '" . $$label . "'";
}
echo "];";
?>

function updatefield() {
for (i = 0; i < arglist.length; i += 2) if (arglist[i + 1] != '')  {
	if($('#' + arglist[i]).is(':checkbox'))
		$('#' + arglist[i]).click();
	else $('#' + arglist[i]).val(arglist[i + 1]);
	}

	$('#bktime').val(curtime); // in case it is closed
}


function CleanEmail(obj) { obj.value = obj.value.replace(/[!#$%^&*()=}{\]\[:;><\?/|\\]/g, ''); }
function CleanTel(obj) { obj.value = obj.value.replace(/[^0-9 \+]/g, ''); }
function CleanPass(obj) { obj.value = obj.value.replace(/[^0-9A-Za-z]/g, ''); }
function CleanText(obj) { obj.value = obj.value.replace(/[!@#$%^&*()=}{\]\[\"\':;><\?/|\\]/g, ' '); }
function invalid(id, str) { alert(str); $(id).focus(); }
function checkSubmit() {

	$('#book_form input[type=text], input[type=password]').each(function() {
		this.value = this.value.trim();
		});
	bkdate = $('#bkdate').val();
	if (bkdate == "") {
		return invalid('#bkdate', 'Invalid date, try again');
		}
	
	var nday = $scope.numberOfDay($scope.bkdate);
	if(isNaN(nday) || nday > 60 || nday < 0)
		return invalid('#bkdate', 'Invalid date, try again');

	if ($('#bktime').val() == "" || $('#bktime').val().indexOf("not available") > -1){
		return invalid('#bktime', 'Invalid time, try again');
            }
	if ($('#bktime').val().substr(0,6) == "closed")
		return invalid('#bktime', 'The restaurant is ' + $('#bktime').val() +', try another time/day');
	if ($('#bkemail').val() == "" || !IsEmail($('#bkemail').val()))
		return invalid('#bkemail', 'Invalid email, try again');
	if ($('#bkfirst').val() == "")
		return invalid('#bkfirst', 'Invalid firstname, try again');
	if ($('#bklast').val() == "")
		return invalid('#bklast', 'Invalid name, try again');
        
        if (!$("#bkmobile").intlTelInput("isValidNumber")){
                return invalid('#bkmobile', 'Invalid mobile number, try again');
            }else{
                var country = $("#bkmobile").intlTelInput("getSelectedCountryData");
                $("#bkmobilecountry").val('+'+country.dialCode);
            }
            
            
	if ($('#bkchtc').is(':checked') == false)
		return invalid('#bkchtc', 'You need to approve the terms and conditions in order to reserve a table');
        
        if ($('#bkcover').val() != "" && $('#bkcover').val() == 'more...')
		$('#bkcover').val(10);
            
	
        
	if(tracking != '') {
		removelgcookie("weeloy_be_tracking");
		setlgcookie("weeloy_be_tracking", tracking, 1);
		}
		
	if(fullfeature) {
		$('#book_form').attr('action', 'prebook.php'); 
                //$('#bkcountry').val('+65');
                //$('#bkmobile').val( $('#bkmobile').intlTelInput("getNumber"));
	
            book_form.submit();
		return false;
		}
	else {
		alert('Reservation will be open on Sept 31, 2015');
		return false;
		}
	
    }

function setlgcookie(name, value, duration){
	value = escape(value);
	if(duration){
		var date = new Date();
		date.setTime(date.getTime() + (duration * 1 * 3600*1000));
		value += "; expires=" + date.toGMTString();
	}
	document.cookie = name + "=" + value;
}

function getlgcookie(name){
	value = document.cookie.match('(?:^|;)\\s*'+name+'=([^;]*)');
	return value ? unescape(value[1]) : "";
}

//Removes the cookies
function removelgcookie(name) { setlgcookie(name, '', -1); }

</script>
        
        
        
    <script src="build/js/intlTelInput.js"></script>
    <script>
      $("#bkmobile").intlTelInput({
        //allowExtensions: true,
        //autoFormat: false,
        autoHideDialCode: false,
        //autoPlaceholder: false,
        //defaultCountry: "auto",
        //ipinfoToken: "yolo",
        //nationalMode: false,
        //numberType: "MOBILE",
        onlyCountries: ['sg', 'th', 'hk', 'cn', 'au', 'my', 'jp', 'id', 'tw'],
        preferredCountries: ['sg', 'th', 'hk', 'cn', 'au'],
        utilsScript: "lib/libphonenumber/build/utils.js"
      });
      $("#bkmobile").intlTelInput("setNumber", "<?php echo $bkmobile_international;?>");
    </script>

</body>
</html>