
<?php
// get ip and block

require_once("conf/conf.init.inc.php");
require_once("lib/wpdo.inc.php");
require_once("lib/wglobals.inc.php");
require_once("lib/class.booking.inc.php");
require_once("conf/conf.session.inc.php");


//$confirmation = @$_REQUEST['refid'];
//$email = @$_REQUEST['uid'];
    $qString_array = array();
    parse_str(@$_SERVER['QUERY_STRING'], $qString_array);
    //var_dump($_POST);
    $booking = new WY_Booking();

    $email = $booking->base64url_decode($qString_array['uid']);
    $confirmation = $booking->base64url_decode($qString_array['refid']);


$siteUrl = __SERVERNAME__ . __ROOTDIR__ . "/event/singapore/food-selfie-contest-2015/";

$actual_link = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

if (strpos($actual_link, 'modules/photo_contest/') !== false) {
    echo '<script>window.location.href = "https://www.weeloy.com/event/singapore/food-selfie-contest-2015/"; </script>';
    die();
}

$fbId = WEBSITE_FB_APP_ID;
$mediaUrl = __S3HOST__;
//$fbId = WEBSITE_FB_APP_ID;
?>
<!DOCTYPE html>
<html lang="en" xml:lang="en" xmlns:fb="http://ogp.me/ns/fb#" >
    <head>
        <title>Weeloy - Food photo contest 2015 - Book and Win</title>
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" type="text/css" href="css/style-new1.css">

        <link rel="stylesheet" type="text/css" href="../../../css/styles_front.css">
        <link rel="stylesheet" type="text/css" href="../../../css/new_layout.css">

        <script type="text/javascript" src="js/jquery-1.9.1.min.js" ></script>
        <link rel="stylesheet" type="text/css" href="css/popupwindow.css">

        <link href='https://fonts.googleapis.com/css?family=Lora:400,700' rel='stylesheet' type='text/css'>
        <script src="js/popupwindow.js"></script>
        
        <meta property="og:title" content="Weeloy - Food photo contest 2015 - Book and Win" />
        <meta property="og:description" content="Dine around Singapore  & Share your Food Photo Be a food explorer, Challenge youself and your buddies to dine at various restaurants listed on Weeloy.com! It’s easy to snap, share and win!Winning your dream holiday is as easy as 1, 2, 3!" />
        <meta property="og:image:width" content="450"/>
        <meta property="og:image:height" content="298"/>
        <meta property="og:type" content="website" />
        <meta property="og:image" content="https://static.weeloy.com/images/food-selfie-content-2015/contest_image_header_light.jpg" />

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <script>
            function validateEmail(email) {
                var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(email);
            }
            function verifyForm() {

                var res = true;
                if (!validateEmail($('#email').val()))
                {
                    $('#div-email').attr("class", "input-group has-error");
                    res = false;
                }
                else
                    $('#div-email').removeClass("has-error");
                if (false && $('#confirmation').val().length < 3)
                {
                    $('#div-confirmation').attr("class", "input-group has-error");
                    res = false;
                }
                else
                    $('#div-confirmation').removeClass("has-error");    //div-displayname

                if ($('#name').val().length < 3)
                {

                    $('#div-displayname').attr("class", "input-group has-error");
                    res = false;
                }
                else
                    $('#div-displayname').removeClass("has-error");


                return res;

            }
            function addFiles(evt) {
                var res = verifyForm();

                if (res === false) {
                    //$("html, body").animate({scrollTop: "0px"});
                    return false;
                } else {
                    $('#chk-agree').prop("checked", true);


                    var formData = new FormData($(this).parents('form')[0]);
                    // /foodselfie/updatephoto
                    $.ajax({
                        url: '../../../api/uploadphoto/selfie-contest',
                        type: 'POST',
                        xhr: function () {
                            var myXhr = $.ajaxSettings.xhr();
                            return myXhr;
                        },
                        beforeSend: function (xhr) {

                            //$(placeholder).addClass('loading');
                            $("#ajaxloader").show();
                            $("#errorAjax").html('');
                        },
                        success: function (data) {
                            
                            location.reload();
                            
                            var html = "";
                            if (data.data === 1) {
                                var mediaUrl = "<?php echo $mediaUrl ?>";
                                var siteUrl = "<?php echo siteUrl ?>";
                                document.getElementById('pageNo').value = 1;
                                $("#upload-container").css('display', 'none');
                                html = '<ul>';
                                var row = data.errors,
                                        count = 0;
                                console.log(JSON.stringify(row));
                                for (var i = 0; i < row.length; i++) {

                                    //while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
                                    html += '<li class="col-lg-4 col-md-6 col-sm-6 col-xs-12 photocallery-head">';
                                    html += '<div class="gallary-header" ><span>Name:</span>' + row[i]['displayname'] + '</div> ';
                                    html += '<div class="list-image-wrapper"><img src="' + mediaUrl + row[i]['uploadurl'] + '" ></div>';
                                    html += '<h1 onClick="javascript:openPopup(\'' + row[i]['id'] + '\');">Vote Now</h1>';
                                    html += ' <div id="pop-up-' + row[i]['id'] + '" class="pop-up-display-content"><div class="likeImg"><img id="pic_' + row[i]['id'] + '" src="' + mediaUrl + row[i]['uploadurl'] + '"  for="' + siteUrl + 'viewpicture.php?picture=' + row[i]['id'] + '" width="100%" height="auto"><div class="fblikemiddle"> <div class="fb-like" data-href="' + siteUrl + '.viewpicture.php?picture=' + row[i]['id'] + '" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div><img class ="fb-share" id="img-' + row[i]['id'] + '" for="' + siteUrl + 'viewpicture.php?picture=' + row[i]['id'] + '"  src="images/facebook_share1.png" height="24px" width="70px" onclick="fbshare(' + row[i]['id'] + ');"> </div></div></li>';
                                    count++;
                                    if ((i % 6 == 0))
                                        html += '<li class="clearfix"></li></ul><ul>';
                                }
                                html += '<li class="clearfix"></li></ul>';

                                //location.reload();
                                //return true;
                                if (row.length > 0) {
                                    $("#allGalleries").html(data.html);
                                }
                                $('#allGalleries ul:lt(' + x + ')').fadeIn('slow');
                                $(".form-control").val('');
                                $('html, body').animate({
                                    scrollTop: $("#allGalleries").offset().top
                                }, 2000);
                                $(".fileUpload").show();
                                $("#loadMoreCenter").show();
                                //FB.XFBML.parse();
                            } else {
                                $("#errorAjax").html(data.errors);
                            }
                        },
                        complete: function () {
                            $("#ajaxloader").hide();
                        },
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false
                    });

                }
                return false;
            }
            ;

            jQuery(document).ready(function ($) {
                var fileInput = $('#upload');
                fileInput.on('click', addFiles);

                document.getElementById('pageNo').value = 1;
                size_li = $("#allGalleries ul").size();
                x = 1;
                $('#allGalleries ul:lt(' + x + ')').fadeIn('slow');
                $('#loadMore').click(function () {
                    x = (x + 1 <= size_li) ? x + 1 : size_li;
                    $('#allGalleries ul:lt(' + x + ')').fadeIn('slow');
                    if (x == size_li)
                        $("#loadMoreCenter").hide();
                });

//                $('#myfile').change(function (e) {
//                    $("#uploadForm").submit();
//                });

                $(".fileUpload").click(function () {

                    $("#upload-container").css('display', 'block');
                    $(this).hide();
                });



                $("#loadMores").click(function () {

                    $.ajax({
                        type: "POST",
                        url: "paginate.php",
                        dataType: "json",
                        data: {
                            page: document.getElementById('pageNo').value
                        },
                        success: function (data) {
                            console.log(JSON.stringify(data));

                            if (data.status == 'success') {
                                $("#allGalleries").append(data.html);
                                document.getElementById('pageNo').value = data.nextPage;
                                $('#allGalleries ul:lt(' + data.nextPage + ')').fadeIn('slow');
                                $('#allGalleries ul:lt(' + data.nextPage + ')').css('padding', '0px');
                                FB.XFBML.parse();
                                if (data.hasNext == "0") {
                                    $("#loadMoreCenter").hide();
                                }

                            }
                        }
                    });

                });
                function ajaxShare(res) {
                    $.ajax({
                        type: "POST",
                        url: "like.php",
                        dataType: "json",
                        data: {
                            url: res
                        },
                        success: function (data) {
                            console.log(data);
                            //alert(data);
                        }
                    });
                }
                window.fbAsyncInit = function () {
                    FB.init({
                        appId: "<?php echo $fbId ?>", //weeloy localhost fb appId
                        status: true,
                        version: 'v2.2',
                        cookie: true,
                        xfbml: true
                    });
                    FB.Event.subscribe('edge.create', function (response) {
                        console.log(response);
                        ajaxShare(response);


                    });
                    FB.Event.subscribe('edge.remove', function (response) {
                        console.log(response);
                        ajaxShare(response);


                    });
                }; //end fbAsyncInit

                // Load the SDK Asynchronously
                (function (d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0];
                    if (d.getElementById(id))
                        return;
                    js = d.createElement(s);
                    js.id = id;
                    js.src = "//connect.facebook.net/en_US/sdk.js";
                    fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));

            });
//            function dynamicfbshare(id, uploadurl) {
//
//                var link = "<?php echo $siteUrl ?>viewpicture.php/?picture=" + id,
//                        description = "Please Vote for this Picture in the weely.com",
//                        caption = "Weeloy Food-Selfie Contest";
//                var obj = {
//                    method: 'feed',
//                    link: link,
//                    picture: "<?php echo $siteUrl ?>" + uploadurl,
//                    name: caption,
//                    caption: caption,
//                    description: description,
//                    display: 'popup'
//                }
//                fbshareglobal(obj);
//            }

            function fbshareglobal(obj) {
                FB.ui(obj, function (response) {
                    console.log(JSON.stringify(response));
                });
            }
            function fbshare(ele) {
                var id = ele,
                        link = $("#pic_" + id).attr('for'),
                        description = " Hey Friends,\n Help me win a Holiday trip for 2 to Phuket \n Vote for me now @ Weeloy Food-Photo Contest!",
                        caption = "Weeloy Food-Photo Contest";
                var obj = {
                    method: 'feed',
                    link: link,
                    picture: $("#pic_" + id).attr('src'),
                    name: caption,
                    caption: '#foodphotocontest2015,#weeloy',
                    description: description,
                    display: 'popup'
                }
                console.log(JSON.stringify(obj));
                fbshareglobal(obj);

                return false;
            }
            function fbshareFriends() {

                var obj = {
                    method: 'share',
                    href: '<?php echo $siteUrl ?>',
//                    link: "<?php echo $siteUrl ?>",
//                    picture: "http://localhost:8888/weeloy.com/modules/photo_contest/images/background_light.jpg",
//                    name: "Weeloy Food-Photo Contest",
//                    caption: "Weeloy Food-Photo Contest",
//                    description:"Dine around Singapore  & Share your Food Photo Be a food explorer, Challenge youself and your buddies to dine at various restaurants listed on Weeloy.com! It’s easy to snap, share and win!Winning your dream holiday is as easy as 1, 2, 3!" ,
                    display: 'popup'
                }
                console.log(JSON.stringify(obj));
                fbshareglobal(obj);

                return false;
            }

            function openPopup(id) {
                $('#pop-up-' + id).popUpWindow({action: "open", size: "uploads-popup medium"});
            }
        </script>
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <!-- Google Tag Manager -->
        <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-TQZ2W7"
                          height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <script>(function (w, d, s, l, i) {
                w[l] = w[l] || [];
                w[l].push({'gtm.start':
                            new Date().getTime(), event: 'gtm.js'});
                var f = d.getElementsByTagName(s)[0],
                        j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
                j.async = true;
                j.src =
                        '//www.googletagmanager.com/gtm.js?id=' + i + dl;
                f.parentNode.insertBefore(j, f);
            })(window, document, 'script', 'dataLayer', 'GTM-TQZ2W7');</script>
        <!-- End Google Tag Manager -->
        <div id="fb-root"></div>

        <header>
            <div class="navbar-fixed-top navbar-fixed-top-index " >
                <div class="logo-container--fix" style="width: 25%;display: inline-block;  vertical-align: middle;">
                    <h1 class="logo" style=" font-size: 25px;margin: 5px; padding: 0;">
                        <a href="../../../" target="_blank" > <img src="../../../images/logo_w_t_small.png" alt=""></a></h1>
                </div>
            </div>
                <div class=" container-fluid ">
                    <div class="container header-back" style="max-width: 800px;">
                        <div class="row">


                        </div>
                    </div>
               </div>
        </header>
        <section class="heading">
            <div class=" container-fluid">
                <div class="container">
                    <div class="row">
                        <h1>Dine around Singapore <br>
                            & Share your Food Photo </h1>
                        <p>Be a food explorer, Challenge yourself and your buddies to dine at various 
                            restaurants listed on Weeloy.com! It’s easy to snap, share and win! </p>

                    </div>
                </div>
            </div>
        </section>
        <section class="main-content">
            <div class="container">
                <div class="row box-padding">
                    <h1>Winning your dream holidays is as easy as 1, 2, 3!</h1>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="row text-center" style="margin-bottom: 20px;">
                            <div class="image-red "><img src="https://static3.weeloy.com/images/food-selfie-content-2015/step1-2.png" width="120" height="119" class="image-red-img"></div>
                            <div class="content-box col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                <a href='https://www.weeloy.com/search/Singapore'><h2>Book with Weeloy.com</h2></a>
                                <p>Pick a restaurant and book a date.</p>
                            </div>
                        </div>

                        <div class="row text-center"  style="margin-bottom: 20px;">
                            <div class="image-red"><img src="https://static2.weeloy.com/images/food-selfie-content-2015/step2-2.png" width="120" height="119" class="image-red-img"></div>
                            <div class="content-box col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                <h2>Share your memory</h2>
                                <p>Submit your favourite food photo from the restaurant with your best memories.</p>
                            </div>
                        </div>

                        <div class="row text-center" style="margin-bottom: 20px;">
                            <div class="image-red"><img src="https://static4.weeloy.com/images/food-selfie-content-2015/step3-2.png" width="120" height="119" class="image-red-img"></div>
                            <div class="content-box col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                <h2>Get the most votes & win prizes</h2>
                                <p>The highest vote wins! Share your entry and get as many votes as possible.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 border-image div-class"><img src="https://static.weeloy.com/images/food-selfie-content-2015/subimage_light.jpg" class="img-responsive lagrge-img img-thumbnail"></div>
                </div>
            </div>
        </section>
        <section class="google">
            <div class="container">
                <div class="row text-center">
                    <h1>Start NOW!</h1>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <p>Please ensure the Photo captures the food and the tangible atmosphere of a Restaurant before submitting! <a href="tc.php" style="font-size:12px;" target="_blank">more details</a> </p>
                        <div class="fileUpload btn" style ='padding: 12px 40px;font-size:15px;'> <span >Upload Photo</span>

                        </div>
                        <div class="clearfix"></div>

                        <div class="form" id="upload-container" style="display:none;" >
                            <form id="uploadForm" class="formContainer" action="upload.php" method="post" >
                                <h5 style=" color: #2aacd2;padding-top:10px;padding-bottom:10px;  ">Please enter your Display name when uploading the image. (One Photo submission per restaurant/booking) </h5>
                                <h5 style=" color: #2aacd2;padding-top:10px;padding-bottom:10px;  ">All users must have booked with weeloy (same email address).</h5>

                                <?php if (isset($confirmation) && ($confirmation != '') && isset($email) && ($email != '')) { ?>
                                    <input id="email" name="email" type="hidden" class="form-control" value="<?php echo $email ?>">
                                    <input id="confirmation" name="confirmation" type="hidden" class="form-control" value="<?php echo $confirmation ?>">

                                <?php } else { ?>
                                    <div class='row' style='margin-bottom:15px;'>
                                        <div class="input-group" id="div-email">
                                            <span class="input-group-addon input-group-addon75" style='padding:6px 24px;'>
                                                <label for="firstname" style='margin:0px;'>Email</label></span>
                                            <input id="email" name="email" type="text" class="form-control" value="">
                                        </div>
                                    </div>

                                    <div class='row' style='margin-bottom:50px;'>
                                        <div class="input-group" id="div-confirmation">
                                            <!--<span class="input-group-addon input-group-addon75"  style='padding:6px 24px;'><label for="name"  style='margin:0px;' >Reservation No</label></span> -->
                                            <input id="confirmation" name="confirmation" type="hidden" class="form-control" value="" >
                                        </div>
                                    </div>

                                <?php } ?>
                                <div class='row' style='margin-bottom:15px;'>
                                    <div class="input-group" id="div-displayname">
                                        <span class="input-group-addon input-group-addon75"><label for="name">Display Name</label></span> 
                                        <input id="name" name="name" type="text" class="form-control" value="">
                                    </div>
                                </div>


                                <div class='row' style='margin-bottom:10px;'>
                                    <div class="input-group" id="div-picture">
                                        <span class="input-group-addon input-group-addon75"><label for="picture">Upload Photo</label></span>
                                        <input id="picture" name="file" type="file" class="form-control"  >
                                    </div>
                                </div>
                                <div class='row' style='margin-bottom:10px;text-align:right;'>
                                    <input type="checkbox" id="chk-agree" value="agree" checked style='visibility:hidden;'>
                                </div>
                                <div id="ajaxloader"><img src="images/25-1.gif" width="31px" height="31px" /></div>
                                <div id="errorAjax"></div>

                                <div id="upload" class="uploadphoto btn"> <span>Submit your Photo</span> 
                                </div>
                                &nbsp<h6>By submitting, I agree with the <a href='tc.php' target="_blank">terms and conditions</a></h6>


                            </form>
                        </div>
                        <p style ="margin-top:20px"> Contest Period: 15<sup>th </sup> September to 25<sup>th</sup> November 2015</br>
                            Photo Submission Deadline: 30<sup>th</sup> November 2015</p>

                        <!--<h4>The top entry with the most votes will win a 3 Days/2 Nights Phuket holiday for 2.</h4>
                        <div class="google-maps">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d255281.15034880844!2d103.8470128!3d1.31473075!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31da11238a8b9375%3A0x887869cf52abf5c4!2sSingapore!5e0!3m2!1sen!2ssg!4v1439927292951" width="600" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>-->
                    </div>
                </div>
            </div>
        </section>
        <section class="przes">
            <div class=" container-fluid">
                <div class="container">
                    <div class="row">
                        <h1>Prizes</h1>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 prizess-head">
                            <h1>Top Prize</h1>
                            <li class="list-image-wrapper"><img src="https://static.weeloy.com/images/food-selfie-content-2015/samples/top-prize.jpg" width="100%" height="auto"></li>
                            <div class="prize-content">
                                <li>The entry with the highest votes win a 3 Days/2 Nights Phuket trip with return airfare & Movenpick Resort stay.</li>


                                <div class="prize-box"><h1>About Movenpick Resort & Spa</h1>
                                    <p>Situated at the heart of Karon Beach, Phuket. Movenpick Phuket Resort and & Spa offers stunning view of the Karon Beach </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 prizess-head">
                            <h1 class="running-up">Runner Up</h1>
                            <li class="list-image-wrapper"><img src="https://static.weeloy.com/images/food-selfie-content-2015/samples/runnerup_light.jpg" width="100%" height="auto"></li>
                            <div class="prize-content">
                                <li>The 2nd highest voted entry win a Dinner for 2 at Shutters (Amara Sanctuary Resort Sentosa)</li>

                                <div class="prize-box"><h1>About Shutters</h1>
                                    <p>Shutters is a modern restaurant located at the Amara Sanctuary Resort Sentosa serving lunch and dinner International buffets complemented by an all-day dining menu in between. </p>
                                </div>


                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 prizess-head">
                            <h1 class="prrticipation">Participation Prizes</h1>

                            <li class="list-image-wrapper"><img src="https://static.weeloy.com/images/food-selfie-content-2015/samples/participation.jpg" width="100%" height="auto"></li>


                            <div class="prize-content">
                                <li>$25 Dining Vouchers for Top 20 participants from our list of restaurant partners </li>
                                <div class="prize-box">  <h1>AN EXCITING DINING EXPERIENCE AWAITS YOU</h1>
                                    <p style='font-size:13px'>Absinthe &#183; Artistry  &#183;  Cato &#183; Drury Lane Cafe &#183;  Da Paolo  &#183; Forlino &#183; Giardino Italian Pizzerie & Bar &#183; Giardino Pizza Bar & Grill &#183; Koskos &#183; L'Entrecote &#183; Match &#183; Pluck &#183; Sabio By The Sea &#183; Scrumptious At The Turf  &#183; Shin Minori Japanese &#183; SPRMRKT &#183; The Latin Quarter &#183; The Steakhouse &#183; Xin Yue</p>
<!--                                        <p>The Steakhouse &#183; Giardino Italian Pizzeria & Bar &#183;  Giardino Pizza Bar & Grill &#183; Pluck &#183; Shin Minori Japanese &#183; Absinthe &#183; Artistry &#183; Koskos &#183; Match &#183; Sear &#183; SPRMRKT &#183; Forlino &#183; Sabio By the Sea &#183; Pisco &#183; Bumbo Rum Club &#183; L'Entrecote
                                    </p>-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="photocallery">
            <div class=" container-fluid">
                <div class="container">
                    <div class="row">
                        <h1>Photo Gallery</h1>
                        <div id="allGalleries">
                            <?php
                            $count = 0;
                            $sql = "SELECT * FROM _2015_photo_contest WHERE status!='decline' ORDER by id DESC LIMIT 0,48";
                            $data = pdo_multiple_select($sql);
                            $allCount = pdo_multiple_select("SELECT * FROM _2015_photo_contest WHERE status!='decline' ORDER by id DESC");
                            $allCount = count($allCount);
                            $rowcount = count($data);
                            $loadMore = $rowcount + 1;
                            if ($rowcount > 0) {
                                echo '<ul>';
                                foreach ($data as $key => $row) {
                                    ?>
                                    <?php $uploadUrl = $row['uploadurl']; ?>
                                    <li class="col-lg-4 col-md-6 col-sm-6 col-xs-12 photocallery-head">
                                        <div class="gallary-header "  >
                                            <?php  if(!empty($row['restaurant_name'])){?>
                                            <span style="margin:10px;"><?php echo $row['displayname'] ?> @ <?php echo $row['restaurant_name'] ?></span>
                                            <?php } else {?>
                                            <span style="margin:10px;"><?php echo $row['displayname'] ?></span>
                                            <?php } ?>
                                        </div> 
                                        <div class="list-image-wrapper" style="
                                             height:320px; 
                                             
                                             background:  url(<?php echo $mediaUrl ?><?php echo $uploadUrl; ?>) no-repeat center center;
                                             background-size: contain;
                                             background-color:rgba(245, 245, 245, 1) ;
                                             ">
                                        
                                        </div>
                                        <h1 onClick="javascript:openPopup('<?php echo $row['id']; ?>');">Vote Now</h1>

                                        <div id="pop-up-<?php echo $row['id']; ?>" class="pop-up-display-content"> 
                                            <div class="likeImg">
                                                <img id="pic_<?php echo $row['id']; ?>"src="<?php echo $mediaUrl ?><?php echo $uploadUrl; ?>" for ="<?php echo $siteUrl ?>viewpicture.php?picture=<?php echo $row['id'] ?>" width="100%" height="auto">

                                                <div class="fblikemiddle">
                                                    <div style="padding-bottom:10px;"><b><?php echo $row['displayname'] ?> @  <?php echo $row['restaurant_name'] ?> + <?php echo $row['wheelwin'] ?> </b></div>

                                                    <div class="fb-like" data-href="viewpicture.php?picture=<?php echo $row['id'] ?>" data-layout="button_count" send="false" data-action="like" data-show-faces="false" data-share="false"></div>
                                                    <img class ="fb-share" id="img-<?php echo $row['id']; ?>" for="<?php echo $siteUrl ?>viewpicture.php?picture=<?php echo $row['id'] ?>" src="images/facebook_share1.png" height="24spx" onclick="fbshare(<?php echo $row['id'] ?>);" width="70px" />

                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <?php
                                    $count++;

                                    if (($count % 48 == 0) && ($count != $allCount))
                                        echo '<li class="clearfix"></li></ul><ul>';
                                }
                                echo '<li class="clearfix"></li></ul>';
                            }
                            ?>


                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </section>
        <section class="buttons">
            <div class=" container-fluid">
                <div class="container">
                    <div class="row">
                        <?php if ($allCount > $loadMore) { ?>
                            <center id="loadMoreCenter">
                                <input type="hidden" name="pageNo" id="pageNo" value="1" />
                                <input type="button" name="input" id="loadMores" class="view-more btn btn-responsive paginate" value="View More Entries">
                            </center>
                        <?php } else { ?>
                            <input type="hidden" name="pageNo" id="pageNo" value="1" />
                        <?php } ?>
                        <center>
                            <h1>Want to find out more?</h1>
                        </center>
                        <center>
                            <a href="tc.php" target="_blank" > <input type="button" name="input" id="inputid" class="faq btn btn-responsive" value="Contest Terms & Conditions"></a>
                        </center>
                        <center>
                            <a href="https://www.weeloy.com/how-it-works" target="_blank" ><input type="button" name="input" id="inputid" class="learn btn btn-responsive" value="Learn About Weeloy"></a>
                        </center>

                    </div>
                </div>
            </div>
        </section>
        <div class="pre-footer">
            <div class=" container-fluid">
                <div class="container" style=' padding-right: 0px; padding-left: 0px;'>
                    <div class="row text-center">
                        <h1>In Partnership with:</h1>
                        <li class="col-lg-12 col-md-12 col-sm-12 col-xs-12  text-center" style="margin-bottom:15px;">
                            <!--<a href='../../../restaurant/thailand/phuket/mint' alt='mint movenpick restaurant on weeloy' target="_blank">-->
                            <img src="https://static.weeloy.com/images/food-selfie-content-2015/partnerlogo/movenpick1.jpg"  class="pre-footer-images">
                            <!--</a>-->
                            <a href='../../../restaurant/singapore/shutters?utm_source=food-selfie-contest-2015&utm_medium=SG_SG_R_ShinMinoriJapaneseRestaurant&utm_campaign=fsc2015_0001bu' alt='shutters restaurant on weeloy' target="_blank">
                            <img src="https://static.weeloy.com/images/food-selfie-content-2015/partnerlogo/amara_singapore.jpg"  class="pre-footer-images">
                            </a>
                            <a href='../../../restaurant/singapore/absinthe?utm_source=food-selfie-contest-2015&utm_medium=SG_SG_R_AbsintheRestaurant&utm_campaign=fsc2015_0001a' alt='absinthe restaurant on weeloy' target="_blank"><img src="https://static.weeloy.com/images/food-selfie-content-2015/partnerlogo/absinthe_logo.png"  class="pre-footer-images"></a>
                            <a href='../../../restaurant/singapore/shin-minori-japanese-restaurant?utm_source=food-selfie-contest-2015&utm_medium=SG_SG_R_ShinMinoriJapaneseRestaurant&utm_campaign=fsc2015_0001b' alt='shin minori japanese restaurant on weeloy' target="_blank"><img src="https://static.weeloy.com/images/food-selfie-content-2015/partnerlogo/shin_minori.png"  class="pre-footer-images"></a>
                            <a href='../../../restaurant/singapore/drury-lane?utm_source=food-selfie-contest-2015&utm_medium=SG_SG_R_DruryLaneRestaurant&utm_campaign=fsc2015_0001d' alt='drury-lane on weeloy' target="_blank"><img src="https://static.weeloy.com/images/food-selfie-content-2015/partnerlogo/drury_lane.jpg"  class="pre-footer-images"></a>
                            <a href='../../../restaurant/singapore/giardino-pizza-bar-grill?utm_source=food-selfie-contest-2015&utm_medium=SG_SG_R_GiardinoPizzaBarGrillRestaurant&utm_campaign=fsc2015_0001e' alt='giardino pizza bar grill restaurant on weeloy' target="_blank"><img src="https://static.weeloy.com/images/food-selfie-content-2015/partnerlogo/giardino_pizza_logo.png"  class="pre-footer-images"></a>
                            <a href='../../../restaurant/singapore/artistry-cafe?utm_source=food-selfie-contest-2015&utm_medium=SG_SG_R_ArtistryCafeRestaurant&utm_campaign=fsc2015_0001f' alt='artistry cafe restaurant on weeloy' target="_blank"><img src="https://static.weeloy.com/images/food-selfie-content-2015/partnerlogo/artistry_logo.png"  class="pre-footer-images"></a>
                            <a href='../../../restaurant/singapore/forlino?utm_source=food-selfie-contest-2015&utm_medium=SG_SG_R_ForlinoRestaurant&utm_campaign=fsc2015_0001g' alt='forlino restaurant on weeloy' target="_blank"><img src="https://static.weeloy.com/images/food-selfie-content-2015/partnerlogo/Forlino.png"  class="pre-footer-images"></a>
                            <a href='../../../restaurant/singapore/koskos?utm_source=food-selfie-contest-2015&utm_medium=SG_SG_R_KoskosRestaurant&utm_campaign=fsc2015_0001h' alt='koskos restaurant on weeloy' target="_blank"><img src="https://static.weeloy.com/images/food-selfie-content-2015/partnerlogo/koskos.png"  class="pre-footer-images"></a>
                            <a href='../../../restaurant/singapore/giardino-pizzeria-bar?utm_source=food-selfie-contest-2015&utm_medium=SG_SG_R_GiardinoPizzriaBarRestaurant&utm_campaign=fsc2015_0001i' alt='giardino pizzeria bar restaurant on weeloy' target="_blank"><img src="https://static.weeloy.com/images/food-selfie-content-2015/partnerlogo/giardino_italian_logo.png"  class="pre-footer-images"></a>
                            <a href='../../../restaurant/singapore/match?utm_source=food-selfie-contest-2015&utm_medium=SG_SG_R_MatchRestaurant&utm_campaign=fsc2015_0001j' alt='match restaurant on weeloy' target="_blank"><img src="https://static.weeloy.com/images/food-selfie-content-2015/partnerlogo/match_Logo.jpg"  class="pre-footer-images"></a>
                            <a href='../../../restaurant/singapore/scrumptious-at-the-turf?utm_source=food-selfie-contest-2015&utm_medium=SG_SG_R_ScrumptiousAtTheTurfRestaurant&utm_campaign=fsc2015_0001k' alt='scrumptious-at-the-turf restaurant on weeloy' target="_blank"><img src="https://static.weeloy.com/images/food-selfie-content-2015/partnerlogo/scrumptious.png"  class="pre-footer-images"></a>
                            <a href='../../../restaurant/singapore/pluck?utm_source=food-selfie-contest-2015&utm_medium=SG_SG_R_PluckRestaurant&utm_campaign=fsc2015_0001l' alt='pluck restaurant on weeloy' target="_blank"><img src="https://static.weeloy.com/images/food-selfie-content-2015/partnerlogo/pluck_Logo.png"  class="pre-footer-images"></a>  
                            <a href='../../../restaurant/singapore/sabio-by-the-sea?utm_source=food-selfie-contest-2015&utm_medium=SG_SG_R_SabioByTheSeaRestaurant&utm_campaign=fsc2015_0001m' alt='sabio by the sea restaurant on weeloy' target="_blank"><img src="https://static.weeloy.com/images/food-selfie-content-2015/partnerlogo/sabiobythesea.png"  class="pre-footer-images"></a>
                            <a href='../../../restaurant/singapore/da-paolo-bistro-bar?utm_source=food-selfie-contest-2015&utm_medium=SG_SG_R_DaPaoloBistroBarRestaurant&utm_campaign=fsc2015_0001n' alt='da-paolo-bistro-bar restaurant on weeloy' target="_blank"><img src="https://static.weeloy.com/images/food-selfie-content-2015/partnerlogo/da_paolo.png"  class="pre-footer-images"></a>
                            <a href='../../../restaurant/singapore/cato?utm_source=food-selfie-contest-2015&utm_medium=SG_SG_R_CatoRestaurant&utm_campaign=fsc2015_0001o' alt='cato on weeloy' target="_blank"><img src="https://static.weeloy.com/images/food-selfie-content-2015/partnerlogo/cato.png"  class="pre-footer-images"></a>
                            <a href='../../../restaurant/singapore/lentrecote?utm_source=food-selfie-contest-2015&utm_medium=SG_SG_R_LentrecoteRestaurant&utm_campaign=fsc2015_0001p' alt='lentrecote restaurant on weeloy' target="_blank"><img src="https://static.weeloy.com/images/food-selfie-content-2015/partnerlogo/lentrecote.png"  class="pre-footer-images"></a>
                            <a href='../../../restaurant/singapore/sprmrkt?utm_source=food-selfie-contest-2015&utm_medium=SG_SG_R_SprmrktRestaurant&utm_campaign=fsc2015_0001q' alt='sprmrkt restaurant on weeloy' target="_blank"><img src="https://static.weeloy.com/images/food-selfie-content-2015/partnerlogo/sprmrkt.png"  class="pre-footer-images"></a>
                            <a href='../../../restaurant/singapore/the-latin-quarter?utm_source=food-selfie-contest-2015&utm_medium=SG_SG_R_theLatinQuarterRestaurant&utm_campaign=fsc2015_0001r' alt='the-latin-quarter on weeloy' target="_blank"><img src="https://static.weeloy.com/images/food-selfie-content-2015/partnerlogo/latin_quarter.jpg"  class="pre-footer-images"></a>
                            <a href='../../../restaurant/singapore/the-steak-house?utm_source=food-selfie-contest-2015&utm_medium=SG_SG_R_TheSteakHouseRestaurant&utm_campaign=fsc2015_0001s' alt='the steak house restaurant on weeloy' target="_blank"><img src="https://static.weeloy.com/images/food-selfie-content-2015/partnerlogo/the_steakhouse_logo.png"  class="pre-footer-images"></a>
                            <a href='../../../restaurant/singapore/xin-yue?utm_source=food-selfie-contest-2015&utm_medium=SG_SG_R_XinYueRestaurant&utm_campaign=fsc2015_0001t' alt='xin-yue restaurant on weeloy' target="_blank"><img src="https://static.weeloy.com/images/food-selfie-content-2015/partnerlogo/xinyue.png"  class="pre-footer-images"></a>



                        </li>


                    </div>
                </div>
            </div>
        </div> 
    </body>
    <footer>
        <div class="follow-us">
            <div class="container">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h3>Find us on</h3>
                    <ul>
                        <li><a  target="_blank"  href="https://www.facebook.com/weeloy.sg"  id='social-facebook'><span class="fa fa-facebook"></span></a></li>
                        <li><a  target="_blank"  href="https://twitter.com/weeloyasia"  id='social-twitter'><span class="fa fa-twitter"></span></a></li>
                        <li><a  target="_blank"  href="https://www.linkedin.com/company/weeloy-pte-ltd"  id='social-linkedin'><span class="fa fa-linkedin"></span></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="footer-bg">
            <div class="container">
                <div class="footer-col col-md-3 col-sm-6 col-xs-12">
                    <h3>INFORMATION</h3>
                    <ul class="footer-col-content">
                        <li><a href="../../../blog" id='info-blog'>Food blog</a></li>
                        <li><a href="../../../how-it-works" id='info-hiw'>How It Works</a></li>
                        <li><a href="../../../info-contact" id='info-contact'>Contact Us</a></li>
                        <li><a href="../../../info-partner" id='info-partner'>Partner</a></li>
                        <li><a href="../../../info-faq" id='info-faq'>FAQ's</a></li>
                        <li><a target="_blank" href="https://www.weeloy.com/templates/tnc/termsservices.php" id='info-tns'>Terms and Conditions of Service</a></li>
                        <li><a target="_blank" href="https://www.weeloy.com/templates/tnc/termsconditions.php" id='info-tnc'>Privacy policy and Terms</a></li>
                    </ul>
                </div>
                <div class="footer-col col-md-3 col-sm-6 col-xs-12">
                    <h3>OUR LOCATIONS</h3>
                    <ul class="footer-col-content">
                        <li style="color:#fff">Singapore</li>
                        <li style="color:#fff">Malaysia</li>
                        <li style="color:#fff">Thailand</li>
                    </ul>
                </div>
                <div class="footer-col col-md-3 col-sm-6 col-xs-12">
                    <h3>MOBILE</h3>
                    <div class="footer-col-content">
                        <p>
                            <a rel="nofollow" class="mobile-picture-div" target="_blank" href="http://itunes.apple.com/app/id973030193">
                                <img width="196" height="60" src="../../../images/home_picture/app-store-badge_en.png" alt="Available on the App Store">
                            </a>
                        </p>
                        <p>
                            <a  rel="nofollow" class="mobile-picture-div" target="_blank" href="https://play.google.com/store/apps/details?id=com.weeloy.client">
                                <img width="196" height="60" src="../../../images/home_picture/play-store-badge_en.png" alt="Get it on Google Play">
                            </a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-log-12 col-md-12 col-sm-12 col-xs-12 copyright">
                <p class="text-center">Copyright &copy; 2015 Weeloy. All Rights Reserved.</p>
            </div>
        </div>
    </footer>

    <script src="js/bootstrap.min.js"></script>


</html>


