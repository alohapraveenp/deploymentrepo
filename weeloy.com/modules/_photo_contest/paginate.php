<?php
require_once("conf/conf.init.inc.php");
require_once("lib/wpdo.inc.php");
require_once("lib/wglobals.inc.php");
$return = array();
$return['status'] = 'failure';
$return['nextPage'] = 1;
$return['message'] = 'Unable to process the request.';

 $siteUrl = __SERVERNAME__ . __ROOTDIR__ . "/event/singapore/food-selfie-contest-2015/";
 $mediaUrl = __S3HOST__ ;

//echo json_encode($_POST);exit;
//var_dump($_POST);
if (!empty($_POST)) {
    $return['message'] = '';
    $offset = $_POST['page'] * 6;
    $return['nextPage'] = $_POST['page'] + 1;
    $return['hasNext'] = "0";

    $html = '';

    $count = 0;
    $sql = "SELECT * FROM _2015_photo_contest WHERE status!='decline' ORDER by id DESC LIMIT " . $offset . ",6";
    $data = pdo_multiple_select($sql);
    $allCount = pdo_multiple_select("SELECT * FROM _2015_photo_contest WHERE status!='decline' ORDER by id DESC " . $offset . ",7");
    $allCount = count($allCount);
    $rowCount = count(data);

    if ($allCount > $rowcount)
        $return['hasNext'] = 1;
    if ($rowcount > 0) {
        $html.='<ul>';
        foreach ($data as $row) {
            $html.='<li class="col-lg-4 col-md-4 col-sm-4 col-xs-12 photocallery-head">';
            $html.='<div class="gallary-header" ><span style="margin:10px;">'.$row['displayname'].'"@"'.$row['restaurant_name'].'</span></div> ';
            $html.='<div class="list-image-wrapper"><img src="' .$mediaUrl.$row['uploadurl'] . '" ></div>';
            $html.='<h1 onClick="javascript:openPopup(\'' . $row['id'] . '\');">Vote Now</h1>';
            $html.=' <div id="pop-up-' . $row['id'] . '" class="pop-up-display-content"><div class="likeImg"><img id="pic_' .$row['id'] .'" src="'.$mediaUrl.$row['uploadurl'] . '"  for="' . $siteUrl . 'viewpicture.php?picture=' . $row['id'] . '" width="100%" height="auto"><div class="fblikemiddle"> <div class="fb-like" data-href="' . $siteUrl . '.viewpicture.php?picture=' . $row['id'] . '" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div><img class ="fb-share" id="img-' . $row['id'] . '" for="' . $siteUrl . 'viewpicture.php?picture=' . $row['id'] . '"  src="images/facebook_share1.png" height="24px" width="70px" onclick="fbshare(' . $row['id'] . ');">

               </div></div></li>';
            $count++;

            if (($count % 6 == 0) && ($count != $rowcount))
                $html.='<li class="clearfix"></li></ul><ul>';
        }
        $html.='<li class="clearfix"></li></ul>';
    }

    $return['html'] = $html;
    $return['status'] = 'success';

    echo json_encode($return);
    exit;
}
else {
    $return['status'] = 'failure';
    $return['message'] = 'Invalid POST request.';
}
                


echo json_encode($return);
?>