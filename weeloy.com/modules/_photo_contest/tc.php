<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<html>
    <head>

        <title>Weeloy-Photo-contest</title>
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" type="text/css" href="css/style-new1.css">
        <link rel="stylesheet" type="text/css" href="../../../css/styles_front.css">
        <link rel="stylesheet" type="text/css" href="../../../css/new_layout.css">
        <script type="text/javascript" src="js/jquery-1.9.1.min.js" ></script>
        <link href='http://fonts.googleapis.com/css?family=Lora:400,700' rel='stylesheet' type='text/css'>
        <meta property="og:type" content="article" />
        <meta property="og:title" content="Weeloy Food-Photo Contest" />
        <meta property="og:description" content="Dine around Singapore  & Share your Food Photo Be a food explorer, Challenge youself and your buddies to dine at various restaurants listed on Weeloy.com! It’s easy to snap, share and win!Winning your dream holiday is as easy as 1, 2, 3!" />
        <meta property="og:image:width" content="450"/>
        <meta property="og:image:height" content="298"/>
        <meta property="og:image" content="https://static.weeloy.com/images/food-selfie-content-2015/contest_image_header_light.jpg" />

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <body>
        <header>
            <div class="navbar-fixed-top navbar-fixed-top-index " >
                <div class="logo-container--fix" style="width: 25%;display: inline-block;  vertical-align: middle;">
                    <h1 class="logo" style=" font-size: 25px;margin: 5px; padding: 0;">
                        <a href="../../../index.php" target="_blank" > <img src="../../../images/logo_w_t_small.png" alt=""></a></h1>
                </div>
            </div>
            <div class=" container-fluid header-back">
                <div class="container">
                </div>
            </div>
        </header>
        <div class="container">
            <div class="row">
                <div class="span1">
                    <div class="page-header">
                        <span class="hd-txt"><b>THE CONTEST</b></span>
                        <p class="head-term">
                            <strong>
                                1.
                            </strong>
                            Terms & Conditions applies to all participants of the “Weeloy Food-Photo Contest”. The organiser of the contest is Weeloy Pte Ltd.

                        </p>
                    </div>
                    <div class="page-header">
                        <span class="hd-txt"> <b >ELIGIBILITY FOR PARTICIPANTS</b></span>
                        <p class="head-term">

                            <strong>1.</strong> The Contest is open to all participants who book and dine in any restaurant in Singapore listed on Weeloy.com.</br>

                            <strong>2.</strong>  The Contest period is from 15th September to 25th November 2015, by 23:59hrs.</br>

                            <strong>3.</strong>  Each participant may submit only ONE *Food-Photo per restaurant.</br>

                            <strong>4.</strong> Multiple entries for the same restaurant by a participant will be rejected. </br>
                            <strong>5.</strong> Participants may submit UNLIMITED entries from different restaurants listed on Weeloy.com.</br>
                            <strong>6.</strong> Each entry submitted is final and cannot be modified nor re-uploaded. </br>

                            Weeloy.com </br>

                        <h6>*Food-Photo are images of oneself with its food captured within the space of a single image with or without the participant.</h6>

                        </p>
                    </div>
                    <div class="page-header">
                        <span class="hd-txt"><b>HOW TO ENTER</b></span></br>
                        <span class="head-term"><strong>1. To enter the Contest</strong></span>
                        <p class="term">
                            a. One must make a FREE reservation through Weeloy.com.</br>

                            b. *Dine at the restaurant. </br>

                            c. Take a picture by yourself with the food from the restaurant within a single image. </br>

                            d. Submit this image in <a href ="https://www.weeloy.com/event/singapore/food-selfie-contest-2015/">www.weeloy.com/event/singapore/food-selfie-contest-2015/</a></br>

                        <h6>*Weeloy Pte Ltd will not be liable for the expenses incurred during your dining experience with the 

                            Restaurant of your choice.</h6>
                        </p>
                        <span class="head-term"><strong>2. All entries are subjected to moderation by Weeloy and may be rejected anytime within the submission period if they:</strong></span>
                        <p class="term">
                            a. Are submitted past the contest submission deadline;</br>

                            b. Incomplete Entry (Incomplete Fields During Submission)</br>

                            c. Do not meet the objectives/criteria of the contest</br>

                            d. Contain defamatory, malicious, indecent or any inappropriate content  </br>

                            e. Breach any of the Contest Terms and Conditions</br>
                        </p>
                        <p class="head-term">
                            3. By entering the Contest, each participant's consent is hereby granted to Weeloy as a 

                            perpetual, non-exclusive, royalty-free license to feature their name, photo or any 

                            personal details supplied as part of the Contest for promotional/marketing purposes.
                        </p>
                    </div>
                    <div class="page-header">
                        <span class="hd-txt"><b>DETERMINATION OF WINNER</b></span></br>
                        <p class="head-term">
                            1. Winners of the Contest will be determined by the <strong>highest number of votes</strong> among all the 

                            entries within the submission & voting deadline (Last vote in by 22nd October 2015, 23:59hr). Should there be a TIE in votes, a panel of Judges from Weeloy Pte Ltd will 

                            determine the Winner based on: </br>
                            <span class="term">
                                a. Visual appeal of the Food Photo
                            </span></br>
                            <span class="term">
                                b. Originality of the Food Photo
                            </span></br>
                            <span class="term">
                                c. Fun factor of the Food Photo
                            </span></br>
                        </p>
                        <p>
                            2. Winners will be notified via SMS, Phone Call, E-mail, <a href ="https://www.weeloy.com/event/singapore/food-selfie-contest-2015/">www.weeloy.com/event/singapore/food-selfie-contest-2015/</a> or Weeloy Pte Ltd’s 

                            Social media platforms on 26th  October 2015.
                        </p>
                        <p>
                            3. The results from voting and/or the decision of the Judges will be final and irrefutable.
                        </p>
                        <p>
                            4. Failure to comply with the requirements stated in the Terms & Conditions may result in 

                            the winner being disqualified, after which another winner will be selected as his/her 

                            replacement based on the next highest votes. In addition, Weeloy Pte Ltd reserves the 

                            right to refuse awarding any prize to a winner who has violated any guidelines or rules 

                            stated in the Terms & Conditions, gained unfair advantage or obtained the winner status 

                            using FRAUDULENT means.
                        </p>
                    </div>
                    <div class="page-header">
                        <span class="hd-txt"><b>PRIZES</b></span></br>
                        <p class="head-term">
                            1. Only ONE participant will be selected as the First, Second prize winners respectively. 

                            And TWENTY participants will be selected as Third prize winners.</br>

                        <p class="term">● First Prize- 3 Days 2 Nights stay at Mövenpick Resort & Spa Karon Beach Phuket + Zuji travel vouchers or any other Air flight vouchers to Phuket for S$400 </p>
                        <p class="term">● Second Prize - Romantic Dinner for 2 at Shutters Restaurant located at the resort island of Sentosa (Amara Sanctuary, Sentosa). </p>
                        <p class="term">● Third Prize - Top 20 contestants after the top 2 places will be awarded S$25 worth of Dining Voucher each from Selected Restaurants on Weeloy.com</p>


                        </p>
                        <p class="head-term">
                            2. Prizes will be available for collection from 26th October 2015 till 13th November 2015.(Mon-

                            Fri 9am-6pm)
                        </p>
                        <p class="head-term">
                            3. Collection of all prizes will be at 83 Amoy Street, Singapore 069902.
                        </p>
                        <p class="head-term">
                            4. Failure to collect the prize within the prize collection period may lead to a forfeit of the 

                            prize and Weeloy Pte Ltd holds the decision to award the prize to another participant.
                        </p>
                        <p class="head-term">
                            5. The prizes are non-refundable and no cash alternative will be given.
                        </p>
                        <p class="head-term">
                            6. In the event that the winner chooses not to accept the prize it will result in he/she 

                            forfeiting the prize which then can be awarded to another participant chosen by Weeloy 

                            Pte Ltd.
                        </p>

                    </div>
                    <div class="page-header">
                        <span class="hd-txt"><b>GENERAL</b></span></br>
                        <p class="head-term">
                            1. All selections and decisions made by Weeloy Pte Ltd in conjunction with the Contest 

                            are final and binding. No discussions will be required or entertained in this regard.
                        </p>
                        <p class="head-term">
                            2. Weeloy Pte Ltd shall be entitled to terminate or modify the Contest or rules at any time 

                            prior to the closing date. Weeloy Pte Ltd may publish any such modification or 

                            termination notice on Weeloy Pte Ltd’s Website, Social Media platforms or on 

                            <a href ="www.weeloy.com/event/singapore/food-selfie-contest-2015/">www.weeloy.com/event/singapore/food-selfie-contest-2015</a>.
                        </p>
                        <p class="head-term">
                            3. Any act, omission, event or circumstances occurred which is beyond the reasonable 

                            control of Weeloy Pte Ltd, preventing Weeloy Pte Ltd from complying to these Terms & 

                            Conditions or providing the prizes will not result in Weeloy Pte Ltd being liable for any 

                            failure to perform / delay in performing its obligations.
                        </p>
                        <p class="head-term">
                            4. All dates and timing reference are based on Singapore GMT +8 Standard Time Zone.
                        </p>
                    </div>

                </div>


            </div>
        </div>

    </body>
    <footer>
        <div class="follow-us">
            <div class="container">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h3>Find us on</h3>
                    <ul>
                        <li><a  target="_blank"  href="https://www.facebook.com/weeloy.sg"  id='social-facebook'><span class="fa fa-facebook"></span></a></li>
                        <li><a  target="_blank"  href="https://twitter.com/weeloyasia"  id='social-twitter'><span class="fa fa-twitter"></span></a></li>
                        <li><a  target="_blank"  href="https://www.linkedin.com/company/weeloy-pte-ltd"  id='social-linkedin'><span class="fa fa-linkedin"></span></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="footer-bg">
            <div class="container">
                <div class="footer-col col-md-3 col-sm-6 col-xs-12">
                    <h3>INFORMATION</h3>
                    <ul class="footer-col-content">
                        <li><a href="../../../blog" id='info-blog'>Food blog</a></li>
                        <li><a href="../../../how-it-works" id='info-hiw'>How It Works</a></li>
                        <li><a href="../../../info-contact" id='info-contact'>Contact Us</a></li>
                        <li><a href="../../../info-partner" id='info-partner'>Partner</a></li>
                        <li><a href="../../../info-faq" id='info-faq'>FAQ's</a></li>
                        <li><a target="_blank" href="https://www.weeloy.com/templates/tnc/termsservices.php" id='info-tns'>Terms and Conditions of Service</a></li>
                        <li><a target="_blank" href="https://www.weeloy.com/templates/tnc/termsconditions.php" id='info-tnc'>Privacy policy and Terms</a></li>
                    </ul>
                </div>
                <div class="footer-col col-md-3 col-sm-6 col-xs-12">
                    <h3>OUR LOCATIONS</h3>
                    <ul class="footer-col-content">
                        <li style="color:#fff">Singapore</li>
                        <li style="color:#fff">Malaysia</li>
                        <li style="color:#fff">Thailand</li>
                    </ul>
                </div>
                <div class="footer-col col-md-3 col-sm-6 col-xs-12">
                    <h3>MOBILE</h3>
                    <div class="footer-col-content">
                        <p>
                            <a  rel="nofollow" class="mobile-picture-div" target="_blank" href="http://itunes.apple.com/app/id973030193">
                                <img width="196" height="60" src="../../../images/home_picture/app-store-badge_en.png" alt="Available on the App Store">
                            </a>
                        </p>
                        <p>
                            <a rel="nofollow"  class="mobile-picture-div" target="_blank" href="https://play.google.com/store/apps/details?id=com.weeloy.client">
                                <img width="196" height="60" src="../../../images/home_picture/play-store-badge_en.png" alt="Get it on Google Play">
                            </a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-log-12 col-md-12 col-sm-12 col-xs-12 copyright">
                <p class="text-center">Copyright &copy; 2015 Weeloy. All Rights Reserved.</p>
            </div>
        </div>
    </footer>
    <script src="js/bootstrap.min.js"></script>
</html>

