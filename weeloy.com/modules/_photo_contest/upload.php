<?php

//load mysql classes and supporting classes
require_once("conf/conf.init.inc.php");
require_once("lib/wpdo.inc.php");
require_once("lib/wglobals.inc.php");
require_once("lib/class.booking.inc.php");
require_once("lib/class.review.inc.php");
require_once("lib/class.media.inc.php");
require_once("lib/imagelib.inc.php");

$return = array();
$return['status'] = 'failure';
$return['message'] = 'Maximum upload size exceeds the server setting.';
$bkconfirmation = trim($_POST['confirmation']);
$bkEmail = $_POST['email'];
$displayName = $_POST['name'];

 $siteUrl = __SERVERNAME__ . __ROOTDIR__ . "/event/singapore/food-selfie-contest-2015/";
  $mediaUrl = __S3HOST__ ;

$sql = "SELECT * FROM _2015_photo_contest where confirmation ='$bkconfirmation'";
$data = pdo_single_select($sql);

//fetching booking details based on booking confirmation 

$res = new WY_Booking();
$bkres = $res->getBooking($bkconfirmation);
$restaurant = $res->restaurant;
$is_wheelable = $res->restaurantinfo->is_wheelable;
$title = $res->restaurantinfo->title;
$review = new WY_Review();
$reviewCount = $review->getReview($bkconfirmation, '', 'myreview');
$bkusername = $res->firstname . " " . $res->lastname;
$wheelwin = $res->wheelwin;



$bookingDate = $res->rdate . " " . $res->rtime;

//booking filter checking=>past,current or upcomming

date_default_timezone_set("Asia/Singapore");
$date = date('Y-m-d H:i:s');
$str = strtotime($bookingDate) - strtotime($date);
$divr = 60 * 60 * 24;
$dateDiff = floor($str / $divr);

//restaurant duplicate checking

$emresCount = pdo_single_select("SELECT count(*) as number FROM  _2015_photo_contest where email ='$bkEmail' and restaurant_name='$title'");


//confirmation duplicate checking
if (count($data) > 0) {
    $return['status'] = 'failure';
    $return['message'] = 'This photo has already been uploaded.';
    echo json_encode($return);
    exit;
}

if ($bkres == false) {
    $return['status'] = 'failure';
    $return['message'] = 'Confirmation does not exist.';
    echo json_encode($return);
    exit;
}
if ($res->email !== $bkEmail) {
    $return['status'] = 'failure';
    $return['message'] = 'wrong restaurant or email';
    echo json_encode($return);
    exit;
}
if ($res->status === 'cancel') {
    $return['status'] = 'failure';
    $return['message'] = 'Confirmation has already been canceled';
    echo json_encode($return);
    exit;
}

//email and restaurant duplicte checking
if ($emresCount['number'] > 0) {
    $return['status'] = 'failure';
    $return['message'] = 'This email & restaurant  has already been uploaded.';
    echo json_encode($return);
    exit;
}
//same email and same restaruant
// spin wheel checking 
//if wheelable means need to check if whellwin or not
//if not wheelable need to check date& time passed or upcomming

if ($is_wheelable) {
    if ($wheelwin == '' && empty($wheelwin)) {
        $return['status'] = 'information';
        $return['message'] = 'Oops, we could not verify your submission. Have you dine at the restaurant listed in Weeloy.com before submitting the photo?';
        echo json_encode($return);
        exit;
    }
} else {
    if ($dateDiff > 0) {
        $return['status'] = 'failure';
        $return['message'] = 'Invalid Date and time'; //need to change the proper error msg
        echo json_encode($return);
        exit;
    }
}

$insertObj = array(
    'bkemail' => $bkEmail,
    'bkusername' => $bkusername,
    'bkconfirmation' => $bkconfirmation,
    'displayName' => $displayName,
    'wheelwin' => $wheelwin,
    'restaurant' => $title,
    'reviewCount' => $reviewCount
);
$media = new WY_Media($bkEmail, $bkconfirmation, 'foodselfie');

//CHECK FOLDER EXIST
$media->l_folder_exist();
$res = $media->uploadphotoContest($insertObj, 'foodselfie');

if (@$res[0] < 0) {
        $return['status'] = 'failure';
        $return['message'] = $res[1];
         echo json_encode($return);exit;

    }else{ 
       echo "uplaod else";
              $count = 0;
              $sql = "SELECT * FROM _2015_photo_contest WHERE status !='decline' ORDER by id DESC LIMIT 0,6";
    
               
                $data = pdo_multiple_select($sql);
             
                

//                    $result = mysqli_query($conn, "SELECT * FROM _2015_photo_contest ORDER by id DESC LIMIT 0,6");
//                    $rowcount = mysqli_num_rows($result);
                 $rowCount = count($data);
                 echo "count <> ".$rowCount;
                 var_dump($data);
           
                if ($rowCount > 0) {
                    $html.='<ul>';
                    echo "in count condition";
                    foreach ($data as $row) {
                                       
                        //while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {

                        $html.='<li class="col-lg-4 col-md-4 col-sm-4 col-xs-12 photocallery-head">';
                        $html.='<div class="gallary-header" ><span>Name:</span>' . $row['displayname'] . '</div> ';
                        //$html.='<img src="'.$row['image'].'" width="100%" height="auto" >';
                        $html.='<div class="list-image-wrapper"><img src="' . $mediaUrl.$row['uploadurl'] . '" ></div>';
                        $html.='<h1 onClick="javascript:openPopup(\'' . $row['id'] . '\');">Vote Now</h1>';
                        $html.=' <div id="pop-up-' . $row['id'] . '" class="pop-up-display-content"><div class="likeImg"><img id="pic_' . $row['id'] . '" src="' . $mediaUrl.$row['uploadurl'] . '"  for="' . $siteUrl . 'viewpicture.php?picture=' . $row['id'] . '" width="100%" height="auto"><div class="fblikemiddle"> <div class="fb-like" data-href="' . $siteUrl . '.viewpicture.php?picture=' . $row['id'] . '" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div><img class ="fb-share" id="img-' . $row['id'] . '" for="' . $siteUrl . 'viewpicture.php?picture=' . $row['id'] . '"  src="images/facebook_share1.png" height="24px" width="70px" onclick="fbshare(' . $row['id'] . ');">
                             </div></div></li>';
                        $count++;

                        if (($count % 6 == 0))
                            $html.='<li class="clearfix"></li></ul><ul>';
                    }
                    $html.='<li class="clearfix"></li></ul>';
                 
           
         $return['html'] =$html;
        $return['status'] = 'success';
        $return['message'] = $res[1];  
        var_dump($return);die;
        echo json_encode($return);exit;
    }
    }
    




//if (is_array($_FILES) && isset($_FILES['userImage'])) {
//
//    if (is_uploaded_file($_FILES['userImage']['tmp_name'])) {
//
//        $uploaddir = '.' . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'tmp' . DIRECTORY_SEPARATOR;
//        $res = (file_exists($uploaddir) !== true);
//
//        if ($res) {
//            mkdir($uploaddir, 0777, true);
//        }
//        $originalName = $_FILES['userImage']['name'];
//        $tmpName = explode('.', basename($_FILES['userImage']['name']));
//        $ext = $tmpName[count($tmpName) - 1];
//        $newName = md5($tmpName[0] . date('Ymd', time()));
//        $file_name = $newName . '.' . $ext;
//        $targetPath = $uploaddir . $file_name;
//        $sourcePath = $_FILES['userImage']['tmp_name'];
//        $data = getimagesize($sourcePath);
//        $width = $data[0];
//        $height = $data[1];
//
//        //if($width == 340 && $height == 266){
//        if (true) {
//
//            if (move_uploaded_file($sourcePath, $targetPath)) {
//
//                copy($targetPath, '.' . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . $file_name);
//                echo "before insert";
//                //$insert = "insert into image(image)values('uploads/" . $file_name . "')";
//                $sql = "INSERT INTO _2015_photo_contest (email,username,confirmation,displayname,fblike,share,filename,originalfname,uploadurl,reviews,wheelwin,restaurant_name,totalcount) values('$bkEmail','$bkusername','$bkconfirmation','$displayName','0','0','$file_name','$originalName','uploads/" . $file_name . "','$reviewCount','$wheelwin','$restaurant','0')";
//                echo $sql;
//                $response = pdo_exec($sql);
//                echo "before insert excute query";
//                $html = '';
//                $count = 0;
//                $sql = "SELECT * FROM _2015_photo_contest WHERE status='approve' ORDER by id DESC LIMIT 0,6";
//                //echo $sql;
//                $data = pdo_multiple_select($sql);
//
////                    $result = mysqli_query($conn, "SELECT * FROM _2015_photo_contest ORDER by id DESC LIMIT 0,6");
////                    $rowcount = mysqli_num_rows($result);
//                $rowCount = count($data);
//                if ($data > 0) {
//                    $html.='<ul>';
//                    foreach ($data as $row) {
//                        //while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
//
//                        $html.='<li class="col-lg-4 col-md-4 col-sm-4 col-xs-12 photocallery-head">';
//                        $html.='<div class="gallary-header" ><span>Name:</span>' . $row['displayname'] . '</div> ';
//                        //$html.='<img src="'.$row['image'].'" width="100%" height="auto" >';
//                        $html.='<div class="list-image-wrapper"><img src="' . $row['uploadurl'] . '" ></div>';
//                        $html.='<h1 onClick="javascript:openPopup(\'' . $row['id'] . '\');">Vote Now</h1>';
//                        $html.=' <div id="pop-up-' . $row['id'] . '" class="pop-up-display-content"><div class="likeImg"><img id="pic_' . $row['id'] . '" src="' . $row['uploadurl'] . '"  width="100%" height="auto"><div class="fblikemiddle"> <div class="fb-like" data-href="' . $siteUrl . '.viewpicture.php?picture=' . $row['id'] . '" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div><img class ="fb-share" id="img-' . $row['id'] . '" for="' . $siteUrl . 'viewpicture.php?picture=' . $row['id'] . '"  src="images/facebook_share1.png" height="20px" onclick="fbshare(' . $row['id'] . ');">
//               </div></div></li>';
//                        $count++;
//
//                        if (($count % 6 == 0))
//                            $html.='<li class="clearfix"></li></ul><ul>';
//                    }
//                    $html.='<li class="clearfix"></li></ul>';
//                }
//
//                $sql = "SELECT * FROM _2015_photo_contest WHERE confirmation ='$bkconfirmation' ";
//                $data = pdo_single_select($sql);
//
//                $return ['uploadid'] = $data['id'];
//                $return ['path'] = $data['uploadurl'];
//
//                $return['html'] = $html;
//                $return['status'] = 'success';
//                echo json_encode($return);
//                exit;
//            } else {
//                $return['status'] = 'failure';
//                $return['message'] = 'Unable to upload image.';
//                exit;
//            }
//        } else {
//            $return['status'] = 'failure';
//            $return['message'] = 'Image resolution should be below 4000x2000.Please upload image with resolution 4000x2500';
//            exit;
//        }
//    } else {
//        $return['status'] = 'failure';
//        $return['message'] = 'Image resolution should be below 4000x2000.Please upload image with resolution 4000x2500';
//        exit;
//    }
//}
//echo json_encode($return);
//exit;
?>