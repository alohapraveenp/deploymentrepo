<?

require_once("config.inc.php");

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
<title>Weeloy</title>
<link rel="icon" href="/favicon.ico" type="image/gif" sizes="16x16">
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="css/admin-style.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap-slider.css" rel="stylesheet">
<link href="css/datepicker.css" rel="stylesheet">
<link href="css/carousel.css" rel="stylesheet">
<link href='https://fonts.googleapis.com/css?family=Montserrat|Roboto:400,900,700italic,700,500italic,500,400italic' rel='stylesheet' type='text/css'>
<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<script src="js/selectivizr-min.js"></script>
<script src="js/modernizr.js"></script>
<link rel="stylesheet" type="text/css" href="css/ie.css" media="screen" />
<![endif]-->
</head>
<body>

<div class="ProfileBox">
  <div class="container">
    <div class="row">
    <div class="col-md-12 ProfileName">
    			 <img src='images/logo_w.png'><hr />          
                 <nav class="navbar navbar-default white-bg" role="navigation" style='border: 0px; padding: 0px;'>
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
<?

print_navbar();

?>
                    </div>
                </nav>
                <hr />
                <h5>Back Office Message</h5>
                <p class = 'small'>Cras at eget metus. Nullam id dolor id nibh ultricies </p><hr/>
    </div>
    </div>
  </div>
</div>


<div class="move">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="white-bg">

<?

print_body();

?>    

    </div>
    </div>
    </div>

<div class="container">
    <div class="row">
        <div class="white-bg">
	        <h2>Reporting</h2>
        <div class="col-md-4"><p>
        <h4><span class="glyphicon glyphicon-th-list"></span> Visits</h4>col-md-4
		<p>Visits in the past&nbsp;30 days</p>
<input class="button subbutton btn btn-primary customColor"" value="Reports">

        </p></div>
        
        <div class="col-md-4"><p>
<h4><span class="glyphicon glyphicon-leaf"> Vouchers</h4>
<p>One-Dines-Free Vouchers in the past&nbsp;30 days</p>
        </p></div>
        
        <div class="col-md-4"><p>
        
<h4><span class="glyphicon glyphicon-ok-sign"> Search</h4>
<p>352 searches in the past&nbsp;30 days</p>

        </p></div>
        <div class="clearfix visible-md-block"></div>
        <div class="clearfix visible-lg-block "></div><!--This clearfix has to be set every 3 div because that's that col-lg break to a new line-->
    </div>
    </div>
</div>


<div class="container">
    <div class="row">
        <div class="white-bg">
	        <h2>Latest News</h2>
        <div class="col-md-4"><p>
        <img src="../images/admin1.jpg" width='200px'>
        <h4>Check our new features</h4>
		<p style='color:grey;'>VisLorem ipsum dolor sit amet, conse adipiscing elit. Curabitur metus felis, venenatis eu ultricies vel, vehicula eu urna. Phasellus eget augue id est fringilla feugiat id a tellus. Sed hendrerit...</p>

        </p></div>

        <div class="col-md-4"><p>

        <img src="../images/admin2.jpg" width='200px'>
        <h4>How To View The Promotions Of The Wheel</h4>
		<p style='color:grey;'>VisLorem ipsum dolor sit amet, conse adipiscing elit. Curabitur metus felis, venenatis eu ultricies vel, vehicula eu urna. Phasellus eget augue id est fringilla feugiat id a tellus. Sed hendrerit...</p>

        </p></div>

        <div class="col-md-4"><p>
        
        <img src="../images/admin3.jpg" width='200px'>
       <h4>Welcome Our New Restaurant Partner</h4>
		<p style='color:grey;'>The Good Earth Chinese Restaurant and Bistro 99 from Arnoma Hotel Bangkok! Already famous in Asia,The Good Earth restaurant continuously delights guests with superb Cantonese Food......</p>

         </p></div>
        <div class="clearfix visible-md-block"></div>
        <div class="clearfix visible-lg-block "></div><!--This clearfix has to be set every 3 div because that's that col-lg break to a new line-->
    </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="white-bg">
	        <h2>WeeloY Blog</h2>
        <div class="col-md-8"><p>
        
		<p style='color:grey;'>Restaurants talk to Restaurant. Questions and Answers. Share your Experience.</p>
         </p></div>

        <div class="col-md-4"><p>
        
<input class="button subbutton btn btn-primary customColor"" value="Open Weeloy Blog">
         </p></div>

        <div class="clearfix visible-md-block"></div>
        <div class="clearfix visible-lg-block "></div><!--This clearfix has to be set every 3 div because that's that col-lg break to a new line-->
    </div>
    </div>
</div>


    <div class="row">
      <div class="col-md-4">
        <div class="white-bg NoMargin">
          <h2>Useful Links</h2>
          <ul class="usefullinks">
            <li><a href="">Latest News</a></li>
            <li><a href="">Recent Blogs</a></li>
            <li><a href="">Support Forums</a></li>
            <li><a href="">Company Information</a></li>
            <li><a href="">Term &amp; Conditions</a></li>
          </ul>
        </div>
      </div>
      <div class="col-md-4">
        <div class="white-bg NoMargin">
          <div data-jmodtip="&lt;strong&gt;Edit module&lt;/strong&gt;&lt;br /&gt;Newsletter&lt;br /&gt;Position: footer-b" data-jmodediturl="" class="rt-block box1 nomarginright jmoddiv">
            <div class="module-surround">
              <div class="module-title">
                <h2 class="title">Newsletter</h2>
              </div>
              <div class="acymailing_introtext">Stay tuned with our latest news!</div>
              <br />
              <form>
                <input type="text" value="" name="user[name]" class="form-control" placeholder="Name" disabled="disabled" id="user_name_formAcymailing21311">
                <br />
                <input type="text" value=""name="user[email]" class="form-control" placeholder="E-mail" disabled="disabled" id="user_email_formAcymailing21311">
                <br />
                <input type="submit" onclick="try{ return submitacymailingform('optin','formAcymailing21311'); }catch(err){alert('The form could not be submitted '+err);return false;}" name="Submit" value="Subscribe" class="button subbutton btn btn-primary customColor">
              </form>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="white-bg NoMargin">
          <h2>About Us</h2>
          <div class="custombox1">
            <p>We are Weeloy An awesome Social Distribution Partner.</p>
            <div class="aboutus">
              <ul>
                <li><span><i class="fa fa-home"></i>83 Amoy Street, Singapore 069902</span></li>
                <li><span><i class="fa fa-phone"></i></span></li>
                <li><span><i class="fa fa-envelope"></i> support@weeloy.com</span></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div id="CopyrightSec">
    <div class="container">
      <div class="row">
        <div class="col-md-4">
          <div class="SocialIcons"> <a target="_blank" href="http://www.facebook.com/" title="Facebook"><span class="fa fa-facebook"></span></a> <a target="_blank" href="http://www.twitter.com/" title="Twitter"><span class="fa fa-twitter"></span></a> <a target="_blank" href="http://www.google.com/" title="Google"><span class="fa fa-google-plus"></span></a> <a target="_blank" href="http://www.dribbble.com/" title="Dribbble"><span class="fa fa-dribbble"></span></a> <a target="_blank" href="http://www.youtube.com/" title="Youtube"><span class="fa fa-youtube"></span></a> </div>
          <div class="copyright">&copy; 2014 Design by&nbsp;Weeloy</div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type='text/javascript' src="js/bootstrap-slider.js"></script>
<script type='text/javascript' src="js/datepicker.js"></script>


<?
print_javascript();
?>

</body>
</html>