<?php
require_once("lib/composer/vendor/autoload.php"); 
require_once("inc/utilities.inc.php");

//require_once("lib/class.twig_cache.inc.php");

$loader = new Twig_Loader_Filesystem('templates');

if (strstr($_SERVER['HTTP_HOST'], 'www.weeloy.com')){   
    $twig = new Twig_Environment($loader,array( 'cache' => 'tmp/twig_cache'));
}else{
    $twig = new Twig_Environment($loader);
}

$twig->addGlobal("session", $_SESSION);
$twig->addFilter('var_dump', new Twig_Filter_Function('var_dump'));
$twig->addFilter( new Twig_SimpleFilter('cast_to_array', function ($stdClassObject) {
    $response = array();
    foreach ($stdClassObject as $key => $value) {
        $response[$key] =  $value;
    }
    return $response;
}));
$twig->addFilter( new Twig_SimpleFilter('explode_cuisine', function ($stdClassObject) {
                $cuis = explode('|', $stdClassObject);
                 $cuisine = '';
                 foreach ($cuis as $cui){
                     $cuisine .= $cui.', ';
                 }
                 return substr($cuisine, 0,strlen($cuisine)-2);
}));

$twig->addFilter( new Twig_SimpleFilter('get_restaurant_desc', function ($stdClassObject) {
                 $description = explode('||||', $stdClassObject);
                 return trim($description[0]);
}));


$twig->addFilter( new Twig_SimpleFilter('clean_description', function ($stdClassObject) {
                 $desc = str_replace('||||', '', $stdClassObject);
                 return trim($desc);
}));
$twig->addFilter( new Twig_SimpleFilter('get_review_class', function ($stdClassObject) {
                $result = (round($stdClassObject * 2) / 2) * 10;
                return 'review-'. $result;
}));


$lexer = new Twig_Lexer($twig, array(
    'tag_comment'   => array('{#', '#}'),
    'tag_block'     => array('{%', '%}'),
    'tag_variable'  => array('{[{', '}]}'), // was array('{{', '}}')
    'interpolation' => array('#{', '}'),
));
$twig->setLexer($lexer);
//$twig->addFilter( new Twig_SimpleFilter('test_slider', function ($value){print_slider($value, $value, $value, false);}));