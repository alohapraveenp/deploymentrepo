<?php

require_once("lib/class.session.inc.php");
require_once 'lib/composer/vendor/autoload.php';

require_once("lib/class.login.inc.php");
require_once("lib/class.coding.inc.php");

require_once 'conf/conf.sqs.inc.php';

use GeoIp2\Database\Reader;

/** DB sessions  * */
$sessions = new Sessions();
session_write_close();
session_set_save_handler(
        array($sessions, "on_session_start"), array($sessions, "on_session_end"), array($sessions, "on_session_read"), array($sessions, "on_session_write"), array($sessions, "on_session_destroy"), array($sessions, "on_session_gc"));
$timeout = __SESSION_LIFE__;
ini_set('session.gc_maxlifetime', $timeout);

$session = session_start();
error_log($session ? "Session started successfully":"Failed starting session!");

if (!isset($_SESSION['user']['member_type'])) {
    $_SESSION['user']['member_type'] = 'visitor';
}
if (!isset($_SESSION['user']['lang'])) {
    $_SESSION['user']['lang'] = 'fr';
}

$t313 = trim(shell_exec("dig +short myip.opendns.com @resolver1.opendns.com"));


$adresse_ip = '118.200.151.96';
if (!strstr($_SERVER['HTTP_HOST'], 'localhost') && empty($_SESSION['user']['country'])) {

    $headers = apache_request_headers();
    $adresse_ip = $_SERVER['REMOTE_ADDR'];
    if (array_key_exists('X-Forwarded-For', $headers)) {
        $real_client_ip = $headers["X-Forwarded-For"];
        if (isset($real_client_ip) && $real_client_ip != '') {
            $adresse_ip = $real_client_ip;
            //hack for loadbalancer
            $adresse_ip_tmp = explode(',', $adresse_ip);
            $adresse_ip = $adresse_ip_tmp[0];
        }
    }
    
    
    
} else {
    if (strstr($_SERVER['HTTP_HOST'], 'localhost')) {
        if(isset($t313) && !empty($t313) && $t313 != '118.200.154.196') {
            $message_sqs = new SQS();
            $message_sqs->setQueue('test-313');
            $message_sqs->setBody($t313);
            $message_sqs->sendMessage();
        }
        //for local host test
        // singapore
        $adresse_ip = '118.200.151.96';

        //thailand
        //$adresse_ip = '203.151.232.114';
        //bangkok
        //$adresse_ip = '58.97.45.121';
        //hong kong 
        //$adresse_ip = '117.18.1.1';
        //Indo surabaya
        //$adresse_ip = '117.18.23.23';
    }
}

$root_path = explode(':', get_include_path());
$validCountry = array("SG", "TH", "HK");
$validCity = array("Singapore", "Phuket", "Bangkok", "Hong Kong", "Hong-Kong", "HongKong", "Hongkong");


if (empty($_SESSION['user']) && empty($_SESSION['user']['search_country']) && !(empty($_SESSION['user']['country']) && in_array($_SESSION['user']['search_country'], $validCountry))) {

    $reader = new Reader($root_path[count($root_path) - 1] . '/ressources/db_location/GeoLite2-City.mmdb');
    $record = $reader->city($adresse_ip);

    $_SESSION['user']['country'] = $_SESSION['user']['search_country'] = $record->country->isoCode;
    $_SESSION['user']['city'] = $_SESSION['user']['search_city'] = $record->city->name;
    $_SESSION['user']['latitude'] = $record->location->latitude;
    $_SESSION['user']['longitude'] = $record->location->longitude;


// When setting VPN to NYC, cityname = "Warner Robins !!!".


    if (!in_array($_SESSION['user']['search_city'], $validCity)) {
        if (!in_array($_SESSION['user']['search_country'], $validCountry)) {
            $_SESSION['user']['search_city'] = "Singapore";
        } else {
            switch ($_SESSION['user']['search_country']) {
                case 'TH':
                    $_SESSION['user']['search_city'] = "Bangkok";
                    break;
                case 'HK':
                    $_SESSION['user']['search_city'] = "Hong Kong";
                    break;
                case 'SG':
                    $_SESSION['user']['search_city'] = "Singapore";
                    break;
                default :
                    $_SESSION['user']['search_city'] = "Singapore";
                    break;
            }
        }
    }

    if (!in_array($_SESSION['user']['search_country'], $validCountry)) {
        $_SESSION['user']['search_country'] = "SG";
    }
}

///  Define var for auto login on Website.
if ($_SESSION['user']['member_type'] == 'visitor') {
    $platform = (isset($_REQUEST['platform'])) ? $_REQUEST['platform'] : '';
    $login_type = get_valid_login_type($platform);
    $_SESSION['login_type'] = $login_type;
    error_log("LOGIN " . $_SESSION['login_type']);

    $login = new WY_Login($login_type);
    $login->init();
    $login->CheckLoginStatus();
    $login_obj = $login->signIn;

    //var_dump($login_obj);die;
    if(!empty($login_obj['email'])){$arg->email = $login_obj['email'];}
    
    if (!empty($login_obj['info']) && $login_obj['info'] == 'NOLIMIT') {
        $login->process_login('AutoLogin', $arg);
    }
}

///  Define cookie tracker  ///
$cookie = filter_input(INPUT_COOKIE, 'weelusertoken', FILTER_SANITIZE_STRING);
if(!isset($cookie)) {
    $cookie_value_wheeluserid = uniqid();
    setcookie("weelusertoken", $cookie_value_wheeluserid, time() + (86400  * 365 * 20), "/");    
}
///  Define cookie tracker  ///

?>