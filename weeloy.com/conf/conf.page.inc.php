<?php

    if (empty($curPage)) {
        $curPage = "home";
    }
    
    //$Users->LogEvent($_SESSION['userId'],50,'','',$curPage);

 
    $pages 		= new pages($curPage);

    
    /// REMOVE SOME logs
    if(empty($trsl[$_SESSION['user']['lang']])){$trsl[$_SESSION['user']['lang']] = array();}
    
    
    $pages->set_breadcrumb($trsl[$_SESSION['user']['lang']],$_SESSION['user']['lang']);
    
    if ($pages->get_php()){
		include_once($pages->get_php());
    }
     
    if (isset($sub_twig)) {
        $pages->add_subtemplate($sub_twig);
    }

    $toInclude = $pages->get_template_info();

    $twig_param['page'] = $toInclude;

?>