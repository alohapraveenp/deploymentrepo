<?php

require_once("lib/composer/vendor/autoload.php");   

require_once("inc/utilities.inc.php");

//echo realpath(dirname(__FILE__));
$loader = new Twig_Loader_Filesystem(get_include_path(). 'templates/email');

$twig = new Twig_Environment($loader,array('debug' => true));


$lexer = new Twig_Lexer($twig, array(
    'tag_comment'   => array('{#', '#}'),
    'tag_block'     => array('{%', '%}'),
    'tag_variable'  => array('{[{', '}]}'), // was array('{{', '}}')
    'interpolation' => array('#{', '}'),
));
$twig->setLexer($lexer);
//$twig->addFilter( new Twig_SimpleFilter('test_slider', function ($value){print_slider($value, $value, $value, false);}));