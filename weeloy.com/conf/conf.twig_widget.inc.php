<?php

require_once("lib/composer/vendor/autoload.php"); 
require_once("inc/utilities.inc.php");

$loader = new Twig_Loader_Filesystem('../widget');
$twig = new Twig_Environment($loader,array('debug' => true));


$lexer = new Twig_Lexer($twig, array(
    'tag_comment'   => array('{#', '#}'),
    'tag_block'     => array('{%', '%}'),
    'tag_variable'  => array('{[{', '}]}'), // was array('{{', '}}')
    'interpolation' => array('#{', '}'),
));
$twig->setLexer($lexer);