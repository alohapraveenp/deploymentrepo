<?php

$clientID = 50;
$authentification = "MSG";

$post_string = '
    <SOAP-ENV:Envelope xmlns:xsi = "http://www.w3.org/1999/XMLSchema/instance" xmlns:SOAP-ENV= "http://schemas.xmlsoap.org/soap/envelope" xsi:schemaLocation= "http://www.infogenesis.com/schemas/ver1.4/POSTransGatewaySchema.xsd">
        <SOAP-ENV:Body xsi:type= "client-authentication-request-Body">
            <client-authentication-request-Body>
                <client-authentication-request>
                    <client-id>' . $clientID . '</client-id>
                    <authentication-code>' . $authentification . '</authentication-code>
                </client-authentication-request>
            </client-authentication-request-Body>
        </SOAP-ENV:Body>
    </SOAP-ENV:Envelope>';


//$address = "agysremote.agilysys.asia";
$address = '61.8.239.79';

$url = parse_url($address);
$site = $url["host"];
$path = $url["path"];
$port = 7008;

$fp = fsockopen($address, $port, $errno, $errstr, 30);
if (!$fp) {
    echo "$errstr ($errno)<br />\n";
} else {
    // send request
    fputs($fp, "POST  /InfoGenesis  HTTP/1.1\r\n");
    fputs($fp, "Host: $address:$port\r\n");
    fputs($fp, "User-Agent: PHP/connect\r\n");
    fputs($fp, "Content-length: " . strlen($post_string) . "\r\n");
    fputs($fp, "\r\n");
    fputs($fp, $post_string);

    fputs($fp, "\r\n");
    // get data
    $doc = "";
    while (!feof($fp)) {
        $dd = fgets($fp, 8000);
        $doc .= $dd;
        if (preg_match("/\/SOAP/", $dd)) // bug in Agylysis system -> content-length should be small, it waits until timeout
            break;
    }
    fclose($fp);
    echo $doc;
}
?>