<?php

require_once("conf/conf.init.inc.php");
require_once("lib/wpdo.inc.php");
require_once("lib/class.debug.inc.php");

class WY_Agilysys_xml_api {

    private $url = 'agysremote.agilysys.asia';
    private $port = 80;
    private $tablename = 0;
    private $guestcount = 0;
    private $profitcenter = 0;
    private $employee = "";
    private $checktype = "";
    private $clientID = 0;
    private $sessionID = 0;
    private $authentification = "";
    private $header = array();
    private $body = '';
    private $restaurant = '';
    private $confirmation = '';
    private $mode = '';

    public function __construct($restaurant, $confirmation, $partner_booking_id, $table, $pax, $mode, $new_table) {

        //$this->url = "61.8.239.79";
        $this->restaurant = $restaurant;
        $this->confirmation = $confirmation;
        $this->partner_booking_id = $partner_booking_id;
        $this->tablename = $table;
        $this->new_table = $new_table;
        $this->guestcount = $pax;
        $this->mode = $mode;
        $this->port = 7008;
        $this->profitcenter = 10;
        $this->employee = '104';
        $this->checktype = "1";
        $this->clientID = 50;
        $this->sessionID = 0;
        $this->authentification = "MSG";

        if ($mode === "prod") {
            $this->url = "61.8.239.79";
            $this->profitcenter = 90;
            $this->employee = '99999';
            $this->clientID = 998;
            $this->authentification = "1234";
        }
    }

    public function openOrder() {


        $confirmation = $this->confirmation;
        $this->setHeaderBody();

        $this->body .= '
        <process-order-request-Body>
            <process-order-request>
                <trans-services-header>
                    <client-id>' . $this->clientID . '</client-id>
                    <session-id>' . $this->sessionID . '</session-id>
                    <authentication-code>' . $this->authentification . '</authentication-code>
                </trans-services-header>
                <order-type>open</order-type>
                <order-header>
                    <table-name>' . $this->tablename . '</table-name>
                    <employee-id>' . $this->employee . '</employee-id>
                    <guest-count>' . $this->guestcount . '</guest-count>
                    <profitcenter-id>' . $this->profitcenter . '</profitcenter-id>
                    <check-type-id>' . $this->checktype . '</check-type-id>
                    <receipt-required>no</receipt-required>
                </order-header>' .
//                <order-body>
//                    <item>
//                        <item-id>6</item-id>
//                        <item-quantity>1</item-quantity>
//                        <item-price></item-price>
//                        <item-kitchen-print-indicator>no</item-kitchen-print-indicator>
//                        <seat-number>1</seat-number>
//                    </item>
//                    <item>
//                        <item-id>10</item-id>
//                        <item-quantity>2</item-quantity>
//                        <item-price></item-price>
//                        <item-kitchen-print-indicator>no</item-kitchen-print-indicator>
//                        <seat-number>2</seat-number>
//                    </item>
//                    <item>
//                        <item-id>12</item-id>
//                        <item-quantity>1</item-quantity>
//                        <item-price></item-price>
//                        <item-kitchen-print-indicator>no</item-kitchen-print-indicator>
//                        <seat-number>1</seat-number>
//                    </item>
//                </order-body>

                '</process-order-request>
        </process-order-request-Body>';
        $this->setFooterBody();



        $resultat = $this->execute_query();

        WY_debug::recordDebug("DEBUG", "POS XML 1 execute_query", 'openOrder ' . $resultat);

        if ($resultat != "") {
            $Envelope = simplexml_load_string($resultat);
// xml name space declaration
            $Envelope->registerXPathNamespace('soap', 'http://schemas.xmlsoap.org/soap/envelope/');
            $Envelope->registerXPathNamespace('auth', 'http://developer.intuit.com/');

// you can use the prefixes that are already defined in the document
            $nodes = $Envelope->xpath('/SOAP-ENV:Envelope/SOAP-ENV:Body/process-order-response-Body/process-order-response/service-completion-status');
            $service_completion = (string) $nodes[0];

            $nodes = $Envelope->xpath('/SOAP-ENV:Envelope/SOAP-ENV:Body/process-order-response-Body/process-order-response/order-number');
            $order_id = (string) $nodes[0];

            $nodes = $Envelope->xpath('/SOAP-ENV:Envelope/SOAP-ENV:Body/process-order-response-Body/process-order-response/order-guid');
            $order_guid = (string) $nodes[0];

            WY_debug::recordDebug("DEBUG", "POS XML 2", 'openOrder ' . $service_completion . ' ' . $order_id . ' ' . $confirmation);

            //var_dump($service_completion, $order_id, $order_guid);
            if ($service_completion == 'ok') {
                $sql = "UPDATE booking SET partner_booking_id = '$order_id', partner_id = 3 WHERE confirmation = '$confirmation' limit 1;";
                pdo_exec($sql);
                return $order_id;
            }
        }
        return false;
    }

    public function saveOrderDetails($order_details = NULL) {
        
        if(empty($order_details)){
            $confirmation = $this->confirmation;
            $order_details = $this->getRemoteOrderDetails($confirmation);
        }else{
            $confirmation = '';
        }
        

        if (!isset($order_details['summary'])) {
            WY_debug::recordDebug("DEBUG", "POS saveOrderDetails", 'getRemoteOrderDetails no data ');
            return true;
        }
        
        $order_summary = $order_details['summary'];
        $order_items = $order_details['details'];

        $order_number = $order_summary['order-number'];
        $order['order_number'] = $order_summary['order-number'];
        $order['order_amount'] = $order_summary['gross-sales-amount'];
        $order['table'] = $order_summary['table'];
        
        if( empty(strlen($order_summary['table'])) || strlen($order_summary['table']) < 1){
            $order['table'] = 0;
        }

        $restaurant = $this->restaurant;

        $sql = "INSERT INTO `_integration_agilysys_order` (`restaurant`, `confirmation`, `table_num`, `order_id`, `amount`) "
                . "VALUES ('$restaurant', '$confirmation', :table, :order_number, :order_amount) ON DUPLICATE KEY UPDATE table_num = :table, amount = :order_amount;";

        pdo_insert($sql, $order);

        if(count($order_items) > 0){
            $sql = "DELETE FROM _integration_agilysys_order_details WHERE `restaurant` = '$restaurant' AND `confirmation` = '$confirmation' AND `order_id`= '$order_number'";
            pdo_exec($sql);
        }
        foreach ($order_items as $items) {
            $sql = "INSERT INTO `_integration_agilysys_order_details` (`restaurant`, `confirmation`, `order_id`, `item_id`, `qty`, `amount`, `seat`, `course`, `fired`) "
                    . "VALUES ('$restaurant', '$confirmation', '$order_number', :id, :qty, :amount, :seat, :course, :fired) ;";
            pdo_insert($sql, $items);
        }

        return true;
    }

    public function getOrderDetails() {
        $confirmation = $this->confirmation;

        $sql = "SELECT confirmation, order_id, amount FROM _integration_agilysys_order WHERE confirmation LIKE '$confirmation'";
        $data = pdo_single_select($sql);
        if (count($data) < 1) {
            return -1;
        }
        if (!empty($data['order_id'])) {
            $sql = "SELECT iaod.item_id, iaod.qty, iaod.amount, iaod.seat, iaod.course, iaod.fired, iaom.item_description, iaom.item_button_text, iaom.item_base_price FROM _integration_agilysys_order_details iaod, _integration_agilysys_menu iaom  "
                    . "WHERE  iaod.restaurant = iaom.restaurant and iaod.item_id = iaom.item_id and iaod.order_id LIKE '$data[order_id]'";

            $sql = "SELECT iaom.item_description FROM _integration_agilysys_order_details iaod, _integration_agilysys_menu iaom  "
                    . "WHERE  iaod.restaurant = iaom.restaurant and iaod.item_id = iaom.item_id and iaod.order_id LIKE '$data[order_id]'";

            $items = pdo_multiple_select($sql);
        }
        $resutlat['order_summary'] = $data;
        $resutlat['order_items'] = $items;
        return $resutlat;
    }

    public function simulateItemsOrder() {
        $confirmation = $this->confirmation;

        $this->setHeaderBody();

        $order = $this->getOrderId($confirmation);

        $this->body .= '
        <process-order-request-Body>
            <process-order-request>
                <trans-services-header>
                    <client-id>' . $this->clientID . '</client-id>
                    <session-id>' . $this->sessionID . '</session-id>
                    <authentication-code>' . $this->authentification . '</authentication-code>
                </trans-services-header>
                <order-type>open</order-type>
                <order-header>
                    <table-name>' . $this->tablename . '</table-name>
                    <employee-id>' . $this->employee . '</employee-id>
                    <guest-count>' . $this->guestcount . '</guest-count>
                    <profitcenter-id>' . $this->profitcenter . '</profitcenter-id>
                    <check-type-id>' . $this->checktype . '</check-type-id>
                    <order-number>' . $order . '</order-number>
                    <receipt-required>no</receipt-required>
                </order-header>
                <order-body>
                    <item>
                        <item-id>6</item-id>
                        <item-quantity>1</item-quantity>
                        <item-price></item-price>
                        <item-kitchen-print-indicator>no</item-kitchen-print-indicator>
                        <seat-number>1</seat-number>
                    </item>
                    <item>
                        <item-id>10</item-id>
                        <item-quantity>2</item-quantity>
                        <item-price></item-price>
                        <item-kitchen-print-indicator>no</item-kitchen-print-indicator>
                        <seat-number>2</seat-number>
                    </item>
                    <item>
                        <item-id>12</item-id>
                        <item-quantity>1</item-quantity>
                        <item-price></item-price>
                        <item-kitchen-print-indicator>no</item-kitchen-print-indicator>
                        <seat-number>1</seat-number>
                    </item>
                </order-body>
                </process-order-request>
        </process-order-request-Body>';
        $this->setFooterBody();

        $resultat = $this->execute_query();

        if ($resultat != "") {
            return true;
        	}
        return false;
    }

    public function getListOpenorder() {

        $this->setHeaderBody();

        $this->body .= '
        <order-list-request-Body>
            <order-list-request>
                <trans-services-header>
                    <client-id>' . $this->clientID . '</client-id>
                    <session-id>' . $this->sessionID . '</session-id>
                    <authentication-code>' . $this->authentification . '</authentication-code>
                </trans-services-header>
                <order-search-criteria>
                </order-search-criteria>
            </order-list-request>
        </order-list-request-Body>';
        $this->setFooterBody();

        $resultat = $this->execute_query();

        WY_debug::recordDebug("DEBUG", "POS XML 1 execute_query", 'getListOpenorder ' . $resultat);

        if ($resultat != "") {
            $Envelope = simplexml_load_string($resultat);
// xml name space declaration
            $Envelope->registerXPathNamespace('soap', 'http://schemas.xmlsoap.org/soap/envelope/');
            $Envelope->registerXPathNamespace('auth', 'http://developer.intuit.com/');

// you can use the prefixes that are already defined in the document
            $nodes = $Envelope->xpath('/SOAP-ENV:Envelope/SOAP-ENV:Body/order-list-response-Body/order-list-response/service-completion-status');
            $service_completion = (string) $nodes[0];

            $order_ids = $Envelope->xpath('/SOAP-ENV:Envelope/SOAP-ENV:Body/order-list-response-Body/order-list-response/order-list');
            $res_json = json_encode($order_ids);
            $res_array = json_decode($res_json);

            $resultat = array();

            foreach ($res_array[0] as $res) {
                foreach ($res as $r) {
                    $tmp = array();
                    $r = $r->{'@attributes'};
                    $tmp['order-number'] = $r->{"order-number"};
                    $tmp['table'] = $r->{"table"};
                    $tmp['employee-id'] = $r->{"employee-id"};
                    $resultat[] = $tmp;
                }
            }
            WY_debug::recordDebug("DEBUG", "POS XML 2", 'getListOpenorder ' . count($resultat));
            return $resultat;
        }

        return array();
    }

    public function updateTableListOpenorder() {
        $table_open = $this->getListOpenorder();
        $still_open = false;
        $response = array();

        foreach ($table_open as $table) {
            if ($table['order-number'] == $this->partner_booking_id) {
                $still_open = true;
                $response['status'] = 'open';

                if ($table['table'] != $this->tablename && $table['table'] != $this->new_table && $table['table'] != '0') {
                    $sql = "UPDATE booking SET restable = '$table[table]' WHERE confirmation = '$this->confirmation'";
                    $response['status'] = 'updated';
                    pdo_exec($sql);
                    $this->new_table = $table['table'];
                }
            }
        }
        if (!$still_open) {
            $response['status'] = 'not_found';
        }
        $response['check_id'] = $this->partner_booking_id;
        $response['table'] = $this->tablename;
        $response['new_table'] = $this->new_table;
        return $response;
    }

    public function getOrderSummary() {

        $confirmation = $this->confirmation;
        $order = $this->partner_booking_id; //$this->getOrderId($confirmation);

        $this->setHeaderBody();

        $this->body .= '
        <order-summary-request-Body>
            <order-summary-request>
                <trans-services-header>
                    <client-id>' . $this->clientID . '</client-id>
                    <session-id>' . $this->sessionID . '</session-id>
                    <authentication-code>' . $this->authentification . '</authentication-code>
                </trans-services-header>
                <order-summary-header>
                    <profitcenter-id>' . $this->profitcenter . '</profitcenter-id>
                    <employee-id>' . $this->employee . '</employee-id>
                    <order-number>' . $order . '</order-number>
                </order-summary-header>
            </order-summary-request>
        </order-summary-request-Body>';
        $this->setFooterBody();

        $resultat = $this->execute_query();

        WY_debug::recordDebug("DEBUG", "POS XML 1 execute_query", 'getOrderSummary ' . $resultat);

        if ($resultat != "") {
            $Envelope = simplexml_load_string($resultat);
// xml name space declaration
            $Envelope->registerXPathNamespace('soap', 'http://schemas.xmlsoap.org/soap/envelope/');
            $Envelope->registerXPathNamespace('auth', 'http://developer.intuit.com/');

// you can use the prefixes that are already defined in the document
            $nodes = $Envelope->xpath('/SOAP-ENV:Envelope/SOAP-ENV:Body/order-summary-response-Body/order-summary-response/service-completion-status');
            $service_completion = (string) $nodes[0];

            $order_ids = $Envelope->xpath('/SOAP-ENV:Envelope/SOAP-ENV:Body/order-summary-response-Body/order-summary-response/order-summary-response-data');
            $res_json = json_encode($order_ids);
            $res_array = json_decode($res_json);

            WY_debug::recordDebug("DEBUG", "POS XML 2", 'getOrderSummary ' . count($res_array));

            return get_object_vars($res_array[0]);
        }
        return false;
    }

    public function getOrderId() {
        $confirmation = $this->confirmation;
        $sql = "SELECT partner_booking_id FROM booking WHERE confirmation LIKE '$confirmation'";
        $data = pdo_single_select($sql);
        return $data['partner_booking_id'];
    }

    public function getOrderAmountDetails() {
        $confirmation = $this->confirmation;
        $order = $this->partner_booking_id; //$this->getOrderId($confirmation);
        $this->setHeaderBody();
        //$order = 500084;

        $this->body .= '
        <order-summary-request-Body>
            <order-summary-request>
                <trans-services-header>
                    <client-id>' . $this->clientID . '</client-id>
                    <session-id>' . $this->sessionID . '</session-id>
                    <authentication-code>' . $this->authentification . '</authentication-code>
￼               </trans-services-header>
                <order-summary-header>
                <profitcenter-id>' . $this->profitcenter . '</profitcenter-id>
                <employee-id>' . $this->employee . '</employee-id>
                <order-number>' . $order . '</order-number>
                </order-summary-header>
                </order-summary-request>
            </order-summary-request-Body>';
        $this->setFooterBody();

        $resultat = $this->execute_query();

        WY_debug::recordDebug("DEBUG", "POS XML 1 execute_query", 'getOrderAmountDetails ' . $resultat);

        if ($resultat != "") {
            $Envelope = simplexml_load_string($resultat);
// xml name space declaration
            $Envelope->registerXPathNamespace('soap', 'http://schemas.xmlsoap.org/soap/envelope/');
            $Envelope->registerXPathNamespace('auth', 'http://developer.intuit.com/');

// you can use the prefixes that are already defined in the document
            $nodes = $Envelope->xpath('/SOAP-ENV:Envelope/SOAP-ENV:Body/order-summary-response-Body/order-summary-response/service-completion-status');
            $service_completion = (string) $nodes[0];

            $nodes = $Envelope->xpath('/SOAP-ENV:Envelope/SOAP-ENV:Body/order-summary-response-Body/order-summary-response/order-summary-response-data');
            $order = $nodes[0];

            $res_json = json_encode($order);
            $res_array = json_decode($res_json);

            WY_debug::recordDebug("DEBUG", "POS XML 2", 'getOrderSummary ' . count($res_array));
            return get_object_vars($res_array);
        }
        return null;
    }

    public function getOrderAmount() {
        $confirmation = $this->confirmation;
        $res = $this->getOrderAmountDetails($confirmation);
        return (isset($res['order-amount'])) ? $res['order-amount'] : "";
    }

    public function getRemoteOrderDetails($order_number = NULL, $employee_id = NULL) {
        $confirmation = $this->confirmation;
        $this->setHeaderBody();
        $order = $this->partner_booking_id; //$this->getOrderId($confirmation);
        //$order = 500339;
        
        if(empty($order_number)){
                $order_number = $order;
        }
        
        if(empty($employee_id)){
                $employee_id = $this->employee;
        }
        
        $this->body .= '
        <order-detail-request-Body>
            <order-detail-request>
                <trans-services-header>
                    <client-id>' . $this->clientID . '</client-id>
                    <session-id>' . $this->sessionID . '</session-id>
                    <authentication-code>' . $this->authentification . '</authentication-code>
                </trans-services-header>
                <profitcenter-id>' . $this->profitcenter . '</profitcenter-id>
                <employee-id>' . $employee_id . '</employee-id>
                <order-number>' . $order_number . '</order-number>
            </order-detail-request>
        </order-detail-request-Body>';
        $this->setFooterBody();

        $response = $this->execute_query();

        WY_debug::recordDebug("DEBUG", "POS XML 1 execute_query", 'getRemoteOrderDetails ' . $response);

        if ($response != "") {
            $Envelope = simplexml_load_string($response);
// xml name space declaration
            $Envelope->registerXPathNamespace('soap', 'http://schemas.xmlsoap.org/soap/envelope/');
            $Envelope->registerXPathNamespace('auth', 'http://developer.intuit.com/');

// you can use the prefixes that are already defined in the document
            $nodes = $Envelope->xpath('/SOAP-ENV:Envelope/SOAP-ENV:Body/order-detail-response-Body/order-detail-response/service-completion-status');
            $service_completion = (string) $nodes[0];

            // GET SUMMARY
            $nodes = $Envelope->xpath('/SOAP-ENV:Envelope/SOAP-ENV:Body/order-detail-response-Body/order-detail-response/order-detail-data/summary-data');
            $order_summary = $nodes[0];

            $res_json = json_encode($order_summary);
            $res_array = json_decode($res_json);

            $resultat['summary'] = get_object_vars($res_array);

            // GET DETAILS
            $nodes = $Envelope->xpath('/SOAP-ENV:Envelope/SOAP-ENV:Body/order-detail-response-Body/order-detail-response/order-detail-data/order-data');
            $order_details = $nodes[0];

            $res_json = json_encode($order_details);
            $res_array = json_decode($res_json);
      
            WY_debug::recordDebug("DEBUG", "POS XML 2", 'getRemoteOrderDetails ' . count($res_array));

            $details = array();

            foreach ($res_array as $res) {
                foreach ($res as $r) {
                    
                    $tmp = array();
                    if(!empty($r->{'@attributes'})){
                        $r = $r->{'@attributes'};
                    }
                    
                    $tmp['id'] = $r->{"id"};
                    $tmp['qty'] = $r->{"qty"};
                    $tmp['amount'] = $r->{"amount"};
                    $tmp['seat'] = $r->{"seat"};
                    $tmp['course'] = $r->{"course"};
                    $tmp['fired'] = $r->{"fired"};
                    $details[] = $tmp;
                }
            }
            $resultat['details'] = $details;
            return $resultat;
        }
        return null;
    }

    public function updateFullMenu() {
        $this->setHeaderBody();
        $this->body .= '
            <item-data-request-Body>
                <item-data-request>
                    <trans-services-header>
                        <client-id>' . $this->clientID . '</client-id>
                        <session-id>' . $this->sessionID . '</session-id>
                        <authentication-code>' . $this->authentification . '</authentication-code>
                    </trans-services-header>
                </item-data-request>
            </item-data-request-Body>';
        $this->setFooterBody();

        $resultat = $this->execute_query();

        
        // continue from here

        $Envelope = new SimpleXMLElement($resultat);
        $Envelope->registerXPathNamespace('soap', 'http://schemas.xmlsoap.org/soap/envelope/');
        $Envelope->registerXPathNamespace('auth', 'http://developer.intuit.com/');


        $body = $Envelope->xpath('//SOAP-ENV:Envelope/SOAP-ENV:Body/item-data-response-Body/item-data-response/item-data');
        $res_json = json_encode($body);
        $res_array = json_decode($res_json);



        $keys_dico = array();
        foreach ($res_array as $key_cat => $cat_obj) {
            foreach ($cat_obj as $key => $obj) {
                //var_dump($key);
                if (isset($keys_dico[$key])) {
                    if ($keys_dico[$key] < strlen($obj)) {
                        $keys_dico[$key] = strlen($obj);
                    }
                } else {
                    $keys_dico[$key] = strlen($obj);
                }
            }
        }
        $keys = array();
        foreach ($keys_dico as $key) {
            $keys[] = $key;
        }
        //var_dump($keys );die;


        foreach ($res_array as $key_cat => $cat_obj) {

            $tmp = get_object_vars($cat_obj);

            $obj = array();

            $obj['itemid'] = $tmp['item-id'];
            $obj['itemdescription'] = $tmp['item-description'];
            $obj['itembuttontext'] = $tmp['item-button-text'];
            $obj['itembaseprice'] = $tmp['item-base-price'];
            $obj['itemproductclass'] = $tmp['item-product-class'];
            $obj['itemrevenueid'] = $tmp['item-revenue-id'];

            if (!empty($tmp['choicegroup-selection-data'])) {
                $obj['choicegroupselectiondata'] = json_encode($tmp['choicegroup-selection-data']);
            } else {
                $obj['choicegroupselectiondata'] = NULL;
            }


            //var_dump($obj['itemdescription']);die;

            $sql = "INSERT INTO `_integration_agilysys_menu` (`restaurant`, `item_id`, `item_description`, `item_button_text`, `item_base_price`, `item_product_class`, `item_revenue_id`, `choicegroup_selection_data`) "
                    . "VALUES ('SG_SG_R_Thanying', :itemid, :itemdescription, :itembuttontext, :itembaseprice, :itemproductclass, :itemrevenueid, :choicegroupselectiondata) "
                    . "ON DUPLICATE KEY UPDATE `item_description` = :itemdescription, `item_button_text` = :itembuttontext, `item_base_price` = :itembaseprice, `item_product_class`  = :itemproductclass, `item_revenue_id` = :itemrevenueid, `choicegroup_selection_data` = :choicegroupselectiondata";

            pdo_insert($sql, $obj);
        }
        return true;
    }
    
    public function saveOpenCheckDetails(){
        $list_open_order = $this->getListOpenorder();
        foreach ($list_open_order as $open_order){
            $order = $this->getRemoteOrderDetails($open_order['order-number'], $open_order['employee-id']);
            
            if(!empty($order['summary']) && $order['summary']["total-amount"] > 0  ){
                $this->saveOrderDetails($order);
            }
        }
        return true;
    }

    private function execute_query() {

        $this->setHeader();



        /*
          $this->body = '<?xml version="1.0"?>
          <SOAP-ENV:Envelope xmlns:xsi="http://www.w3.org/1999/XMLSchema/instance" xsi:schemaLocation="http://www.infogenesis.com/schemas/ver1.4/POSTransGatewaySchema.xsd" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope">
          <SOAP-ENV:Body xsi:type="process-order-request-Body">
          <process-order-request-Body>
          <process-order-request>
          <trans-services-header>
          <client-id>998</client-id>
          <session-id>0</session-id>
          <authentication-code>1234</authentication-code>
          </trans-services-header>
          <order-type>open</order-type>
          <order-header>
          <table-name>TESTING</table-name>
          <employee-id>99999</employee-id>
          <guest-count>0</guest-count>
          <profitcenter-id>91</profitcenter-id>
          <check-type-id>1</check-type-id>
          <receipt-required>yes</receipt-required>
          </order-header>
          </process-order-request>
          </process-order-request-Body>
          </SOAP-ENV:Body>
          </SOAP-ENV:Envelope>';

         */


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->header);
        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_PORT, $this->port);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->body);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $server_output = curl_exec($ch);

        curl_close($ch);
        return $server_output;
    }

    private function setHeader() {
        $this->header = array();
        $this->header[0] = "Content-Type: text/xml";
        $this->header[1] = 'Content-length: ' . strlen($this->body);
    }

    private function setHeaderBody() {
        $this->body = '    
            <SOAP-ENV:Envelope
            xmlns:xsi = "http://www.w3.org/1999/XMLSchema/instance"
            xmlns:SOAP-ENV= "http://schemas.xmlsoap.org/soap/envelope"
            xsi:schemaLocation= "http://www.infogenesis.com/schemas/ver1.4/POSTransGatewaySchema.xsd">
                <SOAP-ENV:Body xsi:type= "item-data-request-Body">';
    }

    private function setFooterBody() {
        $this->body .= '    
                </SOAP-ENV:Body>
            </SOAP-ENV:Envelope>';
    }

}

//
//    public function closeOrder() {
//        
//        
//    	$booking_confirmation = $this->confirmation;
//        $order = $this->getOrderId($booking_confirmation);
//        $this->setHeaderBody();
//
//        $this->body .= '
//        <process-order-request-Body>
//            <process-order-request>
//                <trans-services-header>
//                    <client-id>' . $this->clientID . '</client-id>
//                    <session-id>' . $this->sessionID . '</session-id>
//                    <authentication-code>' . $this->authentification . '</authentication-code>
//                </trans-services-header>
//                <order-type>closed</order-type>
//                <order-header>
//                    <order-number>500363</order-number>
//                    <table-name>' . $this->tablename . '</table-name>
//                    <employee-id>' . $this->employee . '</employee-id>
//                    <guest-count>' . $this->guestcount . '</guest-count>
//                    <profitcenter-id>' . $this->profitcenter . '</profitcenter-id>
//                    <check-type-id>' . $this->checktype . '</check-type-id>
//                    <receipt-required>no</receipt-required>
//                </order-header>
//                
//                
//                <order-payment>
//<payment-data>
//<tender-id>1</tender-id>
//<tender-amount-total>0</tender-amount-total>
//<tip-amount>0</tip-amount>
//</payment-data>
//</order-payment>
//                </process-order-request>
//        </process-order-request-Body>';
//        $this->setFooterBody();
//
//        $resultat = $this->execute_query();
//
//        if ($resultat != "") {
//            $Envelope = simplexml_load_string($resultat);
//// xml name space declaration
//            $Envelope->registerXPathNamespace('soap', 'http://schemas.xmlsoap.org/soap/envelope/');
//            $Envelope->registerXPathNamespace('auth', 'http://developer.intuit.com/');
//
//// you can use the prefixes that are already defined in the document
//            $nodes = $Envelope->xpath('/SOAP-ENV:Envelope/SOAP-ENV:Body/process-order-response-Body/process-order-response/service-completion-status');
//            $service_completion = (string) $nodes[0];
//
//            $nodes = $Envelope->xpath('/SOAP-ENV:Envelope/SOAP-ENV:Body/process-order-response-Body/process-order-response/order-number');
//            $order_id = (string) $nodes[0];
//
//            $nodes = $Envelope->xpath('/SOAP-ENV:Envelope/SOAP-ENV:Body/process-order-response-Body/process-order-response/order-guid');
//            $order_guid = (string) $nodes[0];
//
//            //var_dump($service_completion, $order_id, $order_guid);
//            if ($service_completion == 'ok') {
//                $sql = "UPDATE booking SET partner_booking_id = '$order_id', partner_id = 3 WHERE booking.confirmation = '$booking_confirmation';";
//                pdo_exec($sql);
//                return $order_id;
//            }
//        }
//    }
//
//    
//    
//    public function updateTable($table = 'P2') {
//    	$booking_confirmation = $this->confirmation;
//        $this->setHeaderBody();
//
//        $order = $this->getOrderId($booking_confirmation);
//        
//        $this->tablename = $table;
//                
//        $this->body .= '
//        <process-order-request-Body>
//            <process-order-request>
//                <trans-services-header>
//                    <client-id>' . $this->clientID . '</client-id>
//                    <session-id>' . $this->sessionID . '</session-id>
//                    <authentication-code>' . $this->authentification . '</authentication-code>
//                </trans-services-header>
//                <order-type>open</order-type>
//                <order-header>
//                    <table-name>TY05</table-name>
//                    <employee-id>' . $this->employee . '</employee-id>
//                    <guest-count>' . $this->guestcount . '</guest-count>
//                    <profitcenter-id>' . $this->profitcenter . '</profitcenter-id>
//                    <check-type-id>' . $this->checktype . '</check-type-id>
//                    <order-number>' . $order . '</order-number>
//                    <receipt-required>no</receipt-required>
//                </order-header>' .
//
//                
//
//                '</process-order-request>
//        </process-order-request-Body>';
//        
////        $this->body .= '<table-data-request-Body>
////            <table-data-request>
////            <trans-services-header>
////            <client-id>' . $this->clientID . '</client-id>
////            <session-id>' . $this->sessionID . '</session-id>
////            <authentication-code>' . $this->authentification . '</authentication-code>
////            </trans-services-header>
////            </table-data-request>
////            </table-data-request-Body>';
//        
//        $this->setFooterBody();
//
//        $resultat = $this->execute_query();
//
//        if ($resultat != "") {
//            $Envelope = simplexml_load_string($resultat);
//// xml name space declaration
//            $Envelope->registerXPathNamespace('soap', 'http://schemas.xmlsoap.org/soap/envelope/');
//            $Envelope->registerXPathNamespace('auth', 'http://developer.intuit.com/');
//
//// you can use the prefixes that are already defined in the document
//            $nodes = $Envelope->xpath('/SOAP-ENV:Envelope/SOAP-ENV:Body/process-order-response-Body/process-order-response/service-completion-status');
//            $service_completion = (string) $nodes[0];
//
//            $nodes = $Envelope->xpath('/SOAP-ENV:Envelope/SOAP-ENV:Body/process-order-response-Body/process-order-response/order-number');
//            $order_id = (string) $nodes[0];
//
//            $nodes = $Envelope->xpath('/SOAP-ENV:Envelope/SOAP-ENV:Body/process-order-response-Body/process-order-response/order-guid');
//            $order_guid = (string) $nodes[0];
//
//            //var_dump($service_completion, $order_id, $order_guid);
//            if ($service_completion == 'ok') {
//                $sql = "UPDATE booking SET partner_booking_id = '$order_id', partner_id = 3 WHERE booking.confirmation = '$booking_confirmation';";
//                pdo_exec($sql);
//                return $order_id;
//            }
//        }
//    }
//
//    
    
    