 <?php

$tablename = 16;
$guestcount = 4;
$profitcenter = 10;
$employee = '104';
$checktype = "1";
$clientID = 50;
$sessionID = 0;
$authentification = "MSG";

// you will receive the '<order-number>500084</order-number>' to be used for further query on that table

$post_string = '<SOAP-ENV:Envelope
       xmlns:xsi = "http://www.w3.org/1999/XMLSchema/instance"
       xmlns:SOAP-ENV= "http://schemas.xmlsoap.org/soap/envelope"
	   xsi:schemaLocation= "http://www.infogenesis.com/schemas/ver1.4/POSTransGatewaySchema.xsd">
       <SOAP-ENV:Body xsi:type= "process-order-request-Body">
<process-order-request-Body>
   <process-order-request>
      <trans-services-header>
         <client-id>' . $clientID . '</client-id>
         <session-id>' . $sessionID . '</session-id>
         <authentication-code>' . $authentification . '</authentication-code>
       </trans-services-header>
      <order-type>open</order-type>
      <order-header>
         <table-name>' . $tablename . '</table-name>
         <employee-id>' . $employee . '</employee-id>
         <guest-count>' . $guestcount . '</guest-count>
         <profitcenter-id>' . $profitcenter . '</profitcenter-id>
         <check-type-id>' . $checktype . '</check-type-id>
         <receipt-required>no</receipt-required>
      </order-header>
   </process-order-request>
</process-order-request-Body>
       </SOAP-ENV:Body>
      </SOAP-ENV:Envelope>';


	$address = "agysremote.agilysys.asia";
	$url = parse_url($address);
	$site = $url["host"];
	$path = $url["path"];
	$port = 7008;

	$fp = fsockopen ($address, $port, $errno, $errstr, 30);
	if (!$fp) {
		echo "$errstr ($errno)<br />\n";
	} else {
    // send request
    fputs ($fp, "POST  /InfoGenesis  HTTP/1.1\r\n");
    fputs ($fp, "Host: $address:$port\r\n");
    fputs ($fp, "User-Agent: PHP/connect\r\n");
    fputs ($fp, "Content-length: ".strlen($post_string)."\r\n");
    fputs ($fp, "\r\n");
    fputs ($fp, $post_string);

   	fputs ($fp, "\r\n");
    // get data
    $doc ="";
    while (!feof($fp)) {
        $dd = fgets($fp,8000);
        $doc .= $dd;
        if(preg_match("/\/SOAP/", $dd)) // bug in Agylysis system -> content-length should be small, it waits until timeout
        	break;
     }
    fclose ($fp);
    echo $doc;
	}
?>