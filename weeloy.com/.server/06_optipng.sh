#!/bin/bash

mkdir -p /var/log/deploy/
LOGFILE="/var/log/deploy/06_optipng.log"

mkdir -p /tmp/optipng
cp -r /var/www/html/.server/optipng/* /tmp/optipng/
cd /tmp/optipng
./configure
make
make test
make install
rm -rf /tmp/optipng