#!/bin/bash

mkdir -p /var/log/deploy/
LOGFILE="/var/log/deploy/05_jpegoptim.log"

mkdir -p /tmp/jpegoptim
cp -r /var/www/html/.server/jpegoptim/* /tmp/jpegoptim/
cd /tmp/jpegoptim
./configure
make
make strip
make install
rm -rf /tmp/jpegoptim