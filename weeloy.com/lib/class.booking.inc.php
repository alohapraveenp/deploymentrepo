<?php

// $langtemplate = array( 'af', /* afrikaans */ 'ar', /* arabic */ 'bg', /* bulgarian */ 'ca', /* catalan */ 'cs', /* czech */ 'da', /* danish */ 'de', /* german */ 'el', /* greek */ 'en', /* english */ 'es', /* spanish */ 'et', /* estonian */ 'fi', /* finnish */ 'fr', /* french */ 'gl', /* galician */ 'he', /* hebrew */ 'hi', /* hindi */ 'hr', /* croatian */ 'hu', /* hungarian */ 'id', /* indonesian */ 'it', /* italian */ 'ja', /* japanese */ 'ko', /* korean */ 'ka', /* georgian */ 'lt', /* lithuanian */ 'lv', /* latvian */ 'ms', /* malay */ 'nl', /* dutch */ 'no', /* norwegian */ 'pl', /* polish */ 'pt', /* portuguese */ 'ro', /* romanian */ 'ru', /* russian */ 'sk', /* slovak */ 'sl', /* slovenian */ 'sq', /* albanian */ 'sr', /* serbian */ 'sv', /* swedish */ 'th', /* thai */ 'tr', /* turkish */ 'uk', /* ukrainian */ 'zh' /* chinese */ ); 
require_once("lib/class.debug.inc.php");
require_once("lib/class.restaurant.inc.php");
require_once("lib/class.allote.inc.php");
require_once("lib/class.media.inc.php");
require_once "lib/class.translation.inc.php";
require_once "lib/class.notification.inc.php";
require_once "lib/class.partner.inc.php";
require_once("lib/class.profile.inc.php");
require_once("lib/class.pos_integration.inc.php");
require_once("lib/class.blocklist.inc.php");
require_once("lib/class.analytics.inc.php");
require_once("lib/class.restaurant_cancel_policy.inc.php");
require_once("lib/class.payment.inc.php");
require_once("lib/Browser.inc.php");
require_once("lib/class.replication.inc.php");

class WY_restaurantConf {

    var $restaurant;
    var $title;
    var $tel;
    var $email;
    var $smsid;
    var $URLrestaurant;
    var $logo;
    var $address;
    var $zip;
    var $city;
    var $country;
    var $GPS;
    var $images;
    var $map;
    var $url;
    var $restaurant_tnc;
    var $status;
    var $is_bookable;
    var $is_wheelable;
    var $internal_path;

}

class WY_Booking {

    use CleanIng;
	use IpWorld;

    var $restaurant;
    var $confirmation;
    var $type;
    var $status;
    var $restCode;
    var $membCode;
    var $salutation;
    var $firstname;
    var $lastname;
    var $email;
    var $mobile;
    var $cdate;
    var $rdate;
    var $rtime;
    var $cover;
    var $product;
    var $company;
    var $hotelguest;
    var $state;
    var $flag;
    var $canceldate;
    var $revenue;
    var $generic;
    var $specialrequest;
    var $mealtype;
    var $wheelsegment;
    var $wheelwin;
    var $wheeldesc;
    var $wheelwinangle;
    var $country;
    var $language;
    var $tracking;
    var $booker;
    var $partner_booking_id;
    var $partner_id;
    var $ip;
    var $tablename;
    var $restable;
    var $browser;
    var $msg;
    var $mailer;
    var $restaurantinfo;
    var $reviewgrade;
    var $bookings;
    var $result;
    var $bookid;
    var $deposit_id;
    var $updatestatus;
    var $optin;
            

    function __construct() {
        $this->restaurant = "";
    }

    function error_msg($val, $msg) {

        $this->msg = $msg;
        return $this->result = $val;
    }

    function trace_error($e) {
        $trace = preg_replace("/\'|\"/", "`", $e->getTraceAsString());
        $traceAr = $e->getTrace();
        $file = preg_replace("/^.*\//", "", $traceAr[0]['file']);
        $msg = $e->getMessage() . " - " . $file . " - " . $traceAr[0]['line'];
        WY_debug::recordDebug("class-booking", "BOOKING", $msg . " \n " . $trace);
    }

	function createwalking($theRestaurant, $obj) {

	    date_default_timezone_set("Asia/Singapore");

		$rdate = date("Y-m-d");
		$rtime = date("H:i");
		$email = $obj["email"];
		$mobile = $obj["phone"];
		$cover = $obj["pers"];
		$salutation = "Mr";
		$firstname = $obj["first"];
		$lastname = $obj["last"];
		$tablename = $obj["tablename"];
		$state = $obj["state"];
		$duration = $obj["duration"];
		$cleartime = $obj["cleartime"];
		$captain = $obj["captain"];
		$seated = $obj["seated"];
		$country = "Singapore";
		$language = "en";
		$specialrequest = $obj["comment"];
		$type = "booking";
		$tracking = "walkin|tms";
		$booker = "";
		$company = "";
		$hotelguest = "";
		$optin = "";
		$booking_deposit_id = "";
		$status = "";
		$extra = "";
		$product = "";
		$browser = NULL;
		$generic = preg_replace("/\'|\"/", "’", $obj["more"]);
		
		$this->createBooking($theRestaurant, $rdate, $rtime, $email, $mobile, $cover, $salutation, $firstname, $lastname, $country, $language, $specialrequest, $type, $tracking, $booker, $company, $hotelguest, $state, $optin, $booking_deposit_id, $status, $extra, $product, $browser = NULL);
		if($this->result > 0) {
			pdo_exec("UPDATE booking SET generic = '$generic', tablename = '$tablename' WHERE confirmation = '$this->confirmation' limit 1");

			if($tablename != "") {
				$this->setBooking($this->restaurant, $this->confirmation, "seated", "state");
				}
			if($captain != "") {
				$captain = preg_replace("/\'|\"|’/", "`", $captain);
				$this->addtoGeneric('{"more":"{’captain’: ’' . $captain . '’}"}');
				}
			if($seated == "1") {
				$captain = preg_replace("/\'|\"|’/", "`", $captain);
				$this->addtoGeneric('{"more":"{’seated’: ’' . $rtime . '’}"}');
				}
			}

		return $this->confirmation;
		}
		
    function createBooking($theRestaurant, $rdate, $rtime, $email, $mobile, $cover, $salutation, $firstname, $lastname, $country, $language, $specialrequest, $type, $tracking, $booker, $company, $hotelguest, $state, $optin, $booking_deposit_id, $status, $extra, $product, $browser = NULL) {
        
        $argv = array('theRestaurant', 'rdate', 'rtime', 'email', 'mobile', 'cover', 'salutation', 'firstname', 'lastname', 'country', 'language', 'specialrequest', 'type', 'tracking', 'booker', 'company', 'hotelguest', 'state', 'optin', 'booking_deposit_id', 'status', 'extra', 'product');
        $ret = "";
        for ($i = 0; $i < count($argv); $i++) {
            $tt = $argv[$i];
            $org = $$tt;
            $$tt = $this->cleanpattern($$tt, "&nbsp;");
            if (($language == 'cn' || $language == 'hk' || $language == 'th' || $language == 'my' || $language == 'jp' || $language == 'kr' || $language == 'id' || $language == 'ru' || $language == 'vi') &&
                    ($tt == "salutation" || $tt == "firstname" || $tt == "lastname" || $tt == "specialrequest"))
                $$tt = $this->clean_input($$tt);
            else if ($tt == "extra")
                $$tt = $this->clean_input($$tt);
            else
                $$tt = $this->clean_text($$tt);

            if ($tt == "mobile")
                $$tt = $this->clean_tel($$tt);
        }

        if ($this->email_validation($email) < 0) {
            return $this->error_msg(-1, "Invalid email " . $email);
        }
		
		$bkIP = $this->getIP();
        date_default_timezone_set("Asia/Singapore");
        $cdateday = date("Y-m-d");

        $res = new WY_restaurant;
        $res->getRestaurant($theRestaurant);
        
 		$hasBeenCheck = "noavailcheck";
       
        //checking if country empty => restaurant country
        
        if(empty($country)){$country = $res->country;}
        if ($res->result < 0) {
            WY_debug::recordDebug("ERROR", "BOOKING", "Invalid restaurant " . $theRestaurant . ". Createdate =  " . $cdateday . " . Booking date = " . $rdate . ", ip=" . $bkIP);
            return $this->error_msg(-1, "Invalid restaurant " . $theRestaurant);
        }
        
        
        /*************************************************************************************************/
            //checking  burntends black listing
        
        if($res->checkBlockedList() > 0){
            $eobj = new WY_Blokedlist($theRestaurant);
            $isBlocked = $eobj->checkblockedUser($email,$mobile);
            if($isBlocked){
                $contact_email ='sales@weeloy.com';
                if($theRestaurant == 'SG_SG_R_BurntEnds'){
                    $contact_email ='enquiries@burntends.com.sg';
                }
                return $this->error_msg(-1, "For this request, please contact the restaurant at ".$contact_email);
            }
        }
        
       /*************************************************************************************************/

		$testingDup = ($email != "no@email.com" && $mobile != "+65 99999999" && $lastname != "nolastname" && $firstname != "nofirstname" && preg_match("/CHOPE|HGW|QUANDOO/i", $booker) == false);
        // test re-submission, use now() to bypass the cache
        if($testingDup) {
			$data = pdo_single_select("SELECT confirmation, now() as nn from booking WHERE restaurant = '$theRestaurant' and rdate = '$rdate' and rtime = '$rtime' and cover = '$cover' and email = '$email' and mobile = '$mobile' and lastname = '$lastname' and firstname = '$firstname' and booker = '$booker' and (status = '' or status = 'pending_payment') limit 1");
			if (count($data) > 0) {
				WY_debug::recordDebug("ERROR", "DBL-BOOKING", "Restaurant =  " . $theRestaurant . ", date = " . $rdate . ", time = " . $rtime . ", email = " . $email . ", mobile = " . $mobile . ", lastname = " . $lastname . ", firstname = " . $firstname . ", covers = " . $cover . ", ip=" . $bkIP . ", nn=" . $data["nn"]);
				return $this->error_msg(-1, "You have alreay made a previous booking with the same information: " . $data['confirmation']);
				}
			}

        if ((empty($tracking) || !preg_match("/walkin|tms|waiting|overwrite|remote/i", $tracking)) && $type != "event") { // don't check if walkin and tms or type event
			// check that rdate >= cdate
			$log = w_checkdate($cdateday, $rdate);
			if ($log < 0) {
				WY_debug::recordDebug("ERROR", "BOOKING", "wrong date ($log). Createdate =  " . $cdateday . " . Booking date = " . $rdate . ", ip=" . $bkIP);
				return $this->error_msg(-1, "Invalid date $cdateday, $rdate");
			}

			$emptybooking = '';
			$res->CheckAvailability($theRestaurant, $rdate, $rtime, $cover, $emptybooking, $product);
			if ($res->result <= 0) {
				$str = ""; for ($i = 0; $i < count($argv); $i++) { $tt = $argv[$i];  $str .= $tt . '=' . $$tt . ','; }
				$str .= 'callerror = ' . $res->msg;
				WY_debug::recordDebug("ERROR", "BOOKING", "No more availability. Restaurant =  " . $theRestaurant . " - " . $str);
				return $this->error_msg(-1, $res->msg . "No more availability. Restaurant =  " . $theRestaurant . ", date = " . $rdate . ", time = " . $rtime . ", covers = " . $cover);
				}
			$hasBeenCheck = "availcheck" . $res->result;
        }

        //define a CPP booking or weeloy
        if (empty($tracking) || !preg_match("/callcenter|website|facebook|walkin|tms|waiting|overwrite|remote/i", $tracking)) {
            $tracking .= (!empty($tracking) ? "|" : "") . 'weeloy';
			//$type = "weeloy";
			
            if (preg_match("/credit-suisse.com|weeloy.com/i", $email) == true) {
                if ($res->checkRestaurantPartnership($theRestaurant, 'cpp_credit_suisse') != false) {
                     $tracking .= (!empty($tracking) ? "|" : "") . 'cpp_credit_suisse';
                }
            }
        }

        $generic = $sep = "";
        if ($extra !== "") {
            $extra_match = array("area" => 1, "purpose" => 1, "choice" => 1, "notescode" => 0); // 1 -> add it to the specialrequest
            foreach ($extra_match as $key => $val) {
                $$key = "";
                if (preg_match("/" . $key . "=([^|]+)/", $extra, $match)) {
                    if (!empty($match[1])) {
                        $$key = $match[1];
                        $generic .=  $sep . '’' . $key . '’:’' . $$key . '’';
                        $sep = ', ';
                        }
 				if (!empty($$key) && $val == 1)
					$specialrequest .= ((!empty($specialrequest)) ? ", " : "") . $$key;
               }
            }
		}
		if(!empty($generic)) 
			$generic = '{' . $generic . '}';

        if(empty($browser)){
            $browser = new Browser();
            $browser = "name=" . $browser->getBrowser() . "|version=" . $browser->getVersion() . "|platform=" . $browser->getPlatform() . "|language=" . substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
        }
        $this->browser = $browser;
        $this->ip = $bkIP;
        $this->restaurant = $theRestaurant;
        $this->status = $status;
        $this->cdate = $cdateday . ' ' . date("H:i");
        $this->type = $type;
        $this->rdate = $rdate;
        $this->rtime = $rtime;
        $this->email = $email;
        $this->mobile = $mobile;
        $this->cover = $cover;
        $this->product = $product;
        $this->company = $company;
        $this->salutation = $salutation;
        $this->firstname = $firstname;
        $this->lastname = $lastname;
        $this->country = $country;
        $this->language = $language;
        $this->tracking = $tracking;
        $this->booker = $booker;
        $this->specialrequest = $specialrequest;
        $this->hotelguest = $hotelguest;
        $this->state = $state;
        $this->generic = $generic;

        $avgpAr = explode(",", $res->pricing);
        if (count($avgpAr) < 0)
            $avgpAr[0] = 100;
        if (count($avgpAr) == 1)
            $avgpAr[1] = $avgpAr[0];
        $this->revenue = ((intval($avgpAr[0]) + intval($avgpAr[1])) / 2) * $this->cover;

        list($this->confirmation, $this->restCode, $this->membCode) = $this->createUniqConfNumer();

        if (!empty($res->wheelRescode) && strlen($res->wheelRescode) == 4)
            $this->restCode = $res->wheelRescode;

        // give the same last member code. Possibly we can configure it in the member account
        // added membercod  in member account
        $data = pdo_single_select("select email, membercode from member where email = '$this->email'  order by ID DESC limit 1");
        if (count($data) > 0 && strlen($data['membercode']) == 4 && preg_match("/^\d{4}$/", $data['membercode']))
            $this->membCode = $data['membercode'];

        //$data = pdo_single_select("select confirmation, restCode, membCode, restaurant from booking where email = '$this->email' && membCode != '0000' order by ID DESC limit 1");
        //if (count($data) > 0 && strlen($data['membCode']) == 4 && preg_match("/^\d{4}$/", $data['membCode']))
        // $this->membCode = $data['membCode'];

        if (preg_match("/TheFunKitchen|OneKitchen/", $this->restaurant))
            $this->membCode = "1122";
        if (!$res->is_wheelable || $email == "no@email.com" || $mobile == "+65 99999999")
            $this->membCode = "0000";

        //INSERT BOOKING IN DB
        if ($optin != "1")
            $optin = "";

        // test re-submission, again, use now() to bypass the cache
        if($testingDup) {
			$data = pdo_single_select("SELECT count(*) as value, now() as nn from booking WHERE restaurant = '$theRestaurant' and rdate = '$rdate' and rtime = '$rtime' and cover = '$cover' and email = '$email' and mobile = '$mobile' and lastname = '$lastname' and firstname = '$firstname' and booker = '$booker' and (status = '' or status = 'pending_payment')  ");
			if(intval($data["value"]) > 0) {
				WY_debug::recordDebug("ERROR", "DBL-BOOKING2", "Restaurant =  " . $theRestaurant . ", date = " . $rdate . ", time = " . $rtime . ", email = " . $email . ", mobile = " . $mobile . ", lastname = " . $lastname . ", firstname = " . $firstname . ", covers = " . $cover . ", ip=" . $bkIP . ", nn=" . $data["nn"]);
				return $this->error_msg(-1, "You have alreay made some previous booking with the same information");
				}
			}

		$now = (strstr($_SERVER['HTTP_HOST'], 'localhost:8888')) ? "NOW()" : "DATE_ADD(NOW(), INTERVAL 8 HOUR)";
        $sql = "INSERT INTO booking (confirmation, type, restCode, membCode, restaurant, status, rdate, cdate, rtime, email, mobile, country, cover, product, company, hotelguest, salutation, lastname, firstname, mealtype, specialrequest, revenue, browser, language, tracking, booker, ip, state, optin, generic, booking_deposit_id) VALUES ('$this->confirmation','$this->type', '$this->restCode', '$this->membCode', '$this->restaurant', '$this->status', '$this->rdate', $now, '$this->rtime', '$this->email', '$this->mobile', '$this->country', '$this->cover', '$this->product', '$this->company', '$this->hotelguest', '$this->salutation', '$this->lastname', '$this->firstname', '$this->mealtype', '$this->specialrequest', '$this->revenue', '$this->browser', '$this->language', '$this->tracking', '$this->booker', '$this->ip', '$state', '$optin', '$this->generic', '$booking_deposit_id')";
        $result = pdo_exec($sql);
        
        $logger = new WY_log("website");
        $logger->LogEvent('', 100, 0, $this->confirmation, $hasBeenCheck, date("Y-m-d H:i:s"));
        
	$this->postBooking($this->confirmation);
	}

   function postBooking($confirmation) {
	
        if ($this->getBooking($confirmation) == false) {
			WY_debug::recordDebug("ERROR", "BOOKING", "Unable to find booking. Confirmation =  " . $confirmation . " - " . $this->msg);
			return $this->error_msg(-1, $this->msg . ", Unable to find booking. Confirmation =  " . $confirmation . " - " . $this->msg);
			}
		
	$mobile = $this->mobile;
	$email = $this->email;

	try {
		// restaurant profile
		$prof = new WY_Profile();
		$prof->insertProf($this);

		// merge booking code and profile code
		if(!empty($prof->subprofcode)) {
        	WY_log::insertLog(0, 6001, 'BOOKING-PROFCODE', $this->restaurant, $confirmation, date("Y-m-d H:i"), '', '', 1);
			$obj = preg_replace("/’/", "\"", $this->generic);
			$objAr = json_decode($obj, true);
			$notescode = (!empty($objAr["notescode"])) ? $objAr["notescode"] : "";
			$mergecode = $this->merge($notescode, $prof->subprofcode, ",");
			if(!empty($mergecode) && $notescode != $mergecode && strlen($mergecode) > strlen($notescode))
				$this->addtoGeneric('{"more":"{’notescode’: ’' . $mergecode . '’}"}');			
			}

		if ($this->notifitycondition($this->tracking, $this->booker, $this->type)) {
			$this->updateGuestBookingCount($this->restaurant, $this->email, 'booking', 'noofbooking');
			}

		// every booker become a member
		if ($this->email != "no@email.com" && !preg_match("/walkin|CALLCENTER|tms|waiting|WEBSITE|facebook|GRABZ|remote/i", $this->tracking) && $this->tracking == '') {
			$login = new WY_Login(set_valid_login_type("member"));
			$firsttimeuser = $login->register_booking($this->email, $this->firstname, $this->lastname, $this->mobile, $this->country, $this->membCode);
			if ($firsttimeuser == true) {
				$gender = (substr($this->salutation, 0, 2) == 'Mr') ? "Male" : "Female";
				pdo_exec("UPDATE member SET gender = '$gender', salutation ='$this->salutation' WHERE email = '$this->email'");
				}
			}

		if (preg_match('/99999999/', $this->mobile))
			$mobile = ""; // default mobile number for tms
		if (preg_match('/@email.com/', $this->email))
			$email = "";
		
		if(!empty($email) || !empty($mobile)) {
			if(!empty($email) && !empty($mobile)) 
				$condition = "(email = '$email' or mobile = '$mobile') ";
			else if(!empty($email))
				$condition = "email = '$email'";
			else $condition = "mobile = '$mobile'";

			$data = pdo_single_select("select count(*) as total from booking where restaurant = '$this->restaurant' && $condition and status = '' ");
			if (count($data) > 0 && !empty($data['total']) && intval($data['total']) > 1) {
				$repeat = intval($data['total']) ;
				$this->addtoGeneric('{"more":"{’repeat’: ’' . $repeat . '’}"}');
				$prof->updateRepeat($repeat);
				}
			}

		// booking for madison Room
		if ($this->restaurant == 'SG_SG_R_MadisonRooms') {
			$madisonRoom = new WY_Partner('SG_SG_R_MadisonRooms');
			$madisonRoom->pushEventToPartner($this, 'NetSuite', 'A');
			//$madisonRoom->pushEventToPartner($this, 'Revel');
		}

		if(preg_match('/localhost|dev\./' , $_SERVER['HTTP_HOST']) || $this->restaurant == 'SG_SG_R_TheFunKitchen') {
			WY_Replication::replicate($confirmation, 'new_booking');
			}

	} catch (Exception $e) { $this->trace_error($e); }

	return $this->result = 1;
    }



    function cancelBooking($confirmation, $reason = "", $type = "", $bookeremail = "") {

        $restaurant = $this->restaurant;

        if (!isset($restaurant)) {
            return $this->error_msg(-1, "Invalid Restaurant");
        }

        date_default_timezone_set("Asia/Singapore");

        $cdate = date("Y-m-d");
        if ($type == 'backoffice' || $type == 'tms') {
            $date = strtotime("-2 day");
            $cdate = date("Y-m-d", $date);
            $loguserid = (isset($_SESSION['user_backoffice']['id'])) ? $_SESSION['user_backoffice']['id'] : "unknown";     
        }else{ 
            $loguserid = (isset($_SESSION['user']['user_id'])) ? $_SESSION['user']['user_id'] : "unknown";
            $type ='other';
        }
        
        if($type == 'tms')
        	$data = pdo_single_select("SELECT confirmation, tracking, booker, type, email from booking WHERE rdate >= '$cdate' and restaurant = '$restaurant' and confirmation = '$confirmation' limit 1");
        else $data = pdo_single_select("SELECT confirmation, tracking, booker, type, email from booking WHERE rdate >= '$cdate' and restaurant = '$restaurant' and confirmation = '$confirmation' and wheelwin = '' and status != 'cancel' limit 1");
        if (count($data) <= 0) {
            return $this->error_msg(-1, "Reservation date already passed or won some price.");
        }

		if(!empty($bookeremail) && preg_match("/\@weeloy.com$/", $bookeremail) && $data['email'] != $bookeremail) {
            	$this->msg = "Weeloy Staff can only modif his or her own emails " . $bookeremail . " -> " . $data['email'];
	        return $this->result = -1;
		}

		$now = (strstr($_SERVER['HTTP_HOST'], 'localhost:8888')) ? "NOW()" : "DATE_ADD(NOW(), INTERVAL 8 HOUR)";
        pdo_exec("update booking set status = 'cancel',reason='$reason', canceldate=$now where restaurant = '$restaurant' and confirmation = '$confirmation' limit 1");
        $this->status = 'cancel';
        
        $logger = new WY_log("website");
      
        $logger->LogEvent($loguserid, 703, $confirmation, $type, '', date("Y-m-d H:i:s"));
        
        if ($this->notifitycondition($data["tracking"], $data["booker"], $data["type"])) {
            $this->updateGuestBookingCount($this->restaurant, $this->email, 'cancel', 'noofcancel');
            //$this->notifyCancel();
        }

//        $notification = new WY_Notification();
//        $notification->notifyCancel($this);

        if ($this->restaurant == 'SG_SG_R_MadisonRooms') {
            $madisonRoom = new WY_Partner('SG_SG_R_MadisonRooms');
            $madisonRoom->pushEventToPartner($this, 'NetSuite', 'C');
        }

	   $prof = new WY_Profile();
	   $prof->decProfileRepeat($this);

        if(preg_match('/localhost|dev\./' , $_SERVER['HTTP_HOST']) || $this->restaurant == 'SG_SG_R_TheFunKitchen') {
            WY_Replication::replicate($confirmation, 'upd_booking');
        }
        return $this->result = 1;
    }

    function cancelPayPendingBooking($restaurant, $confirmation) {
        
        $logger = new WY_log("website");
        $loguserid = (isset($_SESSION['user']['user_id'])) ? $_SESSION['user']['user_id'] : "unknown";
        $logger->LogEvent($loguserid, 703, $confirmation, 'website', '', date("Y-m-d H:i:s"));
        $restaurant = $restaurant;

        if (!isset($restaurant)) {
            return $this->error_msg(-1, "Invalid Restaurant");
        }
		$now = (strstr($_SERVER['HTTP_HOST'], 'localhost:8888')) ? "NOW()" : "DATE_ADD(NOW(), INTERVAL 8 HOUR)";
        pdo_exec("update booking set status = 'cancel', canceldate=$now where restaurant = '$restaurant' and confirmation = '$confirmation' limit 1");
        $this->status = 'cancel';

        if(preg_match('/localhost|dev\./' , $_SERVER['HTTP_HOST']) || $this->restaurant == 'SG_SG_R_TheFunKitchen') {
            WY_Replication::replicate($confirmation, 'upd_booking');
        }        
        return $this->result = 1;
    }

    function noshowBooking($confirmation) {
        $notification = new WY_Notification();

        $restaurant = $this->restaurant;

        if (!isset($restaurant)) {
            return $this->error_msg(-1, "Invalid Restaurant");
        }

        date_default_timezone_set("Asia/Singapore");
        $cdate = date("Y-m-d");
        $data = pdo_single_select("SELECT confirmation,booking_deposit_id from booking WHERE restaurant = '$restaurant' and confirmation = '$confirmation' and wheelwin = '' limit 1");

        pdo_exec("update booking set status = 'noshow'  where restaurant = '$restaurant' and confirmation = '$confirmation' limit 1");
        $this->updateGuestBookingCount($this->restaurant, $this->email, 'noshow', 'noofnoshow');
        $this->status = 'noshow';
        $this->deposit_id = $data['booking_deposit_id'];

        $notification->notify($this, 'noshow');
        
        $logger = new WY_log("backoffice");
        $loguserid = (isset($_SESSION['user_backoffice']['id'])) ? $_SESSION['user_backoffice']['id'] : "unknown";

        $logger->LogEvent($loguserid, 704, $confirmation, 'backoffice', '', date("Y-m-d H:i:s"));

        if(preg_match('/localhost|dev\./' , $_SERVER['HTTP_HOST']) || $this->restaurant == 'SG_SG_R_TheFunKitchen') {
            WY_Replication::replicate($confirmation, 'upd_booking');
        }

        return 1;
    }

    function bookingValidate($restaurant, $confirmation) {

        if (!isset($confirmation)) {
            $this->msg = "Invalid confirmation";
            return $this->result = -1;
        }

        if ($this->getBooking($confirmation) == false) {
            $this->msg = "Unable to find confirmation $confirmation";
            return $this->result = -1;
        }

        if (!isset($this->restaurant) || $this->restaurant != $restaurant) {
            $this->msg = "Invalid Restaurant ";
            return $this->result = -1;
        }

        $this->addtoGeneric('{"more":"{’validate’: ’1’}"}');
        return $this->result = 1;
    }

    function bookingSummary($restaurant, $confirmation) {

        if (!isset($confirmation)) {
            $this->msg = "Invalid confirmation";
            return $this->result = -1;
        }

        if ($this->getBooking($confirmation) == false) {
            $this->msg = "Unable to find confirmation $confirmation";
            return $this->result = -1;
        }

        if (!isset($this->restaurant) || $this->restaurant != $restaurant) {
            $this->msg = "Invalid Restaurant ";
            return $this->result = -1;
        }

		$prof = new WY_Profile;
		$data = $prof->read1Prof($this->restaurant, $this->email, $this->mobile);
		$repeatguest = $data['repeatguest'];
		$dd = new DateTime($data['lastvisit']);
		$lastvisit = $dd->format('d-m-Y');
		$notes = $bkcode = "";
		if($this->generic != "" && strlen($this->generic) > 5) {
			$obj = preg_replace("/’/", "\"", $this->generic);
			$more = json_decode($obj, true);
			$notes = (!empty($more["notestext"])) ? $more["notestext"] : "";
			$bkcode = (!empty($more["notescode"])) ? $more["notescode"] : "";
        	}
		
		$margin = "      ";
        $summary = Array();
        $dd = new DateTime($this->rdate);
		$rdate = $dd->format('d-m-Y');

        //$summary[] = array("type" => "title", "data" => "SUMMARY", "font-size" => '2', 'text-align' => 'center');
        $summary[] = array("type" => "line", "data" => $margin . "Guest : " . $this->salutation . " " . $this->firstname . " " . $this->lastname, "font-size" => '2', 'text-align' => 'left');
        $summary[] = array("type" => "line", "data" => $margin . "Pax : " . $this->cover, "font-size" => '2', 'text-align' => 'left');
        $summary[] = array("type" => "line", "data" => $margin . "Date : " . $rdate, "font-size" => '2', 'text-align' => 'left');
        $summary[] = array("type" => "line", "data" => $margin . "Time : " . $this->rtime, "font-size" => '2', 'text-align' => 'left');
        $summary[] = array("type" => "line", "data" => $margin . "Table : " . $this->tablename, "font-size" => '2', 'text-align' => 'left');
        $summary[] = array("type" => "line", "data" => $margin . "Last visit : " . $lastvisit, "font-size" => '2', 'text-align' => 'left');
        $summary[] = array("type" => "line", "data" => $margin . "Repeat Guest : " . $repeatguest, "font-size" => '2', 'text-align' => 'left');
        $summary[] = array("type" => "seperator", "data" => $margin, "font-size" => '2', 'text-align' => 'left');
        $summary[] = array("type" => "line", "data" => $margin . "Request: " . $this->specialrequest, "font-size" => '2', 'text-align' => 'left');
        $summary[] = array("type" => "seperator", "data" => $margin, "font-size" => '2', 'text-align' => 'left');
        $summary[] = array("type" => "line", "data" => $margin . "Notes: " . $notes, "font-size" => '2', 'text-align' => 'left');
        $summary[] = array("type" => "line", "data" => $margin . "BKG Codes: " . $bkcode, "font-size" => '2', 'text-align' => 'left');
        $this->result = 1;
        return $summary;
    }

    function updateGuestBookingCount($restId, $memberId, $type, $label) {

        if (!isset($restId) || !isset($memberId)) {
            return $this->error_msg(-1, "Invalid Restaurant");
        }
        $dataExist = pdo_single_select("SELECT * from member_booking_stats WHERE restaurant_id ='$restId' and member_id='$memberId'");

        if (count($dataExist) > 0) {
            if ($type == 'booking') {
                $newCount = $dataExist['noofbooking'] + 1;
                $newTotal = $dataExist['total'] + 1;
            }
            if ($type == 'cancel') {
                $newCount = $dataExist['noofcancel'] + 1;
                $newTotal = $dataExist['total'] - 1;
            }
            if ($type == 'noshow') {
                $newTotal = $dataExist['total'] - 1;
                $newCount = $dataExist['noofnoshow'] + 1;
            }
            pdo_exec("update member_booking_stats set $label = '$newCount',total = '$newTotal' WHERE restaurant_id = '$restId' and member_id='$memberId' limit 1");
            return 1;
        } else {
            if ($type == 'booking') {
                $total = 1;
                $sql = "INSERT INTO member_booking_stats (member_id,restaurant_id,noofbooking,noofcancel,noofnoshow,total) VALUES ('$memberId','$restId',$total,0,0,$total)";
                $result = pdo_exec($sql);
                return 1;
            }
        }
    }

    public function setReviewStatus($email, $confirmation, $status) {

        pdo_exec("update booking set review_status = '$status' where email = '$email' and confirmation = '$confirmation' limit 1");
        $this->status = $status;
        return 1;
    }

    // update by the GUEST, check availability
    function UpdateBooking($confirmation, $cover, $rtime, $rdate, $specialrequest, $data) {

        if (!empty($data['firstname']))
            $firstname = $data['firstname'];
        if (!empty($data['lastname']))
            $lastname = $data['lastname'];

        $argv = array('confirmation', 'cover', 'rdate', 'rtime', 'specialrequest', 'firstname', 'lastname');
        for ($i = 0; $i < count($argv); $i++) {
            $tt = $argv[$i];
            if (!isset($$tt))
                continue;
            $org = $$tt;
            $$tt = $this->cleanpattern($$tt, "&nbsp;");
            $$tt = $this->clean_input($$tt);
        }

        if (empty($confirmation)) {
            return $this->error_msg(-1, "Invalid confirmation " . $confirmation);
        }

        if ($this->getBooking($confirmation) == false) {
            return $this->error_msg(-1, "Unable to find confirmation $confirmation");
        }

        if (empty($this->restaurant)) {
            return $this->error_msg(-1, "Invalid Restaurant");
        }

        date_default_timezone_set("Asia/Singapore");
		$now = (strstr($_SERVER['HTTP_HOST'], 'localhost:8888')) ? "NOW()" : "DATE_ADD(NOW(), INTERVAL 8 HOUR)";

        $cdateday = date("Y-m-d");

        $tracking = $this->tracking;
        $theRestaurant = $this->restaurant;
        $res = new WY_restaurant;
        $getRest = $res->getRestaurant($theRestaurant);
        if ($res->result < 0) {
             $this->msg = "Invalid restaurant";
            return $this->error_msg(-1, "Invalid restaurant " . $theRestaurant);
        }

 		$hasBeenCheck = "noavailcheck";
        $updateQue = pdo_single_select("select restaurant, lastname, firstname, product from booking where restaurant = '$theRestaurant' and confirmation = '$confirmation' limit 1");
        $product = (isset($updateQue['product'])) ? $updateQue['product'] : "";

        if(empty($tracking) || !preg_match("/walkin|tms|waiting|overwrite|remote/i", $tracking)) { // don't check if walkin and tms
            // check that rdate >= cdate
            $log = w_checkdate($cdateday, $rdate);
            if ($log < 0) { 
                $this->msg = "Invalid date $cdateday, $rdate";
                return $this->error_msg(-1, "Invalid date $cdateday, $rdate");
            }

            $emptybooking = '';
            $res->CheckAvailability($theRestaurant, $rdate, $rtime, $cover, $emptybooking, $product);
            if ($res->result <= 0) {
                $this->msg = "No more availability. Restaurant =  " . $theRestaurant . ", date = " . $rdate . ", time = " . $rtime . ", covers = " . $cover;
                return $this->error_msg(-1, $res->msg . "No more availability. Restaurant =  " . $theRestaurant . ", date = " . $rdate . ", time = " . $rtime . ", covers = " . $cover);
            }
			$hasBeenCheck = "availcheck" . $res->result;
        }

        pdo_insert("insert into booking_modif (restaurant, confirmation, rdate, rtime, cover, lastname, firstname, mobile, specialrequest, generic, theTs) select restaurant, confirmation, rdate, rtime, cover, lastname, firstname, mobile, specialrequest, generic, $now from booking where restaurant = '$theRestaurant' and confirmation = '$confirmation' limit 1");

        if (!isset($firstname))
            $firstname = $updateQue['firstname'];
        if (!isset($lastname))
            $lastname = $updateQue['lastname'];

        pdo_exec("update booking set cover='$cover', rtime='$rtime', firstname='$firstname',lastname='$lastname',rdate='$rdate',specialrequest='$specialrequest', flag = flag | 1, updatestatus='activate' where restaurant = '$theRestaurant' and confirmation = '$confirmation' limit 1");

        $logger = new WY_log("website");
        $loguserid = (isset($_SESSION['user']['user_id'])) ? $_SESSION['user']['user_id'] : "unknown";
        $logger->LogEvent($loguserid, 702, $confirmation, 'update', $hasBeenCheck, date("Y-m-d H:i:s"));
        
        // booking for madison Room
        if ($this->restaurant == 'SG_SG_R_MadisonRooms') {
            $madisonRoom = new WY_Partner('SG_SG_R_MadisonRooms');
            $madisonRoom->pushEventToPartner($this, 'NetSuite', 'U');
            //$madisonRoom->pushEventToPartner($this, 'Revel');
        }

        if(preg_match('/localhost|dev\./' , $_SERVER['HTTP_HOST']) || $this->restaurant == 'SG_SG_R_TheFunKitchen') {
            WY_Replication::replicate($confirmation, 'upd_booking');
        }

        return $this->result = 1;
    }

    function updateBookingStatus($paymentKey, $booking, $status) {
        date_default_timezone_set("Asia/Singapore");
        $now = (strstr($_SERVER['HTTP_HOST'], 'localhost:8888')) ? "NOW()" : "DATE_ADD(NOW(), INTERVAL 8 HOUR)";
        $fields = "status = '$status',booking_deposit_id = '$paymentKey'";
        if ($status === 'pending_payment') {
            $fields = "status = '$status',booking_deposit_id = '$paymentKey',cdate = $now ";
        }
        $sql = "UPDATE booking SET $fields WHERE confirmation LIKE '$booking'";

        return pdo_exec($sql);
    }

    // modification backoffice, no checking, OBSELETE us MODIFULLBOOKING for TMS / BACKOFFICE
    function modifBooking($confirmation, $email, $cover, $rtime, $rdate, $specialrequest) {

        $argv = array('confirmation', 'email', 'cover', 'rtime', 'rdate', 'specialrequest');
        for ($i = 0; $i < count($argv); $i++) {
            $tt = $argv[$i];
            if (!isset($$tt))
                continue;
            $org = $$tt;
            $$tt = $this->clean_input($$tt);
        }

        if ($this->email_validation($email) < 0) {
            return $this->error_msg(-1, "Invalid email " . $email);
        }

        if (empty($confirmation)) {
            return $this->error_msg(-1, "Invalid confirmation " . $confirmation);
        }

        if ($this->getBooking($confirmation) == false) {
            return $this->error_msg(-1, "Unable to find confirmation $confirmation");
        }

        if (empty($this->restaurant)) {
            return $this->error_msg(-1, "Invalid Restaurant");
        }

        date_default_timezone_set("Asia/Singapore");
		$now = (strstr($_SERVER['HTTP_HOST'], 'localhost:8888')) ? "NOW()" : "DATE_ADD(NOW(), INTERVAL 8 HOUR)";

        $restaurant = $this->restaurant;
        $updrequest = ($specialrequest != "-") ? ", specialrequest='$specialrequest' " : "";
        pdo_insert("insert into booking_modif (restaurant, confirmation, rdate, rtime, cover, lastname, firstname, mobile, specialrequest, generic, theTs) select restaurant, confirmation, rdate, rtime, cover, lastname, firstname, mobile, specialrequest, generic, $now from booking where restaurant = '$restaurant' and confirmation = '$confirmation' limit 1");
        pdo_exec("update booking set updatestatus='activate', flag = flag | 1, cover='$cover', rtime='$rtime', rdate='$rdate' $updrequest where restaurant = '$restaurant' and confirmation = '$confirmation' limit 1");
        
        $logger = new WY_log("backoffice");
        $loguserid = (isset($_SESSION['user_backoffice']['id'])) ? $_SESSION['user_backoffice']['id'] : "unknown";
        $logger->LogEvent($loguserid, 702, $confirmation, 'backoffice', 'modifbook', date("Y-m-d H:i:s"));
        return 1;
    }

    function modifullBooking($confirmation, $json, $notiflag = true, $bookeremail = "") {

        if (!isset($confirmation)) {
            $this->msg = "Invalid confirmation";
            return $this->result = -1;
        }

        if ($this->getBooking($confirmation) == false) {
            $this->msg = "Unable to find confirmation $confirmation";
            return $this->result = -1;
        }

        if (!isset($this->restaurant)) {
            $this->msg = "Invalid Restaurant";
            return $this->result = -1;
        }

		if(!empty($bookeremail) && preg_match("/\@weeloy.com$/", $bookeremail) && $this->email != $bookeremail) {
            	$this->msg = "Weeloy Staff can only modif his or her own emails " . $bookeremail . " -> " . $this->email;
	        return $this->result = -1;
		}
		
        $restaurant = $this->restaurant;
        $mapping = array("first" => "firstname", "last" => "lastname", "email" => "email", "phone" => "mobile", "date" => "rdate", "time" => "rtime", "pers" => "cover", "comment" => "specialrequest", "more" => "generic");
        $obj = json_decode($json, true);

        $cmd = array();
        foreach ($mapping as $key => $val)
            if (isset($obj[$key])) {
                $data = ($key != "more") ? $this->clean_input($obj[$key]) : $this->bkgetMore($obj[$key], $this->generic);
                $cmd[] = "$val='$data'";
            }
        if (count($cmd) < 1) {
            $this->msg = "Nothing to update " . $json;
            return $this->result = -1;
        }
        $cmd[] = "updatestatus='activate'";
        $cmd[] = "flag = flag | 1";
        $cmdstr = implode(", ", $cmd);

        date_default_timezone_set("Asia/Singapore");
		$now = (strstr($_SERVER['HTTP_HOST'], 'localhost:8888')) ? "NOW()" : "DATE_ADD(NOW(), INTERVAL 8 HOUR)";

        pdo_insert("insert into booking_modif (restaurant, confirmation, rdate, rtime, cover, lastname, firstname, mobile, specialrequest, generic, theTs) select restaurant, confirmation, rdate, rtime, cover, lastname, firstname, mobile, specialrequest, generic, $now from booking where restaurant = '$restaurant' and confirmation = '$confirmation' limit 1");
        pdo_exec("update booking set $cmdstr where restaurant = '$restaurant' and confirmation = '$confirmation' limit 1");

        if ($this->restaurant == "SG_SG_R_MadisonRooms") {
            if ($this->getBooking($confirmation)) {
                $madisonRoom = new WY_Partner('SG_SG_R_MadisonRooms');
                $madisonRoom->pushEventToPartner($this, 'NetSuite', 'U');
            }
        }
        $logger = new WY_log("backoffice");
        $loguserid = (isset($_SESSION['user_backoffice']['id'])) ? $_SESSION['user_backoffice']['id'] : "unknown";
        $logger->LogEvent($loguserid, 702, $confirmation, 'backoffice', '', date("Y-m-d H:i:s"));
        //modification for payemnt booking
        
//        
//        $isCCRequired = false;
//    
//        $rest = new WY_restaurant();
//        $exp = array();

        
        // get update booking details
        $this->getBooking($confirmation);
//        $depositFlg = WY_restaurant::s_checkbkdeposit($this->restaurant);
//        $track_arra = explode("|", $this->tracking);
//        $exp = array_merge($exp, $track_arra);
//        $isTms = in_array('tms',$exp);
//  
//    
//        if (WY_restaurant::s_checkbkdeposit($this->restaurant) > 0) {
//            $res_policy = new WY_Restaurant_policy;
//            if(empty($this->deposit_id)){
//                $requiredccDetails = $res_policy->isRequiredccDetails($restaurant,$this->rtime,$this->cover);
//                $isWaived = $this->chkWaived($this->generic);
//                if($requiredccDetails && $isWaived == 0 ){
//                    $isCCRequired = true;
//                }
//            }
//        }
//        if($isTms){
//            $isCCRequired = false;
//        }
//
//        if($isCCRequired || $this->status === 'pending_payment' || $this->status === 'expired' ){
//            pdo_exec("update booking set status = 'pending_payment' where restaurant = '$restaurant' and confirmation = '$confirmation' limit 1");
//            $this->status ='pending_payment';
//            $notification = new WY_Notification();
//            $notification->notify($this, 'booking_pending'); 
//        }else{
//            $notification = new WY_Notification();
//            $notification->notify($this, 'update');
//        }

	if($notiflag) {
        	$notification = new WY_Notification();
        	$notification->notify($this, 'update');
        	}

        if(preg_match('/localhost|dev\./' , $_SERVER['HTTP_HOST']) || $this->restaurant == 'SG_SG_R_TheFunKitchen') {
            WY_Replication::replicate($confirmation, 'upd_booking');
        }
        
        return $this->result = 1;
    }

    function addtoGeneric($json) {
        $obj = json_decode($json, true);
        if (!empty($obj["more"])) {
            $val = $this->bkgetMore($obj["more"], $this->generic);
            if ($val != $this->generic) {
                pdo_exec("update booking set generic = '$val' where restaurant = '$this->restaurant' and confirmation = '$this->confirmation' limit 1");
                $this->generic = $val;
            }
        }
    }

    function bkgetMore($obj, $org) {

        $mapping = array("event", "preference", "birthday", "seated", "captain", "cleartime", "left", "duration", "repeat", "remotebooking", "notifysmswait", "notestext", "notescode", "area", "purpose", "choice", "validate", "statepos", "chck_form_status", "ccwaived", "orders");
        $org = preg_replace("/’/", "\"", $org);
        $obj = preg_replace("/’/", "\"", $obj);
        $oldAr = json_decode($org, true);
        $newAr = json_decode($obj, true);

        foreach ($mapping as $key)
            if (isset($newAr[$key])) {
                $oldAr[$key] = $this->clean_input($newAr[$key]);
                if ($key == "left" && !empty($oldAr["seated"])) {
                    $seated = new DateTime("2010-01-01 " . $oldAr["seated"]);
                    $left = new DateTime("2010-01-01 " . $oldAr["left"]);
                    $interval = $seated->diff($left);
                    $oldAr["duration"] = strval(intval($interval->h) * 60 + intval($interval->i));
                }
            }

        $new = json_encode($oldAr, true);
        $new = preg_replace("/\'|\"/", "’", $new);
        return $new;
    }

    function incsmsBooking($confirmation) {

        if ($this->confirmation !== $confirmation)
            return;

        if (empty($this->generic) || preg_match("/notifysmswait/", $this->generic) == false) {
            return $this->addtoGeneric('{"more":"{’notifysmswait’:’1’}"}');
        }

        $val = preg_replace_callback("/’notifysmswait’:’(\d+)’/", function ($matches) {
            return "’notifysmswait’:’" . (intval($matches[1]) + 1) . "’";
        }, $this->generic);
        pdo_exec("update booking set generic = '$val' where restaurant = '$this->restaurant' and confirmation = '$this->confirmation' limit 1");
    }

    function ProcessExternalBooking($data, $restoMapping, $booker, $bkstatus = NULL) {

        if (preg_match("/CHOPE|HGW|QUANDOO/i", $booker) == false) {
            $this->msg = "unauthorized booker " . $booker;
            return $this->result = -1;
        }

        foreach ($data as $key => $value) {
            $data[$key] = $this->clean_input($value);
        }

        foreach ($restoMapping as $key => $value) {
            if (preg_match("/" . $key . "/", $data["restaurant"])) {
                $bkrestaurant = $value;
                break;
            }
        }

        if (empty($bkrestaurant)) {
            $this->msg = "Unable to parse the restaurant";
            return $this->result = -2;
        }

//        $unixtime = strtotime($data["rdate"] . " " . $data["rtime"]);
//        $bkdate = date("Y-m-d", $unixtime);
//        $bktime = date("H:i", $unixtime);

        $bkdate = $data["rdate"];
        $bktime = $data["rtime"];
        $remotebooking = $data["confirmation"];
        $bkemail = $data["email"];
        $bkmobile = $data["mobile"];
        $bkcover = $data["cover"];
        $bksalutation = $data["salutation"];
        $bkfirst = $data["first"];
        $bklast = $data["last"];
        
        $bkspecialrequest = $data["specialrequest"];
        $bktracking = "remote";
        $bkcountry = "Singapore";
        $language = "en";
        $company = $hotelguest = $bkstate = $optin = "";
        $booking_deposit_id = $status = $extra = $product = "";

        if($bkstatus == 'pending_payment'){
            $status = $bkstatus;
        }
 
       
        switch ($data["processing"]) {
            case "booking":
            	// is it already there ? QUANDOO
            	if($booker == "QUANDOO" && strlen($remotebooking) > 4) {
					$this->getRemoteBooking($bkrestaurant, $booker, $remotebooking, $bkmobile, $bkemail);
					if ($this->result > 0){
                            // we found a duplicate so it s an error
                            return $this->result = -1;
                        }
        			} 
        			
        		$this->result = 1;	         
               	$status = $this->createBooking($bkrestaurant, $bkdate, $bktime, $bkemail, $bkmobile, $bkcover, $bksalutation, $bkfirst, $bklast, $bkcountry, $language, $bkspecialrequest, "thirdparty", $bktracking, $booker, $company, $hotelguest, $bkstate, $optin, $booking_deposit_id, $status, $extra, $product);
                if ($this->result < 0)
                    return $this->result;
                $this->addtoGeneric('{"more":"{’remotebooking’: ’' . $remotebooking . '’}"}');
                return $this->result;
                break;

            case "modification":
                $this->getRemoteBooking($bkrestaurant, $booker, $remotebooking, $bkmobile, $bkemail);
                if ($this->result < 0) {
                	$status = $this->createBooking($bkrestaurant, $bkdate, $bktime, $bkemail, $bkmobile, $bkcover, $bksalutation, $bkfirst, $bklast, $bkcountry, $language, $bkspecialrequest, "thirdparty", $bktracking, $booker, $company, $hotelguest, $bkstate, $optin, $booking_deposit_id, "modified", $extra, $product);
					return $this->result = 1;
                    }
                if ($this->rdate != $kbrdate || $this->rtime != $bktime || $this->cover != $bkcover || $this->speciarequest != $bkspecialrequest || $this->firstname != $bkfirst || $this->lastname != $bklast)
                    $this->modifullBooking($this->confirmation, '{"first":"' . $bkfirst . '","last":"' . $bklast . '","date":"' . $bkdate . '","time":"' . $bktime . '","pers":"' . $bkcover . '","comment":"' . $bkspecialrequest . '"}');
                return $this->result;
                break;

            case "cancellation":
                $this->getRemoteBooking($bkrestaurant, $booker, $remotebooking, $bkmobile, $bkemail);
                if ($this->result < 0) {
                	$status = $this->createBooking($bkrestaurant, $bkdate, $bktime, $bkemail, $bkmobile, $bkcover, $bksalutation, $bkfirst, $bklast, $bkcountry, $language, $bkspecialrequest, "thirdparty", $bktracking, $booker, $company, $hotelguest, $bkstate, $optin, $booking_deposit_id, "cancel", $extra, $product);
					return $this->result = 1;
                    }                
                if($this->status != 'cancel') // for CHOPE which tried to cancel booking already cancelled
                	$this->cancelBooking($this->confirmation, "", "", "");
                return $this->result;
                break;
        }

        $this->msg = "invalid option";
        return $this->result = -1;
    }

    function getActiveupdate() {
        $status = 'active';
        $data = pdo_single_select("SELECT confirmation,email from booking WHERE updatestatus = '$status' and wheelwin = '' limit 1");
        return $data;
    }

    function setDeactiveupdate($confirmation) {
        $status = 'inactive';
        pdo_exec("update booking set updatestatus='$status' where confirmation = '$confirmation' and wheelwin = '' limit 1");
    }

    function submitBookingWin($confirmation, $restCode, $membCode, $spinwin) {

        if ($this->getBooking($confirmation) == false)
            return $verify = "wrong_confirmation " . $confirmation;

        if ($this->wheelwin != "")
            return $verify = "wrong_spin";

        if ($restCode != $this->restCode || $membCode != $this->membCode)
            return $verify = "wrong_code";

        if ($spinwin == "")
            return $verify = "wrong_value";

        $resto = new WY_restaurant();
        $resto->getRestaurant($this->restaurant);

        //if (!preg_match("/\|$spinwin\|/", $resto->wheel))
        //    return $verify = "wrong_choice";
        // if (strpos($resto->wheel,$spinwin) === false) {
        //      return $verify = "wrong_choice";
        //  }


        $this->storeBookingWin($spinwin, $wheeldesc);
        return "ok";
    }

    function submitBookingCompleteWin($confirmation, $restCode, $membCode, $spinsegment, $spinwin, $spindesc, $spinsource, $wheelversion = NULL, $angle = NULL) {

        if ($this->getBooking($confirmation) == false)
            return $verify = "wrong_confirmation " . $confirmation;

        if ($this->wheelwin != "")
            return $verify = "wrong_spin";

        if ($restCode != $this->restCode || $membCode != $this->membCode)
            return $verify = "wrong_code";

        if ($spinwin == "")
            return $verify = "wrong_value";

        $resto = new WY_restaurant;
        $resto->getRestaurant($this->restaurant);

        //if (!preg_match("/\|$spinwin\|/", $resto->wheel))
        //    return $verify = "wrong_choice";
        //if (strpos($resto->wheel, $spinwin) === false) {
        //  return $verify = "wrong_choice";
        //}

        $this->storeBookingCompleteWin($spinsegment, $spinwin, $spindesc, $spinsource, $wheelversion, $angle);
        return "ok";
    }

    function verifyBookingWin($confirmation, $restCode, $membCode) {

        if ($this->getBooking($confirmation) == false)
            return $verify = "wrong_confirmation" . $confirmation;

        if ($this->wheelwin != "")
            return $verify = "wrong_spin";

        if ($restCode != $this->restCode || $membCode != $this->membCode)
            return $verify = "wrong_code";

        //CHECK COSTUMER/RESTAURANT-CODE HERE
        return $verify = "ok";
    }

    function storeBookingWin($wheelwin, $wheeldesc) {
        $notification = new WY_Notification();

        $restaurant = $this->restaurant;
        $confirmation = $this->confirmation;
        $this->wheelwin = $wheelwin;
        $this->wheeldesc = $wheeldesc;
        pdo_exec("update booking set wheelwin = '$wheelwin' where restaurant = '$restaurant' and confirmation = '$confirmation' limit 1");
        

        $notification->notify($this, 'wheel_win');
    }

    function storeBookingCompleteWin($wheelsegment, $wheelwin, $wheeldesc, $spinsource, $wheelversion = NULL, $angle = NULL) {
        $notification = new WY_Notification();

        $restaurant = $this->restaurant;
        $confirmation = $this->confirmation;
        $this->wheelwin = $wheelwin;
        $this->wheeldesc = $wheeldesc;

        pdo_exec("update booking set wheelversion = '$wheelversion', wheelsegment = '$wheelsegment', wheelwinangle = '$angle', wheelwin = '$wheelwin', wheeldesc = '$wheeldesc', spinsource = '$spinsource' where restaurant = '$restaurant' and confirmation = '$confirmation' limit 1");

        $notification->notify($this, 'wheel_win');

        $loguserid = (isset($_SESSION['user']['user_id'])) ? $_SESSION['user']['user_id'] : "unknown";

        $logger = new WY_log("backoffice");
        $logger->LogEvent($loguserid, 713, $confirmation, 'website', '', date("Y-m-d H:i:s"));
    }

    function updatemembercode($email, $membcode) {
        pdo_exec("update booking set membCode = '$membcode' where email = '$email' ");
        return 1;
    }

    function emailContent() {
        return $emailEle = array(
            'btn_gcalender' => 'Add to google calendar',
            'footermsg_bookingwallet' => 'Add your booking to your wallet',
            'addtional_info' => 'Additional Info',
            'food_selfie_message2' => 'Take a Food-Selfie photo when you dine, submit your entry for the contest & Win!',
            'title_reservation' => 'Dining Reservation',
            'title_dining' => 'Dining Reservation',
            'email_content' => 'Thank you for choosing',
            'booking_message' => 'Your reservation is confirmed',
            'booking_details' => 'Booking Details',
            'data_time' => 'Date & Time',
            'guests' => 'Guests',
            'confirmation_no' => 'Confirmation No',
            'weeloy_spin_code' => 'Weeloy Spin Code',
            'restaurant_name' => 'Restaurant Name',
            'address' => 'Address',
            'footer_content' => 'Book on the Go, Anytime, Anywhere',
            'app_message' => 'Try out our app via App Store or Google play below',
            'more_info' => 'more info',
            'request_no' => 'Request No',
            'request_content1' => 'Please note that',
            'request_details' => 'Request Details',
            'request_content2' => 'is on First come, First Serve basis',
            'title_request' => 'Dining Request',
            'title_spinwheel' => 'Congratulations',
            'title_spinwheel2' => 'You won an Offer',
            'wheelwin_content1' => 'You won',
            'wheelwin_content2' => 'by spinning the Weeloy Wheel',
            'terms_conditions' => 'Terms & Conditions',
            'phone no' => 'Phone no',
            'food_selfie_message1' => 'Your Join the Weeloy Food-Selfie Contest & Win a 3D2N holiday to Phuket for 2.',
            'btn_sharebooking' => 'Share your booking',
            'btn_invite' => 'Invite your friends',
            'btn_cancel' => 'Cancel booking',
            'btn_update' => 'Update/Cancel Booking',
            'footermsg_anytime' => 'Your booking with you, Anytime',
            'title_dining' => 'Dining Reservation',
            'title_cancel' => 'Cancellation',
            'cancel_content1' => 'Your reservation with',
            'cancel_content2' => 'has been cancelled',
            'reminder' => 'Reminder',
            'reminder_content' => 'Just a gentle reminder of your upcoming reservation with',
            'title_review' => 'Thank you for your review',
            'review_details' => 'Review Details',
            'review_rateall' => 'Overall',
            'review_ratefood' => 'Food',
            'review_rateambiance' => 'Ambiance',
            'review_rateservice' => 'Service',
            'review_rateprice' => 'Price',
            'comment' => 'Comment',
            'btn_writereview' => 'write a review',
            'review_reminder_content' => 'Thank you for your recent dining reservation with',
            'review_reminder_message' => 'Your feedback is important to us. As we want to improve your satisfaction everyday,  we would like to know how you enjoyed your dining experience at',
            'booking_message_need_reconfirm' => 'Your table has been blocked, we are sending you an e-mail to reconfirm the booking'
        );
    }

    function getEmailContent() {
        $trad = new WY_Translation;
        //testing added staic data
        $trad->readcontent('EMAIL', $this->language);
        $content = $trad->content;
        $data = array('row' => $content);
        $trans = $data['row'];
        $length = count($trans);
        $emailEle = $this->emailContent();
        foreach ($emailEle as $key => $ele)
//            $lbl = strtolower($emailEle[$key]);
            if (isset($emailEle[$key]))
                for ($i = 0; $i <= count($trans); $i++)
                    if (isset($trans[$i]['content']) && strtolower($trans[$i]['content']) == strtolower($emailEle[$key]) && $trans[$i]['translated'] != '') {
                        $cc = $trans[$i]['translated'];
                        $emailEle[$key] = $cc;
                    }
        return $emailEle;
    }
    function getProfileBooking($restaurant, $email, $mobile, $extraemail, $extramobile, $account="") {
        $query = "restaurant = '$restaurant'";
        if (!empty($account)) {
		$resdata = new WY_restaurant();
		$data = $resdata->getListAccountRestaurant($account);
		if($data > 1) {
			$resto = array();
			foreach ($data as $row)
				$resto[] = "'" . $row['restaurant'] . "'";
			$restolist = implode(",", $resto);
			$query = " restaurant in ($restolist) ";
			$restaurant = $resto[0];
			}
		}
		
		$rule = array("email" => "no.*email.com", "mobile" => "999999");
		foreach($rule as $label => $pat) {
			$tt = $$label;
			if($tt != "" && preg_match("/$pat/", $tt) == false) $profqery[] = "$label = '$tt'";
			$tt = ${"extra" . $label};
			if($tt != "" && preg_match("/$pat/", $tt) == false) $profqery[] = "$label = '$tt'";
			}
		$cn = count($profqery);
		if($cn > 1) $query .= " and (" . implode(" or ", $profqery) . ")";
		else if($cn > 0) $query .= " and " . $profqery[0];
		else {
			$this->result = -1;
			return array();
			}
		//error_log('GETPROFILE ' . "$restaurant, $email, $mobile, $extraemail, $extramobile, $query");

		$fields = "restaurant as resto, confirmation as booking, rdate as date, rtime as time, cover as pax, status as bkstatus, cdate as createdate, tablename, generic";
        $data = pdo_multiple_select("SELECT $fields FROM booking WHERE $query limit 50 union SELECT $fields FROM booking_15_16 WHERE $query limit 50");	// order by rdate DESC, rtime DESC
        $this->msg = "";
		$this->result = (count($data) > 0) ? 1 : -1;
		return $data;
		}
		
    function getVisit($restaurant, $mode, $options = "") {
        $resultat = array();
        $this->result = 1;
        $this->msg = "";

        $restaurant = $this->clean_input($restaurant);
        if (empty($restaurant)) {
            $this->result = -1;
            $this->msg = "invalid restaurant";
            return array();
        }

        $qeryresto = " restaurant = '$restaurant' ";

        $res = new WY_restaurant;
        $res_policy = new WY_Restaurant_policy;

        $resto = array();
        
        if ($mode == "email") {
            $email = $restaurant;
            $data = $res->getListAccountRestaurant($email);
            $cn = count($data);
            if ($cn <= 0) {
                $this->msg = "invalid account $email";
                return $this->result = -1;
            }

            if ($cn > 0 && $cn < 15) {
                foreach ($data as $row)
                    $resto[] = "'" . $row['restaurant'] . "'";
                $restolist = implode(",", $resto);
                $qeryresto = " restaurant in ($restolist) ";
                $restaurant = $resto[0];
            	}
        }
        else $resto[] = $restaurant;
        
        switch($mode) {
        	case "bydate":
        		$qry = "rdate > DATE_SUB( NOW(), INTERVAL 3 DAY )";
        		break;
        		
        	case "today":
        		$qry = "rdate > DATE_SUB( NOW(), INTERVAL 1 DAY )";
        		break;
        		
        	case "adate":
        		$qry = "rdate >= '" . Date('Y-m-d') . "'";
        		if($options != "" && strpos($options, "-") !== false) {
        			$tmp = explode("-", $options);
        			if(count($tmp) == 3 && (checkdate($tmp[1],$tmp[2],$tmp[0])))	// month,day,year
        				$qry = "rdate = '" . $options . "'";
        		}
                //error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": qry: ".$qry);
        		break;
        		
        	default:
 		        $qry = "rdate > DATE_SUB( NOW(), INTERVAL 3 MONTH) ";
 		        break;
 		   }
       		
        $qry .= " and rdate != '00-00-0000' and type != 'event' ";

		$fields = "restaurant as resto, confirmation as booking, booking_deposit_id, email, mobile as phone, salutation,lastname as last, firstname as first, rdate as date, rtime as time, cover as pers, specialrequest as comment, generic as more, status as bkstatus, cdate as createdate, state, wheelwin, restCode, membCode, tracking, booker, browser, canceldate, type, company, hotelguest,product,tablename, restable, optin, ip, flag, (flag & 1) as modified";
        $data = pdo_multiple_select("SELECT $fields FROM booking WHERE $qeryresto and $qry order by rdate DESC, rtime DESC limit 3000");
        return $data;
    }
    function getChargeDetails($confirmation){
        if(empty($confirmation))
            return;
        
        $chargeAr = array();
        $this->getbooking($confirmation);
        $payment = new WY_Payment(); 
        $res_policy = new WY_Restaurant_policy;

     
        
        if(empty($this->deposit_id))
             return $chargeAr;
        
        $deposittDetails = $payment->getDepositDetails($this->deposit_id,$confirmation);
        
        if($this->restaurant == 'SG_SG_R_TheFunKitchen' || $this->restaurant == 'SG_SG_R_Bacchanalia' ){
            $paymentDetails = $res_policy->getResChargeDetails($this->deposit_id,$confirmation); 
        }
        // else if($this->restaurant == 'SG_SG_R_TheOneKitchen'  ){
        //     $paymentDetails = $payment->getBookingDepDetails($confirmation);
        // }
        else{
          
           $paymentDetails = $this->getDepositDetailsByID($this->deposit_id, $this->rdate, $this->rtime, $this->restaurant, $deposittDetails['payment_method'], $confirmation); 
            
        }

        $currency =  ($paymentDetails['currency']) ? $paymentDetails['currency'] : 'SGD';
        
        if (count($paymentDetails) > 0) {
            $chargeAr = array(
               'isShowDetails' => $paymentDetails['isPayment'],
               'payment_method' => $paymentDetails['payment_method'],
               'deposit'        => 'paid',
               'refund'         => 1,
               'depstatus'     => $paymentDetails['status'],
               'amount'       => $paymentDetails['amount'],
               'refund_amount' => $paymentDetails['refund_amount'],
               'currency'      => $currency
            );
            
        }
        return $chargeAr;
        
    }

    function getmodifVisit($restaurant, $mode) {

        $this->result = 1;
        $this->msg = "";

        $restaurant = $this->clean_input($restaurant);
        if (empty($restaurant)) {
            $this->result = -1;
            $this->msg = "invalid restaurant";
            return array();
        }

        $qeryresto = " restaurant = '$restaurant' ";

        $res = new WY_restaurant;

        if ($mode == "email") {
            $email = $restaurant;
            $data = $res->getListAccountRestaurant($email);
            $cn = count($data);
            if ($cn <= 0) {
                $this->msg = "invalid account $email";
                return $this->result = -1;
            }

            if ($cn > 0 && $cn < 15) {
                foreach ($data as $row)
                    $resto[] = "'" . $row['restaurant'] . "'";
                $restolist = implode(",", $resto);
                $qeryresto = " restaurant in ($restolist) ";
                $restaurant = $resto[0];
            }
        }

        $qry = ($mode !== "bydate") ? "rdate > DATE_SUB( NOW(), INTERVAL 3 MONTH) " : "rdate > DATE_SUB( NOW(), INTERVAL 3 DAY )";
        $qry .= " and rdate != '00-00-0000' ";
        //return pdo_multiple_select("SELECT confirmation as booking, rdate, rtime, cover, lastname, firstname, mobile, specialrequest, generic, theTs FROM (select * from booking_modif WHERE $qeryresto and $qry ORDER BY theTs DESC) as dd group by confirmation order by rdate DESC, rtime DESC limit 1500");
        return pdo_multiple_select("SELECT confirmation as booking, rdate, rtime, cover, theTs FROM (select * from booking_modif WHERE $qeryresto and $qry ORDER BY theTs DESC) as dd group by confirmation order by rdate DESC, rtime DESC limit 1500");
    }

    function get1booking($confirmation) {

        $this->result = 1;
        $this->msg = "";

        $confirmation = $this->clean_input($confirmation);
        if (empty($confirmation)) {
            $this->result = -1;
            $this->msg = "invalid confirmation";
            return array();
        }

        $year = intval(Date('Y')) - 1;
        return pdo_single_select("SELECT restaurant as resto, confirmation as booking, email, mobile as phone, salutation, lastname as last, firstname as first, rdate as date, rtime as time, cover as pers, specialrequest as comment, generic as more, status as bkstatus, cdate as createdate, state, wheelwin, restCode, membCode, tracking, booker, browser, canceldate, type, company, hotelguest, tablename, optin, ip FROM booking WHERE confirmation='$confirmation' limit 1");
    }

    function getmemberbooking($email) {

        $this->result = 1;
        $this->msg = "";

        $email = clean_input($email);

        $email = $this->clean_input($email);

        if (empty($email) || $this->email_validation($email) < 0) {
            $this->result = -1;
            $this->msg = "invalid email";
            return array();
        }

        $year = intval(Date('Y')) - 1;
        return pdo_single_select("SELECT restaurant as resto, confirmation as booking, email, mobile as phone, salutation, lastname as last, firstname as first, rdate as date, rtime as time, cover as pers, specialrequest as comment, generic as more, status as bkstatus, cdate as createdate, state, wheelwin, restCode, membCode, tracking, booker, browser, canceldate, type, company, hotelguest, tablename, optin, ip FROM booking WHERE email='$email' order by cdate limit 100");
    }

    function getBooking($conf) {
      

        $restaurant = new WY_restaurant();
        $this->result = -1;
        $this->msg = "";

        $data = pdo_single_select("SELECT booking.id ,booking.restaurant as brestaurant, confirmation, booking.pushnotify_token,language, booking.status as bstatus,booking.booking_deposit_id,restCode, membCode, salutation, lastname, firstname, booking.mobile, booking.email as member_email, booking.flag as flag, smsid, rdate, rtime, cdate, cover, product, company, hotelguest, specialrequest, generic, booking.state as bkstate, canceldate, title, tel, wheelwin, tracking, booker, booking.partner_booking_id, booking.partner_id, ip, tablename, restable, restaurant.email as res_email, address, city, zip, logo, booking.country as res_country, map, GPS, restaurant_tnc, restaurant.status as res_status, restaurant.dfminpers, restaurant.dfmaxpers, is_wheelable, is_bookable, aws_email_verification,type,revenue,browser,updatestatus,optin from booking, restaurant WHERE restaurant.restaurant = booking.restaurant and confirmation = '$conf' LIMIT 1");

        if (count($data) <= 0) {
            $this->result = -1;
            $this->msg = "unknown booking " . $conf;
            return false;
        }
        $this->bookid = $data['id'];
        ;
        $this->restaurant = $data['brestaurant'];
        $this->confirmation = $data['confirmation'];
        $this->status = $data['bstatus'];
        $this->restCode = $data['restCode'];
        $this->membCode = $data['membCode'];
        $this->salutation = $data['salutation'];
        $this->firstname = $data['firstname'];
        $this->lastname = $data['lastname'];
        $this->mobile = $data['mobile'];
        $this->email = $data['member_email'];
        $this->rdate = $data['rdate'];
        $this->cdate = $data['cdate'];
        $this->rtime = $data['rtime'];
        $this->pushToken = $data['pushnotify_token'];
        if ($this->rtime != "")
            $this->rtime = substr($this->rtime, 0, strlen($this->rtime) - 3);
        $this->cover = $data['cover'];
        $this->generic = $data['generic'];
        $this->specialrequest = $data['specialrequest'];
        $this->company = $data['company'];
        $this->product = $data['product'];
        $this->state = $data['bkstate'];
        $this->flag = $data['flag'];
        $this->canceldate = $data['canceldate'];
        $this->tracking = $data['tracking'];
        $this->booker = $data['booker'];
        $this->partner_booking_id = $data['partner_booking_id'];
        $this->partner_id = $data['partner_id'];
        $this->ip = $data['ip'];
        $this->tablename = $data['tablename'];
        $this->restable = $data['restable'];
        $this->wheelwin = $data['wheelwin'];
        $this->hotelguest = $data['hotelguest'];
        $this->language = $data['language'];
        $this->deposit_id = $data['booking_deposit_id'];
        
        $this->type = $data['type'];
        $this->revenue = $data['revenue'];
        $this->browser = $data['browser'];
        $this->updatestatus = $data['updatestatus'];
        $this->optin = $data['optin'];


        $this->restaurantinfo = new WY_restaurantConf;
        $this->restaurantinfo->title = $data['title'];
        $this->restaurantinfo->tel = $data['tel'];
        $this->restaurantinfo->email = $data['res_email'];
        $this->restaurantinfo->smsid = $data['smsid'];
        //$this->restaurantinfo->URLrestaurant = $data['url'];
        $this->restaurantinfo->address = $data['address'];
        $this->restaurantinfo->city = $data['city'];
        $this->restaurantinfo->zip = $data['zip'];
        $this->restaurantinfo->country = $data['res_country'];
        $this->restaurantinfo->logo = $data['logo'];
        $this->restaurantinfo->GPS = $data['GPS'];
        $this->restaurantinfo->map = $data['map'];
        $this->restaurantinfo->restaurant_tnc = $data['restaurant_tnc'];
        $this->restaurantinfo->dfmaxpers = $data['dfmaxpers'];
        $this->restaurantinfo->dfminpers = $data['dfminpers'];

        $this->restaurantinfo->status = $data['res_status'];
        $this->restaurantinfo->is_bookable = $data['is_bookable'];
        $this->restaurantinfo->is_wheelable = $data['is_wheelable'];

        $this->booking_type = $this->getBookingType($this);

        $this->restaurantinfo->internal_path = $restaurant->getRestaurantInternalPath($data['brestaurant']);

        $this->restaurantinfo->aws_email_verification = $data['aws_email_verification'];

        $this->result = 1;
        return true;
    }

    function getBookingType($booking) {
        $type = 'booking';
        if ($booking->restaurantinfo->is_bookable && !$booking->restaurantinfo->is_wheelable) {
            return 'listing';
        }
        if (!$booking->restaurantinfo->is_bookable) {
            return 'request';
        }

        return $type;
    }

    function getRemoteBooking($restaurant, $booker, $conf, $mobile, $email) {

        $data = pdo_single_select("select confirmation from booking where restaurant = '$restaurant' and tracking = 'remote' and booker = '$booker' and generic like '%$conf%' limit 1");
        if (count($data) > 0)
            return $this->getBooking($data["confirmation"]);

		if($mobile != '' && $email != '' && $booker = 'HGW') {
	    	$data = pdo_single_select("select confirmation from booking where restaurant = '$restaurant' and tracking = 'remote' and booker = '$booker' and email = '$email' && mobile = '$mobile' limit 2");
			if (count($data) == 1) {
	            WY_debug::recordDebug("ERROR", "CONSOLIDATION", "GOOD alternate " . $theRestaurant . ", conf =  " . $conf . ", mobile = " . $mobile . ", email=" . $email . ", confirmation=" . $data["confirmation"]);
				return $this->getBooking($data["confirmation"]);
				}
			}
			
 	  	WY_debug::recordDebug("ERROR", "CONSOLIDATION", "NO alternate " . $theRestaurant . ", conf =  " . $conf . ", mobile = " . $mobile . ", email=" . $email);
    	$this->msg = "unable to found remote booking from " . $booker . " : " . $conf . " for restaurant " . $restaurant;
    	return $this->result = -1;
    }

    function getbookinginvoice($endate, $type, $email, $restaurant, $membertype) {

        $resdata = new WY_restaurant();
        if ($resdata->ismember_priviledge($membertype) == false) {
            $this->msg = "you do not have the right permission";
            $this->result = -1;
            return array();
        }

        $nmonth = 1;        
		$fdate = date_create($endate);
		date_sub($fdate, date_interval_create_from_date_string($nmonth . " month"));
		$startday = date_format($fdate, "Y-m-d");
			
        $field = "confirmation, type, restaurant, cdate, rdate, rtime, email, mobile, status, country, cover, salutation, lastname, firstname, tracking, flag, tablename, booking_deposit_id as deposit";
		$goodbooking = "lastname != 'test' and firstname != 'test' and specialrequest not like 'test %' and restaurant NOT IN ('SG_SG_R_TheFunKitchen', 'SG_SG_R_TheOneKitchen', 'FR_PR_R_LaTableDeLydia', 'KR_SE_R_TheKoreanTable')";

		if($nmonth > 1)
			ini_set('memory_limit','180M');
			
        if (strpos($type, "r") !== false)
            $sql = "SELECT $field FROM booking WHERE rdate >= '$startday' and rdate < '$endate' and $goodbooking ORDER BY rdate, restaurant ASC";
        else if (strpos($type, "c") !== false)
            $sql = "SELECT $field FROM booking WHERE cdate >= '$startday' and cdate < '$endate'  and $goodbooking ORDER BY cdate, restaurant ASC";
        else return array();

        $data = pdo_multiple_select($sql);
        $this->result = (count($data) > 0) ? 1 : -1;
        return $data;
        	
    }


    function getBookingField($restaurant, $fieldAr, $starting, $ending, $index) {

        $validArg = array("restaurant", "starting", "ending", "field");
        $validFied = array("restaurant", "starting", "ending", "field");

        $mapping = array("restaurant" => "restaurant", "booking" => "confirmation", "type" => "type", "bkstatus" => "status", "date" => "rdate", "time" => "rtime", "pers" => "cover", "fullname" => "concat(salutation, ' ', firstname, ' ', lastname)", "email" => "email", "phone" => "mobile", "comment" => "specialrequest", "tablename" => "tablename", "tracking" => "tracking", "createdate" => "cdate", "canceldate" => "canceldate", "wheelwin" => "wheelwin", "booker" => "booker", "hotelguest" => "hotelguest", "company" => "company", "state" => "state", "restCode" => "restCode", "browser" => "browser", "language" => "language", "optin" => "optin", "review_status" => "review_status");

        foreach ($validArg as $label)
            $$label = clean_input($$label);

        $date1 = new DateTime($starting);
        $date2 = new DateTime($ending);
        if ($date1 > $date2) {
            $tmp = $starting;
            $starting = $ending;
            $ending = $tmp;
        }

        $sep = "";
        foreach ($mapping as $label => $value)
            if (in_array($label, $fieldAr)) {
                $field .= $sep . $value;
                $sep = ", ";
            }

        if ($field == "")
            return array();

        $size = 5000;
        $limit = $size;
        if ($index >= 0 && $index < 100)
            $limit = ($index * $size) . ", " . (($index + 1) * $size);

        return pdo_multiple_select("SELECT $field FROM booking WHERE rdate >= '$starting' and rdate <= '$ending' and restaurant = '$restaurant' ORDER BY rdate DESC limit $limit");
    }

	// type is either "r" for booking date, 
	// "c" for create date, 
	// "g" for the groupe of restaurant defined by email account
	
    function getBookingdata($year, $type, $email, $restaurant, $membertype) {

        $resdata = new WY_restaurant();
        $priviledge = $resdata->ismember_priviledge($membertype);
        $resto = $resdata->getListAccountRestaurant($email);
        $data = array();

        if (count($resto) < 1) {
            $this->msg = "invalid account";
            $this->result = -1;
            return array();
        	}

		$flag = false;
		$restolist = $sep = "";
		foreach ($resto as $row) {
			if ($row['restaurant'] == $restaurant)
				$flag = true;
			$restolist .=  $sep . "'" . $row['restaurant'] . "'";
			$sep = ",";
			}

        if ($flag == false && $priviledge == false) {
            $this->msg = "invalid restaurant " . $restaurant;
            $this->result = -1;
            return array();
        }

		$qry = (strpos($type, "g") !== false) ? " and restaurant in ($restolist) " : " and restaurant = '$restaurant' ";
        $field = "cdate, rdate, rtime, status, state, type, country, cover, lastname, firstname, mealtype, specialrequest, generic as more, language, tracking";
		$col = (strpos($type, "r") !== false) ? "rdate" : "cdate";
        $sql = "SELECT $field FROM booking WHERE year($col) = '$year' $qry ORDER BY $col ASC";
        $data["bookings"] = pdo_multiple_select_numindex($sql);
        $data["fields"] = "cdate,rdate,rtime,status,state,type,country,cover,lastname,firstname,mealtype,specialrequest,more,language,tracking";
        $data["title"] = (strpos($type, "g") === false) ? $data["title"] = WY_restaurant::getTitle($restaurant) : "The Groupe";
        $this->result = (count($data["bookings"]) > 0) ? 1 : -1;
        return $data;
    }

    // g -> multiple restaurant
    // more than 10 resto YOU must be admin
    // when -> number of days : 32 is default
    // c -> create date, r-> booking date
    // x -> specific month-year
    function getBookingDayReportData($when, $type, $email, $restaurant, $membertype) {
        $resdata = new WY_restaurant();
        $resto = $resdata->getListAccountRestaurant($email);
        $nb_resto = $resdata->getNbListRestaurant($email, $membertype);
        $priviledge = $resdata->ismember_priviledge($membertype);
		$extratest = "";
		
        if($nb_resto < 1 || count($resto) < 1) {
            $this->msg = "invalid account";
            $this->result = -1;
            return array();
        	}

		if(empty($restaurant)) {
			$restaurant = $resto[0]["restaurant"];
			$type .= "g";
			$demoresto = $resdata->getdemoResto();
			if(count($demoresto) > 0) {
				$extratest = " and restaurant not in ('" . implode("','", $demoresto) . "') ";
				}	// filter demo restaurant
			}
			
        if(empty($when))
            $when = '32';

		$flag = false;
		$restolist = $sep = "";
		foreach ($resto as $row) {
			if ($row['restaurant'] == $restaurant)
				$flag = true;
			$restolist .=  $sep . "'" . $row['restaurant'] . "'";
			$sep = ",";
			}

        if ($flag == false && $priviledge == false) {
            $this->msg = "invalid restaurant " . $restaurant;
            $this->result = -1;
            return array();
        	}

        $grpreq = (strpos($type, "g") !== false) ? " and restaurant in ($restolist) " : " and restaurant = '$restaurant' ";
        if ($priviledge && $nb_resto > 10 && strpos($type, "g") !== false) // super admin
            $grpreq = "";

        $today = date("Y-m-d");
        $lastday = date_create($today);
        date_add($lastday, date_interval_create_from_date_string("1 days"));
        $lastday = date_format($lastday, "Y-m-d");

        $when = intval($when);

        $date = date_create($today);
       	date_sub($date, date_interval_create_from_date_string($when . " days"));
        $startday = date_format($date, "Y-m-d");
        $field = "confirmation, type, restCode, membCode, restaurant, cdate, rdate, rtime, canceldate, email, mobile, status, country, cover, company, hotelguest, salutation, lastname, firstname, mealtype, tablename, specialrequest, revenue, generic as more, browser, language, tracking, booker, ip, flag, state, optin, wheelwin, booking_deposit_id";
		if(strpos($type, "a") !== false)
			$field = "date(cdate) as cdate, rdate, if(substring(rtime, 1, 2) < 16, 1, 2), status, state, tracking, IF(wheelwin='', wheelwin, 1) as wheelwin, cover as pax, product";
	
		$flagx = (strpos($type, "x") !== false);
		$flagy = (strpos($type, "y") !== false);
		$flagc = (strpos($type, "c") !== false);
		$flagr = (strpos($type, "r") !== false);
	
        if($flagc || $flagr) {
            $fielddate = ($flagc) ? "cdate" : "rdate";
            $sql = "SELECT $field FROM booking WHERE $fielddate >= '$startday' and $fielddate <= '$lastday' $grpreq $extratest ORDER BY $fielddate, restaurant ASC";
            }
            
        else if($flagx || $flagy) {
            $tmpAr = explode("|", $type);
            $month = intval($tmpAr[1]) + 1;
            $year = intval($tmpAr[2]);
            $diffyear = intval(date("Y")) - $year;
            $fielddate = ($flagx) ? "cdate" : "rdate";
            if ($month < 1 || $month > 12 || $diffyear < -1 || $diffyear > 1) {
                $this->result = -1;
                return array('', '');
            	}
            $sql = "SELECT $field FROM booking WHERE month($fielddate) = '$month' and year($fielddate) = '$year' $grpreq $extratest ORDER BY $fielddate, restaurant ASC";
        } else {
            $sql = "SELECT $field FROM booking WHERE cdate >= '$lastday' $grpreq $extratest ORDER BY cdate, restaurant ASC";
        }


	// keep this one, this is to suppress 'as'
        $field = "confirmation, type, restCode, membCode, restaurant, cdate, rdate, rtime, canceldate, email, mobile, status, country, cover, company, hotelguest, salutation, lastname, firstname, mealtype, tablename, specialrequest, revenue, more, browser, language, tracking, booker, ip, flag, state, optin, wheelwin, booking_deposit_id";
		if(strpos($type, "a") !== false)
        	$field = "cdate, rdate, mealtype, status, state, tracking, wheelwin, pax, product";

		// reset "generic"
 	return array($sql, $field);
    }

	function getBookingDayReport($when, $type, $email, $restaurant, $membertype) {
	
		list($sql, $field) = $this->getBookingDayReportData($when, $type, $email, $restaurant, $membertype);
		if($this->result < 0)
			return array();
		if(strpos($type, "o") !== false) {
	        $data["bookings"] = pdo_multiple_select_numindex($sql);
	        $data["fields"] = $field;
			$this->result = (count($data["bookings"]) > 0) ? 1 : -1;
        	}
        else {
			$data = pdo_multiple_select($sql);
			$this->result = (count($data) > 0) ? 1 : -1;
        	}
		return $data;
		}
		
    function duplicate($restaurant, $rdate, $email, $mobile, $log = 0) {

        $this->result = -1;
        if($log == 0) $rdate = substr($rdate, 6, 4) . "-" . substr($rdate, 3, 2) . "-" . substr($rdate, 0, 2);
        $data = pdo_single_select("SELECT salutation, lastname, firstname, email, mobile, company, country, confirmation, rtime, cover from booking where restaurant = '$restaurant' and rdate = '$rdate' and status = '' and (email = '$email' or mobile = '$mobile') limit 1");
        if (count($data) > 0) {
            $this->confirmation = $data['confirmation'];
            $this->rtime = $data['rtime'];
            $this->cover = $data['cover'];
            $this->lastname = $data['lastname'];
            $this->firstname = $data['firstname'];
            $this->result = 1;
        }
        return $this->result;
    }

    function getBookingPhone($restaurant, $phone) {
        // search in one or multiple restaurant

        $restaurant = $this->clean_input($restaurant);
        if (preg_match("/,/", $restaurant) == false) {
            $data = pdo_single_select("SELECT salutation, lastname, firstname, email, mobile, company from profile, profile_sub where restaurant = '$restaurant' and mobile = '$phone' and profile.systemid = profile_sub.systemid limit 1");
            if(count($data) > 0)
            	return $data;
            return pdo_single_select("SELECT salutation, lastname, firstname, email, mobile, company, country, hotelguest from booking where restaurant = '$restaurant' and mobile = '$phone'  limit 1");
            }
        else {
            $set = "'" . preg_replace("/,/", "','", $restaurant) . "'";
            $data = pdo_single_select("SELECT salutation, lastname, firstname, email, mobile, company from profile, profile_sub where restaurant in ($set) and mobile = '$phone' and profile.systemid = profile_sub.systemid  limit 1");
            if(count($data) > 0)
            	return $data;
            return pdo_single_select("SELECT salutation, lastname, firstname, email, mobile, company, country, hotelguest from booking where restaurant in ($set) and mobile = '$phone'  limit 1");
        }
    }

    function readmadisonmember() {
        $data['emails'] = pdo_multiple_select("SELECT email from member_madison");
        $data['mobiles'] = pdo_multiple_select("SELECT mobilephone from member_madison");
        return $data;
    }

    function getBookingEmail($restaurant, $email) {
        // search in one or multiple restaurant

        $this->result = 1;
        $restaurant = $this->clean_input($restaurant);
        if (preg_match("/,/", $restaurant) == false)
            return pdo_single_select("SELECT salutation, lastname, firstname, email, mobile, company, country, hotelguest from booking where restaurant = '$restaurant' and email = '$email'  limit 1");
        else {
            $set = "'" . preg_replace("/,/", "','", $restaurant) . "'";
            return pdo_single_select("SELECT salutation, lastname, firstname, email, mobile, company, country, hotelguest from booking where restaurant in ($set) and email = '$email'  limit 1");
        }
    }

    function getMadisonBookingPhone($restaurant, $phone) {
        // search in one or multiple restaurant

        $this->result = 1;
        $restaurant = $this->clean_input($restaurant);
        $phone = str_replace('+', '', trim($phone));
        $phone = str_replace(' ', '', $phone);

        $sql = "SELECT salutation, lastname, firstname, email, mobilephone as mobile, companyname as company from member_madison where mobilephone LIKE '%$phone%' limit 1";

        $data = pdo_single_select($sql);

        if (count($data) < 1)
            return $this->result = -1;

        $data['country'] = 'Singapore';
        $data['hotelguest'] = 'no';

        return $data;
    }

    function getMadisonMemberEmail($restaurant, $email) {

        $this->result = 1;
        $restaurant = $this->clean_input($restaurant);
        $sql = "SELECT salutation, lastname, firstname, email, mobilephone as mobile, companyname as company from member_madison where email = '$email' limit 1";
        $data = pdo_single_select($sql);

        if (count($data) < 1) {
            return $this->result = -1;
        }

        $data['country'] = 'Singapore';
        $data['hotelguest'] = 'no';

        return $data;
    }

    function getBookingDepositDetails($confirmation, $type = NULL) {
        $sql = "SELECT b.status, b.rdate, b.rtime, b.cover, b.cdate,  r.lunch_charge as booking_deposit_lunch, r.dinner_charge as booking_deposit_dinner FROM booking b, restaurant_cancel_policy r WHERE b.restaurant = r.restaurant AND b.product = r.product AND  b.confirmation LIKE '$confirmation'";
        $data = pdo_single_select($sql);

        date_default_timezone_set('Asia/Singapore');
        $now = strtotime('now');
        date_default_timezone_set('UTC');
        $bck_date = date($data['cdate']);

        $diff = strtotime('+60 minutes', strtotime(date($data['cdate']))) - $now;
        
        $deposit = null;
        if ($data['status'] == 'pending_payment') {
            $amount = 100;
            $booking_time_ar = explode(':', $data['rtime']);

            //dinner 
            if (5 < $booking_time_ar[0] && $booking_time_ar[0] < 14) {

                $amount = $data['cover'] * $data['booking_deposit_lunch'];
            } else {
                $amount = $data['cover'] * $data['booking_deposit_dinner'];
            }
            
   
            $time_left = 0;

            if ($diff > 0) {

                $time_left = $diff;
            }

            if ($type === 'mybookings') {
                $time_left = $diff;
            }

            $deposit = array(
                'amount' => $amount,
                'currency' => 'SGD',
                'time_left' => $time_left);
        }
        return $deposit;
    }
    
   function getBookingDepositBaccha($restaurant,$confirmation){
       $respolicy = new WY_Restaurant_policy;
       $this->getBooking($confirmation);
       $depositAmount = $respolicy->getDefaultPrice($restaurant);
       $booking_time_ar = explode(':', $this->rtime);
       //dinner 
       if (5 < $booking_time_ar[0] && $booking_time_ar[0] < 14) {
           $amount = $this->cover * $depositAmount['lunch_charge'];
       } else {
           $amount = $this->cover * $depositAmount['dinner_charge'];
       }

       $deposit = array(
           'amount' => $amount,
           'currency' => 'SGD',
           'time_left' => ''
       );
       return $deposit;
       
       
   }

    function setBooking($restaurant, $confirmation, $value, $label, $captain = null) {
        if ($this->getBooking($confirmation) == false)
            return $this->error_msg(-1, "Confirmation " . $confirmation . " does not exits. Unable so set $label to $value.");

        if ($this->confirmation != $confirmation || $this->restaurant != $restaurant)
            return $this->error_msg(-1, "Invalid confirmation " . $confirmation . " for " . $this->restaurant . ". Unable so set $label to $value.");

		$value = preg_replace("/\'|\"|’/", "`", $value);
	
        switch ($label) {
            case 'tablename':
                 pdo_exec("update booking set tablename='$value' where confirmation = '$confirmation' and restaurant = '$restaurant' limit 1");
                  if($captain != null) {
			$captain = preg_replace("/\'|\"|’/", "`", $captain);
			$this->addtoGeneric('{"more":"{’captain’: ’' . $captain . '’}"}');
			}
                 break;
                 
           case 'notestext':
				$this->addtoGeneric('{"more":"{’notestext’: ’' . $value . '’}"}');
                break;
                 
            case 'state':
                date_default_timezone_set('Asia/Singapore');
                $generic = "";
                $time = date("H:i");
                if(substr($value, 0, 9) == "cleartime") {
					$data = explode("|", $value);
					foreach($data as $row) {
						$rowAr = explode("=", $row);
						$this->addtoGeneric('{"more":"{’' . $rowAr[0] . '’: ’' . $rowAr[1] . '’}"}');
						}
					}
                else {
					if($label == "state" && $value == "no show")
						pdo_exec("update booking set status = 'noshow' where confirmation = '$confirmation' and restaurant = '$restaurant' limit 1");
					else if ($value == "seated" || $value == "left")
						$this->addtoGeneric('{"more":"{’' . $value . '’: ’' . $time . '’}"}');
                 	pdo_exec("update booking set state='$value' where confirmation = '$confirmation' and restaurant = '$restaurant' limit 1");
               		}
				
                if ($value == "seated") {
                    if ($restaurant == "SG_SG_R_MadisonRooms") {
                        $madisonRoom = new WY_Partner('SG_SG_R_MadisonRooms');
                        $madisonRoom->pushEventToPartner($this, 'Revel');
                    }
                }             			
                break;
        }

        $this->msg = "The $label '$value' for confirmation $confirmation for restaurant $restaurant has been updated";
        //error_log("SET BOOKING " . "update booking set $label='$value' where confirmation = '$confirmation' and restaurant = '$restaurant' limit 1");
        return $this->result = 1;
    }

    function getUserBookings($email, $options) {

        $restaurant = new WY_restaurant();
        $payment = new WY_Payment();
        $media = new WY_Media();
        $res_policy = new WY_Restaurant_policy;
        $select = '';
        $where = '';
        $order = '';
        $limit = '';
        $join = '';


        ////  FILTER CREATION  ////
        if (isset($options['last'])) {
            $order .= ' rdate DESC, rtime DESC';
            $limit .= $options['last'];
        }
        if (isset($options['win_only'])) {
            $where .= " AND wheelwin NOT LIKE ''";
        }
        if (!empty($order)) {
            $order = ' ORDER BY ' . $order;
        }
        if (!empty($limit)) {
            $limit = ' LIMIT ' . $limit;
        }
        if (isset($options['tracking']) && $options['tracking'] == 'no_white_label') {
            //$where .= " AND tracking NOT IN('CALLCENTER','WEBSITE', 'facebook','walkin','tms', 'GRABZ') ";
            $where .= " AND tracking NOT REGEXP '(CALLCENTER|WEBSITE|facebook|walkin|tms|GRABZ|website)' ";
        
        }

        $now_date = date('Y-m-d');

        if (!empty($options['period'])) {
            switch ($options['period']) {
                case 'today':
                    $where .= " AND rdate = '$now_date' ";
                    break;
                case 'past':
                    $where .= " AND rdate < '$now_date' ";
                    break;
                default:
                    $where .= " AND rdate > '$now_date' ";
                    break;
            }
        }

        //// END FILTER CREATION  ////
        //force review 
        $options['include_reviews'] = true;
        if (isset($options['include_reviews'])) {
            $select .= ', review.reviewgrade ';
            $join .= ' LEFT JOIN review ON  review.confirmation = booking.confirmation';
        }
        if (isset($options['expected_reviews'])) {
            $where .= " AND review.reviewgrade IS NULL ";
        }
        $sql = "SELECT booking.id,booking.confirmation,booking.status as bstatus,booking.booking_deposit_id, booking.specialrequest, membCode, booking.restaurant, booking.email as member_email, product,lastname, firstname, rdate,cdate,rtime, cover, company, booking.state as bkstate, booking.flag as flag, title, tel, wheelwin, tracking, booker, ip, tablename, hotelguest, restaurant.is_bookable, restaurant.is_wheelable, restaurant.email, restaurant.dfminpers, restaurant.dfmaxpers, address, city, zip, booking.country, map, GPS, restaurant.images $select from  restaurant, booking $join WHERE 1 and restaurant.restaurant = booking.restaurant and booking.email = '$email' $where $order $limit";

        $data = pdo_multiple_select($sql);



        if (count($data) <= 0)
            return false;
        foreach ($data as $row) {
            $whiteLabel = false;

            

                $current = new WY_Booking();
                //kala added booking id
                $current->bookid = $row['id'];
                $current->confirmation = $row['confirmation'];
                $current->membcode = $row['membCode'];
                $current->status = $row['bstatus'];
                $current->firstname = $row['firstname'];
                $current->lastname = $row['lastname'];
                $current->email = $row['member_email'];
                $current->rdate = $row['rdate'];
                $current->rtime = $row['rtime'];
                $current->cdate = $row['cdate'];
                $current->cover = $row['cover'];
                $current->company = $row['company'];
                $current->state = $row['bkstate'];
                $current->flag = $row['flag'];
                $current->tracking = $row['tracking'];
                $current->booker = $row['booker'];
                $current->ip = $row['ip'];
                $current->tablename = $row['tablename'];
                $current->wheelwin = $row['wheelwin'];
                $current->hotelguest = $row['hotelguest'];
                $current->specialrequest = $row['specialrequest'];
                $current->deposit_id = $row['booking_deposit_id'];
                $current->product= $row['product'];

                $current->reviewgrade = $row['reviewgrade'];

                $current->restaurantinfo = new WY_restaurantConf;

                $current->restaurantinfo->restaurant = $row['restaurant'];
                $current->restaurantinfo->title = $row['title'];
                $current->restaurantinfo->tel = $row['tel'];
                $current->restaurantinfo->email = $row['email'];
                $current->restaurantinfo->URLrestaurant = ""; //$data['url'];
                $current->restaurantinfo->address = $row['address'];
                $current->restaurantinfo->city = $row['city'];
                $current->restaurantinfo->zip = $row['zip'];
                $current->restaurantinfo->country = $row['country'];
                $current->restaurantinfo->GPS = $row['GPS'];

                $current->restaurantinfo->is_bookable = $row['is_bookable'];
                $current->restaurantinfo->is_wheelable = $row['is_wheelable'];
                $current->restaurantinfo->dfmaxpers = $row['dfmaxpers'];
                $current->restaurantinfo->dfminpers = $row['dfminpers'];


                //depsoit details
                $current->amount = "";
                $current->currency = "";
                $current->timeleft = "";
                $current->bkObject = "nodeposit";
                $current->refundamount = "";
                $current->isrefund = 'norefund';
                $current->transaction_id = $row['booking_deposit_id'];
                $current->payment_method = "";
                $current->payment = 0;

                $current->restaurantinfo->internal_path = $restaurant->getRestaurantInternalPath($row['restaurant']);

                if (!isset($image_resto[$current->restaurantinfo->restaurant])) {

                    $current->restaurantinfo->defImg = $media->getDefaultPicture($current->restaurantinfo->restaurant);
                }
                $depositdetails = $payment->getDepositDetails($row['booking_deposit_id'], $row['confirmation']);

                $current->time_left = $this->getDateInterval($row['cdate'], '', 'min');

                if (!empty($row['booking_deposit_id']) && $row['booking_deposit_id'] !== NULL) {
                    $current->bkObject = "deposit";
                     if($row['restaurant'] == 'SG_SG_R_TheFunKitchen' || $row['restaurant'] == 'SG_SG_R_Bacchanalia' ){
                           
                            $dpDetails = $res_policy->getResChargeDetails($row['booking_deposit_id'],$row['confirmation']);
                        }else{
                              $dpDetails = $this->getDepositDetailsByID($row['booking_deposit_id'], $row['rdate'], $row['rtime'], $row['restaurant'], $depositdetails['payment_method'], $row['confirmation']);
                        }

                  

                    if (count($dpDetails) > 0) {
                        $current->payment_method = $dpDetails['payment_method'];
                        $current->amount = $dpDetails['amount'];
                        $current->currency = "SGD";
                        $current->refundamount = $dpDetails['refund_amount'];
                        $current->paystatus = $dpDetails['status'];
                        $current->payment = $dpDetails['isPayment'];

                        if ($dpDetails['status'] === 'COMPLETED') {
                            $current->isrefund = 'refund';
                        }
                    }
                }

                //kala added this line for date interval from currentdate to booking date
                //$days = (strtotime($endDate) - strtotime($startDate)) / (60 * 60 * 24);
                $this->bookings[] = $current;
            
        }

        return true;
    }

    public function getBookingPaymentInfo($payment_id, $confirmation, $status) {
        $result = array();
        $res = new WY_restaurant();
        $payment = new WY_Payment();
        $payment_method = 'carddetails';
        if ($status !== 'cancel') {
            $depositdetails = $payment->getDepositDetails($payment_id, $confirmation);
            $payment_method = $depositdetails['payment_method'];
        }
        $this->getBooking($confirmation);
        $dpDetails = $this->getDepositDetailsByID($payment_id, $this->rdate, $this->rtime, $this->restaurant, $payment_method, $confirmation);

        if (count($dpDetails) > 0) {
            $paymentFlag = $res->chkPaymentInfo($this->restaurant, 'PHILOU');
            $result = $paymentFlag;

            $refundArr = null;
            if ($dpDetails['status'] === 'REFUNDED') {
                $refundArr = array(
                    'refund_id' => $payment_id,
                    'amount' => $dpDetails['amount'],
                    'date' => $dpDetails['updated_at'],
                    'currencey' => 'SGD',
                    'payment_method' => $dpDetails['payment_method']
                );
            }
            $payment_method = ($dpDetails['payment_method'] == 'paypal') ? $dpDetails['payment_method'] : 'stripe';
            if ($status === 'cancel' || $status === 'noshow') {
                $depositdetails = $payment->getDepositDetails($dpDetails['object_id'], $confirmation);
                $amount = ($dpDetails['payment_method'] == 'carddetails') ? $dpDetails['amount'] : $dpDetails['refund_amount'];
                $result['payment_id'] = $payment_id;
                $result['payment_reference_id'] = $dpDetails['object_id'];
                $result['payment_method'] = $payment_method;
                $result['amount'] = $amount;
                $result['currency'] = "SGD";
                $result['suggested_amount'] = $dpDetails['amount'];
                $result['status'] = $dpDetails['status'];
                $result['refund'] = $refundArr;
                $result['time_left'] = null;
            } else {
                $payment_status = ($dpDetails['payment_method'] != 'carddetails') ? $dpDetails['status'] : "CCDSUBMITTED";
                $amount = ($dpDetails['payment_method'] != 'carddetails') ? $dpDetails['amount'] : null;
                $suggested_amount = ($dpDetails['payment_method'] == 'carddetails') ? $dpDetails['amount'] : $dpDetails['refund_amount'];
                $payment_reference_id = ($dpDetails['payment_method'] === 'carddetails') ? $payment_id : $dpDetails['object_id'];
                $dppayment_id = ($dpDetails['payment_method'] === 'carddetails') ? null : $payment_id;

                $result['payment_id'] = $dppayment_id;
                $result['payment_reference_id'] = $payment_reference_id;
                $result['payment_method'] = $payment_method;
                $result['amount'] = $amount;
                $result['currency'] = "SGD";
                $result['suggested_amount'] = "$suggested_amount";
                $result['status'] = $payment_status;
                $result['refund'] = null;
                $result['time_left'] = null;
            }
        }
        return $result;
    }

    //kala created this function for load single booking based on booking id

    function getBookingId($bookId, $options) {
        require_once("lib/wpdo.inc.php");

        $restaurant = new WY_restaurant();
        $select = '';
        $where = '';
        $order = '';
        $limit = '';
        $join = '';
        $options['include_reviews'] = true;
        if (isset($options['include_reviews'])) {
            $select .= ', review.reviewgrade ';
            $join .= ' LEFT JOIN review ON  review.confirmation = booking.confirmation';
        }


        $sql = "SELECT booking.id,booking.confirmation,booking.status as bstatus, membCode, booking.restaurant, booking.email as member_email,mobile ,lastname, firstname, rdate, rtime, cover, company, booking.state as bkstate, booking.flag as flag, title, tel, wheelwin, tracking, booker, ip, tablename, hotelguest, restaurant.email, address, city, zip, booking.country, map, GPS, restaurant.images $select from  restaurant, booking $join WHERE 1 and restaurant.restaurant = booking.restaurant and booking.id = '$bookId'";
        //$sql = "SELECT * from booking WHERE id = '$bookId'";

        $row = pdo_single_select($sql);
        $current = new WY_Booking();
        //kala added booking id
        $current->bookid = $row['id'];
        $current->confirmation = $row['confirmation'];
        $current->membcode = $row['membCode'];
        $current->status = $row['bstatus'];
        $current->firstname = $row['firstname'];
        $current->lastname = $row['lastname'];
        $current->email = $row['member_email'];
        $current->mobile = $row['mobile'];
        $current->rdate = $row['rdate'];
        $current->rtime = $row['rtime'];
        $current->cover = $row['cover'];
        $current->company = $row['company'];
        $current->state = $row['bkstate'];
        $current->flag = $row['flag'];
        $current->tracking = $row['tracking'];
        $current->booker = $row['booker'];
        $current->ip = $row['ip'];
        $current->tablename = $row['tablename'];
        $current->wheelwin = $row['wheelwin'];
        $this->hotelguest = $row['hotelguest'];

        $current->reviewgrade = $row['reviewgrade'];

        $current->restaurantinfo = new WY_restaurantConf;
        $current->restaurantinfo->restaurant = $row['restaurant'];
        $current->restaurantinfo->title = $row['title'];
        $current->restaurantinfo->tel = $row['tel'];
        $current->restaurantinfo->email = $row['email'];
        $current->restaurantinfo->URLrestaurant = ""; //$data['url'];
        $current->restaurantinfo->address = $row['address'];
        $current->restaurantinfo->city = $row['city'];
        $current->restaurantinfo->zip = $row['zip'];
        $current->restaurantinfo->country = $row['country'];
        $current->restaurantinfo->GPS = $row['GPS'];

        $current->restaurantinfo->internal_path = $restaurant->getRestaurantInternalPath($row['restaurant']);

        //echo $current->interval ;
        $this->bookings[] = $current;

        return $current;
    }

    //kala created this function for booking id convert to base64url encode
    function base64url_encode($data) {
        return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
    }

    function base64url_decode($data) {
        return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
    }

    function getDateInterval($dateTo, $dateNow = '', $returnType = 'd') {
        //error_log("Date  ".$dateTo);
        
        date_default_timezone_set('Asia/Singapore');
        $time_dateTo = strtotime($dateTo);
        $interval = $time_dateTo - strtotime("now");
        $interval_hours = $interval / (60 * 60);
        //error_log("interval  ".$interval_hours);
        return $interval_hours;
    }

    function checkReservation($confirmation) {

        $restaurant = $this->restaurant;
        if (empty($restaurant) || empty($confirmation)) {
            $this->mesg = "Invalid Request, the name of the restaurant or confirmation is empty";
            return -1;
        }
        $data = pdo_single_select("SELECT confirmation, restaurant from booking WHERE restaurant = '$restaurant' and confirmation = '$confirmation' LIMIT 1");
        return (count($data) > 0) ? 1 : -1;
    }

    function getAReservation() {

        $theRestaurant = $this->restaurant;
        $theConfirmation = $this->confirmation;

        if (empty($theRestaurant) || empty($theConfirmation)) {
            return $this->error_msg(-1, "Invalid Request, the name of the restaurant or confirmation is empty");
        }

        $this->email = "";
        $this->mobile = "";
        $this->cover = "";
        $this->company = "";
        $this->state = "";
        $this->lastname = "";
        $this->firstname = "";
        $this->rdate = "";
        $this->rtime = "";
        $this->wheelwin = "";
        $this->ip = "";
        $this->tablename = "";
        $this->hotelguest = "";

        $data = pdo_single_select("SELECT confirmation, restaurant, rdate, cdate, rtime, email, mobile, cover, company, state, lastname, firstname, mealtype, wheelwin, tracking, booker, ip, tablename, hotelguest from booking WHERE restaurant = '$theRestaurant' and confirmation = '$theConfirmation' LIMIT 1");
        if (count($data) <= 0)
            return -1;

        $this->restaurant = $data['restaurant'];
        $this->confirmation = $data['confirmation'];
        $this->email = $data['email'];
        $this->mobile = $data['mobile'];
        $this->cover = $data['cover'];
        $this->company = $data['company'];
        $this->state = $data['state'];
        $this->lastname = $data['lastname'];
        $this->firstname = $data['firstname'];
        $this->rdate = $data['rdate'];
        $this->cdate = $data['cdate'];
        $this->rtime = $data['rtime'];
        $this->wheelwin = $data['wheelwin'];
        $this->tracking = $data['tracking'];
        $this->booker = $data['booker'];
        $this->ip = $data['ip'];
        $this->tablename = $data['tablename'];
        $this->hotelguest = $data['hotelguest'];

        return 1;
    }

    function getRestoConfirmation($theRestaurant) {

        $this->bookings = array();

        $data = pdo_multiple_select("SELECT confirmation, restCode, restaurant, rdate, cdate, rtime, email, mobile, cover, company, hotelguest, state, lastname, firstname, mealtype, wheelwin, tracking, booker, ip, salutation, country, language, specialrequest, generic, type, tablename from booking WHERE restaurant = '$theRestaurant' order by rdate, rtime ");
        if (count($data) <= 0)
            return -1;

        foreach ($data as $row) {
            $current = new WY_Booking();
            $current->restaurant = $row['restaurant'];
            $current->confirmation = $row['confirmation'];
            $current->restCode = $row['restCode'];
            $current->firstname = $row['firstname'];
            $current->lastname = $row['lastname'];
            $current->mobile = $row['mobile'];
            $current->email = $row['email'];
            $current->rdate = $row['rdate'];
            $current->cdate = $row['cdate'];
            $current->rtime = $row['rtime'];
            $current->cover = $row['cover'];
            $current->company = $row['company'];
            $current->hotelguest = $row['hotelguest'];
            $current->state = $row['state'];
            $current->tracking = $row['tracking'];
            $current->booker = $row['booker'];
            $current->ip = $row['ip'];
            $current->tablename = $row['tablename'];
            $current->wheelwin = $row['wheelwin'];
            $current->salutation = $row['salutation'];
            $current->country = $row['country'];
            $current->language = $row['language'];
            $current->specialrequest = $row['specialrequest'];
            $current->generic = $row['generic'];
            $current->type = $row['type'];

            $this->bookings[] = $current;
        }
        return 1;
    }

    function getAllConfirmation() {

        $this->bookings = array();

        $data = pdo_multiple_select("SELECT confirmation, restaurant, rdate, cdate, rtime, email, mobile, cover, company, hotelguest, state, lastname, firstname, mealtype, ip, tablename from booking order by restaurant, rdate, rtime");
        if (count($data) <= 0)
            return -1;

        foreach ($data as $row) {
            $current = new WY_Booking();
            $current->restaurant = $row['restaurant'];
            $current->confirmation = $row['confirmation'];
            $current->restCode = $row['restCode'];
            $current->firstname = $row['firstname'];
            $current->lastname = $row['lastname'];
            $current->mobile = $row['mobile'];
            $current->email = $row['email'];
            $current->rdate = $row['rdate'];
            $current->cdate = $row['cdate'];
            $current->rtime = $row['rtime'];
            $current->cover = $row['cover'];
            $current->company = $row['company'];
            $current->hotelguest = $row['hotelguest'];
            $current->state = $row['state'];
            $current->ip = $row['ip'];
            $current->tablename = $row['tablename'];
            $current->wheelwin = $row['wheelwin'];
            $this->bookings[] = $current;
        }
        return 1;
    }

    static function lastvisit($restaurant, $email, $mobile, $extraemail, $extramobile) {
        if(empty($email) && empty($mobile))
        	return "";
        $objToday = new DateTime(date("Y-m-d"));
        $today = $objToday->format("Y-m-d");
        	
        $qry = "";
        if(!empty($email) && strlen($email) > 7) $qry .= (!empty($qry) ? " or " : " ") . " email = '$email'";
        if(!empty($mobile) && strlen($mobile) > 7) $qry .= (!empty($qry) ? " or " : " ") . " mobile = '$mobile'";
        if(!empty($extraemail) && strlen($extraemail) > 7) $qry .= (!empty($qry) ? " or " : " ") . " email = '$extraemail'";
        if(!empty($extramobile) && strlen($extramobile) > 7) $qry .= (!empty($qry) ? " or " : " ") . " mobile = '$extramobile'";
        
        $data = pdo_single_select("SELECT confirmation, rdate, rtime, cover from booking WHERE restaurant = '$restaurant' and ($qry) and rdate < '$today' and status = '' order by rdate DESC limit 1");
        if (count($data) > 0)
            return $data["rdate"];
        return "";
    }

    static function firstvisit($restaurant, $email, $mobile, $extraemail, $extramobile) {
        if(empty($email) && empty($mobile))
        	return "";
        $objToday = new DateTime(date("Y-m-d"));
        $today = $objToday->format("Y-m-d");
        	
        $qry = "";
        
        if(!empty($email) && strlen($email) > 7) $qry .= (!empty($qry) ? " or " : " ") . " email = '$email'";
        if(!empty($mobile) && strlen($mobile) > 7) $qry .= (!empty($qry) ? " or " : " ") . " mobile = '$mobile'";
        if(!empty($extraemail) && strlen($extraemail) > 7) $qry .= (!empty($qry) ? " or " : " ") . " email = '$extraemail'";
        if(!empty($extramobile) && strlen($extramobile) > 7) $qry .= (!empty($qry) ? " or " : " ") . " mobile = '$extramobile'";
        
        $data = pdo_single_select("SELECT confirmation, rdate, rtime, cover from booking WHERE restaurant = '$restaurant' and ($qry) and rdate < '$today' and status = '' order by rdate ASC limit 1");
        if (count($data) > 0)
            return $data["rdate"];
        return "";
    }

    // only get 'active' booking, to calculate remaining allotement
    static function getDateBooking($theRestaurant, $product) {

        $objToday = new DateTime(date("Y-m-d"));
        $today = $objToday->format("Y-m-d");

        $data = pdo_multiple_select("SELECT confirmation, rdate, rtime, cover from booking WHERE restaurant = '$theRestaurant' and rdate >= '$today' and (status = '' or status = 'pending_payment') and product = '$product'  order by rdate, rtime");
        if (count($data) <= 0)
            return array();

        $kk = 0;
        foreach ($data as $row) {
            $objDateTime = new DateTime($row['rdate']);
            $ndays = $objToday->diff($objDateTime)->format('%a');
            $horaire = ((intval(substr($row['rtime'], 0, 2)) * 2) + ceil((intval(substr($row['rtime'], 3, 5)) / 30)));
            $mydata[$kk]['ndays'] = $ndays;
            $mydata[$kk]['confirmation'] = $row['confirmation'];
            $mydata[$kk]['date'] = $row['rdate'];
            $mydata[$kk]['rtime'] = $row['rtime'];
            $mydata[$kk]['cover'] = $row['cover'];
            $mydata[$kk]['time'] = $horaire;
            $kk++;
        }
        return $mydata;
    }

	static function getBookingWithTable($theRestaurant, $tablename, $rdate, $rtime) {
		$data = pdo_multiple_select("SELECT confirmation, rdate, rtime, generic, tablename from booking WHERE restaurant = '$theRestaurant' and rdate = '$rdate' and tablename = '$tablename' and generic like '%seated%'");
		if (count($data) <= 0)
			return "";

		//echo print_r($data, true);
		$theTime = (intval(substr($rtime, 0, 2)) * 60) + intval(substr($rtime, 3, 2));
		foreach($data as $row) {
			if(preg_match("/’seated’:’(\d{2}:\d{2})’/", $row['generic'], $match)) {
				$stime = (intval(substr($match[1], 0, 2)) * 60) + intval(substr($match[1], 3, 2));
				if(abs($theTime - $stime) < 60)
					return $row['confirmation'];
				}
			}
		return "";
		}

	static function addFieldGeneric($theRestaurant, $confirmation, $orders) {
		$data = pdo_single_select("SELECT generic from booking WHERE restaurant = '$theRestaurant' and confirmation = '$confirmation' limit 1");
		if(count($data) < 1) {
			return -1;
			}
		
		$generic = $data['generic'];	
		$genAr = array();
		if(!empty($generic)) {
			$genAr = json_decode(preg_replace("/’/", "\"", $generic), true);
			}
		$genAr['orders'] = json_decode(preg_replace("/’/", "\"", $orders));
		$newgeneric = preg_replace("/\"/", "’", json_encode($genAr));
		pdo_exec("update booking set generic = '$newgeneric' WHERE restaurant = '$theRestaurant' and confirmation = '$confirmation' limit 1");
		}
		
	function import($theRestaurant, &$fielddata, $remoteconf) {
		$data = "";
		$this->restaurant = $theRestaurant;
		if(empty($remoteconf) || strlen($remoteconf) < 3)
			return $this->result = -1;

		$criteria = "%" . $remoteconf . "%";
		$data = pdo_single_select("SELECT confirmation from booking WHERE restaurant = '$theRestaurant' and type = 'import' and generic like '$criteria' limit 1");
		if(count($data) > 0) {
			$confirmation = $data['confirmation'];
			$update = $sep = "";
			foreach($fielddata as $label => $value) {
				$update .= $sep . $label . "='" . $value . "'";
				$sep = ",";
				}
			pdo_exec("update booking set $update WHERE restaurant = '$theRestaurant' and type = 'import' and confirmation = '$confirmation' limit 1");
			echo "update booking set $update WHERE restaurant = '$theRestaurant' and type = 'import' and confirmation = '$confirmation' limit 1" . "<hr />";
			}
		else {
			list($confirmation, $restCode, $membCode) = $this->createUniqConfNumer();
			$insert_label = $insert_data = "";
			foreach($fielddata as $label => $value) {
				$insert_label .= ", " . $label;
				$insert_data .= ", '" . $value . "'";
				}
			 pdo_insert("insert into booking (restaurant, type, confirmation $insert_label) value ('$theRestaurant', 'import', '$confirmation' $insert_data )");
                    
			//echo "insert into booking (restaurant, confirmation $insert_label) value ('$theRestaurant', '$confirmation' $insert_data )" . "<hr />";
			if(isset($confirmation)) {
                            if($this->getBooking($confirmation)){
				$prof = new WY_Profile();
				$result = $prof->insertProf($this);
				if($result < 1)
					echo "profil for " . $confirmation . " NOT CREATED: " . $result . "<br />";
                            }

                        }else  { echo "ERROR UNABLE TO FOUND " . $confirmation . "<br />";}
                       
			}
		return $confirmation;
		}
		
    // only get 'active' booking, to calculate remaining allotement
    static function getThatDayBooking($theRestaurant, $rdate, $product) {

        // rdate yyyy-mm-dd
        // current day of week. Mysql format => conversion d-m-Y to Y-m-d
        $ctime = strtotime($rdate);
        $thedate = date('Y-m-d', $ctime);

        $data = pdo_multiple_select("SELECT confirmation, rdate, rtime, cover from booking WHERE restaurant = '$theRestaurant' and rdate = '$thedate' and (status = '' or status = 'pending_payment') and product = '$product'  order by rtime");
        if (count($data) <= 0)
            return array();

        $kk = 0;
        foreach ($data as $row) {
            $horaire = ((intval(substr($row['rtime'], 0, 2)) * 2) + ceil((intval(substr($row['rtime'], 3, 5)) / 30)));
            $mydata[$kk]['ndays'] = 0;
            $mydata[$kk]['confirmation'] = $row['confirmation'];
            $mydata[$kk]['date'] = $row['rdate'];
            $mydata[$kk]['rtime'] = $row['rtime'];
            $mydata[$kk]['cover'] = $row['cover'];
            $mydata[$kk]['time'] = $horaire;
            $kk++;
        }
        return $mydata;
    }

    function getBookingByTracking($tracking) {
        $sql = "SELECT ID, type, restaurant, status, lastname, cdate, rdate, rtime, wheelwin FROM booking WHERE tracking ='" . $tracking . "' ";
        return pdo_multiple_select($sql);
    }

    function getBookingBySource($tracking) {
        switch ($tracking) {
            case 'gourmand':
                $campaign = 'w-gourmand_restaurant_20150702_0018';
                break;
            case 'marie-france':
                $campaign = 'w-marie-france_restaurant_20150729_0019';
                break;
            case 'durulane':
                $campaign = 'e-drurylane_restaurant_20151008_0022';
                break;
            default:
                $campaign = $tracking;
                break;
        }
        $sql = "SELECT ID, type, restaurant, status, lastname, cdate, rdate, rtime, wheelwin FROM booking WHERE tracking ='" . $campaign . "' ";
        return pdo_multiple_select($sql);
    }

    public function getSpinResult($confirmation) {
        $data = pdo_single_select("SELECT wheelsegment, wheelwin, wheeldesc, wheelwinangle, spinsource from booking WHERE confirmation = '$confirmation' LIMIT 1");
        if (count($data) <= 0)
            return -1;

        $this->wheelsegment = $data['wheelsegment'];
        $this->wheelwin = $data['wheelwin'];
        $this->wheeldesc = $data['wheeldesc'];
        $this->wheelwinangle = $data['wheelwinangle'];
        $this->spinsource = $data['spinsource'];

        return $data;
    }

    function notifitycondition($tracking, $booker) {
        return (preg_match("/facebook|website|remote/i", $tracking) == false && preg_match("/CHOPE|HGW|QUANDOO/i", $booker) == false);
    }

    public function getLatestBookings($type = 'weeloy', $mode = 'member') {
        $where = '';


        if ($type === 'callcenter') {
            $where .= "tracking IN('CALLCENTER','WEBSITE', 'facebook', 'GRABZ') ";
        } else {
            $where .= " tracking NOT IN('CALLCENTER','WEBSITE', 'facebook', 'GRABZ') ";
        }
        if ($mode === 'listing') {
            $where .='and is_wheelable=0';
        } else if ($mode === 'request') {
            $where .='and is_wheelable=1 and is_bookable=0';
        } else {
            $where .='and is_wheelable=1';
        }
        $sql = "SELECT booking.id ,booking.restaurant as brestaurant, confirmation, booking.pushnotify_token,language, booking.status as bstatus, restCode, membCode, salutation, lastname, firstname, booking.mobile, booking.email as member_email, smsid, rdate, rtime, cdate, cover, company, hotelguest, specialrequest, generic, booking.state as bkstate, booking.flag as flag, canceldate, title, tel, wheelwin, tracking, booker, ip, tablename, restaurant.email as res_email, address, city, zip, logo, booking.country as res_country, map, GPS, restaurant_tnc, restaurant.status as res_status, is_wheelable, is_bookable from booking, restaurant WHERE restaurant.restaurant = booking.restaurant and $where ORDER BY RAND() DESC limit 1";
        $data = pdo_single_select($sql);
        $this->getBooking($data['confirmation']);

        return 1;
    }

    function iswhitelabel($tracking) {
        return preg_match("/CALLCENTER|facebook|website/i", $tracking);
    }

    function isthirdparty($tracking, $booker, $type) {
        return (preg_match("/remote/i", $tracking) == false && preg_match("/CHOPE|HGW|QUANDOO/i", $booker) == false && preg_match("/thirdparty/i", $type) == false);
    }

    function savetableSetting($json) {

        $this->result = -1;
        $obj = json_decode($json);
        $restaurant = $obj->restaurant;
        if ($restaurant == "")
            return $this->result = -1;

        $data = $obj->data;
        foreach ($data as $row) {
            $confirmation = $row->confirmation;
            $tablename = $row->tablename;
            pdo_exec("update booking set tablename='$tablename' where restaurant='$restaurant' and confirmation='$confirmation' limit 1");
        }

        return $this->result = 1;
    }

    private function nonroman($str) {
        $limit = strlen($str);
        for ($i = 0; $i < $limit; $i++) {
            if ($i < 32 || $i > 127)  // printable roman characters
                return false;
        }
        return true;
    }

    private function createUniqConfNumer() {

        $prefix = substr($this->restaurant, 8, 5);
        $length = 7;
        //$piece = substr($this->lastname, 0, 1) . substr($this->firstname, 0, 1);
        //if ($this->nonroman($piece) && false) $prefix .= $piece;
        //else $length = 7;
        // no l, O, I
        $keyMiniscule = array("a", "b", "c", "d", "e", "f", "g", "h", "j", "k", "m", "n", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z");
        $keyMajuscule = array("A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "M", "N", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z");
        $keyNumber = array("1", "2", "3", "4", "5", "6", "7", "8", "9");
        //$keyAr = array_merge($keyMiniscule, $keyMajuscule);
        $keyAr = $keyMajuscule;

        $limit = count($keyAr) - 1;

        for ($k = 10; $k > 0; $k--) {
            $Conf = $prefix;
            for ($i = 0; $i < $length; $i++)
                $Conf .= $keyAr[rand(0, $limit)];

            if ($this->checkReservation($Conf) < 0) {  // is this reservation unique ?
                $restCode = rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9);
                $membCode = rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9);
                return array($Conf, $restCode, $membCode);
            }
        }
        return "0000";
    }

    function setPushToken($confirmation, $token) {
        $res = pdo_exec("update booking set pushnotify_token='$token' where confirmation='$confirmation' limit 1");
        if ($res === 0) {
            $res = 1;
        }
        return $res;
    }

    function correctingBookingRestaurant($restaurant) {
        $data = pdo_multiple_select("SELECT confirmation, restaurant, rdate, rtime, email, mobile, cover, salutation, firstname, lastname, country, language, specialrequest, type, tracking, booker, company, hotelguest, state, optin, booking_deposit_id, status from booking where restaurant = '$restaurant' order by cdate");
        if (count($data) <= 0)
            return -1;

        foreach ($data as $row) {
            $ret = $this->correctingBooking($row['confirmation'], $row['restaurant'], $row['rdate'], $row['rtime'], $row['email'], $row['mobile'], $row['cover'], $row['salutation'], $row['firstname'], $row['lastname'], $row['country'], $row['language'], $row['specialrequest'], $row['type'], $row['tracking'], $row['booker'], $row['company'], $row['hotelguest'], $row['state'], $row['optin'], $row['booking_deposit_id'], $row['status']);
            if ($ret != "")
                echo $ret;
        }
    }

    function correctingBooking($confirmation, $theRestaurant, $rdate, $rtime, $email, $mobile, $cover, $salutation, $firstname, $lastname, $country, $language, $specialrequest, $type, $tracking, $booker, $company, $hotelguest, $state, $optin, $booking_deposit_id, $status) {

        $argv = array('confirmation', 'theRestaurant', 'rdate', 'rtime', 'email', 'mobile', 'cover', 'salutation', 'firstname', 'lastname', 'country', 'language', 'specialrequest', 'type', 'tracking', 'booker', 'company', 'hotelguest', 'state', 'optin', 'booking_deposit_id', 'status');
        $ret = "";
        for ($i = 0; $i < count($argv); $i++) {
            $tt = $argv[$i];
            $org = $$tt;
            $$tt = $this->cleanpattern($$tt, "&nbsp;");
            if (($language == 'cn' || $language == 'hk' || $language == 'th' || $language == 'my' || $language == 'jp' || $language == 'kr' || $language == 'id' || $language == 'ru' || $language == 'vi') &&
                    ($tt == "salutation" || $tt == "firstname" || $tt == "lastname" || $tt == "specialrequest"))
                $$tt = $this->clean_input($$tt);
            else
                $$tt = $this->clean_text($$tt);
            if ($tt == "mobile")
                $$tt = $this->clean_tel($$tt);
            if ($org != $$tt) {
                pdo_exec("update booking set $tt='" . $$tt . "' where confirmation='$confirmation' limit 1");
                $ret .= "update booking set $tt='" . $$tt . "' where confirmation='$confirmation' limit 1" . ";($org)<br/>";
            }
        }
        return ($ret != "") ? $confirmation . ":" . $ret : "";
    }

    function clean($str) {

        if ($str[0] == "|")
            $str = substr($str, 1);

        if ($str[strlen($str) - 1] == "|")
            $str = substr($str, 0, strlen($str) - 1);

        $str = preg_replace("/\|\|+/", "|", $str);

        return $str;
    }
    
    function chkWaived($options){
        $isWaived = 0;
        $text_vars = preg_match("/’ccwaived’:’1’/", $options, $matches);
        $temp = array_shift( $matches );
        $k= preg_replace('/\s+/', '',$temp);
        $waived =  explode(":",$k);
        if($waived[0] == "’ccwaived’"){
            $isWaived = 1;
        }
        return $isWaived;
    }

    function getDepositDetailsByID($depositId, $bkdate, $bktime, $restaurant, $payment_method = null, $confirmation) {

        $this->result = 1;
        $data = 0;
        if (empty($depositId)) {
            $this->result = -1;
            $this->msg = "invalid depositId";
            return array();
        }
        $isPayment = 1;
        $sql = "SELECT * FROM  payment  WHERE  payment_id  = '$depositId'   limit 1 ";
        $data = pdo_single_select($sql);
        $this->getBooking($confirmation);
        $chkagflg = '10';
        $chkbendsflg = 11;
        $bendsProd = array('barseats','counterseats', 'cheftable');
        $isCancelflg = 15;

         if (($restaurant == 'SG_SG_R_BurntEnds' || $restaurant == 'SG_SG_R_TheOneKitchen')  && !in_array(trim(strtolower($this->product)), $bendsProd, true)) {
             $chkbendsflg = 13;
         }


        // if no config, load default values
        if (count($data) <= 0) {
            $isPayment = 0;
            $sql = "SELECT id,data,amount,curency as currency,receiver,paykey,object_id,card_id,status,payment_method,token FROM booking_deposit  WHERE  paykey = '$depositId' AND object_id='$confirmation' limit 1 ";
            $data = pdo_single_select($sql);
        }
        //hack philippe
        $res = new WY_restaurant();
        $res->getRestaurant($restaurant);
        $where = '';
        if($res->checkMultiProductAlllote() > 0){
            $where .= " AND product = '$this->product'";
        }
            //$amara_group = array('SG_SG_R_TheOneKitchen','SG_SG_R_SilkRoad', 'SG_SG_R_Element');
            $amara_group = array('SG_SG_R_SilkRoad', 'SG_SG_R_Element');
            if (in_array($restaurant, $amara_group, true)) {
                $chkagflg = '11';
                $generic = $this->generic;
                if(!empty($generic)) {
                    $genAr = json_decode(preg_replace("/’/", "\"", $generic), true);
                    if(isset($genAr['choice'])){
                        $prdt = $genAr['choice'];
                        $where .= " AND product = '$prdt'";
                    }
                }
    
                 
        }
        if (count($data) > 0) {
            $datetime = date('Y-m-d H:i:s', strtotime("$bkdate $bktime"));
            $interval = $this->getDateInterval($datetime, '', 'h');
            $depPer = pdo_single_select("SELECT * FROM restaurant_cancel_policy WHERE restaurant ='$restaurant'  $where order by duration ASC");
            $len = count($depPer);
            $last = $len - 1;
            $data['isPayment'] = $isPayment;
            if ($data['status'] == 'COMPLETED') {
                //if ($len > 0) {
                    $tmpVal = 0;
                         if (trim($payment_method) === 'carddetails') {

                            if($chkagflg == '11'){
                                $fixed_amount  = $this->getAmaraFixedCancelCharge($restaurant);
                                $data['refund_amount'] = $this->getAmaraClPaymentInfo($interval,$this->cover,$depPer,$bkdate, $bktime,$fixed_amount,$data['amount']);

                            }else if($restaurant == 'SG_SG_R_Nouri'){
                                $chargeamt = $interval > 5 ? ($interval < 7 ? 50 : 0) : 70; 
                                $charge = $chargeamt * $this->cover;
                                $data['refund_amount']  = $charge;
                            }
                            else{
                                if ($interval <= $depPer['duration']) {
                                    $data['refund_amount'] = $data['amount'] * ($depPer['percentage'] / 100);
                                } else if ($interval >= $depPer['duration']) {
                                    $data['refund_amount'] = $data['amount'] * ($depPer['belowperct'] / 100);
                                }
                                if($chkbendsflg == 13 && intval($interval) <= 72){
                                    $isCancelflg = 16;
                                }
                                 
                            }
                        } else {
                            if($interval >= $depPer['duration']) {
                                $data['refund_amount'] = $data['amount'] * ($depPer['belowperct'] / 100);
                                if($restaurant == 'SG_SG_R_Pollen' || $restaurant == 'SG_SG_R_Esquina'){
                                     $data['refund_amount'] = $data['refund_amount'] - 5;
                                }
                            } else if ($interval <= $depPer['duration']) {
                                $data['refund_amount'] = $data['amount'] * ($depPer['percentage'] / 100);
                            }
                        }
            }
            if ($data['status'] == 'pending_payment') {
                $data['refund_amount'] = "0";
            }
            if (isset($data['refund_amount'])) {
                $data['refund_amount'] = $data['refund_amount'];
            } else {
                $data['refund_amount'] = "0";
            }
        }
      $data['cancelflg'] = $isCancelflg;
     

        return $data;
    }

    function getAmaraClPaymentInfo($interval,$pax,$data,$rtime,$rdate,$fixed_amount,$amount){
        $tnc_amount = '0';
        $meal_type = 'lunch' ; 
        $interval = intval($interval);
        $charge =  ($meal_type == 'lunch') ? $data['lunch_charge'] : $data['dinner_charge'] ;
        if(intval($amount) > 0){
            $charge_amount = intval($amount) ;
            $service_charge = $charge_amount * 0.10;
            $gst = $charge_amount * 0.07 ;
            $charge =  $charge_amount - $service_charge - $gst;

        }
   
        if($interval <= $data['duration']){
       
            $tnc_amount = $pax * $charge * 1.10 * 1.07 ;
            $tnc_amount = round($tnc_amount, 2);
            
        }else if($interval > $data['duration'] &&  $interval < 144) {  
            $tnc_amount = $fixed_amount;
        }
        return $tnc_amount;

    }
    function getAmaraFixedCancelCharge($restaurant){
        $charge ='80';
        $depPer = pdo_single_select("SELECT  lunch_charge FROM  restaurant_cancel_policy WHERE restaurant ='$restaurant' AND product ='' ");
        if(count($depPer) >0){
           $charge = $depPer['lunch_charge']; 

        }
      
        return $charge;
    }
}
?>