<?php

class WY_stats {

   var $restaurant;
   var $sdate;
   var $edate;
   var $cover;
   var $ucover;
   var $avgcover;
   var $pricing;
   var $currency;
   var $revenue;
   var $booking;
   var $profit;
   var $avgorder;
   var $monthc;
   var $monthr;
   var $monthcbkg;
   var $monthrbkg;
   var $monthrcvr;
   var $yearcntr;
   var $yearlng;
   var $yearoffer;
   var $wheel_cn;
   
   var $msg;
   

   function __construct($theRestaurant, $sdate, $edate) {

		$this->restaurant = $theRestaurant;
		$this->sdate = $sdate;
		$this->edate = $edate;
	}
	
	function bullet() {

		$theRestaurant = $this->restaurant;
		$sdate = $this->sdate;
		$edate = $this->edate;

		$data = pdo_single_select("SELECT count(*) as total, sum(cover) as guest, avg(cover) as average from booking WHERE restaurant = '$theRestaurant' and cdate > '$sdate' and cdate < '$edate' and status != 'cancel' and status != 'noshow' and state != 'no show'");

		$this->booking = intval($data['total']);
		$this->cover = intval($data['guest']);
		$this->avgcover = floatval($data['average']);

		//$data = pdo_single_select("SELECT sum(cover) as total FROM booking WHERE restaurant = '$theRestaurant' and cdate > '$sdate' and cdate < '$edate' and generic like '%repeat%'");
		$data = pdo_single_select("SELECT sum(cover) as total FROM booking WHERE restaurant = '$theRestaurant' and cdate > '$sdate' and cdate < '$edate' and mobile != '' and status != 'cancel' and status != 'noshow' and state != 'no show' group by mobile having count(*) > 1");
		$this->ucover = intval($data['total']);

		list($this->pricing, $this->currency) = getPricing($theRestaurant);
		$this->avgorder = floor($this->pricing * $this->avgcover);
		$this->revenue = $this->pricing * $this->cover;
		$this->profit = floor($this->revenue * 0.6);
		}

	function linechart() {

		$theRestaurant = $this->restaurant;
		$sdate = $this->sdate;
		$edate = $this->edate;
		$year = '2015'; //substr($sdate, 0, 4);
		
		$data = pdo_multiple_select("SELECT MONTH(cdate), COUNT(cdate) from booking WHERE restaurant = '$theRestaurant' and cdate > '$sdate' and cdate < '$edate' GROUP BY MONTH(cdate)");	

		foreach($data as $row) {
			$this->monthc[] = $row[0];
			$this->monthcbkg[] = intval($row[1]);
			}

		//$data = pdo_multiple_select("SELECT MONTH(rdate), COUNT(rdate), sum(cover) from booking WHERE restaurant = '$theRestaurant' and rdate > '$sdate' and rdate < '$edate' and MONTH(rdate) = '1' GROUP BY MONTH(rdate)");
		$data = pdo_multiple_select("SELECT MONTH(rdate), COUNT(rdate), sum(cover) from booking WHERE YEAR(rdate) = '$year' GROUP BY MONTH(rdate)");
		
		foreach($data as $row) {
			$this->monthr[] = $row[0];
			$this->monthrbkg[] = intval($row[1]);
			$this->monthrcvr[] = intval($row[2]);
			}

		$data = pdo_multiple_select("SELECT country, count(country) from booking WHERE restaurant = '$theRestaurant' and cdate > '$sdate' and cdate < '$edate' GROUP BY country limit 6");
		
		$country = array("SG" => 30, "TH" => 20, "MY" => 10, "ID" => 10, "CN" => 15, "JP" => 5);
		foreach($country as $key => $value) 
			$this->yearcntr[$key] = $value;

/*
		foreach($data as $row) 
			$this->yearcntr[$row[0]] = intval($row[1]);
*/
			
		$data = pdo_multiple_select("SELECT language, count(language) from booking WHERE restaurant = '$theRestaurant' and cdate > '$sdate' and cdate < '$edate' GROUP BY language limit 6");
		
		$langue = array("English" => 30, "Chinese" => 20, "Malay" => 10, "Basha" => 10, "Japanese" => 15, "French" => 5);
		foreach($langue as $key => $value) 
			$this->yearlng[$key] = $value;

/*
		foreach($data as $row)
			$this->yearlng[$row[0]] = intval($row[1]);
*/

		$data = pdo_single_select("SELECT count(wheelwin) from booking WHERE restaurant = '$theRestaurant' and cdate > '$sdate' and cdate < '$edate' and wheelwin != ''");
		$this->wheel_cn = intval($data[0]);
		
		$data = pdo_multiple_select("SELECT wheelwin, count(wheelwin) from booking WHERE restaurant = '$theRestaurant' and cdate > '$sdate' and cdate < '$edate' and wheelwin != '' GROUP BY wheelwin limit 9");
		
		foreach($data as $row) {
			$ext = sprintf("(%2.0f%%)", (floatval($row[1]) / $this->wheel_cn) * 100);
			$this->yearoffer[$row[0] . $ext] = intval($row[1]);
			}

		}

	function bookinglinechart() {
		$theRestaurant = $this->restaurant;
		$sdate = $this->sdate;
		$edate = $this->edate;

		return pdo_multiple_select("SELECT confirmation, MONTH(rdate) as rmonth, cdate, rtime, cover, tracking, status, language, country from booking WHERE restaurant = '$theRestaurant' and cdate > '$sdate' and cdate < '$edate' order by rmonth");
		}
				
	function minmax($data) {
	
		$min = 999999999;
		$max = 0;
		for($i = 0; $i < count($data); $i++) {
			if($data[$i] > $max) $max = $data[$i];
			if($data[$i] < $min) $min = $data[$i];
			}
		return array($min, $max);	
		}
}

?>
