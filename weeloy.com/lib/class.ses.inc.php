<?php

require_once 'lib/composer/vendor/autoload.php';

use Aws\Ses\SesClient;

class SES {

    private $client = '';
    private $queue = '';
    private $queueUrl = '';
    private $body = '';
    private $attributes = [];

    public function __construct() {
        $this->client = SesClient::factory(array(
                    'key' => awsAccessKey,
                    'secret' => awsSecretKey,
                    'region' => 'us-west-2'
        ));
    }
    
    public function verifyEmailAddress($email) {
        $args['EmailAddress'] = $email;
        return $this->client->verifyEmailAddress($args); 
    }

    public function deleteVerifiedEmailAddress($args) {
        //args should be $args['EmailAddress'] = 'phil.benedetti@gmail.com';
        return $this->client->deleteVerifiedEmailAddress($args);
    }
    
    public function listVerifiedEmailAddresses() {
        $res = $this->client->listVerifiedEmailAddresses();
        return $res['VerifiedEmailAddresses'];
    }
    
    public function listIdentities(){
        return $this->client->listIdentities();
    }
    
    public function getIdentityVerificationAttributes($args){
        //args should be $args['Identities'] = array('phil.benedetti@gmail.com', 'gaolinch@hotmail.com');
        return $this->client->getIdentityVerificationAttributes($args);
    }
    
    public function getIdentityVerificationStatus($email){
        $args['Identities'] = array($email);
        $res = $this->client->getIdentityVerificationAttributes($args);
        
        if(!isset($res['VerificationAttributes']) || count($res['VerificationAttributes']) < 1){
            return false;
        }
        
        foreach ($res['VerificationAttributes'] as $r){
            return $r['VerificationStatus'];
        }
    }
}
