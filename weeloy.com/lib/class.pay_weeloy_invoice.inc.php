<?php

require_once 'lib/class.payment_paypal.inc.php';

class WY_Pay_invoice {

    public $pay_object = NULL;
    public $mode = 'test';
    public $amount = 0;
    public $controller ;
    public $paymentSource;
    var $result;
    var $msg = "";

    function __construct($payment) {
        $this->restaurant = $payment['restaurant'];
        $this->amount = $payment['amount'];
        $this->controller = $payment['controller'];
        $this->paymentSource = $payment['source'];
 
        switch ($this->controller) {
            case 'paypal':
                $this->pay_object = new WY_Payment_Paypal($this->restaurant, $this->amount,$payment['return_url']);
                break;
            case 'stripe':
                break;
            default:
                return $this->result = -1;
        }
        return $this->result = 1;
    }
    
    
    public function  doPayment(){
       return $this->pay_object->makePay();
    }


}
