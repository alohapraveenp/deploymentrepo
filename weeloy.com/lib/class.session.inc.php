<?php

class Sessions {

    function __construct() {
    }

    public function on_session_start($save_path, $session_name) {
        //error_log('[NO_ERROR] ' . $session_name . " [NO_ERROR] " . session_id());
        return TRUE;
    }

    function on_session_end() {
        // Nothing needs to be done in this function
        // since we used persistent connection.
        return TRUE;
    }

    function on_session_read($key) {
        //error_log('[NO_ERROR]' . $key);
        $sql = "select session_data from sessions ";
        $sql .= "where session_id ='$key' ";
        $sql .= "and timestamp(session_expiration) > timestamp(now())";

        try {
            $db = getConnection('session');
            if ($stmt = $db->query($sql)) {
                $data = $stmt->fetch(PDO::FETCH_OBJ);
                //error_log(printf($data)); 
                //error_log($sql); 
                if(count($data)>0 && $data !== false){
                    $data_session = get_object_vars($data);
                    return $data_session['session_data'];
                }
                return FALSE;
            }
        } catch (PDOException $e) {
            return $stmt;
        }
        return FALSE;
    }

    function on_session_write($key, $val) {
        $val = addslashes($val);
        $insert_sql = "insert into sessions values('$key', ";
        $insert_sql .= "'$val', timestamp(DATE_ADD(NOW(), INTERVAL 90 MINUTE))) ON DUPLICATE KEY UPDATE ";
        $insert_sql .= "session_data ='$val', session_expiration = timestamp(DATE_ADD(NOW(), INTERVAL 90 MINUTE))";

        // First we try to insert, if that doesn't succeed, it means
        // session is already in the table and we try to update
        try {
            $db = getConnection('session', null, "write");
            $stmt = $db->query($insert_sql);
            if($stmt->rowCount()){
                return TRUE;
            }else{
                return FALSE;
            }
            //return $stmt->rowCount();
        } catch (PDOException $e) {
            echo '{"error":{"text":' . __FILE__." ".__FUNCTION__." ".__LINE__." ".$e->getMessage() . '}}';
            return FALSE;
        }
        
    }

    function on_session_destroy($key) {
        $sql = "delete from sessions where session_id = '$key'";
        try {
            $db = getConnection('session', null, "write");
            $affected_rows = $db->exec($sql);
        } catch (PDOException $e) {
            echo '{"error":{"text":' . __FILE__." ".__FUNCTION__." ".__LINE__." ".$e->getMessage() . '}}';
            exit;
        }
        $this->on_session_gc();
        //return $affected_rows;
        return TRUE;
    }

    function on_session_gc() {
        //error_log("[NO_ERROR] on_session_gc = $value");
        $sql = "delete from sessions where timestamp(session_expiration) < timestamp(now())";
        try {
            $db = getConnection('session', null, "write");
            $affected_rows = $db->exec($sql);
        } catch (PDOException $e) {
            echo '{"error":{"text":' . __FILE__." ".__FUNCTION__." ".__LINE__." ".$e->getMessage() . '}}';
            exit;
        }
        return TRUE;
    }
}
?>