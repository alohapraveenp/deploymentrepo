<?php

class translate {

    private $db;
    private $zone;

//	private static $instance;

    function __construct($lang, $arr_page = null) {
        $this->db = getConnection();
        //$this->lang_dir = 'lang/';
        $this->lang = $lang;
        $this->add_zone('global');
        //$this->add_zone('notif');
        //$this->add_zone('preset');

        if (is_array($arr_page)) {
            $this->add_zone($arr_page['name']);
            $this->add_zone($arr_page['lang_files']);
        }
        
        // MIGHT BE WRONG to be verified
        if ($_SESSION['user']['member_type'] != 'visitor') {
            //$this->add_zone('myaccount');
        }
    }

    function add_translation($zone, $elem, $lang, $content) {
        
    }

    function remove_translation($elem) {
        
    }

    function modify_translation($zone, $elem, $lang, $content) {
        
    }

    function load($lang = null) {
        $schlang = '';
        foreach ($this->zone as $key) {
            $schlang.= " zone = '" . $key . "' OR ";
        }
        $schlang = substr($schlang, 0, strlen($schlang) - 3);
        $Sql = "SELECT * FROM translation WHERE ( " . $schlang . ")	";
        if (isset($lang)) {
            $Sql .= " AND lang = '" . $this->lang . "'  ";
        }
        $stmt = $this->db->query($Sql);

        if ($stmt->execute()) {
            while ($Data = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $trsl[$Data['lang']][$Data['elem_name']] = $Data['content'];
            }
        }

        if (isset($trsl)) {
            return $trsl;
        }

        return false;
    }

    function load_zone($zone = home, $lang = null) {

        if (!isset($zone) || $zone == "") {
            $zone = 'home';
        }

        $Sql = "SELECT * FROM translations WHERE zone=:zone";
        $stmt = $this->db->dbh->prepare($Sql);

        if ($stmt->execute(array(':zone' => $zone))) {
            while ($Data = $stmt->fetch(PDO::FETCH_ASSOC)) {

                $trsl[$Data['lang']][$Data['elem_name']] = $Data['content'];
            }
        }

        if (isset($trsl)) {
            return $trsl;
        }

        return false;
    }

    function load_zone_list() {

        $Sql = "SELECT DISTINCT(zone) FROM translations  ";
        $stmt = $this->db->dbh->query($Sql);
        $Out = array();
        if ($stmt->execute()) {
            while ($Data = $stmt->fetch(PDO::FETCH_OBJ)) {
                array_push($Out, $Data->zone);
            }
        }

        return $Out;
    }

    function set_lang($lang) {
        $this->lang = $lang;
    }

    function reset_zone() {
        unset($this->zone);
    }

    function add_zone($zone) {

        if (is_array($zone)) {
            foreach ($zone as $nzone) {
                $this->zone[] = $nzone;
            }
        } else {
            $this->zone[] = $zone;
        }

        return true;
    }

}

?>