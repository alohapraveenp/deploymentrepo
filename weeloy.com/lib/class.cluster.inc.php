<?php

$cluster_type = array("MASTER", "SLAVE");

class WY_Cluster {

    var $clustname;
    var $clustparent;
    var $clusttype;
    var $clustcategory;
    var $clustcontent;
    var $msg;
    var $result;
    public $full_object = array();

    function __construct() {
        $this->clustname = $this->clustparent = $this->clusttype = $this->clustcategory = $this->clustcontent = array();
        $this->msg = "";
    }

    function error_msg($val, $msg) {
        error_log("SET CLUSTER MSG " . $msg);
        $this->msg = $msg;
        return $this->result = $val;
    }

    function create($type, $name, $category, $parent, $content) {

        global $cluster_type;

        if (empty($type) || !in_array($type, $cluster_type))
            return $this->result = $this->msg = -3;

        if ($type == "MASTER") {
            if ($parent != "")
                return $this->error_msg(-1, "Master cluster cannot not have parent -$parent-");

            $data = pdo_single_select("SELECT clustname from cluster WHERE clusttype = 'MASTER' and clustname = '$name' and clustcategory = '$category'  limit 1");
            if (count($data) > 0)
                return $this->error_msg(-1, "Master cluster $name for $category already exists");
            pdo_insert("INSERT INTO cluster(clusttype, clustname, clustparent, clustcategory, clustcontent) VALUES ('$type', '$name', '$parent', '$category', '$content')");
            $this->msg = "";
            return 1;
        }

        if ($type == "SLAVE") {
            $data = pdo_single_select("SELECT clustname from cluster WHERE clusttype = 'MASTER' and clustname = '$parent' and clustcategory = '$category'  limit 1");
            if (count($data) <= 0)
                return $this->error_msg(-1, "Master cluster $name for $category does NOT exists");

            $data = pdo_single_select("SELECT clustname from cluster WHERE clusttype = 'SLAVE' and clustname = '$name' and clustparent = '$parent' and clustcategory = '$category' limit 1");
            if (count($data) > 0)
                return $this->error_msg(-1, "/Slave cluster $name for $category already exists");

            pdo_insert("INSERT INTO cluster(clusttype, clustname, clustparent, clustcategory, clustcontent) VALUES ('$type', '$name', '$parent', '$category', '$content')");
            $this->msg = "";
            return 1;
        }
    }

    function reademail() {
    	if($this->read("SLAVE", "", "", "", "") > 0)
    		return $this->full_object;
    	}
    	
    function read($type, $name, $category, $parent, $content, $orderBy = "") {

        global $cluster_type;

        $qryAr = array("type", "name", "category", "parent", "content");

        if (!empty($type) && !in_array($type, $cluster_type))
            $type = "";

        $queryWhere = $sep = "";
        foreach ($qryAr as $keyval)
            if (!empty($$keyval)) {
                $queryWhere .= $sep . "clust" . $keyval . "='" . $$keyval . "'";
                $sep = " and ";
            }

        if (empty($queryWhere))
            $this->error_msg(-1, "Invalid empty query");

        $this->clustname = $this->clustcategory = $this->clustcontent = $this->clusttype = $this->clustparent = array();

        $data = pdo_multiple_select("SELECT clusttype, clustname, clustparent, clustcategory, clustcontent from cluster WHERE $queryWhere $orderBy");
        if (count($data) < 1)
            return $this->result = -1;

        foreach ($data as $row) {
            $this->clustname[] = $row['clustname'];
            $this->clustcategory[] = $row['clustcategory'];
            $this->clustcontent[] = $row['clustcontent'];
            $this->clusttype[] = $row['clusttype'];
            $this->clustparent[] = $row['clustparent'];
        }
        $this->full_object = $data;
        return $this->result = $this->msg = 1;
    }

    function delete($type, $name, $category) {

        global $cluster_type;

        if (empty($type) || !in_array($type, $cluster_type) || empty($category))
            return $this->result = $this->msg = -3;

        if ($type == 'MASTER') { // delete the entire cluster
            pdo_exec("delete from cluster WHERE clustname = '$name' and clustcategory = '$category' limit 1");
            pdo_exec("delete from cluster WHERE clustparent = '$name' and clustcategory = '$category'");
        } else
            pdo_exec("delete from cluster WHERE clusttype = '$type' and clustname = '$name' and clustcategory = '$category' limit 1");

        return $this->result = $this->msg = 1;
    }

    function update($type, $name, $category, $parent, $content) {

        global $cluster_type;

        if (empty($type) || !in_array($type, $cluster_type) || empty($category)) {
            $this->msg = "$type, $name, $parent, $content";
            return $this->result = -1;
        }

        pdo_exec("update cluster set clustname = '$name', clustcontent = '$content', clustparent = '$parent' WHERE clusttype = '$type' and clustname = '$name' and clustcategory = '$category' limit 1");

        return $this->result = $this->msg = 1;
    }

    function getCluster($type, $name, $category, $parent) {
        global $cluster_type;

        if (empty($type) || !in_array($type, $cluster_type))
            $type = "MASTER";

        if ($type == "MASTER")
            $parent = '';

        $data = array();
        $this->read($type, $name, $category, $parent, "", "");
        if ($this->result > 0) {
            $data['clustname'] = $this->clustname[0];
            $data['clustcategory'] = $this->clustcategory[0];
            $data['clusttype'] = $this->clusttype[0];
            $data['clustparent'] = $this->clustparent[0];
            $data['clustcontent'] = $this->clustcontent[0];
        }
        return $data;
    }

    function getCategory() {
        $data = pdo_multiple_select_index("SELECT distinct clustcategory from cluster order by clustcategory");
        return $data;
    }

    function getParent() {
        $data = pdo_multiple_select_index("SELECT distinct clustname from cluster where clusttype = 'MASTER' order by clustcategory");
        return $data;
    }

    function getListMasterCluster() {

        global $cluster_type;

        return $this->read("MASTER", "", "", "", "", "order by clustcategory");
    }

    function getListSlaveCluster($cname, $category) {

        global $cluster_type;

        if (empty($cname) || empty($category))
            return $this->result = -1;

        return $this->read("SLAVE", "", $category, $cname, "", "order by clustname"); // parent = cname
    }

    function getListSlaveClusterByObject($cname, $category) {

        global $cluster_type;

        if (empty($cname) || empty($category))
            return $this->result = -1;

        return $this->read("SLAVE", "", $category, $cname, "", "order by clustname"); // parent = cname
    }

    public function getParentClusterAdditionalInformation($cname, $category) {
        $sql = "SELECT cluster_parent, cluster_category, cluster_name, info_key, info_value FROM cluster_additional_info WHERE cluster_parent = '$cname' AND cluster_category = '$category'";
        $data = pdo_multiple_select($sql);
        $info = array();
        foreach ($data as $d) {
            $info[$d['info_key']] = $d['info_value'];
        }
        $res['cluster_parent'] = $data['cluster_parent'];
        $res['cluster_category'] = $data['cluster_category'];
        $res['cluster_name'] = $data['cluster_name'];
        $res['info'] = $info;
        return $res;
    }

	static public function getClusterContent($name, $parent) {
        $data = pdo_single_select("SELECT clustcontent FROM cluster WHERE clustname = '$name' and clustparent = '$parent' limit 1");
        return (!empty($data["clustcontent"])) ? $data["clustcontent"] : "";
		}
		
}

?>
