<?php

/**
 * 	Richard Kefs (c) 2014
 *
 * 	Contains all classes definition for login session.
 *
 */
require_once("conf/conf.init.inc.php");
require_once("lib/wpdo.inc.php");
require_once("lib/class.async.inc.php");
require_once 'conf/conf.s3.inc.php';
require_once 'conf/conf.sqs.inc.php';
require_once("lib/class.restaurant.inc.php");
require_once("lib/class.analytics.inc.php");

class WY_Spool {

    private $s3 = NULL;
    private $sqs = NULL;
    private $db = NULL;
    private $bucket = SPOOL_HISTORY_BUCKET;
    private $queue = SPOOL_HISTORY_QUEUE;

    function __construct() {
        
    }

    function register_mail($recipient, $subject, $body, $opts, $restaurant = NULL, $booking_id = NULL) {

        if ($this->is_test_env()) {
            $parts = explode('@', $recipient);
            // Stick the @ back onto the domain since it was chopped off.

            $domain = $parts[1];
            if ($domain == 'weeloy.com' || $recipient == 'phil.benedetti@gmail.com') {
                $recipient = $recipient;
            } else {
                //$recipient = "vs.kala@weeloy.com";
                $recipient = "support@weeloy.com";
            }
        }

        $data[] = $recipient;
        $data[] = $subject;
        $data[] = $body;
        $data[] = urlencode(serialize($opts));
        $datastring = implode("||||", $data);
        $datastring = preg_replace("/\'|\"/", "’", $datastring); //\xc2\xb4
        //$random = rand(0, 9999999);
        $random = uniqid();

        pdo_exec("INSERT INTO spool (type, timestamp, status, data, restaurant, recipient, booking_id) VALUES ('MAIL', NOW(), '$random', '$datastring', '$restaurant', '$recipient', '$booking_id')");

        if (strpos($_SERVER['HTTP_HOST'], 'localhost') !== false) {
            $protocol = "http://";
        } else {
            $protocol = "https://";
        }


        $url = $protocol . $_SERVER['HTTP_HOST'] . __ROOTDIR__ . "/modules/mailspool/mailspool.php?mailid=$random";

        if (strpos($_SERVER['HTTP_HOST'], 'api') !== false || strpos($_SERVER['HTTP_HOST'], 'preprod.ap-southeast-1.elasticbeanstalk.com') !== false) {
            if (strpos($_SERVER['HTTP_HOST'], 'dev') !== false) {
                $url = "https://dev.weeloy.com/modules/mailspool/mailspool.php?mailid=$random";
            } else {
                $url = "https://www.weeloy.com/modules/mailspool/mailspool.php?mailid=$random";
            }
        }
        $async = new WY_Async;
        return $async->async_curl($url);
    }

    function register_sms($recipient, $subject, $body, $opts, $expeditor, $restaurant = NULL, $booking_id = NULL, $type = 'SMSP', $provider = 'textmagic', $isPaid = 0) {

        if ($this->is_test_env()) {
            $recipient = str_replace(' ', '', $recipient);
            if (!preg_match("/6597570508|6582452082|6596542259/i", $recipient)) {
                return true;
            }
        }
        $db_history = getConnection('dwh');
        $data[] = $recipient;
        $data[] = $subject;
        $data[] = $body;
        $data[] = urlencode(serialize($opts));
        $data[] = $expeditor;
        $data[] = $type;
        $data[] = $provider;
        $datastring = implode("||||", $data);
        $datastring = preg_replace("/\’|\"/", " ", $datastring); //\xc2\xb4
        //$datastring = urlencode($datastring);
        //$random = rand(0, 9999999);
        $random = uniqid();


        pdo_exec("INSERT INTO spool (type, timestamp, status, data, restaurant, recipient, booking_id, provider) VALUES ('SMS', NOW(), '$random', '$datastring', '$restaurant', '$recipient', '$booking_id', '$provider')");
        pdo_exec("INSERT INTO sms_tracking_invoice (restaurant,booking_id,data_id, paid_sms,type,provider) VALUES ('$restaurant', '$booking_id','$random',$isPaid,'$type','$provider')",'dwh');
        $protocol = 'https://';
        if (strpos($_SERVER['HTTP_HOST'], 'localhost') !== false) {
            $protocol = 'http://';
        }

        $url = $protocol . $_SERVER['HTTP_HOST'] . __ROOTDIR__ . "/modules/mailspool/smsspool.php?mailid=$random";

        if (strpos($_SERVER['HTTP_HOST'], 'api') !== false || strpos($_SERVER['HTTP_HOST'], 'preprod.ap-southeast-1.elasticbeanstalk.com') !== false) {
            if (strpos($_SERVER['HTTP_HOST'], 'dev') !== false) {
                $url = "https://dev.weeloy.com/modules/mailspool/smsspool.php?mailid=$random";
            } else {
                $url = "https://www.weeloy.com/modules/mailspool/smsspool.php?mailid=$random";
            }
        }
        if (strpos($_SERVER['HTTP_HOST'], 'localhost') !== false) {
            $url = __BASE_URL__ . "/modules/mailspool/smsspool.php?mailid=$random";
        }


        $async = new WY_Async;
        $async->async_curl($url);
    }

    function register_pushnotif($pn_type, $recipient, $subject, $body, $opts, $app_name, $restaurant = NULL, $booking_id = NULL) {
//        if ($this->is_test_env()) {
//            //return true;
//            $recipient = "";
//        }
        $data[] = $pn_type;
        $data[] = $recipient;
        $data[] = $subject;
        $data[] = $body;
        $data[] = urlencode(serialize($opts));
        $data[] = $app_name;
        $datastring = implode("||||", $data);
        $datastring = preg_replace("/\'|\"/", "’", $datastring); //\xc2\xb4
        //$random = rand(0, 9999999);
        $random = uniqid();
        pdo_exec("INSERT INTO spool (type, timestamp, status, data, restaurant, recipient, booking_id) VALUES ('PUSHNOTIF', NOW(), '$random', '$datastring', '$restaurant', '$recipient', '$booking_id')");

        $url = "https://" . $_SERVER['HTTP_HOST'] . __ROOTDIR__ . "/modules/mailspool/pnspool.php?mailid=$random";

        if (strpos($_SERVER['HTTP_HOST'], 'api') !== false || strpos($_SERVER['HTTP_HOST'], 'preprod.ap-southeast-1.elasticbeanstalk.com') !== false) {
            if (strpos($_SERVER['HTTP_HOST'], 'dev') !== false) {
                $url = "https://dev.weeloy.com/modules/mailspool/pnspool.php?mailid=$random";
            } else {
                $url = "https://www.weeloy.com/modules/mailspool/pnspool.php?mailid=$random";
            }
        }
        if (strpos($_SERVER['HTTP_HOST'], 'localhost') !== false) {
            $url = __BASE_URL__ . "/modules/mailspool/pnspool.php?mailid=$random";
        }

        $async = new WY_Async;
        $async->async_curl($url);
    }

    function register_replication($replication_data, $replication_type) {

        $random = uniqid();

        $data[] = urlencode(serialize($replication_data));
        $datastring = implode("||||", $data);
        // $datastring = preg_replace("/\'|\"/", "’", $datastring); //\xc2\xb4

        pdo_exec("INSERT INTO spool (type, timestamp, status, data, restaurant, recipient, booking_id) VALUES ('REPLICATION', NOW(), '$random', '$datastring', '$replication_data->restaurant', '$replication_type', '$replication_data->confirmation')");

        if (strpos($_SERVER['HTTP_HOST'], 'localhost') !== false) {
            $protocol = "http://";
        } else {
            $protocol = "https://";
        }

        $url = $protocol . $_SERVER['HTTP_HOST'] . __ROOTDIR__ . "/modules/mailspool/replication.php?statusId=$random";

        if (strpos($_SERVER['HTTP_HOST'], 'api') !== false || strpos($_SERVER['HTTP_HOST'], 'preprod.ap-southeast-1.elasticbeanstalk.com') !== false) {
            if (strpos($_SERVER['HTTP_HOST'], 'dev') !== false) {
                $url = "https://dev.weeloy.com/modules/mailspool/replication.php?statusId=$random";
            } else {
                $url = "https://www.weeloy.com/modules/mailspool/replication.php?statusId=$random";
            }
        }
        $async = new WY_Async;
        return $async->async_curl($url);
    }

    function execute_mail($mailid) {

        $db_history = getConnection('dwh',NULL,'write');
        $mailer = new JM_Mail;
        if ($mailid != "") {
            $data = pdo_single_select("select ID, type, data, timestamp, restaurant, recipient, booking_id from spool where type = 'MAIL' and status = '$mailid'  limit 1");
            if (count($data) > 0) {
                $id = $this->senddata($mailer, $data);
                $unique_id = $this->saveContentToS3($data['ID'], $data['data'], $data['timestamp']);
                pdo_exec("INSERT INTO spool_history (type, timestamp, status, restaurant, recipient, booking_id,  data, sent_on) VALUES ('MAIL', '$data[timestamp]', '$mailid', '$data[restaurant]', '$data[recipient]', '$data[booking_id]', '$unique_id', NOW())",'dwh');
                //$db_history->exec("INSERT INTO spool_history (type, timestamp, status, restaurant, recipient, booking_id,  data, sent_on) VALUES ('MAIL', '$data[timestamp]', '$mailid', '$data[restaurant]', '$data[recipient]', '$data[booking_id]', '$unique_id', NOW())");

                pdo_exec("delete from spool where type = 'MAIL' and ID = '$id' limit 1");
            }
        }
    }

    function execute_mail_backlog() {
        $db_history = getConnection('dwh');
        $mailer = new JM_Mail;
        // search for mail that were not sent !!!
        $data = pdo_multiple_select("select ID, type, data, status, restaurant, recipient, booking_id, timestamp, try from spool where type = 'MAIL' and DATE_SUB(CURRENT_TIMESTAMP(), INTERVAL 5 MINUTE) > timestamp ORDER BY `try` ASC");

        if (count($data) > 0) {
            foreach ($data as $row) {
                $try = intval($row['try']);
                if($try % 10 == 0 && $try > 2){
                    $email_header['from'] = array('support@weeloy.com'=>'Weeloy Support');
                    $email_header['to'] = 'philippe.benedetti@weeloy.com';
                    
                    //$this->register_mail('philippe.benedetti@weeloy.com', 'Weeloy WARNING - EMAIL stuck in spool', 'Weeloy WARNING - EMAIL stuck in spool', $email_header);
                    if(empty($row['recipient'])) {
                    	pdo_exec("INSERT INTO spool_bug (type, timestamp, status, restaurant, recipient, booking_id, data) VALUES ('MAIL', '$row[timestamp]', '$row[status]', '$row[restaurant]', '$row[recipient]', '$row[booking_id]', '$row[data]')");
	                    pdo_exec("delete from spool where type = 'MAIL' and ID = '" . $row['ID'] . "' limit 1");
                    	continue;
                    	}
                }
                
                pdo_exec("UPDATE spool SET `try` = `try` + 1 WHERE id = $row[ID]");

                $ID = $this->senddata($mailer, $row);
                if ($ID) {
                    $unique_id = $this->saveContentToS3($row['ID'], $row['data'], $row['timestamp']);
                    pdo_exec("INSERT INTO spool_history (type, timestamp, status, restaurant, recipient, booking_id,data, sent_on) VALUES ('MAIL', '$row[timestamp]', '$row[status]', '$row[restaurant]', '$row[recipient]', '$row[booking_id]', '$unique_id', NOW())",'dwh');                                   
                    //$db_history->exec("INSERT INTO spool_history (type, timestamp, status, restaurant, recipient, booking_id,data, sent_on) VALUES ('MAIL', '$row[timestamp]', '$row[status]', '$row[restaurant]', '$row[recipient]', '$row[booking_id]', '$unique_id', NOW())");                                   
                    pdo_exec("delete from spool where type = 'MAIL' and ID = '" . $ID . "' limit 1");
                }
            }
        }
    }

    function execute_sms($mailid) {
        $db_history = getConnection('dwh');
        $sms_mailer = new JM_Sms();

        if ($mailid != "") {
            $data = pdo_single_select("select ID, type, data, timestamp, restaurant, recipient, booking_id, provider from spool where type = 'SMS' and status = '$mailid' limit 1");

            if (count($data) > 0) {
                pdo_exec("INSERT INTO sms_status_history (ID, sent, reference, received, recipient, statuscode, errorcode, errordescription, message_id,booking_id,reply_message,reply_datetime,last_update) VALUES (NULL, '', '$mailid', '', '', '', '', '','','$data[booking_id]','',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);",'dwh');

                //$db_history->exec("INSERT INTO sms_status_history (ID, sent, reference, received, recipient, statuscode, errorcode, errordescription, message_id,booking_id,reply_message,reply_datetime,last_update) VALUES (NULL, '', '$mailid', '', '', '', '', '','','$data[booking_id]','',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);");

                $id = $this->sendsms($sms_mailer, $data, $mailid, 'direct');
                $unique_id = $this->saveContentToS3($data['ID'], $data['data'], $data['timestamp']);
                 pdo_exec("INSERT INTO spool_history (type, timestamp, status, restaurant, recipient, booking_id, provider, data, sent_on) VALUES ('SMS', '$data[timestamp]','$mailid', '$data[restaurant]', '$data[recipient]', '$data[booking_id]', '$data[provider]', '$unique_id', NOW())",'dwh');
                //$db_history->exec("INSERT INTO spool_history (type, timestamp, status, restaurant, recipient, booking_id, provider, data, sent_on) VALUES ('SMS', '$data[timestamp]','$mailid', '$data[restaurant]', '$data[recipient]', '$data[booking_id]', '$data[provider]', '$unique_id', NOW())");

                pdo_exec("delete from spool where type = 'SMS' and ID = '$id' limit 1");
            }
        }
    }

    function execute_sms_backlog() {
        $db_history = getConnection('dwh');
        $sms_mailer = new JM_Sms();
        // search for sms that were not sent !!!
        $data = pdo_multiple_select("select ID, type, data, status, timestamp, restaurant, recipient, booking_id from spool where type = 'SMS' and DATE_SUB(CURRENT_TIMESTAMP(), INTERVAL 5 MINUTE) > timestamp ORDER BY `try` ASC");
        if (count($data) > 0) {
            foreach ($data as $row) {
                   
                $sql = "UPDATE spool SET  `try` = `try` + 1 WHERE id = $row[ID]";
                pdo_exec($sql);
                
                $ID = $this->sendsms($sms_mailer, $row, '', 'backlog');

                if ($ID) {
                    $unique_id = $this->saveContentToS3($row['ID'], $row['data'], $row['timestamp']);
                     pdo_exec("INSERT INTO spool_history (type, timestamp, status, restaurant, recipient, booking_id,data, sent_on) VALUES ('SMS', '$row[timestamp]', '$row[status]', '$row[restaurant]', '$row[recipient]', '$row[booking_id]', '$unique_id', NOW())",'dwh');
                    //$db_history->exec("INSERT INTO spool_history (type, timestamp, status, restaurant, recipient, booking_id,data, sent_on) VALUES ('SMS', '$row[timestamp]', '$row[status]', '$row[restaurant]', '$row[recipient]', '$row[booking_id]', '$unique_id', NOW())");
                    pdo_exec("delete from spool where type = 'SMS' and ID = '" . $row['ID'] . "' limit 1");
                }
            }
        }
    }

    function execute_pushnotif($mailid) {

        $db_history = getConnection('dwh');

        $pn_mailer = new JM_Pushnotif();

        if ($mailid != "") {
            $data = pdo_single_select("select ID, type, data,restaurant, recipient, booking_id ,timestamp from spool where type = 'PUSHNOTIF' and status = '$mailid' limit 1");

            if (count($data) > 0) {
                $id = $this->sendpushnotif($pn_mailer, $data);
                $unique_id = $this->saveContentToS3($data['ID'], $data['data'], $data['timestamp']);
                pdo_exec("INSERT INTO spool_history (type, timestamp, status, restaurant, recipient, booking_id, data, sent_on) VALUES ('PUSHNOTIF', '$data[timestamp]', '$mailid', '$data[restaurant]', '$data[recipient]', '$data[booking_id]', '$unique_id', NOW())",'dwh');
                //$db_history->exec("INSERT INTO spool_history (type, timestamp, status, restaurant, recipient, booking_id, data, sent_on) VALUES ('PUSHNOTIF', '$data[timestamp]', '$mailid', '$data[restaurant]', '$data[recipient]', '$data[booking_id]', '$unique_id', NOW())");
                pdo_exec("delete from spool where type = 'PUSHNOTIF' and ID = '$id' limit 1");
            }
        }
    }

    function execute_pushnotif_back_log() {
        $db_history = getConnection('dwh');
        $pn_mailer = new JM_Pushnotif();
        // search for sms that were not sent !!!
        $data = pdo_multiple_select("select ID, type, data, status, timestamp, restaurant, recipient, booking_id from spool where type = 'PUSHNOTIF' and DATE_SUB(CURRENT_TIMESTAMP(), INTERVAL 5 MINUTE) > timestamp ORDER BY `try` ASC");
        if (count($data) > 0) {
            foreach ($data as $row) {
                                
                $sql = "UPDATE spool SET  `try` = `try` + 1 WHERE id = $row[ID]";
                pdo_exec($sql);
                
                $ID = $this->sendpushnotif($pn_mailer, $row);
                if ($ID) {
                    $unique_id = $this->saveContentToS3($row['ID'], $row['data'], $row['timestamp']);
                    pdo_exec("INSERT INTO spool_history (type, timestamp, status, restaurant, recipient, booking_id, data, sent_on) VALUES ('PUSHNOTIF', '$row[timestamp]', '$row[status]', '$row[restaurant]', '$row[recipient]', '$row[booking_id]',  '$unique_id', NOW())",'dwh');
                    
                    //$db_history->exec("INSERT INTO spool_history (type, timestamp, status, restaurant, recipient, booking_id, data, sent_on) VALUES ('PUSHNOTIF', '$row[timestamp]', '$row[status]', '$row[restaurant]', '$row[recipient]', '$row[booking_id]',  '$unique_id', NOW())");
                    pdo_exec("delete from spool where type = 'PUSHNOTIF' and ID = '" . $ID . "' limit 1");
                }
            }
        }
    }

    function senddata($mailer, $row) {
        $id = $row['ID'];
        $datastring = $row['data'];
        $restaurant = $row['restaurant'];

        $data = explode("||||", $datastring);
        $recipient = $data[0];

        $subject = $data[1];
        $body = $data[2];
        $opts = unserialize(urldecode($data[3]));

        $value = reset($opts['from']);

        $res = '';
        
        if (!empty($value) && strpos($value, 'emailwhitelabel') !== false) {
            
            list($key, $val) = each($opts['from']);
            
            $val = str_replace("|emailwhitelabel", "", $val);
            $opts['from'] = array($key => $val);
            
            $res = WY_restaurant::getAWSEmailRestaurantStatus($restaurant);
            
            if($res == 'verified'){
                $response = $mailer->send_fromwhitelabelAws($recipient, $subject, $body, $opts);
            }else{
                $response = $mailer->send_fromswiftmailer($recipient, $subject, $body, $opts);
                //$response = $mailer->send_fromwhitelabel($recipient, $subject, $body, $opts);
            }
            
            return $id;
        } 
        else if (strstr($_SERVER['HTTP_HOST'], 'localhost')) {
            $response = $mailer->send_fromlocalhost($recipient, $subject, $body, $opts);
            return $id;
        }

        $response = $mailer->send_fromswiftmailer($recipient, $subject, $body, $opts);
        if ($response) {
            if (isset($opts['guest_qrcode'])) {
                unlink($opts['guest_qrcode']);
            }
        } else {
            
        }
        return $id;
    }

    function sendsms($mailer, $row, $mailid = '', $source_test) {
        $id = $row['ID'];
        $datastring = $row['data'];
        $data = explode("||||", $datastring);
        $recipient = $data[0];

        $subject = $data[1];
        $body = $data[2];
        $expeditor = $data[4];
        $sms_type = $data[5];
        $provider = $data[6];

        if ($mailer->sendMessage($recipient, $body, $expeditor, $mailid, $source_test, $sms_type, $provider)) {
            return $id;
        }
        return $id;
    }

    function sendpushnotif($mailer, $row) {
        $id = $row['ID'];
        $datastring = $row['data'];
        $data = explode("||||", $datastring);
        $pn_type = $data[0];
        $recipient = $data[1];
        $body = $data[3];
        $app_name = $data[5];
        $type = $row['restaurant'];
        $mailer->sendPushnotif($recipient, $body, $pn_type, NULL, $app_name, $type);
        return $id;
    }

    function is_test_env() {

//        return false;
        
        if (strpos($_SERVER['HTTP_HOST'], 'api') !== false) {
            return false;
        }

        if (strpos($_SERVER['HTTP_HOST'], 'www') !== false) {
            return false;
        }

        if (strpos($_SERVER['HTTP_HOST'], 'prod') !== false) {
            return false;
        }

//        if (strpos($_SERVER['HTTP_HOST'], 'dev') !== false) {
//            return false;
//        }
//        
//        if (strpos($_SERVER['HTTP_HOST'], 'codersvn') !== false) {
//            return false;
//        }

        return true;
    }

    function getspoolhistory($query, $page = 1) {
        $data = array();
        $result = array();


        if ($query === 'week') {
            $filter = "WEEKOFYEAR(sent_on)=WEEKOFYEAR(NOW())";
        }
        if ($query === 'lastweek') {
            $filter = "WEEKOFYEAR(sent_on)=WEEKOFYEAR(NOW())-1";
        }
        if ($query === 'month') {
            $filter = "MONTH(sent_on)";
            
        }
        if ($query === 'lastmonth') {

            $filter = "YEAR(sent_on) = YEAR(CURRENT_DATE - INTERVAL 1 MONTH)
                    AND MONTH(sent_on) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH)";
        }
        if ($query === 'year') {
            $filter = "YEAR(sent_on)";
        }
        if ($query === 'day') {
            $filter = "CAST(sent_on AS DATE)";
        }
        if ($query === 'lastweek' || $query === 'week' || $query === 'lastmonth' ) {
            $data = pdo_multiple_select("select ID, type, status, restaurant, booking_id, data,CAST(sent_on AS DATE) AS DATE,count(*) as count from spool_history WHERE restaurant !='' and  type='SMS' and $filter  group by type,restaurant,$filter order by ID desc", 'dwh');
        } else if ($query === 'day') {
            $data = pdo_multiple_select("select ID, type, status, restaurant, booking_id, data,CAST(sent_on AS DATE) AS DATE,count(*) as count from spool_history WHERE restaurant !='' and type='SMS' group by type,restaurant,$filter order by ID desc", 'dwh');
        }else if($query === 'month'){
            $data = pdo_multiple_select("select ID, type, status, restaurant, booking_id, data,CAST(sent_on AS DATE) AS DATE,count(*) as count from spool_history WHERE restaurant !='' and  type='SMS' and $filter  group by type,restaurant,MONTH(sent_on),YEAR(sent_on) order by ID desc", 'dwh');
        }
        if ($query === 's3') {
            $query = pdo_single_select("SELECT count(*) as total FROM spool_history WHERE restaurant !='' AND type='MAIL'", 'dwh');


            $limit = 500;
            if ($page === 1) {
                $offset = $page . "," . $limit;
            } else {
                $start = $page * 500;
                $offset = $start . "," . $limit;
            }
            //limit $page,$limit
            $data = pdo_multiple_select("select ID, type, status, restaurant, booking_id, data,CAST(sent_on AS DATE) AS DATE from spool_history WHERE restaurant !='' AND type='MAIL' order by ID desc limit $offset ", 'dwh');
            $result['data'] = $data;
            $result['totalCount'] = $query['total'];
            return $result;
        }


        return $data;
    }

    public function saveContentToS3($id, $data, $sent_date) {
        $logger = new WY_log("website");
        $steps = "-1";
         //get s3
        $this->getS3();
        //get Sqs
        $this->getSqs();
        //get DB
        $this->getDb();

        $unique_id = uniqid();
        $file_name = __TMPDIR__ . $unique_id;
        $compressed_file_name = __TMPDIR__ . $unique_id . ".gz";

        $date = date_parse($sent_date);

        $destination_file_name = $date['year'] . '/' . $date['month'] . '/' . $date['day'] . '/' . $unique_id . ".gz";

        $this->createFile($file_name, $data);

        $this->gzCompressFile($file_name);
        $steps .= "-2";
        if ($this->s3->putObjectFile($compressed_file_name, $this->bucket, $destination_file_name)) {
		$steps .= "-3";
		$this->deleteFile($file_name);
		$this->deleteFile($compressed_file_name);
		$steps .= "-4";

		$sql = "UPDATE spool_history SET data = '$unique_id' WHERE ID = $id";
		pdo_exec($sql,'dwh');
		$steps .= "-5";

		$this->sqs->setQueue($this->queue);
		$this->sqs->setBody('migrate ' . $file_name);
		$this->sqs->sendMessage();
		 $steps .= "-6";
		//error_log($file_name);
        }
        $logger->LogEvent('', 2001, $id, '', 'saves3content' . $steps, date("Y-m-d H:i:s"));
        return $unique_id;
    }

    public function gzCompressFile($source, $level = 9) {
        $dest = $source . '.gz';
        $mode = 'wb' . $level;
        $error = false;
        if ($fp_out = gzopen($dest, $mode)) {
            if ($fp_in = fopen($source, 'rb')) {
                while (!feof($fp_in))
                    gzwrite($fp_out, fread($fp_in, 1024 * 512));
                fclose($fp_in);
            } else {
                $error = true;
            }
            gzclose($fp_out);
        } else {
            $error = true;
        }
        if ($error)
            return false;
        else
            return $dest;
    }

    public function createFile($id, $content) {
        $myfile = fopen("$id", "w") or die("Unable to open file!");
        fwrite($myfile, $content);
        fclose($myfile);
        return true;
    }

    public function deleteFile($id) {
        return unlink($id);
    }

    public function getS3() {
        if (empty($this->s3)) {
            $this->s3 = new S3(awsAccessKey, awsSecretKey);
        }
    }

    public function getSqs() {
        if (empty($this->sqs)) {
            $this->sqs = new SQS();
        }
    }

    public function getDb() {
        if (empty($this->db)) {
            $this->db = getConnection('dwh',NULL,'write');
        }
    }

    function getS3EmailTemplate($obj) {
        //get s3
        $this->getS3();
        //get Sqs
        $this->getSqs();
        //get DB
        $this->getDb();
        $data = $obj['item'];
        $date = date_parse($data['date']);


        $data['dataid'] = $data['dataid']; //'5705cfce9a07d';

        $filename = $date['year'] . '/' . $date['month'] . '/' . $date['day'] . '/' . $data['dataid'] . ".gz";

        //echo $filename;
        //$filename = "2016/4/7/5705cfce9a07d.gz";

        $result = $this->s3->getObject($this->bucket, $filename);

        $bodyAsString = $result->body;

        $content = gzdecode($bodyAsString, 20000);
        $body = explode('||||', $content);
        $re_cont = urldecode($body[3]);

        $tmpArray = array('htm' => $content, 'emheader' => unserialize($re_cont));

        return $tmpArray;
    }

}

?>