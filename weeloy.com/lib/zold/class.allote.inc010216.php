<?php

class WY_Allote {

    var $restaurant;
    var $mealduration;
    var $perpax;    // standard allotement is on table unit. Perpax set it on person
    var $openDay;
    var $openLunch;
    var $openDinner;
    var $openDayHours;
    var $openWeekHours;
    var $openWeekSlots;
    var $laptime;
    var $cutofflunch;
    var $cutoffdiner;
    var $aDay;
    var $msg;
    var $result;
	var $maxdaybooking;
	
    function __construct($theRestaurant) {
        $this->restaurant = $theRestaurant;
        $this->mealduration = 3;
        $this->perpax = 0;
    }

		
    function getOpenHour($ohvalue, $weekday) {

		// openhour is either open hours or book hours
		// 2 encode: byslot or standard
		// byslot is only used for book hours
		// a week is 7 days, start Sunday
		// 7 days, lunch is [start, end] => day 1 (0, 1), day 2 (2, 3), day 3 (4, 5)... day 7 (12, 13)
		// 7 days, dinner is [start, end] => day 1 (14, 15), day 2 (16, 17), day 3 (18, 19)... day 7 (26, 27)
		// so 28 values in total

        if (empty($ohvalue))
            return $this->result = -1;

		$dd = ""; for($i = 0; $i < 20; $i++) $dd .= "0"; for($i = 0; $i < 8; $i++) $dd .= "1"; for($i = 0; $i < 10; $i++) $dd .= "0"; for($i = 0; $i < 8; $i++) $dd .= "1"; for($i = 0; $i < 10; $i++) $dd .= "0"; 
		$ohvalue = "BYSLOT|||$dd|||$dd|||$dd|||$dd|||$dd|||$dd|||$dd";

		$this->openWeekSlots = array();
		if(substr($ohvalue, 0, 6) == "BYSLOT") {
			$byslot = explode("|||", $ohvalue);
			if (count($byslot) < 8)
				return $this->result = -1;
							
			for($i = 0; $i < 7; $i++) {
				$this->openWeekSlots[] = $vv = $byslot[$i+1];
				// lunch
				for($j = 18; $j < 32; $j++)
					if($vv[$j] == "1") {
						$this->openWeekHours[$i][0] = $j;
						break;
						}
				if($j >= 32) // close
					$this->openWeekHours[$i][0] = $this->openWeekHours[$i][1] = 18;
				else {
					for($j = 31; $j > 18 && $vv[$j] == "0"; $j--);
					$this->openWeekHours[$i][1] = $j;
					}
				// dinner
				for($j = 32; $j < 48; $j++)
					if($vv[$j] == "1") {
						$this->openWeekHours[$i][2] = $j;
						break;
						}
				if($j >= 48) // close
					$this->openWeekHours[$i][2] = $this->openWeekHours[$i][3] = 32;
				else {
					for($j = 47; $j > 32 && $vv[$j] == "0"; $j--);
					$this->openWeekHours[$i][3] = $j;
					}
				}
			
		} else {	
			$valAr = explode("|||", $ohvalue);
			if (count($valAr) < 28)
				return $this->result = -1;

			for ($i = 0; $i < 7; $i++) {
				$k = ($i * 2);
				$this->openWeekHours[$i][0] = intval($valAr[$k]);
				$this->openWeekHours[$i][1] = intval($valAr[$k + 1]);
				$this->openWeekHours[$i][2] = intval($valAr[$k + 14]);
				$this->openWeekHours[$i][3] = intval($valAr[$k + 15]);
				}
			}
        $this->openDay = $this->openLunch = $this->openDinner = -1;
        $this->openDayHours[0] = $this->openDayHours[1] = $this->openDayHours[2] = $this->openDayHours[3] = -1;
        if ($weekday >= 0 && $weekday < 7) {
            for ($i = 0; $i < 4; $i++)
                $this->openDayHours[$i] = $this->openWeekHours[$weekday][$i];
            $this->openLunch = ($this->openDayHours[0] < $this->openDayHours[1]) ? 1 : 0;
            $this->openDinner = ($this->openDayHours[2] < $this->openDayHours[3]) ? 1 : 0;
            $this->openDay = $this->openLunch + $this->openDinner;
        }
        return $this->result = 1;
    }

	// unravel oneway slider construct
    private function setUpRange($col, $a, $b) {
        $start = floor($a * 0.5) . ':' . ($a % 2) * 3 . '0';
        $end = floor($b * 0.5);
        if ($end >= 24)
            $end = "0" . ($end - 24);
        $end = $end . ':' . ($b % 2) * 3 . '0';
        return $start . " - " . $end;
    }

	// unravel the openhour, bookinghour slider construct
    private function setUpMultipleRange($col, $a, $b) {
        $start_range = $a;
        $end_range = $a + 1;
        if ($a == $b) {
            return array();
        }
        while ($a <= $b) {
            $start = floor($start_range * 0.5) . ':' . ($start_range % 2) * 3 . '0';
            $end = floor($end_range * 0.5);
            if ($end >= 24)
                $end = "0" . ($end - 24);
            $end = $end . ':' . ($end_range % 2) * 3 . '0';

            $result[] = $start . " - " . $end;
            $start_range = $end_range;
            $end_range = $end_range + 1;
            $a = $a + 1;
            
            if ($end_range > 48) {
                $end_range = $end_range - 48;
            }
        }
        return $result;
    }

	//generate html code for template
    function templateAlloteOpenHour($ohvalue, $no_html = false) {

        $array_data = array();
        $tmp_car = array('<br>', '&nbsp;', '&nbsp;', ' ');
        $weekday = array("Sunday", "Monday", "Tuesday", "Wednesday &nbsp;&nbsp;&nbsp;", "Thursday", "Friday", "Saturday");

        $this->getOpenHour($ohvalue, $weekday);
        if ($this->result < 0)
            return ($no_html) ? $array_data : "";

        $template = "<table class='table table-striped' style='font-family:Roboto;font-size:14px;font-weight:bold'>";
        $template .= "<th>Day</th><th>Lunch</th><th>Dinner</th>";
        for ($kk = 0; $kk < 7; $kk++) {
            $col = $kk * 2;
            $ll = $this->setUpRange($kk, $this->openWeekHours[$kk][0], $this->openWeekHours[$kk][1]);
            $dd = $this->setUpRange($kk + 7, $this->openWeekHours[$kk][2], $this->openWeekHours[$kk][3]);
            if ($this->openWeekHours[$kk][0] == $this->openWeekHours[$kk][1])
                $ll = "closed<br>&nbsp;";
            if ($this->openWeekHours[$kk][2] == $this->openWeekHours[$kk][3])
                $dd = "closed<br>&nbsp;";
            $template .= "<tr><td>" . $weekday[$kk] . "</td><td>$ll</td><td>$dd</td></tr>";
            $array_data[$kk] = array('day' => str_replace($tmp_car, '', $weekday[$kk]), 'lunch' => str_replace($tmp_car, '', $ll), 'dinner' => str_replace($tmp_car, '', $dd));
        }
        $template .= "</table>";

        return ($no_html) ? $array_data : $template;
    }

    function templatePickupHours($ohvalue, $no_html = false) {

        $array_data = array();
        $tmp_car = array('<br>', '&nbsp;', '&nbsp;', ' ');
        $weekday = array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");

        $this->getOpenHour($ohvalue, $weekday);
        if ($this->result < 0)
            return ($no_html) ? $array_data : "";
        for ($kk = 0; $kk < 7; $kk++) {
            $col = $kk * 2;
            $ll = $this->setUpMultipleRange($kk, $this->openWeekHours[$kk][0], $this->openWeekHours[$kk][1]);
            $dd = $this->setUpMultipleRange($kk + 7, $this->openWeekHours[$kk][2], $this->openWeekHours[$kk][3]);

            $array_data[$kk] = array('pickuphour' => array_merge($ll, $dd));
        }
        return $array_data;
    }

    function consoBookingAllote($theRestaurant, $aDate) {

        $book = array();
        //$booking = new WY_Booking();
        if ($aDate == "") {
            $data = WY_Booking::getDateBooking($theRestaurant);
            $block = WY_Block::getDateBlock($theRestaurant);           
            }
        else {
        	$data = WY_Booking::getThatDayBooking($theRestaurant, $aDate);
            $block = WY_Block::getThatDateBlock($theRestaurant, $aDate);
        	}

        $perpax = $this->perpax;
        $mealduration = $this->mealduration; //3 slots per repas

        foreach ($data as $row) {
            $bdata = intval($row['ndays']);
            $slottime = (intval(substr($row['rtime'], 0, 2)) * 2) + (intval(substr($row['rtime'], 3, 1)) / 3);
            for ($i = 0; $i < $mealduration; $i++) {
                if (!isset($book[$bdata][$slottime + $i]))
                    $book[$bdata][$slottime + $i] = 0;
                $inc = ($perpax) ? intval($row['cover']) : 1;
                $book[$bdata][$slottime + $i] += $inc;
            }
        }

        foreach ($block as $row) {
            $bdata = intval($row['ndays']);
            $slottime = (intval(substr($row['stime'], 0, 2)) * 2) + (intval(substr($row['stime'], 3, 1)) / 3);
            $nslots = intval($row['slots']);
            for ($i = 0; $i < $nslots; $i++) {
                if (!isset($book[$bdata][$slottime + $i]))
                    $book[$bdata][$slottime + $i] = 0;
                $book[$bdata][$slottime + $i] += intval($row['tablepax']);
                error_log('BLOCK ' . $slottime . intval($row['tablepax']));
            }
        }

        return $book;
    }

    function readAlloteRestaurant($theRestaurant, $type, $year) {

        $tmp = $year - intval(date('Y'));
        if (($type != "lunch" && $type != "dinner") || $tmp < 0 || $tmp > 1)
            return "";

        $data = pdo_single_select("SELECT ID, restaurant, type, allotment from allotment where restaurant = '$theRestaurant' and type = '$type' and year = '$year' limit 1");
        if (count($data) <= 0) {
            for ($value = $sep = "", $i = 0; $i <= 366; $i++, $sep = "|")
                $value .= $sep . 5;
            pdo_exec("insert into allotment (restaurant, type, allotment, year) values ('$theRestaurant', '$type', '$value', '$year') ");
            $data = pdo_single_select("SELECT ID, restaurant, type, allotment from allotment where restaurant = '$theRestaurant' and type = '$type' and year = '$year' limit 1");
            if (count($data) <= 0) {
                error_log("saveALLOTE unable to create $type data");
                return -1;
            }
        }
        return $data['allotment'];
    }

    function getAlloteRestaurant($theRestaurant, $year) {

        $data['lunch'] = $this->readAlloteRestaurant($theRestaurant, 'lunch', $year);
        $data['dinner'] = $this->readAlloteRestaurant($theRestaurant, 'dinner', $year);

		$DayOfYear = intval(date('z')); // current day of year
        if($this->maxdaybooking > 365 - $DayOfYear) {
			$data['lunch'] = substr($data['lunch'], 0, strlen($data['lunch']) - 1) . $this->readAlloteRestaurant($theRestaurant, 'lunch', $year+1);
			$data['dinner'] = substr($data['dinner'], 0, strlen($data['dinner']) - 1) . $this->readAlloteRestaurant($theRestaurant, 'dinner', $year+1);
			}

        return $data;
    }

    function saveAlloteRestaurant($theRestaurant, $year, $size, $start, $newlunchdata, $newdinnerdata) {

        $oldlunchdata = $this->readAlloteRestaurant($theRestaurant, 'lunch', $year);
        if ($oldlunchdata == "")
            return $this->result = -1;
        $oldlunchAr = explode("|", $oldlunchdata);
        $newlunchAr = explode("|", $newlunchdata);
        for ($i = 0, $k = $start; $i < $size; $i++, $k++)
            $oldlunchAr[$k] = $newlunchAr[$i];
        $dataStr = implode("|", $oldlunchAr);
        pdo_exec("Update allotment set allotment='$dataStr' where restaurant = '$theRestaurant' and type = 'lunch' and year = '$year' limit 1");


        $olddinnerdata = $this->readAlloteRestaurant($theRestaurant, 'dinner', $year);
        if ($olddinnerdata == "")
            return $this->result = -1;
        $olddinnerAr = explode("|", $olddinnerdata);
        $newdinnerAr = explode("|", $newdinnerdata);
        for ($i = 0, $k = $start; $i < $size; $i++, $k++)
            $olddinnerAr[$k] = $newdinnerAr[$i];
        $dataStr = implode("|", $olddinnerAr);
        pdo_exec("Update allotment set allotment='$dataStr' where restaurant = '$theRestaurant' and type = 'dinner' and year = '$year' limit 1");

        return $this->result = 1;
    }

    function ComputeOpenToday($ohvalue) {

        $openStr = "Open Today";
        $closeStr = "Close Today";

        // get current day, and current day of the week
        // then check Open Hour, and availability

        $today = date('d-m-Y');
        $cyear = intval(date('Y'));
        $ctime = strtotime($rdate);
        $weekday = intval(date('w', $ctime)); // current day of week
        $yearday = intval(date('z', $ctime)); // current day of year

        $this->getOpenHour($ohvalue, $weekday);
        if ($this->result < 0 || $this->openDay <= 0) {
            $this->result = 1;
            return $closeStr;
        }

        $data = $this->getAlloteRestaurant($this->restaurant, $cyear); // configuration from the backoffice
        if (count($data) < 1)
            return $closeStr;

        $this->result = 1;
        $lunchAr = explode("|", $data['lunch']);
        $dinnerAr = explode("|", $data['dinner']);
        return ($lunchAr[$yearday] > 0 || $dinnerAr[$yearday] > 0) ? $openStr : $closeStr;
    }

    // check if we can approve this reservation by check the allotement for that day that time.
    // 1 check open hour if open
    // 2 check allotement availabele is bigger that the requested party
    // 3 check existing booking to check what's really left
    // 4 check current time

    private function getCutofftime($curslot, $slottime) {

        if ($curslot < 32 && $slottime < 32 && !empty($this->cutofflunch)) {
            $tt = intval(substr($this->cutofflunch, 0, 2));
            $uu = intval(substr($this->cutofflunch, 3, 2));
            $cutoffslot = ($tt > 0 && $tt < 32 && $uu >= 0 && $uu < 60) ? ($tt * 2) + floor($uu / 30) : 0;
            return ($cutoffslot > 0 && $cutoffslot <= $curslot) ? 1 : 0;
        }

        if ($curslot >= 32 && $slottime >= 32 && !empty($this->cutoffdiner)) {
            $tt = intval(substr($this->cutoffdiner, 0, 2));
            $uu = intval(substr($this->cutoffdiner, 3, 2));
            $cutoffslot = ($tt > 0 && $tt < 32 && $uu >= 0 && $uu < 60) ? ($tt * 2) + floor($uu / 30) : 0;
            return ($cutoffslot > 0 && $cutoffslot <= $curslot) ? 2 : 0;
        }

        return 0;
    }

    function getAllote1Slot($ohvalue, $rdate, $rtime, $rpers, $conf) {

 		// check the validity of an available slots, for verfication of the 1, 2, 3ieme page of the booking engine
        // rdate formt yyyy-mm-dd
        // rtime format hh:mm

        date_default_timezone_set('Asia/Singapore');

        $ctime = strtotime($rdate);
        $weekday = intval(date('w', $ctime)); // current day of week
        $yearday = intval(date('z', $ctime)); // current day of year
        $cyear = intval(substr($rdate, 0, 4));
        $tmp = $cyear - intval(date('Y'));
        if ($tmp < 0 || $tmp > 1) {
            WY_debug::recordDebug("ERROR-API", "API-BOOKING", "Invalid Year Date" . " \n " . "$rdate, $rtime, $rpers, $conf, $cyear");
            $cyear = intval(date('Y'));
        }

        //after 16:00, is dinner
        $timeAr = explode(":", $rtime);
        $whichmeal = (intval($timeAr[0]) >= 16) ? "dinner" : "lunch";
        if ($whichmeal == "dinner" && $this->openDinner <= 0)
            return -1;
        if ($whichmeal == "lunch" && $this->openLunch <= 0)
            return -1;
		
        $this->getOpenHour($ohvalue, $weekday);
        if ($this->result < 0 || $this->openDay <= 0)
            return $this->result = -1;

        $mealduration = $this->mealduration; //3 slots per repas or 5 if previously set
        $perpax = $this->perpax;
        $cover = ($perpax != 1) ? 1 : $rpers;
		$slottime = (intval(substr($rtime, 0, 2)) * 2) + (intval(substr($rtime, 3, 1)) / 3);
		$offset = ($whichmeal == "lunch") ? 0 : 2;

		// if($slottime < $this->openWeekHours[$weekday][$offset] || ($slottime + $mealduration) > $this->openWeekHours[$weekday][$offset+1]) return -1;
			
        // check the allotment for that day
        $data = $this->getAlloteRestaurant($this->restaurant, $cyear); // configuration from the backoffice
        if (count($data) < 1)
            return $this->result = -1;

        $this->result = 0;
        // is request greater that configured availability ?
        $mealAr = ($whichmeal == "lunch") ? explode("|", $data['lunch']) : explode("|", $data['dinner']);
        $availPax = intval($mealAr[$yearday]);
        if ($availPax < $cover)
            return $availPax;

        if ($yearday == intval(date('z'))) {
            if ($this->laptime < 0 || $this->laptime > 5)
                $this->laptime = 0;
            $slice = $curslot = (intval(date('H')) * 2) + floor(intval(date('i')) / 30);
            $slice += ($this->laptime * 2);
            $slottime = (intval(substr($rtime, 0, 2)) * 2) + (intval(substr($rtime, 3, 1)) / 3);
            if ($slice > $slottime) {
                WY_debug::recordDebug("ERROR-API", "API-BOOKING", "Too early" . " \n " . "$rdate, $rtime, $rpers, $conf, $slice, $slottime, time= " . date('H:i') . " " . $this->restaurant);
                return $this->result = -1;
            }

            if (($cutoff = $this->getCutofftime($curslot, $slottime)) > 0) {
                $tt = ($cutoff == 1) ? $this->cutofflunch : $this->cutoffdiner;
                WY_debug::recordDebug("ERROR-API", "API-BOOKING", "Cutoff" . " \n " . "$rdate, $rtime, $rpers, conf=$conf, $tt, $cutoff");
                error_log("ERROR-API Cutoff" . " \n " . "$rdate, $rtime, $rpers, conf=$conf, $tt, $cutoff, " . date('H:i'));
                return $this->result = -1;
            }
        }

        //$booking = new WY_Booking();
        $data = WY_Booking::getThatDayBooking($this->restaurant, $rdate);
        $block = WY_Block::getThatDateBlock($this->restaurant, $rdate);
        if (count($data) <= 0 && count($block) <= 0) // no booking, so there are some availability
            return $this->result = 1;

		$book = array();
		$slottime = (intval(substr($rtime, 0, 2)) * 2) + (intval(substr($rtime, 3, 1)) / 3);
		for ($i = 0; $i < $mealduration; $i++) {
			$book[$slottime + $i] = ($availPax - $cover);
			}

        if (count($data) > 0) {
			foreach ($data as $row) {
				if (!empty($conf) && $conf == $row['confirmation'])
					continue;
				$slottime = (intval(substr($row['rtime'], 0, 2)) * 2) + (intval(substr($row['rtime'], 3, 1)) / 3);
				$cover = ($perpax != 1) ? 1 : intval($row['cover']);
				for ($i = 0; $i < $mealduration; $i++) {
					if (isset($book[$slottime + $i])) {
						$book[$slottime + $i] -= $cover;
						if ($book[$slottime + $i] < 0)
							return $this->result = -1;
						}
					}
				}
			}
		
		if (count($block) > 0) {
			foreach ($block as $row) {
				$slottime = (intval(substr($row['stime'], 0, 2)) * 2) + (intval(substr($row['stime'], 3, 1)) / 3);
				$nslots = intval($row['slots']);
				for ($i = 0; $i < $nslots; $i++) {
					if (isset($book[$slottime + $i])) {
						$book[$slottime + $i] -= intval($row['tablepax']);
						if ($book[$slottime + $i] < 0)
							return $this->result = -1;
						}
					}
				}
			}
			
        return $this->result = 1;
    }

    // 2 periods: lunch and dinner. If needed, we could add a third period for breakfast using same model
    // 4 Phases
    // 1) get open hours information for a week, "STARTING" the current day !important
    // 2) get availability for the restaurant for both lunch and dinner
    // 3) decrement existing availability with the bookings, each booking is a table for 1 and 1/2 hour -> 3 slots
    // 4) update the current day -> no walking => close the period 1 slot before (30mns). Else close previous slot of the day.

    function AlloteComputeOpenHour($ohvalue, $isnoWalking) {

		// function to send proper availability information to the booking engine
        // date_default_timezone_set('Singapore'); 
        // 1) PHASE: first get open hours information for a week, "STARTING" the current day !important
        // and replicate the week for the next 2 months -> booking was limited to 2 month ahead (70days)
        // Now booking is limited to end of year

        $weekday = date('w'); // current day of the week
        $cyear = intval(date('Y'));
        $this->getOpenHour($ohvalue, $weekday);
        if ($this->result < 0)
            return array();

        $lunch = $dinner = array();

        $nchar_lunch = 4;
        $nchar_dinner = 4;
        $emptylunch = $emptydinner = "";
        for ($i = 0; $i < $nchar_lunch; $i++)
            $emptylunch .= "0"; // "0000"
        for ($i = 0; $i < $nchar_dinner; $i++)
            $emptydinner .= "0";

        $limit = $nchar_lunch * 4;  // 4 is the number of bits
        $day = $weekday;
        for ($k = 0; $k < 7; $k++, $day = ($day < 6) ? $day + 1 : 0) {
            $Lstart = $this->openWeekHours[$day][0];
            $Lend = $this->openWeekHours[$day][1];

            $cd = 0;
            $startingReference = 18;  // 18 -> 9 * 2, 9h00 for lunch. Starting Reference for the OpenHours
            if ($Lstart != $Lend) {
                for ($i = 0, $pp = 1; $i < $limit; $i++, $pp <<= 1) { // lunch, set the bit when the time is between start and end, for the 16 digits. Last possible is 16h30 -> slot 33 (everything is divided by 1/2 hour)
                    $slot = $i + $startingReference;
                    if ($slot >= $Lstart && $slot <= $Lend)
                        $cd += $pp;
                }
            }

            $lunch[] = $cd;

            $Dstart = $this->openWeekHours[$day][2];
            $Dend = $this->openWeekHours[$day][3];

            $cd = 0;
            $startingReference = 32;  // 32 -> 16 * 2, 16h00 for dinner. Starting Reference for the OpenHours
            $limit = $nchar_dinner * 4;  // 4 is the number of bits
            if ($Dstart != $Dend) {
                for ($i = 0, $pp = 1; $i < $limit; $i++, $pp <<= 1) { // lunch, set the bit when the time is between start and end, for the 16 digits. Last possible is 16h30 -> slot 33 (everything is divided by 1/2 hour)
                    $slot = $i + $startingReference;
                    if ($slot >= $Dstart && $slot <= $Dend)
                        $cd += $pp;
                }
            }
            $dinner[] = $cd;
        }

        $yearday = intval(date('z')); // current day of year
        //$maxdaybooking = 365 - $yearday;	//
        //$maxdaybooking = 70;	// 70 days max
        // 1)a) replicate the week for the next 2 months -> booking is limited to 2 month ahead

        for ($i = 7; $i < $this->maxdaybooking; $i++) {
            $lunch[$i] = $lunch[$i % 7];
            $dinner[$i] = $dinner[$i % 7];
        }


        // 2) PHASE: get availability for the restaurant for both lunch and dinner

        $data = $this->getAlloteRestaurant($this->restaurant, $cyear); // configuration from the backoffice
        // 3) PHASE: decrement existing availability with the bookings, each booking is a table for 1 and 1/2 hour -> 3 slots
        // for lunch or dinner depending of the reservation time !!! Attention: no overlapping -> potential problem is 16h

        $lunchAr = explode("|", $data['lunch']);
        $dinnerAr = explode("|", $data['dinner']);

        $mealduration = $this->mealduration; //3 slots per repas or 5 if previously set
        $book = $this->consoBookingAllote($this->restaurant, ""); // information from the booking
        $limit = $this->maxdaybooking;

        for ($i = 0; $i < $limit; $i++) {
            $maxtable = intval($lunchAr[$i + $yearday]);
            $offset = 18; //starting of dinner slot: 18/2 = 9h
            if ($maxtable <= 0)
                $lunch[$i] = 0;
            else if (isset($book[$i])) {
                $row = $book[$i];
                while (list($slot, $val) = each($row))
                    if ($val >= $maxtable) {
                        $lunch[$i] &= ~(1 << ($slot - $offset));  // unset corresponding bit for slot
                        for ($mm = 1; $mm < $mealduration && (($slot - $offset) - $mm) >= 0; $mm++) // close also the preceding slots for $mealduration - 1
                            $lunch[$i] &= ~(1 << (($slot - $offset) - $mm));
                    }
            }

            $maxtable = intval($dinnerAr[$i + $yearday]);
            $offset = 32; //starting of dinner slot  32/2 = 16h
            if ($maxtable <= 0)
                $dinner[$i] = 0;
            else if (isset($book[$i])) {
                $row = $book[$i];
                while (list($slot, $val) = each($row))
                    if ($val >= $maxtable) {
                        $dinner[$i] &= ~(1 << ($slot - $offset)); // unset corresponding bit for slot
                        for ($mm = 1; $mm < $mealduration && (($slot - $offset) - $mm) >= 0; $mm++) // close also the preceding slots for $mealduration - 1
                            $dinner[$i] &= ~(1 << (($slot - $offset) - $mm));
                    }
            }
        }

        // 4) PHASE: update today's information based upon the current time 	
        date_default_timezone_set('Asia/Singapore');

        if ($this->laptime < 0 || $this->laptime > 5)
            $this->laptime = 0;
        $slice = $curslot = (intval(date('H')) * 2) + floor(intval(date('i')) / 30);
        $slice += ($this->laptime * 2);
        if ($slice >= $this->openDayHours[1])
            $lunch[0] = $emptylunch;
        if ($slice >= $this->openDayHours[3])
            $dinner[0] = $emptydinner;

        $cutoff = $this->getCutofftime($curslot, $curslot);
        if ($cutoff == 1)
            $lunch[0] = $emptylunch;
        else if ($cutoff == 2)
            $dinner[0] = $emptydinner;

        for ($slot = $this->openDayHours[0]; $slot <= $this->openDayHours[1] && $slot < $slice && $slice <= $this->openDayHours[1]; $slot++) {
            $lunch[0] &= ~(1 << ($slot - 18));
        }

        for ($slot = $this->openDayHours[2]; $slot < $this->openDayHours[3] && $slot < $slice && $slice < $this->openDayHours[3]; $slot++) {
            $dinner[0] &= ~(1 << ($slot - 32));
        }

        if ($isnoWalking) {
            error_log("NO WALKING " . $this->restaurant);
            if ($slice >= $this->openDayHours[0] - 1)
                $lunch[0] = $emptylunch;
            if ($slice >= $this->openDayHours[2] - 1)
                $dinner[0] = $emptydinner;
        }

        $lunchdata = $dinnerdata = "";
        for ($i = 0; $i < $this->maxdaybooking; $i++) {
            $lunchdata .= sprintf("%04x", $lunch[$i]);
            $dinnerdata .= sprintf("%04x", $dinner[$i]);
        }

        $data = array();
        $data['lunchstart'] = 9;
        $data['dinnerstart'] = 16;
        $data['lunchdata'] = strtoupper($lunchdata);
        $data['dinnerdata'] = strtoupper($dinnerdata);

        return $data;
    }

    function getAlloteReportSlot($ohvalue, $rdate) {

        //date_default_timezone_set('Singapore'); 
        // 1) PHASE: first get open hours information for a day,
        // reformat the date format
        // rdate formt yyyy-mm-dd

        $tmpAr = explode("-", $rdate);
        $ctime = strtotime($rdate); // unix
        $todayflag = (date('d-m-Y') == date('d-m-Y', $ctime));
        $weekday = intval(date("w", $ctime)); // current day of the week
        $yearday = intval(date('z', $ctime)); // current day of year of specific date
        $cyear = intval(substr($rdate, 0, 4));
        $tmp = $cyear - intval(date('Y'));
        if ($tmp < 0 || $tmp > 1) {
            WY_debug::recordDebug("ERROR-API", "API-BOOKING", "Invalid Year Date" . " \n " . "$rdate, $cyear");
            $cyear = intval(date('Y'));
        }

        $this->getOpenHour($ohvalue, $weekday);
        if ($this->result < 0)
            return -1;

        $data = $this->getAlloteRestaurant($this->restaurant, $cyear); // configuration from the backoffice
        // set the allotement for the opening hours
        // in one array, 0-15 for lunch, 16 - 32 for dinner

        $lunchAr = explode("|", $data['lunch']);
        $dinnerAr = explode("|", $data['dinner']);

        $maxlunch = intval($lunchAr[$yearday]);
        $maxdinner = intval($dinnerAr[$yearday]);
        for ($i = 0; $i < 32; $i++)
            $repasbits[] = $booked[] = 0;

	// lunch, set the bit when the time is between start and end, for the 16 digits. Last possible is 16h30 -> slot 33 (everything is divided by 1/2 hour)

        $startingReference = 18;  // 18 -> 9 * 2, 9h00 for lunch. Starting Reference for the OpenHours
        $Lstart = $this->openDayHours[0] - $startingReference;
        $Lend = $this->openDayHours[1] - $startingReference;
        for ($i = $Lstart; $i < $Lend; $i++)
            $repasbits[$i] = $maxlunch;

        $startingReference = 32;  // 32 -> 16 * 2, 16h00 for dinner. Starting Reference for the OpenHours
        $Dstart = $this->openDayHours[2] - $startingReference;
        $Dend = $this->openDayHours[3] - $startingReference;
        for ($i = $Dstart; $i < $Dend; $i++)
            $repasbits[$i + 14] = $maxdinner;

        $book = $this->consoBookingAllote($this->restaurant, $rdate); // information from the booking with mysql format date yyyy-mm-dd

        if (count($book) > 0 && isset($book[0])) {
            while (list($slot, $val) = each($book[0])) {
                $bslot = (intval($slot) < 32) ? (intval($slot) - 18) : (intval($slot) - 32) + 14;
                if ($bslot < 0) {
                    error_log('getdayDispo ' . $slot . ' ' . $val);
                    continue;
                }
                $repasbits[$bslot] -= $val;
                $booked[$bslot] = $val;
            }
        }

        // 4) PHASE: update today's information based upon the current time 	
        date_default_timezone_set('Asia/Singapore');

        if ($todayflag) {
            for ($slot = (intval(date('H')) * 2) + floor(intval(date('i')) / 30) - 18; $slot >= 0; $slot--)
                $repasbits[$slot] = 0;
        }

        //$this->printdebug($repasbits, $rdate);		
        for ($i = 0; $i < 14; $i++)
            $lunchbits[] = $repasbits[$i];
        for ($i = 14; $i < 32; $i++)
            $dinnerbits[] = $repasbits[$i];

        return array("avail" => implode(",", $repasbits), "booked" => implode(",", $booked), "pax" => strval($this->perpax));
    }

    function printdebug($repasbits, $rdate) {

        for ($i = 0; $i < 32; $i++) {
            $mtype = ($i < 14) ? "LUNCH" : "DINNER";
            error_log($mtype . " " . (floor($i / 2) + 9) . ":" . (($i % 2) * 3) . "0" . "   ->   " . $repasbits[$i]);
        }
    }

}

class WY_Block {
   use CleanIng;

   var $blockID;
   var $description;
   var $date;
   var $slots;
   var $result;
   var $msg;
   

	function __construct($theRestaurant) {
		$this->restaurant = $theRestaurant;
	}

	private function getUniqueCode() {
		
		for($i = 0; $i < 100; $i++) {
			$uniqueID = rand(10000000,100000000 - 1);
        	$data = pdo_single_select("SELECT blockID FROM alloteblock WHERE restaurant ='$this->restaurant' and blockID = '$uniqueID' limit 1");
       		if(count($data) <= 0)
        		return $uniqueID;
 			}			
		return false;
		}
	
	private function retval($val, $msg) {
		$this->result = $val;
		$this->msg = $msg;
		return array($val, $msg);
		}

	static function getDateBlock($theRestaurant) {
        date_default_timezone_set('Asia/Singapore');
		return pdo_multiple_select("SELECT blockID, date, stime, slots, tablepax, DATEDIFF(date, CURDATE()) as ndays FROM alloteblock WHERE restaurant ='$theRestaurant' and date >= CURDATE() ORDER by date");
		}
		
	static function getThatDateBlock($theRestaurant, $aDate) {
        date_default_timezone_set('Asia/Singapore');
		return pdo_multiple_select("SELECT blockID, date, stime, slots, tablepax, 0 as ndays FROM alloteblock WHERE restaurant ='$theRestaurant' and date = '$aDate' ORDER by date");
		}
		
	function getBlocks() {
		$this->result = 1;
		$theRestaurant = $this->restaurant;
		return pdo_multiple_select("SELECT * FROM alloteblock WHERE restaurant ='$theRestaurant' ORDER by date");
	}

	function insertBlock($blockID, $date, $stime, $tablepax, $description, $slots) {

        $blockID = preg_replace("/[^a-zA-Z0-9]/", "", $blockID);
        if(strlen($blockID) < 7)
        	return $this->retval(-1,"invalid ID " . $blockID); 
 
        $data = pdo_single_select("SELECT blockID FROM alloteblock WHERE restaurant ='$this->restaurant' and blockID = '$blockID' limit 1");
        if(count($data) > 0) {
       		$blockID = $this->getUniqueCode();
        	if($blockID == false)
        		return $this->retval(-1,"unable to create a unique ID "); 
        	}

		$arg = array('blockID', 'date', 'stime', 'tablepax', 'description', 'slots');
		for($i = 0; $i < count($arg); $i++) { $tt = $arg[$i]; $$tt = $this->clean_input($$tt); }

		if($blockID == "")
			return $this->retval(-1, "Block id is invalid -$blockID-");
		
		$theRestaurant = $this->restaurant;
		$this->result = pdo_insert("INSERT INTO  alloteblock (restaurant, blockID, date, stime, tablepax, description, slots ) VALUES ('$theRestaurant', '$blockID', '$date', '$stime', '$tablepax', '$description', '$slots')");
		return ($this->result >= 0) ? $this->retval(1, "Block $blockID has been created") : $this->retval(-1, "Fail. Block $blockID already exists");
	}

	function updateBlock($blockID, $date, $stime, $tablepax, $description, $slots) {

		$arg = array('blockID', 'date', 'stime', 'tablepax', 'description', 'slots');
		for($i = 0; $i < count($arg); $i++) { $tt = $arg[$i]; $$tt = $this->clean_input($$tt); }

		$theRestaurant = $this->restaurant;		
		$blockID = intval(preg_replace("/[^0-9]/", "", $blockID));

		if($blockID == "")
			return $this->retval(-1, "Undefined Block id");
		
		$affected_rows = pdo_exec("Update alloteblock set blockID='$blockID', date='$date', stime='$stime', tablepax='$tablepax', description='$description', slots='$slots' where blockID='$blockID' and restaurant = '$theRestaurant' limit 1");
		$this->result = 1;
		return $this->retval(1, "Block $blockID has been updated");
	}

	function delete($blockID) {
		$blockID = $this->clean_input($blockID);
		if($blockID == "")
			return $this->retval(-1, "Block id is invalid -$blockID-");
	
		$theRestaurant = $this->restaurant;
		$affected_rows = pdo_exec("DELETE FROM alloteblock WHERE blockID = '$blockID' and restaurant = '$theRestaurant' LIMIT 1");
		return $this->retval(1, "Block $blockID has been deleted");
	}
}

?>
