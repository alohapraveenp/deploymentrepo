<?php

/**
 * 	Richard Kefs (c) 2014
 *
 * 	Contains all classes definition for mail session.
 *
 */
require_once("conf/conf.mail.inc.php");
require_once("lib/wpdo.inc.php");
require_once("lib/class.spool.inc.php");
require_once("lib/class.mailin2.sblue.inc.php");
require_once("lib/class.debug.inc.php");
require_once("lib/composer/vendor/autoload.php");

class JM_Mail {

    var $db;
    var $recipient;
    var $subject;
    var $body;
    var $header;
    var $result;
    var $twig;

    function sendmail($recipient, $subject, $body, $opts = NULL, $restaurant = NULL, $booking_id = NULL) {
        $spool = new WY_Spool;
        $black_list = array('no@email.com');
        $result = true;
        if (is_array($recipient)) {
            foreach ($recipient as $r) {
                if (in_array($r, $black_list, true)) {
                    return false;
                }
                $result = $result && $spool->register_mail($r, $subject, $body, $opts, $restaurant, $booking_id);
            }
            return $result;
        } else {
            if (in_array($recipient, $black_list, true)) {
                return false;
            }
            return $spool->register_mail($recipient, $subject, $body, $opts, $restaurant, $booking_id);
        }
    }

    function extractheaderfield($type, $opts) {
        if (isset($opts[$type])) {
            list($key, $val) = each($opts[$type]);
            return ($key !== "0" && $key !== 0) ? array($key, $val) : array($val, $val);
        }
        return array("", "");
    }

    function send_fromlocalhost($recipient, $subject, $body, $opts = NULL) {

        $opts['send_from_localhost'] = false;
        $this->send_fromswiftmailer($recipient, $subject, $body, $opts);
    }

    function send_fromwhitelabel($recipient, $subject, $body, $opts = NULL) {

        $headers = $replyto = $replytolabel = $cc = $cclabel = $bcc = $bcclabel = $from = $fromlabel = "";

        list($from, $fromlabel) = $this->extractheaderfield('from', $opts);
        if ($from == "")
            $from = $fromlabel = 'support@weeloy.com';

        list($replyto, $replytolabel) = $this->extractheaderfield('replyto', $opts);
        if (empty($replyto)) {
            $replyto = $from;
            $replytolabel = $fromlabel;
        }
        list($bcc, $bcclabel) = $this->extractheaderfield('bcc', $opts);
        list($cc, $cclabel) = $this->extractheaderfield('cc', $opts);

        //$body = preg_replace("/=’/", "=3D\"", $body);
        $body = preg_replace("/’/", "\"", $body);
        $text = preg_replace("/\<[^\>]+\>/", "", $body);

        $mailin = new Mailin(MailinURL, MailinAPIKEY);

        $data = array(
            "to" => array($recipient => $fromlabel),
            "from" => array($from, $fromlabel),
            "replyto" => array($replyto, $replytolabel),
            "subject" => $subject,
            //"text" => $text,
            "html" => $body,
            "attachment" => array(),
            "headers" => array("Content-Type" => "text/html; charset=utf-8", "X-param1" => "value1", "X-param2" => "value2", "X-Mailin-custom" => "my custom value", "X-Mailin-IP" => "102.102.1.2", "X-Mailin-Tag" => "My tag"),
            "inline_image" => array()
        );
        if (!empty($cc))
            $data["cc"] = array($cc => $cclabel);
        if (!empty($bcc))
            $data["bcc"] = array($bcc => $bcclabel);

        $res = $mailin->send_email($data);
        if ($res['code'] != "success") {
            $content = preg_replace("/\'|\"/", "’", print_r($res, true));
            WY_debug::recordDebug("ERROR", "EMAIL", "Fail Sending content" . $content . ", recipient = " . $recipient . ", subject =  " . $subject . ", from = " . $from . ", replyto = " . $replyto . ", ip = " . $_SERVER['REMOTE_ADDR']);
        }
    }

    function send_fromwhitelabelAws($recipient, $subject, $body, $opts = NULL) {

        //error_log(print_r($opts,true));

        return $this->send_fromswiftmailer($recipient, $subject, $body, $opts);
    }

    function send_fromswiftmailer($recipient, $subject, $body, $opts = NULL) {

        if (isset($opts['from'])) {
            $fromAr = $opts['from'];
        } else {
            $fromAr = array('support@weeloy.com' => 'Weeloy - Support');
            $opts['from'] = 'support@weeloy.com';
        }

        if (isset($opts['send_from_localhost']) && $opts['send_from_localhost'] === true) {
            $transport = Swift_SmtpTransport::newInstance('localhost', 8888);
        } else {
            $transport = Swift_SmtpTransport::newInstance('email-smtp.us-west-2.amazonaws.com', 587, 'tls')
                    ->setUsername(AWSAccessKeyId)
                    ->setPassword(AWSSecretKey);
        }

        $mailer = Swift_Mailer::newInstance($transport);
        //Create the message
        $message = Swift_Message::newInstance()
                ->setSubject($subject)
                ->setFrom($opts['from']);

        $message->setTo($recipient);


        if (!isset($body) || $body == "") {
            $body = 'default body';
        }

        $body = str_replace('’', '"', $body);
        $root_path = explode(':', get_include_path());


        if (!isset($opts['white_label'])) {
            //$cid1 = $message->embed(Swift_Image::fromPath($root_path[count($root_path)-1].'https://static.weeloy.com/emails/weeloy-logo-transparent.png'));
            if (!isset($opts['imasia_label'])) {
                //$cid1 = $message->embed(Swift_Image::fromPath('https://static.weeloy.com/emails/weeloy-logo-transparent-114x57.png'));
                $cid1 = $message->embed(Swift_Image::fromPath('https://static.weeloy.com/emails/weeloy_new58x58.jpg'));
                $body = str_replace('[top_logo.png]', $cid1, $body);
            }
        }


        if (!isset($opts['white_label']) && isset($opts['guest_qrcode']) && file_exists($opts['guest_qrcode'])) {
            $cid5 = $message->embed(Swift_Image::fromPath($opts['guest_qrcode']));
            $body = str_replace('[guest_qrcode.png]', $cid5, $body);
        }

        if (!empty($opts['passbook'])) {

            //$body = $body . '<a href="'.$opts['passbook'].'" ><img src="http://www.patentlyapple.com/.a/6a0120a5580826970c01b7c7a3ca63970b-pi" /></a>';
            // $message->attach(
            //     Swift_Attachment::fromPath($opts['passbook']['name'])->setFilename('passbook.pkpass')
            // );
        }

        if (isset($opts['replyto']) && $opts['replyto'] != "") {
            $message->setReplyTo(
                    $opts['replyto']
            );
        }

        if (isset($opts['bcc'])) {
            $message->setBcc($opts['bcc']);
        }
        if (isset($opts['cc']))
            $message->setCc(array($opts['cc']));
        

        $message->setBcc(array('weeloyhistory@gmail.com'));
        $message->setBody($body, 'text/html');
        if ($mailer->send($message, $failures)) {
            //removing all passbook files
            if (!empty($opts['passbook']['name'])) {
                foreach ($opts['passbook']['path'] AS $path) {
                    if (file_exists($path)) {
                        unlink($path);
                    }
                }
                //Remove our unique temporary folder
                if (is_dir($opts['passbook']['folder'])) {
                    rmdir($opts['passbook']['folder']);
                }
            }
            return true;
        } else {
             return false;
         }
    }

    function getTemplate($template, $data, $subfolder = 'old') {
        $root_path = explode(':', get_include_path());
        $temPath = 'templates/email/';

        $loader = new Twig_Loader_Filesystem($root_path[count($root_path) - 1] . $temPath);

        $this->twig = new Twig_Environment($loader, array('debug' => true));

        $lexer = new Twig_Lexer($this->twig, array(
            'tag_comment' => array('{#', '#}'),
            'tag_block' => array('{%', '%}'),
            'tag_variable' => array('{[{', '}]}'), // was array('{{', '}}')
            'interpolation' => array('#{', '}'),
        ));
        $this->twig->setLexer($lexer);



        return $this->twig->render($template . '.html.twig', array('data' => $data));
    }

    function swifttest($recipient, $subject, $body, $opt=NULL) {
        $this->send_fromswiftmailer($recipient, $subject, $body, $opt);
    }

    function ltest() {
        $recipient = "richard@kefs.me";
        $subject = "testing mail";
        $body = "this is a test";

        $this->sendmail($recipient, $subject, $body);
    }

//philippe.benedetti@weeloy.com
    function testlocalhost() {
        $recipient = "richardkefs@gmail.com";
        $subject = "testing mail";
        $body = "this is a test";
        $headers = "From: richard@kefs.me";

        mail($recipient, $subject, $body, $headers);
    }

}

?>