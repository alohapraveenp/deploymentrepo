<?php

require_once 'lib/class.tracking.inc.php';

class WY_Report {

    function __construct() {
        
    }

    public function getReworldReport($partner) {
        $booking = new WY_Booking();
        $tracking = new WY_tracking();
        $resultats = array(
            'direct_bookings' => $booking->getBookingByTracking($partner)
            , 'number_redirect' => $tracking->getNumberRedirect($partner)
            , 'indirect_bookings' => $booking->getBookingBySource($partner)
        );

        $res = array('resultats' => $resultats);
        return $res;
    }

}

?>
