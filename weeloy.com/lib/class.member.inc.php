<?php

require_once("lib/class.booking.inc.php");
require_once("lib/class.sms.inc.php");
require_once("lib/class.restaurant.inc.php");
require_once("lib/class.login.inc.php");

define("__CALL_CENTER__", 1);
define("__RESTRICTED_RESERVATION__", 2);
define("__RESTRICTED_SUPERADMIN_COUNTRY__", 4);
define("__RESTRICTED_AVAILABILITY__", 0x10);
define("__GROUP_REPORT__", 0x20);
define("__GROUP_TMS__", 0x40);
if(!defined("__LOGIN_8HOURS__"))  define("__LOGIN_8HOURS__", 0x80);
define("__PABX__", 0x100);
if(!defined("__2FACTAUTH__")) define("__2FACTAUTH__", 0x200);
define("__RESTRICTED_CALL_CENTER__", 0x400);
define("__MGT_CALL_CENTER__", 0x800);
define("__NO_ACCESS_AVAILABILITY__", 0x1000);
define("__RESTRICTED_DSB__", 0x2000);
define("__DSB__", 0x4000);
define("__EDITPROFILES__", 0x10000);


define("__ALLOWCREATEDELETE__", 0x8000);

$permlabelAr = array(
    "perm_callcenter" => "call center",
    "perm_limited_reservation" => "restricted booking",
    "perm_limited_country" => "restricted country",
    "perm_limited_availability" => "restricted availability",
    "perm_limited_dbs" => "restricted dsb",
    "perm_noaccess_availability" => "no access availability",
    "perm_limited_callcenter" => "restricted call center",
    "perm_mgt_callcenter" => "management call center",
    "perm_group_report" => "group report",
    "perm_group_tms" => "group tms",
    "perm_8hours" => "8h login",
    "perm_editprofiles" => "edit profiles",
    "pabx" => "pabx",
    "dsb" => "dsb",
    "2factauth" => "2-fact Authentification",
    "perm_RestoCRD" => "Allow create/Restaurant"
);

$permvalueAr = array(
    "perm_callcenter" => __CALL_CENTER__,
    "perm_limited_reservation" => __RESTRICTED_RESERVATION__,
    "perm_limited_country" => __RESTRICTED_SUPERADMIN_COUNTRY__,
    "perm_limited_availability" => __RESTRICTED_AVAILABILITY__,
    "perm_limited_dbs" => __RESTRICTED_DSB__,
    "perm_noaccess_availability" => __NO_ACCESS_AVAILABILITY__,
    "perm_limited_callcenter" => __RESTRICTED_CALL_CENTER__,
    "perm_mgt_callcenter" => __MGT_CALL_CENTER__,
    "perm_group_report" => __GROUP_REPORT__,
    "perm_group_tms" => __GROUP_TMS__,
    "perm_8hours" => __LOGIN_8HOURS__, 
    "perm_editprofiles" => __EDITPROFILES__,
    "pabx" => __PABX__,
    "dsb" => __DSB__,
    "2factauth" => __2FACTAUTH__,
    "perm_RestoCRD" => __ALLOWCREATEDELETE__
);

$admin_member_type_allowed = array('visitor', 'super_weeloy', 'admin');
$backoffice_member_type_allowed = array('visitor', 'restaurant', 'weeloy_sales', 'super_weeloy', 'admin');
$super_member_type_allowed = array('super_weeloy', 'admin');
$backoffice_listing_member_type_allowed = array('restaurant', 'restaurant_booking');
$fullfeature_member_type_allowed = array('weeloy_agent', 'weeloy_admin', 'admin');
$rotation_member_type_allowed = array('visitor', 'restaurant', 'weeloy_sales', 'super_weeloy', 'admin');

class WY_Member {

    var $id;
    var $email;
    var $mobile;
    var $name;
    var $firstname;
    var $country;
    var $member;
    var $gender;
    var $address;
    var $address1;
    var $city;
    var $zip;
    var $region;
    var $state;
    var $fax;
    var $cuisine;
    var $mealtype;
    var $creditcard;
    var $pricing;
    var $stars;
    var $rating;
    var $evoucher;
    var $member_permission;
    var $status;
    var $salutation;
    var $membercode;
    var $result;
    var $msg;

    function __construct($user = NULL) {

	$this->result = 1;
	$this->msg = "";
        if (AWS) {
            $this->globalshow = __S3HOST__ . __S3DIR__;
        } else {
            $this->globalshow = __SHOWDIR__;
        }
        if (!empty($user)) {
            $this->email = $user;
        }
    }

    function getMember($theMember) {
        $data = pdo_single_select("SELECT 
                        id, mobile, email, name, firstname, email, country, member, gender,salutation,address, address1, city, zip, region, state, fax, cuisine, mealtype, creditcard, pricing, stars, rating, evoucher, member_permission, member_type, status,membercode
                        from member WHERE email = '$theMember' LIMIT 1");

        if (count($data) < 1)
            return $data;

        $this->id = $data['id'];
        $this->email = $data['email'];
        $this->mobile = $data['mobile'];
        $this->name = $data['name'];
        $this->firstname = $data['firstname'];
        $this->email = $data['email'];
        $this->country = $data['country'];
        $this->member = $data['member'];
        $this->gender = $data['gender'];
        $this->address = $data['address'];
        $this->address1 = $data['address1'];
        $this->city = $data['city'];
        $this->zip = $data['zip'];
        $this->region = $data['region'];
        $this->state = $data['state'];
        $this->fax = $data['fax'];
        $this->cuisine = $data['cuisine'];
        $this->mealtype = $data['mealtype'];
        $this->creditcard = $data['creditcard'];
        $this->pricing = $data['pricing'];
        $this->stars = $data['stars'];
        $this->rating = $data['rating'];
        $this->evoucher = $data['evoucher'];
        $this->member_permission = $data['member_permission'];
        $this->status = $data['status'];
        $this->salutation = $data['salutation'];
        $this->membercode = $data['membercode'];

        return $data;
    }

    function deleteMember($email) {
    	$email = clean_input($email);
    	if(empty($email)) 
    		return $this->result = -1;

        pdo_exec("DELETE from member where email = '$email' limit 1");
        pdo_exec("DELETE from login where email = '$email' limit 1");
    }

    function getAllMember() {
        $data = pdo_multiple_select("SELECT email, name as lastname, firstname, country, member_type as type FROM member where  status != 'deleted' order by name");
        $this->result = (count($data) < 1) ? -1 : 1;
        return $data;
    }
    
    function getListMember() {

        $this->email = array();
        $this->name = array();
        $this->firstname = array();

        $data = pdo_multiple_select("SELECT email, name, firstname, country FROM member order by name");
        if (count($data) < 1)
            return;

        foreach ($data as $row) {
            $this->email[] = $row['email'];
            $this->name[] = $row['name'];
            $this->firstname[] = $row['firstname'];
            $this->country[] = $row['country'];
        }
    }

    function read_permission($email) {

        $data = pdo_single_select("SELECT member_permission FROM member where email = '$email' limit 1");
        if (count($data) > 0)
            return intval($data['member_permission']);
        return 0;
    }

    function readPermissionValue() {
    	global $permlabelAr, $permvalueAr; 

	$data = array();
	reset($permlabelAr);
	while(list($label, $value) = each($permlabelAr)) {
		$data[$value] = $permvalueAr[$label];
		}

        return $data;
    }

    function is_restrictedCountry($email) {
        return ($this->read_permission($email) & __RESTRICTED_SUPERADMIN_COUNTRY__);
    }

    function is_allowCreateResto($email) {
        return ($this->read_permission($email) & __ALLOWCREATEDELETE__);
    }

    function write_permission($email, $permission) {

        pdo_exec("update member set member_permission = '$permission' where email = '$email' limit 1");
    }

    function getListMemberRestaurant() {

        $this->email = array();
        $this->name = array();
        $this->firstname = array();
        $this->country = array();

        $data = pdo_multiple_select("SELECT email, name, firstname, country, mobile, gender, salutation FROM member where member_type != 'member' order by name");
        if (count($data) < 1)
            return;

        foreach ($data as $row) {
            $this->email[] = $row['email'];
            $this->name[] = $row['name'];
            $this->firstname[] = $row['firstname'];
            $this->country[] = $row['country'];
        }
    }
    function getListMemberRestaurantNew() {

        $this->email = array();
        $this->name = array();
        $this->firstname = array();
        $this->country = array();

        $data = pdo_multiple_select("SELECT email, name as lastname, firstname, country,status, member_type, member_permission, mobile, gender, salutation FROM member where member_type != 'member' order by name");
        return $data ;
    }

    function readMemberResto($email) {	
	$email = clean_input($email);
	if(empty($email)) 
		return $this->result = -1;

	return pdo_multiple_select("SELECT restaurant_id FROM restaurants_managers where member_id= '$email' order by restaurant_id");
    }

    function updateMemberPermission($member_id, $obj) {
	$member_id = clean_input($member_id);
	if(empty($member_id)) 
		return $this->result = -1;
		
	$valid = array("lastname", "firstname", "country", "status", "member_type", "member_permission");
	while(list($field, $val) = each($obj)) {
		if(in_array($field, $valid)) {
			error_log('PERM ' . $field . " = " . $val);
			$$field = clean_input($val);
			}
		}

        pdo_exec("update member set name = '$lastname', firstname = '$firstname', country = '$country', status = '$status', member_type = '$member_type', member_permission = '$member_permission' where email = '$member_id' limit 1");
        pdo_exec("delete from restaurants_managers where member_id = '$member_id' ");

	if(isset($obj['resto']) && count($obj['resto']) > 0) {
		foreach ($obj['resto'] as $restaurant) {
			pdo_exec("INSERT INTO restaurants_managers (member_id, restaurant_id, status) VALUES ('$member_id', '$restaurant', 'active')");
			}		
		}

	return $this->result = 1;
    }
				
    function addMembersRestaurants($member_id, $restaurantAr) {
        foreach ($restaurantAr as $restaurant) {
            $Sql = "INSERT INTO restaurants_managers (member_id, restaurant_id, status)
                    VALUES ('$member_id', '$restaurant', 'active') ON DUPLICATE KEY UPDATE status = 'active' ";
            pdo_exec($Sql);
	return $this->result = 1;
        }
    }

    function deleteMembersRestaurants($member_id, $restaurantAr) {
        foreach ($restaurantAr as $restaurant) {
            $Sql = "INSERT INTO restaurants_managers (member_id, restaurant_id, status)
                    VALUES ('$member_id', '$restaurant', 'deleted') ON DUPLICATE KEY UPDATE status = 'deleted' ";
            pdo_exec($Sql);
	return $this->result = 1;
        }
    }

    // function addAppManagerRestaurants($user_id, $restaurantAr) {
    //     foreach ($restaurantAr as $restaurant) {
    //         $Sql = "INSERT INTO restaurant_app_managers (user_id, restaurant_id, status)
    //                 VALUES ('$user_id', '$restaurant', 'active') ON DUPLICATE KEY UPDATE status = 'active' ";
    //         pdo_exec($Sql);
    //     }
    // }

    // function deleteAppManagerRestaurants($user_id, $restaurantAr) {
    //     foreach ($restaurantAr as $restaurant) {
    //         $Sql = "INSERT INTO restaurant_app_managers (user_id, restaurant_id, status)
    //                 VALUES ('$user_id', '$restaurant', 'deleted') ON DUPLICATE KEY UPDATE status = 'deleted' ";
    //         pdo_exec($Sql);
    //     }
    // }

    function saveNewsletterAddress($email) {
        $Sql = "INSERT INTO members_newsletter (email, status)
                    VALUES ('$email', 'active') ON DUPLICATE KEY UPDATE status = 'active' ";
        return pdo_exec($Sql);
//return true;
    }

    function getIdByEmail($email) {
        $sql = "SELECT ID from member WHERE email ='$email'";
        $data = pdo_single_select($sql);
        return $data['ID'];
    }
    
    
    function getNameById($user_id) {
        $data = pdo_single_select("SELECT firstname, name from member WHERE ID ='$user_id' limit 1");
        if(!isset($data['firstname']))
        	return "";
        	
        $name = $data['firstname'] ." ".$data['name'];
        return $name;
    }

    function partialCreateMember($type, $gender, $first, $name, $mobile, $country, $email) {
        pdo_insert("INSERT INTO `login` (`ID`, `Email`, `Password`, `tmpPassword`, `tmpTimeStamp`, `Token`, `TheTS`, `applitoken`, `appliname`, `userid`, `facebookid`, `UI`, `oldID`, `stateIMP`) VALUES (NULL, '$email', 'no_pass_please_reset_pass', NULL, CURRENT_TIMESTAMP, NULL, '0000-00-00 00:00:00.000000', '', '', '', '', NULL, NULL, NULL);");
        return pdo_insert("INSERT into member (member_type, gender, firstname, name, mobile, country, email) values ('$type', '$gender', '$first', '$name', '$mobile', '$country', '$email')");
    }

    function partialUpdateMember($type, $gender, $first, $name, $mobile, $country, $email) {

        $Sql = "UPDATE member SET 
                member_type = '$type',
                gender = '$gender',
                firstname = '$first', 
                name = '$name', 
                mobile = '$mobile', 
                country = '$country' WHERE email = '$email' limit 1";

        return pdo_exec($Sql);
    }

    function updateMember() {

        $Sql = "UPDATE member SET 
                gender = '$this->gender', 
                email = '$this->email',
                firstname = '$this->firstname', 
                name = '$this->name', 
                mobile = '$this->mobile', 
                country = '$this->country',
                member = '$this->member',
                address = '$this->address',
                address1 = '$this->address1',
                city = '$this->city',
                zip = '$this->zip',
                region = '$this->region',
                state = '$this->state',
                fax = '$this->fax',
                cuisine = '$this->cuisine',
                mealtype = '$this->mealtype',
                creditcard = '$this->creditcard',
                pricing = '$this->pricing',
                stars = '$this->stars',
                rating = '$this->rating',
                evoucher = '$this->evoucher',   
                status = '$this->status',
                membercode = '$this->membercode',
                salutation ='$this->salutation'
                WHERE email = '$this->email'";
        return pdo_exec($Sql);
//return true;
    }

    function getLoginDetails($email) {
        $data = pdo_single_select("SELECT * FROM login WHERE email = '$email' limit 1");
        return $data;
    }

    public function setFavoriteRestaurant($restaurant, $is_favorite) {
        $sql = "INSERT into members_restaurants_favorite (member, restaurant, is_favorite) VALUES ('$this->email','$restaurant', '$is_favorite') ON DUPLICATE KEY UPDATE is_favorite = '$is_favorite' ";
        return pdo_exec($sql);
    }

    public function getBooking($date1 = NULL, $date2 = NULL, $sort = 'ASC', $tracking = 'no_white_label', $pending_reviews_popup = NULL) {
 
        $booking = new WY_Booking;
        $res = new WY_restaurant;
        $limit = '';
        if (empty($pending_reviews_popup) && empty($date1) && empty($date2)) {
            $date1 = date('Y-m-d', time() - 3600 * 24 * 30);
            $date2 = date('Y-m-d', time() + 3600 * 24 * 84);
        }

        $date_now = date('Y-m-d');
        $date_90 = date('Y-m-d', strtotime("-90 days"));
  
        $where = " AND rdate > :date1 AND rdate < :date2  ";
        if (isset($pending_reviews_popup)) {
            $where = " and ((rest.is_wheelable = '1' and b.wheelwin != '') 
                    or  (rest.is_wheelable = '0' and b.rdate < '$date_now' and b.rdate > '$date_90'))
                    AND b.review_status IS NULL AND b.status = '' ";
            $limit = " LIMIT $pending_reviews_popup";
        }

        if ($tracking == 'no_white_label') {
            $where .= " AND tracking NOT LIKE '%WEBSITE%' AND tracking NOT LIKE '%callcenter%'  AND tracking NOT LIKE '%remote%' AND tracking NOT LIKE '%facebook%' AND tracking NOT LIKE '%GRABZ%' ";
        }

        $sql = "SELECT b.ID, b.confirmation, b.tracking, b.type, b.restaurant, rest.title as title,rest.cuisine, "
                . "rest.is_wheelable, m.name as image, m.morder, rest.address, rest.address1, rest.zip, rest.city,  rest.country,  rest.dfminpers, rest.dfmaxpers, rest.GPS, rest.map, rest.currency, rest.booking_deposit_lunch, rest.booking_deposit_dinner,"
                . "b.status, b.restCode, b.membCode, b.email, b.mobile, b.salutation, b.lastname, b.firstname, b.cdate, b.rdate, b.rtime, "
                . "b.cover, b.revenue, b.specialrequest,rest.mealtype, b.wheelsegment,wheelwin, b.wheeldesc, b.spinsource, b.browser, b.language, b.country, b.ip,b.booking_deposit_id,"
                . "r.reviewgrade, r.foodgrade, r.ambiancegrade, r.servicegrade, r.pricegrade, r.comment, r.response_to, r.reviewdtcreate "
                . " FROM booking b "
                . "LEFT JOIN restaurant rest ON b.restaurant = rest.restaurant "
                . "LEFT JOIN media m ON m.restaurant = rest.restaurant "
                . "LEFT JOIN review r ON b.confirmation = r.confirmation "
                . "WHERE b.email=:query  AND m.object_type = 'restaurant' AND m.media_type = 'picture' $where GROUP BY confirmation ORDER BY rdate $sort, m.morder ASC $limit";


        try {
            $db = getConnection();
            $stmt = $db->prepare($sql);

            $stmt->bindParam("query", $this->email);
            if (!empty($date1)) {
                $stmt->bindParam("date1", $date1);
            }
            if (!empty($date2)) {
                $stmt->bindParam("date2", $date2);
            }

            $stmt->execute();
            $bookings_data = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $db = null;
        } catch (Exception $ex) {
            var_dump($ex);
        }
        $bookings = array();
        $bookingsAr = array();
        $bkglabel = array('ID', 'confirmation', 'tracking', 'restaurant', 'title', 'cuisine', 'is_wheelable', 'status', 'restCode', 'membCode', 'address', 'address1', 'zip', 'city', 'country',  'dfminpers', 'dfmaxpers', 'GPS', 'map', 'currency', 'email', 'mobile', 'salutation', 'lastname', 'firstname', 'cdate', 'rdate', 'rtime', 'cover', 'revenue', 'specialrequest', 'mealtype', 'wheelsegment', 'wheelwin', 'wheeldesc', 'spinsource', 'browser', 'language', 'country', 'ip', 'image', 'morder', 'is_wheelable');
        $reviewlabel = array('reviewgrade', 'foodgrade', 'ambiancegrade', 'servicegrade', 'pricegrade', 'comment', 'reviewdtcreate');
        foreach ($bookings_data as $data) {
            $reviewgrade = $data['reviewgrade'];
            $response_to = $data['response_to'];

            foreach ($data as $key => $value) {
                if (in_array($key, $bkglabel)){
                    $bookings[$key] = $value;
                }
                else if (!in_array($key, $reviewlabel) || $reviewgrade == NULL){
                    continue;
                }
                else if (!$response_to){
                    $bookings['review'][$key] = $value;
                    }
                else if ($response_to){
                    $bookings['review_response'][$key] = $value;
                }
            }

            if ($reviewgrade == NULL || $response_to){
                $bookings['review'] = NULL;
            }
            if ($reviewgrade == NULL || !$response_to){
                $bookings['review_response'] = NULL;
            }

            
            // use it for cpp text
            $bookings['booking_type'] = $data['tracking'];
            $bookings['booking_type_text'] = '';
            if($bookings['is_wheelable']){
                if(preg_match("/cpp_credit_suisse/i", $data['tracking']) == true){
                    $bookings['booking_type'] = 'cpp_credit_suisse';
                    $bookings['booking_type_text'] = 'Spin the CPP wheel now';
                }else{
                    $bookings['booking_type'] = 'booking';
                    $bookings['booking_type_text'] = 'Spin the wheel now';
                }
            }
            
            $bookings['is_customer_wheelable'] = $bookings['is_wheelable'];
            
            date_default_timezone_set('Asia/Singapore');
            //date_default_timezone_set('UTC');
            $now = strtotime(date("Y-m-d H:i:s"));
            $bck_date = date($data['cdate']);
            $diff = strtotime('+15 minutes', strtotime(date($data['cdate'])))  - $now ;
            date_default_timezone_set( 'UTC' );
             $deposit = null;
             $payment =null;
             $time_left = 0;
             
            if($bookings['status'] == 'pending_payment'){
                $amount = '100';
                $booking_time_ar = explode(':', $data['rtime']);
                //dinner 
                if( 5 < $booking_time_ar[0] && $booking_time_ar[0] < 16 ){
                    $amount = $data['cover'] * $data['booking_deposit_lunch'];
                }else{
                    $amount = $data['cover'] * $data['booking_deposit_dinner'];
                }
                if($diff > 0){
                    $time_left = $diff;
                }else{
                    continue;
                }
                
                
                 $payment = $res->chkPaymentInfo($data['restaurant']);
                 $payment['payment_id'] =null;
                 $payment['payment_reference_id'] =$data['booking_deposit_id'];
                 $payment['amount'] =null;
                 $payment['suggested_amount'] ="$amount";
                 $payment['payment_method'] =null;
                 $payment['status'] ='pending_payment';
                 $payment['cancelled_charge'] =null;
                 $payment['currency'] = $data['currency'];
                 $payment['refund']=null;
                 $payment['time_left'] =$time_left;
            
            }

            if (!empty($data['booking_deposit_id']) && $data['booking_deposit_id'] !== NULL) {
                if($bookings['status'] != 'pending_payment'){
                    $cancel_policy = $booking->getBookingPaymentInfo($data['booking_deposit_id'],$data['confirmation'],$bookings['status']);
                    if(count($cancel_policy)>0){
                        $payment = $cancel_policy;
                    }
                }
            }

            $bookings['payment'] = $payment;


            
            $bookingsAr[] = $bookings;
            $bookings = array();
        }
        return $bookingsAr;
    }

    public function getBookingsUserFilter($user, $filter, $date1, $date2, $booking_status = NULL) {
        $where = '';
        if ($date1 == '2013-12-01') {
            $date1 = date('Y-m-d', time() - 3600 * 24);
        }
        if ($date2 == '2015-12-01') {
            $date2 = date('Y-m-d', time() + 3600 * 24 * 1000);
        }

        if ($date1 == 'START_DATE') {
            $date1 = date('Y-m-d', time());
            $date2 = date('Y-m-d', time() + 3600 * 24 * 7);
        }

        if ($filter == 'all') {
            $where = '';
        } else {
            if ($filter == 'not_spun') {
                $where = ' AND (wheelwin IS NULL OR wheelwin LIKE "") ';
            } else {
                $where = ' AND (wheelwin IS NOT NULL && wheelwin NOT LIKE "") ';
            }
        }
        if (!empty($booking_status)) {
            $where .= " AND b.status NOT LIKE 'cancel' ";
        }
        $selectresto = "b.restaurant='" . $user . "'";


        $sql = "SELECT b.ID, b.confirmation, b.type, b.restaurant, rest.title as title, rest.address, rest.address1, rest.zip, rest.city, rest.country, rest.dfminpers, rest.dfmaxpers, b.status, b.restCode, b.membCode, b.email, b.mobile, b.salutation, b.lastname, b.firstname, b.cdate, b.rdate, b.rtime, "
                . "b.cover, b.revenue, b.specialrequest,rest.mealtype, b.wheelsegment,wheelwin, b.wheeldesc, b.spinsource, b.browser, b.language, b.country, b.ip, b.tracking as tracking, "
                . "r.reviewgrade, r.foodgrade, r.ambiancegrade, r.servicegrade, r.pricegrade, r.comment, r.response_to, r.reviewdtcreate, m.name as image, m.morder"
                . " FROM booking b "
                . "LEFT JOIN restaurant rest ON b.restaurant = rest.restaurant "
                . "LEFT JOIN media m ON m.restaurant = rest.restaurant "
                . "LEFT JOIN review r ON b.confirmation = r.confirmation "
                . "WHERE type = 'booking' AND " . $selectresto . " "
                . "AND rdate > :date1 AND rdate < :date2 $where "
                . "AND m.object_type = 'restaurant' "
                . "AND m.media_type = 'picture' GROUP BY confirmation ORDER BY rdate ASC, m.morder ASC ";




        $db = getConnection();
        $stmt = $db->prepare($sql);
//$stmt->bindParam("query", $user);
        $stmt->bindParam("date1", $date1);
        $stmt->bindParam("date2", $date2);
        $stmt->execute();
        $data_sql = $stmt->fetchAll(PDO::FETCH_BOTH);
        $db = null;

        $bookingsAr = array();
        foreach ($data_sql as $data) {
            // white label tag
            $is_whitelabel = "0";
            if (preg_match("/CALLCENTER|WEBSITE|facebook|GRABZ/", $data['tracking']) == true ){
                $is_whitelabel = "1";
            }
            $is_wheelable = "1";
            if ($is_whitelabel ){
                $is_wheelable = "0";
            }
            $bookings['ID'] = $data['ID'];
            $bookings['confirmation'] = $data['confirmation'];
            $bookings['restaurant'] = $data['restaurant'];
            $bookings['title'] = $data['title'];
            $bookings['status'] = $data['status'];
            $bookings['restCode'] = $data['restCode'];
            $bookings['membCode'] = $data['membCode'];
            $bookings['address'] = $data['address'];
            $bookings['address1'] = $data['address1'];
            $bookings['min_pax'] = $data['dfminpers'];
            $bookings['max_pax'] = $data['dfmaxpers'];
            $bookings['zip'] = $data['zip'];
            $bookings['city'] = $data['city'];
            $bookings['country'] = $data['country'];
            $bookings['image'] = $data['image'];
            $bookings['email'] = $data['email'];
            $bookings['mobile'] = $data['mobile'];
            $bookings['salutation'] = $data['salutation'];
            $bookings['lastname'] = $data['lastname'];
            $bookings['firstname'] = $data['firstname'];
            $bookings['cdate'] = $data['cdate'];
            $bookings['rdate'] = $data['rdate'];
            $bookings['rtime'] = $data['rtime'];
            $bookings['cover'] = $data['cover'];
            $bookings['revenue'] = $data['revenue'];
            $bookings['specialrequest'] = $data['specialrequest'];
            $bookings['mealtype'] = $data['mealtype'];
            $bookings['is_whitelabel'] = $is_whitelabel;
            $bookings['is_wheelable'] = $is_wheelable;
            $bookings['is_customer_wheelable'] = $is_wheelable;
            $bookings['wheelsegment'] = $data['wheelsegment'];
            $bookings['wheelwin'] = $data['wheelwin'];
            $bookings['wheeldesc'] = $data['wheeldesc'];
            $bookings['spinsource'] = $data['spinsource'];
            $bookings['browser'] = $data['browser'];
            $bookings['language'] = $data['language'];
            $bookings['country'] = $data['country'];
            $bookings['ip'] = $data['ip'];
            if ($data['reviewgrade'] != NULL && !$data['response_to']) {
                $bookings['review']['reviewgrade'] = $data['reviewgrade'];
                $bookings['review']['foodgrade'] = $data['foodgrade'];
                $bookings['review']['ambiancegrade'] = $data['ambiancegrade'];
                $bookings['review']['servicegrade'] = $data['servicegrade'];
                $bookings['review']['pricegrade'] = $data['pricegrade'];
                $bookings['review']['comment'] = $data['comment'];
                $bookings['review']['reviewdtcreate'] = $data['reviewdtcreate'];
            } else {
                $bookings['review'] = NULL;
            }

            if ($data['reviewgrade'] != NULL && $data['response_to']) {
                $bookings['review_response']['reviewgrade'] = $data['reviewgrade'];
                $bookings['review_response']['foodgrade'] = $data['foodgrade'];
                $bookings['review_response']['ambiancegrade'] = $data['ambiancegrade'];
                $bookings['review_response']['servicegrade'] = $data['servicegrade'];
                $bookings['review_response']['pricegrade'] = $data['pricegrade'];
                $bookings['review_response']['comment'] = $data['comment'];
                $bookings['review_response']['reviewdtcreate'] = $data['reviewdtcreate'];
            } else {
                $bookings['review_response'] = NULL;
            }

            // use it for cpp text
            $bookings['booking_type'] = $data['tracking'];
            
            $bookings['booking_type_text'] = '';
            if($is_wheelable){
                if(preg_match("/cpp_credit_suisse/i", $data['tracking']) == true){
                    $bookings['booking_type'] = 'cpp_credit_suisse';
                    $bookings['booking_type_text'] = 'Spin the CPP wheel now';
                }else{
                    $bookings['booking_type'] = 'booking';
                    $bookings['booking_type_text'] = 'Spin the wheel now';
                }
            }
            
            $bookingsAr[] = $bookings;
            $bookings = array();
        }
        return $bookingsAr;
    }

    public function addEmailNewsletter($email) {
        $sql = "INSERT INTO members_newsletter (email, status) VALUES ('$email', 'active') ON DUPLICATE KEY UPDATE status = 'active';";
        return pdo_exec($sql);
    }

    public function sendContactEmail($email, $message, $firstname, $lastname) {
        
        $email =  filter_var($email, FILTER_SANITIZE_EMAIL);
        $message =  filter_var($message, FILTER_SANITIZE_STRING);
        
        $body = "Message form " . $firstname . " " . $lastname . "<br/>"
                . $email . "<br/>"
                . $message;

        $mailer = new JM_Mail();
        $email_from = 'info@weeloy.com';
        $email_to = 'info@weeloy.com';

        $opts['from'] = array($email_from => 'Info Weeloy.com');
        $opts['replyto'] = $email;

        return $mailer->sendmail($email_to, "Weeloy - Contact support", nl2br($body), $opts, NULL, 'supportcntct');
    }
    
    public function sendWebsiteContactEmail($email_client, $email_to, $message, $firstname, $lastname) {
        
        $email_client =  filter_var($email_client, FILTER_SANITIZE_EMAIL);
        $email_to =  filter_var($email_to, FILTER_SANITIZE_EMAIL);
        $message =  filter_var($message, FILTER_SANITIZE_STRING);
        
        
        $body = "Message form " . $firstname . " " . $lastname . "<br/>"
                . $email_client . "<br/>"
                . $message;

        $mailer = new JM_Mail();
        
        $email_from = 'info@weeloy.com';
        $opts['from'] = array($email_from => 'Info Weeloy.com');
        
        $opts['replyto'] = $email_client;

        return $mailer->sendmail($email_to, "Message form " . $firstname . " " . $lastname, nl2br($body), $opts, NULL, 'supportcntct');
    }


    public function getMemberId($email) {
        $sql = "SELECT ID from member WHERE email LIKE '$email'";
        return pdo_single_select($sql);
    }

    public function updateProfile($email, $label, $value) {
        $sql = "UPDATE member SET $label=:value WHERE email=:email limit 1";
        try {
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam("email", $email);
            $stmt->bindParam("value", $value);
            $obj = $stmt->execute();
            if ($obj) {
                return true;
            }
            $db = null;
        } catch (PDOException $e) {
             return false;
        }
    }
    public function createUserAcl($email,$role,$acl) {
        $sql = "INSERT INTO user_acl (email, role,access_level) VALUES ('$email', '$role','$acl') ON DUPLICATE KEY UPDATE email = '$email';";
        return pdo_exec($sql);
        
    }
    
    public function getUserAcl() {
        return pdo_multiple_select("select * from user_acl where email !='' ");
	}
    
    public function updateUserAcl($id, $email, $role, $acl){
         return pdo_exec("UPDATE user_acl SET email = '$email', role ='$role', access_level='$acl' where email = '$email' limit 1");
	}
    
    public function deleteUserAcl($email){
         return pdo_exec("DELETE from user_acl where email = '$email' limit 1");
	}
    
    public function getUserPermission($email){
        $data = pdo_single_select("select access_level from user_acl where email='$email' limit 1 ");
        return (isset($data['access_level'])) ? $data['access_level'] : "";
	}
    
    public function getUserAclEmail($role){
	$roles = explode(",",$role);
	$in_values = '"' . implode('","', $roles) . '"';
	return pdo_multiple_select("select email from user_acl where role  IN ($in_values) ");
	}
    
    public function getSuperweeloyMember(){
        return pdo_multiple_select("select email from member where member_type IN ('super_weeloy','admin') ");
    	}
    
    public function getUserRole($email){
        $data = pdo_single_select("select role from user_acl where email ='$email' limit 1");
        return (count($data)>0) ? $data['role'] : $data; 
    	}

    public function updateuserMembercode($email,$membercode){
	$sms = new JM_Sms();
	$booking = new WY_Booking();
	$booking->updatemembercode($email,$membercode);
	$recipient_mobile = $this->mobile;
	$smsid = "Weeloy";
	$sms_message = '[NOREPLY] Dear '. $this->firstname . ' '. $this->name.' Your New  Membercode  ' . $membercode ;
	$sms->sendSmsMessage($recipient_mobile, $sms_message, $smsid, '', '');
	return 1;
	}
        
    function getMemberDetails($email,$password = NULL){
       
 		$lg = new WY_Login('backoffice');
 		return $lg->getLogindetails($email,$password);
    	}
        
   function savecallcentermember($restaurant,$data){
       $restaurantAr = array($restaurant);
       $this->addMembersRestaurants($data['email'], $restaurantAr) ;
       $isMember = $this->getMemberId($data['email']);

        $data['action'] = (count($isMember)>0) ? 'update' : 'create'; 
       if($data['action'] === 'create'){
            $this->partialCreateMember('callcenter', '', $data['firstname'], $data['lastname'], $data['mobile'], '', $data['email']);
        }else{
            $this->partialUpdateMember('callcenter', '', $data['firstname'], $data['lastname'], $data['mobile'], '', $data['email']);
        }
        return $this->result;
    }
    function getCallMemberDetails($restaurant){
        
         $sql = "SELECT email,firstname,name as lastname,mobile,restaurants_managers.status FROM member,restaurants_managers where member.email = restaurants_managers.member_id  and member_type='callcenter' and restaurants_managers.restaurant_id = '$restaurant' and restaurants_managers.status = 'active' order by ID DESC";
        return pdo_multiple_select($sql);
        return pdo_multiple_select("select * from callcenter_member where restaurant ='$restaurant' AND status !='deleted' ");
    }
    function deleteccMember($email,$restaurant){
        error_log("MEMBER" .$email . "Restaurnat" .$restaurant);
         $restaurantAr = array($restaurant);
         $this->deleteMembersRestaurants($email, $restaurantAr);
         $this->deleteMember($email);
        return $this->result;
        
    }
       

}

?>
