<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once 'lib/class.restaurant.inc.php';
require_once("lib/class.analytics.inc.php");
require_once("conf/conf.session.inc.php");

class WY_Restaurant_Section extends WY_Restaurant {

    public function getRestaurantSectionConfig($restaurant) {

        if (empty($restaurant)) {
            return false;
        }
        
        $sectionConf = array();
        $data = '';
        $sql = "SELECT s.id, s.product as name, s.description, s.min_pax, s.max_pax, s.require_confirmation, s.require_confirmation_text, s.open_time, s.open_time_extended, s.additional_info, c.duration as cancel_policy, c.percentage, c.lunch_charge, c.dinner_charge FROM ( SELECT id,product,restaurant,description,min_pax,max_pax,require_confirmation,require_confirmation_text,open_time,open_time_extended,additional_info FROM restaurant_section_config WHERE restaurant ='$restaurant'  GROUP BY product, restaurant ) AS s LEFT JOIN ( SELECT duration,percentage, product,restaurant,lunch_charge,dinner_charge FROM restaurant_cancel_policy WHERE restaurant ='$restaurant' GROUP BY product, restaurant ) AS c ON c.product= s.product ORDER BY s.id ASC";
        //$sql = "SELECT rsc.id, rsc.product as name, rsc.description, rsc.min_pax, rsc.max_pax, rsc.require_confirmation, rsc.require_confirmation_text, rsc.open_time, rcp.duration as cancel_policy, rcp.percentage, rcp.lunch_charge, rcp.dinner_charge FROM restaurant_section_config rsc, restaurant_cancel_policy rcp WHERE rsc.restaurant = rcp.restaurant AND rsc.product = rcp.product AND rsc.restaurant = '$restaurant' GROUP BY rcp.restaurant, rcp.product ORDER BY rsc.id ASC";
        $data = pdo_multiple_select($sql);
        $j = 1;
        foreach ($data as $con) {
            $time = json_decode($con['open_time'], true);
            
            $i = 1;
            
            $con['require_confirmation'] = ($con['require_confirmation']) ? true : false;
            
            foreach ($time as $key => $value) {
                $cancel_charge = ($key == 'lunch') ? $con['lunch_charge'] : $con['dinner_charge'];
                $open_hour = array(
                    'id' => $i,
                    'name' => $key,
                    'times' => explode(',', $value),
                    'cancel_charge' => intval($cancel_charge)
                );
                $times[] = $open_hour;
                $i++;
            }
            
            $time_extended = json_decode($con['open_time_extended'], true);
			if(empty($time_extended))
				break;
				
            $i = 1;
            
            
            foreach ($time_extended as $key => $value) {
                $cancel_charge = ($key == 'lunch') ? $con['lunch_charge'] : $con['dinner_charge'];
                $open_hour = array(
                    'id' => $i,
                    'name' => $key,
                    'times' => explode(',', $value),
                    'cancel_charge' => intval($cancel_charge)
                );
                $times_extended[] = $open_hour;
                $i++;
            }
            
            $con['id'] = $j;
            $con['cancel_policy'] = intval($con['cancel_policy']);
            $con['open_hour'] = $times;
            $con['open_hour_extended'] = $times_extended;
            $con['additional_info'] = json_decode($con['additional_info'], true);
            $times = [];
            unset($con['lunch_charge']);
            unset($con['dinner_charge']);
            unset($con['open_time']);
            unset($con['percentage']);
            $sectionConf[] = $con;
            $j++;
        }

        return $sectionConf;
    }
    public function getSectionByProduct($product,$restaurant){
        if (empty($product)) {
            return false;
        }
        $sql = "SELECT  * FROM restaurant_section_config WHERE product ='$product' and restaurant ='$restaurant' ";
        return  $data = pdo_single_select($sql);
        
    }
    public function saveSection($data){

         $restaurant = $data['restaurant'];
         $product = $data['product'];
         $description = $data['description'];
         $min_pax = $data['min_pax'];
         $max_pax = $data['max_pax'];
         $require_confirmation = $data['require_confirmation'];
         $require_confirmation_text = $data['require_confirmation_text'];
         $additional_info = $data['additional_info'];

         $sql = "SELECT restaurant FROM restaurant_section_config WHERE product ='$product' and restaurant ='$restaurant' limit 1";
         $cn = pdo_single_select($sql);

         $res='';
         $open_time= array(
             'lunch'=>$data['lunch'],
             'dinner'=>$data['dinner']
         );
         $open_time = json_encode($open_time);

         if(count($cn)>0){
              $sql = "update restaurant_section_config set description='$description',min_pax='$min_pax',max_pax='$max_pax',require_confirmation='$require_confirmation',require_confirmation_text='$require_confirmation_text',open_time='$open_time',additional_info='$additional_info' where restaurant = '$restaurant' and product='$product' limit 1";
              
              $res = pdo_exec($sql); 
         }else{
            $sql = "INSERT INTO restaurant_section_config (restaurant,product,description,min_pax,max_pax,require_confirmation,require_confirmation_text,open_time,additional_info) VALUES ('$restaurant','$product','$description','$min_pax','$max_pax','$require_confirmation','$require_confirmation_text','$open_time','$additional_info')";
            $res = pdo_exec($sql); 
         }

       return $res;

        
    }

}
