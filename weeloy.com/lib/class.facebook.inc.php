<?php

require_once 'composer/vendor/facebook/php-sdk-v4/autoload.php';
require_once 'conf/conf.init.inc.php';

use Facebook\FacebookCanvasLoginHelper;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;
use Facebook\FacebookRequestException;
use Facebook\FacebookSession;

class WY_Facebook {

	function __construct($app_id = null, $secret = null) {
		self::init($app_id, $secret);
	}

	function init($app_id = null, $secret = null) {

/*		if (!defined('WEBSITE_FB_APP_ID') && !defined('WEBSITE_FB_APP_SECRET')) {
			die('WEBSITE_FB_APP_ID and WEBSITE_FB_APP_SECRET are not defined, please check facebook config');
		} else {
			$app_id = WEBSITE_FB_APP_ID;
			$secret = WEBSITE_FB_APP_SECRET;
		}
*/
		FacebookSession::setDefaultApplication($app_id, $secret);
	}

	public function getSession($echo = false) {
		// login helper with redirect_uri

		$helper = new FacebookRedirectLoginHelper(HTTP);

		try {echo 2;
			$session = $helper->getSessionFromRedirect();
                        echo 3;
		} catch (FacebookRequestException $ex) {
			// When Facebook returns an error
		} catch (Exception $ex) {
			// When validation fails or other local issues
		}

		// see if we have a session
		if (isset($session)) {
			// graph api request for user data
			$request = new FacebookRequest($session, 'GET', '/me');
			$response = $request->execute();
			// get response
			$graphObject = $response->getGraphObject();

			// print data
			if ($echo) {
				echo '<pre>' . print_r($graphObject, 1) . '</pre>';
			}
		} else {
			// show login url
			if ($echo) {
				echo '<a href="' . $helper->getLoginUrl() . '">Login</a>';
			}
		}
	}

	function loginCanvas() {
		$helper = new FacebookCanvasLoginHelper();
		try {
			$session = $helper->getSession();
		} catch (FacebookRequestException $ex) {
			// When Facebook returns an error
		} catch (\Exception $ex) {
			// When validation fails or other local issues
		}
		if ($session) {
			return $session;
		}
		return false;
	}

	function isLoggedIn() {
		$helper = new FacebookRedirectLoginHelper(HTTP);

		try {
			$session = $helper->getSessionFromRedirect();
		} catch (FacebookRequestException $ex) {
			// When Facebook returns an error
		} catch (Exception $ex) {
			// When validation fails or other local issues
		}
                
                

		// see if we have a session
		if (isset($session)) {
			// graph api request for user data
			$request = new FacebookRequest($session, 'GET', '/me');
			$response = $request->execute();
			// get response
			$graphObject = $response->getGraphObject();

			return $graphObject;
		}

		return false;
	}

}
