<?php
require_once "lib/class.paypal.inc.php";
class WY_Invoice {

    use CleanIng;

	const PERBOOKING = 1;
	const PERPAX = 2;
	const WEBSITEBOOKING = 4; 
	const WEBSITEPAX = 8;
	const NOTINVOICE = 0x10;
	const MRS = 0x20;
	const MCC = 0x40;
	const MTMS = 0x80;
	const MMKG = 0x100;

    var $restaurant;
    var $name;
    var $invoicedate;
    var $msg;
    var $content;
    var $result;

    function __construct() {
    	$this->restaurant = $this->msg = "";
    	}
    
    private function getService($data) {
    	$service = 0;

		if(!empty($data['type']) && $data['type'] == "B") $service |= self::PERBOOKING;
		else $service |= self::PERPAX;
		if(!empty($data['websitebkg'])) $service |= self::WEBSITEBOOKING;
		if(!empty($data['websitepax'])) $service |= self::WEBSITEPAX;
		if(!empty($data['notinvoiced'])) $service |= self::NOTINVOICE;
		if(!empty($data['mrs'])) $service |= self::MRS;
		if(!empty($data['mcc'])) $service |= self::MCC;
		if(!empty($data['mtms'])) $service |= self::MTMS;
		if(!empty($data['mmkg'])) $service |= self::MMKG;
		
		return $service;
     	}
    		
    public function readAllInvoice($date){

        $date = $this->clean_input($date);
        $where = ($date == "" || $date == "all") ? "" : " where invoicedate <= '$date' ";
        	
        $data = pdo_multiple_select("SELECT * FROM invoice $where ORDER BY invoicedate, restaurant");
  
		$this->msg = "";
        $this->result = 1;

        return $data;
    }
    
    public function readInvoice($restaurant, $invoicedate){

        $content = array("restaurant", "invoicedate");
        for ($i = 0; $i < count($content); $i++) {
            $tt = $content[$i];
            $$tt = $this->clean_input($$tt);
        }

		if($restaurant == "")
			return $this->result = -1;
		
        $data = pdo_multiple_select("SELECT * FROM invoice WHERE invoicedate = '$invoicedate' where restaurant = '$restaurant' ORDER BY invoicedate, restaurant");
  
		$this->msg = "";
        $this->result = 1;

        return $data;
    }
    
	function createInvoice($restaurant, $invoicedate, $subtotal, $license, $currency, $segments, $details) {
		
        $content = array("restaurant", "invoicedate", "subtotal", "license", "currency", "segments", "details");
        for ($i = 0; $i < count($content); $i++) {
            $tt = $content[$i];
            $$tt = $this->clean_input($$tt);
        }
		
		if($restaurant == "")
			return $this->result = -1;
		
		pdo_exec("replace INTO invoice (restaurant, invoicedate, subtotal, license, currency, segments, details, status) VALUES ('$restaurant', '$invoicedate', '$subtotal', '$license', '$currency', '$segments', '$details', '')"); 
   		return $this->result = 1;
		}

	function updateStatus($restaurant, $invoicedate, $status) {
		
        $content = array("restaurant", "invoicedate", "status");
        for ($i = 0; $i < count($content); $i++) {
            $tt = $content[$i];
            $$tt = $this->clean_input($$tt);
        }
		
		if($restaurant == "")
			return $this->result = -1;

		pdo_exec("update invoice set status='$status' where restaurant = '$restaurant' and invoicedate = '$invoicedate' limit 1"); 
   		return $this->result = 1;
		}
		
	function totalInvoice($restaurant, $invoicedate, $total) {
		
        $content = array("restaurant", "invoicedate", "total");
        for ($i = 0; $i < count($content); $i++) {
            $tt = $content[$i];
            $$tt = $this->clean_input($$tt);
        }
		
		if($restaurant == "")
			return $this->result = -1;
		
		pdo_exec("update invoice set total='$total' where restaurant = '$restaurant' and invoicedate = '$invoicedate' limit 1"); 
   		return $this->result = 1;
		}

    public function readAllInvConfig(){

        $cmds["config"] = pdo_multiple_select("SELECT * FROM invoice_conf WHERE status = 'active' ORDER BY restaurant");
   
		$this->msg = "";
        $this->result = 1;

        return $cmds;
    }
    
   
    public function readInvConfig($restaurant){

        $cmds["config"] = pdo_multiple_select("SELECT * FROM invoice_conf WHERE restaurant = '$restaurant' and status = 'active' limit 1");
  
		$this->msg = "";
        $this->result = 1;

        return $cmds;
    }
    
	function createInvConfig($data) {
		
	   	$this->result = -1;
		$this->restaurant = $data['restaurant'];
		if($this->restaurant == "")
			return $this->result = -1;
		
		$json = preg_replace("/'|\"/", "’", json_encode($data));	
		error_log("JSON " . $json);
		$tmp = pdo_single_select("SELECT restaurant FROM invoice_conf where restaurant = '$this->restaurant' and status = 'active' limit 1");
		if(count($tmp) > 0) {
			pdo_exec("update invoice_conf set status = 'old' where restaurant = '$this->restaurant' and status = 'active' limit 1"); 
			}
		
		$service = $this->getService($data);	
		pdo_exec("INSERT INTO invoice_conf (restaurant, service, config, date, status) VALUES ('$this->restaurant', '$service', '$json', NOW(), 'active')"); 
   		return $this->result = 1;

		}

	function updateInvConfig($data) {
		
		return $this->createInvConfig($data);		
		}

	function deleteInvConfig($restaurant) {
		
	   	$this->result = -1;
	   	$this->restaurant = $restaurant;
		if($this->restaurant == "")
			return $this->result = -1;
		
		pdo_exec("delete from invoice_conf where restaurant = '$this->restaurant' and status = 'active' limit 1"); 
   		return $this->result = 1;
		}
                
          private function getUniqueCode() {

        //        Why link to restaurant? 
                for ($i = 0; $i < 100; $i++) {
                    $uniqueID = rand(10000000, 100000000 - 1);
                    $data = pdo_single_select("SELECT orderID FROM online_order WHERE restaurant ='$this->restaurant' and orderID = '$uniqueID' limit 1");
                    if (count($data) <= 0)
                        return $uniqueID;
                }
                return false;
            }
                
        function invoicepayment($data){
              $orderID = $this->getUniqueCode();
                
             if ($orderID == false)
                return $this->error_msg(-1, "unable to create a unique ID ");
             $payArr =[
                 'payment_id'=>$orderID,
                  'amount'=>$data['amount']
             ];
    
               $payment = new WY_Paypal();
                $result = $payment->payinvoice($payArr);
                if($result['status']=='Success'){
                    $data['status']='Success';
                    $data['paypalUrl'] = $result['paypalUrl'];
                }else{
                    $data['status']='Fail';
                    $data['paypalUrl'] = "";
                }
        
         return $data;
            
        }
}
