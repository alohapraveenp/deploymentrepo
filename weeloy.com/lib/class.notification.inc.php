<?php

require_once("lib/wpdo.inc.php");
require_once("lib/class.debug.inc.php");
require_once("lib/class.restaurant.inc.php");
require_once("lib/class.media.inc.php");
require_once("lib/class.booking.inc.php");
require_once("conf/conf.init.inc.php");
require_once("lib/class.translation.inc.php");
require_once('lib/class.mail.inc.php');
require_once('lib/class.sms.inc.php');
require_once('lib/class.pushnotif.inc.php');
require_once('lib/class.payment.inc.php');
require_once('lib/class.event.inc.php');
require_once('lib/class.allote.inc.php');
require_once("lib/class.restaurant_cancel_policy.inc.php");
require_once 'lib/class.marketing_campaign.inc.php';

class WY_Notification {

    public $sms_type = '';
    public $email_service = NULL;
    public $sms_service = NULL;
    public $apn_service = NULL;
    public $restaurant_configuration = array('email' => true, 'sms' => true, 'sms_premium' => true, 'apns' => true);
    public $notification_type = array('email', 'sms', 'sms premium', 'push notification');
    public $smsprovider = 'sms_premium';
    public $pname = 'SMSP';

    public function __construct() {
        $this->email_service = new JM_Mail;
        $this->sms_service = new JM_Sms();
        $this->apn_service = new JM_Pushnotif();
        $this->payment_service = new WY_Payment();

    }

    public function notify($booking_origin, $notification_type) {
        
       
        $booking = clone $booking_origin;
     
        if($booking->restaurant != 'SG_SG_R_Bacchanalia'){
            if ($this->checkisRemote($booking->tracking) && $booking->force_notification != true) {
                return;
            }

            if (preg_match("/CHOPE|HGW|QUANDOO/i", $booking->booker) == true && $booking->force_notification != true) {
                return;
            }
        }
        
        if (preg_match("/DNB/i", $booking->tracking) == true)
            return;
        
        $res = new WY_restaurant;
        $res->getRestaurant($booking->restaurant);
       
        $data_object = $this->getNotificationObject($booking);
     
        $data_object->wemailflg = $booking->wemailflg = $res->emailWhitelabel();
        
         //$provider ='sms_premium';

        if ($notification_type == 'booking') {
            $notification_type = $booking->booking_type;
 
        }
        if($notification_type == 'reminder_booking'){
            $notification_type ='reminder_'.$booking->booking_type;
        }
        if($notification_type == 'expired'){
            $notification_type ='expired_'.$booking->booking_type;
        }
         
        $member_notification = $this->notifyMember($notification_type, $booking, $data_object);  
        $restaurant_notification = $this->notifyRestaurant($notification_type, $booking, $data_object, $res);
 
        return $member_notification && $restaurant_notification;
    } 
    private function  getNotifyEventObj($object){
  
        $data_object = $object;
        $data_object->confirmation = $object->event_id;
        $data_object->baseUrl = __BASE_URL__;
        $data_object->date = date("F j, Y",strtotime($object->date));
        $data_object->restaurantinfo->email = $object->restaurant_email;
        return $data_object;
    }
    
    
    public function notifyEvent($object, $notification_type){
        
       
        $res = new WY_restaurant;
            
        $res->getRestaurant($object->restaurant);

        $data_object = $this->getNotifyEventObj($object);
        
        $data_object->wemailflg = $object->wemailflg = $res->emailWhitelabel();
       
        $member_notification = $this->notifyMember($notification_type, $object, $data_object);  
      
        $restaurant_notification = $this->notifyRestaurant($notification_type, $object, $data_object, $res);
        
        
        

        return $member_notification && $restaurant_notification;
        
        
    }

    private function notifyMember($template_type, $booking, $data_object) {


        $template = $template_type . '_member';

        
        if (isset($data_object->white_label) && $data_object->white_label == true) {
            $template .= '_white_label';
        }
    

        $config =  $this->getConfiguration($booking->restaurant, $template);

        if($config == false){
            return false;
        }
  
         
        /**         * ************************** EMAIL MEMBER **************************** */
        if ($this->restaurant_configuration['email']) {
            $email_header = $this->getEmailHeader($template, $booking, $data_object);

            $email_content = $this->getEmailContent($template, $booking, $data_object);
            
         
         
           
            if ($email_content !== false) {
                $this->email_service->sendmail($email_header['to'], $email_content['subject'], $email_content['body'], $email_header, $booking->restaurant, $booking->confirmation);
            }
        }

        /**         * *************************** SMS MEMBER **************************** */
        if ($this->restaurant_configuration[$this->smsprovider]) {
            $header_sms = $this->getSmsHeader($template, $booking, $data_object);
            $sms_content = $this->getSmsBody($template, $booking, $data_object);
            if ($sms_content !== false) {
                $this->sms_service->sendSmsMessage($header_sms['recipient'], $sms_content, $header_sms['from'], $booking->restaurant, $booking->confirmation, $this->pname);
            }
        }

        /**         * *************************** PUSH NOTIF MEMBER **************************** */
        if ($this->restaurant_configuration['apns']) {
            $apn_message = $this->getApnBody($template, $booking, $data_object);
            
            
            if ($apn_message !== false){
            //    $this->apn_service->sendnotifyMultiple($recipient, $booking->restaurant, $booking->confirmation, $apn_message, 'member');
            }
        }

        return true;
    }

    private function notifyRestaurant($template_type, $booking, $data_object, $res) {
         
         $template = $template_type . '_restaurant';
        
        if (isset($data_object->white_label) && $data_object->white_label == true) {
            $template .= '_white_label';
        }

        $config = $this->getConfiguration($booking->restaurant, $template);
        

    
        if($config == false){
            return false;
        }

 
        /*         * * ************************** EMAIL RESTAURANT **************************** */
        if ($this->restaurant_configuration['email']) {
            $email_header = $this->getEmailHeader($template, $booking, $data_object);
            
         
   
            $email_content = $this->getEmailContent($template, $booking, $data_object);
            


          
            if ($email_content !== false) {
                if(isset($booking->tracking) && $this->checkisImagineAsia($booking->tracking)) {
                    $this->email_service->sendmail('tablebooking@image-asia.com', $email_content['subject'], $email_content['body'], $email_header, $booking->restaurant, $booking->confirmation);
                    //$this->email_service->sendmail('philippe.benedetti@weeloy.com', $email_content['subject'], $email_content['body'], $email_header, $booking->restaurant, $booking->confirmation);
                }
                $this->email_service->sendmail($email_header['to'], $email_content['subject'], $email_content['body'], $email_header, $booking->restaurant, $booking->confirmation);
                
               
            }
        }

        /*         * * *************************** SMS RESTAURANT **************************** */
        if ($this->restaurant_configuration[$this->smsprovider]) {
            $header_sms = $this->getSmsHeader($template, $booking, $data_object);
            $sms_message = $this->getSmsBody($template, $booking, $data_object);
            if ($sms_message !== false) {
                $this->sendSmsToRestaurant($res, $booking, $data_object, $sms_message, $header_sms['from'], $this->pname);
            }
        }

        /*         * * *************************** PUSH NOTIF RESTAURANT **************************** */
        if ($this->restaurant_configuration['apns']) {
            $apn_message = $this->getApnBody($template, $booking, $data_object);
            if ($apn_message !== false) {
            //    $this->apn_service->sendnotifyMultiple($recipient, $booking->restaurant, $booking->confirmation, $apn_message, 'restaurant');
            }
        }
        return true;
    }

    
    
    
    private function getEmailHeader($template_type, $booking, $data_object) {
        $email_header = array();

        if (isset($data_object->white_label) && $data_object->white_label == true) {
            //$template_type .= '_white_label';
            $email_header['white_label'] = true;
        }
        if (isset($data_object->imasia_label) && $data_object->imasia_label == true) {
            $email_header['imasia_label'] = true;
        }
       $ps_restaurants = array('SG_SG_R_PscafeAtHardingRoad', 'SG_SG_R_PscafeAtAnnSiangHillPark', 'SG_SG_R_PscafeAtPalaisRenaissance');

        switch ($template_type) {
            case 'booking_member_white_label':
            case 'request_member_white_label':
            case 'listing_member_white_label':
            case 'update_member_white_label':
            case 'cancel_member_white_label':
            case 'reminder_booking_member_white_label':
            case 'reminder_listing_member_white_label':
            case 'reminder_request_member_white_label':
            case 'noshow_member_white_label':
            case 'booking_pending_member_white_label':
            case 'event_menu_selected_member_white_label':
            case 'event_menu_selection_req_member_white_label':
            case 'reminder_pdflink_member_white_label':
            case 'event_booking_member_white_label':
            case 'expired_booking_member_white_label':
            case 'reminder_review_member_white_label':
            case 'review_member_white_label':
            case 'event_management_member_white_label':
                 if (in_array($booking->restaurantinfo->restaurant, $ps_restaurants, true)) {
                     $email_header['cc'] = 'concierge@pscafe.com';   //concierge@pscafe.com
                 }
            case 'reminder_review_bacchana_member_white_label':
                $email_header['from'] = array('reservation@weeloy.com' => 'reservation');

                if ($booking->wemailflg && $booking->restaurantinfo->aws_email_verification == 'verified' ){
                    $email_header['from'] = array($booking->restaurantinfo->email => $booking->restaurantinfo->title . "|emailwhitelabel");
                }
                else{
                    $email_header['from'] = array('reservation@weeloy.com' => $booking->restaurantinfo->title);
                }
                
                $email_header['replyto'] = array($booking->restaurantinfo->email => $booking->restaurantinfo->title);
                $email_header['to'] = $booking->email;
                break;

            case 'booking_restaurant_white_label':
            case 'request_restaurant_white_label':
            case 'listing_restaurant_white_label':
            case 'update_restaurant_white_label':
            case 'cancel_restaurant_white_label':
            case 'sms_response_restaurant_white_label':
            case 'event_request_restaurant_white_label':
            case 'event_menu_selected_restaurant_white_label':
                  if (in_array($booking->restaurantinfo->restaurant, $ps_restaurants, true)) {
                     $email_header['cc'] = 'concierge@pscafe.com';   //concierge@pscafe.com
                  }
            case 'event_booking_restaurant_white_label':
            case 'review_restaurant_white_label':
            case 'booking_pending_restaurant_white_label':   
            case 'event_management_restaurant_white_label':
                    if (in_array($booking->restaurantinfo->restaurant, $ps_restaurants, true)) {
                     $email_header['cc'] = 'concierge@pscafe.com';   //concierge@pscafe.com
                    }
            case 'event_mnt_reminder_restaurant_white_label':
                $email_header['to'] = $booking->restaurantinfo->email;
                $email_header['from'] = array('reservation@weeloy.com' => 'Weeloy.com');
                $email_header['replyto'] = array($booking->email);
                break;
            case 'booking_member':
            case 'request_member':
            case 'listing_member':
            case 'update_member':
            case 'cancel_member':
            case 'noshow_member':
            case 'wheel_win_member':
            case 'reminder_review_member':
            case 'reminder_booking_member':
            case 'reminder_listing_member':
            case 'reminder_request_member':
            case 'listing_pending_member':
            case 'booking_pending_member':
            case 'event_booking_member':
            case 'event_management_member':
            case 'expired_booking_member':
            case 'expired_listing_member':
            case 'reminder_review_bacchana_member':
                $email_header['to'] = $booking->email;
                $email_header['from'] = array('reservation@weeloy.com' => 'Weeloy.com');
                $email_header['replyto'] = 'info@weeloy.com';
                break;

            case 'booking_restaurant':
            case 'request_restaurant':
            case 'listing_restaurant':
            case 'update_restaurant':
            case 'cancel_restaurant':
            case 'review_restaurant':
            case 'wheel_win_restaurant':
            case 'event_booking_restaurant':
            case 'booking_pending_restaurant':
            case 'event_management_restaurant':
                if (preg_match('/,/', $booking->restaurantinfo->email)) {
                     $email_header['to'] = explode(",",$booking->restaurantinfo->email);
                }else{
                    $email_header['to'] = $booking->restaurantinfo->email;
                }
                //$email_header['to']  = array('vs.kala@weeloy.com','vksuriya2007@gmail.com');
                $email_header['from'] = array('reservation@weeloy.com' => 'Weeloy.com');
                $email_header['replyto'] = array($booking->email);
                 if (in_array($booking->restaurantinfo->restaurant, $ps_restaurants, true)) {
                     $email_header['cc'] = 'concierge@pscafe.com';   //concierge@pscafe.com
                  }

                break;

            default:
                return false;
        }
        
    
        return $email_header;
    }

    private function getEmailContent($template_type, $booking, $data_object) {

        
        $email_content = array();
        $subject = '';
        $template_name = '';
        


//        if ($data_object->white_label == true) {
//            $template_type .= '_white_label';
//        }


        
        switch ($template_type) {
            case 'booking_member_white_label':
            case 'listing_member_white_label':
            case 'request_member_white_label':
                $subject = "Reservation at " . $booking->restaurantinfo->title . " on " . $data_object->rdate;
                $template_name = 'confirmation/booking/member/white_label/confirmation';
                break;
            case 'booking_pending_member_white_label':
           
                $subject = "Reservation at " . $booking->restaurantinfo->title . " on " . $data_object->rdate;
                $template_name = 'confirmation/booking/member/white_label/pending';
                break;
            
            case 'expired_booking_member_white_label':
                 $subject = "Reservation at " . $booking->restaurantinfo->title . " on " . $data_object->rdate;
                 $template_name = 'confirmation/booking/member/white_label/expired';
                break;
            
            case 'booking_restaurant_white_label':
            case 'listing_restaurant_white_label':
            case 'request_restaurant_white_label':
                if($data_object->restaurant == 'SG_SG_R_BurntEnds' || $data_object->restaurant == 'SG_SG_R_TheOneKitchen'){
                    $extra = '';
                    if($data_object->product == 'Chef table' || ($data_object->product == 'Counter seats' && $data_object->cover > '5')){$extra .= ' - form needed';}
                    $subject = "[" . $data_object->product . $extra . "] New reservation: " . $data_object->rdate . ", " . $booking->salutation . " " . $booking->firstname . " " . $booking->lastname . " at " . $booking->restaurantinfo->title . " - " . $booking->confirmation;
                    $template_name = 'confirmation/booking/restaurant/white_label/confirmation';                    
                }else{
                    $subject = "New reservation: " . $data_object->rdate . ", " . $booking->salutation . " " . $booking->firstname . " " . $booking->lastname . " at " . $booking->restaurantinfo->title . " - " . $booking->confirmation;
                    $template_name = 'confirmation/booking/restaurant/white_label/confirmation';
                }
                break;
             case 'booking_pending_restaurant_white_label':
           
                $subject = "Reservation at " . $booking->restaurantinfo->title . " on " . $data_object->rdate;
                $template_name = 'confirmation/booking/restaurant/white_label/pending';
                break;
            case 'update_member_white_label':
                $subject = "Reservation Updated at " . $booking->restaurantinfo->title . " on " . $data_object->rdate;
                $template_name = 'update/member/white_label/update';
                break;
            case 'update_restaurant_white_label':
                $subject = "Update Reservation: $data_object->rdate, $booking->salutation, $booking->firstname $booking->lastname - $booking->confirmation";
                $template_name = 'update/restaurant/white_label/update';
                break;

            case 'cancel_member_white_label':
                $subject = "Reservation Cancelled at " . $booking->restaurantinfo->title . " on " . $data_object->rdate;
                $template_name = 'cancel/member/white_label/cancel';
                break;
            case 'cancel_restaurant_white_label':
                $subject = "Cancellation Reservation: " . $data_object->rdate . ", " . $booking->salutation . " " . $booking->firstname . " " . $booking->lastname;
                $template_name = 'cancel/restaurant/white_label/cancel';
                break;

            case 'reminder_booking_member_white_label':
            case 'reminder_listing_member_white_label':
            case 'reminder_request_member_white_label':
                $subject = "Reservation at " . $booking->restaurantinfo->title . " on " . $data_object->rdate;
                $template_name = 'reminder/member/white_label/confirmation';
                break;
           case 'noshow_member_white_label':
                $subject = "Reservation noshow  at " . $booking->restaurantinfo->title . " on " . $data_object->rdate;
                $template_name = 'noshow/member/white_label/noshow';
                break;
            

            case 'booking_member':
                $subject = "Reservation at " . $booking->restaurantinfo->title . " on " . $data_object->rdate;
                $template_name = 'confirmation/booking/member/weeloy/confirmation';
                break;
            case 'booking_restaurant':
                $subject = "New reservation: " . $data_object->rdate . ", " . $booking->salutation . " " . $booking->firstname . " " . $booking->lastname . " at " . $booking->restaurantinfo->title . " - " . $booking->confirmation;
                $template_name = 'confirmation/booking/restaurant/weeloy/confirmation';
                break;

            case 'listing_member':
              
                $subject = "Reservation at " . $booking->restaurantinfo->title . " on " . $data_object->rdate;
                $template_name = 'confirmation/listing/member/weeloy/confirmation';
                break;
            
            case 'expired_listing_member':
                $subject = "Reservation at " . $booking->restaurantinfo->title . " on " . $data_object->rdate;
                $template_name = 'confirmation/listing/member/weeloy/expired';
                break;
            case 'listing_restaurant':
                if($data_object->restaurant == 'SG_SG_R_BurntEnds' || $data_object->restaurant == 'SG_SG_R_TheOneKitchen'){
                    $extra = '';
                    if($data_object->product == 'Chef table' || ($data_object->product == 'Counter seats' && $data_object->cover > '5')){$extra .= ' - form needed';}
                    $subject = "[" . $data_object->product . $extra . "] New reservation: " . $data_object->rdate . ", " . $booking->salutation . " " . $booking->firstname . " " . $booking->lastname . " at " . $booking->restaurantinfo->title . " - " . $booking->confirmation;
                    $template_name = 'confirmation/listing/restaurant/weeloy/confirmation';
                }else{
                    $subject = "New reservation: " . $data_object->rdate . ", " . $booking->salutation . " " . $booking->firstname . " " . $booking->lastname . " at " . $booking->restaurantinfo->title . " - " . $booking->confirmation;
                    $template_name = 'confirmation/listing/restaurant/weeloy/confirmation';                
                }
                break;

            case 'request_member':
                $subject = "Reservation at " . $booking->restaurantinfo->title . " on " . $data_object->rdate;
                $template_name = 'confirmation/request/member/weeloy/confirmation';
                break;
            case 'request_restaurant':
                $subject = "New reservation: " . $data_object->rdate . ", " . $booking->salutation . " " . $booking->firstname . " " . $booking->lastname . " at " . $booking->restaurantinfo->title . " - " . $booking->confirmation;
                $template_name = 'confirmation/request/restaurant/weeloy/confirmation';
                break;


            case 'update_member':
                $subject = "Reservation Updated at " . $booking->restaurantinfo->title . " on " . $data_object->rdate;
                $template_name = 'update/member/weeloy/update';
                break;
            case 'update_restaurant':
                $subject = "Update Reservation: $data_object->rdate, $booking->salutation, $booking->firstname $booking->lastname - $booking->confirmation";
                $template_name = 'update/restaurant/weeloy/update';
                break;

            case 'cancel_member':
                $subject = "Reservation Cancelled at " . $booking->restaurantinfo->title . " on " . $data_object->rdate;
                $template_name = 'cancel/member/weeloy/cancel';
                break;
            
            case 'expired_booking_member':
                 $subject = "Reservation at " . $booking->restaurantinfo->title . " on " . $data_object->rdate;
                 $template_name = 'confirmation/booking/member/weeloy/expired';
                break;
            case 'cancel_restaurant':
                $subject = "Cancellation Reservation: " . $data_object->rdate . ", " . $booking->salutation . " " . $booking->firstname . " " . $booking->lastname;
                $template_name = 'cancel/restaurant/weeloy/cancel';
                break;
            
            case 'noshow_member':
                $subject = "Reservation noshow  at " . $booking->restaurantinfo->title . " on " . $data_object->rdate;
                $template_name = 'noshow/member/weeloy/noshow';
                break;
            
            case 'wheel_win_member':
                $subject = "Wheel Prize at " . $booking->restaurantinfo->title . " on " . $data_object->rdate;
                $template_name = 'wheel_win/member/weeloy/win';
                break;
            
            case 'wheel_win_restaurant':
                $subject = "Wheel Prize at " . $booking->restaurantinfo->title . " on " . $data_object->rdate;
                $template_name = 'wheel_win/restaurant/weeloy/win';
                break;
            
            case 'reminder_review_member':
                $subject = "Review reminder on the " . $booking->restaurantinfo->title;
                $template_name = 'reminder/member/weeloy/review';
                if($data_object->restaurant == 'SG_SG_R_Bacchanalia' ){
                   $subject = "Thank You for dining at " . $booking->restaurantinfo->title;
                   $template_name = 'reminder/member/weeloy/review_bacchanalia';
                }
                break;
            case 'reminder_review_member_white_label':
                $subject = "Review reminder on the " . $booking->restaurantinfo->title;
                $template_name = 'reminder/member/white_label/review';
                if($data_object->restaurant == 'SG_SG_R_Bacchanalia'){
                    $subject = "Thank You for dining at " . $booking->restaurantinfo->title;
                    $template_name = 'reminder/member/white_label/review_bacchanalia';
                }
                break;
            
            case 'reminder_booking_member':            
                $subject = "Reservation at " . $booking->restaurantinfo->title . " on " . $data_object->rdate;
                $template_name = 'reminder/member/weeloy/confirmation';
                break;
            
            case 'reminder_listing_member':            
                $subject = "Reservation at " . $booking->restaurantinfo->title . " on " . $data_object->rdate;
                $template_name = 'confirmation/listing/member/weeloy/confirmation';
                break;
            
            case 'reminder_request_member':
                $subject = "Reservation at " . $booking->restaurantinfo->title . " on " . $data_object->rdate;
                $template_name = 'confirmation/request/member/weeloy/confirmation';
                break;
            
            case 'sms_response_restaurant_white_label':
                $subject = "Sms reply from  " . $booking->firstname . " " . $booking->lastname;
                $template_name = 'sms_response/restaurant/white_label/reply';
                break;
            
            case 'review_restaurant':
                $subject = "New review on the  " . $booking->restaurantinfo->title;
                $template_name = 'review/restaurant/weeloy/review';
                break;
             case 'review_restaurant_white_label':
                $subject = "New review on the  " . $booking->restaurantinfo->title;
                $template_name = 'review/restaurant/white_label/review';
                break;
            
            case 'booking_pending_member':
                $subject = "Reservation at " . $booking->restaurantinfo->title . " on " . $data_object->rdate;
                $template_name = 'confirmation/booking/member/weeloy/pending';
                break;
            case 'listing_pending_member':
                $subject = "Reservation at " . $booking->restaurantinfo->title . " on " . $data_object->rdate;
                $template_name = 'confirmation/listing/member/weeloy/pending';
                break;
             case 'booking_pending_restaurant':
                $subject = "Reservation at " . $booking->restaurantinfo->title . " on " . $data_object->rdate;
                $template_name = 'confirmation/booking/restaurant/weeloy/pending';
                break;
            case 'event_booking_member':
                $subject = "Reservation at " . $booking->restaurant_title . " on " . $data_object->date;
                $template_name = 'event/member/weeloy/confirmation';
                break;
             case 'event_booking_restaurant':
                $subject = "Reservation at " . $booking->restaurant_title . " on " . $data_object->date;
                $template_name = 'event/restaurant/weeloy/confirmation';
                break;
              case 'event_management_member_white_label':
                $subject = "Reservation at " . $booking->restaurant_title . " on " . $data_object->date;
                 
                $template_name = 'event/member/white_label/confirmation_event_management';
                break;
            case 'event_management_restaurant_white_label':
                $subject = "Reservation at " . $booking->restaurant_title . " on " . $data_object->date;
                $template_name = 'event/restaurant/white_label/confirmation_event_management';
                break;
            
            case 'event_request_restaurant_white_label':
                $subject = "Event Request at " . $booking->restaurant_title . " on " . $data_object->date;
                $template_name = 'event/restaurant/white_label/request';
                break;
            case 'event_menu_selected_restaurant_white_label':
                $subject = "Event Request at " . $booking->restaurant_title . " on " . $data_object->date;
                $template_name = 'event/restaurant/white_label/menu_selected';
                break;
            case 'event_menu_selected_member_white_label':
                $subject = "Event Request at " . $booking->restaurant_title . " on " . $data_object->date;
                $template_name = 'event/member/white_label/menu_selected';
                break;
            case 'event_menu_selection_req_member_white_label':
                $subject = "Event Menu Selection Request at " . $booking->restaurant_title . " on " . $data_object->date;
                $template_name = 'event/member/white_label/menu_selection_request';
                break;
             case 'event_booking_restaurant_white_label':
                $subject = "Reservation at " . $booking->restaurant_title . " on " . $data_object->date;
                $template_name = 'event/restaurant/white_label/confirmation';
                break;
             case 'event_booking_member_white_label':
                $subject = "Reservation at " . $booking->restaurant_title . " on " . $data_object->date;
                $template_name = 'event/member/white_label/confirmation';
                break;
             case 'event_booking_restaurant':
                $subject = "Reservation at " . $booking->restaurant_title . " on " . $data_object->date;
                $template_name = 'event/restaurant/weeloy/confirmation';
                break;
            case 'reminder_pdflink_member_white_label':
                $subject = "Reservation at " . $booking->restaurantinfo->title . " on " . $data_object->rdate;
                $template_name = 'reminder/member/white_label/menupdflink';
                break;
            case 'reminder_review_bacchana_member_white_label':
            case 'reminder_review_bacchana_member':
                 $subject = "Reservation at " . $booking->restaurantinfo->title . " on " . $data_object->rdate;
                 $template_name = 'reminder/member/white_label/review';
                 break;
             case 'event_mnt_reminder_restaurant_white_label':
                $subject = "Reservation at " . $booking->restaurant_title . " on " . $data_object->date;
                $template_name = 'event_reminder/restaurant/white_label/reminder';
                break;
     
            default:
                return false;
        }
        
        if($template_type === 'review_restaurant' || $template_type === 'review_restaurant_white_label' ){
            if($booking->reviewmode !== 'weeloypro'){
                return false;
            }
        }

        $email_content['subject'] = $subject;
        

        
        $email_content['body'] = $this->email_service->getTemplate($template_name, $booking);
  
     
        return $email_content;
    }

    public function getSmsHeader($template_type, $booking, $data_object) {
        
        $res = new WY_restaurant;
        
        $header_sms = array();
        $header_sms['from'] = 'Weeloy';
        
        $sms_header = $res->getRestaurantSmsNumber($booking->restaurant);
        
        if(!empty($sms_header)){
            $header_sms['from'] = $sms_header;
        }

        if ($data_object->white_label == true) {
            //$template_type .= '_white_label';
            $header_sms['white_label'] = true;
        }

//        switch ($template_type) {
//            default:
//                if (!empty($data_object->restaurantinfo->smsid)) {
//                    $header_sms['from'] = $data_object->restaurantinfo->smsid;
//                }
//                break;
//        }
        
        $header_sms['recipient'] = $booking->mobile;
        return $header_sms;
    }

    private function getSmsBody($template_type, $booking, $data_object) {
//        if ($data_object->white_label == true) {
//            $template_type .= '_white_label';
//        }

        
        switch ($template_type) {
            case 'booking_member_white_label':
            case 'listing_member_white_label':
            case 'request_member_white_label':
                $template_name = 'booking_member_white_label';
                break;
            case 'booking_restaurant_white_label':
            case 'listing_restaurant_white_label':
            case 'request_restaurant_white_label':
                $template_name = 'booking_restaurant';
                break;

            case 'update_member_white_label':
                $template_name = 'update_member';
                break;
            case 'update_restaurant_white_label':
                $template_name = 'update_restaurant';
                break;

            case 'cancel_member_white_label':
                $template_name = 'cancel-callcenter';
                break;
            case 'cancel_restaurant_white_label':
                $template_name = 'cancel_restaurant';
                break;

            case 'booking_member':
                $template_name = 'booking_member';
                break;
            case 'listing_member':
                $template_name = 'listing_member';
                break;
            case 'request_member':
                $template_name = 'request_member';
                break;
            case 'booking_restaurant':
            case 'listing_restaurant':
            case 'request_restaurant':
                $template_name = 'booking_restaurant';
                break;

//            case 'update_member':
//                $template_name = 'update_member';
//                break;
//            case 'update_restaurant':
//                $template_name = 'update_restaurant';
//                break;

            case 'cancel_member':
                $template_name = 'cancel';
                break;
            case 'cancel_restaurant':
                $template_name = 'cancel_restaurant';
                break;
            case 'wheel_win_member':
                $template_name = 'wheel_win_member';
                break;
            
            case 'reminder_booking_member':
                $template_name = 'reminder_booking_member';
                break;
            case 'reminder_listing_member':
                $template_name = 'reminder_listing_member';
                break;
            case 'reminder_request_member':
                $template_name = 'reminder_request_member';
                break;
            case 'reminder_booking_member_white_label':
                $template_name = 'reminder_booking_whitelabel';
                break;
            case 'booking_pending_member_white_label':
                $template_name = 'pending_booking_callcenter';
                break;
            case 'expired_booking_member':
            case 'expired_listing_member':
            case 'expired_booking_member_white_label':
                $template_name = 'expired_booking';
                break;
            

            
            default:
                return false;
        }
        
        if(isset($booking->resendFlg)){
             return false;
        }


        return $this->sms_service->getSmsMessage($template_name, $booking, $data_object);
    }

    private function getApnBody($template_type, $booking, $data_object) {
//        if ($data_object->white_label == true) {
//            $template_type .= '_white_label';
//        }
        

        switch ($template_type) {
            case 'booking_restaurant_white_label':
                $template_name = 'booking_restaurant';
                break;
            
            case 'cancel_restaurant_white_label':
                $template_name = 'cancel_restaurant';
                break;

            case 'booking_member':
            case 'listing_member':
            case 'request_member':
                $template_name = 'booking_member';
                break;
            case 'booking_restaurant':
            case 'listing_restaurant':
            case 'request_restaurant':
                $template_name = 'booking_restaurant';
                break;

            case 'update_member':
                $template_name = 'update_member';
                break;
            case 'update_restaurant':
                $template_name = 'update_restaurant';
                break;

            case 'cancel_member':
                $template_name = "Your booking at : " . $booking->restaurantinfo->title . "has been cancelled";
                break;
            case 'cancel_restaurant':
                $template_name = 'cancel_restaurant';
                break;

            case 'reminder_booking_member':
            case 'reminder_listing_member':
            case 'reminder_request_member':
                $template_name = 'booking';
                break;
            
            default:
                return false;
        }
//        if($template_name==='update_member'){
//            if ($data_object->white_label == false) {
//                //$pushToken="b1fec0f5e1b8fc551a511030d73b6832fbf8a7995bee56c9dddc249e4154a6df";
//                $pushToken = $booking->pushToken;
//                $this->apn_service->sendIospassNotify($pushToken);
//            }
//        }

        return $this->apn_service->getpushNotifyMessage($template_name, $booking, $data_object->date_sms);
    }

    

        function notifyExternalWebsiteNewsletterRegistration($restaurant, $email) {

        $mailer = new JM_Mail;
        $sms = new JM_Sms();
        $apn = new JM_Pushnotif();

        $res = new WY_restaurant;
        $res->getRestaurant($restaurant);

       // $data_object->email = $email;
                
        $header['from'] = array('reservation@weeloy.com' => 'Weeloy.com');
        $header['replyto'] = array($email);


        // $recipient = "support@weeloy.com";
        $recipient = $res->email;
        $body = $mailer->getTemplate('externalwebsite/restaurant/newsletter/registration', $email);
        $subject = "New newsletter registration";

        return $mailer->sendmail($recipient, $subject, $body, $header);
        
    }

    
    function notifyCatering($orderDetails, $items) {

        $mailer = new JM_Mail;
        $sms = new JM_Sms();
        $apn = new JM_Pushnotif();

        $res = new WY_restaurant;
        $res->getRestaurant($orderDetails->restaurant);

        $data_object = $orderDetails;
        $data_object->siteUrl = __SERVERNAME__ . __ROOTDIR__;

        $header['from'] = array('reservation@weeloy.com' => 'Weeloy.com');
        $header['replyto'] = array('info@weeloy.com');

        if ($this->checkisWhitelabel($data_object->tracking)) {
            if ($res->emailWhitelabel())
                $header['from'] = array($booking->restaurantinfo->email => $booking->restaurantinfo->title);
            $header['replyto'] = array($booking->restaurantinfo->email);
        }

//        if (isset($qrcode_guest)) {
//            $header['guest_qrcode'] = $qrcode_guest;
//        }

        $recipient = $orderDetails->email;
        $recipient_mobile = $orderDetails->mobile;

        $title = $res->title;
        $data_object->title = $title;
        $data_object->address = $res->address;
        $data_object->city = $res->city;
        $data_object->zip = $res->zip;
        $rescountry = $res->country;
        $data_object->rescountry = $rescountry;
        $data_object->restTel = $res->tel;
        $data_object->itemdetails = $items;
        $tmpArray = array();
        $menuobj = array('item_title' => '', 'quantity' => '');
        $menus = array('item');

        foreach ($items as $item) {
            $menuobj['item_title'] = $item->item_title;
            $menuobj['quantity'] = $item->quantity;
            array_push($tmpArray, $menuobj);
        }

        $data_object->itemdetails = $tmpArray;

        //$recipient = "philippe.benedetti@weeloy.com";
        $subject = "Online Order at " . $title . " on " . $orderDetails->delivery_date;

        $body = $mailer->getTemplate('catering/notify_member', $data_object);

        if (!empty($recipient) && $recipient != "no@email.com") {
            $mailer->sendmail($recipient, $subject, $body, $header, $orderDetails->restaurant, $orderDetails->orderID);
        }

        $header['from'] = array('reservation@weeloy.com' => 'Weeloy.com');

        // $recipient = "support@weeloy.com";
        $recipient = $res->email;
        $body = $mailer->getTemplate('catering/notify_restaurant_catering', $data_object);
        $subject = "New Online Order at " . $res->title . " on " . $orderDetails->delivery_date;

        $res = $mailer->sendmail($recipient, $subject, $body, $header, $orderDetails->restaurant, $orderDetails->orderID);
    }

    function notifyreminderCatering($orderDetails) {

        $mailer = new JM_Mail;
        $sms = new JM_Sms();
        $apn = new JM_Pushnotif();

        $res = new WY_restaurant;
        $res->getRestaurant($orderDetails->restaurant);

        $data_object = $orderDetails;
        $data_object->siteUrl = __SERVERNAME__ . __ROOTDIR__;

        $header['from'] = array('reservation@weeloy.com' => 'Weeloy.com');
        $header['replyto'] = array('info@weeloy.com');

        if ($this->checkisWhitelabel($data_object->tracking)) {
            if ($res->emailWhitelabel())
                $header['from'] = array($booking->restaurantinfo->email => $booking->restaurantinfo->title);
            $header['replyto'] = array($booking->restaurantinfo->email);
        }

//        if (isset($qrcode_guest)) {
//            $header['guest_qrcode'] = $qrcode_guest;
//        }

        $recipient = $orderDetails->email;
        $recipient_mobile = $orderDetails->phone;

        $title = $res->title;
        $data_object->title = $title;
        $data_object->address = $res->address;
        $data_object->city = $res->city;
        $data_object->zip = $res->zip;
        $rescountry = $res->country;
        $data_object->rescountry = $rescountry;
        $data_object->restTel = $res->tel;

        //$recipient = "philippe.benedetti@weeloy.com";
        $subject = "Online Order at " . $title . " on " . $orderDetails->delivery_date;

        $body = $mailer->getTemplate('catering/notify_member_reminder', $data_object);


        if (!empty($recipient) && $recipient != "no@email.com") {
            $mailer->sendmail($recipient, $subject, $body, $header, $orderDetails->restaurant_id, $orderDetails->orderID);
        }

        $header['from'] = array('reservation@weeloy.com' => 'Weeloy.com');

        $recipient = $res->email;
        $body = $mailer->getTemplate('catering/notify_restaurant_catering_reminder', $data_object);
        $subject = "New Online Order at " . $res->title . " on " . $orderDetails->delivery_date;
        $mailer->sendmail($recipient, $subject, $body, $header, $orderDetails->restaurant_id, $orderDetails->orderID);
    }

    public function getConfiguration($restaurant, $type) {
        $res = pdo_single_select("SELECT * FROM notification_configuration where restaurant = '$restaurant' and notification_type ='$type'  limit 1 ");
        // if no config, load default values
        if (count($res) <= 0) {
            $res = pdo_single_select("SELECT * FROM notification_configuration_default WHERE notification_type = '$type' AND status LIKE 'active' limit 1");  
          
            if (count($res) <= 0) {
         
                return false;
            }
        }
      
        $this->restaurant_configuration = array('email' => $res['email'], 'sms' => $res['sms'], 'sms_premium' => $res['sms_premium'], 'apns' => $res['push_notification']);
        
        if ($res['sms_premium']==true) {
            $this->smsprovider = 'sms_premium';
            $this->pname = 'SMSP';
        }
        else if ($res['sms']==true) {
            $this->smsprovider = 'sms';
            $this->pname = 'SMS';
        }
        return true;
    }

    function createNotifyConfiAction($data) {
   
        if (isset($data)) {
            if ($data['mode'] === 'update') {
                $Sql = "UPDATE notification_configuration_default SET parent_group_action = '{$data['parent_group_action']}',notification_type = '{$data['notification_type']}', group_action = '{$data['group_action']}',white_label = '{$data['white_label']}',email= '{$data['status']}',sms= '{$data['sms']}',sms_premium= '{$data['sms_premium']}',push_notification= '{$data['push_notification']}',status = '{$data['status']}' where  id='{$data['id']}' limit 1";
                pdo_exec($Sql);
            } else {

                pdo_exec("INSERT INTO notification_configuration_default (parent_group_action, group_action, white_label,notification_type,status,email,sms,sms_premium,push_notification) VALUES ('{$data['parent_group']}','{$data['group']}', '{$data['white_label']}','{$data['notification_type']}','{$data['status']}','{$data['email']}','{$data['sms']}','{$data['sms_premium']}','{$data['push_notification']}')");
            }

            return 1;
        }
        return 0;
    }   
    
    function updateAction($data) {
 
        if (isset($data)) {
            $sql = "SELECT * from notification_configuration where restaurant='{$data['restaurant']}' and notification_type='{$data['notification_type']}' limit 1";
            $getData = pdo_single_select($sql);
            if (count($getData) > 0) {
                $Sql = "UPDATE notification_configuration SET email = '{$data['email']}', sms = '{$data['sms']}',push_notification ='{$data['push_notification']}',sms_premium = '{$data['sms_premium']}'  where restaurant='{$data['restaurant']}' and notification_type='{$data['notification_type']}' limit 1";
                pdo_exec($Sql);
            } else {

                pdo_exec("INSERT INTO notification_configuration (restaurant,notification_type, email, sms, push_notification,sms_premium) VALUES ('{$data['restaurant']}','{$data['notification_type']}', '{$data['email']}', '{$data['sms']}','{$data['push_notification']}','{$data['sms_premium']}')");
            }
            return 1;
        } else {
            return 0;
        }
    }
   
    function getNotificationAdminType() {
        $sql = "SELECT * from notification_configuration_default where status='active' order by ID ASC";
        $data = pdo_multiple_select($sql);
        return $data;
    }

    function getNotificationConfigAction($theRestaurant) {
        $sql = "SELECT * from notification_configuration where restaurant='$theRestaurant'";
        $getData = pdo_multiple_select($sql);

        return $getData;
    }

    function getNotificationDateFormat($rdate, $rtime) {
        date_default_timezone_set('UTC');

        $date_sms = date("j/m/Y H:i", mktime(substr($rtime, 0, 2), substr($rtime, 3, 4), 0, intval(substr($rdate, 5, 2)), intval(substr($rdate, 8, 2)), intval(substr($rdate, 0, 4))));
        $date_gcalender = date("j-m-Y H:i", mktime(substr($rtime, 0, 2), substr($rtime, 3, 4), 0, intval(substr($rdate, 5, 2)), intval(substr($rdate, 8, 2)), intval(substr($rdate, 0, 4))));
        $bkdate = date("F j, Y, g:i a", mktime(substr($rtime, 0, 2), substr($rtime, 3, 4), 0, intval(substr($rdate, 5, 2)), intval(substr($rdate, 8, 2)), intval(substr($rdate, 0, 4))));
        // Notification Member

        $date_sms = date("j/m/Y H:i", mktime(substr($rtime, 0, 2), substr($rtime, 3, 4), 0, intval(substr($rdate, 5, 2)), intval(substr($rdate, 8, 2)), intval(substr($rdate, 0, 4))));
        //$rdate = date("F j, Y, g:i a", mktime(substr($rtime, 0, 2), substr($rtime, 3, 4), 0, intval(substr($rdate, 5, 2)), intval(substr($rdate, 8, 2)), intval(substr($rdate, 0, 4))));
        $rdateres = date("l F j, Y, g:i a", mktime(substr($rtime, 0, 2), substr($rtime, 3, 4), 0, intval(substr($rdate, 5, 2)), intval(substr($rdate, 8, 2)), intval(substr($rdate, 0, 4))));
        $rdate_date_only = date("F j, Y", mktime(substr($rtime, 0, 2), substr($rtime, 3, 4), 0, intval(substr($rdate, 5, 2)), intval(substr($rdate, 8, 2)), intval(substr($rdate, 0, 4))));
        $rdate_time_only = date("H:i", mktime(substr($rtime, 0, 2), substr($rtime, 3, 4), 0, intval(substr($rdate, 5, 2)), intval(substr($rdate, 8, 2)), intval(substr($rdate, 0, 4))));
        $rtime = preg_replace("/:00/", "h", $rtime);
        //$date_gcalender = date("j-m-Y H:i", mktime(substr($rtime, 0, 2), substr($rtime, 3, 4), 0, intval(substr($rdate, 5, 2)), intval(substr($rdate, 8, 2)), intval(substr($rdate, 0, 4))));
        $dataObj = array('date_sms' => $date_sms, 'rdate' => $bkdate, 'rtime' => $rtime, 'rdateres' => $rdateres, 'rdate_date_only' => $rdate_date_only, 'rdate_time_only' => $rdate_time_only, 'date_gcalender' => $date_gcalender);

        return $dataObj;
    }

    function checkisWhitelabel($tracking) {
        return preg_match("/walkin|CALLCENTER|tms|waiting|WEBSITE|facebook|GRABZ|remote/i", $tracking);
    }

    private function checkisRemote($tracking) {
        return preg_match("/remote/i", $tracking);
    }

    private function checkisImagineAsia($tracking){
         return preg_match("/IMASIA/i", $tracking);
    }

    private function getNotificationObject($booking) {
       
        $dateObj = $this->getNotificationDateFormat($booking->rdate, $booking->rtime);

        $data_object = $booking;
           

        //booiking date & time user readable formate
        $data_object->bktime = $booking->rtime;
        $data_object->rtime = $dateObj['rtime'];
        $data_object->rdate = $dateObj['rdate'];
        $data_object->rdateres = $dateObj['rdateres'];
         

        // google calender date & time formate 
        //don't change utc timezone
        date_default_timezone_set('Asia/Singapore');
       
        $date_gcalender_end =  strtotime('+90 Minutes',strtotime($dateObj['date_gcalender']));
        $date_gcalender = strtotime($dateObj['date_gcalender']);
        $data_object->bkstartdate = gmdate("Ymd\THis\Z", $date_gcalender);
        $data_object->bkenddate = gmdate("Ymd\THis\Z", $date_gcalender_end);
        



        $data_object->rdate_time_only = $dateObj['rdate_time_only'];
        $data_object->rdate_date_only = $dateObj['rdate_date_only'];

        $data_object->date_sms = $dateObj['date_sms'];

        $recipient = $booking->email;
        $recipient_mobile = $booking->mobile;
        $tracking = $booking->tracking;

        //check the booking is whitelabel or not 
        $data_object->white_label = $this->checkisWhitelabel($tracking);
        
        //********************************imasia tracking*************************//
        $data_object->imasia_label =$this->checkisImagineAsia($tracking); 
        
        if($data_object->imasia_label){$data_object->white_label =false;}
      //********************************imasia tracking*************************//
        if(!$data_object->white_label){
            $data_object->siteUrl = __SERVERNAME__ . __ROOTDIR__;
            $data_object->baseUrl = __BASE_URL__;            
        }else{
            $data_object->siteUrl = __SERVERNAME__ . __ROOTDIR__;
            $data_object->baseUrl = __BASE_URL_WHITE_LABEL__;   
        }
        
        $data_object->baseemail = $booking->base64url_encode($recipient);
        $data_object->baseconfirmation = $booking->base64url_encode($booking->confirmation);
        $data_object->baserdate = $booking->base64url_encode($dateObj['rdate']);
        $data_object->basebookid = $booking->base64url_encode($booking->bookid);

        //fb share link
        $data_object->shareLink = urlencode($data_object->siteUrl . "/modules/booking/bookingdetails.php/?b=" . $data_object->basebookid . "&rd=" . $data_object->baseconfirmation);

        $data_object->rescountry = $booking->restaurantinfo->country;
        $data_object->restTel = $booking->restaurantinfo->tel;
        $data_object->ispromo = false;

        $res = new WY_restaurant;
        $res->getRestaurant($booking->restaurant);
        
        /*         * ************************************* */
        //Email->restaurant terms &condition
        /*         * ************************************* */

        $data_object->restc = "";
        $res->lastorderdiner = "";
        $res->lastorderlunch = "";

        if (isset($res->bookinfo)) {
            $llorder = ($res->lastorderlunch != "12:00") ? $res->lastorderlunch : "";
            $ldorder = ($res->lastorderdiner != "18:00") ? $res->lastorderdiner : "";
            $showinfo = $res->bookinfo;

            if ($showinfo != "") {
                if ($llorder !== "")
                    $llorder = "Last order for lunch is " . $llorder;
                $data_object->llorder = $llorder;
                if ($ldorder !== "")
                    $ldorder = "Last order for diner is " . $ldorder;
                $data_object->ldorder = $ldorder;
            }

            $data_object->restc = $showinfo;
        }

        /* checking cpp promotion and cpp booking */
        //promotion 
        if (!$booking->restaurantinfo->is_wheelable && preg_match("/cpp_credit_suisse/i", $tracking) == true) {
            $data = $res->getActivePromotion($booking->restaurant, $res->affiliate_program);
     
            if (count($data) > 0) {
                $data_object->offer = $data['offer_cpp'];
                $data_object->offer_description = $data['description_cpp'];
                $data_object->ispromo = true;
            }
        } else if (preg_match("/cpp_credit_suisse/i", $tracking) == true) {
            $data_object->offermsgres = 'This is corporate booking. The corporate Wheel will be automatically selected in your Weeloy app.';
            $data_object->offermsgmem = 'Please bring your bussiness card to enjoy your corparate program Promotion';
            $data_object->iscpp = true;
        }
        
        $data_object->isRefund = false;
        $data_object->isDeposit =false;
        $data_object->isInvoice =false;
        $data_object->burntEndsSplReq = array();
        $data_object->bacchanalia_info = array();
        $meal_type = WY_Allote::getMealType($data_object->bktime);
        $data_object->mealtype =  $meal_type;
        if($booking->restaurant == 'SG_SG_R_TheFunKitchen' || $booking->restaurant == 'SG_SG_R_Bacchanalia' ){
            $data_object->bacchanalia_info = array(
                    'lunch' =>'2-Course and 3-Course set menu (Tuesday to Saturday).',
                    'dinner' =>'4-Course and 7-Course set menu (Tuesday to Saturday).',
                    'parking' =>'Valet parking available for both lunch and dinner only at ($4).',
                    'corkage_fee' =>'Corkage fee applies ($50+per bottle) or Buy-1-Waive-1.',
                    'info' =>''
                );
        }
 
         if(isset($booking->deposit_id) && $booking->deposit_id !== ''){
             
             $data_object->payment_id =$booking->deposit_id;
             $depsoitDestails = $this->payment_service->getDepositDetails($booking->deposit_id,$booking->confirmation);
             
             $paymentDetails = $this->payment_service->getPaymentDetails($booking->deposit_id);

            if( $booking->product != '' && $booking->product != 'Bar Seats' && $booking->product != 'Bar seats'  && $booking->status ==''  ){
//                $meal_type = WY_Allote::getMealType($data_object->bktime);
                $product = str_replace(' ', '-', $booking->product);
                if($booking->product == 'Chef table' || strtolower($booking->product) == 'counter seats' && $booking->cover > 5){
                    $data_object->burntEndsSplReq['pdf_link'] = 'http://www.burntends.com.sg/wp-content/uploads/2016/08/'.strtolower($product).'-'.$meal_type.'.pdf';
                    $data_object->burntEndsSplReq['msg'] ="The Chef needs to know your preferences. Do not forget to fill up this form and to return it to eat@burntends.com.sg before you reservation date." ;
                }
       
            }
            if($booking->status==='cancel'){
                   
                    if(count($paymentDetails)>0){
                        $data_object->currency = $paymentDetails['currency'];
                        $data_object->refundstatus = $paymentDetails['status'];
                        $data_object->payment_id = $booking->deposit_id;

                        if($paymentDetails['payment_method']==='carddetails'){
                            
                            $data_object->amount = $paymentDetails['amount'];
                            $data_object->isInvoice = true;
                        }else{
                            $data_object->amount =$paymentDetails['refund_amount'];
                            $data_object->isDeposit = true;
                        }
                        $data_object->payment_method = ($depsoitDestails['payment_method']==='paypal') ? $depsoitDestails['payment_method'] : 'credit card';
                        $data_object->isRefund = ($paymentDetails['refund_amount'] > 0) ? true : false;

                    }
            }else if($booking->status==='noshow'){
                        
                    if(count($paymentDetails)> 0){
                        $data_object->currency = $paymentDetails['currency'];
                        $data_object->payment_id = $booking->deposit_id;
                        $data_object->amount = $paymentDetails['amount'];
                        $data_object->refundstatus = $paymentDetails['status'];
                        $data_object->isInvoice = true;
                    }
            }else{
                
                    if(count($depsoitDestails)>0  ){
                         $policy = new WY_Restaurant_policy;
                        if($booking->restaurant == 'SG_SG_R_TheFunKitchen' || $booking->restaurant == 'SG_SG_R_Bacchanalia' ){
//                            $data_object->bacchanaliacancelpolicy = $policy->bacchanalia_clpolicy($booking->restaurant); 
                            $data_object->bacchanaliacancelpolicy = $policy->getBacchaDeadline($booking->restaurant,$booking->rdate,$data_object->bktime,$data_object->cover);
                        }
                        // if($booking->restaurant == 'SG_SG_R_TheOneKitchen'){
                        //     $data_object->medinicancelpolicy = $policy->getMediniCancelPolicy($booking->restaurant,'',$booking->rdate,'',$data_object->cover,$data_object->product); 
                        // }else{
                        $generic = $booking->generic;
                        $product = $booking->product;
                        if(!empty($generic)) {
                            $genAr = json_decode(preg_replace("/’/", "\"", $generic), true);
                            if(isset($genAr['choice'])){
                                $product = $genAr['choice'];
                            }
                        }
                     
                            $data_object->cancelpolicy = $res->getCancelPolicyAll($booking->restaurant,'',$depsoitDestails['amount'],$product,$booking->rdate,$data_object->bktime);
                       // }
                        $data_object->currency = $depsoitDestails['curency'];
                        $data_object->amount = $depsoitDestails['amount'];
                        $data_object->despositstatus = $depsoitDestails['status'];
                       if($depsoitDestails['payment_method'] ==='carddetails'){
                           $data_object->isInvoice = true;
                       }else{
                           $data_object->isDeposit = true;
                       }
                        $data_object->payment_method = ($depsoitDestails['payment_method']==='paypal') ? $depsoitDestails['payment_method'] : 'credit card';

                    }
                      
            }

        }

        $prev_booking = pdo_single_select("SELECT rdate, rtime, cover, specialrequest, lastname, firstname, mobile, theTs from booking_modif WHERE  confirmation = '$booking->confirmation' order by theTs DESC LIMIT 1");
        
        if(count($prev_booking)>0){
            $data_object->prev_bkrdate = date('F  j,Y', strtotime($prev_booking['rdate']));
            $data_object->prev_bkrtime =$prev_booking['rtime'];
            $data_object->prev_bkcover = $prev_booking['cover'];
            $data_object->prev_bkspecialrequest = $prev_booking['specialrequest'];
            $data_object->prev_bklastname = $prev_booking['lastname'];
            $data_object->prev_bkfirstname = $prev_booking['firstname'];
            $data_object->prev_bkmobile = $prev_booking['mobile'];
            $data_object->prev_bookingtheTs = $prev_booking['theTs'];
        }
        
        if ($data_object->white_label == false) {
                // add passbook for all bookings on weeloy.com 
                $passbook_link = __BASE_URL__ . '/passbook/' . base64_encode($booking->confirmation . '|||' . $booking->email);
                $data_object->passbook = $passbook_link;
         }
            $data_object->isPaymentlink = false;
         
            // CALLCENTER - payment link notification
            if ($res->checkbkdeposit()>0 && ($booking->status == 'pending_payment' || $booking->status == 'expired') ) {
              $payment_link = __BASE_URL__ .'/modules/booking/deposit/deposit_payment_method.php?action=bktrack_pendingpayment&bkemail='.$booking->email.'&refid='.$booking->confirmation;
             if($data_object->white_label){
                $payment_link = $payment_link.'&bktracking='.$booking->tracking;  
             }

             $campaign = new WY_MarketingCampaign();
             $shortUrl = $campaign->bitly_shorten($payment_link);
             $data_object->link = $shortUrl;
             $data_object->expiredtime = $this->sms_service->getExpiredTime($booking->cdate,$booking->restaurant,$booking->rdate);
         
       
             $data_object->payment_link = $payment_link;
             $data_object->isPaymentlink = true;
             $policy = new WY_Restaurant_policy;
            if($booking->restaurant == 'SG_SG_R_TheFunKitchen' || $booking->restaurant == 'SG_SG_R_Bacchanalia' ){
                 $data_object->bacchanaliacancelpolicy = $policy->getBacchaDeadline($booking->restaurant,$booking->rdate,$data_object->bktime,$data_object->cover); 
                 
            }
            if($booking->restaurant == 'SG_SG_R_TheOneKitchen'){
                 $data_object->medinicancelpolicy = $policy->getMediniCancelPolicy($booking->restaurant,'',$booking->rdate,'',$data_object->cover,$data_object->product); 
                 
            }
         }

        //Email Translation
        $trad = new WY_Translation;
        $emailEle = $trad->getEmailContent('EMAIL', $booking->language);
        $data_object->labelContent = $emailEle;

        return $data_object;
    }

    private function sendSmsToRestaurant($res, $booking, $data_object, $sms_message, $headersms, $pname) {
        
        $restManager = $res->getRestMulticontact();
        for ($i = 0; $i < count($restManager); $i++) {
            $recipient_mobile = $restManager[$i]['mobile'];
            if (!empty($recipient_mobile)) {
                if ($data_object->white_label == true) {
                    $paid = 1;
                } else {
                    $paid = ($i > 0) ? 1 : 0;
                }

                $this->sms_service->sendSmsMessage($recipient_mobile, $sms_message, $headersms, $booking->restaurant, $booking->confirmation, $pname, $paid);
            }
        }
    }
}
