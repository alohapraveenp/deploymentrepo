<?php

require_once("lib/class.wheel.inc.php");
require_once("lib/class.debug.inc.php");
require_once("lib/class.string.inc.php");

$backoffice_listing_member_type_allowed = array('restaurant', 'restaurant_booking');
class WY_dailyspecial {

	var $content;

    function getallrestaurantdetails($code){
        $data = pdo_single_select("SELECT
                                        id, 
                                        title as name,
                                        restaurant as code,
                                        mgr_name as firstname,
                                        mgr_tel as restaurantphone,
                                        mgr_email as restaurantemail,
                                        tel as owner_phone,
                                        email,
                                        cuisine as cuisines,
                                        url as website,
                                        city,
                                        country,
                                        address,
                                        'true' as owner,
                                        zip as postalcode,
                                        status,
                                        'Partner' as type
                                    FROM 
                                        restaurant
                                    Where 
                                        restaurant ='$code' order by ID Limit 1","aurora");
        $datareturn = array();
        $datareturn = $data; 
        return (count($data) > 0) ? $datareturn: "";
    }

    function getallrestaurant() {
        $data = pdo_multiple_select("SELECT title,restaurant FROM restaurant","aurora");
        $datareturn = array();
        $datareturn = $data; 
        return (count($data) > 0) ? $datareturn: "";
    }


   function getrestTitle($theRestaurantx) {
        $theRestaurant = $theRestaurantx;

        $data = pdo_single_select("SELECT title FROM restaurant WHERE restaurant ='$theRestaurant' limit 1","aurora");
        return (count($data) > 0) ? $data['title'] : "";
    }

    //add new daily special board post
    function PostDailySpecial($restaurant, $files) {
    	//Insert into daily_special_board table
    	$restaurantname = $restaurant['restaurant'];
    	$headline = $restaurant['headline'];
    	$description = $restaurant['description'];
    	$category = serialize($restaurant['selectedOption']);
    	$imgname = $restaurant['imgname'];

    	$sql = "INSERT INTO daily_special_board (restaurant,headline,description,category,date_created,date_updated,deal_date,image) VALUES ('$restaurantname','$headline','$description','$category',now(),now(),now()+ INTERVAL 1 DAY,'$imgname')";
        pdo_exec($sql,"aurora");

        //upload into s3
		$deal_date = date('Y-m-d', strtotime(date('Y-m-d') . ' +1 day'));
		$id = 'dsb/'.$deal_date.'/'.$restaurantname;
		$media = new WY_Media($id);
		
		for ($i = 0; $i < count($files['name']); $i++) {
            
            // $media->upload($files['tmp_name'][$i], basename($files['name'][$i]));
            // if ($media->addImg($id, basename($files['name'][$i]), "restaurant", "restaurant", null, 'picture'))
            //     error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": Successfully add and upload image ".$files['name'][$i]."!");
            // else
            //     error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": Failed add image ".$files['name'][$i]."!");
        }

        return true;
    } //end of new daily special board

    function insertimg($app) {
        for($x=0;$x < count($app); $x++)
        {
            $id = $app[$x]['id'];
            $img = $app[$x]['image'];
            $sql = "INSERT INTO wy_posts_img(post_id,img) value ($id,'$img')";
            pdo_exec($sql,"blog");

        }
        return true;
    } 

    function insertcont($app) {
        for($x=0;$x < count($app); $x++)
        {
            $id = $app[$x]['id'];
            $content = $app[$x]['content'];
            $sql = "INSERT INTO wy_posts_content(post_id,content) value ($id,'$content')";
            pdo_exec($sql,"blog");

        }
        return true;
    } 

    //log in with credentials return
    function getLogindetails($login){

    	$email = $login['email'];
    	$password = $login['password'];
    	$this->content = array();
            
        $logCredentials = pdo_single_select("SELECT email,Password, tmpPassword,is_twofactauth FROM login where email = '$email' limit 1");

        //$member = pdo_single_select("SELECT member_type,member_permission FROM member where email = '$email' limit 1");
        
        //$membertype = $member['member_type'];

        if(count($logCredentials)>0){
            $store_password = $logCredentials['Password'];
            $tmp_password = $logCredentials['tmpPassword'];

            $retval = $this->check_password($password, $store_password, $tmp_password);
            
	        if ($retval < 0) {
	        	$this->msg = "This is wrong password or email";
	            return $this->result = 0;
	        }
            
            global $backoffice_listing_member_type_allowed;

	        $where = 'WHERE 1';
	        $join = '';

	        // if (in_array($membertype, $backoffice_listing_member_type_allowed)) {
	        //     $join .= ' JOIN restaurants_managers mr ON mr.restaurant_id=h.restaurant ';
	        //     $where .= " AND mr.member_id='" . $email . "' AND mr.status = 'active'";
	        // }

	        $sql = "SELECT title, restaurant FROM restaurant Where email = '$email' LIMIT 1";

	        
	        $loginsql = pdo_single_select($sql);
            if(count($loginsql)>0)
            {
                $i = 0; 
                foreach($loginsql as $data) {
                    
                    $content[$i]['content'] = $data;
                    $i++;
                }
                $this->content = $content;
                return $this->result = 1;
            }
            else
            {
                $sql2 = "SELECT name as title, code as restaurant FROM production.dsb_registration LIMIT 1";
                $loginsql2 = pdo_single_select($sql2);
                if(count($loginsql2)>0)
                {
                    $i = 0; 
                    foreach($loginsql2 as $data) {
                        
                        $content[$i]['content'] = $data;
                        $i++;
                    }
                    $this->content = $content;
                    return $this->result = 1;
                }
                else
                {
                    $this->msg = "This is wrong password or email";
                    return $this->result = 0;
                }
            }
	        

        }
        else
        {
        	$this->msg = "This is wrong password or email";
	        return $this->result = 0;
        }
    }


    //log in with credentials return
    function getLogindetailsget($email,$password ){

        //$email = $login['email'];
        //$password = $login['password'];
        $this->content = array();
            
        $logCredentials = pdo_single_select("SELECT email,Password, tmpPassword,is_twofactauth FROM login where email = '$email' limit 1");

        //$member = pdo_single_select("SELECT member_type,member_permission FROM member where email = '$email' limit 1");
        
        //$membertype = $member['member_type'];

        if(count($logCredentials)>0){
            $store_password = $logCredentials['Password'];
            $tmp_password = $logCredentials['tmpPassword'];

            $retval = $this->check_password($password, $store_password, $tmp_password);
            
            if ($retval < 0) {
                $this->msg = "This is wrong password or email";
                return $this->result = 0;
            }
            
            global $backoffice_listing_member_type_allowed;

            $where = 'WHERE 1';
            $join = '';

            // if (in_array($membertype, $backoffice_listing_member_type_allowed)) {
            //     $join .= ' JOIN restaurants_managers mr ON mr.restaurant_id=h.restaurant ';
            //     $where .= " AND mr.member_id='" . $email . "' AND mr.status = 'active'";
            // }

            $sql = "SELECT title,restaurant FROM restaurant Where email = '$email' LIMIT 1";

            
            $loginsql = pdo_single_select($sql);
            if(count($loginsql)>0)
            {
                $i = 0; 
                foreach($loginsql as $data) {
                    
                    $content[$i]['content'] = $data;
                    $i++;
                }
                $this->content = $content;
                return $this->result = 1;
            }
            else
            {
                $sql2 = "SELECT name as title, code as restaurant FROM production.dsb_registration LIMIT 1";
                $loginsql2 = pdo_single_select($sql2);
                if(count(loginsql2)>0)
                {
                    $i = 0; 
                    foreach($loginsql as $data) {
                        
                        $content[$i]['content'] = $data;
                        $i++;
                    }
                    $this->content = $content;
                    return $this->result = 1;
                }
                else
                {
                    $this->msg = "This is wrong password or email";
                    return $this->result = 0;
                }
            }

        }
        else
        {
            $this->msg = "This is wrong password or email";
            return $this->result = 0;
        }
    }

    //check for password
    function check_password($password, $store_password, $tmp_password) {
        if ($store_password != $this->set_password($password)) {
            if ($tmp_password == '' || $tmp_password != $this->set_password($password)) {
                return -7;
            }
            else
            {
        		return 1;    	
            }
        }
        else
        {
        	return 1;
        }
        
    }
   
    function set_password($password) {
        $key = hash('sha256', $password . __SALT__);
        return $key;
    }


    //Get daily special board data for today
    function GetDailySpecialBoard() {
        $this->content = array();
        $dsbsql = null;

        $sql = "Select ID, restaurant, headline, description, category, image, deal_date, code from daily_special_board where date(now()) = date(deal_date)";
        $dsbsql = pdo_multiple_select($sql,"aurora");
        
        if(count($dsbsql)>0)
        {
            $x = 0; 
            foreach($dsbsql as $data) 
            {      
                $content[$x]['content'] = $data;
                $x++;
            }

            $this->content = $content;
            return $this->result = 1;
        }
        else
        {
            $this->msg = "No Daily Specials for Today";
            return $this->result = 0;
        }
       

    } //End of Get daily special board data for today

    //get magazine
    function getMagazine() {
            ini_set('default_charset', 'utf-8');
        $sql = "SELECT p1.ID,p1.post_title,p1.post_status,p1.post_date,p1.post_content,p1.guid,wm2.meta_value FROM wy_posts 
                p1 LEFT JOIN wy_postmeta wm1 ON ( wm1.post_id = p1.ID AND wm1.meta_value IS NOT NULL AND wm1.meta_key = '_thumbnail_id' ) 
                LEFT JOIN wy_postmeta wm2 ON ( wm1.meta_value = wm2.post_id AND wm2.meta_key = 'amazonS3_info' AND wm2.meta_value IS NOT NULL ) 
                WHERE p1.post_status='publish' AND post_type = 'post' ORDER BY p1.post_date DESC ";

        $article = pdo_multiple_select($sql, 'blog');
        $identifier = 'x';
        $blog_article = array();
        foreach ($article as $ba) {
            $meta = unserialize($ba['meta_value']);
            $ba['imageUrl'] = $meta['key'];
            if (!empty($ba['imageUrl'])) {
                $filePath = pathinfo($ba['imageUrl']);
                $ba['imageUrl'] = $filePath['dirname'] . '/' . $filePath['filename'] . '-356' . $identifier . '220' . '.' . $filePath['extension'];
            }
            if (strlen($ba['post_content']) > 50)
                $ba['description'] = substr($ba['post_content'], 0, 50) . '...';
                $ba['description'] = preg_replace("/’/", "\"", $ba['description']);  //utf8_encode($ba['description']);
                $ba['description']  = preg_replace('/[^a-z0-9$¢£€¥ ]+/ui', ' ', $ba['description']);

            $blog_article[] = $ba;
        }

        return $blog_article;
    }

    


    function md_restaurant() {

        $result = array();

        //$sql = "SELECT ID, code, name, country, city, address, postcode, email, phone, owner, cuisine, url, pricepoints, logo, cover, status, owner_title, firstname, lastname, owner_email, owner_phone, type FROM dsb_registration ORDER BY title ASC";

        //Production
        // $sql = "SELECT 
        //             title as title,
        //             city as city,
        //             cuisine as cuisine,
        //             '$$$' as price,
        //             '?' as dailyspecials,
        //             'partner' as partner
        //         FROM 
        //             weeloy88_business.restaurant
        //         where status  = 'active'
        //         UNION All
        //         Select
        //             name as title,
        //             city as city,
        //             cuisine as cuisine,
        //             '$$$' as price,
        //             '?' as dailyspecials,
        //             'nonpartner' as partner
        //         FROM
        //             production.dsb_registration";

        $sql = "SELECT 
                    title as title,
                    city as city,
                    cuisine as cuisine,
                    '$$$' as price,
                    'DailySpecials' as dailyspecials,
                    'partner' as partner
                FROM 
                    weeloy88_business.restaurant
                where status  = 'active'
                UNION All
                Select
                    name as title,
                    city as city,
                    cuisine as cuisine,
                    pricepoints as price,
                    'DailySpecials' as dailyspecials,
                    'nonpartner' as partner
                FROM
                    production.dsb_registration";
        
        $data = pdo_multiple_select($sql,"aurora");

        foreach ($data as $key => $resto) {
            $this->restogeneric = $resto['partner'];
            $md_verified = $this->readGeneric('md_verified');
            
            if ($md_verified == 'N' OR $md_verified == '') {
                array_push($result, $resto);
            }
            
        }
        return $result;
    }

    function readGeneric($label) {
        if($this->restogeneric === '')
            return "";
        $generic = preg_replace("/’/", "\"", $this->restogeneric);
        $obj = json_decode($generic, true);
        return (isset($obj[$label])) ? $obj[$label] : "";
    }
    public function registerDSBRestaurant($restaurant, $files) {
        //$_POST["Stage"] = "Load";
        if (isset($restaurant) && $restaurant != null && isset($restaurant['country']) && !empty($restaurant['country']) && isset($restaurant['city']) && !empty($restaurant['city']) && isset($restaurant['name']) && !empty($restaurant['name'])) {
            //error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": restaurant: ".print_r($restaurant, true));
            $id = $this->getCountryIsoCode($restaurant['country']) . '_' . $this->getCityIsoCode($restaurant['city']) . '_D_' . $restaurant['name'];
            $logo = '';
            $cover = '';
            $logoFileName = '';
            if (isset($restaurant['images']['logo']) && $restaurant['images']['logo'] != null && $restaurant['images']['logo']['media_type'] == 'picture') {
                $logo = $restaurant['images']['logo']['name'];
                if (!empty($logo))
                    $logoFileName = "Logo.".pathinfo($restaurant['images']['logo']['name'], PATHINFO_EXTENSION);
            }
            if (isset($files) && count($files)) {
                $media = new DSB_Media($id);
                //error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": ".count($files['name'])." files");
                $destination = "";
                for ($i = 0; $i < count($files['name']); $i++) {
                    error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": processing ".$files['name'][$i]);
                    // Logo: 150x150; Cover: 2560x965
                    if ($files['name'][$i] == $logo) {
                            $media->uploadImage($files['tmp_name'][$i], $logoFileName);
                       } else {
                        $cover = $files['name'][$i];
                        $media->uploadImage($files['tmp_name'][$i], basename($files['name'][$i]));
                       
                    }
                }
            }
            $sql_check = "SELECT ID FROM dsb_registration WHERE name = '".$id."' LIMIT 1";
            $sql_ins = "INSERT INTO dsb_registration (code, name, country, city, address, postcode, email, phone, owner, cuisine, url, pricepoints, logo, cover, status, owner_title, firstname, lastname, owner_email, owner_phone, type) VALUES ('$id','".$restaurant['name']."', '".$restaurant['country']."', '".$restaurant['city']."', '".$restaurant['address']."', '".$restaurant['postalcode']."', '".$restaurant['restaurantemail']."', '".WY_StringProcessor::FilterPhoneNumber($restaurant['restaurantphone'])."', '".$restaurant['owner']."', '".$restaurant['cuisines']."', '".$restaurant['website']."', '".$restaurant['pricepoint']."', '$logoFileName', '$cover', 'dsb', '".$restaurant['title']."', '".$restaurant['firstname']."', '".$restaurant['lastname']."', '".$restaurant['email']."', '".$restaurant['phone']."', '".$restaurant['type']."')";
            //error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": sql_check: ".$sql_check.", sql_ins: ".$sql_ins);
            $result = pdo_insert_unique($sql_check, $sql_ins, "aurora");
            if ($result == 0)
                error_log(__FILE__." ".__FUNCTION__." ".__LINE__." New restaurant: ".$restaurant['name']." saved");
            else {
                $sql = "UPDATE dsb_registration SET name = :name, country = :country, city = :city, address = :address, postcode = :postcode, email = :email, phone = :phone, owner = :owner, cuisine = :cuisine, url = :url, pricepoints = :pricepoints, logo = :logo, cover = :cover, owner_title = :title, firstname = :firstname, lastname = :lastname, owner_email = :owner_email, owner_phone = :owner_phone, type = :type WHERE ID = :ID";
                $parameters = array('name' => $restaurant['name'], 'country' => $restaurant['country'], 'city' => $restaurant['city'], 'address' => $restaurant['address'], 'postcode' => $restaurant['postalcode'], 'email' => $restaurant['restaurantemail'], 'phone' => WY_StringProcessor::FilterPhoneNumber($restaurant['restaurantphone']), 'owner' => $restaurant['owner'], 'cuisine' => $restaurant['cuisines'], 'url' => $restaurant['website'], 'pricepoints' => $restaurant['pricepoint'], 'logo' => $logoFileName, 'cover' => $cover, 'owner_title' => $restaurant['title'], 'firstname' => $restaurant['firstname'], 'lastname' => $restaurant['lastname'], 'owner_email' =>$restaurant['email'], 'owner_phone' => $restaurant['phone'], 'type' => $restaurant['type'], 'ID' => $result);
                //error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": sql: ".$sql." params: ".print_r($parameters, true));
                pdo_update($sql, $parameters, "aurora");
            }
            return $this->result = 0;
        } else
            error_log(__FILE__." ".__FUNCTION__." ".__LINE__.": Invalid input parameters: ".print_r($_REQUEST, true));
         return $this->result = 0;
    }
   
}


?>
