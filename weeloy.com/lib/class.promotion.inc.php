<?php

class WY_Promotion {

    use CleanIng;

    var $dirname;
    var $globalshow;
    var $restaurant;
    var $name;
    var $description;
    var $value;
    var $tnc;
    var $start;
    var $end;
    var $is_default;
    var $timeline;
    var $promotionInfo;
    var $result;
    var $msg;

    function __construct($theRestaurant) {
        $this->restaurant = $theRestaurant;
        $this->result = 0;
        $this->msg = "";
        $this->dirname = __UPLOADDIR__ . "$theRestaurant/";
        if (AWS) {
            $this->globalshow = __S3HOST__ . __S3DIR__;
        } else {
            $this->globalshow = __SHOWDIR__;
        }
    }

    private function retval($val, $msg) {
        $this->result = $val;
        $this->msg = $msg;
        return array($val, $msg);
    }

    // for backward compatibility
    function insert($pname, $promotionvalue = NULL, $promotiontnc = NULL) {
        return $this->fullsave($pname, $_REQUEST['offer'], $_REQUEST['description'], $_REQUEST['start'], $_REQUEST['end'], $promotiontnc);
    }

    function fullinsert($pname, $offer, $description, $start, $end, $tnc, $cpp_type) {

        if($cpp_type == 'credit suisse'){
            $cpp_type = 'cpp_credit_suisse';
        }
        
        $arg = array('pname', 'offer', 'description', 'start', 'end', 'tnc');
        for ($i = 0; $i < count($arg); $i++) {
            $tt = $arg[$i];
            $$tt = $this->clean_input($$tt);
        }

        if ($pname == "")
            return $this->retval(-1, "Promotion name is invalid -$pname-");

        var_dump("SELECT name FROM promotion WHERE restaurant = '$this->restaurant' and name = '$pname' limit 1");
        $data = pdo_single_select("SELECT name FROM promotion WHERE restaurant = '$this->restaurant' and name = '$pname' limit 1");
        if (count($data) > 0)
            return $this->retval(-1, "Promotion $pname already exists. No new promotion created");

        $data = pdo_single_select("SELECT MAX(value) as value FROM promotion WHERE restaurant = '$this->restaurant' ");
        $is_default = ($data['value'] == 0) ? 1 : 0;
        $value = $data['value'] + 1;
        pdo_exec("INSERT INTO promotion (restaurant, name, offer, description, start, end, value, is_default, tnc, cpp_type) VALUES ('$this->restaurant', '$pname', '$offer', '$description', '$start', '$end', '$value', '$is_default', '$tnc', '$cpp_type')");

        return $this->retval(1, "Promotion $pname has been created");
    }

    function fullupdate($pname, $offer, $description, $start, $end, $tnc, $cpp_type) {

        $arg = array('pname', 'offer', 'description', 'start', 'end', 'tnc', 'cpp_type');
        for ($i = 0; $i < count($arg); $i++) {
            $tt = $arg[$i];
            $$tt = $this->clean_input($$tt);
        }

        if ($pname == "")
            return $this->retval(-1, "Promotion name is invalid -$pname-");
        $data = pdo_single_select("SELECT name FROM promotion WHERE restaurant = '$this->restaurant' ");
        if (count($data) < 0)
            return $this->retval(-1, "Promotion $pname does not exists");
        list($start, $end) = $this->orderdate($start, $end);

        pdo_exec("UPDATE promotion set offer='$offer', description='$description', start='$start', end='$end' , cpp_type='$cpp_type' where restaurant = '$this->restaurant' and name = '$pname' limit 1");
        return $this->retval(1, "Promotion $pname has been updated");
    }

    function delete($pname) {
        $pname = $this->clean_input($pname);
        if ($pname == "")
            return $this->retval(-1, "Promotion name is invalid -$pname-");

        $theRestaurant = $this->restaurant;
        $affected_rows = pdo_exec("DELETE FROM promotion WHERE name = '$pname' and restaurant = '$theRestaurant' LIMIT 1");
        return $this->retval(1, "Promotion $pname has been deleted");
    }

    function setDefault($pname) {
        $pname = $this->clean_input($pname);
        if ($pname == "")
            return $this->retval(-1, "Promotion name is invalid -$pname-");

        pdo_exec("UPDATE promotion SET is_default = 0 WHERE restaurant = '$this->restaurant' AND is_default = 1  AND cpp_type IS NULL");
        pdo_exec("UPDATE promotion SET is_default = 1 WHERE restaurant = '$this->restaurant' AND name = '$pname'  AND cpp_type IS NULL limit 1");
        return $this->retval(1, "$pname has been set as default");
    }

    function setupPromo($pname, $value) {
        $pname = $this->clean_input($pname);
        if ($pname == "")
            return $this->retval(-1, "Promotion name is invalid -$pname-");

        if ($value == 1)
            $this->retval(-1, "Promotion $pname is already to the top");

        $value = intval($value);
        $mvalue = $value - 1;

        pdo_exec("UPDATE promotion SET value = '$value' WHERE restaurant = '$this->restaurant' AND value = '$mvalue' limit 1");
        pdo_exec("UPDATE promotion SET value = '$mvalue' WHERE restaurant = '$this->restaurant' AND name = '$pname' limit 1");

        $this->retval(1, "Promotion $pname is has been prioritized one up");
    }

    function readPromotionName() {
        $this->result = 1;
        $theRestaurant = $this->restaurant;
        return pdo_column("SELECT name FROM promotion where restaurant = '$theRestaurant' ORDER BY morder");
    }

    function getPromotionsList() {
        $this->result = 1;
        $theRestaurant = $this->restaurant;
        return pdo_multiple_select("SELECT ID, name, restaurant, offer, value, tnc, morder, description, start, end, is_default, cpp_type as cpp FROM promotion WHERE restaurant ='$theRestaurant' ORDER by value");
    }

    public function getDefaultPromotionDetails() {
        $this->result = 1;
        $data = pdo_single_select("SELECT  offer,value, tnc, end FROM promotion WHERE restaurant = '$this->restaurant' AND is_default = 1  AND cpp_type IS NULL");
        
        return (!empty($data['offer'])) ? $data : 'NO PROMOTION AVAILABLE';
    }

    public function getActivePromotionDetails() {
        $this->result = 1;
        $data = pdo_single_select("SELECT  ID,offer,value, tnc, end FROM promotion WHERE restaurant = '$this->restaurant'  AND cpp_type IS NULL ORDER BY value asc  LIMIT 1");

        if (isset($data['offer']) && $data['offer'] != '') {
            return $data;
        } else {
            return $this->getDefaultPromotionDetails();
        }
        return false;
    }

    private function orderdate($start, $end) {
        $startAr = explode("-", $start);
        $endAr = explode("-", $end);

        if ($endAr[0] < $startAr[0] || ($endAr[0] == $startAr[0] && $endAr[1] < $startAr[1]) || ($endAr[0] == $startAr[0] && $endAr[1] == $startAr[1] && $endAr[2] < $startAr[2])) {
            return array($end, $start);
        }
        return array($start, $end);
    }

}

?>
