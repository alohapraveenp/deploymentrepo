<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once "lib/class.notification.inc.php";
require_once("lib/class.restaurant.inc.php");
require_once("lib/class.paypal.inc.php");
require_once("lib/class.booking.inc.php");
require_once("lib/composer/vendor/stripe/stripe-php/init.php");
require_once("lib/class.analytics.inc.php");
require_once("lib/class.event.inc.php");
require_once("lib/class.replication.inc.php");
/**
 * Description of class
 *
 * @author kala
 */
class WY_Payment {
    var $msg;
    var $result;

//put your code here
    function __construct() {
        

    }
    
    //get weeloy payment method list
    
    function getPaymentMethodList(){
         $data = pdo_multiple_select("SELECT * from wp_payment_method where status ='active'");
         return $data;
    }
    
    function savePaymentServices($data){
         $sql = "INSERT INTO restaurant_payment_service (restaurant,payment_type, payment_mode, product,status) VALUES
                 ('{$data['restaurant']}','{$data['payment_type']}', '{$data['payment_mode']}', '{$data['product']}','{$data['status']}')";

        $result =  pdo_insert($sql);
  
        return $result;
        
    }
    function updatePaymentServices($data){
        
       $result = pdo_exec("update restaurant_payment_service  set status='{$data['status']}',payment_type ='{$data['payment_type']}',payment_mode ='{$data['payment_mode']}'  where restaurant ='{$data['restaurant']}' and product = '{$data['product']}' limit 1");
       return $result;
        
    }
    function getPaymentServices($restaurant){
         $data = pdo_multiple_select("SELECT * from restaurant_payment_service where restaurant ='$restaurant'");
         return $data;
        
    }

    function depositrefund($deposit,$restaurant, $confirmation, $email, $token,$amount,$type){

        $response = array(); 
        $paypal = new WY_Paypal($restaurant);
        $logger = new WY_log("website");
        $booking = new WY_Booking();
        $booking->getBooking($confirmation);
        $bookingid = (isset($booking->bookid)) ? $booking->bookid : $confirmation;
        $paymentdetails = array();

        if($type === 'backoffice'){
            $loguserid = (isset($_SESSION['user_backoffice']['id'])) ? $_SESSION['user_backoffice']['id'] : "unknown";
        }else{
            $loguserid = (isset($_SESSION['user']['user_id'])) ? $_SESSION['user']['user_id'] : "unknown";
        }

        $logger->LogEvent($loguserid, 1000, 1, $bookingid,'paypal_refund', date("Y-m-d H:i:s"));

        if(!empty($deposit)){$deposit = trim($deposit);}
        $senderEmail = $email;

        $senderDetails = $paypal->getPaymentDetails($deposit,'refund');

        if(isset($senderDetails)){
            $senderEmail = $senderDetails['senderEmail'];
        }
 
        $data = array('payKey'=> $deposit,"requestEnvelope"=> $paypal->envelope,'currencyCode' => 'SGD',
            "receiverList" => array(
                "receiver" => array(
                    array(
                        "amount" => $amount,
                        "email"  => $senderEmail,
                    )
                ),
        ));
        

        $response = $paypal->_paypalSend($data, "Refund", $paypal->apiUrl);

        $status ="refundfailed";
        $paymentdetails['status'] = $status;
        if (strstr($_SERVER['HTTP_HOST'], 'localhost')){  
            
            $response['responseEnvelope']['ack'] = "Success";
            $response['payKey'] = 'AP-6SD77277FY187810D';
            $response['refundInfoList']['refundInfo'][0]['receiver']['amount'] = $amount;
            $response['refundInfoList']['refundInfo'][0]['refundStatus'] = 'REFUNDED';
        }

        if ($response['responseEnvelope']['ack'] == "Success") {

          $logger->LogEvent($loguserid, 1000, 2, $bookingid,'paypal_refund_success', date("Y-m-d H:i:s"));
          $refundAmount= $response['refundInfoList']['refundInfo'][0]['receiver']['amount'];
          $status = $response['refundInfoList']['refundInfo'][0]['refundStatus'];
          $paymentdetails['status'] = $status;

          $this->updateRefundDetails($status,$deposit,$refundAmount);
            //$this->updatePayStatus($confirmation,$deposit,$status);
            return $paymentdetails;

        }else{
          $logger->LogEvent($loguserid, 1000, 2, $bookingid,'paypal_refund_failed', date("Y-m-d H:i:s"));
        }
        return $paymentdetails;

    }



    function savePayment($amount,$object_type,$object_id,$receiver,$payment_id,$payment_method,$status,$refund,$refund_amount,$objStatus='pay',$currency ='SGD'){

        $sql = "INSERT INTO payment (amount, currency, object_type, object_id, receiver, payment_id, payment_method, status, refund,refund_amount,object_status,created_at, updated_at) VALUES
                 ('$amount', '$currency', '$object_type', '$object_id', '$receiver', '$payment_id', '$payment_method', '$status', '$refund','$refund_amount','$objStatus', NOW(), NOW())";
   
        $result = pdo_insert($sql);

        if ($object_type == 'booking_deposit') {
            if(preg_match('/localhost|dev\./' , $_SERVER['HTTP_HOST']) || $receiver == 'SG_SG_R_TheFunKitchen') {
                WY_Replication::replicate($payment_id, 'ins_payment');
            } 
        }

        return $result;

    }
    function saveDeposit($data,$amount,$receiver,$token,$payment_id,$status='',$deposit_type = null,$confirmation,$currency ='SGD'){

        $sql = "INSERT INTO booking_deposit (data,amount, curency, time,receiver, token, paykey,object_id,payment_method, status,created_at, updated_at) VALUES
                 ('$data','$amount', '$currency', '', '$receiver','$token', '$payment_id','$confirmation','$deposit_type', '$status',NOW(), NOW())";

        $result =  pdo_insert($sql);
  
        if(preg_match('/localhost|dev\./' , $_SERVER['HTTP_HOST']) || $receiver == 'SG_SG_R_TheFunKitchen') {
            WY_Replication::replicate($confirmation, 'ins_bkdeposit');
        } 

        return $result;

    }
    
    function updateccDetails($payment_id,$object_id,$amount,$status){
       
         $bk_ccdetails = pdo_exec("update booking_deposit  set status='$status',amount ='$amount', status ='$status'  where paykey ='$payment_id' and object_id = '$object_id' limit 1");
        if(preg_match('/localhost|dev\./' , $_SERVER['HTTP_HOST'])) {
            WY_Replication::replicate($object_id, 'upd_bkdeposit');
        }   
            $this->updatePaymentId($payment_id,$object_id);
         return 1;
    }

    
    function updatePaymentId($payment_id,$confirmation){
        
        if(isset($confirmation)){
            $data = pdo_single_select("SELECT * from booking where confirmation ='$confirmation' limit 1");
            if(count($data)>0){
                $bk_payment = pdo_exec("update booking  set status ='', booking_deposit_id='$payment_id' where confirmation ='$confirmation' limit 1");
                if(preg_match('/localhost|dev\./' , $_SERVER['HTTP_HOST'])) {
                    WY_Replication::replicate($confirmation, 'upd_booking');
                } 
                return 1;
            }else{
                return 0;
            }
            
        }
        
    }

    function checkValidBooking($confirmation){
        
         if(isset($confirmation)){
            $data = pdo_single_select("SELECT * from booking where confirmation ='$confirmation' limit 1");
            if(count($data)>0){
                return 1;
            }
        }
        return 0;
        
    }
    function updatePayment($object_id,$amount){
        if(isset($object_id)){
            $data = pdo_exec("update booking_deposit  set amount ='$amount'  where object_id = '$object_id' limit 1");
            if(count($data)>0){
                    return 1;
                }
            }
        return 0;
    }

    
    
    //get depsoit details  using deposit id
    function geDepositById($depositId,$type ='paykey'){
        $this->result = 1;
        if (empty($depositId)) {
        $this->result = -1;
        $this->msg = "invalid depositId";
            return 0;
        }
        if($type === 'booking_id'){
             $query = "object_id = '$depositId' ";
        }else{
            $query = "paykey = '$depositId' ";
        }

        $data = pdo_single_select("SELECT * from booking_deposit where $query limit 1 ");
        return $data;
    }
    function updatePayStatus($confirmation, $paykey, $card_id,$status, $payment_method=null){
        $notify = 0;
        $data = pdo_single_select("SELECT * from booking_deposit where paykey='$paykey' and object_id='$confirmation' and  status ='' limit 1 ");
        
        if(count($data)>0){
          $notify =1;
        }

        if(isset($confirmation)){
            if($status === 'COMPLETED'){
                $bk = pdo_exec("update booking set status='' where confirmation='$confirmation' limit 1");
                if(preg_match('/localhost|dev\./' , $_SERVER['HTTP_HOST'])) {
                    WY_Replication::replicate($confirmation, 'upd_booking');
                } 
            }
            $bk_deposit = pdo_exec("update booking_deposit  set status='$status',card_id ='$card_id' where paykey='$paykey' and object_id='$confirmation'  limit 1");
            if(preg_match('/localhost|dev\./' , $_SERVER['HTTP_HOST'])) {
                WY_Replication::replicate($confirmation, 'upd_bkdeposit');
            }
            if($payment_method !== 'carddetails'){
                $bk_payment = pdo_exec("update payment set status='$status',object_status='$status' where payment_id ='$paykey' limit 1");
                if(preg_match('/localhost|dev\./' , $_SERVER['HTTP_HOST'])) {
                    WY_Replication::replicate($paykey, 'upd_payment');
                }
            }
        }
        return $notify;
    }

    
     

    
    function updateRefundDetails($status,$payment_id,$amount){
        $bk_payment = pdo_exec("update payment  set status='$status',refund='1', refund_amount ='$amount', object_status ='$status'  where payment_id ='$payment_id' limit 1");
        if(preg_match('/localhost|dev\./' , $_SERVER['HTTP_HOST'])) {
            WY_Replication::replicate($payment_id, 'upd_payment');
        }          
        return 1;
    }

    function updatepaymentDetails($status,$payment_id,$amount,$chargeId){

        $bk_payment = pdo_exec("update payment set status='$status',amount ='$amount', object_status ='$status',object_id='$payment_id'  where payment_id ='$payment_id' limit 1");
        if(preg_match('/localhost|dev\./' , $_SERVER['HTTP_HOST'])) {
            WY_Replication::replicate($payment_id, 'upd_payment');
        }          
        return 1;
    }

    private function getDepositCurency($deposit){
        $curency ='SGD';
        $data = pdo_single_select("SELECT curency from booking_deposit where paykey='$deposit' limit 1 ");
        if(count($data)>0){
          $curency =$data['curency'];
        }
        return $curency;
    }
    public function getDepositDetails($depositId,$objectId) {
        //$data = pdo_single_select("SELECT * from booking_deposit where paykey = '$depositId' limit 1 ");
        $sql = "SELECT * FROM booking_deposit  WHERE paykey = '$depositId' AND object_id='$objectId'   limit 1 ";
        //error_log($sql);
        $data = pdo_single_select($sql);
        if (count($data) > 0) {
            return $data;
        }
        return 0;
    }
    public function getPaymentDetails($depositId,$mode = 'payment_id'){
        if($mode === 'payment_id'){
             $qery = "payment_id = '$depositId' ";
        }else{
            $qery = "object_id = '$depositId' ";
        }
        $data = pdo_single_select("SELECT * from payment where $qery limit 1 ");
        if(count($data)>0){
            return $data;
        }
        return 0;
        
    }
    
    public function stripe_api_log($booking_id,$restaurant,$header,$parameters,$server_output,$response_code){
       
        if($response_code == 200){
            $sql = "INSERT INTO log_stripe_api (`booking_id`, `restaurant`, `headers`, `parameters`, `response`) VALUES ('$booking_id', '$restaurant', '".json_encode($header)."',  '".json_encode($parameters)."', '".json_encode($server_output)."');";
            pdo_exec($sql, 'dwh');
            return true;
        }else{
            $sql = "INSERT INTO log_stripe_api_error (`booking_id`, `restaurant`,`headers`, `parameters`, `response`) VALUES ('$booking_id', '$restaurant', '".json_encode($header)."',  '".json_encode($parameters)."', '".json_encode($server_output)."');";
            pdo_exec($sql, 'dwh');
            return true;
        }
        
    }
    
    public function updateEventPaymentId($event_id,$order_id,$status,$payment_id,$card_id){
        $bk_payment = pdo_exec("update event_booking  set status='$status',paykey='$payment_id',card_id='$card_id'  where orderID='$order_id' and event_id='$event_id' limit 1");
        $sql = "SELECT object_id FROM event_booking WHERE orderID = '$order_id' and event_id='$event_id' limit 1";
        $data = pdo_single_select($sql);
        if(count($data) > 0){
            $confirmation = $data['object_id'];
            $this->updatePaymentId($payment_id,$confirmation);
        }
        return 1;
    }
    public function update($event_id,$order_id,$status,$payment_id,$card_id){
        $bk_payment = pdo_exec("update event_booking  set status='$status',paykey='$payment_id',card_id='$card_id'  where orderID='$order_id' and event_id='$event_id' limit 1");
        return 1;
    }
    
    
    public function getHeader(){
        //if (!function_exists('getallheaders'))  {
            if (!is_array($_SERVER)) {
                return array();
            }

            $headers = array();
            foreach ($_SERVER as $name => $value) {
               
                if (substr($name, 0, 5) == 'HTTP_') {
                    $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
                }
            }
           
            return $headers;
        //}
    }
    
    
    public function getEventPaymentDetails($order_id){
          
        if(empty($order_id)){
            $this->result = 0;
            $this->msg = "invalid OrderId";
            return false;
        }   
        $event = new WY_Event();
        $evt_payment = $event->getEventTicket($order_id);
        $this->result = 1;
        $this->msg = "success";
        return $evt_payment;
   
    }
    
    public function getRestaurantPayment($restaurant){
        $data = pdo_multiple_select("SELECT * from payment where receiver = '$restaurant' AND object_type ='invoice' ");
        return $data;
    }
    
    public static function updatePaypalStatus($status,$order_id){
       $bk_payment = pdo_exec("update payment set status='$status'where object_id ='$order_id' limit 1");      
       return 1; 
    }
    public function getBookingDepDetails($confirmation){
       if(empty($confirmation))
            return;
        $chargeAr = array();
        $booking = new WY_Booking();
        $booking->getbooking($confirmation);
        $payment = new WY_Payment(); 
        $isPayment = 0;
        $sql = "SELECT d.amount, d.curency as currency, d.receiver,d.paykey as payment_id, d.object_id, d.payment_method, d.status,p.payment_id,p.refund,p.refund_amount,p.status as payment_status FROM ( SELECT amount, curency, receiver, paykey, object_id, payment_method,status FROM booking_deposit WHERE paykey ='$booking->deposit_id' AND object_id ='$confirmation') AS d LEFT JOIN ( SELECT refund,refund_amount,payment_id,object_id,status FROM payment ) AS p ON d.paykey = p.payment_id AND d.object_id = p.object_id limit 1";
        $data = pdo_single_select($sql);
        if (count($data) >0) {
            if ($data['status'] == 'COMPLETED' && $data['payment_status'] !== 'REFUNDED') {
                $data['isPayment'] = 1;
                $data['refund_amount'] =  $data['amount'];
            }
            if($data['payment_status'] == 'REFUNDED'){
                $data['status'] =  $data['payment_status'];
                $data['isPayment'] = 1;
            }
        }
        return $data;
        // $sql = "SELECT r.reviewgrade as score, r.comment, r.reviewdtvisite as booking_date, r.reviewdtcreate as post_date, m.firstname as user_name, r.user_id as email FROM ( SELECT reviewgrade,comment,reviewdtvisite,reviewdtcreate,user_id FROM review WHERE restaurant ='$theRestaurant' AND reviewgrade >2) AS r LEFT JOIN ( SELECT firstname,email FROM member ) AS m ON r.user_id = m.email ORDER BY r.reviewgrade DESC";
   
        
    }
    public function saveReddotPaymentDetails($confirmation,$payment_id,$status){
        $payment_status = ($status == 'Paid') ? 'COMPLETED' : 'pending_payment';
        $notify = 7;

        $data = pdo_single_select("SELECT * from booking_deposit where  object_id='$confirmation' limit 1 ");

        if(count($data)>0){
            if($data['status'] == 'pending_payment' || $data['status'] == 'rejected'){
                $notify = 8;
            }
        }
        //update payment_id in booking table
        if($status == 'Paid' && ($data['status'] == 'pending_payment' || $data['status'] == 'rejected')) {
            $this->updatePaymentId($payment_id,$confirmation);
            
            // update payment_id in booking_Deposit table 
            $bk_ccdetails = pdo_exec("update booking_deposit  set paykey='$payment_id',status='$payment_status' where object_id = '$confirmation' limit 1");
            if(preg_match('/localhost|dev\./' , $_SERVER['HTTP_HOST'])) {
                WY_Replication::replicate($confirmation, 'upd_bkdeposit');
            }             
            $depDetails = $this->geDepositById($confirmation,'booking_id');
            //save payment details in payment table
            $this->savePayment($depDetails['amount'],'booking',$confirmation,$depDetails['receiver'],$payment_id,$depDetails['payment_method'],$payment_status,'','','deposit',$depDetails['curency']);
        }
        if($notify == 8){
            $booking = new WY_Booking();
            $booking->getBooking($confirmation);
            $notification = new WY_Notification();
            $notification->notify($booking, 'booking');
        }
        
        return $notify;
       
    }
    public function saveReddotPaymentStatus($confirmation,$status){
        $notify = 7;
        $data = pdo_single_select("SELECT * from booking_deposit where  object_id='$confirmation' and  status ='pending_payment' limit 1 ");
        if(count($data)>0){
          $notify = 8;
          $bk_ccdetails = pdo_exec("update booking_deposit set  status='rejected' where object_id = '$confirmation' limit 1");
            if(preg_match('/localhost|dev\./' , $_SERVER['HTTP_HOST'])) {
                WY_Replication::replicate($confirmation, 'upd_bkdeposit');
            }             
        }
        if($notify == 8){
            $booking = new WY_Booking();
            $booking->getBooking($confirmation);
            $notification = new WY_Notification();
            $notification->notify($booking, 'booking_pending');
        }
         return $notify;
        
    }
    
      //if booking status pending payment 

    public function getBookingppDetails($bkrestaurant,$refid){
       $res = new WY_restaurant;
       $booking = new WY_Booking();
       $booking->getBooking($refid);
       $generic = $booking->generic;
       $product = '';
       if(!empty($generic)) {
                    $genAr = json_decode(preg_replace("/’/", "\"", $generic), true);
                    if(isset($genAr['choice'])){
                       $product = $genAr['choice'];
                    }
        }
        $depositDetails = $res->getCancelPolicyAll($bkrestaurant,  '', 0, $product,$booking->rdate ,$booking->rtime,$booking->cover);  
       if($bkrestaurant == 'SG_SG_R_Pollen' || $bkrestaurant == 'SG_SG_R_Nouri'){
           $depositDetails['charge']  = $depositDetails['charge'] * $booking->cover;
       }
       $deposit = array(
           'amount' => $depositDetails['charge'],
           'currency' => 'SGD',
           'time_left' => ''
       );
       return $deposit;
        // $deposit = array();
        // $data = pdo_single_select("SELECT * from booking_deposit where  object_id='$refid' and receiver ='$bkrestaurant' limit 1 ");
        // if(count($data)>0){
        //     $deposit = array(
        //         'amount' => $data['amount'],
        //         'currency' => $data['curency'],
        //         'time_left' => ''
        //     );
 
        // }
        // return $deposit;
        
    }
     public function saveCredentials($params){
        $restaurant = $params['restaurant'];
        if(empty($restaurant)){
            return  false;
        }
       $data1 = pdo_single_select("SELECT * FROM restaurant_payment  where restaurant_id LIKE '%$restaurant%'  ");
       if (count($data1 > 0)) {
           pdo_exec("delete from restaurant_payment where restaurant_id LIKE '%$restaurant%'  ");
       }
        $query = "INSERT INTO restaurant_payment (restaurant_id,paypal_id,stripe_public_key,stripe_secret_key,reddot_mid,reddot_key,reddot_secret_key) VALUES ('$restaurant','{$params['paypal_id']}','{$params['stripe_public_key']}','{$params['stripe_secret_key']}','{$params['reddot_mid']}','{$params['reddot_key']}','{$params['reddot_secret_key']}')";
        $data = pdo_insert($query);
        return $data;
        
    }
    public function getRestaurantcrd($restaurant){
        if(empty($restaurant)){
            return  false;
        }
         $data = pdo_single_select("SELECT * FROM restaurant_payment  where restaurant_id LIKE '%$restaurant%'  ");
       return $data;

    }
    public function updateCredentials($params) {

        $restaurant = $params['restaurant_id'];

        $sql = "UPDATE restaurant_payment SET paypal_id ='{$params['paypal_id']}',stripe_public_key ='{$params['stripe_public_key']}',stripe_secret_key ='{$params['stripe_secret_key']}',reddot_mid ='{$params['reddot_mid']}',reddot_key='{$params['reddot_key']}',reddot_secret_key='{$params['reddot_secret_key']}' WHERE restaurant_id  ='$restaurant'";
        pdo_exec($sql);

        return 1;
    }
    public function deletepaymentDetails($restaurant) {
 
        pdo_exec("delete from restaurant_payment where restaurant_id LIKE '%$restaurant%'");
        return 1;
        
    }
    public function getAdminPaymentConfig($restaurant){
        if(empty($restaurant)){
            return  false;
        }
        $data = pdo_multiple_select("SELECT * FROM restaurant_cancel_policy_temp where restaurant = '$restaurant'  ");
       return $data;
        
    }
    
   
    

}
