<?php

class WY_Allote {

    var $restaurant;
    var $mealduration;
    var $perpax;    // standard allotement is on table unit. Perpax set it on person
    var $twositting;
    var $multProduct;
    var $product;
    var $nowalking;
    var $ohvalue;
    var $openDay;
    var $openLunch;
    var $openDinner;
    var $openDayHours;
    var $openWeekHours;
    var $openWeekSlots;
    var $laptime;
    var $cutofflunch;
    var $cutoffdiner;
    var $dailycutoff;
    var $aDay;
    var $msg;
    var $result;
    var $maxdaybooking;
    var $bookhours;
    var $HourBySlotflg;
    var $Bookhoursflg;
    var $extraallote;

    function __construct($theRestaurant) {
        $this->restaurant = $theRestaurant;
        $this->mealduration = 3;
        $this->perpax = 0;
        $this->product = "";
        $this->multProduct = array(0);
    }

	private function errormsg($msg, $val) {
		$this->msg = $msg;
		return $this->result = $val;
		}
		
    function testing() {
        $aa = array();
        for ($i = 0; $i < 50; $i++)
            $aa[$i] = "0";
        for ($i = 18; $i < 24; $i++)
            $aa[$i] = "1";
        for ($i = 36; $i < 50; $i++)
            $aa[$i] = "1";
        $aa[39] = "0";
        $dd = implode("", $aa);
        return "BYSLOT|||$dd|||$dd|||$dd|||$dd|||$dd|||$dd|||$dd";
    }

	static function getMealType($rtime) {
		$rtime = substr($rtime, 0, 5);
        $rtimeAr = explode(":", $rtime);
        $slot = (intval($rtimeAr[0]) * 2) + (intval($rtimeAr[1]) / 30);
        return ($slot >= 32) ? 'dinner' : 'lunch';
 		}
		
    function convert16to2($numstr) {
        substr(base_convert("1" . $numstr, 16, 2), 1);
        return $tt;
    }


    function getBitsMealAvail($start_meal, $end_meal, $weekslots, $not_hbs, $nchar_meal, $refStartMeal) {

 	$cd = 0;
	 if ($start_meal >= $end_meal) 
		return $cd;
            	
    $limit = $nchar_meal * 4;  // 4 is the number of bits
	for ($i = 0, $pp = 1; $i < $limit; $i++, $pp <<= 1) { // lunch, set the bit when the time is between start and end, for the 16 digits. Last possible is 16h30 -> slot 33 (everything is divided by 1/2 hour)
		$slot = $i + $refStartMeal;
		if ($slot >= $start_meal && $slot <= $end_meal)
			if ($not_hbs || substr($weekslots, $slot, 1) == "1") // if booking slot activated
				$cd += $pp;
		}
        return $cd;
    }

    function getSlotNumber($rtime) {
        $rtimeAr = explode(":", $rtime);
        return (intval($rtimeAr[0]) * 2) + (intval($rtimeAr[1]) / 30);
    }

    function getOpenHour($ohvalue, $weekday) {

		// openhour is either open hours or book hours
		// 2 encode: byslot or standard
		// byslot is only used for book hours
		// a week is 7 days, start Sunday
		// 7 days, lunch is [start, end] => day 1 (0, 1), day 2 (2, 3), day 3 (4, 5)... day 7 (12, 13)
		// 7 days, dinner is [start, end] => day 1 (14, 15), day 2 (16, 17), day 3 (18, 19)... day 7 (26, 27)
		// so 28 values in total

		if (empty($ohvalue))
			return $this->result = -1;

		$this->openWeekSlots = array(0, 0, 0, 0, 0, 0, 0, 0);
		if ($this->HourBySlotflg && substr($this->bookhours, 0, 6) == "BYSLOT") {
			$byslot = explode("|||", $this->bookhours);
			if (count($byslot) < 8) {
				return $this->result = -1;
				}

			$this->openWeekSlots = array();
			for ($i = 0; $i < 7; $i++) 
				$this->openWeekSlots[] = $vv = substr(base_convert("1" . $byslot[$i + 1], 16, 2), 1);
			}
		 else $this->HourBySlotflg = false;
		
		$valAr = explode("|||", $ohvalue);
		if (count($valAr) < 28)
			return $this->result = -1;

		for ($i = 0; $i < 7; $i++) {
			$k = ($i * 2);
			$this->openWeekHours[$i][0] = intval($valAr[$k]);
			$this->openWeekHours[$i][1] = intval($valAr[$k + 1]);
			$this->openWeekHours[$i][2] = intval($valAr[$k + 14]);
			$this->openWeekHours[$i][3] = intval($valAr[$k + 15]);
			}
			$this->openDay = $this->openLunch = $this->openDinner = -1;
			$this->openDayHours[0] = $this->openDayHours[1] = $this->openDayHours[2] = $this->openDayHours[3] = -1;
			if ($weekday >= 0 && $weekday < 7) {
				for ($i = 0; $i < 4; $i++)
					$this->openDayHours[$i] = $this->openWeekHours[$weekday][$i];
				$this->openLunch = ($this->openDayHours[0] < $this->openDayHours[1]) ? 1 : 0;
				$this->openDinner = ($this->openDayHours[2] < $this->openDayHours[3]) ? 1 : 0;
				$this->openDay = $this->openLunch + $this->openDinner;
			}
			return $this->result = 1;
    }

    // unravel oneway slider construct
    private function setUpRange($col, $a, $b) {
        $start = floor($a * 0.5) . ':' . ($a % 2) * 3 . '0';
        $end = floor($b * 0.5);
        if ($end >= 24)
            $end = "0" . ($end - 24);
        $end = $end . ':' . ($b % 2) * 3 . '0';
        return $start . " - " . $end;
    }

    // unravel the openhour, bookinghour slider construct
    private function setUpMultipleRange($col, $a, $b) {
        $start_range = $a;
        $end_range = $a + 1;
        if ($a == $b) {
            return array();
        }
        while ($a <= $b) {
            $start = floor($start_range * 0.5) . ':' . ($start_range % 2) * 3 . '0';
            $end = floor($end_range * 0.5);
            if ($end >= 24)
                $end = "0" . ($end - 24);
            $end = $end . ':' . ($end_range % 2) * 3 . '0';

            $result[] = $start . " - " . $end;
            $start_range = $end_range;
            $end_range = $end_range + 1;
            $a = $a + 1;

            if ($end_range > 48) {
                $end_range = $end_range - 48;
            }
        }
        return $result;
    }

    //generate html code for template
    function templateAlloteOpenHour($ohvalue, $no_html = false) {

        $array_data = array();
        $tmp_car = array('<br>', '&nbsp;', '&nbsp;', ' ');
        $weekday = array("Sunday", "Monday", "Tuesday", "Wednesday &nbsp;&nbsp;&nbsp;", "Thursday", "Friday", "Saturday");

        $this->getOpenHour($ohvalue, $weekday);
        if ($this->result < 0)
            return ($no_html) ? $array_data : "";

        $template = "<table class='table table-striped' style='font-family:Roboto;font-size:14px;font-weight:bold'>";
        $template .= "<th>Day</th><th>Lunch</th><th>Dinner</th>";
        for ($kk = 0; $kk < 7; $kk++) {
            $col = $kk * 2;
            $ll = $this->setUpRange($kk, $this->openWeekHours[$kk][0], $this->openWeekHours[$kk][1]);
            $dd = $this->setUpRange($kk + 7, $this->openWeekHours[$kk][2], $this->openWeekHours[$kk][3]);
            if ($this->openWeekHours[$kk][0] == $this->openWeekHours[$kk][1])
                $ll = "closed<br>&nbsp;";
            if ($this->openWeekHours[$kk][2] == $this->openWeekHours[$kk][3])
                $dd = "closed<br>&nbsp;";
            $template .= "<tr><td>" . $weekday[$kk] . "</td><td>$ll</td><td>$dd</td></tr>";
            $array_data[$kk] = array('day' => str_replace($tmp_car, '', $weekday[$kk]), 'lunch' => str_replace($tmp_car, '', $ll), 'dinner' => str_replace($tmp_car, '', $dd));
        }
        $template .= "</table>";

        return ($no_html) ? $array_data : $template;
    }

    function templatePickupHours($ohvalue, $no_html = false) {

        $array_data = array();
        $tmp_car = array('<br>', '&nbsp;', '&nbsp;', ' ');
        $weekday = array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");

        $this->getOpenHour($ohvalue, $weekday);
        if ($this->result < 0)
            return ($no_html) ? $array_data : "";
        for ($kk = 0; $kk < 7; $kk++) {
            $col = $kk * 2;
            $ll = $this->setUpMultipleRange($kk, $this->openWeekHours[$kk][0], $this->openWeekHours[$kk][1]);
            $dd = $this->setUpMultipleRange($kk + 7, $this->openWeekHours[$kk][2], $this->openWeekHours[$kk][3]);

            $array_data[$kk] = array('pickuphour' => array_merge($ll, $dd));
        }
        return $array_data;
    }

    function consoBookingAllote($theRestaurant, $aDate) {

        $book = array();
        if ($aDate == "") {
            $data = WY_Booking::getDateBooking($theRestaurant, $this->product);
            $block = WY_Block::getDateBlock($theRestaurant, $this->product);
        } else {
            $data = WY_Booking::getThatDayBooking($theRestaurant, $aDate, $this->product);
            $block = WY_Block::getThatDateBlock($theRestaurant, $aDate, $this->product);
        }

        $perpax = $this->perpax;
        $mealduration = $this->mealduration; //3 slots per repas

        foreach ($data as $row) {
            $bdata = intval($row['ndays']);
            $slottime = $this->getSlotNumber($row['rtime']);
            for ($i = 0; $i < $mealduration; $i++) {
                if (!isset($book[$bdata][$slottime + $i]))
                    $book[$bdata][$slottime + $i] = 0;
                $inc = ($perpax) ? intval($row['cover']) : 1;
                $book[$bdata][$slottime + $i] += $inc;
            }
        }

        foreach ($block as $row) {
            $bdata = intval($row['ndays']);
            $slottime = (intval(substr($row['stime'], 0, 2)) * 2) + (intval(substr($row['stime'], 3, 1)) / 3);
            $nslots = intval($row['slots']);
            for ($i = 0; $i < $nslots; $i++) {
                if (!isset($book[$bdata][$slottime + $i]))
                    $book[$bdata][$slottime + $i] = 0;
                $book[$bdata][$slottime + $i] += intval($row['tablepax']);
            }
        }

        return $book;
    }

    function adjustSitting($year, $zday, $maxday, &$dinner) {

        $tmp = $this->readtwoSitting($this->restaurant, $year);
        if ($this->result < 0)
            return -1;
        $data = explode("|", $tmp);

        $P1mask = $P2mask = ~0;
        $bound = explode("-", $data[0]);
        $first = (intval($bound[0])) - 32;
        $nb = intval($bound[1]);
        if ($nb > 8)
            $nb = 8;

        for ($i = 0; $i < $nb; $i++)
            $P1mask ^= (1 << ($first + $i));

        $bound = explode("-", $data[1]);
        $first = (intval($bound[0])) - 32;
        $nb = intval($bound[1]);
        if ($nb > 8)
            $nb = 8;

        for ($i = 0; $i < $nb; $i++)
            $P2mask ^= (1 << ($first + $i));

        $start = ($zday * 2) + 2;
        if ($maxday > 365 - $zday)
            $maxday = 365 - $zday;
        for ($i = 0; $i < $maxday; $i++) {
            $k = $i * 2;
            if ($data[$k + $start] == 0)
                $dinner[$i] &= $P1mask;
            if ($data[$k + $start + 1] == 0)
                $dinner[$i] &= $P2mask;
        }
    }

    function checkSitting($year, $zday, $slot) {
        $tmp = $this->readtwoSitting($this->restaurant, $year);
        if ($this->result < 0)
            return -1;
        $data = explode("|", $tmp);
        $index = ($zday * 2) + 2;
        $bound = explode("-", $data[0]);
        $first = intval($bound[0]);
        $last = $first + intval($bound[1]);
        if ($data[$index] == 0 && $slot < $last && $slot > $first)
            return -1;
        $bound = explode("-", $data[1]);
        $first = intval($bound[0]);
        $last = $first + intval($bound[1]);
        if ($data[$index + 1] == 0 && $slot < $last && $slot > $first)
            return -1;
        return 1;
    }

    function readtwoSitting($theRestaurant, $year) {
        $this->msg = "";
        $this->result = 1;
        $type = "2sitting";
        $data = pdo_single_select("SELECT ID, restaurant, type, allotment from allotment where restaurant = '$theRestaurant' and type = '$type' and year = '$year' limit 1");
        if (count($data) <= 0) {
            $value = "36-4|40-8";
            for ($i = 0; $i < 366; $i++)
                $value .= "|1|1";
            pdo_exec("insert into allotment (restaurant, type, allotment, year) values ('$theRestaurant', '$type', '$value', '$year') ");
            return $value;
        }
        return $data['allotment'];
    }

    function writetwoSitting($theRestaurant, $value, $year) {
        $this->msg = "";
        $value = preg_replace("/[^0-9\|\-]/", "", $value);
        $tt = explode("|", $value);
        if (count($tt) != 734) {
            $this->msg = "invalid data " . count($tt);
            return $this->result = -1;
        }
        $type = "2sitting";
        $data = pdo_single_select("SELECT ID, restaurant, type, allotment from allotment where restaurant = '$theRestaurant' and type = '$type' and year = '$year' limit 1");
        if (count($data) <= 0) {
            $value = "36-4|40-8";
            for ($i = 0; $i < 366; $i++)
                $value .= "|1|1";
            pdo_exec("insert into allotment (restaurant, type, allotment, year) values ('$theRestaurant', '$type', '$value', '$year')");
            return $this->result = -1;
        }
        pdo_exec("update allotment set allotment = '$value' where restaurant = '$theRestaurant' and type = '$type' and year = '$year' limit 1");
        return $this->result = 1;
    }

    private function readAlloteRestaurant($theRestaurant, $type, $year) {

        $product = $this->product;
        $qryprod = (!empty($this->product)) ? " and product = '$product' " : " ";

        $tmp = $year - intval(date('Y'));
        if (($type != "lunch" && $type != "dinner") || $tmp < 0 || $tmp > 1) {
            WY_debug::recordDebug("ERROR-API", "READALLOTE-1", "saveALLOTE unable to create $type data" . " \n " . "$theRestaurant, $type, $year");
            return "";
        }
        $data = pdo_single_select("SELECT ID, restaurant, type, allotment from allotment where restaurant = '$theRestaurant' and type = '$type' and year = '$year' $qryprod limit 1");
        if (count($data) <= 0) {
            for ($value = $sep = "", $i = 0; $i <= 366; $i++, $sep = "|")
                $value .= $sep . 5;
            pdo_exec("insert into allotment (restaurant, type, allotment, year, product) values ('$theRestaurant', '$type', '$value', '$year', '$product') ");
            $data = pdo_single_select("SELECT ID, restaurant, type, allotment from allotment where restaurant = '$theRestaurant' and type = '$type' and year = '$year' $qryprod  limit 1");
            if (count($data) <= 0) {
                WY_debug::recordDebug("ERROR-API", "READALLOTE-2", "saveALLOTE unable to create $type data" . " \n " . "$theRestaurant, $type, $year");
                return "";
            }
        }
        return $data['allotment'];
    }

    function saveAlloteRestaurant($theRestaurant, $year, $size, $start, $newlunchdata, $newdinnerdata) {

        $product = $this->product;
        $qryprod = (!empty($this->product)) ? " and product = '$product' " : " ";

        $oldlunchdata = $this->readAlloteRestaurant($theRestaurant, 'lunch', $year);
        if ($oldlunchdata == "")
            return $this->result = -1;
        $oldlunchAr = explode("|", $oldlunchdata);
        $newlunchAr = explode("|", $newlunchdata);
        for ($i = 0, $k = $start; $i < $size; $i++, $k++)
            $oldlunchAr[$k] = $newlunchAr[$i];
        $dataStr = implode("|", $oldlunchAr);
        pdo_exec("Update allotment set allotment='$dataStr' where restaurant = '$theRestaurant' and type = 'lunch' and year = '$year' $qryprod limit 1");


        $olddinnerdata = $this->readAlloteRestaurant($theRestaurant, 'dinner', $year);
        if ($olddinnerdata == "")
            return $this->result = -1;
        $olddinnerAr = explode("|", $olddinnerdata);
        $newdinnerAr = explode("|", $newdinnerdata);
        for ($i = 0, $k = $start; $i < $size; $i++, $k++)
            $olddinnerAr[$k] = $newdinnerAr[$i];
        $dataStr = implode("|", $olddinnerAr);
        pdo_exec("Update allotment set allotment='$dataStr' where restaurant = '$theRestaurant' and type = 'dinner' and year = '$year' $qryprod limit 1");

        return $this->result = 1;
    }

    function getAlloteRestaurant($theRestaurant, $year) {

        $data['lunch'] = $this->readAlloteRestaurant($theRestaurant, 'lunch', $year);
        $data['dinner'] = $this->readAlloteRestaurant($theRestaurant, 'dinner', $year);

        $DayOfYear = intval(date('z')); // current day of year
        $cyear = intval(date('Y'));
        if ($this->maxdaybooking > (365 - $DayOfYear) && $year == $cyear ) {
            $data['lunch'] = substr($data['lunch'], 0, strlen($data['lunch']) - 1) . $this->readAlloteRestaurant($theRestaurant, 'lunch', $year + 1);
            $data['dinner'] = substr($data['dinner'], 0, strlen($data['dinner']) - 1) . $this->readAlloteRestaurant($theRestaurant, 'dinner', $year + 1);
        }

        return $data;
    }

    function ComputeOpenToday($ohvalue) {

        $openStr = "Open Today";
        $closeStr = "Close Today";

        // get current day, and current day of the week
        // then check Open Hour, and availability

        $today = date('d-m-Y');
        $cyear = intval(date('Y'));
        $ctime = strtotime($rdate);
        $weekday = intval(date('w', $ctime)); // current day of week
        $yearday = intval(date('z', $ctime)); // current day of year

        $this->getOpenHour($ohvalue, $weekday);
        if ($this->result < 0 || $this->openDay <= 0) {
            $this->result = 1;
            return $closeStr;
        }

        $data = $this->getAlloteRestaurant($this->restaurant, $cyear); // configuration from the backoffice
        if (count($data) < 1)
            return $closeStr;

        $this->result = 1;
        $lunchAr = explode("|", $data['lunch']);
        $dinnerAr = explode("|", $data['dinner']);
        return ($lunchAr[$yearday] > 0 || $dinnerAr[$yearday] > 0) ? $openStr : $closeStr;
    }

    // check if we can approve this reservation by check the allotement for that day that time.
    // 1 check open hour if open
    // 2 check allotement availabele is bigger that the requested party
    // 3 check existing booking to check what's really left
    // 4 check current time

    private function getCutofftime($curslot, $slottime) {

        if ($this->dailycutoff == 1 && !empty($this->cutofflunch))
            return ($this->getCutoffValue($curslot, $this->cutofflunch) > 0) ? 3 : 0;

        if ($curslot < 32 && $slottime < 32 && !empty($this->cutofflunch))
            return ($this->getCutoffValue($curslot, $this->cutofflunch) > 0) ? 1 : 0;

        if ($curslot >= 32 && $slottime >= 32 && !empty($this->cutoffdiner))
            return ($this->getCutoffValue($curslot, $this->cutoffdiner) > 0) ? 2 : 0;

        return 0;
    }

    private function getCutoffValue($curslot, $data) {

        $tt = intval(substr($data, 0, 2));
        $uu = intval(substr($data, 3, 2));
        $cutoffslot = ($tt > 0 && $tt < 32 && $uu >= 0 && $uu < 60) ? ($tt * 2) + floor($uu / 30) : 0;
        return ($cutoffslot > 0 && $cutoffslot <= $curslot) ? 1 : 0;
    }

    function CheckDayAvailability($rtime, $pax) {

		// BurntEnds -> prefech availability info for all date, at a specific time, for specific pax
        $mealduration = $this->mealduration; //3 slots per repas or 5 if previously set
        $info = $this->AlloteComputeOpenHour($pax);
        $slot = $this->getSlotNumber($rtime);
        if ($slot >= 32) {
            $data = $info['dinnerdata'];
            $slot -= 32;
        } else {
            $data = $info['lunchdata'];
            $slot -= 18;
        }

		/* for ($mask = 0, $mm = 1; $mm < $mealduration; $mm++) $mask += (1 << ($slot + $mm)); // not good */

        $mask = (1 << $slot);
        $retval = "";

        //for ($i = $k = 0; $i < 7; $i++, $k += 4) error_log('AVAIL ' . substr($data, $k, 4) . " " . hexdec(substr($data, $k, 4)) . " " . $mask . " " . $rtime);

        // do it for the next 100 day, need 4 char per day to check availability for dinner or lunch
        for ($i = $k = 0; $i < 100; $i++, $k += 4) {
            $retval .= (hexdec(substr($data, $k, 4)) & $mask) ? "1" : "0";
        }
        $valAr['availability'] = $retval;
        $valAr['stardate'] = date('Y-m-d');
        $this->result = 1;
        return $valAr;
    }

    function getAllote1Slot($rdate, $rtime, $pax, $conf) {

        // check the validity of an available slots, for verfication of the 1, 2, 3ieme page of the booking engine
        // rdate formt yyyy-mm-dd
        // rtime format hh:mm

        $ohvalue = $this->ohvalue;

        date_default_timezone_set('Asia/Singapore');

        $ctime = strtotime($rdate);
        $weekday = intval(date('w', $ctime)); // current day of week
        $yearday = intval(date('z', $ctime)); // current day of year
        $cyear = intval(substr($rdate, 0, 4));
        $tmp = $cyear - intval(date('Y'));
        if ($tmp < 0 || $tmp > 1) {
            WY_debug::recordDebug("ERROR-API", "API-BOOKING", "Invalid Year Date" . " \n " . "$rdate, $rtime, $pax, $conf, $cyear");
            $cyear = intval(date('Y'));
        }

        $this->getOpenHour($ohvalue, $weekday);
        if ($this->result < 0 || $this->openDay <= 0) 
            return $this->errormsg("OpenHour or Openday Close", -1); 

        //after 16:00, is dinner
        $timeAr = explode(":", $rtime);
        $whichmeal = (intval($timeAr[0]) >= 16) ? "dinner" : "lunch";
        if ($whichmeal == "dinner" && $this->openDinner <= 0)
            return $this->errormsg("for dinner and dinner close", -1); 
        if ($whichmeal == "lunch" && $this->openLunch <= 0)
            return $this->errormsg("for lunch and lunch close", -1); 

        $mealduration = $this->mealduration; //3 slots per repas or 5 if previously set
        $perpax = $this->perpax;
        $twositting = $this->twositting;
        $cover = ($perpax != 1) ? 1 : $pax;
        $slottime = $this->getSlotNumber($rtime);
        $offset = ($whichmeal == "lunch") ? 0 : 2;

        if ($slottime < $this->openWeekHours[$weekday][$offset] || $slottime > $this->openWeekHours[$weekday][$offset + 1]) {
            return $this->errormsg("Open week hours close", -1); 
        	}

        // use slot time booking hour. The precise slot must be activated
        if ($this->HourBySlotflg && substr($this->openWeekSlots[$weekday], $slottime, 1) == "0") {
            return $this->errormsg("Open Week Hours Slot close", -1); 
        	}

        if ($twositting == 1 && $whichmeal == "dinner" && $this->checkSitting($cyear, $yearday, $slottime) < 0) {
             return $this->errormsg("2sitting not configured", -1); 
        	}

        // check the allotment for that day
        $data = $this->getAlloteRestaurant($this->restaurant, $cyear); // configuration from the backoffice
        if (count($data) < 1)
            return $this->errormsg("Can't get data from backoffice Allotment", -1); 

        $this->result = 0;
        // is request greater that configured availability ?
        $mealAr = ($whichmeal == "lunch") ? explode("|", $data['lunch']) : explode("|", $data['dinner']);
        $availPax = intval($mealAr[$yearday]);
        if ($availPax < $cover) {
            $this->result = -1;
            $this->msg = "avail pax < cover"; 
            return $availPax;
            }

		$curday = intval(date('z'));
		$slice = $curslot = (intval(date('H')) * 2) + floor(intval(date('i')) / 30);
        if ($yearday <= $curday + 2 && $this->laptime == 72) {
		    WY_debug::recordDebug("API", "API-BOOKING", "Retrocession -2" . " \n " . "$rdate, $rtime, $pax, $conf, $slice, $slottime, time= " . date('H:i') . " " . $this->restaurant);
			return $this->errormsg("Too Early 72h", -1); 
         	}
        if ($yearday <= $curday + 1 && $this->laptime == 48) {
		    WY_debug::recordDebug("API", "API-BOOKING", "Retrocession -2" . " \n " . "$rdate, $rtime, $pax, $conf, $slice, $slottime, time= " . date('H:i') . " " . $this->restaurant);
			return $this->errormsg("Too Early 48h", -1); 
         	}
        	
        if ($yearday == $curday) {

            if ($this->laptime == 24) {
                WY_debug::recordDebug("API", "API-BOOKING", "Retrocession -1" . " \n " . "$rdate, $rtime, $pax, $conf, $slice, $slottime, time= " . date('H:i') . " " . $this->restaurant);
            	return $this->errormsg("Too Early 24h", -1); 
            	}
            if ($this->laptime < 0 || $this->laptime > 10)
                $this->laptime = 0;

			$slice += floor($this->laptime * 2);
			$slottime = $this->getSlotNumber($rtime);
			if ($slice > $slottime) {
				WY_debug::recordDebug("API", "API-BOOKING", "Too early" . " \n " . "$rdate, $rtime, $pax, $conf, $slice, $slottime, time= " . date('H:i') . " " . $this->restaurant);
				return $this->errormsg("Too Early", -1); 
				}
 
            if (($cutoff = $this->getCutofftime($curslot, $slottime)) > 0) {
                $tt = ($cutoff == 1) ? $this->cutofflunch : $this->cutoffdiner;
                WY_debug::recordDebug("API", "API-BOOKING", "Cutoff" . " \n " . "$rdate, $rtime, $pax, conf=$conf, $tt, $cutoff");
                error_log("ERROR-API Cutoff" . " \n " . "$rdate, $rtime, $pax, conf=$conf, $tt, $cutoff, " . date('H:i'));
            	return $this->errormsg("Cutoff", -1); 
            }
        }

        //$booking = new WY_Booking();
        $data = WY_Booking::getThatDayBooking($this->restaurant, $rdate, $this->product);
        $block = WY_Block::getThatDateBlock($this->restaurant, $rdate, $this->product);
        $extraweight = $this->extraallote; // this is only for restaurant section such as Burntends

        if (count($data) <= 0 && count($block) <= 0) // no booking, so there are some availability
            return $this->result = 1;

        $book = array();
        $slottime = $this->getSlotNumber($rtime);
        for ($i = 0; $i < $mealduration; $i++) {
            $book[$slottime + $i] = ($availPax - $cover);
        }

        if (count($data) > 0) {
            foreach ($data as $row) {
                if (!empty($conf) && $conf == $row['confirmation'])
                    continue;
                $slottime = $this->getSlotNumber($row['rtime']);
                $cover = ($perpax != 1) ? 1 : (intval($row['cover'])+$extraweight);
                for ($i = 0; $i < $mealduration; $i++) {
                    if (isset($book[$slottime + $i])) {
                        $book[$slottime + $i] -= $cover;
                        if ($book[$slottime + $i] < 0)
            				return $this->errormsg("No avail with existing bookings", -1); 
                    }
                }
            }
        }

        if (count($block) > 0) {
            foreach ($block as $row) {
                $slottime = (intval(substr($row['stime'], 0, 2)) * 2) + (intval(substr($row['stime'], 3, 1)) / 3);
                $nslots = intval($row['slots']);
                for ($i = 0; $i < $nslots; $i++) {
                    if (isset($book[$slottime + $i])) {
                        $book[$slottime + $i] -= intval($row['tablepax']);
                        if ($book[$slottime + $i] < 0)
            				return $this->errormsg("No avail with existing blocks", -1); 
                    }
                }
            }
        }

        return $this->result = 1;
    }

    // 2 periods: lunch and dinner. If needed, we could add a third period for breakfast using same model
    // 4 Phases
    // 1) get open hours information for a week, "STARTING" the current day !important
    // 2) get availability for the restaurant for both lunch and dinner
    // 3) decrement existing availability with the bookings, each booking is a table for 1 and 1/2 hour -> 3 slots
    // 4) update the current day -> no walking => close the period 1 slot before (30mns). Else close previous slot of the day.

    function AlloteComputeOpenHour($pax) {

        // function to send proper availability information to the booking engine
        // date_default_timezone_set('Singapore'); 
        // 1) PHASE: first get open hours information for a week, "STARTING" the current day !important
        // and replicate the week for the next 2 months -> booking was limited to 2 month ahead (70days)
        // Now booking is limited to end of year

        $ohvalue = $this->ohvalue;

        $isnoWalking = $this->nowalking;
        $weekday = date('w'); // current day of the week
        $cyear = intval(date('Y'));
        $this->getOpenHour($ohvalue, $weekday);
        if ($this->result < 0)
            return array();

        $lunch = $dinner = array();

        $nchar_lunch = 4;
        $nchar_dinner = 4;
        $emptylunch = $emptydinner = "";
        for ($i = 0; $i < $nchar_lunch; $i++)
            $emptylunch .= "0"; // "0000"
        for ($i = 0; $i < $nchar_dinner; $i++)
            $emptydinner .= "0";

        $day = $weekday;
        $dataOWH = $this->openWeekHours;
        $dataOWS = $this->openWeekSlots;
        $not_hbs = ($this->HourBySlotflg === false);
        // booking/open HOUR, or booking per slot ?
        for ($k = 0; $k < 7; $k++, $day = ($day < 6) ? $day + 1 : 0) {
        	$lunch[] = $this->getBitsMealAvail($dataOWH[$day][0], $dataOWH[$day][1], $dataOWS[$day], $not_hbs, $nchar_lunch, 18);
        	$dinner[] = $this->getBitsMealAvail($dataOWH[$day][2], $dataOWH[$day][3], $dataOWS[$day], $not_hbs, $nchar_dinner, 32);
        	}

        $yearday = intval(date('z')); // current day of year
        //$maxdaybooking = 365 - $yearday;	//
        //$maxdaybooking = 70;	// 70 days max
        // 1)a) replicate the week for the next 2 months -> booking is limited to 2 month ahead

        for ($i = 7; $i < $this->maxdaybooking; $i++) {
            $lunch[$i] = $lunch[$i % 7];
            $dinner[$i] = $dinner[$i % 7];
        }


        // 2) PHASE: get availability for the restaurant for both lunch and dinner

        $data = $this->getAlloteRestaurant($this->restaurant, $cyear); // configuration from the backoffice
        // 3) PHASE: decrement existing availability with the bookings, each booking is a table for 1 and 1/2 hour -> 3 slots
        // for lunch or dinner depending of the reservation time !!! Attention: no overlapping -> potential problem is 16h

        $lunchAr = explode("|", $data['lunch']);
        $dinnerAr = explode("|", $data['dinner']);

        $mealduration = $this->mealduration; //3 slots per repas or 5 if previously set
        $maxdaybooking = $this->maxdaybooking;
        $book = $this->consoBookingAllote($this->restaurant, ""); // information from the booking
        $extraweight = $this->extraallote; // this is only for restaurant section such as Burntends

        for ($i = 0; $i < $maxdaybooking; $i++) {
            $maxtable = intval($lunchAr[$i + $yearday]) - $pax;
            $offset = 18; //starting of lunch slot: 18/2 = 9h
            if ($maxtable < 0)
                $lunch[$i] = 0;
            else if (isset($book[$i])) {
                $row = $book[$i];
                while (list($slot, $val) = each($row)) {
                	$val += $extraweight;
                    if ($val > $maxtable) {
                    	$diff = $slot - $offset;
                        for ($mm = 0; $mm < $mealduration && $diff >= $mm; $mm++) // close also the preceding slots for $mealduration - 1
                            $lunch[$i] &= ~(1 << ($diff - $mm));
                    }
                }
            }

            $maxtable = intval($dinnerAr[$i + $yearday]) - $pax;
            $offset = 32; //starting of dinner slot  32/2 = 16h
            if ($maxtable < 0)
                $dinner[$i] = 0;
            else if (isset($book[$i])) {
                $row = $book[$i];
                while (list($slot, $val) = each($row)) {
                	$val += $extraweight;
                    if ($val > $maxtable) {
                    	$diff = $slot - $offset;
                        for ($mm = 0; $mm < $mealduration && $diff >= $mm; $mm++) // close also the preceding slots for $mealduration - 1
                            $dinner[$i] &= ~(1 << ($diff - $mm));
                    }
                }
            }
        }

        // 4) PHASE: update today's information based upon the current time 	
        date_default_timezone_set('Asia/Singapore');

        $slice = $curslot = (intval(date('H')) * 2) + floor(intval(date('i')) / 30);
        if ($this->laptime == 24) { 
            $lunch[0] = $emptylunch;
            $dinner[0] = $emptydinner;
            }
        else if ($this->laptime == 48) { 
            $lunch[0] = $lunch[1] = $emptylunch;
            $dinner[0] = $dinner[1] = $emptydinner;
            }
        else if ($this->laptime == 72) { 
            $lunch[0] = $lunch[1] = $lunch[2] = $emptylunch;
            $dinner[0] = $dinner[1] = $dinner[2] = $emptydinner;
            }
        else {
            if ($this->laptime < 0 || $this->laptime > 10)
                $this->laptime = 0;
			$slice += floor($this->laptime * 2);
			if ($slice >= $this->openDayHours[1]) $lunch[0] = $emptylunch;
			if ($slice >= $this->openDayHours[3]) $dinner[0] = $emptydinner;
	   		}

        $cutoff = $this->getCutofftime($curslot, $curslot);
        if ($cutoff == 1) $lunch[0] = $emptylunch;
        else if ($cutoff == 2) $dinner[0] = $emptydinner;
        else if ($cutoff == 3) {
            $lunch[0] = $emptylunch;
            $dinner[0] = $emptydinner;
            }

        for ($slot = $this->openDayHours[0]; $slot <= $this->openDayHours[1] && $slot < $slice && $slice <= $this->openDayHours[1]; $slot++) {
            $lunch[0] &= ~(1 << ($slot - 18));
        }

        for ($slot = $this->openDayHours[2]; $slot < $this->openDayHours[3] && $slot < $slice && $slice < $this->openDayHours[3]; $slot++) {
            $dinner[0] &= ~(1 << ($slot - 32));
        }

        if ($isnoWalking) {
            if ($slice >= $this->openDayHours[0] - 1)
                $lunch[0] = $emptylunch;
            if ($slice >= $this->openDayHours[2] - 1)
                $dinner[0] = $emptydinner;
        }

        if (isset($this->twositting) && $this->twositting == 1) {
            $this->adjustSitting($cyear, $yearday, $maxdaybooking, $dinner);
        }

        $lunchdata = $dinnerdata = "";
        for ($i = 0; $i < $this->maxdaybooking; $i++) {
            $lunchdata .= sprintf("%04x", $lunch[$i]);
            $dinnerdata .= sprintf("%04x", $dinner[$i]);
        }

        $data = array();
        $data['lunchstart'] = 9;
        $data['dinnerstart'] = 16;
        $data['lunchdata'] = strtoupper($lunchdata);
        $data['dinnerdata'] = strtoupper($dinnerdata);

        return $data;
    }

    function getAlloteReportSlot($ohvalue, $rdate) {

        //date_default_timezone_set('Singapore'); 
        // 1) PHASE: first get open hours information for a day,
        // reformat the date format
        // rdate formt yyyy-mm-dd

        $tmpAr = explode("-", $rdate);
        $ctime = strtotime($rdate); // unix
        $todayflag = (date('d-m-Y') == date('d-m-Y', $ctime));
        $weekday = intval(date("w", $ctime)); // current day of the week
        $yearday = intval(date('z', $ctime)); // current day of year of specific date
        $cyear = intval(substr($rdate, 0, 4));
        $tmp = $cyear - intval(date('Y'));
        if ($tmp < 0 || $tmp > 1) {
            WY_debug::recordDebug("ERROR-API", "API-BOOKING", "Invalid Year Date" . " \n " . "$rdate, $cyear");
            $cyear = intval(date('Y'));
        }

        $this->getOpenHour($ohvalue, $weekday);
        if ($this->result < 0)
            return -1;

        $data = $this->getAlloteRestaurant($this->restaurant, $cyear); // configuration from the backoffice
        // set the allotement for the opening hours
        // in one array, 0-15 for lunch, 16 - 32 for dinner

        $lunchAr = explode("|", $data['lunch']);
        $dinnerAr = explode("|", $data['dinner']);

        $maxlunch = intval($lunchAr[$yearday]);
        $maxdinner = intval($dinnerAr[$yearday]);
        for ($i = 0; $i < 32; $i++)
            $repasbits[] = $booked[] = 0;

// lunch, set the bit when the time is between start and end, for the 16 digits. Last possible is 16h30 -> slot 33 (everything is divided by 1/2 hour)

        $flg = ($this->HourBySlotflg == false);
        $startingReference = 18;  // 18 -> 9 * 2, 9h00 for lunch. Starting Reference for the OpenHours
        $Lstart = $this->openDayHours[0] - $startingReference;
        $Lend = $this->openDayHours[1] - $startingReference;
        for ($i = $Lstart; $i < $Lend; $i++)
            if ($flg || substr($this->openWeekSlots[$weekday], $i + $startingReference, 1) == "1") // if booking slot activated
                $repasbits[$i] = $maxlunch;

        $startingReference = 32;  // 32 -> 16 * 2, 16h00 for dinner. Starting Reference for the OpenHours
        $Dstart = $this->openDayHours[2] - $startingReference;
        $Dend = $this->openDayHours[3] - $startingReference;
        for ($i = $Dstart; $i < $Dend; $i++)
            if ($flg || substr($this->openWeekSlots[$weekday], $i + $startingReference, 1) == "1") // if booking slot activated
                $repasbits[$i + 14] = $maxdinner;

        $book = $this->consoBookingAllote($this->restaurant, $rdate); // information from the booking with mysql format date yyyy-mm-dd

        if (count($book) > 0 && isset($book[0])) {
            while (list($slot, $val) = each($book[0])) {
                $bslot = (intval($slot) < 32) ? (intval($slot) - 18) : (intval($slot) - 32) + 14;
                if ($bslot < 0) {
                    continue;
                }
                $repasbits[$bslot] -= $val;
                $booked[$bslot] = $val;
            }
        }

        // 4) PHASE: update today's information based upon the current time 	
        date_default_timezone_set('Asia/Singapore');

        if ($todayflag) {
            for ($slot = (intval(date('H')) * 2) + floor(intval(date('i')) / 30) - 18; $slot >= 0; $slot--)
                $repasbits[$slot] = 0;
        }

        //$this->printdebug($repasbits, $rdate);		
        for ($i = 0; $i < 14; $i++)
            $lunchbits[] = $repasbits[$i];
        for ($i = 14; $i < 32; $i++)
            $dinnerbits[] = $repasbits[$i];

        return array("avail" => implode(",", $repasbits), "booked" => implode(",", $booked), "pax" => strval($this->perpax));
    }

    function printdebug($repasbits, $rdate) {

        for ($i = 0; $i < 32; $i++) {
            $mtype = ($i < 14) ? "LUNCH" : "DINNER";
        }
    }

}

class WY_Block {

    use CleanIng;

    var $restaurant;
    var $product;
    var $blockID;
    var $description;
    var $date;
    var $slots;
    var $result;
    var $msg;

    function __construct($theRestaurant, $product) {
        $this->restaurant = $theRestaurant;
        $this->product = $product;
    }

    private function getUniqueCode() {

        for ($i = 0; $i < 100; $i++) {
            $uniqueID = rand(10000000, 100000000 - 1);
            $data = pdo_single_select("SELECT blockID FROM alloteblock WHERE restaurant ='$this->restaurant' and blockID = '$uniqueID' limit 1");
            if (count($data) <= 0)
                return $uniqueID;
        }
        return false;
    }

    private function retval($val, $msg) {
        $this->result = $val;
        $this->msg = $msg;
        return array($val, $msg);
    }

    static function getDateBlock($theRestaurant, $product) {
        date_default_timezone_set('Asia/Singapore');
        return pdo_multiple_select("SELECT blockID, date, stime, slots, tablepax, DATEDIFF(date, CURDATE()) as ndays FROM alloteblock WHERE restaurant ='$theRestaurant' and date >= CURDATE() and product = '$product'  ORDER by date");
    }

    static function getThatDateBlock($theRestaurant, $aDate, $product) {
        date_default_timezone_set('Asia/Singapore');
        return pdo_multiple_select("SELECT blockID, date, stime, slots, tablepax, 0 as ndays FROM alloteblock WHERE restaurant ='$theRestaurant' and date = '$aDate' and product = '$product'  ORDER by date");
    }

    function getBlocks() {
        $this->result = 1;
        $theRestaurant = $this->restaurant;
        $product = $this->product;
        return pdo_multiple_select("SELECT * FROM alloteblock WHERE restaurant ='$theRestaurant' and product = '$product' ORDER by date");
    }

    function insertBlock($blockID, $date, $stime, $tablepax, $description, $slots) {

        $blockID = preg_replace("/[^a-zA-Z0-9]/", "", $blockID);
        if (strlen($blockID) < 7)
            return $this->retval(-1, "invalid ID " . $blockID);

        $product = $this->product;
        $theRestaurant = $this->restaurant;
        $data = pdo_single_select("SELECT blockID FROM alloteblock WHERE restaurant ='$theRestaurant' and blockID = '$blockID' limit 1");
        if (count($data) > 0) {
            $blockID = $this->getUniqueCode();
            if ($blockID == false)
                return $this->retval(-1, "unable to create a unique ID ");
        }

        $arg = array('blockID', 'date', 'stime', 'tablepax', 'description', 'slots');
        for ($i = 0; $i < count($arg); $i++) {
            $tt = $arg[$i];
            $$tt = $this->clean_input($$tt);
        }

        if ($blockID == "")
            return $this->retval(-1, "Block id is invalid -$blockID-");

        $this->result = pdo_insert("INSERT INTO  alloteblock (restaurant, blockID, date, stime, tablepax, description, slots, product) VALUES ('$theRestaurant', '$blockID', '$date', '$stime', '$tablepax', '$description', '$slots', '$product')");
        return ($this->result >= 0) ? $this->retval(1, "Block $blockID has been created") : $this->retval(-1, "Fail. Block $blockID already exists");
    }

    function updateBlock($blockID, $date, $stime, $tablepax, $description, $slots) {

        $arg = array('blockID', 'date', 'stime', 'tablepax', 'description', 'slots');
        for ($i = 0; $i < count($arg); $i++) {
            $tt = $arg[$i];
            $$tt = $this->clean_input($$tt);
        }

        $theRestaurant = $this->restaurant;
        $blockID = intval(preg_replace("/[^0-9]/", "", $blockID));

        if ($blockID == "")
            return $this->retval(-1, "Undefined Block id");

        $affected_rows = pdo_exec("Update alloteblock set blockID='$blockID', date='$date', stime='$stime', tablepax='$tablepax', description='$description', slots='$slots' where blockID='$blockID' and restaurant = '$theRestaurant' limit 1");
        $this->result = 1;
        return $this->retval(1, "Block $blockID has been updated");
    }

    function delete($blockID) {
        $blockID = $this->clean_input($blockID);
        if ($blockID == "")
            return $this->retval(-1, "Block id is invalid -$blockID-");

        $theRestaurant = $this->restaurant;
        $affected_rows = pdo_exec("DELETE FROM alloteblock WHERE blockID = '$blockID' and restaurant = '$theRestaurant' LIMIT 1");
        return $this->retval(1, "Block $blockID has been deleted");
    }

}

?>
