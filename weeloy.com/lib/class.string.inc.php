<?php
class WY_StringProcessor {
	private static $salutations = ["Dr.", "Mr.", "Mrs.", "Miss"];
	static function SplitFullname($fullname) {
		$names = [];
		if (isset($fullname) && !empty($fullname)) {
			$nameAr = explode(" ", $fullname);
			if(count($nameAr) > 1) {
				if (in_array($nameAr[0], self::$salutations)) {
					$names['salutation'] = $nameAr[0];
					array_splice($nameAr, 0, 1);
				} else
					$names['salutation'] = "";
				$names['firstname'] = $nameAr[0];
				$names['lastname'] = "";
				for($k = 1; $k < count($nameAr); $k++)
					$names['lastname'] .= $nameAr[$k] . " ";
				$names['lastname'] = trim($names['lastname']);
			} else {
				$names['salutation'] = "";
				$names['firstname'] = "";
				$names['lastname'] = $fullname;
			}
		}
		return $names;
	}

	static function FilterValue($value) {
		$value = preg_replace("/\"|\'/", "", $value);
		$value = preg_replace("/\\\\/", "", $value);
		$value = preg_replace("/’/", "`", $value);
		$value = preg_replace("/\s+/", " ", $value);
		$value = preg_replace("/\//", "_", $value);
		return trim($value);
	}
	static function FilterOutNonAlphaNumeric($string) {
		$string = preg_replace("/[^A-Za-z0-9]/", '_', $string);
		$string = preg_replace('/[_]+/', '_', $string);
		return $string;
	}
	static function FilterSpace($string) {
		return preg_replace("/ /","_",$string);
	}
	static function FilterPhoneNumber($number) {
		$number = trim(preg_replace("/[^0-9+ ]/", "", $number));
		if(strlen($number) == 8)
			$number = "+65 " . $number;
		else if(substr($number, 0, 2) == "65")
			$number = "+65 " . substr($number, 2);
		return $number;
	}
}
?>