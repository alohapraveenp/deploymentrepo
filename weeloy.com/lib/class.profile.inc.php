<?php
require_once("lib/class.booking.inc.php");
require_once("lib/class.media.inc.php");
require_once("conf/conf.s3.inc.php");

use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;

class WY_profile {
   use CleanIng;
   
	private $dirname;
    var $systemid;
    var $confirmation;
    var $salutation;
    var $firstname;
    var $lastname;
    var $name;
    var $mobile, $email;
    var $cmobile, $cemail, $cextramobile, $cextraemail;
    var $restaurant;
	var $subID;	
	var $subcontent;	
	var $subprofcode;	// codekey0000
	var $subrepeat;	
    var $company;
    var $createdate;
    var $result;
    var $msg;
    var $s3;
    var $bucket;  
	function __construct() {
		$this->msg = "";
		$this->result = 1;

	}
			
	function insertProf($bkg) {
	
		if($this->getBookInfo($bkg) < 0)
			return $this->result;
		
		if(!empty($this->systemid)) {
			$qry = "";
			// update current information
			if(!empty($this->mobile) && $this->mobile != $this->cmobile && empty($this->cextramobile))
				$qry .= "extramobile = '$this->mobile'";
			if(!empty($this->email) && $this->email != $this->cemail && empty($this->cextraemail))
				$qry .=  ( (!empty($qry)) ? ", " : "" ) . "extraemail = '$this->email'";
			if(!empty($qry))
				pdo_exec("UPDATE profile set $qry  where systemid = '$this->systemid' limit 1");
				
			$this->result = 2;
			// update no valid email or mobile

			if(empty($this->subID)) {
				pdo_exec("INSERT INTO profile_sub (systemid, restaurant, content) VALUES ('$this->systemid', '$this->restaurant', '{ ’allergies’: [], ’likes’: [], ’dislikes’: [] }')");
				$this->result = 3;
				} 
			return $this->result;
			} 

		$this->systemid = $this->getuniqid();
		if($this->systemid == -1) {
			$this->msg = "Unable to get unique id " . $bkg->confirmation;
			return $this->result = -1;
			}
	   
		pdo_exec("delete from profile_sub where systemid = '$this->systemid' and restaurant = '$this->restaurant' ");
		pdo_exec("INSERT INTO profile (systemid, salutation , firstname , lastname , email , mobile , company, createdate) VALUES ('$this->systemid', '$this->salutation', '$this->firstname', '$this->lastname', '$this->email', '$this->mobile', '$this->company', '$this->createdate')");
		pdo_exec("INSERT INTO profile_sub (systemid, restaurant, content) VALUES ('$this->systemid', '$this->restaurant', '{ }')");
		return $this->result = 1;
	}

	function decProfileRepeat($bkg) {
	
		$this->msg = "";
		$this->result = 1;

		if($this->getBookInfo($bkg) < 0)
			return $this->result;
		
		if(!empty($this->systemid) && !empty($this->restaurant) && strlen($this->systemid) > 10) 
			pdo_exec("update profile_sub set repeatguest = repeatguest-1 where restaurant = '$this->restaurant' and systemid = '$this->systemid' and repeatguest > 0 limit 1");
		return $this->result = 1;
		}

	function updateRepeat($repeatguest) {
		if(!empty($this->systemid) && !empty($this->restaurant) && strlen($this->systemid) > 10) 
			pdo_exec("update profile_sub set repeatguest = '$repeatguest' where restaurant = '$this->restaurant' and systemid = '$this->systemid' limit 1");
		return $this->result = 1;
		}
	function setFormat($merchant) {
		pdo_exec("update profile_sub set content = '{’data’:[{’label’:’PX’,’description’:’VIP’}]}' where restaurant = '$merchant'");
	}
	function setformatBacchanalia() {
			$this->setFormat('SG_SG_R_Bacchanalia');
		}
	function setformatMBS() {
			$this->setFormat('SG_SG_R_DbBistroOysterBar');
		}	
	function updateProfile($merchant, $content) {
		pdo_exec("update profile_sub set content = '$content' where restaurant = '$merchant' and systemid = '$this->systemid' limit 1");
	}
	function updateprofilBacchanalia($content) {
			$this->updateProfile('SG_SG_R_Bacchanalia', $content);
		}
	function updateprofilMBS($content) {
			$this->updateProfile('SG_SG_R_DbBistroOysterBar', $content);
		}	
	function search($theRestaurant, $email, $mobile) {
		$this->restaurant = $theRestaurant;
		$this->email = $email;
		$this->mobile = $mobile;

		if(empty($this->restaurant) || (empty($this->email) && empty($this->mobile)) || (strlen($this->email) < 7 && strlen($this->mobile) < 7) )
			return $this->result = -1;

		$qry = "";
		if(!empty($this->email))  
			$qry = "(email = '$this->email' or extraemail = '$this->email')";
		if(!empty($this->mobile)) {
			$qry .=  ( (!empty($qry)) ? " or " : "" ) . "(mobile = '$this->mobile' or extramobile = '$this->mobile')";
			}
		if($qry == "") // this should never happen
			return $this->result = -2;
			
		$data = pdo_single_select("SELECT systemid, lastname, firstname, extramobile, extraemail from profile WHERE $qry limit 1");
		if(count($data) > 0)  {
			$this->systemid  = $data['systemid'];
			$this->firstname  = $data['firstname'];
			$this->lastname  = $data['lastname'];
			$data = pdo_single_select("SELECT systemid from profile_sub WHERE restaurant = '$this->restaurant' and systemid = '$this->systemid' limit 1");
			if(count($data) > 0) 
				return $this->result = 1;
			}
		return $this->result = -3;
		}
		
	private function getBookInfo($bkg) {
		$this->salutation = $bkg->salutation;
		$this->confirmation = $bkg->confirmation;
		$this->restaurant = $bkg->restaurant;
		$this->firstname = $bkg->firstname;
		$this->lastname = $bkg->lastname;
		$this->email = (!preg_match('/@email.com/', $bkg->email)) ? $bkg->email : "";
		$this->mobile = (!preg_match('/99999999/', $bkg->mobile)) ? $bkg->mobile : "";
		$this->company = $bkg->company;
		$this->createdate = $bkg->cdate;

		$this->systemid = '';
		$this->subID = "";		
		$this->subcontent = "";
		$this->subprofcode = "";
		$this->subrepeat = 0;
		
		
		if(empty($this->lastname) || empty($this->firstname) || empty($this->restaurant) || (empty($this->email) && empty($this->mobile)) || (strlen($this->email) < 7 && strlen($this->mobile) < 7) ) {
			$this->msg = "Unable to get unique id " . $bkg->confirmation;
			return $this->result = -2;
			}
		
		if(preg_match("/test/i", $this->firstname) || preg_match("/test/i", $this->lastname) ) {
			$this->msg = "testing booking " . $bkg->confirmation;
			return $this->result = -3;
			}

		$qry = "";
		if(!empty($this->email))  
			$qry = "(email = '$this->email' or extraemail = '$this->email')";
		if(!empty($this->mobile)) {
			$qry .=  ( (!empty($qry)) ? " or " : "" ) . "(mobile = '$this->mobile' or extramobile = '$this->mobile')";
			}
		if($qry == "") // this should never happen
			return $this->result = -4;
		$data = pdo_single_select("SELECT systemid, mobile, email, extramobile, extraemail from profile WHERE $qry limit 1");
		if(count($data) > 0)  {
			$this->systemid  = $data['systemid'];
			$this->cmobile  = (!preg_match('/99999999/', $data['mobile'])) ? $data['mobile'] : "";
			$this->cextramobile  = (!preg_match('/99999999/', $data['extramobile'])) ? $data['extramobile'] : "";
			$this->cemail  = (!preg_match('/@email.com/', $data['email'])) ? $data['email'] : "";
			$this->cextraemail  = (!preg_match('/@email.com/', $data['extraemail'])) ? $data['extraemail'] : "";
			$data = pdo_single_select("SELECT ID, systemid, repeatguest, content from profile_sub WHERE systemid = '$this->systemid' and restaurant = '$this->restaurant' limit 1");
			if(count($data) > 0) {
				$this->subID = $data['ID'];	
				$this->subcontent = $data['content'];	
				$this->subrepeat = $data['repeatguest'];	
				if($this->subcontent != "") {
					$obj = preg_replace("/’/", "\"", $this->subcontent);
					$objAr = json_decode($obj, true);
					$this->subprofcode = (!empty($objAr["codekey0000"])) ? $objAr["codekey0000"] : "";
					}
				}
			}
		return $this->result = 1;
		}
		
	function read1ProfBooking($restaurant, $systemid) { 
		$this->result = 1;
		$restaurant = $this->clean_input($restaurant);    
		$systemid = $this->clean_input($systemid);    

		$data = pdo_single_select("SELECT email, mobile, extraemail, extramobile from profile WHERE systemid = '$systemid' limit 1");	
		if(count($data) < 0) {
			$this->msg = "invalid id " . $systemid;
			return $this->result = -1;
			}
		
		$bkg = new WY_Booking();
		$newdata = $bkg->getProfileBooking($restaurant, $data["email"], $data["mobile"], $data["extraemail"], $data["extramobile"]);
		$this->msg = $bkg->msg;
		$this->result = $bkg->result;
		return $newdata;
		}
			
	function read1Prof($restaurant, $email, $mobile, $account = "") {    
	
		$restaurant = $this->clean_input($restaurant);    
		$email = $this->clean_input($email);    
		$mobile = $this->clean_input($mobile); 
		
		$search = "";
		if($mobile != "" && strlen($mobile) > 0)
			$search .= " (mobile = '$mobile' or extramobile ='$mobile')  ";
			 
        	if ($this->email_validation($email) >= 0)
			$search .= (($search != "") ? "or" : "") . " (email = '$email' or extraemail ='$email') ";

		if($search == "") {
			$this->msg = "Invalid arguements " . $email . " " . $mobile;	
			$this->result = -1;
           		return null;
            		}

		$data = pdo_single_select("SELECT salutation, firstname, lastname, email, mobile, extraemail, extramobile, company, createdate, birth, repeatguest, content, profile.systemid from profile, profile_sub WHERE profile_sub.restaurant = '$restaurant' and profile.systemid = profile_sub.systemid and ( $search ) limit 1");	
		$this->msg = "";
		$this->result = (count($data) > 0) ? 1 : -1; 
		if($this->result > 0) {
			$format = pdo_single_select_index("SELECT content from profile_sub WHERE restaurant = '$restaurant' and systemid = 'PROFILEFORMAT0000' limit 1");	
			$data["lastvisit"] = WY_booking::lastvisit($restaurant, $data["email"], $data["mobile"], $data["extraemail"], $data["extramobile"]);
			$data["firstvisit"] = WY_booking::firstvisit($restaurant, $data["email"], $data["mobile"], $data["extraemail"], $data["extramobile"]);
			$data["codeformat"] = $this->readCodeProf($restaurant);
			$data["profformat"] = (count($format) > 0) ? $format[0] : "";
			$bkg = new WY_Booking();
			$data["bookings"] = $bkg->getProfileBooking($restaurant, $data["email"], $data["mobile"], $data["extraemail"], $data["extramobile"], $account);
			}
		return $data;
		}

	function getProfiles ($restaurant, $start, $end) {
		$restaurant = $this->clean_input($restaurant);
		$start = $this->clean_input($start);
		$end = $this->clean_input($end);
		try {
			$sql = "select systemid from booking, profile where restaurant = '$restaurant'  and rdate >= '$start' and rdate <= '$end' and booking.email != 'no@email.com' and profile.email = booking.email union select distinct systemid from profile, booking where restaurant = '$restaurant'  and rdate >= '$start' and rdate <= '$end'  and profile.mobile = booking.mobile  and  booking.mobile != '+65 99999999'";
			$data = pdo_multiple_select($sql);
			return $data; 
		} catch (Exception $e) {
			error_log(__FILE__." ".__FUNCTION__." ".__LINE__." Exception: ".$e);
		}
	}
	
	function readShortProf($restaurant, $email, $mobile) {    
	
		$this->result = 1;
		$restaurant = $this->clean_input($restaurant);    
		$email = $this->clean_input($email);    
		$mobile = $this->clean_input($mobile); 
		
		$search = "";
		if($mobile != "" && strlen($mobile) > 0)
			$search .= " (mobile = '$mobile' or extramobile ='$mobile')  ";
			 
        	if ($this->email_validation($email) >= 0)
			$search .= (($search != "") ? "or" : "") . " (email = '$email' or extraemail ='$email') ";

		if($search == "") {
			$this->msg = "Invalid arguements " . $email . " " . $mobile;	
			$this->result = -1;
           	return null;
            }

		$data = pdo_single_select("SELECT email, mobile, extraemail, extramobile, birth, repeatguest, content, profile.systemid from profile, profile_sub WHERE profile_sub.restaurant = '$restaurant' and profile.systemid = profile_sub.systemid and ( $search ) limit 1");	
		$this->msg = "";
		$this->result = (count($data) > 0) ? 1 : -1; 
		if($this->result > 0) {
			$data["lastvisit"] = WY_booking::lastvisit($restaurant, $data["email"], $data["mobile"], $data["extraemail"], $data["extramobile"]);
			$data["firstvisit"] = WY_booking::firstvisit($restaurant, $data["email"], $data["mobile"], $data["extraemail"], $data["extramobile"]);
			}
		return $data;
		}

	
	function readCodeBooking($restaurant) {        
	
		$this->msg = "";
		$this->result = 1;
		$restaurant = $this->clean_input($restaurant);    
		$data = pdo_single_select("SELECT content from profile_sub WHERE systemid = 'BOOKINGCODE000000' and restaurant = '$restaurant' limit 1");
		if(empty($data["content"])) 
			$data["content"] = "";
		return $data["content"];
		}
	
	function updateCodeBooking($restaurant, $content) {        
		$restaurant = $this->clean_input($restaurant); 
		$content = preg_replace("/\'|\"/", "’", $content); 
		$content = preg_replace("/,’[^h:]+hashKey’:’object:\d+’/", "", $content);
        	pdo_exec("INSERT INTO profile_sub (restaurant, systemid, content) VALUES ('$restaurant', 'BOOKINGCODE000000', '$content') ON DUPLICATE KEY UPDATE content='$content';");
		$this->msg = "";
		return $this->result = 1;
		}

	function updateMasterprofile($restaurant, $obj) {
		$restaurant = $this->clean_input($restaurant);
		if($restaurant == "") {
			$this->msg = "invalid empty restaurant";
			return $this->result = -1;
			}
		
        	$arg = array('systemid', 'salutation', 'firstname', 'lastname', 'email', 'mobile');
        	for ($i = 0; $i < count($arg); $i++) {
            		$tt = $arg[$i];
            		$$tt = (isset($obj[$tt])) ? $this->clean_input($obj[$tt]) : "";
        		}
        		
		$data = pdo_single_select("SELECT systemid from profile_sub WHERE systemid = '$systemid' and restaurant = '$restaurant' limit 1");	
		if(count($data) < 0) {
			$this->msg = "invalid id - " . $systemid;
			return $this->result = -1;
			}
		$data = pdo_single_select("SELECT systemid from profile WHERE systemid = '$systemid' limit 1");	
		if(count($data) < 0) {
			$this->msg = "invalid id " . $systemid;
			return $this->result = -1;
			}

  		pdo_exec("update profile set salutation = '$salutation', firstname = '$firstname', lastname = '$lastname', email = '$email', mobile = '$mobile' where systemid = '$systemid' limit 1");		
        	$this->msg = "";
		return $this->result = 1;
		}

	
	function readCodeProf($restaurant) {        
	
		$this->msg = "";
		$this->result = 1;
		$restaurant = $this->clean_input($restaurant);    
		$data = pdo_single_select("SELECT content from profile_sub WHERE systemid = 'PROFILECODE000000' and restaurant = '$restaurant' limit 1");
		if(empty($data["content"])) 
			$data["content"] = "";
		return $data["content"];
		}
	
	function updateCodeProf($restaurant, $content) {        
		$restaurant = $this->clean_input($restaurant); 
		$content = preg_replace("/\'|\"/", "’", $content); 
		$content = preg_replace("/,’[^h:]+hashKey’:’object:\d+’/", "", $content);
        	pdo_exec("INSERT INTO profile_sub (restaurant, systemid, content) VALUES ('$restaurant', 'PROFILECODE000000', '$content') ON DUPLICATE KEY UPDATE content='$content';");
		$this->msg = "";
		return $this->result = 1;
		}
	
	function readProf($restaurant) {        
	
		$restaurant = $this->clean_input($restaurant);    
		$data = pdo_multiple_select("SELECT profile.systemid as systemid, salutation , firstname , lastname , email , mobile , company, createdate, birth, repeatguest, picture from profile, profile_sub WHERE profile_sub.restaurant = '$restaurant' and profile.systemid = profile_sub.systemid");	
		$this->msg = "";
		$this->result = 1;
		return $data;
		}
	
	function readSubProf($restaurant) {        
	
		$restaurant = $this->clean_input($restaurant);    
		$data = pdo_multiple_select("SELECT systemid, content from profile_sub WHERE restaurant = '$restaurant'");	
		$this->msg = "";
		$this->result = 1;
		return $data;
		}

	function deleteSubProf($restaurant, $systemid) {
	
		$restaurant = $this->clean_input($restaurant);    
		pdo_exec("delete from profile_sub where restaurant = '$restaurant' and sytemid = '$systemid' limit 1");
		$this->msg = "";
		return $this->result = 1;
		}

	function updateFormatdSubProf($restaurant, $oformat, $nformat) {
		// $nformat has always the same or greater size than oformat
		$limit = count($oformat);
		$content = $sep = "";
		for($i = 0; $i < $limit; $i++, $sep = ", ") {
			$old = $oformat[$i];
			$new = $nformat[$i];
			$content .=  $sep . "’" . $nformat[$i] . "’: ’’";
			if($old != $new)
				pdo_exec("update profile_sub set content = REPLACE(content, '’" . $old . "’:', '’" . $new . "’:') where  restaurant = '$restaurant'");
			}
		$newstring = "";
		for($i = $limit; $i < count($nformat); $i++, $sep = ", ") {
			$newstring .=  ", ’" . $nformat[$i] . "’: ’’";
			$content .=  $sep . "’" . $nformat[$i] . "’: ’’";
			}
		$newstring .= " }";
		$content = "{ " . $content . " }";
		pdo_exec("update profile_sub set content = REPLACE(content, ' }', '$newstring' ) where  restaurant = '$restaurant'");
		pdo_exec("update profile_sub set content = REPLACE(content, '{, ', '{ ' ) where  restaurant = '$restaurant'");
		
		$data = pdo_multiple_select("SELECT systemid from profile_sub WHERE restaurant = '$restaurant' and systemid = 'PROFILEFORMAT0000' limit 1");
		if(count($data) < 1)
	        pdo_exec("INSERT INTO profile_sub (restaurant, systemid, content) VALUES ('$restaurant', 'PROFILEFORMAT0000', '$content')");
	    else pdo_exec("update profile_sub set content = '$content' where  restaurant = '$restaurant' and systemid = 'PROFILEFORMAT0000' limit 1");
		return $this->result = 1;
		}
	function updateSubProf($restaurant, $systemid, $subprof) {
		pdo_exec("update profile_sub set content = '$subprof' where restaurant = '$restaurant' and systemid = '$systemid' limit 1");
		return $this->result = 1;
		}
    function getS3() {
        if (!isset($this->s3)) {
            $this->bucket = awsBucket;
            $this->s3 = new S3(awsAccessKey, awsSecretKey);
            //$this->s3->putBucket($this->bucket, S3::ACL_PUBLIC_READ);
        }
        return $this->s3;
    }
	function updateProfilePicture($profile, $url) {
		if (AWS && isset($profile["lastname"]) && !empty($profile["lastname"]) && isset($url) && !empty($url))
			try {
				//$media = new WY_Media();
				// Instantiate the client.
				$sql = "SELECT systemid FROM profile WHERE lastname = :lastname";
				$params = array('lastname' => $profile['lastname']);
				if (isset($profile['firstname']) && !empty($profile['firstname'])) {
					$sql .= " AND firstname = :firstname";
					$params['firstname'] = $profile['firstname'];
				}
				if (isset($profile['email']) && !empty($profile['email'])) {
					$sql .= "  AND email = :email";
					$params['email'] = $profile['email'];
				}
				if (isset($profile['mobile']) && !empty($profile['mobile'])) {
					$sql .=  " AND mobile = :mobile";
					$params['mobile'] = $profile['mobile'];
				}
				$sql .=  " LIMIT 1";
				$result = pdo_single_select_with_params($sql, $params);
				if (count($result) > 0) {
					error_log("Updating table profile with picture for ".print_r($profile)." to ".$url);
					$sql = "UPDATE profile SET picture = :url WHERE systemid = :ID";
		            $parameters = array('url' => $url, 'ID' => $result["systemid"]);
		            pdo_update($sql, $parameters);
		            return true;
		    	} else
		    		error_log("Profile not found: ".print_r($profile));
		    } catch (S3Exception $s3e) {
			    echo '{"error":{"text":'. $s3e->getMessage() .'}}';
			} catch(Exception $e) {
	    	    echo '{"error":{"text":'. $e->getMessage() .'}}';
	    	}
    	return false;
    }

	function import($restaurant, &$profile) {
		if ((!isset($profile['firstname']) && !isset($profile['lastname'])) || (empty($profile['firstname']) && empty($profile['lastname'])))  {
			echo "<br/>Skip profile without valid name!<br/>";
			return;
		}
		$content = $profile['content'];
		$email = $this->email = $profile['email'];
		$mobile = $this->mobile = $profile['mobile'];
		$internal = $profile['internal'];
		$salutation = $profile['salutation'];
		$lastname = $profile['lastname'];
		$firstname = $profile['firstname'];
		$company = $profile['company'];
		$format = $profile['PROFILEFORMAT0000'];

		$mobQy = ($mobile != "+65 99999999" && !empty($mobile)) ? "or mobile = '$mobile'" : "";
		$data = pdo_single_select("SELECT systemid from profile WHERE internal = '$internal' and ((lastname = '$lastname' and firstname = '$firstname') $mobQy) limit 1");	
		if(count($data) < 1) {
			$systemid = $this->getuniqid();
			$sql = "INSERT INTO profile (systemid, salutation, firstname, lastname, mobile, email, internal) VALUES ('$systemid', '$salutation', '$firstname', '$lastname', '$mobile', '$email', '$internal')";
			echo "<br />inserting: ".$sql."<br/>";
			pdo_exec($sql);
			pdo_exec("INSERT INTO profile_sub (restaurant, systemid, content) VALUES ('$restaurant', '$systemid', '$content')");
		} else {
			$systemid = $data['systemid'];
			echo "<br />updating $systemid <br />";
			pdo_exec("update profile_sub set content = '$content' where restaurant = '$restaurant' and systemid = '$systemid' limit 1");
			pdo_exec("update profile set salutation = '$salutation', lastname = '$lastname', firstname = '$firstname', mobile = '$mobile', email = '$email' where systemid = '$systemid' and internal = '$internal' limit 1");
			}
		$data = pdo_single_select("SELECT systemid from profile_sub WHERE restaurant = '$restaurant' and systemid = 'PROFILEFORMAT0000' limit 1");	
		if(count($data) < 1) pdo_exec("INSERT INTO profile_sub (restaurant, systemid, content) VALUES ('$restaurant', 'PROFILEFORMAT0000', '$format')");
		else pdo_exec("update profile_sub set content = '$format' where restaurant = '$restaurant' and systemid = 'PROFILEFORMAT0000' limit 1");
		}
		
	function getuniqid() {
		$piece = preg_replace("/[^0-9a-zA-Z]/", "", substr($this->email, 0, 4) . substr($this->mobile, 0, 8));
		for($i = 0; $i < 20; $i++) {
			$systemid = $piece . rand(1000000000, 9999999999);				
			$data = pdo_single_select("SELECT systemid from profile WHERE systemid = '$systemid' limit 1");
			if(count($data) < 1) 
				return $systemid;
			}
		return -1;
		}
}

?>
