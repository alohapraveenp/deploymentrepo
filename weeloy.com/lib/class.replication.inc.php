<?php

require_once("lib/wpdo.inc.php");
require_once("lib/class.debug.inc.php");
require_once("lib/class.restaurant.inc.php");
require_once("lib/class.media.inc.php");
require_once("lib/class.booking.inc.php");
require_once("conf/conf.init.inc.php");
require_once("lib/class.translation.inc.php");
require_once('lib/class.mail.inc.php');
require_once('lib/class.sms.inc.php');
require_once('lib/class.pushnotif.inc.php');
require_once("lib/class.spool.inc.php");
require_once("lib/class.payment.inc.php");
require_once("lib/class.login.inc.php");

class WY_Replication {

    public function __construct() {
        
    }

    public static function replicate($uniqueId, $replication_type) {

        $spool = new WY_Spool;
        $replication_data = "";
        $result = false;

        switch ($replication_type) {

            case "new_booking": 

                $booking_data = new WY_Booking();
                $result = $booking_data->getBooking($uniqueId);
                if ($result == true) {
                    $replication_data = $booking_data; 
                    $result = ($booking_data->restaurant == 'SG_SG_R_TheFunKitchen');
                }    
    
                break;
    
            case "upd_booking":
                
                $booking_data = new WY_Booking();
                $result = $booking_data->getBooking($uniqueId);
                
                if ($result == true) {
                    $replication_data = $booking_data; 
                    $result = ($booking_data->restaurant == 'SG_SG_R_TheFunKitchen');
                }  
                
                break;

            case "ins_bkdeposit":
                            
                $bkdeposit_data = new WY_Payment();
                $data = $bkdeposit_data->geDepositById($uniqueId, 'booking_id');

                $data['restaurant'] = $data['receiver'];
                $data['confirmation'] = $data['object_id'];
                $data['email'] = json_decode($data['data'])->email;

                $replication_data = (object) $data;
                $result = ($data['restaurant'] == 'SG_SG_R_TheFunKitchen');                
                break;

            case "upd_bkdeposit":

                $bkdeposit_data = new WY_Payment();
                $data = $bkdeposit_data->geDepositById($uniqueId, 'booking_id');

                $data['restaurant'] = $data['receiver'];
                $data['confirmation'] = $data['object_id'];
                $data['email'] = json_decode($data['data'])->email;

                $replication_data = (object) $data;
                $result = ($data['restaurant'] == 'SG_SG_R_TheFunKitchen');                
                break;

            case "ins_payment":
                
                $payment_data = new WY_Payment();
                $data = $payment_data->getPaymentDetails($uniqueId, 'payment_id');

                $data['restaurant'] = $data['receiver'];
                $data['confirmation'] = $data['payment_id'];
                $data['email'] = 'amin.mahetar@weeloy.com';

                $replication_data = (object) $data;
                $result = ($data['restaurant'] == 'SG_SG_R_TheFunKitchen');                                
                break;

            case "upd_payment":
                            
                $payment_data = new WY_Payment();
                $data = $payment_data->getPaymentDetails($uniqueId, 'payment_id');

                $data['restaurant'] = $data['receiver'];
                $data['confirmation'] = $data['payment_id'];
                $data['email'] = 'amin.mahetar@weeloy.com';

                $replication_data = (object) $data;
                $result = ($data['restaurant'] == 'SG_SG_R_TheFunKitchen');                
                break;
                
            case "new_login":
            case "upd_login":
            case "del_login":

             	// for all call need to login->resyncduration() before applying proper action on remote region
				$email = $uniqueId[0];
				$field = $uniqueId[1];
				$value = $uniqueId[2];
				
                $data = WY_Login::getDetailsLog1Hour($email, $field, $value);
                $data['restaurant'] = $data['confirmation'] = '';                
                $replication_data = (object) $data;
                $result = (preg_match("/weeloy.com/", $email) && isset($data['email']));                
                break;
                            
            default:
				WY_debug::recordDebug("ERROR", "REPLICATION", "Invalid CMD =  " . $replication_type);
				$result = false;
            	break;
            	                
        }

        if ($result == true) {
            $spool->register_replication($replication_data, $replication_type);
        }    
    }

    public function execute_replication($statusId) {
        
        $db_history = getConnection('dwh',NULL,'write');
        
        if ($statusId != "") {
            $data = pdo_single_select("select ID, type, data, timestamp, restaurant, recipient, booking_id from spool where type = 'REPLICATION' and status = '$statusId'  limit 1");

            if (count($data) > 0) {

                pdo_exec("INSERT INTO spool_history (type, timestamp, status, restaurant, recipient, booking_id,  data, sent_on) VALUES ('$data[type]', '$data[timestamp]', '$statusId', '$data[restaurant]', '$data[recipient]', '$data[booking_id]', '$data[data]', NOW())",'dwh');

                pdo_exec("delete from spool where type = 'REPLICATION' and status = '$statusId' limit 1");

                $data_replicate = unserialize(urldecode($data['data']));

                switch ($data['recipient']) {

                    case "new_booking":
                        $this->replicateBooking($data_replicate, "INSERT_BOOKING");
                        break;

                    case "upd_booking":
                        $this->replicateBooking($data_replicate, "UPDATE_BOOKING");
                        break;    

                    case "ins_bkdeposit":
                        $this->replicateBooking($data_replicate, "INS_BKDEPOST");
                        break;     

                    case "upd_bkdeposit":
                        $this->replicateBooking($data_replicate, "UPD_BKDEPOST");
                        break;    

                    case "ins_payment":
                        $this->replicateBooking($data_replicate, "INS_PAYMENT");
                        break;      

                    case "upd_payment":
                        $this->replicateBooking($data_replicate, "UPD_PAYMENT");
                        break;     
                
					case "new_login":
					case "upd_login":
					case "del_login":
						// to be done
						break;
                }    
            } 
        }
         
    }

    function execute_replication_backlog() {
        $db_history = getConnection('dwh',NULL,'write');
        // search for replication requests that were not sent !!!
        $data = pdo_multiple_select("select ID, type, data, status, restaurant, recipient, booking_id, timestamp, try from spool where type = 'REPLICATION' and DATE_SUB(CURRENT_TIMESTAMP(), INTERVAL 5 MINUTE) > timestamp ORDER BY `try` ASC");

        if (count($data) > 0) {
            foreach ($data as $row) {
                pdo_exec("INSERT INTO spool_history (type, timestamp, status, restaurant, recipient, booking_id,  data, sent_on) VALUES ('$row[type]', '$row[timestamp]', '$row[status]', '$row[restaurant]', '$row[recipient]', '$row[booking_id]', '$row[data]', NOW())",'dwh');

                pdo_exec("delete from spool where type = 'REPLICATION' and status = '$row[status]' limit 1");

                $data_replicate = unserialize(urldecode($row['data']));

                switch ($row['recipient']) {

                    case "new_booking":
                        $this->replicateBooking($data_replicate, "INSERT_BOOKING");
                        break;

                    case "upd_booking":
                        $this->replicateBooking($data_replicate, "UPDATE_BOOKING");
                        break;    

                    case "ins_bkdeposit":
                        $this->replicateBooking($data_replicate, "INS_BKDEPOST");
                        break;     

                    case "upd_bkdeposit":
                        $this->replicateBooking($data_replicate, "UPD_BKDEPOST");
                        break;    

                    case "ins_payment":
                        $this->replicateBooking($data_replicate, "INS_PAYMENT");
                        break;      

                    case "upd_payment":
                        $this->replicateBooking($data_replicate, "UPD_PAYMENT");
                        break;     
                
                }   
            }
        }
    }    

    function replicateBooking ($booking_data, $cmd) {
        $params = 
          ["credential" => "6LdmpA4CFDGKCqaA_8MlE9KA2wY_smquvqOw_Tj",
          "confirmation" => $booking_data->confirmation,
          "restaurant" => $booking_data->restaurant,
          "email" => $booking_data->email,
          "cmd" => $cmd,
          "data" => json_decode(json_encode($booking_data), true)
        ];

        $decodeParam = ["paramdata" => base64_encode(json_encode($params))];

        $apiUrl = 'https://ho9mc3ttb2.execute-api.ap-southeast-2.amazonaws.com/Booking';
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $apiUrl );
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        // curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, TRUE);
        curl_setopt($ch, CURLOPT_POST, true );
        if ($params != '') {
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($decodeParam));
        }

        $response_data = curl_exec($ch);
        curl_close( $ch );

        $response_data = json_decode($response_data, TRUE);

        // return $response_data;

    }

}
