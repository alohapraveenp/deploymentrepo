<?php
//require_once('import_mbs_member.php');
/**
 * upload.php
 *
 * Copyright 2013, Moxiecode Systems AB
 * Released under GPL License.
 *
 * License: http://www.plupload.com/license
 * Contributing: http://www.plupload.com/contributing
 */

#!! IMPORTANT: 
#!! this file is just an example, it doesn't incorporate any security checks and 
#!! is not recommended to be used in production environment as it is. Be sure to 
#!! revise it and customize to your needs.

// Make sure file is not cached (as it happens for example on iOS devices)
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
class WY_Upload {
	/* 
	// Support CORS
	header("Access-Control-Allow-Origin: *");
	// other CORS headers if any...
	if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
		exit; // finish preflight CORS requests here
	}
	*/
	// 5 minutes execution time
	//@set_time_limit(5 * 60);

	// Uncomment this one to fake upload time
	// usleep(5000);

	// Settings
	private $targetDir;
	private $cleanupTargetDir; // Remove old files
	private $maxFileAge; // Temp file age in seconds
	public function __construct($targetFolder, $cleanup = true, $age = 5 * 3600)
	{
		$this->targetDir = isset($targetFolder) && !empty($targetFolder) ? $targetFolder : sys_get_temp_dir();
		$this->cleanupTargetDir = $cleanup;
		$this->maxFileAge = $age;
	}
	public function Upload() {
		$starting = microtime(true);
		try {
		// Create target dir
		if (!file_exists($this->targetDir))
			@mkdir($this->targetDir);

		// Get a file name
		if (isset($_REQUEST["name"]))
			$fileName = $_REQUEST["name"];
		elseif (!empty($_FILES))
			$fileName = $_FILES["fileToUpload"]["name"];
		else
			$fileName = uniqid("file_");

		$filePath = $this->targetDir . DIRECTORY_SEPARATOR . $fileName;

		// Chunking might be enabled
		$chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
		$chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;

		// Remove old temp files	
		if ($this->cleanupTargetDir) {
			if (!is_dir($this->targetDir) || !$dir = opendir($this->targetDir)) {
				//die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Failed to open temp directory."}, "id" : "id"}');
				error_log("Failed to open temp directory!");
				return null;
			}

			while (($file = readdir($dir)) !== false) {
				$tmpfilePath = $this->targetDir . DIRECTORY_SEPARATOR . $file;

				// If temp file is current file proceed to the next
				if ($tmpfilePath == "{$filePath}.part") 
					continue;

				// Remove temp file if it is older than the max age and is not the current file
				if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $this->maxFileAge))
					@unlink($tmpfilePath);
			}
			closedir($dir);
		}	

		// Open temp file
		if (!$out = @fopen("{$filePath}.part", $chunks ? "ab" : "wb")) {
			//die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
			error_log("Failed to open output stream.");
			return null;
		}

		if (isset($_FILES) && !empty($_FILES)) {
			if ($_FILES['fileToUpload']['error'] === UPLOAD_ERR_OK) { 
				//echo "Upload successful! <br/>";
				error_log("Upload successful! <br/>");
			} else { 
				//echo "Error: ".$_FILES["fileToUpload"]["error"]."<br/>";
				error_log("Error: ".$_FILES["fileToUpload"]["error"]);
				if ($_FILES['file']['error'] >= 3)
					//die();
					return null;
			} 
			if (!is_uploaded_file($_FILES["fileToUpload"]["tmp_name"])) {
				//die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Failed to move uploaded file."}, "id" : "id"}');
				//echo "Failed to move uploaded file: ".$_FILES["fileToUpload"]["tmp_name"]."<br/>";
				error_log("Failed to move uploaded file: ".$_FILES["fileToUpload"]["tmp_name"]);
				//die();
				return null;
			}
			// Read binary input stream and append it to temp file
			if (!$in = @fopen($_FILES["fileToUpload"]["tmp_name"], "rb")) {
				//die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
				error_log("Failed to open input stream.");
				return null;
			}
		} else if (!$in = @fopen("php://input", "rb")) {
			//die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
			error_log("Failed to open input stream.");
			return null;
		}
		while ($buff = fread($in, 4096))
			fwrite($out, $buff);

		@fclose($out);
		@fclose($in);

		// Check if file has been uploaded
		if (!$chunks || $chunk == $chunks - 1)
			// Strip the temp .part suffix off 
			rename("{$filePath}.part", $filePath);

		error_log("File uploaded successfully to ".$filePath);
		return $filePath;
	} catch (Exception $e) {
		error_log(__FILE__. " ".__FUNCTION__. " ".__LINE__. " Exception: ".$e->getMessage());
	}
	error_log(__FUNCTION__." DONE time = %.2f".(microtime(true) - $starting));
	return null;
	}
}
?>