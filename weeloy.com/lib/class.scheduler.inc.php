<?php

/**
 * 	Richard Kefs (c) 2014
 *
 * 	Contains all classes definition for login session.
 *
 */
require_once("conf/conf.init.inc.php");
require_once("lib/wpdo.inc.php");
require_once("lib/class.async.inc.php");
require_once 'conf/conf.s3.inc.php';
require_once 'conf/conf.sqs.inc.php';
require_once("lib/class.restaurant.inc.php");

class WY_Scheduler {

    private $task = NULL;
    private $last_exec = NULL;
    private $count_exec = 0;
    private $frequency = 0;
    private $frequency_type = NULL;

    function __construct($task) {
        $this->task = $task;
    }

    public function run() {

        if ($this->getTaskDetails() && $this->checkRunValidity()) {

            $execution = $this->updateTaskExecution();
                
            switch ($this->task) {
                case 'reminder_booking':
                    include 'modules/scheduler/tasks/notification_booking_reminder.php';
                    break;
                case 'reminder_review':
                    include 'modules/scheduler/tasks/notification_review_reminder.php';
                    break;
                case 'crawler_email':
                    include 'modules/scheduler/tasks/crawler_email.php';
                    break;
                case 'booking_expiry':
                    include 'modules/scheduler/tasks/update_pending_booking_status.php';
                    break;
                case 'save_pos_order':
                    include 'modules/scheduler/tasks/save_pos_order.php';
                    break;
                case 'retrieve_pos_order':
                    include 'modules/scheduler/tasks/retrieve_pos_order.php';
                    break;
                default:
                    echo true;
                    break;
            }
            if ($execution) {
                echo 'task executed';
                return true;
            }
        } else {
            echo 'task not found or not valid';
        }
    }

    private function getTaskDetails() {
        $sql = "SELECT last_exec, count_exec, frequency, frequency_type FROM phpjobscheduler_stat WHERE task = :task ";
        $task = pdo_single_select_with_params($sql, array('task' => $this->task));

        if (count($task) > 0) {
            $this->last_exec = $task['last_exec'];
            $this->count_exec = $task['count_exec'];
            $this->frequency = $task['frequency'];
            $this->frequency_type = $task['frequency_type'];
            return true;
        }
        return false;
    }

    private function updateTaskExecution() {

        $script_tz = date_default_timezone_get();
        date_default_timezone_set('UTC');
        $date = date('Y-m-d H:i:s');
        $sql = "UPDATE phpjobscheduler_stat SET last_exec = '$date', count_exec = count_exec + 1 WHERE task = :task ";

        $result = pdo_exec_with_params($sql, array('task' => $this->task));
        date_default_timezone_set($script_tz);
        return $result;
    }

    private function checkRunValidity() {

        $script_tz = date_default_timezone_get();
        date_default_timezone_set('UTC');

        $string_frequency = '+' . $this->frequency . $this->frequency_type;
        $now = strtotime($this->last_exec);
        $next_exec = strtotime($string_frequency, $now);
        $isValid = ($next_exec <= strtotime('+5 sec'));

        date_default_timezone_set($script_tz);
        return $isValid;
    }

}
