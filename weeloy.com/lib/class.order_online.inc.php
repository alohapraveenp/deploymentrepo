<?php

class WY_OrderOnline {

    use CleanIng;

    private $restaurant;
    private $orderid;
    private $type;
    private $cdate;
    private $delivery_date;
    private $rtime;
    private $total;
    private $price;
    private $currency;
    private $tax;
    private $access; // pickup or delivery
    private $email;
    private $mobile;
    private $address;
    private $zip;
    private $country;
    private $status;
    private $idescription;
    private $unit_price;
    private $icurrency;
    var $msg;
    var $result;

    public function __construct($theRestaurant = NULL) {
        $this->restaurant = $theRestaurant;
    }

    private function error_msg($result, $msg) {

        $this->msg = $msg;
        return $this->result = $result;
    }

    private function getUniqueCode() {

        //        Why link to restaurant? 
        for ($i = 0; $i < 100; $i++) {
            $uniqueID = rand(10000000, 100000000 - 1);
            $data = pdo_single_select("SELECT orderID FROM online_order WHERE restaurant ='$this->restaurant' and orderID = '$uniqueID' limit 1");
            if (count($data) <= 0)
                return $uniqueID;
        }
        return false;
    }

    public function getOrders() {
        $cmds["restaurant"] = $this->restaurant;
        $cmds["orders"] = pdo_multiple_select("SELECT * FROM online_order WHERE restaurant ='$this->restaurant'");
        $cmds["orders_items"] = pdo_multiple_select("SELECT * FROM online_order_item WHERE restaurant ='$this->restaurant'");

        $this->msg = "";
        $this->result = 1;

        return $cmds;
    }

    public function createOrder($cmds) {

        $orders = $cmds["orders"];
        $orders_items = $cmds["orders_items"];

        
        $orderID = preg_replace("/[^a-zA-Z0-9]/", "", $orders["orderID"]);
        if (strlen($orderID) < 7)
            return $this->error_msg(-1, "invalid ID " . $orderID);

        $data = pdo_single_select("SELECT orderID FROM online_order WHERE restaurant ='$this->restaurant' and orderID = '$orderID' limit 1");
        if (count($data) > 0) {
            $orderID = $this->getUniqueCode();
            if ($orderID == false)
                return $this->error_msg(-1, "unable to create a unique ID ");
        }

        $content = array("status", "itemindex", "delivery_date", "total", "tax", "currency", "delivery_mode", "status", "title", "firstname", "lastname", "email", "mobile", "address", "zip");
        for ($i = 0; $i < count($content); $i++) {
            $tt = $content[$i];
            $$tt = $this->clean_input($orders[$tt]);
        }

        if ($status != "new") {
            $this->msg = "Invalid status";
            return $this->result = 1;
        }

        $status = "";
        $sql = "INSERT INTO online_order (restaurant,orderID,itemindex,cdate,delivery_date,total,tax,currency,delivery_mode,status,title,firstname,lastname,email,mobile,address,zip) VALUES ("
                . "'$this->restaurant','$orderID','$itemindex',NOW(),'$delivery_date','$total','$tax','$currency','$delivery_mode','','$title','$firstname','$lastname','$email','$mobile','$address','$zip')";

        pdo_exec($sql);
        
        $content = array("itemID", "status", "unit_price", "quantity", "item_title");
        foreach ($orders_items as $row) {
            for ($i = 0; $i < count($content); $i++) {
                $tt = $content[$i];
                $$tt = $this->clean_input($row[$tt]);
            }
            if ($status == "delete")
                continue;
            pdo_exec("INSERT INTO online_order_item (restaurant,orderID,itemID,status,unit_price,quantity,item_title) VALUES ('$this->restaurant','$orderID','$itemID','','$unit_price','$quantity','$item_title')");
        }

        $this->msg = $orderID;
        return $this->result = 1;
    }

    public function updateOrders($cmds) {

        $this->msg = "";
        $orders = $cmds["orders"];
        $orders_items = $cmds["orders_items"];

        $orderID = preg_replace("/[^a-zA-Z0-9]/", "", $orders["orderID"]);
        if (strlen($orderID) < 7)
            return $this->error_msg(-1, "invalid ID " . $orderID);

        $data = pdo_single_select("SELECT orderID FROM online_order WHERE restaurant ='$this->restaurant' and orderID = '$orderID' limit 1");
        if (count($data) <= 0)
            return $this->error_msg(-1, "ID does not exists " . $orderID);

        $content = array("status", "itemindex", "delivery_date", "total", "tax", "currency", "delivery_mode", "status", "title", "firstname", "lastname", "email", "mobile", "address", "zip");
        for ($i = 0; $i < count($content); $i++) {
            $tt = $content[$i];
            $$tt = $this->clean_input($orders[$tt]);
        }

		if($orders_items == "status") {
        	pdo_exec("update online_order set status='$status' where restaurant='$this->restaurant' and orderID='$orderID' limit 1");
	        return $this->result = 1;
			}
			
        pdo_exec("update online_order set itemindex='$itemindex',status='$status',delivery_date='$delivery_date',total='$total',tax='$tax',currency='$currency',delivery_mode='$delivery_mode',title='$title',firstname='$firstname',lastname='$lastname',email='$email',mobile='$mobile',address='$address',zip='$zip' where restaurant='$this->restaurant' and orderID='$orderID' limit 1");

        $content = array("itemID", "status", "unit_price", "quantity", "item_title");
        foreach ($orders_items as $row) {
            for ($i = 0; $i < count($content); $i++) {
                $tt = $content[$i];
                $$tt = $this->clean_input($row[$tt]);
            }
            $action = $status;
            $status = "";
            if ($action == "new")
                pdo_exec("INSERT INTO online_order_item (restaurant,orderID,itemID,status,unit_price,quantity,item_title, tax) VALUES ('$this->restaurant','$orderID','$itemID','','$unit_price','$quantity','$item_title','$tax')");
            else if ($action == "delete")
                pdo_exec("DELETE FROM online_order_item WHERE orderID = '$orderID' and itemID = '$itemID' and restaurant ='$this->restaurant' limit 1");
            else
                pdo_exec("update online_order_item set status='$status',unit_price='$unit_price',quantity='$quantity',item_title='$item_title',tax='$tax' where restaurant='$this->restaurant' and orderID='$orderID' and itemID='$itemID' limit 1");
        }
        return $this->result = 1;
    }

    public function deleteOrders($orderID) {
        
//        Why link to restaurant? 
        
        pdo_exec("DELETE FROM online_order WHERE orderID = '$orderID' and restaurant ='$this->restaurant' limit 1");
        pdo_exec("DELETE FROM online_order_item WHERE orderID = '$orderID' and  restaurant ='$this->restaurant'");

        $this->msg = "";
        return $this->result = 1;
    }
    
    public function notifyonlineorder($order_id){
        $sql = "SELECT online_order.ID, 
                online_order.orderID,
                online_order.restaurant, 
                online_order.total, 
                online_order.currency, 
                online_order.email,
                online_order.firstname,
                online_order.lastname,
                online_order.mobile,
                online_order.total_items, 
                online_order.delivery_time, 
                online_order.delivery_date, 
                online_order.payment_status, 
                online_order.cdate, 
                online_order_item.quantity, 
                menu.item_title, 
                menu.price, 
                restaurant.restaurant 
                AS restaurant 
                FROM online_order 
                LEFT JOIN restaurant 
                ON online_order.restaurant = restaurant.restaurant 
                LEFT JOIN online_order_item 
                ON online_order_item.orderID = online_order.orderID 
                LEFT JOIN menu 
                ON online_order_item.itemID = menu.ID 
                WHERE online_order.ID = {$order_id}";
      $order =pdo_multiple_obj($sql);
      return $order;
    }
    
    
    //orders
    
    static function getOrder($order_id){
       
        $sql = "SELECT online_order.ID,
                online_order.orderID,
                online_order.restaurant, 
                online_order.email,
                online_order.firstname,
                online_order.lastname,
                online_order.mobile,
                online_order.remarks,
                online_order.company,
                online_order.total, 
                online_order.currency, 
                online_order.total_items, 
                online_order.delivery_time, 
                online_order.delivery_date,
                online_order.payment_status, 
                restaurant.restaurant, 
                online_order.cdate
                FROM online_order  
                LEFT JOIN restaurant 
                ON online_order.restaurant = restaurant.restaurant 
                WHERE  online_order.ID = {$order_id}";
                $order =pdo_multiple_obj($sql);
               
        $cmds['order'] = $order[0] ;
        $paySql = "SELECT * FROM payment_details WHERE  orderID ='{$order_id}'";
          
        $cmds["payment"] = pdo_single_select($paySql);
        return $cmds;
    }

    static function getOrderList($user, $take, $skip){
        $orders = array();
        
        $sql = "SELECT online_order.ID, online_order.restaurant, online_order.total, online_order.currency, online_order.total_items, online_order.delivery_date, online_order.delivery_time, online_order.payment_status, restaurant.title, online_order.cdate FROM online_order  LEFT JOIN restaurant ON online_order.restaurant = restaurant.restaurant WHERE online_order.email = '{$user}' AND online_order.payment_status = 1 LIMIT {$take} OFFSET {$skip}";
        $orders['orders'] = pdo_multiple_select($sql);
        
        $sql = "SELECT count(online_order.ID) AS total_orders FROM online_order WHERE online_order.email = '{$user}' AND payment_status = 1";
        $orders['count'] = pdo_multiple_select($sql);

        return $orders;
    }
        
    static function getOrderDetails($order_id){
        $sql = "SELECT online_order.ID, 
                online_order.restaurant, 
                online_order.total, 
                online_order.currency, 
                online_order.total_items, 
                online_order.delivery_time, 
                online_order.payment_status, 
                online_order.cdate, 
                online_order_item.quantity, 
                menu.item_title, 
                menu.price, 
                restaurant.title 
                AS restaurant 
                FROM online_order 
                LEFT JOIN restaurant 
                ON online_order.restaurant = restaurant.restaurant 
                LEFT JOIN online_order_item 
                ON online_order_item.orderID = online_order.orderID 
                LEFT JOIN menu 
                ON online_order_item.itemID = menu.ID 
                WHERE online_order.ID = {$order_id}";
                
        return pdo_multiple_obj($sql);
    }
    
    public function createOrderPhil($data){
        $amount = 0;
        $total_items = 0;
        foreach($_SESSION['cart']['items'] as $key => $item) {
            $amount += $item->quantity * $item->price; 
            $total_items++;
        }
        
        
        //ORDER ID GENERATION
        
        $orderID = $this->getUniqueCode();
        if ($orderID == false)
            return $this->error_msg(-1, "unable to create a unique ID ");
        
         //ORDER ID GENERATION

        $time = date('Y-m-d H:m:s');
        $sql = "INSERT INTO online_order (restaurant, orderID, email, firstname, lastname, mobile, company, total, currency, total_items, delivery_time, delivery_date, remarks, cdate, updated_at) VALUES ('{$_SESSION['cart']['items'][0]->restaurant}', '$orderID','{$data['email']}', '{$data['firstname']}', '{$data['lastname']}', '{$data['phone']}', '{$data['company']}', '{$amount}', '{$_SESSION['cart']['items'][0]->currency}', '{$total_items}', '{$data['delivery_time']}', '{$data['delivery_date']}', '{$data['remarks']}','{$time}', '{$time}')";
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $lastInsertID = $db->lastInsertId();
        foreach ($_SESSION['cart']['items'] as $key => $value) {
            $sql = "INSERT INTO online_order_item (orderID, itemID, restaurant, status, quantity, unit_price, item_title) VALUES ('$orderID', '{$value->ID}',  '{$_SESSION['cart']['items'][0]->restaurant}', 'new' ,'{$value->quantity}',  '{$value->price}',  '{$value->item_title}')";
            $stmt = $db->prepare($sql);
            $stmt->execute();
        }
        $restaurant =$_SESSION['cart']['items'][0]->restaurant;
        
        $payment = new WY_Paypal($restaurant);
        $res = new WY_restaurant();
        $restpayment =  $res->getRestPaypalId($restaurant);
        $internationalPath = $res->getRestaurantInternalPath($restaurant);

        $data = [
            "payment_id" => $lastInsertID,
            "orderId" =>$orderID,
            "amount" => number_format($amount, 2),
            "total_items" => $total_items,
            "paypal_id"=>$restpayment['paypal_id'],
            'rest_path'=>$internationalPath
        ];
        $result = $payment->paynow($data);
        if($result['status']=='Success'){
            $data['status']='Success';
            $data['paypalUrl'] = $result['paypalUrl'];
        }else{
            $data['status']='Fail';
            $data['paypalUrl'] = "";
        }
        
        return $data;
    }
    
    //orders

    //shopping cart
    static function getItemDetails($data){
        $sql  = "SELECT ID, restaurant, item_title, item_description, price, currency FROM menu WHERE ID = '{$data['menu_id']}' LIMIT 1";
        $db   = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->execute();

        $menu_items = $stmt->fetchAll(PDO::FETCH_OBJ);
        if (count($menu_items) > 0) {
            $menu_item = $menu_items[0];
            $menu_item->restaurant_path = $data['restaurant_path'];

            $menu_item->quantity = 1;
            if (!isset($menu_item->price)) {
                $menu_item->price = 0;
            }

            if (isset($_SESSION['cart']) && isset($_SESSION['cart']['items'])) {
                if($menu_item->restaurant != $_SESSION['cart']['items'][0]->restaurant) {
                    throw new Exception('can not add item from many restaurant', 1);
                }
                $addToCart = true;
                foreach ($_SESSION['cart']['items'] as $key => $value) {
                    if ($value->ID == $menu_item->ID) {
                        $addToCart = false;
                    }
                }
                if ($addToCart) {
                    array_push($_SESSION['cart']['items'], $menu_item);
                } else {
                    throw new Exception('Item already in cart', 1);
                }
            } else {
                $_SESSION['cart']['items'] = [$menu_item];
            }
        } else {
        throw new Exception('Item not found', 1);
        }
    }
    
    static function putItemInCart($item_id, $quantity){
        if(!preg_match('/^[0-9]+$/', $quantity)) {
            throw new Exception('input not valid', 1);
        }
        $issetInCart = false;
        foreach ($_SESSION['cart']['items'] as $key => $value) {
            if($value->ID == $item_id) {
                $_SESSION['cart']['items'][$key]->quantity = (int) $quantity;
                $issetInCart = true;
            }
        }
        return $issetInCart;
    }
    
    static function deleteItemInCart($item_id){
        $issetInCart = false;
        if(!isset($_SESSION['cart'])) {
            throw new Exception("Cart not found", 1);
        } else {
            if(count($_SESSION['cart']['items']) == 1) {
                unset($_SESSION['cart']);
            } else {
                foreach ($_SESSION['cart']['items'] as $key => $value) {
                    if($value->ID == $item_id) {
                        array_splice($_SESSION['cart']['items'], $key, 1);
                        $issetInCart = true;
                    }
                }
                if(!$issetInCart) {
                    throw new Exception("item does not in cart", 1);
                }
            }
        } 
        return $issetInCart;
    }

    static function getCart(){
        if(!isset($_SESSION['cart'])) {
            throw new Exception("cart not found", 1);
        }
        return $_SESSION['cart'];
    }
    
    static function setDeliveryTime($data){
            if(!isset($_SESSION['cart'])) {
                throw new Exception("cart not found", 1);
            } else {
                $_SESSION['cart']['delivery_time'] = $data['delivery_time'];
            }
            return true;
    }
    
    static function deleteCart(){
        unset($_SESSION['cart']);
        return true;
    }
    //shopping cart
}

?>
