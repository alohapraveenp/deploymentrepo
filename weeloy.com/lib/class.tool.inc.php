<?php

require_once("conf/conf.init.inc.php");
require_once("lib/wpdo.inc.php");
require_once("lib/wglobals.inc.php");
require_once("conf/conf.session.inc.php");

class WY_Tool {

    function json_validate($string) {
        // decode the JSON data
        $result = json_decode($string);

        // switch and check possible JSON errors
        switch (json_last_error()) {
            case JSON_ERROR_NONE:
                $error = '';
                break;
            case JSON_ERROR_DEPTH:
                $error = 'The maximum stack depth has been exceeded.';
                break;
            case JSON_ERROR_STATE_MISMATCH:
                $error = 'Invalid or malformed JSON.';
                break;
            case JSON_ERROR_CTRL_CHAR:
                $error = 'Control character error, possibly incorrectly encoded.';
                break;
            case JSON_ERROR_SYNTAX:
                $error = 'Syntax error, malformed JSON.';
                break;
            // PHP >= 5.3.3
            case JSON_ERROR_UTF8:
                $error = 'Malformed UTF-8 characters, possibly incorrectly encoded.';
                break;
            // PHP >= 5.5.0
            case JSON_ERROR_RECURSION:
                $error = 'One or more recursive references in the value to be encoded.';
                break;
            // PHP >= 5.5.0
            case JSON_ERROR_INF_OR_NAN:
                $error = 'One or more NAN or INF values in the value to be encoded.';
                break;
            case JSON_ERROR_UNSUPPORTED_TYPE:
                $error = 'A value of a type that cannot be encoded was given.';
                break;
            default:
                $error = 'Unknown JSON error occured.';
                break;
        }

        return $error;
        
    }

    function getDataCheck() {
        // error_log("In class");
        
        $objTables = array();
        $_SESSION['tablecleaning'] = array();
        
        $mytables = list_table_names(); 
        
        foreach($mytables as $table) {
            $tablecolumns = list_table_field($table);
            // echo "<hr /><h3>" . $table . "</h3>";
            foreach($tablecolumns as $col) {
                $data = pdo_multiple_select("SELECT * from `$table` where `$col` like '{%' limit 1");
                    if(count($data) > 0) {
                        $data = pdo_multiple_select("SELECT * from `$table` where `$col` like '{%'  &&  `$col` != '{}' limit 1");
                        // echo "<p style='margin-left:50px'>" . $col . "</p>";
                        $objTables[] = $table . "-" . $col;
                        $_SESSION['tablecleaning'][] = $table . "-" . $col;
                    }
            }
        }

        return $objTables; 
    }

    function getColumnCheck ($record) {
        $itemAr = explode("-", $record);

        $table = $itemAr[0];
        $col = $itemAr[1];     
        $errdata = array();   

        $data = pdo_multiple_select("SELECT ID, $col AS COLMN, replace($col,'’','\"') as JASON_MSG from `$table` where  `$col` != '' and  `$col` != '{}' and `$col` LIKE '{%'");
            foreach($data as $row) {
                if ($this->json_validate($row['JASON_MSG']) != '') {
                    $errdata[] = $row;
                }
            }
        return $errdata;
    }

    function updColumn ($record, $id, $value) {

        $updcol = "'" . str_replace('"','’', $value) . "'";

        $itemAr = explode("-", $record);

        $table = $itemAr[0];
        $col = $itemAr[1];   

        $affected_rows = pdo_exec("UPDATE $table SET $col = $updcol WHERE ID = $id limit 1");
        return "Column $col has been updated";
    }

}

?>
