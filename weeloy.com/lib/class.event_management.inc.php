<?php

require_once("conf/conf.init.inc.php");
require_once "lib/class.notification.inc.php";
require_once "lib/class.restaurant.inc.php";

class WY_EventManagement {

    use CleanIng;

    //public $firstname = '';

    function __construct($theRestaurant = NULL) {
        
    }

    static function getEventProjet($args) {
        $parameters['event_id'] = $args['event_id'];
        $sql = "SELECT * FROM event_management WHERE eventID = :event_id";
        return pdo_single_select_with_params($sql, $parameters);
    }

    static function createEventProjet($args) {
 
        $date = explode('-', $args['rdate']);
        $parameters['id'] = uniqid();
        $parameters['restaurant'] = $args['restaurant'];
        $parameters['firstname'] = $args['firstname'];
        $parameters['lastname'] = $args['lastname'];
        $parameters['company'] = (!empty($args['company'])) ? $args['company'] : NULL;
        $parameters['email'] = $args['email'];
        $parameters['phone'] = $args['phone'];
        $parameters['rdate'] = $date[2]."-".$date[1] ."-" .$date[0];
        $parameters['rtime'] = $args['rtime'];
        $parameters['pax'] = $args['pax'];
        $parameters['occasion'] = (!empty($args['occasion'])) ? $args['occasion'] : NULL;
        $parameters['special_requests'] = (!empty($args['special_requests'])) ? $args['special_requests'] : NULL;
        $parameters['name'] = $args['name'];
        $parameters['title'] = $args['salutation'];

        $sql = "INSERT INTO `event_management` (`ID`, `eventID`, `restaurant`, `title`, `firstname`, `lastname`, `company`, `email`, `phone`, `rdate`, `rtime`, `pax`, `occasion`, `name`,`special_requests`, `status`) VALUES (NULL, :id, :restaurant, :title, :firstname, :lastname, :company, :email, :phone, :rdate, :rtime, :pax, :occasion,:name, :special_requests, 'request_sent');";
        if (pdo_insert($sql, $parameters)) {
            $parameters['eventID'] = $parameters['id'];
            WY_EventManagement::notifyEventMangement($parameters, 'event_request');
            return true;
        }


        return false;
    }

    static function getEventMenu($args) {
        $parameters['event_id'] = $args['event_id'];
        $sql = "SELECT event_menu.*, menu_categorie.price FROM `event_menu` LEFT JOIN menu_categorie ON menu_categorie.value = event_menu.notes WHERE menu_categorie.restaurant  = event_menu.restaurant  AND `displayID` LIKE '' AND `eventID` LIKE :event_id ORDER BY morder ASC";
        //$sql = "SELECT menu_categorie.menuID, menu_categorie.value, menu_categorie.description FROM event_menu, menu_categorie WHERE event_menu.menuID = menu_categorie.menuID AND eventID = :event_id GROUP BY event_menu.menuID";
        $menu_categories = pdo_multiple_select_with_params($sql, $parameters);

        $sql = "SELECT * FROM event_menu, menu WHERE event_menu.menuID = menu.menuID AND event_menu.menuItemID = menu.itemID AND eventID = :event_id AND  event_menu.restaurant = menu.restaurant AND `displayID`  NOT LIKE '' ORDER BY event_menu.morder ASC";
        $items = pdo_multiple_select_with_params($sql, $parameters);

        $menu = array();
        foreach ($menu_categories as $menu_categorie) {
            $arr_item = array();
            foreach ($items as $item) {
                if ($menu_categorie['menuItemID'] == $item['displayID']) {
                    $arr_item[] = $item;
                }
            }
            $menu_categorie['items'] = $arr_item;
            $menu[] = $menu_categorie;
        }
        return $menu;
    }

    static function saveMenuSelection($args) {
        $parameters['event_id'] = $args['event_id'];
        $parameters['status'] = $args['status'];
        
        //$inQuery = implode(',', $args['items']['id']);
        foreach ($args['items'] as $item){
            $parameters['item'] = $item['id'];
            $parameters['qte'] = $item['qte'];
            $sql = "UPDATE event_menu SET `status` = :status, `qte` = :qte  WHERE `eventID` = :event_id AND `menuItemID` = :item ";
            pdo_exec_with_params($sql, $parameters);
        }
        return true;
        //return true;
    }


    static function notifyEventMangement($args, $notify_type) {
      
        $res = new WY_restaurant;
        $res->getRestaurant($args['restaurant']);

        $evt_request->restaurant = $args['restaurant'];
        $evt_request->restaurantinfo = $res;
        $evt_request->restaurant_email = $res->email;
        $evt_request->restaurant_title = $res->title;
        $evt_request->event_id = $args['id'];
        $evt_request->email = $args['email'];
        $evt_request->date = $args['rdate'];
        $evt_request->firstname = $args['firstname'];
        $evt_request->lastname = $args['lastname'];
        $evt_request->salutation = $args['title'];
        $evt_request->company = $args['company'];
        $evt_request->name = (isset($args['occasion'])) ? $args['occasion'] : '';
        $evt_request->order = WY_EventManagement::getEventProjet(array('event_id' =>$args['eventID'])); 
        //error_log("PAYMENT DETAILS" .print_r(json_decode ($evt_request->order['payment_details']),true));
        if(isset($evt_request->order['payment_details'])){
            $evt_request->order['payment_details'] = json_decode($evt_request->order['payment_details'], true);
        }
        $evt_request->tnc = (isset($args['tnc'])) ? $args['tnc'] : '';
        $evt_request->white_label = true;
        //if ($notify_type == 'event_menu_selection_req' ) {
            $evt_request->event_id = $args['eventID'];
            $evt_request->link = __BASE_URL__ . '/' . $res->internal_path . '/event-management/menu-selection/' . $evt_request->event_id . '?dspl_h=f&dspl_f=f';
            $ps_restaurants = array('SG_SG_R_TheFunKitchen','SG_SG_R_TheOneKitchen','SG_SG_R_PscafeAtHardingRoad', 'SG_SG_R_PscafeAtAnnSiangHillPark', 'SG_SG_R_PscafeAtPalaisRenaissance', 'SG_SG_R_PscafeAtParagon', 'SG_SG_R_PscafePetitAtMartinRoad', 'SG_SG_R_ChopsueyCafeAtMartinRoad', 'SG_SG_R_ChopsueyCafeAtDempseyHill');
        if (in_array($evt_request->restaurant, $ps_restaurants, true)) {
            $evt_request->link = __BASE_URL__ . '/modules/event_management/menu-selection.php?event_id='.$evt_request->event_id;
        }
        //}
        $notify = new WY_Notification;
        $notify->notifyEvent($evt_request, $notify_type);
    }

    function updatePaymentStatus($event_id, $email, $status) {
        // risk of injection - USE pdo_exec_with_params instead 
        $sql = "UPDATE event_management SET status = '$status' WHERE eventID = '$event_id'  AND email = '$email' ";
        return pdo_exec($sql);
    }
    
    static function requestform($restaurant,$email){
        
        $res = new WY_restaurant;
        $mailer = new JM_Mail;
        $res->getRestaurant($restaurant);
        $evt_request = new stdClass();
        $evt_request->restaurant = $restaurant;
        $evt_request->res_title = $res->title;
        //$evt_request->request_form = __BASE_URL__ . '/' . $res->internal_path . '/event_management/request?dspl_f=f&dspl_h=f';
        
        $header['from'] = array('reservation@weeloy.com' => $res->title);
        $header['replyto'] = array($res->email);
        $header['white_label'] = true;
        $evt_request->request_form = __BASE_URL__ . '/modules/event_management/request.php?restaurant='.$restaurant;
        $ps_restaurants = array('SG_SG_R_PscafeAtHardingRoad', 'SG_SG_R_PscafeAtAnnSiangHillPark', 'SG_SG_R_PscafeAtPalaisRenaissance');
        if (in_array($evt_request->restaurant, $ps_restaurants, true)) {
            $header['cc'] = 'concierge@pscafe.com';
            if($evt_request->restaurant !=='SG_SG_R_TheFunKitchen'){
                $evt_request->request_form ="http://www.pscafe.com/events-group-bookings/";
            }
 
        }
        
        //$header['cc'] = 'vksuriya2007@gmail.com';

        $body = $mailer->getTemplate('event/member/white_label/requestform', $evt_request);

        $mailer->sendmail($email, "Event Booking Form ", $body, $header, NULL, 'welcome');
        WY_EventManagement::saveRequestFormDetails($email,$restaurant);
        return true;
    }
     function updateprice($args) {
         
        $parameters['event_id'] = $args['event_id'];
        $parameters['total_amount'] = $args['total_amount'];
        $parameters['amount'] = $args['amount'];
        $parameters['gst'] = $args['gst'];
        $parameters['service_charge'] = $args['service_charge'];
        
        $parameters['status'] = $args['status'];
        $now = (strstr($_SERVER['HTTP_HOST'], 'localhost:8888')) ? "NOW()" : "DATE_ADD(NOW(), INTERVAL 8 HOUR)";
        $parameters['updated_at'] = $now;
        //WY_EventManagement::savemodifyDetails($args['event_id']);
        $sql = "UPDATE event_management SET `total_amount` = :total_amount,`amount` = :amount,`gst` = :gst,`service_charge` = :service_charge, `status` = :status,`updated_at` = :updated_at   WHERE `eventID` = :event_id";
        $data = pdo_exec_with_params($sql, $parameters);
       
        return $data;
   
    }
    
    static function savemodifyDetails($event_id){
         //update  payment details
        $parameters['event_id'] = $event_id;
        
        $sql = "SELECT total_amount,restaurant,payment_details FROM event_management WHERE eventID = :event_id";
        $pre_details = pdo_single_select_with_params($sql, $parameters);
        
        if(isset($pre_details)){
            if(!empty($pre_details['payment_details'])){
                $parameters['payment_details'] = $pre_details['payment_details'];
                $parameters['total_amount'] = $pre_details['total_amount'];
                $parameters['restaurant'] = $pre_details['restaurant'];
                $sql = "INSERT INTO `event_management_modify` (`ID`, `eventID`, `restaurant`, `total_amount`, `payment_details`) VALUES (NULL, :event_id, :restaurant, :total_amount, :payment_details);";
                pdo_insert($sql, $parameters);
                $params['payment_details'] ='';
                $params['event_id'] = $parameters['event_id'];
                $sql = "UPDATE event_management SET `payment_details` = :payment_details WHERE `eventID` = :event_id";
                $data = pdo_exec_with_params($sql, $params);
            }
        }
    }
    function saveEvPaymentDetails($args){
        
        $parameters['event_id'] = $args['event_id'];
        $parameters['status'] = $args['status'];
        $dataArr = array(
            'type' => $args['type'],
            'deposit_amount' => $args['amount'],
            'currency'  => 'SGD',
            'card_id'  => $args['card_id'],
            'payment_id'  => $args['payment_id'],
         );
        $parameters['payment_details'] = json_encode($dataArr);
        $now = (strstr($_SERVER['HTTP_HOST'], 'localhost:8888')) ? "NOW()" : "DATE_ADD(NOW(), INTERVAL 8 HOUR)";
        $parameters['updated_at'] = $now;
        $sql = "UPDATE event_management SET `payment_details` = :payment_details,`status` = :status,`updated_at` = :updated_at  WHERE `eventID` = :event_id";
        return pdo_exec_with_params($sql, $parameters);
        
    }
    
    function getEventModifyDetails($event_id){
        
        $parameters['event_id'] = $event_id;
        
        $sql = "SELECT * FROM event_management_modify WHERE eventID = :event_id order by ID DESC";
        $pre_details = pdo_multiple_select_with_params($sql, $parameters);
        $total_depositamount = 0;
        $total_amount = 0;
        $length = count($pre_details);
        $last = $length-1;
        $last_payment = 0;
        for ($i = 0; $i < count($pre_details); $i++) {
  
            $payment_details = json_decode($pre_details[$i]['payment_details']);
            $total_amount = $total_amount+ $pre_details[$i]['total_amount'];
            if($i == $last){
                 $last_payment =  $pre_details[$i]['total_amount'];
            }
            $total_depositamount = $total_depositamount + $payment_details->deposit_amount;
        }
//        foreach ($pre_details as $dt) {
//            $payment_details = json_decode($dt['payment_details']);
//            $total_amount = $total_amount+ $dt['total_amount'];
//            $total_depositamount = $total_depositamount + $payment_details->deposit_amount;
//        }
        $pre_details['total_deposit'] = $total_depositamount;
        $pre_details['total_amount'] = $total_amount;
        $pre_details['last_total'] = $last_payment;

        return $pre_details;
        
    }
    
     static function saveRequestFormDetails($email,$restaurant) {
        $parameters['email'] = $email;
        $parameters['restaurant'] = $restaurant;
         
        $sql = "INSERT INTO `event_request_form` (`ID`,`restaurant`,`email`) VALUES (NULL,:restaurant, :email);";
        if (pdo_insert($sql, $parameters)) {
            return true;
        }
    }
    
    function getRequestformList($restaurant) {
        $parameters['restaurant'] = $restaurant;
        $sql = "SELECT email,created_at as date FROM event_request_form WHERE restaurant = :restaurant";
        return pdo_multiple_select_with_params($sql, $parameters);
    }
    
    
    
}
