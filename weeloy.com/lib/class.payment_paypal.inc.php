<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once ('lib/class.restaurant.inc.php');
require_once ('lib/class.payment.inc.php');
require_once ('lib/class.booking.inc.php');



class WY_Payment_Paypal {
    
    public $restaurant;
    public $orderid;
    public $type; 
    public $api_user;
    public $api_pass ;
    public $api_sin ;
    public $appId ;
    public $envelope;
    public $paypalUrl;
    public $apiUrl;
    public $amount;
    public $payment_object = NULL;
    public $mode;
    public $return_url;
    public $ref_id;

    function __construct($theRestaurant = NULL, $amount = 0, $return_url = NULL, $mode = 'invoice',$ref_id ='') {
        
        $this->restaurant = $theRestaurant;
        $this->amount = $amount;
        $this->mode =  $mode;
        $this->return_url = $return_url;
        $this->ref_id = $ref_id;
       
     
        if ($this->is_test_env()) {
       
            $this->api_user = "payment-facilitator_api1.weeloy.com";

            $this->api_pass = "MUX6UNLQ7RQVQVXF";

            $this->appId = "APP-80W284485P519543T";
            $this->api_sin = "AFcWxV21C7fd0v3bYYYRCpSSRl31AiWe1nT8lQB-heMKYK3JadVKyPvW";

            $this->apiUrl = "https://svcs.sandbox.paypal.com/AdaptivePayments/";

            $this->paypalUrl = "https://sandbox.paypal.com/webscr?cmd=_ap-payment&paykey=";

            $this->envelope = array(
                "errorLanguage" => "en_US",
                "detailLevel" => "ReturnAll"
            );

            $this->header = array(
                "X-PAYPAL-SECURITY-USERID:payment-facilitator_api1.weeloy.com",
                "X-PAYPAL-SECURITY-PASSWORD:MUX6UNLQ7RQVQVXF",
                "X-PAYPAL-SECURITY-SIGNATURE:AFcWxV21C7fd0v3bYYYRCpSSRl31AiWe1nT8lQB-heMKYK3JadVKyPvW",
                "X-PAYPAL-APPLICATION-ID:APP-80W284485P519543T ",
                "X-PAYPAL-REQUEST-DATA-FORMAT:" . "JSON",
                "X-PAYPAL-RESPONSE-DATA-FORMAT:" . "JSON"
            );
               
        }else{
            
            $this->api_user = "payment_api1.weeloy.com";
            $this->api_pass = "56JESSRHN3ZPHESX";
            $this->appId = "APP-43C57808MN721350K";
            $this->api_sin = "AFcWxV21C7fd0v3bYYYRCpSSRl31AsfRY-WsSNgyBGb5xlpDzECF-ozh";
            $this->apiUrl = 'https://svcs.paypal.com/AdaptivePayments/'; 
            $this->paypalUrl = "https://paypal.com/webscr?cmd=_ap-payment&paykey=";
            $this->envelope = array(
                "errorLanguage" => "en_US",
                "detailLevel" => "ReturnAll"
            );

            $this->header = array(
                "X-PAYPAL-SECURITY-USERID:payment_api1.weeloy.com",
                "X-PAYPAL-SECURITY-PASSWORD:56JESSRHN3ZPHESX",
                "X-PAYPAL-SECURITY-SIGNATURE:AFcWxV21C7fd0v3bYYYRCpSSRl31AsfRY-WsSNgyBGb5xlpDzECF-ozh",
                "X-PAYPAL-APPLICATION-ID:APP-43C57808MN721350K",
                "X-PAYPAL-REQUEST-DATA-FORMAT:" . "JSON",
                "X-PAYPAL-RESPONSE-DATA-FORMAT:" . "JSON"
          );
   
        }
    }
    function getJsonObject($currency,$restpayment_id,$ref_id){
        return  array(
            "actionType" => "PAY",
            "currencyCode" => $currency,
            "receiverList" => array(
                "receiver" => array(
                    array(
                        "amount" => $this->amount,
                        "email" => $restpayment_id
                    )
                )
            ),
           "returnUrl" => $this->return_url.$ref_id, 
           "cancelUrl" => $this->return_url,
           "requestEnvelope" => $this->envelope
        );
    }
    
    public function makePay(){  
      
        $res = new WY_restaurant();
        
        $currency = $res->getCurrency($this->restaurant);
        $receiver = ($this->mode == 'invoice') ? 'weeloy' : $this->restaurant;
        $restpayment = $res->getRestaurantPaymentId($receiver);
        $restpayment_id = $restpayment['paypal_id'];
        $ref_id = uniqid();
        $currency =  (!empty($currency)) ? $currency : "SGD";
        
         
        $data = $this->getJsonObject($currency,$restpayment_id,$ref_id); 
        


        $response = $this->paypalCall($data, "Pay", $this->apiUrl);
     
        $resArray = array('paypalUrl'=> '' ,'status' => '');
        if (count($response) > 0) {
            $payment = new WY_Payment();
            if (isset($response['responseEnvelope']['ack']) == "Success") {
                $resArray['status'] = $response['responseEnvelope']['ack'];
                $payment_id = $payment->savePayment($this->amount, $this->mode, $ref_id, $this->restaurant, $response['payKey'], 'paypal', 'pending', 0, 0, 'payment_created');
            }
        }
        $resArray['paypalUrl'] = $this->paypalUrl . $response['payKey'];
        return $resArray;
  
    }
    
    public function makeDeposit(){
 
        $res = new WY_restaurant();
        $currency = $res->getCurrency($this->restaurant);
       
        $restpayment = $res->getRestaurantPaymentId($this->restaurant);
        $restpayment_id = $restpayment['paypal_id'];
        $currency =  (!empty($currency)) ? $currency : "SGD";
        $payToken = md5(time());
           //paypal return url
        
        

        if($this->mode == 'event_ordering'){
             $this->return_url =__BASE_URL__."/modules/payment/paypal/paypal_event_ordering.php?refid="; 
        }
        if($this->mode == 'booking'){
           $booking = new WY_Booking();
           $booking->getBooking($this->ref_id); 
           $title= $booking->restaurantinfo->title;
           $city = $booking->restaurantinfo->city;
           $this->return_url =__BASE_URL__."/modules/booking/deposit/deposit_confirm_redirect.php?booking_deposit_id=$this->ref_id&token=$payToken&type=paypal&reaction=bkdepsoit"; 
        }
       //testing purpose only if go live  need to remove this condition
        //$this->amount ='10';
        
        
        $data = $this->getJsonObject($currency,$restpayment_id,$this->ref_id); 
        
        $response = $this->paypalCall($data, "Pay", $this->apiUrl);

        $resArray = array('paypalUrl'=> '' ,'status' => '','paykey ' => '');
        if (count($response) > 0) {
            $payment = new WY_Payment();
            if (isset($response['responseEnvelope']['ack']) == "Success") {
                $resArray['status'] = $response['responseEnvelope']['ack'];
                if($this->mode == 'booking'){
                    $payment_id = $payment->saveDeposit(json_encode($data),$this->amount, $this->restaurant,$payToken,$response['payKey'],'pending_payment','paypal', $this->ref_id,$currency);
                }
                if($this->mode == 'event_ordering'){
                    $resArray['status'] = $response['responseEnvelope']['ack'];
                    $payment_id = $payment->savePayment($this->amount, $this->mode, $this->ref_id, $this->restaurant, $response['payKey'], 'paypal', 'pending', 0, 0, 'payment_created');
                }
                $resArray['paypalUrl'] = $this->paypalUrl.$response['payKey'];
                $resArray['returnurl'] = $this->return_url;
            }
        }
        return $resArray;
     
    }
    function paypalCall($data, $call, $apiUrl= NULL) {
        
        if (strstr($_SERVER['HTTP_HOST'], 'localhost')  ){   
            $response['responseEnvelope']['ack'] = "Success";
            $response['payKey'] = 'AP-3UG78227CL455093H';
            return $response;
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,  $this->apiUrl.$call);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->header);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, TRUE);
        $response_data = curl_exec($ch);
        $response_data = json_decode($response_data, TRUE);
        curl_close($ch);
             
        return $response_data;

    }
    
    public function getPaymentResponse($transaction){
        $data = array('payKey'=>$transaction,"requestEnvelope"=> $this->envelope);

        $response = $this->paypalCall($data, "PaymentDetails", $this->apiUrl);

        $status = 'pending';
        if ($response['responseEnvelope']['ack'] == "Success") {
            $status = "COMPLETED";
        }

        return $status;
    }
    function is_test_env() {

        if (strpos($_SERVER['HTTP_HOST'], 'api') !== false) {
            return false;
        }

        if (strpos($_SERVER['HTTP_HOST'], 'www') !== false) {
            return false;
        }

        if (strpos($_SERVER['HTTP_HOST'], 'prod') !== false) {
            return false;
        }
        if (strpos($_SERVER['HTTP_HOST'], 'codersvn') !== false) {
            return false;
        }
        //testing purpose only if go live  need to remove this condition
        $ps_restaurants = array('SG_SG_R_TheFunKitchen','SG_SG_R_PscafeAtHardingRoad', 'SG_SG_R_PscafeAtAnnSiangHillPark', 'SG_SG_R_PscafeAtPalaisRenaissance', 'SG_SG_R_PscafeAtParagon', 'SG_SG_R_PscafePetitAtMartinRoad', 'SG_SG_R_ChopsueyCafeAtMartinRoad', 'SG_SG_R_ChopsueyCafeAtDempseyHill');
        if (in_array($this->restaurant, $ps_restaurants, true)) {
             return true;
        }
        

        return true;
    }
    
    public function makeRefund(){
        $res = new WY_restaurant();
        $payment = new WY_Payment(); 
        $currency = $res->getCurrency($this->restaurant);
        if($this->mode == 'booking'){
           $booking = new WY_Booking();
           $booking->getBooking($this->ref_id); 
        }
        
        $paymentdetails = array();
        $status ="refundfailed";
        $paymentdetails['status'] = $status;
        
        $data = array('payKey'=> $booking->deposit_id,"requestEnvelope"=> $this->envelope,'currencyCode' => $currency,
            "receiverList" => array(
                "receiver" => array(
                    array(
                        "amount" => $this->amount,
                        "email"  => $booking->email,
                    )
                ),
        ));

        $response = $this->paypalCall($data, "Refund", $this->apiUrl);
        if (strstr($_SERVER['HTTP_HOST'], 'localhost')){  
            
            $response['responseEnvelope']['ack'] = "Success";
            $response['payKey'] = 'AP-8DX33992VT2080908';
            $response['refundInfoList']['refundInfo'][0]['receiver']['amount'] = $this->amount;
            $response['refundInfoList']['refundInfo'][0]['refundStatus'] = 'REFUNDED';
        }
        
        if ($response['responseEnvelope']['ack'] == "Success") {

          $refundAmount= $response['refundInfoList']['refundInfo'][0]['receiver']['amount'];
          $status = $response['refundInfoList']['refundInfo'][0]['refundStatus'];
          $paymentdetails['status'] = $status;

          $payment->updateRefundDetails($status,$booking->deposit_id,$refundAmount);
          return $paymentdetails;

        }
        return $paymentdetails;
        
    }
    
    
}

