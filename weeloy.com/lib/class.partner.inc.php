<?php
require_once('conf/conf.init.inc.php');
require_once('lib/passbook/pkpass/src/PKPass.php');

class WY_Partner {

    public $restaurant;
    public $api_endpoint;
    public $login;
    public $password;
   
    function __construct($restaurant) {
        $this->restaurant = $restaurant;
    }
    
    function getCredentials($partner){
        if($partner == 'NetSuite'){
            if($this->restaurant == 'SG_SG_R_MadisonRooms' ){
                $this->api_endpoint = 'https://rest.netsuite.com/app/site/hosting/restlet.nl?script=customscriptmr_reqapi&deploy=1';
                $this->login = '';
                $this->password = '';
            }
        }
        if($partner == 'Revel'){
            if($this->restaurant == 'SG_SG_R_MadisonRooms' ){
                $this->api_endpoint = 'http://revelup.net/bacchanalia/api/weeloy/OpenTable';
                $this->login = 'Q0YPUy2eFLEG0jKYM2WuuxUmIomzbpnn';
                $this->password = '98oFyomYyWh7TeVaSjizNhRwQImLax1r0BYixs6bbk4cRzu530cypTyFmRTFMuaR';
            }
        }   
    }
    
    public function pushEventToPartner($booking, $partner = NULL, $action = NULL) {
        if($partner == 'NetSuite' ){
                $this->pushEventToPartner_Netsuite($booking, $action);
        }
        if($partner == 'Revel' ){
                $this->pushEventToPartner_Revel($booking);
        }        
        return true;
    }    

    public function pushEventToPartner_Netsuite($booking, $action = 'A'){
        $partner = 'NetSuite';
        $this->getCredentials('NetSuite');
        
        if($this->restaurant == 'SG_SG_R_MadisonRooms' ){
            //header
            $header = array(); 
            $header[0] = 'Content-type: application/json';
            $header[1] = 'Keep-alive: "timeout=30"';  
            $header[2] = 'Authorization : NLAuth nlauth_account="4150703", nlauth_email="alejandro@madisonrooms.asia", nlauth_signature="Alejandro023", nlauth_role="1000"'; 
            
            $consumer_info = $this->getMadisonConsumerInfo($booking->email);
            
            if(empty($consumer_info)){
                error_log('Madison Member not found');
                return -1;
            }
            
            //parameters
            $fields = array();
            $fields['event_id'] = $booking->confirmation;
            
            //$fields['customer_id'] = $consumer_info['netsuite_id']; //$this->getMadisonConsumerInfo($booking->email);
            //$fields['company'] = $consumer_info['netsuite_company_id']; 
            
            $fields['company'] = $consumer_info['netsuite_id'];
            
            $fields['title'] = 'Booking';
            $fields['location'] = $this->getLocation($booking->generic);
            $fields['status'] = ($action != 'C') ? 'CONFIRMED' : 'CANCELLED';
            $fields['alldayevent'] = 'F';
            $fields['startdate'] = $this->convertDate($booking->rdate);
            $fields['enddate'] = $this->getEndDate($booking->rdate, $booking->rtime);
            $fields['starttime'] = $this->convertTime($booking->rtime);
            $fields['endtime'] = $this->getEndTime($booking->rdate, $booking->rtime);
            $fields['event_phoneno'] = $booking->mobile;
            $fields['table_no'] = 'NA';
            $fields['no_of_attendees'] = $booking->cover;
            $fields['special_request'] = $booking->specialrequest;
            $fields['purpose'] = $this->getPurpose($booking->generic);
            $fields['room_no'] = 'NA';
            $fields['message'] = 'none';

            if($action == 'A'){
                $parameters = array("type"=>"event", "action"=>"A", "data"=>$fields);
            }else{
                $sql = "SELECT partner_booking_id FROM booking WHERE confirmation LIKE '$booking->confirmation'";
                $data = pdo_single_select($sql);
                $parameters = array("type"=>"event", "action"=>"U", "id"=>"$data[partner_booking_id]", "data"=>$fields);
            }
            
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_HTTPHEADER,$header);
            curl_setopt($ch, CURLOPT_URL,$this->api_endpoint);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS,
            json_encode($parameters));

            // receive server response ...
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $server_output = curl_exec ($ch);
            $response_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            
            curl_close ($ch);
            // further processing ....
            if ($server_output != "") {
                $response = json_decode($server_output);
  
               if($response->error == false || $response_code == 200)
                    pdo_exec("UPDATE booking SET partner_id = '1', partner_booking_id = '$response->id' WHERE booking.confirmation = '$booking->confirmation' limit 1");

                if($response_code == 200){                    
                    $sql = "INSERT INTO log_partner_api (`partner`, `restaurant`, `booking_confirmation`, `headers`, `parameters`, `response`) VALUES ('$partner', '$this->restaurant', '$booking->confirmation', '".json_encode($header)."',  '".json_encode($parameters)."', '$server_output');";
                    pdo_exec($sql, 'dwh');
                    return true;
                }else{
                    $sql = "INSERT INTO log_partner_api_error (`partner`, `restaurant`, `booking_confirmation`, `headers`, `parameters`, `response`) VALUES ('$partner', '$this->restaurant', '$booking->confirmation', '".json_encode($header)."',  '".json_encode($parameters)."', '$server_output');";
                    pdo_exec($sql, 'dwh');
                    return true;
                }

             } 
            //else { ... }
        }
        
        
    }
  
    public function pushEventToPartner_Revel($booking){
        $partner = 'Revel';
        $this->getCredentials($partner);
        
        if($this->restaurant == 'SG_SG_R_MadisonRooms' ){
            //header
            $header = array(); 
            $header[0] = 'Content-type: application/json';
            $header[1] = 'Keep-alive: "timeout=30"';  
            $header[2] = "API-AUTHENTICATION: $this->login:$this->password"; 
            
            
            //parameters
            $fields = array();
            
            // partner event id // NETSUITE event id
            $event_id = $booking->partner_booking_id;
            
            
            $fields['customer_email'] = $booking->email;
            $fields['eventId'] = $event_id;
            $fields['party_size'] = $booking->cover;
            $fields['customer_name'] = $booking->firstname . ' ' . $booking->lastname;
            $fields['customer_phone_number'] = $booking->mobile;
            $fields['note'] = $booking->specialrequest;
            $fields['targeted_time'] = $booking->rdate . ' ' . $booking->rtime.':00';

            $parameters = $fields;
            
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            curl_setopt($ch, CURLOPT_HEADER, 1);
            curl_setopt($ch, CURLOPT_URL,$this->api_endpoint);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS,
            json_encode($parameters));

            // receive server response ...
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $server_output = curl_exec ($ch);
            $response_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            
            curl_close ($ch);

            // further processing ....
            if ($server_output != "") {
                if($response_code == 200){
                    $sql = "INSERT INTO log_partner_api (`partner`, `restaurant`, `booking_confirmation`, `headers`, `parameters`, `response`) VALUES ('$partner', '$this->restaurant', '$booking->confirmation', '".json_encode($header)."',  '".json_encode($parameters)."', '$server_output');";
                    pdo_exec($sql, 'dwh');
                    return true;
                }else{
                    $sql = "INSERT INTO log_partner_api_error (`partner`, `restaurant`, `booking_confirmation`, `headers`, `parameters`, `response`) VALUES ('$partner', '$this->restaurant', '$booking->confirmation', '".json_encode($header)."',  '".json_encode($parameters)."', '$server_output');";
                    pdo_exec($sql, 'dwh');
                    return true;
                }

            } 
            //else { ... }
        }
        
        
    }
 
    
    function getMadisonConsumerInfo($email){
        $sql = "SELECT * from member_madison WHERE email LIKE '$email'";
        //var_dump($sql);
        $data = pdo_single_select($sql);
        return $data;
    }
    
    function getMadisonCompanyId($email){
        $sql = "SELECT * from member_madison WHERE email LIKE '$email'";
        $data = pdo_single_select($sql);
        return $data['netsuite_id'] . ' ' . $data['firstname'] . ' ' . $data['lastname'];
    }
    
    function convertDate($date){
        $date_tmp = explode('-',$date);
        return $date_tmp[2]. '/' . $date_tmp[1]. '/' . $date_tmp[0]; 
    }

    function getLocation($booking_generic = NULL){
        $location = 'MadisonRooms Singapore';
        if(!empty($booking_generic)){
            $booking_generic = str_replace('’', '"', $booking_generic);
            $tmp_arr = json_decode($booking_generic);
            if(!empty($tmp_arr->area)){
                $location = $tmp_arr->area;
            }
        }
        return $location;
    }
    
    function getPurpose($booking_generic){
        $purpose = 'NA';
        if(!empty($booking_generic)){
            $booking_generic = str_replace('’', '"', $booking_generic);
            $tmp_arr = json_decode($booking_generic);
            if(!empty($tmp_arr->purpose)){
                $purpose = $tmp_arr->purpose;
            }
        }
        return $purpose;
    }
    
    function convertTime($time){
        $time_tmp = explode(':',$time);
        $am_pm = 'am';
        if($time_tmp[0]>12){
            $am_pm = 'pm';
            $time_tmp[0] = $time_tmp[0] - 12;
        }
        return $time_tmp[0]. ':' . $time_tmp[1]. ' ' . $am_pm; 
    }
    function getEndDate($date, $time){
        $date=date_create($date." ". $time);
        return date("d-m-Y", $date->getTimestamp() + 90 * 60);
    }
    function getEndTime($date, $time){
        $date=date_create($date." ". $time);
        return date("h:i a", $date->getTimestamp() + 90 * 60);
    }
}