<?php

require_once("lib/class.member.inc.php");

class WY_log {

   var $object; 
   var $iplong;
   var $frontend;
   var $month;
   var $count;
   var $msg;
   
	function __construct($where) {
		switch ($where) {
			case "website":
				$this->frontend = 1;
				break;
			case "api":
				$this->frontend = 2;
				break;
			case "backoffice":
				$this->frontend = 3;
				break;
			case "admin":
				$this->frontend = 4;
				break;
			case "tracking":
				$this->frontend = 5;
				break;
            case "mobile":
				$this->frontend = 6;
				break;
			}
		}
	

	function LogEvent($user_id = '', $action, $target, $object, $other, $theDate="", $source = NULL, $user_token = NULL) {
		self::insertLog($user_id, $action, $target, $object, $other, $theDate, $source, $user_token, $this->frontend);
    	}
            
    static public function insertLog($user_id, $action, $target, $object, $other, $theDate, $source, $user_token, $frontend=1) {    
    	       
		if(empty($user_id) && isset($_SESSION['user_id'])){
			$user_id = $_SESSION['user_id'];
		}

		$headers = apache_request_headers();
		if(isset($headers["X-Forwarded-For"])){
			$real_client_ip  = $headers["X-Forwarded-For"]; 
		}

		if(isset($real_client_ip) && $real_client_ip !='')
				$adresse_ip = $real_client_ip;
		else $adresse_ip = $_SERVER['REMOTE_ADDR'];

		if($adresse_ip == '::1'){
			$adresse_ip = '127.0.0.1';
		}
		$adresse_ip_tmp = explode(',',$adresse_ip);
		$adresse_ip = $adresse_ip_tmp[0];

		$iplong = ip2long($adresse_ip);
	
		if($source == NULL){
			//if(empty($_SESSION['tracking'])){
			//	$_SESSION['tracking'] = filter_input(INPUT_COOKIE, 'weelredir', FILTER_SANITIZE_STRING);
                            
			//}
			//$source = $_SESSION['tracking'];
            $source = filter_input(INPUT_COOKIE, 'weelredir', FILTER_SANITIZE_STRING);
                       
		}
                    
		if($user_token == NULL){
                  
			if(empty($_SESSION['user_token'])){
				$_SESSION['user_token'] = filter_input(INPUT_COOKIE, 'weelusertoken',FILTER_SANITIZE_STRING);
			}
			$user_token = $_SESSION['user_token'];
		}
		
		$now = (strstr($_SERVER['HTTP_HOST'], 'localhost:8888')) ? "NOW()" : "DATE_ADD(NOW(), INTERVAL 8 HOUR)";
                
		$qDate = ($theDate == "") ? $now : "'$theDate'";

		
                
// SQLSTATE Error in DB 
		$ll = array('user_id', 'theDate', 'action', 'iplong', 'target', 'object', 'other', 'frontend', 'source', 'user_token');
		foreach($ll as $vv) {
			$$vv = clean_input($$vv);
                }
			
		pdo_exec("INSERT INTO log_users(user_id, log_date, action, ip, target, object, other, front_end, source, user_token) VALUES ('$user_id', " . $now . ", '$action', '$iplong', '$target', '$object','$other', $frontend, '$source', '$user_token')",'dwh');
	}
	
	function readLog($object, $sdate, $edate) {
	
		$data = pdo_multiple_select("SELECT MONTH(log_date), COUNT(log_date) from log_users WHERE object = '$object' and log_date > '$sdate' and log_date < '$edate' GROUP BY MONTH(log_date)", "dwh");	

		foreach($data as $row) {
			$this->month[] = $row[0];
			$this->count[] = $row[1];
			}
	}
        
        function getBookingLog($booking_id){
            $member = new WY_Member();
            $logEvent = array();
            $data = pdo_multiple_select("SELECT u.user_id,u.action,u.action,u.target,u.log_date,u.other,u.source,a.description FROM log_users u,log_users_actions a WHERE u.action = a.id_action AND u.target='$booking_id' ", "dwh");
                foreach ($data as $log) {
                    
                    $datetime = strtotime( $log['log_date'] ); 
                    $log['log_date']= date(" F j, Y, g:i a",$datetime);
                    if($log['user_id'] != '0'){
                       $log['name'] =$member->getNameById($log['user_id']);
                    }else{
                       $log['name'] = 'anonymous'; 
                    }
                    $logEvent[] = $log;
                 
                }
                 return $logEvent;

        }
}

?>
