<?php
require_once("lib/class.restaurant.inc.php");

class WY_Notes {

    use CleanIng;

//    const NOWDATE = "DATE_ADD(NOW(), INTERVAL 8 HOUR)";

    var $channel;
    var $mode;
    var $app;
    var $content;
    var $result;
    var $msg;
    var $debug;

    public function __construct($channel, $app, $mode) {
       $this->channel = $this->clean_input($channel);
       $this->mode = $mode;
       $this->app = $app;
       
       $this->result = 1;
       $this->msg = "";

		$resto = new WY_restaurant;
		if($resto->checkRestaurant($this->channel) == false)
			return $this->returnError(-1, "Invalid Channel " . $this->channel, -1);

		$this->checkHistory();
    }

    public function returnError($errorno, $msg, $val) {
        $this->result = $errorno;
        $this->msg = $msg;
        return $val;
    }

    public function readNotes() {

		switch($this->app) {
			case "chat":
				$mode = ($this->mode === "T") ? date('Y-m-d') : "history";
		        $data = pdo_single_select("SELECT content FROM notes where restaurant = '$this->channel' and application = '$this->app' and mode = '$mode'  LIMIT 1");
				return (isset($data["content"])) ? $data["content"] : "";

			default:
				return $this->returnError(-1, "Invalid Application " . $this->app, -1);			
			}
		return $this->result = -1;			
		}

    public function writeNotes($content) {

		$content = $this->clean_input($content);		
		switch($this->app) {
			case "chat":
				$mode = ($this->mode === "T") ? date('Y-m-d') : "history";
				$data = pdo_single_select("SELECT content FROM notes where restaurant = '$this->channel' and application = '$this->app' and mode = '$mode' LIMIT 1");
				if(!isset($data["content"])) {
					$this->mergeHistory();
		 			$content = date('l jS \of F Y') . "|||" . $content;
					pdo_exec("INSERT INTO notes (restaurant, application, mode, content) VALUES ('$this->channel','$this->app','$mode','$content')");
					}
				else pdo_exec("UPDATE notes set content = concat(content, '|||', '$content') where restaurant = '$this->channel' and application = '$this->app' and mode = '$mode' LIMIT 1"); 
				return 1;
            	
			default:
				return $this->returnError(-1, "Invalid Application " . $this->app, -1);			
			}
		return $this->result = -1;			
		}
		
    public function checkHistory() {
		$data = pdo_single_select("SELECT ID FROM notes where restaurant = '$this->channel' and application = '$this->app' and mode = 'history' LIMIT 1");
 		if(!isset($data["ID"])) 
			pdo_exec("INSERT INTO notes (restaurant, application, mode, content) VALUES ('$this->channel', '$this->app', 'history', '')"); 		
   	}

    public function mergeHistory() {
		$data = pdo_single_select("SELECT content FROM notes where restaurant = '$this->channel' and application = '$this->app' and mode != 'history' LIMIT 1");
 		if(isset($data["content"]))  {
			$content = $data["content"];
			$today = date('Y-m-d');
			pdo_exec("UPDATE notes set content = concat(content, '$content') where restaurant = '$this->channel' and application = '$this->app' and mode = 'history' LIMIT 1"); 
			pdo_exec("delete from notes where restaurant = '$this->channel' and application = '$this->app' and mode != 'history' and mode != '$today' LIMIT 1");
			}
   	}
}

?>