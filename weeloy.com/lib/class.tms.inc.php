<?php

class WY_Tms {
    
    var $restaurant;
    var $name;
    var $date;
    var $content;
    var $result;
    var $msg;

    function __construct() {
    	$this->restaurant = $this->msg = "";
    	}
    	
    function readprofile($email) {
    
    	$cls = new WY_Cluster;
   		$this->result = -1;
    	
    	$cls->read('SLAVE', $email, 'TMS', 'TMS', '', '');
    	if($cls->result < 0 || count($cls->clustcontent) < 1 || !isset($cls->clustcontent[0]) || $cls->clustcontent[0] == "") {
     		$this->msg = "unable to find " . $email;
    		return array();
      		}
    		 
    	$profile = array();
    	$contentAr = explode(";", $cls->clustcontent[0]);
    	if(substr($contentAr[0], 0, strlen("restaurant=")) != "restaurant=") 
    		return array();
    	
    	$profile["name"] = $profile["object"] = array();
	
    	$this->restaurant = $profile["restaurant"] = $theRestaurant = substr($contentAr[0], strlen("restaurant="));

		$res = new WY_restaurant;
		$res->getRestaurant($theRestaurant);
		if($res->result <= 0) {
			$debug = new WY_debug;
			$debug->writeDebug("ERROR-BACKOFFICE", "REPORT", "Invalid Restaurant :" . $theRestaurant);
			return array();
			}
		$profile["restotitle"] = $res->title;
		$profile["twositting"] = ($res->twoSitting() != 0) ? 1 : 0;
		$profile["captain"] = ($res->tmsCaptain() != 0) ? 1 : 0;
		$slot = $res->fiveSlotMeal();
		$profile["mealduration"] = ($slot === 0) ? 3 : $slot;
		$profile["PrinterIP"] = $res->readGeneric("IPADPrinter");
		$profile["tmspos"] = ($res->checktmspos() != 0) ? 1 : 0;
		$profile["tmsV2"] = ($res->tmsV2() != 0) ? 1 : 0;
		$profile["tmsSync"] = ($res->checkTMSSync() != 0) ? 1 : 0;
		$profile["dinnertime"] = $res->getDinnerTime();
		

		$mediadata = new WY_Media($theRestaurant);
		$profile["logo"] = $mediadata->getLogo($theRestaurant);

        $data = pdo_multiple_select("select name, object from tms where restaurant = '$this->restaurant' and name != '_calendar' and name != '_block' order by name");
    	foreach($data as $row) {
    		$profile["name"][] = $row["name"];
    		$profile["object"][] = $row["object"];
        	}
        	
    	if(substr($contentAr[1], 0, strlen("topic=")) == "topic=") {
    		$profileAr = explode("|", substr($contentAr[1], strlen("topic=")));
    		foreach($profileAr as $topic)
    			$profile["section"][] = $topic;
    		}
    	if(!isset($profile["section"])) 
    		$profile["section"] = "";	// not to get slim error
    	
		$this->result = 1;
		return $profile;
    	}

	function saveLayout($json) {
		
	   	$this->result = -1;
		$obj = json_decode($json);
		$this->restaurant = $obj->restaurant;
		$this->name = $obj->layoutName;
		if($this->restaurant == "" || $this->name == "")
			return $this->result = -1;
		
		$json = preg_replace("/'|\"/", "’", $json);	
		if($this->name == "_userpreferences" || $this->name == "_userpreferences_prompt") {
			$name = $this->name;
			$accountname = $obj->email;
			pdo_exec("delete from tms where restaurant = '$this->restaurant' and name = '$this->name' and (object like '%$accountname%' or email = '$accountname') limit 1"); 
			pdo_exec("insert into tms (restaurant, name, email, object) VALUES ('$this->restaurant', '$this->name', '$accountname', '$json')"); 
			return  $this->result = 1;
			}
			
		pdo_exec("insert into tms (restaurant, name, object) VALUES ('$this->restaurant', '$this->name', '$json') ON DUPLICATE KEY UPDATE object = '$json'"); 
   		return $this->result = 1;

		}

	function deleteLayout($json) {
		
	   	$this->result = -1;	   	
		$obj = json_decode($json);
		$this->restaurant = $obj->restaurant;
		$this->name = $obj->layoutName;
		if($this->restaurant == "" || $this->name == "")
			return $this->result = -1;

		if($this->name == "_userpreferences" || $this->name == "_userpreferences_prompt")
			return $this->result = -1;
		
		pdo_exec("delete from tms where restaurant = '" . $this->restaurant . "' and name = '" . $this->name . "' limit 1"); 
   		return $this->result = 1;
		}

	function renameLayout($json) {
		
	   	$this->result = -1;
		$obj = json_decode($json);
		$this->restaurant = $restaurant = $obj->restaurant;
		$this->name = $oldname = $obj->layoutName;
		if($this->name == "_userpreferences" || $this->name == "_userpreferences_prompt")
			return $this->result = -1;

		$newname = $obj->data;
		if(isset($newname) && $newname != "")
			$newname = clean_input($newname);
		if(!isset($this->restaurant) || $this->restaurant == "" || !isset($this->name) || $this->name == "" || !isset($newname) || $newname == "")
			return $this->result = -1;
		
		$data = pdo_single_select("select name, object from tms where restaurant = '$restaurant' and name = '$oldname' limit 1");
		if(isset($data["object"])) $objstr = str_replace("’layoutName’:’$oldname", "’layoutName’:’$newname", $data["object"]);

		pdo_exec("update tms set name = '$newname', object = '$objstr' where restaurant = '$restaurant' and name = '$oldname' limit 1"); 
   		return $this->result = 1;
		}
		
	function getCalendar($restaurant) {
		$this->restaurant = $restaurant = clean_input($restaurant);
       		$data = pdo_multiple_select("select name, object from tms where restaurant = '$this->restaurant' and name = '_calendar' limit 1");
		$this->result = (count($data) > 0) ? 1 : -1;
		return $data;
		}
		
	function setCalendar($restaurant, $json) {
		$this->restaurant = $restaurant = clean_input($restaurant);
		$json = preg_replace("/'|\"/", "’", $json);	
		
		pdo_exec("delete from tms where restaurant = '$this->restaurant' and name = '_calendar' limit 1"); 
		pdo_exec("insert into tms (restaurant, name, object) VALUES ('$this->restaurant', '_calendar', '$json')"); 
		}
		
	function getBlock($restaurant) {
		$this->restaurant = $restaurant = clean_input($restaurant);
       		$data = pdo_multiple_select("select name, object from tms where restaurant = '$this->restaurant' and name = '_block' limit 1");
		$this->result = (count($data) > 0) ? 1 : -1;
		return $data;
		}
		
	function setBlock($restaurant, $json) {
		$this->restaurant = $restaurant = clean_input($restaurant);
		$json = preg_replace("/'|\"/", "’", $json);	
		
		pdo_exec("delete from tms where restaurant = '$this->restaurant' and name = '_block' limit 1"); 
		pdo_exec("insert into tms (restaurant, name, object) VALUES ('$this->restaurant', '_block', '$json')"); 
		}
		
}
