<?php

require_once('conf/conf.init.inc.php');
require_once("lib/class.spool.inc.php");


define('API_ACCESS_KEY', 'AIzaSyC-l7EvSzWQEzbIl2umc9uM8-DCmap9EUo');

class JM_Pushnotif {

    public $lang;
    protected $message;
    protected $badge;
    protected $sound;
    protected $development;

    function __construct() {
        
    }

    function sendPushnotif($recipient, $message, $pn_type, $options = NULL, $app_name = 'weeloy_pro', $type = NULL) {
        switch ($pn_type) {
            case 'ios':
                $this->initIosPushNotif($options);
                $this->sendIosNotif($recipient, $message, $app_name, $type);
                break;
            case 'android':
                $this->initAndroidPushNotif($options);
                $this->sendAndroidPushNotif($recipient, $message, $app_name, $type);
                break;
        }
    }

    function initIosPushNotif($options) {
        $this->badge = 3;
        $this->sound = 'default';
        $this->development = true;
    }

    public function sendIosNotif($recipient, $message, $app_name = 'weeloy_pro', $type = 'restaurant') {

        $deviceToken = $recipient;

        $scheme = 'prod';

//        if($this->is_test_env()){
//            $scheme = "dev";  
//        }

        if ($app_name == 'weeloy_pro') {
            if ($scheme != 'dev') {
                $certifile = '../../ressources/notifications/certificats/apn/weeloy_pro/aps_weeloypro_2016.pem';
                $trust = '../../ressources/notifications/certificats/entrust_2048_ca.cer';
                $passphrase = 'Mateo8/seal';
                $url = 'ssl://gateway.push.apple.com:2195';
            } else {
                $certifile = '../../ressources/notifications/certificats/apn/weeloy_pro/aps_dev_weeloypro_2016.pem';
                $trust = '../../ressources/notifications/certificats/entrust_2048_ca.cer';
                $passphrase = 'Mateo8/seal';
                $url = 'ssl://gateway.sandbox.push.apple.com:2195';
            }
        } else {
            if ($scheme != 'dev') {
                $certifile = '../../ressources/notifications/certificats/apn/weeloy/apn_weeloy_2016.pem';
                $trust = '../../ressources/notifications/certificats/entrust_2048_ca.cer';
                $passphrase = 'Mateo8/seal';
                $url = 'ssl://gateway.push.apple.com:2195';
            } else {
                $certifile = '../../ressources/notifications/certificats/apn/weeloy/aps_dev_weeloy_2016.pem';
                $trust = '../../ressources/notifications/certificats/entrust_2048_ca.cer';
                $passphrase = 'Mateo8/seal';
                $url = 'ssl://gateway.sandbox.push.apple.com:2195';
            }

            
        }
 
        

        // Put your alert message here:
        // $message = trim($message);
////////////////////////////////////////////////////////////////////////////////

        $ctx = stream_context_create();
        stream_context_set_option($ctx, 'ssl', 'local_cert', $certifile);
        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
        stream_context_set_option($ctx, 'ssl', 'cafile', $trust);

        //Setup stream (connect to Apple Push Server)
        // Open a connection to the APNS server
        $fp = stream_socket_client($url, $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);



        if (!$fp)
            exit("Failed to connect: $err $errstr" . PHP_EOL);

        echo 'Connected to APNS' . PHP_EOL;
 

        
        //Create the payload body
        // Create the payload body
        $body['aps'] = array(
            'alert' => $message,
            'sound' => 'default'
        );

        // Encode the payload as JSON
        $payload = json_encode($body);
        //$msg = pack("C", 1) . pack("N", $apple_identifier) . pack("N", $apple_expiry) . pack("n", 32) . pack('H*', str_replace(' ', '', $deviceToken)) . pack("n", strlen($payload)) . $payload;
        // Build the binary notification
        $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

// Send it to the server
        $result = fwrite($fp, $msg, strlen($msg));
  
        if ($type === 'admin') {
            
            stream_set_blocking($fp, 0);
            usleep(1000000); //Wait for Apple to respond.
            $result = $this->checkAppleErrorResponse($fp);
           
            if ($result !== '0-No errors encountered') {
                $error = 1;
                $this->saveAdminPushnotifyTracking('ios', $app_name, $error, $message);
            }
            
            fclose($fp);
            return $result;
            
        } else {
            if (!$result)
                echo 'Message not delivered' . PHP_EOL;
            else
                echo 'Message successfully delivered' . PHP_EOL;


            // Close the connection to the server
            fclose($fp);
        }
    }

    public function sendIosPushNotif($recipient, $message, $app_name = 'weeloypro', $title = 'New Message') {

        $deviceToken = $recipient;

        $resMessage = array();
        $scheme = 'prod';

        $this->rootdir = __SERVERROOT__ . __ROOTDIR__ . '/';
        // Put your private key's passphrase here:
        if ($app_name == 'weeloypro') {
            if ($scheme != 'dev') {
                $certifile = $this->rootdir . 'ressources/notifications/certificats/apn_weeloy_pro.pem';
                $trust = $this->rootdir . 'ressources/notifications/certificats/entrust_2048_ca.cer';
                $passphrase = 'Lmdpdw31';
                $url = 'ssl://gateway.push.apple.com:2195';
            } else {
                $certifile = '../../ressources/notifications/certificats/apn_dev_weeloy_pro.pem';
                $trust = '../../ressources/notifications/certificats/entrust_2048_ca.cer';
                $passphrase = 'Weeloy2014';
                $url = 'ssl://gateway.sandbox.push.apple.com:2195';
            }
        } else {
            if ($scheme != 'dev') {
                $certifile = $this->rootdir . 'ressources/notifications/certificats/apn/weeloy/aps_weeloy_2016.pem';
                $trust = $this->rootdir . 'ressources/notifications/certificats/entrust_2048_ca.cer';
                $passphrase = 'Mateo8/seal';
                $url = 'ssl://gateway.push.apple.com:2195';
            } else {
                $certifile = '../../ressources/notifications/certificats/apn/weeloy/aps_dev_weeloy_2016.pem';
                $trust = '../../ressources/notifications/certificats/entrust_2048_ca.cer';
                $passphrase = 'Mateo8/seal';
                $url = 'ssl://gateway.sandbox.push.apple.com:2195';
            }
            
            
        }



// Put your alert message here:
        // $message = trim($message);
////////////////////////////////////////////////////////////////////////////////

        
        $ctx = stream_context_create();
        stream_context_set_option($ctx, 'ssl', 'local_cert', $certifile);
        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
        stream_context_set_option($ctx, 'ssl', 'cafile', $trust);

        //Setup stream (connect to Apple Push Server)
// Open a connection to the APNS server
        $fp = stream_socket_client($url, $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);

        if (!$fp) {
            $resMessage['connecterror'] = "Failed to connect: $err $errstr" . PHP_EOL;
            return $resMessage;
        } else {
            $alert = array('title' => $title, 'body' => $message);
            $body['aps'] = array(
                'alert' => $alert,
                'sound' => 'default'
            );
            $payload = json_encode($body);
            // Build the binary notification
            //$msg = pack("C", 1) . pack("N", $apple_identifier) . pack("N", $apple_expiry) . pack("n", 32) . pack('H*', str_replace(' ', '', $deviceToken)) . pack("n", strlen($payload)) . $payload; 
            $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
            //$msg = chr(1) . chr($id) . chr($id) . chr($id) . chr($id) . pack('N', time() + 3600) . chr(0) . chr(32) . pack('H*', str_replace(' ', '', $deviceToken)) . chr(0) . chr(strlen($$payload)) . $$payload; 
            // Send it to the server
            $result = fwrite($fp, $msg, strlen($msg));

            stream_set_blocking($fp, 0);
            usleep(1000000); //Wait for Apple to respond.
            $result = $this->checkAppleErrorResponse($fp);
            fclose($fp);
            return $result;
        }

    }

    public function sendIospassNotify($pushToken) {
        $this->rootdir = __SERVERROOT__ . __ROOTDIR__ . '/';

        //  QUICK FIX  - MUST SEE IF WE HAVE ISSUES SOMEWHERE ELSE - email - passbook...
        if (strpos($_SERVER['HTTP_HOST'], 'api.weeloy.com') !== false) {
            //$certifile = __SERVERROOT__ . '/../ressources/passbook/certificates/passbookCertificate.pem';  
            $certifile = __SERVERROOT__ . '/../ressources/passbook/certificates/passbook_20160508.p12';
        } else {
            //$certifile =$this->rootdir.'ressources/passbook/certificates/passbookCertificate.pem'; 
            $certifile = $this->rootdir . 'ressources/passbook/certificates/AppleWWDRCA.pem';
        }

        $passphrase = 'Twpfpb@96';

        $url = 'ssl://gateway.push.apple.com:2195';

        $ctx = stream_context_create();
        $body = array();
        $body['aps'] = array();

        stream_context_set_option($ctx, 'ssl', 'local_cert', $certifile);
        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

        //stream_context_set_option($ctx, 'ssl', 'cafile', $trust);
        $fp = stream_socket_client("ssl://gateway.push.apple.com:2195", $err, $errstr, 60, STREAM_CLIENT_CONNECT, $ctx);

        stream_set_blocking($fp, 0);


// production server is ssl://feedback.push.apple.com:2196
        if (!$fp)
            exit("Failed to connect: $err $errstr" . PHP_EOL);


        $payload = json_encode($body);
        $apple_expiry = time() + (90 * 24 * 60 * 60);

        $apple_identifier = 'pass.com.weeloy';

        $msg = pack("C", 1) . pack("N", $apple_identifier) . pack("N", $apple_expiry) . pack("n", 32) . pack('H*', str_replace(' ', '', $pushToken)) . pack("n", strlen($payload)) . $payload;

        $result = fwrite($fp, $msg, strlen($msg));
        $this->checkAppleErrorResponse($fp);

        // usleep(500); //Pause for half a second. Note I tested this with up to a 5 minute pause, and the error message was still available to be retrieved

        $this->checkAppleErrorResponse($fp);

        fclose($fp);
    }

    function checkAppleErrorResponse($fp) {

        //byte1=always 8, byte2=StatusCode, bytes3,4,5,6=identifier(rowID). Should return nothing if OK.
        $apple_error_response = fread($fp, 6);
        //NOTE: Make sure you set stream_set_blocking($fp, 0) or else fread will pause your script and wait forever when there is no response to be sent.

        if ($apple_error_response) {
            //unpack the error response (first byte 'command" should always be 8)
            $error_response = unpack('Ccommand/Cstatus_code/Nidentifier', $apple_error_response);

            if ($error_response['status_code'] == '0') {
                $error_response['status_code'] = '0-No errors encountered';
            } else if ($error_response['status_code'] == '1') {
                $error_response['status_code'] = '1-Processing error';
            } else if ($error_response['status_code'] == '2') {
                $error_response['status_code'] = '2-Missing device token';
            } else if ($error_response['status_code'] == '3') {
                $error_response['status_code'] = '3-Missing topic';
            } else if ($error_response['status_code'] == '4') {
                $error_response['status_code'] = '4-Missing payload';
            } else if ($error_response['status_code'] == '5') {
                $error_response['status_code'] = '5-Invalid token size';
            } else if ($error_response['status_code'] == '6') {
                $error_response['status_code'] = '6-Invalid topic size';
            } else if ($error_response['status_code'] == '7') {
                $error_response['status_code'] = '7-Invalid payload size';
            } else if ($error_response['status_code'] == '8') {
                $error_response['status_code'] = '8-Invalid token';
            } else if ($error_response['status_code'] == '255') {
                $error_response['status_code'] = '255-None (unknown)';
            } else {
                $error_response['status_code'] = $error_response['status_code'] . '-Not listed';
            }


            return $error_response['status_code'];

        } else {
            return $error_response['status_code'] = '0-No errors encountered';
        }
        //return false;
    }

    public function checkIosNotif() {

        $this->rootdir = __SERVERROOT__ . __ROOTDIR__ . '/';
        $scheme = 'prod';
        if ($scheme != 'dev') {
            $certifile = $this->rootdir . 'ressources/notifications/certificats/apn_weeloy.pem';
            $passphrase = 'Lmdpdw31';
            $url = 'ssl://feedback.push.apple.com:2196';
        } else {
            $certifile = 'ressources/notifications/certificats/apn_dev.pem';
            $passphrase = 'Weeloy2014';
            $url = 'ssl://feedback.sandbox.push.apple.com:2196';
        }

        $ctx = stream_context_create();
        stream_context_set_option($ctx, 'ssl', 'local_cert', $certifile);
        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

        $apns = stream_socket_client($url, $errcode, $errstr, 60, STREAM_CLIENT_CONNECT, $ctx);

        if (!$apns) {
            echo "ERROR $err: $errstr\n";
            return;
        } else
            echo 'APNS FEEDBACK CONNECTION ESTABLISHED...<br/>';

        $feedback_tokens = array();
        //and read the data on the connection:
        while (!feof($apns)) {
            $data = fread($apns, 38);
            if (strlen($data)) {
                $feedback_tokens[] = unpack("N1timestamp/n1length/H*devtoken", $data);
            }
        }
        fclose($apns);

        foreach ($feedback_tokens as $feedback_token) {
            $this->deletePNDeviceByToken($feedback_token['devtoken']);
        }

        return $feedback_tokens;
    }

    function initAndroidPushNotif($options) {
        $this->badge = 3;
        $this->sound = 'default';
        $this->development = true;
    }

    public function sendAndroidPushNotif($recipient, $message, $app_name, $type = 'restaurant') {

        $registrationId = $recipient;
        if (isset($_REQUEST['device_id'])) {
            $registrationId = $_REQUEST['device_id'];
        }

        $msg = array(
            "title" => 'New Message',
            "message" => $message,
            "body" => $message
        );
        $fields = array
            (
            'to' => $registrationId,
            'data' => $msg,
        );
        $headers = array
            (
            'Authorization: key=' . API_ACCESS_KEY,
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://android.googleapis.com/gcm/send');  //https://gcm-http.googleapis.com/gcm/send
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);

        $result_array = json_decode($result);

        if ($result_array !== NULL) {
            if ($result_array->failure === 1) {
                $res = $result_array->results[0];
                if ($res->error === 'InvalidRegistration' || $res->error === 'NotRegistered') {
                    //$this->deletePNDeviceByToken($recipient);
                }
            }
        }
        curl_close($ch);
        if ($type === 'admin') {
            return $result;
        } else {
            echo $result;
        }
        //echo $result;
    }

    static public function sendPushnotifMessage($recipient, $message, $pn_type, $options, $app_name = 'weeloy', $restaurant = NULL, $booking_id = NULL) {
        $opts = $subject = NULL;
        $spool = new WY_Spool;
        $spool->register_pushnotif($pn_type, $recipient, $subject, $message, $opts, $app_name, $restaurant, $booking_id);
    }

    function deletePNDeviceByToken($token) {
        $sql = "UPDATE `mobile_devices` SET `status` = 'notregistered' WHERE `mobile_devices`.`mobile_id` = '$token';";
        try {
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $obj = $stmt->execute();
        } catch (PDOException $e) {
            echo 'error';
        }
    }

        //Push notification for marketing

    function notifyConsumer($type, $message, $env, $deviceId, $title) {
        $section = explode('_', $type);

        $totalcount = 0;
        $pushCount = array();
        $failcount = array();
        $results = array();
       
         $in_array=['philippe.benedetti@weeloy.com','vs.kala@weeloy.com','edwin.tan@weeloy.com','matthew.fam@weeloy.com']; //testing purpose only 
         $in_values = '"' . implode('","', $in_array) . '"';
    

        /**********************************************************************************************************************/
            //test Users List
        if ($env === 'test') {
             $deviceType = $section[0];
     
            $appName = $section[2] == 'weeloy' ? 'weeloy' : 'weeloy_pro';

                $sql = "SELECT * FROM mobile_devices WHERE user_id IN ($in_values) AND mobile_type ='$deviceType' AND app_name ='$appName' AND status = 'active'  order by ID DESC ";
                $data = pdo_multiple_select($sql);
           
            if ($section[0] === 'ios') {
                //$deviceId = $row['mobile_id']; 
        
                $result = $this->sendPushnotifMessage($deviceId, $message, 'ios', NULL,$appName,'admin','');
                if (count($data) > 0){
                 foreach ($data as $row) {
                    $deviceId = $row['mobile_id']; 
                    $result = $this->sendPushnotifMessage($deviceId, $message, 'ios', NULL, $appName, 'admin','');
                 }
                }else{
                     
                    $result = $this->sendPushnotifMessage($deviceId, $message, 'ios', NULL, $appName, 'admin',''); 
                     //$result = $this->sendPushnotifMessage('9bd32fbd05842191113e4e4eb182fdb0f483478cd46c11ccdd2107d916969fa2', $message, 'ios', NULL,'weeloy','admin','');
                }


            } else {
                 $result = $this->sendAndroidPushNotif($deviceId, $message, $appName, 'admin');
                 if (count($data) > 0){
                    foreach ($data as $row) {
                        $deviceId = $row['mobile_id'];
                        $result = $this->sendAndroidPushNotif($deviceId, $message, $appName, 'admin');
                    }
                 }else{
                      $result = $this->sendAndroidPushNotif($deviceId, $message, $appName, 'admin');
                 }
                $result = (array) json_decode($result);

                if (isset($result['success']) && $result['success'] === 1) {
                    $pushCount[] = '0-No errors encountered';
                   
                } else {
                    $failcount[] = '8-Invalid token';
                }
            }

            $result = array("success" => $pushCount, "fails" => $failcount);
       /**********************************************************************************************************************/
        } else {
             $deviceType = $section[0];
        
            $appName = $section[1] == 'weeloy' ? 'weeloy' : 'weeloy_pro';
            //test query
             //$sql = "SELECT * FROM `mobile_devices` WHERE `user_id` LIKE '%@weeloy.com%' AND mobile_type ='$deviceType' AND app_name ='$appName' ";
               //$sql = "SELECT * FROM mobile_devices WHERE user_id IN ($in_values) AND mobile_type ='$deviceType' AND app_name ='$appName' AND status = 'active'  order by ID DESC ";
              
            $sql = "SELECT md.ID,md.mobile_id, md.user_id, md.mobile_type FROM mobile_devices md WHERE app_name = '$appName' AND mobile_type = '$deviceType' AND md.status = 'active'   ";
            $data = pdo_multiple_select($sql);

            if (count($data) > 0) {
                $failcount = array();
                foreach ($data as $row) {
                    $deviceId = $row['mobile_id'];
                    $pn_type = $row['mobile_type'];
                    if ($pn_type === 'ios') {
                        $result = $this->sendPushnotifMessage($deviceId, $message, $pn_type, NULL,$appName,'admin','');
                    } else {
                        $result = $this->sendAndroidPushNotif($deviceId, $message, 'android', $title);
                        $result = (array) json_decode($result);
                        if (isset($result['success'])) {
                            $pushCount[] = '0-No errors encountered';
                        } else {
                            $failcount[] = '8-Invalid token';
                            $results[] = array('deviceId' => $row['ID'], 'code' => '8-Invalid token');
                        }
                    }
                   
                }
                
            }
        }
         $errorcount = count($failcount);
                //tracking error count
                $totalcount = count($data);

                $date = date("Y-m-d");
                $sql = "INSERT INTO notification_tracking(message,title,errorcount,totalcount,date,device,app_name) VALUES ('$message','$title','$errorcount','$totalcount','$date','$deviceType','$appName')";
                $notifyId = pdo_insert($sql);
                $result = array("success" => $totalcount, "fails" => 0);
                return 1;
        return 1;
    }

    function getNotificationAll() {
        $sql = "select * from notification_tracking";
        $data = pdo_multiple_select($sql);
        return $data;
    }

    function is_test_env() {

        if (strpos($_SERVER['HTTP_HOST'], 'api') !== false) {
            return false;
        }

        if (strpos($_SERVER['HTTP_HOST'], 'www') !== false) {
            return false;
        }

        if (strpos($_SERVER['HTTP_HOST'], 'prod') !== false) {
            return false;
        }

        if (strpos($_SERVER['HTTP_HOST'], 'codersvn') !== false) {
            return false;
        }

        return true;
    }

    function sendnotifyMultiple($email, $restaurant, $confirmation, $notif_message, $type = 'member') {
        if ($type === 'restaurant') {
            // $sql = "SELECT md.mobile_id, md.user_id, md.mobile_type FROM restaurant_app_managers ram, mobile_devices md WHERE app_name = 'weeloy_pro' AND md.status = 'active' AND ram.user_id = md.user_id AND `restaurant_id` LIKE '" . $restaurant . "'";
            $sql = "SELECT md.mobile_id, md.user_id, md.mobile_type FROM restaurants_managers ram, mobile_devices md WHERE app_name = 'weeloy_pro' AND md.status = 'active' AND ram.member_id = md.user_id AND `restaurant_id` LIKE '" . $restaurant . "'";
        } else {
            $sql = "SELECT md.mobile_id, md.user_id, md.mobile_type FROM mobile_devices md WHERE app_name = 'weeloy' AND md.status = 'active' AND  md.user_id = '$email'";
        }

        $data = pdo_multiple_select($sql);


        $data = pdo_multiple_select($sql);
        foreach ($data as $row) {
            $recipient = $row['mobile_id'];
            $pn_type = $row['mobile_type'];
            $this->sendPushnotifMessage($recipient, $notif_message, $pn_type, null, 'weeloy', $restaurant, $confirmation); // null for options ?
        }
    }

    function getpushNotifyMessage($type = 'bknew', $booking, $date) {
        $notif_message = "";
        if ($type === 'bknew') {
            $notif_message = "New booking: " . $booking->restaurantinfo->title . " " . $booking->cover . 'pp - ' . $date;
        }
        if ($type === 'bknewres') {
            $notif_message = "New booking: " . $date . " " . $booking->cover . 'pp - ' . $booking->restaurantinfo->title;
        }
        if ($type === 'cancel' || $type === 'cancel-restaurant') {
            $notif_message = "Your booking at : " . $booking->restaurantinfo->title . "has been cancelled";
        }

        return $notif_message;
    }

    function saveAdminPushnotifyTracking($device_type, $appname, $error, $message) {
        $date = date("Y-m-d");
        $sql = "UPDATE notification_tracking SET errorcount = errorcount + 1 WHERE device = '$device_type' AND date='$date' AND app_name='$appname' AND message like '%$message%' limit 1";
        pdo_exec($sql);
    }

}
