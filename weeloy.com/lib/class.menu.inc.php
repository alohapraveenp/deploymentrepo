<?php
require_once("lib/class.restaurant.inc.php");

define("SPICY1", 1);
define("SPICY2", 2);
define("SPICY3", 4);
define("VEGI", 8);
define("CHEFRECO", 16);
define("HALAL", 32);
define("TAKEOUT", 64);

class WY_Menu {

    use CleanIng;

    const MENUFLG = "IF(`extraflag`&1>0,1,0) as spicy1,
					IF(`extraflag`&2>0,1,0) as spicy2, 
					IF(`extraflag`&4>0,1,0) as spicy3, 
					IF(`extraflag`&8>0,1,0) as vegi, 
					IF(`extraflag`&16>0,1,0) as chef_reco,
					IF(`extraflag`&32>0,1,0) as halal,
					IF(`extraflag`&64>0,1,0) as takeout";
					
    const CATEGORYFLG = "IF(`typeflag`&1>0,0,1) as is_public,
                         IF(`typeflag`&2>0,1,0) as is_drink";

    private $ID;
    private $restaurant;
    private $value;
    private $type;
    private $morder;
    private $price;
    private $mimage;
    private $currency;
    private $extraflag;
    private $again;

    public function __construct($theRestaurant) {
        $this->restaurant = $theRestaurant;
    }

    private function retval($result, $msg) {
        $this->msg = $msg;
        return $this->result = $result;
    }

    private function getUniqueCode() {

        for ($i = 0; $i < 100; $i++) {
            $uniqueID = rand(10000000, 100000000 - 1);
            $data = pdo_single_select("SELECT menuID FROM menu WHERE restaurant ='$this->restaurant' and menuID = '$uniqueID' limit 1");
            if (count($data) <= 0)
                return $uniqueID;
        }
        return false;
    }

    private function getextraFlag($valAr) {
        $labelAr = array('spicy1' => SPICY1, 'spicy2' => SPICY2, 'spicy3' => SPICY3, 'vegi' => VEGI, 'chef_reco' => CHEFRECO, 'halal' => HALAL, 'takeout' => TAKEOUT);
        $extraflag = 0;
        reset($labelAr);
        foreach ($labelAr as $key => $value) {
            if (!empty($valAr[$key]) && $valAr[$key] == '1')
                $extraflag |= $value;
        }
        return $extraflag;
    }

    public function getMenuCategories($privacy = 'public') {

        $flg = self::CATEGORYFLG;
        $sql = "SELECT * FROM menu_categorie, $flg WHERE restaurant ='$this->restaurant' AND status = 'active' ORDER BY type ASC, morder ASC";


        error_log($sql);


        return pdo_multiple_select($sql);
    }

    public function getMenus() {
        $flg = self::MENUFLG;
        return pdo_multiple_select("SELECT *, $flg FROM menu WHERE restaurant ='$this->restaurant' AND status = 'active' ORDER BY type ASC, morder ASC");
    }

    function getCategorieList($privacy = 'public' ) {
        $where = '';
        if($privacy == 'public'){
            $where .= ' AND typeflag&1 = 0';
        }
        
        $flg = self::CATEGORYFLG;        
        $sql = "SELECT *, $flg FROM  menu_categorie mc WHERE mc.restaurant ='$this->restaurant' AND mc.status = 'active' $where ORDER BY morder ASC";
        return pdo_multiple_select($sql);
    }
    
    

    function getCategorieItems($categorie_id, $option = NULL) {
        $where = '';
        if (!empty($option) && $option == 'catering_only') {
            $where .= " AND `extraflag`&64>0 ";
        }
        $flg = self::MENUFLG;
        return pdo_multiple_select("SELECT m.*, $flg FROM menu m  WHERE m.menu_categorie_id = '$categorie_id' $where ORDER BY type ASC, morder ASC");
    }

    function getMenuList() {
        return pdo_multiple_select("SELECT m.*, mc.value as categorie FROM menu m, menu_categorie mc WHERE mc.id = m.menu_categorie_id AND m.restaurant ='$this->restaurant' AND m.status = 'active' AND mc.status = 'active' ORDER BY type ASC, morder ASC");
    }

    public function createMenu($title, $morder, $description, $items, $currency = 'SGD') {

        $menuID = $this->getUniqueCode();

        $arg = array('title', 'morder', 'description', 'items', 'currency');
        for ($i = 0; $i < count($arg); $i++) {
            $tt = $arg[$i];
            $$tt = $this->clean_input($$tt);
        }

        $sql = "INSERT INTO menu_categorie (restaurant, menuID, value, description, type, morder, price, currency, extraflag, typeflag, status) VALUES ('$this->restaurant', '$menuID', '$title', '$description', 'categorie', '$morder', '', '', '', 0, 'active');";
        $categorie_id = pdo_insert($sql);

        $data = $categorie_id;

        if (count($items) > 0) {
            foreach ($items as $item) {

                $id_item = $this->clean_number($item['ID']);
                $item_title = $this->clean_input($item['item_title']);
                $item_description = $this->clean_input($item['item_description']);
                $morder = $this->clean_number($item['morder']);
                $price = $this->clean_number($item['price']);
                $mimage = $this->clean_input($item['mimage']);

                if (empty($item['item_title']) || empty($item['item_description']))
                    continue;

                $extraflag = $this->getextraFlag($item);

				$sql = "INSERT INTO menu (restaurant, menuID, item_title, item_description, type, morder, price, currency, mimage, extraflag, typeflag, status, menu_categorie_id) VALUES ('$this->restaurant', '$menuID', '$item_title', '$item_description', 'item', '$morder', '$price', '$currency', '$mimage', $extraflag, 0, 'active', '$categorie_id');"; 
				$item_realid = pdo_insert($sql);
				$data .= "|" . $id_item . "=" . $item_realid;
            	}
        }
        return retval(1, $data, "NEW");
    }

    public function updateMenu($id, $title, $description, $morder, $items) {

        $data = $sep = "";
		$arg = array('id', 'title', 'description', 'morder');
		for($i = 0; $i < count($arg); $i++) { $tt = $arg[$i]; $$tt = $this->clean_input($$tt); }
        
        $sql = "INSERT INTO menu_categorie (ID, restaurant, value, description, type, morder, typeflag, status) VALUES ('$id', '$this->restaurant', '$title', '$description', 'categorie', '$morder', 0, 'active') "
                . "ON DUPLICATE KEY UPDATE id=LAST_INSERT_ID(id), value = '$title', description = '$description', morder = '$morder', currency = 'SGD', extraflag = '1111', typeflag = 0, status = 'active' ";

        $categorie_id = pdo_insert($sql);

        if (count($items) > 0) {
            foreach ($items as $item) {

				$id_item = $this->clean_number($item['ID']);                               
				$item_title = $this->clean_input($item['item_title']);
				$item_description = $this->clean_input($item['item_description']);
				$morder = $this->clean_number($item['morder']);
				$price = $this->clean_number($item['price']);
				$mimage = $this->clean_input($item['mimage']);

				if(empty($item['item_title'])) {
					error_log("ID ITEM -" . "delete from menu where ID = '$id_item' and restaurant = '$this->restaurant' and menu_categorie_id = '$categorie_id' limit 1");
					if($id_item != '')
						pdo_exec("delete from menu where ID = '$id_item' and restaurant = '$this->restaurant' and menu_categorie_id = '$categorie_id' limit 1");
					continue;
					}
	
               $extraflag = $this->getextraFlag($item);

				$id = (intval($id_item) > 0) ? $id_item : "";
				$sql = "INSERT INTO menu (ID, restaurant, item_title, item_description, type, morder, price, currency, mimage, extraflag, typeflag, status, menu_categorie_id) VALUES ( '$id' , '$this->restaurant', '$item_title', '$item_description', 'item', '$morder', '$price', 'SGD', '$mimage', '$extraflag', 0, 'active', '$categorie_id')  
					   ON DUPLICATE KEY UPDATE id=LAST_INSERT_ID(id), item_title = '$item_title', item_description = '$item_description', morder = '$morder', price = '$price', currency ='SGD', mimage = '$mimage', extraflag = '$extraflag', typeflag = 0, status = 'active' "; 

				$item_realid = pdo_insert($sql);
				
				if(intval($id_item) < 0) {
					$data .= $sep . $id_item . "=" . $item_realid;
					$sep = "|";
					}
				}	
            }
        return $this->retval(1, $data, "UPDATE");
    }

    public function deleteMenu($ID) {
        $sql = "DELETE FROM menu WHERE menu_categorie_id = '$ID'";
        pdo_exec($sql);

        $sql = "DELETE FROM menu_categorie WHERE id = '$ID'";
        pdo_exec($sql);
        return 1;
    }

    public function cleanfullMenus() {
        $data = pdo_multiple_select("SELECT distinct restaurant FROM menu_categorie where menuID = '' order by restaurant limit 10");
        foreach ($data as $row) {
            $this->restaurant = $row["restaurant"];
            echo $this->restaurant . "<br />";
            $this->getfullMenus();
        }
        return "DONE";
    }

    // for duplication and backward compatibility
    public static function resetfullMenusID($restaurant) {
        $cmds["menus"] = pdo_multiple_select("SELECT * FROM menu_categorie WHERE restaurant ='$restaurant'");

        foreach ($cmds["menus"] as $row)
            if (!empty($row['menuID'])) {
                $master_id = $row['ID'];
                $menuID = $row['menuID'];
                pdo_exec("update menu set menu_categorie_id='$master_id' where restaurant='$restaurant' and menuID='$menuID'");
            }
    }

    public function getfullMenus() {

        $this->msg = "";
        $this->result = 1;

        $flg = self::MENUFLG;
        $cmds["restaurant"] = $this->restaurant;
        $cmds["currency"] = WY_restaurant::getCurrency($this->restaurant);
        $cmds["menus"] = pdo_multiple_select("SELECT * FROM menu_categorie WHERE restaurant ='$this->restaurant'");
        $cmds["menus_items"] = pdo_multiple_select("SELECT *, $flg FROM menu WHERE restaurant ='$this->restaurant' order by morder");

        $again = 0;
        foreach ($cmds["menus"] as $row)
            if (empty($row['menuID']) || intval($row['menuID']) == 0) {
                $menuID = $this->getUniqueCode();
                $master_id = $row['ID'];
                pdo_exec("update menu set menuID='$menuID' where restaurant='$this->restaurant' and menu_categorie_id='$master_id'");
                pdo_exec("SET @i:=0; update menu set itemID = (@i:=@i+1) where restaurant = '$this->restaurant'");
                $dd = pdo_single_select("SELECT MAX(itemID) as max FROM menu WHERE restaurant ='$this->restaurant' and menu_categorie_id='$master_id'");
                $itemindex = intval($dd["max"] + 1);
                pdo_exec("update menu_categorie set menuID='$menuID', itemindex='$itemindex' where restaurant='$this->restaurant' and ID ='$master_id' limit 1");
                $again++;
            }

        if ($again > 0) { // update only once
            $cmds["menus"] = pdo_multiple_select("SELECT * FROM menu_categorie WHERE restaurant ='$this->restaurant'");
            $cmds["menus_items"] = pdo_multiple_select("SELECT * FROM menu WHERE restaurant ='$this->restaurant' order by morder");
            WY_debug::recordDebug("ERROR-MENU", "API-MENU", "Updateting Menu ID $menu_categorie_id with $menuID");
        }

        return $cmds;
    }

    public function createfullMenus($cmds) {

        $menus = $cmds["menus"];
        $menus_items = $cmds["menus_items"];

        $menuID = preg_replace("/[^a-zA-Z0-9]/", "", $menus["menuID"]);
        if (strlen($menuID) < 7)
            return $this->retval(-1, "invalid ID " . $menuID);

        $data = pdo_single_select("SELECT menuID FROM menu_categorie WHERE restaurant ='$this->restaurant' and menuID = '$menuID' limit 1");
        if(count($data) > 0) {
       		$menuID = $this->getUniqueCode();
        	if($menuID == false)
        		return $this->retval(-1,"unable to create a unique ID "); 
        	}
        		
        $content = array('value', 'description', 'type', 'itemindex', 'morder', 'price', 'currency', 'extraflag', 'typeflag', 'status');
		for($i = 0; $i < count($content); $i++) {
			$tt = $content[$i];
			$$tt = $this->clean_input($menus[$tt]);
			}
		
		if($status != "new") {
			$this->msg = "Invalid status";
			return $this->result = 1;
			}
			
        $sql = "INSERT INTO menu_categorie (restaurant, menuID, value, description, type, itemindex, morder, price, currency, extraflag, typeflag, status) VALUES ("
        	.  "'$this->restaurant', '$menuID', '$value', '$description', '$type', '$itemindex', '$morder', '$price', '$currency', '$extraflag', '$typeflag', 'active')";

        $master_id = pdo_insert($sql);

        $content = array('itemID', 'item_title', 'item_description', 'type', 'morder', 'price', 'currency', 'mimage', 'status', 'spicy1', 'spicy2', 'spicy3', 'vegi', 'chef_reco', 'halal', 'takeout');
		foreach ($menus_items as $row) {
			for($i = 0; $i < count($content); $i++) {
				$tt = $content[$i];
				$$tt = $this->clean_input($row[$tt]);
				}
			if($status == "delete") continue;
			$extraflag = $this->getextraFlag($row);
			
			pdo_exec("INSERT INTO menu (restaurant, menuID, itemID, item_title, item_description, type, morder, price, currency, mimage, extraflag, typeflag, status, menu_categorie_id) VALUES ('$this->restaurant','$menuID','$itemID', '$item_title', '$item_description', '$type', '$morder', '$price', '$currency', '$mimage', '$extraflag', '$typeflag', 'active', '$master_id')");
			}
		
		$this->msg = $menuID;	
		return $this->result = 1;
    }

    public function updatefullMenus($cmds) {

        $menus = $cmds["menus"];
        $menus_items = $cmds["menus_items"];

        //error_log('Update Menu ' . print_r($menus, true));

        $menuID = preg_replace("/[^a-zA-Z0-9]/", "", $menus["menuID"]);
        if (strlen($menuID) < 7)
            return $this->retval(-1, "invalid ID " . $menuID);

        $data = pdo_single_select("SELECT ID, menuID FROM menu_categorie WHERE restaurant ='$this->restaurant' and menuID = '$menuID' limit 1");
        if (count($data) <= 0)
            return $this->retval(-1, "ID does not exists " . $menuID);

        $master_id = $data["ID"];

        $content = array('value', 'description', 'type', 'itemindex', 'morder', 'price', 'currency', 'extraflag', 'typeflag', 'status');
		for($i = 0; $i < count($content); $i++) {
			$tt = $content[$i];
			$$tt = $this->clean_input($menus[$tt]);
			}

        pdo_exec("update menu_categorie set value='$value', description='$description', type='$type', itemindex='$itemindex', morder='$morder', price='$price', currency='$currency', extraflag='$extraflag', typeflag='$typeflag', status='active' where restaurant='$this->restaurant' and menuID='$menuID' limit 1");

        $content = array('itemID', 'item_title', 'item_description', 'type', 'morder', 'price', 'currency', 'mimage', 'status', 'spicy1', 'spicy2', 'spicy3', 'vegi', 'chef_reco', 'halal', 'takeout');
		foreach ($menus_items as $row) {
			for($i = 0; $i < count($content); $i++) {
				$tt = $content[$i];
				if(!isset($row[$tt])) { error_log('UNSET -' . $tt . '-'); $row[$tt] = ""; }
				$$tt = $this->clean_input($row[$tt]);
				}
			$extraflag = $this->getextraFlag($row);

			$action = $status;
			$status = "";
			if($action == "new")
				pdo_exec("INSERT INTO menu (restaurant, menuID, itemID, item_title, item_description, type, morder, price, currency, mimage, extraflag, typeflag, status, menu_categorie_id) VALUES ('$this->restaurant','$menuID','$itemID', '$item_title', '$item_description', '$type', '$morder', '$price', '$currency', '$mimage', '$extraflag', '$typeflag', 'active', '$master_id')");
			else if($action == "delete")
		        pdo_exec("DELETE FROM menu WHERE menuID = '$menuID' and itemID = '$itemID' and restaurant ='$this->restaurant' limit 1");
			else pdo_exec("update menu set item_title='$item_title', item_description='$item_description', type='$type', morder='$morder', price='$price', currency='$currency', mimage='$mimage', extraflag='$extraflag', typeflag='$typeflag', status='active' where restaurant='$this->restaurant' and menuID='$menuID' and itemID='$itemID' limit 1");
 			
			}
		$this->msg = "";
		return $this->result = 1;
   }
   public function createglobalmenu($email,$cmds){
            $res = new WY_restaurant;
            $menus = $cmds["menus"];
            $defaultRes = $res->getListAccountRestaurant($email);
            if(count($defaultRes) > 0) {
                foreach ($defaultRes as $rest) {
                     $this->restaurant = $rest['restaurant'];
                     $getRest = $res->getRestaurant($this->restaurant);
                    if($res->takeOutRestaurant()){
                      $this->createfullMenus($cmds) ;
                    }
                    
                }
            }else{
                return $this->retval(-1,"unable to create a group menu "); 
            }
       
   }
   public function updateGlobalmenu($email,$cmds){
            $res = new WY_restaurant;
            $defaultRes = $res->getListAccountRestaurant($email);
            if(count($defaultRes) > 0) {
                foreach ($defaultRes as $rest) {
                    $res->getRestaurant($rest['restaurant']);
                    if($res->takeOutRestaurant()){
                      $this->restaurant = $rest['restaurant'];
                      error_log("RESTAURANT " .$this->restaurant);
                      $this->updatefullMenus($cmds);
                    }
//                     $this->restaurant = $res['restaurant'];
//                    $this->updatefullMenus($cmds);
                }
           } else{
               return $this->retval(-1,"unable to update  a group menu "); 
           }   
   }


    public function deletefullMenus($menuID) {
        pdo_exec("DELETE FROM menu_categorie WHERE menuID = '$menuID' and restaurant ='$this->restaurant' limit 1");
        pdo_exec("DELETE FROM menu WHERE menuID = '$menuID' and  restaurant ='$this->restaurant'");

        $this->msg = "";
        return $this->result = 1;
    }

}

?>
