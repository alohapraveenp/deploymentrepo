<?php
require_once("lib/class.restaurant.inc.php");
require_once ("lib/class.booking.inc.php");
require_once ("lib/class.notification.inc.php");
require_once("lib/class.analytics.inc.php");

class WY_Review {
   use CleanIng;

    var $dirname;
    var $showdirname;
    var $confirmation;
    var $user_id;
    var $restaurant;
    var $reviewgrade;
    var $foodgrade;
    var $ambiancegrade;
    var $servicegrade;
    var $pricegrade;
    var $comment;
    var $reviewdtvisite;
    var $reviewdtcreate;
    var $response_to;
    var $data;
    var $restaurant_title;
    var $msg;
    var $result;
    var $reviews;
    var $source;
    var $id;
    var $review_status;

    function __construct($theRestaurant = NULL) {
        $this->restaurant = $theRestaurant;
        $this->dirname = __UPLOADDIR__ . "$theRestaurant/";
        $this->showdirname = __SHOWDIR__ . "$theRestaurant/";
    }

    function postReview($confirmation, $user_id, $restaurant_id, $review_grade = 0,$food_rate, $ambiance_rate, $service_rate, $price_rate, $comment, $response_to = NULL, $user_agent = NULL) {

		$arg = array('confirmation', 'user_id', 'restaurant_id', 'food_rate', 'ambiance_rate', 'service_rate', 'price_rate', 'comment', 'response_to', 'user_agent');
		for($i = 0; $i < count($arg); $i++) { 
			$tt = $arg[$i]; 
			$$tt = $this->clean_input($$tt); 
			}

        $data = pdo_single_select("SELECT confirmation FROM review WHERE confirmation = '$confirmation' limit 1");
        if(count($data) > 0)
        	return true;
        if($review_grade == 0){
            $review_grade = ($food_rate + $ambiance_rate + $service_rate + $price_rate) / 4;
        }
        $sql = "INSERT INTO  review (confirmation, user_id, restaurant, reviewgrade, foodgrade, ambiancegrade, servicegrade, pricegrade, comment, reviewdtvisite, reviewdtcreate, response_to, user_agent) 
                                VALUES ('$confirmation', '$user_id', '$restaurant_id', '$review_grade', '$food_rate', '$ambiance_rate', '$service_rate', '$price_rate', '$comment', NOW(), NOW(), '$response_to', '$user_agent')";
        $userheader="";
        if (pdo_exec($sql)) {
            
            $sql = "UPDATE booking SET review_status = 'posted' WHERE confirmation = '$confirmation'";
            pdo_exec($sql);
            
            $source= explode( '/', $user_agent );
            $notification = new WY_Notification();
            $this->getReview($confirmation);
            $this->source = $source[0];
            
            //notifi post review 
            //if($source[0]==='WeeloyPro'){
            $booking = new WY_Booking();
            $booking->getBooking($confirmation);
            $booking->review =$this; 
            $booking->reviewmode ='member';
            if($source[0] === 'WeeloyPro'){
                $booking->reviewmode = 'weeloypro';
            }
                
            $logger = new WY_log("website");
            $loguserid = (isset($_SESSION['user']['user_id'])) ? $_SESSION['user']['user_id'] : "0"; 
            $logger->LogEvent($loguserid, 711, $confirmation, 'website', '', date("Y-m-d H:i:s"));
               $notification->notify($booking,'review');

            return true;
        }
        return false;
    }
    function postReviewGrade($confirmation, $user_id, $restaurant_id, $grade, $comment, $type,$response_to = NULL, $user_agent = NULL) {

		$arg = array('confirmation', 'user_id', 'restaurant_id', 'grade', 'comment', 'response_to', 'user_agent');
		for($i = 0; $i < count($arg); $i++) { 
			$tt = $arg[$i]; 
			$$tt = $this->clean_input($$tt); 
			}

        $data = pdo_single_select("SELECT confirmation FROM review WHERE confirmation = '$confirmation' limit 1");
        //if(count($data) > 0)
        	//return true;
 
        //$review_grade = ($food_rate + $ambiance_rate + $service_rate + $price_rate) / 4;
        $review_grade= $grade;
        $sql = "INSERT INTO review (confirmation, user_id, restaurant, reviewgrade, comment, reviewdtvisite, reviewdtcreate, response_to, user_agent) 
                                VALUES ('$confirmation', '$user_id', '$restaurant_id', '$review_grade', '$comment', NOW(), NOW(), '$response_to', '$user_agent') ON DUPLICATE KEY UPDATE reviewgrade='$review_grade',comment ='$comment';";

        $userheader="";
        $id = pdo_insert($sql);
        if (isset($id)) {
            if($type === 'update'){
                $sql = "UPDATE booking SET review_status = 'posted' WHERE confirmation = '$confirmation'";
                pdo_exec($sql);
            }
            
            $source= explode( '/', $user_agent );
                $notification = new WY_Notification();
               $this->getReview($confirmation);
            //notifi post review 
            
                $booking = new WY_Booking();
                $booking->getBooking($confirmation);
                $booking->review = $this;
                $booking->reviewmode ='member';
                if($source[0] === 'WeeloyPro'){
                    $booking->reviewmode = 'weeloypro';
                }
                //$data_object = array('booking'=>$booking, 'review'=>$this);
                $notification->notify($booking,'review');
                $logger = new WY_log("website");
                $loguserid = (isset($_SESSION['user']['user_id'])) ? $_SESSION['user']['user_id'] : "0"; 
                $logger->LogEvent($loguserid, 711, $confirmation, 'website', '', date("Y-m-d H:i:s"));
           
            return $id;
        }
        return false;
    }

    function saveResponse($confirmation, $response) {

		$this->result = 1;
		$this->msg = "";
        $theRestaurant = $this->restaurant;
        $confirmation = $this->clean_input($confirmation);
        $response = $this->clean_input($response);
        $Sql = "Update review set response_to='$response' where restaurant = '$theRestaurant' and confirmation = '$confirmation' limit 1";
        pdo_exec($Sql);
        return $this->result;
    }

    function getReview($confirmation_id, $response_to = NULL,$type="") {

        $confirmation_id = $this->clean_input($confirmation_id);
        $response_to = $this->clean_input($response_to);
        $type = $this->clean_input($type);

		$this->confirmation = "";		
        $where = '';
        if (isset($response_to) && $response_to != '') {
            $where = " AND response_to='" . $response_to . "'";
        	}
        	
        $sql = "SELECT * FROM  review  WHERE confirmation LIKE '" . $confirmation_id . "'" . $where;
        $row = pdo_single_select($sql);

		if(count($row) <= 0) {
			error_log("REVIEW " . $confirmation_id . " " . count($row));
			return;
			}
        $sql = "SELECT review_status FROM  booking  WHERE confirmation LIKE '" . $confirmation_id . "'";
        $res_review = pdo_single_select($sql);
  
	$this->id =$row['ID'];
        $this->confirmation = $row['confirmation'];
        $this->user_id = $row['user_id'];
        $this->restaurant = $row['restaurant'];
        $this->reviewgrade = $row['reviewgrade'];
        $this->foodgrade = $row['foodgrade'];
        $this->ambiancegrade = $row['ambiancegrade'];
        $this->servicegrade = $row['servicegrade'];
        $this->pricegrade = $row['pricegrade'];
        $this->comment = $row['comment'];
        $this->reviewdtvisite = $row['reviewdtvisite'];
        $this->reviewdtcreate = $row['reviewdtcreate'];
        $this->response_to = $row['response_to'];
        $this->review_status = $res_review['review_status'];
        
        if($type == 'myreview'){
           return $row; 
        }
    }
    function getLatestview(){
        $sql = "SELECT * FROM  review WHERE confirmation!='' ORDER BY RAND() DESC limit 1 ";
        $row = pdo_single_select($sql); 
        
        $this->confirmation = $row['confirmation'];
        $this->user_id = $row['user_id'];
        $this->restaurant = $row['restaurant'];
        $this->reviewgrade = $row['reviewgrade'];
        $this->foodgrade = $row['foodgrade'];
        $this->ambiancegrade = $row['ambiancegrade'];
        $this->servicegrade = $row['servicegrade'];
        $this->pricegrade = $row['pricegrade'];
        $this->comment = $row['comment'];
        $this->reviewdtvisite = $row['reviewdtvisite'];
        $this->reviewdtcreate = $row['reviewdtcreate'];
        $this->response_to = $row['response_to'];
 
   
    }

    function getReviews() {
        $theRestaurant = $this->restaurant;
        $this->data = pdo_multiple_select("SELECT confirmation, user_id, restaurant, reviewgrade, foodgrade, ambiancegrade, servicegrade, pricegrade, comment, reviewdtvisite, reviewdtcreate, reviewdtcreate, response_to FROM review WHERE restaurant ='$theRestaurant' ORDER by reviewdtvisite DESC");
    }

    function getReviewsList($limit = '') {
        $limit = $this->clean_input($limit);
        $theRestaurant = $this->restaurant;
        if($limit != ''){
          $where = "ORDER BY review.reviewgrade DESC $limit";
        }else{
           $where = "ORDER BY review.reviewgrade DESC"; 
        }

        $sql = "SELECT reviewgrade as score, foodgrade as food_score, ambiancegrade as ambiance_scrore, servicegrade as service_score, pricegrade as price_score, comment, reviewdtvisite as booking_date, reviewdtcreate as post_date, firstname as user_name, email FROM review, booking WHERE review.confirmation = booking.confirmation AND review.restaurant ='$theRestaurant' AND review.reviewgrade >2 $where";

         //$sql = "SELECT r.reviewgrade as score, r.comment, r.reviewdtvisite as booking_date, r.reviewdtcreate as post_date, m.firstname as user_name, r.user_id as email FROM ( SELECT reviewgrade,comment,reviewdtvisite,reviewdtcreate,user_id FROM review WHERE restaurant ='$theRestaurant' AND reviewgrade >2) AS r LEFT JOIN ( SELECT firstname,email FROM member ) AS m ON r.user_id = m.email ORDER BY r.reviewgrade DESC";
        $this->data = pdo_multiple_select($sql);
    }    
    
    function getReviewsCount() {
        $theRestaurant = $this->restaurant;

        $sql = "SELECT COUNT(ID) as count, AVG(reviewgrade) as score_avg FROM review WHERE restaurant = '$theRestaurant' AND reviewgrade >2";
        $this->data = pdo_single_select($sql);
        return array('count'=>  $this->data['count'], 'score'=>  round($this->data['score_avg']), 'review_desc'=> $this->getReviewDesc(round($this->data['score_avg'])));
    }
    
    private function getReviewDesc($score){
        if($score > 4.49){
            return 'Excellent';
        }elseif ($score > 3.74) {
            return 'Very Good';
        }elseif ($score > 2.99) {
            return 'Good';
        }else{
            return 'Average';
        }
        return 'Good';
    }

	function getpartialUserReview($limit) {
	
		if($limit < 0 || $limit > 20)
			$limit = 3;
        $data = pdo_multiple_select("SELECT comment as reviewdesc, reviewdtvisite as reviewdtvisite, reviewdtcreate as reviewdtcreate, firstname as reviewguest,reviewgrade as reviewgrade FROM review, member WHERE review.user_id = member.email AND restaurant = '$this->restaurant'  ORDER by reviewdtvisite DESC LIMIT $limit");
        return (count($data) > 0) ? $data : array(0);
		}
		            
    function getUserReviews($email, $options) {

        $email = $this->clean_input($email);
        $options = $this->clean_input($options['include_reviews']);

        $where = '';
        $order = '';
        $limit = '';

        ////  FILTER CREATION  ////
        if (isset($options['last'])) {
            $order .= ' reviewdtcreate DESC';
            $limit .= $options['last'];
        }

        if (isset($order) && $order != "") {
            $order = ' ORDER BY ' . $order;
        }
        if (isset($limit) && $limit != "") {
            $limit = ' LIMIT ' . $limit;
        }
        //// END FILTER CREATION  ////


        $data = pdo_multiple_select("SELECT confirmation, user_id,r.id as reviewid, r.restaurant, restau.title as title, reviewgrade, foodgrade, ambiancegrade, servicegrade, pricegrade, comment, reviewdtvisite, reviewdtcreate, reviewdtcreate, response_to FROM review r, restaurant restau WHERE 1 AND restau.restaurant = r.restaurant AND r.user_id = '$email' $where $order $limit");
        if (count($data) <= 0)
            return false;
            
        foreach ($data as $row) {
            $current = new WY_Review();
            $current->id =$row['reviewid'];
            $current->confirmation = $row['confirmation'];
            $current->user_id = $row['user_id'];
            $current->restaurant = $row['restaurant'];
            $current->reviewgrade = $row['reviewgrade'];
            $current->foodgrade = $row['foodgrade'];
            $current->ambiancegrade = $row['ambiancegrade'];
            $current->servicegrade = $row['servicegrade'];
            $current->pricegrade = $row['pricegrade'];
            $current->comment = $row['comment'];
            $current->reviewdtvisite = $row['reviewdtvisite'];
            $current->reviewdtcreate = $row['reviewdtcreate'];
            $current->response_to = $row['response_to'];

            $current->rdate = date('Y-m-d', strtotime($current->reviewdtvisite));
            $current->rtime = date('H:i:s', strtotime($current->reviewdtvisite));

            $current->restaurant_title = $row['title'];
            //kala added this line for retaurant path
            $resto = new WY_restaurant();
            $resto->getRestaurant($row['restaurant']);
            $current->internal_path = $resto->getRestaurantInternalPath($row['restaurant']);
            $current->review_desc=$this->getReviewDesc($row['reviewgrade']);

            $this->reviews[] = $current;
        }

        return true;
    }

//    function notifyPostReview($confirmation_id, $response_to = NULL) {
//        $notification = new WY_Notification();
//        $confirmation_id = $this->clean_input($confirmation_id);
//        $response_to = $this->clean_input($response_to);
//        $mailer = new JM_Mail;
//
//        $booking = new WY_Booking();
//        $booking->getBooking($confirmation_id);
//
//        if(!isset($booking->restaurant) || $booking->restaurant == ""){
//            return false;
//        }
//        
//        $this->getReview($confirmation_id, $response_to);
//        // Notification Member
//        $rtime = preg_replace("/:00/", "h", $booking->rtime);
//        $rdate = date("F j, Y, g:i a", mktime(substr($booking->rtime, 0, 2), substr($booking->rtime, 3, 4), 0, intval(substr($booking->rdate, 5, 2)), intval(substr($booking->rdate, 8, 2)), intval(substr($booking->rdate, 0, 4))));
//        $booking->rtime = $rtime;
//        $booking->rdate = $rdate;
//        $emailEle = $booking->getEmailContent();     
//
//        $data_object = array('booking'=>$booking, 'review'=>$this,'labelContent'=>$emailEle);
//         $notification->notify($data_object,'review');
//   
//        $header['from'] = array('sales@weeloy.com' => 'Weeloy.com');
//        $header['replyto'] = array('sales@weeloy.com');
//
//        $recipient = $booking->email;
//        
//        $subject = "New review on the " . $booking->restaurantinfo->title;
//        //$body = $mailer->getTemplate('review/notifyreview_member', $data_object);
//        
//        //$mailer->sendmail($recipient, $subject, $body, $header);
//       
//        //email restau
//        $recipient = $booking->restaurantinfo->email;
//        //$recipient = "support@weeloy.com";
//        $subject = "You have a new review";
//
//        $body = $mailer->getTemplate('review/notifyreview_restaurant', $data_object);
//        
//        $mailer->sendmail($recipient, $subject, $body, $header, $booking->restaurantinfo->title, $booking->confirmation);
//    }
    function testreqHeader($headerInfo){
         $sql = "INSERT INTO  testrequest_header (header_content) 
                                VALUES ('$headerInfo')";
         pdo_exec($sql);
         return 1;
    }
    function getLatestreview(){
        
    }
}

?>
