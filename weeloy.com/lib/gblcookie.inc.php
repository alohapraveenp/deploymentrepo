<?php

function mng_RestoCookie($variable) {

	$tmpname = getRestoCookie();
        
	if(!empty($_REQUEST[$variable]))
		setRestoCookie($_REQUEST[$variable]);
	else if(!empty($tmpname))
		$_REQUEST[$variable] = $tmpname;
		
	return $_REQUEST[$variable];
}

function setRestoCookie($cookie_value) {

	setcookie('weeloy_resto', '', time() - 3600);
	setcookie('weeloy_resto', $cookie_value, time() + (86400 * 30), "/");
}

function getRestoCookie() {

if(!isset($_COOKIE['weeloy_resto']))
	return "";
	
return $_COOKIE['weeloy_resto'];
}


function mng_ClearRestoCookie($variable) {
	setcookie('weeloy_resto', '', time() - 3600);
	$_REQUEST[$variable] = '';
}

function setloginAdminCookie($plaintext) {

	$cookie_value = myencode($plaintext);
	setcookie('weeloy_admin', '', time() - 3600);
	setcookie('weeloy_admin', $cookie_value, time() + (86400 * 30), "/");
}

function getloginAdminCookie() {

	if(!isset($_COOKIE['weeloy_admin']))
		return "";
	
	$cookie_value = $_COOKIE['weeloy_admin'];
	return mydecode($cookie_value);
}

function mng_MemberCookie($variable) {

        $tmpname = getMemberCookie();
        if(!empty($_REQUEST[$variable]))
		setMemberCookie($_REQUEST[$variable]);
	else if(!empty($tmpname))
		$_REQUEST[$variable] = $tmpname;
     
	//error_log("COOKIE " . $_COOKIE['weeloy_resto']);
}

function setMemberCookie($cookie_value) {
	setcookie('weeloy_member', '', time() - 3600);
	setcookie('weeloy_member', $cookie_value, time() + (86400 * 30), "/");
}


function getMemberCookie() {

if(!isset($_COOKIE['weeloy_member']))
	return "";
	
return $_COOKIE['weeloy_member'];
}

function mng_CuisineCookie($variable) {

        $tmpname = getCuisineCookie();
        if(!empty($_REQUEST[$variable]))
		setCuisineCookie($_REQUEST[$variable]);
	else if(!empty($tmpname))
		$_REQUEST[$variable] = $tmpname;
     
	//error_log("COOKIE " . $_COOKIE['weeloy_resto']);
}

function setCuisineCookie($cookie_value) {

	setcookie('weeloy_cuisine', '', time() - 3600);
	setcookie('weeloy_cuisine', $cookie_value, time() + (86400 * 30), "/");
}

function getCuisineCookie() {

if(!isset($_COOKIE['weeloy_cuisine']))
	return "";
	
return $_COOKIE['weeloy_cuisine'];
}

function mng_emailmnCookie($variable){
    $tmpname = getEmailmngCookie();
        if(!empty($_REQUEST[$variable]))
		setEmailmngCookie($_REQUEST[$variable]);
	else if(!empty($tmpname))
		$_REQUEST[$variable] = $tmpname;
}
function getEmailmngCookie() {

if(!isset($_COOKIE['weeloy_emailmng']))
	return "";
	
return $_COOKIE['weeloy_emailmng'];
}
function setEmailmngCookie($cookie_value) {

	setcookie('weeloy_emailmng', '', time() - 3600);
	setcookie('weeloy_emailmng', $cookie_value, time() + (86400 * 30), "/");
}
?>