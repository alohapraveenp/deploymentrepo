<?php 

function notifyBooking($booking, $qrcode_guest = NULL) {

        if (preg_match("/remote/i", $booking->tracking) == true) {
            return;
        }

        if (preg_match("/CHOPE|HGW|QUANDOO/i", $booking->booker) == true) {
            return;
        }


        $mailer = new JM_Mail;
        $sms = new JM_Sms();
        $apn = new JM_Pushnotif();

        $res = new WY_restaurant;
        $res->getRestaurant($booking->restaurant);

        date_default_timezone_set('UTC');

        $date_sms = date("j/m/Y H:i", mktime(substr($booking->rtime, 0, 2), substr($booking->rtime, 3, 4), 0, intval(substr($booking->rdate, 5, 2)), intval(substr($booking->rdate, 8, 2)), intval(substr($booking->rdate, 0, 4))));
        $rdate = date("F j, Y, g:i a", mktime(substr($booking->rtime, 0, 2), substr($booking->rtime, 3, 4), 0, intval(substr($booking->rdate, 5, 2)), intval(substr($booking->rdate, 8, 2)), intval(substr($booking->rdate, 0, 4))));

        // Notification Member
        $rtime = preg_replace("/:00/", "h", $booking->rtime);
        $date_sms = date("j/m/Y H:i", mktime(substr($booking->rtime, 0, 2), substr($booking->rtime, 3, 4), 0, intval(substr($booking->rdate, 5, 2)), intval(substr($booking->rdate, 8, 2)), intval(substr($booking->rdate, 0, 4))));
        $rdate = date("F j, Y, g:i a", mktime(substr($booking->rtime, 0, 2), substr($booking->rtime, 3, 4), 0, intval(substr($booking->rdate, 5, 2)), intval(substr($booking->rdate, 8, 2)), intval(substr($booking->rdate, 0, 4))));
        $rdateres = date("l F j, Y, g:i a", mktime(substr($booking->rtime, 0, 2), substr($booking->rtime, 3, 4), 0, intval(substr($booking->rdate, 5, 2)), intval(substr($booking->rdate, 8, 2)), intval(substr($booking->rdate, 0, 4))));


        $rdate_date_only = date("F j, Y", mktime(substr($booking->rtime, 0, 2), substr($booking->rtime, 3, 4), 0, intval(substr($booking->rdate, 5, 2)), intval(substr($booking->rdate, 8, 2)), intval(substr($booking->rdate, 0, 4))));
        $rdate_time_only = date("H:i", mktime(substr($booking->rtime, 0, 2), substr($booking->rtime, 3, 4), 0, intval(substr($booking->rdate, 5, 2)), intval(substr($booking->rdate, 8, 2)), intval(substr($booking->rdate, 0, 4))));
        $date_gcalender = date("j-m-Y H:i", mktime(substr($booking->rtime, 0, 2), substr($booking->rtime, 3, 4), 0, intval(substr($booking->rdate, 5, 2)), intval(substr($booking->rdate, 8, 2)), intval(substr($booking->rdate, 0, 4))));

        $data_object = clone $booking;
        $data_object->wemailflg = $booking->wemailflg = $res->emailWhitelabel();
        $data_object->rtime = $rtime;
        $data_object->rdate = $rdate;
        $data_object->rdateres = $rdateres;
        $data_object->siteUrl = __SERVERNAME__ . __ROOTDIR__;


        //$endTime = date("H:i:s", strtotime('+90 minutes', $this->rtime));
        //$date_gl = date("j/m/Y H:i", mktime(substr($endTime, 0, 2), substr($endTime, 3, 4), 0, intval(substr($endTime, 5, 2)), intval(substr($this->rdate, 8, 2)), intval(substr($this->rdate, 0, 4))));
        //$data_object->bkendtime = date("Ymd\THis\Z", strtotime($date_sms));


        date_default_timezone_set('UTC');
        $date_gcalender = strtotime($date_gcalender);
        $data_object->bkstartdate = gmdate("Ymd\THis\Z", $date_gcalender);

        $data_object->rdate_time_only = $rdate_time_only;
        $data_object->rdate_date_only = $rdate_date_only;

        $header['from'] = array('reservation@weeloy.com' => 'Weeloy.com');
        $header['replyto'] = array('info@weeloy.com');
//        if (isset($qrcode_guest)) {
//            $header['guest_qrcode'] = $qrcode_guest;
//        }

        $recipient = $booking->email;
        $recipient_mobile = $booking->mobile;
        $tracking = $booking->tracking;
        $data_object->isDeposit = false;
        if(isset($booking->deposit_id) && $booking->deposit_id !== ''){
            $depsoitDestails = $this->getDepositDetails($booking->deposit_id);
            if(count($depsoitDestails)>0){
                $data_object->isDeposit =true;
                $data_object->currency =$depsoitDestails['curency'];
                $data_object->amount =$depsoitDestails['amount'];
                $data_object->despositstatus =$depsoitDestails['status'];
            
            }
            
        }


        $is_white_label = false;
        if ($this->checkisWhitelabel($booking->tracking)) {
            $is_white_label = true;
            $header['white_label'] = true;
            if($booking->wemailflg)
				$header['from'] = array($booking->restaurantinfo->email => $booking->restaurantinfo->title . "|emailwhitelabel");
			else $header['from'] = array('reservation@weeloy.com' => $booking->restaurantinfo->title);
                        
                        
			
			//$header['from'] = array('reservation@weeloy.com' => $booking->restaurantinfo->title);
                        
                        
            $header['replyto'] = array($booking->restaurantinfo->email);
        }
        $data_object->whiteLabel =$is_white_label;

        //$recipient = "philippe.benedetti@weeloy.com";
        $subject = "Reservation at " . $booking->restaurantinfo->title . " on " . $rdate;

        $data_object->baseemail = $booking->base64url_encode($booking->email);
        $data_object->baseconfirmation = $booking->base64url_encode($booking->confirmation);
        $data_object->baserdate = $booking->base64url_encode($rdate);

        //getting booking id purpose
        $data = pdo_single_select("SELECT id,restaurant from booking WHERE  confirmation = '$booking->confirmation' LIMIT 1");
        if (count($data) > 0) {
            $data_object->basebookid = $booking->base64url_encode($data['id']);
        }
        $data_object->shareLink = urlencode($data_object->siteUrl . "/modules/booking/bookingdetails.php/?b=" . $data_object->basebookid . "&rd=" . $data_object->baseconfirmation);

        $rescountry = $res->country;
        $data_object->rescountry = $rescountry;
        $data_object->restTel = $booking->restaurantinfo->tel;
        $data_object->ispromo = false;

        if (!$booking->restaurantinfo->is_wheelable && preg_match("/cpp_credit_suisse/i", $booking->tracking) == true) {
            $data = $res->getActivePromotion($booking->restaurant, $res->affiliate_program);
            if (count($data) > 0) {
                $data_object->offer = $data['offer_cpp'];
                $data_object->offer_description = $data['description_cpp'];
                $data_object->ispromo = true;
            }
        } else if (preg_match("/cpp_credit_suisse/i", $booking->tracking) == true) {
            $data_object->offermsgres = 'This is corporate booking. The corporate Wheel will be automatically selected in your Weeloy app.';
            $data_object->offermsgmem = 'Please bring your bussiness card to enjoy your corparate program Promotion';
            $data_object->iscpp = true;
        }


        /*         * ************************************* */
        //Email->restaurant terms &condition
        /*         * ************************************* */

        $data_object->restc = "";
        $res->lastorderdiner = "";
        $res->lastorderlunch = "";

        if (isset($res->bookinfo)) {
            $llorder = ($res->lastorderlunch != "12:00") ? $res->lastorderlunch : "";
            $ldorder = ($res->lastorderdiner != "18:00") ? $res->lastorderdiner : "";
            $showinfo = $res->bookinfo;

            if ($showinfo != "") {
                if ($llorder !== "")
                    $llorder = "Last order for lunch is " . $llorder;
                $data_object->llorder = $llorder;
                if ($ldorder !== "")
                    $ldorder = "Last order for diner is " . $ldorder;
                $data_object->ldorder = $ldorder;
            }

            $data_object->restc = $showinfo;
        }

        /**         * ************************************ */
        //Email->translation  dynamic content
        /*         * **************** ************************************* */

//        $emailEle = $this->getEmailContent();
             
        $trad = new WY_Translation;
        $emailEle = $trad->getEmailContent('EMAIL', $booking->language);
        $data_object->labelContent = $emailEle;

        /* End************************************************** */
        if (strpos($tracking, 'WEBSITE') !== false || strpos($tracking, 'facebook') !== false) {
            // WHITE LABEL 
            // Booking form on restaurant website

            $body = $mailer->getTemplate('callcenter/notifycallcenter_member', $data_object);

            $sms_message = "[NOREPLY] " . $booking->restaurantinfo->title . "\n" . $date_sms . "\n" . "Confirmation: " . $booking->confirmation . ", \n" . str_replace(" ", "", $booking->restaurantinfo->tel) . ' ' . $booking->restaurantinfo->address . " " . $booking->restaurantinfo->zip . ", \n" . $booking->restaurantinfo->city;
        } else if (strpos($tracking, 'CALLCENTER') !== false) {

            // WHITE LABEL 
            // Booking form on callcenter
            $body = $mailer->getTemplate('callcenter/notifycallcenter_member', $data_object);
            $sms_message = "[NOREPLY] " . $booking->restaurantinfo->title . "\n" . $date_sms . "\n" . "Confirmation: " . $booking->confirmation . ", \n" . str_replace(" ", "", $booking->restaurantinfo->tel) . ' ' . $booking->restaurantinfo->address . " " . $booking->restaurantinfo->zip . ", \n" . $booking->restaurantinfo->city;
        } else if (!$booking->restaurantinfo->is_bookable && $booking->restaurantinfo->is_wheelable) {
            // --> in weeloy.com 
            //   KALA
            //  UPDATE  notifyrequest_member and   notifylisting_member
            // Request weeloy.com
            $body = $mailer->getTemplate('booking/notifyrequest_member', $data_object);

            //$body = $mailer->getTemplate('newnotifyrequest_member', $data_object);
            //SMS UPDATE KALA
//            if ($res->country == 'Singapore') {
//                $sms_message = $this->restaurantinfo->title . "\n" . $date_sms . "\n" . "Reference: " . $this->confirmation . "\nWeeloy Code: " . $this->membCode . ",\n Book a restaurant with Weeloy & Win a Free $20 UBER ride!\n" . str_replace(" ", "", $this->restaurantinfo->tel);
//            } else {
//                $sms_message = $this->restaurantinfo->title . "\n" . $date_sms . "\n" . "Reference: " . $this->confirmation . "\nWeeloy Code: " . $this->membCode . ",\n" . str_replace(" ", "", $this->restaurantinfo->tel) . "\n" . $this->restaurantinfo->address . " " . $this->restaurantinfo->zip . ", \n" . $this->restaurantinfo->city;
//            }
            $sms_message = $booking->restaurantinfo->title . "\n" . $date_sms . "\n" . "Reference: " . $booking->confirmation . "\nWeeloy Code: " . $booking->membCode . ",\n" . str_replace(" ", "", $booking->restaurantinfo->tel) . "\n" . $booking->restaurantinfo->address . " " . $booking->restaurantinfo->zip . ", \n" . $booking->restaurantinfo->city;
        } else {

            //PASSBOOK
            if (!$is_white_label) {
                // add passbook for all bookings on weeloy.com 
                $passbook_link = __BASE_URL__ . '/passbook/' . base64_encode($booking->confirmation . '|||' . $booking->email);
                $data_object->passbook = $passbook_link;
            }
            // Booking weeloy.com
            if ($booking->restaurantinfo->is_wheelable) {
                $body = $mailer->getTemplate('booking/notifybooking_member', $data_object);
       
                //$body = $mailer->getTemplate('review/notifyreview_reminder', $data_object);
//                if ($res->country == 'Singapore') {
//                    //SMS UPDATE KALA
//                    $sms_message = $this->restaurantinfo->title . "\n" . $date_sms . "\n" . "Confirmation: " . $this->confirmation . "\nWeeloy Code: " . $this->membCode . ",\n Book a restaurant with Weeloy & Win a Free $20 UBER ride!\n" . str_replace(" ", "", $this->restaurantinfo->tel);
//                } else {
//                    // 
//                    $sms_message = $this->restaurantinfo->title . "\n" . $date_sms . "\n" . "Confirmation: " . $this->confirmation . "\nWeeloy Code: " . $this->membCode . ",\n" . str_replace(" ", "", $this->restaurantinfo->tel) . "\n" . $this->restaurantinfo->address . " " . $this->restaurantinfo->zip . ", \n" . $this->restaurantinfo->city;
//                }
                $sms_message = $booking->restaurantinfo->title . "\n" . $date_sms . "\n" . "Confirmation: " . $booking->confirmation . "\nWeeloy Code: " . $booking->membCode . ",\n" . str_replace(" ", "", $booking->restaurantinfo->tel) . "\n" . $booking->restaurantinfo->address . " " . $booking->restaurantinfo->zip . ", \n" . $booking->restaurantinfo->city;
            } else {

                // Listing only
                $body = $mailer->getTemplate('booking/notifylisting_member', $data_object);


//                if ($res->country == 'Singapore') {
//                   $sms_message = $this->restaurantinfo->title . "\n" . $date_sms . "\n" . "Confirmation: " . $this->confirmation . ",\n Book a restaurant with Weeloy & Win a Free $20 UBER ride!\n" . str_replace(" ", "", $this->restaurantinfo->tel);
//                    
//                } else {
//                    $sms_message = $this->restaurantinfo->title . "\n" . $date_sms . "\n" . "Confirmation: " . $this->confirmation . ",\n" . str_replace(" ", "", $this->restaurantinfo->tel) . "\n" . $this->restaurantinfo->address . " " . $this->restaurantinfo->zip . ", \n" . $this->restaurantinfo->city;
//                }
                $sms_message = $booking->restaurantinfo->title . "\n" . $date_sms . "\n" . "Confirmation: " . $booking->confirmation . ",\n" . str_replace(" ", "", $booking->restaurantinfo->tel) . "\n" . $booking->restaurantinfo->address . " " . $booking->restaurantinfo->zip . ", \n" . $booking->restaurantinfo->city;
                if ($booking->restaurant == 'SG_SG_R_Bacchanalia') {
                    $sms_message = $booking->restaurantinfo->title . "\n" . $date_sms . ",\n" . str_replace(" ", "", $booking->restaurantinfo->tel) . "\n" . $booking->restaurantinfo->address . " " . $booking->restaurantinfo->zip . ", \n" . $booking->restaurantinfo->city;
                }
                //SMS UPDATE KALA
            }
        }


        if (!empty($booking->email) && $booking->email != "no@email.com") {
            $mailer->sendmail($recipient, $subject, $body, $header, $booking->restaurant, $booking->confirmation);
        }
        $smsid = "Weeloy";
        if ($this->checkisWhitelabel($data_object->tracking) && !empty($data_object->restaurantinfo->smsid))
            $smsid = $data_object->restaurantinfo->smsid;
        $sms->sendSmsMessage($recipient_mobile, $sms_message, $smsid, $booking->restaurant, $booking->confirmation);
        //unset($header['guest_qrcode']);
        //unlink('tmp',$a->image(6));
        //restaurant Email 
        unset($header['passbook']);

        $notif_message = "New booking: " . $booking->restaurantinfo->title . " " . $booking->cover . 'pp - ' . $date_sms;
        if (!$is_white_label) {
            //get all device related to this account
            $sql = "SELECT md.mobile_id, md.user_id, md.mobile_type FROM mobile_devices md WHERE app_name = 'weeloy' AND md.status = 'active' AND  md.user_id = '$booking->email'";


            $data = pdo_multiple_select($sql);
            foreach ($data as $row) {
                $recipient = $row['mobile_id'];
                $pn_type = $row['mobile_type'];
                $apn->sendPushnotifMessage($recipient, $notif_message, $pn_type, null, 'weeloy', $booking->restaurant, $booking->confirmation); // null for options ?
            }
        }
        $recipient = $booking->restaurantinfo->email;

        if ($is_white_label === true) {
            $header['replyto'] = array($booking->email);
        }
 		
 		$header['from'] = array('reservation@weeloy.com' => 'Weeloy.com');

        // $recipient = "support@weeloy.com";
        $subject = "New reservation: " . $rdate . ", " . $booking->salutation . " " . $booking->firstname . " " . $booking->lastname . " at " . $booking->restaurantinfo->title . " - " . $booking->confirmation;

        if ($this->checkisWhitelabel($tracking)) {
            $body = $mailer->getTemplate('callcenter/notifycallcenter_restaurant', $data_object);
        } else if (!$booking->restaurantinfo->is_bookable && $booking->restaurantinfo->is_wheelable) {
            $body = $mailer->getTemplate('booking/notifyrequest_restaurant', $data_object);
        } else {
            if ($booking->restaurantinfo->is_wheelable) {
                $body = $mailer->getTemplate('booking/notifybooking_restaurant', $data_object);
            } else {
                $body = $mailer->getTemplate('booking/notifylisting_restaurant', $data_object);
            }
        }
        $mailer->sendmail($recipient, $subject, $body, $header, $booking->restaurant, $booking->confirmation);


        //SMS to restaurant Manager
        if ($res->smsNotifToRestaurant()) {
             $sms_message = 'NEW BOOKING ' . $booking->restaurantinfo->title . "\n" . $date_sms . " " . $booking->cover . "pax\n" . "Confirmation: " . $booking->confirmation . "\nName " . $booking->firstname . ' ' . $booking->lastname . "\nPhone " . $booking->mobile;
            $this->sendSmsToRestaurant($res, $booking, $data_object,$sms_message, $smsid, 'SMSP');
            //$recipient_mobile = $res->getManagerPhoneNumber();
           

            //$sms->sendSmsMessage($recipient_mobile, $sms_message, $smsid, $this->restaurant, $this->confirmation);
        }
        $notif_message = "New booking: " . $date_sms . " " . $booking->cover . 'pp - ' . $booking->restaurantinfo->title;
        //get all device related to this account
        $sql = "SELECT md.mobile_id, md.user_id, md.mobile_type FROM restaurant_app_managers ram, mobile_devices md WHERE app_name = 'weeloy_pro' AND md.status = 'active' AND ram.user_id = md.user_id AND `restaurant_id` LIKE '" . $booking->restaurant . "'";
        $data = pdo_multiple_select($sql);
        foreach ($data as $row) {
            $recipient = $row['mobile_id'];
            $pn_type = $row['mobile_type'];
            $apn->sendPushnotifMessage($recipient, $notif_message, $pn_type, null, 'weeloy_pro', $booking->restaurant, $booking->confirmation); // null for options ?
        }
    }
