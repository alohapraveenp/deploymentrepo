<?php

require_once("lib/class.spool.inc.php");

class JM_Sms {

    static public function sendMessage($recipient, $message, $expeditor = 'Weeloy', $reference = 'NULL', $source_test, $type = 'SMSP', $provider = 'textmagic') {
            
        $message = html_entity_decode($message);
//$country = JM_Sms::extractCountry($recipient);
//$this->route_sms($recipient, $message, $expeditor, $reference);
        $res = '';

        $recipient = str_replace(" ", "", $recipient);

//        if(strpos($recipient,'6590253065') !== false
////                  || strpos($recipient,'6596517408') !== false
////                       || strpos($recipient,'6596542259') !== false
//                ){
//            return JM_Sms::sendSmsSmsgateway($recipient, $message, $expeditor, $reference);
//        }
//          $provider = JM_Sms::getSmsProvider($reference);
//       
        switch ($provider) {

            case 'smsgateway':
                $res = JM_Sms::sendSmsSmsgateway($recipient, $message, $expeditor, $reference);
                break;
            case 'textmagic':
                $res = JM_Sms::sendTextmagic($recipient, $message, $expeditor, $reference, $source_test);
//$res = JM_Sms::sendSmsClickatell($recipient, $message, $expeditor, $reference, $source_test);
                break;
            case 'nexmo':
                $res = JM_Sms::sendNexmo($recipient, $message, $expeditor, $reference,$source_test);
                break;
            default:
                $res = JM_Sms::sendSmsSmsgateway($recipient, $message, $expeditor, $reference);
                break;
        }
        return $res;
    }

    static public function sendSmsMessage($recipient, $message, $expeditor = 'weeloy', $restaurant = NULL, $booking_id = NULL, $type = 'SMSP', $isPaid = 0) {
//$sms_message = $this->restaurantinfo->title."\n".$date_sms."\n". "Confirmation: " .$this->confirmation."\nWeeloy Code: ".$this->membCode." (for rewards)\n". $this->restaurantinfo->address . ", \n". $this->restaurantinfo->city." ". $this->restaurantinfo->zip.", \n". str_replace(" " , "", $this->restaurantinfo->tel);
        if (strlen($message) > 160) {
            $message = substr($message, 0, 160);
        }

        $opts = $subject = NULL;
        $spool = new WY_Spool;

        $message = htmlspecialchars($message);

        //$provider = ($type === 'SMS') ? 'smsgateway' : 'textmagic';
        $provider = ($type === 'SMS') ? 'nexmo' : 'textmagic';

        $spool->register_sms($recipient, $subject, $message, $opts, $expeditor, $restaurant, $booking_id, $type, $provider, $isPaid);
    }

    static public function sendSmsSmsgateway($recipient, $message, $expeditor, $reference) {
        $xml = new SimpleXMLElement('<MESSAGES/>');
//https://dashboard.onlinesmsgateway.com
        $authentication = $xml->addChild('AUTHENTICATION');
        $authentication->addChild('PRODUCTTOKEN', 'd192a5cb-ded9-4378-9883-7c076cca0ffb');

        $msg = $xml->addChild('MSG');

        $expeditor = '00447860054506';
        $msg->addChild('FROM', $expeditor);
        $msg->addChild('TO', $recipient);
        $msg->addChild('BODY', $message);
        $msg->addChild('REFERENCE', $reference);

        $xml = $xml->asXML();

        $url = "https://secure.cm.nl/smssgateway/cm/gateway.ashx";

        $header = array('Content-Type: application/xml');

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

// receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }

    static public function sendSmsCommzgate($recipient, $message, $expeditor, $reference) {

        $username = '62580002';
        $password = '250419';
        $mobile = str_replace('+', '', $recipient);

        $postfields = "";
        $postfields .= "ID=$username";
        $postfields .= "&Password=$password";
        $postfields .= "&Mobile=$mobile";
        $postfields .= "&Type=A";
        $postfields .= "&Message=$message";
        $postfields .= "&Sender=T$expeditor";
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => 'https://www.commzgate.net/gateway/SendMsg',
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postfields,
            CURLOPT_RETURNTRANSFER => true
        ));

        $response = curl_exec($ch);

        curl_close($ch);

        return $response;
    }

    static public function sendSmsClickatell($recipient, $message, $expeditor, $reference, $source_test) {

        $username = 'weeloy';
        $password = 'ORdZEAQCcGdMeH';
        $app_id = '3546942';

        $mobile = str_replace('+', '', $recipient);

//$message = str_replace('&amp;', '%26', $message);
        $message = str_replace('&amp;', '&', $message);
        $message = urlencode($message);

        $postfields = "";
        $postfields .= "user=$username";
        $postfields .= "&password=$password";
        $postfields .= "&api_id=$app_id";
        $postfields .= "&to=$mobile";
        $postfields .= "&from=Weeloy";
        $postfields .= "&text=$message";

        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => 'http://api.clickatell.com/http/sendmsg',
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postfields,
            CURLOPT_RETURNTRANSFER => true
        ));

        $response = curl_exec($ch);

        curl_close($ch);

        return $response;
    }

    static public function sendTextmagic($recipient, $message, $expeditor, $reference, $source_test) {

        $username = SMS_USERNAME;
        $api_key = SMS_APP_KEY;

        $header = array(
            "X-TM-Username:$username",
            "X-TM-Key:$api_key",
            'Content-Type :application/x-www-form-urlencoded'
        );

        $mobile = str_replace('+', '', $recipient);

        $postfields_array = array();
        $postfields_array['text'] = $message;
        $postfields_array['phones'] = $mobile;
        //$postfields_array['from'] = '6597570508';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_URL, SMS_URL);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postfields_array));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

// receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);

        $response_data = json_decode($response, TRUE);

        if (isset($response_data)) {
            $message_id = $response_data['messageId'];
            $db_history = getConnection('dwh');
            $sql = "UPDATE sms_status_history SET recipient ='$mobile', message_id = '$message_id' WHERE reference ='$reference'";
            $db_history->exec($sql);
//              $db_history->exec("INSERT INTO sms_tracking (message_id, booking_id, reply_message,last_update) VALUES ('$message_id', '$data[booking_id]', '', NOW())");
        }

        return $response;
    }

    
   static public function sendNexmo($recipient, $message, $expeditor, $reference, $source_test) {

       $username = 'adf0c965';
       $api_key = '6b555b9fdf3e463f';
       
       $mobile_from = str_replace('+', '', $expeditor);
       $mobile = str_replace('+', '', $recipient);
       
       //test hk number nexmo
       if($expeditor == 'Weeloy'){
            $mobile_from = '85258086922';
       }
       
       
       
       $url = 'https://rest.nexmo.com/sms/json?' . http_build_query(
            [
              'api_key' =>  $username,
              'api_secret' => $api_key,
              'to' => $mobile,
              'from' => $mobile_from,
              'text' => $message
            ]
        );

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
       
        curl_close($ch);

        $response_data = json_decode($response, TRUE);

        if (isset($response_data)) {
            $message_id = $response_data['message-id'];
            $db_history = getConnection('dwh');
            $sql = "UPDATE sms_status_history SET recipient ='$mobile', message_id = '$message_id' WHERE reference ='$reference'";
            $db_history->exec($sql);
//              $db_history->exec("INSERT INTO sms_tracking (message_id, booking_id, reply_message,last_update) VALUES ('$message_id', '$data[booking_id]', '', NOW())");
        }

        return $response;
    }
     public function sendSilverStreet($recipient, $message, $expeditor, $reference, $source_test) {
         $username = 'weeloy1';
         $password = 'bcx80b6';
       
         $mobile_from = str_replace('+', '', $expeditor);
         $mobile = str_replace('+', '', $recipient);
       
        //test hk number silverstreet
        if($expeditor == 'Weeloy'){
             //$mobile_from = '85258086922';
            $mobile_from ='6596542259';
        }
       
       error_log("MOBILE FROM " .$mobile_from);
       
       $url = 'https://api.silverstreet.com/send.php?' . http_build_query(
            [
              'username' =>  $username,
              'password' => $password,
              'destination' =>$mobile,
              'sender' =>$mobile_from,
              'body' => $message

            ]
        );
        error_log("URL " .$url);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
         error_log("SMS SILVER STREET " .print_r($response,true));
        curl_close($ch);
        $response_data = json_decode($response, TRUE);
        return $response;
         
     }
    
    
    
    
    public function extractCountry($recipient) {
        if (strpos($recipient, '+65') !== false || strpos($recipient, '0065') !== false) {
            return 'singapore';
        }
        if (strpos($recipient, '+66') !== false || strpos($recipient, '0066') !== false) {
            return 'thailand';
        }
        return 'others';
    }

    public function getSmsMessage($type, $obj, $data_object) {

        $date = $data_object->date_sms;
     
        $weeloycode = "";
        if ($obj->membCode !== "" && $obj->membCode !== "0000"){
            $weeloycode = "\nWeeloy Code: " . $obj->membCode . ",";
        }
        if($obj->restaurant =='SG_SG_R_Bacchanalia' ){
            $sms_message = $this->getSmsMessageBacchanalia($type, $obj, $data_object);
            return $sms_message;
        }

        switch ($type) {

            case 'booking_member':
          
                   $sms_message = $obj->restaurantinfo->title . "\n" . $date . "\n" . "Reference: " . $obj->confirmation . $weeloycode . "\n" . str_replace(" ", "", $obj->restaurantinfo->tel) . "\n" . $obj->restaurantinfo->address . " " . $obj->restaurantinfo->zip . ", \n" . $obj->restaurantinfo->city;
        
                $ps_restaurants = array('SG_SG_R_PscafeAtHardingRoad', 'SG_SG_R_PscafeAtAnnSiangHillPark', 'SG_SG_R_PscafeAtPalaisRenaissance', 'SG_SG_R_PscafeAtParagon', 'SG_SG_R_PscafePetitAtMartinRoad', 'SG_SG_R_ChopsueyCafeAtMartinRoad', 'SG_SG_R_ChopsueyCafeAtDempseyHill');
                if (in_array($obj->restaurant, $ps_restaurants, true)) {
                    $sms_message = $this->getSmsMessagePsCafeGroup($type, $obj, $data_object);
                }
                
                break;

            case 'booking_member_white_label':
                $sms_message = "[NOREPLY] " . $obj->restaurantinfo->title . "\n" . $date . "\n" . "Confirmation: " . $obj->confirmation . ", \n" . str_replace(" ", "", $obj->restaurantinfo->tel) . ',  ' . $obj->restaurantinfo->address . " " . $obj->restaurantinfo->zip . ", \n" . $obj->restaurantinfo->city;
                $ps_restaurants = array('SG_SG_R_PscafeAtHardingRoad', 'SG_SG_R_PscafeAtAnnSiangHillPark', 'SG_SG_R_PscafeAtPalaisRenaissance', 'SG_SG_R_PscafeAtParagon', 'SG_SG_R_PscafePetitAtMartinRoad', 'SG_SG_R_ChopsueyCafeAtMartinRoad', 'SG_SG_R_ChopsueyCafeAtDempseyHill');
                if (in_array($obj->restaurant, $ps_restaurants, true)) {
                    $sms_message = $this->getSmsMessagePsCafeGroup($type, $obj, $data_object);
                }
                
                break;

            case 'listing_member':
           
                   $sms_message = $obj->restaurantinfo->title . "\n" . $date . "\n" . "Confirmation: " . $obj->confirmation . ",\n" . str_replace(" ", "", $obj->restaurantinfo->tel) . "\n" . $obj->restaurantinfo->address . " " . $obj->restaurantinfo->zip . ", \n" . $obj->restaurantinfo->city;
   
                break;

            case 'request_member':
                
                  $sms_message = $obj->restaurantinfo->title . "\n" . $date . "\n" . "Reference: " . $obj->confirmation . $weeloycode . "\n" . str_replace(" ", "", $obj->restaurantinfo->tel) . "\n" . $obj->restaurantinfo->address . " " . $obj->restaurantinfo->zip . ", \n" . $obj->restaurantinfo->city;
              
                break;

            case 'booking_restaurant':
                $sms_message = 'NEW BOOKING ' . $date . " " . $obj->cover . "pax\n" . $obj->firstname . ' ' . $obj->lastname . "\nPhone " . $obj->mobile . "Confirmation: " . $obj->confirmation;
                break;


            case 'callcenter':
                $sms_message = "[NOREPLY] " . $obj->restaurantinfo->title . "\n" . $date . "\n" . "Confirmation: " . $obj->confirmation . ", \n" . str_replace(" ", "", $obj->restaurantinfo->tel) . ' ' . $obj->restaurantinfo->address . " " . $obj->restaurantinfo->zip . ", \n" . $obj->restaurantinfo->city;
                $ps_restaurants = array('SG_SG_R_PscafeAtHardingRoad', 'SG_SG_R_PscafeAtAnnSiangHillPark', 'SG_SG_R_PscafeAtPalaisRenaissance', 'SG_SG_R_PscafeAtParagon', 'SG_SG_R_PscafePetitAtMartinRoad', 'SG_SG_R_ChopsueyCafeAtMartinRoad', 'SG_SG_R_ChopsueyCafeAtDempseyHill');
                if (in_array($obj->restaurant, $ps_restaurants, true)) {
                    $sms_message = $this->getSmsMessagePsCafeGroup($type, $obj, $data_object);
                }
                break;
            case 'is_wheelable':
                $sms_message = $obj->restaurantinfo->title . "\n" . $date . "\n" . "Confirmation: " . $obj->confirmation . $weeloycode . "\n" . str_replace(" ", "", $obj->restaurantinfo->tel) . "\n" . $obj->restaurantinfo->address . " " . $obj->restaurantinfo->zip . ", \n" . $obj->restaurantinfo->city;
                break;

            case 'cancel-callcenter':
                switch ($obj->restaurant) {
                    case 'SG_SG_R_BurntEnds':
                        
                        //default one
                        
                        $sms_message = '[NOREPLY] Your booking at ' . $obj->restaurantinfo->title . "\n on the " . $date . "\n has been cancelled";
                        break;
                    default:
                         $sms_message = '[NOREPLY] Your booking at ' . $obj->restaurantinfo->title . "\n on the " . $date . "\n has been cancelled";
                        break;
                }                
                break;
            case 'cancel':
                switch ($obj->restaurant) {
                    case 'SG_SG_R_BurntEnds':
                        if(!empty($data_object->amount) && $data_object->amount > 0){
                            $sms_message = '[NOREPLY] Your booking at ' . $obj->restaurantinfo->title . "\n on the " . $date . "\n has been cancelled - Cancellation fee - $data_object->amount";
                        }else{
                            $sms_message = '[NOREPLY] Your booking at ' . $obj->restaurantinfo->title . "\n on the " . $date . "\n has been cancelled";
                        }
                        break;
                    case 'SG_SG_R_TheOneKitchen':
                        if(!empty($data_object->amount) && $data_object->amount > 0){
                            $sms_message = '[NOREPLY] Your booking at ' . $obj->restaurantinfo->title . "\n on the " . $date . "\n has been cancelled - Cancellation fee - $data_object->amount";
                        }else{
                            $sms_message = '[NOREPLY] Your booking at ' . $obj->restaurantinfo->title . "\n on the " . $date . "\n has been cancelled";
                        }
                        break;  
                    default:
                  
                          $sms_message = '[NOREPLY] Your booking at ' . $obj->restaurantinfo->title . "\n on the " . $date . "\n has been cancelled";
                     
                        break;
                }
                break;

            case 'cancel_restaurant':
                    $sms_message = 'BOOKING CANCELLED ' . $obj->restaurantinfo->title . "\n" . $date . " " . $obj->cover . "pax\n" . "Confirmation: " . $obj->confirmation . "\nName " . $obj->firstname . ' ' . $obj->lastname . "\nPhone " . $obj->mobile;
                 break;

            case 'wheelwin':
                $sms_message = 'Congratulations, you won ' . $obj->wheelwin . ' by turning the Weeloy Wheel! Thank you for choosing ' . $obj->restaurantinfo->title;
                break;


            case 'update_member':
                $sms_message = "Booking update: " . $obj->restaurantinfo->title . "\n" . $date . "\n" . "Confirmation: " . $obj->confirmation . ", \n" . str_replace(" ", "", $obj->restaurantinfo->tel) . ' ' . $obj->restaurantinfo->address . " " . $obj->restaurantinfo->zip . ", \n" . $obj->restaurantinfo->city;
                break;

            case 'update_restaurant':
                $sms_message = 'Booking update: ' . $date . " " . $obj->cover . "pax\n" . $obj->firstname . ' ' . $obj->lastname . "\nPhone " . $obj->mobile . "Confirmation: " . $obj->confirmation;
                break;
            case 'wheel_win_member':
                $sms_message = 'Congratulations, you won ' . $obj->wheelwin . ' by turning the Weeloy Wheel! Thank you for choosing ' . $obj->restaurantinfo->title;
                break;


            case 'reminder_booking_member':
                $sms_message = $obj->restaurantinfo->title . "\n" . $date . "\n" . "Reference: " . $obj->confirmation . $weeloycode . "\n" . str_replace(" ", "", $obj->restaurantinfo->tel) . "\n" . $obj->restaurantinfo->address . " " . $obj->restaurantinfo->zip . ", \n" . $obj->restaurantinfo->city;
                
               break;

            case 'reminder_listing_member':
                $sms_message = $obj->restaurantinfo->title . "\n" . $date . "\n" . "Confirmation: " . $obj->confirmation . ",\n" . str_replace(" ", "", $obj->restaurantinfo->tel) . "\n" . $obj->restaurantinfo->address . " " . $obj->restaurantinfo->zip . ", \n" . $obj->restaurantinfo->city;
                
                
                break;

            case 'reminder_request_member':
                $sms_message = $obj->restaurantinfo->title . "\n" . $date . "\n" . "Reference: " . $obj->confirmation . $weeloycode . "\n" . str_replace(" ", "", $obj->restaurantinfo->tel) . "\n" . $obj->restaurantinfo->address . " " . $obj->restaurantinfo->zip . ", \n" . $obj->restaurantinfo->city;
                break;
            case 'pending_booking_callcenter' :
                $sms_message = "Thank you for choosing".$obj->restaurantinfo->title."! To confirm your booking,"."\n"."please click here:".$data_object->link."\n"."Table has been blocked until(".$obj->rtime.")."; 
                break;
            case 'expired_booking':
                $sms_message = "The booking has not been confimed within the time frame and has been cancelled automatically."."\n"."We look forwward to your next visit."; 
                break;
                        
        }

        return $sms_message;
    }

    private function getSmsMessagePsCafeGroup($type, $obj, $data_object) {
        $date = $data_object->date_sms;
        $sms_message = '';

        $date = $date = str_replace('/', '-', $date);
        $date = date("M d @ h:ia", strtotime($date));

        switch ($obj->restaurant) {


            case 'SG_SG_R_PscafeAtHardingRoad':
                $sms_message = "[NOREPLY] Thank you for reserving PSCafe at 28B Harding Rd, $date for " . $obj->cover . "pax " . str_replace(" ", "", $obj->restaurantinfo->tel) . ". We look forward to seeing you. (ref:$obj->confirmation)";
                break;


            case 'SG_SG_R_PscafeAtAnnSiangHillPark':
                $sms_message = "[NOREPLY] Thank you for reserving PSCafe at 45 Ann Siang Hill, $date for " . $obj->cover . "pax " . str_replace(" ", "", $obj->restaurantinfo->tel) . ". We look forward to seeing you. (ref:$obj->confirmation)";
                break;

            case 'SG_SG_R_PscafeAtPalaisRenaissance':
                $sms_message = "[NOREPLY] Thank you for reserving PSCafe at Palais, 390 Orchard Rd, $date for " . $obj->cover . "pax " . str_replace(" ", "", $obj->restaurantinfo->tel) . ". We look forward to seeing you. (ref:$obj->confirmation)";
                break;

            case 'SG_SG_R_PscafeAtParagon':
                $sms_message = "[NOREPLY] Thank you for reserving PSCafe at Paragon, 290 Orchard Rd, $date for " . $obj->cover . "pax " . str_replace(" ", "", $obj->restaurantinfo->tel) . ". We look forward to seeing you. (ref:$obj->confirmation)";
                break;

            case 'SG_SG_R_PscafePetitAtMartinRoad':
                $sms_message = "[NOREPLY] Thank you for reserving PSPetit at 38 Martin Rd, $date for " . $obj->cover . "pax " . str_replace(" ", "", $obj->restaurantinfo->tel) . ". We look forward to seeing you. (ref:$obj->confirmation)";
                break;

            case 'SG_SG_R_ChopsueyCafeAtMartinRoad':
                $sms_message = "[NOREPLY] Thank you for reserving Chopsuey at 38 Martin Rd, $date for " . $obj->cover . "pax " . str_replace(" ", "", $obj->restaurantinfo->tel) . ". We look forward to seeing you. (ref:$obj->confirmation)";
                break;

            case 'SG_SG_R_ChopsueyCafeAtDempseyHill':
                $sms_message = "[NOREPLY] Thank you for reserving Chopsuey at Blk10 Dempsey Hill, $date for " . $obj->cover . "pax " . str_replace(" ", "", $obj->restaurantinfo->tel) . ". We look forward to seeing you. (ref:$obj->confirmation)";
                break;

            default:
                $sms_message = "[NOREPLY] Thank you for reserving Chopsuey at Blk10 Dempsey Hill, $date for " . $obj->cover . "pax " . str_replace(" ", "", $obj->restaurantinfo->tel) . ". We look forward to seeing you. (ref:$obj->confirmation)";
                break;
        }

        return $sms_message;
    }
    
    private function promotionsms($type,$date,$obj,$weeloycode){
         switch ($type) {
            case 'booking_member':
                $sms_message = $obj->restaurantinfo->title . "\n" . $date . "\n" . "Reference: " . $obj->confirmation . $weeloycode . "\nBook a restaurant and Win $50 Dining vouchers!!\n" . str_replace(" ", "", $obj->restaurantinfo->tel);
                break;
            case 'listing_member':
                $sms_message = $obj->restaurantinfo->title . "\n" . $date . "\n" . "Confirmation: " . $obj->confirmation . ",\nBook a restaurant and Win $50 Dining vouchers!!\n" . str_replace(" ", "", $obj->restaurantinfo->tel) ;
                break;

            case 'request_member':
                $sms_message = $obj->restaurantinfo->title . "\n" . $date . "\n" . "Reference: " . $obj->confirmation . $weeloycode . "\nBook a restaurant and Win $50 Dining vouchers!!\n" . str_replace(" ", "", $obj->restaurantinfo->tel);
                break;
         }
         return $sms_message;
        
    }
    
    private function getSmsMessageBacchanalia($type, $obj, $data_object){
         $date = $data_object->date_sms;
        
            $expiredDate = $this->getExpiredTime($obj->cdate,$obj->restaurant,$data_object->rdate);
            $date1 = date("d/m/Y", strtotime($data_object->rdate));
         switch ($type) {
            case 'booking_member': 
            case 'listing_member': 
            case 'callcenter': 
                  $sms_message = "Thank you for choosing ".$obj->restaurantinfo->title . " your booking has been confirmed. Booking for ". $obj->cover . "pax on " .$date1 . " at " .  $data_object->bktime .". We look forward to your visit.";
                break;
            case 'pending_booking_callcenter' :
                  $sms_message ="Thank you for choosing ". $obj->restaurantinfo->title."! To confirm your booking, click "."\n".$data_object->link."\n"."Table has been blocked until ".$expiredDate; 
                 //$sms_message = $obj->restaurantinfo->title . "\n" . $date . "\n" . "Reference: " . $obj->confirmation . $weeloycode . "\n" . str_replace(" ", "", $obj->restaurantinfo->tel) . "\n" . $obj->restaurantinfo->address . " " . $obj->restaurantinfo->zip . ", \n" . $obj->restaurantinfo->city;
                break;
            case 'booking_member_white_label':
                   $sms_message = "Thank you for choosing ".$obj->restaurantinfo->title . " your booking has been confirmed. Booking for ". $obj->cover . "pax on " .$date1 . " at " .  $data_object->bktime .". We look forward to your visit.";
               break; 
     
            case 'reminder_booking_member':
            case 'reminder_listing_member':
                  $sms_message = $obj->restaurantinfo->title . "\n" ."Please be reminded you are booked for ".$obj->cover . "pax on ". $date . "at " .  $data_object->bktime  . ",\n See you soon!";
               break;
            case 'reminder_booking_whitelabel':
                 $sms_message = $obj->restaurantinfo->title . "\n" ."Please be reminded you are booked for ".$obj->cover . "pax on ". $date . "at " .  $data_object->bktime  . ",\n See you soon!";
                break;
            case 'cancel':
                  $sms_message = 'Your booking at ' . $obj->restaurantinfo->title . "\n on the " . $date . "\n has been cancelled";
                  break;
            case  'cancel-callcenter':
                  $sms_message = 'Your booking at ' . $obj->restaurantinfo->title . "\n on the " . $date . "\n has been cancelled";
                  break;
             default:
                return false;
           
 
         }
  
            return $sms_message;
        
    }

    public static function getSmsProvider($smsid) {
        //get sms provider
        $provider = "textmagic";
        $providerdata = pdo_single_select("select * from sms_tracking_invoice where data_id = '$smsid'  limit 1", 'dwh');
        if (count($providerdata) > 0) {
            $provider = $providerdata['provider'];
        }
        return $provider;
    }
    
    public function saveSmsTemplate($args){
 
        $parameters['restaurant'] = $args['restaurant'];
        $parameters['message'] = $args['message'];
        $parameters['title'] = $args['title'];
        $parameters['status'] = $args['status'];

        if($args['action'] == 'create'){
            $sql = "INSERT INTO `sms_template` (`restaurant`, `message`, `title`, `status`) VALUES ( :restaurant, :message, :title, :status)";
            $result = pdo_insert($sql, $parameters);
            return $result;
        }else{
            $parameters['id'] = $args['id'];
            $sql = "UPDATE sms_template SET `message` = :message,`title` = :title,`status` = :status WHERE `id` = :id  AND `restaurant` LIKE :restaurant";
            return pdo_exec_with_params($sql, $parameters);
        }
    }

    public function getSmsTemplate($restaurant){
        
        $parameters['restaurant'] = $restaurant;
        $sql = "SELECT *  FROM `sms_template` WHERE  `restaurant` LIKE :restaurant ORDER BY ID DESC";
        $template = pdo_multiple_select_with_params($sql, $parameters);
        return $template;
        
    }
    public function deleteSmsTemplate($args){
        
        $parameters['restaurant'] = $args['restaurant'];
        $parameters['id'] = $args['id'];
        $sql = "DELETE FROM `sms_template` WHERE  `id` = :id  AND `restaurant` LIKE :restaurant";
        return pdo_exec_with_params($sql, $parameters);
    }
    public function getExpiredTime($cdate,$restaurant,$rdate){
        
        date_default_timezone_set('asia/singapore');
        $interval = strtotime("+240 minutes") - strtotime("$cdate");
        $exdate = date('h:i A', strtotime("+240 minutes")); 
        $exdate48 = date('M j, Y h:i A', strtotime("+2880 minutes"));
        if($restaurant == 'SG_SG_R_Bacchanalia'){
            $interval = strtotime("+720 minutes") - strtotime("$cdate");
            $exdate =  date('M j, Y h:i A', strtotime("+720 minutes")); //12 hours
        }
        $intDay = (strtotime("$rdate") - strtotime("$cdate") )/ (60*60*24);
        $interval_day = $interval / (60*60*24);
            if($restaurant == 'SG_SG_R_Bacchanalia'){
                $expirydate = $exdate;
            }else if ($restaurant == 'SG_SG_R_Nouri'){
                $expirydate = $exdate48;
            }else if($restaurant == 'SG_SG_R_Pollen'){
                $exdate = date('M j, Y h:i A', strtotime("+720 minutes"));
                if($intDay < 3)
                      $expirydate =$exdate;
                else
                    $expirydate = $exdate48 ; //48 hours
            }else{
                if($interval_day < 1){
                    $expirydate ='today '.$exdate;
                }else {
                   $expirydate ='tomorrow '. $exdate; 
                }
            }
            return $expirydate;
            
    }
     function getSmsTracking($query, $page = 1,$restaurant = NULL) {
        $data = array();
        $result = array();


        if ($query === 'week') {
            $filter = "WEEKOFYEAR(sent_on)=WEEKOFYEAR(NOW())";
        }
        if ($query === 'lastweek') {
            $filter = "WEEKOFYEAR(sent_on)=WEEKOFYEAR(NOW())-1";
        }
        if ($query === 'month') {
            $filter = "MONTH(sent_on)";
            
        }
        if ($query === 'lastmonth') {

            $filter = "YEAR(sent_on) = YEAR(CURRENT_DATE - INTERVAL 1 MONTH)
                    AND MONTH(sent_on) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH)";
        }
        if ($query === 'year') {
            $filter = "YEAR(sent_on)";
        }
        if ($query === 'day') {
            $filter = "CAST(sent_on AS DATE)";
        }
        if ($query === 'lastweek' || $query === 'week' || $query === 'lastmonth' ) {
            $data = pdo_multiple_select("select ID, type, status, restaurant, booking_id, data,CAST(sent_on AS DATE) AS DATE,count(*) as count from spool_history WHERE restaurant ='$restaurant' and  type='SMS' and $filter  group by type,restaurant,$filter order by ID desc", 'dwh');
        } else if ($query === 'day') {
            $data = pdo_multiple_select("select ID, type, status, restaurant, booking_id, data,CAST(sent_on AS DATE) AS DATE,count(*) as count from spool_history WHERE restaurant ='$restaurant' and type='SMS' group by type,restaurant,$filter order by ID desc", 'dwh');
        }else if($query === 'month'){
            $data = pdo_multiple_select("select ID, type, status, restaurant, booking_id, data,CAST(sent_on AS DATE) AS DATE,count(*) as count from spool_history WHERE restaurant ='$restaurant' and  type='SMS' and $filter  group by type,restaurant,MONTH(sent_on),YEAR(sent_on) order by ID desc", 'dwh');
        }
        if ($query === 's3') {
            $query = pdo_single_select("SELECT count(*) as total FROM spool_history WHERE restaurant !='' AND type='MAIL'", 'dwh');


            $limit = 500;
            if ($page === 1) {
                $offset = $page . "," . $limit;
            } else {
                $start = $page * 500;
                $offset = $start . "," . $limit;
            }
            //limit $page,$limit
            $data = pdo_multiple_select("select ID, type, status, restaurant, booking_id, data,CAST(sent_on AS DATE) AS DATE from spool_history WHERE restaurant !='' AND type='MAIL' order by ID desc limit $offset ", 'dwh');
            $result['data'] = $data;
            $result['totalCount'] = $query['total'];
            return $result;
        }


        return $data;
    }
    
    

}
