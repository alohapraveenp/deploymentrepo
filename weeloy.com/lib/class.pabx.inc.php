<?php
require_once("lib/class.restaurant.inc.php");
class WY_Pabx {

    use CleanIng;

    const NOWDATE = "DATE_ADD(NOW(), INTERVAL 8 HOUR)";
    const OLDDATE = "DATE_ADD(NOW(), INTERVAL 5 HOUR)";

    var $restaurant;
    var $fix;
    var $virtual;
    var $origin;
    var $result;
    var $msg;
    var $debug;

    public function __construct($debug = 0) {
       $this->result = 1;
       $this->fix = $this->virtual = $this->origin = $this->msg = $this->restaurant = "";
       $this->debug = $debug;
    }

    public function returnError($errorno, $msg, $val) {
        $this->result = $errorno;
        $this->msg = $msg;
        return $val;
    }

    public function getCalls($email) {
        $restaurant = new WY_restaurant();
        //$priviledge = $restaurant->ismember_priviledge($membertype);
        $restaurants = $restaurant->getListAccountRestaurant($email);
        if (count($restaurants) < 1) {
            $this->msg = "invalid account";
            $this->result = -1;
            return array();
        }
        if (!isset($restaurants) || empty($restaurants)) {
            $this->msg = "invalid account";
            $this->result = -1;
            return array();
        }
        $parameters = "";
        foreach ($restaurants as $restaurant) {
            if (!empty($parameters))
                $parameters .= ", ";
            $parameters .= '"'.$restaurant["restaurant"].'"';
        }
        return pdo_multiple_select("SELECT * FROM nexmo_test WHERE restaurant in (".$parameters.")");
    }
    
    public function getAccountLine($email) {
    	$cls = new WY_Cluster;
    	$cls->read("SLAVE", $email, "CALLCENTER", "", "", "");  // should only return 1 answer
    	if ($cls->result > 0 && !empty($cls->clustcontent[0])) {
    		$content = $cls->clustcontent[0];
    		error_log("CALLCENTER " . $content);
    		if(preg_match("/.*phone=(\+?\d+)/", $content, $results))
    			return $results[1];
    		}
    	$this->result = -1;
    	return "";
   	}
    	
    public function readPabx($fixline) {
        $this->fix = preg_replace("/\D/", "", $fixline); // filter + - . ,  
        if (empty($this->fix) || strlen($this->fix) < 8)
            return $this->returnError(-1, "invalid phone number " . $this->fix, array());

        $this->result = 1;
        $data = pdo_single_select("SELECT restaurant, fixline, fromline, virtual, cdate FROM nexmo_test WHERE fixline ='$this->fix' and type = 'start' order by cdate DESC limit 1");
        if (count($data) <= 0)
            return $this->returnError(-1, "no call " . $this->fix, array());
			
        return $data;
    }

    function initiatePabx($to, $from, $uuid) {
        $now = self::NOWDATE;
        $restaurant = "";
        
        if($this->debug > 0)
        	WY_debug::recordDebug("ERROR", "PABX-connect 0", $to);

        $pabxinfo = WY_restaurant::getRestaurantPabx($to); 
        if($this->debug > 0)
	        WY_debug::recordDebug("ERROR", "PABX-connect 1", $to . " - " . $pabxinfo['fixline']);

        if(!isset($pabxinfo['fixline']) || strlen($pabxinfo['fixline']) < 8)
        	return;
        	
        $restaurant = $pabxinfo['restaurant'];
        $fixline = preg_replace("/\D/", "", $pabxinfo['fixline']); // "6562211016";
        $request = json_encode($_REQUEST);

        pdo_exec("INSERT INTO nexmo_test set restaurant = '$restaurant', value = '$request', convID = '$uuid', virtual = '$to', fixline = '$fixline', fromline = '$from', cdate = $now, type = 'start' ");

		if(preg_match("/dev.weeloy.com/", $_SERVER["HTTP_HOST"]))
			$url = "dev.weeloy.com/modules/pabx/status.php";
		else $url = "www.weeloy.com/modules/pabx/status.php";
        $ncco = $this->getString('connect', $from, $fixline, $url); // ($to == "6531585412") ? $this->getString('talk', "") : 

        if($this->debug > 0)
	        WY_debug::recordDebug("ERROR", "PABX-connect", $ncco);
	        
        header('Content-Type: application/json');
        echo $ncco;
    }

    public function insertPabx() {
        $now = self::NOWDATE;
        $old = self::OLDDATE;
        $restaurant = "";

        $request = json_encode($_REQUEST);

        if (!empty($entityBody)) {
            $obj = json_decode($entityBody);
            $convID = (isset($obj->conversation_uuid)) ? $obj->conversation_uuid : "";
            $to = (isset($obj->to)) ? $obj->to : "";
            $from = (isset($obj->from)) ? $obj->from : "";

            $data = pdo_single_select("SELECT restaurant, virtual, fixline, fromline FROM nexmo_test WHERE convID = '$convID' AND type = 'start' limit 1");
            if (isset($data['fromline'])) {
                $virtual = $data['virtual'];
                $fixline = $data['fixline'];
                $from = $data['fromline'];
                $restaurant = $data['restaurant'];

                pdo_exec("INSERT INTO nexmo_test set restaurant = '$restaurant', value = '$entityBody', convID = '$convID', fixline = '$fixline', fromline = '$from', virtual = '$virtual', cdate = $now, type = 'body'");
            }

            //pdo_exec("INSERT INTO nexmo_archive (restaurant, value, convID, fixline, fromline, virtual, cdate, type) select restaurant, value, convID, fixline, fromline, virtual, cdate, type from nexmo_test where cdate < $old");
            //pdo_exec("delete from nexmo_test where cdate < $old");
        }

        if (!empty($request) && $request != '[]') {
            if (isset($_REQUEST['to']) && isset($_REQUEST['from'])) {
                $virtual = $_REQUEST['to'];
                $fromline = $_REQUEST['from'];
                $data = pdo_single_select("SELECT * FROM nexmo_test WHERE virtual = '$virtual' and fromline = '$fromline' AND type = 'start' order by cdate DESC limit 1");
                if (isset($data['fromline'])) {
                    $fixline = $data['fixline'];
                    $convID = $data['convID'];
                    $restaurant = $data['restaurant'];
                }
            }

            pdo_exec("INSERT INTO nexmo_test set restaurant = '$restaurant', value = '$request', convID = '$convID', fixline = '$fixline', fromline = '$fromline', virtual = '$virtual', cdate = $now, type = 'report'");
        }
    }

    function getString($action, $from, $to, $url) {
        $ncco = '';
        switch ($action) {
            case 'talk':
                $ncco = '[ { "action": "talk", "text": "Hello Russell, welcome to a Call made with Voice API" } ]';
                break;

            case 'connect':
                $ncco = '[
                {
                  "action": "connect",
                  "eventUrl": ["http://' . $url . '"],
                  "timeout": "12",
                  "from": "' . $from . '",
                  "endpoint": [
                    {
                      "type": "phone",
                        "number": "' . $to . '"
                    }]
                }
                ]';
                break;

            default:
                $ncco = "";
        }
        return $ncco;
    }

    // user config to set his/her current fixed line (to). 
    // From backoffice, callcenter, 
    // and use cluster CALLCENTER with user email

    static function updateUserfixline($line, $email) {
        $cls = new WY_Cluster;
        $cls->read("SLAVE", $email, "CALLCENTER", "", "", "");  // should only return 1 answer
        if ($cls->result > 0) {
            $parent = $cls->clustparent[0];
            $content = $cls->clustcontent[0];
            $content = preg_replace("/;?phone=\+?[\d ]+/", "", $content);
            $content = $content . ((!empty($content)) ? ";" : "") . "phone=" . $line;
            $cls->update("SLAVE", $email, "CALLCENTER", $parent, $content);  // $type, $name, $category, $parent, $content
        }
        return $cls->result;
    }
    
    // https://docs.nexmo.com/tools/application-api/api-reference#list
    
    function getNexmoAppInfo() {
			$base_url = 'https://api.nexmo.com' ;
			$version = '/v1';
			$action = '/applications/?';

			$url = $base_url . $version . $action . http_build_query([
				'api_key' => 'adf0c965',
				'api_secret' => '6b555b9fdf3e463f'
			]);

			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json'));
			curl_setopt($ch, CURLOPT_HEADER, 1);
			$response = curl_exec($ch);

			$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
			$header = substr($response, 0, $header_size);
			$body = substr($response, $header_size);

			if (strpos($header, '200')){
				$decoded_response = json_decode($body, true);
				echo("You have " . $decoded_response['count'] . " applications<br />");
				echo("Page " . $decoded_response['page_index']
					. " lists " . $decoded_response['page_size'] . " applications<br />");
				echo("Use the links to navigate. For example: "
					. $base_url .  $decoded_response['_links']['last']['href'] . "<br />"  );
				$applications = $decoded_response['_embedded']['applications'] ;

				foreach ( $applications as $application ) {
					echo "  Application ID is:" . $application['id'] . "<br />"  ;
					foreach($application['voice']['webhooks'] as $webhook)
						echo ( "    " . $webhook['endpoint_type'] . " is " . $webhook['endpoint'] . "<br />"  );
				}
			}
			else {
				$error = json_decode($body, true);
				echo("Your request failed because:<br />");
				echo("  " . $error['type'] . "  " . $error['error_title']   );
			}    
    	}

}

?>