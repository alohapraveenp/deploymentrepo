<?php


require_once 'lib/class.payment_paypal.inc.php';
require_once 'lib/class.payment_stripe.inc.php';
require_once 'lib/class.payment_adyen.inc.php';
require_once 'lib/class.payment_reddot.inc.php';
require_once  'lib/class.booking.inc.php' ;

class WY_Payment_new {

    public $pay_object = NULL;
    public $environment = 'test';
    public $amount = 0;
    public $controller ;
    public $referenceId;
    public $restaurant;
    public $token = NULL;
    var $result;
    var $msg = "";

    function __construct($payment) {
        
        $this->amount = $payment['amount'];
        $this->controller = $payment['payment_type'];
        $this->referenceId = $payment['reference_id'];
        $this->restaurant = $payment['restaurant'];
        $this->mode = $payment['mode'];
        $this->token = (isset($payment['token'])) ? $payment['token'] : NULL;
       
        switch ($this->controller) {
            case 'adyen':
                $this->pay_object = new WY_Payment_Adyen($this->referenceId, $this->amount,$this->token);
                break;
            case 'paypal': 
                $return_url = $url =  __BASE_URL__ ."/modules/booking/deposit/deposit_confirmation_confirmed.php?bkconfirmation=".$this->referenceId."&type=p";
                 $this->pay_object = new WY_Payment_Paypal($this->restaurant, $this->amount, $return_url, $this->mode, $this->referenceId);
                break;
            case 'reddot':
                $this->pay_object = new WY_Payment_Reddot($this->referenceId, $this->amount,$this->token);
                break;
            case 'stripe':
                 $this->pay_object = new WY_Payment_Stripe($this->referenceId, $this->amount);
                break;
            default:
                return $this->result = -1;
        }
        return $this->result = 1;
    }
    
    
    public function  saveCreditCard(){
       return $this->pay_object->saveCardDetails();
    }
    public function doDeposit(){
         return $this->pay_object->makeDeposit();
    }
    public function depositRefund(){
        return $this->pay_object->makeRefund();
    }
    
   
    
    


}