<?php



    function notifyBookingNew($booking) {

        if ($this->checkisRemote($booking->tracking)) {
            return;
        }

        if (preg_match("/CHOPE|HGW|QUANDOO/i", $booking->booker) == true) {
            return;
        }

        if (preg_match("/DNB/i", $booking->tracking) == true)
                return;

        $res = new WY_restaurant;
        $res->getRestaurant($booking->restaurant);

        $data_object = $this->getNotificationObject($booking);
        $data_object->wemailflg = $booking->wemailflg = $res->emailWhitelabel();
        $provider ='sms_premium';
        
        $this->checkNotifyConfig('booking_member',$booking->restaurant,$data_object); 
        
        /* **************************** EMAIL MEMBER **************************** */
        if( $this->restaurant_configuration['email']){ 
               
            $header = $this->getHeader('booking_member', $booking, $data_object);
            $recipient = $booking->email;
            $subject = $this->getSubject('booking_member', $booking, $data_object);
            $body = $this->getEmailBody('booking_member', $booking, $data_object);

            $this->email_service->sendmail($recipient, $subject, $body, $header, $booking->restaurant, $booking->confirmation);
        }
        /* *****************************End EMAIL MEMBER **************************** */
        
        /* ***************************** SMS MEMBER **************************** */
        if($this->restaurant_configuration[$this->smsprovider]){ 
            $header_sms = $this->getSmsHeader('booking_member', $booking, $data_object);
            $recipient_mobile = $booking->mobile;
            $sms_message = $this->getSmsBody('booking_member', $booking, $data_object);
            $this->sms_service->sendSmsMessage($recipient_mobile, $sms_message, $header_sms['from'], $booking->restaurant, $booking->confirmation,$this->pname );
        }
        /* ***************************** end SMS MEMBER **************************** */
        
        /* ***************************** PUSH NOTIF MEMBER **************************** */
       if($this->restaurant_configuration['apns']){ 
            $apn_message = $this->getApnBody('booking_member', $booking, $data_object);
        //$recipient-> booking email
        //the white label test must be moved
       
        //if (!$data_object->whiteLabel) {
            
            //$apn_message = $this->apn_service->getpushNotifyMessage($booking->restaurantinfo->title, $booking->cover, $data_object->date_sms, 'bkcancel');
            $this->apn_service->sendnotifyMultiple($recipient, $booking->restaurant, $booking->confirmation, $apn_message, 'member');
       }
        //}
        /* ***************************** end PUSH NOTIF MEMBER **************************** */
 
        
       
       
       $this->checkNotifyConfig('booking_restaurant',$booking->restaurant,$data_object); 

        /* **************************** EMAIL RESTAURANT **************************** */
       if($this->restaurant_configuration['email']){ 
         
        $header = $this->getHeader('booking_restaurant', $booking, $data_object);
        $recipient = $booking->restaurantinfo->email;
        $subject = $this->getSubject('booking_restaurant', $booking, $data_object);
        $body = $this->getEmailBody('booking_restaurant', $booking, $data_object);
        $this->email_service->sendmail($recipient, $subject, $body, $header, $booking->restaurant, $booking->confirmation);
        /* *****************************End EMAIL RESTAURANT **************************** */
       }
    
        if($this->restaurant_configuration[$this->smsprovider]){ 
           
            /* ***************************** SMS MEMBER **************************** */
             
            $header_sms = $this->getSmsHeader('booking_restaurant', $booking, $data_object);
            //$recipient_mobile = $res->getManagerPhoneNumber();
     
            
            // NEED a template for this sms body
            $sms_message = $this->getSmsBody('booking_restaurant', $booking, $data_object);
             //$this->sms_service->sendSmsMessage($recipient_mobile, $sms_message, $header_sms['from'], $booking->restaurant, $booking->confirmation,$this->pname);
            
            //send multiple sms 
            $this->sendSmsToRestaurant($res,$booking,$data_object,$sms_message,$header_sms['from'],$this->pname);
            
            /* ***************************** end SMS MEMBER **************************** */

        }
        if($this->restaurant_configuration['apns']){ 
            $apn_message = $this->getApnBody('booking_restaurant', $booking, $data_object);
            //$notif_message = $this->apn_service->getpushNotifyMessage($booking->restaurantinfo->title, $booking->cover, $data_object->date_sms, 'bkcancel');
            $this->apn_service->sendnotifyMultiple($recipient, $booking->restaurant, $booking->confirmation, $apn_message, 'restaurant');
        }

        /*         * ************************************ end restuarant section ***************** */
    }
