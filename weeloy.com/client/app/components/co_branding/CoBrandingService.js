var CoBrandingService = angular.module('CoBrandingService', []);
CoBrandingService.service('Branding', ['$http', '$q', function($http, $q) {
    this.getBanner = function(type,tag) {
        var API_URL = 'api/event/banner/'+type +'/'+tag;
        var defferred = $q.defer();
        $http.get(API_URL).success(function(response) {
            defferred.resolve(response);
        });
        return defferred.promise;
    };
      this.getRestaurant = function(status, city, cuisine, tags) {
        if (status === undefined) {
            status = 'both';
        }
        if (city === undefined) {
            city = 'singapore';
        }
        if (city === 'hong-kong') {
            city = 'Hong Kong';
        }
        
        cuisine = '';
      
        if (tags !== undefined) {
            if (Array.isArray(tags)) {
                tags = tags.join('|');
            }
        } else {
            tags = '';
        }
      
        var defferred = $q.defer();
        var params = {
            status: status,
            city: city,
            cuisine: cuisine,
            tags: tags,

        };
        $http.get('api/search/restaurant', {
            params: params,
            cache: true,
        }).success(function(response) {
            var restaurants = [];
            if (response.status === 1) {               
                if (response.data.no_result === true) {
                    defferred.reject('No result found');
                } else {
                    response.data.restaurant.forEach(function(value) {
                        restaurants.push(new Restaurant(value));
                    });
                }
            }
            defferred.resolve(restaurants);
        });
        return defferred.promise;
    };
    
   this.getInternalPath = function(restaurant) {
    var restaurant_details = restaurant.split('_');
    var city;
    switch (restaurant_details[1]) {
        case 'SG':
            city = 'singapore';
            break;
        case 'HK':
            city = 'hong-kong';
            break;
        case 'BK':
            city = 'bangkok';
            break;
        case 'PK':
            city = 'phuket';
            break;
        case 'KL':
            city = 'kuala-lumpur';
            break;
        default:
            city = 'singapore';
            break;
    }
    var type = 'restaurant';
    var restaurant_name = restaurant.substr(8);
    restaurant_name = restaurant_name.replace(/_/g, '');
    restaurant_name = restaurant_name.replace(/([A-Z])/g, '-$1');
    restaurant_name = restaurant_name.replace(/[-]+/, '-');
    restaurant_name = restaurant_name.toLowerCase();
    if (restaurant_name.charAt(0) == '-') {
        restaurant_name = restaurant_name.substr(1);
    }
    

        var restaurant_url = type + '/' + city + '/' + restaurant_name;
        return restaurant_url;
    };
    

}]);
