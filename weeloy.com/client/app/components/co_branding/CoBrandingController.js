var CoBrandingController = angular.module('CoBrandingController', ['InfoCustomerService','CoBrandingService','TitleAndMetaTag']);


CoBrandingController.controller('CoBrandingCtrl', [
    '$rootScope',
    '$scope',
    'InfoCustomer',
    'Branding',
    '$routeParams',
    function($rootScope, $scope, InfoCustomer,Branding,$routeParams) {
        $rootScope.page ='co-branding page';
        $scope.book_button_text = 'Book Now';
        $scope.type = 'event_co_branding';
        $scope.tag = $routeParams.branding;
        $scope.init = function() {
            $scope.pageheader  = {
              title: "",
              subtitle: "",
              description: "",
              images: ""
            };
          };
          $scope.init();
       Branding.getBanner($scope.type,$scope.tag).then(function(response) {
            console.log("RESPONSE " + JSON.stringify(response));
            if (response.status == 1 && response.count > 0) {
                $scope.pageheader.title = response.data.title;
                $scope.pageheader.subtitle = response.data.tag;
                $scope.pageheader.images = "https://media.weeloy.com/upload/restaurant/"+response.data.type + "/"+response.data.images;
                console.log("PAGE HEADER IMAGE " + $scope.pageheader.images);
                $scope.pageheader.description = response.data.description; 
                //console.log(response);
            }
      });
       Branding.getRestaurant('','','',$scope.tag).then(function(response) {
           //$scope.items =
           if(response){
                $scope.restaurants = response;
             
           }
           
            
       });

  
        
    }]);





