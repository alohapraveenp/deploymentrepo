var FatherDayController = angular.module('FatherDayController', ['FatherDayService','TitleAndMetaTag']);
FatherDayController.controller('FatherDayCtrl', [
    '$rootScope', 
    '$scope',
    'TitleAndMeta',
    'FatherDay', function($rootScope, $scope, TitleAndMeta, FatherDay) {
        $scope.book_button_text = 'Book Now';
        
        var title = "Father's Day Menus & Promotions";
        var description = "Let your dad know how important he means to you this Father's Day with a delectable meal together at one of the finest restaurants we have line-up for you. Savour this lovely occasion to express your gratitude for his love and care all these while. ";
        TitleAndMeta.setTitle(title);
        TitleAndMeta.setMetaDescription(description);
        
        TitleAndMeta.setFaceBookMetaTitle(title);
        TitleAndMeta.setFaceBookMetaDescription(description);
        $scope.init = function() {
            $scope.festivepromo  = {
              title: "",
              subtitle: "",
              description: "",
              images: ""
            };
          };

          $scope.init();
        
        FatherDay.getFatherDayEvent().then(function(response) {
            if (response.status == 1 && response.count > 0) {
                var data = response.data.events;
                 for(var i = 0; i < data.length; i++ ) {
                     data[i].internal_path = FatherDay.getInternalPath(data[i].restaurant) + "/booknow";
                 }
                $scope.events = data;
                $scope.festivepromo.title = response.data.landing.title;
                $scope.festivepromo.subtitle = response.data.landing.tag;
                $scope.festivepromo.images = "https://media.weeloy.com/upload/restaurant/"+response.data.landing.type + "/"+response.data.landing.images;
                $scope.festivepromo.description = response.data.landing.description; 
                //console.log(response);
            }
        });
        $scope.btnBookNowEvent_click = function(restaurant) { 
            var url = FatherDay.getInternalPath(restaurant) + "/booknow";
            document.location.href = "/" + url;
            //window.open(url, '_blank', 'toolbar=no, scrollbars=yes, resizable=yes, top=30, left=30, width=800, height=900');
        };
        $scope.getRestaurantLink = function(restaurant) { 
           return FatherDay.getInternalPath(restaurant);
        };
}]); 
