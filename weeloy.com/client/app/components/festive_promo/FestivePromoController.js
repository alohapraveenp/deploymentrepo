var FestivePromoController = angular.module('FestivePromoController', ['FestivePromoService','TitleAndMetaTag','RestaurantEventItemnew']);
FestivePromoController.controller('FestivePromoCtrl', [
    
    '$rootScope', 
    '$scope',
    'TitleAndMeta',
    'FestivePromo','AuditLog', function($rootScope, $scope, TitleAndMeta, FestivePromo,AuditLog) {
   
        $rootScope.page ='year-end-festive';
        AuditLog.logevent(50, ''); 
        $scope.book_button_text = 'Book Now';

        $scope.init = function() {
            $scope.festivepromo  = {
              title: "",
              subtitle: "",
              description: "",
              images: ""
            };
          };
          

          $scope.init();

        FestivePromo.getYearEndEvent().then(function(response) {
           
            if (response.status == 1 && response.count > 0) {
                var data = response.data.events;
                 for(var i = 0; i < data.length; i++ ) {
                     data[i].internal_path = FestivePromo.getInternalPath(data[i].restaurant) + "/booknow";
                 }
                $scope.events = data;
                $scope.festivepromo.title = response.data.landing.title;
                $scope.festivepromo.subtitle = response.data.landing.tag;
                $scope.festivepromo.images = "https://media.weeloy.com/upload/restaurant/"+response.data.landing.type + "/"+response.data.landing.images;
                $scope.festivepromo.description = response.data.landing.description; 
                //console.log(response);
            }
        });
         $scope.BookNowEvent = function(restaurant) { 
            alert("HAI");
            var url = FestivePromo.getInternalPath(restaurant) + "/booknow";
            document.location.href = "/" + url;
            //window.open(url, '_blank', 'toolbar=no, scrollbars=yes, resizable=yes, top=30, left=30, width=800, height=900');
        };

        // var title = ' The Ultimate Easter Dining 2017- Reserve your table now';
        // var description = 'The Ultimate Easter Dining 2017. Reserve your table with one of the finest restaurants we have lined up for you and hurry!';
        // TitleAndMeta.setTitle(title);
        // TitleAndMeta.setMetaDescription(description);
        
        // TitleAndMeta.setFaceBookMetaTitle(title);
        // TitleAndMeta.setFaceBookMetaDescription(description);
        $scope.btnBookNowEvent_click = function(restaurant) { 
            alert("HAI");
            var url = FestivePromo.getInternalPath(restaurant) + "/booknow";
            document.location.href = "/" + url;
            //window.open(url, '_blank', 'toolbar=no, scrollbars=yes, resizable=yes, top=30, left=30, width=800, height=900');
        };
        // $scope.getRestaurantLink = function(restaurant) { 
        //    return FestivePromo.getInternalPath(restaurant);
        // };
}]); 

