var PaymentController = angular.module('PaymentController', [
    'TitleAndMetaTag',
    'app.shared.directives.paymentIframe',
 
]);
PaymentController.controller('PaymentCtrl', [
    '$rootScope',
    '$scope',
    '$routeParams',
    'API',
    'TitleAndMeta',
    function($rootScope, $scope, $routeParams, API, TitleAndMeta) {
        console.log(JSON.stringify($routeParams));
        console.log("in payment controller");
     
        var restaurant = new Restaurant();
        var RestaurantID = restaurant.getRestaurantIdFromUrlParam($routeParams.city, $routeParams.restaurant);

        $scope.RestaurantID = RestaurantID;

        API.restaurant.getFullInfo(RestaurantID)
            .then(function(result) {
                result.setMediaServer($rootScope.mediaServer);
                var title = result.getTitle() + ' - Book with Weeloy and Get Rewarded';
                var description = 'Book a Restaurant in ' + result.getCity() + ' at ' + result.getTitle() + ' with Weeloy and get yourself rewarded with exclusive deals and promotions. Simply book your table and spin the wheel!';
                TitleAndMeta.setTitle(title);
                $scope.takeoutitle = (result.takeoutrestaurant > 0) ? '/ Take Out' : '';
                if (result.book_button.text.toLowerCase() == 'booking coming soon') {
                    $scope.disableBookButtton = true;
                }
                $scope.restaurant = result;

            })
            .catch(function(e) {
                console.log(e);
            });
    }
]);