var CateringController = angular.module('CateringController', ['TitleAndMetaTag', 'ShoppingCart', 'CheckoutService']);
CateringController.controller('CateringCtrl', [
    '$rootScope',
    '$scope',
    '$routeParams',
    '$location',
    '$timeout',
    'TitleAndMeta',
    'API',
    'Checkout',
    function($rootScope, $scope, $routeParams, $location, $timeout, TitleAndMeta, API, Checkout) {
        var restaurant = new Restaurant();
        var RestaurantID = restaurant.getRestaurantIdFromUrlParam($routeParams.city, $routeParams.restaurant);
        $scope.ItemsInCart = [];

        Checkout.getCartInfo()
            .then(function(response) {
                if (response.status == 1) {
                    $scope.ItemsInCart = response.data.items;
                    $rootScope.$broadcast('updateCart', response.data.items);
                    $scope.DeliveryTime = response.data.delivery_time;
                    var RestaurantID = response.data.items[0].restaurant;
                }
            });

        API.restaurant.getFullInfo(RestaurantID).then(function(result) {
            var title = result.getTitle() + ' - Catering';
            var description = 'Catering details for ' + result.getTitle();
            TitleAndMeta.setTitle(title);
            TitleAndMeta.setMetaDescription(description);
            TitleAndMeta.setFaceBookMetaTitle(title);
            TitleAndMeta.setFaceBookMetaDescription(description);
            $scope.restaurant = result;
        });
        API.catering.getCateringMenu(RestaurantID).then(function(result) {
            $scope.cateringmenu = result.data;
        });
        $scope.addToCart = function(item, evt) {
            console.log(evt.target);
            // $(evt.target).next(messag) is next element 
            // $(evt.target).parent().find('span').hide()
            $(evt.target).parent().parent().find('.added-to-cart').show();

            $timeout(function() {
                $(evt.target).parent().parent().find('.added-to-cart').hide();
            }, 3000, false);

            var addItem = true;
            item.restaurant_path = $location.$$path;
            if ($scope.ItemsInCart.length > 0) {
                $scope.ItemsInCart.forEach(function(vaue) {
                    if (vaue.ID == item.ID) {
                        addItem = false;
                    }
                });
            }
            if (!addItem) {
                $rootScope.$broadcast('IncreaseItemInCart', item.ID);
                return;
            }
            if ($scope.ItemsInCart.length > 0 && $scope.ItemsInCart[0].restaurant != item.restaurant) {
                $rootScope.showSystemMsg = true;
                $rootScope.systemMsg = 'Can not add item from many restaurant.';
                $timeout(function() {
                    $rootScope.showSystemMsg = false;
                    $rootScope.$apply();
                }, 3000, false);
                return;
            }


            Checkout.addItem(item).then(function(response) {
                if (response.status == 1) {
                    $scope.ItemsInCart.push(item);
                    $rootScope.$broadcast('AddItemToCart', item);
                    $scope.showCart = true;
                } else {
                    alert(response.data);
                }
            });
        };
        $scope.Affix_cart = function() {
            var offset;
            offset = $('#affix-cart').offset();
            var LastElement = $('#affix-cart');
            $(window).scroll(function() {
                CartAffix(offset, LastElement);
            });
            $(window).resize(function() {
                CartAffix(offset, LastElement);
            });
        };

        function CartAffix(offset, LastElement) {
            if ($(window).scrollTop() > offset.top - 40) {
                LastElement.addClass('fixedCartTop');
            } else {
                LastElement.removeClass('fixedCartTop');

            }
        }
    }
]);



CateringController.directive('setClassWhenAtTop', function($window) {
    var $win = angular.element($window); // wrap window object as jQuery object

    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var notAtTopClass = attrs.setClassWhenNotAtTop,
                topClass = attrs.setClassWhenAtTop, // get CSS class from directive's attribute value

                offsetTop = element.offset().top - 40; // get element's offset top relative to document

            $win.on('scroll', function(e) {
                if ($win.scrollTop() >= offsetTop) {
                    element.addClass(topClass);
                    element.removeClass(notAtTopClass);
                } else {

                    element.removeClass(topClass);
                    element.addClass(notAtTopClass);
                }
            });
        }
    };
});



/*      
CateringController.directive('setClassWhenTouchFooter', function ($window, $timeout) {
  var $win = angular.element($window); // wrap window object as jQuery object

  return {
    restrict: 'A',
    link: function (scope, element, attrs) {
       
        var notAtTopClass = attrs.setClassWhenNotAtTop,
            touchFooterClass = attrs.setClassWhenTouchFooter,
            offsetFooter = element.offset().top, // get element's offset top relative to document
    offsetFollow = $('.follow-us').offset().top;


console.log($window.innerHeight);
console.log($win.height());
console.log(offsetFooter);
console.log(offsetFollow);
console.log(touchFooterClass);

$timeout(function () {
        console.log($window.innerHeight);
        console.log($window.offsetHeight);
    console.log(offsetFooter);
        console.log(offsetFollow);
        console.log(touchFooterClass);

                }, 3000);
        
      $win.on('scroll', function (e) {
        if ($win.scrollTop()  >= offsetFooter) {
        console.log(touchFooterClass);   
            console.log('dsa');
        } else {
            console.log('dsa1');
        }
      });
    }
  };
});
*/
