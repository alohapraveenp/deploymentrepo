var HomeService = angular.module('HomeService', []);
HomeService.service('Home', ['$http', '$q', function($http, $q) {
    this.getBestRestaurants = function(city, limit) {
        var defferred = $q.defer();
        var API_URL = 'api/restaurant/getShowcase/' + city + '/' + limit;
        $http.get(API_URL, {
            cache: true
        }).success(function(response) {
            defferred.resolve(response);
        });
        return defferred.promise;
    };

    this.getFoodSelfieContestRestaurants = function(city, limit) {
        var defferred = $q.defer();
        var API_URL = 'api/restaurant/getFoodSelfieContestShowcase/' + city + '/' + limit;
        $http.get(API_URL, {
            cache: false
        }).success(function(response) {
            var restaurants = [];
            if (response.status == 1) {
                response.data.forEach(function(value) {
                    restaurants.push(new Restaurant(value));
                });
            } else {
                defferred.reject('No data found');
            }
            defferred.resolve(restaurants);
        });
        return defferred.promise;
    };

    this.getToprestaurants = function() {

        var defferred = $q.defer();
        var API_URL = 'api/getTopRestaurant';
        $http.get(API_URL, {
            cache: true
        }).success(function(response) {
            var restaurants = [];
            if (response.status == 1) {
                response.data.forEach(function(value) {
                    restaurants.push(new Restaurant(value));
                });
            } else {
                defferred.reject('No data found');
            }
            defferred.resolve(restaurants);
        });
        return defferred.promise;
    };
    this.getTopBlogArticles = function() {
        var defferred = $q.defer();
        var API_URL = 'api/getBlogArticles';
        $http.get(API_URL, {
            cache: true
        }).success(function(response) {

            defferred.resolve(response);
        });
        return defferred.promise;
    };

    this.getHomebanners = function() {
        var type = "home";
        var defferred = $q.defer();
        var API_URL = 'api/home/getcategories/' + type;
        $http.get(API_URL, {
            cache: true
        }).success(function(response) {
            defferred.resolve(response);
        });
        return defferred.promise;
    };

    this.getHomeEvent = function() {
        var defferred = $q.defer();
        var API_URL = 'api/home/getEventAll';
        $http.get(API_URL, {
            cache: true
        }).success(function(response) {
            defferred.resolve(response);
        });
        return defferred.promise;


    };
    this.getDynamicTag = function() {
        var type = "footer";
        var defferred = $q.defer();
        var API_URL = 'api/home/getcategories/' + type;
        $http.get(API_URL, {
            cache: true
        }).success(function(response) {
            defferred.resolve(response);
        });
        return defferred.promise;


    };



}]);
