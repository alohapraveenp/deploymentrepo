var CheckoutInfoController = angular.module('CheckoutInfoController', ['ShoppingCart', 'CheckoutService', 'LoginService']);
CheckoutInfoController.controller('CheckoutInfoCtrl', [
    '$rootScope',
    '$scope',
    '$location',
    'API',
    'Checkout',
    'loginService',
    function($rootScope, $scope, $location, API, Checkout, loginService) {
        $scope.DeliveryDate = moment().format('YYYY-MM-DD');
        $scope.DeliveryDateFormatted = moment().format('DD-MM-YYYY');
        $scope.DeliveryTimeRange = [];
        $scope.pickupHours = [];
        API.pickuphour.getPickupHours('SG_SG_R_TheFunKitchen')
            .then(function(response) {
                $scope.pickup = [];
                if (response.status == 1) {
                    response.data.forEach(function(value, key) {
                        var day = [];
                        value.pickuphour.forEach(function(value2, key2) {
                            day.push({
                                selected: false,
                                time: value2,
                            });
                        });
                        $scope.pickupHours.push(day);
                    });
                    var d = new Date($scope.DeliveryDate);
                    var n = d.getDay();
                    $scope.DeliveryTimeRange = $scope.pickupHours[n];
                    //Math.round($scope.DeliveryTimeRange.length/3)
                    $scope.chunkedData = chunk($scope.DeliveryTimeRange, Math.round($scope.DeliveryTimeRange.length / 4));
                }
            });

        function chunk(arr, size) {
            var newArr = [];
            for (var i = 0; i < arr.length; i += size) {
                newArr.push(arr.slice(i, i + size));
            }
            return newArr;
        }

        // Math.round($scope.DeliveryTimeRange.length/3);
        if ($rootScope.loggedin) {
          
            $scope.OrderUser = {
                email: $rootScope.user.email,
                firstname: $rootScope.user.firstname,
                lastname: $rootScope.user.name,
                mobile: $rootScope.user.prefix + $rootScope.user.mobile,
            };
        }
        $scope.SelectTime = function(time) {
            $scope.DeliveryTimeRange.forEach(function(value, key) {
                $scope.DeliveryTimeRange[key].selected = false;
            });
            time.selected = true;
            $scope.DeliveryTime = time.time;
        };
        $rootScope.$on('UserLogin', function(evt, user) {
            $scope.OrderUser = {
                email: $rootScope.user.email,
                firstname: $rootScope.user.firstname,
                lastname: $rootScope.user.name,
                mobile: $rootScope.user.prefix + $rootScope.user.mobile,
            };
        });
        Checkout.getCartInfo()
            .then(function(response) {
                if (response.status == 1) {
                    $scope.items = response.data.items;
                    $rootScope.$broadcast('updateCart', response.data.items);
                    $scope.DeliveryTime = response.data.delivery_time;
                    var RestaurantID = response.data.items[0].restaurant;
                    API.restaurant.getFullInfo(RestaurantID)
                        .then(function(result) {
                            $scope.restaurant = result;
                        });
                } else {
                    $location.path('/');
                    return false;
                }
            });
        $('#datetimepicker').datetimepicker({
            inline: true,
            locale: 'en',
            format: 'DD/MM/YYYY',
            //enabledDates: ['11/14/2015', '11/15/2015', '11/16/2015'],
        });
        $('#datetimepicker').on("dp.change", function(e) {
            $scope.DeliveryDate = e.date.format('YYYY-MM-DD'); // e.date is momentjs object
            $scope.DeliveryDateFormatted = e.date.format('DD-MM-YYYY');
            var d = new Date($scope.DeliveryDate);
            var n = d.getDay();
            $scope.DeliveryTimeRange = $scope.pickupHours[n];
            $scope.chunkedData = chunk($scope.DeliveryTimeRange, Math.round($scope.DeliveryTimeRange.length / 4));

            $scope.$apply();
        });
        $scope.SaveCart = function(OrderUser, remarks) {
            // if ($rootScope.loggedin) {

            // } else {
            //     if (OrderUser.password != OrderUser.password_confirmation) {
            //         $rootScope.showSystemMsg = true;
            //         $rootScope.systemMsg = 'Password confimation does not match';
            //         setTimeout(function() {
            //             $rootScope.showSystemMsg = false;
            //             $rootScope.$apply();
            //         }, 3000);
            //         return false;
            //     } else {
            //         loginService.register(OrderUser).then(function(response) {
            //             console.log(response);
            //         });
            //     }
            // };

            Checkout.saveCart(OrderUser.email, OrderUser.firstname, OrderUser.lastname, OrderUser.mobile, OrderUser.company, $scope.DeliveryTime, $scope.DeliveryDate, remarks)
                .then(function(response) {
                    console.log("sfdfd"+JSON.stringify(response));
                    if (response.status == 1) {
                        var form = document.createElement('form');
                        form.action = response.data.paypalUrl;
                        form.method = 'POST';

                        //                            var inputs = [{
                        //                                    name: 'cmd',
                        //                                    value: '_xclick',
                        //                                }, {
                        //                                    name: 'business',
                        //                                    value: 'pvh8692@gmail.com',
                        //                                }, {
                        //                                    name: 'amount',
                        //                                    value: response.data.amount,
                        //                                }, {
                        //                                    name: 'quantity',
                        //                                    value: 1,
                        //                                }, {
                        //                                    name: 'currency_code',
                        //                                    value: 'SGD',
                        //                                }, {
                        //                                    name: 'firstname',
                        //                                    value: 'firstname',
                        //                                }, {
                        //                                    name: 'lastname',
                        //                                    value: 'lastname',
                        //                                }, {
                        //                                    name: 'item_name',
                        //                                    value: 'Order online with ' + $scope.restaurant.restaurantinfo.title,
                        //                                }, {
                        //                                    name: 'notify_url',
                        //                                    value: BASE_URL + '/modules/shoppingcart/paypal_ipn.php',
                        //                                }, {
                        //                                    name: 'return',
                        //                                    value: BASE_URL + '/payment-success?payment_id=' + response.data.payment_id,
                        //                                }, ];
                        //                            inputs.forEach(function (value) {
                        //                                var input = document.createElement('input');
                        //                                input.type = 'hidden';
                        //                                input.value = value.value;
                        //                                input.name = value.name;
                        //                                form.appendChild(input);
                        //                            });
                        document.body.appendChild(form);

                        form.submit();
                    }
                });
        };
    }
]);
