var VDayController = angular.module('VDayController', ['VDayService','TitleAndMetaTag','ng.deviceDetector']);

VDayController.controller('VDayCtrl', [
    '$rootScope', 
    '$scope',
    'TitleAndMeta',
    'VDay','deviceDetector', function($rootScope, $scope, TitleAndMeta, VDay, deviceDetector) {
        $scope.book_button_text = 'Book Now';
          $scope.isMobile = deviceDetector.isMobile(); //deviceDetector.isMobile();
        console.log("SDASD"+deviceDetector.isMobile());
        var title = 'Valentine\'s day 2017 with Weeloy - Reserve your table now';
        var description = 'Valentine\'s day 2017 with Weeloy. Valentine\'s day restaurant menu. Reserve your table with one of the finest restaurants we have lined up for you and hurry!';
        TitleAndMeta.setTitle(title);
        TitleAndMeta.setMetaDescription(description);
        
        TitleAndMeta.setFaceBookMetaTitle(title);
        TitleAndMeta.setFaceBookMetaDescription(description);
        
        VDay.getVDayEvent().then(function(response) {
            if (response.status == 1 && response.count > 0) {
                 response.data.events.forEach(function(value, key) {
                  console.log(value.name);
                  value.name = value.name.replace("Vday - ", ""); 
                  value.name = value.name.replace("Vday -", "");  
                  console.log(value.name);
                    //$scope.RestaurantEvents.push(new RestaurantEvent(value));
                });
                $scope.events = response.data.events;
                $scope.landing = response.data.landing;
                $scope.image = "https://media.weeloy.com/upload/restaurant/event_valentines/"+$scope.landing.images;
                console.log("getVDayEvent(): image: "+$scope.image);
            } else
                console.error("getVDayEvent(): error: "+JSON.stringify(response));
        });
        $scope.btnBookNowEvent_click = function(restaurant) { 

            var url = VDay.getInternalPath(restaurant) + "/booknow";
            document.location.href = "/" + url;
            //window.open(url, '_blank', 'toolbar=no, scrollbars=yes, resizable=yes, top=30, left=30, width=800, height=900');
        };
        $scope.getRestaurantLink = function(restaurant) { 
           return VDay.getInternalPath(restaurant);
        };
}]);
$(document).ready(function() { 
   $('[data-toggle="popover"]').popover();
  
}); 

