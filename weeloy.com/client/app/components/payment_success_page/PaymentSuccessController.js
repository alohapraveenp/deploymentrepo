var PaymentSuccessController = angular.module('PaymentSuccessController', []);
PaymentSuccessController.controller('PaymentSuccessCtrl', [
    '$rootScope',
    '$scope',
    '$routeParams',
    '$window',
    '$location',
    'API',
    function($rootScope, $scope, $routeParams, $window, $location, API) {
        if ($location.$$search.payment_id !== undefined && $location.$$search.payment_id !== '') {
            API.payment.check($routeParams.payment_id)
                .then(function(response) {
                    console.log("SDASDSA"+JSON.stringify(response));
                    if (response.status) {
                        response.data.delivery_date = moment(response.data.delivery_date).format('MMM DD, YYYY');
                        $scope.message = 'Payment Successfull';
                        $scope.showCart = false;
                        $scope.order = response.data.order;
                        $scope.payment = response.data.payment;
                        API.payment.deleteCart()
                            .then(function(response) {
                                console.log(response);
                                if (response.status == 1) {
                                    $rootScope.cart = [];
                                    $window.cart = [];
                                }
                            });
                        var RestaurantID = response.data.order.restaurant;
                        API.restaurant.getFullInfo(RestaurantID)
                            .then(function(result) {
                                $scope.restaurant = result;
                            })
                            .catch(function(e) {
                                console.log(e);
                            });
                    } else {
//                        $location.path(BASE_PATH);
//                        return false;
                    }
                });
        } else {
            $location.path(BASE_PATH);
            return false;
        }
        $scope.viewAllItem = function(order_id) {
            API.myorder.getOrderDetail(order_id)
                .then(function(response) {
                    $scope.items = response.data;
                });
        };

        $scope.loadmap = function(lat, lng, isMobile) {
            if (document.getElementById('gmap-sdk') !== undefined && document.getElementById('gmap-sdk') !== null) {
                CreateMap(lat, lng, isMobile);
            } else {
                var script = document.createElement("script");
                script.type = "text/javascript";
                script.id = 'gmap-sdk';
                script.src = "//maps.google.com/maps/api/js?callback=CreateMap";
                document.body.appendChild(script);
            }
        };

        $scope.loadmap2 = function(lat, lng, isMobile) {
            alert('boom');
        };

        $window.CreateMap = function() {
            CreateMap($scope.restaurant.getLatitude().lat, $scope.restaurant.getLatitude().lng, $scope.isMobile);
        };

        function CreateMap(lat, lng, isMobile) {
            var myLatlng = new google.maps.LatLng(lat, lng);
            console.log(lat);
            console.log(lng);
            console.log(isMobile);
            var mapOptions = {
                zoom: 14,
                center: myLatlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                scrollwheel: false,
                navigationControl: false,
                mapTypeControl: false,
            };
            if (isMobile) {
                mapOptions.scaleControl = false;
                mapOptions.draggable = false;
                mapOptions.zoomControl = false;
            }
            map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
            });
        }
    }
]);
