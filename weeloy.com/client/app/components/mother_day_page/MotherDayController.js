var MotherDayController = angular.module('MotherDayController', ['MotherDayService','TitleAndMetaTag']);
MotherDayController.controller('MotherDayCtrl', [
    '$rootScope', 
    '$scope',
    'TitleAndMeta',
    'MotherDay','AuditLog', function($rootScope, $scope, TitleAndMeta, MotherDay,AuditLog) {
        $rootScope.page='mothers-day';
        AuditLog.logevent(50, ''); 
        $scope.book_button_text = 'Book Now';
        
        var title = 'Amazing Mother s Day Deals and Menus with Weeloy - Reserve your table now';
        var description = 'Amazing Mother s Day Deals and Menus 2016 with Weeloy. Mother s Day 2016 restaurant menu. Reserve your table with one of the finest restaurants we have lined up for you and hurry!';
        TitleAndMeta.setTitle(title);
        TitleAndMeta.setMetaDescription(description);
        
        TitleAndMeta.setFaceBookMetaTitle(title);
        TitleAndMeta.setFaceBookMetaDescription(description);
        
        MotherDay.getMotherDayEvent().then(function(response) {
            if (response.status == 1 && response.count > 0) {
                $scope.events = response.data;
                console.log(response);
            }
        });
        $scope.btnBookNowEvent_click = function(restaurant) { 
            var url = MotherDay.getInternalPath(restaurant) + "/booknow";
            document.location.href = "/" + url;
            //window.open(url, '_blank', 'toolbar=no, scrollbars=yes, resizable=yes, top=30, left=30, width=800, height=900');
        };
        $scope.getRestaurantLink = function(restaurant) { 
           return MotherDay.getInternalPath(restaurant);
        };
}]); 
