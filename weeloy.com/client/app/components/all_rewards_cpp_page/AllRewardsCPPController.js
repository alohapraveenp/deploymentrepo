var AllRewardsCPPController = angular.module('AllRewardsCPPController', ['AllRewardsCPPService', 'TitleAndMetaTag']);
AllRewardsCPPController.controller('AllRewardsCPPCtrl', ['$rootScope', '$scope', '$routeParams', 'API', 'RewardCPP', 'TitleAndMeta', function($rootScope, $scope, $routeParams, API, RewardCPP, TitleAndMeta) {

    var restaurant = new Restaurant();
    var RestaurantID = restaurant.getRestaurantIdFromUrlParam($routeParams.city, $routeParams.restaurant);

    //hide on webview mobile app
    $scope.mobileWebview = ($routeParams.dspl_h === 'f' && $routeParams.dspl_f === 'f' );

    API.restaurant.getFullInfo(RestaurantID)
        .then(function(result) {
            $scope.disableBookButtton = 'false';
            $scope.restaurant = result;
            var title = result.getTitle() + ' - Wheel Details';
            TitleAndMeta.setTitle(title);
            if (result.getStatus() != 'active') {
                $scope.disableBookButtton = true;
            }
        });
    RewardCPP.getCPPReward(RestaurantID)
        .then(function(response) {
            if (response.status == 1) {
                $scope.rewards = response.data;
            }
        });
}]);
