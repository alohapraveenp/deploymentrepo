var MyReviewController = angular.module('MyReviewController', ['ui.bootstrap', 'MyBookingReview', 'AuditLog']);
MyReviewController.controller('MyReviewCtrl', [
    '$rootScope',
    '$scope',
    '$location',
    'API',
    'AuditLog',
    function($rootScope, $scope, $location, API, AuditLog) {
        $rootScope.checkLoggedin();
        $rootScope.MenuUserSelected = 'myreviews';
        $scope.showreviewList = true;
        $rootScope.page = "my_reviews";

        $scope.showreviewpostList = false;
           AuditLog.logevent(50, '');

        if ($location.$$search.f == 'review') {
            $scope.confirmation = $location.$$search.rf;
            API.myreview.getBooking($scope.confirmation).then(function(response) {
                AuditLog.logevent(105, 'bkemail_reviewpost');
                if (response.data.reviewcount == 0) {
                    $scope.reviewList = response.data;
                    $scope.showreviewpostList = true;
                    $scope.showreviewList = false;
                } else {
                    $("#msg-alert").css('display', 'block');
                    $(".alert").addClass('alert-info');
                    $(".alert").html(' The review for this booking has already been posted.');
                    $scope.close_msg();

                }

            });
        }
        $scope.close_msg = function() {
            setTimeout(function() {
                $('#msg-alert').fadeOut('slow');
                $(".alert").html('');
                $('#msg-alert').removeClass('alert-info');
                window.location.href = "myreviews";

            }, 10000); // <-- time in milliseconds  
        }


        $scope.shownoData = false;
        if($location.$$search.id){
            $scope.reviewid=$location.$$search.id;
            console.log("id="+$scope.reviewid);
        }
        API.myreview.getMyReviews().then(function(response) {
         
            if (response.status == 1) {
                var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
                var weekday = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
                response.data.reviews.forEach(function(value, key) {
                    var rtime = value.reviewdtcreate.split(/[-|:| ]/g);
                    var d = new Date(rtime[0], rtime[1], rtime[2], rtime[3], rtime[4], rtime[5]);
                    response.data.reviews[key].timeStr = monthNames[d.getMonth()] + ' ' + d.getDate() + ' ' + d.getFullYear();
                });
                if (response.count > 0) {
                    $scope.shownoData = true;
                }
                $scope.ReviewsCount = response.count;
                var resData = [];
//                var values =response.data.reviews;
            if($location.$$search.id){
                    response.data.reviews.forEach(function(values, keys) {

                                 if(values.id===$scope.reviewid){
                                     console.log("in id"+ val);
                                     resData.push(values);
                                 }
                     });
                 $scope.myReviews = resData;
                  console.log(JSON.stringify(resData));
           }else{
                           
                $scope.myReviews = response.data.reviews;
            }
            };
        });
    }
]);
