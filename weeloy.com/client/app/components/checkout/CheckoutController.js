var CheckoutController = angular.module('CheckoutController', ['CheckoutService', 'ShoppingCart']);
CheckoutController.controller('CheckoutCtrl', [
    '$rootScope',
    '$scope',
    '$window',
    '$location',
    '$routeParams',
    'Checkout',
    'API',
    function($rootScope, $scope, $window, $location, $routeParams, Checkout, API) {
        moment.locale('en');
        $scope.today = moment().format('dddd (MMM DD, YYYY)');
        $scope.showCart = false;
        $scope.DeliveryTime = [{
            selected: true,
            time: '10:30AM - 11:00AM',
        }, {
            selected: false,
            time: '11:00AM - 11:30AM',
        }, {
            selected: false,
            time: '11:30AM - 12:00PM',
        }, {
            selected: false,
            time: '12:00PM - 12:30PM',
        }, {
            selected: false,
            time: '12:30PM - 1:00PM',
        }, {
            selected: false,
            time: '1:00PM - 1:30PM',
        }];


        Checkout.getItems().then(function(response) {
            if (response.status == 1) {
                $scope.items = response.data;
                $rootScope.$broadcast('updateCart', response.data);
            } else {
                $location.path(BASE_PATH);
                return false;
            }
        });

        if ($rootScope.cart === undefined || $rootScope.cart.length === 0) {
            $location.path(BASE_PATH);
            return false;
        }

        $scope.SelectTime = function(time) {
            $scope.DeliveryTime.forEach(function(value, key) {
                $scope.DeliveryTime[key].selected = false;
            });
            time.selected = true;
        };


        var RestaurantID = $rootScope.cart[0].restaurant;
        API.restaurant.getFullInfo(RestaurantID)
            .then(function(result) {
                result.setMediaServer($rootScope.mediaServer);
                $scope.restaurant = result;
            });

        $scope.Checkout = function(items) {
            var SelectDeliveryTime;
            $scope.DeliveryTime.forEach(function(value, key) {
                if (value.selected === true) {
                    SelectDeliveryTime = $scope.DeliveryTime[key].time;
                }
            });
            Checkout.updateTime(SelectDeliveryTime)
                .then(function(response) {
                    $location.path('checkout/info');
                });
        };
    }
]);
