var CheckoutService = angular.module('CheckoutService', []);
CheckoutService.service('Checkout', ['$http', '$q', function($http, $q) {
    this.getItems = function() {
        var defferred = $q.defer();
        $http.get('api/cart/items').success(function(response) {
            defferred.resolve(response);
        });
        return defferred.promise;
    };
    this.deleteItem = function(item) {
        var defferred = $q.defer();
        $http.delete('api/cart/items/' + item.ID).success(function(response) {
            defferred.resolve(response);
        });
        return defferred.promise;
    };
    this.addItem = function(item) {
        var defferred = $q.defer();
        $http.post('api/cart/items', {
            menu_id: item.ID,
            restaurant_path: item.restaurant_path,
        }).success(function(response) {
            defferred.resolve(response);
        });
        return defferred.promise;
    };
    this.updateItem = function(item) {
        var defferred = $q.defer();
        $http.put('api/cart/items/' + item.ID, {
            quantity: item.quantity
        }).success(function(response) {
            defferred.resolve(response);
        });
        return defferred.promise;
    };
    this.saveCart = function(email, firstname, lastname, phone, company, delivery_time, delivery_date, remarks) {
        var defferred = $q.defer();
        email = email || '';
        firstname = firstname || '';
        lastname = lastname || '';
        phone = phone || '';
        delivery_time = delivery_time || '';
        delivery_date = delivery_date || '';
        remarks = remarks || '';
        company = company || '';

        $http.post('api/cart', {
            email: email,
            firstname: firstname,
            lastname: lastname,
            phone: phone,
            company: company,
            delivery_time: delivery_time,
            delivery_date: delivery_date,
            remarks: remarks
        }).success(function(response) {
            defferred.resolve(response);
        });
        return defferred.promise;
    };
    this.updateTime = function(delivery_time) {
        var defferred = $q.defer();
        $http.post('api/cart/delivery-time', {
            delivery_time: delivery_time
        }).success(function(response) {
            defferred.resolve(response);
        });
        return defferred.promise;
    };
    this.getCartInfo = function() {
        var defferred = $q.defer();
        $http.get('api/cart').success(function(response) {
            defferred.resolve(response);
        });
        return defferred.promise;
    };
}]);
