var BookingController = angular.module('BookingController', [
    'TitleAndMetaTag',
    'app.shared.directives.bookIframe',
    'app.shared.directives.bkdepositIframe', 
    'app.shared.directives.booknowsectionIframe', 
]);
BookingController.controller('BookingCtrl', [
    '$rootScope',
    '$scope',
    '$routeParams',
    'API',
    'TitleAndMeta',
    function($rootScope, $scope, $routeParams, API, TitleAndMeta) {
        var restaurant = new Restaurant();
        var RestaurantID = restaurant.getRestaurantIdFromUrlParam($routeParams.city, $routeParams.restaurant);

        $scope.RestaurantID = RestaurantID;


        API.restaurant.getFullInfo(RestaurantID)
            .then(function(result) {
                result.setMediaServer($rootScope.mediaServer);
                var title = result.getTitle() + ' - Book with Weeloy - Instant Confirmation';
                var description = 'Book a Restaurant in ' + result.getCity() + ' at ' + result.getTitle() + ' with Weeloy - instant confirmation. Enjoy deals and promtion with partner restaurants';
                
                TitleAndMeta.setTitle(title);
                TitleAndMeta.setMetaDescription(description);
                TitleAndMeta.setFaceBookMetaTitle(title);
                TitleAndMeta.setFaceBookMetaDescription(description);
                
                $scope.takeoutitle = (result.takeoutrestaurant > 0) ? '/ Take Out' : '';
                if (result.book_button.text.toLowerCase() == 'booking coming soon') {
                    $scope.disableBookButtton = true;
                }

                $scope.restaurant = result;
            })
            .catch(function(e) {
                console.log(e);
            });
    }
]);
