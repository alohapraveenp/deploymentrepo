var InfoCustomerController = angular.module('InfoCustomerController', ['InfoCustomerService','TitleAndMetaTag','RestaurantEventItemnew']);
FestivePromoController.controller('InfoCustomerCtrl', [
    
    '$rootScope', 
    '$scope',
    'TitleAndMeta',
    'InfoCustomer','AuditLog', function($rootScope, $scope, TitleAndMeta, InfoCustomer,AuditLog) {
   
        $rootScope.page ='year-end-festive';
        AuditLog.logevent(50, ''); 
        $scope.book_button_text = 'Book Now';
        $scope.type = 'event_vip_page';
        $scope.promotions =[];

        $scope.init = function() {
            $scope.pageheader  = {
              title: "",
              subtitle: "",
              description: "",
              images: ""
            };
          };

          $scope.init();

        InfoCustomer.getBanner($scope.type).then(function(response) {
           
            if (response.status == 1 && response.count > 0) {
                $scope.pageheader.title = response.data.title;
                $scope.pageheader.subtitle = response.data.tag;
                $scope.pageheader.images = "https://media.weeloy.com/upload/restaurant/"+response.data.type + "/"+response.data.images;
                $scope.pageheader.description = response.data.description; 
                //console.log(response);
            }
        });
        InfoCustomer.getPromotions('promotions').then(function(response) {
                if (response.status == 1 && response.count > 0) {
                    var data = response.data.categories;
                     for (var i = 0; i< data.length; i++) {
                        data[i].image = "https://media.weeloy.com/upload/restaurant/"+data[i].images[0].restaurant+"/"+data[i].images[0].image;
                     }
                    $scope.promotions = data;
                }
               
        });
        
        
        
     
       
}]); 

