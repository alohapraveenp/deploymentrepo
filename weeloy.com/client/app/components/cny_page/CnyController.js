var CnyController = angular.module('CnyController', ['CnyService','TitleAndMetaTag']);
CnyController.controller('CnyCtrl', [
    '$rootScope', 
    '$scope',
    'TitleAndMeta',
    'Cny', function($rootScope, $scope, TitleAndMeta, Cny) {
        $scope.book_button_text = 'Book Now';
        var title = 'Chinese New Year 2016 with Weeloy - Reserve your table now';
        var description = 'Chinese New Year 2016 with Weeloy. Chinese new year restaurant menu. Reserve your table with one of the finest restaurants we have lined up for you and hurry!';
        TitleAndMeta.setTitle(title);
        TitleAndMeta.setMetaDescription(description);
        
        TitleAndMeta.setFaceBookMetaTitle(title);
        TitleAndMeta.setFaceBookMetaDescription(description);
        
        Cny.getCnyEvent().then(function(response) {
            if (response.status == 1 && response.count > 0) {
                $scope.events = response.data.events;
                $scope.landing = response.data.landing;
                $scope.image = "https://media.weeloy.com/upload/restaurant/event_lny/"+$scope.landing.images;
                console.log("getCnyEvent(): image: "+$scope.image);
            } else
                console.error("getCnyEvent(): error: "+JSON.stringify(response));
        });
        $scope.btnBookNowEvent_click = function(restaurant) { 
            var url = Cny.getInternalPath(restaurant) + "/booknow";
            document.location.href = "/" + url;
            
            //window.open(url, '_blank', 'toolbar=no, scrollbars=yes, resizable=yes, top=30, left=30, width=800, height=900');
        };
        $scope.getRestaurantLink = function(restaurant) { 
           return Cny.getInternalPath(restaurant);
        };
}]); 
