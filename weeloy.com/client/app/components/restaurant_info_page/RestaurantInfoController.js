(function(app) {
    app.controller('RestaurantInfoCtrl', RestaurantInfoCtrl);
    RestaurantInfoCtrl.$inject = [
        '$rootScope',
        '$scope',
        '$routeParams',
        '$window',
        '$timeout',
        '$location',
        'API',
        'TitleAndMeta',
        'Checkout',
        'AuditLog',
        'Notification',
        'deviceDetector',
    ];

    function RestaurantInfoCtrl(
        $rootScope,
        $scope,
        $routeParams,
        $window,
        $timeout,
        $location,
        API,
        TitleAndMeta,
        Checkout,
        AuditLog,
        Notification,
        deviceDetector
    ) {
        $rootScope.LoadFacebookSDK();
        $scope.showReviewSection = true;
        $scope.isMobile = deviceDetector.isMobile();
        $rootScope.page = 'restaurant';
        $scope.ItemsInCart = cart;
        //$scope.paginator = new Pagination(4);
        $scope.currentPage = 0;
        $scope.pageSize = 10;

        


        var restaurant = new Restaurant();
        var RestaurantID = restaurant.getRestaurantIdFromUrlParam($routeParams.city, $routeParams.restaurant);

        $scope.RestaurantID = RestaurantID;
        if ($scope.ItemsInCart.length > 0 && $scope.ItemsInCart[0].restaurant == RestaurantID) {
            $scope.showCart = true;
        }
        if ($scope.ItemsInCart.length > 0 && $scope.ItemsInCart[0].restaurant != RestaurantID) {
            $scope.showCartMessage = true;
        }

        config.ImageSizes.forEach(function(value) {
            if (value > $(window).width() && $scope.bannerSizePath === undefined) {
                $scope.bannerSizePath = '/' + value + '/';
                return false;
            }
        });

        API.restaurant.getFullInfo(RestaurantID)
            .then(function(result) {
                AuditLog.logevent(50, result.ID);
                result.setMediaServer($rootScope.mediaServer);
                
                   //kala added this line
                 $scope.BestOffers = result.best_offer;

                var title = result.getTitle() + ' - Book with Weeloy and Get Rewarded';
                var description = 'Book a Restaurant in ' + result.getCity() + ' at ' + result.getTitle() + ' with Weeloy and get yourself rewarded with exclusive deals and promotions. Simply book your table and spin the wheel!';
                TitleAndMeta.setTitle(title);
                TitleAndMeta.setMetaDescription(description);
                TitleAndMeta.setFaceBookMetaTitle(title);
                TitleAndMeta.setFaceBookMetaDescription(description);


                $scope.takeoutitle = (result.takeoutrestaurant > 0) ? '/ Take Out' : '';
                if (result.book_button.text.toLowerCase() == 'booking coming soon') {
                    $scope.disableBookButtton = true;
                }

                if (result.getChefType().toLowerCase().indexOf('female') > -1) {
                    $scope.chef_gender = 'female';
                } else if (result.getChefType().toLowerCase().indexOf('multiple') > -1) {
                    $scope.chef_gender = 'multiple';
                } 
                else {
                    $scope.chef_gender = 'male';
                }

                $scope.affiliate_program = false;


                //[pte_cpp_credit_suisse]
                if (typeof $rootScope.UserSession.affiliate_program !== "undefined" && $rootScope.UserSession.affiliate_program !== '') {
                    if ($rootScope.UserSession.affiliate_program === result.affiliate_program) {
                        $scope.affiliate_program = true;
                    }
                }

                //SG_SG_R_Audace
                
                $scope.restaurant = result;
                $scope.specials = {};
                if($scope.restaurant.restaurant == 'SG_SG_R_Pollen')
                {
                    $scope.specials = [ 
                                { item:'Wednesday - Monday'},
                                { item:'Lunch : 12:00 - 15:00 (Last Order 14:30)'}, 
                                { item:'AfternoonTea : 15:00 - 17:00 (Last Order 16:30)'},
                                { item:'Dinner : 18:00 - 22:00 (Last Order 21:30)'},  
                                { item:''},
                                { item:'Tuesday (Terrace)'},  
                                { item:'Lunch : 12:00 - 17:00'},
                                { item:'Dinner : 17:00 - 21:00 (Last Order 20:30)'}
                             ];
                }
                else if($scope.restaurant.restaurant == 'SG_SG_R_Audace')
                {
                    $scope.specials = [ 
                                { item:'Monday - Sunday'},
                                { item:'Breakfast : 07:30 - 10:00'}, 
                                { item:''}, 
                                { item:'Tuesday - Saturday'},  
                                { item:'Lunch : 11.30 - 14:00'},  
                                { item:'Afternoon Tea : 14:00 - 18:00'},
                                { item:'Dinner : 18:30 - 00:00 (Last Order 22:00)'},
                                { item:''},
                                { item:'Sunday'},
                                { item:'Brunch : 11:30pm - 16:00 (Last Order 15:00)'}
                             ];
                }

            })
            .catch(function(e) {
            });


        API.restaurant.getRestaurantService(RestaurantID)
            .then(function(result) {
                var RestaurantServices = [];
                result.forEach(function(service, key) {
                    var include = false;
                    RestaurantServices.forEach(function(value, k) {
                        if (value.category == service.getCategoryName()) {
                            include = true;
                            key = k;
                        }
                    });
                    if (include === false) {
                        var newService = {
                            category: service.getCategoryName(),
                            category_id: service.getCategoryId(),
                            category_icon: service.pico_categorie,
                            category_css: service.css_class_categorie,
                            services: [{
                                name: service.service,
                                icon: service.pico_service,
                                service_icon_css: service.css_class_service,
                            }],
                        };
                        RestaurantServices.push(newService);
                    } else {

                        service = {
                            name: service.service,
                            icon: service.pico_service,
                            service_icon_css: service.css_class_service,
                        };
                        RestaurantServices[key].services.push(service);
                    }
                });
                $scope.RestaurantServices = RestaurantServices;
                //console.log("RestaurantServices: "+JSON.stringify($scope.RestaurantServices));
            })
            .catch(function(e) {
            });

        var reviewPage = 1;
        getReviews(reviewPage);
        $scope.loadMoreReviews = function() {
            reviewPage++;
            getReviews(reviewPage, 'append');
        };

        function getReviews(page, type) {
             
            API.restaurant.getReviews(RestaurantID).then(function(response) {
                var tmp;
                if (response.status == 1) {
                    if (response.data.reviews !== undefined && RestaurantID !=='SG_SG_R_Bacchanalia' ) { //

                        response.data.reviews.forEach(function(value, key) {
                            moment.locale('en');
                            tmp = response.data.reviews[key].user_name;
                            try {
				if(typeof tmp === "string" && tmp.length > 0) {
                            		tmp = tmp.trim().replace(/ .*$/g, "").substring(0,7);
                            		}
                            } catch(e) { console.log(e.message); }
                            response.data.reviews[key].user_name = tmp; 
                            response.data.reviews[key].time = moment(value.post_date).format('MMM YYYY');
                        });
  
                        if (type == 'append') {
                            if (response.count === 0) {
                                $scope.hideLoadMoreReviews = true;
                            }
                            if (response.data.count === 0) {
                                $scope.hideLoadMoreReviews = true;
                            }
                            if(response.data.reviews.length < 4){
                                 $scope.hideLoadMoreReviews = true;
                            }
                            response.data.reviews.forEach(function(value) {
                                $scope.reviews.reviews.push(value);
                            });
                            
                        } else {
                            $scope.reviews = response.data;
                            if (response.data.count === 0 || response.data.count <= 4) {
                                $scope.hideLoadMoreReviews = true;
                            }
                            $scope.numberOfPages = function(){
                                if($scope.reviews.reviews){
                                    return Math.ceil($scope.reviews.reviews.length/$scope.pageSize);    
                                }
                            };
                        }
                    }
                }
            });
        }
        
        API.restaurant.getRestaurantPublicEvent(RestaurantID).then(function(response) {
            if (response.status == 1 && response.count > 0) {
                $scope.RestaurantEvents = [];
                response.data.event.forEach(function(value, key) {
                    console.log(value);
                    $scope.RestaurantEvents.push(new RestaurantEvent(value));
                });
                $scope.showEventSection = true;
            }
        });
        API.restaurant.getCateringPictures(RestaurantID, 3).then(function(response) {
            if (response.status == 1 && response.count > 0) {
                $scope.CateringPictures = response.data;
                $scope.showCateringPictures = true;
            }
        });
        
        //currently not in use
//        API.restaurant.getBestOffers(RestaurantID, 3).then(function(response) {
//            if (response.status == 1 && response.count > 0) {
//                //$scope.BestOffers = response.data;
//            }
//        });





        // $scope.btnBookNow_click = function(restaurant) {
        //     var url = restaurant.getInternalPath() + "/book-now";
        //     window.open(url, '_blank', 'toolbar=no, scrollbars=yes, resizable=yes, top=30, left=30, width=800, height=900');
        // };

        $scope.eventBooking = function(restaurant) {
            var url = restaurant.getInternalPath() + "/event/book-now";
            window.open(url, '_blank', 'toolbar=no, scrollbars=yes, resizable=yes, top=30, left=30, width=800, height=900');
        };

        $scope.btnOrderNow_click = function(restaurant) {
            var url = restaurant.getInternalPath() + "/catering";
            window.open(url, '_blank', 'toolbar=no, scrollbars=yes, resizable=yes, top=30, left=30, width=800, height=900');
        };

        $scope.takeout = function(MenuItem) {
            alert(MenuItem.mimage);
        };

        $scope.fbshare = function(restaurant) {

            var pictureUrl = restaurant.getBannerImage(),
                resTitle = restaurant.getTitle(),
                resDescription = restaurant.getDescription()[0].body[0];
            FB.init({
                appId: FB_ID,
                xfbml: true,
                cookie: true,
                version: 'v2.2'
            });

            var obj = {
                method: 'feed',
                link: "https://www.weeloy.com/" + restaurant.getInternalPath(),
                picture: 'https:' + pictureUrl,
                name: resTitle,
                caption: 'Book ' + resTitle + ' & win rewards with Weeloy',
                description: resDescription,
                display: 'popup'
            };

            FB.ui(obj, function(response) {

            });
        };


        $scope.showReview = function() {
            $scope.showReviewSection = true;
          
            setTimeout(function() {
                var offset = $('.reviews-section').offset();
        
                $("html, body").animate({
                    scrollTop: offset.top - 100,
                }, 500);
            }, 100);
        };

        $scope.Affix = function() {
            setTimeout(function() {
                var offset;
                offset = $('#affix-book-btn').offset();
                var LastElement = $('.right .last');
                $(window).scroll(function() {
                    BookBtnAffix(offset, LastElement);
                });
                $(window).resize(function() {
                    BookBtnAffix(offset, LastElement);
                });
            }, 500);
        };

        function BookBtnAffix(offset, LastElement) {
            if ($(window).scrollTop() > offset.top + 40) {
                LastElement.addClass('fixedTop');
                LastElement.css('margin-left', offset.left - 11);
                var info_right_margin_top = 300 + $('#shopping-cart').height() + 'px';
                $('.info_right').css('margin-top', info_right_margin_top);
                if ($(window).scrollTop() - offset.top + 60 < LastElement.height() - $('#shopping-cart').height()) {
                    LastElement.css('top', offset.top - $(window).scrollTop() + 40);
                } else {
                    LastElement.css('top', -180);
                }
            } else {
                LastElement.removeClass('fixedTop');
                LastElement.css('margin-left', 0);
                $('.info_right').css('margin-top', '0px');
            }
        }

        $scope.anchorAffix = function() {
            setTimeout(function() {
                var offset;
                offset = $('#merchant-detail-navigation').offset();
                $(window).scroll(function() {
                    AnchorNavAffix(offset);
                });
                $(window).resize(function() {
                    AnchorNavAffix(offset);
                });
            }, 500);
        };

        function AnchorNavAffix(offset) {
            var AnchorNav = $('#merchant-detail-navigation');
            if ($(window).scrollTop() > $('.banner_restaurant').height() - 40) {
                AnchorNav.css({
                    marginLeft: offset.left,
                    top: '40px',
                    left: '0px',
                    width: $('#breadcrumb').width(),
                    position: 'fixed',
                });
                $('#breadcrumb').css({
                    marginTop: '60px',
                });
            } else {
                AnchorNav.css({
                    marginLeft: '0px',
                    top: '0px',
                    position: 'relative',
                });
                $('#breadcrumb').css({
                    marginTop: '0px',
                });
            }
        }


        $scope.loadmap = function(lat, lng, isMobile) {
            if (document.getElementById('gmap-sdk') !== undefined && document.getElementById('gmap-sdk') !== null) {
                CreateMap(lat, lng, isMobile);
            } else {
                var script = document.createElement("script");
                script.type = "text/javascript";
                script.id = 'gmap-sdk';
                script.src = "//maps.google.com/maps/api/js?callback=CreateMap";
                document.body.appendChild(script);
            }
        };

        $scope.loadmap2 = function(lat, lng, isMobile) {
            alert('boom');
        };

        $window.CreateMap = function() {
            CreateMap($scope.restaurant.getLatitude().lat, $scope.restaurant.getLatitude().lng, $scope.isMobile);
        };

        function CreateMap(lat, lng, isMobile) {
            var myLatlng = new google.maps.LatLng(lat, lng);

            var mapOptions = {
                zoom: 14,
                center: myLatlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                scrollwheel: false,
                navigationControl: false,
                mapTypeControl: false,
            };
            if (isMobile) {
                mapOptions.scaleControl = false;
                mapOptions.draggable = false;
                mapOptions.zoomControl = false;
            }
            map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
            });
        }

        $scope.addToCart = function(item, evt) {
            var addItem = true;
            item.restaurant_path = $location.$$path;
            $scope.ItemsInCart.forEach(function(vaue) {
                if (vaue.ID == item.ID) {
                    addItem = false;
                }
            });
            if (!addItem) {
                $rootScope.$broadcast('IncreaseItemInCart', item.ID);
                return;
            }
            if ($scope.ItemsInCart.length > 0 && $scope.ItemsInCart[0].restaurant != item.restaurant) {
                Notification.show('warning', 'Can not add item from many restaurant.');
                return;
            }
            Checkout.addItem(item).then(function(response) {
                if (response.status == 1) {
                    $scope.ItemsInCart.push(item);
                    $rootScope.$broadcast('AddItemToCart', item);
                    $scope.showCart = true;
                } else {
                    alert(response.data);
                }
            });
        };

    }

    app.filter('day', function() {
        return function(str) {
            str = str.toLowerCase();
            switch (str) {
                case 'monday':
                    return 'Mon';
                case 'tuesday':
                    return 'Tue';
                case 'wednesday':
                    return 'Wed';
                case 'thursday':
                    return 'Thu';
                case 'friday':
                    return 'Fri';
                case 'saturday':
                    return 'Sat';
                case 'sunday':
                    return 'Sun';
                default:
                    return str;
            }
        };
    });
    app.filter('get_review_class', function() {
        'use strict';
        return function(input) {
            if (input === undefined) {
                return false;
            }

            var res = (Math.round(input * 2) * 0.5) * 10;

            return 'review-' + res;
        };
    });
})(angular.module('RestaurantInfoController', [
    'TrustAsResource',
    'TitleAndMetaTag',
    'ShoppingCart',
    'CheckoutService',
    'LengthFilter',
    'ReviewProfile',
    'weeloy.notify',
    'ng.deviceDetector',
    'PaginatorService',
    'app.shared.filters.timeFormat',
    'app.shared.directives.disablejump'
]));
$(document).ready(function() { 
   $('[data-toggle="popover"]').popover();
}); 
