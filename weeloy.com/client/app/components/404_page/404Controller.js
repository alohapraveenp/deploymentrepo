var F404Controller = angular.module('F404Controller', []);
    F404Controller.controller('F404Ctrl', ['$rootScope', '$scope', '$location', '$timeout', function($rootScope, $scope, $location, $timeout) {   
        $timeout(function() {
            $location.path('/');
        }, 3000);
}]);