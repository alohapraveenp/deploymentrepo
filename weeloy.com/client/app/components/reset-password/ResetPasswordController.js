(function(app) {
    app.controller('ResetPasswordCtrl', controller);

    controller.$inject = ['$rootScope', '$scope', '$window', '$location', 'API', 'Notification', 'loginService', '$timeout'];

    function controller($rootScope, $scope, $window, $location, API, Notification, loginService, $timeout) {
        $scope.success_update = false;
        $scope.success_update_bo = false;
        $scope.ResetPassword = function(password, password_confirmation) {
            var email = $location.$$search.email;
            $scope.redirect_to = $location.$$search.r_to;
            if (password != password_confirmation) {
                Notification.show('warning', 'Passwords do not match');
                return;
            }
            if (password.length < 6 || password.length > 12) {
                Notification.show('warning', 'Password must be between 6 and 12 characters');
                return;
            }
            API.account.reset(email, $location.$$search.token, password)
                .then(function(result) {
                    Notification.show('success', result);

                    if ($scope.redirect_to === 'backoffice') {
                        $scope.success_update_bo = true;
                        $timeout(function() {
                            $window.location.href = 'https://www.weeloy.com/backoffice';
                        }, 2000, false);
                    } else {

                        $scope.success_update = true;


                        loginService.login(email, password)
                            .then(function(result) {
                                $rootScope.$broadcast('UserLogin', result);
                                $scope.loggedin = true;
                                $scope.user = result;
                                $('#loginModal').modal('hide');
                                var msg = 'You have successfully logged in, ' + result.getFirstName();
                                Notification.show('success', msg);
                                $timeout(function() {
                                    $window.location.href = 'https://www.weeloy.com';
                                }, 2000, false);
                            }).catch(function(error) {
                                Notification.show('warning', error);
                            });
                    }
                })
                .catch(function(error) {
                    Notification.show('warning', error);
                });
        };
    }
})(angular.module('ResetPasswordController', ['weeloy.notify', 'LoginService', 'UserValidation']));
