var DailySpecialService = angular.module('DailySpecialService', []);
DailySpecialService.service('DailySpecial', ['$http', '$q', function($http, $q) {
    this.getDailySpecial = function() {
        var defferred = $q.defer();
        // local var API_URL = 'http://localhost:8888/weeloy.com/api/md_marketing/getdailyspecial/';
         // var API_URL = 'https://dev.weeloy.asia/api/md_marketing/getdailyspecial/';
        var API_URL = 'api/dailyboard.php/md_dailyspecial/getdailyspecial';
        //var API_URL = 'https://dev.weeloy.asia/api/dailyboard.php/md_dailyspecial/getdailyspecial';
        $http.post(API_URL, {
            "body":{
              "data": {
                "code": ""  
              }
            }
        }).success(function(response) {
            defferred.resolve(response);
        });
        return defferred.promise;
    };

}]);
