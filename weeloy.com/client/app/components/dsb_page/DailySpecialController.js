var DailySpecialController = angular.module('DailySpecialController', ['NfSearch','DailySpecialService', 'ngFileUpload']);

DailySpecialController.controller('DailySpecialCtrl', [
    '$rootScope',
    '$scope',
    '$location',
    'DailySpecial',
    '$sce',
    '$window',
    'Upload',
    '$http',
    function($rootScope, $scope, $location, DailySpecial, $sce, $window,Upload,$http) {
        
        $scope.dsbfrontpage = "https://media.weeloy.com/upload/restaurant/event_mother/mothersday2.jpg";
        $scope.restaurant = null;
        $scope.items = [];
        $scope.itemstemp = [];
        $scope.temp=null;
        $scope.cuisinelist = [];
        $scope.today = new Date();
        $scope.todaydate = new Date();
        $scope.todaydateArr = $scope.todaydate.toDateString().split(' ');
        $scope.todaydateFormat = $scope.todaydateArr[1] + ' ' + $scope.todaydateArr[2] + ' ' + $scope.todaydateArr[3];
        $scope.loadingimg = true;
        $scope.detailsshow = null;
        $scope.restourl = null;
        $scope.showbooking = false;
        $scope.btnbooking = true;

        //$scope.today.setDate($scope.today.getDate() +1);
        $scope.today = $scope.today.getFullYear() + "-" +   ("0"+($scope.today.getMonth()+1)).slice(-2) + "-" + ("0" + $scope.today.getDate()).slice(-2);

        $scope.loading = true;  
        var index = 0;
        
        //get and load the daily specials
        DailySpecial.getDailySpecial().then(function(response) {
          $scope.dsblist = null;
            var dsblistcheck = response.status;
            var dsblistchecks = response.count;
            $scope.loadingimg = false;
            if(dsblistcheck == 1)
            {
              var dsblist = response.data;
              
              if(dsblistchecks === '0')
              {
                alert("There are no Daily Specials for Today");
              }

              for (i = 0; i < dsblist.length; i++) {
                    cc = dsblist[i];
                    $scope.itemstemp[i] = cc;
                  }
                  $scope.more();  
            }
            else
            {
              $scope.loading = false;
            } 
           
        });

        $scope.more = function()
        {
          $scope.loading = false;
          temp = $scope.itemstemp.slice(index, index+12);
              for (x = 0; x < temp.length; x++) {
                    cc = temp[x];
                    $scope.items.push(cc);
              }
              index=index+12;

        };

        $scope.closemodal = function()
        {
          $('#dbsformmodallist').modal('toggle');
        };

        $scope.showdetails = function(oo)
        {
          $scope.showbooking = false;
          $scope.btnbooking = true;
          $scope.detailsshow = oo;
          console.log($scope.detailsshow);
          $('#dbsformmodallist').modal('show');
        };

        $scope.booknowmodalmain = function(id)
        {
          
          $scope.restourl =$sce.trustAsResourceUrl('https://www.weeloy.com/modules/booking/book_form.php?bkrestaurant='+id+'&#038;bktracking=WEBSITE&#038;');
          $('#dbsformmodalbooknow').modal('show');
        };

        $scope.booknowmodal = function(id)
        {
          $scope.btnbooking = false;
          $scope.showbooking = true;
          $scope.restourl =$sce.trustAsResourceUrl('https://www.weeloy.com/modules/booking/book_form.php?bkrestaurant='+id+'&#038;bktracking=WEBSITE&#038;');
          //$('#dbsformmodalbooknow').modal('show');
        };

        $scope.registernewresto = function()
        {
          $('#dbsformmodalregistration').modal('show');
        };
    }]);

// we create a simple directive to modify behavior of <div>
DailySpecialController.directive("whenScrolled", function(){
  return{
    
    restrict: 'A',
    link: function(scope, elem, attrs){
    
      raw = elem[0];

      elem.bind("scroll", function(){
        if(raw.scrollTop+raw.offsetHeight+5 >= raw.scrollHeight){
          scope.loading = true;          
        // we can give any function which loads more elements into the list
          scope.$apply(attrs.whenScrolled);
        }
      });
    }
  };
});



