function DataBooking() {

    var i, j, timeslotAr = [],
            persAr = [],
            hourslotAr = [],
            minuteslotAr = [];

//    for (i = 1; i < 10; i++)
//        persAr.push(i);

    for (i = 9; i < 24; i++)
        for (j = 0; j < 60; j += 15) {
            timeslotAr.push((i < 10 ? '0' : '') + i + ':' + (j === 0 ? '0' : '') + j);
        }

    for (i = 9; i < 24; i++)
        hourslotAr.push((i < 10 ? '0' : '') + i);

    for (i = 0; i < 60; i += 5)
        minuteslotAr.push((i < 10 ? '0' : '') + i);

    return {
        name: "",
        npers: 0,
        selecteddate: null,
        ntimeslot: 0,
        specialrequest: "",
        timeslot: timeslotAr,
        hourslot: hourslotAr,
        minuteslot: minuteslotAr,
        pers: persAr,
        opers: 0,
        otime: '',
        odate: '',
        event: ['Birthday', 'Wedding', 'Reunion'],
        opened: false,
        minDate: null,
        maxDate: null,
        dateOptions: {
            startingDay: 1
        },
        setInit: function (ddate, rtime, npers, name, specialrequest, firstname, lastname, minpax, maxpax) {

            for (i = minpax; i <= maxpax; i++)
                persAr.push(String(i));

            this.pers = persAr;
            this.timeslot = timeslotAr;
            this.hourslot = hourslotAr;
            this.minuteslot = minuteslotAr;
            this.ntimeslot = rtime.substring(0, 5);
            this.name = name;
            this.npers = npers;
            this.specialrequest = specialrequest;

            if (ddate instanceof Date === false) {
                var t0, t1;
                t0 = ddate.split('-');
                t1 = this.ntimeslot.split(':');
                this.selecteddate = new Date(parseInt(t0[0]), parseInt(t0[1]) - 1, parseInt(t0[2]), parseInt(t1[0]), parseInt(t1[1]), 0);
            } else
                this.selecteddate = ddate;

            this.odate = this.selecteddate;
            this.otime = this.ntimeslot;
            this.opers = npers;
            this.firstname = firstname;
            this.lastname = lastname;

            this.minDate = new Date();
            this.maxDate = new Date();
            this.maxDate.setTime(this.maxDate.getTime() + (60 * 24 * 3600 * 1000)); // remainingday  120 days
        },
        //        dateopen: function($event) {
        //            this.opened = true;
        //        
        //            $event.preventDefault();
        //            $event.stopPropagation();
        //        },

        calendar: function () {
            var cdate = this.selecteddate;
            this.selecteddate = ((cdate.getDate() <= 9) ? '0' : '') + cdate.getDate() + "/" + ((cdate.getMonth() <= 9) ? '0' : '') + (cdate.getMonth() + 1) + "/" + cdate.getFullYear();
        },
        ischange: function () {
            return !(this.npers === this.opers && this.ntimeslot === this.otime && this.selecteddate === this.odate);
        }
    };
}

var MyBookingController = angular.module('MyBookingController', ['ui.bootstrap', 'MyBookingReview', 'AuditLog']);

MyBookingController.controller('MyBookingCtrl', [
    '$rootScope',
    '$scope',
    '$location',
    '$timeout',
    'AuditLog',
    'API',
    function ($rootScope, $scope, $location, $timeout, AuditLog, API) {
        $rootScope.LoadFacebookSDK();
        $rootScope.checkLoggedin();

        $scope.rating = 5;

        $rootScope.MenuUserSelected = 'mybookings';
        if ($location.$$search.f === undefined) {
            $location.search('f', 'today');
            return;
        }
        $rootScope.page = 'my_bookings';
        $rootScope.audit_other = $location.$$search.f;
        AuditLog.logevent(50, '');

        $scope.mydata = new DataBooking();
        $scope.mydata.submit = function () {
            var alertmsg = "showAlert_" + $scope.modifycount;

            $scope[alertmsg] = false;
            if ($scope.mydata.ntimeslot === 'not available for lunch' || $scope.mydata.ntimeslot === 'not available for dinner') {

                $scope.msg = "Date and time Invalid!..";
                $("#invite-alert-" + $scope.modifycount).removeClass();
                $scope[alertmsg] = true;
                $("#invite-alert-cancel-" + $scope.modifycount).addClass('ng-show error-msg');
                $scope.close_msg($scope.modifycount);
                return false;

            }

            API.mybooking.updateBooking($scope.mydata).then(function (response) {
                if (response.data == 1) {
                    var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
                    var weekday = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
                    var rdate = $scope.mydata.selecteddate.split('-');
                    var rtime = $scope.mydata.ntimeslot.split(':');
                    $scope.bookings[$scope.modifycount].rdate = $scope.mydata.selecteddate;
                    $scope.bookings[$scope.modifycount].rtime = $scope.mydata.ntimeslot;
                    var d = new Date(parseInt(rdate[0]), parseInt(rdate[1] - 1), parseInt(rdate[2]), rtime[0], rtime[1]);
                    $scope.bookings[$scope.modifycount].timeStr = monthNames[d.getMonth()] + ' ' + d.getDate() + ', ' + d.getUTCFullYear();
                    $scope.bookings[$scope.modifycount].cover = $scope.mydata.npers;
                    $scope.bookings[$scope.modifycount].specialrequest = $scope.mydata.specialrequest;

                    AuditLog.logevent(103, '');

                    $scope.msg = "SuccessFully Updated your Booking details!..";
                    $("#invite-alert-" + $scope.modifycount).removeClass();
                    $scope[alertmsg] = true;
                    $("#invite-alert-cancel-" + $scope.modifycount).addClass('success-msg ');
                    $scope.close_msg($scope.modifycount);
                    $scope.close_container($scope.modifycount);
                    //$scope[isopen] = false;
                } else {
                    $scope.msg = "SuccessFully Updated your Booking details!..";
                    $("#invite-alert-" + $scope.modifycount).removeClass();
                    $scope[alertmsg] = true;
                    $scope.close_container($scope.modifycount);
                    //window.location.reload();
                }

            });

        };


        $scope.close_container = function (mode) {
            var isopen = "showModifSection_" + mode;
            $timeout(function () {
                $scope[isopen] = false;
                console.log("isopen=" + $scope[isopen]);
            }, 2000);
        };
        $scope.mydata.cancel = function () {

            $scope.toggleview($scope.itemCount, 'showCancelSection_');
        };

        $scope.error12 = 'not available for lunch';
        $scope.error13 = 'not available for dinner';
        $scope.updateTimerScope = function (n, bktime) {
            if (n < 0) {
                n = 0;
            }

            $scope.curday = n;
            $scope.lunch = [];
            $scope.dinner = [];
            nchar_lunch = 4;
            nchar_dinner = 4;
            index = n * nchar_lunch;
            patlunch = parseInt('0x' + $scope.lunchdata.substring(index, index + nchar_lunch));
            index = n * nchar_dinner;
            console.log("n=" + n);
            patdinner = parseInt('0x' + $scope.dinnerdata.substring(index, index + nchar_dinner));

            var curval = $scope.bktime;
            found = 0;
            localtime = "";
            limit = nchar_lunch * 4;

            for (log = i = 0, k = 1; i < limit; i++, k *= 2)
                if ((patlunch & k) === k) {
                    log++;
                    ht = (9 + Math.floor(i / 2)) + ':' + ((i % 2) * 3) + '0';
                    if (ht === curval)
                        found = 1;
                    $scope.lunch.push(ht);
                }

            if (log === 0) {

                $scope.lunch.push($scope.error12); // 'not available for lunch'
                localtime = $scope.error12;

                $scope.mydata.ntimeslot = "";
            }

            limit = nchar_dinner * 4;
            //if(limit > 17) limit = 16;    // minight
            for (log = i = 0, k = 1; i < limit; i++, k *= 2)
                if ((patdinner & k) === k) {
                    log++;
                    ht = (16 + Math.floor(i / 2)) + ':' + ((i % 2) * 3) + '0';
                    if (ht === curval)
                        found = 1;
                    $scope.dinner.push(ht);
                }
            if (log === 0) {
                $scope.dinner.push('not available for dinner'); //'not available for dinner'
                localtime = 'not available for dinner';

                $scope.mydata.ntimeslot = "";
            }

            curval = bktime;
            if (localtime.substr(0, 5) === 'close') {
                curtime = localtime;
                $scope.bktime = curtime;
            } else if (curval.substr(0, 5) === 'close' || found === 0) {
                $scope.bktime = '';
            }



        };
        var dropdown_label = ['cover', 'date', 'mtime'];

        $scope.modif = function (index, item, obj) {
            console.log(JSON.stringify(item));
            $("#invite-alert-cancel-" + index).css('display', 'none');

            //$scope['showAlert_'+index]=false;
            $scope.msg = "";

            for (i = 0; i < dropdown_label.length; i++)
                $scope[dropdown_label[i] + 'opened'] = false;

            var ele = $("#member-bklist >#btncontainer > .currentpanel").attr('id');
            if ($("#member-bklist >#btncontainer > .currentpanel").hasClass('active')) {
                $("#member-bklist >#btncontainer > .currentpanel").hide();
            }
            var op = obj.target.getAttribute("data-target");
            $("#" + op).show();
            $("#" + ele).addClass("active");

            $scope.restaurant = item.restaurantinfo.restaurant;
            $scope.modifycount = index;
            API.mybooking.allotment(item.restaurantinfo.restaurant).then(function (response) {

                $scope.lunchdata = response.data.lunchdata;
                $scope.dinnerdata = response.data.dinnerdata;
                var nday = $scope.numberOfDay(item.rdate, item.rtime);

                $scope.updateTimerScope(nday, item.rtime);
            });

            // nday = $scope.numberOfDay($scope.bkdate);
            //$scope.updateTimerScope(nday);

            $scope.mydata.setInit(item.rdate, item.rtime, item.cover, item.confirmation, item.specialrequest, item.firstname, item.lastname, item.restaurantinfo.dfminpers, item.restaurantinfo.dfmaxpers);

            $scope.toggleview(index, 'showModifSection_');
            //            $scope.toggleview(index, 'showCancelServey_');
            //        $scope.toggleview(index, 'showCancelSection_');
        };






        $scope.updatendays = function () {
            var nday;
            var bdate = $scope.mydata.selecteddate;
            console.log(bdate);


            //data.selecteddate = ((cdate.getDate() <= 9) ? '0' : '') + cdate.getDate() + "/" + ((cdate.getMonth() <= 8) ? '0' : '') + (cdate.getMonth() + 1) + "/" + cdate.getFullYear();
            bdate = bdate.getFullYear() + "-" + ((bdate.getMonth() <= 8) ? '0' : '') + (bdate.getMonth() + 1) + "-" + ((bdate.getDate() <= 9) ? '0' : '') + bdate.getDate();
            $scope.bkdate = bdate;
            console.log("nday=" + $scope.bkdate);
            nday = $scope.numberOfDay($scope.bkdate, $scope.mydata.ntimeslot);

            $scope.updateTimerScope(nday, $scope.mydata.ntimeslot);
            $scope.dateopened = ($scope.dateopened) ? false : true;


        };

        //update booking 
        $scope.numberOfDay = function (aDate, aTime) {
           
            var dateAr = aDate.split('-');

            var date = new Date(dateAr[0], parseInt(dateAr[1]) - 1, dateAr[2], 23, 59, 59);

            return Math.floor(((date.getTime() - Date.now()) / (24 * 60 * 60 * 1000)));
//            var dateAr = aDate.split('-');
//            console.log(dateAr);
//            var timeAr = aTime.split(':');
//            var date = new Date(parseInt(dateAr[0]), parseInt(dateAr[1] - 1), parseInt(dateAr[2]), timeAr[0], timeAr[1]);
//            return Math.floor(((date.getTime() - Date.now()) / (24 * 60 * 60 * 1000)));
        };
//        $scope.numberOfDayts = function(aDate) {
//		var dateAr = aDate.split('-');
//                 console.log(dateAr);
//		var date = new Date(dateAr[2], parseInt(dateAr[1]) - 1, dateAr[0], 23, 59, 59);
//		return Math.floor(((date.getTime() - Date.now()) / (24 * 60 * 60 * 1000)));
//	};


        $scope.localhost = ($location.$$host === 'localhost');
        $scope.MyBookingPage = $location.$$search.f;
        var bookings = API.mybooking.get($location.$$search.f);
        $rootScope.LoadFacebookSDK();
        bookings.then(function (response) {

            if (response.status == 1) {
                var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
                var weekday = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
                if (response.data.bookings !== undefined && response.data.bookings !== null) {
                    $scope.showBookingList = true;

                    response.data.bookings.forEach(function (value, key) {

                        var rdate = value.rdate.split('-');
                        var rtime = value.rtime.split(':');

                        var d = new Date(parseInt(rdate[0]), parseInt(rdate[1] - 1), parseInt(rdate[2]), rtime[0], rtime[1]);
                        var currentTime = new Date();
                        response.data.bookings[key].time = d;
                        if (currentTime.getTime() - d.getTime() > 0) {
                            response.data.bookings[key].passed = true;
                        } else {
                            response.data.bookings[key].passed = false;
                        }

                        response.data.bookings[key].paymentUrl = response.data.bookings[key].restaurantinfo.internal_path + "/" + response.data.bookings[key].confirmation + "/deposit_pendingpayment_method?action=pending_payment&mode=bklist";
                        response.data.bookings[key].timeStr = monthNames[d.getMonth()] + ' ' + d.getDate() + ', ' + d.getUTCFullYear();
                        if (response.data.bookings[key].restaurantinfo.dfmaxpers < 4) {
                            response.data.bookings[key].restaurantinfo.dfmaxpers = 4;
                        }
                        if (response.data.bookings[key].restaurantinfo.dfminpers < 1 || response.data.bookings[key].restaurantinfo.dfminpers > 3) {

                            response.data.bookings[key].restaurantinfo.dfminpers = 1;
                        }

                        //response.data.bookings[key].image = 'https://media3.weeloy.com/upload/restaurant/SG_SG_R_TheFunKitchen/2_The_fun_kitchen.jpg';
                    });
                    $scope.bookings = response.data.bookings;
                } else {
                    $scope.showBookingList = false;
                }
            }
        });

        //get cancel policy details

        $scope.cancelPolicy = function (restaurant, payment_method, amount, product, rdate, rtime, pax) {


            if (restaurant === "SG_SG_R_TheFunKitchen" || restaurant === "SG_SG_R_Bacchanalia") {
                console.log("rdate =" + rdate);
                API.mybooking.bacchanaliacancelpolicy(restaurant, rdate, rtime, pax).then(function (response) {

                    var data = response.data.data;
                    if (typeof data !== 'undefined') {
                        console.log("POLICY=" + JSON.stringify(data.priceDetails));
                        $scope.policybaccahanalia = data.priceDetails.message;
                    }
                });
            } else {
                API.mybooking.getCancelPolicy(restaurant, payment_method, amount, product).then(function (response) {
                    var data = response.data;
                    var temArr = [];


                    if (typeof response.data !== 'undefined' && response.data !== "") {
                        $scope.cancelmsg = data.message;
                        $scope.policy = data.range;
                        $scope.freerange = data.lastRange;

                    }

                });
            }
        };

        $scope.cancel = function (index, item, obj) {


            var ele = $("#member-bklist >#btncontainer > .currentpanel").attr('id');
            if ($("#member-bklist >#btncontainer > .currentpanel").hasClass('active')) {
                $("#member-bklist >#btncontainer > .currentpanel").hide();
            }
            var op = obj.target.getAttribute("data-target");
            $("#" + op).show();
            $("#" + ele).addClass("active");
            var indexvalue = "showCancelServey_" + index,
                    currentpanel = "showCancelSection_" + index;
            var cancelConfirmation = "showCancelConfirmation_" + index;
            $scope.showReviewSection = false;
            if (item.bkObject === 'nodeposit') {
                $scope[currentpanel] = false;
            }
            $scope[indexvalue] = false;
            $scope.showInviteSection = false;
            $scope.CancelItem = item;
            $scope[cancelConfirmation] = false;
            $scope.itemCount = index;
            $scope.toggleview(index, 'showCancelServey_');
            $scope.toggleview(index, 'showCancelSection_');
            if (item.bkObject === 'nodeposit') {
                $scope.toggleview(index, 'showModifSection_');
            }

            if (item.bkObject === 'deposit') {
                $scope.cancelPolicy(item.restaurantinfo.restaurant, item.payment_method, item.amount, item.product, item.rdate, item.rtime, item.cover);
            }

            AuditLog.logevent(101, item.bookid);

        };


        $scope.toggleview = function (index, label) {

            var indexvalue = label + index;
            $scope.localhost = ($location.$$host === 'localhost');
            $scope[indexvalue] = (typeof $scope[indexvalue] === 'undefined' || $scope[indexvalue] === false) ? true : false;

            $scope.itemCount = index;

        };
        $scope.cleardropdown = function (selector) {

            for (i = 0; i < dropdown_label.length; i++)
                if (selector != dropdown_label[i]) {
                    //console.log("selector=" + selector + "dropdownlable=" + dropdown_label[i]);
                    $scope[dropdown_label[i] + 'opened'] = false;
                }

        };



        $scope.opendropdown = function ($event, selector) {

            $scope.cleardropdown(selector);
            console.log("selector=" + selector + "<>" + $scope[selector + 'opened']);
            $scope[selector + 'opened'] = ($scope[selector + 'opened']) ? false : true;
//            $timeout(function () {
//                  if (selector == "date"){
//                      $scope.dateopened =  ($scope.dateopened) ? false : true;
//                  }else{
//                    $scope[selector + 'opened'] = ($scope[selector + 'opened']) ? false : true;
//                }
//            });
            console.log("scope=" + $scope[selector + 'opened']);
//                if (selector == "date" || true) {
//			$event.preventDefault();
//			$event.stopPropagation();
//		}


        };


        $scope.Review = function (index, item, obj) {
    

            $scope.ReviewItem = item;
            $scope.ReviewItem.count = index;
            var review = {};
            $scope.review = review;
            review.confirmation = item.confirmation;
            review.restaurant = item.restaurantinfo.restaurant;
            $scope.currIndex = index;
            var ele = $("#member-bklist >#btncontainer > .currentpanel").attr('id');
            if ($("#member-bklist >#btncontainer > .currentpanel").hasClass('active')) {
                $("#member-bklist >#btncontainer > .currentpanel").hide();
            }

            var op = obj.target.getAttribute("data-target");
            $("#" + op).show();
            $("#" + ele).addClass("active");
            var indexvalue = "showReviewSection_" + index;
            $scope[indexvalue] = true;



        };
        $scope.ReviewNow = function (review) {

            var data = {
                confirmation_id: $scope.review.confirmation,
                user_id: $rootScope.user.email,
                restaurant_id: $scope.review.restaurant,
                grade: $scope.grade,
                type: 'update',
                comment: $scope.review.comment,
            };

            API.mybooking.reviewPostGrade(data).then(function (response) {
                AuditLog.logevent(105, $scope.review.confirmation);
                window.location.reload();
            });

        };


        $scope.cancelConfirmation = function (count, item) {
            var cancelmaincontainer = "showCancelConfirmation_" + count,
                    indexvalue = "showCancelServey_" + count,
                    slName = "cl-servey_" + count,
                    alertmsg = "showAlert_" + count;
            $scope.servey = $('input[name=' + slName + ']:radio:checked').val();
            $scope[alertmsg] = false;
            $scope.msg = "Please select the main reason for your cancellation.!!";

            if ($scope.servey === undefined) {

                $("#invite-alert-cancel-" + count).addClass('error-msg');
                $scope[alertmsg] = true;
                $scope.close_msg(count);
                return false;

            }
            if ($scope.servey === 'Other') {

                if ($scope.other === "" || $scope.other === undefined) {
                    $("#invite-alert-cancel-" + count).addClass('error-msg');
                    $scope[alertmsg] = true;

                    this.allotment = function (restaurant) {

                        var defferred = $q.defer();
                        var API_URL = 'api/restaurant/allote/' + restaurant;
                        $http.get(API_URL, {
                            cache: true,
                        }).success(function (response) {
                            defferred.resolve(response);
                        });
                        return defferred.promise;

                    };
                    $scope.close_msg(count);
                    return false;
                } else {
                    $scope.servey = $scope.other;
                }

            }

            API.mybooking.getCancelConfirmation(item).then(function (response) {
                $scope[indexvalue] = false;
                $scope.msg = "";
                $scope.bookingData = response.data;
                //$scope.showCancelServey = false;
                $scope[cancelmaincontainer] = true;
                $scope.showInviteSection = false;
                console.log($scope[cancelmaincontainer]);
            });
        };
        $scope.close_msg = function (count) {

            $timeout(function () {
                $('#invite-alert').fadeOut('slow');
                $("#invite-alert-cancel-" + count).fadeOut('slow');
            }, 1000); // <-- time in milliseconds  
        };
        $scope.confirmCancel = function (item) {
            API.mybooking.CancelBooking(item, $scope.servey).then(function (response) {

                if (response.data == 'OK') {
                    $scope.msg = "";
                    window.location.reload();
                } else {
                    $scope.showCancelServey = false;
                    $scope.showCancelConfirmation = true;
                    $scope.showInviteSection = false;
                    $scope.msg = response.errors;
                    $("#invite-alert-cancel").addClass('error-msg');
                    $scope.showAlert = true;
                }
            });
        };

        $scope.rateFunction = function(rating) {
            $scope.grade = rating;
        };
        $scope.cancelbookingrefund = function (item) {
            API.mybooking.cancelbookingrefund(item, $scope.servey).then(function (response) {

                if (response.data == 'OK') {
                    $scope.msg = "";
                    window.location.reload();
                } else {
                    $scope.showCancelServey = false;
                    $scope.showCancelConfirmation = true;
                    $scope.showInviteSection = false;
                    $scope.msg = response.errors;
                    $("#invite-alert-cancel").addClass('error-msg');
                    $scope.showAlert = true;
                }
            });

        };


        $scope.inviteFrd = function (item) {
            $scope.showInviteSection = true;
        };

        $scope.fbshare = function (item) {

            var description, caption;
            var picture = 'https://media3.weeloy.com/upload/restaurant/' + item.restaurantinfo.restaurant + '/1440/' + item.restaurantinfo.defImg;
            if (item.canceldate !== null) {
                description = "I regret having to cancel my reservation" + item.wheelwin + " @ " + item.restaurantinfo.title + " #weeloy";
                caption = "Booking Cancel " + "@ " + item.restaurantinfo.title;
            } else {
                description = "I just enjoyed " + item.wheelwin + " @ " + item.restaurantinfo.title + " #weeloy";
                caption = item.wheelwin + " @ " + item.restaurantinfo.title;
            }

            var obj = {
                method: 'feed',
                link: 'https://www.weeloy.com/' + item.restaurantinfo.internal_path,
                picture: picture,
                name: item.restaurantinfo.title,
                caption: caption,
                description: description,
                display: 'popup'
            };
            console.log(JSON.stringify(obj));

            FB.ui(obj, function (response) {
                AuditLog.logevent(102, item.restaurantinfo.restaurant);
            });
        };
        $scope.tws_click = function (item) {
            AuditLog.logevent(106, item.restaurantinfo.restaurant);
            var twtTitle = item.restaurantinfo.title,
                    twtUrl = 'https://www.weeloy.com/' + item.restaurantinfo.internal_path,
                    maxLength = 140 - (twtUrl.length + 1),
                    twtLink = 'http://twitter.com/home?status=' + encodeURIComponent(twtTitle + ' ' + twtUrl);
            window.open(twtLink, "_blank", "toolbar=yes, scrollbars=yes, resizable=no, width=600, height=400");
        };


        



    }
]);
