var InfoContactController = angular.module('InfoContactController', []);
InfoContactController.controller('InfoContactCtrl', ['$rootScope', '$scope', 'API', 'AuditLog', function($rootScope, $scope, API, AuditLog) {
        $rootScope.page='contact_us';
    $scope.cities = [{
        id: 1,
        name: 'Singapore'
    }, {
        id: 2,
        name: 'Bangkok'
    }, {
        id: 3,
        name: 'Phuket'
    }, {
        id: 4,
        name: 'Hong Kong'
    }, ];
  AuditLog.logevent(50, ''); 
    $scope.contact = new Object();
    $scope.contact.city = $scope.cities[0];
    $scope.ContactSubmit = function(contact) {
        API.contact.sendContact(contact.firstname, contact.lastname, contact.email, contact.message).then(function(response) {
              AuditLog.logevent(130, ''); 
            $scope.showSuccessMessage = true;
        });
    };
}]);
