var EventController = angular.module('EventController', ['EventService']);
EventController.controller('EventCtrl', [
    '$rootScope', 
    '$scope',
    
    'Event', function($rootScope, $scope, Event) {
        $scope.book_button_text = 'Book Now';
        
        Event.getEventEvent().then(function(response) {
            if (response.status == 1 && response.count > 0) {
                $scope.events = response.data;
                console.log(response);
            }
        });
        $scope.btnBookNowEvent_click = function(restaurant) { 
            var url = Event.getInternalPath(restaurant) + "/booknow";
            document.location.href = "/" + url;
            //window.open(url, '_blank', 'toolbar=no, scrollbars=yes, resizable=yes, top=30, left=30, width=800, height=900');
        };
        $scope.getRestaurantLink = function(restaurant) { 
           return Event.getInternalPath(restaurant);
        };
}]); 
