var RestaurantGroupController = angular.module('RestaurantGroupController', [
    'TitleAndMetaTag'
]);
RestaurantGroupController.controller('RestaurantGroupCtrl', [
    '$rootScope',
    '$scope',
    '$routeParams',
    'API',
    'TitleAndMeta',
    function($rootScope, $scope, $routeParams, API, TitleAndMeta) {


        var GroupName = $routeParams.groupname;
        API.group.getGroup(GroupName)
            .then(function(response) {
                $scope.info = response[0];
                $scope.restaurants = response[1];

                var title = $scope.info.page_title;
                var description = $scope.info.page_description;
                TitleAndMeta.setTitle(title);
                TitleAndMeta.setMetaDescription(description);
                TitleAndMeta.setFaceBookMetaTitle(title);
                TitleAndMeta.setFaceBookMetaDescription(description);
                    
            });
    }
]);


