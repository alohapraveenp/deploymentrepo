WeeloyApp.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
           
    $routeProvider
        .when('/', {
            templateUrl: '../app/components/home/_home.tpl.html',
            controller: 'HomeCtrl',
        })
        .when('/index.html', {
            templateUrl: '../app/components/home/_home.tpl.html',
            controller: 'HomeCtrl',
        })
        .when('/index.php', {
            templateUrl: '../app/components/home/_home.tpl.html',
            controller: 'HomeCtrl',
        })
//        .when('/newhome', {
//            templateUrl: '../app/components/home/_newhome.tpl.html',
//            controller: 'HomeCtrl',
//        })
        .when('/search', {
            templateUrl: '../app/components/search_page/_search_page.tpl.html',
            controller: 'SearchCtrl',
        })
        .when('/search/:city', {
            templateUrl: '../app/components/search_page/_search_page.tpl.html',
            controller: 'SearchCtrl',
        })
        .when('/search/:city/:query', {
            templateUrl: '../app/components/search_page/_search_page.tpl.html',
            controller: 'SearchCtrl',
        })
        .when('/search/:city/:param_name_1/:param_1', {
            templateUrl: '../app/components/search_page/_search_page.tpl.html',
            controller: 'SearchCtrl',
        })
        .when('/search/:city/:query/:param_name_1/:param_1', {
            templateUrl: '../app/components/search_page/_search_page.tpl.html',
            controller: 'SearchCtrl',
        })
        .when('/search/:city/:param_name_1/:param_1/:param_name_2/:param_2', {
            templateUrl: '../app/components/search_page/_search_page.tpl.html',
            controller: 'SearchCtrl',
        })
        .when('/search/:city/:query/:param_name_1/:param_1/:param_name_2/:param_2', {
            templateUrl: '../app/components/search_page/_search_page.tpl.html',
            controller: 'SearchCtrl',
        })
        .when('/search/:city/:param_name_1/:param_1/:param_name_2/:param_2/:param_name_3/:param_3', {
            templateUrl: '../app/components/search_page/_search_page.tpl.html',
            controller: 'SearchCtrl',
        })
        .when('/search/:city/:query/:param_name_1/:param_1/:param_name_2/:param_2/:param_name_3/:param_3', {
            templateUrl: '../app/components/search_page/_search_page.tpl.html',
            controller: 'SearchCtrl',
        })
        .when('/search/:city/:param_name_1/:param_1/:param_name_2/:param_2/:param_name_3/:param_3/:param_name_4/:param_4', {
            templateUrl: '../app/components/search_page/_search_page.tpl.html',
            controller: 'SearchCtrl',
        })
        .when('/search/:city/:query/:param_name_1/:param_1/:param_name_2/:param_2/:param_name_3/:param_3/:param_name_4/:param_4', {
            templateUrl: '../app/components/search_page/_search_page.tpl.html',
            controller: 'SearchCtrl',
        })
        .when('/restaurant/:city/:restaurant', {
            templateUrl: '../app/components/restaurant_info_page/_restaurant_info_page.tpl.html',
            controller: 'RestaurantInfoCtrl',
        })
        .when('/restaurant/:city/:restaurant/booknow', {
            templateUrl: '../app/components/booking/_booking.tpl.html',
            controller: 'BookingCtrl',
        })
        .when('/restaurant/:city/:restaurant/booknow-section', {
            templateUrl: '../app/components/booking/_booking_section.tpl.html',
            controller: 'BookingCtrl',
        })
        .when('/restaurant/:city/:restaurant/section-booking', {
            templateUrl: '../app/components/section_booking/_section_booking.tpl.html',
            controller: 'SectionBookingCtrl',
        })
        .when('/restaurant/:city/:restaurant/event/:event_id', {
            templateUrl: '../app/components/event_booking/_event_booking.tpl.html',
            controller: 'EventBookingCtrl',
        })
        .when('/restaurant/:city/:restaurant/order-details/:order_id', {
            templateUrl: '../app/components/event_booking/_event_booking_confimation.tpl.html',
            controller: 'EventBookingDetailCtrl',
        })
        .when('/restaurant/:city/:restaurant/event_management/request', {
            templateUrl: '../app/components/event_management/_event_management_request.tpl.html',
            controller: 'EventManagementCtrl',
        })
        .when('/restaurant/:city/:restaurant/event-management/staff/:event_id', {
            templateUrl: '../app/components/event_management/_event_management_staff_form.tpl.html',
            controller: 'EventManagementCtrl',
        })
        .when('/restaurant/:city/:restaurant/event-management/menu-selection/:event_id', {
            templateUrl: '../app/components/event_management/_event_management_request.tpl.html',
            controller: 'EventManagementCtrl',
        })
        .when('/restaurant/:city/:restaurant/dining-rewards', {
            templateUrl: '../app/components/all_rewards_page/_dining_rewards.tpl.html',
            controller: 'AllRewardsCtrl',
        })
        .when('/restaurant/:city/:restaurant/dining-rewards-corporate-program', {
            templateUrl: '../app/components/all_rewards_cpp_page/_dining_rewards_corporate_program.tpl.html',
            controller: 'AllRewardsCPPCtrl',
        })
        .when('/cobranding/:branding', {
            templateUrl: '../app/components/co_branding/_co_branding_page.tpl.html',
            controller: 'CoBrandingCtrl',
        })
        .when('/group/:groupname', {
            templateUrl: '../app/components/restaurant_group_page/_restaurant_group_page.tpl.html',
            controller: 'RestaurantGroupCtrl',
        })
        .when('/singapore/location/:groupname', {
            templateUrl: '../app/components/restaurant_group_page/_restaurant_group_page.tpl.html',
            controller: 'RestaurantGroupCtrl',
        })
        .when('/singapore/favorite_cuisine/:groupname', {
            templateUrl: '../app/components/restaurant_group_page/_restaurant_group_page.tpl.html',
            controller: 'RestaurantGroupCtrl',
        })
        .when('/mybookings', {
            templateUrl: '../app/components/mybookings_page/_mybookings_page.tpl.html',
            controller: 'MyBookingCtrl',
        })
        .when('/myaccount', {
            templateUrl: 'client/app/components/myaccount_page/_myaccount_page.tpl.html',
            controller: 'MyAccountCtrl',
        })
        .when('/myreviews', {
            templateUrl: '../app/components/myreviews_page/_my_reviews_page.tpl.html',
            controller: 'MyReviewCtrl',
        })
        .when('/myorders', {
            templateUrl: '../app/components/myorders_page/_myorders_page.tpl.html',
            controller: 'MyOrderCtrl',
        })
        .when('/restaurant/:city/:restaurant/catering', {
            templateUrl: '../app/components/catering_page/_catering_page.tpl.html',
            controller: 'CateringCtrl',
        })
        .when('/contact', {
            templateUrl: '../app/components/info_contact_page/_info_contact_page.tpl.html',
            controller: 'InfoContactCtrl',
        })
        .when('/partner', {
            templateUrl: '../app/components/info_partner_page/_info_partner.tpl.html',
            controller: 'InfoPartnerCtrl',
        })
        .when('/weeloy-vip', {
            templateUrl: '../app/components/info_customer_page/_info_customer.tpl.html',
            controller: 'InfoCustomerCtrl',
        })
        .when('/how-it-works', {
            templateUrl: '../app/components/how_it_work_page/how_it_work.tpl.html',
            controller: 'HowItWorkCtrl',
        })
        .when('/faq', {
            templateUrl: '../app/components/faq_page/faq.tpl.html',
            controller: 'FAQCtrl',
        })
        .when('/all-rewards/:restaurant', {
            templateUrl: '../app/components/all_rewards_page/_all_rewards.tpl.html',
            controller: 'AllRewardsCtrl',
        })
        .when('/terms-and-conditions-of-service', {
            templateUrl: '../app/components/terms_and_conditions_of_service_page/_terms_and_coditions_of_service.tpl.html',
            controller: 'TermsAndConditionsOfServiceCtrl',
        })
        .when('/privacy-policy', {
            templateUrl: '../app/components/privacy_policy_page/_privacy_policy.tpl.html',
            controller: 'PrivacyPolicyCtrl',
        })
        .when('/download-mobile-app', {
            templateUrl: '../app/components/z_landing_mobile/_landing_mobile_download.tpl.html',
            controller: 'LandingMobileDownloadCtrl',
        })
        .when('/checkout', {
            templateUrl: '../app/components/checkout/_checkout.tpl.html',
            controller: 'CheckoutCtrl',
        })
        .when('/payment-success', {
            templateUrl: '../app/components/payment_success_page/_payment_success_page.tpl.html',
            controller: 'PaymentSuccessCtrl',
        })
        .when('/checkout/info', {
            templateUrl: '../app/components/checkout_info/_checkout_info.tpl.html',
            controller: 'CheckoutInfoCtrl',
        })
        
        .when('/post-review', {
            templateUrl: '../app/components/write_review/_write_review_page.tpl.html',
            controller: 'WriteReviewCtrl',
        })
      
        .when('/reset-password', {
            templateUrl: '../app/components/reset-password/_reset_password.tpl.html',
            controller: 'ResetPasswordCtrl',
        })
        .when('/allevents', {
            templateUrl: '../app/components/event_page/_event_page.tpl.html',
            controller: 'EventCtrl',
        })
        .when('/chinese-new-year-2017-menu', {
            templateUrl: '../app/components/cny_page/_cny_page.tpl.html',
            controller: 'CnyCtrl',
        })
       // .when('/mother-day-2016-restaurant-menu', {
       //     templateUrl: '../app/components/mother_day_page/_mother_day_page.tpl.html',
       //     controller: 'MotherDayCtrl',
       // })
        .when('/fathers-day-2017-menu', {
            templateUrl: '../app/components/father_day_page/_father_day_page.tpl.html',
            controller: 'FatherDayCtrl',
        })
       .when('/valentines-day-2017-menu', {
           templateUrl: '../app/components/v_day_page/_v_day_page.tpl.html',
           controller: 'VDayCtrl',
       })
//       .when('/easter-day-2017-menu', {
//           templateUrl: '../app/components/festive_promo/_festive_promo_page.tpl.html',
//           controller: 'FestivePromoCtrl',
//       })
        .when('/mothers-day-2017', {
           templateUrl: '../app/components/festive_promo/_festive_promo_page.tpl.html',
           controller: 'FestivePromoCtrl',
       })

        .when('/dailyspecials', {
            templateUrl: '../app/components/dsb_page/_dsb_page.tpl.html',
            controller: 'DailySpecialCtrl',
        })
        .when('/dsb-registration-form', {
            templateUrl: '../app/components/dsb_page/_dsb_register_form.tpl.html',
            controller: 'DailySpecialCtrl',
        })

        .when('/restaurant/:city/:restaurant/booking-confirmation', {
            templateUrl: '../app/components/booking/_booking_deposit.tpl.html',
            controller: 'BookingCtrl',
        })
         .when('/restaurant/:city/:restaurant/:query/deposit_pendingpayment_method', {
            templateUrl: '../app/components/payment/_pending_deposit.tpl.html',
            controller: 'PaymentCtrl',
        })
//        .when('/dine-and-win-contest-2016', {
//            templateUrl: '../app/components/contest_page/_contest_page.tpl.html',
//            controller: 'ContestCtrl',
//        })
        .when('/404', {
            templateUrl: '../app/components/404_page/_404.tpl.html',
            controller: 'F404Ctrl',
        })

        .otherwise({
         redirectTo:"404"

        });
 
    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
    });
}]);