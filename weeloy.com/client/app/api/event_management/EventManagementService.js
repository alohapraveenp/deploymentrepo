(function(app) {
    app.service('EventManagementService', ['$http', '$q', function($http, $q) {
        this.getEventManagementProject = function(event_id) {
            var API_URL = 'api/v2/event/management/' + event_id + '/';
            var defferred = $q.defer();
            $http.get(API_URL).success(function(response) {
                defferred.resolve(response);
            });
             return defferred.promise;
        };
        this.createEventManagementProject = function(event) {
            var API_URL = 'api/v2/event/management/create/';
            var defferred = $q.defer();
            $http.post(API_URL, event).success(function(response) {
                defferred.resolve(response);
            });
             return defferred.promise;
        };
        this.getEventManagementMenuProposition = function(event_id) {
            var API_URL = 'api/v2/event/management/menu/' + event_id + '/';
            var defferred = $q.defer();
            $http.get(API_URL).success(function(response) {
                defferred.resolve(response);
            });
             return defferred.promise;
        };
        this.saveMenuSelection = function(event_id, items, status) {
            var API_URL = 'api/v2/event/management/menu/' + event_id + '/save/';
            var defferred = $q.defer();
            $http.post(API_URL, {
                    items: items,
                    status: status
                }).success(function(response) {
                defferred.resolve(response);
            });
             return defferred.promise;
        };
        this.notifysaveMenu = function(items) {
            var API_URL = 'api/v2/event/management/menu/notify/';
            var defferred = $q.defer();
            $http.post(API_URL, {
                    items: items,
                }).success(function(response) {
                defferred.resolve(response);
            });
             return defferred.promise;
        };
        
        this.updateOrderstatus = function(restaurant,event_id,status,email){
               return $http.post("api/services.php/evordering/updatestatus/", 
                    {
                        'restaurant': restaurant,
                        'event_id': event_id,
                        'status' : status,
                        'email' : email,
                        'token' : '',
                    }).then(function(response) {
                        return response.data.data;
                   });
            
        };
        this.updatePrice = function(restaurant,event_id,email,amount,gst,service_charge,totalamount,status){
               return $http.post("api/v2/event/management/updateprice/", 
                    {
                        'restaurant': restaurant,
                        'event_id': event_id,
                        'total_amount' : totalamount,
                        'amount' : amount,
                        'gst' : gst,
                        'service_charge' : service_charge,
                        'status' : status,
                        'email' : email,
                    }).then(function(response) {
                        return response.data.data;
                   });
        };
        this.savepaymentdetails = function(restaurant,event_id,status,type,amount,card_id,payment_id){
               return $http.post("api/v2/event/management/paymentdetails/", 
                    {
                        'restaurant': restaurant,
                        'event_id': event_id,
                        'type' : type,
                        'amount' : amount,
                        'card_id' : card_id,
                        'payment_id' : payment_id,
                        'status'  :status,
                    }).then(function(response) {
                        return response.data.data;
                   });
        };
        this.modifyDetails = function(event_id){
                var API_URL = "api/v2/event/management/modifydetails/"+ event_id +"/";
                var defferred = $q.defer();
                $http.get(API_URL).success(function(response) {
                    defferred.resolve(response);
                });
                 return defferred.promise;
        };
        
    }]);

})(angular.module('app.api.event.management', []));
