(function(app) {
    app.service('RewardService', ['$http', '$q', function($http, $q) {
        this.getReward = function(query) {
            var API_URL = 'api/wheeldescription/' + query + '/unique_only';
            var defferred = $q.defer();
            $http.get(API_URL, {
                cache: true
            }).success(function(response) {
                defferred.resolve(response);
            });

            return defferred.promise;
        };
    }]);
})(angular.module('app.api.restaurant.reward', []));
