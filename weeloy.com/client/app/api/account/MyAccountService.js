(function(app) {
    app.service('AccountService', ['$http', '$q', function($http, $q) {
        this.updateMemberProfile = function(label, value) {
            return $http.post("api/services.php/rmloginc/module/forgot/", {
                    'email': email
                })
                .then(function(response) {
                    return response.data;
                });
        };
        this.getMemberProfile = function(data) {
            return $http.get("api/services.php/rmloginc/module/forgot/", {
                    'email': email
                })
                .then(function(response) {
                    return response.data;
                });
        };
        this.updateUserProfile = function(data, file) {
            if (data.salutation.toLowerCase() == 'mr.') {
                data.gender = 'Male';
            }
            if (data.salutation.toLowerCase() == 'ms.' || data.salutation.toLowerCase() == 'mrs.') {
                data.gender = 'Female';
            }
   
            return $http({
                url: "api/user/profile/update",
                method: "POST",
                processData: false,
                contentType: false,
                data: $.param({
                    email: data.email,
                    gender: data.gender,
                    firstname: data.firstname,
                    lastname: data.lastname,
                    mobile: data.mobile,
                    salutation: data.salutation,
                    membercode: data.membercode,


                }),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).then(function(response) {

                return response;
            });
        };

        this.getUserAccount = function() {
            return $http.get("api/user/profile/getdetails")
                .then(function(response) {
                    return response.data.data;

                });
        };

        this.reset = function(email, token, password) {
            var deferred = $q.defer();
            var params = {
                email: email,
                token: token,
                password: password
            };
            $http.post('api/forgotPassword/reset', params)
                .success(function(response) {
                    if (response.status === 0) {
                        deferred.reject(response.data);
                    } else {
                        deferred.resolve('Password update successfully');
                    }
                });
            return deferred.promise;
        };

    }]);

})(angular.module('app.api.account.myaccount', []));
