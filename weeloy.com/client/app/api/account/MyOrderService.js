(function(app) {
    app.service('MyOrderService', ['$http', '$q', function($http, $q) {
        this.getOrders = function(page) {
            var defferred = $q.defer();
            $http.get('api/orders?page=' + page).success(function(response) {
                defferred.resolve(response);
            });
            return defferred.promise;
        };
        this.getOrderDetail = function(id) {
            var defferred = $q.defer();
            $http.get('api/order/' + id + '/items', {
                cache: true
            }).success(function(response) {
                defferred.resolve(response);
            });
            return defferred.promise;
        };
    }]);

})(angular.module('app.api.account.myorder', []));
