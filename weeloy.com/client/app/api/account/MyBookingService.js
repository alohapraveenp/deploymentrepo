(function(app) {

    app.service('MyBookingService', ['$http', '$q', function($http, $q) {
        this.get = function(period) {
            var defferred = $q.defer();
            var url = 'api/user/booking/period/' + period;
            $http.get(url, {
                cache: false,
            }).success(function(response) {
                defferred.resolve(response);
            });
            return defferred.promise;
        };



        //Cancel booking
        // fetching booking details  
        //if exist
        this.getCancelConfirmation = function(query) {
            var defferred = $q.defer();
            var API_URL = 'api/confirmation/' + query.confirmation + '/' + query.email;
            $http.get(API_URL, {
                cache: true,
            }).success(function(response) {
                defferred.resolve(response);
            });
            return defferred.promise;
        };
        this.reviewPost = function(data) {
            var defferred = $q.defer();
            $http.post('api/review', data).success(function(response) {
                 defferred.resolve(response);
            });
             return defferred.promise;
        };
        this.reviewPostGrade = function(data) {
              var defferred = $q.defer();
            $http.post('api/review/grade', data).success(function(response) {
                defferred.resolve(response);
            });
               
            return defferred.promise;
        };
       


        // cancelbooking 
        this.CancelBooking = function(query, survey) {

            var defferred = $q.defer();
            var API_URL = 'api/visit/cancel/' + query.resto + '/' + query.booking + '/' + query.email;
            $http({
                url: API_URL,
                method: "POST",
                data: $.param({
                    reason: survey,
                    confirmation: query.booking,
                    restaurant: query.resto,
                    email: query.email
                }),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function(response) {
                defferred.resolve(response);
            });

            return defferred.promise;
        };
        this.getCancelPolicy = function (restaurant,payment_method,amount,product) {
            return $http.post("api/services.php/cancelpolicy/list",
                    {
                        'restaurant': restaurant,
                        'type': payment_method,
                        'amount':amount,
                        'product': product
                    }).then(function (response) {
                return response.data;
            });
        };
        this.bacchanaliacancelpolicy = function(restaurant,date,time,pax){
            var API_URL = "api/v2/restaurant/cancelpolicy/getPolicy";
               return  $http({
                    url: API_URL,
                    method: "POST",
                    data: $.param({
                            'restaurant': restaurant,
                            'date' 	: date,
		            'time'	: time,
                            'pax' : pax
                }),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
             }).success(function(response) {
                 return response.data; 
             });
//                var url = 'api/v2/restaurant/cancelpolicy/'+ restaurant;
//		return $http.get(url).success(function(response) {
//			return response.data; 
//                    });
       };
        
        
        this.cancelbookingrefund =function(query){
              var defferred = $q.defer();
		var API_URL = 'api/visit/payment/cancel/';
				$http({
					url: API_URL,
					method: "POST",
					data: $.param({
						reason: "",
						confirmation: query.confirmation,
						restaurant: query.restaurantinfo.restaurant,
						email: query.email,
                                                deposit:query.deposit_id,
                                                amount :query.refundamount,
                                               'payment_method':query.payment_method,
                                                
					   
					}),
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded'
					}
				}).success(function(response) {
                defferred.resolve(response);
            });
            return defferred.promise;
		
	};

        this.updateBooking = function(data) {
            var defferred = $q.defer();
            var API_URL = 'api/booking/update';
            var cdate = data.selecteddate;
            console.log("cdate=" + cdate + "fullyear=" + cdate.getFullYear());
            //data.selecteddate = ((cdate.getDate() <= 9) ? '0' : '') + cdate.getDate() + "/" + ((cdate.getMonth() <= 8) ? '0' : '') + (cdate.getMonth() + 1) + "/" + cdate.getFullYear();
            data.selecteddate = cdate.getFullYear() + "-" + ((cdate.getMonth() <= 8) ? '0' : '') + (cdate.getMonth() + 1) + "-" + ((cdate.getDate() <= 9) ? '0' : '') + cdate.getDate();

            $http({
                url: API_URL,
                method: "POST",
                data: $.param({
                    confirmation: data.name,
                    npers: data.npers,
                    selecteddate: data.selecteddate,
                    ntimeslot: data.ntimeslot,
                    specialrequest: data.specialrequest
                }),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function(response) {
                defferred.resolve(response);
            });

            return defferred.promise;


        };

        this.allotment = function(restaurant) {

            var defferred = $q.defer();
            var API_URL = 'api/restaurant/allote/' + restaurant;
            $http.get(API_URL, {
                cache: true,
            }).success(function(response) {
                defferred.resolve(response);
            });
            return defferred.promise;

        };
    }]);

})(angular.module('app.api.account.mybooking', []));
