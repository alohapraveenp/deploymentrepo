(function(app) {
    app.service('PaymentService', ['$http', '$q', function($http, $q) {
        this.check = function(payment_id) {
            var defferred = $q.defer();
            $http.get('api/order/' + payment_id).success(function(response) {
                defferred.resolve(response);
            });
            return defferred.promise;
        };

        this.deleteCart = function() {
            var defferred = $q.defer();
            $http.delete('api/cart').success(function(response) {
                defferred.resolve(response);
            });
            return defferred.promise;
        };

        this.getEventBooking = function(order_id) {
            var defferred = $q.defer();
            $http.get('api/event-booking/order/' + order_id)
                .then(function(response) {
                    if (response.data.status === 1) {
                        defferred.resolve(response.data);
                    } else {
                        defferred.reject(response);
                    }
                }, function(error) {
                    defferred.reject(error);
                });
            return defferred.promise;
        };
    }]);
})(angular.module('app.api.payment', []));
