(function(app) {
    app.factory('API', API);
    API.$inject = [
        'RestaurantService',
        'RestaurantGroupService',
        'RewardService',
        'CateringService',
        'PickupHourService',
        'ContactService',
        'AccountService',
        'MyBookingService',
        'MyOrderService',
        'MyReviewService',
        'PaymentService',
        'EventManagementService',
    ]; 

    function API(
        RestaurantService,
        RestaurantGroupService,
        RewardService,
        CateringService,
        PickupHourService,
        ContactService,
        AccountService,
        MyBookingService,
        MyOrderService,
        MyReviewService,
        PaymentService,
        EventManagementService
    ) {
        var service = {
            restaurant: RestaurantService,
            group: RestaurantGroupService,
            reward: RewardService,
            catering: CateringService,
            pickuphour: PickupHourService,
            contact: ContactService,
            account: AccountService,
            mybooking: MyBookingService,
            myorder: MyOrderService,
            myreview: MyReviewService,
            payment: PaymentService,
            eventmanagement: EventManagementService
        };
        return service;
    }
})(angular.module('app.api', [
    'app.api.restaurant',
    'app.api.restaurant.group',
    'app.api.restaurant.reward',
    'app.api.restaurant.catering',
    'app.api.restaurant.pickuphour',
    'app.api.contact',
    'app.api.account.myaccount',
    'app.api.account.mybooking',
    'app.api.account.myorder',
    'app.api.account.myreview',
    'app.api.payment',
    'app.api.event.management',
]));
