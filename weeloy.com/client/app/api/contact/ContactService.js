(function(app) {
    app.service('ContactService', ['$http', '$q', function($http, $q) {
        this.sendContact = function(firstname, lastname, email, message) {
            var API_URL = 'api/send-contact-email';
            var defferred = $q.defer();
            $http({
                url: API_URL,
                method: "POST",
                data: $.param({
                    firstname: firstname,
                    lastname: lastname,
                    email: email,
                    message: message
                }),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function(response) {
                defferred.resolve(response);
            });

            return defferred.promise;
        };
    }]);

})(angular.module('app.api.contact', []));
