(function(app) {
    app.filter('timeFormat', function() {
        return function(timeStr, format) {
            if (timeStr === null) {
                return null;
            } else {
                return moment(timeStr).format(format);
            }
        };
    });
})(angular.module('app.shared.filters.timeFormat', []));
