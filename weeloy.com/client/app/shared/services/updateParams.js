var UpdateParams = angular.module('UpdateParams', []);
UpdateParams.service('params', ['$rootScope', '$route', '$routeParams', function($rootScope, $route, $routeParams) {
    this.update = function(param_key, value) {
        for (key in $routeParams) {
            if (key == param_key) {
                $routeParams[key] = value;
            };
        };
        $route.updateParams($routeParams);
    };
}]);
