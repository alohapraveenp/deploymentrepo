var RestaurantTagItem = angular.module('RestaurantTagItem', []);
RestaurantTagItem.directive('restaurantTagItem', ['$rootScope', function($rootScope) { 
    return {
        restrict: 'E',
        templateUrl: 'client/app/shared/partial/_restaurant_tag_item.html',
        replace: true,
        scope: {
            tag: '=',
            openInNewTab: '@',
            mediaServer: '=',
        },
        link: function(scope, element, attrs) {
            
            var img = document.createElement('img');
            var imageSizeSmallCategory = 0;
          
             
            // default size of restaurant image is 500px;
            // default size of wheel image is 150px;
            var ImageWidth = ($(element).find('.img-transparent-small-categories').width());
              $(element).find('.img-transparent-small-categories').height('');

            if ($rootScope.imageSizeSmallCategory === undefined && ImageWidth > 0) {
                config.ImageSizes.forEach(function(value, key) {
                    if (value > ImageWidth && $rootScope.imageSizeSmallCategory === undefined) {
                        imageSizeSmallCategory = '/' + value + '/';
                        $rootScope.imageSizeSmallCategory = imageSizeSmallCategory;
                    }
                });
            } else {
                imageSizeSmallCategory = $rootScope.imageSizeSmallCategory;
            }

            if (typeof imageSizeSmallCategory == 'undefined'){
                $rootScope.imageSizeSmallCategory = imageSizeSmallCategory = '/500/';
            }
            if (imageSizeSmallCategory === 500 || imageSizeSmallCategory === '/500/') {
                    imageSizeSmallCategory = '/';
                } else {
                    if(!(imageSizeSmallCategory[0] == '/' && imageSizeSmallCategory.charAt[imageSizeSmallCategory.length-1] == '/')){
                         imageSizeSmallCategory = '/' + imageSizeSmallCategory + '/';
                    }
                    imageSizeSmallCategory = imageSizeSmallCategory.replace(/\/\//g,'/');
                    if (typeof imageSizeSmallCategory == 'undefined'){
                        imageSizeSmallCategory = '/360/';
                    }
                }
//patch 
//
//
//
            //my_image = scope.category.images[Math.floor(Math.random()*scope.category.images.length)];
            // the image will load from random server and best size match with current screen width
            var url;
           
            my_image = scope.tag.images[Math.floor(Math.random()*scope.tag.images.length)];
            // the image will load from random server and best size match with current screen width
        
            //console.log(JSON.stringify( my_image));
            if (scope.mediaServer === undefined) {
                url = '//media.weeloy.com/upload/restaurant/' + encodeURIComponent(my_image.restaurant.trim()) + imageSizeSmallCategory + encodeURIComponent(my_image.image);
            } else {
                url = scope.mediaServer + '/upload/restaurant/' + encodeURIComponent(my_image.restaurant.trim()) + imageSizeSmallCategory + encodeURIComponent(my_image.image);
            }
//           if(typeof scope.tag.images != 'undefined' ){
//              if (scope.mediaServer === undefined) {
//                url = '//media.weeloy.com/upload/category/' + encodeURIComponent(scope.tag.restaurant.trim()) + imageSizeSmallCategory + encodeURIComponent(scope.tag.picture);
//            } else {
//                  url = scope.mediaServer + '/upload/category/' + encodeURIComponent(scope.tag.restaurant.trim()) + imageSizeSmallCategory + encodeURIComponent(scope.tag.picture);
//            }
//            }
            
//            url ="https://media.weeloy.com/upload/restaurant/SG_SG_R_HighlanderAtChijmes/360/Layout_01.jpg";
             $(element).find('.img-transparent-small-categories').css('background-image', 'url(' + url + ')');
            img.src = url;
                if (scope.openInNewTab == 'true' && $(window).width() > 480) {
                    scope.OpenRestaurantInNewTab = '_blank';
                }
         
              //$(element).find('.img-transparent').css('background', 'url(' + url + ')');
              $(element).find('.img-transparent-small-categories').attr('src',url);
//              img.className = img.width > img.height ? 'landscape' : 'portrait';
                    img.onload = function() {
                        
                            $(element).find('.img-transparent-small-categories').css('background', 'url(' + url + ')');

                    };


            scope.go = function(restaurant) {
                document.location.href = "/" + restaurant.internal_path + "/booknow";
                //WindowOpen(restaurant.internal_path + '/book-now');
            };

//            function WindowOpen(url) {
//                window.open(url, '_blank', 'toolbar=no, scrollbars=yes, resizable=yes, top=30, left=30, width=800, height=900');
//            }
        },
    };
}]);