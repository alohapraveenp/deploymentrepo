(function(app) {
    app.directive('paymentIframe', ['$rootScope','$routeParams', '$window', '$sce', function($rootScope,$routeParams, $window, $sce) {
           
        return {
            restrict: 'AE',
            scope: {
                restaurant: '='
            },
            template: '<iframe width="100%" style="margin-top:60px;max-width:550px;border: none; background: transparent" height="1150" ng-src="{{url}}"></iframe>',
            link: function(scope, element, attrs) {
            
                var qrParam =$routeParams.query;
              var url = $rootScope.base_url + '/modules/booking/deposit/deposit_payment_method.php?action=pending_payment&deposit_id='+qrParam+'&restaurant='+scope.restaurant.restaurant;
            
                scope.url = $sce.trustAsResourceUrl(url);
            },
        };
    }]);

})(angular.module('app.shared.directives.paymentIframe', []));


