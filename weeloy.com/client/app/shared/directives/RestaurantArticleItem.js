var RestaurantArticleItem = angular.module('RestaurantArticleItem', []);
RestaurantArticleItem.directive('restaurantArticleItem', ['$rootScope', function ($rootScope) {
        return {
            restrict: 'E',
            templateUrl: 'client/app/shared/partial/_restaurant_blogarticle_item.html',
            scope: {
                article: '=',
                openInNewTab: '@',
                mediaServer: '=',
            },
            link: function (scope, element, attrs) {

                var img = document.createElement('img');
                var imageSize = 500;
                var wheelSize = 150;
                var ImageWidth = imageSize;
                // default size of restaurant image is 500px;
                // default size of wheel image is 150px;

                var windowWidth = $(window).width();
                if (windowWidth < 768) {
                    ImageWidth = windowWidth;
                }
                if (767 < windowWidth < 1200) {
                    ImageWidth = Math.round(windowWidth * 0.8 * 0.45);
                }
                if (1199 < windowWidth) {
                    ImageWidth = Math.round(windowWidth * 0.8 * 0.25);
                }
                var wheelImageWidth = Math.round(ImageWidth * 0.8 * 0.3);
                if ($rootScope.imageSize === undefined && ImageWidth > 0) {
                    config.ImageSizes.forEach(function(value, key) {
                        if (value > ImageWidth && $rootScope.imageSize === undefined) {
                            imageSize = value;
                            $rootScope.imageSize = imageSize;
                        }
                    });
                } else {
                    imageSize = $rootScope.imageSize;
                }
//patch 

                if (typeof imageSize == 'undefined') {
                    $rootScope.imageSize = imageSize = '/500/';
                }
                if (imageSize === 500 || imageSize === '/500/') {
                    imageSize = '/';
                } else {
                    if (!(imageSize[0] == '/' && imageSize.charAt[imageSize.length - 1] == '/')) {
                        imageSize = '/' + imageSize + '/';
                    }

                    imageSize = imageSize.replace(/\/\//g, '/');
                    if (typeof imageSize == 'undefined') {
                        imageSize = '/360/';
                    }
                }
//patch 
//
//
                //my_image = scope.category.images[Math.floor(Math.random()*scope.category.images.length)];
                // the image will load from random server and best size match with current screen width
                var url;

                if (typeof scope.article.imageUrl != 'undefined') {
                    if (scope.mediaServer === undefined) {
                        url = 'https://d1zfy3x6jx1ipx.cloudfront.net/' + encodeURIComponent(scope.article.imageUrl);
                    } else {
                        url = 'https://d1zfy3x6jx1ipx.cloudfront.net/' + encodeURIComponent(scope.article.imageUrl);
                    }
                }
                img.src = url;
                if (scope.openInNewTab == 'true' && $(window).width() > 480) {
                    scope.OpenRestaurantInNewTab = '_blank';
                }

                $(element).find('.img-transparent').css('background', 'url(' + url + ')');
                img.onload = function () {

                    $(element).find('.img-transparent').css('background', 'url(' + img.src + ')');
                };
                scope.go = function (restaurant) {
                    WindowOpen(restaurant.internal_path + '/book-now');
                };

                //console.log('blog');
                //console.log($rootScope.imageSize);

                function WindowOpen(url) {
                    window.open(url, '_blank', 'toolbar=no, scrollbars=yes, resizable=yes, top=30, left=30, width=800, height=900');
                }
            },
        };
    }]);