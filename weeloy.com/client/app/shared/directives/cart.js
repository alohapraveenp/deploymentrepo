var ShoppingCart = angular.module('ShoppingCart', ['CheckoutService']);
ShoppingCart.directive('cart', ['$rootScope', 'Checkout', function($rootScope, Checkout) {
    return {
        restrict: 'E',
        templateUrl: 'client/app/shared/partial/_shopping_cart.html',
        scope: {
            items: '=',
            cx: '=',
            minorder: '=',
            tax: '=',
        },
        link: function(scope, element, attrs) {
            if (scope.items === undefined) {
                scope.items = [];
                }

            checkItemInCart();
            $rootScope.$on('AddItemToCart', function(evt, item) {
                checkItemInCart();
            });
            $rootScope.$on('updateCart', function(evt, items) {
                scope.items = items;
                checkItemInCart();
            });
            $rootScope.$on('IncreaseItemInCart', function(evt, item_id) {
                scope.items.forEach(function(value, key) {
                    if (value.ID == item_id) {
                        scope.increaseItemNumber(scope.items[key]);
                    }
                });
            });
            scope.increaseItemNumber = function(item, evt) {
                scope.items.forEach(function(value, key) {
                    if (value.ID == item.ID) {
                        scope.items[key].quantity++;
                        Checkout.updateItem(item).then(function(response) {
                            console.log(response);
                            if (response.status == 1) {
                                checkItemInCart();
                            } else {
                                scope.items[key].quantity--;
                                alert(response.data);
                            }
                        });
                    }
                });
                return false;
            };

            scope.decreaseItemNumber = function(item, evt) {
                scope.items.forEach(function(value, key) {
                    if (value.ID == item.ID && item.quantity > 1) {
                        scope.items[key].quantity--;
                        Checkout.updateItem(item).then(function(response) {
                            console.log(response);
                            if (response.status == 1) {
                                checkItemInCart();
                            } else {
                                scope.items[key].quantity++;
                                alert(response.data);
                            }
                        });
                    }
                });
                evt.preventDefault();
                return false;
            };

            scope.removeItem = function(item) {
                scope.items.forEach(function(value, key) {
                    if (item.ID == value.ID) {
                        Checkout.deleteItem(item).then(function(response) {
                            console.log(response);
                            if (response.status == 1) {
                                scope.items.splice(key, 1);
                                checkItemInCart();
                            } else {
                                console.log(response.data);
                            }
                        });
                    }
                });
            };

            function checkItemInCart() {
                if (scope.items.length > 0) {
                    var total = 0;
                    scope.items.forEach(function(value, key) {
                        if (scope.items[key].quantity === undefined) {
                            scope.items[key].quantity = 1;
                        }
                        if (value.price === undefined || value.price === '' || parseInt(value.price) <= 0) {
                            value.price = 0;
                        }
                        total += parseInt(value.price) * parseInt(value.quantity);
                    });
                    
                    scope.total = total;
                    scope.total_gst = money_multiply(total, scope.tax);
                    
                    $rootScope.cart = scope.items;
                } else {
                    $rootScope.cart = [];
                    scope.total = 0;
                }
            }
                function money_multiply(a, b) {
                    var log_10 = function (c) {
                        return Math.log(c) / Math.log(10);
                    },
                            ten_e = function (d) {
                                return Math.pow(10, d);
                            },
                            pow_10 = -Math.floor(Math.min(log_10(a), log_10(b))) + 1;
                    return ((a * ten_e(pow_10)) * (b * ten_e(pow_10))) / ten_e(pow_10 * 2);
        }
            }
    };
}]);
