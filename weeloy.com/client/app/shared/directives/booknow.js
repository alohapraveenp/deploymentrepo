(function(app) {
    app.directive('bookIframe', ['$rootScope', '$window', '$sce', function($rootScope, $window, $sce) {
        return {
            restrict: 'AE',
            scope: {
                restaurant: '='
            },
            template: '<iframe width="100%" style="margin-top:60px;max-width:550px;border: none; background: transparent" height="1150" ng-src="{{url}}" onload="window.parent.parent.scrollTo(0,0)"></iframe>',
            link: function(scope, element, attrs) {
              var url;
              if(scope.restaurant.multiprod_allotment){
                   url = $rootScope.base_url + '/' + scope.restaurant.getInternalPath() + '/book-now-section?bkrestaurant='+scope.restaurant.restaurant; 
              }else{
                   url = $rootScope.base_url + '/' + scope.restaurant.getInternalPath() + '/book-now'; 
              }
           
                scope.url = $sce.trustAsResourceUrl(url);
            },
        };
    }]);

})(angular.module('app.shared.directives.bookIframe', []));
