var MyBookingReview = angular.module('MyBookingReview', []);
MyBookingReview.directive('starRating', ['$rootScope', '$http', function($rootScope, $http) {
  
            return {
                restrict: 'A',
                template: '<span style="margin-left:15px;" > <strong> Your Rating :</strong> </span><ul class="rating"><li ng-repeat="star in stars" ng-class="star" ng-click="toggle($index)">\u2605 </li></ul>',
    
                scope: {
                    ratingValue: '=',
                    max: '=',
                    onRatingSelected: '&'
                },
                link: function (scope, elem, attrs) {
                    var updateStars = function () {
                        scope.stars = [];
                        for (var i = 0; i < scope.max; i++) {
                            scope.stars.push({
                                filled: i < scope.ratingValue
                            });
                        }
                    };

                    scope.toggle = function (index) {
                        scope.ratingValue = index + 1;
                        scope.onRatingSelected({
                            rating: index + 1
                        });
                    };

                    scope.$watch('ratingValue',
                            function (oldVal, newVal) {
                                if (newVal) {
                                    updateStars();
                                }
                            }
                    );
                }
            };
  
     
//    return {
//        restrict: 'E',
//        scope: {
//            reviewItem: '=rdataReview'
//        },
//        templateUrl: 'client/app/shared/partial/_my_review_page_reviewpost_section.html',
//        link: function(scope, element, attrs) {
//                  var review = {};
//            scope.review = review;
//
//            $(".sliders").jRating({
//                step: true,
//                length: 5,
//                canRateAgain: true,
//                showRateInfo: false,
//                nbRates: 5,
//                sendRequest: false,
//                onClick: RateElement,
//            });
//
//            function RateElement(element, rate, attrs) {
//                 
//
//                rate = Math.round(rate / 36 * 5);
//
//                switch ($(element).attr('rate-model')) {
//                    case 'review.food':
//
//                        review.food = rate;
//                        break;
//                    case 'review.ambiance':
//
//                        review.ambiance = rate;
//
//                        break;
//                    case 'review.service':
//                        review.service = rate;
//                        break;
//                    case 'review.price':
//                        review.price = rate;
//                        break;
//                    default:
//                        break;
//                }
//                scope.review = review;
//
//            }
//            scope.ReviewNow = function() {
//                var data = {
//                    confirmation_id: scope.reviewItem.booking,
//                    user_id: scope.reviewItem.email,
//                    restaurant_id: scope.reviewItem.resto,
//                    food_rate: scope.review.food,
//                    ambiance_rate: scope.review.ambiance,
//                    service_rate: scope.review.service,
//                    price_rate: scope.review.price,
//                    comment: scope.review.comment,
//                };
//                
//                $http.post('api/review', data).success(function(response) {
//                      $("#msg-alert").css('display','block');
//                        $(".alert").addClass('alert-success');
//                        $(".alert").html('The review for this booking has been posted successfully.');
//                        $("#vignette_review").css('display','none');
//                        $("#review-msg").css('display','block');
//      
//                });
//            };
//        },
//    };
}]);


