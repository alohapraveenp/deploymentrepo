var ShoppingCartEvent = angular.module('ShoppingCartEvent', []);
ShoppingCartEvent.directive('cartevent', ['$rootScope', 'API', function ($rootScope, Checkout, API) {
        return {
            restrict: 'E',
            templateUrl: 'client/app/shared/partial/_shopping_cart_event.html',
            scope: {
                items: '=',
                menus: '=',
                counters: '=',
                categories: '=',
                selectioncomplete: '=',
                status: '=',
                totalamount: '=',
                minimumorder: '=',
                deposit: '='
                
            },
            link: function (scope, element, attrs) {
                if (scope.items === undefined) {
                    scope.items = [];
                }
                if (scope.menus === undefined) {
                    scope.menus = [];
                }
                scope.removeItem = function (item) {
                    scope.items.forEach(function (value, key) {
                        var index = scope.items.indexOf(item);
                        if (index > -1) {
                            scope.items.splice(index, 1);
                            scope.counters[item.displayID] = scope.counters[item.displayID] - 1;
                            scope.selectioncomplete = false;
                        }
                    });
                };

                scope.saveEventMenu = function (status) {
                        scope.status = 'menu_selected';
//                    var event_id = '';
//                    var selected_items = [];
//                    
//                    scope.items.forEach(function (value, key) {
//                        event_id = value.eventID;
//                        selected_items.push(value.menuItemID);
//                    });
//                    
//                    API.eventmanagement.saveMenuSelection(event_id, selected_items, status)
//                            .then(function (result) {
//                                alert('saved');
//                                
//                            });
                };
                


            }
        };
    }]);
