function Service(options) {
   	this.ID = null;
   	this.active = '';
   	this.categorie_id = '';
   	this.pico_categorie = '';
   	this.pico_service = '';
   	this.service = '';
   	this.categorie = '';
        this.css_class_categorie = '';
        this.css_class_service = '';
        

    BaseModel.call(this, options);
}

Service.prototype = new BaseModel();

Service.prototype.getCategoryName = function() {
	return this.categorie;
};

Service.prototype.getCategoryId = function() {
	return this.categorie_id;
};