function RestaurantEvent(options) {
    this.ID = '';
    this.city = '';
    this.country = '';
    this.description = '';
    this.start = '';
    this.end = '';
    this.morder = '';
    this.name = '';
    this.picture = '';
    this.price = '';
    this.title = '';
    this.restaurant = '';
    this.eventID = '';
    this.tnc = '';
    this.pdf_link = '';
    this.is_perbookingrice ='';
    this.mediaServer = '//media.weeloy.com';

    BaseModel.call(this, options);
}

RestaurantEvent.prototype = new BaseModel();

RestaurantEvent.prototype.getID = function() {
    return this.ID;
};
RestaurantEvent.prototype.getEventID = function() {
    return this.eventID;
};

RestaurantEvent.prototype.getCity = function() {
    return this.city;
};

RestaurantEvent.prototype.getCountry = function() {
    return this.country;
};

RestaurantEvent.prototype.getDescription = function() {
    return this.description;
};

RestaurantEvent.prototype.getPdfLink = function() {
    return this.pdf_link;
};

RestaurantEvent.prototype.getStartTime = function(format) {
    return moment(this.start).format(format);
};

RestaurantEvent.prototype.getEndTime = function(format) {
    return moment(this.end).format(format);
};

RestaurantEvent.prototype.getOrder = function() {
    return this.morder;
};
RestaurantEvent.prototype.getPrice = function() {
    return this.price;
};

RestaurantEvent.prototype.hasPicture = function() {
    if (this.picture === '' || this.picture === null) {
        return false;
    } else {
        return true;
    }
};

RestaurantEvent.prototype.getPicture = function(mediaServer) {
    mediaServer = mediaServer || this.mediaServer;
    return mediaServer + '/upload/restaurant/' + this.restaurant + '/' + this.picture;
};

RestaurantEvent.prototype.getTitle = function() {
    return this.title;
};

RestaurantEvent.prototype.setRestaurant = function(restaurant_id) {
    this.restaurant = restaurant_id;
    return this;
};

RestaurantEvent.prototype.getEventBookingUrl = function(restaurant) {
    return restaurant.getInternalPath() + '/event/' + this.eventID;
};

RestaurantEvent.prototype.getEventOrderTnc = function() {
    return this.tnc;
};
