function EventBooking(options) {
    this.eventID = '';
    this.restaurant = '';
    this.title = '';
    this.name = '';
    this.description ='';
    this.rdate = '';
    this.rtime = '';
    this.maxpax = '';
    this.tnc = '';
    this.status = '';
    this.amount ='';
    this.type ='';
    this.start ='';
    this.end ='';
    this.location = '';
    this.price ='';

    BaseModel.call(this, options);
}

EventBooking.prototype = new BaseModel();

EventBooking.prototype.getTitle = function() {
    return this.title;
};



EventBooking.prototype.getBookingDate = function() {
    return this.rdate;
};

EventBooking.prototype.getBookingTime = function() {
    return this.rtime;
};

EventBooking.prototype.getNumberPeople = function() {
    return this.pax;
};

EventBooking.prototype.getName = function() {
    return this.name;
};

EventBooking.prototype.getDescription = function() {
    return this.description;
};

EventBooking.prototype.getStatus = function() {
    return this.status;
};

EventBooking.prototype.setRestaurant = function(restaurant){
    this.restaurant = restaurant;
};
EventBooking.prototype.getPrice = function(){
    return this.price;
};






