function EventManagement(options) {
    this.eventID = '';
    this.restaurant = '';
    this.firstname = '';
    this.lastname = '';
    this.rdate = '';
    this.rtime = '';
    this.pax = '';
    this.email = '';
    this.phone = '';
    this.occasion = '';
    this.name = '';
    this.special_requests = '';
    this.tnc = '';
    this.tnc_cancel = '';
    this.status = '';
    this.notes = '';
    this.morder = '';
    this.item_limit = '';
    this.more = '';
    this.payment_method  = '';
    this.payment_details  = '';
    this.setup  = '';
    this.billing  = '';
    this.extraflag  = '';
    this.total_amount  = '';
    this.gst = '';
    this.service_charge ='';
    this.amount ='';
    this.booked_by = '';
    this.salutation = '';
    this.company = '';
    this.globaltnc = '';
    this.globalspltnc = '';
    this.fullname = '';
    
    
    BaseModel.call(this, options);
}

EventManagement.prototype = new BaseModel();

EventManagement.prototype.getFirstName = function() {
    return this.firstname;
};

EventManagement.prototype.getLastName = function() {
    return this.lastname;
};

EventManagement.prototype.getBookingDate = function() {
    return this.rdate;
};

EventManagement.prototype.getBookingTime = function() {
    return this.rtime;
};

EventManagement.prototype.getNumberPeople = function() {
    return this.pax;
};

EventManagement.prototype.getEmail = function() {
    return this.email;
};

EventManagement.prototype.getPhone = function() {
    return this.phone;
};

EventManagement.prototype.getOccasion = function() {
    return this.occasion;
};
EventManagement.prototype.getName = function() {
    return this.name;
};

EventManagement.prototype.getSpecialRequests = function() {
    return this.special_requests;
};

EventManagement.prototype.getStatus = function() {
    return this.status;
};

EventManagement.prototype.setRestaurant = function(restaurant){
    this.restaurant = restaurant;
};

EventManagement.prototype.setStatus = function(status){
    this.status = status;
};

EventManagement.prototype.getNotes = function() {
    return this.pax;
};

EventManagement.prototype.setMorder = function(status){
    this.status = status;
};

EventManagement.prototype.getItemLimit = function() {
    return this.pax;
};
EventManagement.prototype.getTotalAmount = function() {
    return this.total_amount;
};

EventManagement.prototype.getSalutation = function() {
    return this.salutation;
};

EventManagement.prototype.setSalutation = function(salutation){
    this.salutation = salutation;
};

EventManagement.prototype.getCompany = function() {
    return this.company;
};

EventManagement.prototype.setCompany = function(company){
    this.company = company;
};