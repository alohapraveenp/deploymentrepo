angular.module('templates-main', ['../app/components/404_page/_404.tpl.html', '../app/components/all_rewards_cpp_page/_dining_rewards_corporate_program.tpl.html', '../app/components/all_rewards_page/_dining_rewards.tpl.html', '../app/components/booking/_booking.tpl.html', '../app/components/booking/_booking_deposit.tpl.html', '../app/components/booking/_booking_section.tpl.html', '../app/components/cancel_booking/_cancel_booking.tpl.html', '../app/components/catering_page/_catering_page.tpl.html', '../app/components/checkout/_checkout.tpl.html', '../app/components/checkout_info/_checkout_info.tpl.html', '../app/components/cny_page/_cny_page.tpl.html', '../app/components/co_branding/_co_branding_page.tpl.html', '../app/components/contest_page/_contest_page.tpl.html', '../app/components/dsb_page/_dsb_page.tpl.html', '../app/components/dsb_page/_dsb_register_form.tpl.html', '../app/components/event_booking/_event_booking.tpl.html', '../app/components/event_booking/_event_booking_confimation.tpl.html', '../app/components/event_management/_event_management_request.tpl.html', '../app/components/event_management/_event_management_staff_form.tpl.html', '../app/components/event_page/_event_page.tpl.html', '../app/components/faq_page/faq.tpl.html', '../app/components/father_day_page/_father_day_page.tpl.html', '../app/components/festive_promo/_festive_promo_page.tpl.html', '../app/components/home/_home.tpl.html', '../app/components/home/_newhome.tpl.html', '../app/components/how_it_work_page/how_it_work.tpl.html', '../app/components/info_contact_page/_info_contact_page.tpl.html', '../app/components/info_customer_page/_info_customer.tpl.html', '../app/components/info_partner_page/_info_partner.tpl.html', '../app/components/mother_day_page/_mother_day_page.tpl.html', '../app/components/myaccount_page/_myaccount_page.tpl.html', '../app/components/mybookings_page/_mybookings_page.tpl.html', '../app/components/myorders_page/_myorders_page.tpl.html', '../app/components/myreviews_page/_my_reviews_page.tpl.html', '../app/components/payment/_pending_deposit.tpl.html', '../app/components/payment_success_page/_payment_success_page.tpl.html', '../app/components/privacy_policy_page/_privacy_policy.tpl.html', '../app/components/reset-password/_reset_password.tpl.html', '../app/components/restaurant_group_page/_restaurant_group_page.tpl.html', '../app/components/restaurant_info_page/_restaurant_info_page.tpl.html', '../app/components/search_page/_search_page.tpl.html', '../app/components/search_page/_search_tag.tpl.html', '../app/components/section_booking/_section_booking.tpl.html', '../app/components/terms_and_conditions_of_service_page/_terms_and_coditions_of_service.tpl.html', '../app/components/v_day_page/_v_day_page.tpl.html', '../app/components/write_review/_write_review_page.tpl.html', '../app/components/z_landing_mobile/_landing_mobile_download.tpl.html', '../app/shared/partial/_breadcrumb.tpl.html', '../app/shared/partial/_footer.tpl.html', '../app/shared/partial/_footer_new.tpl.html', '../app/shared/partial/_forgot_password_form.tpl.html', '../app/shared/partial/_login_form.tpl.html', '../app/shared/partial/_menu.tpl.html', '../app/shared/partial/_menu_user.tpl.html', '../app/shared/partial/_message_wrapper.tpl.html', '../app/shared/partial/_restaurant_item.tpl.html', '../app/shared/partial/_restaurant_services_and_share.tpl.html', '../app/shared/partial/_signup_form.tpl.html']);

angular.module("../app/components/404_page/_404.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/components/404_page/_404.tpl.html",
    "<meta name=\"prerender-status-code\" content=\"404\">\n" +
    "<div style=\"margin-top: 100px;\">\n" +
    "	<h1 class=\"text-center\">404 Page not Found</h1>\n" +
    "        <h4 class=\"text-center\">Sorry but the page you are looking for could not be found. Please click one of the links from the header.</h4>        \n" +
    "        <div class=\"text-center\">\n" +
    "            \n" +
    "            <br><br>\n" +
    "            <p><span>Go to the <a href=\"/\" title=\"Weeloy home page\">home page</a></span></p>\n" +
    "            <br><br>\n" +
    "            \n" +
    "            <p><span>Reserve a <a href=\"search/singapore\" title=\"Reserve restaurant in Singapore\">restaurant in Singapore</a></span></p>\n" +
    "            <br>\n" +
    "            <p><span>Reserve a <a href=\"search/bangkok\" title=\"Reserve restaurant in Bangkok\">restaurant in Bangkok</a></span></p>\n" +
    "            <br>\n" +
    "            <p><span>Reserve a <a href=\"search/phuket\" title=\"Reserve restaurant in Phuket\">restaurant in Phuket</a></span></p>\n" +
    "        </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("../app/components/all_rewards_cpp_page/_dining_rewards_corporate_program.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/components/all_rewards_cpp_page/_dining_rewards_corporate_program.tpl.html",
    "<div id=\"all-rewards\">\n" +
    "    <div class=\"container-fluid\">\n" +
    "        <div class=\"col-lg-8 col-md-8 col-sm-8 col-xs-12 text-center wheel\">\n" +
    "            <div ng-show=\"!mobileWebview\" id=\"breadcrumb\" breadcrumb base-url=\"base_url\" base-path=\"base_path\" restaurant=\"restaurant\"></div>\n" +
    "            <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12 wheel-image\">\n" +
    "                <a ng-if=\"!mobileWebview\" href=\"{{restaurant.getInternalPath()}}/booknow\">\n" +
    "                    <img class=\"img-responsive\" ng-src=\"{{restaurant.getWheelCPPRewardImage()}}\">\n" +
    "                </a>\n" +
    "                <img ng-if=\"mobileWebview\" class=\"img-responsive\" ng-src=\"{{restaurant.getWheelRewardImage()}}\">\n" +
    "            </div>\n" +
    "            <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12 col-lg-offset-3 col-md-offset-3 col-sm-offset-3 text-center wheel-text\" ng-bind=\"Str.AllRewardPage.description\"></div>\n" +
    "        </div>\n" +
    "        <div class=\"col-lg-4 col-md-4 col-sm-4 col-xs-12\">\n" +
    "            <div class=\"col-lg-8 col-md-8 col-sm-8 col-xs-12 restaurant\">\n" +
    "                <div class=\"trans-block text-center\">\n" +
    "                    <div class=\"bar-logo text-center\">\n" +
    "                        <a  ng-if=\"!mobileWebview\" rel=\"nofollow\" ng-href=\"{{restaurant.getInternalPath()}}\">\n" +
    "                            <img ng-src=\"{{restaurant.getLogoImage()}}\">\n" +
    "                        </a>\n" +
    "                        <img  ng-if=\"mobileWebview\" ng-src=\"{{restaurant.getLogoImage()}}\">\n" +
    "                    </div>\n" +
    "                    <a  ng-if=\"!mobileWebview\" rel=\"nofollow\" ng-href=\"{{restaurant.getInternalPath()}}\">\n" +
    "                        <h1 ng-bind=\"restaurant.getTitle()\"></h1>\n" +
    "                    </a>\n" +
    "                    <h1  ng-if=\"mobileWebview\" ng-bind=\"restaurant.getTitle()\"></h1>\n" +
    "                    <p><span ng-bind=\"restaurant.getAddress()\"></span><span ng-if=\"restaurant.getAddress()\">,</span>\n" +
    "                        <br><span ng-bind=\"restaurant.restaurantinfo.region\"></span>\n" +
    "                        <span ng-bind=\"restaurant.getZip()\"><span ng-if=\"restaurant.getZip()\">,</span></span>\n" +
    "                        <br> <span ng-bind=\"restaurant.getCountry()\"></span>\n" +
    "                    </p>\n" +
    "                </div>\n" +
    "                <div class=\"reward\">\n" +
    "                    <div class=\"style\">\n" +
    "                        <h2 ng-bind=\"Str.AllRewardPage.Text_Reward_Rating\"></h2>\n" +
    "                        <p ng-bind=\"rewards.wheelvalue\"></p> \n" +
    "                    </div>\n" +
    "                    <div class=\"style\">\n" +
    "                        <h2>{{Str.AllRewardPage.Text_Best_Rewards_Of}} {{restaurant.getTitle()}}</h2>\n" +
    "                        <p ng-repeat=\"reward in rewards.wheel track by $index\" ng-bind=\"reward\"></p>\n" +
    "                    </div>\n" +
    "                    <div class=\"style last\">\n" +
    "                        <h2 ng-bind=\"Str.AllRewardPage.Text_How_It_Works\"></h2>\n" +
    "                        <p ng-bind=\"Str.AllRewardPage.Text_How_It_Works_1\"></p>\n" +
    "                        <p ng-bind=\"Str.AllRewardPage.Text_How_It_Works_2\"></p>\n" +
    "                    </div>\n" +
    "                    <div ng-show=\"!mobileWebview\" class=\"style last\"> \n" +
    "                        <a ng-href=\"{{restaurant.getInternalPath()}}/booknow\">\n" +
    "                        <button analytics-on=\"click\"\n" +
    "                        analytics-event=\"c_book_resto_cppwheeldetails\"\n" +
    "                        analytics-category=\"c_book_resto\" ng-show=\"disableBookButtton != true\" id=\"btn_book\" class=\"book-button-sm btn-leftBottom-orange\" style=\"margin-bottom:15px;width:100%;\" ng-href=\"{{restaurant.getInternalPath()}}/booknow\">BOOK NOW</button>\n" +
    "                        </a>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("../app/components/all_rewards_page/_dining_rewards.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/components/all_rewards_page/_dining_rewards.tpl.html",
    "<div id=\"all-rewards\">\n" +
    "    <div class=\"container-fluid\">\n" +
    "        <div class=\"col-lg-8 col-md-8 col-sm-8 col-xs-12 text-center wheel\">\n" +
    "            <div ng-show=\"!mobileWebview\" id=\"breadcrumb\" breadcrumb base-url=\"base_url\" base-path=\"base_path\" restaurant=\"restaurant\"></div>\n" +
    "            <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12 wheel-image\">\n" +
    "                <a ng-if=\"!mobileWebview\" href=\"{{restaurant.getInternalPath()}}/booknow\">\n" +
    "                    <img class=\"img-responsive\" ng-src=\"{{restaurant.getWheelRewardImage()}}\">\n" +
    "                </a>\n" +
    "                <img ng-if=\"mobileWebview\" class=\"img-responsive\" ng-src=\"{{restaurant.getWheelRewardImage()}}\">\n" +
    "            </div>\n" +
    "            \n" +
    "            <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12 col-lg-offset-3 col-md-offset-3 col-sm-offset-3 text-center wheel-text\" ng-bind=\"Str.AllRewardPage.description\"></div>\n" +
    "        </div>\n" +
    "        <div class=\"col-lg-4 col-md-4 col-sm-4 col-xs-12\">\n" +
    "            <div class=\"col-lg-8 col-md-8 col-sm-8 col-xs-12 restaurant\">\n" +
    "                <div class=\"trans-block text-center\">\n" +
    "                    <div class=\"bar-logo text-center\">\n" +
    "                        <a  ng-show=\"!mobileWebview\"  rel=\"nofollow\" ng-href=\"{{restaurant.getInternalPath()}}\">\n" +
    "                            <img ng-src=\"{{restaurant.getLogoImage()}}\">\n" +
    "                        </a>\n" +
    "                        <img ng-show=\"mobileWebview\"  ng-src=\"{{restaurant.getLogoImage()}}\">\n" +
    "                    </div>\n" +
    "                    <a  ng-show=\"!mobileWebview\"  rel=\"nofollow\" ng-href=\"{{restaurant.getInternalPath()}}\">\n" +
    "                        <h1 ng-bind=\"restaurant.getTitle()\"></h1>\n" +
    "                    </a>\n" +
    "                    <h1 ng-show=\"mobileWebview\"   ng-bind=\"restaurant.getTitle()\"></h1>\n" +
    "                    <p><span ng-bind=\"restaurant.getAddress()\"></span><span ng-if=\"restaurant.getAddress()\">,</span>\n" +
    "                        <br><span ng-bind=\"restaurant.restaurantinfo.region\"></span>\n" +
    "                        <span ng-bind=\"restaurant.getZip()\"><span ng-if=\"restaurant.getZip()\">,</span></span>\n" +
    "                        <br> <span ng-bind=\"restaurant.getCountry()\"></span>\n" +
    "                    </p>\n" +
    "                </div>\n" +
    "                <div class=\"reward\">\n" +
    "                    <div class=\"style\">\n" +
    "                        <h2 ng-bind=\"Str.AllRewardPage.Text_Reward_Rating\"></h2>\n" +
    "                        <p ng-bind=\"restaurant.getWheelValue()\"></p>\n" +
    "                    </div>\n" +
    "                    <div class=\"style\">\n" +
    "                        <h2>{{Str.AllRewardPage.Text_Best_Rewards_Of}} {{restaurant.getTitle()}}</h2>\n" +
    "                        <p ng-repeat=\"reward in rewards.wheel track by $index\" ng-bind=\"reward\"></p>\n" +
    "                    </div>\n" +
    "                    <div class=\"style last\">\n" +
    "                        <h2 ng-bind=\"Str.AllRewardPage.Text_How_It_Works\"></h2>\n" +
    "                        <p ng-bind=\"Str.AllRewardPage.Text_How_It_Works_1\"></p>\n" +
    "                        <p ng-bind=\"Str.AllRewardPage.Text_How_It_Works_2\"></p>\n" +
    "                    </div>\n" +
    "                    \n" +
    "                    <div ng-show=\"!mobileWebview\" class=\"style last\">\n" +
    "                        <a ng-href=\"{{restaurant.getInternalPath()}}/booknow\">\n" +
    "                        <button analytics-on=\"click\"\n" +
    "                        analytics-event=\"c_book_resto_wheeldetails\"\n" +
    "                        analytics-category=\"c_book_resto\" ng-show=\"disableBookButtton != true\" id=\"btn_book\" class=\"book-button-sm btn-leftBottom-orange\" style=\"margin-bottom:15px;width:100%;\" >BOOK NOW</button>\n" +
    "                        </a>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("../app/components/booking/_booking.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/components/booking/_booking.tpl.html",
    "<div class=\"restaurant-info-page\" ng-if=\"restaurant != undefined\">\n" +
    "    <div class=\"text-center\" ng-style=\"restaurant.bannerStyleBookNow(bannerSizePath)\" style=\"overflow: hidden;\">\n" +
    "        <div class=\"container\" style=\"padding: 60px 0 0; margin-bottom: -55px\">\n" +
    "            <div id=\"breadcrumb\" breadcrumb base-url=\"base_url\" base-path=\"base_path\" restaurant=\"restaurant\"></div>\n" +
    "        </div>\n" +
    "        <book-iframe restaurant=\"restaurant\"></book-iframe>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("../app/components/booking/_booking_deposit.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/components/booking/_booking_deposit.tpl.html",
    "<div class=\"restaurant-info-page\" ng-if=\"restaurant != undefined\">\n" +
    "\n" +
    "    <div class=\"text-center\" ng-style=\"restaurant.bannerStyleBookNow(bannerSizePath)\" style=\"overflow: hidden;\">\n" +
    "        <div class=\"container\" style=\"padding: 60px 0 0; margin-bottom: -55px\">\n" +
    "            <div id=\"breadcrumb\" breadcrumb base-url=\"base_url\" base-path=\"base_path\" restaurant=\"restaurant\"></div>\n" +
    "        </div>\n" +
    " \n" +
    "        <bkdeposit-iframe restaurant=\"restaurant\"></bkdeposit-iframe>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("../app/components/booking/_booking_section.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/components/booking/_booking_section.tpl.html",
    "<div class=\"restaurant-info-page\" ng-if=\"restaurant != undefined\">\n" +
    "    <div class=\"text-center\" ng-style=\"restaurant.bannerStyleBookNow(bannerSizePath)\" style=\"overflow: hidden;\">\n" +
    "        <div class=\"container\" style=\"padding: 60px 0 0; margin-bottom: -55px\">\n" +
    "            <div id=\"breadcrumb\" breadcrumb base-url=\"base_url\" base-path=\"base_path\" restaurant=\"restaurant\"></div>\n" +
    "        </div>\n" +
    "        <booknowsection-iframe restaurant=\"restaurant\"></booknowsection-iframe>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("../app/components/cancel_booking/_cancel_booking.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/components/cancel_booking/_cancel_booking.tpl.html",
    "<div id=\"cancel_booking\">\n" +
    "    <div class=\"container\">\n" +
    "        <div class=\"col-xs-12 col-md-6 col-md-offset-3\">\n" +
    "            \n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>");
}]);

angular.module("../app/components/catering_page/_catering_page.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/components/catering_page/_catering_page.tpl.html",
    "<div  class=\"restaurant-info-page\" ng-if=\"restaurant != undefined\" >\n" +
    "    <a href=\"https://media.weeloy.com/upload/restaurant/SG_SG_R_TheFunKitchen/1440/catering.jpg\" class=\"fresco\">\n" +
    "        <div class=\"banner_restaurant\" ng-style=\"restaurant.bannerStyleCart()\" style=\"background-position: center center\">\n" +
    "        <div class=\"trans-block col-md-3\"  style='margin-left:20px;'>\n" +
    "                <div class=\"bar-logo\"><img ng-src=\"{{restaurant.getLogoImage()}}\" class=\"img-responsive\" alt=\"\"></div>\n" +
    "                <h1 ng-bind=\"restaurant.getTitle()\"></h1>\n" +
    "                <p><span ng-bind=\"restaurant.getAddress()\"></span>, <span ng-bind=\"restaurant.getZip()\"></span></p>\n" +
    "            </div>\n" +
    "            \n" +
    "        </div>\n" +
    "    </a>\n" +
    "<div id=\"catering\" class=\"main\">\n" +
    "    <div class=\"content\">\n" +
    "           <!-- <h1>Catering menu of {{restaurant.getTitle()}} {{takeoutitle}}</h1>-->\n" +
    "            <div class=\"\" ng-class=\"\" ng-repeat=\"menu in cateringmenu\">\n" +
    "                \n" +
    "                <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">\n" +
    "                    <h1 ng-bind=\"menu.categorie.value\"></h1>\n" +
    "                    <div class=\"col-lg-4 col-md-4 col-sm-6 col-xs-12 catering-vignette-parent \" ng-class=\"\"   ng-repeat=\"MenuItem in menu.items\" ng-if=\"MenuItem.takeout == 1\">\n" +
    "                        <div class=\"border catering-vignette\" >\n" +
    "                            <div class=\"custom-grid\">\n" +
    "                                <article class=\"ribbon_holder\">\n" +
    "                                        <figure>\n" +
    "                                            <div class=\"img-absolute\" >\n" +
    "                                                <img class=\"\" style=\"width:100%; height: auto;\"  ng-src=\"//media1.weeloy.com/upload/restaurant/{{restaurant.getRestaurantId()}}/300/{{MenuItem.mimage}}\">\n" +
    "                                            </div>\n" +
    "                                        </figure>\n" +
    "                                </article>\n" +
    "                            </div>\n" +
    "                            <div class=\"category-item\">\n" +
    "                                <div class=\"category-item-info\">\n" +
    "                                    <div class=\"location-icon\">\n" +
    "                                        <div class=\"location-text\" style=\"margin-top: 5px;\">\n" +
    "                                            <p style='font-weight: bold'><span>{{MenuItem.item_title}}</span></p>\n" +
    "                                            <p style='font-size: 12px'><span>{{MenuItem.item_description}}</span></p>\n" +
    "                                            <p><span>{{MenuItem.price}} {{restaurant.getCurrency()}}</span></p>\n" +
    "                                        </div>\n" +
    "                                    </div>\n" +
    "                                </div>\n" +
    "                                <div class=\"category-link\">\n" +
    "                                   \n" +
    "                                    <span ng-if=\"MenuItem.takeout == 1 && takeoutitle !== ''\" style='cursor:pointer' class=\"cart-label label label-info\" ng-click=\"addToCart(MenuItem, $event)\">\n" +
    "                                        <i class=\"fa fa-plus\"></i> \n" +
    "                                        <span>Add to Cart</span>\n" +
    "                                    </span>\n" +
    "                                    <span class=\"added-to-cart\" style=\"margin-left: 6px; display:none;\"> Added</span>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div class=\"sidebar\">\n" +
    "            <div class=\"inner cart-container\" set-class-when-not-at-top=\"cart-container\" set-class-when-at-top=\"fix-to-top\" >\n" +
    "                <div >\n" +
    "                    <cart ng-show=\"ItemsInCart | lengthMoreThan:0\" items=\"ItemsInCart\" \n" +
    "                          ng-attr-cx=\"true\" \n" +
    "                          ng-attr-minorder=\"restaurant.getMinOnlineOrder()\" \n" +
    "                          ng-attr-tax=\"restaurant.getOnlineOrderTax()\" ></cart>\n" +
    "                    <span ng-show=\"ItemsInCart | lengthLessThan:1 \" items=\"ItemsInCart\"  >Your cart is empty</span>\n" +
    "                    <br><br>\n" +
    "                </div>\n" +
    "                <br>\n" +
    "                <br>\n" +
    "            </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "    \n" +
    "    \n" +
    "    \n" +
    "    \n" +
    "    <!--<div set-class-when-touch-footer=\"cart-container3\" id=\"ttt\" style=\"background: red;width: 100px; height: 100px;\"></div>-->\n" +
    "</div>\n" +
    "");
}]);

angular.module("../app/components/checkout/_checkout.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/components/checkout/_checkout.tpl.html",
    "<div id=\"checkout\">\n" +
    "    <div class=\"container\">\n" +
    "        <p class=\"checkout-header text-center\">checkout cart</p>\n" +
    "        <div class=\"col-xs-12 restaurant\">\n" +
    "            <div class=\"bar-logo pull-left\">\n" +
    "                <img ng-src=\"{{restaurant.getLogoImage()}}\" class=\"img-responsive\" alt=\"\">\n" +
    "            </div>     \n" +
    "            <div class=\"restaurant-info\">\n" +
    "                <h1 ng-bind=\"restaurant.restaurantinfo.title\"></h1>\n" +
    "                 <p><span ng-bind=\"restaurant.restaurantinfo.address\"></span>, <span ng-bind=\"restaurant.restaurantinfo.zip\"></span></p>\n" +
    "            </div>       \n" +
    "        </div>\n" +
    "        <div class=\"cart col-sm-6 col-xs-12\">\n" +
    "            <p class=\"cart-header\" ng-bind=\"today\"></p>\n" +
    "            <cart ng-show=\"items | lengthMoreThan:0\" items=\"items\"></cart>\n" +
    "            <p ng-show=\"items | length:0\">Your cart is empty</p>\n" +
    "        </div>\n" +
    "        <div class=\"cart col-sm-6 col-xs-12\">\n" +
    "            <p class=\"cart-header\">Select delivery time</p>\n" +
    "            <ul class=\"delivery-time\">\n" +
    "                <li ng-repeat=\"time in DeliveryTime\" ng-click=\"SelectTime(time)\">\n" +
    "                    <p ng-class=\"time.selected ? 'selected' : ''\"><span class=\"fa fa-clock-o\"></span><span ng-bind=\"time.time\"></span></p>\n" +
    "                </li>\n" +
    "            </ul>\n" +
    "        </div>\n" +
    "        <div class=\"checkout-btn col-xs-12\" ng-show=\"items | lengthMoreThan:0\">\n" +
    "            <hr>\n" +
    "            <button class=\"btn btn-default pull-right\" ng-click=\"Checkout(items)\">Continue to checkout</button>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("../app/components/checkout_info/_checkout_info.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/components/checkout_info/_checkout_info.tpl.html",
    "<div id=\"checkout-info\">\n" +
    "    <div class=\"container\">\n" +
    "        <p class=\"checkout-header text-center\">checkout cart</p>\n" +
    "        <div id=\"main-form\" class=\"col-xs-12 col-md-8\">\n" +
    "            <a rel=\"nofollow\"  ng-href=\"{{restaurant.getInternalPath()}}/catering\">\n" +
    "                <p class=\"back-to-cart\">\n" +
    "                    <i class=\"fa fa-chevron-left\"></i>\n" +
    "                    <span>Back to restaurant page</span>\n" +
    "                </p>\n" +
    "            </a>\n" +
    "            <div id=\"contant-main-form\">\n" +
    "                <form id=\"form-border\" name=\"OrderForm\" ng-submit=\"OrderForm.$valid && SaveCart(OrderUser, remarks)\">\n" +
    "                    <div class=\"title col-xs-12 col-sm12 col-md-12 col-lg-12\">\n" +
    "                        <div class=\"numberCircle\">1</div>\n" +
    "                        <h4>Personal Details</h4>\n" +
    "                    </div>\n" +
    "                    <p ng-if=\"OrderUser.email === undefined\">Have an account width us? <a data-toggle=\"modal\" data-target=\"#loginModal\">Log in</a> to checkout faster!</p>\n" +
    "                    <div class=\"form-group col-xs-12 col-sm12 col-md-6 col-lg-6\">\n" +
    "                        <label>First name*</label>\n" +
    "                        <input type=\"text\" class=\"form-control\" name=\"firstname\" ng-model=\"OrderUser.firstname\" required>\n" +
    "                        <span ng-show=\"OrderForm.$submitted && OrderForm.firstname.$error.required\" ng-bind=\"Str.template.TextPleaseEnterYourFirstname\"></span>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group col-xs-12 col-sm12 col-md-6 col-lg-6\">\n" +
    "                        <label>Last name</label>\n" +
    "                        <input type=\"text\" class=\"form-control\" name=\"lastname\" ng-model=\"OrderUser.lastname\" required>\n" +
    "                        <span ng-show=\"OrderForm.$submitted && OrderForm.lastname.$error.required\" ng-bind=\"Str.template.TextPleaseEnteYourLastName\"></span>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group col-xs-12 col-sm12 col-md-6 col-lg-6\">\n" +
    "                        <label>Email*</label>\n" +
    "                        <input type=\"email\" class=\"form-control\" name=\"email\" ng-model=\"OrderUser.email\" required>\n" +
    "                        <span ng-show=\"OrderForm.$submitted && OrderForm.email.$error.required\" ng-bind=\"Str.template.TextPleaseEnterAValidEmail\"></span>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group col-xs-12 col-sm12 col-md-6 col-lg-6\">\n" +
    "                        <label>Phone*</label>\n" +
    "                        <input type=\"text\" class=\"form-control\" name=\"mobile\" ng-model=\"OrderUser.mobile\" required>\n" +
    "                        <span ng-show=\"OrderForm.$submitted && OrderForm.mobile.$error.required\" ng-bind=\"Str.template.TextPleaseEnterYourPhoneNumber\"></span>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group col-xs-12 col-sm12 col-md-6 col-lg-6\">\n" +
    "                        <label>Company name(Optional)</label>\n" +
    "                        <input type=\"text\" class=\"form-control\" ng-model=\"OrderUser.company\">\n" +
    "                    </div>\n" +
    "                    <!-- <div class=\"password-section\" ng-if=\"!loggedin\">\n" +
    "                        <div class=\"form-group col-xs-12 col-sm12 col-md-12 col-lg-12\">\n" +
    "                            Sign up below to have a better checkout experience next time!\n" +
    "                        </div>\n" +
    "                        <div class=\"form-group col-xs-12 col-sm12 col-md-6 col-lg-6\">\n" +
    "                            <label>Password(Optional)</label>\n" +
    "                            <input type=\"password\" class=\"form-control\" ng-model=\"OrderUser.password\" required>\n" +
    "                        </div>\n" +
    "                        <div class=\"form-group col-xs-12 col-sm12 col-md-6 col-lg-6\">\n" +
    "                            <label>Enter your password again</label>\n" +
    "                            <input type=\"password\" class=\"form-control\" ng-model=\"OrderUser.password_confirmation\" required>\n" +
    "                        </div>\n" +
    "                    </div> -->\n" +
    "                    <div class=\"title col-xs-12 col-sm12 col-md-12 col-lg-12\">\n" +
    "                        <div class=\"numberCircle\">2</div>\n" +
    "                        <h4>Pickup date and time</h4>\n" +
    "                    </div>\n" +
    "                    <div class=\"title col-xs-12\">\n" +
    "                        <div class=\"form-group\">\n" +
    "                            <div class=\"row\">\n" +
    "                                <div class=\"col-md-8\">\n" +
    "                                    <div id=\"datetimepicker\"></div>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                        <div class=\"cart\" >\n" +
    "                            <p class=\"cart-header\">Select pickup time</p>\n" +
    "                            <div class=\"row\">\n" +
    "                                <div class=\"col-lg-3 col-md-4 col-sm-4 col-xs-6\" ng-repeat=\"rows in chunkedData\">\n" +
    "                                    <ul class=\"delivery-time\" class=\"cart col-sm-6 col-xs-12\">\n" +
    "                                        <li ng-repeat=\"time in rows\" ng-click=\"SelectTime(time)\">\n" +
    "                                            <p ng-class=\"time.selected ? 'selected' : ''\"><span class=\"fa fa-clock-o\"></span><span ng-bind=\"time.time\" style=\"font-size: 13px;\"></span></p>\n" +
    "                                        </li>\n" +
    "                                    </ul>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div class=\"title col-xs-12 col-sm12 col-md-12 col-lg-12\">\n" +
    "                        <div class=\"numberCircle\">3</div>\n" +
    "                        <h4>Special requests</h4>\n" +
    "                        <textarea class=\"form-control remarks\" ng-model=\"remarks\"></textarea>\n" +
    "                    </div>\n" +
    "                    <div class=\"title col-xs-12 col-sm12 col-md-12 col-lg-12\">\n" +
    "                        <div class=\"numberCircle\">4</div>\n" +
    "                        <h4>Terms and conditions</h4>\n" +
    "                        <div class=\" col-xs-12 col-sm12 col-md-12 col-lg-12\">\n" +
    "                            <p><span ng-bind=\"restaurant.getOnlineOrderTnc()\"></span></p>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    \n" +
    "                    <div class=\"title col-xs-12 col-sm12 col-md-12 col-lg-12\">\n" +
    "                        <div class=\"numberCircle\">5</div>\n" +
    "                        <h4>Payment method</h4>\n" +
    "                        <div class=\"form-group checkbox col-xs-12 col-sm12 col-md-12 col-lg-12\">\n" +
    "                            <label>\n" +
    "                                <input type=\"radio\" name=\"optionsPaymentRadios\" id=\"optionsPaymentRadios1\"  value=\"option1\" required> Credit card\n" +
    "                            </label>\n" +
    "                            <label>\n" +
    "                                <input type=\"radio\" name=\"optionsPaymentRadios\" id=\"optionsPaymentRadios2\" value=\"option2\" required> Paypal\n" +
    "                            </label>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    \n" +
    "                    <div class=\"title col-xs-12 col-sm12 col-md-12 col-lg-12\">                        \n" +
    "                        <div class=\"form-group checkbox col-xs-12 col-sm12 col-md-12 col-lg-12\">\n" +
    "                            <label>\n" +
    "                                <input type=\"checkbox\" name=\"optionsRadios_tnc\" id=\"optionsRadios1\" value=\"option1\" ng-model=\"OrderUser.optionsRadios_tnc\" required> I accept the terms & conditions\n" +
    "                                <span ng-show=\"OrderForm.$submitted && OrderForm.optionsRadios_tnc.$error.required\" ng-bind=\"Str.template.TextPleaseAcceptTnc\"></span>\n" +
    "                            </label>\n" +
    "                        </div>\n" +
    "                        \n" +
    "                        <div class=\"form-group checkbox col-xs-12 col-sm12 col-md-12 col-lg-12\">\n" +
    "                            <label>\n" +
    "                                \n" +
    "                                <input type=\"checkbox\" name=\"optionsRadios\" id=\"optionsRadios2\" value=\"option2\" checked> I want to receive email & newsletter\n" +
    "                            </label>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "\n" +
    "                    <div class=\"title col-xs-12 col-sm12 col-md-12 col-lg-12\">\n" +
    "                        <button type=\"submit\" class=\"btn btn-default\">Submit Order</button>\n" +
    "                    </div>\n" +
    "                </form>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div id=\"sidebar-right\" class=\"col-xs-12 col-md-4 sidebar\">\n" +
    "            <div class=\"sidebar-contant\">\n" +
    "                <div class=\"img-logo\">\n" +
    "                    <img ng-src=\"{{restaurant.getLogoImage()}}\" />\n" +
    "                </div>\n" +
    "                <h3 class=\"text-center\" ng-bind=\"restaurant.getTitle()\"></h3>\n" +
    "                <p class=\"text-center\" ng-if=\"false\">\n" +
    "                    <span ng-bind=\"restaurant.getAddress()\"></span>, <span ng-bind=\"restaurant.getZip()\"></span>\n" +
    "                </p>\n" +
    "                <span>Pick up Address</span>\n" +
    "                <p><span ng-bind=\"restaurant.getAddress()\"></span>, <span ng-bind=\"restaurant.getZip()\"></span></p>\n" +
    "                <span>Pick up Date</span>\n" +
    "                <p ng-bind=\"DeliveryDateFormatted\"></p>\n" +
    "                <span>Pick up Time</span>\n" +
    "                <p ng-bind=\"DeliveryTime\"></p>\n" +
    "                <hr/>\n" +
    "                <cart ng-show=\"items | lengthMoreThan:0\" items=\"items\" \n" +
    "                      ng-attr-cx=\"false\" \n" +
    "                      ng-attr-minorder=\"restaurant.getMinOnlineOrder()\" \n" +
    "                      ng-attr-tax=\"restaurant.getOnlineOrderTax()\"></cart>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("../app/components/cny_page/_cny_page.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/components/cny_page/_cny_page.tpl.html",
    "<style>\n" +
    "    .introduction{\n" +
    "        margin: 50px 0px;\n" +
    "        font-weight: bold;\n" +
    "    }\n" +
    "</style>\n" +
    "<div  class=\"landing-page\">\n" +
    "    <div class=\"container\">\n" +
    "        <div class=\"left\">\n" +
    "            <div class=\"banner_restaurant\" style=\"background-image: url({{image}}); background-size: cover; background-position: 50% 50%; background-repeat: no-repeat no-repeat;\">\n" +
    "            </div>\n" +
    "             <h1>{{landing.title}}</h1>\n" +
    "             <h2>{{landing.tag}}</h2>\n" +
    "            <p class='introduction'>\n" +
    "                {{landing.description}}\n" +
    "            </p>\n" +
    "            <p style='margin-top:30px;margin-left:120px;padding-left:50px; color: #2aacd2; display: block; font-family: proximanova-semibold;font-size: 20px;'> CNY Menus & Promos</p>\n" +
    "            <div class=\"events\" >\n" +
    "                <ul style=\"max-width:600px; margin: auto; padding: 0px;\">\n" +
    "                    <li class=\"clearfix\" ng-repeat=\"evt in events\">\n" +
    "                        \n" +
    "                        <a analytics-on=\"click\"\n" +
    "                                        analytics-event=\"c_go_resto\"\n" +
    "                                        analytics-category=\"c_go_resto_cny\" \n" +
    "                                        analytics-label=\"c_go_resto_cny_{{evt.restaurant}}\"\n" +
    "                                        ng-href=\"{{getRestaurantLink(evt.restaurant)}}\"  style=\"text-decoration:none;\" title=\"Celebrate the CNY with {{evt.title}} - reserve your table with weeloy\">\n" +
    "                            <h1>Celebrate the CNY with {{evt.title}}</h1>\n" +
    "                        </a>\n" +
    "                        <div class=\"cnt\">\n" +
    "                            <h2 ng-bind=\"evt.name\"></h2>\n" +
    "                            <span ng-if=\"evt.start === evt.end\" ng-bind=\"evt.end | date : format : shortDate\"></span>\n" +
    "                            <span  ng-if=\"evt.start !== evt.end\">{{evt.start| date : format : shortDate}} to {{evt.end| date : format : shortDate}}, {{evt.area}}</span>\n" +
    "                            <p ng-bind=\"evt.description\"></p>\n" +
    "                            \n" +
    "                            <a analytics-on=\"click\"\n" +
    "                                        analytics-event=\"c_get_pdf\"\n" +
    "                                        analytics-category=\"c_get_pdf_cny\" \n" +
    "                                        analytics-label=\"c_get_pdf_cny_{{evt.restaurant}}\" href=\"{{ mediaServer}}/upload/restaurant/{{evt.restaurant}}/{{evt.pdf_link}}\" target=\"_blank\" ng-if=\"evt.pdf_link\" title=\"{{evt.title}} Menu for chinese new year 2016\">Download CNY menu</a>\n" +
    "\n" +
    "                            <div style=\"margin-top:10px;\">\n" +
    "                                <button analytics-on=\"click\"\n" +
    "                                        analytics-event=\"c_book_resto\"\n" +
    "                                        analytics-category=\"c_book_resto_cny\" \n" +
    "                                        analytics-label=\"c_book_resto_cny_{{evt.restaurant}}\" ng-show=\"restaurant.status != 'comingsoon'\" class=\"book-button-sm btn-leftBottom-orange\" ng-click=\"btnBookNowEvent_click(evt.restaurant)\" ng-bind=\"book_button_text\"></button>\n" +
    "                            </div> \n" +
    "                        </div>\n" +
    "                        <div class=\"img-block\">\n" +
    "                            <a href=\"{{ image}}\" class=\"fresco\" title=\"Chinese New Year @ {{landing.title}} - reserve restaurant table now\">\n" +
    "                                <img ng-src=\"{{ image}}\" class=\"img-responsive\" alt=\"{{landing.title}} Chinese New Year Menu - reserve restaurant table now\" style='max-height: 300px;'>\n" +
    "                                \n" +
    "                            </a>\n" +
    "                        </div>\n" +
    "                    </li>\n" +
    "                </ul>\n" +
    "            </div>\n" +
    "            <div style=\"text-align: center;\">\n" +
    "                <a href='search/singapore' style=\" color: #555555;text-decoration: none;font-size: 17px;\">View more restaurants</a>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("../app/components/co_branding/_co_branding_page.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/components/co_branding/_co_branding_page.tpl.html",
    "<section class ='early_book_section' style='margin-top:100px;'>\n" +
    "    <div class=\"dsbbannerholder\">\n" +
    "      <div class=\"dsbbannerimg\" style=\"background-image: url({{ pageheader.images}})\">\n" +
    "          <div class = \"titleboard\">\n" +
    "            <div class=\"dsbtextc\">{{pageheader.title}}</div>\n" +
    "            <div class=\"dsbtext\">{{pageheader.description}}</div>\n" +
    "\n" +
    "            \n" +
    "          </div>\n" +
    "        </div>  \n" +
    "    </div>\n" +
    "    <!-- <div class = \"titleboard\"><h1>Daily Special Board for {{todaydateFormat}}</h1><a href ng-click=\"registernewresto()\">Register New Restaurant</a></div>  -->\n" +
    "    <div class='showdata' when-scrolled =\"more()\">\n" +
    "         <div  ng-repeat=\"item in restaurants\" style=' padding-top:0px;'>\n" +
    "            <div class=\"itempanel\">\n" +
    "              <div class=\"itempanelinside\">\n" +
    "                <div class=\"itemimage\">\n" +
    "                     <a ng-href=\"{{ item.getInternalPath() }}\" target=\"_blank\">\n" +
    "                        <img ng-src=\"images/transparent.png\" Title= \"{{item.title}}\" style='width:100%;height: 100%;background: url(\"https://media2.weeloy.com/upload/restaurant/{{item.restaurant}}/{{item.image}}\");background-position: center;background-repeat: no-repeat;background-size: cover;'>\n" +
    "                     </a>\n" +
    "                </div>\n" +
    "                 <div class=\"headline\">\n" +
    "                   {{item.title}}\n" +
    "                </div>\n" +
    "                  <div class='item_details'>\n" +
    "                   <div class='item_details'><i class=\"fa fa-tag\"></i><span ng-repeat=\"cusine in item.getCuisineAsArray()\">{{cusine}}<span ng-show=\"!$last\">, </span></span>\n" +
    "                    </div>\n" +
    "                    <div ng-if=\"item.city != '' || item.region != ''\"><i class=\"fa fa-map-marker\"></i> {{item.city}}\n" +
    "                        <span><span ng-if=\"item.region != ''\">, </span> {{item.region}}</span>\n" +
    "                    </div>\n" +
    "                    <div class=\"cpricing\">\n" +
    "                        {{item.pricing}}\n" +
    "                    </div>\n" +
    "                  </div>\n" +
    "                <div class=\"cbtnholder\">\n" +
    "                     <a ng-href=\"{{ item.getInternalPath() }}/booknow\" target=\"_blank\">\n" +
    "                    <center>\n" +
    "                        <input class=\"input-group-addon book-button-sm2 btn-leftBottom-blue-noborder\" type=\"button\" name=\"\" value=\"Book Now\">\n" +
    "                    </center>\n" +
    "                     </a>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "       \n" +
    "         </div>\n" +
    "         \n" +
    "    </div>\n" +
    "           \n" +
    "         \n" +
    "    </div>\n" +
    "    <div ng-show=\"loadingimg\">\n" +
    "            <center>\n" +
    "              <img src=\"images/loading2.gif\">\n" +
    "            </center>\n" +
    "          </div>\n" +
    "\n" +
    "</section>\n" +
    "\n" +
    "\n" +
    "");
}]);

angular.module("../app/components/contest_page/_contest_page.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/components/contest_page/_contest_page.tpl.html",
    "<style>\n" +
    "    .main-content{\n" +
    "        margin: 20px 0px;\n" +
    "        font-weight: bold;\n" +
    "    }\n" +
    "    h1{\n" +
    "        text-align: center;\n" +
    "    }\n" +
    "    h2{\n" +
    "        text-align: center;\n" +
    "        padding: 20px 0;\n" +
    "    }\n" +
    "    h3{\n" +
    "        padding: 25px;\n" +
    "        text-align: center;\n" +
    "    }\n" +
    "    h4{\n" +
    "        padding-top: 80px;\n" +
    "\n" +
    "    }\n" +
    "\n" +
    "    .container .wrapper{\n" +
    "        max-width: 750px;\n" +
    "        margin: auto;\n" +
    "    }\n" +
    "\n" +
    "    .container ul{\n" +
    "        padding:0px;\n" +
    "    }\n" +
    "\n" +
    "    .container li {\n" +
    "        list-style: outside none none;\n" +
    "        padding-bottom: 10px;\n" +
    "        text-decoration: none;\n" +
    "    }\n" +
    "</style>\n" +
    "\n" +
    "<div  class=\"zen-month\">\n" +
    "    <div class=\"container\" style=\"\">\n" +
    "        <h1>\n" +
    "            <div ng-if='!isMobile' title='Dine and win contest - Book a restaurant & Win $50 Dining vouchers' class=\"banner_restaurant\" style=\"margin-top:50px;background-size:contain;background-image: url('https://static.weeloy.com/images/headers/contest-main-banner.jpg'); background-position: center center; background-repeat: no-repeat no-repeat;\">\n" +
    "                <img src=\"https://static.weeloy.com/images/headers/contest-main-banner.jpg\" style=\"visibility: hidden;max-height: 390px;max-width: 100%;\" alt=\"Dine and win contest - Book a restaurant & Win $50 Dining vouchers\"/>\n" +
    "            </div>\n" +
    "            <div ng-if='isMobile' title='Dine and win contest - Book a restaurant & Win $50 Dining vouchers' class=\"banner_restaurant\" style=\"margin-top:50px;background-size:contain;background-image: url('https://static.weeloy.com/images/headers/contest-main-banner-mobile.png'); background-position: center center; background-repeat: no-repeat no-repeat;\">\n" +
    "                <img src=\"https://static.weeloy.com/images/headers/contest-main-banner-mobile.png\" style=\"visibility: hidden;max-height: 600px;max-width: 100%\" alt=\"Dine and win contest - Book a restaurant & Win $50 Dining vouchers\"/>\n" +
    "            </div>\n" +
    "        </h1>\n" +
    "        <div class=\"wrapper\">\n" +
    "            <h2 style='padding-top:10px;'>Book a restaurant & Win $50 Dining vouchers</h2>\n" +
    "            <p class='main-content' >\n" +
    "                Book your favourite restaurant on Weeloy.com or mobile app between 10 September to 31 December 2016 and stand a chance to win $50 Dining Vouchers in our Dine & Win contest.\n" +
    "                10 lucky winners will win $50 dining vouchers each. Double* your chance of winning when you book with Weeloy mobile app! \n" +
    "            </p>\n" +
    "            <div style=\"text-align:center; padding: 30px;\">\n" +
    "                <button ng-click=\"go('/search/singapore')\" analytics-on=\"click\" analytics-event=\"c_book_resto\" analytics-category=\"c_book_resto_restopage1\" class=\"book-button btn-leftBottom-orange ng-binding\">Book Now</button>\n" +
    "            </div>\n" +
    "            <h3>How to Participate</h3>\n" +
    "            <ul >\n" +
    "                <li>\n" +
    "                    Reserve a table and dine at any Singapore based restaurants listed on <a href=\"www.weeloy.com\">weeloy.com</a> or Weeloy mobile app (<a href ='http://itunes.apple.com/app/id973030193' >iOS</a> or <a href ='https://play.google.com/store/apps/details?id=com.weeloy.client' >Android</a>)  between 10 September till 31 December 2016, 2359 (GMT +8, Singapore time).\n" +
    "                </li>\n" +
    "                <li>\n" +
    "                    All successful reservations within the qualification period automatically qualifies.\n" +
    "                </li>\n" +
    "                <li>\n" +
    "                    Double your chances of winning by making a reservation on Weeloy mobile app. Free to download on  App store  or  Play store\n" +
    "                </li>\n" +
    "            </ul>\n" +
    "            <h3> Eligibility </h3>\n" +
    "            <ul >\n" +
    "                <li>\n" +
    "                    Every successful reservation made on the Weeloy mobile app are entitled to 2 (TWO) chances of winning the contest and 1 (ONE) chance of winning if made on <a href=\"www.weeloy.com\">Weeloy.com</a>.\n" +
    "                </li>\n" +
    "                <li>\n" +
    "                    Successful reservation – The reservation must be made between 10 September to 31 December 2016, 2359 (GMT +8, Singapore time). Reservations that were cancelled, no-show (failed to turn up) or modified outside the qualifying window will be disqualified. Weeloy reserves the right to reject any entry for any reason.\n" +
    "                </li>\n" +
    "            </ul>\n" +
    "            <h4> The terms and conditions </h4>\n" +
    "            <div style='font-style: italic;font-size: 12px;' >\n" +
    "                <h5 >Contest Qualification</h5>\n" +
    "                <p>1. This promotion is organized by Weeloy Pte Ltd (\"Weeloy\"). It is open to all patrons of Weeloy, except for employees of Weeloy Pte. Ltd. and participating restaurants/advertising partners, and their respective immediate family members.\n" +
    "                    2. This promotion is valid from <b>10 September 2016 – 31 December 2016.</b>\n" +
    "                    3. This promotion is open to all residents of Singapore with a valid address in Singapore and who are at least 18 years old.\n" +
    "                    4. Participants must make at least One successful reservation at any restaurants listed on <a href=\"www.weeloy.com\"><b>weeloy.com</b></a> or <b>Weeloy mobile app <a href ='http://itunes.apple.com/app/id973030193' >iOS</a> or <a href ='https://play.google.com/store/apps/details?id=com.weeloy.client' >Android</a></b>.\n" +
    "                    5. Every successful reservation entitles the participant to One chance (if reserve on Weeloy.com) or Two chances (if reserve on Weeloy mobile app) of winning the contest during the qualification period. Participants must not cancel, no-show (failed to turn-up) or modify the booking the reservation outside the qualifying window.\n" +
    "                    6. Reservation must happen by 2359hour, 31 December 2016 to qualify.\n" +
    "                    7. Previous winners in earlier contests organized by Weeloy Pte Ltd in the past 6 months will not qualify for the draws in this campaign. Weeloy reserves the right to disqualify any such entries from previous winners at any point in time.\n" +
    "\n" +
    "                <h5 >Drawing of Winner(s)</h5> \n" +
    "                <p>8. Ten (10) winners will be drawn at the end of the qualifying period at <b>83 Amoy Street, Singapore 069902</b>. The Draw will be conducted on the Monday after the contest concludes, at the same venue.\n" +
    "                    9. A total 10 winners will win S$50 dining voucher each.\n" +
    "                    10. Winners will be notified via their registered email and contact number. The winners list will be announced on Weeloy Facebook by 31th December, 2016.\n" +
    "                    11. Participants will be contacted after the conclusion of the draw through the email address and/or telephone number provided in the reservation details and must satisfy a verification process before being declared the winner. Participants must provide a copy of their NRIC/FIN/Proof of local address for verification. If a participant cannot be contacted or fails to respond by the end of the next working day after the draw, this participant will be disqualified and another winner will be selected. Weeloy’s decision shall be final and irrevocable.\n" +
    "                    12. The award of the prizes may be subject to additional terms and conditions which are not mentioned herein and the winners must agree to such terms and conditions in writing in order to receive the prize. Weeloy reserves the right to declare no winner for any draw if it is unable to contact or verify participants after repeated attempts, or if a designated winner fails to agree to the additional terms and conditions or for any other reason whatsoever.\n" +
    "                    13. By participating in this promotion, the winners agree to participate in any form of publicity for this promotion, and to allow Weeloy to use their personal data, photographs and video/audio recordings for marketing & promotional purposes. Weeloy reserves the right to disqualify the winner(s) in the event that the winner(s) refuses or fails to participate in such publicity for this promotion.\n" +
    "                    14. Prizes are non-transferable or exchangeable for cash, credit, goods or services. Weeloy reserves the right to replace the prizes with other items of similar value without giving prior notice.\n" +
    "                    15. By participating in this promotion, the participant agrees to be bound by the official rules, regulations and decisions of Weeloy. Weeloy reserves the right to disqualify any winners if he/she is found to have violated the rules and regulations in any way. The decisions of Weeloy on all matters relating to this promotion are final, conclusive and binding.</p>\n" +
    "\n" +
    "                <h5 >Personal Data</h5> \n" +
    "                <p> 16. Each participant’s personal data will be collected, used, disclosed and/or processed by Weeloy Pte Ltd for the following purposes:\n" +
    "                    (a) For Weeloy or its designated representatives or business partners to provide goods and services to you or parties designated by you and matters ancillary thereto.\n" +
    "                    (c) For verification and record of your personal particulars including comparing it with information from other sources and using the information to communicate with you.\n" +
    "                    (d) For research and analysis, including surveys and polls.\n" +
    "                    (e) To send you notices, information, promotions and updates including marketing and advertising materials in relation to Weeloy’s goods and services and those of third party organizations selected by Weeloy.\n" +
    "                    (f) To be disclosed to the sponsors/advertisers for the purpose of prize collection.</p>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("../app/components/dsb_page/_dsb_page.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/components/dsb_page/_dsb_page.tpl.html",
    "<style media=\"screen\">\n" +
    "\n" +
    ".dailyspecialcontainer {\n" +
    "    float: left;\n" +
    "    height: 340px;\n" +
    "    width: 272px;\n" +
    "    margin-bottom: 15px;\n" +
    "    margin-left: 15px;\n" +
    "\n" +
    "}\n" +
    "\n" +
    ".dailyspecialimage {\n" +
    "    width: 270px;\n" +
    "    height: 160px;\n" +
    "    background-color: #f2f2f2;\n" +
    "    margin-bottom: 5px;\n" +
    "}\n" +
    "\n" +
    ".dailyrestaurantcontainer\n" +
    "{\n" +
    "  font-weight: bold;\n" +
    "  font-size: 14px;\n" +
    "}\n" +
    "\n" +
    ".dailytitlecontainer \n" +
    "{\n" +
    "    font-weight: bold;\n" +
    "    font-size: 16px;\n" +
    "}\n" +
    "\n" +
    ".dailyspecialdesc {\n" +
    "    width: 270px;\n" +
    "    height: 110px;\n" +
    "    overflow-y: auto;\n" +
    "}\n" +
    "\n" +
    ".clearboth \n" +
    "{\n" +
    "  clear: both;\n" +
    "}\n" +
    "\n" +
    ".custombtn \n" +
    "{\n" +
    "  padding: 9px 100px;\n" +
    "}\n" +
    "\n" +
    ".showdata::-webkit-scrollbar {\n" +
    "  display: none;\n" +
    "}\n" +
    "\n" +
    ".mainholder{\n" +
    "    height: 100%;\n" +
    "    width: 100%;\n" +
    "    overflow: hidden;\n" +
    "}\n" +
    "\n" +
    ".weeloy-text {\n" +
    "  color: #FFFFFF;\n" +
    "  background-color: #283271 !important;\n" +
    "}\n" +
    "\n" +
    ".weeloy-text:hover {\n" +
    "    color: #FFFFFF;\n" +
    "}\n" +
    "\n" +
    ".modal-dialog-dsb {\n" +
    "    width: 880px;\n" +
    "    margin: 20px auto;\n" +
    "}\n" +
    "\n" +
    ".modal-dialog-dsbbooknow{\n" +
    "  width: 600px;\n" +
    "  margin: 10px auto;\n" +
    "}\n" +
    "\n" +
    ".modal-dialog-dsbbookregister {\n" +
    "    width: 660px;\n" +
    "    margin: 10px auto;\n" +
    "}\n" +
    "\n" +
    ".showdata {\n" +
    "    height: 530px;\n" +
    "    max-width: 990px;\n" +
    "    overflow-y: scroll;\n" +
    "    margin-left: auto;\n" +
    "    margin-right: auto;\n" +
    "}\n" +
    "\n" +
    ".dsbbannerholder{\n" +
    "  max-width: 990px;\n" +
    "  height: 170px;\n" +
    "  margin-left: auto;\n" +
    "  margin-right: auto;\n" +
    "  margin-bottom: 10px;\n" +
    "}\n" +
    "\n" +
    ".modal-header-dsb {\n" +
    "    min-height: 16.42857143px;\n" +
    "    padding: 15px;\n" +
    "    border-bottom: none;\n" +
    "}\n" +
    "\n" +
    ".modal-open .modal {\n" +
    "    overflow-x: hidden;\n" +
    "    overflow-y: auto;\n" +
    "}\n" +
    "\n" +
    ".itempanel {\n" +
    "    width: 198px;\n" +
    "    height: 265px;\n" +
    "    background-color: #ffffff;\n" +
    "    float: left;\n" +
    "}\n" +
    "\n" +
    ".itempanelinside {\n" +
    "  border: 1px solid #e2e2e2;\n" +
    "  /*border-radius: 4px;*/\n" +
    "  width: 183px;\n" +
    "  height: 255px;\n" +
    "  margin-left: auto;\n" +
    "  margin-right: auto;\n" +
    "}\n" +
    "\n" +
    ".headline{\n" +
    "    font-weight: bold;\n" +
    "    height: 35px;\n" +
    "    font-size: 12px;\n" +
    "    padding-left: 5px;\n" +
    "    padding-right: 5px;\n" +
    "    padding-top: 5px;\n" +
    "    margin-bottom: 2px;\n" +
    "    line-height: 14px;\n" +
    "\n" +
    "    overflow: hidden;\n" +
    "    text-overflow: ellipsis;\n" +
    "    display: -webkit-box;\n" +
    "    max-height: 36px;\n" +
    "    -webkit-line-clamp: 2;\n" +
    "    -webkit-box-orient: vertical;\n" +
    "}\n" +
    "\n" +
    ".itemimage{\n" +
    "  \n" +
    "  width: 182px;\n" +
    "  height: 125px;\n" +
    "}\n" +
    "\n" +
    ".restoiconimage{\n" +
    "    width: 24px;\n" +
    "    height: 24px;\n" +
    "    margin-left: 5px;\n" +
    "    margin-top: 2px;\n" +
    "    margin-bottom: 5px;\n" +
    "}\n" +
    "\n" +
    ".itemcontent{\n" +
    "    height: 58px;\n" +
    "    font-size: 12px;\n" +
    "    padding-left: 5px;\n" +
    "    padding-right: 5px;\n" +
    "    margin-bottom: 3px;\n" +
    "    line-height: 14px;\n" +
    "    overflow: hidden;\n" +
    "    text-overflow: ellipsis;\n" +
    "    display: -webkit-box;\n" +
    "    max-height: 60px;\n" +
    "    -webkit-line-clamp: 4;\n" +
    "    -webkit-box-orient: vertical;\n" +
    "}\n" +
    "\n" +
    ".restoicon{\n" +
    "  float: left;\n" +
    "  width: 24px;\n" +
    "  height: 24px;\n" +
    "}\n" +
    ".restodet{\n" +
    "  float: left;\n" +
    "  width: 145px;\n" +
    "  height: 30px;\n" +
    "  margin-left: 7px;\n" +
    "  font-size: 11px;\n" +
    "}\n" +
    "\n" +
    ".restoname {\n" +
    "    height: 15px;\n" +
    "    line-height: 14px;\n" +
    "    overflow: hidden;\n" +
    "    text-overflow: ellipsis;\n" +
    "}\n" +
    "\n" +
    ".restoadd {\n" +
    "    height: 15px;\n" +
    "    line-height: 14px;\n" +
    "    overflow: hidden;\n" +
    "    text-overflow: ellipsis;\n" +
    "    line-height: 9px;\n" +
    "}\n" +
    "\n" +
    ".modalimage {\n" +
    "    max-width: 840px;\n" +
    "    margin-left: auto;\n" +
    "    margin-right: auto;\n" +
    "    max-height: 473px;\n" +
    "    margin-bottom: 13px;\n" +
    "}\n" +
    "\n" +
    ".modalheader {\n" +
    "    margin-top: 15px;\n" +
    "    margin-left: 20px;\n" +
    "    margin-right: 20px;\n" +
    "    margin-bottom: 15px;\n" +
    "    font-size: 30px;\n" +
    "}\n" +
    "\n" +
    ".modaltextcontent {\n" +
    "    padding-left: 20px;\n" +
    "    padding-right: 20px;\n" +
    "    background-color: #fff;\n" +
    "}\n" +
    "\n" +
    ".modalcontent {\n" +
    "    width: 880px;\n" +
    "    position: relative;\n" +
    "    background-color: #fff;\n" +
    "    -webkit-background-clip: padding-box;\n" +
    "    background-clip: padding-box;\n" +
    "    border: 1px solid rgba(253, 253, 253, 0.2);\n" +
    "    outline: 0;\n" +
    "    /* -webkit-box-shadow: 0 3px 9px rgba(0,0,0,.5); */\n" +
    "    /* box-shadow: 0 3px 9px rgba(0,0,0,.5); */\n" +
    "}\n" +
    "\n" +
    ".modalcontentbooknow {\n" +
    "    width: 650px;\n" +
    "    height: 700px;\n" +
    "    position: relative;\n" +
    "    background-color: #fff;\n" +
    "    -webkit-background-clip: padding-box;\n" +
    "    background-clip: padding-box;\n" +
    "    border: 1px solid #999;\n" +
    "    border: 1px solid rgba(0,0,0,.2);\n" +
    "    outline: 0;\n" +
    "    -webkit-box-shadow: 0 3px 9px rgba(0,0,0,.5);\n" +
    "    box-shadow: 0 3px 9px rgba(0,0,0,.5);\n" +
    "}\n" +
    "\n" +
    ".modalcontentbooknowmod {\n" +
    "    width: 650px;\n" +
    "    height: 940px;\n" +
    "    position: relative;\n" +
    "    background-color: #fff;\n" +
    "    -webkit-background-clip: padding-box;\n" +
    "    background-clip: padding-box;\n" +
    "    border: 1px solid #999;\n" +
    "    border: 1px solid rgba(0,0,0,.2);\n" +
    "    outline: 0;\n" +
    "    -webkit-box-shadow: 0 3px 9px rgba(0,0,0,.5);\n" +
    "    box-shadow: 0 3px 9px rgba(0,0,0,.5);\n" +
    "}\n" +
    "\n" +
    ".modalbuttom {\n" +
    "    background-color: #fff;\n" +
    "    height: 50px;\n" +
    "    margin-top: 20px;\n" +
    "    margin-left: 20px;\n" +
    "    margin-bottom: 20px;\n" +
    "}\n" +
    "\n" +
    ".restoiconimagemodal{\n" +
    "    width: 50px;\n" +
    "    height: 50px;\n" +
    "    float: left;\n" +
    "    margin-right: 10px;\n" +
    "}\n" +
    "\n" +
    ".restonamemodal {\n" +
    "    font-weight: bold;\n" +
    "    height: 15px;\n" +
    "    line-height: 14px;\n" +
    "    \n" +
    "}\n" +
    "\n" +
    ".restoaddmodal {\n" +
    "    height: 15px;\n" +
    "    line-height: 14px;\n" +
    "    overflow: hidden;\n" +
    "    text-overflow: ellipsis;\n" +
    "}\n" +
    "\n" +
    ".restonamemodal {\n" +
    "  height: 15px;\n" +
    "  width: 600px;\n" +
    "  line-height: 14px;\n" +
    "}\n" +
    "\n" +
    ".restoaddressmodal {\n" +
    "  height: 15px;\n" +
    "  width: 600px;\n" +
    "  line-height: 14px;\n" +
    "}\n" +
    "\n" +
    ".dailyspecialbtn {\n" +
    "    float: right;\n" +
    "    margin-right: 20px;\n" +
    "    margin-top: 0px;\n" +
    "}\n" +
    "\n" +
    ".partners{\n" +
    "  float: right;\n" +
    "  width: 15px;\n" +
    "  height: 15px;\n" +
    "  margin-top: 10px;\n" +
    "}\n" +
    "\n" +
    ".regbtn{\n" +
    "  float: right;\n" +
    "}\n" +
    "\n" +
    ".titleboard{\n" +
    "    text-align: center;\n" +
    "    color: #fff;\n" +
    "    padding-top: 60px;\n" +
    "}\n" +
    "\n" +
    ".dsbbtnregtext{\n" +
    "  color: #fff;\n" +
    "  text-align: center;\n" +
    "}\n" +
    "\n" +
    ".modalimageinner{\n" +
    "  background-position: inherit !important;\n" +
    "  background-repeat: no-repeat !important;\n" +
    "  background-size: inherit !important;\n" +
    "}\n" +
    "\n" +
    ".modalbookingarea{\n" +
    "    width: 878px;\n" +
    "    position: relative;\n" +
    "    background-color: #fff;\n" +
    "}\n" +
    "\n" +
    ".dsbtext {\n" +
    "    font-size: 15px;\n" +
    "}\n" +
    "\n" +
    ".dsbtextc {\n" +
    "    font-size: 25px;\n" +
    "}\n" +
    "\n" +
    ".dsbtextd {\n" +
    "    font-size: 15px;\n" +
    "}\n" +
    "\n" +
    ".dsbbtnreg{\n" +
    "  float: right;\n" +
    "}\n" +
    "\n" +
    ".dsbbtnregtext{\n" +
    "  color: #fff;\n" +
    "}\n" +
    "\n" +
    ".dsbbannerimg {\n" +
    "    background-image: url(images/dsbbanner.jpg);\n" +
    "    background-repeat: no-repeat;\n" +
    "    background-size: cover;\n" +
    "    background-position: center;\n" +
    "    /* background-size: 100% 100%; */\n" +
    "    height: 100%;\n" +
    "    width: 100%;\n" +
    "}\n" +
    "\n" +
    ".closebtn {\n" +
    "    float: right;\n" +
    "    margin-right: 20px;\n" +
    "    margin-top: 15px;\n" +
    "    display: none;\n" +
    "}\n" +
    "\n" +
    "\n" +
    ".modal-dialog-dsb:hover .closebtn{\n" +
    "  display: block;\n" +
    "}\n" +
    "\n" +
    "@media screen and (max-width : 621px) {\n" +
    "  .itempanel {\n" +
    "      width: 100%;\n" +
    "      height: 265px;\n" +
    "      background-color: #ffffff;\n" +
    "      float: left;\n" +
    "      padding-left: 15px;\n" +
    "      padding-right: 15px;\n" +
    "  }\n" +
    "}\n" +
    "\n" +
    "@media  screen and (min-width : 622px) and (max-width : 850px) {\n" +
    "  .itempanel {\n" +
    "      width: 33%;\n" +
    "      height: 265px;\n" +
    "      background-color: #ffffff;\n" +
    "      float: left;\n" +
    "      padding-left: 15px;\n" +
    "      padding-right: 15px;\n" +
    "  }\n" +
    "}\n" +
    "\n" +
    "@media screen and (min-width : 851px) and (max-width : 1004px) {\n" +
    "  .itempanel {\n" +
    "      width: 25%;\n" +
    "      height: 265px;\n" +
    "      background-color: #ffffff;\n" +
    "      float: left;\n" +
    "      padding-left: 15px;\n" +
    "      padding-right: 15px;\n" +
    "  }\n" +
    "}\n" +
    "\n" +
    "@media screen and (max-width : 850px) {\n" +
    "  .modal-dialog-dsb {\n" +
    "      width: 98%;\n" +
    "      margin: 20px auto;\n" +
    "  }\n" +
    "\n" +
    "  .modal-dialog-dsbbookregister {\n" +
    "      width: 98%;\n" +
    "      margin: 10px auto;\n" +
    "  }\n" +
    "\n" +
    "  .modalcontent{\n" +
    "    width: 100%;\n" +
    "  }\n" +
    "\n" +
    "  .modalimage {\n" +
    "    max-width: 95%;\n" +
    "    max-height: 95%;  \n" +
    "  }\n" +
    "\n" +
    "  .modalbuttom {\n" +
    "      margin-bottom: 40px;\n" +
    "  }\n" +
    "\n" +
    "  .dailyspecialbtn {\n" +
    "      float: right;\n" +
    "      width: 100%;\n" +
    "      margin-top: 3px;\n" +
    "      margin-right: 0px;\n" +
    "      z-index: 99999999;\n" +
    "      position: relative;\n" +
    "  }\n" +
    "\n" +
    "  .modalimageinner{\n" +
    "    background-position: center !important;\n" +
    "    background-repeat: no-repeat !important;\n" +
    "    background-size: cover !important; \n" +
    "  }\n" +
    "}\n" +
    "\n" +
    "\n" +
    "\n" +
    "@media screen and (max-width : 650px) {\n" +
    "  .modal-dialog-dsbbookregister {\n" +
    "      width: 98%;\n" +
    "      margin: 10px auto;\n" +
    "  }\n" +
    "\n" +
    "  .modalcontentbooknow {\n" +
    "      width: 100%;\n" +
    "      height: 1000px;\n" +
    "      position: relative;\n" +
    "      background-color: #fff;\n" +
    "      -webkit-background-clip: padding-box;\n" +
    "      background-clip: padding-box;\n" +
    "      border: 1px solid #999;\n" +
    "      border: 1px solid rgba(0,0,0,.2);\n" +
    "      outline: 0;\n" +
    "      -webkit-box-shadow: 0 3px 9px rgba(0,0,0,.5);\n" +
    "      box-shadow: 0 3px 9px rgba(0,0,0,.5);\n" +
    "  }\n" +
    "\n" +
    "  .modalcontentbooknowmod {\n" +
    "      width: 100%;\n" +
    "      height: 1000px;\n" +
    "      position: relative;\n" +
    "      background-color: #fff;\n" +
    "      -webkit-background-clip: padding-box;\n" +
    "      background-clip: padding-box;\n" +
    "      border: 1px solid #999;\n" +
    "      border: 1px solid rgba(0,0,0,.2);\n" +
    "      outline: 0;\n" +
    "      -webkit-box-shadow: 0 3px 9px rgba(0,0,0,.5);\n" +
    "      box-shadow: 0 3px 9px rgba(0,0,0,.5);\n" +
    "  }\n" +
    "\n" +
    "}\n" +
    "\n" +
    "</style>\n" +
    "<section class ='early_book_section' style='margin-top:100px;'>\n" +
    "    <div class=\"dsbbannerholder\">\n" +
    "      <div class=\"dsbbannerimg\">\n" +
    "          <!-- <div class = \"dsbbtnreg\">\n" +
    "            <div class=\"dsbbtnregtext\">\n" +
    "              For Restaurants\n" +
    "            </div>\n" +
    "            <a href ng-click=\"registernewresto()\">\n" +
    "              <button id=\"btn_book\"\n" +
    "                      class=\"book-button-sm btn-leftBottom-orange\"\n" +
    "                      ng-click=\"registernewresto()\">Register\n" +
    "              </button>\n" +
    "            </a>\n" +
    "          </div> -->\n" +
    "          <div class = \"titleboard\">\n" +
    "            <div class=\"dsbtext\">Daily Special Board</div>\n" +
    "            <div class=\"dsbtextc\">SINGAPORE</div>\n" +
    "            <div class=\"dsbtextd\">{{todaydateFormat}}</div> \n" +
    "            \n" +
    "          </div>\n" +
    "        </div>  \n" +
    "    </div>\n" +
    "    <!-- <div class = \"titleboard\"><h1>Daily Special Board for {{todaydateFormat}}</h1><a href ng-click=\"registernewresto()\">Register New Restaurant</a></div>  -->\n" +
    "    <div class='showdata' when-scrolled =\"more()\">\n" +
    "         <div  ng-repeat=\"item in items\" style=' padding-top:0px;'>\n" +
    "            <div class=\"itempanel\">\n" +
    "              <div class=\"itempanelinside\">\n" +
    "                <div class=\"itemimage\">\n" +
    "                  <a ng-href=\"#\" ng-click=\"showdetails(item)\" ng-if=\"item.resttype === 'Partner'\"> \n" +
    "                     <img ng-src=\"images/transparent.png\" Title= \"Book Now\" style='width:100%;height: 100%;background: url(\"https://s3-ap-southeast-2.amazonaws.com/media.weeloy.asia/upload/dsb/{{today}}/{{item.code}}/small/{{item.image}}\");background-position: center;background-repeat: no-repeat;background-size: cover;'>\n" +
    "                  </a>\n" +
    "                  <a ng-href=\"#\" ng-click=\"showdetails(item)\" ng-if=\"item.resttype !== 'Partner'\"> \n" +
    "                     <img ng-src=\"images/transparent.png\" style='width:100%;height: 100%;background: url(\"https://s3-ap-southeast-2.amazonaws.com/media.weeloy.asia/upload/dsb/{{today}}/{{item.code}}/small/{{item.image}}\");background-position: center;background-repeat: no-repeat;background-size: cover;'>\n" +
    "                  </a>\n" +
    "                </div>\n" +
    "                 <div class=\"headline\">\n" +
    "                   {{item.headline}}\n" +
    "                </div>\n" +
    "                   <div class=\"itemcontent\">\n" +
    "                       {{item.description}}\n" +
    "                   </div>\n" +
    "                 \n" +
    "                    <div class=\"restoicon\">\n" +
    "                        <div class=\"restoiconimage\">\n" +
    "                          <a ng-href=\"#\" ng-click=\"showdetails(item)\" target=\"{{ OpenRestaurantInNewTab }}\"> \n" +
    "                             <img ng-src=\"images/transparent.png\" style='width:100%;height: 100%;background: url(\"https://s3-ap-southeast-2.amazonaws.com/media.weeloy.asia/upload/restaurant/{{item.code}}/{{item.restologo}}\");    background-position: center;background-repeat: no-repeat;background-size: cover;'>\n" +
    "                          </a>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div class=\"restodet\">\n" +
    "                      \n" +
    "                        <div class=\"partners\" ng-if=\"item.resttype === 'Partner'\">\n" +
    "                              <a href ng-click=\"booknowmodalmain(item.code)\"><img ng-src=\"images/transparent.png\" Title= \"Book Now\" style='width:100%;height: 100%;background: url(\"images/dsbcard_bookicon.png\");background-position: center;background-repeat: no-repeat;background-size: cover;'></a>\n" +
    "                        </div> \n" +
    "                        <div class=\"restoname\">\n" +
    "                            {{item.restaurant}}\n" +
    "                        </div> \n" +
    "                        \n" +
    "                        <div class=\"restoadd\">\n" +
    "                            {{item.restoaddress}}\n" +
    "                        </div>\n" +
    "                  </div>\n" +
    "            </div>\n" +
    "         </div>\n" +
    "         \n" +
    "    </div>\n" +
    "    <div ng-show=\"loadingimg\">\n" +
    "            <center>\n" +
    "              <img src=\"images/loading2.gif\">\n" +
    "            </center>\n" +
    "          </div>\n" +
    "\n" +
    "</section>\n" +
    "\n" +
    "<!-- Modal Popup -->\n" +
    "    <div class=\"modal fade\" id=\"dbsformmodallist\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">\n" +
    "      <div class=\"modal-dialog-dsb\" role=\"document\">\n" +
    "        <div class=\"modalcontent\">\n" +
    "          <div class=\"closebtn\">\n" +
    "            <a href ng-click=\"closemodal()\" >\n" +
    "              <img ng-src=\"images/dsbx.png\">\n" +
    "            </a>\n" +
    "          </div> \n" +
    "          <div class=\"modalheader\">\n" +
    "              {{detailsshow.headline}}\n" +
    "          </div> \n" +
    "          <div class=\"modalimage\">\n" +
    "            <img class=\"modalimageinner\" ng-src=\"images/transparent.png\" style='width:100%;height: 100%;background: url(\"https://s3-ap-southeast-2.amazonaws.com/media.weeloy.asia/upload/dsb/{{today}}/{{detailsshow.code}}/main/{{detailsshow.image}}\");background-repeat: no-repeat;'>\n" +
    "          </div>\n" +
    "          <div class=\"modaltextcontent\">\n" +
    "              {{detailsshow.description}}\n" +
    "            </div>\n" +
    "            <div class=\"modalbuttom\">\n" +
    "              <div class=\"restoiconimagemodal\">\n" +
    "                <a ng-href=\"#\" target=\"{{ OpenRestaurantInNewTab }}\"> \n" +
    "                   <img ng-src=\"images/transparent.png\" style='width:100%;height: 100%;background: url(\"https://s3-ap-southeast-2.amazonaws.com/media.weeloy.asia/upload/restaurant/{{detailsshow.code}}/{{detailsshow.restologo}}\");background-position: center;background-repeat: no-repeat;background-size: cover;'>\n" +
    "                </a>\n" +
    "              </div>\n" +
    "              <div class=\"restomodal\">\n" +
    "                  <div class=\"restonamemodal\">\n" +
    "                      {{detailsshow.restaurant}}\n" +
    "                  </div>\n" +
    "                  <div class=\"dailyspecialbtn\" ng-if=\"detailsshow.resttype === 'Partner'\" ng-show=\"btnbooking\">\n" +
    "                    <center><input class=\"btn weeloy-text custombtn\" ng-click=\"booknowmodal(detailsshow.code)\" type=\"button\" name=\"\" value=\"Book This Special\"></center>\n" +
    "                  </div>\n" +
    "                  <div class=\"restoaddressmodal\">\n" +
    "                      {{detailsshow.restoaddress}} <!-- <span class='glyphicon glyphicon-map-marker'></span> -->\n" +
    "                  </div>\n" +
    "                  \n" +
    "              </div>\n" +
    "              \n" +
    "            </div>\n" +
    "            <div class=\"modalbookingarea\" ng-show=\"showbooking\">\n" +
    "                <iframe ng-src=\"{{restourl}}\" width=\"100%\" height=\"940px\" frameborder=\"0\" marginwidth=\"0\" marginheight=\"0\" scrolling=\"yes\">\n" +
    "              \n" +
    "                </iframe>\n" +
    "            </div>\n" +
    "\n" +
    "        </div>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"modal fade\" id=\"dbsformmodalbooknow\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">\n" +
    "      <div class=\"modal-dialog-dsbbooknow\" role=\"document\">\n" +
    "        <div class=\"modalcontentbooknowmod\">\n" +
    "            <iframe ng-src=\"{{restourl}}\" width=\"100%\" height=\"1000px\" frameborder=\"0\" marginwidth=\"0\" marginheight=\"0\" scrolling=\"yes\">\n" +
    "              \n" +
    "            </iframe>\n" +
    "        </div>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "\n" +
    "\n" +
    "    <div class=\"modal fade\" id=\"dbsformmodalregistration\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">\n" +
    "      <div class=\"modal-dialog-dsbbookregister\" role=\"document\">\n" +
    "        <div class=\"modalcontentbooknow\">\n" +
    "            \n" +
    "            \n" +
    "            <iframe src=\"../modules/DailySpecialBoard/RegistrationFormShort.php\" width=\"100%\" height=\"100%\" frameborder=\"0\" marginwidth=\"0\" marginheight=\"0\" scrolling=\"no\">\n" +
    "            </iframe>\n" +
    "            <!-- <iframe src=\"http://localhost:8888/weeloy.com/modules/DailySpecialBoard/registrationformshort.php\" width=\"100%\" height=\"100%\" frameborder=\"0\" marginwidth=\"0\" marginheight=\"0\" scrolling=\"no\">\n" +
    "            </iframe> -->\n" +
    "        </div>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "");
}]);

angular.module("../app/components/dsb_page/_dsb_register_form.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/components/dsb_page/_dsb_register_form.tpl.html",
    "<style media=\"screen\">\n" +
    "	.dsbbannerholder{\n" +
    "		max-width: 625px;\n" +
    "    	height: 185px;\n" +
    "		margin-left: auto;\n" +
    "		margin-right: auto;\n" +
    "		margin-bottom: 10px;\n" +
    "	}\n" +
    "	.dsbbannerimg {\n" +
    "	    /*background-image: url(images/dsbbanner.jpg);\n" +
    "	    background-repeat: no-repeat;\n" +
    "	    background-size: cover;\n" +
    "	    background-position: center;*/\n" +
    "	    height: 100%;\n" +
    "	    width: 100%;\n" +
    "	}\n" +
    "	.titleboard{\n" +
    "	    text-align: center;\n" +
    "	    color: #fff;\n" +
    "	    padding-top: 60px;\n" +
    "	}\n" +
    "	.dsbtext {\n" +
    "	    font-size: 15px;\n" +
    "	}\n" +
    "\n" +
    "	.dsbtextc {\n" +
    "	    font-size: 25px;\n" +
    "	}\n" +
    "	.regcont{\n" +
    "	 width: 650px;\n" +
    "	 height: 900px;\n" +
    "	 margin-left: auto;\n" +
    "     margin-right: auto;\n" +
    "	}\n" +
    "\n" +
    "	.dsbtitle {\n" +
    "            font-size: 18px;\n" +
    "            font-weight: bold;\n" +
    "	}\n" +
    "    \n" +
    "</style>\n" +
    "<section class ='early_book_section' style=\"margin-top:100px;\">\n" +
    "    <div class=\"regcont\">\n" +
    "<!--        <iframe src=\"https://dev.weeloy.com/modules/DailySpecialBoard/RegistrationFormShort.php\" width=\"100%\" height=\"100%\" frameborder=\"0\" marginwidth=\"0\" marginheight=\"0\" scrolling=\"no\">\n" +
    "        </iframe> -->\n" +
    "    </div>\n" +
    "\n" +
    "</section>\n" +
    "\n" +
    "");
}]);

angular.module("../app/components/event_booking/_event_booking.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/components/event_booking/_event_booking.tpl.html",
    "<div id=\"checkout-info\" class=\"event-booking\">\n" +
    "    <div class=\"container\">\n" +
    "        <div id=\"\" class='col-xs-12 col-sm-12  hidden-md  hidden-lg' ng-class=\"{'col-xs-12 col-sm-12  hidden-md  hidden-lg':true}\" >\n" +
    "            <div class=\"sidebar-contant\">\n" +
    "                <div class=\"img-logo\">\n" +
    "                    <img ng-src=\"{{restaurant.getLogoImage()}}\" />\n" +
    "                </div>\n" +
    "                <h3 class=\"text-center\" ng-bind=\"restaurant.getTitle()\"></h3>\n" +
    "                <p class=\"text-center\" ng-if=\"false\">\n" +
    "                    <span ng-bind=\"restaurant.getAddress()\"></span>, <span ng-bind=\"restaurant.getZip()\"></span>\n" +
    "                </p>\n" +
    "\n" +
    "                <hr/>\n" +
    "                <cart ng-show=\"items | lengthMoreThan:0\" items=\"items\" ng-attr-cx=\"false\" ng-attr-minorder=\"restaurant.getMinOnlineOrder()\" ng-attr-tax=\"restaurant.getOnlineOrderTax()\"></cart>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "\n" +
    "        <div id=\"main-form\" ng-class=\"{'col-xs-12 col-sm-12 col-md-8 col-lg-8':true}\" class=\"col-xs-12 col-sm-12 col-md-8 col-lg-8\">\n" +
    "<!--            <a rel=\"nofollow\" ng-href=\"{{restaurant.getInternalPath()}}\">\n" +
    "                <p class=\"back-to-cart\" ng-if=\"bktracking !== 'WEBSITE'\">\n" +
    "                    <i class=\"fa fa-chevron-left\"></i>\n" +
    "                    <span>Back to restaurant page</span>\n" +
    "                </p>\n" +
    "            </a>-->\n" +
    "            <div id=\"contant-main-form\">\n" +
    "                <div class=\"col-xs-12\">\n" +
    "                    <div class=\"cnt\">\n" +
    "                        <h2 ng-bind=\"RestaurantEvent.getTitle()\"></h2>\n" +
    "                        <!--<span ng-if=\"RestaurantEvent.getStartTime('MMM DD, YYYY') === RestaurantEvent.getEndTime('MMM DD, YYYY')\">{{RestaurantEvent.getStartTime('MMM DD, YYYY')}} to {{RestaurantEvent.getEndTime('MMM DD, YYYY')}} in {{RestaurantEvent.getCity()}}</span>-->\n" +
    "                        <p ng-bind=\"RestaurantEvent.description\"></p>\n" +
    "                    </div>\n" +
    "                    <div ng-if=\"RestaurantEvent.hasPicture()\" class=\"img-block\">\n" +
    "                        <a href=\"{{ RestaurantEvent.getPicture()}}\" class=\"fresco\" data-fresco-caption=\"{{ RestaurantEvent.getTitle()}} - weeloy.com\" data-fresco-group=\"event\">\n" +
    "                            <img ng-src=\"{{ RestaurantEvent.getPicture()}}\" class=\"img-responsive\" alt=\"{{ RestaurantEvent.getTitle()}} - weeloy.com\">\n" +
    "                        </a>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div id=\"main-form\" class=\"col-xs-12 col-sm12 col-md-12 col-lg-12\" ng-show=\"balPax <= 0\">\n" +
    "                    <h1>This event is full.</h1>\n" +
    "                </div>\n" +
    "                <form id=\"form-border\" name=\"OrderForm\" ng-show='balPax > 0' ng-submit=\"SaveOrderRequest(OrderForm, Order, RestaurantEvent, DeliveryDate)\" novalidate\" >\n" +
    "                    <div class=\"title col-xs-12 col-sm12 col-md-12 col-lg-12\">\n" +
    "                        <div class=\"numberCircle\">1</div>\n" +
    "                        <h4>Personal Details</h4>\n" +
    "                    </div>\n" +
    "                    <p ng-if=\"!loggedin && bktracking !== 'WEBSITE'\">Have an account width us? <a data-toggle=\"modal\" data-target=\"#loginModal\">Log in</a> to checkout faster!</p>\n" +
    "                    <div class=\"form-group col-xs-12 col-sm12 col-md-6 col-lg-6\">\n" +
    "                        <label>First name*</label>\n" +
    "                        <input type=\"text\" class=\"form-control\" name=\"first_name\" ng-model=\"Order.first_name\" required>\n" +
    "                        <div class=\"error\">\n" +
    "                            <span ng-show=\"OrderForm.$submitted && OrderForm.first_name.$error.required\">Please enter your first name</span>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group col-xs-12 col-sm12 col-md-6 col-lg-6\">\n" +
    "                        <label>Last name*</label>\n" +
    "                        <input type=\"text\" class=\"form-control\" name=\"last_name\" ng-model=\"Order.last_name\" required>\n" +
    "                        <div class=\"error\">\n" +
    "                            <span ng-show=\"OrderForm.$submitted && OrderForm.last_name.$error.required\">Please enter your last name</span>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group col-xs-12 col-sm12 col-md-6 col-lg-6\">\n" +
    "                        <label>Email*</label>\n" +
    "                        <input type=\"email\" class=\"form-control\" name=\"email\" ng-model=\"Order.email\" required>\n" +
    "                        <div class=\"error\">\n" +
    "                            <span ng-show=\"OrderForm.$submitted && OrderForm.email.$error.required\">Please enter your email</span>\n" +
    "                            <span ng-show=\"OrderForm.$submitted && OrderForm.email.$error.email\">Please enter a valid email</span>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group col-xs-12 col-sm12 col-md-6 col-lg-6\">\n" +
    "                        <label>Phone*</label>\n" +
    "                        <input type=\"text\" class=\"form-control\" name=\"phone\" ng-model=\"Order.phone\" ng-pattern=\"/[+|0-9|\\s]+$/\" required>\n" +
    "                        <div class=\"error\">\n" +
    "                            <span ng-show=\"OrderForm.$submitted && OrderForm.phone.$error.required\">Please enter your phone number</span>\n" +
    "                            <span ng-show=\"OrderForm.$submitted && OrderForm.phone.$error.pattern\">Please enter a valid phone number</span>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "\n" +
    "                    <div class=\"form-group col-xs-12 col-sm12 col-md-6 col-lg-6\">\n" +
    "                        <label>Number of people*</label>\n" +
    "                         <select class=\"form-control\" ng-model=\"Order.pax\" required style='line-height:5px; overflow-x: hidden; '>\n" +
    "                            <option mg-selected=\"{{pax == Order.pax }}\" ng-repeat=\"pax in range\"  value=\"{{pax}}\"  ng-bind=\"pax\"  >\n" +
    "                            </option>\n" +
    "                        </select>\n" +
    "                        \n" +
    "  \n" +
    "<!--                        <select class=\"form-control\" ng-model=\"Order.pax\" required>\n" +
    "                            <option mg-selected=\"{{pax.pax == Order.pax }}\" ng-repeat=\"pax in tableSize\"  value=\"{{pax.pax}}\" ng-if=\"pax.pax < balPax + 1\" ng-bind=\"pax.pax\" >\n" +
    "                            </option>\n" +
    "                        </select>-->\n" +
    "                        <div class=\"error\">\n" +
    "                            <span ng-show=\"OrderForm.$submitted && Order.pax == '' || OrderForm.$submitted && OrderForm.pax.$error.required\">Please enter number of people</span>\n" +
    "\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group col-xs-12 col-sm12 col-md-6 col-lg-6\">\n" +
    "                        <label>Company name(Optional)</label>\n" +
    "                        <input type=\"text\" class=\"form-control\" ng-model=\"Order.company\">\n" +
    "                    </div>\n" +
    "                    <div class=\"title col-xs-12 col-sm12 col-md-12 col-lg-12\">\n" +
    "                        <div class=\"numberCircle\">2</div>\n" +
    "                        <h4>Event date and time</h4>\n" +
    "                    </div>\n" +
    "                    <div class=\"title col-xs-12\">\n" +
    "                        <div class=\"form-group\">\n" +
    "                            <div class=\"row\">\n" +
    "                                <div class=\"col-md-8\">\n" +
    "                                    <div id=\"datetimepicker\"></div>\n" +
    "                                </div>\n" +
    "                                \n" +
    "                                <div class=\"col-sm-12 col-md-4\">\n" +
    "                                    <ul class=\"delivery-time col-sm-12 col-xs-12\">\n" +
    "                                        <li ng-click=\"SelectTime(time)\" ng-repeat=\"time in DeliveryTimeRange\" >\n" +
    "                                            <p ng-class=\"time.selected ? 'selected' : ''\"><span class=\"fa fa-clock-o\"></span><span ng-bind='time.time' style=\"font-size: 13px;\"></span></p>\n" +
    "                                        </li>\n" +
    "                                    </ul>\n" +
    "\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "<!--                    <div class=\"title col-xs-12\">\n" +
    "                        <div class=\"form-group\">\n" +
    "                            <div class=\"row\">\n" +
    "                                <div class=\"col-md-8\">\n" +
    "                                    <div id=\"timepicker\"></div>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>-->\n" +
    "\n" +
    "                    <div class=\"title col-xs-12 col-sm12 col-md-12 col-lg-12\">\n" +
    "                        <div class=\"numberCircle\">3</div>\n" +
    "                        <h4>Special requests</h4>\n" +
    "                        <textarea class=\"form-control remarks\" ng-model=\"Order.special_request\"></textarea>\n" +
    "                    </div>\n" +
    "                    \n" +
    "                    <div class=\"title col-xs-12 col-sm12 col-md-12 col-lg-12\" ng-show=\"RestaurantEvent.getEventOrderTnc() != ''\">\n" +
    "                        <div class=\"numberCircle\">4</div>\n" +
    "                        <h4>Terms and conditions</h4>\n" +
    "                        <div class=\" col-xs-12 col-sm12 col-md-12 col-lg-12\">\n" +
    "                            <p><span ng-bind=\"RestaurantEvent.getEventOrderTnc()\"></span></p>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div class=\"title col-xs-12 col-sm12 col-md-12 col-lg-12\">\n" +
    "                        <div class=\"numberCircle\" ng-show=\"RestaurantEvent.getEventOrderTnc() != ''\">5</div>\n" +
    "\n" +
    "                        <div class=\"numberCircle\" ng-show=\"RestaurantEvent.getEventOrderTnc() == ''\">4</div>\n" +
    "                        <h4>Payment method</h4>\n" +
    "                        <div class=\"form-group checkbox col-xs-12 col-sm12 col-md-12 col-lg-12\">\n" +
    "                            <label>\n" +
    "                                <input type=\"radio\" name=\"optionsPaymentRadios\" id=\"optionsPaymentRadios1\" value=\"creditcard\" checked> Credit card\n" +
    "                            </label>\n" +
    "                            <label  ng-if ='restaurant.paypalflg' >\n" +
    "                                 <input  type=\"radio\" name=\"optionsPaymentRadios\" id=\"optionsPaymentRadios2\"  value=\"paypal\"> Paypal\n" +
    "                             </label>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div class=\"title col-xs-12 col-sm12 col-md-12 col-lg-12\">\n" +
    "                        <div class=\"form-group checkbox col-xs-12 col-sm12 col-md-12 col-lg-12\">\n" +
    "                            <label>\n" +
    "                                <input type=\"checkbox\" name=\"optionsRadios_tnc\" id=\"optionsRadios1\" value=\"option1\" ng-model=\"Order.optionsRadios_tnc\" required  > I accept the terms & conditions\n" +
    "                            </label>\n" +
    "                        </div>\n" +
    "                        <div class=\"form-group checkbox col-xs-12 col-sm12 col-md-12 col-lg-12\">\n" +
    "                            <label>\n" +
    "                                <input type=\"checkbox\" name=\"optionsRadios\" id=\"optionsRadios2\" value=\"option2\" checked> I want to receive email & newsletter\n" +
    "                            </label>\n" +
    "                            <div class=\"error\">\n" +
    "                                <span ng-show=\"OrderForm.$submitted && OrderForm.optionsRadios_tnc.$error.required\" ng-bind=\"Str.template.TextPleaseAcceptTnc\"></span>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "\n" +
    "                    <div class=\"title col-xs-12 col-sm12 col-md-12 col-lg-12\">\n" +
    "                        <button type=\"submit\" class=\"btn btn-default\">Submit Order</button>\n" +
    "                    </div>\n" +
    "                </form>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "\n" +
    "        <div id=\"sidebar-right\" class='hidden-xs hidden-sm col-md-4 col-lg-4 sidebar' ng-class=\"{'hidden-xs hidden-sm col-md-4 col-lg-4 sidebar':true}\" >\n" +
    "            <div class=\"sidebar-contant\">\n" +
    "                <div class=\"img-logo\">\n" +
    "                    <img ng-src=\"{{restaurant.getLogoImage()}}\" />\n" +
    "                </div>\n" +
    "                <h3 class=\"text-center\" ng-bind=\"restaurant.getTitle()\"></h3>\n" +
    "                <p class=\"text-center\" ng-if=\"false\">\n" +
    "                    <span ng-bind=\"restaurant.getAddress()\"></span>, <span ng-bind=\"restaurant.getZip()\"></span>\n" +
    "                </p>\n" +
    "                <span>Event Address</span>\n" +
    "                <p><span ng-bind=\"restaurant.getAddress()\"></span>, <span ng-bind=\"restaurant.getZip()\"></span></p>\n" +
    "                <span>Event Date</span>\n" +
    "                <p ng-bind=\"DeliveryDate | timeFormat:'DD - MM - YYYY'\"></p>\n" +
    "\n" +
    "                <span ng-show='Order.time'>Time</span>\n" +
    "                <p ng-bind=\"Order.time\"></p>\n" +
    "                <div ng-show=\"balPax > 1\">\n" +
    "                    <p><span>Number of people</span>: <span ng-bind=\"Order.pax\"></span></p>\n" +
    "                    <p><span>Price</span>: SGD <span ng-bind=\"RestaurantEvent.getPrice()\"></span></p>\n" +
    "                    <p ng-if=\"isperbookingflg === 15 \"><span>Total</span>: SGD <span ng-bind=\"Order.pax * RestaurantEvent.getPrice()\"></span></p>\n" +
    "                </div>\n" +
    "                <hr/>\n" +
    "                <cart ng-show=\"items | lengthMoreThan:0\" items=\"items\" ng-attr-cx=\"false\" ng-attr-minorder=\"restaurant.getMinOnlineOrder()\" ng-attr-tax=\"restaurant.getOnlineOrderTax()\"></cart>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "\n" +
    "");
}]);

angular.module("../app/components/event_booking/_event_booking_confimation.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/components/event_booking/_event_booking_confimation.tpl.html",
    "<div id=\"payment-success\">\n" +
    "    <div class=\"container\" ng-if=\"Order.orderID\">\n" +
    "        <p  ng-if=\"bktracking!=='WEBSITE'\"><a href=\"/\">Go to homepage</a></p>\n" +
    "        <p  ng-if=\"bktracking!=='WEBSITE'\"><a ng-href=\"{{restaurant_url}}\">Go to {{restaurant.getTitle()}}</a></p>\n" +
    "        <div class=\"col-xs-12 col-sm-6 restaurant\">\n" +
    "            <div class=\"bar-logo pull-left\">\n" +
    "                <img ng-src=\"{{restaurant.getLogoImage()}}\" class=\"img-responsive\" alt=\"\">\n" +
    "            </div>\n" +
    "            <div class=\"restaurant-info\">\n" +
    "                <h1 ng-bind=\"restaurant.title\"></h1>\n" +
    "                <p><span ng-bind=\"restaurant.address\"></span><span ng-bind=\"restaurant.zip\"></span></p>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"col-xs-12\">\n" +
    "                    <div class=\"cnt\">\n" +
    "                        <h2 ng-bind=\"RestaurantEvent.getTitle()\"></h2>\n" +
    "                        <!--<span ng-if=\"RestaurantEvent.getStartTime('MMM DD, YYYY') === RestaurantEvent.getEndTime('MMM DD, YYYY')\">{{RestaurantEvent.getStartTime('MMM DD, YYYY')}} to {{RestaurantEvent.getEndTime('MMM DD, YYYY')}} in {{RestaurantEvent.getCity()}}</span>-->\n" +
    "                        <p ng-bind=\"RestaurantEvent.description\"></p>\n" +
    "                    </div>\n" +
    "                    <div ng-if=\"RestaurantEvent.hasPicture()\" class=\"img-block\">\n" +
    "                        <a href=\"{{ RestaurantEvent.getPicture()}}\" class=\"fresco\" data-fresco-caption=\"{{ RestaurantEvent.getTitle()}} - weeloy.com\" data-fresco-group=\"event\">\n" +
    "                            <img ng-src=\"{{ RestaurantEvent.getPicture()}}\" class=\"img-responsive\" alt=\"{{ RestaurantEvent.getTitle()}} - weeloy.com\">\n" +
    "                        </a>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "        \n" +
    "        <div class=\"col-xs-12\" style=\"padding-top: 20px\">\n" +
    "            <div class=\"col-xs-12\">\n" +
    "                <div class=\"form-group\">\n" +
    "                    <strong>Order number:</strong> <span ng-bind=\"Order.orderID\"></span>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <strong>Email:</strong> <span ng-bind=\"Order.email\"></span>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <strong>Name:</strong> <span ng-bind=\"Order.first_name\"></span> <span ng-bind=\"Order.last_name\"></span>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <strong>Phone:</strong> <span ng-bind=\"Order.phone\"></span>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\" ng-show=\"Order.company\">\n" +
    "                    <strong>Company:</strong> <span ng-bind=\"Order.company\"></span>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <strong>Number of people:</strong> <span ng-bind=\"Order.pax\"></span>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <strong>Total:</strong> <span ng-bind=\"Order.curency\"></span> <span ng-bind=\"Order.amount\"></span>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <strong>Event  Date:</strong> <span ng-bind=\"Order.event_date \"></span>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <strong>Payment Status :</strong> <span ng-bind=\"Order.status\"></span>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    \n" +
    "    \n" +
    "    \n" +
    "    \n" +
    "    <div class=\"container\" ng-if=\"!Order.orderID\">\n" +
    "\n" +
    "        <div class=\"col-xs-12 col-sm-6 restaurant\">\n" +
    "            <div class=\"restaurant-info\">\n" +
    "                <h1>An error occurred during the payment process. \n" +
    "                    Please try again or contact the restaurant.</h1>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("../app/components/event_management/_event_management_request.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/components/event_management/_event_management_request.tpl.html",
    "<section id='event-management' style='margin-top:-80px;padding-top:60px; '>\n" +
    "    <div id=\"checkout-info\" class=\"event-booking\">\n" +
    "        <div class=\"container\" >\n" +
    "            <div ng-class=\"{'col-md-2 col-lg-2' :status === 'confirmed' }\"></div>\n" +
    "            <div id=\"main-form\" class=\"col-sm-12 col-md-8 col-lg-8\" ng-class=\"{'col-sm-12 col-md-8 col-lg-8' :status !== 'confirmed','col-sm-12 col-md-8 col-lg-8' :status === 'confirmed'}\" style=\"padding-bottom: 80px;\">\n" +
    "\n" +
    "                <div id=\"event_header\" class=\"text-center\" ng-if=\"status === 'confirmed'\">\n" +
    "                    <div class=\"img-logo\">\n" +
    "                        <img ng-src=\"{{restaurant.getLogoImage()}}\" />\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div ng-if=\"!event_id && !status\">\n" +
    "                    <p class=\"checkout-header text-center\">Event Request</p>\n" +
    "                    <div class=\"event-request-form\">\n" +
    "                        <h1>The reservation details</h1>\n" +
    "                        <form id=\"form-border\" name=\"EventForm\" ng-submit=\"SaveEventRequest(EventForm, EventObject)\" novalidate>\n" +
    "                            <div class='row'>\n" +
    "                                <div class=\"form-group col-xs-12 col-sm12 col-md-6 col-lg-6\">\n" +
    "                                    <label>Date*</label>\n" +
    "\n" +
    "                                    <input type=\"text\" class=\"form-control\" id=\"wtimepicker6\" name=\"rdate\"  ng-model=\"EventObject.rdate\" jqdatepicker  required>        \n" +
    "\n" +
    "                                    <div class=\"error\">\n" +
    "                                        <span ng-show=\"EventForm.$submitted && EventForm.rdate.$error.required\">Please enter the booking date</span>\n" +
    "                                    </div>\n" +
    "                                </div>\n" +
    "                                <div class=\"form-group col-xs-12 col-sm12 col-md-6 col-lg-6\">\n" +
    "                                    <label>Time*</label>\n" +
    "                                    <input type=\"text\" class=\"form-control\" id=\"wtimepicker7\" name=\"rtime\" ng-model=\"EventObject.rtime\"  jtimedatepicker required>\n" +
    "                                    <div class=\"error\">\n" +
    "                                        <span ng-show=\"EventForm.$submitted && EventForm.rtime.$error.required\">Please enter the booking time</span>\n" +
    "                                    </div>\n" +
    "                                </div>\n" +
    "\n" +
    "                                <div class=\"form-group col-xs-12 col-sm12 col-md-6 col-lg-6\">\n" +
    "                                    <label>Number of people*</label>\n" +
    "                                    <input type=\"text\" class=\"form-control\" ng-model=\"EventObject.pax\" name=\"pax\" required >\n" +
    "                                    <div class=\"error\">\n" +
    "                                        <span ng-show=\"EventForm.$submitted && EventForm.pax.$error.required\">Please enter number of people</span>\n" +
    "                                    </div>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                            <div class='row'>\n" +
    "                                <div class=\"form-group col-xs-12 col-sm12 col-md-6 col-lg-6\">\n" +
    "                                    <label>First name*</label>\n" +
    "                                    <input type=\"text\" class=\"form-control\" name=\"firstname\" ng-model=\"EventObject.firstname\" required>\n" +
    "                                    <div class=\"error\">\n" +
    "                                        <span ng-show=\"EventForm.$submitted && EventForm.firstname.$error.required\">Please enter your first name</span>\n" +
    "                                    </div>\n" +
    "                                </div>\n" +
    "                                <div class=\"form-group col-xs-12 col-sm12 col-md-6 col-lg-6\">\n" +
    "                                    <label>Last name*</label>\n" +
    "                                    <input type=\"text\" class=\"form-control\" name=\"lastname\" ng-model=\"EventObject.lastname\" required>\n" +
    "                                    <div class=\"error\">\n" +
    "                                        <span ng-show=\"EventForm.$submitted && EventForm.lastname.$error.required\">Please enter your last name</span>\n" +
    "                                    </div>\n" +
    "                                </div>\n" +
    "\n" +
    "                                <div class=\"form-group col-xs-12 col-sm12 col-md-6 col-lg-6\">\n" +
    "                                    <label>Company name(Optional)</label>\n" +
    "                                    <input type=\"text\" class=\"form-control\" ng-model=\"EventObject.company\">\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                            <div class='row'>\n" +
    "                                <div class=\"form-group col-xs-12 col-sm12 col-md-6 col-lg-6\">\n" +
    "                                    <label>Email*</label>\n" +
    "                                    <input type=\"email\" class=\"form-control\" name=\"email\" ng-model=\"EventObject.email\" required>\n" +
    "                                    <div class=\"error\">\n" +
    "                                        <span ng-show=\"EventForm.$submitted && EventForm.email.$error.required\">Please enter your email</span>\n" +
    "                                        <span ng-show=\"EventForm.$submitted && EventForm.email.$error.email\">Please enter a valid email</span>\n" +
    "                                    </div>\n" +
    "                                </div>\n" +
    "                                <div class=\"form-group col-xs-12 col-sm12 col-md-6 col-lg-6\">\n" +
    "                                    <label>Phone*</label>\n" +
    "                                    <input type=\"text\" class=\"form-control\" name=\"phone\" ng-model=\"EventObject.phone\" ng-pattern=\"/[+|0-9|\\s]+$/\" required>\n" +
    "                                    <div class=\"error\">\n" +
    "                                        <span ng-show=\"EventForm.$submitted && EventForm.phone.$error.required\">Please enter your phone number</span>\n" +
    "                                        <span ng-show=\"EventForm.$submitted && EventForm.phone.$error.pattern\">Please enter a valid phone number</span>\n" +
    "                                    </div>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                            <div class=\"row\">\n" +
    "                                <div class=\"form-group col-xs-12 col-sm12 col-md-6 col-lg-6\">\n" +
    "                                    <label>Occasion *</label>\n" +
    "                                    <!--<textarea class=\"form-control remarks\" ng-model=\"EventObject.occasion\" name=\"occasion\" required></textarea>-->\n" +
    "                                    <select ng-model=\"EventObject.occasion\" class=\"form-control\" required style='border-radius: 0px;'>\n" +
    "                                        <option ng-value=\"corporate\" >Corporate dinner</option>\n" +
    "                                        <option ng-value=\"meeting\">Meeting and Seminars</option>\n" +
    "                                        <option ng-value=\"social\">Social ie Anniversary / Birthday</option>\n" +
    "                                        <option ng-value=\"solemnisation\">Solemnisation Ceremony</option>\n" +
    "                                        <option ng-value=\"wedding\">Weddings</option>\n" +
    "                                        <option ng-value=\"other\">Others (please specify in special requests)</option>\n" +
    "                                    </select>\n" +
    "\n" +
    "                                    <div class=\"error\">\n" +
    "                                        <span ng-show=\"EventForm.$submitted && EventObject.occasion == '' || EventForm.$submitted && EventForm.occasion.$error.required\">Please enter the occasion of the event</span>\n" +
    "                                    </div>\n" +
    "                                </div>\n" +
    "                                <div class=\"form-group col-xs-12 col-sm12 col-md-6 col-lg-6\">\n" +
    "                                    <label>Event Name*</label>\n" +
    "                                    <input type=\"text\" class=\"form-control\" ng-model=\"EventObject.name\" name=\"name\" required >\n" +
    "                                    <div class=\"error\">\n" +
    "                                        <span ng-show=\"EventForm.$submitted && EventForm.name.$error.required\">Please enter event name</span>\n" +
    "                                    </div>\n" +
    "                                </div>\n" +
    "                                 \n" +
    "                            </div>\n" +
    "                            <div class=\"row\">\n" +
    "                                <div class=\"title col-xs-12 col-sm12 col-md-12 col-lg-12\">\n" +
    "                                    <h4>Special requests</h4>\n" +
    "                                    <textarea class=\"form-control remarks\" ng-model=\"EventObject.special_requests\"></textarea>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                            <div class=\"row\">\n" +
    "                                <div class=\"title col-xs-12 col-sm12 col-md-12 col-lg-12\">\n" +
    "                                    <button type=\"submit\" class=\"btn-creditcard btn-leftBottom-orange pull-right\">Submit request</button>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                        </form>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "\n" +
    "                <div style=\"padding-top:30px; padding-bottom: 30px;\" class='' ng-if=\"status == 'request_sent' || status == 'menu_built' || status == 'menu_pre_selected' || status == 'menu_selected' || status == 'confirmed' || status == 'pending_payment'\">\n" +
    "                    <p class=\"checkout-header text-center\">Event Reservation Request</p>\n" +
    "                    <div class=\"event-request-form\">\n" +
    "                        <div class='row'>\n" +
    "                            <div class=\"form-group col-xs-12 col-sm12 col-md-6 col-lg-6\">\n" +
    "                                <label>Event date: </label>\n" +
    "                                <label ng-bind=\"EventObject.rdate\"></label>\n" +
    "                            </div>\n" +
    "                            <div class=\"form-group col-xs-12 col-sm12 col-md-6 col-lg-6\">\n" +
    "                                <label>Time: </label>\n" +
    "                                <label ng-bind=\"EventObject.rtime\"></label>\n" +
    "                            </div>\n" +
    "\n" +
    "                            <div class=\"form-group col-xs-12 col-sm12 col-md-6 col-lg-6\">\n" +
    "                                <label>Number of people: </label>\n" +
    "                                <label ng-bind=\"EventObject.pax\"></label>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                        <br/>\n" +
    "                        <div class='row'>\n" +
    "                            <div class=\"form-group col-xs-12 col-sm12 col-md-6 col-lg-6\">\n" +
    "                                <label>First name: </label>\n" +
    "                                <label ng-bind=\"EventObject.firstname\"></label>\n" +
    "                            </div>\n" +
    "                            <div class=\"form-group col-xs-12 col-sm12 col-md-6 col-lg-6\">\n" +
    "                                <label>Last name: </label>\n" +
    "                                <label ng-bind=\"EventObject.lastname\"></label>\n" +
    "                            </div>\n" +
    "\n" +
    "                            <div class=\"form-group col-xs-12 col-sm12 col-md-6 col-lg-6\">\n" +
    "                                <label>Company name(Optional)</label>\n" +
    "                                <label ng-bind=\"EventObject.company\"></label>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                        <br/>\n" +
    "                        <div class='row'>\n" +
    "                            <div class=\"form-group col-xs-12 col-sm12 col-md-6 col-lg-6\">\n" +
    "                                <label>Email: </label>\n" +
    "                                <label ng-bind=\"EventObject.email\"></label>\n" +
    "                            </div>\n" +
    "                            <div class=\"form-group col-xs-12 col-sm12 col-md-6 col-lg-6\">\n" +
    "                                <label>Phone: </label>\n" +
    "                                <label ng-bind=\"EventObject.phone\"></label>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                        <div class=\"row\">\n" +
    "                            <div class=\"form-group col-xs-12 col-sm12 col-md-12 col-lg-12\">\n" +
    "                                <label>Occasion: </label>\n" +
    "                                <label ng-bind=\"EventObject.occasion\"></label>\n" +
    "                            </div>\n" +
    "                            <div class=\"form-group col-xs-12 col-sm12 col-md-6 col-lg-6\">\n" +
    "                                <label>Event name: </label>\n" +
    "                                <label ng-bind=\"EventObject.name\"></label>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                        \n" +
    "                        <div class=\"row\" ng-if=\"EventObject.special_requests\">\n" +
    "                            <div class=\"title col-xs-12 col-sm12 col-md-12 col-lg-12\">\n" +
    "                                <h4>Special requests</h4>\n" +
    "                                <label ng-bind=\"EventObject.special_requests\"></label>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                     <div class=\"row\" ng-if=\"status == 'request_sent'\">\n" +
    "                            <div class=\"title col-xs-12 col-sm12 col-md-12 col-lg-12\">\n" +
    "                                <h4>Your request has been submitted to the restaurant. The restaurant will get back to you for further information.</h4>\n" +
    "                            </div>\n" +
    "                     </div>\n" +
    "                   \n" +
    "                </div>\n" +
    "                \n" +
    "                <div ng-if=\"status == 'menu_built' && EventObject.eventID\">\n" +
    "                    <p class=\"checkout-header text-center\">Menu selection</p>\n" +
    "                    <div ng-if=\"status != 'menu_selected'\"  class=\"\" ng-class=\"\" ng-repeat=\"menu in menus\" style=\"margin-bottom: 30px\" >\n" +
    "                        <!--display food-->\n" +
    "                        <div ng-if=\"menu.food && !menu.drink && !menu.option\" class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\" style=\"padding:20px; background-color: #eee\">\n" +
    "                            <!--<input type=\"radio\" name=\"setmeal\" value=\"{{menu.notes}}\" style=\"float:left; margin-right: 20px;\">-->\n" +
    "                            <h4 style=\"float: left;\" ng-bind=\"menu.notes\"></h4>\n" +
    "\n" +
    "                            <h5 style=\"float: left; margin-left:30px; \">(SGD {{menu.price}})</h5>\n" +
    "                            <h5 style=\"float: left; margin-left:30px; \" ng-if='false'>Total {{menu.qte}}</h5>\n" +
    "                            <!--<input ng-model=\"menu.qte\" class=\"col-lg-3\"  type=\"text\">-->\n" +
    "                            <div class=\"clear-both\"></div>\n" +
    "                            <div class=\"row\" ng-class=\"\" ng-repeat=\"item in menu.items\" >\n" +
    "                                <div  ng-if=\"menu.menuID === item.displayID\" ng-class=\"{'col-xs-10 col-sm-10 col-md-10 col-lg-10' :status !== 'confirmed','col-sm-12 col-md-12 col-lg-12' :status === 'confirmed'}\" style='display: table'>\n" +
    "                                    <div class=\"category-item-info\" style='display: table-cell' >\n" +
    "                                        <div class=\"location-icon\">\n" +
    "                                            <div class=\"location-text\" style=\"margin-top: 5px;\">\n" +
    "                                                <p class=\"col-lg-9\" style='font-weight: bold;margin-top:15px;' ng-if=\"item.item_description == 'section' || item.item_description == 'section_or'\"><span>{{item.item_title}}</span></p>\n" +
    "                                                <p class=\"col-lg-9\" style='font-size: 12px' ng-if=\"item.item_description != 'section' && item.item_description != 'section_or'\"><span>{{item.item_title}}</span><img style=\"width: 20px;margin-left: 20px;\" ng-if=\"item.vegetarian\" ng-src=\"images/restaurant_icons/vegi.png\"></p>\n" +
    "                                                <p class=\"col-lg-9\" style='font-size: 12px' ng-if=\"item.item_description != 'section' && item.item_description != 'section_or'\"><span>{{item.item_description}}</span></p>\n" +
    "                                                <input ng-change=\"setQte(item, $event)\" ng-model=\"item.qte\" class=\"col-lg-3\"  type=\"text\" ng-if=\"(item.section === 'section_or' || item.section === 'section_or_nd') && item.item_description != 'section' && (item.item_description != 'section_or' || item.item_description != 'section_or_nodisplay')\">\n" +
    "                                            </div>\n" +
    "                                            <div>\n" +
    "                                           </div>\n" +
    "                                            <span style=\"padding-left:80px\" class=\"col-lg-12\" ng-if=\"item.section === 'section_or' && item.item_description != 'section' && item.item_description != 'section_or' \">---- OR ----</span>\n" +
    "                                            \n" +
    "                                        </div>\n" +
    "                                    </div>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                            <hr>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "\n" +
    "                    <div ng-if=\"(menu.drink || menu.option) && status != 'menu_selected'\"  class=\"\" ng-class=\"\" ng-repeat=\"menu in menus\" style=\"margin-bottom: 30px\" >\n" +
    "\n" +
    "                        <!--display drinks-->\n" +
    "                        <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">\n" +
    "                            <!--                            <h1 ng-bind=\"menu.notes\"></h1>-->\n" +
    "                            <h4 style=\"float: left;\" ng-bind=\"menu.notes\"></h4>\n" +
    "                            <!--<h5 style=\"float: left; margin-left:30px; \">(SGD {{menu.price}})</h5>-->\n" +
    "                            <h5 style=\"float: left; margin-left:30px; \" ng-if=\"false && menu.qte > 0\">Total {{menu.qte}}</h5>\n" +
    "                            <div style=\"clear: both\"></div>\n" +
    "                            <div class=\"row\" ng-class=\"\" ng-repeat=\"item in menu.items\" style='margin-bottom: 30px;'>\n" +
    "\n" +
    "                                <div  ng-if=\"menu.menuID === item.displayID\" ng-class=\"{'col-xs-10 col-sm-10 col-md-10 col-lg-10' :status !== 'confirmed','col-sm-12 col-md-12 col-lg-12' :status === 'confirmed'}\" style='display: table'>\n" +
    "                                    <div class=\"category-item-info\" style='display: table-cell' >\n" +
    "                                        <div class=\"location-icon\">\n" +
    "                                            <div class=\"location-text\" style=\"margin-top: 5px;\">\n" +
    "                                                <!--<input class=\"col-lg-1\" type=\"checkbox\" >-->\n" +
    "                                                <!--<div class=\"col-lg-8\" >-->\n" +
    "                                                <p class=\"col-lg-9\" style='font-weight: bold;margin-top:15px;'><span>{{item.item_title}}</span></p>\n" +
    "                                                <p class=\"col-lg-9\" style='font-size: 12px' ><span>{{item.item_description}}</span></p>\n" +
    "                                                <p ng-if=\"!menu.setmeal && item.price > 0\" class=\"col-lg-9\" style='font-size: 12px' ><span>SGD {{item.price}}</span></p>\n" +
    "                                                <!--</div>-->\n" +
    "                                                <p ng-if=\"menu.drink\" >\n" +
    "                                                    <input  ng-change=\"setQte(item, $event)\"  class=\"col-lg-3\" type=\"text\"  ng-model=\"item.qte\" ng-if=\"item.price && item.price > 0\">\n" +
    "                                            \n" +
    "                                                    <p  class=\"col-lg-3\"  ng-if=\"!item.price || item.price < 1\">INCLUDED</p>\n" +
    "                                                </p>\n" +
    "                                                <span ng-if='menu.option' >\n" +
    "                                                    <input  ng-change=\"setQte(item, $event)\"  class=\"col-lg-3\" type=\"text\"  ng-model=\"item.qte\" ng-if=\"item.price && item.price >= 0\">\n" +
    "                                                </span>\n" +
    "                                            </div>\n" +
    "                                            <div>\n" +
    "\n" +
    "                                            </div>\n" +
    "                                        </div>\n" +
    "                                    </div>\n" +
    "                                </div>\n" +
    "\n" +
    "\n" +
    "\n" +
    "\n" +
    "                                <!--                                <div class=\"col-xs12 col-sm-12 col-md-10 col-lg-10\" style='display: table'>\n" +
    "                                                                    <div class=\"category-item-info\" style='display: table-cell'>\n" +
    "                                                                        <div class=\"location-icon\">\n" +
    "                                                                            <div class=\"location-text\" style=\"margin-top: 5px;\">\n" +
    "                                                                                <p style='font-weight: bold'><span>{{item.item_title}}</span></p>\n" +
    "                                                                                <p style='font-size: 12px'><span>{{item.item_description}}</span></p>\n" +
    "                                                                            </div>\n" +
    "                                                                        </div>\n" +
    "                                                                    </div>\n" +
    "                                                                    <div class=\"\" style='min-width: 160px;text-align: right;vertical-align: middle;display: table-cell'>\n" +
    "                                                                        <span  style='cursor:pointer; font-weight: 400;font-size: 90%;' class=\"cart-label label label-info\" >\n" +
    "                                                                           <input class=\"col-lg-3\" type=\"text\" >\n" +
    "                                                                        </span>\n" +
    "                                                                        \n" +
    "                                                                    </div>\n" +
    "                                                                </div>-->\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "\n" +
    "                    </div>\n" +
    "\n" +
    "\n" +
    "                    <!--\n" +
    "                    <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">\n" +
    "                                                <h1 ng-bind=\"menu.notes\"></h1>\n" +
    "                                                <p>Please select {{menu.item_limit}} of {{menu.notes}} categorie.</p>\n" +
    "                                                <div class=\"row\" ng-class=\"\" ng-repeat=\"MenuItem in menu.items\" style='margin-bottom: 30px;'>\n" +
    "                                                    <div class=\"col-lg-4 col-md-4 \">\n" +
    "                                                        <img ng-if=\"MenuItem.mimage\" class=\"\" style=\"width:100%; height: auto;\"  ng-src=\"//media1.weeloy.com/upload/restaurant/{{restaurant.getRestaurantId()}}/300/{{MenuItem.mimage}}\">\n" +
    "                                                    </div>\n" +
    "                                                    <div class=\"col-xs12 col-sm-12 col-md-10 col-lg-10\" style='display: table'>\n" +
    "                                                        <div class=\"category-item-info\" style='display: table-cell'>\n" +
    "                                                            <div class=\"location-icon\">\n" +
    "                                                                <div class=\"location-text\" style=\"margin-top: 5px;\">\n" +
    "                                                                    <p style='font-weight: bold'><span>{{MenuItem.item_title}}</span></p>\n" +
    "                                                                    <p style='font-size: 12px'><span>{{MenuItem.item_description}}</span></p>\n" +
    "                                                                </div>\n" +
    "                                                            </div>\n" +
    "                                                        </div>\n" +
    "                                                        <div class=\"\" style='min-width: 160px;text-align: right;vertical-align: middle;display: table-cell'>\n" +
    "                                                            <a href=\"\" class=\"book-button-sm btn-leftBottom-orange\" ng-click=\"selectItem(MenuItem, $event)\" >Select</a>\n" +
    "                                                            <span  style='cursor:pointer; font-weight: 400;font-size: 90%;' class=\"cart-label label label-info\" ng-click=\"selectItem(MenuItem, $event)\" ng-if=\"!selectioncomplete && items.indexOf(MenuItem) < 0 && (counters[menu.menuID] < menu.item_limit || !counters[menu.menuID])\">\n" +
    "                                                                <i class=\"fa fa-plus\"></i> \n" +
    "                                                                <span>Select this item</span>\n" +
    "                                                            </span>\n" +
    "                                                            <span  style='font-weight: 400;font-size: 90%;'  class=\"cart-label label label-success\" ng-if=\"items.indexOf(MenuItem) > -1\">\n" +
    "                                                                <span>Selected</span>\n" +
    "                                                            </span>\n" +
    "                                                        </div>\n" +
    "                                                    </div>\n" +
    "                                                </div>\n" +
    "                                            </div>Ï\n" +
    "\n" +
    "\n" +
    "                    <div  style='' >\n" +
    "                        <p class=\"checkout-header text-center\">Dietary Restriction</p>\n" +
    "                        <div class=\"\" ng-class=\"\" ng-repeat=\"dietary in dietaries\">\n" +
    "                            <div ng-class=\"{'col-xs-10 col-sm-10 col-md-10 col-lg-10' :status !== 'confirmed','col-sm-12 col-md-12 col-lg-12' :status === 'confirmed'}\" style='display: table'>\n" +
    "                                <div class=\"category-item-info\" style='display: table-cell' >\n" +
    "                                    <div class=\"location-icon\">\n" +
    "                                        <div class=\"location-text\" style=\"margin-top: 5px;\">\n" +
    "                                            <p class=\"col-lg-9\" style='font-weight: bold;margin-top:15px;'><span>{{dietary.title}}</span></p>\n" +
    "\n" +
    "                                            <input class=\"col-lg-3\" type=\"text\" ng-if=\"item.item_description != 'section' && item.item_description != 'section_or'\">\n" +
    "                                        </div>\n" +
    "                                        <div>\n" +
    "\n" +
    "                                        </div>\n" +
    "                                        <span style=\"padding-left:80px\" class=\"col-lg-12\" ng-if=\"item.section === 'section_or' && item.item_description != 'section' && item.item_description != 'section_or'\">---- OR ----</span>\n" +
    "                                    </div>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>-->\n" +
    "\n" +
    "\n" +
    "\n" +
    "\n" +
    "<!--                    <div  style='' >\n" +
    "                        <p class=\"checkout-header text-center\">Dietary Restriction</p>\n" +
    "                        <div class=\"\" ng-class=\"\" ng-repeat=\"dietary in dietaries\">\n" +
    "                            <div ng-class=\"{'col-xs-10 col-sm-10 col-md-10 col-lg-10' :status !== 'confirmed','col-sm-12 col-md-12 col-lg-12' :status === 'confirmed'}\" style='display: table'>\n" +
    "                                <div class=\"category-item-info\" style='display: table-cell' >\n" +
    "                                    <div class=\"location-icon\">\n" +
    "                                        <div class=\"location-text\" style=\"margin-top: 5px;\">\n" +
    "                                            <p class=\"col-lg-9\" style='font-weight: bold;margin-top:15px;'><span>{{dietary.title}}</span></p>\n" +
    "\n" +
    "                                            <input class=\"col-lg-3\" type=\"text\" ng-if=\"item.item_description != 'section' && item.item_description != 'section_or'\">\n" +
    "                                        </div>\n" +
    "                                        <div>\n" +
    "\n" +
    "                                        </div>\n" +
    "                                        <span style=\"padding-left:80px\" class=\"col-lg-12\" ng-if=\"item.section === 'section_or' && item.item_description != 'section' && item.item_description != 'section_or'\">---- OR ----</span>\n" +
    "                                    </div>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>-->\n" +
    "\n" +
    "\n" +
    "\n" +
    "                    <div  >\n" +
    "                        <button class=\"btn-creditcard btn-leftBottom-orange pull-right\" ng-click=\"saveEventMenu('selected', EventObject)\">Continue</button>\n" +
    "                    </div>\n" +
    "\n" +
    "                </div>\n" +
    "                \n" +
    "                <div style='clear: both'></div>\n" +
    "                \n" +
    "                    <div ng-if=\"status === 'menu_pre_selected' || status === 'menu_selected' || status === 'confirmed'\" >\n" +
    "                    <p style=\"margin-top:20px\"  class=\"checkout-header text-center\">Menu selection</p>\n" +
    "\n" +
    "                    <div class=\"\" ng-class=\"\" ng-repeat=\"menu in menus\">\n" +
    "                        <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">\n" +
    "                            <h4 ng-if='menu.qte > 0' class=\"col-lg-6\" ng-bind=\"menu.notes\"></h4>\n" +
    "                            <!--<h5 style=\"float: left; margin-left:30px; \">(SGD 110)</h5>-->\n" +
    "                           <h5 class=\"col-lg-3\" ng-if=\"menu.setmeal\" style=\"float: left; margin-left:30px; \">(SGD {{menu.price}})</h5>\n" +
    "                               <div class=\"clear-both\"></div>\n" +
    "                            <div class=\"row\" ng-class=\"\" ng-repeat=\"item in menu.items\" >\n" +
    "                                <div  ng-if=\"menu.menuID === item.displayID\" ng-class=\"{'col-xs-10 col-sm-10 col-md-10 col-lg-10' :status !== 'confirmed','col-sm-12 col-md-12 col-lg-12' :status === 'confirmed'}\" style='display: table'>\n" +
    "                                    <div class=\"category-item-info\" style='display: table-cell' >\n" +
    "                                        <div class=\"location-icon\">\n" +
    "                                            <div class=\"location-text\" style=\"margin-top: 5px;\" ng-if=\"item.qte > 0 || menu.setmeal\">\n" +
    "                                                <p class=\"col-lg-9\" style='font-weight: bold;margin-top:15px;'><span>{{item.item_title}}</span></p>\n" +
    "<!--                                                <p class=\"col-lg-9\" style='font-size: 12px' ng-if=\"item.item_description != 'section' && item.item_description != 'section_or'\"><span>{{item.item_description}}</span></p>-->\n" +
    "                                                <label class=\"col-lg-3\" type=\"text\" readonly ng-value=\"{{item.qte || 0}}\" ng-if=\"item.item_description != 'section' && item.item_description != 'section_or'\"><span ng-if=\"item.qte > 0\">{{item.qte}} portions</span><span ng-if=\"item.qte < 1 && menu.setmeal\">included</span></label>\n" +
    "                                                <!--                                                <input class=\"col-lg-3\" type=\"text\" readonly ng-value=\"5 portions\" ng-if=\"item.item_description != 'section' && item.item_description != 'section_or'\">-->\n" +
    "                                            </div>\n" +
    "                                            <div>\n" +
    "\n" +
    "                                            </div>\n" +
    "                                            <span style=\"padding-left:80px\" class=\"col-lg-12\" ng-if=\"item.section === 'section_or' && item.item_description != 'section' && item.item_description != 'section_or' && item.item_description !='' \">---- OR ----</span>\n" +
    "                                        </div>\n" +
    "                                    </div>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    \n" +
    "\n" +
    "\n" +
    "<!--                    <div  style='' ng-if=\"status === 'menu_selected' || status === 'confirmed'\">\n" +
    "                        <p class=\"checkout-header text-center\">Dietary Restriction</p>\n" +
    "                        <div class=\"\" ng-class=\"\" ng-repeat=\"dietary in dietaries.slice(0, 1)\">\n" +
    "                            <div ng-class=\"{'col-xs-10 col-sm-10 col-md-10 col-lg-10' :status !== 'confirmed','col-sm-12 col-md-12 col-lg-12' :status === 'confirmed'}\" style='display: table'>\n" +
    "                                <div class=\"category-item-info\" style='display: table-cell' >\n" +
    "                                    <div class=\"location-icon\">\n" +
    "                                        <div class=\"location-text\" style=\"margin-top: 5px;\">\n" +
    "                                            <p class=\"col-lg-9\" style='font-weight: bold;margin-top:15px;'><span>{{dietary.title}}</span></p>\n" +
    "                                            <label class=\"col-lg-3\" type=\"text\" readonly ng-value=\"1 pax\" ng-if=\"item.item_description != 'section' && item.item_description != 'section_or'\">1 pax</label>\n" +
    "                                        </div>\n" +
    "                                        <div>\n" +
    "\n" +
    "                                        </div>\n" +
    "                                        <span style=\"padding-left:80px\" class=\"col-lg-12\" ng-if=\"item.section === 'section_or' && item.item_description != 'section' && item.item_description != 'section_or'\">---- OR ----</span>\n" +
    "                                    </div>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>-->\n" +
    "\n" +
    "\n" +
    "\n" +
    "\n" +
    "                    <div class=\"order-now\"  ng-if=\"status === 'menu_pre_selected'\" style=\"margin-top:20px;\">\n" +
    "                        <label style=\"margin-top: 30px;\"> By clicking Save your selection your menu selection will be send to the restaurant</label>\n" +
    "                        <button style=\"margin-top: 20px;\" class=\"btn-creditcard btn-leftBottom-orange pull-right\" ng-click=\"saveEventMenu('selected')\">Save you selection</button>\n" +
    "                    </div>\n" +
    "                    <div style='clear: both'></div>\n" +
    "                    <div  ng-if=\"(status === 'menu_selected' || status == 'menu_pre_selected' ||  status === 'confirmed') && EventObject.more.media.length > 0 \" >\n" +
    "                        <p style=\"margin-top:50px\" class=\"checkout-header text-center\">Media</p>\n" +
    "\n" +
    "                        <div class=\"col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12\" >\n" +
    "                            <div class='col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12' ng-repeat=\"evt in EventObject.more.media\" >\n" +
    "                                <a href=\"{{ mediaServer}}/upload/restaurant/{{EventObject.restaurant}}/700/{{evt.name}}\" class=\"fresco\" >\n" +
    "                                    <img ng-src=\"{{ mediaServer}}/upload/restaurant/{{EventObject.restaurant}}/180/{{evt.name}}\" class=\"img-responsive\"  style='max-height: 300px;'>\n" +
    "\n" +
    "                                </a>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div style='clear: both'></div>\n" +
    "\n" +
    "                    <div style='' ng-if=\"EventObject.billing && (status === 'menu_selected' || status == 'confirmed') \">\n" +
    "                        <p style=\"margin-top:50px\" class=\"checkout-header text-center\">Billing</p>\n" +
    "                        <div class=\"\" ng-class=\"\">\n" +
    "                            <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">\n" +
    "                                <!--                                <h4>Tncs</h4>-->\n" +
    "                                <div class=\"row\" ng-class=\"\" style=\"white-space:pre-wrap;margin-top: -20px;\">\n" +
    "                                    <p ng-bind-html=\"EventObject.billing\"></p>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div style='clear: both'></div>\n" +
    "                    <div style='' ng-if=\"(status === 'menu_selected' || status == 'confirmed') && EventObject.tnc_cancel\">\n" +
    "                        <p style=\"margin-top:50px\" class=\"checkout-header text-center\">Tncs</p>\n" +
    "                        <div class=\"\" ng-class=\"\">\n" +
    "                            <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">\n" +
    "                                <div class=\"row\" ng-class=\"\" style=\"white-space:pre-wrap;margin-top: -20px;\">\n" +
    "                                    <p ng-bind-html=\"EventObject.tnc_cancel\"></p>\n" +
    "                                </div>\n" +
    "\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div style='clear: both'></div>\n" +
    "                    <div style='' ng-if=\"EventObject.more.additionalnote && (status === 'menu_selected' || status == 'confirmed') \">\n" +
    "                        <p style=\"margin-top:50px\" class=\"checkout-header text-center\">Additional Note</p>\n" +
    "                        <div class=\"\" ng-class=\"\">\n" +
    "                            <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">\n" +
    "                                <!--                                <h4>Tncs</h4>-->\n" +
    "                                <div class=\"row\" ng-class=\"\" style=\"white-space:pre-wrap;margin-top: -20px;\">\n" +
    "                                    <p ng-bind-html=\"EventObject.more.additionalnote\"></p>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                        <div style='clear: both'></div>\n" +
    "                    <div  style='' ng-if=\"status === 'menu_selected'\">\n" +
    "                        <p class=\"checkout-header text-center\">Payment </p>\n" +
    "                        <div class=\"\" ng-class=\"\">\n" +
    "                            <p ng-if =\"EventObject.more.deposit !== 'nodeposit' && totalamount >0 && isRequiredcc\" >\n" +
    "                                <h4 > Restaurant required {{EventObject.more.deposit}}%  deposit amount </h4>\n" +
    "                                 <span  style=\"margin-left:20px;\" > Required deposit amount  : (SGD {{ amount * (EventObject.more.deposit / 100)  | number:2 }} )</span>\n" +
    "                                 <span ng-if='depositamount > 0 && isRequiredcc' style=\"margin-left:20px;\"> Paid : SGD {{depositamount | number:2}} </span>\n" +
    "                                <span ng-if='payable_amount > 0' style=\"margin-left:50px;\"> Balance Amount : SGD {{payable_amount * (EventObject.more.deposit / 100) | number:2 }} </span>\n" +
    "                            </p>\n" +
    "                            <div class=\"form-group checkbox col-lg-12 col-md-12 col-sm-12 col-xs-12\">\n" +
    "                                <!--<h4>Cancellation policies</h4>-->\n" +
    "                                <label ng-show =\"EventObject.payment_method.creditcard === true\">\n" +
    "                                    <input  class=\"payment_mode\" ng-model=\"swiftcode\" type=\"radio\"  name=\"optionsPaymentRadios\" id=\"optionsPaymentRadios1\" ng-checked=\"isRequiredcc\" value=\"creditcard\" ng-change=\"logIt('creditcard')\" > Credit Card\n" +
    "                                </label>\n" +
    "\n" +
    "                                <label ng-show =\"EventObject.payment_method.banktransfer === true\">\n" +
    "                                    <input  ng-model=\"swiftcode\" class=\"payment_mode\" type=\"radio\"  name=\"optionsPaymentRadios\" id=\"optionsPaymentRadios1\" value=\"banktransfer\" ng-change=\"logIt('banktransfer')\" >  Bank Transfer\n" +
    "                                    <label style='padding-left:30px;' ng-show=\"swiftcode === 'banktransfer'\"> <strong> Swift Code : AAAA BB CC DDD </strong></label>\n" +
    "                                </label>\n" +
    "                            </div>\n" +
    "\n" +
    "                            <form name=\"myForm\"  ng-if=\"status !== 'confirmed'\">\n" +
    "                                <label style=\"margin: 20px;\">\n" +
    "                                    <input type=\"checkbox\" ng-model=\"tncsaccepted\" style=\"margin: 20px;\">I accept terms and conditions\n" +
    "                                </label><br/>\n" +
    "\n" +
    "                                <div class=\"order-now\" ng-show ='isRequiredcc === true'  >\n" +
    "                                    <button ng-if=\"status === 'menu_selected' && tncsaccepted && (swiftcode === 'creditcard' || swiftcode === 'banktransfer')\" class=\"btn-creditcard btn-leftBottom-orange pull-right\" ng-click=\"submitCreditCardDetails()\">Submit your informations</button>\n" +
    "                                </div>\n" +
    "                                <div class=\"order-now\" ng-show ='isRequiredcc === false'  >\n" +
    "                                    <button ng-if=\"status === 'menu_selected' && tncsaccepted\" class=\"btn-creditcard btn-leftBottom-orange pull-right\" ng-click=\"submitCreditCardDetails()\">Submit your informations</button>\n" +
    "                                </div>\n" +
    "                            </form>\n" +
    "                        </div>\n" +
    "                    </div>    \n" +
    "                </div>\n" +
    "       \n" +
    "                <div style=\"padding-bottom: 30px\"  ng-if=\"status === 'menu_selected+++' || status === 'pending_payment' || status == 'confirmed'\">\n" +
    "                    <p class=\"checkout-header text-center\">Payment Details</p>\n" +
    "                    <div class='' ng-if=\"status === 'menu_selected+++'\">\n" +
    "                        <div class=\"event-request-form\">\n" +
    "                            <h2>Thank you, your request has  been sent to the restaurant</h2>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div style='clear: both'></div>\n" +
    "                        <div class='' ng-if=\"status === 'confirmed'\" >\n" +
    "                            <div class=\"event-request-form\" style='margin-left:30px;'>\n" +
    "                      \n" +
    "                                <p ng-if='status'>Status : <span ng-bind=\"status\"></span></p>\n" +
    "                                <p ng-if='totalamount > 0 '>Total Amount : SGD <span ng-bind=\"(amount | number:2)\"></span></p>\n" +
    "                                <p ng-if='EventObject.payment_details.payment_id'>Transaction Id : <span ng-bind=\"EventObject.payment_details.payment_id\"></span></p>\n" +
    "                                <p ng-if='pre_depositamount > 0'>Previous Deposit Amount : SGD <span ng-bind=\"(pre_depositamount ) | number:2\"></span></p>\n" +
    "                                <p ng-if='EventObject.payment_details.deposit_amount'>Deposited Amount : SGD <span ng-bind=\"EventObject.payment_details.deposit_amount | number:2\"></span></p>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    <div class='' ng-if=\"status === 'pending_payment'\">\n" +
    "                        <div class=\"event-request-form\">\n" +
    "                            <h2>Restaurant waiting for payment confirmation. </h2>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "\n" +
    "                <div id=\"event_footer\" ng-if=\"status === 'confirmed'\">\n" +
    "                    <h3 class=\"text-center\" ng-bind=\"restaurant.getTitle()\"></h3>\n" +
    "\n" +
    "                    <p class=\"text-center\"><span ng-bind=\"restaurant.getAddress()\"></span>, <span ng-bind=\"restaurant.getZip()\"></span></p>\n" +
    "\n" +
    "                    <p class=\"text-center\"><span ng-bind=\"restaurant.getPhone()\"></span></p>\n" +
    "\n" +
    "                    <p class=\"text-center\"><span ng-bind=\"restaurant.getUrl()\"></span></p>\n" +
    "                </div>\n" +
    "\n" +
    "            </div>\n" +
    "\n" +
    "            <div ng-class=\"{'col-md-2 col-lg-2' :status === 'confirmed' }\"></div>\n" +
    "\n" +
    "            <div ng-show=\"true && status !== 'confirmed'\" id=\"sidebar-right\" class='hidden-xs hidden-sm  col-md-4 sidebar' ng-class=\"{'hidden-xs hidden-sm col-md-4 sidebar':status !== 'confirmed', 'hidden-xs hidden-sm col-md-12':status === 'confirmed'}\" style=\"top:60px;box-shadow: 2px 2px 5px 2px rgba(204, 204, 204, 1);border-radius: 2px;\">\n" +
    "                <div class=\"sidebar-contant\">\n" +
    "                    <div class=\"img-logo\">\n" +
    "                        <img ng-src=\"{{restaurant.getLogoImage()}}\" />\n" +
    "                    </div>\n" +
    "                    <h3 class=\"text-center\" ng-bind=\"restaurant.getTitle()\"></h3>\n" +
    "\n" +
    "                    <p class=\"text-center\" ng-if=\"restaurant.getAddress()\"><span ng-bind=\"restaurant.getAddress()\"></span>, <span ng-bind=\"restaurant.getZip()\"></span></p>\n" +
    "\n" +
    "                    <p class=\"text-center\"><span ng-bind=\"restaurant.getPhone()\"></span></p>\n" +
    "\n" +
    "                    <p class=\"text-center\"><span ng-bind=\"restaurant.getUrl()\"></span></p>\n" +
    "                    <!--                <p ng-bind=\"DeliveryDate | timeFormat:'DD-MM-YYYY'\"></p>\n" +
    "                                    <p><span>Number of people</span>: <span ng-bind=\"EventObject.pax\"></span></p>\n" +
    "                                    <p><span>Price</span>: SGD <span ng-bind=\"RestaurantEvent.getPrice()\"></span></p>\n" +
    "                                    <p><span>Total</span>: SGD <span ng-bind=\"EventObject.pax :  RestaurantEvent.getPrice()\"></span></p>-->\n" +
    "                    <hr/>\n" +
    "                    <cartevent ng-show=\"items | lengthMoreThan:0\" items=\"items\" status=\"status\" ng-attr-minorder=\"EventObject.more.minimumprice\" deposit=\"EventObject.more.deposit\"  totalamount=\"totalamount\" menus=\"menus\" counters=\"counters\" minimumorder =\"minimumorder\" selectioncomplete=\"selectioncomplete\"></cartevent>\n" +
    "\n" +
    "\n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</section>\n" +
    "\n" +
    "");
}]);

angular.module("../app/components/event_management/_event_management_staff_form.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/components/event_management/_event_management_staff_form.tpl.html",
    "<style>\n" +
    "    body{\n" +
    "        font-family: arial;\n" +
    "        margin: 8px;\n" +
    "        background-color: #ffffff;\n" +
    "        color: #333333;\n" +
    "        line-height: unset;\n" +
    "    }\n" +
    "    .printarea{\n" +
    "        height: 1100px;\n" +
    "        width: 790px;\n" +
    "    }\n" +
    "    #header{\n" +
    "        width: 100%;\n" +
    "        text-align: center;\n" +
    "    }  \n" +
    "    #title{\n" +
    "        width: 100%;\n" +
    "        text-align: center;\n" +
    "        margin-bottom: 10px;\n" +
    "        font-size: 16px;\n" +
    "    } \n" +
    "    #footer{\n" +
    "        bottom: 0px;\n" +
    "        /*position: absolute;*/\n" +
    "        font-size: 11px;\n" +
    "        width: 100%;\n" +
    "        text-align: center;\n" +
    "    }  \n" +
    "    .content-staff{\n" +
    "        height: 1000px;\n" +
    "        font-size: 12px;\n" +
    "    }\n" +
    "    .content-staff table {\n" +
    "        width : 100%;\n" +
    "        height: 98%;\n" +
    "        padding: 0;\n" +
    "        margin: 0;\n" +
    "        border-collapse:collapse;\n" +
    "    }\n" +
    "    .content-staff tr {\n" +
    "        padding: 0;\n" +
    "        margin: 0;\n" +
    "\n" +
    "    }\n" +
    "    .content-staff th {\n" +
    "        background-color: #dddddd;\n" +
    "        -webkit-print-color-adjust: exact; \n" +
    "        height: 20px;\n" +
    "        text-align: center;\n" +
    "\n" +
    "    }\n" +
    "    .content-staff td {\n" +
    "        vertical-align: top;\n" +
    "        vertical-align: top;\n" +
    "        padding: 0;\n" +
    "        margin: 0;\n" +
    "        overflow: hidden;\n" +
    "    }\n" +
    "    .add-border-all{\n" +
    "        border-top: 1px solid black; \n" +
    "        border-bottom: 1px solid black;  \n" +
    "        border-left: 1px solid black; \n" +
    "        border-right: 1px solid black; \n" +
    "    }\n" +
    "    .add-border-top{\n" +
    "        border-top: 1px solid black; \n" +
    "    }\n" +
    "    .add-border-bottom{\n" +
    "        border-bottom: 1px solid black;  \n" +
    "    }\n" +
    "    .add-border-left{\n" +
    "        border-left: 1px solid black; \n" +
    "    }\n" +
    "    .add-border-right{\n" +
    "        border-right: 1px solid black; \n" +
    "    }\n" +
    "    .padding-cell{\n" +
    "        padding:5px;\n" +
    "    }\n" +
    "    .content-staff > table tr td table tr td{\n" +
    "        padding:5px;\n" +
    "    }\n" +
    "    .content-small-padding{\n" +
    "        padding:1px 3px!important;\n" +
    "        white-space: nowrap;\n" +
    "    }\n" +
    "    .content-valign-middle{\n" +
    "        vertical-align: middle!important;\n" +
    "    }\n" +
    "    label{\n" +
    "        font-weight: normal!important;\n" +
    "    }\n" +
    "    .payment-section {\n" +
    "        margin-left:40px;\n" +
    "    }\n" +
    "    .header-img{\n" +
    "        width :auto;\n" +
    "        max-height: 39px;\n" +
    "\n" +
    "    }\n" +
    "\n" +
    "\n" +
    "    @media screen{\n" +
    "        #header{\n" +
    "            display: none;\n" +
    "        }\n" +
    "        #footer{\n" +
    "            display: none;\n" +
    "        }\n" +
    "    }\n" +
    "    @media print {\n" +
    "        #header{\n" +
    "            display: block;\n" +
    "            margin-bottom: 20px;\n" +
    "        }\n" +
    "        #footer{\n" +
    "            bottom: 0px;\n" +
    "            position: absolute;\n" +
    "            font-size: 11px;\n" +
    "            width: 100%;\n" +
    "            text-align: center;\n" +
    "        }\n" +
    "    }\n" +
    "</style>\n" +
    "\n" +
    "<section>\n" +
    "    <div class=\"printarea\">\n" +
    "        <div>\n" +
    "            <div class=\"\" id=\"header\">\n" +
    "                <img class=\"header-img\" ng-src=\"{{restaurant.getLogoImage()}}\"  getLogoImage/>\n" +
    "            </div>\n" +
    "            <div class=\"\" id=\"title\">\n" +
    "                <div class=\"\">Event Order</div>\n" +
    "            </div>\n" +
    "            <div class=\"content-staff\">\n" +
    "                <table style=\"height:100%\">\n" +
    "                    <tr style=\"height:10%\">\n" +
    "                        <td colspan=2>\n" +
    "                            <table width='100%'>\n" +
    "                                <tr>\n" +
    "                                    <td class='add-border-all padding-cell' width=\"50%\">\n" +
    "                                        <table>\n" +
    "                                            <tr>\n" +
    "                                                <td class='content-small-padding'  style=\"width:30%\">Client:</td>\n" +
    "                                                <td class='content-small-padding' ><label ng-bind=\"EventObject.firstname\"></label></td>\n" +
    "                                            </tr>\n" +
    "                                            <tr>\n" +
    "                                                <td class='content-small-padding'  style=\"width:30%\">Phone:</td>\n" +
    "                                                <td class='content-small-padding' ><label ng-bind=\"EventObject.phone\"></label></td>\n" +
    "                                            </tr>\n" +
    "                                            <tr>\n" +
    "                                                <td class='content-small-padding'  style=\"width:30%\">Email:</td>\n" +
    "                                                <td class='content-small-padding' ><label ng-bind=\"EventObject.email\"></label></td>\n" +
    "                                            </tr>\n" +
    "                                        </table>\n" +
    "                                    </td>\n" +
    "                                    <td class='add-border-all padding-cell' width=\"50%\">\n" +
    "                                        <table>\n" +
    "                                            <tr>\n" +
    "                                                <td class='content-small-padding' style=\"width:30%\">Date:</td>\n" +
    "                                                <td class='content-small-padding' ><label ng-bind=\"EventObject.rdate\"></label></td>\n" +
    "                                            </tr>\n" +
    "                                            <tr>\n" +
    "                                                <td class='content-small-padding' style=\"width:30%\">Event id#:</td>\n" +
    "                                                <td  class='content-small-padding' ng-bind=\"EventObject.more.beo\"></td>\n" +
    "\n" +
    "                                            </tr>\n" +
    "                                            <tr>\n" +
    "                                                <td class='content-small-padding' style=\"width:30%\">Booked by:</td>\n" +
    "                                                <td class='content-small-padding'  ng-bind=\"EventObject.booked_by\"></td>\n" +
    "                                            </tr>\n" +
    "                                            <tr>\n" +
    "                                                <td class='content-small-padding' style=\"width:30%\">Phone:</td>\n" +
    "                                                <td class='content-small-padding' ng-bind=\"restaurant.getPhone()\" ></td>\n" +
    "                                            </tr>\n" +
    "                                        </table>\n" +
    "                                    </td>\n" +
    "                                </tr>\n" +
    "                            </table>\n" +
    "                        </td>\n" +
    "                    </tr>\n" +
    "                    <tr  style=\"height:10%\">\n" +
    "                        <td colspan=2>\n" +
    "                            <table >\n" +
    "                                <tr >\n" +
    "                                    <th class='add-border-all '>Date</th>\n" +
    "                                    <th class='add-border-all '>Time</th> \n" +
    "                                    <th class='add-border-all '>Function</th>\n" +
    "                                    <th class='add-border-all '>Venue</th>\n" +
    "                                    <th class='add-border-all '>Guaranteed</th> \n" +
    "                                    <th class='add-border-all '>Expected</th>\n" +
    "                                </tr>\n" +
    "\n" +
    "                                <tr ng-repeat=\"timing_item in EventObject.more.timing\">\n" +
    "                                    <td class='add-border-all content-small-padding'  style=\"width:20%;text-align:center; vertical-align: middle;\" ng-bind=\"EventObject.rdate\"></td>\n" +
    "                                    <td class='add-border-all content-small-padding'  style=\"width:10%;\" ng-bind=\"timing_item.time\"></td> \n" +
    "                                    <td class='add-border-all content-small-padding'  style=\"width:40%;\" ng-bind=\"timing_item.function\"></td>\n" +
    "                                    <td class='add-border-all content-small-padding'  style=\"width:10%;text-align:center; vertical-align: middle;\" ng-bind=\"timing_item.venue\"></td> \n" +
    "                                    <td class='add-border-all content-small-padding'  style=\"width:10%;text-align:center; vertical-align: middle;\" ng-bind=\"timing_item.garantee\"></td>\n" +
    "                                    <td class='add-border-all content-small-padding'  style=\"width:10%;text-align:center; vertical-align: middle;\" ng-bind=\"timing_item.expected\"></td>\n" +
    "                                <tr/>\n" +
    "                            </table>\n" +
    "                        </td>\n" +
    "                    </tr>\n" +
    "                    <tr  style=\"height:70%\"  >\n" +
    "                        <td width=\"50%\" >\n" +
    "                            <table style=\"height: 100%;\" class=\"add-border-all \">\n" +
    "                                <tr>\n" +
    "                                    <th  class='add-border-bottom' style=\"padding:0px\" >\n" +
    "                                        Food & beverage\n" +
    "                                    </th> \n" +
    "                                </tr>\n" +
    "                                <tr>\n" +
    "                                    <td style=\"\">\n" +
    "\n" +
    "                                        <div class=\"\" ng-class=\"\" ng-repeat=\"menu in menus\">\n" +
    "                                            <div class=\"col-xs-12 fixed\">\n" +
    "                                                <h4 ng-if='menu.qte > 0' class=\"fixed\" ng-bind=\"menu.notes\"></h4>\n" +
    "\n" +
    "                                                <div class=\"row\" ng-class=\"\" ng-repeat=\"item in menu.items\" >\n" +
    "                                                    <div  ng-if=\"menu.menuID === item.displayID\" class=\"fixed\" style='display: table;'>\n" +
    "                                                        <div class=\"category-item-info\" style='display: table-cell' >\n" +
    "                                                            <div class=\"location-icon\">\n" +
    "                                                                <div class=\"location-text\" style=\"margin-top: 5px;\" ng-if=\"item.qte > 0 || menu.setmeal\">\n" +
    "                                                                    <p class=\"col-xs-9 fixed\" style='font-weight: bold;margin-top:15px;' ng-if=\"item.item_description == 'section' || item.item_description == 'section_or'\"><span>{{item.item_title}}</span></p>\n" +
    "                                                                    <p class=\"col-xs-9 fixed \" style='font-size: 12px;' ng-if=\"item.item_description != 'section' && item.item_description != 'section_or'\"><span>{{item.item_title}}</span></p>\n" +
    "                                                                    <label class=\"col-xs-3 fixed\" type=\"text\" readonly ng-value=\" {{item.qte|| 0}}\" ng-if=\"item.item_description != 'section' && item.item_description != 'section_or'\" ><span ng-if=\"item.qte > 0\">{{item.qte}} portions</span><span ng-if=\"item.qte < 1 && menu.setmeal\">included</span></label>\n" +
    "                                                                </div>\n" +
    "                                                                <div ng-if=\"item.section === 'section_or' && item.item_description != 'section' && item.item_description != 'section_or'\" class=\"location-text\" style=\"margin-top: 5px;\">\n" +
    "                                                                    <p class=\"col-xs-12 fixed\" >---- OR ----</span> </p>\n" +
    "                                                                </div>\n" +
    "                                                            </div>\n" +
    "                                                        </div>\n" +
    "                                                    </div>\n" +
    "                                                </div>\n" +
    "                                            </div>\n" +
    "\n" +
    "                                    </td> \n" +
    "                                </tr>\n" +
    "                            </table>\n" +
    "                        </td>\n" +
    "                        <td  width=\"50%\" >\n" +
    "                            <table style=\"height:10%\" class=\"add-border-top add-border-right\">\n" +
    "                                <tr>\n" +
    "                                    <th class='add-border-bottom' style=\"padding:0px\" >\n" +
    "                                        Set up\n" +
    "                                    </th> \n" +
    "                                </tr>\n" +
    "                                <tr>\n" +
    "                                    <td>\n" +
    "                                        <span ng-bind=\"EventObject.setup\"></span>\n" +
    "                                    </td> \n" +
    "                                </tr>\n" +
    "                            </table>\n" +
    "                            <table style=\"height:45%\" class=\"add-border-top add-border-right\">\n" +
    "                                <tr>\n" +
    "                                    <th class='add-border-top add-border-bottom' style=\"padding:0px\" >\n" +
    "                                        Billing\n" +
    "                                    </th> \n" +
    "                                </tr>\n" +
    "                                <tr>\n" +
    "                                    <td>\n" +
    "                                        <div style=\"white-space:pre-wrap;\">\n" +
    "                                            <label ng-bind-html=\"EventObject.billing\"></label>\n" +
    "                                        </div>\n" +
    "\n" +
    "                                    </td> \n" +
    "                                </tr>\n" +
    "                            </table>\n" +
    "                            <table style=\"height:45%\" class=\"add-border-top add-border-right \">\n" +
    "                                <tr>\n" +
    "                                    <th class='add-border-top add-border-bottom' style=\"padding:0px\" >\n" +
    "                                        Payment\n" +
    "                                    </th> \n" +
    "                                </tr>\n" +
    "                                <tr class=\"add-border-bottom \"> \n" +
    "                                    <td>\n" +
    "                                        <h5 ng-if='EventObject.tnc_cancel' class='text-center' >Tncs</h5>\n" +
    "                                        <div style=\"white-space:pre-wrap;\">\n" +
    "                                            <label ng-bind-html=\"EventObject.tnc_cancel\"></label>\n" +
    "                                        </div>\n" +
    "                                        <h5  style='margin-left :60px;'>Transaction Details</h5>\n" +
    "                                        <div class='payment-section' >\n" +
    "                                            <p  ng-if='totalamount > 0'>Total Amount : SGD <span ng-bind=\"amount | number:2\"></span></p>\n" +
    "                                            <p  ng-if='EventObject.payment_details.type'>Payment Type : <span ng-bind=\"EventObject.payment_details.type\"></span></p>\n" +
    "                                            <p  ng-if='EventObject.payment_details.payment_id'>Transaction Id : <span ng-bind=\"EventObject.payment_details.payment_id\"></span></p>\n" +
    "                                            <p  ng-if='EventObject.payment_details.deposit_amount'>Deposited Amount : SGD <span ng-bind=\"EventObject.payment_details.deposit_amount | number:2\"></span></p>\n" +
    "                                        </div>\n" +
    "                                    </td> \n" +
    "                                </tr>\n" +
    "                            </table>\n" +
    "                        </td>\n" +
    "                    </tr>\n" +
    "                    <tr style=\"height:5%\">\n" +
    "                        <td colspan=2>\n" +
    "                            <table width='100%'>\n" +
    "                                <tr >\n" +
    "                                    <td colspan=2 class='content-valign-middle' >By submitting the credit card details, the client accepted terms and conditions</td>\n" +
    "                                </tr>\n" +
    "                            </table>\n" +
    "                        </td>\n" +
    "                    </tr>\n" +
    "                    <tr style=\"height:5%\">\n" +
    "                        <td colspan=2>\n" +
    "                            <table width='100%'>\n" +
    "                                <tr >\n" +
    "                                    <td class='content-valign-middle' width=\"50%\"></td>\n" +
    "                                    <!--                                    <td class='content-valign-middle' width=\"50%\">Identity Card/Passport No: ........................................................</td>-->\n" +
    "                                </tr>\n" +
    "                            </table>\n" +
    "                        </td>\n" +
    "                    </tr>\n" +
    "                </table>\n" +
    "            </div>\n" +
    "            <div class=\"\" id=\"footer\">Flower Dome, Garden By The Bay 18 Marina Gardens Drive #01-09, Singapore, 018953 +65 6604 9988 www.pollen.com </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</section>");
}]);

angular.module("../app/components/event_page/_event_page.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/components/event_page/_event_page.tpl.html",
    "<style>\n" +
    "    .introduction{\n" +
    "        margin: 50px 0px;\n" +
    "        font-weight: bold;\n" +
    "    }\n" +
    "\n" +
    "</style>\n" +
    "<div  class=\"landing-page\">\n" +
    "    <div class=\"container\">\n" +
    "        <div class=\"left\">\n" +
    "            <h1 style='   border: medium none;font-size: 36px;padding-top: 30px;text-align: center;'>Amazing deals to share</h1>\n" +
    "<!--            <div class=\"banner_restaurant\" style=\"background-image: url('https://static4.weeloy.com/images/event/book_restaurant_event_2016.jpg'); background-size: cover; background-position: 50% 50%; background-repeat: no-repeat no-repeat;\">\n" +
    "            </div>-->\n" +
    "            <p class='introduction'>\n" +
    "                Looking for some inspiration on where to dine next? Weeloy event listing brings the world to you instantly with great deals, seasonal event to exotic food tasting right here as we showcase the very best from our restaurant partners.\n" +
    "            </p>\n" +
    "            <div class=\"events\" >\n" +
    "                <ul style=\"max-width:600px; margin: auto; padding: 0px;\">\n" +
    "                    <li class=\"clearfix\" ng-repeat=\"evt in events\">\n" +
    "\n" +
    "                        <a analytics-on=\"click\"\n" +
    "                                        analytics-event=\"c_go_resto\"\n" +
    "                                        analytics-category=\"c_go_resto_event\" \n" +
    "                                        analytics-label=\"c_go_resto_event_{{evt.restaurant}}\"\n" +
    "                                        ng-href=\"{{getRestaurantLink(evt.restaurant)}}\"  style=\"text-decoration:none;\" title=\"Events of {{evt.title}} restaurant - reserve your table with weeloy\">\n" +
    "                            <h1>{{evt.title}}</h1>\n" +
    "                        </a>\n" +
    "                        <div class=\"cnt\">\n" +
    "                            <h2 ng-bind=\"evt.name\"></h2>\n" +
    "                            <span ng-if=\"evt.start === evt.end\" ng-bind=\"evt.end | date : format : shortDate\"></span>\n" +
    "                            <span  ng-if=\"evt.start !== evt.end\">{{evt.start| date : format : shortDate}} to {{evt.end| date : format : shortDate}}, {{evt.area}}</span>\n" +
    "                            <p ng-bind=\"evt.description\"></p>\n" +
    "                            \n" +
    "                            <a analytics-on=\"click\"\n" +
    "                                        analytics-event=\"c_get_pdf\"\n" +
    "                                        analytics-category=\"c_get_pdf_event\" \n" +
    "                                        analytics-label=\"c_get_pdf_event_{{evt.restaurant}}\" ng-href=\"{{getRestaurantLink(evt.restaurant)}}\" title=\"{{evt.title}} all event 2016\">Visit restaurant page</a>\n" +
    "\n" +
    "                            <div style=\"margin-top:10px;\">\n" +
    "                                <button analytics-on=\"click\"\n" +
    "                                        analytics-event=\"c_book_resto\"\n" +
    "                                        analytics-category=\"c_book_resto_event\" \n" +
    "                                        analytics-label=\"c_book_resto_event_{{evt.restaurant}}\" ng-show=\"restaurant.status != 'comingsoon'\" class=\"book-button-sm btn-leftBottom-orange\" ng-click=\"btnBookNowEvent_click(evt.restaurant)\" ng-bind=\"book_button_text\"></button>\n" +
    "                            </div> \n" +
    "                        </div>\n" +
    "                        <div class=\"img-block\">\n" +
    "                            <a href=\"{{ mediaServer}}/upload/restaurant/{{evt.restaurant}}/{{evt.picture}}\" class=\"fresco\" title=\"Event @ {{evt.title}} - reserve restaurant table now\">\n" +
    "                                <img ng-src=\"{{ mediaServer}}/upload/restaurant/{{evt.restaurant}}/180/{{evt.picture}}\" class=\"img-responsive\" alt=\"{{evt.title}} restaurant event - reserve restaurant table now\" style='max-height: 300px;'>\n" +
    "                                \n" +
    "                            </a>\n" +
    "                        </div>\n" +
    "                    </li>\n" +
    "                </ul>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("../app/components/faq_page/faq.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/components/faq_page/faq.tpl.html",
    "<div id=\"faq-page\">\n" +
    "    <div class=\"container\">\n" +
    "        <div class=\"row\">\n" +
    "            <div class=\"col-md-10 left-sec\">\n" +
    "                <div class=\"ol-typo-colswrap cols-2\">\n" +
    "                    <h2><strong>Frequently Asked Questions</strong></h2>\n" +
    "                    <p> </p>\n" +
    "                    <h3 style=\"margin: 0; color: #0f9de8;\">Discover WEELOY</h3>\n" +
    "                    <div class=\"ol-typo-column\">\n" +
    "                        <p>What is WEELOY?</p>\n" +
    "                    </div>\n" +
    "                    <div class=\"ol-typo-column last\">\n" +
    "                        <p>WEELOY is a dynamic distribution platform offering WEELOY Users, referred to as Weeloyers, dining privileges at select participating Weeloy restaurants. Weeloy makes it easier for Weeloyers to search for restaurants, compare offers and request a table reservation with reservation confirmation at select restaurants. Weeloy will be first introduced in Singapore, then in Thailand and Malaysia. Our intention is to expand our operations across the Asia Pacific region and eventually globally, providing you a wider selection of restaurants and services.</p>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"ol-typo-colswrap cols-2\">\n" +
    "                    <h3> </h3>\n" +
    "                    <h3 style=\"margin: 0; color: #0f9de8;\">Reservation Process</h3>\n" +
    "                    <div class=\"ol-typo-column\">\n" +
    "                        <p>How much does it cost to become a Weeloyer and to use the Weeloy systems?</p>\n" +
    "                    </div>\n" +
    "                    <div class=\"ol-typo-column last\">\n" +
    "                        <p>Absolutely FREE.</p>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"ol-typo-colswrap cols-2\">\n" +
    "                    <div class=\"ol-typo-column\">\n" +
    "                        <p>Do I need to register to use Weeloy to search for restaurant and to be a Weeloyer?</p>\n" +
    "                    </div>\n" +
    "                    <div class=\"ol-typo-column last\">\n" +
    "                        <p>You do not need to register to search restaurants and compare offers. However, once you decide to make a table reservation; just click on the “Get Weeloy Code” at your selected restaurant’s page; provide your name, mobile number, email address, date of visit, time and number of persons in your party. And, be sure to “tick” in the box provided that you agree to the terms and conditions. Once you complete this booking process for your first booking; you are automatically registered as a Weeloyer. And, you are no longer required to provide your profile information again for future table reservations.</p>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"ol-typo-colswrap cols-2\">\n" +
    "                    <div class=\"ol-typo-column\">\n" +
    "                        <p>How do I make a table reservation?</p>\n" +
    "                    </div>\n" +
    "                    <div class=\"ol-typo-column last\">\n" +
    "                        <p>Simply, search for your restaurant, select a restaurant and click on “Get “Weeloy Code” at the restaurant page or click on the “Book” tab at restaurant listing.</p>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"ol-typo-colswrap cols-2\">\n" +
    "                    <div class=\"ol-typo-column\">\n" +
    "                        <p>What happens after I get my “Weeloy Code” to book a table?</p>\n" +
    "                    </div>\n" +
    "                    <div class=\"ol-typo-column last\">\n" +
    "                        <p>Your weeloy code is automatically sent to your mobile number and email address registered with Weeloy when you requested your first “Weeloy Code”. A table reservation request is also sent to the restaurant to alert them of your visit. Select restaurants allow Weeloy to send you a confirmation of your table reservation request on their behalf. While others will contact you directly provided advance reservations are accepted. Then, when you arrive at the restaurant please identify yourself as a Weeloyer and present your “Weeloy Code” to the restaurant staff prior to ordering your meal.</p>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"ol-typo-colswrap cols-2\">\n" +
    "                    <div class=\"ol-typo-column\">\n" +
    "                        <p>Do I have access to a personal account on your website?</p>\n" +
    "                    </div>\n" +
    "                    <div class=\"ol-typo-column last\">\n" +
    "                        <p>Yes, once registered, a personal account is created for you. To access your personal account simply click on the button “Sign In” tab located at the top right of the Home page and enter your User name and password. We recommend you go to the Weeloyer MyAccount link to change your password, view and amend your details, current and past reservations, restaurants visited and share them with your friends. Also, see details of the special incentives for sharing WEELOY with your friends through your preferred social media networks and to add reviews about the restaurants you visited.</p>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"ol-typo-colswrap cols-2\">\n" +
    "                    <div class=\"ol-typo-column\">\n" +
    "                        <p>What happens if friends join my table and they are Weeloyers too?</p>\n" +
    "                    </div>\n" +
    "                    <div class=\"ol-typo-column last\">\n" +
    "                        <p>Your family and friends are most welcomed! However, only one Weeloyer’s benefit shall apply per party per table. And, you will have to decide amongst your friends or family members; whos’ Weeloyer benefit will be used for this visit.</p>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"ol-typo-colswrap cols-2\">\n" +
    "                    <h3> </h3>\n" +
    "                    <h3 style=\"margin: 0; color: #0f9de8;\">Information about the WEELOY Wheel</h3>\n" +
    "                    <div class=\"ol-typo-column\">\n" +
    "                        <p>What does the number in the middle of the Wheel mean?</p>\n" +
    "                    </div>\n" +
    "                    <div class=\"ol-typo-column last\">\n" +
    "                        <p>The number represents the total score of the Wheel. The Wheel has 24 parts and in each part an offer is displayed, which has a point ranking based on the perceived value of the offer. The total score of the Wheel is the summation of all 24 parts point score. The higher the score, the better the offers! </p>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"ol-typo-colswrap cols-2\">\n" +
    "                    <div class=\"ol-typo-column\">\n" +
    "                        <p>Where and how often can I spin the WEELOY Wheel?</p>\n" +
    "                    </div>\n" +
    "                    <div class=\"ol-typo-column last\">\n" +
    "                        <p>Weeloyers are entitled to spin the WEELOY Wheel at select participating restaurants as identified on the WEELOY website with an icon of the Wheel. A Weeloyer is entitled to spin the WEELOY Wheel only once per visit, per party per table.</p>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"ol-typo-colswrap cols-2\">\n" +
    "                    <div class=\"ol-typo-column\">\n" +
    "                        <p>Is the Wheel displayed on the restaurant page the Wheel I will spin at the restaurant?</p>\n" +
    "                    </div>\n" +
    "                    <div class=\"ol-typo-column last\">\n" +
    "                        <p>The Wheel displayed on the restaurant page is the \"Wheel of the Day” and is subject to change depending on the day you actually visit the restaurant and at the sole discretion of the respective restaurant management. Therefore, you may spin the same Wheel or a new Wheel when you visit the restaurant.</p>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"ol-typo-colswrap cols-2\">\n" +
    "                    <h3> </h3>\n" +
    "                    <h3 style=\"margin: 0; color: #0f9de8;\">Searching for a Restaurant</h3>\n" +
    "                    <div class=\"ol-typo-column\">\n" +
    "                        <p>How can I search for participating restaurants?</p>\n" +
    "                    </div>\n" +
    "                    <div class=\"ol-typo-column last\">\n" +
    "                        <p>Simply, visit the WEELOY website. You can use the search filters to find your favorite restaurant by name, favorite food or cuisine, location, your budget and “best Wheel”.</p>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"ol-typo-colswrap cols-2\">\n" +
    "                    <h3> </h3>\n" +
    "                    <h3 style=\"margin: 0; color: #0f9de8;\">Restaurant Content</h3>\n" +
    "                    <div class=\"ol-typo-column\">\n" +
    "                        <p>Who’s responsible for the restaurant content displayed?</p>\n" +
    "                    </div>\n" +
    "                    <div class=\"ol-typo-column last\">\n" +
    "                        <p>The respective restaurant is responsible to update its content and offers as often as they deem appropriate. Weeloy provides the distribution platform that enables its consumer Users the ability to easily search for restaurants and make a table reservation. However, Weeloy is not responsible or liable for the accuracy or timeliness of the restaurant content displayed on the Weeloy websites.</p>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"op-typo-pre-title\"> </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("../app/components/father_day_page/_father_day_page.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/components/father_day_page/_father_day_page.tpl.html",
    "<style>\n" +
    "     body{ background-color: white !important}\n" +
    "    .introduction{\n" +
    "        margin: 50px 0px;\n" +
    "        font-weight: bold;\n" +
    "    }\n" +
    "\n" +
    "    .eventholder \n" +
    "    {\n" +
    "        background-color: #fff;\n" +
    "        padding: 7px;\n" +
    "        position: relative;\n" +
    "        margin-bottom: 20px;\n" +
    "    }\n" +
    "    .events{\n" +
    "        margin-left:15%;\n" +
    "    }\n" +
    "    .banner-img {\n" +
    "        width: 70%;\n" +
    "        height: 295px;\n" +
    "        margin-left: 15%;\n" +
    "\n" +
    "    }\n" +
    "\n" +
    "    @media screen and (max-width: 1020px) {\n" +
    "        body {\n" +
    "            width:100%;\n" +
    "        }\n" +
    "    }\n" +
    "    @media screen and (max-width: 768px) {\n" +
    "            width:100%;\n" +
    "    }\n" +
    "\n" +
    "@media screen and (max-width: 480px) {\n" +
    "    .banner-img {\n" +
    "        width:100%;\n" +
    "        height:295px;\n" +
    "        margin:auto;\n" +
    "    }\n" +
    "     .events{\n" +
    "        margin-left:2%;\n" +
    "    }\n" +
    "}\n" +
    "\n" +
    "</style>\n" +
    "<div  class=\"landing-page\" style='background-color: white !important' >\n" +
    "    <div class=\"container-fluid\">\n" +
    "        <div class=\"row\">\n" +
    "                <img class='banner-img img-responsive' src=\"{{festivepromo.images}}\" alt=\"\"/>\n" +
    "            </div>\n" +
    "          \n" +
    "    </div>\n" +
    "    <section id=\"amazing\" style='background-color:white!important;'>\n" +
    "    <div class=\"container\">\n" +
    "        <p style=\"font-size:20px;\" class=\"text-center\">{{festivepromo.title}} </p>\n" +
    "        <p style=\"font-size:20px;margin-top:10px;text-align:center;margin-bottom: 20px;\n" +
    "                 padding-bottom: 10px;\">{{festivepromo.description}}</p>\n" +
    "        <div>\n" +
    "            <div class=\"zen-amazing\">\n" +
    "                <div class=\"col-xs-12 col-sm-4\" ng-repeat=\"event in events\">\n" +
    "                     <restaurant-event-itemnew open-in-new-tab=\"true\" media-server=\"mediaServer\" event=\"event\"></restaurant-event-itemnew>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</section>\n" +
    "\n" +
    "\n" +
    "<!--    <div class=\"container-fluid\">\n" +
    "         <div class=\"row\">\n" +
    "             <div class='col-lg-12 col-md-8 col-sm-12 col-xs-12 '>\n" +
    "                    <p style=\"font-size:20px;margin-top:10px;text-align:center;\">\n" +
    "                       {{festivepromo.title}} \n" +
    "                    </p>\n" +
    "                    <p style=\"font-size:20px;margin-top:10px;text-align:center;margin-bottom: 20px;\n" +
    "    padding-bottom: 10px;\">{{festivepromo.subtitle}}.</p>\n" +
    "                 <p style=\"margin-left:20px;text-align:center;font-size:20px;margin-bottom:20px;padding-bottom:15px;\">from some of the best restaurants in Singapore.</p>\n" +
    "                </div>\n" +
    "         </div>\n" +
    "            <div class=\"row\">\n" +
    "        <div class=\"col-md-12\" style='width:80%!important;margin-left:15%;'>\n" +
    "               <div ng-repeat=\"evt in events\" style='margin-bottom:10px;'>\n" +
    "                        <div class=\"col-md-4\">\n" +
    "                            <a href=\"{{ mediaServer}}/upload/restaurant/{{evt.restaurant}}/{{evt.picture}}\" class=\"fresco\" title=\"{{evt.title}} - reserve restaurant table now\">\n" +
    "                               <img ng-src=\"{{mediaServer}}/upload/restaurant/{{evt.restaurant}}/270/{{evt.picture}}\" class=\"img-responsive\" alt=\"{{evt.title}} year end festive special menus - reserve restaurant table now\" style='max-height:270px;float:left;margin-bottom:15px;'>\n" +
    "                            </a>\n" +
    "                            <span style='margin-left:5px;padding-left:5px;display:table;'>\n" +
    "                            <p id ='event-title' style='margin-bottom:0px;font-family: sans-serif;padding-left:5px;'><strong>{{evt.name}}</strong></p>\n" +
    "                             <span style='margin-top:0px;padding-left:5px;' >{{evt.end| date : format : shortDate}}</span>\n" +
    "                            <p style='margin-top:0px;padding-left:5px;'>{{evt.title}}, {{evt.area}}</p>\n" +
    "                            <p id='evt-content' ng-maxlength=\"15\" href ng-if=\"evt.description !== ''\" data-toggle=\"popover\" data-placement=\"top\" title=\"{{evt.description}}\" onmouseenter=\"$(this).popover('show')\" onmouseleave=\"$(this).popover('hide')\"  style='font-family: Roboto, Arial, Helvetica;padding-left:5px;'>{{evt.description | limitTo:180}}...</p>\n" +
    "                            \n" +
    "                             <button  ng-show=\"restaurant.status != 'comingsoon'\" class=\"book-button-sm btn-leftBottom-blue\" ng-click=\"btnBookNowEvent_click(evt.restaurant)\" ng-bind=\"book_button_text\"></button>\n" +
    "                            </span>\n" +
    "                    \n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "            <img style='float:left;width:200px;height:200px; margin-right:10px;' src=\"img/me.jpg\" />\n" +
    "            <p>Lots of text here...With the four tiers of grids available you're bound to run into issues where, at certain breakpoints, your columns don't clear quite right as one is taller than the other. To fix that, use a combination of a .clearfix and o</p>\n" +
    "        </div>\n" +
    "    </div>-->\n" +
    "<!--         <ul class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12 list-unstyled\" style='margin-left:15%!important;padding-left:0px;width:70%' >\n" +
    "                    \n" +
    "                    <li  class=\"col-md-4 col-sm-12\" ng-repeat=\"evt in events\" style='height:270px;padding-left:0px;'>\n" +
    "                        \n" +
    "                         <div class=\"col-md-4 col-sm-6\" style='margin-left:0px !important;padding-left:0px;margin-bottom:0px;'>\n" +
    "                            <a ng-if='!isMobile' href=\"{{mediaServer}}/upload/restaurant/{{evt.restaurant}}/{{evt.picture}}\" class=\"fresco\" title=\"Valentine day @ {{evt.title}} - reserve restaurant table now\">\n" +
    "                                <img ng-src=\"{{ mediaServer}}/upload/restaurant/{{evt.restaurant}}/360/{{evt.picture}}\" class=\"img-responsive\" alt=\"{{evt.title}} Valentine day Menu - reserve restaurant table now\" style='margin-bottom:10px;width:100%;margin-top:10px;padding-top:10px;margin-left:15px;'>\n" +
    "                            </a>\n" +
    "                              <a ng-if='isMobile' ng-click=\"btnBookNowEvent_click(evt.restaurant)\"  title=\"Valentine day @ {{evt.title}} - reserve restaurant table now\">\n" +
    "                                <img ng-src=\"{{ mediaServer}}/upload/restaurant/{{evt.restaurant}}/360/{{evt.picture}}\" class=\"img-responsive\" alt=\"{{evt.title}} Valentine day Menu - reserve restaurant table now\" style='margin-bottom:10px;width:100%;margin-top:10px;padding-top:10px;margin-left:15px;'>\n" +
    "                            </a>\n" +
    "                        </div>\n" +
    "                        <div class=\"col-md-8 col-sm-12 col-xs-12\" style='margin-bottom:10px;padding-bottom:10px;padding-left:15px;padding-right:0px;margin-top:20px;'>\n" +
    "                            <p id ='event-title' style='margin-bottom:0px;font-family: sans-serif;'><strong>{{evt.name}}</strong></p>\n" +
    "                            <div class=\"cnt\" style='margin-top:0px;max-height:190px;'>\n" +
    "                                <p style='margin-top:0px;max-height:160px;'>\n" +
    "                            <span ng-if=\"evt.start === evt.end\" ng-bind=\"evt.end | date : format : shortDate\"></span>\n" +
    "                            <span style='margin-top:0px;' >{{evt.end| date : format : shortDate}}</span>\n" +
    "                             <p style='margin-top:0px;'>{{evt.title}}, {{evt.area}}</p>\n" +
    "                             \n" +
    "                             <p id='evt-content' ng-maxlength=\"15\" href ng-if=\"evt.description !== ''\" data-toggle=\"popover\" data-placement=\"top\" title=\"{{evt.description}}\" onmouseenter=\"$(this).popover('show')\" onmouseleave=\"$(this).popover('hide')\"  style='float: left;font-family: Roboto, Arial, Helvetica;'>{{evt.description | limitTo:150}}...</p>\n" +
    "                              <span ng-if=\"evt.price!= '' \"  style='margin-top:10px;'>${{evt.price}} per couple </span>\n" +
    "                             <div style=\"margin-top:20px;padding-top:20px;\">\n" +
    "                                <button  ng-show=\"restaurant.status != 'comingsoon'\" class=\"book-button-sm btn-leftBottom-blue\" ng-click=\"btnBookNowEvent_click(evt.restaurant)\" ng-bind=\"book_button_text\"></button>\n" +
    "                             </div> \n" +
    "                            </p>\n" +
    "                        </div>\n" +
    "                \n" +
    "                        </div>\n" +
    "  \n" +
    "                        <a analytics-on=\"click\"\n" +
    "                                        analytics-event=\"c_go_resto\"\n" +
    "                                        analytics-category=\"c_go_resto_VDay\" \n" +
    "                                        analytics-label=\"c_go_resto_VDay_{{evt.restaurant}}\"\n" +
    "                                        ng-href=\"{{getRestaurantLink(evt.restaurant)}}\"  style=\"text-decoration:none;\" title=\"Celebrate the VDay with {{evt.title}} - reserve your table with weeloy\">\n" +
    "                            <h1>Celebrate the VDay with {{evt.title}}</h1>\n" +
    "                        </a>\n" +
    "                        <div class=\"cnt\">\n" +
    "                            <h2 ng-bind=\"evt.name\"></h2>\n" +
    "                            <span ng-if=\"evt.start === evt.end\" ng-bind=\"evt.end | date : format : shortDate\"></span>\n" +
    "                            <span  ng-if=\"evt.start !== evt.end\">{{evt.start| date : format : shortDate}} to {{evt.end| date : format : shortDate}}, {{evt.area}}</span>\n" +
    "                            <p ng-bind=\"evt.description\"></p>\n" +
    "                            \n" +
    "                            <a analytics-on=\"click\"\n" +
    "                                        analytics-event=\"c_get_pdf\"\n" +
    "                                        analytics-category=\"c_get_pdf_VDay\" \n" +
    "                                        analytics-label=\"c_get_pdf_VDay_{{evt.restaurant}}\"href=\"{{ mediaServer}}/upload/restaurant/{{evt.restaurant}}/{{evt.pdf_link}}\" target=\"_blank\" ng-if=\"evt.pdf_link\" title=\"{{evt.title}} Menu for Valentine day 2016\">Download VDay menu</a>\n" +
    "\n" +
    "                            <div style=\"margin-top:10px;\">\n" +
    "                                <button analytics-on=\"click\"\n" +
    "                                        analytics-event=\"c_book_resto\"\n" +
    "                                        analytics-category=\"c_book_resto_VDay\" \n" +
    "                                        analytics-label=\"c_book_resto_VDay_{{evt.restaurant}}\" ng-show=\"restaurant.status != 'comingsoon'\" class=\"book-button-sm btn-leftBottom-orange\" ng-click=\"btnBookNowEvent_click(evt.restaurant)\" ng-bind=\"book_button_text\"></button>\n" +
    "                            </div> \n" +
    "                        </div>\n" +
    "                       \n" +
    "                    </li>\n" +
    "                   \n" +
    "                </ul>-->\n" +
    "<!--        <div class=\"left\">\n" +
    "            <div class=\"events\"  >\n" +
    "                <div class=\"row\">\n" +
    "                    <div ng-repeat=\"evt in events\">\n" +
    "                        <div class=\"col-md-3 eventholder\">\n" +
    "                            <a href=\"{{ mediaServer}}/upload/restaurant/{{evt.restaurant}}/{{evt.picture}}\" class=\"fresco\" title=\"{{evt.title}} - reserve restaurant table now\">\n" +
    "                               <img ng-src=\"{{mediaServer}}/upload/restaurant/{{evt.restaurant}}/270/{{evt.picture}}\" class=\"img-responsive\" alt=\"{{evt.title}} year end festive special menus - reserve restaurant table now\" style='max-height:270px;'>\n" +
    "                            </a>\n" +
    "                            <p id ='event-title' style='margin-bottom:0px;font-family: sans-serif;'><strong>{{evt.name}}</strong></p>\n" +
    "                             <span style='margin-top:0px;' >{{evt.end| date : format : shortDate}}</span>\n" +
    "                            <p style='margin-top:0px;'>{{evt.title}}, {{evt.area}}</p>\n" +
    "                            <p id='evt-content' ng-maxlength=\"15\" href ng-if=\"evt.description !== ''\" data-toggle=\"popover\" data-placement=\"top\" title=\"{{evt.description}}\" onmouseenter=\"$(this).popover('show')\" onmouseleave=\"$(this).popover('hide')\"  style='font-family: Roboto, Arial, Helvetica;max-width:270px;'>{{evt.description | limitTo:200}}...</p>\n" +
    "                            \n" +
    "                             <button  ng-show=\"restaurant.status != 'comingsoon'\" class=\"book-button-lg btn-leftBottom-blue\" ng-click=\"btnBookNowEvent_click(evt.restaurant)\" ng-bind=\"book_button_text\" style='width:98%!important;'></button>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    \n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "        </div>-->\n" +
    "<!--    </div>-->\n" +
    "</div>\n" +
    "");
}]);

angular.module("../app/components/festive_promo/_festive_promo_page.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/components/festive_promo/_festive_promo_page.tpl.html",
    "<style>\n" +
    "     body{ background-color: white !important}\n" +
    "    .introduction{\n" +
    "        margin: 50px 0px;\n" +
    "        font-weight: bold;\n" +
    "    }\n" +
    "\n" +
    "    .eventholder \n" +
    "    {\n" +
    "        background-color: #fff;\n" +
    "        padding: 7px;\n" +
    "        position: relative;\n" +
    "        margin-bottom: 20px;\n" +
    "    }\n" +
    "    .events{\n" +
    "        margin-left:15%;\n" +
    "    }\n" +
    "    .banner-img {\n" +
    "        width: 70%;\n" +
    "        height: 295px;\n" +
    "        margin-left: 15%;\n" +
    "\n" +
    "    }\n" +
    "\n" +
    "    @media screen and (max-width: 1020px) {\n" +
    "        body {\n" +
    "            width:100%;\n" +
    "        }\n" +
    "    }\n" +
    "    @media screen and (max-width: 768px) {\n" +
    "            width:100%;\n" +
    "    }\n" +
    "\n" +
    "@media screen and (max-width: 480px) {\n" +
    "    .banner-img {\n" +
    "        width:100%;\n" +
    "        height:295px;\n" +
    "        margin:auto;\n" +
    "    }\n" +
    "     .events{\n" +
    "        margin-left:2%;\n" +
    "    }\n" +
    "}\n" +
    "\n" +
    "</style>\n" +
    "<div  class=\"landing-page\" style='background-color: white !important' >\n" +
    "    <div class=\"container-fluid\">\n" +
    "        <div class=\"row\">\n" +
    "                <img class='banner-img img-responsive' src=\"{{festivepromo.images}}\" alt=\"\"/>\n" +
    "            </div>\n" +
    "          \n" +
    "    </div>\n" +
    "    <section id=\"amazing\" style='background-color:white!important;'>\n" +
    "    <div class=\"container\">\n" +
    "        <p style=\"font-size:20px;\" class=\"text-center\">{{festivepromo.title}} </p>\n" +
    "        <p style=\"font-size:20px;margin-top:10px;text-align:center;margin-bottom: 20px;\n" +
    "                 padding-bottom: 10px;\">Celebrate the joyous occasion with your mum at one of these restaurants.</p>\n" +
    "        <div>\n" +
    "            <div class=\"zen-amazing\">\n" +
    "                <div class=\"col-xs-12 col-sm-4\" ng-repeat=\"event in events\">\n" +
    "                     <restaurant-event-itemnew open-in-new-tab=\"true\" media-server=\"mediaServer\" event=\"event\"></restaurant-event-itemnew>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</section>\n" +
    "\n" +
    "\n" +
    "<!--    <div class=\"container-fluid\">\n" +
    "         <div class=\"row\">\n" +
    "             <div class='col-lg-12 col-md-8 col-sm-12 col-xs-12 '>\n" +
    "                    <p style=\"font-size:20px;margin-top:10px;text-align:center;\">\n" +
    "                       {{festivepromo.title}} \n" +
    "                    </p>\n" +
    "                    <p style=\"font-size:20px;margin-top:10px;text-align:center;margin-bottom: 20px;\n" +
    "    padding-bottom: 10px;\">{{festivepromo.subtitle}}.</p>\n" +
    "                 <p style=\"margin-left:20px;text-align:center;font-size:20px;margin-bottom:20px;padding-bottom:15px;\">from some of the best restaurants in Singapore.</p>\n" +
    "                </div>\n" +
    "         </div>\n" +
    "            <div class=\"row\">\n" +
    "        <div class=\"col-md-12\" style='width:80%!important;margin-left:15%;'>\n" +
    "               <div ng-repeat=\"evt in events\" style='margin-bottom:10px;'>\n" +
    "                        <div class=\"col-md-4\">\n" +
    "                            <a href=\"{{ mediaServer}}/upload/restaurant/{{evt.restaurant}}/{{evt.picture}}\" class=\"fresco\" title=\"{{evt.title}} - reserve restaurant table now\">\n" +
    "                               <img ng-src=\"{{mediaServer}}/upload/restaurant/{{evt.restaurant}}/270/{{evt.picture}}\" class=\"img-responsive\" alt=\"{{evt.title}} year end festive special menus - reserve restaurant table now\" style='max-height:270px;float:left;margin-bottom:15px;'>\n" +
    "                            </a>\n" +
    "                            <span style='margin-left:5px;padding-left:5px;display:table;'>\n" +
    "                            <p id ='event-title' style='margin-bottom:0px;font-family: sans-serif;padding-left:5px;'><strong>{{evt.name}}</strong></p>\n" +
    "                             <span style='margin-top:0px;padding-left:5px;' >{{evt.end| date : format : shortDate}}</span>\n" +
    "                            <p style='margin-top:0px;padding-left:5px;'>{{evt.title}}, {{evt.area}}</p>\n" +
    "                            <p id='evt-content' ng-maxlength=\"15\" href ng-if=\"evt.description !== ''\" data-toggle=\"popover\" data-placement=\"top\" title=\"{{evt.description}}\" onmouseenter=\"$(this).popover('show')\" onmouseleave=\"$(this).popover('hide')\"  style='font-family: Roboto, Arial, Helvetica;padding-left:5px;'>{{evt.description | limitTo:180}}...</p>\n" +
    "                            \n" +
    "                             <button  ng-show=\"restaurant.status != 'comingsoon'\" class=\"book-button-sm btn-leftBottom-blue\" ng-click=\"btnBookNowEvent_click(evt.restaurant)\" ng-bind=\"book_button_text\"></button>\n" +
    "                            </span>\n" +
    "                    \n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "            <img style='float:left;width:200px;height:200px; margin-right:10px;' src=\"img/me.jpg\" />\n" +
    "            <p>Lots of text here...With the four tiers of grids available you're bound to run into issues where, at certain breakpoints, your columns don't clear quite right as one is taller than the other. To fix that, use a combination of a .clearfix and o</p>\n" +
    "        </div>\n" +
    "    </div>-->\n" +
    "<!--         <ul class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12 list-unstyled\" style='margin-left:15%!important;padding-left:0px;width:70%' >\n" +
    "                    \n" +
    "                    <li  class=\"col-md-4 col-sm-12\" ng-repeat=\"evt in events\" style='height:270px;padding-left:0px;'>\n" +
    "                        \n" +
    "                         <div class=\"col-md-4 col-sm-6\" style='margin-left:0px !important;padding-left:0px;margin-bottom:0px;'>\n" +
    "                            <a ng-if='!isMobile' href=\"{{mediaServer}}/upload/restaurant/{{evt.restaurant}}/{{evt.picture}}\" class=\"fresco\" title=\"Valentine day @ {{evt.title}} - reserve restaurant table now\">\n" +
    "                                <img ng-src=\"{{ mediaServer}}/upload/restaurant/{{evt.restaurant}}/360/{{evt.picture}}\" class=\"img-responsive\" alt=\"{{evt.title}} Valentine day Menu - reserve restaurant table now\" style='margin-bottom:10px;width:100%;margin-top:10px;padding-top:10px;margin-left:15px;'>\n" +
    "                            </a>\n" +
    "                              <a ng-if='isMobile' ng-click=\"btnBookNowEvent_click(evt.restaurant)\"  title=\"Valentine day @ {{evt.title}} - reserve restaurant table now\">\n" +
    "                                <img ng-src=\"{{ mediaServer}}/upload/restaurant/{{evt.restaurant}}/360/{{evt.picture}}\" class=\"img-responsive\" alt=\"{{evt.title}} Valentine day Menu - reserve restaurant table now\" style='margin-bottom:10px;width:100%;margin-top:10px;padding-top:10px;margin-left:15px;'>\n" +
    "                            </a>\n" +
    "                        </div>\n" +
    "                        <div class=\"col-md-8 col-sm-12 col-xs-12\" style='margin-bottom:10px;padding-bottom:10px;padding-left:15px;padding-right:0px;margin-top:20px;'>\n" +
    "                            <p id ='event-title' style='margin-bottom:0px;font-family: sans-serif;'><strong>{{evt.name}}</strong></p>\n" +
    "                            <div class=\"cnt\" style='margin-top:0px;max-height:190px;'>\n" +
    "                                <p style='margin-top:0px;max-height:160px;'>\n" +
    "                            <span ng-if=\"evt.start === evt.end\" ng-bind=\"evt.end | date : format : shortDate\"></span>\n" +
    "                            <span style='margin-top:0px;' >{{evt.end| date : format : shortDate}}</span>\n" +
    "                             <p style='margin-top:0px;'>{{evt.title}}, {{evt.area}}</p>\n" +
    "                             \n" +
    "                             <p id='evt-content' ng-maxlength=\"15\" href ng-if=\"evt.description !== ''\" data-toggle=\"popover\" data-placement=\"top\" title=\"{{evt.description}}\" onmouseenter=\"$(this).popover('show')\" onmouseleave=\"$(this).popover('hide')\"  style='float: left;font-family: Roboto, Arial, Helvetica;'>{{evt.description | limitTo:150}}...</p>\n" +
    "                              <span ng-if=\"evt.price!= '' \"  style='margin-top:10px;'>${{evt.price}} per couple </span>\n" +
    "                             <div style=\"margin-top:20px;padding-top:20px;\">\n" +
    "                                <button  ng-show=\"restaurant.status != 'comingsoon'\" class=\"book-button-sm btn-leftBottom-blue\" ng-click=\"btnBookNowEvent_click(evt.restaurant)\" ng-bind=\"book_button_text\"></button>\n" +
    "                             </div> \n" +
    "                            </p>\n" +
    "                        </div>\n" +
    "                \n" +
    "                        </div>\n" +
    "  \n" +
    "                        <a analytics-on=\"click\"\n" +
    "                                        analytics-event=\"c_go_resto\"\n" +
    "                                        analytics-category=\"c_go_resto_VDay\" \n" +
    "                                        analytics-label=\"c_go_resto_VDay_{{evt.restaurant}}\"\n" +
    "                                        ng-href=\"{{getRestaurantLink(evt.restaurant)}}\"  style=\"text-decoration:none;\" title=\"Celebrate the VDay with {{evt.title}} - reserve your table with weeloy\">\n" +
    "                            <h1>Celebrate the VDay with {{evt.title}}</h1>\n" +
    "                        </a>\n" +
    "                        <div class=\"cnt\">\n" +
    "                            <h2 ng-bind=\"evt.name\"></h2>\n" +
    "                            <span ng-if=\"evt.start === evt.end\" ng-bind=\"evt.end | date : format : shortDate\"></span>\n" +
    "                            <span  ng-if=\"evt.start !== evt.end\">{{evt.start| date : format : shortDate}} to {{evt.end| date : format : shortDate}}, {{evt.area}}</span>\n" +
    "                            <p ng-bind=\"evt.description\"></p>\n" +
    "                            \n" +
    "                            <a analytics-on=\"click\"\n" +
    "                                        analytics-event=\"c_get_pdf\"\n" +
    "                                        analytics-category=\"c_get_pdf_VDay\" \n" +
    "                                        analytics-label=\"c_get_pdf_VDay_{{evt.restaurant}}\"href=\"{{ mediaServer}}/upload/restaurant/{{evt.restaurant}}/{{evt.pdf_link}}\" target=\"_blank\" ng-if=\"evt.pdf_link\" title=\"{{evt.title}} Menu for Valentine day 2016\">Download VDay menu</a>\n" +
    "\n" +
    "                            <div style=\"margin-top:10px;\">\n" +
    "                                <button analytics-on=\"click\"\n" +
    "                                        analytics-event=\"c_book_resto\"\n" +
    "                                        analytics-category=\"c_book_resto_VDay\" \n" +
    "                                        analytics-label=\"c_book_resto_VDay_{{evt.restaurant}}\" ng-show=\"restaurant.status != 'comingsoon'\" class=\"book-button-sm btn-leftBottom-orange\" ng-click=\"btnBookNowEvent_click(evt.restaurant)\" ng-bind=\"book_button_text\"></button>\n" +
    "                            </div> \n" +
    "                        </div>\n" +
    "                       \n" +
    "                    </li>\n" +
    "                   \n" +
    "                </ul>-->\n" +
    "<!--        <div class=\"left\">\n" +
    "            <div class=\"events\"  >\n" +
    "                <div class=\"row\">\n" +
    "                    <div ng-repeat=\"evt in events\">\n" +
    "                        <div class=\"col-md-3 eventholder\">\n" +
    "                            <a href=\"{{ mediaServer}}/upload/restaurant/{{evt.restaurant}}/{{evt.picture}}\" class=\"fresco\" title=\"{{evt.title}} - reserve restaurant table now\">\n" +
    "                               <img ng-src=\"{{mediaServer}}/upload/restaurant/{{evt.restaurant}}/270/{{evt.picture}}\" class=\"img-responsive\" alt=\"{{evt.title}} year end festive special menus - reserve restaurant table now\" style='max-height:270px;'>\n" +
    "                            </a>\n" +
    "                            <p id ='event-title' style='margin-bottom:0px;font-family: sans-serif;'><strong>{{evt.name}}</strong></p>\n" +
    "                             <span style='margin-top:0px;' >{{evt.end| date : format : shortDate}}</span>\n" +
    "                            <p style='margin-top:0px;'>{{evt.title}}, {{evt.area}}</p>\n" +
    "                            <p id='evt-content' ng-maxlength=\"15\" href ng-if=\"evt.description !== ''\" data-toggle=\"popover\" data-placement=\"top\" title=\"{{evt.description}}\" onmouseenter=\"$(this).popover('show')\" onmouseleave=\"$(this).popover('hide')\"  style='font-family: Roboto, Arial, Helvetica;max-width:270px;'>{{evt.description | limitTo:200}}...</p>\n" +
    "                            \n" +
    "                             <button  ng-show=\"restaurant.status != 'comingsoon'\" class=\"book-button-lg btn-leftBottom-blue\" ng-click=\"btnBookNowEvent_click(evt.restaurant)\" ng-bind=\"book_button_text\" style='width:98%!important;'></button>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    \n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "        </div>-->\n" +
    "<!--    </div>-->\n" +
    "</div>\n" +
    "");
}]);

angular.module("../app/components/home/_home.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/components/home/_home.tpl.html",
    "<section class=\"header-container\">\n" +
    "    <video ng-if=\"screenWidth > 550\" loop=\"loop\" preload=\"none\" muted=\"muted\" id=\"pretzel-video\" poster=\"https://static2.weeloy.com/videos/banner/pluck/high/pluck.jpg\" class=\"video-playing\">\n" +
    "<!--<video  loop=\"loop\" preload=\"none\" muted=\"muted\" id=\"pretzel-video\"  class=\"video-playing\">-->\n" +
    "<!--        <source src=\"https://static2.weeloy.com/videos/banner/pluck/high/pluck.m4v\" type=\"video/mp4\">-->\n" +
    "<!--         <source src=\"https://static2.weeloy.com/videos/banner/pluck/high/pluck.m4v\" type=\"video/mp4\">-->\n" +
    " \n" +
    "                <source home-page-video id='videoplayer' src=\"images/Sticky_pudding.mp4\" type=\"video/mp4\">\n" +
    "\n" +
    "<!--        <source src=\"https://static2.weeloy.com/videos/banner/pluck/high/pluck.webm\" type=\"video/webm\" />\n" +
    "        <source src=\"https://static2.weeloy.com/videos/banner/pluck/high/pluck.ogv\" type=\"video/ogg\" />-->\n" +
    "    </video>\n" +
    "    <!-- search -->\n" +
    "    <div class=\"custom-form-search-bar container\">\n" +
    "        <nf-search class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\" hide-input-on-mobile=\"true\" placeholder=\"Str.template.SearchInputPlaceHolder\" button-text=\"Str.template.SearchBtnText\" cities=\"cities\"></nf-search>\n" +
    "    </div>\n" +
    "    <div class=\"view-all-restaurant text-center col-lg-12 col-md-12 col-sm-12 col-xs-12\">\n" +
    "        <div id='div_view_all_resto'>\n" +
    "            <a analytics-on=\"click\" analytics-event=\"c_home_view_all1\" analytics-category=\"home_view_all1\" href=\"search/singapore\" ng-bind=\"Str.homepage.ViewAllRestaurant\" class=\"ng-binding\">View all restaurants in Singapore</a>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</section>\n" +
    "\n" +
    "<section id=\"blog\" ng-if='isShowTag'>\n" +
    "    <div class=\"container\">\n" +
    "        <h2 class=\"text-center\" style=\"margin-bottom: 48px;\">Daily Special Board</h2>\n" +
    "        \n" +
    "        <center>\n" +
    "            <div class=\"col-md-10 col-md-push-1 col-sm-10 col-sm-push-1 col-xs-12\" >\n" +
    "                <a href=\"dailyspecials\" style=\"background: 0 0;font-size: 14px;padding: 7px 20px;border-radius: 32px;border: 1px solid #fff;color: #fff;font-family: proximanova;cursor: pointer;text-decoration: none;\">\n" +
    "                    + <span>more daily specials</span>\n" +
    "                </a>\n" +
    "            </div>\n" +
    "        </center>\n" +
    "\n" +
    "\n" +
    "    </div>\n" +
    "</section>\n" +
    "<section class=\"early_book_restaurant\">\n" +
    "    <div class=\"ctn\">\n" +
    "        <div id=\"best-restaurant\" class=\"container\">\n" +
    "            <h1 class=\"title_home_page text-center ng-binding\">Book a table and win instant offers</h1>\n" +
    "            <h2 class=\"sub_title_home_page text-center ng-binding\">Free Restaurants Booking & Reservation - Instant confirmation</h2>\n" +
    "            <div class=\"col-xs-12 col-sm-4\" ng-repeat=\"category in Categories\">\n" +
    "                <restaurant-category-item open-in-new-tab=\"true\" media-server=\"mediaServer\" category=\"category\"></restaurant-category-item>\n" +
    "            </div>\n" +
    "            <div class=\"more-restaurant col-md-10 col-md-push-1 col-sm-10 col-sm-push-1 col-xs-12\"><a ng-href=\"search/singapore\" id=\"more-restau\">+ <span ng-bind=\"Str.homepage.MoreRestaurant\">more restaurants</span></a></div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</section>\n" +
    "<section id=\"popular\">\n" +
    "    <div class=\"container\">\n" +
    "        <div class=\"header\">\n" +
    "            <h2 class=\"text-center\">Popular Cuisines</h2>\n" +
    "            <p class=\"text-center\">Flavours from around the world</p>\n" +
    "        </div>\n" +
    "        <div class=\"col-xs-12\">\n" +
    "            <ul>\n" +
    "                <li class=\"col-md-3 col-sm-3\">\n" +
    "                    <a ng-href=\"/search/singapore/cuisine/buffet\"><img width=\"279\" height=\"177\" src=\"client/assets/images/popular/image.png\" class=\"img-responsive\" style=\"background-image: url('client/assets/images/popular/img1.jpg'); background-position: center;background-size:cover;\">\n" +
    "                        <p class=\"hover text-center\">BUFFET</p></a>\n" +
    "                </li>\n" +
    "                <li class=\"col-md-3 col-sm-3\">\n" +
    "                    <a ng-href=\"/search/singapore/cuisine/brunch\"><img width=\"279\" height=\"177\" src=\"client/assets/images/popular/image.png\" class=\"img-responsive\" style=\"background-image: url('client/assets/images/popular/img2.jpg'); background-position: center;background-size:cover;\">\n" +
    "                    <p class=\"hover text-center\">BRUNCH</p></a>\n" +
    "                </li>\n" +
    "                <li class=\"col-md-3 col-sm-3\">\n" +
    "                    <a ng-href=\"/search/singapore/cuisine/chinese\"><img width=\"279\" height=\"177\" src=\"client/assets/images/popular/image.png\" class=\"img-responsive\" style=\"background-image: url('client/assets/images/popular/img3.jpg'); background-position: center;background-size:cover;\">\n" +
    "                    <p class=\"hover text-center\">CHINESE CUISINE</p></a>\n" +
    "                </li>\n" +
    "                <li class=\"col-md-3 col-sm-3\">\n" +
    "                    <a ng-href=\"/search/singapore/cuisine/japanese\"><img width=\"279\" height=\"177\" src=\"client/assets/images/popular/image.png\" class=\"img-responsive\" style=\"background-image: url('client/assets/images/popular/img4.jpg'); background-position: center;background-size:cover;\">\n" +
    "                    <p class=\"hover text-center\">JAPANESE CUISINE</p></a>\n" +
    "                </li>\n" +
    "                <li class=\"col-md-3 col-sm-3\">\n" +
    "                    <a ng-href=\"/search/singapore/cuisine/italian\"><img width=\"279\" height=\"177\" src=\"client/assets/images/popular/image.png\" class=\"img-responsive\" style=\"background-image: url('client/assets/images/popular/img5.jpg'); background-position: center;background-size:cover;\">\n" +
    "                    <p class=\"hover text-center\">ITALIAN CUISINE</p></a>\n" +
    "                </li>\n" +
    "                <li class=\"col-md-3 col-sm-3\">\n" +
    "                    <a ng-href=\"/search/singapore/cuisine/french\"><img width=\"279\" height=\"177\" src=\"client/assets/images/popular/image.png\" class=\"img-responsive\" style=\"background-image: url('client/assets/images/popular/img6.jpg'); background-position: center;background-size:cover;\">\n" +
    "                    <p class=\"hover text-center\">FRENCH CUISINE</p></a>\n" +
    "                </li>\n" +
    "                <li class=\"col-md-3 col-sm-3\">\n" +
    "                    <a ng-href=\"/search/singapore/cuisine/western\"><img width=\"279\" height=\"177\" src=\"client/assets/images/popular/image.png\" class=\"img-responsive\" style=\"background-image: url('client/assets/images/popular/img7.jpg'); background-position: center;background-size:cover;\">\n" +
    "                    <p class=\"hover text-center\">WESTERN CUISINE</p></a>\n" +
    "                </li>\n" +
    "                <li class=\"col-md-3 col-sm-3\">\n" +
    "                    <a ng-href=\"/search/singapore/cuisine/bar\"><img width=\"279\" height=\"177\" src=\"client/assets/images/popular/image.png\" class=\"img-responsive\" style=\"background-image: url('client/assets/images/popular/img8.jpg'); background-position: center;background-size:cover;\">\n" +
    "                    <p class=\"hover text-center\">BARS & LOUNGES</p></a>\n" +
    "                </li>\n" +
    "            </ul>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</section>\n" +
    "<section id=\"amazing\">\n" +
    "    <div class=\"container\">\n" +
    "        <h2 class=\"text-center\">Amazing deals to share</h2>\n" +
    "        <div>\n" +
    "            <div class=\"zen-amazing\">\n" +
    "                <div class=\"col-xs-12 col-sm-3\" ng-repeat=\"event in events\">\n" +
    "                    <restaurant-event-item open-in-new-tab=\"true\" media-server=\"mediaServer\" event=\"event\"></restaurant-event-item>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"more-restaurant text-center col-md-12\">\n" +
    "            <a ng-href=\"allevents\" id=\"more-restau\">+ <span>see more deals</span></a>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</section>\n" +
    "<section class=\"zen-month\">\n" +
    "    <div class=\"container\">\n" +
    "        <h2 class=\"text-center\">Featured restaurants</h2>\n" +
    "        <p class=\"text-center zen-text\">Selection of the month</p>\n" +
    "        <div class=\"css-creen\">\n" +
    "            <div class=\"col-md-4 col-sm-4 col-xs-12 zen-column\" ng-repeat=\"restaurant in TopRestaurants track by $index\">\n" +
    "                <restaurant-item open-in-new-tab=\"true\" media-server=\"mediaServer\" restaurant=\"restaurant\" indexcount=\"$index\"></restaurant-item>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</section>\n" +
    "<section id=\"blog\">\n" +
    "    <div class=\"container\">\n" +
    "        <h2 class=\"text-center\">What's hot on the blog</h2>\n" +
    "        <p class=\"blog-text text-center\">Read & Book</p>\n" +
    "        <div class=\"col-md-12 col-sm-12 col-xs-12\">\n" +
    "            <div class=\"col-md-4 col-sm-4 col-xs-12 blog-col\" ng-repeat=\"article in blogarticles\">\n" +
    "                <restaurant-article-item open-in-new-tab=\"true\" media-server=\"mediaServer\" article=\"article\"></restaurant-article-item>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</section>\n" +
    "<section id=\"zen-location\" class=\"row container\">\n" +
    "    <div class=\"col-md-12 col-sm-12 col-xs-12 \">\n" +
    "        <ul>\n" +
    "            <li class=\"col-md-4 col-sm-4 col-xs-12\">\n" +
    "                <a ng-href=\"/search/singapore\"><img width=\"37\" height=\"21\" class=\"img-responsive\" src=\"client/assets/images/37x21.png\" style=\"background-image: url('client/assets/images/location/img1.jpg'); background-position: center;background-size: cover;\">\n" +
    "                    <p class=\"hover\">SINGAPORE</p>\n" +
    "                </a>\n" +
    "            </li>\n" +
    "            <li class=\"col-md-4 col-sm-4 col-xs-12\">\n" +
    "                <a ng-href=\"/search/bangkok\"><img width=\"37\" height=\"21\" class=\"img-responsive\" src=\"client/assets/images/37x21.png\" style=\"background-image: url('client/assets/images/location/img2.jpg'); background-position: center;background-size: cover;\">\n" +
    "                    <p class=\"hover\">BANGKOK</p>\n" +
    "                </a>\n" +
    "            </li>\n" +
    "            <li class=\"col-md-4 col-sm-4 col-xs-12\">\n" +
    "                <a ng-href=\"/search/phuket\"><img width=\"37\" height=\"21\" class=\"img-responsive\" src=\"client/assets/images/37x21.png\" style=\"background-image: url('client/assets/images/location/img3.jpg'); background-position: center;background-size: cover;\">\n" +
    "                    <p class=\"hover\">PHUKET</p>\n" +
    "                </a>\n" +
    "            </li>\n" +
    "        </ul>\n" +
    "    </div>\n" +
    "</section>\n" +
    "<script>\n" +
    "(function() {\n" +
    "\n" +
    "    function init() {\n" +
    "        var speed = 250,\n" +
    "            easing = mina.easeinout;\n" +
    "\n" +
    "        [].slice.call(document.querySelectorAll('#grid > a')).forEach(function(el) {\n" +
    "            var s = Snap(el.querySelector('svg')),\n" +
    "                path = s.select('path'),\n" +
    "                pathConfig = {\n" +
    "                    from: path.attr('d'),\n" +
    "                    to: el.getAttribute('data-path-hover')\n" +
    "                };\n" +
    "\n" +
    "            el.addEventListener('mouseenter', function() {\n" +
    "                path.animate({\n" +
    "                    'path': pathConfig.to\n" +
    "                }, speed, easing);\n" +
    "            });\n" +
    "\n" +
    "            el.addEventListener('mouseleave', function() {\n" +
    "                path.animate({\n" +
    "                    'path': pathConfig.from\n" +
    "                }, speed, easing);\n" +
    "            });\n" +
    "        });\n" +
    "\n" +
    "        setTimeout(lanchVideo, 2000);\n" +
    "\n" +
    "    }\n" +
    "\n" +
    "    function lanchVideo() {\n" +
    "        var v = document.getElementsByTagName(\"video\")[0];\n" +
    "        v.play();\n" +
    "    }\n" +
    "    init();\n" +
    "\n" +
    "})();\n" +
    "</script>\n" +
    "");
}]);

angular.module("../app/components/home/_newhome.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/components/home/_newhome.tpl.html",
    "<section class=\"header-container\" style=\"background-image: url('images/banner_1728x450.jpg'); background-position: center;background-size:cover;\">\n" +
    "    <div class=\"custom-new-search-bar container\">\n" +
    "        <nf-search class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\" hide-input-on-mobile=\"true\" placeholder=\"Str.template.SearchInputPlaceHolder\" button-text=\"Str.template.SearchBtnText\" cities=\"cities\"></nf-search>\n" +
    "    </div>\n" +
    "        \n" +
    "      \n" +
    "<!--    <div class=\"view-all-restaurant text-center col-lg-12 col-md-12 col-sm-12 col-xs-12\">\n" +
    "        <div id='div_view_all_resto'>\n" +
    "            <a analytics-on=\"click\" analytics-event=\"c_home_view_all1\" analytics-category=\"home_view_all1\" href=\"search/singapore\"  class=\"ng-binding\">View all restaurants in Singapore ads  {{UserSession.search_city}}</a>\n" +
    "        </div>\n" +
    "    </div>-->\n" +
    "\n" +
    "        <div class=\"header-text text-center col-lg-12 col-md-12 col-sm-12 col-xs-12\">\n" +
    "            <h5 >{{headertitle}}</h5>\n" +
    "            <span  >{{UserSession.search_city}}</span>\n" +
    "        </div>\n" +
    " \n" +
    "</section>\n" +
    "    <section class ='early_book_section'>\n" +
    "<!--    <div class=\"ctn\">-->\n" +
    "        <div id=\"section_first\" class=\"container\" >\n" +
    "            <div class=\"header\">\n" +
    "            <h3 class=\"text-center\">Book a table and win instant offers</h3>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"col-xs-12\">\n" +
    "            <div  ng-repeat=\"category in Categories\" style=' padding:0px;'>\n" +
    "                <restaurant-category-itemnew open-in-new-tab=\"true\" media-server=\"mediaServer\" category=\"category\"></restaurant-category-itemnew>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        </div>\n" +
    "    </section>\n" +
    "<section id=\"popular-section\">\n" +
    "    <div class=\"container\">\n" +
    "        <div class=\"header\">\n" +
    "            <h3 class=\"text-center\">Popular Cuisines - Flavours from around the world</h3>\n" +
    "        </div>\n" +
    "        <div class=\"col-xs-12\">\n" +
    "            <div class=\"col-md-3 col-sm-3\">\n" +
    "               <a ng-href=\"/search/singapore/cuisine/buffet\"><img  src=\"client/assets/images/popular/image.png\" class=\"img-responsive\" style=\"background-image: url('client/assets/images/popular/img1.jpg'); background-position: center;background-size:cover;\">\n" +
    "                   <p class=\"hover text-center\">BUFFET</p></a>\n" +
    "           </div>\n" +
    "           <div class=\"col-md-3 col-sm-3\">\n" +
    "               <a ng-href=\"/search/singapore/cuisine/brunch\"><img  src=\"client/assets/images/popular/image.png\" class=\"img-responsive\" style=\"background-image: url('client/assets/images/popular/img2.jpg'); background-position: center;background-size:cover;\">\n" +
    "               <p class=\"hover text-center\">BRUNCH</p></a>\n" +
    "           </div>\n" +
    "           <div class=\"col-md-3 col-sm-3\">\n" +
    "               <a ng-href=\"/search/singapore/cuisine/chinese\"><img  src=\"client/assets/images/popular/image.png\" class=\"img-responsive\" style=\"background-image: url('client/assets/images/popular/img3.jpg'); background-position: center;background-size:cover;\">\n" +
    "               <p class=\"hover text-center\">CHINESE CUISINE</p></a>\n" +
    "           </div>\n" +
    "           <div class=\"col-md-3 col-sm-3\">\n" +
    "               <a ng-href=\"/search/singapore/cuisine/japanese\"><img src=\"client/assets/images/popular/image.png\" class=\"img-responsive\" style=\"background-image: url('client/assets/images/popular/img4.jpg'); background-position: center;background-size:cover;\">\n" +
    "               <p class=\"hover text-center\">JAPANESE CUISINE</p></a>\n" +
    "           </div>\n" +
    "           <div class=\"col-md-3 col-sm-3\">\n" +
    "               <a ng-href=\"/search/singapore/cuisine/italian\"><img  src=\"client/assets/images/popular/image.png\" class=\"img-responsive\" style=\"background-image: url('client/assets/images/popular/img5.jpg'); background-position: center;background-size:cover;\">\n" +
    "               <p class=\"hover text-center\">ITALIAN CUISINE</p></a>\n" +
    "           </div>\n" +
    "           <div class=\"col-md-3 col-sm-3\">\n" +
    "               <a ng-href=\"/search/singapore/cuisine/french\"><img  src=\"client/assets/images/popular/image.png\" class=\"img-responsive\" style=\"background-image: url('client/assets/images/popular/img6.jpg'); background-position: center;background-size:cover;\">\n" +
    "               <p class=\"hover text-center\">FRENCH CUISINE</p></a>\n" +
    "           </div>\n" +
    "           <div class=\"col-md-3 col-sm-3\">\n" +
    "               <a ng-href=\"/search/singapore/cuisine/western\"><img  src=\"client/assets/images/popular/image.png\" class=\"img-responsive\" style=\"background-image: url('client/assets/images/popular/img7.jpg'); background-position: center;background-size:cover;\">\n" +
    "               <p class=\"hover text-center\">WESTERN CUISINE</p></a>\n" +
    "           </div>\n" +
    "           <div class=\"col-md-3 col-sm-3\">\n" +
    "               <a ng-href=\"/search/singapore/cuisine/bar\"><img  src=\"client/assets/images/popular/image.png\" class=\"img-responsive\" style=\"background-image: url('client/assets/images/popular/img8.jpg'); background-position: center;background-size:cover;\">\n" +
    "               <p class=\"hover text-center\">BARS & LOUNGES</p></a>\n" +
    "                </li>\n" +
    "            </ul>\n" +
    "           </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</section>\n" +
    "<!--<section id=\"amazing-section\">\n" +
    "    <div class=\"container\">\n" +
    "        <div class=\"header\">\n" +
    "            <h3 class=\"text-center\">Amazing deals to share</h3>\n" +
    "        </div>\n" +
    "        <div>\n" +
    "            <div class=\"event-amazing\">\n" +
    "                <div class=\"col-xs-12 col-md-12\">\n" +
    "                    <div ng-repeat=\"event in events\">\n" +
    "                    <restaurant-event-itemnew open-in-new-tab=\"true\" media-server=\"mediaServer\" event=\"event\"></restaurant-event-itemnew>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"more-restaurant text-center col-md-12\">\n" +
    "            <a ng-href=\"allevents\" id=\"more-restau\">+ <span>see more deals</span></a>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</section>-->\n" +
    "\n" +
    "\n" +
    "\n" +
    "");
}]);

angular.module("../app/components/how_it_work_page/how_it_work.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/components/how_it_work_page/how_it_work.tpl.html",
    "<style>\n" +
    "    \n" +
    ".container3 {\n" +
    "    position: relative;\n" +
    "    width: 100%;\n" +
    "    height: 0;\n" +
    "    padding-bottom: 56.25%;\n" +
    "}\n" +
    ".youtube_video {\n" +
    "    position: absolute;\n" +
    "    top: 0;\n" +
    "    left: 0;\n" +
    "    width: 100%;\n" +
    "    height: 100%;\n" +
    "}\n" +
    "    \n" +
    "</style>\n" +
    "\n" +
    "\n" +
    "<div id=\"how-it-works\">\n" +
    "\n" +
    "\n" +
    "    <section class=\"how-it-work\" style=\"z-index:115;\">\n" +
    "        <div class='container'>\n" +
    "\n" +
    "            <div class=\"container3\" style='margin-top: 40px;margin-bottom: 40px;'>\n" +
    "                    <iframe src=\"https://www.youtube.com/embed/nqfDGTuEHhY?rel=0&autoplay=1\" class=\"youtube_video\" frameborder=\"0\" allowfullscreen></iframe>\n" +
    "            </div>\n" +
    "\n" +
    "            <h1 class=\"title_info_page text-center\">How it works</h1>\n" +
    "            <h2 class=\"sub_title_home_page text-center\">In 2 easy steps. Book & Spin to Win</h2>\n" +
    "            \n" +
    "            \n" +
    "            \n" +
    "            \n" +
    "            <div class=\"row how-image\" style='text-align: center;margin-bottom: 50px;background-color: #013440; color:white;'>\n" +
    "                <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\" style=\" margin-top: 30px;\">\n" +
    "                    <img src=\"https://static2.weeloy.com/images/how-it-works/find.png\" alt=\"weeloy find select reataurant\">\n" +
    "                    <!--<img width=\"350\" height=\"193\" class=\"icon-find\" src=\"https://static2.weeloy.com/images/sprites/transparent.png\" alt=\"weeloy find select reataurant\">-->\n" +
    "                    <p class=\"title\">BOOK on weeloy.com or the app</p>\n" +
    "                    <br>\n" +
    "                </div>\n" +
    "                <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\" style=\" margin-top: 30px;\">\n" +
    "                    <img src=\"https://static2.weeloy.com/images/how-it-works/win.png\" alt=\"weeloy confirm booking\">\n" +
    "                    <!--<img width=\"350\" height=\"193\" class=\"icon-spin\"  src=\"https://static2.weeloy.com/images/sprites/transparent.png\" alt=\"weeloy confirm booking\">-->\n" +
    "                    <p class=\"title\" >Spin the wheel at the restaurant & WIN</p>\n" +
    "                    <br>\n" +
    "                </div>\n" +
    "<!--                <div class=\"col-lg-4 col-md-4 col-sm-4 col-xs-12\" style=\" margin-top: 30px;\">\n" +
    "                    <img src=\"https://static2.weeloy.com/images/how-it-works/win.png\" alt=\"weeloy get discount\">\n" +
    "                    <img width=\"350\" height=\"193\" class=\"icon-win\"  src=\"https://static2.weeloy.com/images/sprites/transparent.png\" alt=\"weeloy get discount\">\n" +
    "                    <p class=\"title\">Win an offer instantly</p>\n" +
    "                    <br>\n" +
    "                </div>-->\n" +
    "            </div>\n" +
    "\n" +
    "\n" +
    "            <!--<div class=\"more-info\"><a href=\"how-it-works\" id=\"more-hiw\">+ <span ng-bind=\"Str.homepage.MoreInfo\"></span></a></div>-->\n" +
    "        </div>\n" +
    "    </section>\n" +
    "\n" +
    "\n" +
    "\n" +
    "    <section>\n" +
    "        <div class=\"container\" style=\"margin-top:40px;margin-bottom:40px;\">\n" +
    "            <div class=\"row row-description\" style=\"margin-bottom:40px;\">\n" +
    "                <div class=\"col-sm-4\" style='text-align:center; margin-bottom:40px;'>\n" +
    "                    <img src=\"https://static2.weeloy.com/images/how-it-works/find.png\" style=\"border: 1px solid #2aa8cf;max-width:200px; max-height:200px;padding: 10px;background: #013440\" />\n" +
    "                </div>\n" +
    "                <div class=\"col-sm-8\">\n" +
    "                    <h2 class='sub_title_info_page'>1. BOOK A RESTAURANT</h2>\n" +
    "                    <p class=\"decription\">Book your restaurant on Weeloy.com or on the mobile App. You will receive an instant confirmation by email or SMS.</p>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"row row-description\" style=\"margin-bottom:40px;\">\n" +
    "                <div class=\"col-sm-4\" style='text-align:center; margin-bottom:40px;'>\n" +
    "                    <img src=\"https://static2.weeloy.com/images/how-it-works/win.png\" style=\"border: 1px solid #2aa8cf;max-width:200px; max-height:200px; padding: 10px;background: #013440\" />\n" +
    "                </div>\n" +
    "                <div class=\"col-sm-8\">\n" +
    "                    <h2 class='sub_title_info_page'>2. SPIN THE WHEEL AND WIN A REWARD INSTANTLY</h2>\n" +
    "                    <p class=\"decription\">Download the Weeloy App on your mobile. Upon arrival at the restaurant, open the Weeloy app and retrieve your reservation from \"Your bookings\" in the app Menu. Use the \"Spin the Wheel\" button, and let the staff of the restaurant enter the restaurant code.</p>\n" +
    "                    <p class=\"decription\">Win one of the offers of the Wheel and enjoy your dining! With the Wheel, you win an Offer everytime you book.</p>\n" +
    "                    <h2 class='sub_title_info_page' style=\"transform: none; font-variant: none;\">The Higher the Rewards Rating the Better the offers on the restaurant Wheel.</h2>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "<!--            <div class=\"row row-description\" style=\"margin-bottom:40px;\">\n" +
    "                <div class=\"col-sm-4\" style='text-align:center; margin-bottom:40px;'>\n" +
    "                    <img src=\"https://static2.weeloy.com/images/how-it-works/win.png\" style=\"border: 1px solid #2aa8cf;max-width:200px; max-height:200px;\" />\n" +
    "                </div>\n" +
    "                <div class=\"col-sm-8\">\n" +
    "                    <h2 class='sub_title_info_page'>3. WIN A REWARD INSTANTLY</h2>\n" +
    "                    <p class=\"decription\">Win one of the offers of the Wheel and enjoy your dining! With the Wheel, you win an Offer everytime you book.</p>\n" +
    "                    <h2 class='sub_title_info_page' style=\"transform: none; font-variant: none;\">The Higher the Rewards Rating the Better the offers on the restaurant Wheel.</h2>\n" +
    "                </div>\n" +
    "            </div>-->\n" +
    "            <div class=\"row row-description\">\n" +
    "                <div class=\"col-sm-4\">\n" +
    "                </div>\n" +
    "                <div class=\"col-sm-8\">\n" +
    "                    <a class=\"btn btn-bitbucket\" style=\"background-color: #2aabd2;padding: 10px 30px;\" href=\"search\">View Restaurants</a>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </section>\n" +
    "</div>\n" +
    "");
}]);

angular.module("../app/components/info_contact_page/_info_contact_page.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/components/info_contact_page/_info_contact_page.tpl.html",
    "<div id=\"contact-page\">\n" +
    "    <div class=\"container\">\n" +
    "        <div class=\"contact-info col-lg-7 col-lg-offset-1 col-md-7 col-md-offset-1 col-sm-8 col-xs-12\">\n" +
    "            <div class=\"col-lg-8 col-md-8 col-sm-10 col-xs-12\">\n" +
    "                <h3>Your feedback is important to us.</h3>\n" +
    "                <div class=\"info\">\n" +
    "                    <p>Please use the form for any question. We would be happy to assist you.</p>\n" +
    "                    <p>To easily find your answers, please check the FAQ's page.</p>\n" +
    "                </div>\n" +
    "                <div class=\"address\">\n" +
    "                    <p>Contact information:</p>\n" +
    "                    <p><strong>Weeloy Pte Ltd</strong></p>\n" +
    "                    <p>83 Amoy Street</p>\n" +
    "                    <p>Singapore 069902</p>\n" +
    "                    <!--<p>E-mail: info@weeloy.com</p>-->\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"contact-form col-lg-4 col-md-4 col-sm-4 col-xs-12\">\n" +
    "            <p class=\"contact-form-title\">Contact form</p>\n" +
    "            <form method=\"post\" name=\"ContactForm\" ng-submit=\"ContactForm.$valid && ContactSubmit(contact)\" novalidate>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <input type=\"text\" name=\"firstname\" ng-model=\"contact.firstname\" placeholder=\"First name\" ng-pattern=\"/^[a-z ,.'-]+$/i\" required>\n" +
    "                    <div class=\"error\">\n" +
    "                        <span ng-show=\"ContactForm.$submitted && ContactForm.firstname.$error.required\">Please enter your first name.</span>\n" +
    "                        <span ng-show=\"ContactForm.$submitted && ContactForm.firstname.$error.pattern\">First name must be alpha only.</span>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <input type=\"text\" name=\"lastname\" ng-model=\"contact.lastname\" placeholder=\"Last name\" ng-pattern=\"/^[a-z ,.'-]+$/i\" maxlength=\"40\" required>\n" +
    "                    <div class=\"error\">\n" +
    "                        <span ng-show=\"ContactForm.$submitted && ContactForm.lastname.$error.required\">Please enter your last name.</span>\n" +
    "                        <span ng-show=\"ContactForm.$submitted && ContactForm.lastname.$error.pattern\">Last name must be alpha only.</span>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <input type=\"email\" name=\"email\" ng-model=\"contact.email\" placeholder=\"Email address\" maxlength=\"40\" ng-pattern=\"/^([\\w-]+(?:\\.[\\w-]+)*)@((?:[\\w-]+\\.)*\\w[\\w-]{0,66})\\.([a-z]{2,6}(?:\\.[a-z]{2})?)$/i\" required>\n" +
    "                    <div class=\"error\">\n" +
    "                        <span ng-show=\"ContactForm.$submitted && ContactForm.email.$error.required\">Please enter your email.</span>\n" +
    "                        <span ng-show=\"ContactForm.$submitted && ContactForm.email.$error.pattern\">Please enter a valid email.</span>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <select name=\"city\" ng-model=\"contact.city\" ng-options=\"city as city.name for city in cities\"></select>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <input type=\"text\" name=\"phone\" ng-model=\"contact.phone\" placeholder=\"Phone\" ng-pattern=\"/^[0-9\\+| ]{6,20}/\" maxlength=\"600\" required>\n" +
    "                    <div class=\"error\">\n" +
    "                        <span ng-show=\"ContactForm.$submitted && ContactForm.phone.$error.required\">Please enter your phone number.</span>\n" +
    "                        <span ng-show=\"ContactForm.$submitted && ContactForm.phone.$error.pattern\">Please enter a valid phone number.</span>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <textarea ng-model=\"contact.message\" name=\"message\" placeholder=\"Write your message here. We will get back to you within 2 business days.\" required></textarea>\n" +
    "                    <div class=\"error\">\n" +
    "                        <span ng-show=\"ContactForm.$sumitted && ContactForm.message.$error.required\">Please enter a message.</span>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <input type=\"submit\" value=\"Submit\">\n" +
    "                </div>\n" +
    "            </form>\n" +
    "            <div class=\"alert alert-success alert-dismissible\" role=\"alert\" ng-show=\"showSuccessMessage\">\n" +
    "                <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>\n" +
    "                <span>Your message has been sent!</span>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("../app/components/info_customer_page/_info_customer.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/components/info_customer_page/_info_customer.tpl.html",
    "<style>\n" +
    "     \n" +
    "\n" +
    "</style>\n" +
    "<section class ='early_book_section' style='margin-top:60px;'>\n" +
    "<div  class=\"landing-page\" style='background-color: white !important' >\n" +
    "<!--    <div class=\"container\">\n" +
    "        <div class=\"row\">\n" +
    "                <img class='banner-img img-responsive' src=\"{{pageheader.images}}\" alt=\"\"/>\n" +
    "            </div>\n" +
    "          \n" +
    "    </div>-->\n" +
    "  <div class=\"dsbbannerholder\">\n" +
    "      <div class=\"dsbbannerimg\" style=\"background-image: url({{ pageheader.images}})\">\n" +
    "          <div class = \"titleboard\">\n" +
    "          </div>\n" +
    "        </div>  \n" +
    "    </div>\n" +
    "    <section id=\"amazing\" style='background-color:white!important;margin-left:20px;margin-right:20px;'>\n" +
    "        <div class=\"container\">\n" +
    "            <p style=\"font-size:20px;\" class=\"text-center\">{{pageheader.title}} </p>\n" +
    "            <p ng-if='pageheader.subtitle' style=\"font-size:23px;margin-top:10px;text-align:center;margin-bottom: 20px;\n" +
    "                     padding-bottom: 10px;\">{{pageheader.subtitle}}</p>\n" +
    "            <p>{{pageheader.description}}</p>\n" +
    "            \n" +
    "            <div class=\"col-md-6 col-sm-4\" ng-repeat=\"item in promotions\">\n" +
    "                <div class ='col-md-12'>\n" +
    "                 <div class=\"headline\">\n" +
    "                    <strong> {{item.title}}</strong>\n" +
    "                </div>\n" +
    "                <div class=\"promoimages\">\n" +
    "                     <a ng-href=\"{{item.link }}\" target=\"_blank\">\n" +
    "                        <img ng-src=\"images/transparent.png\" Title= \"{{item.title}}\" style='width:100%;background: url(\"{{item.image}}\");background-position: center;background-repeat: no-repeat;background-size: cover;s'>\n" +
    "                     </a>\n" +
    "                </div>\n" +
    "                 <div class=\"headline\">\n" +
    "                   {{item.description}}\n" +
    "                </div>\n" +
    "                <div class=\"cbtnholder\">\n" +
    "                     <a ng-href=\"{{ item.link }}\" target=\"_blank\">\n" +
    "                    <center>\n" +
    "                        <input class=\"input-group-addon book-button-sm2 btn-leftBottom-blue-noborder\" type=\"button\" name=\"\" value=\"Book Now\">\n" +
    "                    </center>\n" +
    "                     </a>\n" +
    "                </div>\n" +
    "             \n" +
    "            </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "\n" +
    "    </section>\n" +
    "\n" +
    "\n" +
    "</div>\n" +
    "</section>\n" +
    "");
}]);

angular.module("../app/components/info_partner_page/_info_partner.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/components/info_partner_page/_info_partner.tpl.html",
    "<div id=\"partner-page\">\n" +
    "    <div class=\"contact-us-section\">\n" +
    "        <div class=\"container\">\n" +
    "            <div class=\"col-lg-12 col-sm-12 col-md-12 col-xs-12\">\n" +
    "                <h1 class=\"text-center\">Your technology and eMarketing partner</h1>\n" +
    "                <div class=\"info\">\n" +
    "                    <p class=\"text-center\">Increase your market reach and online visibility,</p>\n" +
    "                    <p class=\"text-center\">gain new and repeative customers and</p>\n" +
    "                    <p class=\"text-center\">protect your brand with our very efficient, innovative,</p>\n" +
    "                    <p class=\"text-center\">and white label solutions.</p>\n" +
    "                </div>\n" +
    "                <div class=\"contact-us-btn text-center\">\n" +
    "                    <button ng-click=\"showPartnerContactSection = true\">Contact us</button>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    \n" +
    "    <div class=\"partner-contact-section\" id=\"partner-contact-section\" ng-show=\"showPartnerContactSection\">\n" +
    "        <div class=\"container\">\n" +
    "            <div class=\"solution-text-3 col-lg-12 col-sm-12 col-md-12 col-xs-12\">\n" +
    "                <p class=\"text-center\">Weeloy Pte Ltd</p>\n" +
    "                <p class=\"text-center\">83 Amoy Street</p>\n" +
    "                <p class=\"text-center\">Singapore 069902e</p>\n" +
    "            </div>\n" +
    "            <div class=\"solution-text-1 col-lg-12 col-sm-12 col-md-12 col-xs-12\">\n" +
    "                <p class=\"text-center\">\n" +
    "                    <a href=\"mailto:partner@weeloy.com?subject=I am interested in your partner solution\">partner@weeloy.com</a>\n" +
    "                    <noscript>\n" +
    "                        &lt;em&gt;Email address protected by JavaScript. Activate javascript to see the email.&lt;/em&gt;\n" +
    "                    </noscript>\n" +
    "                </p>\n" +
    "            </div>\n" +
    "            <div class=\"solution-text-3 col-lg-12 col-sm-12 col-md-12 col-xs-12\">\n" +
    "                <p class=\"text-center\">Singapore: +65 9025 3065</p>\n" +
    "                <p class=\"text-center\">Thailand: +66 81 8943467</p>\n" +
    "                <p class=\"text-center\">Hong Kong: +85 2 90350906</p>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div class=\"solution-section\">\n" +
    "        <div class=\"container\">\n" +
    "            <div class=\"solution-title\">\n" +
    "                <p class=\"text-center\">Streamline your business with a full suite</p>\n" +
    "                <p class=\"text-center\">of integrated and proprietary products</p>\n" +
    "            </div>\n" +
    "            <div class=\"solution-text col-lg-12 col-md-12 col-sm-12 col-xs-12\">\n" +
    "                <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">\n" +
    "                    <p><span class=\"fa fa-check-circle\"></span>a web and mobile reservation portal</p>\n" +
    "                    <p><span class=\"fa fa-check-circle\"></span>a white label Reservation System</p>\n" +
    "                </div>\n" +
    "                <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">\n" +
    "                    <p><span class=\"fa fa-check-circle\"></span>a robust Call Center software</p>\n" +
    "                    <p><span class=\"fa fa-check-circle\"></span>a state-of-the-art Table Management software</p>\n" +
    "                </div>\n" +
    "                <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">\n" +
    "                    <p><span class=\"fa fa-check-circle\"></span>a powerful Catering and On-line Ordering software</p>\n" +
    "                    <!--<p><span class=\"fa fa-check-circle\"></span></p>-->\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"solution-image\">\n" +
    "            <div class=\"container\">\n" +
    "                <div class=\"text-center col-lg-8 col-md-8 col-sm-10 col-xs-12 col-lg-offset-2 col-md-offset-2 col-sm-offset-1\">\n" +
    "                    <img src=\"images/partner_page/partner-solution-mac.png\">\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"solution-text-2\">\n" +
    "            <div class=\"container\">\n" +
    "                <p class=\"text-center\">All our solutions are centralized into a powerful CRM for business reports,</p>\n" +
    "                <p class=\"text-center\">data analysis and marketing operations.</p>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"solution-text-3\">\n" +
    "            <div class=\"container\">\n" +
    "                <p class=\"text-center\">And beyond products and technology, we believe in exceptional client</p>\n" +
    "                <p class=\"text-center\">service. You will be supported by a professional team responsive to your </p>\n" +
    "                <p class=\"text-center\">needs and insightful about the challenges you face.</p>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div class=\"consumer-section\">\n" +
    "        <div class=\"container\">\n" +
    "            <div class=\"consumer-title\">\n" +
    "                <p class=\"text-center\">Online reservation portal and</p>\n" +
    "                <p class=\"text-center\">consumer mobile app for your guests</p>\n" +
    "            </div>\n" +
    "            <div class=\"consumer-text\">\n" +
    "                <p class=\"text-center\">Our consumer portal, Weeloy.com is a dynamic on-line reservation portal </p>\n" +
    "                <p class=\"text-center\">and a consumer mobile app providing consumers a fun dining experience and </p>\n" +
    "                <p class=\"text-center\">an easy way to search and book while giving you a new way to optimize your </p>\n" +
    "                <p class=\"text-center\">margin, and generate additional business and enhance the customer value </p>\n" +
    "                <p class=\"text-center\">proposition.</p>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div class=\"feature-section\">\n" +
    "        <div class=\"container\">\n" +
    "            <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12 feature-title\">\n" +
    "                <p class=\"text-center\">Your dedicated restaurant page on weeloy.com</p>\n" +
    "            </div>\n" +
    "            <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12 feature-image text-center\">\n" +
    "                <img src=\"images/partner_page/ipad-feature-section.png\" alt=\"\">\n" +
    "            </div>\n" +
    "            <div class=\"feature-text col-lg-12 col-md-12 col-sm-12 col-xs-12\">\n" +
    "                <p class=\"text-center feature-text-title\">features:</p>\n" +
    "                <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">\n" +
    "                    <p><span class=\"fa fa-check-circle\"></span>Rich content as a video of your signature dishes</p>\n" +
    "                    <p><span class=\"fa fa-check-circle\"></span>High resolution photo gallery</p>\n" +
    "                </div>\n" +
    "                <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">\n" +
    "                    <p><span class=\"fa fa-check-circle\"></span>A clear menu proposition</p>\n" +
    "                    <p><span class=\"fa fa-check-circle\"></span>A description of your main events and all services provided by your restaurant</p>\n" +
    "                </div>\n" +
    "                <div class=\"feature-text-last col-lg-12 col-md-12 col-sm-12 col-xs-12\">\n" +
    "                    <p class=\"text-center\"> </p>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("../app/components/mother_day_page/_mother_day_page.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/components/mother_day_page/_mother_day_page.tpl.html",
    "<style>\n" +
    "    .introduction{\n" +
    "        margin: 50px 0px;\n" +
    "        font-weight: bold;\n" +
    "    }\n" +
    "\n" +
    "</style>\n" +
    "<div  class=\"landing-page\">\n" +
    "    <div class=\"container\">\n" +
    "        <div class=\"left\">\n" +
    "            <div class=\"banner_restaurant\" style=\"background-image: url('https://static2.weeloy.com/images/headers/mother-day-2016-weeloy-banner.jpg'); height:400px; background-size: cover; background-position: 50% 50%; background-repeat: no-repeat no-repeat;\">\n" +
    "            </div>\n" +
    "            \n" +
    "            <h1>Amazing Mother's Day Deals and Menus</h1>\n" +
    "            \n" +
    "            <p class='introduction'>\n" +
    "                Looking at the best restaurant to celebrate Mother's Day with your love ones? We've got some great venues with special Mother's Day menus for you and your family to have a memorable day to remember for years to come.\n" +
    "            </p>\n" +
    "            <div class=\"events\" >\n" +
    "                <ul style=\"max-width:600px; margin: auto; padding: 0px;\">\n" +
    "                    <li class=\"clearfix\" ng-repeat=\"evt in events\">\n" +
    "                        \n" +
    "                        <a analytics-on=\"click\"\n" +
    "                                        analytics-event=\"c_go_resto\"\n" +
    "                                        analytics-category=\"c_go_resto_mother_day\" \n" +
    "                                        analytics-label=\"c_go_resto_mother_day_{{evt.restaurant}}\"\n" +
    "                                        ng-href=\"{{getRestaurantLink(evt.restaurant)}}\"  style=\"text-decoration:none;\" title=\"Celebrate the Mother's Day with {{evt.title}} - reserve your table with weeloy\">\n" +
    "                            <h1>Celebrate the Mother's Day with {{evt.title}}</h1>\n" +
    "                        </a>\n" +
    "                        <div class=\"cnt\">\n" +
    "                            <h2 ng-bind=\"evt.name\"></h2>\n" +
    "                            <span ng-if=\"evt.start === evt.end\" ng-bind=\"evt.end | date : format : shortDate\"></span>\n" +
    "                            <span  ng-if=\"evt.start !== evt.end\">{{evt.start| date : format : shortDate}} to {{evt.end| date : format : shortDate}}, {{evt.area}}</span>\n" +
    "                            <p ng-bind=\"evt.description\"></p>\n" +
    "                            \n" +
    "                            <a analytics-on=\"click\"\n" +
    "                                        analytics-event=\"c_get_pdf\"\n" +
    "                                        analytics-category=\"c_get_pdf_mother_day\" \n" +
    "                                        analytics-label=\"c_get_pdf_mother_day_{{evt.restaurant}}\"href=\"{{evt.pdf_link}}\" target=\"_blank\" ng-if=\"evt.pdf_link\" title=\"{{evt.title}} Menu for mother's day 2016\">Download Mother's Day menu</a>\n" +
    "\n" +
    "                            <div style=\"margin-top:10px;\">\n" +
    "                                <button analytics-on=\"click\"\n" +
    "                                        analytics-event=\"c_book_resto\"\n" +
    "                                        analytics-category=\"c_book_resto_mother_day\" \n" +
    "                                        analytics-label=\"c_book_resto_mother_day_{{evt.restaurant}}\" ng-show=\"restaurant.status != 'comingsoon'\" class=\"book-button-sm btn-leftBottom-orange\" ng-click=\"btnBookNowEvent_click(evt.restaurant)\" ng-bind=\"book_button_text\"></button>\n" +
    "                            </div> \n" +
    "                        </div>\n" +
    "                        <div class=\"img-block\">\n" +
    "                            <a href=\"{{ mediaServer}}/upload/restaurant/{{evt.restaurant}}/{{evt.picture}}\" class=\"fresco\" title=\"mother's day @ {{evt.title}} - reserve restaurant table now\">\n" +
    "                                <img ng-src=\"{{ mediaServer}}/upload/restaurant/{{evt.restaurant}}/180/{{evt.picture}}\" class=\"img-responsive\" alt=\"{{evt.title}} mother's day Menu - reserve restaurant table now\" style='max-height: 300px;'>\n" +
    "                                \n" +
    "                            </a>\n" +
    "                        </div>\n" +
    "                    </li>\n" +
    "                </ul>\n" +
    "            </div>\n" +
    "            <div style=\"text-align: center;\">\n" +
    "                <a href='search/singapore' style=\" color: #555555;text-decoration: none;font-size: 17px;\">View more restaurants</a>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("../app/components/myaccount_page/_myaccount_page.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/components/myaccount_page/_myaccount_page.tpl.html",
    "<div id=\"my-account\">\n" +
    " \n" +
    "    <div ng-include=\"'client/app/shared/partial/_menu_user.tpl.html'\"></div>\n" +
    "    <div class=\"container\">\n" +
    "        <div id='msg-alert' class=\"alert\" style=\"display:none;\">\n" +
    "  </div>\n" +
    "        <div ng-show=\"showAccountForm == 'myAccountForm'\" ng-include=\"'client/app/shared/partial/_my_account_form.html'\" class=\"col-lg-12 col-md-8 col-sm-10 col-xs-12 \"></div>\n" +
    "        <div ng-show=\"showAccountForm == 'changePasswordForm'\" ng-include=\"'client/app/shared/partial/_change_password_form.html'\" class=\"col-lg-12 col-md-8 col-sm-10 col-xs-12 col-lg-offset-2 col-md-offset-2 col-sm-offset-1\"></div>\n" +
    "  \n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("../app/components/mybookings_page/_mybookings_page.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/components/mybookings_page/_mybookings_page.tpl.html",
    "<div id=\"booking-page\">\n" +
    "    <div ng-include=\"'client/app/shared/partial/_menu_user.tpl.html'\"></div>\n" +
    "    <div class=\"container\">\n" +
    "        <div class=\"col-lg-3 col-md-3 col-sm-4 col-xs-12 left-menu\">\n" +
    "            <ul>\n" +
    "                <li>\n" +
    "                    <a href=\"mybookings?f=today\" class=\"{{ (MyBookingPage == 'today') ? 'mybooking-menu-selected' : ''}}\">Today</a>\n" +
    "                </li>\n" +
    "                <li>\n" +
    "                    <a href=\"mybookings?f=coming\" class=\"{{ (MyBookingPage == 'coming') ? 'mybooking-menu-selected' : ''}}\">Coming Soon</a>\n" +
    "                </li>\n" +
    "                <li>\n" +
    "                    <a href=\"mybookings?f=past\" class=\"{{ (MyBookingPage == 'past') ? 'mybooking-menu-selected' : ''}}\">Past bookings</a>\n" +
    "                </li>\n" +
    "            </ul>\n" +
    "        </div>\n" +
    "        <div class=\"col-lg-9 col-md-9 col-sm-8 col-xs-12 my-booking\">\n" +
    "            <div  class=\"panel-group\" ng-repeat=\"item in bookings\" ng-show=\"showBookingList\" id=\"member-bklist\">\n" +
    "                \n" +
    "                <div class=\"item\"> \n" +
    "                  \n" +
    "                    <div class=\"col-lg-3 col-md-3 col-sm-3 col-xs-3 vignette_picture\">\n" +
    "                        <a class=\"res-path\" ng-href=\"{{ item.restaurantinfo.internal_path}}\">\n" +
    "                            <img id=\"img_logo_1\" ng-src=\"{{ item.restaurantinfo.defImg}}\">\n" +
    "                        </a>\n" +
    "                    </div>\n" +
    "                    <div class=\"col-md-6 infos\">\n" +
    "                        <h4 class=\"res-title\" ng-bind=\"item.restaurantinfo.title\"></h4>\n" +
    "                        <p class=\"time\" ng-bind=\"item.timeStr\"></p>\n" +
    "                        <p >Time :{{ item.rtime }}</p>\n" +
    "                    \n" +
    "                        <p>Covers: {{ item.cover}}</p>\n" +
    "                            <p  ng-if = \"item.payment===1 && (item.status == 'cancel' || item.status == 'noshow')  && item.amount >0  \" >Payment Id : {{item.deposit_id}}</p>\n" +
    "                            <p  ng-if = \"item.payment===1 && (item.status == 'cancel' || item.status == 'noshow') && item.amount >0  && item.paystatus!=='REFUNDED'\" >Cancelled Charge : {{item.currency}} {{item.amount}}</p>\n" +
    "                            <p  ng-if = \"item.paystatus==='REFUNDED' && item.refundamount >0 \" > Refund Amount :  {{item.currency}} {{item.refundamount}}</p>\n" +
    "\n" +
    "                    </div>\n" +
    "                    <div class=\"col-md-3 action_controls\">\n" +
    "                        <p class=\"spin_wheel_info\">\n" +
    "                            <!-- <a target='_blank' href='wheel/rotation/rotation_always_ndb.php?confirmation={[{booking.confirmation}]}&membCode={[{booking.membcode}]}&guestname={[{booking.firstname}]}' >Spin the wheel</a> -->\n" +
    "                        </p>\n" +
    "          \n" +
    "                        <div class=\"cancel_container\" ng-if=\" MyBookingPage == 'today' || MyBookingPage == 'coming' \">\n" +
    "                        \n" +
    "                            <div id=\"booking_depsoit_{{$index}}\" class=\"depsoit-details\" ng-if=\"(item.status ==='pending_payment' && item.time_left<=20 && item.passed==false)\"  >\n" +
    "\n" +
    "                                <p><a  href=\"{{item.paymentUrl}}\" target = \"_blank\"  > Pending Payment</a></p>\n" +
    "                              \n" +
    "                            </div>\n" +
    "               \n" +
    "                            <div id=\"booking_status_{{$index}}\" ng-if=\" (item.status != 'cancel' && item.status != 'pending_payment' && item.passed==false) && (item.wheelwin == null || item.wheelwin == '')\"  class=\"cancel-details\" >\n" +
    "<!--                                <p><a id=\"bookingcancel\" class=\"btn btn-blue\"  data-target=\"targetSection_{{$index}}\" ng-click=\"cancel($index, item, $event)\"  style=\"border-radius:2px;\">Update/Cancel </a></p>-->\n" +
    "                                <p>\n" +
    "                                    <a id=\"bookingcancel\"  ng-if = \"item.bkObject==='nodeposit'\" class=\"btn btn-blue\"  data-target=\"targetSectionmodif_{{$index}}\" ng-click=\"modif($index, item, $event)\"  style=\"border-radius:2px;\">Update/Cancel </a>\n" +
    "                                    <a id=\"bookingcancel\"  ng-if = \"item.bkObject==='deposit'\" class=\"btn btn-blue\"  data-target=\"targetSection_{{$index}}\" ng-click=\"cancel($index, item, $event)\"  style=\"border-radius:2px;\">Cancel </a>\n" +
    "                                </p>\n" +
    "                                \n" +
    "                               <!-- <p ng-if='localhost'><a id=\"bookingmodif\" class=\"btn btn-blue\"  data-target=\"targetSectionmodif_{{$index}}\" ng-click=\"modif($index, item, $event)\"  style=\"border-radius:2px;\">Modify </a></p>-->\n" +
    "                            </div>\n" +
    "                            <div  ng-if=\" (item.status != 'cancel' && item.status === 'pending_payment' && item.passed==false && item.time_left>20)\"  class=\"cancel-details\" >\n" +
    "                                {{item.status}}\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                        <p class=\"booking-status text-center\" ng-if= \"item.status == 'cancel' ||  item.status == 'noshow' \">\n" +
    "                                   {{ (item.status == 'cancel') ? 'Booking cancelled' : 'noshow'}}\n" +
    "                        </p>\n" +
    "                        <p class=\"booking-status text-center\" ng-if= \"item.status != 'cancel' ||  item.status != 'noshow' \">\n" +
    "                                   {{ item.wheelwin }}\n" +
    "                        </p>\n" +
    "\n" +
    "<!--                        <div class=\"spin_wheel_info text-center\" ng-if=\"(item.reviewgrade == null || item.reviewgrade == '') && item.wheelwin != null && item.wheelwin != '' && item.status != 'cancel'\">\n" +
    "\n" +
    "                            <button id=\"reviewCancel\" type=\"button\" class=\"btn btn-blue btn-review\" data-target=\"targetSectionRev_{{$index}}\" ng-click=\"Review($index,item,$event)\">Review Now</button>\n" +
    "                        </div>-->\n" +
    "                        <div class=\"spin_wheel_info text-center\" ng-if=\"(item.reviewgrade == null || item.reviewgrade == '') && (item.status != 'cancel' && item.status != 'noshow') \">\n" +
    "<!--        \n" +
    "                            <div ng-if =\"(item.wheelwin != null && item.wheelwin != '')\" >\n" +
    "                                <button id=\"reviewCancel\" type=\"button\" class=\"btn btn-blue btn-review\" data-target=\"targetSectionRev_{{$index}}\" ng-click=\"Review($index,item,$event)\">Review Now</button>\n" +
    "                            </div>-->\n" +
    "                         \n" +
    "                            <div ng-if=\"(item.passed ==true) \">\n" +
    "                                <button id=\"reviewCancel\" type=\"button\" class=\"btn btn-blue btn-review\" data-target=\"targetSectionRev_{{$index}}\" ng-click=\"Review($index,item,$event)\">Review Now</button> \n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                        <div class=\"spin_wheel_info text-center\" ng-if=\"item.reviewgrade != null && item.status != 'cancel'\">\n" +
    "                            <a href=\"myreviews?f=past\" id=\"member-mybooking-reviews\">\n" +
    "                                <span title=\"reviewScore:{{item.reviewgrade}}\" class=\"reviews {{item.reviewgrade| get_review_class}} spin-info \" > &nbsp</span> \n" +
    "                                <span></span>\n" +
    "                            </a>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div ng-if=\"(MyBookingPage == 'today' || MyBookingPage == 'coming') && item.status == 'cancel' && (item.wheelwin == null || item.wheelwin == '')\" class=\"share-social text-right col-lg-12 col-md-12 col-sm-12 col-xs-12\">\n" +
    "                        <button class=\"btn btn-facebook text-left fbshare\" ng-click=\"fbshare(item)\"><i class=\"fa fa-facebook\"></i> Share</button>\n" +
    "                        <button class=\"btn btn-twitter text-right twitte-share\" ng-click=\"tws_click(item)\"><i class=\"fa fa-twitter\"></i> Twitter</button>\n" +
    "                    </div>\n" +
    "                    <div ng-if=\" MyBookingPage == 'past' && item.status != 'cancel' && item.wheelwin !== null && item.wheelwin !== ''\" class=\"share-social text-right col-lg-12 col-md-12 col-sm-12 col-xs-12\">\n" +
    "                        <button class=\"btn btn-facebook text-left fbshare\" ng-click=\"fbshare(item)\"><i class=\"fa fa-facebook\"></i> Share</button>\n" +
    "                        <button class=\"btn btn-twitter text-right twitte-share\" ng-click=\"tws_click(item)\"><i class=\"fa fa-twitter\"></i> Twitter</button>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                   <div id=\"btncontainer\" class=\"panel-body\" >\n" +
    "                        <div id=\"targetSection_{{$index}}\" class=\"currentpanel\" ng-include=\"'client/app/shared/partial/_my_booking_page_cancel_section.html'\" ng-show=\"showCancelSection_{{$index}}\" ></div>\n" +
    "                        <div id=\"targetSectionmodif_{{$index}}\" class=\"currentpanel\" ng-include=\"'client/app/shared/partial/_my_booking_page_modif_section.html'\" ng-show=\"showModifSection_{{$index}}\" ></div>\n" +
    "                        <div id=\"targetSectionRev_{{$index}}\" class=\"currentpanel review-section\" ng-include=\"'client/app/shared/partial/_my_booking_page_review_section.html'\" ng-show=\"showReviewSection_{{$index}}\" ></div>\n" +
    "                       \n" +
    "<!--                        <review-section  id=\"targetSectionRev_{{$index}}\" class=\"currentpanel\"  id=\"{{$index}}\" review-item=\"item\" ng-show=\"showReviewSection_{{$index}}\"></review-section>-->\n" +
    "                    </div>\n" +
    "           </div>\n" +
    "            <span ng-show=\"showBookingList == false\" class=\"no-data-available\">\n" +
    "                <h4>\n" +
    "                <p ng-if=\" MyBookingPage == 'today' || MyBookingPage == 'coming'\">\n" +
    "                        No new bookings available. <br /><br />\n" +
    "                        Make a new reservation <a href =\"https://www.weeloy.com/search/singapore\">here</a>.\n" +
    "                </p>\n" +
    "                <p ng-if=\"MyBookingPage == 'past' \">\n" +
    "                        \"We are feeling a little lonely here.\" <br /><br />\n" +
    "                         Make a new reservation <a href =\"https://www.weeloy.com/search/singapore\">here</a>.\n" +
    "                </p>\n" +
    "                </h4>\n" +
    "            </span>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "\n" +
    "");
}]);

angular.module("../app/components/myorders_page/_myorders_page.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/components/myorders_page/_myorders_page.tpl.html",
    "<div id=\"myorders-page\">\n" +
    "    <div ng-include=\"'client/app/shared/partial/_menu_user.tpl.html'\"></div>\n" +
    "      \n" +
    "    <div class=\"container\" >\n" +
    "         <div class=\"col-lg-3 col-md-3 col-sm-4 col-xs-12 left-menu\">\n" +
    "         </div>\n" +
    "        \n" +
    "        <div class=\"col-lg-9 col-md-9 col-sm-8 col-xs-12\">\n" +
    "        <span class=\"no-data-available\" ng-show=\"shownoData==false\" >\n" +
    "                <h4>\n" +
    "                <p >\n" +
    "                        No new orders available. <br /><br />\n" +
    "                        Make a new order <a href =\"https://www.weeloy.com/search/singapore\">here</a>.\n" +
    "                </p>\n" +
    "                </h4>\n" +
    "         </span>\n" +
    "       <div ng-show=\"orders | lengthMoreThan:0\" ng-if=\"showListOrders\">\n" +
    "        <div id=\"orders\">\n" +
    "            <div class=\"order col-xs-12\">\n" +
    "                <div class=\"order-restaurant\">\n" +
    "                    <p><strong>Restaurant</strong></p>\n" +
    "                </div>\n" +
    "                <div class=\"order-amount text-center\">\n" +
    "                    <p><strong>Total (<span ng-bind=\"orders[0].currency\"></span>)</strong></p>\n" +
    "                </div>\n" +
    "                <div class=\"order-total-items text-center\">\n" +
    "                    <p><strong>Number items</strong></p>\n" +
    "                </div>\n" +
    "                <div class=\"order-total-items\">\n" +
    "                    <p><strong>Delivery date</strong></p>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"order\" ng-repeat=\"order in orders\">\n" +
    "                <div class=\"order-restaurant\">\n" +
    "                    <p>\n" +
    "                        <a ng-href=\"myorders?id={{order.ID}}\" ng-bind=\"order.title\"></a>\n" +
    "                    </p>\n" +
    "                </div>\n" +
    "                <div class=\"order-amount text-center\">\n" +
    "                    <p ng-bind=\"order.total\"></p>\n" +
    "                </div>\n" +
    "                <div class=\"order-total-items text-center\">\n" +
    "                    <p ng-bind=\"order.total_items\"></p>\n" +
    "                </div>\n" +
    "                <div class=\"order-total-items\">\n" +
    "                    <p ng-bind=\"order.time\"></p>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"col-xs-12\" ng-if=\"showPagination\">\n" +
    "            <ul class=\"pagination pull-right\">\n" +
    "                <li ng-repeat=\"p in pages\" ng-class=\"page == p ? 'active':''\"><a ng-href=\"myorders?page={{p}}\" ng-bind=\"p\"></a></li>\n" +
    "            </ul>\n" +
    "        </div>\n" +
    "    </div>\n" +
    " </div>\n" +
    " </div>\n" +
    "    <div class=\"container\" id=\"order-detail\" ng-if=\"showOrderDetail\">\n" +
    "        <a href=\"myorders\">\n" +
    "            <button class=\"btn btn-default back\"><span class=\"fa fa-arrow-left\"></span>My Orders</button>\n" +
    "        </a>\n" +
    "        <!--<p><strong>Restaurant: {{items[0].restaurant}}</strong></p>\n" +
    "        <p><strong>Delivery Time: {{items[0].delivery_time}}</strong></p>\n" +
    "        <p><strong>Order date: {{items[0].time}}</strong></p>-->\n" +
    "        <table class=\"table table-bordered\">\n" +
    "            <thead>\n" +
    "                <tr>\n" +
    "                    <td>Item</td>\n" +
    "                    <td>Restaurant</td>\n" +
    "                    <td>Quantity</td>\n" +
    "                    <td>Price ({{items[0].currency}})</td>\n" +
    "                </tr>\n" +
    "            </thead>\n" +
    "            <tbody>\n" +
    "                <tr ng-repeat=\"item in items\">\n" +
    "                    <td ng-bind=\"item.item_title\"></td>\n" +
    "                    <td ng-bind=\"item.restaurant\"></td>\n" +
    "                    <td ng-bind=\"item.quantity\"></td>\n" +
    "                    <td ng-bind=\"item.price\"></td>\n" +
    "                </tr>\n" +
    "            </tbody>\n" +
    "        </table>\n" +
    "        <p><strong class=\"pull-right\">Total: {{items[0].currency}} {{items[0].total}}</strong></p>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("../app/components/myreviews_page/_my_reviews_page.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/components/myreviews_page/_my_reviews_page.tpl.html",
    "<div id=\"my-review\">\n" +
    "    <div ng-include=\"'client/app/shared/partial/_menu_user.tpl.html'\"></div>\n" +
    "    <div class=\"container\">\n" +
    "        <div class=\"col-lg-3 col-md-3 col-sm-3 col-xs-12\">\n" +
    "            <p>Your reviews</p>\n" +
    "        </div>\n" +
    "        <div class=\"col-lg-9 col-md-9 col-sm-9 col-xs-12\">\n" +
    "            <span class=\"no-data-available\" ng-show=\"shownoData==false\" >\n" +
    "                <h4>\n" +
    "                <p >\n" +
    "                        No  reviews  available. <br /><br />\n" +
    "                        Make a new booking <a href =\"https://www.weeloy.com/search/singapore\">here</a>.\n" +
    "                </p>\n" +
    "                </h4>\n" +
    "            </span>\n" +
    "               <div id='msg-alert' class=\"alert\" style=\"display:none;\">\n" +
    "                \n" +
    "               </div>\n" +
    "            <div class=\"review\" ng-repeat=\"review in myReviews\" ng-show=\"showreviewList==true\">\n" +
    "                <div class=\"col-lg-3 col-md-3 col-sm-3 col-xs-3 vignette_picture\">\n" +
    "                    <a href=\"{{ review.internal_path }}\"><img ng-src=\"{{ review.image }}\"></a>\n" +
    "                   \n" +
    "       \n" +
    "                </div>\n" +
    "                <div class=\"col-lg-6 col-md-9 col-sm-9 col-xs-9 infos\">\n" +
    "                  \n" +
    "                    <h4 ng-bind=\"review.restaurant_title\"></h4>\n" +
    "                    <p ng-bind=\"review.timeStr\"></p>\n" +
    "                    <div class=\"fb-jRate\"></div>\n" +
    "       \n" +
    "                     <div title =\"{{review.reviewgrade }}\" class=\"reviewgrade\" ><span class=\"reviews {{review.reviewgrade| get_review_class}} \" style=\"line-height:10px;width:500px;padding-left:74px;\">&nbsp</span>\n" +
    "                        <br /><span  ng-bind=\"review.review_desc\" style=\"padding-top:0px;\"></span></div>\n" +
    "                  \n" +
    "<!--                    <div class=\"form-group row\">\n" +
    "                       \n" +
    "                        <label for=\"slider_food_1\" class=\"col-sm-3\" style=\"margin-bottom: 0px;\">Food</label>\n" +
    "                        <div class=\"col-sm-9\">\n" +
    "                            <div id=\"text_slider_food_1\" name=\"text_slider_food_1\" style=\"width:120px;\"> <span class=\"reviews {{review.foodgrade| get_review_class}} \" style=\"padding-left:74px;\" >&nbsp</span></div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group row\">\n" +
    "                        <label for=\"slider_ambiance_1\" class=\"col-sm-3\" style=\"margin-bottom: 0px;\">Ambiance</label>\n" +
    "                        <div class=\"col-sm-9\">\n" +
    "                            <div id=\"text_slider_ambiance_1\" name=\"text_slider_ambiance_1\"><span class=\"reviews {{review.ambiancegrade| get_review_class}} \" style=\"padding-left:74px;\" >&nbsp</span></div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group row\">\n" +
    "                        <label for=\"slider_service_1\" class=\"col-sm-3\" style=\"margin-bottom: 0px;\">Service</label>\n" +
    "                        <div class=\"col-sm-9\">\n" +
    "                            <div id=\"text_slider_service_1\" name=\"text_slider_service_1\"><span class=\"reviews {{review.servicegrade| get_review_class}} \" style=\"padding-left:74px;\" >&nbsp</span></div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group row\">\n" +
    "                        <label for=\"slider_price_1\" class=\"col-sm-3\" style=\"margin-bottom: 0px;\">Price</label>\n" +
    "                        <div class=\"col-sm-9\">\n" +
    "                            <div id=\"text_slider_price_1\" name=\"text_slider_price_1\"><span class=\"reviews {{review.pricegrade| get_review_class}} \" style=\"padding-left:74px;\" >&nbsp</span></div>\n" +
    "                        </div>\n" +
    "                    </div>-->\n" +
    "                    <div class=\"form-group row\" style=\"margin-top: 15px;\">\n" +
    "                        <label class=\"col-sm-3\" for=\"review_comment\">Comment</label>\n" +
    "                        <div class=\"col-sm-9\">\n" +
    "                            <div readonly=\"\" ng-bind=\"review.comment\"></div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            \n" +
    "            <div class=\"pannel\" ng-show=\"showreviewpostList\" >\n" +
    "          \n" +
    "            <review-section  id=\"targetSectionRev\" class=\"currentpanel\" rdata-review=\"reviewList\" ng-show=\"showreviewpostLists =true\"></review-section>\n" +
    "<!--                 <div ng-include=\"'client/app/shared/partial/_my_review_page_reviewpost_section.html'\"></div>-->\n" +
    "                \n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("../app/components/payment/_pending_deposit.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/components/payment/_pending_deposit.tpl.html",
    "<div class=\"restaurant-info-page\" ng-if=\"restaurant != undefined\">\n" +
    "    <div class=\"text-center\" ng-style=\"restaurant.bannerStyleBookNow(bannerSizePath)\" style=\"overflow: hidden;\">\n" +
    "        <div class=\"container\" style=\"padding: 60px 0 0; margin-bottom: -55px\">\n" +
    "            <div id=\"breadcrumb\" breadcrumb base-url=\"base_url\" base-path=\"base_path\" restaurant=\"restaurant\"></div>\n" +
    "        </div>\n" +
    "        <payment-iframe restaurant=\"restaurant\"></payment-iframe>\n" +
    "    </div>\n" +
    "</div>");
}]);

angular.module("../app/components/payment_success_page/_payment_success_page.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/components/payment_success_page/_payment_success_page.tpl.html",
    "<div id=\"payment-success\">\n" +
    "    <div class=\"container\">\n" +
    "        <p class=\"text-center\"><span ng-bind=\"message\"></span>, <a href=\"/\">Go to homepage</a></p>\n" +
    "        <div class=\"col-xs-12 restaurant\">\n" +
    "            <div class=\"bar-logo pull-left\">\n" +
    "                <img ng-src=\"{{restaurant.getLogoImage()}}\" class=\"img-responsive\" alt=\"\">\n" +
    "            </div>\n" +
    "            <div class=\"restaurant-info\">\n" +
    "                <h1 ng-bind=\"restaurant.title\"></h1>\n" +
    "                <p><span ng-bind=\"restaurant.address\"></span><span ng-bind=\"restaurant.zip\"></span></p>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"col-xs-12 col-md-6 col-lg-6\">\n" +
    "            \n" +
    "            <div class=\"col-xs-12\">\n" +
    "                <div class=\"form-group\">\n" +
    "                    <strong>Order confirmation number:</strong> <span ng-bind=\"order.orderID\"></span> \n" +
    "                </div>\n" +
    "                <!-- <div class=\"form-group\">\n" +
    "                    <strong>PaymentId:</strong> <span ng-bind=\"order.ID\"></span>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <strong>Restaurant:</strong> <span ng-bind=\"restaurant.title\"></span>\n" +
    "                </div>-->\n" +
    "                <div class=\"form-group\">\n" +
    "                    <strong>Email:</strong> <span ng-bind=\"order.email\"></span>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <strong>Total:</strong> <span >SGD</span>  <span ng-bind=\"order.total\"></span>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <strong>Name:</strong> <span ng-bind=\"order.firstname\"></span> <span ng-bind=\"order.lastname\"></span>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <strong>Phone:</strong> <span ng-bind=\"order.mobile\"></span>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\" ng-show=\"order.company\">\n" +
    "                    <strong>Company:</strong> <span ng-bind=\"order.company\"></span>\n" +
    "                </div>\n" +
    "                <!-- <div class=\"form-group\">\n" +
    "                    <strong>Remarks:</strong> <span ng-bind=\"order.remarks\"></span>\n" +
    "                </div>  -->\n" +
    "                <div class=\"form-group\">\n" +
    "                    <strong>Number items:</strong> <span ng-bind=\"order.total_items\"></span> <a ng-click=\"viewAllItem(order.ID)\">view all</a>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <strong>Pickup: </strong> <span ng-bind=\"order.delivery_date\"></span>, Time: <span ng-bind=\"order.delivery_time\"></span>\n" +
    "                </div>\n" +
    "\n" +
    "\n" +
    "\n" +
    "    <!--             <div class=\"form-group\">\n" +
    "                    <strong>Transaction ID:</strong> <span ng-bind=\"payment.txn_id\"></span>\n" +
    "                </div>-->\n" +
    "\n" +
    "            </div>\n" +
    "            <div class=\"col-xs-12\" ng-if=\"items | lengthMoreThan:0\">\n" +
    "                <table class=\"table table-bordered\">\n" +
    "                    <thead>\n" +
    "                        <tr>\n" +
    "                            <td>#</td>\n" +
    "                            <td>Item</td>\n" +
    "                            <td>Price ({{items[0].currency}})</td>\n" +
    "                            <td>Quantity</td>\n" +
    "                        </tr>\n" +
    "                    </thead>\n" +
    "                    <tbody>\n" +
    "                        <tr ng-repeat=\"item in items\">\n" +
    "                            <td ng-bind=\"$index + 1\"></td>\n" +
    "                            <td ng-bind=\"item.item_title\"></td>\n" +
    "                            <td ng-bind=\"item.price\"></td>\n" +
    "                            <td ng-bind=\"item.quantity\"></td>\n" +
    "                        </tr>\n" +
    "                    </tbody>\n" +
    "                </table>\n" +
    "            </div>\n" +
    "\n" +
    "        </div>\n" +
    "        <div class=\"col-xs-12 col-md-6 col-lg-6\">\n" +
    "            \n" +
    "            <div class=\"col-xs-12\" style=\"margin: 0px 0px 100px 0px;\" >\n" +
    "                <div id=\"map_canvas\" ng-if=\"restaurant\" style=\"width:100%;height:250px;\" ng-init=\"loadmap(restaurant.getLatitude().lat, restaurant.getLatitude().lng, isMobile)\" ng-click=\"loadmap2\"></div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("../app/components/privacy_policy_page/_privacy_policy.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/components/privacy_policy_page/_privacy_policy.tpl.html",
    "<link href='https://fonts.googleapis.com/css?family=Dosis:400,600,700bold|Roboto:400,900,700italic,700,500italic,500,400italic' rel='stylesheet' type='text/css'>\n" +
    "<style>\n" +
    "    #privacy-policy {\n" +
    "        padding-top: 40px;\n" +
    "        font-size: 11px;\n" +
    "        font-family: Roboto;\n" +
    "        margin: 0 0 0 10px;\n" +
    "    }\n" +
    "\n" +
    "    ol p {\n" +
    "        margin: 20px 10px 10px 0;\n" +
    "        text-justify: inter-word;\n" +
    "        text-align: justify;\n" +
    "    }\n" +
    "\n" +
    "    ol li {\n" +
    "        margin: 10px 20px 0 0;\n" +
    "        text-justify: inter-word;\n" +
    "        text-align: justify;\n" +
    "    }\n" +
    "\n" +
    "    ol {\n" +
    "        margin: 10px 0 0 0;\n" +
    "    }\n" +
    "\n" +
    "    h5 {\n" +
    "        color: red;\n" +
    "        text-transform: uppercase;\n" +
    "        margin: 30px 0 10px 0;\n" +
    "    }\n" +
    "\n" +
    "    #section {\n" +
    "        margin: 0 0 0 -40;\n" +
    "    }\n" +
    "\n" +
    "    ol {\n" +
    "        counter-reset: section;\n" +
    "        list-style-type: none;\n" +
    "    }\n" +
    "\n" +
    "    ol li {\n" +
    "        list-style-type: none;\n" +
    "    }\n" +
    "\n" +
    "    ol li ol {\n" +
    "        counter-reset: subsection;\n" +
    "    }\n" +
    "\n" +
    "    ol li ol li ol {\n" +
    "        counter-reset: subsubsection;\n" +
    "    }\n" +
    "\n" +
    "    ol li:before {\n" +
    "        counter-increment: section;\n" +
    "        content: counter(section) \". \";\n" +
    "        /*content:\"Section \" counter(section) \". \";*/\n" +
    "        font-weight: bold;\n" +
    "    }\n" +
    "\n" +
    "    ol li ol li:before {\n" +
    "        counter-increment: subsection;\n" +
    "        content: counter(section) \".\" counter(subsection) \" \";\n" +
    "    }\n" +
    "\n" +
    "    ol li ol li ol li:before {\n" +
    "        counter-increment: subsubsection;\n" +
    "        content: counter(section) \".\" counter(subsection) \".\" counter(subsubsection) \" \";\n" +
    "    }\n" +
    "</style>\n" +
    "<div id=\"privacy-policy\">\n" +
    "    <ol>\n" +
    "        <div class=\"modal-body\">\n" +
    "            <h1 align='center' style='margin:10px 0 40px 0;'> Weeloy Privacy Policy</h1>\n" +
    "            <p>\n" +
    "                I consent for my personal data to be collected, used, processed and stored by Weeloy (as defined below) in connection with my use of Weeloy’s online restaurant system and table reservation services.\n" +
    "            </p>\n" +
    "            <p>\n" +
    "                I would like to receive all latest dining trends, promotional offers and updates from Weeloy and Weeloy's participating merchants in accordance with the following terms and conditions. \n" +
    "            </p>\n" +
    "            <p>\n" +
    "                In compliance with requirements of the Personal Data Protection Act 2012 (\"PDPA\"), Weeloy Pte. Ltd. and/or affiliates including but not limited to establishments and media partners that have entered into an “agreement” with Weeloy (collectively, “Weeloy”) have adopted this Privacy Policy. \n" +
    "                Users are strongly recommended to read this Privacy Policy carefully to have an understanding of Weeloy’s policy and practices with regard to the treatment of personal data provided by Users using Weeloy.com, its sub-domains, any other websites, media platforms or applications including mobile applications operated/developed by Weeloy (collectively, \"Channels\"). This Privacy Policy is applicable to both registered and non-registered Users, and the provisions herein may be updated, revised, varied and/or amended from time to time as Weeloy deems necessary. \n" +
    "                If Users have questions or concerns regarding this Privacy Policy, please contact Weeloy's Customer Service Department at +65 62211016 or email to info@weeloy.com \n" +
    "            </p>\n" +
    "            <li><strong>Consent for Collection of Personal Data</strong>\n" +
    "                <ol>\n" +
    "                    <li>In the course of using the Channels, Users (registered or otherwise) may be requested to disclose to provide personal data in order to enjoy various services offered by the Channels. All personal data is provided voluntary. Users may refuse to provide such personal data or withdraw their consent for the collection, use disclosure and storage of their personal. In so doing, Weeloy would not be able to provide certain services on the Channels.  The following personal data may be requested: name, log-in ID and password, address, email address, telephone number, age, sex, date of birth, country of residence, nationality, education level and work experience that is/are not otherwise publicly available.  Occasionally, Weeloy may also collect additional personal data from Users in connection with contests, surveys, or special offers.</li>\n" +
    "                    <li>When a User provides personal data to Weeloy, the User is deemed to have consented to the collection, storage, use and disclosure of his/her personal data to Weeloy in accordance with this Privacy Policy. </li>\n" +
    "                    <li>If a User is under the age of 13 years (\"minors\"), Weeloy strongly recommends him/her to seek prior consent from a person with parental responsibility, e.g. parent or guardian, who may contact Weeloy at +65 6221 1016 or email to info@weeloy.com for registering the User as member of the Channels. Where collection of minors' personal data is made through persons claiming to be parents or guardians, Weeloy may (but is not obliged to) require proof to verify such claims.</li>\n" +
    "                </ol>\n" +
    "            </li>\n" +
    "            <li><strong>Consent for Collection of Personal Data</strong>\n" +
    "                <ol>\n" +
    "                    <li>Weeloy strives to only collect personal data, which is necessary and adequate but not excessive in relation to the purposes set out below. If Weeloy requires the use of Users’ personal data beyond the stated purposes, Weeloy may request the Users’ prescribed consent to the same or if a User is a minor, the prescribed consent should be given by his/her parent or guardian.</li>\n" +
    "                    <li>Weeloy’s purposes for the collection, use and disclosure of Users' personal data include but are not limited to the following (either independently by Weeloy or together with its establishments and media partners sited in and outside of Singapore):\n" +
    "                        <ol>\n" +
    "                            <li>Daily operation of the services provided to Users.\n" +
    "                            </li>\n" +
    "                            <li>Identify Users who have (i) posted advertisements, materials, messages, photos, views or comments or such other information (collectively “Information”) on the Channels; (ii) viewed any information posted on the Channels; or (iii) enjoyed their benefits as members of the Channels by receiving and using marketing and promotional materials.\n" +
    "                            </li>\n" +
    "                            <li>Provide Users with (i) marketing and promotional materials for their enjoyment of benefits as members of the Channels (as detailed in Paragraph 4 below); and (ii) a platform and forum for posting photos, sharing and discussing their insights in respect of services or products related to food and beverage.\n" +
    "                            </li>\n" +
    "                            <li>Allow members of the Channels to enjoy member benefits by enrolling for special events hosted by Weeloy.\n" +
    "                            </li>\n" +
    "                            <li>Allow Weeloy to compile and analyze aggregate statistics about Users’ use of the Channels and service usage, for Weeloy’s internal use.\n" +
    "                            </li>\n" +
    "                            <li>Facilitate in the provision of services offered by Weeloy (and the participating establishment and media partners) including special events and/or promotions.\n" +
    "                            </li>\n" +
    "                        </ol>\n" +
    "                    </li>\n" +
    "                </ol>\n" +
    "            </li>\n" +
    "            <li><strong>Disclosure and Transfer of Personal Data</strong>\n" +
    "                <ol>\n" +
    "                    <li>Weeloy shall not sell Users' personal data to third parties. Weeloy shall only disclose Users' personal data to third parties where Users have (or deemed to have) provided consent and in the situations expressly set out in in this Privacy Policy. If Users consent to receiving marketing information from Weeloy, Weeloy's strategic partners and business associates, Weeloy shall disclose Users' personal data to such third parties for various purposes, including but not limited to:\n" +
    "                        <ol>\n" +
    "                            <li>providing products and services requested by Users, including providing all marketing and other information about products and services which may be of interests to Users; \n" +
    "                            </li>\n" +
    "                            <li>allowing Weeloy's third party suppliers or external service providers working on Weeloy's behalf and providing services such as hosting and maintenance services, analysis services, e-mail messaging services, delivery services, handling of payment transactions, solvency check and address check, and/or facilitating/improving the quality of services on the Channels; and\n" +
    "                            </li>\n" +
    "                            <li>allowing Weeloy to comply with legal obligations or industry requirements, including disclosures to legal, regulatory, governmental, tax and law enforcement authorities and Weeloy's professional advisers (e.g. accountants, lawyers, and auditors).\n" +
    "                            </li>\n" +
    "                        </ol>\n" +
    "                    </li>\n" +
    "                    <li>\n" +
    "                        Users fully understand and consent that Weeloy may transfer their personal data to any location outside of Singapore (or Users' country of domicile) for the purposes set out in this Privacy Policy. When transferring any personal data outside of Singapore, Weeloy shall protect Users' personal data to a standard comparable to the protection accorded to such personal data under the PDPA.\n" +
    "                    </li>\n" +
    "                </ol>\n" +
    "            </li>\n" +
    "\n" +
    "            <li><strong>Newsletters and other Promotional Materials</strong>\n" +
    "                <ol>\n" +
    "                    <li>Users acknowledge that based on their personal data provided to Weeloy and unless they have expressly opted out, Weeloy (or its media partners) may from time to time send to Users newsletters, promotional materials and marketing materials on the following classes of services, products:\n" +
    "                        <ol>\n" +
    "                            <li>Food and beverage related products and services.\n" +
    "                            </li>\n" +
    "                            <li>Travel related products and services.\n" +
    "                            </li>\n" +
    "                            <li>Special events hosted by Weeloy for Users, including but not limited to courses, workshops, and competitions.\n" +
    "                            </li>\n" +
    "                            <li>Reward, loyalty or privileges programs and related products and services.\n" +
    "                            </li>\n" +
    "                            <li>Special offers including e-vouchers, coupons, discounts, group purchase offers and promotional campaigns.\n" +
    "                            </li>\n" +
    "                            <li>Products and services offered by Weeloy and its advertisers (the names of such entities can be found in the relevant advertisements and/or promotional or marketing materials for the relevant products and services, as the case may be).\n" +
    "                            </li>\n" +
    "                            <li>Donations and contributions for charitable and/or non-profit purposes.\n" +
    "                            </li>\n" +
    "                        </ol>\n" +
    "                    </li>\n" +
    "                    <li>Suitable measures are implemented to make available to Users the option to “opt-out” of receiving such materials. In this regard, Users may choose to sign up or unsubscribe for such materials by logging into the registration or user account maintenance webpage, or clicking on the automatic link appearing in each newsletter/message, or contact the Customer Service Representative of Weeloy at +65 62211016 or email to info@weeloy.com\n" +
    "                    </li>\n" +
    "                </ol>\n" +
    "            </li>\n" +
    "            <li><strong>Access to Personal Data</strong>\n" +
    "                <ol>\n" +
    "                    <li>All Users are entitled to access their personal data at Weeloy.com by signing in to make amendments or updates thereto, except that if a User wishes to change his/her account ID or has any question about the processing of his/her personal data or about this Privacy Policy, or does not accept the amended Privacy Policy, or  wishes to withdraw any consent that has been given to Weeloy, he/she must contact Weeloy Customer support at +65 62211016 or email to info@weeloy.com. Please note that Weeloy may be prevented by law from complying with any request that a User may have submitted hereunder. \n" +
    "                    </li>\n" +
    "                    <li>Weeloy may also charge Users a fee for responding to their requests for access to their personal data as held by Weeloy. If a fee is to be charged, Weeloy shall inform the User of the amount beforehand and respond to the User's request after payment is received.\n" +
    "                    </li>\n" +
    "\n" +
    "                </ol>\n" +
    "            </li>\n" +
    "            <li><strong>Cookies</strong>\n" +
    "                <ol>\n" +
    "                    <li>Weeloy does not collect any personally identifiable information from Users when they visit and browse the Channels, save and except where such information is expressly requested. When Users access the Channels, Weeloy records their visits only. The Channels’ server software records the domain name server address and track the pages the Users visit and store such information in “cookies”, and gathers and stores information like internet protocol (IP) addresses, browser type, referring/exit pages, operating system, date/time stamp, and clickstream data in log files. Weeloy and third-party vendors engaged by Weeloy use cookies (e.g. Google Analytics cookies) to inform, optimize and serve marketing materials based on Users’ past visits to the Channels. \n" +
    "                    </li>\n" +
    "                    <li>Weeloy does not link the information and data automatically collected in the above manner to any personally identifiable information. Weeloy generally uses such automatically collected data to estimate the audience size of the Channels, gauge the popularity of various parts of the Channels, track Users’ movements and number of entries in Weeloy’s promotional activities and special events, measure Users’ traffic patterns and administer the Channels. Such automatically collected information and data shall not be disclosed save and except in accordance with this Privacy Policy.\n" +
    "                    </li>\n" +
    "                </ol>\n" +
    "            </li>\n" +
    "            <li><strong>Links to other Websites, Media Platforms and Applications</strong>\n" +
    "                <ol>\n" +
    "                    <li>The Channels may have the functionalities to allow Users to share with or disclose their personal data to third parties, and/or links to other websites, media platforms and applications of third parties not owned or controlled by Weeloy. Personal data from Users may be collected on these other websites, media platforms, applications when Users visit such websites, media platforms and applications and make use of the services provided therein. \n" +
    "                    </li>\n" +
    "                    <li>Where and when Users decide to click on any advertisement or hyperlink on the Channels which grant Users access to another website, media platform and application, the protection of Users’ personal information and data which are deemed to be private and confidential may be exposed in these other websites, media platforms and applications. \n" +
    "                    </li>\n" +
    "                    <li>Non-registered Users who gain access to the Channels via their accounts in online social networking tools (including but not limited to Facebook) are deemed to have consented to the terms of this Privacy Policy, and such Users’ personal data which they have provided to those networking tools may be obtained by Weeloy and be used by Weeloy and its authorized persons in and outside of the User’s country of domicile. \n" +
    "                    </li>\n" +
    "                    <li>Users are responsible for their choice(s) and are deemed to have provided consent for any sharing of their personal data in the manner provided on or by the Channels.\n" +
    "                    </li>\n" +
    "                </ol>\n" +
    "            </li>\n" +
    "            <li><strong>Security of Personal Data</strong>\n" +
    "                <ol>\n" +
    "                    <li>The security of Users’ personal information and data is important to Weeloy. Weeloy shall use best efforts to ensure that Users’ personal data shall be protected against unauthorized access. Weeloy has implemented appropriate electronic and managerial measures in order to safeguard, protect and secure Users’ personal data. Users are reminded to safeguard his/her unique Username and Password by keeping it secret and confidential.\n" +
    "                    </li>\n" +
    "                    <li>Weeloy uses third party payment gateway service providers to facilitate electronic transactions on the Channels. Regarding sensitive information provided by Users, such as credit card number for completing any electronic transactions, the web browser and third party payment gateway communicate such information using secure socket layer technology (SSL). Weeloy follows generally accepted industry standards to protect the personal data submitted by Users to the Channels, both during transmission and once Weeloy receives it. However, no method of transmission over the Internet, or method of electronic storage, is 100% secure. Therefore, while Weeloy strives to protect Users’ personal data against unauthorized access, Weeloy cannot guarantee its absolute security.\n" +
    "                    </li>\n" +
    "                </ol>\n" +
    "            </li>\n" +
    "            <li><strong>Retention of Personal Data</strong>\n" +
    "                <ol>\n" +
    "                    <li>Weeloy has put in place measures such that Users' personal data in Weeloy's possession or control shall be destroyed and/or anonymised as soon as it is reasonable to assume that the purposes for which Users' personal data were collected are no longer being served by the retention thereof, and/or that retention thereof is no longer necessary for any other legal or business purposes.\n" +
    "                    </li>\n" +
    "                </ol>\n" +
    "            </li>\n" +
    "            <li><strong>Changes to this Privacy Policy</strong>\n" +
    "                <ol>\n" +
    "                    <li>Weeloy reserves the right to update, revise, modify or amend this Privacy Policy in the following manner at any time as Weeloy deems necessary. Users are strongly recommended to review this Privacy Policy frequently. If Weeloy decides to update, revise, modify or amend this Privacy Policy, Weeloy shall post those changes to this webpage and/or other places Weeloy deems appropriate so that Users would be aware of what information Weeloy collects, how Weeloy uses it, and under what circumstances, if any, Weeloy discloses it. If Weeloy makes material changes to this Privacy Policy, Weeloy shall notify Users on this webpage, by email, or by means of a notice on the home page of Weeloy.\n" +
    "                    </li>\n" +
    "                </ol>\n" +
    "            </li>\n" +
    "            <li><strong> Governing Law</strong>\n" +
    "                <ol>\n" +
    "                    <li>This Privacy Policy is governed by the laws of Singapore. All Users and Weeloy agree to submit to the exclusive jurisdiction of the  Singapore courts in any dispute relating to this Privacy Policy.\n" +
    "                    </li>\n" +
    "                </ol>\n" +
    "            </li>\n" +
    "        </div>\n" +
    "\n" +
    "        <br>\n" +
    "        <br>\n" +
    "        <br>\n" +
    "        <br>\n" +
    "        <!-- /modal-footer -->\n" +
    "    </ol>\n" +
    "</div>\n" +
    "");
}]);

angular.module("../app/components/reset-password/_reset_password.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/components/reset-password/_reset_password.tpl.html",
    "<div id=\"reset-password\">\n" +
    "    <div class=\"container\">\n" +
    "        <div class=\"col-xs-12 col-md-6 col-md-offset-3\">\n" +
    "            <form ng-if=\"true && !success_update && !success_update_bo\" name=\"ResetPasswordForm\" ng-submit=\"ResetPasswordForm.$valid && ResetPassword(password, password_confirmation)\" novalidate>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <label>Please enter new password</label>\n" +
    "                    <input type=\"password\" class=\"form-control\" name=\"password\" ng-model=\"password\" ng-minlength=\"8\" ng-maxlength=\"12\" ng-pattern=\"/(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z])/\" required/>\n" +
    "                    <div class=\"error\">\n" +
    "                        <span ng-show=\"ResetPasswordForm.$submitted && ResetPasswordForm.password.$error.required\">Please enter your password</span>\n" +
    "\n" +
    "                        <span ng-show=\"!ResetPasswordForm.password.$error.required && (ResetPasswordForm.password.$error.minlength || ResetPasswordForm.password.$error.maxlength) && ResetPasswordForm.password.$dirty\">Passwords must be between 8 and 12 characters.</span>\n" +
    "                        <span ng-show=\"!ResetPasswordForm.password.$error.required && !ResetPasswordForm.password.$error.minlength && !ResetPasswordForm.password.$error.maxlength && ResetPasswordForm.password.$error.pattern && ResetPasswordForm.password.$dirty\">Must contain one lower &amp; uppercase letter, and one non-alpha character (a number or a symbol.)</span>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <label for=\"password_confirmation\">Confirm Password</label>\n" +
    "                    <input class=\"form-control\"  type=\"password\" name=\"password_confirmation\" ng-model=\"password_confirmation\" valid-password-c required  />\n" +
    "                    <div class=\"error\">\n" +
    "                        <span ng-show=\"ResetPasswordForm.password_confirmation.$error.required && ResetPasswordForm.password_confirmation.$dirty\">Please confirm your password.</span>\n" +
    "                        <span ng-show=\"!ResetPasswordForm.password_confirmation.$error.required && ResetPasswordForm.password_confirmation.$error.noMatch && ResetPasswordForm.password.$dirty\">Passwords do not match.</span>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <button type=\"submit\" class=\"btn btn-default\" ng-disabled=\"!ResetPasswordForm.$valid\">Reset Password</button>\n" +
    "                    <!--<input  ng-disabled=\"!ResetPasswordForm.$valid\" type=\"submit\" class=\"btn btn-default\" value=\"Reset Password\">-->\n" +
    "                </div>\n" +
    "            </form>\n" +
    "\n" +
    "            <div ng-if=\"success_update\" id=\"success-div-weeloy\"><img width=\"50\" ng-src=\"client/assets/images/loading_spinner.gif\">Your password has been updated, redirecting to <a href=\"https://www.weeloy.com\">weeloy.com</a></div>\n" +
    "            <div ng-if=\"success_update_bo\" id=\"success-div-backoffice\"><img width=\"50\" ng-src=\"client/assets/images/loading_spinner.gif\">Your password has been updated, redirecting to <a href=\"https://www.weeloy.com/backoffice\">backoffice</a></div>\n" +
    "\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>");
}]);

angular.module("../app/components/restaurant_group_page/_restaurant_group_page.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/components/restaurant_group_page/_restaurant_group_page.tpl.html",
    "<!--\n" +
    "        <section id=\"top-restaurant\" class=\"early_book_restaurant\">\n" +
    "    <div class=\"container\">\n" +
    "        <h1 class=\"title_home_page text-center\" ng-bind=\"info.title\"></h1>\n" +
    "        <h2 class=\"sub_title_home_page text-center\" ng-bind=\"info.sub_title\"></h2>\n" +
    "        <div id=\"best-restaurant\">\n" +
    "            <div class=\"col-lg-4 col-md- col-sm-6 col-xs-12\" ng-repeat=\"restaurant in restaurants track by $index\"  > \n" +
    "                <restaurant-item open-in-new-tab=\"true\" media-server=\"mediaServer\" restaurant=\"restaurant\" indexcount=\"$index\"></restaurant-item>\n" +
    "            </div>\n" +
    "            \n" +
    "            \n" +
    "        </div>\n" +
    "        \n" +
    "    </div>\n" +
    "</section>-->\n" +
    "\n" +
    "<section class=\"zen-month\">\n" +
    "<!--https://static4.weeloy.com/images/cny/book_restaurant_cny_2016.jpg-->\n" +
    "    <div class=\"container\">\n" +
    "        <h1>\n" +
    "            <div title='info.h1_seo_tag' ng-if='info.banner_url' class=\"banner_restaurant\" style=\"margin-top:50px;height: 300px;background-image: url('{{info.banner_url}}'); background-position: center center; background-repeat: no-repeat no-repeat;\"></div>\n" +
    "        </h1>\n" +
    "\n" +
    "        <div ng-if='!info.banner_url' style=\"height: 60px\"></div>\n" +
    "        \n" +
    "        <h2 class=\"text-center\" ng-bind=\"info.title\" style=\"padding-top: 20px;\"></h2>\n" +
    "        <p class=\"text-center zen-text\" ng-bind=\"info.sub_title\"></p>\n" +
    "        <p class='introduction' ng-bind=\"info.introduction\"></p>\n" +
    "        <div class=\"css-creen\">\n" +
    "            <div class=\"col-md-4 col-sm-4 col-xs-12 zen-column\"  ng-repeat=\"restaurant in restaurants track by $index\">\n" +
    "                <restaurant-item open-in-new-tab=\"true\" media-server=\"mediaServer\" restaurant=\"restaurant\" indexcount=\"$index\"></restaurant-item>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</section>\n" +
    "\n" +
    "");
}]);

angular.module("../app/components/restaurant_info_page/_restaurant_info_page.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/components/restaurant_info_page/_restaurant_info_page.tpl.html",
    "<div class=\"restaurant-info-page\" ng-if=\"restaurant != undefined\">\n" +
    "    <a href=\"{{restaurant.getBannerImage(bannerSizePath)}}\" class=\"fresco\">\n" +
    "        <div class=\"banner_restaurant\" ng-style=\"restaurant.bannerStyle(bannerSizePath)\" style=\"background-position: center center\"></div>\n" +
    "    </a>\n" +
    "    <div class=\"container middle\">\n" +
    "        <div class=\"col-lg-4 col-md-4 col-sm-4 col-xs-12 col-lg-push-8 col-md-push-8 col-sm-push-8 right\">\n" +
    "            <div class=\"trans-block\">\n" +
    "                <div class=\"bar-logo\"><img ng-src=\"{{restaurant.getLogoImage()}}\" class=\"img-responsive\" alt=\"\"></div>\n" +
    "                <div class=\"div-name-address\" style=\"border-radius: 5px;margin: 2%;\">\n" +
    "                    <h1 ng-bind=\"restaurant.getTitle()\"></h1>\n" +
    "                    <p><span ng-bind=\"restaurant.address\"></span>, <span ng-bind=\"restaurant.zip\"></span>\n" +
    "                        <br/>\n" +
    "                        <a target ='_blank' ng-if=\"restaurant.url!=''\" href=\"{{restaurant.trackingUrl}}\">Visit <span ng-bind=\"restaurant.getTitle()\"></span>  website</a>\n" +
    "                    </p>\n" +
    "                </div>\n" +
    "                <div class=\"circle-img\">\n" +
    "                    <p>\n" +
    "                        <a ng-href=\"{{restaurant.getBookNowPageUrl()}}\">\n" +
    "                            <button analytics-on=\"click\" analytics-event=\"c_book_resto\" analytics-category=\"c_book_resto_restopage1\" ng-show=\"restaurant.status != 'comingsoon'\" class=\"book-button btn-leftBottom-orange\" ng-bind=\"restaurant.book_button.text\"></button>\n" +
    "                        </a>\n" +
    "                        <button analytics-on=\"click\" analytics-event=\"c_cs_book_resto\" analytics-category=\"c_cs_book_resto_restopage1\" ng-show=\"restaurant.status == 'comingsoon'\" class=\"book-button btn-leftBottom-orange\" ng-bind=\"restaurant.book_button.text\"></button>\n" +
    "                    </p>\n" +
    "                    <a ng-if='restaurant.is_wheelable == 1' rel=\"nofollow\" href=\"{{restaurant.internal_path}}/dining-rewards\">\n" +
    "                        <img ng-src=\"{{ mediaServer }}/upload/wheelvalues/wheelvalue_{{restaurant.wheelvalue}}.png\" width=\"116\" height=\"114\" alt=\"\">\n" +
    "                    </a>\n" +
    "                    <div ng-if='restaurant.is_wheelable == 0' style='width:116px;height:114px'></div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"inner\">\n" +
    "                <div class=\"style\" ng-if='restaurant.is_wheelable == 1'>\n" +
    "                    <h2>{{Str.RestaurantInfoPage.Text_Best_Rewards_Of}} <span ng-bind=\"restaurant.getTitle()\"></span></h2>\n" +
    "                    <p ng-repeat=\"reward in BestOffers\" ng-bind=\"reward.offer\"></p>\n" +
    "                    <p>\n" +
    "                        <a rel=\"nofollow\" ng-href=\"{{restaurant.internal_path}}/dining-rewards\" ng-bind=\"Str.RestaurantInfoPage.Text_View_All\"></a>\n" +
    "                    </p>\n" +
    "                </div>\n" +
    "                <!-- CPP WHEEL OR PROMOTION CREDIT SUISSE -->\n" +
    "                <div class=\"style\" ng-if='restaurant.is_wheelable === \"1\" && affiliate_program'>\n" +
    "                    <h2><img  src=\"images/cpp_restaurant.jpg\"> <span> Corporate Program Wheel</span></h2>\n" +
    "                    <p>Enjoy better rewards with your corporate program</p>\n" +
    "                    <!--                    <p ng-repeat=\"reward in BestOffers\" ng-bind=\"reward.offer\"></p>-->\n" +
    "                    <p>\n" +
    "                        <a rel=\"nofollow\" ng-href=\"{{restaurant.internal_path}}/dining-rewards-corporate-program\">View all details now</a>\n" +
    "                    </p>\n" +
    "                </div>\n" +
    "                <!-- CPP WHEELLOR PROMOTION CREDIT SUISSE -->\n" +
    "                <div class=\"style\" ng-if=\"restaurant.is_wheelable == 0 && restaurant.best_offer.offer != 'NOT AVAILABLE' && restaurant.best_offer.offer \">\n" +
    "                    <h2>{{Str.RestaurantInfoPage.Instant_Reward}} <span ng-bind=\"restaurant.getTitle()\"></span></h2>\n" +
    "                    <p ng-bind=\"restaurant.best_offer.offer\"></p>\n" +
    "                </div>\n" +
    "                <div class=\"style\" ng-if=\"restaurant.is_wheelable == 0 && restaurant.best_offer.offer_cpp != 'NOT AVAILABLE' && restaurant.best_offer.offer_cpp && affiliate_program\">\n" +
    "                    <h2><img  src=\"images/cpp_restaurant.jpg\"> <span> Corporate Program Promotion</span></h2>\n" +
    "                    <p>Enjoy better rewards with your corporate program</p>\n" +
    "                    <p ng-bind=\"restaurant.best_offer.offer_cpp\"></p>\n" +
    "                </div>\n" +
    "                <div class=\"style\" ng-show=\"reviews.count > 0\">\n" +
    "                    <h2><span ng-bind=\"reviews.count\"></span> <span ng-if=\"reviews.count == 1\">review</span><span ng-if=\"reviews.count > 1\">reviews</span></h2>\n" +
    "                    <p><span class=\"reviews-menu starts stars{{reviews.score}}\"></span> <span ng-bind=\"reviews.score_desc\"></span></p>\n" +
    "                    <p><a style=\"cursor: pointer\" rel=\"nofollow\" ng-click=\"showReview()\">details</a></p>\n" +
    "                </div>\n" +
    "                <div class=\"style\">\n" +
    "                    <h2 ng-bind=\"Str.RestaurantInfoPage.Text_Cuisine\"></h2>\n" +
    "                    <p><span ng-repeat=\"cuisine in restaurant.getCuisineAsArray()\"><span ng-bind=\"cuisine\"></span><span ng-hide=\"$last\">, </span></span>\n" +
    "                    </p>\n" +
    "                </div>\n" +
    "                <div class=\"style\">\n" +
    "                    <h2 ng-bind=\"Str.RestaurantInfoPage.Text_Price\"></h2>\n" +
    "                    <p>\n" +
    "                        <span ng-if=\"restaurant.mealtype.toLowerCase().indexOf('lunch') > -1\">{{Str.RestaurantInfoPage.Text_Lunch}} {{ restaurant.pricing.lunch }}<br></span>\n" +
    "                        <span ng-if=\"restaurant.mealtype.toLowerCase().indexOf('dinner') > -1\">{{Str.RestaurantInfoPage.Text_Dinner}} {{ restaurant.pricing.dinner }}<br></span>\n" +
    "<!--                        <span ng-if=\"restaurant.mealtype.toLowerCase().indexOf('lunch') > -1 || restaurant.mealtype.toLowerCase().indexOf('dinner') > -1\">{{Str.RestaurantInfoPage.Text_Average}} {{ restaurant.pricing.average }}</span>-->\n" +
    "                    </p>\n" +
    "                </div>\n" +
    "                <div class=\"style last\" id=\"affix-book-btn\" ng-init=\"Affix()\">\n" +
    "                    <h2 ng-bind=\"Str.RestaurantInfoPage.Text_Opening_Hours\"></h2>\n" +
    "                    <ul class=\"business-hours\">\n" +
    "                        <li class=\"clearfix\" ng-if =\"(restaurant.restaurant != 'SG_SG_R_Audace') && (restaurant.restaurant != 'SG_SG_R_Pollen')\">\n" +
    "                            <div class=\"day\"></div>\n" +
    "                            <div class=\"lunch\" style=\"font-family: 'proximanova'; text-transform: uppercase;\" ng-bind=\"Str.RestaurantInfoPage.Text_Lunch\"></div>\n" +
    "                            <div class=\"dinner\" style=\"font-family: 'proximanova'; text-transform: uppercase;\" ng-bind=\"Str.RestaurantInfoPage.Text_Dinner\"></div>\n" +
    "                        </li>\n" +
    "                        <li class=\"clearfix\" style=\"font-family: 'proxima_novasemibold'\" ng-if =\"(restaurant.restaurant !=='SG_SG_R_Pollen') && (restaurant.restaurant !== 'SG_SG_R_Audace')\" ng-repeat=\"day in restaurant.openhours\">\n" +
    "                            <div class=\"day\" ng-bind=\"day.day\"></div>\n" +
    "                            <div class=\"lunch\" ng-bind=\"day.lunch\"></div>\n" +
    "                            <div class=\"dinner\" ng-bind=\"day.dinner\"></div>\n" +
    "                        </li>\n" +
    "                         <li class=\"clearfix\" style=\"font-family: 'proximanova-semibold'\" ng-if =\"(restaurant.restaurant === 'SG_SG_R_Pollen') || (restaurant.restaurant === 'SG_SG_R_Audace')\">\n" +
    "                            <div ng-repeat='specialday in specials'>\n" +
    "                                <div style=\"font-family: 'proximanova-semibold'\" ng-bind-html=\"specialday.item\"></div>\n" +
    "                            </div>\n" +
    "                             <!-- <span ><strong>Wednesday - Monday</strong> </span>\n" +
    "                             <div style=\"font-family: 'proxima_novasemibold'\">Lunch : 12:00 - 15:00 (Last Order 14:30)</div>\n" +
    "                             <div style=\"font-family: 'proxima_novasemibold'\">AfternoonTea :  15:00 - 17:00 (Last Order 16:30)</div>\n" +
    "\n" +
    "                             <div style=\"font-family: 'proxima_novasemibold'\">Dinner :  18:00 - 22:00 (Last Order 21:30)</div><br />\n" +
    "                             \n" +
    "                             <span style='padding-top:10px;'><strong>Tuesday (Terrace) </strong></span>\n" +
    "                              <div style=\"font-family: 'proxima_novasemibold'\">Lunch :  12:00 - 17:00</div>\n" +
    "                             <div style=\"font-family: 'proxima_novasemibold'\">Dinner :  17:00 - 21:00 (Last Order 20:30)</div><br /> -->\n" +
    "                         </li>\n" +
    "                    </ul>\n" +
    "                    <div style=\" text-align: center;\">\n" +
    "                        <a ng-href=\"{{restaurant.getBookNowPageUrl()}}\">\n" +
    "                            <button analytics-on=\"click\" analytics-event=\"c_book_resto\" analytics-category=\"c_book_resto_restopage2\" ng-show=\"disableBookButtton != true\" id=\"btn_affix\" class=\"book-button btn-leftBottom-orange\" ng-bind=\"restaurant.book_button.text\"></button>\n" +
    "                        </a>\n" +
    "                        <button analytics-on=\"click\" analytics-event=\"c_cs_book_resto\" analytics-category=\"c_cs_book_resto_restopage2\" ng-show=\"disableBookButtton\" id=\"btn_affix\" class=\"book-button btn-leftBottom-orange\" ng-bind=\"restaurant.book_button.text\"></button>\n" +
    "                    </div>\n" +
    "                    <div ng-if=\"false\" ng-show=\"showCart\">\n" +
    "                        <cart items=\"ItemsInCart\" ng-if=\"ItemsInCart | lengthMoreThan:0\"></cart>\n" +
    "                        <div class=\"order-now\" ng-if=\"ItemsInCart | lengthMoreThan:0\">\n" +
    "                            <a href=\"checkout\">\n" +
    "                                <button class=\"btn btn-default pull-right\">Order Now</button>\n" +
    "                            </a>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <!-- SHOPPING CART ON RESTAURANT PAGE -->\n" +
    "                    <!--                    <div class=\"cart-message\" ng-show=\"showCartMessage\">\n" +
    "                        <span class=\"fa fa-shopping-cart\"></span>\n" +
    "                        <a ng-href=\"{{ ItemsInCart[0].restaurant_path }}\">You have added products in an another store</a>\n" +
    "                    </div>-->\n" +
    "                    <!-- SHOPPING CART ON RESTAURANT PAGE -->\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"info_right\">\n" +
    "                <div class=\"chef\" ng-if=\"restaurant.chef.chef_name != '' && restaurant.chef.chef_name != null\">\n" +
    "                    <div class=\"img-block\" ng-if=\"restaurant.chef.chef_image != '' \">\n" +
    "                        <img ng-src=\"{{ mediaServer }}/upload/restaurant/{{restaurant.restaurant}}/{{restaurant.chef.chef_image}}\" style=\"width:70%;\" class=\"text-center\" alt=\"\">\n" +
    "                        <h2 ng-bind=\"restaurant.chef.chef_name\"></h2>\n" +
    "                    </div>\n" +
    "                    <h3 ng-if=\"restaurant.chef.chef_believe && chef_gender != 'female' && chef_gender != 'multiple'\" ng-bind=\"Str.RestaurantInfoPage.Text_His_Beliefs\"></h3>\n" +
    "                    <h3 ng-if=\"restaurant.chef.chef_believe && chef_gender == 'female'\" ng-bind=\"Str.RestaurantInfoPage.Text_Her_Beliefs\"></h3>\n" +
    "                    <h3 ng-if=\"restaurant.chef.chef_believe && chef_gender == 'multiple'\" ng-bind=\"Str.RestaurantInfoPage.Text_Their_Beliefs\"></h3>\n" +
    "                    \n" +
    "                    <p ng-bind=\"restaurant.chef.chef_believe\"></p>\n" +
    "                    <h3 ng-if=\"restaurant.chef.chef_description && chef_gender != 'female' && chef_gender != 'multiple'\" ng-bind=\"Str.RestaurantInfoPage.Text_His_Cusine\"></h3>\n" +
    "                    <h3 ng-if=\"restaurant.chef.chef_description && chef_gender == 'female'\" ng-bind=\"Str.RestaurantInfoPage.Text_Her_Cusine\"></h3>\n" +
    "                    <h3 ng-if=\"restaurant.chef.chef_description && chef_gender == 'multiple'\" ng-bind=\"Str.RestaurantInfoPage.Text_Their_Cusine\"></h3>\n" +
    "                    <div class=\"span4 collapse-group\">\n" +
    "                        <span ng-show=\"showReadmore\" ng-bind=\"restaurant.chef.chef_description\"></span>\n" +
    "                        <a class=\"btn read_more\" ng-hide=\"showReadmore\" ng-click=\"showReadmore = true\" ng-bind=\"Str.RestaurantInfoPage.ReadMore\"></a>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div id=\"map_canvas\" ng-init=\"loadmap(restaurant.getLatitude().lat, restaurant.getLatitude().lng, isMobile)\" ng-click=\"loadmap2\"></div>\n" +
    "                <div class=\"hidden-on-xs\" ng-include=\"'../app/shared/partial/_restaurant_services_and_share.tpl.html'\"></div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"col-lg-8 col-md-8 col-sm-8 col-xs-12 col-lg-pull-4 col-md-pull-4 col-sm-pull-4 left\">\n" +
    "            <nav id=\"merchant-detail-navigation\" ng-init=\"anchorAffix()\">\n" +
    "                <ul>\n" +
    "                    <li>\n" +
    "                        <a ng-href=\"#description\" disablejump>Description</a>\n" +
    "                        <!-- <a ng-href=\"#description')\" disablejump>Description</a> -->\n" +
    "                    </li>\n" +
    "                    <li>\n" +
    "                        <a ng-href=\"#menu\" disablejump>Menu</a>\n" +
    "                        <!-- <a ng-href=\"#menu')\" disablejump>Menu</a> -->\n" +
    "                    </li>\n" +
    "                    <li>\n" +
    "                        <a ng-href=\"#gallery\" disablejump>Gallery</a>\n" +
    "                        <!-- <a ng-href=\"#gallery')\" disablejump>Gallery</a> -->\n" +
    "                    </li>\n" +
    "                    <li ng-show=\"restaurant.video != ''\">\n" +
    "                        <a ng-href=\"#video\" disablejump>Video</a>\n" +
    "                        <!-- <a ng-href=\"#video')\">Video</a> -->\n" +
    "                    </li>\n" +
    "                     <li ng-show=\"showEventSection\">\n" +
    "                        <a ng-href=\"#events\" disablejump>Events</a>\n" +
    "                        <!-- <a ng-href=\"#video')\">Video</a> -->\n" +
    "                    </li>\n" +
    "                    <li ng-show=\"showReviewSection && reviews.count > 0\">\n" +
    "                        <a ng-href=\"#reviews\" disablejump>Reviews</a>\n" +
    "                        <!-- <a ng-click=\"scrollTo('reviews')\">Reviews</a> -->\n" +
    "                    </li>\n" +
    "                </ul>\n" +
    "            </nav>\n" +
    "            <div id=\"breadcrumb\" breadcrumb base-url=\"base_url\" base-path=\"base_path\" restaurant=\"restaurant\" style=\"padding-left: 0px\"></div>\n" +
    "            <div class=\"information\">\n" +
    "                <h1 id=\"description\" name=\"description\">{{Str.RestaurantInfoPage.Text_About}} {{ restaurant.getTitle() }}</h1>\n" +
    "                <p ng-repeat=\"description in restaurant.getDescription()\"><b ng-bind=\"description.title\"></b>\n" +
    "                    <span ng-repeat=\"line in description.body\"><br>{{ line }}</span>\n" +
    "                </p>\n" +
    "                <div class=\"video\" id=\"video\" name=\"video\" ng-show=\"restaurant.video != ''\">\n" +
    "                    <iframe ng-src=\"{{ restaurant.video | trustAsResource }}\" style=\"width:100%; height:320px\" frameborder=\"0\" webkitallowfullscreen=\"\" mozallowfullscreen=\"\" allowfullscreen=\"\"></iframe>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div id=\"menu\" name=\"menu\" class=\"menu\" ng-show=\"restaurant.menu != undefined && restaurant.menu != '' && restaurant.menu != null\">\n" +
    "                <h1>Menu of {{restaurant.getTitle()}} {{takeoutitle}}</h1>\n" +
    "                <div class=\"panel-menu-group\" id=\"accordion\" role=\"tablist\" aria-multiselectable=\"true\">\n" +
    "                    <div class=\"panel-menu panel-menu-default\" ng-repeat=\"menu in restaurant.menu\">\n" +
    "                        <div class=\"panel-menu-heading\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapse-{{$index}}\" aria-expanded=\"false\" aria-controls=\"collapse1\" class=\"collapsed\">\n" +
    "                            <span class=\"menu-heading\" ng-bind=\"menu.categorie.value\"></span>\n" +
    "                            <i class=\"fa fa-angle-down\"></i>\n" +
    "                        </div>\n" +
    "                        <div id=\"collapse-{{$index}}\" class=\"panel-collapse collapse\">\n" +
    "                            <div class=\"panel-body\">\n" +
    "                                <ul>\n" +
    "                                    <li ng-repeat=\"MenuItem in menu.items\">\n" +
    "                                        <div class=\"menu-item-header\">\n" +
    "                                            <div class=\"pull-left\">\n" +
    "                                                <a ng-if=\"false && MenuItem.mimage !== '' && takeoutitle !== ''\" title=\"{{MenuItem.mimage}}\"><img ng-src=\"http://media5.weeloy.com/upload/restaurant/SG_SG_R_MadrinaaItaliano/140/Food2.jpg\"></a>\n" +
    "                                                <span class=\"menu-title\" ng-bind=\"MenuItem.item_title\"></span>\n" +
    "                                                <img ng-if=\"MenuItem.spicy1 == 1\" ng-src=\"images/restaurant_icons/spicy1.png\">\n" +
    "                                                <img ng-if=\"MenuItem.spicy2 == 1\" ng-src=\"images/restaurant_icons/spicy2.png\">\n" +
    "                                                <img ng-if=\"MenuItem.spicy3 == 1\" ng-src=\"images/restaurant_icons/spicy3.png\">\n" +
    "                                                <img ng-if=\"MenuItem.vegi == 1\" ng-src=\"images/restaurant_icons/vegi.png\">\n" +
    "                                                <img ng-if=\"MenuItem.chef_reco == 1\" ng-src=\"images/restaurant_icons/chef_reco.png\">\n" +
    "                                                <img ng-if=\"MenuItem.halal == 1\" ng-src=\"images/restaurant_icons/halal.png\">\n" +
    "                                            </div>\n" +
    "                                            <div class=\"price-and-cart text-right\">\n" +
    "                                                <span ng-show=\"MenuItem.price!= undefined && MenuItem.price != null && MenuItem.price != 0    \">{{restaurant.currency}} {{MenuItem.price}}</span>\n" +
    "                                                <span ng-if=\"false && MenuItem.takeout == 1 && takeoutitle !== ''\" class=\"cart-label label label-info\" ng-click=\"addToCart(MenuItem, $event)\">\n" +
    "                                                    <i class=\"fa fa-plus\"></i>\n" +
    "                                                    <span>Add to Cart</span>\n" +
    "                                                </span>\n" +
    "                                            </div>\n" +
    "                                        </div>\n" +
    "                                        <p class=\"menu-description\" ng-bind=\"MenuItem.item_description\"></p>\n" +
    "                                    </li>\n" +
    "                                </ul>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"events\" ng-show=\"showEventSection\">\n" +
    "                <h1 id =\"events\">Events of {{restaurant.getTitle()}}</h1>\n" +
    "                <ul>\n" +
    "                    <li class=\"clearfix\" ng-repeat=\"evt in RestaurantEvents\">\n" +
    "                        <div class=\"cnt\">\n" +
    "                            <h2 ng-bind=\"evt.name\"></h2>\n" +
    "                            <span>{{evt.getStartTime('MMM DD, YYYY')}} to {{evt.getEndTime('MMM DD, YYYY')}} in {{evt.city}}</span>\n" +
    "                            <p ng-bind=\"evt.description\"></p>\n" +
    "                            <a href=\"{{ mediaServer }}/upload/restaurant/{{RestaurantID}}/{{evt.pdf_link}}\" target=\"_blank\" ng-if=\"evt.pdf_link\" title=\"{{evt.title}} year end festive special menus 2016\">Download Menus</a>\n" +
    "                            <div class=\"event-book-btn\" ng-if=\"evt.getPrice() != ''\">\n" +
    "                                <a ng-href=\"{{ evt.getEventBookingUrl(restaurant) }}\">\n" +
    "                                    <button class=\"book-button book-button-sm btn-leftBottom-orange\">Book Now</button>\n" +
    "                                </a>\n" +
    "                            </div>\n" +
    "                          \n" +
    "                            \n" +
    "                        </div>\n" +
    "                        <div ng-if=\"(evt.picture!='' || evt.picture!= null)\" class=\"img-block\">\n" +
    "                            <a href=\"{{ mediaServer }}/upload/restaurant/{{RestaurantID}}/300/{{evt.picture}}\" class=\"fresco\" data-fresco-caption=\"{{ evt.name }} - weeloy.com\" data-fresco-group=\"event\">\n" +
    "                                <img ng-src=\"{{ mediaServer }}/upload/restaurant/{{RestaurantID}}/300/{{evt.picture}}\" class=\"img-responsive\" alt=\"{{ evt.name }} - weeloy.com\">\n" +
    "                            </a>\n" +
    "                        </div>\n" +
    "                    </li>\n" +
    "                </ul>\n" +
    "            </div>\n" +
    "            <div class=\"gallery clearfix\">\n" +
    "                <h1 id=\"gallery\" name=\"gallery\">{{Str.RestaurantInfoPage.Text_Gallery_Of}} {{ restaurant.getTitle() }}</h1>\n" +
    "                <ul class=\"clearfix\">\n" +
    "                    <li ng-repeat=\"image in restaurant.pictures\" class=\"{{ ($index < 2) ? 'first' : ($index < 5) ? 'second' : '' }}\">\n" +
    "                        <a ng-href=\"{{ mediaServer }}/upload/restaurant/{{ restaurant.restaurant }}/1440/{{ image.name }}\" class=\"fresco\" data-fresco-caption=\"{{ image.tag }} - weeloy.com\" data-fresco-group=\"restau\">\n" +
    "                            <img ng-if=\"$index < 5\" ng-src=\"{{ mediaServer }}/upload/restaurant/{{ restaurant.restaurant }}/180/{{ image.name}}\" alt=\"{{ image.tag }} - weeloy.com\">\n" +
    "                        </a>\n" +
    "                    </li>\n" +
    "                </ul>\n" +
    "            </div>\n" +
    "            <div class=\"gallery clearfix\" ng-show=\"restaurant.takeoutrestaurant == 1 && showCateringPictures && CateringPictures.length   > 0\">\n" +
    "                <h1>Products of {{ restaurant.getTitle() }} available for catering</h1>\n" +
    "                <ul class=\"clearfix\">\n" +
    "                    <li ng-repeat=\"image in CateringPictures\" class=\"{{ ($index < 0) ? 'first' : ($index < 6) ? 'second' : '' }}\">\n" +
    "                        <a ng-href=\"{{restaurant.internal_path}}/catering\">\n" +
    "                            <img ng-if=\"$index < 6\" ng-src=\"{{ mediaServer }}/upload/restaurant/{{ restaurant.restaurant }}/300/{{ image.name }}\" alt=\"{{ restaurant.getTitle() }} - weeloy.com\">\n" +
    "                        </a>\n" +
    "                    </li>\n" +
    "                </ul>\n" +
    "                <div class='col-lg-4 col-md-4 col-sm-4 col-xs-12 col-lg-push-8 col-md-push-8 col-sm-push-8 '>\n" +
    "                    <a id=\"btn_affix\" class=\"btn btn-book2 custom_button\" ng-href=\"{{restaurant.internal_path}}/catering\" style=\"font-size:18px;width:100%; background-color: {{restaurant.book_button.color}}\">ORDER NOW</a>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div id=\"reviews\" name=\"reviews\" class=\"reviews-section\" ng-show=\"showReviewSection && reviews.count > 0\">\n" +
    "                <h1><p class=\"pull-left\" ><span ng-bind=\"reviews.count\"></span> <span ng-show=\"reviews.count > 1\">reviews</span><span ng-show=\"reviews.count == 1\">review</span></p><p class=\"overflowhidden\"><span class=\"reviews-menu reviews-menu-right stars stars{{reviews.score| number:0}}  pull-right\"></span><span class=\"pull-right\" ng-bind=\"reviews.score_desc\" style='padding-top: 10px;'></span></p></h1>\n" +
    "                <ul>\n" +
    "                    <li ng-repeat=\"review in reviews.reviews | startFrom:currentPage*pageSize | limitTo:pageSize\">\n" +
    "\n" +
    "                        <div class=\"cnt\">\n" +
    "                            <div class='review-label'>\n" +
    "                                <span style=\"float:left;padding-top:10px;padding-right:20px;width:30%!important\" >\n" +
    "                                   <span  ng-if='review.user_name' style='width:10px!important;overflow: hidden;'>\n" +
    "                                       <span ng-bind=\"review.user_name\" style='width:10px!important;overflow: hidden;'></span>, \n" +
    "                                   </span>\n" +
    "                                    <span  ng-if='!review.user_name'>\n" +
    "                                       <span>...,</span>\n" +
    "                                   </span>\n" +
    "                                    <span  ng-if='review.time'>\n" +
    "                                       <span ng-bind=\"review.time\" style='padding-left:10px;'></span>, \n" +
    "                                   </span>\n" +
    "                                    \n" +
    "                                </span> \n" +
    "                                <p maxlength=\"15\" href ng-if=\"review.comment !== ''\" data-toggle=\"popover\" data-placement=\"top\" title=\"{{review.comment}}\" onmouseenter=\"$(this).popover('show')\" onmouseleave=\"$(this).popover('hide')\"  style='float: left;padding-right:10px;padding-top:7px;'>{{review.comment}}</p>\n" +
    "<!--                               <p  maxlength=\"15\" ng-bind=\"review.comment\" popover=\"{{review.comment}} \" data-popover-trigger=\"mouseenter\" style='float: left;padding-right:10px;padding-top:7px;'> </p>-->\n" +
    "                               <div class=\"reviews-menu reviews-menu-right stars stars{{review.score | number:0}}\" style='float:right;padding-top:15px;padding-left:25px;'></div>\n" +
    "                               <br style=\"clear: left;\" />\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </li>\n" +
    "              \n" +
    "                    <div style=\"margin-left:45%;\">\n" +
    "                        <a href=\"#\"> <span ng-hide=\"currentPage == 0\" class=\"glyphicon glyphicon-fast-backward\" ng-click=\"currentPage = currentPage-1\"></span></a>\n" +
    "                            {{currentPage+1}}/{{numberOfPages()}}\n" +
    "                            <a href=\"#\"><span ng-hide=\"currentPage >= reviews.reviews.length/pageSize - 1\" class=\"glyphicon glyphicon-step-forward\" ng-click=\"currentPage=currentPage+1\"></span></a>\n" +
    "                    </div>\n" +
    "\n" +
    "                </ul>\n" +
    "            </div>\n" +
    "            <div class=\"hidden-on-md\" ng-include=\"'../app/shared/partial/_restaurant_services_and_share.tpl.html'\"></div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("../app/components/search_page/_search_page.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/components/search_page/_search_page.tpl.html",
    "<section class=\"content-sec search-section\" caculate-map-content-maxheight>\n" +
    "    <div class=\"container-fluid\" style='max-width: 1380px'>\n" +
    "        <div class=\"search-section\" ng-class=\"SearchSectionClasses\">\n" +
    "            <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">\n" +
    "\n" +
    "                <nf-search id=\"search-bar-div\"\n" +
    "                           class=\"col-lg-10 col-md-10 col-sm-12 col-xs-12 col-lg-offset-1 col-md-offset-1\"\n" +
    "                           is-mobile=\"isMobile\"\n" +
    "                           cities=\"cities\" placeholder=\"Str.template.SearchInputPlaceHolder\"\n" +
    "                           button-text=\"Str.template.SearchBtnText\"></nf-search>\n" +
    "                <div ng-if=\"!isMobile\" id=\"breadcrumb\" breadcrumb base-url=\"base_url\" base-path=\"base_path\" data=\"SearchParams\"></div>\n" +
    "                <section id=\"result_search\" class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">\n" +
    "                    <div class=\"noresult col-lg-10 col-md-10 col-sm-12 col-xs-12 col-lg-offset-1 col-md-offset-1\"\n" +
    "                         ng-show=\"showNoResultMessage\">\n" +
    "                        <p>No Result Found</p>\n" +
    "                        <p class=\"all\">All restaurants in <span ng-bind=\"SearchParams.city.data\"></span>:</p>\n" +
    "                    </div>\n" +
    "                    <div style=\"position:relative; overflow: auto; height: 100px\" ng-if=\"showSpinner\">\n" +
    "                        <spinner></spinner>\n" +
    "                    </div>\n" +
    "                    <div class=\"search-result-item \" ng-class=\"RestaurantItemClasses\"\n" +
    "                         ng-repeat=\"restaurant in restaurantsInMap\"\n" +
    "                         ng-if=\"$index >= skip && $index < skip + restaurantsPerPage\">\n" +
    "                        <restaurant-item restaurant=\"restaurant\" media-server=\"mediaServer\"\n" +
    "                                         open-in-new-tab=\"true\"></restaurant-item>\n" +
    "                    </div>\n" +
    "                </section>\n" +
    "                <section class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">\n" +
    "                    <div class=\"text-center\">\n" +
    "                        <ul class=\"pagination\" ng-show=\"showPagination\">\n" +
    "                            <li ng-show=\"showPrevBtn\" ng-click=\"setPage(page -1)\">\n" +
    "                                <a href=\"#\" aria-label=\"Previous\">\n" +
    "                                    <span ng-click=\"previous()\" aria-hidden=\"true\"><</span>\n" +
    "                                </a>\n" +
    "                            </li>\n" +
    "                            <li ng-repeat=\"pageNum in pageNumbers track by $index\" ng-class=\"{active: pageNum == page}\"\n" +
    "                                ng-click=\"setPage(pageNum)\">\n" +
    "                                <a href=\"#\">{{pageNum}}</a>\n" +
    "                            </li>\n" +
    "                            <li ng-show=\"showNextBtn\" ng-click=\"setPage(page + 1)\">\n" +
    "                                <a href=\"#\" aria-label=\"Next\">\n" +
    "                                    <span aria-hidden=\"true\">></span>\n" +
    "                                </a>\n" +
    "                            </li>\n" +
    "                        </ul>\n" +
    "                    </div>\n" +
    "                </section>\n" +
    "            </div>\n" +
    "            <div class=\"final_box\">\n" +
    "                <div class=\"col-lg-10 col-md-10 col-sm-12 col-xs-12 col-lg-offset-1 col-md-offset-1\">\n" +
    "                    <div class=\"col-lg-6 col-md-6 col-sm-6 showcase-image\">\n" +
    "                        <a ng-href=\"{{restaurantShowCase.internal_path}}\">\n" +
    "                            <img style='max-width:100px; position:absolute;top:-20px;left:-20px;'\n" +
    "                                 ng-src=\"{{ mediaServer }}/upload/wheelvalues/wheelvalue_{{restaurantShowCase.wheelvalue}}.png\"\n" +
    "                                 alt=\"\"/>\n" +
    "                            <img ng-src=\"{{ mediaServer }}/upload/restaurant/{{restaurantShowCase.restaurant}}/360/{{restaurantShowCase.image}}\"/>\n" +
    "                            <span class='final_round'>  </span>\n" +
    "                        </a>\n" +
    "                    </div>\n" +
    "                    <div class=\"col-md-6 col-sm-6\">\n" +
    "                        <div class='over'>\n" +
    "                            <center>\n" +
    "                                <h1><a ng-href=\"{{restaurantShowCase.internal_path}}\"> {{restaurantShowCase.title}} </a>\n" +
    "                                </h1></center>\n" +
    "                            <p class=\"sec_text\">{{restaurantShowCase.cuisine}} </p>\n" +
    "                            <p class=\"dummy\">{{restaurantShowCase.description[0].title}}</p>\n" +
    "                            <p class=\"dummy\">{{restaurantShowCase.description[0].body[0]}}</p>\n" +
    "                            <div class=\"book\">\n" +
    "                                <a ng-href=\"{{restaurantShowCase.internal_path}}\">\n" +
    "                                    <button ng-if=\"restaurantShowCase.status=='active'\" id=\"btn_book\"\n" +
    "                                            class=\"book-button-sm btn-leftBottom-orange\"\n" +
    "                                            ng-click=\"go_restaurant(restaurantShowCase.restaurant)\">BOOK NOW\n" +
    "                                    </button>\n" +
    "                                    <!-- <button ng-if=\"restaurantShowCase.status=='active'\" id=\"btn_book\" class=\"btn custom_button {{restaurantShowCase.book_button.style}}\" ng-click=\"go_restaurant(restaurantShowCase.restaurant)\">BOOK NOW</button> -->\n" +
    "                                </a>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div id=\"google-map\" ng-class=\"GoogleMapSectionClasses\">\n" +
    "            <div id=\"expand-map-content\">\n" +
    "                <div>\n" +
    "                    <span class=\"glyphicon glyphicon-arrow-left\" ng-class=\"(MapExpanded) ? 'map-arrow-rotate' : ''\"\n" +
    "                          ng-click=\"ExpandMap()\"></span>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div id=\"map-canvas\" map=\"restaurants\" media-server=\"mediaServer\" restaurants-per-page=\"restaurantsPerPage\"\n" +
    "                 page=\"page\" restaurants-in-map=\"restaurantsInMap\" fit-bounds></div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</section>\n" +
    "");
}]);

angular.module("../app/components/search_page/_search_tag.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/components/search_page/_search_tag.tpl.html",
    "<section class=\"content-sec search-section\" caculate-map-content-maxheight>\n" +
    "    <div class=\"container-fluid\" style='max-width: 1380px'>\n" +
    "        <div class=\"search-section\" ng-class=\"SearchSectionClasses\">\n" +
    "            <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">\n" +
    "\n" +
    "                <nf-search id=\"search-bar-div\"\n" +
    "                           class=\"col-lg-10 col-md-10 col-sm-12 col-xs-12 col-lg-offset-1 col-md-offset-1\"\n" +
    "                           is-mobile=\"isMobile\"\n" +
    "                           cities=\"cities\" placeholder=\"Str.template.SearchInputPlaceHolder\"\n" +
    "                           button-text=\"Str.template.SearchBtnText\"></nf-search>\n" +
    "                <div ng-if=\"!isMobile\" id=\"breadcrumb\" breadcrumb base-url=\"base_url\" base-path=\"base_path\" data=\"SearchParams\"></div>\n" +
    "                <section id=\"result_search\" class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">\n" +
    "                    <div class=\"noresult col-lg-10 col-md-10 col-sm-12 col-xs-12 col-lg-offset-1 col-md-offset-1\"\n" +
    "                         ng-show=\"showNoResultMessage\">\n" +
    "                        <p>No Result Found</p>\n" +
    "                        <p class=\"all\">All restaurants in <span ng-bind=\"SearchParams.city.data\"></span>:</p>\n" +
    "                    </div>\n" +
    "                    <div style=\"position:relative; overflow: auto; height: 100px\" ng-if=\"showSpinner\">\n" +
    "                        <spinner></spinner>\n" +
    "                    </div>\n" +
    "                    <div class=\"search-result-item \" ng-class=\"RestaurantItemClasses\"\n" +
    "                         ng-repeat=\"restaurant in restaurantsInMap\"\n" +
    "                         ng-if=\"$index >= skip && $index < skip + restaurantsPerPage\">\n" +
    "                        <restaurant-item restaurant=\"restaurant\" media-server=\"mediaServer\"\n" +
    "                                         open-in-new-tab=\"true\"></restaurant-item>\n" +
    "                    </div>\n" +
    "                </section>\n" +
    "                <section class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">\n" +
    "                    <div class=\"text-center\">\n" +
    "                        <ul class=\"pagination\" ng-show=\"showPagination\">\n" +
    "                            <li ng-show=\"showPrevBtn\" ng-click=\"setPage(page -1)\">\n" +
    "                                <a href=\"#\" aria-label=\"Previous\">\n" +
    "                                    <span ng-click=\"previous()\" aria-hidden=\"true\"><</span>\n" +
    "                                </a>\n" +
    "                            </li>\n" +
    "                            <li ng-repeat=\"pageNum in pageNumbers track by $index\" ng-class=\"{active: pageNum == page}\"\n" +
    "                                ng-click=\"setPage(pageNum)\">\n" +
    "                                <a href=\"#\">{{pageNum}}</a>\n" +
    "                            </li>\n" +
    "                            <li ng-show=\"showNextBtn\" ng-click=\"setPage(page + 1)\">\n" +
    "                                <a href=\"#\" aria-label=\"Next\">\n" +
    "                                    <span aria-hidden=\"true\">></span>\n" +
    "                                </a>\n" +
    "                            </li>\n" +
    "                        </ul>\n" +
    "                    </div>\n" +
    "                </section>\n" +
    "            </div>\n" +
    "            <div class=\"final_box\">\n" +
    "                <div class=\"col-lg-10 col-md-10 col-sm-12 col-xs-12 col-lg-offset-1 col-md-offset-1\">\n" +
    "                    <div class=\"col-lg-6 col-md-6 col-sm-6 showcase-image\">\n" +
    "                        <a ng-href=\"{{restaurantShowCase.internal_path}}\">\n" +
    "                            <img style='max-width:100px; position:absolute;top:-20px;left:-20px;'\n" +
    "                                 ng-src=\"{{ mediaServer }}/upload/wheelvalues/wheelvalue_{{restaurantShowCase.wheelvalue}}.png\"\n" +
    "                                 alt=\"\"/>\n" +
    "                            <img ng-src=\"{{ mediaServer }}/upload/restaurant/{{restaurantShowCase.restaurant}}/360/{{restaurantShowCase.image}}\"/>\n" +
    "                            <span class='final_round'>  </span>\n" +
    "                        </a>\n" +
    "                    </div>\n" +
    "                    <div class=\"col-md-6 col-sm-6\">\n" +
    "                        <div class='over'>\n" +
    "                            <center>\n" +
    "                                <h1><a ng-href=\"{{restaurantShowCase.internal_path}}\"> {{restaurantShowCase.title}} </a>\n" +
    "                                </h1></center>\n" +
    "                            <p class=\"sec_text\">{{restaurantShowCase.cuisine}} </p>\n" +
    "                            <p class=\"dummy\">{{restaurantShowCase.description[0].title}}</p>\n" +
    "                            <p class=\"dummy\">{{restaurantShowCase.description[0].body[0]}}</p>\n" +
    "                            <div class=\"book\">\n" +
    "                                <a ng-href=\"{{restaurantShowCase.internal_path}}\">\n" +
    "                                    <button ng-if=\"restaurantShowCase.status=='active'\" id=\"btn_book\"\n" +
    "                                            class=\"book-button-sm btn-leftBottom-orange\"\n" +
    "                                            ng-click=\"go_restaurant(restaurantShowCase.restaurant)\">BOOK NOW\n" +
    "                                    </button>\n" +
    "                                    <!-- <button ng-if=\"restaurantShowCase.status=='active'\" id=\"btn_book\" class=\"btn custom_button {{restaurantShowCase.book_button.style}}\" ng-click=\"go_restaurant(restaurantShowCase.restaurant)\">BOOK NOW</button> -->\n" +
    "                                </a>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div id=\"google-map\" ng-class=\"GoogleMapSectionClasses\">\n" +
    "            <div id=\"expand-map-content\">\n" +
    "                <div>\n" +
    "                    <span class=\"glyphicon glyphicon-arrow-left\" ng-class=\"(MapExpanded) ? 'map-arrow-rotate' : ''\"\n" +
    "                          ng-click=\"ExpandMap()\"></span>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div id=\"map-canvas\" map=\"restaurants\" media-server=\"mediaServer\" restaurants-per-page=\"restaurantsPerPage\"\n" +
    "                 page=\"page\" restaurants-in-map=\"restaurantsInMap\" fit-bounds></div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</section>\n" +
    "");
}]);

angular.module("../app/components/section_booking/_section_booking.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/components/section_booking/_section_booking.tpl.html",
    "<div class=\"restaurant-info-page\" ng-if=\"restaurant != undefined\">\n" +
    "    <div class=\"text-center\" ng-style=\"restaurant.bannerStyleBookNow(bannerSizePath)\" style=\"overflow: hidden;\">\n" +
    "        <div class=\"container\">\n" +
    "            <div id=\"breadcrumb\" breadcrumb base-url=\"base_url\" base-path=\"base_path\" restaurant=\"restaurant\"></div>\n" +
    "            <div class=\"section-booking\">\n" +
    "                <div class=\"section-container col-sm-6 col-sm-offset-3\">\n" +
    "                    <div class=\"section-background\">\n" +
    "                        <div class=\"section row\">\n" +
    "                            <div class=\"section-item col-sm-12\" ng-repeat=\"item in SectionBooking\">\n" +
    "                                <div class=\"section-title col-sm-12\">\n" +
    "                                    <input id=\"section-input-{{ $index }}\" type=\"radio\" name=\"input-section\" ng-model=\"selectedSectionID\" ng-value=\"item.section_id\" ng-click=\"changeSectionRadio(item)\"><label for=\"section-input-{{ $index }}\">{{item.name || item.section_name}}</label>\n" +
    "                                </div>\n" +
    "                                <div class=\"section-desc col-sm-12 form-control\">\n" +
    "                                    <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</span>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                        <div class=\"pax-dinner row\">\n" +
    "                            <div class=\"pax col-sm-6\">\n" +
    "                                <select ng-model=\"pax\" ng-options=\"option as option for option in paxs\" class=\"form-control\" ng-change=\"changePax(pax)\">\n" +
    "                                </select>\n" +
    "                            </div>\n" +
    "                            <div class=\"dinner col-sm-6\">\n" +
    "                                <select ng-model=\"type\" class=\"form-control\" ng-options=\"option as option.name for option in types\" ng-change=\"changeType(type)\">\n" +
    "                                </select>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                        <div class=\"book-order row\">\n" +
    "                            <div class=\"row\">\n" +
    "                                <div class='col-sm-6 col-sm-offset-3 date-picker'>\n" +
    "                                    <div class=\"form-group\">\n" +
    "                                        <div date-picker enabled-dates=\"enabledDates\" selected-date=\"selectedDate\" class=\"input-group date datetime-picker\"></div>\n" +
    "                                    </div>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                            <div class=\"row\">\n" +
    "                                <div class=\"col-sm-6 col-sm-offset-3 hour-picker\">\n" +
    "                                    <div class=\"hour-picker-container\">\n" +
    "                                        <select ng-model=\"time\" class=\"form-control\" ng-options=\"option as option.time_name for option in listTime\" ng-change=\"changeTime(time)\">\n" +
    "                                        </select>\n" +
    "                                    </div>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                            <div class=\"row\">\n" +
    "                                <div class=\"col-sm-3 col-sm-offset-8 book-button\">\n" +
    "                                    <button type=\"button\" class=\"btn\" ng-click=\"bookNow()\">Book Now</button>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("../app/components/terms_and_conditions_of_service_page/_terms_and_coditions_of_service.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/components/terms_and_conditions_of_service_page/_terms_and_coditions_of_service.tpl.html",
    "<link href='https://fonts.googleapis.com/css?family=Dosis:400,600,700bold|Roboto:400,900,700italic,700,500italic,500,400italic' rel='stylesheet' type='text/css'>\n" +
    "<style>\n" +
    "#terms-and-conditions {\n" +
    "    padding-top: 40px;\n" +
    "    font-size: 11px;\n" +
    "    font-family: Roboto;\n" +
    "    margin: 0 0 0 10px;\n" +
    "}\n" +
    "\n" +
    "ol p {\n" +
    "    margin: 20px 10px 10px 0;\n" +
    "    text-justify: inter-word;\n" +
    "    text-align: justify;\n" +
    "}\n" +
    "\n" +
    "ol li {\n" +
    "    margin: 10px 20px 0 0;\n" +
    "    text-justify: inter-word;\n" +
    "    text-align: justify;\n" +
    "}\n" +
    "\n" +
    "ol {\n" +
    "    margin: 10px 0 0 0;\n" +
    "}\n" +
    "\n" +
    "h5 {\n" +
    "    color: red;\n" +
    "    text-transform: uppercase;\n" +
    "    margin: 30px 0 10px 0;\n" +
    "}\n" +
    "\n" +
    "#section {\n" +
    "    margin: 0 0 0 -40;\n" +
    "}\n" +
    "\n" +
    "ol {\n" +
    "    counter-reset: section;\n" +
    "    list-style-type: none;\n" +
    "}\n" +
    "\n" +
    "ol li {\n" +
    "    list-style-type: none;\n" +
    "}\n" +
    "\n" +
    "ol li ol {\n" +
    "    counter-reset: subsection;\n" +
    "}\n" +
    "\n" +
    "ol li ol li ol {\n" +
    "    counter-reset: subsubsection;\n" +
    "}\n" +
    "\n" +
    "ol li:before {\n" +
    "    counter-increment: section;\n" +
    "    content: counter(section) \". \";\n" +
    "    /*content:\"Section \" counter(section) \". \";*/\n" +
    "    font-weight: bold;\n" +
    "}\n" +
    "\n" +
    "ol li ol li:before {\n" +
    "    counter-increment: subsection;\n" +
    "    content: counter(section) \".\" counter(subsection) \" \";\n" +
    "}\n" +
    "\n" +
    "ol li ol li ol li:before {\n" +
    "    counter-increment: subsubsection;\n" +
    "    content: counter(section) \".\" counter(subsection) \".\" counter(subsubsection) \" \";\n" +
    "}\n" +
    "</style>\n" +
    "<div id=\"terms-and-conditions\">\n" +
    "    <ol>\n" +
    "        <div class=\"modal-header\">\n" +
    "            <button type=\"button\" class=\"close btn-primary\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>\n" +
    "            <h4 class=\"modal-title\">Terms and Conditions of Service</h4>\n" +
    "        </div>\n" +
    "        <!-- /modal-header -->\n" +
    "        <div class=\"modal-body\">\n" +
    "            <p>\n" +
    "                Weeloy.com, Weeloy.Dining.com and its sub-domains (hereinafter collectively referred to as the \"Sites\") are powered by Weeloy Pte. Ltd. (hereinafter referred to as \"<strong>Weeloy</strong>\").\n" +
    "            </p>\n" +
    "            <p>\n" +
    "                Weeloy and/or affiliate(s) hereinafter collectively referred to as “<strong>Weeloy</strong>”; if the context requires or permits, “Weeloy” may refer to any one of Weeloy’s and/or affiliate(s) that also operate and provide services via, apart from the Sites, other media platforms or other websites (hereinafter referred to as the \"Platforms\") and applications including mobile applications (hereinafter referred to as the \"Applications\") which are developed in whole or in part by the Weeloy.\n" +
    "            </p>\n" +
    "            <p>\n" +
    "                The Sites, the Platforms and the Applications may be referred to collectively as the \"Channels\" hereinafter. The services provided through the Channels shall only be available to Users, registered or non-registered, uploading, posting, viewing, booking, forwarding and/or otherwise using the advertisements, promotional materials, views, comments and/or other information on the Channels (hereinafter collectively referred to as the “Users”).\n" +
    "            </p>\n" +
    "            <p>\n" +
    "                Access to and use of the contents and services provided on the Channels shall be subject to the WeeloyPrivacy Policy and the terms and conditions, which are stated below and hereinafter referred to as the \"Terms and Conditions\". By using the Channels and any other site and/or media platforms and/or applications accessed through such Channels, Users acknowledge and agree that the Weeloy Privacy Policy and the Terms and Conditions set out below are binding upon them. If a User does not accept either or both of the Weeloy Privacy Policy and the Terms and Conditions, please do not use the Channels.\n" +
    "            </p>\n" +
    "            <p>\n" +
    "                Weeloy reserves the right, at its own discretion, to revise the Weeloy Privacy Policy and the Terms and Conditions at any time without prior notice. Once posted on the Channels, the amended Weeloy Privacy Policy and the Terms and Conditions shall apply to all Users. Users are advised to visit this page periodically to review the latest Weeloy Privacy Policy and the Terms and Conditions.\n" +
    "            </p>\n" +
    "            <p>\n" +
    "                Continued use shall constitute acceptance of all changes and shall remain binding upon the User; unless User serves his/her notice to Weeloy that such changes are unacceptable at which time access to the Channels and the Services (as defined herein) will be terminated.\n" +
    "            </p>\n" +
    "            <p>\n" +
    "                By using the Channels, Users also acknowledge and agree that, if applicable, in addition to being bound by the Terms and Conditions stated herein, they will at the same time shall be bound by the terms and conditions of the relevant and applicable Channel(s) they use and the terms and conditions of the relevant and applicable Channel(s) operated by the Weeloy which their Materials (as defined in Section 3.1 below) are being or have been uploaded to or posted on.\n" +
    "            </p>\n" +
    "            <p>\n" +
    "                The terms “User\" and “Users” herein refer to all individuals and/or entities accessing and/or using the Channels at anytime, in any country, for any reason or purpose.\n" +
    "            </p>\n" +
    "            <div style='margin:0 0 0 -40px;'>\n" +
    "                <li><strong>General Terms</strong>\n" +
    "                    <ol>\n" +
    "                        <li>Weeloy provides online platforms for Users to list the services or products related to food and beverage which they offer and provides online marketing (including but not limited to posting advertisements, promotional campaigns, marketing materials and hosting special events) as well as providing discussion forums and online platforms for Users to upload, post and forward their comments and photos related to their dining experiences (hereinafter referred to as the \"Services\").\n" +
    "                        </li>\n" +
    "                        <li>Weeloy is not a party to nor is it involved in any actual transaction between Users.\n" +
    "                        </li>\n" +
    "                        <li>Users agree that all promotional offers that are posted on Weeloy Channels are not applicable in conjunction with other discounts, promotional offers or during special festive holidays, unless explicitly stated.\n" +
    "                        </li>\n" +
    "                        <li>Weeloy is committed to protect the privacy of the Users. Weeloy uses the information of the Users according to the terms as stated in the Weeloy Privacy Policy\n" +
    "                        </li>\n" +
    "                    </ol>\n" +
    "                </li>\n" +
    "                <li><strong>Prohibited Uses for all Users</strong>\n" +
    "                    <ol>\n" +
    "                        Users of the Channels, registered or non-registered, agree not to use any of the Channels for any of the following purposes which are expressly prohibited:\n" +
    "                        <li>All Users are prohibited from violating or attempting to violate the security of the Channels including, without limitation, accessing data not intended for them or logging into a server or account which they are not authorized to access, attempting to probe, scan or test the vulnerability of a system or network or attempting to breach security or authentication measures without proper authorization, attempting to interfere with service to any user, host or network or sending unsolicited e-mail. Violation of system or network security may result in civil and/or criminal liabilities.\n" +
    "                        </li>\n" +
    "                        <li>A User shall not delete or revise any material or information posted by any other Users.\n" +
    "                        </li>\n" +
    "                        <li>All Users shall not use the Channels (1) for uploading, posting, publishing, transmitting, distributing, circulating or storing material in violation of any applicable laws or regulations; or (2) in any manner that will infringe the copyright, trademark, trade secrets or other intellectual property rights of others or violate the privacy or publicity or other personal rights of others; or (3) in any manner that is harmful, defamatory, libelous, obscene, discriminatory, harassing, threatening, abusive, hateful or is otherwise offensive or objectionable. In particular, all Users shall not print, download, duplicate or otherwise copy or use any personally identifiable information about other Users (if any). All unsolicited communications of any type to Users are strictly prohibited.\n" +
    "                        </li>\n" +
    "                        <li>Users shall not use the Channels if they do not have legal capacity to form legally binding contracts.\n" +
    "                        </li>\n" +
    "                        <li>Users shall not upload or post any advertisement or materials on the Channels which contains any false, inaccurate, misleading or libelous content or contains any computer viruses, trojan horses, worms, computed files or other materials that may interrupt, damage or limit the functionality of any computer software or hardware or telecommunication equipment. Also, the advertisement shall not be fraudulent or involve sale of illegal products.\n" +
    "                        </li>\n" +
    "                        <li>Users shall not engage in spamming, including but not limited to any form of emailing, posting or messaging that is unsolicited.\n" +
    "                        </li>\n" +
    "                    </ol>\n" +
    "                </li>\n" +
    "                <li><strong>Acceptable uses of the Channels</strong>\n" +
    "                    <ol>\n" +
    "                        <p>\n" +
    "                            Specific uses - User(s) uploading or posting advertisements, photos, content, views, comments, messages and/or other information on the Channels (hereinafter collectively referred to as the “Material(s)”). Such User(s) uploading or posting the Materials shall hereinafter be referred to as the \"Posting User(s)\".\n" +
    "                        </p>\n" +
    "                        <li>The Posting User agrees that he/she/it shall only use the Channels for lawful purposes and for enjoying the Services provided through the Channels. Weeloy reserves the right to edit, share, reject, disapprove, erase and delete any Materials posted on the Channels as it sees appropriate.\n" +
    "                        </li>\n" +
    "                        <li>In the event that the Posting User is an individual, he/she shall not post his/her identity card and/or passport number on the Channels.\n" +
    "                        </li>\n" +
    "                        <li>Although Weeloy shall use its reasonable endeavors to restrict access to the database of the Posting Users’ personal data only to the personnel of Weeloy, Weeloy does not guarantee that other parties will not, without Weeloy’s consent, gain access to such database. For the usage and protection of personal data provided by the Posting Users, please refer to the Weeloy Privacy Policy.\n" +
    "                        </li>\n" +
    "                        <li>Users who upload or post Materials on the Channels shall be solely responsible for the Materials uploaded, posted or shared by them and/or any web pages and/or media platforms and/or applications linked to the Channels posted by them. Weeloy reserves the right to edit, share, reject, erase, remove and delete any Materials and links to web pages and/or media platforms and/or applications as it sees appropriate.\n" +
    "                        </li>\n" +
    "                        <li>Weeloy shall have the right to terminate any services to any Posting Users at its sole discretion. If a User uploads or posts Materials on the Channels and subsequently deletes and/or removes the same or the User terminates his/her/its accounts, or that Weeloy deletes any and/or removes such uploaded or posted Materials, such Materials will no longer be accessible by the User who uploaded or posted the same via that User’s account; however, such deleted Materials may still persist and appear on any part of the Channels, and/or be used in any form by Weeloy.\n" +
    "                        </li>\n" +
    "                        <li>Weeloy reserves the right to request any User to cease using or to change his/her/its username immediately upon notice given to the relevant user without giving any reason as and when Weeloydeems appropriate to do so; if any User disagrees and refuses to abide by such request made by Weeloy, Weeloy may at any time at its sole discretion, deactivate that User’s account without prior notification to that User and without prejudice to all Weeloy’s other rights and remedies.\n" +
    "                        </li>\n" +
    "                        <li>This paragraph shall only be applicable to Users posting advertisements, promotional and marketing materials (“Advertiser(s)”):-\n" +
    "                        </li>\n" +
    "                        <li>Upon payment of a service fee to Weeloy or upon acceptance of any free trial promotion offer, Users will be entitled to use the Sites and/or Platforms and/or Applications (as the case may be) to post advertisements and promotional materials (subject to the Terms and Conditions and any specific terms and conditions of any service agreement(s) entered into between Weeloy and the User(s), and in the event of any conflict between the two, the latter shall prevail).\n" +
    "                        </li>\n" +
    "                        <li>Weeloy also reserves the right to change the service fee or institute new charges or fees to be paid by Advertisers for posting advertisements and promotional materials on any of the Channels, as it deems appropriate.\n" +
    "                        </li>\n" +
    "                        <li>In the event that any Advertiser posting advertisements and promotional materials fails to pay the service fee or any other fees or charges due to the Weeloy, Weeloy reserves the right to suspend or terminate that Advertiser’s user account, advertisements and links to web pages and/or media platforms and/or applications without prejudice to all its other rights and remedies.\n" +
    "                        </li>\n" +
    "                        <li>This paragraph shall only be applicable to the use of the Applications:- By using the Applications, Users acknowledge and agree to the following:-\n" +
    "                        </li>\n" +
    "                        <li>If Users use the Applications to upload, post and share materials, including but not limited to instantly uploading, posting and sharing photos taken, the Users are consenting to the Materials being shared;\n" +
    "                        </li>\n" +
    "                        <li>The Users’ use of the Applications may cause personally identifying information to be publicly disclosed and/or associated with the relevant Users, even if Weeloy has not itself provided such information; and\n" +
    "                        </li>\n" +
    "                        <li>The Users shall use the Applications at their own option and risk and at their own accord. The Users will hold Weeloy harmless for activities related to their use of the Applications.\n" +
    "                        </li>\n" +
    "                        <li>Specific uses - User(s) viewing the Materials posted on the Channels\n" +
    "                        </li>\n" +
    "                        <p>(hereinafter referred to as the \"Viewer(s)\") The Viewer agrees that he/she/it shall only use the Channels for lawful purposes and for enjoying the Services provided therein. The Viewer agrees that any personal data received from the Channels or Weeloy shall only be used for the purpose of identifying and/or locating advertisements or materials or any content therein or for the purpose of enjoying the Services provided through the Channels. Any personal data received which are irrelevant to the above purposes shall be disregarded and shall not be saved, stored, collected, processed, used, distributed, published, disclosed or transmitted in any way, including but not limited to for any commercial purpose. The Viewer also agrees that any personal data collected from the Channels or Weeloy shall be promptly and properly deleted when the above purposes have lapsed or been achieved. Weeloy shall not be responsible or held liable in any way if any Users, in breach of the Terms and Conditions or the terms and conditions of any relevant and applicable Channel(s), in any country, use the other Users’ personal data, information or materials (whether obtained from the Channels or not) for any purpose.\n" +
    "                        </p>\n" +
    "                        <li>All Users accept that all personal data, information or materials provided by them publicly on the Channels are voluntarily provided and are given entirely at their own risk. The Weeloy shall not bear the responsibility of protecting the personal data, information or materials so provided publicly therein.\n" +
    "                        </li>\n" +
    "                        <li>Specific uses - User(s) using online restaurant reservation service. For any User using online restaurant reservation service via <a href=\"https://www.weeloy.com/\">www.weeloy.com</a> which is operated by the Weeloy, the User is deemed to have read, understood and accepted the Terms and Conditions before using the said service.\n" +
    "                        </li>\n" +
    "                    </ol>\n" +
    "                </li>\n" +
    "                <li><strong>Content License</strong>\n" +
    "                    <ol>\n" +
    "                        <li>\n" +
    "                            By uploading or posting Materials on the Channels, the User unconditionally grants Weeloy a non-exclusive, worldwide, irrevocable, royalty-free right to exercise the copyright, publicity and database rights (but no other rights) he/she/it has in the Materials in order that Weeloy can use, publish, host, display, promote, copy, download, forward, distribute, reproduce, transfer, edit, sell and re-use the Materials in any form and anywhere, with or without making any commercial gains or profits, and carry out the purposes set out in the Weeloy Privacy Policy and herein.\n" +
    "                        </li>\n" +
    "                    </ol>\n" +
    "                </li>\n" +
    "                <li><strong>Intellectual Property Rights</strong>\n" +
    "                    <ol>\n" +
    "                        <li>\n" +
    "                            All contents of the Channels, including without limitation the text, images, information, comments, layout, database, graphics, photos, pictures, sounds or audio formats, software, brands and HTML are the intellectual properties of Weeloy or the Users (as the case may be) which are protected by applicable copyright and trademark laws and may not be downloaded or otherwise duplicated without the express written permission of Weeloy or the Users (as the case may be). Re-use of any of the foregoing is strictly prohibited and Weeloy reserves all its rights. Any use of any of such content other than those permitted under the Terms and Conditions, the terms and conditions of any specific Channel(s) and the terms and conditions of any service agreement(s) entered into between Weeloy and the User(s) is strictly prohibited and Weeloy reserves all its rights in this respect. For the avoidance of doubt, any purported consent of any third parties on the use of the contents and materials mentioned under this Clause shall not exonerate the Users from the restrictions/prohibitions imposed hereunder in whatsoever manner.\n" +
    "                        </li>\n" +
    "                    </ol>\n" +
    "                </li>\n" +
    "                <li><strong>Contents</strong>\n" +
    "                    <ol>\n" +
    "                        <li>Users acknowledge that the Wheel displayed on the restaurant page on the channels (and most specifically on the weeloy.com) is the current wheel of the participating restaurants at the time of reservations, and subject to change by the restaurant at the time of their visits.\n" +
    "                        </li>\n" +
    "                        <li>Users acknowledge that Weeloy may not pre-screen or pre-approve certain content posted on the Channels or any content sent through the Channels. In any event, Weeloy takes no responsibility whatsoever for the content on the Channels or any content sent through the Channels, or for any content lost and does not make any representations or warranties regarding the content or accuracy of any material therein.\n" +
    "                        </li>\n" +
    "                        <li>Any Materials uploaded or posted on the Channels by the Users may be viewed by users of other web sites and/or media platforms and/or applications linked to the Channels and Weeloy is not responsible for any improper and/or illegal use by any user or third party from linked third party web sites and/or media platforms and/or applications of any data or materials posted on the Channels.\n" +
    "                        </li>\n" +
    "                        <li>Links to third party web sites and/or media platforms and/or applications provided on the Channels are provided solely as a convenience to the Users and as internet navigation tools, and not in any way an endorsement by the Weeloy of the contents on such third party web sites and/or media platforms and/or applications.\n" +
    "                        </li>\n" +
    "                        <li>Unless otherwise stated on the Channels, Weeloy has no control over or rights in such third party web sites and/or media platforms and/or applications and is not responsible for any contents on such third party web sites and/or media platforms and/or applications or any use of services provided by such third party web sites and/or media platforms and/or applications by the Users.\n" +
    "                        </li>\n" +
    "                        <li>All Users acknowledge and agree that they are solely responsible for the form, content and accuracy of any Materials, web page or other information contained therein placed by them.\n" +
    "                        </li>\n" +
    "                        <li>Weeloy is not responsible for the content of any third party web sites and/or media platforms and/or applications linked to the Channels, and does not make any representations or warranties regarding the contents or accuracy of materials on such third party web sites and/or media platforms and/or applications. If any User accesses any linked third party web sites and/or media platforms and/or applications, he/she/it does so entirely at his/her/its own risk.\n" +
    "                        </li>\n" +
    "                        <li>Weeloy shall have the right to remove any Materials uploaded or posted on the Channels at its sole discretion without any compensation or recourse to the Posting Users if Weeloy considers at its sole discretion that such Users have breached or is likely to breach any law or the Terms and Conditions or any terms and conditions of any specific Channel(s) or service agreement(s) entered into between Weeloy and the Posting User(s).\n" +
    "                        </li>\n" +
    "                        <li>In the event, Weeloy decides to remove any paid advertisement for any reasons not relating to any breach of law or the provisions herein, Weeloy may, after deducting any fees that may charged for the period that the advertisement has been posted on the Channels, refund the remaining fees (if any) to the related Posting User in accordance with the Terms and Conditions or the terms and conditions of any specific Channel(s) or service agreement(s) entered into between Weeloy and the related Posting User, without prejudice to Weeloy’s rights and remedies hereunder. Users agree and consent that the Weeloy may, subject to the terms of the Weeloy Privacy Policy, use their personal data and/or other information provided to the Channels for purposes relating to the provision of Services and/or offered by the Weeloy and marketing services and/or special events of the Weeloy.\n" +
    "                        </li>\n" +
    "                    </ol>\n" +
    "                </li>\n" +
    "                <li><strong>Responsibility</strong>\n" +
    "                    <ol>\n" +
    "                        <li>Weeloy may not monitor the Channels at all times but reserves the right to do so.\n" +
    "                        </li>\n" +
    "                        <li>Weeloy does not warrant that any Materials or web page or application will be viewed by any specific number of Users or that it will be viewed by any specific User.\n" +
    "                        </li>\n" +
    "                        <li>Weeloy shall not in any way be considered an agent of any User with respect to any use of the Channels and shall not be responsible in any way for any direct or indirect damage or loss that may arise or result from the use of the Channels, for whatever reason made.\n" +
    "                        </li>\n" +
    "                        <li>Weeloy endeavors to provide quality service to all Users. However, Weeloy does not warrant that the Channels will operate without error at at all time and are free of viruses or other harmful mechanisms. If use of the Channels or their contents result in the need for servicing or replacement of equipment or data by any user, Weeloy shall not be liable for such costs. The Channels and their contents are provided on an “As Is” basis without any warranties of any kind. To the fullest extent permitted by law, Weeloy disclaims all warranties, including, without prejudice to the foregoing, any in respect of merchantability, non-infringement of third party rights, fitness for particular purpose, or about the accuracy, reliability, completeness or timeliness of the contents, services, text, graphics and links of the Channels.\n" +
    "                        </li>\n" +
    "                    </ol>\n" +
    "                </li>\n" +
    "                <li><strong>Own Risk</strong>\n" +
    "                    <ol>\n" +
    "                        <li>All Users shall use the Channels and any other web sites and/or media platforms and/or applications accessed through the Channels entirely at their own risk.\n" +
    "                        </li>\n" +
    "                        <li>ALL Users are responsible for the consequences of their postings. Weeloy does not represent or guarantee the truthfulness, accuracy or reliability of any Materials uploaded or posted by the Posting Users or endorses any opinions expressed by the Posting Users.\n" +
    "                        </li>\n" +
    "                        <li>Any reliance by any User on advertisements and materials posted by the other Users will be at their own risk. Weeloy reserves the right to expel any User and prevent his/her/its further access to the Channels, at any time for breaching this agreement or violating the law and also reserves the right to remove any Materials which is abusive, illegal, disruptive or inappropriate at Weeloy’s sole discretion.\n" +
    "                        </li>\n" +
    "                    </ol>\n" +
    "                </li>\n" +
    "                <li><strong>Indemnity</strong>\n" +
    "                    <ol>\n" +
    "                        <li>\n" +
    "                            All Users agree to indemnify, and hold harmless Weeloy, its officers, directors, employees, agents, partners, representatives, shareholders, servants, attorneys, predecessors, successors and assigns from and against any claims, actions, demands, liabilities, losses, damages, costs and expenses (including legal fees and litigation expenses on a full indemnity basis) arising from or resulting from their use of the Channels or their breach of the terms of this Agreement or any terms and conditions of any specific Channel(s) or service agreement(s) entered into between Weeloy and the User(s). Weeloy will provide prompt notice of any such claim, suit or proceedings to the relevant User.\n" +
    "                        </li>\n" +
    "                    </ol>\n" +
    "                </li>\n" +
    "                <li><strong>Limitation of the Service</strong>\n" +
    "                    <ol>\n" +
    "                        <li>Weeloy shall have the right to limit the use of the Services, including the period of time that Materials will be posted on the Channels, the size, placement and position of the Materials, email messages or any other contents, which are transmitted by the Services.\n" +
    "                        </li>\n" +
    "                        <li>Weeloy reserves the right, at its sole discretion, to edit, modify, share, erase, delete or remove any Materials posted on the Channels, for any reason, without giving any prior notice or reason to the Users. The Users acknowledge that Weeloy shall not be liable to any party for any modification, suspension or discontinuance of the Services.\n" +
    "                        </li>\n" +
    "                    </ol>\n" +
    "                </li>\n" +
    "                <li><strong>Termination of Service</strong>\n" +
    "                    <ol>\n" +
    "                        <li>Weeloy shall have the right to delete or deactivate any account, or block the email or IP address of any User, or terminate the access of Users to the Services, and remove any Materials within the Services immediately without notice for any reason, including but not limited to the reason that the User breached any law or the Terms and Conditions or any terms and conditions of any specific Channel(s) or any service agreement(s) entered into between Weeloy and the User(s).\n" +
    "                        </li>\n" +
    "                        <li>Weeloy reserves the right at any time to take such action as it considers appropriate, desirable or necessary including but not limited to taking legal actions against any such User.\n" +
    "                        </li>\n" +
    "                        <li>Weeloy shall have no obligation to deliver any Materials posted on the Channels to any User at any time, both before or after cessation of the\n" +
    "                        </li>\n" +
    "                        <p>\n" +
    "                            Services or upon removal of the related Material(s) from the Channels.\n" +
    "                        </p>\n" +
    "                        <li>The Terms and Conditions set out hereunder shall become inapplicable to the Users immediately upon the Users discontinuing their use of the Sites and/or the Platforms and/or Applications as the case may be.\n" +
    "                        </li>\n" +
    "                    </ol>\n" +
    "                </li>\n" +
    "                <li><strong>Disclaimer</strong>\n" +
    "                    <ol>\n" +
    "                        <li>Weeloy does not have control over and does not guarantee the truth or accuracy of listings of any Materials posted on the Channels or any content on third party web sites and/or media platforms and/or applications accessed via the Channels.\n" +
    "                        </li>\n" +
    "                        <li>In any event, Weeloy, its officers, directors, employees, agents, partners, representatives, shareholders, servants, attorneys, predecessors and successors shall not be liable for any losses, claims or damages suffered by any User whatsoever and howsoever arising or resulting from his/her/its use or inability to use the Channels and their contents, including negligence and disputes between any parties.\n" +
    "                        </li>\n" +
    "                    </ol>\n" +
    "                </li>\n" +
    "                <li><strong>Limitation of Liability</strong>\n" +
    "                    <ol>\n" +
    "                        <li>\n" +
    "                            Without prejudice to the above and subject to the applicable laws, the aggregate liability of Weeloy to any User for all claims arising from their use of the Services and the Channels shall be limited to the amount of SGD100.\n" +
    "                        </li>\n" +
    "                    </ol>\n" +
    "                </li>\n" +
    "                <li><strong>Security Measures</strong>\n" +
    "                    <ol>\n" +
    "                        <li>Weeloy will use its reasonable endeavors to ensure that its officers, directors, employees, agents and/or contractors will exercise their prudence and due diligence in handling the personal data submitted by the Users, and the access to and processing of the personal data by such persons is on a \"need-to-know\" and \"need-to-use\" basis.\n" +
    "                        </li>\n" +
    "                        <li>Weeloy will use its reasonable endeavors to protect the personal data against any unauthorized or accidental access, processing or erasure of the personal data.\n" +
    "                        </li>\n" +
    "                    </ol>\n" +
    "                </li>\n" +
    "                <li><strong>Severability</strong>\n" +
    "                    <ol>\n" +
    "                        <li>The provisions of the Terms and Conditions shall be enforceable independently of each other and the validity of each provision shall not be affected if any of the others is invalid.\n" +
    "                        </li>\n" +
    "                        <li>In the event, any provision of the Terms and Conditions is determined to be illegal, invalid or unenforceable, the validity and enforceability of the remaining provisions of the Terms and Conditions shall not be affected and, in lieu of such illegal, invalid, or unenforceable provision, there shall be added as part of the Terms and Conditions one or more provisions as similar in terms as may be legal, valid and enforceable under the applicable law.\n" +
    "                        </li>\n" +
    "                    </ol>\n" +
    "                </li>\n" +
    "                <li><strong>Conflict</strong>\n" +
    "                    <ol>\n" +
    "                        <li>\n" +
    "                            If there is any conflict between (1) the Terms and Conditions and/or specific terms of use appearing on the Platforms and/or the Applications and/or any term and (2) conditions of any service agreement(s) entered into between Weeloy and the User(s), and/or any specific terms and conditions of use in respect of any special events hosted by Weeloy, then the latter shall prevail.\n" +
    "                        </li>\n" +
    "                    </ol>\n" +
    "                </li>\n" +
    "                <li><strong>Governing Law and Dispute Resolutions</strong>\n" +
    "                    <ol>\n" +
    "                        <li>The Terms and Conditions and any dispute or matter arising from or incidental to the use of the Channels shall be governed by and construed in accordance with the laws of Singapore unless otherwise specified.\n" +
    "                        </li>\n" +
    "                        <li>Any dispute, controversy or claim arising out of or relating to the Terms and Conditions including the validity, invalidity, breach or termination thereof, shall be settled by arbitration in accordance with the Arbitration Rules as at present in force and as may be amended by the rest of this Clause:\n" +
    "                        </li>\n" +
    "                        <li>The appointing authority shall be Singapore; or alternatively, an appointing authority may be appointed by Weeloy at its sole and absolute discretion in any country, which Weeloy considers as fit and appropriate.\n" +
    "                        </li>\n" +
    "                        <li>Any User(s) who are in dispute with Weeloy acknowledge(s) and agree(s) that the choice of the appointing authority nominated by Weeloy shall be final and conclusive.\n" +
    "                        </li>\n" +
    "                        <li>The place of arbitration shall be in Singapore; or alternatively, at any such arbitral body in any country as Weeloy considers fit and appropriate at its sole and absolute discretion.\n" +
    "                        </li>\n" +
    "                        <li>There shall be only one arbitrator.\n" +
    "                        </li>\n" +
    "                        <li>The language to be used in the arbitral proceedings shall be English.\n" +
    "                        </li>\n" +
    "                        <li>In the event of any breach of the Terms and Conditions by any one party, the other party shall be entitled to remedies in law and equity as determined by arbitration.\n" +
    "                        </li>\n" +
    "                    </ol>\n" +
    "                </li>\n" +
    "                <br>\n" +
    "                <br>\n" +
    "            </div>\n" +
    "            For any query, please email our Customer Service Representative at <a href=\"mailto:info@weeloy.com\">info@weeloy.com</a>\n" +
    "            <br>\n" +
    "            <br>\n" +
    "            <br>\n" +
    "            <br>\n" +
    "    </ol>\n" +
    "    <div class=\"modal-footer\">\n" +
    "        <button type=\"button\" class=\"btn btn-primary\" data-dismiss=\"modal\">Close</button>\n" +
    "    </div>\n" +
    "    <!-- /modal-footer -->\n" +
    "    </div>\n" +
    "");
}]);

angular.module("../app/components/v_day_page/_v_day_page.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/components/v_day_page/_v_day_page.tpl.html",
    "<style>\n" +
    "    .introduction{\n" +
    "        margin-top :20px;\n" +
    "        margin-bottom: 20px;\n" +
    "        font-size:15px;\n" +
    "        font-weight: bold;\n" +
    "    }\n" +
    "     body{ background-color: white !important}\n" +
    "     .bg {\n" +
    "	background: url({{image}});\n" +
    "	background-repeat: no-repeat;\n" +
    "	background-position: center;\n" +
    "        background-size: cover;\n" +
    "        height: 270px;  \n" +
    "        margin-top: 30px;\n" +
    "        padding-top: 20px;\n" +
    "    }\n" +
    "    @media screen and (max-width: 1020px) {\n" +
    "        body {\n" +
    "            width:100%;\n" +
    "        }\n" +
    "    }\n" +
    "    @media screen and (max-width: 768px) {\n" +
    "            width:100%;\n" +
    "    }\n" +
    "\n" +
    "@media screen and (max-width: 480px) {\n" +
    "    width:100%;\n" +
    "}\n" +
    "</style>\n" +
    "    <div class=\"container\" style='background-color: white !important'>\n" +
    "        <div>\n" +
    "             <div class=\"row\">\n" +
    "                    <div class=\"bg\"></div>\n" +
    "            </div>\n" +
    "<!--            <div class=\"banner_restaurant header1\" style=\"background-image: url('images/vdaypagebanner1.jpg'); background-size: cover; background-repeat: no-repeat no-repeat;height:275px !important;margin-top:45px;background-position: center\">\n" +
    "            </div>-->\n" +
    "<!--            <div class=\"banner_restaurant\" style=\"background-image: url('images/vday2.png'); background-size: cover; background-position: 50% 50%; background-repeat: no-repeat no-repeat;\">\n" +
    "\n" +
    "            </div>-->\n" +
    "<!--            <h1>Valentine's Day Special 2017</h1>-->\n" +
    "                <div class='col-lg-12 col-md-8 col-sm-12 col-xs-12 '>\n" +
    "                    <p style=\"font-size:20px;margin-top:10px;text-align:center;\">\n" +
    "                       A selection of special Valentine’s Day offers and events \n" +
    "                    </p>\n" +
    "                    <p style=\"font-size:20px;margin-top:10px;text-align:center;\">from some of the best restaurants in Singapore.</p>\n" +
    "<!--                 <p style=\"margin-left:20px;text-align:center;font-size:20px;margin-bottom:20px;padding-bottom:15px;\">from some of the best restaurants in Singapore.</p>-->\n" +
    "                </div>\n" +
    "           \n" +
    "<!--            <p style='margin-top:30px;margin-left:120px;padding-left:50px; color:#2aacd2; display: block; font-family: proximanova-semibold;font-size: 20px;'> Valentine's Day Menus & Promos</p>-->\n" +
    "<!--            <div class=\"events\" style='margin-left:0px !important;padding-left:0px;'>-->\n" +
    "<!--                <div class=\"row\" style='margin-left:20px !important;padding-left:20px;' >-->\n" +
    "                <ul class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12 list-unstyled\" style='margin-left:0px !important;padding-left:0px;' >\n" +
    "                    \n" +
    "                    <li  class=\"col-md-6 col-sm-12\" ng-repeat=\"evt in events\" style='height:270px;padding-left:0px;'>\n" +
    "                        \n" +
    "                         <div class=\"col-md-4 col-sm-6\" style='margin-left:0px !important;padding-left:0px;margin-bottom:0px;'>\n" +
    "                            <a ng-if='!isMobile' href=\"{{image}}\" class=\"fresco\" title=\"Valentine day @ {{landing.title}} - reserve restaurant table now\">\n" +
    "                                <img ng-src=\"{{ image}}\" class=\"img-responsive\" alt=\"{{landing.title}} Valentine day Menu - reserve restaurant table now\" style='margin-bottom:10px;width:100%;margin-top:10px;padding-top:10px;margin-left:15px;'>\n" +
    "                            </a>\n" +
    "                              <a ng-if='isMobile' ng-click=\"btnBookNowEvent_click(evt.restaurant)\"  title=\"Valentine day @ {{landing.title}} - reserve restaurant table now\">\n" +
    "                                <img ng-src=\"{{ image}}\" class=\"img-responsive\" alt=\"{{landing.title}} Valentine day Menu - reserve restaurant table now\" style='margin-bottom:10px;width:100%;margin-top:10px;padding-top:10px;margin-left:15px;'>\n" +
    "                            </a>\n" +
    "                        </div>\n" +
    "                        <div class=\"col-md-8 col-sm-12 col-xs-12\" style='margin-bottom:10px;padding-bottom:10px;padding-left:15px;padding-right:0px;margin-top:20px;'>\n" +
    "                            <p id ='event-title' style='margin-bottom:0px;font-family: sans-serif;'><strong>{{evt.name}}</strong></p>\n" +
    "                            <div class=\"cnt\" style='margin-top:0px;'>\n" +
    "<!--                            <span ng-if=\"evt.start === evt.end\" ng-bind=\"evt.end | date : format : shortDate\"></span>-->\n" +
    "                            <span style='margin-top:0px;' >{{evt.end| date : format : shortDate}}</span>\n" +
    "                             <p style='margin-top:0px;'>{{landing.title}}, {{evt.area}}</p>\n" +
    "                             \n" +
    "                             <p id='evt-content' ng-maxlength=\"15\" href ng-if=\"evt.description !== ''\" data-toggle=\"popover\" data-placement=\"top\" title=\"{{landing.description}}\" onmouseenter=\"$(this).popover('show')\" onmouseleave=\"$(this).popover('hide')\"  style='float: left;font-family: Roboto, Arial, Helvetica;'>{{landing.description | limitTo:200}}...</p>\n" +
    "                              <span ng-if=\"evt.price!= '' \"  style='margin-top:10px;'>${{evt.price}} per couple </span>\n" +
    "                             <div style=\"margin-top:20px;padding-top:20px;\">\n" +
    "                                <button  ng-show=\"restaurant.status != 'comingsoon'\" class=\"book-button-sm btn-leftBottom-orange\" ng-click=\"btnBookNowEvent_click(evt.restaurant)\" ng-bind=\"book_button_text\"></button>\n" +
    "                             </div> \n" +
    "                        </div>\n" +
    "                \n" +
    "                        </div>\n" +
    "  \n" +
    "<!--                        <a analytics-on=\"click\"\n" +
    "                                        analytics-event=\"c_go_resto\"\n" +
    "                                        analytics-category=\"c_go_resto_VDay\" \n" +
    "                                        analytics-label=\"c_go_resto_VDay_{{evt.restaurant}}\"\n" +
    "                                        ng-href=\"{{getRestaurantLink(evt.restaurant)}}\"  style=\"text-decoration:none;\" title=\"Celebrate the VDay with {{evt.title}} - reserve your table with weeloy\">\n" +
    "                            <h1>Celebrate the VDay with {{evt.title}}</h1>\n" +
    "                        </a>\n" +
    "                        <div class=\"cnt\">\n" +
    "                            <h2 ng-bind=\"evt.name\"></h2>\n" +
    "                            <span ng-if=\"evt.start === evt.end\" ng-bind=\"evt.end | date : format : shortDate\"></span>\n" +
    "                            <span  ng-if=\"evt.start !== evt.end\">{{evt.start| date : format : shortDate}} to {{evt.end| date : format : shortDate}}, {{evt.area}}</span>\n" +
    "                            <p ng-bind=\"evt.description\"></p>\n" +
    "                            \n" +
    "                            <a analytics-on=\"click\"\n" +
    "                                        analytics-event=\"c_get_pdf\"\n" +
    "                                        analytics-category=\"c_get_pdf_VDay\" \n" +
    "                                        analytics-label=\"c_get_pdf_VDay_{{evt.restaurant}}\"href=\"{{ mediaServer}}/upload/restaurant/{{evt.restaurant}}/{{evt.pdf_link}}\" target=\"_blank\" ng-if=\"evt.pdf_link\" title=\"{{evt.title}} Menu for Valentine day 2016\">Download VDay menu</a>\n" +
    "\n" +
    "                            <div style=\"margin-top:10px;\">\n" +
    "                                <button analytics-on=\"click\"\n" +
    "                                        analytics-event=\"c_book_resto\"\n" +
    "                                        analytics-category=\"c_book_resto_VDay\" \n" +
    "                                        analytics-label=\"c_book_resto_VDay_{{evt.restaurant}}\" ng-show=\"restaurant.status != 'comingsoon'\" class=\"book-button-sm btn-leftBottom-orange\" ng-click=\"btnBookNowEvent_click(evt.restaurant)\" ng-bind=\"book_button_text\"></button>\n" +
    "                            </div> \n" +
    "                        </div>-->\n" +
    "                       \n" +
    "                    </li>\n" +
    "                   \n" +
    "                </ul>\n" +
    "<!--                    </div>-->\n" +
    "         \n" +
    "            <div style=\"text-align: center;\">\n" +
    "                <a href='search/singapore' style=\" color: #555555;text-decoration: none;font-size: 17px;\">View more restaurants</a>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>");
}]);

angular.module("../app/components/write_review/_write_review_page.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/components/write_review/_write_review_page.tpl.html",
    "<div id=\"my-review\">\n" +
    "       <div class=\"container\">\n" +
    "    <div class=\"col-lg-offset-2 col-md-offset-2 col-sm-offset-2 col-lg-8 col-md-8 col-sm-8 col-xs-12 my-booking\"> \n" +
    "               <div id='msg-alert' class=\"alert\" style=\"display:none;\">\n" +
    "              </div>\n" +
    "            <div id='review-container' class ='panel-body' style='display:block;'>\n" +
    "               <div id=\"targetSectionRev\" >\n" +
    "                    <div id='review-msg' style='margin-left:15px;text-align:center;'> <h3>Thank you for your review.</h3></div>\n" +
    "                <div class='review-container' style =\"text-align:center;\">\n" +
    "                    <span style='margin-left:15px;' > <strong> Your Rating :</strong> </span>\n" +
    "                    <div class='review-container' style =\"text-align:center;\" >\n" +
    "                        <div class=\"star-rating\" star-rating rating-value=\"rating\" data-max=\"5\" on-rating-selected=\"rateFunction(rating)\"> </div>\n" +
    "                    </div>\n" +
    "<!--                    <span class=\"rating\" >\n" +
    "                        <span  class=\"star\"  ></span><span  class=\"star\"></span><span class=\"star\"></span><span  class=\"star\"></span><span  class=\"star \"></span>\n" +
    "                        <input id='grade' type='hidden'  value='' />\n" +
    "                    </span>-->\n" +
    "                </div>\n" +
    "                <div id='comment' style='margin-left:15px;text-align:center;'><h5>Do you want to leave a comment?</h5></div>\n" +
    "                <div class=\"col-lg-offset-2 col-lg-8 col-md-8 col-sm-8 col-xs-12\">\n" +
    "                <textarea ng-model='comment' class='form-control' rows='4' ></textarea>\n" +
    "                <div class=\"text-center\" style=\"margin-top:10px;\">\n" +
    "                     <input type=\"button\" ng-click=\"RatingNow('update')\" class=\"btn btn_submit_review\" value=\"Submit\"  style=\"background-color: #2BA8CF;color: #fff;  border-radius: 0px;\">\n" +
    "                </div>\n" +
    "                </div>\n" +
    "                \n" +
    "           \n" +
    "           </div>\n" +
    "            <br />\n" +
    "            <br />\n" +
    "            <br />\n" +
    "            <br />\n" +
    "            <br />\n" +
    "            <br />\n" +
    "            <br />\n" +
    "        </div>\n" +
    "            \n" +
    "            \n" +
    "<!--             <review-section  id=\"targetSectionRev\" class=\"currentpanel\" rdata-review=\"reviewList\" ></review-section>-->\n" +
    "  \n" +
    "        </div>\n" +
    "         <div class=\"col-lg-2 col-md-2 col-sm-3 col-xs-12\">\n" +
    "            <p></p>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("../app/components/z_landing_mobile/_landing_mobile_download.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/components/z_landing_mobile/_landing_mobile_download.tpl.html",
    "<section class=\"download_mobile\">\n" +
    "    <div class=\"container\">\n" +
    "        <h1 class=\"title_home_page text-center\" ng-bind=\"Str.LandingMobileDownload.MobileDownloadTitle\"></h1>\n" +
    "        <h2 class=\"sub_title_home_page text-center\" ng-bind=\"Str.LandingMobileDownload.MobileDownloadSubTitle\"></h2>\n" +
    "    </div>\n" +
    "</section>\n" +
    "\n" +
    "<section class=\"available-mobile\">\n" +
    "    <div class=\"text-center \">\n" +
    "        <h2 class=\"title-available-mobile\" ng-bind=\"Str.LandingMobileDownload.AppDownloadText\"></h2>\n" +
    "        <div class=\"mobile-download-widget\">\n" +
    "            <a rel=\"nofollow\" class=\"mobile-picture-div\" target=\"_blank\" href=\"http://itunes.apple.com/app/id973030193\">\n" +
    "                <img width=\"196\" height=\"60\" src=\"images/home_picture/app-store-badge_en.png\" alt=\"Available on the App Store\">\n" +
    "            </a>\n" +
    "            <a rel=\"nofollow\" class=\"mobile-picture-div\" target=\"_blank\" href=\"https://play.google.com/store/apps/details?id=com.weeloy.client\">\n" +
    "                <img width=\"196\" height=\"60\" src=\"images/home_picture/play-store-badge_en.png\" alt=\"Get it on Google Play\">\n" +
    "            </a>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</section>");
}]);

angular.module("../app/shared/partial/_breadcrumb.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/shared/partial/_breadcrumb.tpl.html",
    "<ul>\n" +
    "    <li ng-repeat=\"item in breadcrumb\">\n" +
    "        <a ng-href=\"{{item.href}}\">\n" +
    "            <span ng-bind=\"item.title\"></span><span class=\"fa fa-angle-right\" ng-show=\"!$last\"></span>\n" +
    "        </a>\n" +
    "    </li>\n" +
    "</ul>\n" +
    "");
}]);

angular.module("../app/shared/partial/_footer.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/shared/partial/_footer.tpl.html",
    "<footer>\n" +
    "    <section id=\"zen-social\">\n" +
    "        <h3 ng-bind=\"Str.template.FindUs\" class=\"ng-binding\">Find us on</h3>\n" +
    "        <ul>\n" +
    "            <li><a rel=\"nofollow\" target=\"_blank\" href=\"https://www.facebook.com/weeloy.sg\" id=\"social-facebook\" title=\"follow weeloy on facebook\"><span class=\"fa fa-facebook\"></span></a></li>\n" +
    "            <li><a rel=\"nofollow\" target=\"_blank\" href=\"https://twitter.com/weeloyasia\" id=\"social-twitter\" title=\"follow weeloy on twitter\"><span class=\"fa fa-twitter\"></span></a></li>\n" +
    "            <li><a rel=\"nofollow\" target=\"_blank\" href=\"https://www.instagram.com/weeloysingapore/\" id=\"social-linkedin\" title=\"follow weeloy on instagram\"><span class=\"fa fa-instagram\"></span></a></li>\n" +
    "        </ul>\n" +
    "    </section>\n" +
    "    <footer id=\"zen-footer\">\n" +
    "        <div class=\"col-md-10 col-md-push-1 col-sm-10 col-sm-push-1 col-xs-12\">\n" +
    "            <div class=\"col-md-3 col-sm-3 col-xs-12 zen-col\">\n" +
    "                <h2 class=\"title\">\n" +
    "                        <span>//</span>infomation\n" +
    "                    </h2>\n" +
    "                <ul>\n" +
    "                    <li>\n" +
    "                        <a href=\"blog\" title=\"food blog by weeloy\" onclick=\"document.location.href = 'https://www.weeloy.com/blog';return false;\" style=\"cursor: pointer\" ng-bind=\"Str.template.FooterInformationBlog\"></a>\n" +
    "                    </li>\n" +
    "                    <li>\n" +
    "                        <a href=\"how-it-works\" title=\"weeloy concept how it works\" ng-bind=\"Str.template.FooterInformationHowItWorks\"></a>\n" +
    "                    </li>\n" +
    "                    <li>\n" +
    "                        <a href=\"contact\" title=\"weeloy contact page\" ng-bind=\"Str.template.FooterInformationContactUs\"></a>\n" +
    "                    </li>\n" +
    "                    <li>\n" +
    "                        <a href=\"http://www.weeloy.io/\" onclick=\"document.location.href = 'http://www.weeloy.io/';return false;\" style=\"cursor: pointer\"  title=\"weeloy partner page\">For Restaurants</a>\n" +
    "                    </li>\n" +
    "                    <li>\n" +
    "                        <a href=\"faq\" title=\"faq weeloy\" ng-bind=\"Str.tempate.FooterInformationFaq\"></a>\n" +
    "                    </li>\n" +
    "                    <li>\n" +
    "                        <a target=\"_blank\" href=\"terms-and-conditions-of-service\" ng-bind=\"Str.template.FooterInformationTermOfService\"></a>\n" +
    "                    </li>\n" +
    "                    <li>\n" +
    "                        <a target=\"_blank\" href=\"privacy-policy\" ng-bind=\"Str.template.FooterInformationPrivacyPolicy\"></a>\n" +
    "                    </li>\n" +
    "                </ul>\n" +
    "            </div>\n" +
    "            <div class=\"col-md-2 col-sm-2 col-xs-12 zen-col\">\n" +
    "                <h2 class=\"title\">\n" +
    "                    <span>//</span>OUR LOCATIONS\n" +
    "                </h2>\n" +
    "                <ul>\n" +
    "                    <li><a href=\"search/singapore\" title=\"find and reserve restaurant in singapore\">Singapore</a></li>\n" +
    "                    <li><a href=\"search/bangkok\" title=\"find and reserve restaurant in bangkok\">Bangkok</a></li>\n" +
    "                    <li><a href=\"search/phuket\" title=\"find and reserve restaurant in phuket\">Phuket</a></li>\n" +
    "                    <li><a href=\"search/hong%20kong\" title=\"find and reserve restaurant in hong-kong\">Hong Kong</a></li>\n" +
    "                </ul>\n" +
    "            </div>\n" +
    "            <div class=\"col-md-2 col-sm-2 col-xs-12 zen-col\">\n" +
    "                <h2 class=\"title\">\n" +
    "                        <span>//</span>POPULAR CUISINES\n" +
    "                    </h2>\n" +
    "                <ul>\n" +
    "                    <li><a href=\"/search/singapore/cuisine/chinese\">Chinese</a></li>\n" +
    "                    <li><a href=\"/search/singapore/cuisine/italian\">Italian</a></li>\n" +
    "                    <li><a href=\"/search/singapore/cuisine/japanese\">Japanese</a></li>\n" +
    "                    <li><a href=\"/search/singapore/cuisine/indian\">Indian</a></li>\n" +
    "                    <li><a href=\"/search/singapore/cuisine/french\">French</a></li>\n" +
    "                </ul>\n" +
    "            </div>\n" +
    "            <div class=\"col-md-2 col-sm-2 col-xs-12 zen-col app\">\n" +
    "                <h2 class=\"title\">\n" +
    "                        <span>//</span>MOBILE\n" +
    "                    </h2>\n" +
    "                <ul>\n" +
    "                    <li>\n" +
    "                        <a class=\"mobile-picture-div\" rel=\"nofollow\" target=\"_blank\" href=\"http://itunes.apple.com/app/id973030193\" title=\"Download Weeloy iOS app\">\n" +
    "                            <img width=\"160\" height=\"49\" src=\"client/assets/images/footer/apple.png\">\n" +
    "                        </a>\n" +
    "                    </li>\n" +
    "                    <li>\n" +
    "                        <a class=\"mobile-picture-div\" rel=\"nofollow\" target=\"_blank\" href=\"https://play.google.com/store/apps/details?id=com.weeloy.client\" title=\"Download Weeloy Android app\">\n" +
    "                            <img width=\"160\" height=\"56\" src=\"client/assets/images/footer/android.png\">\n" +
    "                        </a>\n" +
    "                    </li>\n" +
    "                </ul>\n" +
    "            </div>\n" +
    "            <div class=\"col-md-3 col-sm-3 col-xs-12 zen-col form\">\n" +
    "                <h2 class=\"title\"><span>//</span><span>NEWSLETTER</span></h2>\n" +
    "                <form method=\"POST\" ng-submit=\"SubmitNewletter(newletter_email)\" id=\"newsletterform\" name=\"newsletterform\" novalidate>\n" +
    "                    <input class=\"email newsletter_email\" id=\"email\" name=\"newletter_email\" ng-model=\"newletter_email\" type=\"email\" placeholder=\"{{Str.template.FooterNewsLetterPlaceHolder}}\" required=\"true\">\n" +
    "                    <input type=\"submit\" class=\"submit\" name=\"Join\" value=\"{{Str.template.FooterNewsLetterBtn}}\"></input>\n" +
    "                </form>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </footer>\n" +
    "</footer>\n" +
    "");
}]);

angular.module("../app/shared/partial/_footer_new.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/shared/partial/_footer_new.tpl.html",
    "<footer style='border-left:solid black 18px;border-right:solid black 20px;border-bottom:solid black 10px;'>\n" +
    "    <div class=\"top-section\">\n" +
    "        <div class=\"border-shadow\">\n" +
    "        </div>\n" +
    "    \n" +
    "    </div>\n" +
    "\n" +
    "<!--    <section id=\"zen-social\">\n" +
    "        <h3 ng-bind=\"Str.template.FindUs\" class=\"ng-binding\">Find us on</h3>\n" +
    "        <ul>\n" +
    "            <li><a rel=\"nofollow\" target=\"_blank\" href=\"https://www.facebook.com/weeloy.sg\" id=\"social-facebook\" title=\"follow weeloy on facebook\"><span class=\"fa fa-facebook\"></span></a></li>\n" +
    "            <li><a rel=\"nofollow\" target=\"_blank\" href=\"https://twitter.com/weeloyasia\" id=\"social-twitter\" title=\"follow weeloy on twitter\"><span class=\"fa fa-twitter\"></span></a></li>\n" +
    "            <li><a rel=\"nofollow\" target=\"_blank\" href=\"https://www.instagram.com/weeloysingapore/\" id=\"social-linkedin\" title=\"follow weeloy on instagram\"><span class=\"fa fa-instagram\"></span></a></li>\n" +
    "        </ul>\n" +
    "    </section>-->\n" +
    "    <footer id=\"zen-footer\">\n" +
    "            <div class=\"col-md-12 col-sm-12 col-xs-12 \">\n" +
    "                    <div class=\"col-md-1 col-sm-1\">\n" +
    "                       \n" +
    "                   </div>\n" +
    "                     <div class=\"col-md-2 col-sm-2\">\n" +
    "                       <a ng-href=\"/search/singapore\"><img  class=\"img-responsive\" src=\"client/assets/images/location/img1.jpg\" style=\"background-image: url('client/assets/images/location/img1.jpg'); background-position: center;background-size: cover;width: 100%;\" > </a>\n" +
    "                      <p class=\"hover\" style='margin-top:5px;'>SINGAPORE</p>\n" +
    "                      <p>\n" +
    "                         83 Amoy Street, Singapore 069902 <br/>\n" +
    "                         SG: +65 92478778 <br />\n" +
    "                         TH: +66 818262109 <br />\n" +
    "                         support@weeloy.com\n" +
    "                      </p>\n" +
    "                       \n" +
    "                   </div>\n" +
    "                    <div class=\"col-md-2 col-sm-2\">\n" +
    "                        <a ng-href=\"/search/bangkok\"><img class=\"img-responsive\" src=\"client/assets/images/location/img2.jpg\" style=\"background-image: url('client/assets/images/location/img2.jpg'); background-position: center;background-size: cover;\"></a>\n" +
    "                            <p class=\"hover\">BANGKOK</p>\n" +
    "                    \n" +
    "                   </div>\n" +
    "                    <div class=\"col-md-2 col-sm-2\">\n" +
    "                        <a ng-href=\"/search/phuket\"><img  class=\"img-responsive\" src=\"client/assets/images/location/img3.jpg\" style=\"background-image: url('client/assets/images/location/img3.jpg'); background-position: center;background-size: cover;\"></a>\n" +
    "                            <p class=\"hover\">PHUKET</p>\n" +
    "                       \n" +
    "                   </div>\n" +
    "                    <div class=\"col-md-2 col-sm-2\">\n" +
    "                       <a ng-href=\"/search/singapore\"><img  class=\"img-responsive\" src=\"client/assets/images/location/contact-hk.jpg\" style=\"background-image: url('client/assets/images/location/contact-hk.jpg'); background-position: center;background-size: cover;\"> </a>\n" +
    "                            <p class=\"hover\">Hong kong</p>\n" +
    "                     </div>\n" +
    "                    <div class=\"col-md-2 col-sm-2\">\n" +
    "                       <a ng-href=\"/search/singapore\"><img  class=\"img-responsive\" src=\"client/assets/images/location/contact-kl.jpg\" style=\"background-image: url('client/assets/images/location/contact-kl.jpg'); background-position: center;background-size: cover;\"> </a>\n" +
    "                            <p class=\"hover\">Kuala Lumpur</p>\n" +
    "                     </div>\n" +
    "                    <div class=\"col-md-1 col-sm-1\">\n" +
    "                       \n" +
    "                   </div>\n" +
    "                    \n" +
    "            </div>\n" +
    "        <div class=\"col-md-10 col-md-push-1 col-sm-10 col-sm-push-1 col-xs-12\">\n" +
    "            <div  class=\"col-md-10  col-sm-10  col-xs-10\" style=' margin-left:20%'>\n" +
    "                 <a href=\"blog\" title=\"food blog by weeloy\" onclick=\"document.location.href = 'https://www.weeloy.com/blog';return false;\" style=\"cursor: pointer\" ng-bind=\"Str.template.FooterInformationBlog\"></a> |\n" +
    "                \n" +
    "                 <a href=\"how-it-works\" title=\"weeloy concept how it works\" ng-bind=\"Str.template.FooterInformationHowItWorks\"></a> |\n" +
    "              \n" +
    "                <a href=\"contact\" title=\"weeloy contact page\" ng-bind=\"Str.template.FooterInformationContactUs\"></a> |\n" +
    "\n" +
    "                <a href=\"partner\" title=\"weeloy partner page\" ng-bind=\"Str.template.FooterInformationPartner\"></a> |\n" +
    "\n" +
    "                <a href=\"faq\" title=\"faq weeloy\" ng-bind=\"Str.tempate.FooterInformationFaq\"></a>|\n" +
    "\n" +
    "                <a target=\"_blank\" href=\"terms-and-conditions-of-service\" ng-bind=\"Str.template.FooterInformationTermOfService\"></a> |\n" +
    "\n" +
    "                <a target=\"_blank\" href=\"privacy-policy\" ng-bind=\"Str.template.FooterInformationPrivacyPolicy\"></a>\n" +
    "              \n" +
    "            </div>\n" +
    "<!--            <div class=\"col-md-3 col-sm-3 col-xs-12 zen-col\">\n" +
    "                <h2 class=\"title\">\n" +
    "                        <span>//</span>infomation\n" +
    "                    </h2>\n" +
    "                <ul>\n" +
    "                    <li>\n" +
    "                        <a href=\"blog\" title=\"food blog by weeloy\" onclick=\"document.location.href = 'https://www.weeloy.com/blog';return false;\" style=\"cursor: pointer\" ng-bind=\"Str.template.FooterInformationBlog\"></a>\n" +
    "                    </li>\n" +
    "                    <li>\n" +
    "                        <a href=\"how-it-works\" title=\"weeloy concept how it works\" ng-bind=\"Str.template.FooterInformationHowItWorks\"></a>\n" +
    "                    </li>\n" +
    "                    <li>\n" +
    "                        <a href=\"contact\" title=\"weeloy contact page\" ng-bind=\"Str.template.FooterInformationContactUs\"></a>\n" +
    "                    </li>\n" +
    "                    <li>\n" +
    "                        <a href=\"partner\" title=\"weeloy partner page\" ng-bind=\"Str.template.FooterInformationPartner\"></a>\n" +
    "                    </li>\n" +
    "                    <li>\n" +
    "                        <a href=\"faq\" title=\"faq weeloy\" ng-bind=\"Str.tempate.FooterInformationFaq\"></a>\n" +
    "                    </li>\n" +
    "                    <li>\n" +
    "                        <a target=\"_blank\" href=\"terms-and-conditions-of-service\" ng-bind=\"Str.template.FooterInformationTermOfService\"></a>\n" +
    "                    </li>\n" +
    "                    <li>\n" +
    "                        <a target=\"_blank\" href=\"privacy-policy\" ng-bind=\"Str.template.FooterInformationPrivacyPolicy\"></a>\n" +
    "                    </li>\n" +
    "                </ul>\n" +
    "            </div>-->\n" +
    "            <div class=\"section-mobileapp\" style=\"margin-top:20px;\">\n" +
    "                <div class=\"col-md-6  col-sm-6 col-xs-6 zen-col app\" style='margin-top:20px;margin-left:31%;'>\n" +
    "                    <div class=\"col-md-3 col-sm-3\" style='margin-bottom:10px;' >\n" +
    "                        <a class=\"mobile-picture-div\" rel=\"nofollow\" target=\"_blank\" href=\"http://itunes.apple.com/app/id973030193\" title=\"Download Weeloy iOS app\">\n" +
    "                           <img width=\"160\" height=\"49\" src=\"client/assets/images/footer/apple.png\">\n" +
    "                        </a>\n" +
    "\n" +
    "                    </div>\n" +
    "                    <div class=\"col-md-3 col-sm-3\" style='margin-left:10px;'>\n" +
    "                        <a class=\"mobile-picture-div\" rel=\"nofollow\" target=\"_blank\" href=\"https://play.google.com/store/apps/details?id=com.weeloy.client\" title=\"Download Weeloy Android app\">\n" +
    "                           <img width=\"160\" height=\"49\" src=\"client/assets/images/footer/android.png\">\n" +
    "                       </a>\n" +
    "\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"col-md-12  col-sm-12 col-xs-12 zen-col app\" style='margin-top:20px;'>\n" +
    "                <p>\n" +
    "                    Restaurants have unique challenges, needs and workflows that require unique solutions. Our product suite is designed specifically for restaurants to streamline workflows, create operational efficiencies, eliminate administrative redundancies and allow staff and management to focus on providing an optimal dining experience for their guests.\n" +
    "                </p>\n" +
    "            </div>\n" +
    "          \n" +
    "        </div>\n" +
    "    </footer>\n" +
    "</footer>\n" +
    "");
}]);

angular.module("../app/shared/partial/_forgot_password_form.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/shared/partial/_forgot_password_form.tpl.html",
    "<form name=\"ForgotPasswordForm\" ng-submit=\"ForgotPasswordForm.$valid && ForgotPassword(email)\" novalidate>\n" +
    "    <div class=\"form-group\">\n" +
    "        \n" +
    "        <input type=\"email\" class=\"form-control simple-form-control\" name=\"email\" ng-model=\"email\" placeholder=\"{{Str.template.TextEmail}}\" required>\n" +
    "        \n" +
    "        <div class=\"error\">\n" +
    "            <span ng-show=\"ForgotPasswordForm.$submitted && ForgotPasswordForm.email.$error.required\" ng-bind=\"Str.template.TextPleaseEnterYourEmail\"></span>\n" +
    "            <span ng-show=\"ForgotPasswordForm.$submitted && !ForgotPasswordForm.email.$error.required && ForgotPasswordForm.email.$error.email\" ng-bind=\"Str.template.TextPleaseEnterAValidEmail\"></span>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div class=\"form-group\">\n" +
    "        <input type=\"submit\" class=\"book-button-sm btn-leftBottom-orange\" value=\"{{Str.template.ForgotPasswordBtn}}\"> \n" +
    "    </div>\n" +
    "</form>\n" +
    "");
}]);

angular.module("../app/shared/partial/_login_form.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/shared/partial/_login_form.tpl.html",
    "<form class=\"form-horizontal\" name=\"LoginForm\" ng-submit=\"LoginForm.$valid && Login(user)\" novalidate>\n" +
    "    <div class=\"form-group\">\n" +
    "        <input type=\"email\" class=\"form-control simple-form-control\" name=\"email\" ng-model=\"user.email\" placeholder=\"{{Str.template.TextEmail}}\" required>\n" +
    "        <div class=\"error\">\n" +
    "            <span ng-show=\"LoginForm.$submitted && LoginForm.email.$error.required\" ng-bind=\"Str.template.TextPleaseEnterYourEmail\"></span>\n" +
    "            <span ng-show=\"LoginForm.$submitted && LoginForm.email.$error.email\" ng-bind=\"Str.template.TextPleaseEnterAValidEmail\"></span>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div class=\"form-group\">\n" +
    "        \n" +
    "        <input id=\"password\" name=\"password\" ng-model=\"user.password\" type=\"{{ (showPassword == true) ? 'text' : 'password' }}\" class=\"form-control simple-form-control\" placeholder=\"{{Str.template.TextPassword}}\" required>\n" +
    "        \n" +
    "        <div class=\"error\">\n" +
    "            <span ng-show=\"LoginForm.$submitted && LoginForm.password.$error.required\" ng-bind=\"Str.template.TextPleaseEnterYourPassword\"></span>\n" +
    "        </div>\n" +
    "        <div class=\"show-password\">\n" +
    "            <input id=\"show-password-ip\" type=\"checkbox\" ng-model=\"showPassword\">\n" +
    "            <label style=\"color: #333;font-weight: normal;font-family: proximanova;\" class=\"\" for=\"show-password-ip\" ng-bind=\"Str.template.TextShowPassword\"></label>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div class=\"form-group\">\n" +
    "        <input type=\"submit\" class=\"book-button-sm btn-leftBottom-orange\" value=\"{{Str.template.LoginBtn}}\">\n" +
    "    </div>\n" +
    "</form>\n" +
    "");
}]);

angular.module("../app/shared/partial/_menu.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/shared/partial/_menu.tpl.html",
    "<div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">\n" +
    "    <ul class=\"nav navbar-nav navbar-right\">\n" +
    "        <li>\n" +
    "            <a analytics-on=\"click\" analytics-event=\"c_header_menu_home\" analytics-category=\"header_menu_home\" href=\"/\">Home</a>\n" +
    "        </li>\n" +
    "        <li>\n" +
    "            <a analytics-on=\"click\" analytics-event=\"c_header_menu_search\" analytics-category=\"header_menu_search\" href=\"search/\">Search</a>\n" +
    "        </li>\n" +
    "        <li>\n" +
    "            <a analytics-on=\"click\" analytics-event=\"c_header_menu_how_it_works\" analytics-category=\"header_menu_how_it_works\" href=\"how-it-works/\">How it works</a>\n" +
    "        </li>\n" +
    "         <li>\n" +
    "<!--            <a href=\"blog\" title=\"food blog by weeloy\" onclick=\"document.location.href = 'https://www.weeloy.com/blog';return false;\" style=\"cursor: pointer\" ng-bind=\"Str.template.FooterInformationBlog\"></a>-->\n" +
    "            <a analytics-on=\"click\" analytics-event=\"c_header_menu_blog\" analytics-category=\"header_menu_blog\" href=\"blog\" onclick=\"document.location.href = 'https://www.weeloy.com/blog';return false;\" ng-bind=\"Str.template.FooterInformationBlog\"></a>\n" +
    "        </li>\n" +
    "        <li ng-show=\"loggedin === false\">\n" +
    "            <a href=\"#\" data-toggle=\"modal\" data-target=\"#loginModal\" ng-bind=\"Str.template.LoginBtnText\" title=\"Login Weeloy restaurant reservation system\"></a>\n" +
    "        </li>\n" +
    "        <li>\n" +
    "            <a analytics-on=\"click\" analytics-event=\"c_header_menu_down_apple\" analytics-category=\"header_menu_down_apple\" href=\"https://itunes.apple.com/app/id973030193\" target=\"_blank\" rel=\"nofollow\"><span class=\"download-apple\" style=\"background: transparent url('client/assets/images/icons/mobileicon1.png') no-repeat scroll -1px 0;padding: 5px 10px\"></span></a>\n" +
    "        </li>\n" +
    "        <li>\n" +
    "            <a analytics-on=\"click\" analytics-event=\"c_header_menu_down_android\" analytics-category=\"header_menu_down_android\" href=\"https://play.google.com/store/apps/details?id=com.weeloy.client\" target=\"_blank\" rel=\"nofollow\"><span class=\"download-android\" style=\"background: transparent url('client/assets/images/icons/mobileicon1.png') no-repeat scroll -39px 0;padding: 5px 10px\"></span></a>\n" +
    "        </li>\n" +
    "        <li ng-show=\"loggedin === true\">\n" +
    "            <a class=\"account\" id=\"user-profile-btn\" ng-click=\"showLoggedin($event)\" title=\"Your Weeloy account\">\n" +
    "                <img width=\"36\" height=\"36\" class=\"icon-ico_user-36\" src=\"https://static2.weeloy.com/images/sprites/transparent.png\" alt=\"weeloy member section\"></a>\n" +
    "        </li>\n" +
    "        <li>\n" +
    "            <a analytics-on=\"click\" analytics-event=\"c_header_menu_restaurants\" analytics-category=\"header_menu_down_restaurants\" href=\"http://www.weeloy.io/\" target=\"_blank\" >For Restaurants</a>\n" +
    "        </li>\n" +
    "        \n" +
    "        <div class=\"nav navbar-nav navbar-right loggedin-menu submenu\" ng-show=\"showLoggedinBox\">\n" +
    "            <ul class=\"root\">\n" +
    "                <li>\n" +
    "                    <a href=\"mybookings\" ng-bind=\"Str.template.MenuBooking\" title=\"Weeloy restaurant reservation - my bookings\"></a>\n" +
    "                </li>\n" +
    "                <li>\n" +
    "                    <a href=\"myorders\" ng-bind=\"Str.template.MenuMyOrder\" title=\"Weeloy restaurant reservation - my  orders\"></a>\n" +
    "                </li>\n" +
    "                <li>\n" +
    "                    <a href=\"myreviews\" ng-bind=\"Str.template.MenuReview\" title=\"Weeloy restaurant reservation - my reviews\"></a>\n" +
    "                </li>\n" +
    "                <li>\n" +
    "                    <a href=\"myaccount\" ng-bind=\"Str.template.MenuAccount\" title=\"Weeloy restaurant reservation - my account\"></a>\n" +
    "                </li>\n" +
    "                <li>\n" +
    "                    <a href=\"#\" ng-click=\"logout()\" ng-bind=\"Str.template.MenuLogout\" title=\"Logout Weeloy restaurant reservation system\"></a>\n" +
    "                </li>\n" +
    "            </ul>\n" +
    "        </div>\n" +
    "    </ul>\n" +
    "    <ul class=\"nav navbar-nav navbar-right\" ng-show=\"cart | lengthMoreThan:0\">\n" +
    "        <li>\n" +
    "            <a href=\"checkout/info\"><span class=\"fa fa-shopping-cart\"></span></a>\n" +
    "        </li>\n" +
    "    </ul>\n" +
    "</div>\n" +
    "\n" +
    "<script>\n" +
    "    $('.nav a').on('click', function(){\n" +
    "        $('.btn-navbar').click(); //bootstrap 2.x\n" +
    "        $('.navbar-toggle').click() //bootstrap 3.x by Richard\n" +
    "    });\n" +
    "</script>");
}]);

angular.module("../app/shared/partial/_menu_user.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/shared/partial/_menu_user.tpl.html",
    "<div id=\"menu_user\">\n" +
    "    <ul>\n" +
    "        <li class=\"{{ MenuUserSelected == 'mybookings' ? 'menu-user-selected' : '' }}\">\n" +
    "            <a href=\"mybookings\">Bookings</a>\n" +
    "        </li>\n" +
    "        <li class=\"{{ MenuUserSelected == 'myorders' ? 'menu-user-selected' : '' }}\">\n" +
    "            <a href=\"myorders\">Orders</a>\n" +
    "        </li>\n" +
    "        <li class=\"{{ MenuUserSelected == 'myreviews' ? 'menu-user-selected' : '' }}\">\n" +
    "            <a href=\"myreviews\">Reviews</a>\n" +
    "        </li>\n" +
    "        <li class=\"{{ MenuUserSelected == 'myaccount' ? 'menu-user-selected' : '' }}\">\n" +
    "            <a href=\"myaccount\">Account</a>\n" +
    "        </li>\n" +
    "    </ul>\n" +
    "</div>\n" +
    "\n" +
    "");
}]);

angular.module("../app/shared/partial/_message_wrapper.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/shared/partial/_message_wrapper.tpl.html",
    "<div ng-if=\"true\" id=\"system_msg\" ng-show=\"showSystemMsg\">\n" +
    "    <p ng-bind=\"systemMsg\"></p>\n" +
    "    <i class=\"fa fa-close\" ng-click=\"hideSystemMsg()\"></i>\n" +
    "</div>");
}]);

angular.module("../app/shared/partial/_restaurant_item.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/shared/partial/_restaurant_item.tpl.html",
    "<div class=\"zen-content\">\n" +
    "    <div class=\"img\">\n" +
    "        <a ng-href=\"{{ restaurant.getInternalPath() }}\" target=\"{{ OpenRestaurantInNewTab }}\">\n" +
    "            <img ng-src=\"client/assets/images/month/transparent.png\" class=\"img-responsive img-transparent\">\n" +
    "            <p ng-bind=\"restaurant.title\"></p>\n" +
    "        </a>\n" +
    "    </div>\n" +
    "    <div class=\"item-content\">\n" +
    "        <div class=\"col-sm-7 col-xs-7\">\n" +
    "            <ul>\n" +
    "                <li ng-if=\"(!affiliate_program &&  restaurant.best_offer.offer && restaurant.best_offer.offer != 'NO PROMOTION AVAILABLE') || (affiliate_program &&  restaurant.best_offer.offer_cpp && restaurant.best_offer.offer_cpp!='NO PROMOTION AVAILABLE') \">\n" +
    "                    <i class=\"fa fa-star\"></i>\n" +
    "                    <span ng-if=\"affiliate_program && restaurant.best_offer.offer_cpp\" ng-bind=\"restaurant.best_offer.offer_cpp\"></span>\n" +
    "                    <span ng-if=\"!affiliate_program && restaurant.best_offer.offer\" ng-bind=\"restaurant.best_offer.offer\"></span>\n" +
    "                    <span style=\"font-size: 11px\"> and more...</span>\n" +
    "                    <span style=\"float: right;\">\n" +
    "                        <img ng-if=\"affiliate_program\" ng-src=\"images/cpp_restaurant.jpg\">\n" +
    "                    </span>\n" +
    "                </li>\n" +
    "                \n" +
    "                <li><i class=\"fa fa-tag\"></i><span ng-repeat=\"cusine in restaurant.getCuisineAsArray()\">{{cusine}}<span ng-show=\"!$last\">, </span></span>\n" +
    "                </li>\n" +
    "                <li ng-if=\"restaurant.getCity() != '' || restaurant.getRegion() != ''\"><i class=\"fa fa-map-marker\"></i>\n" +
    "                    <span>{{restaurant.getRegion()}}<span ng-if=\"restaurant.getCity() != ''\">, </span> {{restaurant.getCity()}}</span>\n" +
    "                </li>\n" +
    "            </ul>\n" +
    "        </div>\n" +
    "        <div class=\"col-xs-5 wheel\">\n" +
    "            <div class=\"wheel-group\">\n" +
    "                <img ng-if=\"restaurant.is_wheelable == '1'\" ng-src=\"{{restaurant.wheelImageUrl}}\" class=\"img-responsive zen-show-detail\">\n" +
    "                <img ng-if=\"restaurant.is_wheelable != '1'\" ng-src=\"https://static2.weeloy.com/images/invisible-wheel-light.png\" class=\"img-responsive zen-show-detail\">\n" +
    "                <div class=\"zen-hover\" ng-if=\"restaurant.is_wheelable == '1'\">\n" +
    "                    <div class=\"zen-title\">\n" +
    "                        {{restaurant.getWheelValue()}} is the Scoring of this Wheel of Deals\n" +
    "                    </div>\n" +
    "                    <div class=\"zen-content\">\n" +
    "                        <span class=\"gray\">Best Offers:</span>\n" +
    "                        <p class=\"center\">\n" +
    "                            <span ng-if=\"affiliate_program && restaurant.best_offer.offer_cpp\" ng-bind=\"restaurant.best_offer.offer_cpp\"></span>\n" +
    "                            <span ng-if=\"!affiliate_program && restaurant.best_offer.offer\" ng-bind=\"restaurant.best_offer.offer\"></span>\n" +
    "                        </p>\n" +
    "                        <p class=\"more\">\n" +
    "                            <a ng-href=\"{{restaurant.getInternalPath()}}/dining-rewards\">More info</a>\n" +
    "                        </p>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div class=\"book-now\">\n" +
    "        <a ng-href=\"{{restaurant.getBookNowPageUrl()}}\">\n" +
    "            <button analytics-on=\"click\" analytics-event=\"c_book_resto\" analytics-category=\"c_book_resto\" ng-if=\"restaurant.status=='active'\" id=\"btn_book\" class=\"book-button-sm btn-leftBottom-orange\" ng-bind=\"restaurant.book_button.label\"></button>\n" +
    "        </a>\n" +
    "        <button analytics-on=\"click\" analytics-event=\"c_cs_book_resto\" analytics-category=\"c_cs_book_resto\" ng-if=\"restaurant.status=='comingsoon'\" id=\"btn_book\" class=\"btn custom_button {{restaurant.book_button.style}}\" ng-bind=\"restaurant.book_button.label\"></button>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("../app/shared/partial/_restaurant_services_and_share.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/shared/partial/_restaurant_services_and_share.tpl.html",
    "<ul class=\"additional-info\">\n" +
    "    <li ng-repeat=\"serviceCategory in RestaurantServices\">\n" +
    "        <div class=\"head clearfix\">\n" +
    "            <span class=\"ico category category_{{serviceCategory.category_css}}\">\n" +
    "            <!--    <img ng-src=\"{{serviceCategory.category_icon}}\" width=\"23\" height=\"23\" alt=\"\">-->\n" +
    "            </span> \n" +
    "            <span ng-bind=\"serviceCategory.category\"></span>\n" +
    "        </div>\n" +
    "        <p class=\"service_payment\">\n" +
    "            <span ng-repeat=\"service in serviceCategory.services\">\n" +
    "                <span ng-if=\"serviceCategory.category_id != '3'\">\n" +
    "                    <span ng-bind=\"service.name\"></span><span ng-hide=\"$last\">, </span>\n" +
    "                </span>\n" +
    "                <span ng-if=\"serviceCategory.category_id == '3'\">\n" +
    "                    <span ng-if='service.service_icon_css' class=\"ico service service_{{service.service_icon_css}}\"></span>\n" +
    "                    <span ng-if='service.name ==\"Cash/Nets\"' ng-bind=\"service.name\"></span>\n" +
    "                    <span ng-if='service.name' ng-hide=\"$last\"> </span>\n" +
    "                    <!--<img ng-src=\"{{service.icon}}\" style=\"cursor: pointer\">-->\n" +
    "                </span>\n" +
    "            </span>\n" +
    "        </p>\n" +
    "    </li>\n" +
    "</ul>\n" +
    "<div class=\"style \">\n" +
    "    <h2> Share this restaurant with your friends</h2>\n" +
    "    <button class=\"btn btn-lg btn-social btn-facebook\" ng-click=\"fbshare(restaurant)\"><i class=\"fa fa-facebook\"></i>Share On Facebook</button>\n" +
    "</div>\n" +
    "");
}]);

angular.module("../app/shared/partial/_signup_form.tpl.html", []).run(["$templateCache", function ($templateCache) {
  $templateCache.put("../app/shared/partial/_signup_form.tpl.html",
    "<form name=\"SignupForm\" ng-submit=\"SignupForm.$valid && SignupUser.password == SignupUser.password_confirmation && Signup(SignupUser,mobile)\" novalidate autocomplete=\"off\">\n" +
    "    <div class=\"form-group\">\n" +
    "\n" +
    "            <input name=\"email\" ng-model=\"SignupUser.email\" type=\"text\" class=\"form-control simple-form-control\" value=\"\" placeholder=\"{{Str.template.TextEmail}}\" required>\n" +
    "        <div class=\"error\">\n" +
    "            <span ng-show=\"SignupForm.$submitted && SignupForm.email.$error.required\" ng-bind=\"Str.template.TextPleaseEnterYourEmail\"></span>\n" +
    "            <span ng-show=\"SignupForm.$submitted && !SignupForm.email.$error.required && SignupForm.email.$error.email\" ng-bind=\"Str.template.TextPleaseEnterAValidEmail\"></span>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div class=\"form-group\">\n" +
    "            <input name=\"first_name\" type=\"text\" class=\"form-control simple-form-control\" ng-model=\"SignupUser.first_name\" placeholder=\"{{Str.template.TextFirstName}}\" required>\n" +
    "        <div class=\"error\">\n" +
    "            <span ng-show=\"SignupForm.$submitted && SignupForm.first_name.$error.required\" ng-bind=\"Str.template.TextPleaseEnterYourFirstname\"></span>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div class=\"form-group\">\n" +
    "\n" +
    "        <input name=\"last_name\" type=\"text\" class=\"form-control simple-form-control\" ng-model=\"SignupUser.last_name\" placeholder=\"{{Str.template.TextLastName}}\" required>\n" +
    "\n" +
    "        <div class=\"error\">\n" +
    "            <span ng-show=\"SignupForm.$submitted && SignupForm.last_name.$error.required\" ng-bind=\"Str.template.TextPleaseEnteYourLastName\"></span>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "  \n" +
    "    <div class=\"form-group\">\n" +
    "        <div class=\"input-group\">\n" +
    "            <div class='input-group-btn '>\n" +
    "            <button type='button' id='itemdfcountry' style='height: 40px; border-radius: 2px;' class='btn btn-default dropdown-toggle' data-toggle='dropdown'>\n" +
    "                                               <i class=\"glyphicon glyphicon-earphone\"></i><span class='caret'></span>\n" +
    "                                            </button>\n" +
    "            <ul class='dropdown-menu scrollable-menu' style='font-size:12px;height: auto;\n" +
    "    max-height: 250px;\n" +
    "    overflow-x: hidden;'>\n" +
    "             <li ng-repeat=\"s in countries\"><a ng-if=\"s.b != '';\" href=\"javascript:;\" ng-click=\"listPhoneIndex(s.a);\"  ><i class='famfamfam-flag-{{s.b}}'></i> {{s.a}}</a></li>\n" +
    "            </ul></div>\n" +
    "<!--            <span class=\"input-group-addon\"><i class=\"glyphicon glyphicon-earphone\"></i></span>-->\n" +
    "            <input id=\"txt-mobile\" name=\"mobile\" type=\"text\" class=\"form-control simple-form-control\" ng-model=\"mobile\" placeholder=\"{{Str.template.TextMobile}}\"  ng-change='checkvalidtel();' ng-model-options=\"{updateOn: 'blur'}\" required >\n" +
    "            \n" +
    "        </div>\n" +
    "        <div class=\"error\">\n" +
    "            <span ng-show=\"SignupForm.$submitted && SignupForm.mobile.$error.required\" ng-bind=\"Str.template.TextPleaseEnterYourPhoneNumber\"></span>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div class=\"form-group\">\n" +
    "            <input name=\"password\" type=\"password\" class=\"form-control simple-form-control passtype\" ng-model=\"SignupUser.password\" placeholder=\"{{Str.template.TextPassword}}\" ng-minlength=\"8\" ng-maxlength=\"12\" ng-pattern=\"/(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z])/\" required>\n" +
    "        <div class=\"error\">\n" +
    "            <span ng-show=\"SignupForm.$submitted && SignupForm.password.$error.required\" ng-bind=\"Str.template.TextPleaseEnterYourPassword\"></span>\n" +
    "            <span ng-show=\"!SignupForm.password.$error.required && (SignupForm.password.$error.minlength || SignupForm.password.$error.maxlength) && SignupForm.password.$dirty\">Passwords must be between 8 and 12 characters.</span>\n" +
    "            <span ng-show=\"!SignupForm.password.$error.required && !SignupForm.password.$error.minlength && !SignupForm.password.$error.maxlength && SignupForm.password.$error.pattern && SignupForm.password.$dirty\">Must contain one lower &amp; uppercase letter, and one non-alpha character (a number or a symbol.)</span>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div class=\"form-group\">\n" +
    "            <input name=\"password_confirmation\" type=\"password\" class=\"form-control simple-form-control passtype\" ng-model=\"SignupUser.password_confirmation\" placeholder=\"{{Str.template.TextRePassword}}\" ng-minlength=\"8\" ng-maxlength=\"12\" ng-pattern=\"/(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z])/\" required>\n" +
    "        <div class=\"error\">\n" +
    "            <span ng-show=\"SignupForm.$submitted && SignupForm.password_confirmation.$error.required\" ng-bind=\"Str.template.TextPleaseEnterYourPasswordAgain\"></span>\n" +
    "            <span ng-show=\"SignupForm.$submitted && !SignupForm.password_confirmation.$error.required && SignupUser.password != SignupUser.password_confirmation\" ng-bind=\"Str.template.TextPleaseEnterYourPasswordAgain\"></span>\n" +
    "            <span ng-show=\"!SignupForm.password_confirmation.$error.required && (SignupForm.password_confirmation.$error.minlength || SignupForm.password_confirmation.$error.maxlength) && SignupForm.password_confirmation.$dirty\">Passwords must be between 8 and 12 characters.</span>\n" +
    "            <span ng-show=\"!SignupForm.password_confirmation.$error.required && !SignupForm.password_confirmation.$error.minlength && !SignupForm.password_confirmation.$error.maxlength && SignupForm.password_confirmation.$error.pattern && SignupForm.password_confirmation.$dirty\">Must contain one lower &amp; uppercase letter, and one non-alpha character (a number or a symbol.)</span>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div class=\"form-group\">\n" +
    "        <input type=\"submit\" class=\"book-button-sm btn-leftBottom-orange\" value=\"{{Str.template.RegisterBtn}}\">\n" +
    "    </div>\n" +
    "</form>\n" +
    "");
}]);
