module.exports = function(grunt) {
    require('load-grunt-tasks')(grunt);

    // Project configuration.
    grunt.initConfig({
        uglify: {
            options: {
                banner: '/*! Weeloy copyright(c) 2014. All rights reserved */\r'
            },
            my_target: {
                files: {
                    'weeloy.min.js': ['weeloy.js']
                }
            }
        },
        sass: {
            options: {
                sourceMap: true
            },
            dist: {
                files: {
                    'assets/css/weeloy.css': 'assets/css/scss/weeloy.scss'
                }
            }
        },
        cssmin: {
            options: {
                shorthandCompacting: false,
                roundingPrecision: -1
            },
            target: {
                files: {
                    'assets/css/app.min.css': ["assets/css/app.css"]
                }
            }
        },
        concat_css: {
            options: {
                // Task-specific options go here. 
            },
            all: {
                src: [
                    'bower_components/bootstrap/dist/css/bootstrap.css',
                    'bower_components/bootstrap/dist/css/bootstrap-theme.css',
                    'bower_components/fontawesome/css/font-awesome.css',
                    'bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css',
                    'bower_components/angular-notify/dist/angular-notify.css',
                    'bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css',
                    'assets/css/flags/famfamfam-flags.css',
                    'assets/css/fresco/fresco.css',
                    'assets/css/jRating/jRating.jquery.css',
                    'assets/css/svg_event/component.css',
                    'assets/css/svg_event/normalize.css',
                    'assets/css/weeloy.css'
                ],
                dest: "assets/css/app.css"
            }
        },
        jshint: {
            ignore_warning: {
                options: {
                    '-W015': true
                },
                src: [
                    'app/api/**/*.js',
                    'app/components/event_booking/*.js',
                    'app/components/restaurant_info_page/*.js',
                    'app/components/restaurant_group_page/*.js',
                    'app/components/search_page/*.js',
                    'app/components/all_rewards_page/*.js',
                    'app/components/all_rewards_cpp_page/*.js',
                    'app/components/catering_page/*.js',
                    'app/components/checkout/*.js',
                    'app/components/checkout_info/*.js',
                    'app/components/payment_success_page/*.js',
                    'app/components/payment/*.js',
                    'app/components/mybookings_page/*.js',
                    'app/components/myorders_page/*.js',
                    'app/components/event_page/*.js',
                    'app/components/cny_page/*.js',
                    'app/components/mother_day_page/*.js',
                    'app/components/father_day_page/*.js',
                    'app/components/v_day_page/*.js',
                    'app/components/dsb_page/*.js',
                    'app/components/contest_page/*.js',
                    'app/components/festive_promo/*.js',
                    'app/components/info_customer_page/*.js',
                    'app/shared/directives/*.js',
                    'app/components/reset-password/*.js',
                    'app/components/write_review/*.js',
                    'app/components/event_booking/*.js',
                    'app/components/event_management/*.js',
                    'app/components/co_branding/*.js',
                    'app/shared/controllers/RootController.js',
                    'app/models/*.js',
                    'app/components/home',
                ]
            }
        },
        html2js: {
            options: {
                // custom options, see below
            },
            main: {
                src: ['app/**/*.tpl.html'],
                dest: 'app/templates/templates.js'
            }
        },
        concat: {
            options: {
                separator: ';',
            },
            dist: {
                src: [
                    'assets/js/svg_event/snap.svg-min.js',
                    'bower_components/jquery/dist/jquery.js',
                    'bower_components/angular/angular.js',
                    'bower_components/angular-sanitize/angular-sanitize.js',
                    'bower_components/angular-cache/dist/angular-cache.js',
                    'bower_components/angular-route/angular-route.js',
                    'bower_components/angular-cookie/angular-cookie.js',
                    'bower_components/ng-file-upload/ng-file-upload-all.min.js',
                    'bower_components/ng-file-upload/ng-file-upload-shim.min.js',
                    'bower_components/ui-bootstrap-tpls-0.14.2.min.js',
                    'bower_components/bootstrap/dist/js/bootstrap.js',
                    'bower_components/typeahead.js/dist/typeahead.jquery.js',
                    'bower_components/typeahead.js/dist/bloodhound.js',
                    'bower_components/jRating/jquery/jRating.jquery.js',
                    'bower_components/moment/min/moment.min.js',
                    'bower_components/moment/min/locales.min.js',
                    'bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
                    'bower_components/lodash/dist/lodash.js',
                    'bower_components/angulartics/src/angulartics.js',
                    'bower_components/angulartics/src/angulartics-gtm.js',
                    'bower_components/angular-notify/dist/angular-notify.js',
                    'bower_components/re-tree/re-tree.js',
                    'bower_components/ng-device-detector/ng-device-detector.js',
                    'bower_components/spin.js/spin.js',
                    'bower_components/phil/phil.js',
                    'bower_components/videogular-overlay-play/vg-overlay-play.js',
                    '/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
                    'assets/js/fresco/fresco.js',
                    'app/models/*.js',
                    'app/api/*/*.js',
                    'app/api/api.js',
                    'app/shared/directives/*.js',
                    'app/shared/services/*.js',
                    'app/templates/templates.js',
                    'app/shared/filters/*.js',
                    'app/components/home/*.js',
                    'app/components/event_booking/*.js',
                    'app/components/search_page/*.js',
                    'app/components/restaurant_info_page/*.js',
                    'app/components/restaurant_group_page/*.js',
                    'app/components/mybookings_page/*.js',
                    'app/components/myaccount_page/*.js',
                    'app/components/myreviews_page/*.js',
                    'app/components/myorders_page/*.js',
                    'app/components/catering_page/*.js',
                    'app/components/booking/*.js',
                    'app/components/info_contact_page/*.js',
                    'app/components/info_partner_page/*.js',
                    'app/components/faq_page/*.js',
                    'app/components/how_it_work_page/*.js',
                    'app/components/all_rewards_page/*.js',
                    'app/components/all_rewards_cpp_page/*.js',
                    'app/components/terms_and_conditions_of_service_page/*.js',
                    'app/components/privacy_policy_page/*.js',
                    'app/components/z_landing_mobile/*.js',
                    'app/components/404_page/*.js',
                    'app/components/checkout/*.js',
                    'app/components/payment_success_page/*.js',
                    'app/components/checkout_info/*.js',
                    'app/components/write_review/*.js',
                    'app/components/reset-password/*.js',
                    'app/components/event_page/*.js',
                    'app/components/cny_page/*.js',
                    'app/components/mother_day_page/*.js',
                    'app/components/festive_promo/*.js',
                    'app/components/info_customer_page/*.js',
                    'app/components/father_day_page/*.js',
                    'app/components/v_day_page/*.js',
                    'app/components/dsb_page/*.js',
                    'app/components/contest_page/*.js',
                    'app/components/event_booking/*.js',
                    'app/components/event_management/*.js',
                    'app/components/section_booking/*.js',
                    'app/components/payment/*.js',
                    'app/components/co_branding/*.js',
                    'app/shared/controllers/*.js',
                    'app/config.js',
                    'app/app.js',
                    'app/routes.js'
                ],
                dest: 'weeloy.js',
            }
        },
        watch: {
            'scss': {
                files: ['assets/css/scss/**/*.scss'],
                tasks: ['sass'],
            },
            css: {
                files: ['assets/css/weeloy.css'],
                tasks: ['concat_css', 'cssmin']
            },
            html2js: {
                files: ['app/**/*.tpl.html'],
                tasks: ['html2js']
            },
            scripts: {
                files: [
                    'app/*.js',
                    'app/**/*.js'
                ],
                //tasks: ['jshint', 'concat', 'uglify'],
                //tasks: ['jshint', 'concat'],
                //
                tasks: ['concat'],
                options: {
                    spawn: false,
                }
            }
        },
        sprite: {
            all: {
                src: 'assets/images/home_picture/*.png',
                dest: 'assets/images/sprites/header_footer_sprites.png',
                destCss: 'assets/images/sprites/css/header_footer_sprites.css'
            }
        },
        penthouse: {
            extract: {
                outfile: 'assets/css/out.css',
                css: 'assets/css/app.css',
                url: 'http://localhost:8888/weeloy.com',
                width: 1300,
                height: 900,
                skipErrors: false // this is the default 
            }
        },
        criticalcss: {
            custom: {
                options: {
                    url: "http://localhost:8888/weeloy.com",
                    width: 1800,
                    height: 900,
                    outputfile: "assets/css/out2.css",
                    filename: "assets/css/app.css", // Using path.resolve( path.join( ... ) ) is a good idea here 
                    buffer: 3000 * 1024,
                    ignoreConsole: false
                }
            }
        }
    });

    // Load the plugin that provides the "uglify" task.
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-concat-css');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-html2js');
    grunt.loadNpmTasks('grunt-spritesmith');
    grunt.loadNpmTasks('grunt-penthouse');
    grunt.loadNpmTasks('grunt-criticalcss');
    grunt.loadNpmTasks('load-grunt-tasks');

    // Default task(s).
    grunt.registerTask('default', ['sass', 'concat_css', 'cssmin', 'html2js', 'jshint', 'concat', 'uglify']);

};
